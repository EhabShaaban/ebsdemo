package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.inventory.stocktransformation.statemachines.DObStockTransformationStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObStockTransformationBeans {
    @Bean("stockTransformationAllowedActionService")
    @DependsOn({"authorizationService"})
    public AllowedActionService createStockTransformationAllowedActionService(
            AuthorizationService authorizationService) throws Exception {
        return new AllowedActionService(authorizationService, new DObStockTransformationStateMachine());
    }
}
