package com.ebs.dda.config;

import org.apache.commons.scxml2.model.ModelException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.masterdata.chartofaccounts.HObGlAccountStateMachine;

@Configuration
public class HObGLAccountBeans {

  @Bean("chartOfAccountsAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createChartOfAccountsAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new HObGlAccountStateMachine());
  }
}
