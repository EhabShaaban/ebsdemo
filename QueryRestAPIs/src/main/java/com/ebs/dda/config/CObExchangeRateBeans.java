package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.masterdata.exchangerate.CObExchangeRateStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class CObExchangeRateBeans {
    @Bean("exchangeRateAllowedActionService")
    @DependsOn({"authorizationService"})
    public AllowedActionService createExchangeRateAllowedActionService(
            AuthorizationService authorizationService) throws Exception {
        return new AllowedActionService(authorizationService, new CObExchangeRateStateMachine());
    }
}
