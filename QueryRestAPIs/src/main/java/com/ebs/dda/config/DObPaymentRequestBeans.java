package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObPaymentRequestBeans {
  @Bean("paymentRequestAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createPaymentRequestAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObPaymentRequestStateMachine());
  }
}
