package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObSalesReturnOrderBeans {

  @Bean("salesReturnOrderAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createSalesReturnOrderAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObSalesReturnOrderStateMachine());
  }
}
