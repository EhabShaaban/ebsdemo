package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObSalesInvoiceBeans {

  @Bean("salesInvoiceAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createSalesInvoiceAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObSalesInvoiceWithoutReferenceStateMachine());
  }
}
