package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class ItemVendorRecordBeans {
  @Bean("itemVendorRecordAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createIVRAllowedActionService(
      AuthorizationService authorizationService) {
    return new AllowedActionService(authorizationService);
  }


}
