package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObGoodsIssueBeans {
    @Bean("goodsIssueAllowedActionService")
    @DependsOn({"authorizationService"})
    public AllowedActionService createGoodsIssueAllowedActionService(AuthorizationService authorizationService) throws Exception {
        return new AllowedActionService(authorizationService,new DObGoodsIssueStateMachine());
    }
}
