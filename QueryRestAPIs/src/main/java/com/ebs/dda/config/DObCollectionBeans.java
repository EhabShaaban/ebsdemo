package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObCollectionBeans {

  @Bean("collectionAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createCollectionAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObCollectionStateMachine());
  }

}
