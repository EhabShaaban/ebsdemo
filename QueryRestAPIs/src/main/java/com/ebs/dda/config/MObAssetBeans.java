package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.masterdata.assetmasterdata.MObAssetStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class MObAssetBeans {

  @Bean("assetMasterDataAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAssetMasterDataAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new MObAssetStateMachine());
  }
}
