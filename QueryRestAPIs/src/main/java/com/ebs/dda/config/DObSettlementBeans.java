package com.ebs.dda.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;

@Configuration
public class DObSettlementBeans {

  @Bean("settlementAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createSettlementAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObSettlementStateMachine());
  }
}
