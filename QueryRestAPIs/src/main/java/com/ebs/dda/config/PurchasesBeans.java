package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class PurchasesBeans {

  @Bean("purchaseOrderAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createImportPurchaseOrderAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    AllowedActionService allowedActionService =
        new AllowedActionService(authorizationService, new DObImportPurchaseOrderStateMachine());
    return allowedActionService;
  }
}
