package com.ebs.dda.config;

import com.ebs.dac.aop.logging.LoggerAspect;
import com.ebs.dac.dbo.jpa.repositories.BusinessObjectRepository;
import com.ebs.dac.spring.configs.RepositoryFactoryBean;
import com.ebs.dac.spring.configs.ShiroConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        repositoryFactoryBeanClass = RepositoryFactoryBean.class,
        repositoryBaseClass = BusinessObjectRepository.class,
        basePackages = {
                "com.ebs.dac.security.jpa.repositories",
                "com.ebs.dda.masterdata.dbo.jpa.repositories.*",
                "com.ebs.dda.repositories.masterdata.*",
                "com.ebs.dda.purchases.dbo.jpa.repositories.*",
                "com.ebs.dda.accounting.dbo.jpa.repositories",
                "com.ebs.dda.sales.dbo.jpa.entities.repositories",
                "com.ebs.dda.repositories.inventory",
                "com.ebs.dda.repositories.accounting",
                "com.ebs.dda.repositories.order"
        })
@Import({
        PurchasesBeans.class,
        DObGoodsReceiptBeans.class,
        DObInitialStockUploadBeans.class,
        DObInitialBalanceUploadBeans.class,
        VendorBeans.class,
        ItemBeans.class,
        ItemVendorRecordBeans.class,
        ItemGroupBean.class,
        DObPaymentRequestBeans.class,
        DObLandedCostBeans.class,
        DObNotesReceivablesBeans.class,
        CObExchangeRateBeans.class,
        DObInvoiceBeans.class,
        HObGLAccountBeans.class,
        DObGoodsIssueBeans.class,
        DObSalesInvoiceBeans.class,
        DObCollectionBeans.class,
        DObCostingBeans.class,
        DObSalesOrderBeans.class,
        DObStockTransformationBeans.class,
        DObSalesReturnOrderBeans.class,
        MObAssetBeans.class,
        DObSettlementBeans.class,
        MObCustomerBeans.class,
        LoggerAspect.class

})
@ComponentScan({
        "com.ebs.dac.restexceptionhandlers",
        "com.ebs.dac.requestbodyadvice",
        "com.ebs.dda.rest.inventory.goodsreceipt",
        "com.ebs.dda.rest.accounting",
        "com.ebs.dda.rest.masterdata.exchangerate",
        "com.ebs.dda.rest.masterdata.customer",
        "com.ebs.dda.rest.inventory.goodsissue",
        "com.ebs.dda.rest.masterdata.enterprise",
        "com.ebs.dda.rest.inventory.storetransaction",
        "com.ebs.dda.rest.accounting.notesreceivables",
        "com.ebs.dda.rest.accounting.collection",
        "com.ebs.dda.rest.order",
        "com.ebs.dda.rest.inventory.stocktransformation",
        "com.ebs.dda.rest.inventory.initialstockupload",
        "com.ebs.dda.rest.masterdata.assetmasterdata"
})
public class QueryApplicationLauncher {

    private static final Logger log = LoggerFactory.getLogger(QueryApplicationLauncher.class);

    public static void main(String[] args) {
        log.info(
                ""
                        + SpringApplication.run(QueryApplicationLauncher.class, args)
                        .getBean(ShiroConfig.FilterChainDefinitionInterceptor.class)
                        .getMap());
    }
}
