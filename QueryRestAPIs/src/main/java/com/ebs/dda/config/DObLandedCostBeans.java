package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObLandedCostBeans {
  @Bean("landedCostAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createLandedCostAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new DObLandedCostStateMachine());
  }
}
