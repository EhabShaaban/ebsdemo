package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.masterdata.customer.MObCustomerStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class MObCustomerBeans {

  @Bean("customerAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createCustomerAllowedActionService(
      AuthorizationService authorizationService) throws Exception {
    return new AllowedActionService(authorizationService, new MObCustomerStateMachine());
  }
}
