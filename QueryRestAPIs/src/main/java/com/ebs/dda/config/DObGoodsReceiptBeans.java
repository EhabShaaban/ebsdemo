package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObGoodsReceiptBeans {

  @Bean("goodsReceiptAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAllowedActionService(AuthorizationService authorizationService) throws Exception {
    AllowedActionService allowedActionService = new AllowedActionService(authorizationService, new DObGoodsReceiptStateMachine());
    return allowedActionService;
  }
}
