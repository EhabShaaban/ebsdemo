package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.accounting.initialbalanceupload.statemachines.DObInitialBalanceUploadStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObInitialBalanceUploadBeans {

  @Bean("initialBalanceUploadAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAllowedActionService(AuthorizationService authorizationService)
      throws Exception {
      return new AllowedActionService(authorizationService, new DObInitialBalanceUploadStateMachine());
  }
}
