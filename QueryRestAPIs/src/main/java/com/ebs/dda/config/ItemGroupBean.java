package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class ItemGroupBean {
    @Bean("itemGroupAllowedActionService")
    @DependsOn({"authorizationService"})
    public AllowedActionService createItemGroupAllowedActionService(
            AuthorizationService authorizationService) {
        return new AllowedActionService(authorizationService);
    }
}
