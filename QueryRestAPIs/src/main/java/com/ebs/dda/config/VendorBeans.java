package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.masterdata.vendor.MObVendorStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class VendorBeans {

  @Bean("vendorAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAllowedActionService(AuthorizationService authorizationService)
      throws Exception {
    AllowedActionService allowedActionService =
        new AllowedActionService(authorizationService, new MObVendorStateMachine());
    return allowedActionService;
  }
}
