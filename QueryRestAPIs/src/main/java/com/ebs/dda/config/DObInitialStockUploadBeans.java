package com.ebs.dda.config;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.inventory.initialstockupload.statemachines.DObInitialStockUploadStateMachine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObInitialStockUploadBeans {

  @Bean("initialStockUploadAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAllowedActionService(AuthorizationService authorizationService)
      throws Exception {
    AllowedActionService allowedActionService =
        new AllowedActionService(authorizationService, new DObInitialStockUploadStateMachine());
    return allowedActionService;
  }
}
