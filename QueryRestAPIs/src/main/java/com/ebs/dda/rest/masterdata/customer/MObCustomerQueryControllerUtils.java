package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import java.util.List;

public class MObCustomerQueryControllerUtils {

  private final AuthorizationManager authorizationManager;

  public MObCustomerQueryControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void authorizeActionOnCustomer(
      List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList, String actionName) {
    boolean isAuthorized = false;
    Exception exception = null;
    for (IObCustomerBusinessUnitGeneralModel customerBusinessUnit : customerBusinessUnitsList) {
      try {
        authorizationManager.authorizeAction(customerBusinessUnit, actionName);
        isAuthorized = true;
        break;
      } catch (Exception e) {
        exception = e;
      }
    }
    if (!isAuthorized) {
      throw new AuthorizationException(exception);
    }
  }
}
