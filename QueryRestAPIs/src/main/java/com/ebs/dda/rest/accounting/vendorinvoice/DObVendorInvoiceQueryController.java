package com.ebs.dda.rest.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObInvoiceAuthorizationManager;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.dbo.jpa.repositories.specifications.ReadPurchaseOrderSpecification;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceActionNames;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorItemsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.*;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorItemsGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/accounting/vendorinvoice")
public class DObVendorInvoiceQueryController extends BasicQueryController {

  @Autowired DObPurchaseOrderGeneralModelRep purchaseOrderHeaderGeneralModelRep;
  @Autowired DObInvoiceAuthorizationManager invoiceAuthorizationManager;

  @Autowired DObVendorInvoiceGeneralModelRep dObInvoiceGeneralModelRep;

  @Autowired IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep;

  @Autowired IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;

  @Autowired IObVendorInvoiceDetailsGeneralModelRep dObInvoiceDetailsGeneralModelRep;
  @Autowired CObCompanyGeneralModelRep companyGeneralModelRep;

  @Autowired private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;

  @Autowired private MObItemGeneralModelRep itemGeneralModelRep;

  @Autowired
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;

  @Autowired private IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep;

  @Autowired
  @Qualifier("invoiceAllowedActionService")
  private AllowedActionService invoiceAllowedActionService;

  @Autowired private IObVendorInvoiceCompanyDataGeneralModelRep companyGMRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  @Autowired
  private MObVendorItemsGeneralModelRep vendorItemsGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/purchaseorders/{invoiceTypeCode}/{vendorCode}")
  public @ResponseBody String readConfirmedOrLaterPurchaseOrders(
      @PathVariable String invoiceTypeCode, @PathVariable String vendorCode) {
    boolean allowedReadImportPO =
        isReadPermissionAllowed(IDObPurchaseOrderGeneralModel.PO_SYS_NAME, READ_ALL);

    checkIfAuthorizedOnAtLeastReadOneType(allowedReadImportPO);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    ReadPurchaseOrderSpecification purchaseOrderSpecification =
        new ReadPurchaseOrderSpecification();
    Specification<DObPurchaseOrderGeneralModel> authorizedPurchaseUnit =
        purchaseOrderSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);
    List purchaseOrders = new ArrayList<>();
    purchaseOrders =
        purchaseOrderHeaderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(
                    purchaseOrderSpecification.isAllowedPurchaseOrder(invoiceTypeCode, vendorCode)),
            Sort.by(Sort.Direction.DESC, IDObVendorInvoiceGeneralModel.INVOICE_CODE));

    return serializeRESTSuccessResponse(purchaseOrders);
  }

  private void checkIfAuthorizedOnAtLeastReadOneType(boolean allowedReadPO) {
    if (!allowedReadPO) {
      throw new AuthorizationException("User is not authorized to read any PO type.");
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        invoiceAllowedActionService.getHomeScreenAllowedActions(DObVendorInvoiceGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{invoiceCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String invoiceCode)
      throws Exception {
    DObVendorInvoiceGeneralModel invoice = dObInvoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    if (invoice == null) {
      throw new InstanceNotExistException();
    }

    Set<String> allowedActions = invoiceAllowedActionService.getObjectScreenAllowedActions(invoice);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{invoiceCode}/generaldata")
  public @ResponseBody String readInvoiceGeneralData(@PathVariable String invoiceCode)
      throws Exception {

    DObVendorInvoiceGeneralModel invoiceGeneralData =
        dObInvoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    checkIfEntityIsNull(invoiceGeneralData);
    invoiceAuthorizationManager.authorizeReadGeneralData(invoiceGeneralData);

    return serializeRESTSuccessResponse(invoiceGeneralData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{vendorInvoiceCode}/summary")
  public @ResponseBody String readVendorInvoiceSummaryData(@PathVariable String vendorInvoiceCode)
      throws Exception {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        dObInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);

    if (vendorInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    invoiceAuthorizationManager.authorizeReadInvoiceSummary(vendorInvoiceGeneralModel);
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
            vendorInvoiceCode, IDocumentTypes.VENDOR_INVOICE);
    return serializeRESTSuccessResponse(invoiceSummaryGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME, READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObVendorInvoiceGeneralModel.INVOICE_CODE));

    Specification querySpecification = getInvoiceDataSpecification(status, conditions);

    Page<DObVendorInvoiceGeneralModel> invoicesPage =
        dObInvoiceGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObVendorInvoiceGeneralModel> invoices = invoicesPage.getContent();
    return serializeViewAllResponse(invoices, invoicesPage.getTotalElements());
  }

  private Specification getInvoiceDataSpecification(Status status, Set<String> conditions) {

    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IDObVendorInvoiceGeneralModel.PRUCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObVendorInvoiceGeneralModel.INVOICE_CODE);

    SimpleStringFieldSpecification invoiceTypeSpecification =
        getSimpleStringSpecification(status, IDObVendorInvoiceGeneralModel.INVOICE_TYPE);

    JsonFieldSpecification purchaseUnitNameSpecification =
        getJsonContainsSpecification(status, IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_NAME);

    SimpleStringFieldSpecification vendorCodeNameSpecification =
        getSimpleStringSpecification(status, IDObVendorInvoiceGeneralModel.VENDOR_CODE_NAME);

    SimpleStringFieldSpecification purchaseOrderCodeSpecification =
        getSimpleStringSpecification(status, IDObVendorInvoiceGeneralModel.PURCHASE_ORDER_CODE);

    SimpleStringFieldSpecification invoiceNumberSpecification =
        getSimpleStringSpecification(status, IDObVendorInvoiceGeneralModel.INVOICE_NUMBER);

    SimpleStringFieldSpecification currentStateSpecification =
        getSimpleStringSpecification(status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

    Specification querySpecification =
        Specification.where(userCodeSpecification)
            .and(purchaseUnitConditionSpecification)
            .and(invoiceTypeSpecification)
            .and(vendorCodeNameSpecification)
            .and(purchaseOrderCodeSpecification)
            .and(invoiceNumberSpecification)
            .and(currentStateSpecification)
            .and(purchaseUnitNameSpecification);

    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{invoiceCode}/invoicedetails")
  public @ResponseBody String readInvoiceDetails(@PathVariable String invoiceCode)
      throws Exception {

    IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel =
        dObInvoiceDetailsGeneralModelRep.findByInvoiceCode(invoiceCode);
    checkIfEntityIsNull(invoiceDetailsGeneralModel);
    invoiceAuthorizationManager.authorizeReadSection(
        invoiceDetailsGeneralModel, IDObInvoiceActionNames.READ_INVOICE_DETAILS);
    return serializeRESTSuccessResponse(invoiceDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{invoiceCode}/items")
  public @ResponseBody String readInvoiceItems(@PathVariable String invoiceCode) throws Exception {

    DObVendorInvoiceGeneralModel invoice = dObInvoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    checkIfEntityIsNull(invoice);
    invoiceAuthorizationManager.authorizeReadItems(invoice);

    List<IObVendorInvoiceItemsGeneralModel> invoiceItemsGeneralModel =
        invoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdDesc(invoiceCode);

    return serializeRESTSuccessResponse(invoiceItemsGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/businesspartneritems/{vendorInvoiceCode}/{vendorCode}/{purchaseUnitCode}")
  public @ResponseBody String readItemsByVendorCodeAndPurchaseUnitCode(
          @PathVariable String vendorInvoiceCode, @PathVariable String vendorCode, @PathVariable String purchaseUnitCode) {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, READ_ALL);
    List<MObVendorItemsGeneralModel> allVendorItems = vendorItemsGeneralModelRep.findByVendorCodeAndBusinessUnitCodeOrderByItemCodeDesc(vendorCode, purchaseUnitCode);
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems = invoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdDesc(vendorInvoiceCode);

    List<MObVendorItemsGeneralModel> distinctVendorItemsNotAddedToInvoiceList = getVendorItemsThatNotAddedToTheVendorInvoiceBefore(allVendorItems, vendorInvoiceItems);
    return serializeRESTSuccessResponse(distinctVendorItemsNotAddedToInvoiceList);
  }

  private List<MObVendorItemsGeneralModel> getVendorItemsThatNotAddedToTheVendorInvoiceBefore(List<MObVendorItemsGeneralModel> allVendorItems, List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems) {
    List<MObVendorItemsGeneralModel> vendorItemsNotAddedToInvoiceList = allVendorItems.stream().filter(vendorItem -> vendorInvoiceItems.stream().noneMatch(invoiceItem -> invoiceItem.getItemCode().equals(vendorItem.getItemCode()) && invoiceItem.getOrderUnitCode().equals(vendorItem.getIvrMeasureCode())))
            .collect(Collectors.toList());
    return vendorItemsNotAddedToInvoiceList.stream()
            .filter(distinctByKey(MObVendorItemsGeneralModel::getItemCode))
            .collect(Collectors.toList());
  }

  public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
    Map<Object, Boolean> uniqueMap = new ConcurrentHashMap<>();
    return t -> uniqueMap.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }
  @RequestMapping(
      method = RequestMethod.GET,
      value = "/businesspartneritemsunits/{vendorInvoiceCode}/{itemCode}/{vendorCode}/{purchaseUnitCode}")
  public @ResponseBody String readPurchaseOrderItems(
      @PathVariable String vendorInvoiceCode,
      @PathVariable String itemCode,
      @PathVariable String vendorCode,
      @PathVariable String purchaseUnitCode)
      {
      authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, READ_ALL);
      List<MObVendorItemsGeneralModel> allVendorItems = vendorItemsGeneralModelRep.findByVendorCodeAndBusinessUnitCodeAndItemCodeOrderByIvrMeasureCodeDesc(vendorCode, purchaseUnitCode, itemCode);
      List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems = invoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdDesc(vendorInvoiceCode);
      List<MObVendorItemsGeneralModel> distinctVendorItemsUnitsNotAddedToInvoiceList = getVendorItemUnitsThatNotAddedToTheVendorInvoiceBefore(allVendorItems, vendorInvoiceItems);
      return serializeRESTSuccessResponse(distinctVendorItemsUnitsNotAddedToInvoiceList);
  }
  private List<MObVendorItemsGeneralModel> getVendorItemUnitsThatNotAddedToTheVendorInvoiceBefore(List<MObVendorItemsGeneralModel> allVendorItems, List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems) {
    return allVendorItems.stream().filter(vendorItem -> vendorInvoiceItems.stream().noneMatch(invoiceItem -> invoiceItem.getItemCode().equals(vendorItem.getItemCode()) && invoiceItem.getOrderUnitCode().equals(vendorItem.getIvrMeasureCode())))
            .collect(Collectors.toList());
  }
  @RequestMapping(method = RequestMethod.GET, value = "/{invoiceTypeCode}/{purchaseOrderCode}")
  public @ResponseBody String readInvoiceByTypeAndPurchaseOrderCode(
      @PathVariable String invoiceTypeCode, @PathVariable String purchaseOrderCode) {
    authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME, READ_ALL);
    List<DObVendorInvoiceGeneralModel> invoice =
        dObInvoiceGeneralModelRep.findAllByInvoiceTypeAndPurchaseOrderCode(
            invoiceTypeCode, purchaseOrderCode);
    return serializeRESTSuccessResponse(invoice);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/company/currencyiso")
  public @ResponseBody String getCompanyDataForVendorInvoicePost(@PathVariable String invoiceCode)
      throws Exception {

    DObVendorInvoiceGeneralModel invoiceGeneralModel =
        dObInvoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    invoiceAuthorizationManager.authorizeCreateJournalEntryForInvoice(invoiceGeneralModel);

    CObCompanyGeneralModel companyGeneralModel =
        companyGeneralModelRep.findOneByUserCode(invoiceGeneralModel.getCompanyCode());
    return serializeRESTSuccessResponse(companyGeneralModel.getCurrencyIso());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{vendorInvoiceCode}/taxes")
  public @ResponseBody String readTaxes(@PathVariable String vendorInvoiceCode) throws Exception {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        dObInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);

    if (vendorInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    invoiceAuthorizationManager.authorizeReadTaxes(vendorInvoiceGeneralModel);
    List<IObVendorInvoiceTaxesGeneralModel> vendorInvoiceTaxesGeneralModels =
        vendorInvoiceTaxesGeneralModelRep.findByInvoiceCode(vendorInvoiceCode);
    return serializeRESTSuccessResponse(vendorInvoiceTaxesGeneralModels);
  }

  @GetMapping("/{vendorInvoiceCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String vendorInvoiceCode)
      throws Exception {
    IObVendorInvoiceCompanyDataGeneralModel companyGeneralModel =
        companyGMRep.findOneByDocumentCode(vendorInvoiceCode);
    checkIfEntityIsNull(companyGeneralModel);
    invoiceAuthorizationManager.authorizeReadCompanyData(companyGeneralModel);
    return serializeRESTSuccessResponse(companyGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{vendorInvoiceCode}/postingdetails")
  public @ResponseBody String readPostingDetailsData(@PathVariable String vendorInvoiceCode)
      throws Exception {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        dObInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);
    if (vendorInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    invoiceAuthorizationManager.authorizeActionOnSection(
        vendorInvoiceGeneralModel, IDObInvoiceActionNames.READ_POSTING_DETAILS);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            vendorInvoiceCode, IDocumentTypes.VENDOR_INVOICE);
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readVendorInvoicesTypes() {
    return serializeRESTSuccessResponse(Arrays.asList(VendorInvoiceTypeEnum.values()));
  }
}
