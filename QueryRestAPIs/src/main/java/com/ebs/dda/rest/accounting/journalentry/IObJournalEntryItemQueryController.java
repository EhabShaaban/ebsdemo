package com.ebs.dda.rest.accounting.journalentry;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import com.ebs.dda.accounting.journalentry.apis.IIObJournalEntryActionNames;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IIObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DateContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;

@RestController
@RequestMapping(value = "/services/accounting/journalentry/items")
public class IObJournalEntryItemQueryController extends BasicQueryController {

  @Autowired private IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(
        IDObJournalEntry.SYS_NAME, IIObJournalEntryActionNames.READ_ITEM_SECTION);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(
                Sort.Direction.DESC,
                IIObJournalEntryItemGeneralModel.CODE,
                IIObJournalEntryItemGeneralModel.ID));
    Specification querySpecification = getJournalEntryItemDataSpecification(status, conditions);
    Page pageResult = journalEntryItemsGeneralModelRep.findAll(querySpecification, pageRequest);
    List<IObJournalEntryItemGeneralModel> journalEntryItems = pageResult.getContent();
    long totalNumberOfRecords = pageResult.getTotalElements();

    return serializeViewAllResponse(journalEntryItems, totalNumberOfRecords);
  }

  private Specification getJournalEntryItemDataSpecification(
      Status status, Set<String> conditions) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification purchaseUnitCodeConditionSpecification =
            getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_CODE);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.CODE);

    DateContainsSpecification dueDateSpecification =
        getDateContainsSpecification(status, IIObJournalEntryItemGeneralModel.DUE_DATE);

    SimpleStringFieldSpecification subledgerSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.SUBLEDGER);

    SimpleStringFieldSpecification accountCodeSpecification =
        getCustomStringContainsSpecification(
            status, IIObJournalEntryItemGeneralModel.ACCOUNT, CODE);
    JsonFieldSpecification accountNameSpecification =
        getCustomJsonContainsSpecification(status, IIObJournalEntryItemGeneralModel.ACCOUNT, NAME);

    SimpleStringFieldSpecification subAccountCodeSpecification =
        getCustomStringContainsSpecification(
            status, IIObJournalEntryItemGeneralModel.SUB_ACCOUNT, CODE);
    JsonFieldSpecification subAccountNameSpecification =
        getCustomJsonContainsSpecification(
            status, IIObJournalEntryItemGeneralModel.SUB_ACCOUNT, NAME);

    SimpleStringFieldSpecification debitSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.DEBIT);
    SimpleStringFieldSpecification creditSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.CREDIT);

    SimpleStringFieldSpecification documentCurrencyCodeSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.DOCUMENT_CURRENCY_CODE);

    SimpleStringFieldSpecification companyCurrencyCodeSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_CODE);

    SimpleStringFieldSpecification companyGroupCurrencyCodeSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_CODE);

    SimpleStringFieldSpecification companyCurrencyPriceSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_PRICE);

    SimpleStringFieldSpecification companyGroupCurrencyPriceSpecification =
        getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_PRICE);

    SimpleStringFieldSpecification companySpecification =
            getSimpleStringSpecification(status, IIObJournalEntryItemGeneralModel.COMPANY_CODE);

    Specification querySpecification = Specification.where(purchaseUnitConditionSpecification)
            .and(userCodeSpecification).and(dueDateSpecification)
            .and(subledgerSpecification)
            .and(accountCodeSpecification.or(accountNameSpecification))
            .and(subAccountCodeSpecification.or(subAccountNameSpecification))
            .and(debitSpecification).and(creditSpecification)
            .and(documentCurrencyCodeSpecification).and(companyCurrencyCodeSpecification)
            .and(companyGroupCurrencyCodeSpecification)
            .and(companyCurrencyPriceSpecification)
            .and(companySpecification)
            .and(purchaseUnitCodeConditionSpecification)
            .and(companyGroupCurrencyPriceSpecification);

	return querySpecification;
  }
}
