package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObPaymentRequestAuthorizationManager;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestActionNames;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModel;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObOrderPaymentGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObOrderPaymentGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/services/accounting/paymentrequest")
public class DObPaymentRequestQueryController extends BasicQueryController
    implements InitializingBean {

  private static final String SERVICE_ITEM_TYPE = "SERVICE";

  private DObPaymentRequestQueryControllerUtils controllerUtils;

  @Autowired
  @Qualifier("paymentRequestAllowedActionService")
  private AllowedActionService paymentRequestAllowedActionService;

  @Autowired private IUserAccountSecurityService userAccountSecurityService;
  @Autowired private DObPaymentRequestGeneralModelRep dObPaymentRequestGeneralModelRep;

  @Autowired
  private IObPaymentRequestCompanyDataGeneralModelRep paymentRequestCompanyDataGeneralModelRep;

  @Autowired
  private IObPaymentRequestAccountDetailsGeneralModelRep accountingDetailsGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  @Autowired private DObPaymentRequestAuthorizationManager paymentRequestAuthorizationManager;
  @Autowired private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep;
  @Autowired private IObVendorInvoiceRemainingGeneralModelRep invoiceRemainingGeneralModelRep;
  @Autowired private DObOrderPaymentGeneralModelRep orderGeneralModelRep;
  @Autowired private DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep;

  @Autowired
  private IObPaymentRequestPaymentDetailsGeneralModelRep
      paymentRequestPaymentDetailsGeneralModelRep;

  @Autowired private MObItemGeneralModelRep itemGMRep;

  @Autowired private IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep;
  @Autowired private IObOrderCompanyGeneralModelRep purchaseOrderCompanySectionGeneralModelRep;

  @Autowired
  private IObVendorInvoiceCompanyDataGeneralModelRep vendorInvoiceCompanyDataGeneralModelRep;

  @Autowired private IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        paymentRequestAllowedActionService.getHomeScreenAllowedActions(DObPaymentRequest.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{paymentRequestCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequest =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    controllerUtils.checkIfEntityIsNull(paymentRequest);

    Set<String> allowedActions =
        paymentRequestAllowedActionService.getObjectScreenAllowedActions(paymentRequest);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentRequestCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    controllerUtils.checkIfEntityIsNull(paymentRequestGeneralModel);
    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IActionsNames.READ_ALL);
    return serializeRESTSuccessResponse(paymentRequestGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentRequestCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);

    controllerUtils.checkIfEntityIsNull(paymentRequestGeneralModel);

    IObPaymentRequestCompanyDataGeneralModel paymentRequestCompanyDataGeneralModel =
        paymentRequestCompanyDataGeneralModelRep.findOneByRefInstanceId(
            paymentRequestGeneralModel.getId());

    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.READ_COMPANY_DATA);

    return serializeRESTSuccessResponse(paymentRequestCompanyDataGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentRequestCode}/accountingdetails")
  public @ResponseBody String readAccountingDetailsData(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    controllerUtils.checkIfEntityIsNull(paymentRequestGeneralModel);

    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.READ_ACCOUNTING_DETAILS);

    List<IObPaymentRequestAccountDetailsGeneralModel> AccountDetailsGeneralModel;
    AccountDetailsGeneralModel =
        accountingDetailsGeneralModelRep.findByDocumentCodeOrderByAccountingEntryDesc(
            paymentRequestCode);

    return serializeRESTSuccessResponse(AccountDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentRequestCode}/postingdetails")
  public @ResponseBody String readPostingDetailsData(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.READ_POSTING_DETAILS);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            paymentRequestCode, IDocumentTypes.PAYMENT_REQUEST);
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentRequestCode}/paymentdetails")
  public @ResponseBody String readPaymentDetails(@PathVariable String paymentRequestCode)
      throws Exception {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    controllerUtils.checkIfEntityIsNull(paymentRequestGeneralModel);

    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.READ_PAYMENT_DETAILS);

    IObPaymentRequestPaymentDetailsGeneralModel paymentDetailsGeneralModel =
        paymentDetailsGeneralModelRep.findOneByUserCode(paymentRequestCode);
    return serializeRESTSuccessResponse(paymentDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/paymentforms")
  public @ResponseBody String getPaymentForm() {
    return serializeRESTSuccessResponse(PaymentFormEnum.PaymentForm.values());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/paymenttypes")
  public @ResponseBody String getPaymentType() {
    List<PaymentTypeEnum.PaymentType> paymentTypes =
        new ArrayList<>(Arrays.asList(PaymentTypeEnum.PaymentType.values()));
    //TODO: remove when implement Payment Against Credit/Debit type & Under Settlement
    List<PaymentTypeEnum.PaymentType> filteredPaymentTypes = paymentTypes.stream()
            .filter(type -> !type.equals(PaymentType.BASED_ON_CREDIT_NOTE) && !type.equals(PaymentType.UNDER_SETTLEMENT))
            .collect(Collectors.toList());
    return serializeRESTSuccessResponse(filteredPaymentTypes);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObPaymentRequest.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObPaymentRequestGeneralModel.USER_CODE));

    Specification querySpecification = getPaymentRequestDataSpecification(conditions, status);
    Page<DObPaymentRequestGeneralModel> paymentRequestsPage =
        dObPaymentRequestGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObPaymentRequestGeneralModel> paymentRequests = paymentRequestsPage.getContent();
    return serializeViewAllResponse(paymentRequests, paymentRequestsPage.getTotalElements());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{paymentType}/documenttypes")
  public @ResponseBody String getDocumentType(@PathVariable String paymentType) {
    List<Enum> dueDocumentTypes;
    if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name())) {
      dueDocumentTypes =
          new ArrayList<>(
              Arrays.asList(DueDocumentTypeEnum.GeneralPaymentDueDocumentType.values()));
    } else if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())) {
      dueDocumentTypes =
          new ArrayList<>(Arrays.asList(DueDocumentTypeEnum.VendorPaymentDueDocumentType.values()));
    } else if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.name())) {
      dueDocumentTypes =
          new ArrayList<>(
              Arrays.asList(DueDocumentTypeEnum.CreditNotePaymentDueDocumentType.values()));
    } else if (paymentType.equals(PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT.name())) {
      dueDocumentTypes = null;
    } else {
      dueDocumentTypes =
          new ArrayList<>(Arrays.asList(DueDocumentTypeEnum.DueDocumentType.values()));
    }
    return serializeRESTSuccessResponse(dueDocumentTypes);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}/{businessPartnerCode}/invoices")
  public @ResponseBody String getPostedInvoices(@PathVariable String businessUnitCode, @PathVariable String businessPartnerCode) {
    authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME, IActionsNames.READ_ALL);
    ReadVendorInvoiceSpecification invoiceSpecification = new ReadVendorInvoiceSpecification();
    List<IObVendorInvoiceRemainingGeneralModel> invoices =
        invoiceRemainingGeneralModelRep.findAll(
            Specification.where(
                    invoiceSpecification.isAllowedDobInvoice(
                            businessPartnerCode, businessUnitCode)),
            Sort.by(Sort.Direction.DESC, "id"));
    return serializeRESTSuccessResponse(invoices);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}/{businessPartnerCode}/{paymentType}/orders")
  public @ResponseBody String readOrdersByVendorCode(@PathVariable String businessUnitCode, @PathVariable String businessPartnerCode, @PathVariable String paymentType) {
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    ReadPurchaseOrderForPaymentRequestSpecification orderSpecification =
        new ReadPurchaseOrderForPaymentRequestSpecification();
    List<DObOrderPaymentGeneralModel> orders = new ArrayList<>();
    orders =
        orderGeneralModelRep.findAll(
            Specification.where(
                    orderSpecification.isAllowedDobOrder(
                            businessPartnerCode, paymentType, businessUnitCode)),
            Sort.by(Sort.Direction.DESC, "id"));
    return serializeRESTSuccessResponse(orders);
  }
  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}/{businessPartnerCode}/creditNotes")
  public @ResponseBody String readActivatedCreditNotes(@PathVariable String businessUnitCode, @PathVariable String businessPartnerCode) {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);
    ReadCreditNotesSpecification creditNotesSpecification = new ReadCreditNotesSpecification();
    List<DObAccountingNoteGeneralModel> creditNotes =
            accountingNoteGeneralModelRep.findAll(
                    Specification.where(
                            creditNotesSpecification.creditNoteInAllowedState()
                            .and(creditNotesSpecification.creditNotesAllowedRemaining())
                            .and(creditNotesSpecification.BusinessUnitIs(businessUnitCode))
                            .and(creditNotesSpecification.BusinessPartnerIs(businessPartnerCode))
                            .and(creditNotesSpecification.accountingNoteIsCredit())));

    creditNotes.sort(Comparator.comparing(DObAccountingNoteGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(creditNotes, creditNotes.size());
  }
  @RequestMapping(
      method = RequestMethod.GET,
      value = "/paymentdone/{documentCode}/{vendorCode}/{currencyCode}")
  public @ResponseBody String readPaymentDone(
      @PathVariable String documentCode,
      @PathVariable String vendorCode,
      @PathVariable String currencyCode) {
    List<IObPaymentRequestPaymentDetailsGeneralModel> paymentRequest =
        paymentRequestPaymentDetailsGeneralModelRep
            .findByCurrentStatesLikeAndDueDocumentCodeAndDueDocumentTypeLikeAndAndBusinessPartnerCodeAndCurrencyCode(
                "%" + DObPaymentRequestStateMachine.PAYMENT_DONE_STATE + "%",
                documentCode,
                DueDocumentTypeEnum.PURCHASEORDER,
                vendorCode,
                currencyCode);

    return serializeRESTSuccessResponse(paymentRequest);
  }

  private Specification getPaymentRequestDataSpecification(Set<String> conditions, Status status) {

    Specification querySpecification;

    List<String> businessUnitNames =
        getConditionAttributeValue(IDObPaymentRequestGeneralModel.PURCHASING_UNIT_NAME, conditions);
    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObPaymentRequestGeneralModel.PURCHASING_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.USER_CODE);

    SimpleStringFieldSpecification paymentRequestStateSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.STATE);

    SimpleStringFieldSpecification creationInfoSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.CREATION_INFO);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.PAYMENT_TYPE);

    SimpleStringFieldSpecification businessPartnerCodeSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.BUSINESS_PARTNER_CODE);

    Specification referenceDocumentSpecification = applyReferenceDocumentSpecification(status);

    SimpleStringFieldSpecification businessUnitSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.PURCHASE_UNIT_CODE);

    Specification amountSpecification = applyAmountSpecification(status);

    SimpleStringFieldSpecification bankTransRefSpecification =
        getSimpleStringSpecification(status, IDObPaymentRequestGeneralModel.BANK_TRANS_REF);

    DateContainsSpecification afterExpectedDueDate =
        getDateContainsSpecification(status, IDObPaymentRequestGeneralModel.FROM_EXPECTED_DUE_DATE);

    DateContainsSpecification beforeExpectedDueDate =
        getDateContainsSpecification(status, IDObPaymentRequestGeneralModel.TO_EXPECTED_DUE_DATE);

    querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(businessUnitSpecification)
            .and(paymentRequestStateSpecification)
            .and(userCodeSpecification)
            .and(creationInfoSpecification)
            .and(typeSpecification)
            .and(businessPartnerCodeSpecification)
            .and(referenceDocumentSpecification)
            .and(amountSpecification)
            .and(bankTransRefSpecification)
            .and(beforeExpectedDueDate)
            .and(afterExpectedDueDate);

    querySpecification = applyCreatedByConditionSpecification(querySpecification, conditions);
    return querySpecification;
  }

  private Specification applyAmountSpecification(Status status) {
    DoubleFieldSpecification amountValueSpecification =
        getCustomDoubleEqualsSpecification(
            status, IDObPaymentRequestGeneralModel.AMOUNT, IDObPaymentRequestGeneralModel.VALUE);

    SimpleStringFieldSpecification amountCurrencySpecification =
        getCustomStringContainsOperationSpecification(
            status, IDObPaymentRequestGeneralModel.AMOUNT, IDObPaymentRequestGeneralModel.CURRENCY);
    return amountValueSpecification.or(amountCurrencySpecification);
  }

  private Specification applyReferenceDocumentSpecification(Status status) {
    JsonFieldSpecification referenceDocumentTypeSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObPaymentRequestGeneralModel.REFERENCE_DOCUMENT,
            IDObPaymentRequestGeneralModel.TYPE);
    SimpleStringFieldSpecification referenceDocumentCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObPaymentRequestGeneralModel.REFERENCE_DOCUMENT,
            IDObPaymentRequestGeneralModel.CODE);
    return referenceDocumentCodeSpecification.or(referenceDocumentTypeSpecification);
  }

  private Specification applyCreatedByConditionSpecification(
      Specification querySpecification, Set<String> conditions) {
    List<String> creationNames =
        getConditionAttributeValue(IDObPaymentRequestGeneralModel.CREATION_INFO, conditions);
    if (!creationNames.isEmpty()
        && creationNames.get(0).contains(DObPaymentRequestQueryControllerUtils.LOGGED_IN_USER)) {
      String loggedinUserInfo = getLoggedInUserName();
      CreatedBySpecification createdByAuthorizationSpecification =
          new CreatedBySpecification(loggedinUserInfo);
      querySpecification = querySpecification.and(createdByAuthorizationSpecification);
    }
    return querySpecification;
  }

  private String getLoggedInUserName() {
    return userAccountSecurityService.getLoggedInUser().toString();
  }

  @GetMapping("/{paymentRequestCode}/cost-factor-items")
  public @ResponseBody String readCostFactorItems(@PathVariable String paymentRequestCode)
      throws Exception {
    List<String> purchaseUnitNames = authorizeAndGetConditions(paymentRequestCode);
    Specification<MObItemGeneralModel> filter = createFilter(purchaseUnitNames);
    List<MObItemGeneralModel> items =
        itemGMRep.findAll(filter, Sort.by(Sort.Direction.DESC, IMObItemGeneralModel.CODE));
    return serializeRESTSuccessResponse(items);
  }

  private List<String> authorizeAndGetConditions(String paymentRequestCode) throws Exception {
    try {
      authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);

      DObPaymentRequestGeneralModel paymentRequest =
          dObPaymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);

      authorizationManager.authorizeAction(paymentRequest, IActionsNames.READ_ALL);
      boolean isAllowedType =
          paymentRequest.getPaymentType().equals(PaymentType.OTHER_PARTY_FOR_PURCHASE.toString());
      if (!isAllowedType) {
        throw new AuthorizationException("This operation is not allowed for Vendor PR type");
      }

      return new ArrayList<>(Arrays.asList(paymentRequest.getPurchaseUnitNameEn()));
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  private Specification<MObItemGeneralModel> createFilter(List<String> purchaseUnitNames) {
    JsonListContainsSpecification<MObItemGeneralModel> isAllowedBusinessUnit =
        new JsonListContainsSpecification<>(
            IMObItemGeneralModel.PURCHASING_UNIT_NAMES, purchaseUnitNames);

    SimpleBooleanFieldSpecification<MObItemGeneralModel> isCostFactor =
        new SimpleBooleanFieldSpecification<>(
            new FilterCriteria(IMObItemGeneralModel.COST_FACTOR, "equals", "true"));

    SimpleStringFieldSpecification<MObItemGeneralModel> isServiceItem =
        new SimpleStringFieldSpecification<>(
            new FilterCriteria(IMObItemGeneralModel.ITEM_TYPE_CODE, "equals", SERVICE_ITEM_TYPE));

    Specification<MObItemGeneralModel> filter =
        Specification.where(isServiceItem).and(isCostFactor).and(isAllowedBusinessUnit);
    return filter;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{documentCode}/{documentType}/treasuries")
  public @ResponseBody String readCompanyTreasuries(
      @PathVariable String documentCode, @PathVariable String documentType) {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    String companyCode;
    if (documentType.equals(DueDocumentTypeEnum.INVOICE)) {
      IObVendorInvoiceCompanyDataGeneralModel vendorInvoiceCompanyData =
          vendorInvoiceCompanyDataGeneralModelRep.findOneByDocumentCode(documentCode);
      companyCode = vendorInvoiceCompanyData.getCompanyCode();
    } else {
      IObOrderCompanyGeneralModel purchaseOrderCompany =
          purchaseOrderCompanySectionGeneralModelRep.findByUserCode(documentCode);
      companyCode = purchaseOrderCompany.getCompanyCode();
    }
    List<IObCompanyTreasuriesGeneralModel> companiesTreasuries =
        treasuriesGeneralModelRep.findAllByCompanyCodeOrderByTreasuryCodeDesc(companyCode);
    return serializeRESTSuccessResponse(companiesTreasuries);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{companyCode}/treasuries")
  public @ResponseBody String readCompanyTreasuriesByCompanyCode(@PathVariable String companyCode) {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    List<IObCompanyTreasuriesGeneralModel> companiesTreasuries =
        companyTreasuriesGeneralModelRep.findAllByCompanyCodeOrderByTreasuryCodeDesc(companyCode);
    return serializeRESTSuccessResponse(companiesTreasuries);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    controllerUtils = new DObPaymentRequestQueryControllerUtils();
  }
}
