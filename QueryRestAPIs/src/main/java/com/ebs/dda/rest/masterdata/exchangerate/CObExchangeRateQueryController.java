package com.ebs.dda.rest.masterdata.exchangerate;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.exchangerate.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DateContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DoubleFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateTypeRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "services/masterdata/exchangerate")
public class CObExchangeRateQueryController extends BasicQueryController {
  @Autowired
  @Qualifier("exchangeRateAllowedActionService")
  private AllowedActionService exchangeRateAllowedActionService;

  @Autowired private CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep;

  @Autowired private CObExchangeRateTypeRep exchangeRateTypeRep;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        exchangeRateAllowedActionService.getHomeScreenAllowedActions(CObExchangeRate.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filters}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filters,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(ICObExchangeRate.SYS_NAME, IActionsNames.READ_ALL);

    Status status = constructStatus(pageNum, rowsNum, filters, request);

    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, ICObExchangeRateGeneralModel.EXCHANGE_RATE_CODE));

    Specification querySpecification = getExchangeRateDataSpecification(status);

    Page<CObExchangeRateGeneralModel> exchangeRateGeneralModelsPage =
        exchangeRateGeneralModelRep.findAll(querySpecification, pageRequest);
    List<CObExchangeRateGeneralModel> exchangeRateGeneralModels =
        exchangeRateGeneralModelsPage.getContent();
    return serializeViewAllResponse(
        exchangeRateGeneralModels, exchangeRateGeneralModelsPage.getTotalElements());
  }

  private Specification getExchangeRateDataSpecification(Status status) {

    SimpleStringFieldSpecification firstCurrencyIsoSpecification =
        getSimpleStringSpecification(status, ICObExchangeRateGeneralModel.FIRST_CURRENCY_ISO);

    SimpleStringFieldSpecification secondCurrencyIsoSpecification =
        getSimpleStringSpecification(status, ICObExchangeRateGeneralModel.SECOND_CURRENCY_ISO);

    DoubleFieldSpecification secondValueSpecification =
        getDoubleFieldSpecification(status, ICObExchangeRateGeneralModel.SECOND_VALUE);

    DateContainsSpecification validFromSpecification =
        getDateContainsSpecification(status, ICObExchangeRateGeneralModel.VALID_FROM);

    SimpleStringFieldSpecification createdBySpecification =
        getSimpleStringSpecification(status, ICObExchangeRateGeneralModel.CREATED_BY);

    SimpleStringFieldSpecification exRateTypeSpecification =
            getSimpleStringSpecification(status, ICObExchangeRateGeneralModel.EXCHANGE_RATE_TYPE);

    Specification querySpecification =
        Specification.where(firstCurrencyIsoSpecification)
            .and(secondCurrencyIsoSpecification)
            .and(secondValueSpecification)
            .and(validFromSpecification)
            .and(createdBySpecification)
            .and(exRateTypeSpecification);

    return querySpecification;
  }

  @GetMapping("/types")
  public @ResponseBody String readExchangeRateTypes(HttpServletRequest request) {

    authorizationManager.authorizeAction(ICObExchangeRate.SYS_NAME, IActionsNames.READ_ALL);
    List<CObExchangeRateType> exrateTypes = exchangeRateTypeRep.findAll();

    return serializeRESTSuccessResponse(exrateTypes);
  }
}
