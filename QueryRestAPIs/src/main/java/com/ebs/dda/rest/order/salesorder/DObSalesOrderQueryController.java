package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemUOMGeneralModel;
import com.ebs.dda.jpa.order.salesorder.*;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.accounting.salesinvoice.ItemsLastSalesPriceGeneralModelRep;
import com.ebs.dda.repositories.inventory.stockavailability.DObStockAvailabilityGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemUOMGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.*;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.masterdata.utils.MasterDataQueryControllerUtils;
import com.ebs.dda.rest.utils.Status;
import com.ebs.dda.sales.dbo.jpa.entities.GeneralModels.ItemsLastSalesPriceGeneralModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/order/salesorder")
public class DObSalesOrderQueryController extends BasicQueryController implements InitializingBean {

    public static final String AVAILABLE_QUANTITY = "availableQuantity";
    private String CONSUMABLE_ITEM_TYPE = "CONSUMABLE";
    private String SERVICE_ITEM_TYPE = "SERVICE";

    @Autowired
    private DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep;

    @Autowired
    private IObSalesOrderCompanyAndStoreDataGeneralModelRep companyAndStoreDataGeneralModelRep;

    @Autowired
    private IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep;
    @Autowired
    private IUserAccountSecurityService userAccountSecurityService;
    @Autowired
    private CObSalesOrderTypeRep salesOrderTypeRep;
    @Autowired
    private IObSalesOrderItemsGeneralModelRep salesOrderItemGeneralModelRep;
    @Autowired
    private IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep;
    @Autowired
    private ItemsLastSalesPriceGeneralModelRep itemsLastSalesPriceGeneralModelRep;
    @Autowired
    private DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep;
    @Autowired
    private IObSalesOrderCycleDatesRep salesOrderCycleDatesRep;
    @Autowired
    private MObItemUOMGeneralModelRep itemUOMGeneralModelRep;
    @Autowired
    private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;
    @Autowired
    private MObItemGeneralModelRep itemsRep;

    @Autowired
    @Qualifier("salesOrderAllowedActionService")
    private AllowedActionService salesOrderAllowedActionService;

    private MasterDataQueryControllerUtils masterDataQueryControllerUtils;

    @Override
    public void afterPropertiesSet() {
        masterDataQueryControllerUtils = new MasterDataQueryControllerUtils(authorizationManager);
        masterDataQueryControllerUtils.setItemsRep(itemsRep);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/salesordertypes")
    public @ResponseBody
    String readSalesOrderTypes() {
        authorizationManager.authorizeAction(ICObSalesOrderType.SYS_NAME, IActionsNames.READ_ALL);
        List<CObSalesOrderType> types = salesOrderTypeRep.findAll();
        return serializeRESTSuccessResponse(types);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
    public @ResponseBody
    String readAll(
            @PathVariable int pageNum,
            @PathVariable int rowsNum,
            @PathVariable String filter,
            HttpServletRequest request) {

        authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL);
        Set<String> conditions = authorizationManager.getPermissionConditions();

        Status status = constructStatus(pageNum, rowsNum, filter, request);
        PageRequest pageRequest =
                PageRequest.of(
                        pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObSalesOrderGeneralModel.USER_CODE));

        Specification querySpecification = getSalesOrderDataSpecification(conditions, status);

        Page<DObSalesOrderGeneralModel> salesOrderPage =
                dObSalesOrderGeneralModelRep.findAll(querySpecification, pageRequest);

        List<DObSalesOrderGeneralModel> salesOrders = salesOrderPage.getContent();

        return serializeViewAllResponse(salesOrders, salesOrderPage.getTotalElements());
    }

    private Specification getSalesOrderDataSpecification(Set<String> conditions, Status status) {

        SimpleStringFieldSpecification userCodeSpecification =
                getSimpleStringSpecification(status, IDObSalesOrderGeneralModel.USER_CODE);

        JsonFieldSpecification salesOrderTypeNameSpecification =
                getJsonContainsSpecification(status, IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_NAME);

        SimpleStringFieldSpecification customerCodeSpecification =
                getCustomStringContainsSpecification(
                        status, IDObSalesOrderGeneralModel.CUSTOMER, IDObSalesOrderGeneralModel.CODE);

        JsonFieldSpecification customerNameSpecification =
                getCustomJsonContainsSpecification(
                        status, IDObSalesOrderGeneralModel.CUSTOMER, IDObSalesOrderGeneralModel.NAME);

        JsonFieldSpecification salesOrderSalesResponsibleSpecification =
                getJsonContainsSpecification(status, IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME);

        DateContainsSpecification salesOrderExpectedDeliveryDateSpecification =
                getDateContainsSpecification(status, IDObSalesOrderGeneralModel.EXPECTED_DELIVERY_DATE);

        JsonFieldSpecification purchaseUnitNameSpecification =
                getJsonContainsSpecification(status, IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME);

        SimpleStringFieldSpecification currentStateSpecification =
                getSimpleStringSpecification(status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

        List<String> purchaseUnitNames =
                getConditionAttributeValue(IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME, conditions);
        SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
                new SinglePurchaseUnitNamesConditionSpecification(
                        purchaseUnitNames, IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME_EN);

        Specification querySpecification;

        List<String> salesResponsibleNames =
                getConditionAttributeValue(IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME, conditions);
        if (!salesResponsibleNames.isEmpty()
                && salesResponsibleNames
                .get(0)
                .contains(DObSalesOrderQueryControllerUtils.LOGGED_IN_USER)) {
            String loggedInUserInfo = getLoggedInUserName();
            ConditionalFilterSpecification conditionalFilterSpecification =
                    new ConditionalFilterSpecification(
                            loggedInUserInfo, IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME_EN);
            querySpecification =
                    Specification.where(userCodeSpecification)
                            .and(purchaseUnitConditionSpecification)
                            .and(salesOrderTypeNameSpecification)
                            .and(purchaseUnitNameSpecification)
                            .and(salesOrderSalesResponsibleSpecification)
                            .and(salesOrderExpectedDeliveryDateSpecification)
                            .and(currentStateSpecification)
                            .and(conditionalFilterSpecification)
                            .and(customerCodeSpecification)
                            .or(customerNameSpecification);
        } else {
            querySpecification =
                    Specification.where(userCodeSpecification)
                            .and(purchaseUnitConditionSpecification)
                            .and(salesOrderTypeNameSpecification)
                            .and(purchaseUnitNameSpecification)
                            .and(salesOrderSalesResponsibleSpecification)
                            .and(salesOrderExpectedDeliveryDateSpecification)
                            .and(currentStateSpecification)
                            .and(customerCodeSpecification)
                            .or(customerNameSpecification);
        }
        return querySpecification;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
    public @ResponseBody
    String readHomeScreenActions() throws Exception {
        Set<String> allowedActions =
                salesOrderAllowedActionService.getHomeScreenAllowedActions(DObSalesOrder.class);
        if (allowedActions.size() == 0) {
            throw new ResourceAccessException();
        }
        return serializeRESTSuccessResponse(allowedActions);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{salesOrderCode}")
    public @ResponseBody
    String readObjectScreenActions(@PathVariable String salesOrderCode)
            throws Exception {
        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        DObSalesOrderQueryControllerUtils.checkIfEntityIsNull(salesOrderGeneralModel);

        Set<String> allowedActions =
                salesOrderAllowedActionService.getObjectScreenAllowedActions(salesOrderGeneralModel);

        if (allowedActions.size() == 0) {
            throw new ResourceAccessException();
        }

        return serializeRESTSuccessResponse(allowedActions);
    }

    private String getLoggedInUserName() {
        return userAccountSecurityService.getLoggedInUser().toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/generaldata")
    public @ResponseBody
    String readGeneralData(@PathVariable String salesOrderCode)
            throws Exception {
        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        authorizationManager.authorizeActionOnObject(salesOrderGeneralModel, IActionsNames.READ_ALL);

        return serializeRESTSuccessResponse(salesOrderGeneralModel);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/companystoredata")
    public @ResponseBody
    String readCompanyAndStoreData(@PathVariable String salesOrderCode)
            throws Exception {

        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        authorizationManager.authorizeActionOnObject(
                salesOrderGeneralModel, IDObSalesOrderActionNames.READ_COMPANY_AND_STORE_DATA);

        IObSalesOrderCompanyAndStoreDataGeneralModel companyStoreDataGeneralModel =
                companyAndStoreDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);

        return serializeRESTSuccessResponse(companyStoreDataGeneralModel);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/salesorderdata")
    public @ResponseBody
    String readSalesOrderData(@PathVariable String salesOrderCode)
            throws Exception {

        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        authorizationManager.authorizeActionOnObject(
                salesOrderGeneralModel, IDObSalesOrderActionNames.READ_SALES_ORDER_DATA);

        IObSalesOrderDataGeneralModel salesOrderDataGeneralModel =
                salesOrderDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);

        return serializeRESTSuccessResponse(salesOrderDataGeneralModel);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/items")
    public @ResponseBody
    String ReadItems(@PathVariable String salesOrderCode) throws Exception {

        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        authorizationManager.authorizeActionOnObject(
                salesOrderGeneralModel, IDObSalesOrderActionNames.READ_ITEMS);

        JsonObject salesOrderItemSectionData = new JsonObject();
        constructSalesOrderItemSectionData(salesOrderCode, salesOrderItemSectionData);
        return serializeRESTSuccessResponse(salesOrderItemSectionData);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/unitofmeasures/{itemCode}")
    public @ResponseBody
    String readItemUoms(
            @PathVariable String salesOrderCode, @PathVariable String itemCode) throws Exception {
        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);
        if (salesOrderGeneralModel == null) {
            throw new AuthorizationException();
        }

        authorizationManager.authorizeActionOnObject(
                salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_ITEMS);

        Set<String> allowedActions =
                salesOrderAllowedActionService.getObjectScreenAllowedActions(salesOrderGeneralModel);
        if (!allowedActions.contains(IDObSalesOrderActionNames.UPDATE_ITEMS)) {
            throw new AuthorizationException();
        }

        List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
                itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
        try {

            if (itemPurchaseUnits.size() != 0) {
                String actionName = IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION;
                masterDataQueryControllerUtils.applyAuthorizationOnItemByAction(
                        itemPurchaseUnits, actionName);
            }
        } catch (ConditionalAuthorizationException ex) {
            AuthorizationException authorizationException = new AuthorizationException(ex);
            throw authorizationException;
        }

        List<JsonObject> itemUOMDetailsList = new ArrayList<JsonObject>();
        constructSalesOrderItemUOMData(salesOrderCode, itemCode, itemUOMDetailsList);

        return serializeRESTSuccessResponse(itemUOMDetailsList);
    }

    private void constructSalesOrderItemUOMData(
            String salesOrderCode, String itemCode, List<JsonObject> itemUOMDetailsList) {

        Gson gson = getGson();

        List<MObItemUOMGeneralModel> itemUOMGeneralModels =
                itemUOMGeneralModelRep.findAllByItemCodeOrderByUserCodeDesc(itemCode);
        List<IObSalesOrderItemGeneralModel> salesOrderItemGeneralModels =
                salesOrderItemGeneralModelRep.findBySalesOrderCodeAndItemCode(salesOrderCode, itemCode);
        for (IObSalesOrderItemGeneralModel item : salesOrderItemGeneralModels) {
            MObItemUOMGeneralModel uom =
                    itemUOMGeneralModelRep.findOneByItemCodeAndUserCode(
                            itemCode, item.getUnitOfMeasureCode());
            itemUOMGeneralModels.removeIf(u -> u.getUserCode().equals(uom.getUserCode()));
        }

        for (MObItemUOMGeneralModel itemUOMGeneralModel : itemUOMGeneralModels) {
            JsonObject salesOrderItemUOMData = new JsonObject();

            salesOrderItemUOMData.addProperty("UnitOfMeasure", gson.toJson(itemUOMGeneralModel));

            addLastSalesPriceToResponse(salesOrderCode, itemUOMGeneralModel, salesOrderItemUOMData);
            addAvailableQuantityToResponse(salesOrderCode, itemUOMGeneralModel, salesOrderItemUOMData);

            itemUOMDetailsList.add(salesOrderItemUOMData);
        }
    }

    private void addAvailableQuantityToResponse(
            String salesOrderCode,
            MObItemUOMGeneralModel itemUOMGeneralModel,
            JsonObject salesOrderItemUOMData) {

        IObSalesOrderCompanyAndStoreDataGeneralModel companyAndStoreDataGeneralModel =
                companyAndStoreDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);

        String itemCode = itemUOMGeneralModel.getItemCode();
        String uomCode = itemUOMGeneralModel.getUserCode();
        String itemType = itemUOMGeneralModel.getItemType();

        String companyCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getCompanyCode()
                        : null;

        String storeCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getStoreCode()
                        : null;

        BigDecimal availableQuantity = null;

        if (itemType.equals(SERVICE_ITEM_TYPE) || itemType.equals(CONSUMABLE_ITEM_TYPE)) {
            salesOrderItemUOMData.addProperty(AVAILABLE_QUANTITY, availableQuantity);
            return;
        }

        if (companyCode == null || storeCode == null) {
            salesOrderItemUOMData.addProperty(AVAILABLE_QUANTITY, availableQuantity);
        } else {
            availableQuantity = getAvailableQuantity(salesOrderCode, itemCode, uomCode);
            salesOrderItemUOMData.addProperty(AVAILABLE_QUANTITY, availableQuantity.toPlainString());
        }
    }

    private void addLastSalesPriceToResponse(
            String salesOrderCode,
            MObItemUOMGeneralModel itemUOMGeneralModel,
            JsonObject salesOrderItemUOMData) {

        String itemCode = itemUOMGeneralModel.getItemCode();
        String uomCode = itemUOMGeneralModel.getUserCode();

        ItemsLastSalesPriceGeneralModel itemsLastSalesPriceGeneralModel = null;

        itemsLastSalesPriceGeneralModel = getItemsLastSalesPrice(salesOrderCode, itemCode, uomCode);

        salesOrderItemUOMData.addProperty(
                "lastSalesPrice",
                itemsLastSalesPriceGeneralModel != null
                        ? itemsLastSalesPriceGeneralModel.getOrderUnitPrice()
                        : null);
    }

    private void constructSalesOrderItemSectionData(
            String salesOrderCode, JsonObject salesOrderItemSectionData) {

        Gson gson = getGson();

        List<IObSalesOrderItemGeneralModel> salesOrderItemsGeneralModel =
                salesOrderItemGeneralModelRep.findBySalesOrderCodeOrderByIdDesc(salesOrderCode);

        List<IObSalesOrderTaxGeneralModel> taxesList =
                salesOrderTaxGeneralModelRep.findBySalesOrderCodeOrderByCodeAsc(salesOrderCode);

        BigDecimal itemTotalAmount = getItemTotalAmount(salesOrderCode, salesOrderItemsGeneralModel);
        BigDecimal totalTaxesAmount = calculateTotalTaxesAmount(itemTotalAmount, taxesList);
        BigDecimal totalAmountAfterTaxes = getTotalAmountAfterTaxes(itemTotalAmount, totalTaxesAmount);

        salesOrderItemSectionData.addProperty("Items", gson.toJson(salesOrderItemsGeneralModel));
        salesOrderItemSectionData.addProperty("Taxes", gson.toJson(taxesList));

        addItemSummaryToResponse(
                salesOrderItemSectionData, gson, itemTotalAmount, totalTaxesAmount, totalAmountAfterTaxes);
    }

    private BigDecimal getTotalAmountAfterTaxes(
            BigDecimal itemTotalAmount, BigDecimal totalTaxesAmount) {

        BigDecimal total = null;

        if (itemTotalAmount != null && totalTaxesAmount != null) {
            total = itemTotalAmount.add(totalTaxesAmount);
        }

        return total;
    }

    private BigDecimal calculateTotalTaxesAmount(
            BigDecimal itemTotalAmount, List<IObSalesOrderTaxGeneralModel> taxesList) {

        if (itemTotalAmount == null || taxesList.size() == 0) {
            return null;
        }

        BigDecimal totalTaxesAmount = new BigDecimal(0.0);

        for (IObSalesOrderTaxGeneralModel tax : taxesList) {
            BigDecimal taxTotalAmount =
                    tax.getPercentage()
                            .multiply(itemTotalAmount)
                            .divide(new BigDecimal(100.0));
            tax.setAmount(taxTotalAmount);
            totalTaxesAmount = totalTaxesAmount.add(taxTotalAmount);
        }

        return totalTaxesAmount;
    }

    private void addItemSummaryToResponse(
            JsonObject salesOrderItemSectionData,
            Gson gson,
            BigDecimal itemTotalAmount,
            BigDecimal totalTaxesAmount,
            BigDecimal totalAmountAfterTaxes) {
        JsonObject itemSummary = new JsonObject();

        itemSummary.addProperty(
                "totalAmountBeforeTaxes", itemTotalAmount != null ? itemTotalAmount.toString() : null);

        itemSummary.addProperty(
                "totalTaxes", totalTaxesAmount != null ? totalTaxesAmount.toString() : null);

        itemSummary.addProperty(
                "totalAmountAfterTaxes",
                totalAmountAfterTaxes != null ? totalAmountAfterTaxes.toString() : null);

        salesOrderItemSectionData.addProperty("ItemSummary", gson.toJson(itemSummary));
    }

    private BigDecimal getItemTotalAmount(
            String salesOrderCode, List<IObSalesOrderItemGeneralModel> salesOrderItemsGeneralModel) {

        if (salesOrderItemsGeneralModel.size() == 0) {
            return null;
        }

        BigDecimal itemsTotalAmount = new BigDecimal(0.0);

        for (IObSalesOrderItemGeneralModel item : salesOrderItemsGeneralModel) {
            setItemLastSalesPrice(salesOrderCode, item);
            setItemAvailableQuantity(salesOrderCode, item);

            BigDecimal itemTotalAmount = item.getTotalAmount();
            itemsTotalAmount = itemsTotalAmount.add(itemTotalAmount);
        }

        return itemsTotalAmount;
    }

    private void setItemLastSalesPrice(String salesOrderCode, IObSalesOrderItemGeneralModel item) {

        String itemCode = item.getItemCode();
        String uomCode = item.getUnitOfMeasureCode();

        ItemsLastSalesPriceGeneralModel itemsLastSalesPriceGeneralModel = null;

        itemsLastSalesPriceGeneralModel = getItemsLastSalesPrice(salesOrderCode, itemCode, uomCode);

        if (itemsLastSalesPriceGeneralModel != null) {
            item.setLastSalesPrice(itemsLastSalesPriceGeneralModel.getOrderUnitPrice());
        }
    }

    private ItemsLastSalesPriceGeneralModel getItemsLastSalesPrice(
            String salesOrderCode, String itemCode, String uomCode) {
        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        IObSalesOrderDataGeneralModel salesOrderDataGeneralModel =
                salesOrderDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);

        IObSalesOrderCycleDates salesOrderCycleDates =
                salesOrderCycleDatesRep.findOneByRefInstanceId(salesOrderGeneralModel.getId());
        String purchaseUnitCode = salesOrderGeneralModel.getPurchaseUnitCode();
        String customerCode = salesOrderDataGeneralModel.getCustomerCode();
        ItemsLastSalesPriceGeneralModel itemsLastSalesPriceGeneralModel;
        if (salesOrderCycleDates != null) {
            DateTime salesPriceDate = getSalesPriceDate(salesOrderCycleDates);

            itemsLastSalesPriceGeneralModel =
                    itemsLastSalesPriceGeneralModelRep
                            .findTopByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCodeAndSalesInvoicePostingDateIsLessThanEqualOrderBySalesInvoicePostingDateDesc(
                                    itemCode, uomCode, purchaseUnitCode, customerCode, salesPriceDate);
        } else {
            itemsLastSalesPriceGeneralModel =
                    itemsLastSalesPriceGeneralModelRep
                            .findTopByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCodeOrderBySalesInvoicePostingDateDesc(
                                    itemCode, uomCode, purchaseUnitCode, customerCode);
        }
        return itemsLastSalesPriceGeneralModel;
    }

    private void setItemAvailableQuantity(String salesOrderCode, IObSalesOrderItemGeneralModel item) {

        IObSalesOrderCompanyAndStoreDataGeneralModel companyAndStoreDataGeneralModel =
                companyAndStoreDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);

        String itemCode = item.getItemCode();
        String uomCode = item.getUnitOfMeasureCode();
        String itemTypeCode = item.getItemTypeCode();

        String companyCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getCompanyCode()
                        : null;

        String storeCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getStoreCode()
                        : null;

        if (itemTypeCode.equals(SERVICE_ITEM_TYPE) || itemTypeCode.equals(CONSUMABLE_ITEM_TYPE)) {
            item.setAvailableQuantity(null);
            return;
        }

        if (companyCode == null || storeCode == null) {
            item.setAvailableQuantity(null);
        } else {
            BigDecimal availableQty = getAvailableQuantity(salesOrderCode, itemCode, uomCode);
            item.setAvailableQuantity(availableQty);
        }
    }

    private BigDecimal getAvailableQuantity(String salesOrderCode, String itemCode, String uomCode) {

        DObSalesOrderGeneralModel salesOrderGeneralModel =
                dObSalesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

        IObSalesOrderCompanyAndStoreDataGeneralModel companyAndStoreDataGeneralModel =
                companyAndStoreDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);
        String companyCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getCompanyCode()
                        : null;

        String storeCode =
                companyAndStoreDataGeneralModel != null
                        ? companyAndStoreDataGeneralModel.getStoreCode()
                        : null;

        String purchaseUnitCode = salesOrderGeneralModel.getPurchaseUnitCode();
        CObStockAvailabilitiesGeneralModel stockAvailabilitiesGeneralModel =
                stockAvailabilityGeneralModelRep
                        .findByCompanyCodeAndStorehouseCodeAndUnitOfMeasureCodeAndPurchaseUnitCodeAndItemCodeAndStockType(
                                companyCode,
                                storeCode,
                                uomCode,
                                purchaseUnitCode,
                                itemCode,
                                DObSalesOrderQueryControllerUtils.UNRESTRICTED_STOCK_TYPE);

        return stockAvailabilitiesGeneralModel != null
                ? stockAvailabilitiesGeneralModel.getAvailableQuantity()
                : BigDecimal.ZERO;
    }

    private DateTime getSalesPriceDate(IObSalesOrderCycleDates salesOrderCycleDates) {

        DateTime approvalDate = salesOrderCycleDates.getApprovalDate();
        DateTime cancellationDate = salesOrderCycleDates.getCancellationDate();

        if (approvalDate != null) {
            return approvalDate;
        }

        return cancellationDate;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{businessUnitCode}/getApprovedAndSalesInvoiceActivated")
    public @ResponseBody
    String readApprovedSalesOrders(@PathVariable String businessUnitCode) {
        authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL);

        Set<String> conditions = authorizationManager.getPermissionConditions();
        List<String> salesResponsibleNames =
                getConditionAttributeValue(IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME, conditions);

        List<DObSalesOrderGeneralModel> salesOrderGeneralModelList = new ArrayList<>();

        if (!salesResponsibleNames.isEmpty()
                && salesResponsibleNames.get(0).equals(DObSalesOrderQueryControllerUtils.LOGGED_IN_USER)) {
            String loggedInUserInfo = getLoggedInUserName();
            salesOrderGeneralModelList =
                    dObSalesOrderGeneralModelRep
                            .findAllApprovedAndSalesInvoiceActivatedByPurchaseUnitCodeAndSalesResponsibleNameOrderedByCodeDesc(
                                    businessUnitCode, loggedInUserInfo);
        } else {
            salesOrderGeneralModelList =
                    dObSalesOrderGeneralModelRep
                            .findAllApprovedAndSalesInvoiceActivatedByPurchaseUnitCodeOrderedByCodeDesc(
                                    businessUnitCode);
        }
        return serializeRESTSuccessResponse(salesOrderGeneralModelList);
    }
}
