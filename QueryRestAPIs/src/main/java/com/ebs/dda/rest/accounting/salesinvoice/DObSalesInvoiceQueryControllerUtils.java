package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class DObSalesInvoiceQueryControllerUtils {


  public void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }
}
