package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class DObPaymentRequestQueryControllerUtils {
  public static String LOGGED_IN_USER = "LoggedInUser";


  public void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }
}
