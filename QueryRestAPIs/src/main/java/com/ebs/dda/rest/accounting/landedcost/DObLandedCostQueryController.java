package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.apis.exchangerates.CBEExchangeRateController;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObLandedCostAuthorizationManager;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DateContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.*;
import com.ebs.dda.repositories.accounting.landedcost.specification.ReadPurchaseOrdersForLandedCostCreationSpecification;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/accounting/landedcost")
public class DObLandedCostQueryController extends BasicQueryController implements InitializingBean {

  private static String SERVICE_ITEM = "SERVICE";
  private final String COOKIE_DOMAIN = "shiro.uid.cookie.domain";
  private final String SUCCESS_URL = "shiro.successUrl";
  private DObLandedCostQueryControllerUtils controllerUtils;
  @Autowired
  @Qualifier("landedCostAllowedActionService")
  private AllowedActionService landedCostAllowedActionService;
  @Autowired private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  @Autowired private DObLandedCostAuthorizationManager landedCostAuthorizationManager;
  @Autowired private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
  @Autowired private IObLandedCostFactorItemsGeneralModelRep landedCostFactorItemsGeneralModelRep;
  @Autowired
  private IObLandedCostFactorItemsDetailsGeneralModelRep costFactorItemsDetailsGeneralModelRep;
  @Autowired
  private IObLandedCostFactorItemsSummaryGeneralModelRep
      landedCostFactorItemsSummaryGeneralModelRep;
  @Autowired private IObLandedCostCompanyDataGeneralModelRep companyGMRep;
  @Autowired private IObLandedCostAccountingDetailsGeneralModelRep accountingDetailsGeneralModelRep;
  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;
  @Autowired private DObLandedCostGeneralModelRep dObLandedCostGeneralModelRep;
  @Autowired private MObItemGeneralModelRep itemsRep;
  @Autowired private IObOrderCompanyGeneralModelRep purchaseOrderGMRep;
  @Autowired private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  @Autowired private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  @Autowired private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  @Autowired private Environment environment;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        landedCostAllowedActionService.getHomeScreenAllowedActions(DObLandedCost.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{landedCostCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String landedCostCode)
      throws Exception {
    DObLandedCostGeneralModel landedCost =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCost);

    Set<String> allowedActions =
        landedCostAllowedActionService.getObjectScreenAllowedActions(landedCost);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObLandedCost.SYS_NAME, READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObLandedCostGeneralModel.CODE));

    Specification querySpecification = getLandedCostDataSpecification(status, conditions);

    Page<DObLandedCostGeneralModel> landedCostsPage =
        landedCostGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObLandedCostGeneralModel> landedCosts = landedCostsPage.getContent();
    return serializeViewAllResponse(landedCosts, landedCostsPage.getTotalElements());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/landedcostdetails")
  public @ResponseBody String readLandedCostDetails(@PathVariable String landedCostCode)
      throws Exception {
    IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
        landedCostDetailsGeneralModelRep.findOneByLandedCostCode(landedCostCode);
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCostGeneralModel);

    landedCostAuthorizationManager.authorizeActionOnSection(
        landedCostGeneralModel, IDObLandedCostActionNames.READ_LANDED_COST_DETAILS);
    return serializeRESTSuccessResponse(landedCostDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/currencyprice")
  public @ResponseBody String readLandedCostCurrencyPrice(@PathVariable String landedCostCode)
      throws Exception {
    IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
        landedCostDetailsGeneralModelRep.findOneByLandedCostCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCostDetailsGeneralModel);
    String currencyIso = landedCostDetailsGeneralModel.getInvoiceCurrencyIso();
    String invoiceCode = landedCostDetailsGeneralModel.getVendorInvoiceCode();

    landedCostAuthorizationManager.authorizeActionOnSection(
        landedCostDetailsGeneralModel, IDObLandedCostActionNames.READ_LANDED_COST_DETAILS);

    BigDecimal currencyPrice = getCurrencyPrice(invoiceCode, currencyIso);
    return serializeRESTSuccessResponse(currencyPrice);
  }

  private BigDecimal getCurrencyPrice(String invoiceCode, String currencyIso) {
    BigDecimal currencyPrice;
    IObInvoiceSummaryGeneralModel invoiceSummary =
        invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
            invoiceCode, IDocumentTypes.VENDOR_INVOICE);
    Double remaining = new BigDecimal(invoiceSummary.getTotalRemaining()).doubleValue();
    Double netAmount = new BigDecimal(invoiceSummary.getNetAmount()).doubleValue();

    if (remaining.equals(0.0)) {
      currencyPrice = getPRAverageCurrencyPrice(invoiceCode);
    } else if (remaining.equals(netAmount)) {
      currencyPrice = getCBECurrencyPrice(currencyIso);
    } else {
      BigDecimal cbeCurrencyPrice = getCBECurrencyPrice(currencyIso);
      BigDecimal prAverageCurrencyPrice = getPRAverageCurrencyPrice(invoiceCode);
      currencyPrice =
          cbeCurrencyPrice.compareTo(prAverageCurrencyPrice) > 0
              ? cbeCurrencyPrice
              : prAverageCurrencyPrice;
    }
    return currencyPrice;
  }

  private BigDecimal getCBECurrencyPrice(String currencyIso) {
    CBEExchangeRateController exchangeRateController = new CBEExchangeRateController();
    return BigDecimal.valueOf(
        exchangeRateController.getCurrencyPriceFromCBE(
            currencyIso, environment.getProperty(SUCCESS_URL)));
  }

  private BigDecimal getPRAverageCurrencyPrice(String vendorInvoiceCode) {
    BigDecimal currencyPrice = BigDecimal.ZERO;
    List<DObPaymentRequestGeneralModel> invoicePayments = new LinkedList<>();
    DObVendorInvoiceGeneralModel invoice =
        vendorInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);

    List<DObPaymentRequestGeneralModel> downPayments =
        paymentRequestGeneralModelRep.findAllByReferenceDocumentCode(vendorInvoiceCode);
    invoicePayments.addAll(downPayments);

    List<DObPaymentRequestGeneralModel> installments =
        paymentRequestGeneralModelRep.findAllByReferenceDocumentCode(
            invoice.getPurchaseOrderCode());
    invoicePayments.addAll(installments);

    invoicePayments =
        invoicePayments.stream()
            .filter(
                payment ->
                    payment
                            .getCurrentStates()
                            .contains(DObPaymentRequestStateMachine.PAYMENT_DONE_STATE)
                        && payment.getPaymentType().equals(PaymentType.VENDOR.toString()))
            .collect(Collectors.toList());

    for (DObPaymentRequestGeneralModel payment : invoicePayments) {
      IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
          activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
              payment.getUserCode(), IDocumentTypes.PAYMENT_REQUEST);
      currencyPrice = currencyPrice.add(activationDetails.getCurrencyPrice());
    }
    if (!invoicePayments.isEmpty()) {
      currencyPrice = currencyPrice.divide(BigDecimal.valueOf(invoicePayments.size()));
    }
    return currencyPrice;
  }

  private Specification getLandedCostDataSpecification(Status status, Set<String> conditions) {

    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObLandedCostGeneralModel.PURCHASING_UNIT_NAME, conditions);

    SimpleStringFieldSpecification codeSpec =
        getSimpleStringSpecification(status, IDObLandedCostGeneralModel.CODE);

    SimpleStringFieldSpecification stateSpec =
        getSimpleStringSpecification(status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

    SimpleStringFieldSpecification purchaseUnitCodeSpec =
        getSimpleStringSpecification(status, IDObLandedCostGeneralModel.PURCHASE_UNIT_CODE);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitSpec =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IDObLandedCostGeneralModel.PURCHASING_UNIT_NAME_EN);

    DateContainsSpecification creationDateSpec =
        getDateContainsSpecification(status, IDObLandedCostGeneralModel.CREATION_DATE);

    SimpleStringFieldSpecification typeCodeSpec =
        getSimpleStringSpecification(status, IDObLandedCostGeneralModel.TYPE_CODE);

    SimpleStringFieldSpecification purchaseOrderCodeSpec =
        getSimpleStringSpecification(status, IDObLandedCostGeneralModel.PURCHASE_ORDER_CODE);

    Specification querySpecification =
        Specification.where(codeSpec)
            .and(purchaseUnitCodeSpec)
            .and(purchaseUnitSpec)
            .and(typeCodeSpec)
            .and(creationDateSpec)
            .and(stateSpec)
            .and(purchaseOrderCodeSpec);

    return querySpecification;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    controllerUtils = new DObLandedCostQueryControllerUtils();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String landedCostCode)
      throws Exception {
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCostGeneralModel);
    landedCostAuthorizationManager.authorizeActionOnSection(
        landedCostGeneralModel, IActionsNames.READ_GENERAL_DATA);
    return serializeRESTSuccessResponse(landedCostGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/costfactoritemssummary")
  public @ResponseBody String readCostFactorItemsSummary(@PathVariable String landedCostCode)
      throws Exception {
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCostGeneralModel);
    landedCostAuthorizationManager.authorizeActionOnSection(
        landedCostGeneralModel, IActionsNames.READ_COST_FACTOR_ITEM);
    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        landedCostFactorItemsSummaryGeneralModelRep.findOneByCode(landedCostCode);
    return serializeRESTSuccessResponse(landedCostFactorItemsSummary);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/costfactoritems")
  public @ResponseBody String readCostFactorItem(@PathVariable String landedCostCode)
      throws Exception {

    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    controllerUtils.checkIfEntityIsNull(landedCostGeneralModel);
    landedCostAuthorizationManager.authorizeActionOnSection(
        landedCostGeneralModel, IActionsNames.READ_COST_FACTOR_ITEM);

    List<IObLandedCostFactorItemsGeneralModel> landedCostFactorItems =
        landedCostFactorItemsGeneralModelRep.findAllByCodeOrderByIdDesc(landedCostCode);
    landedCostFactorItems.forEach(this::setCostFactorItemDetails);

    return serializeRESTSuccessResponse(landedCostFactorItems);
  }

  private void setCostFactorItemDetails(IObLandedCostFactorItemsGeneralModel costFactorItem) {
    List<IObLandedCostFactorItemsDetailsGeneralModel> itemDetails =
        costFactorItemsDetailsGeneralModelRep.findByRefInstanceId(costFactorItem.getId());
    costFactorItem.setDetails(itemDetails);
  }

  @GetMapping("/{landedCostCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String landedCostCode)
      throws Exception {
    IObLandedCostCompanyDataGeneralModel companyGeneralModel =
        companyGMRep.findOneByDocumentCode(landedCostCode);
    checkIfEntityIsNull(companyGeneralModel);

    landedCostAuthorizationManager.authorizeActionOnSection(
        companyGeneralModel, IDObLandedCostActionNames.READ_COMPANY_DATA);
    return serializeRESTSuccessResponse(companyGeneralModel);
  }

  @GetMapping("/purchase-orders")
  public @ResponseBody String readPurchaseOrdersForCreation(@RequestParam String landedCostType) {

    isAuthorizedToReadPO(landedCostType);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    List<String> purhcaseOrdersHavePostedLandedCost =
        landedCostGeneralModelRep
            .findByCurrentStatesNotLike("%" + BasicStateMachine.DRAFT_STATE + "%")
            .stream()
            .map(lc -> lc.getPurchaseOrderCode())
            .collect(Collectors.toList());

    ReadPurchaseOrdersForLandedCostCreationSpecification specs =
        new ReadPurchaseOrdersForLandedCostCreationSpecification();

    Specification<IObOrderCompanyGeneralModel> filter =
        Specification.where(specs.isAllowedType(landedCostType))
            .and(specs.isAuthorizedPurchaseUnit(purchaseUnitNames))
            .and(specs.isAllowedState())
            .and(specs.isCompanyExist());

    if (!purhcaseOrdersHavePostedLandedCost.isEmpty()) {
      filter = filter.and(specs.poHasNotPostedLandedCost(purhcaseOrdersHavePostedLandedCost));
    }

    List<IObOrderCompanyGeneralModel> purhcaseOrder =
        purchaseOrderGMRep.findAll(
            filter, Sort.by(Sort.Direction.DESC, IDObPurchaseOrderGeneralModel.CODE));

    return serializeRESTSuccessResponse(purhcaseOrder);
  }

  private void isAuthorizedToReadPO(String landedCostType) {
    boolean isAllowedToReadPO = false;
    if (landedCostType.equals(ICObLandedCostType.IMPORT)) {
      isAllowedToReadPO = isReadPermissionAllowed(IDObPurchaseOrder.SYS_NAME, READ_ALL);
    }
    if (landedCostType.equals(ICObLandedCostType.LOCAL)) {
      isAllowedToReadPO = isReadPermissionAllowed(IDObPurchaseOrder.SYS_NAME, READ_ALL);
    }

    if (!isAllowedToReadPO) {
      throw new AuthorizationException("User is not authorized to read any PO type.");
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/serviceitems/{landedCostCode}")
  public @ResponseBody String readServiceItems(@PathVariable String landedCostCode) {
    try {
      checkIfReadPermissionIsAllowed(IMObItemGeneralModel.SYS_NAME, IActionsNames.READ_ALL);
      DObLandedCostGeneralModel landedCostGeneralModel =
          dObLandedCostGeneralModelRep.findOneByUserCode(landedCostCode);
      List<MObItemGeneralModel> items =
          itemsRep.findAllByTypeCodeAndPurchasingUnitCodesContainsAndCostFactor(
              SERVICE_ITEM, landedCostGeneralModel.getPurchaseUnitCode(), true);

      return serializeRESTSuccessResponse(items);
    } catch (Exception e) {
      throw new AuthorizationException();
    }
  }
}
