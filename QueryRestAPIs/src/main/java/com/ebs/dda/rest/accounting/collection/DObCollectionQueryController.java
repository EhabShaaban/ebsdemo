package com.ebs.dda.rest.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObCollectionAuthorizationManager;
import com.ebs.dda.accounting.collection.apis.IDObCollectionActionNames;
import com.ebs.dda.accounting.dbo.jpa.repositories.specifications.ReadPaymentRequestSpecification;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.collection.*;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.*;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.QueryControllerUtils;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/accounting/collection")
public class DObCollectionQueryController extends BasicQueryController {
  @Autowired DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired DObCollectionGeneralModelRep dObCollectionGeneralModelRep;

  @Autowired IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGeneralModelRep;
  @Autowired IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;

  @Autowired AuthorizationManager authorizationManager;
  @Autowired DObCollectionAuthorizationManager dObCollectionAuthorizationManager;
  @Autowired DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  @Autowired DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;

  @Autowired
  @Qualifier("collectionAllowedActionService")
  private AllowedActionService collectionAllowedActionService;

  @Autowired private CObCollectionTypeRep collectionTypeRep;
  @Autowired private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

  @Autowired
  private IObCollectionAccountingDetailsGeneralModelRep collectionAccountingDetailsGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  @Autowired private IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep;
  @Autowired private DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/collectionmethods")
  public @ResponseBody String getCollectionMethods() {
    return serializeRESTSuccessResponse(CollectionMethodEnum.CollectionMethod.values());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/collectiontypes")
  public @ResponseBody String readCollectionTypes() throws Exception {
    try {
      checkIfReadPermissionIsAllowed(ICObCollectionType.SYS_NAME, READ_ALL);

      List<CObCollectionType> cobCollectionList = collectionTypeRep.findAll();
      return serializeRESTSuccessResponse(cobCollectionList);
    } catch (ResourceAccessException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/salesinvoices")
  public @ResponseBody String getDeliveredSalesInvoices() {

    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObSalesInvoiceGeneralModel.PURCHASE_UNIT, conditions);

    ReadSalesInvoiceForCollectionSpecification salesInvoiceForCollectionSpecification =
        new ReadSalesInvoiceForCollectionSpecification();
    Specification<DObSalesInvoiceGeneralModel> authorizedPurchaseUnit =
        salesInvoiceForCollectionSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesInvoiceGeneralModel> salesInvoices =
        salesInvoiceGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(salesInvoiceForCollectionSpecification.salesInvoicesInAllowedState())
                .and(salesInvoiceForCollectionSpecification.salesInvoiceAllowedRemaining()));

    salesInvoices.sort(Comparator.comparing(DObSalesInvoiceGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(salesInvoices, salesInvoices.size());
  }

  @GetMapping(value = "/salesinvoices/{businessUnitCode}/{customerCode}")
  public @ResponseBody String getDeliveredSalesInvoices(
      @PathVariable String businessUnitCode, @PathVariable String customerCode) {
    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, READ_ALL);
    List<DObSalesInvoiceGeneralModel> salesInvoices =
        salesInvoiceGeneralModelRep
            .findAllByPurchaseUnitCodeAndCustomerCodeAndCurrentStatesLikeOrderByIdDesc(
                businessUnitCode,
                customerCode,
                "%" + DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE + "%");
    return serializeRESTSuccessResponse(salesInvoices);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/salesorders")
  public @ResponseBody String getDeliveredSalesOrders() {

    authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadSalesOrderForCollectionSpecification salesOrderForCollectionSpecification =
        new ReadSalesOrderForCollectionSpecification();
    Specification<DObSalesOrderGeneralModel> authorizedPurchaseUnit =
        salesOrderForCollectionSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesOrderGeneralModel> salesOrders =
        salesOrderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(salesOrderForCollectionSpecification.salesOrdersInAllowedState())
                .and(salesOrderForCollectionSpecification.salesOrdersAllowedRemaining()));

    salesOrders.sort(Comparator.comparing(DObSalesOrderGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(salesOrders, salesOrders.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/debitNotes")
  public @ResponseBody String readPostedDebitNotes() {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadDebitNotesForCollectionSpecification debitNotesForCollectionSpecification =
        new ReadDebitNotesForCollectionSpecification();

    Specification<DObAccountingNoteGeneralModel> authorizedPurchaseUnit =
        debitNotesForCollectionSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObAccountingNoteGeneralModel> debitNotes =
        accountingNoteGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(debitNotesForCollectionSpecification.debitNoteInAllowedState())
                .and(debitNotesForCollectionSpecification.debitNotesAllowedRemaining())
                .and(debitNotesForCollectionSpecification.accountingNoteIsDebit()));

    debitNotes.sort(Comparator.comparing(DObAccountingNoteGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(debitNotes, debitNotes.size());
  }

  @GetMapping(value = "/paymentrequests")
  public @ResponseBody String readPaymentRequests() {
    authorizationManager.authorizeAction(IDObPaymentRequest.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();

    List<String> businessUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadPaymentRequestSpecification specification = new ReadPaymentRequestSpecification();

    List<DObPaymentRequestGeneralModel> paymentRequests =
        paymentRequestGeneralModelRep.findAll(
            Specification.where(
                specification
                    .isAuthorizedBusinessUnit(businessUnitNames)
                    .and(specification.isPaymentDone())
                    .and(specification.isUnderSettlement())
                    .and(specification.remainingIsGreaterThanZero())),
            Sort.by(Sort.Direction.DESC, IDObPaymentRequestGeneralModel.USER_CODE));

    return serializeRESTSuccessResponse(paymentRequests);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{collectionCode}")
  public @ResponseBody String getObjectScreenAllowedActions(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel dObCollectionGeneralModel =
        dObCollectionGeneralModelRep.findOneByUserCode(collectionCode);

    if (dObCollectionGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    Set<String> allowedActions =
        collectionAllowedActionService.getObjectScreenAllowedActions(dObCollectionGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/notesreceivables")
  public @ResponseBody String getActiveNotesReceivables() {
    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadNotesReceivableForCollectionSpecification notesReceivableForCollectionSpecification =
        new ReadNotesReceivableForCollectionSpecification();
    Specification<DObNotesReceivablesGeneralModel> authorizedPurchaseUnit =
        notesReceivableForCollectionSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObNotesReceivablesGeneralModel> notesReceivables =
        notesReceivablesGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(notesReceivableForCollectionSpecification.notesReceivablesInAllowedState()));

    notesReceivables.sort(
        Comparator.comparing(DObNotesReceivablesGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(notesReceivables, notesReceivables.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        collectionAllowedActionService.getHomeScreenAllowedActions(DObCollectionGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{collectionCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        dObCollectionGeneralModelRep.findOneByUserCode(collectionCode);
    QueryControllerUtils.checkIfEntityIsNull(collectionGeneralModel);
    dObCollectionAuthorizationManager.authorizeActionOnSection(
        collectionGeneralModel, IActionsNames.READ_GENERAL_DATA);
    return serializeRESTSuccessResponse(collectionGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObCollection.SYS_NAME, READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObCollectionGeneralModel.USER_CODE));

    Specification querySpecification = getCollectionDataSpecification(status, conditions);

    Page<DObCollectionGeneralModel> collectionsPage =
        dObCollectionGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObCollectionGeneralModel> collections = collectionsPage.getContent();
    return serializeViewAllResponse(collections, collectionsPage.getTotalElements());
  }

  private Specification getCollectionDataSpecification(Status status, Set<String> conditions) {

    List<String> businessUnitNames =
        getConditionAttributeValue(IDObCollectionGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);
    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObCollectionGeneralModel.PURCHASING_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObCollectionGeneralModel.USER_CODE);

    SimpleStringFieldSpecification creationInfoSpecification =
        getSimpleStringSpecification(status, IDObCollectionGeneralModel.CREATION_INFO);

    SimpleStringFieldSpecification businessUnitSpecification =
        getSimpleStringSpecification(status, IDObCollectionGeneralModel.PURCHASE_UNIT_CODE);

    SimpleStringFieldSpecification refDocumentCodeSpecification =
        getCustomStringContainsSpecification(
            status, IDObCollectionGeneralModel.REF_DOCUMENT, IDObCollectionGeneralModel.CODE);

    JsonFieldSpecification refDocumentTypeNameSpecification =
        getCustomJsonContainsSpecification(
            status, IDObCollectionGeneralModel.REF_DOCUMENT, IDObCollectionGeneralModel.TYPE_NAME);

    SimpleStringFieldSpecification businessPartnerCodeSpecification =
        getCustomStringContainsSpecification(
            status, IDObCollectionGeneralModel.BUSINESS_PARTNER, IDObCollectionGeneralModel.CODE);

    JsonFieldSpecification businessPartnerNameSpecification =
        getCustomJsonContainsSpecification(
            status, IDObCollectionGeneralModel.BUSINESS_PARTNER, IDObCollectionGeneralModel.NAME);

    JsonFieldSpecification businessPartnerTypeSpecification =
        getCustomJsonContainsSpecification(
            status, IDObCollectionGeneralModel.BUSINESS_PARTNER, IDObCollectionGeneralModel.TYPE);

    DoubleFieldSpecification amountSpecification =
        getDoubleFieldSpecification(status, IDObCollectionGeneralModel.AMOUNT);

    SimpleStringFieldSpecification stateSpecification =
        getSimpleStringSpecification(status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

    JsonFieldSpecification TypeNameSpecification =
        getJsonContainsSpecification(status, IDObCollectionGeneralModel.REF_DOCUMENT_TYPE_NAME);

    SimpleStringFieldSpecification documentOwnerSpecification =
        getSimpleStringSpecification(status, IDObCollectionGeneralModel.DOCUMENT_OWNER);

    Specification querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(userCodeSpecification)
            .and(creationInfoSpecification)
            .and(
                businessPartnerCodeSpecification
                    .or(businessPartnerNameSpecification)
                    .or(businessPartnerTypeSpecification))
            .and(businessUnitSpecification)
            .and(refDocumentCodeSpecification.or(refDocumentTypeNameSpecification))
            .and(amountSpecification)
            .and(stateSpecification)
            .and(TypeNameSpecification)
            .and(documentOwnerSpecification);
    return querySpecification;
  }

  @GetMapping(value = "/{collectionCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String collectionCode)
      throws Exception {
    IObCollectionCompanyDetailsGeneralModel collectionCompanyDetailsGeneralModel =
        collectionCompanyDetailsGeneralModelRep.findOneByCollectionCode(collectionCode);
    QueryControllerUtils.checkIfEntityIsNull(collectionCompanyDetailsGeneralModel);

    dObCollectionAuthorizationManager.authorizeActionOnSection(
        collectionCompanyDetailsGeneralModel, IDObCollectionActionNames.READ_COMPANY);

    return serializeRESTSuccessResponse(collectionCompanyDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{collectionCode}/accountingdetails")
  public @ResponseBody String readAccountingDetailsData(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        dObCollectionGeneralModelRep.findOneByUserCode(collectionCode);
    QueryControllerUtils.checkIfEntityIsNull(collectionGeneralModel);

    dObCollectionAuthorizationManager.authorizeActionOnSection(
        collectionGeneralModel, IDObCollectionActionNames.READ_ACCOUNTING_DETAILS);

    List<IObCollectionAccountingDetailsGeneralModel> collectionAccountingDetailsGeneralModels;
    collectionAccountingDetailsGeneralModels =
        collectionAccountingDetailsGeneralModelRep.findByDocumentCodeOrderByAccountingEntryDesc(
            collectionCode);

    return serializeRESTSuccessResponse(collectionAccountingDetailsGeneralModels);
  }

  @GetMapping(value = "/{collectionCode}/collectiondetails")
  public @ResponseBody String readCollectionDetails(@PathVariable String collectionCode)
      throws Exception {
    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode);
    QueryControllerUtils.checkIfEntityIsNull(collectionDetailsGeneralModel);

    dObCollectionAuthorizationManager.authorizeActionOnSection(
        collectionDetailsGeneralModel, IDObCollectionActionNames.READ_COLLECTION_DETAILS);

    return serializeRESTSuccessResponse(collectionDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{collectionCode}/postingdetails")
  public @ResponseBody String readActivationDetailsData(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        dObCollectionGeneralModelRep.findOneByUserCode(collectionCode);
    QueryControllerUtils.checkIfEntityIsNull(collectionGeneralModel);
    dObCollectionAuthorizationManager.authorizeActionOnSection(
        collectionGeneralModel, IDObCollectionActionNames.READ_Activation_DETAILS);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndDocumentType(
            collectionCode, collectionGeneralModel.getCollectionDocumentType());
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{companyCode}/treasuries")
  public @ResponseBody String readCompanyTreasuries(@PathVariable String companyCode) {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    List<IObCompanyTreasuriesGeneralModel> companiesTreasuries =
        companyTreasuriesGeneralModelRep.findAllByCompanyCodeOrderByTreasuryCodeDesc(companyCode);
    return serializeRESTSuccessResponse(companiesTreasuries);
  }
}
