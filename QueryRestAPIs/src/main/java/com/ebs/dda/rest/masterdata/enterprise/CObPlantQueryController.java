package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.utils.AuthorizationUtils;
import com.ebs.dda.jpa.masterdata.plant.CObPlant;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/plants")
public class CObPlantQueryController extends BasicQueryController {

  @Autowired private CObPlantGeneralModelRep plantGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/bycompanycode/{companyCode}")
  public @ResponseBody String readPlantsByCompany(@PathVariable String companyCode)
      throws Exception {
    applyAuthorizationForPlant();
    List<CObPlantGeneralModel> plants = plantGeneralModelRep.findByCompanyCode(companyCode);
    return serializeRESTSuccessResponse(plants);
  }

  private RequiresPermissions createPermissionForPlant(String sysName, String readAllActionName) {
    return AuthorizationUtils.createClassPermission(sysName, readAllActionName).getPermission();
  }

  private void applyAuthorizationForPlant() throws Exception {
    CObPlant plant = new CObPlant();
    checkIfReadPermissionIsAllowed(plant.getSysName(), IActionsNames.READ_ALL);
  }
}
