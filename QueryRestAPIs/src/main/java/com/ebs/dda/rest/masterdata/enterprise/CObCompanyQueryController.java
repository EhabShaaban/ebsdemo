package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/companies")
public class CObCompanyQueryController extends BasicQueryController {

  @Autowired private CObCompanyGeneralModelRep companyGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/getall")
  public @ResponseBody String readAll() throws Exception {
    applyAuthorizationForCompany();
    List<CObCompanyGeneralModel> companies = companyGeneralModelRep.findAll();
    return serializeRESTSuccessResponse(companies);
  }
  private void applyAuthorizationForCompany() throws Exception {
    CObCompany company = new CObCompany();
    checkIfReadPermissionIsAllowed(company.getSysName(), IActionsNames.READ_ALL);
  }
}
