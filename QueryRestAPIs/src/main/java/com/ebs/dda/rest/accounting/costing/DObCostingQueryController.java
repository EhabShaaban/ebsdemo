package com.ebs.dda.rest.accounting.costing;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObCostingnAuthorizationManager;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/accounting/costing")
public class DObCostingQueryController extends BasicQueryController implements InitializingBean {

  @Autowired IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  @Autowired IObCostingItemGeneralModelRep costingItemGeneralModelRep;
  @Autowired DObCostingGeneralModelRep costingGeneralModelRep;
  @Autowired DObCostingnAuthorizationManager costingAuthorizationManager;

  @Autowired
  @Qualifier("costingAllowedActionService")
  private AllowedActionService costingAllowedActionService;

  private DObCostingControllerUtil costingControllerUtil;

  @RequestMapping(method = RequestMethod.GET, value = "/{costingCode}/details")
  public @ResponseBody String readCostingDetails(@PathVariable String costingCode)
      throws Exception {
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        costingDetailsGeneralModelRep.findOneByUserCode(costingCode);
    costingAuthorizationManager.authorizeActionOnSection(
        costingDetailsGeneralModel, IActionsNames.READ_DETAILS);
    return serializeRESTSuccessResponse(costingDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{costingCode}/items")
  public @ResponseBody String readCostingItems(@PathVariable String costingCode) throws Exception {
    DObCostingGeneralModel costingGeneralModel =
        costingGeneralModelRep.findOneByUserCode(costingCode);

    costingAuthorizationManager.authorizeActionOnSection(
        costingGeneralModel, IActionsNames.READ_ITEMS);
    List<IObCostingItemGeneralModel> costingItemsGeneralModel =
        costingItemGeneralModelRep.findByUserCodeOrderByItemCodeAsc(costingCode);

    costingControllerUtil.setItemTotalCost(costingItemsGeneralModel);

    return serializeRESTSuccessResponse(costingItemsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{costingCode}")
  public @ResponseBody String getObjectScreenAllowedActions(@PathVariable String costingCode)
      throws Exception {
    DObCostingGeneralModel dObCostingGeneralModel =
        costingGeneralModelRep.findOneByUserCode(costingCode);

    if (dObCostingGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    Set<String> allowedActions =
        costingAllowedActionService.getObjectScreenAllowedActions(dObCostingGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @Override
  public void afterPropertiesSet() {
    costingControllerUtil = new DObCostingControllerUtil();
  }
}
