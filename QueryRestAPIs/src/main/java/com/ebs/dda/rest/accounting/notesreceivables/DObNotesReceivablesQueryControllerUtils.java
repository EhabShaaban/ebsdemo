package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.ReadNotesReceivableSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.ReadSalesInvoiceForNotesReceivableSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.ReadSalesOrderForNotesReceivableSpecification;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Set;

public class DObNotesReceivablesQueryControllerUtils extends BasicQueryController {

  private final AuthorizationManager authorizationManager;

  public DObNotesReceivablesQueryControllerUtils(AuthorizationManager authorizationManager){
    this.authorizationManager = authorizationManager;
  }
  private IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;

  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public void constructNotesReceivableReferenceDocumentsSectionData(
      String notesReceivableCode, JsonObject notesReceivableReferenceDocumentsSectionData) {

    Gson gson = getGson();
    List<IObNotesReceivablesReferenceDocumentGeneralModel>
        notesReceivablesReferenceDocumentGeneralModelList =
            notesReceivablesReferenceDocumentGeneralModelRep.findByNotesReceivableCodeOrderByIdAsc(
                notesReceivableCode);

    notesReceivableReferenceDocumentsSectionData.addProperty(
        "ReferenceDocuments", gson.toJson(notesReceivablesReferenceDocumentGeneralModelList));
  }

  public void constructSalesOrderList(
      String notesReceivableCode, List<DObSalesOrderGeneralModel> validSalesOrders) {

    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
        notesReceivablesReferenceDocumentGeneralModelRep
            .findAllByNotesReceivableCodeAndDocumentType(
                notesReceivableCode,
                RefDocumentTypeEnum.SALES_ORDER);

    for (IObNotesReceivablesReferenceDocumentGeneralModel referenceDocument : referenceDocuments) {
      validSalesOrders.removeIf(
          u -> u.getUserCode().equals(referenceDocument.getRefDocumentCode()));
    }
  }

  public List<DObSalesOrderGeneralModel> getValidSalesOrders(String notesReceivableCode) {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadSalesOrderForNotesReceivableSpecification salesOrderForNotesReceivableSpecification =
        new ReadSalesOrderForNotesReceivableSpecification();
    Specification<DObSalesOrderGeneralModel> authorizedPurchaseUnit =
        salesOrderForNotesReceivableSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesOrderGeneralModel> salesOrders =
        salesOrderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(
                    salesOrderForNotesReceivableSpecification.salesOrdersAllowedBusinessUnit(
                        notesReceivablesGeneralModel))
                .and(
                    salesOrderForNotesReceivableSpecification.salesOrdersAllowedCompany(
                        notesReceivablesGeneralModel))
                .and(
                    salesOrderForNotesReceivableSpecification.salesOrdersAllowedBusinessPartner(
                        notesReceivablesGeneralModel))
                .and(salesOrderForNotesReceivableSpecification.salesOrdersInAllowedState())
                .and(salesOrderForNotesReceivableSpecification.salesOrdersAllowedRemaining()),
            Sort.by(Sort.Direction.DESC, IDObSalesOrderGeneralModel.USER_CODE));
    return salesOrders;
  }

  public List<DObNotesReceivablesGeneralModel> getValidNotesReceivables(
      String notesReceivableCode) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadNotesReceivableSpecification notesReceivableSpecification =
        new ReadNotesReceivableSpecification();
    Specification<DObNotesReceivablesGeneralModel> authorizedPurchaseUnit =
        notesReceivableSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObNotesReceivablesGeneralModel> notesReceivables =
        notesReceivablesGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(
                    notesReceivableSpecification.notesReceivablesAllowedBusinessUnit(
                        notesReceivablesGeneralModel))
                .and(
                    notesReceivableSpecification.notesReceivablesAllowedCompany(
                        notesReceivablesGeneralModel))
                .and(
                    notesReceivableSpecification.notesReceivablesAllowedBusinessPartner(
                        notesReceivablesGeneralModel))
                .and(notesReceivableSpecification.notesReceivablesInAllowedState())
                .and(notesReceivableSpecification.notesReceivablesAllowedRemaining()),
            Sort.by(Sort.Direction.DESC, IDObNotesReceivablesGeneralModel.USER_CODE));
    return notesReceivables;
  }

  public void constructNotesReceivableList(
      String notesReceivableCode, List<DObNotesReceivablesGeneralModel> validNotesReceivables) {

    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
        notesReceivablesReferenceDocumentGeneralModelRep
            .findAllByNotesReceivableCodeAndDocumentType(
                notesReceivableCode,
                RefDocumentTypeEnum.NOTES_RECEIVABLE);

    for (IObNotesReceivablesReferenceDocumentGeneralModel referenceDocument : referenceDocuments) {
      validNotesReceivables.removeIf(
          u -> u.getUserCode().equals(referenceDocument.getRefDocumentCode()));
    }
  }

  public List<DObSalesInvoiceGeneralModel> getValidsalesInvoices(String notesReceivableCode) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    ReadSalesInvoiceForNotesReceivableSpecification salesInvoiceForNotesReceivableSpecification =
        new ReadSalesInvoiceForNotesReceivableSpecification();
    Specification<DObSalesInvoiceGeneralModel> authorizedPurchaseUnit =
        salesInvoiceForNotesReceivableSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesInvoiceGeneralModel> salesInvoices =
        salesInvoiceGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(
                    salesInvoiceForNotesReceivableSpecification.salesInvoicesAllowedBusinessUnit(
                        notesReceivablesGeneralModel))
                .and(
                    salesInvoiceForNotesReceivableSpecification.salesInvoicesAllowedCompany(
                        notesReceivablesGeneralModel))
                .and(
                    salesInvoiceForNotesReceivableSpecification.salesInvoicesAllowedBusinessPartner(
                        notesReceivablesGeneralModel))
                .and(salesInvoiceForNotesReceivableSpecification.salesInvoicesInAllowedState())
                .and(salesInvoiceForNotesReceivableSpecification.salesInvoicesAllowedRemaining()),
            Sort.by(Sort.Direction.DESC, IDObSalesInvoiceGeneralModel.INVOICE_CODE));
    return salesInvoices;
  }

  public void constructsalesInvoiceList(
      String notesReceivableCode, List<DObSalesInvoiceGeneralModel> validsalesInvoices) {

    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
        notesReceivablesReferenceDocumentGeneralModelRep
            .findAllByNotesReceivableCodeAndDocumentType(
                notesReceivableCode,
                RefDocumentTypeEnum.SALES_INVOICE);

    for (IObNotesReceivablesReferenceDocumentGeneralModel referenceDocument : referenceDocuments) {
      validsalesInvoices.removeIf(
          u -> u.getUserCode().equals(referenceDocument.getRefDocumentCode()));
    }
  }

  public void setIObNotesReceivablesReferenceDocumentGeneralModelRep(
      IObNotesReceivablesReferenceDocumentGeneralModelRep
          notesReceivablesReferenceDocumentGeneralModelRep) {
    this.notesReceivablesReferenceDocumentGeneralModelRep =
        notesReceivablesReferenceDocumentGeneralModelRep;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }
}
