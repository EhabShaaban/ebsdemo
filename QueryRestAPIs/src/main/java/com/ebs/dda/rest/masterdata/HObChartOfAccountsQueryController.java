package com.ebs.dda.rest.masterdata;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.dbo.jpa.entities.lookups.LObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountType;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.Level;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.MappedAccountsEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.CurrentStatesFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/chartofaccounts")
public class HObChartOfAccountsQueryController extends BasicQueryController {

  public static final String IN_ACTIVE_STATE = "InActive";
  @Autowired private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
  @Autowired private SubAccountGeneralModelRep subAccountGeneralModelRep;

  @Autowired
  @Qualifier("chartOfAccountsAllowedActionService")
  private AllowedActionService allowedActionService;

  @Autowired private LObGlobalAccountConfigRep globalAccountConfigRep;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(HObRootGLAccount.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @GetMapping("/authorizedactions/{glAccountCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String glAccountCode)
      throws Exception {
    HObChartOfAccountsGeneralModel glAccount =
        chartOfAccountsGeneralModelRep.findOneByUserCode(glAccountCode);
    checkIfEntityIsNull(glAccount);

    Set<String> allowedActions = allowedActionService.getObjectScreenAllowedActions(glAccount);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Direction.DESC, IHObChartOfAccountsGeneralModel.ACCOUNT_ID));

    Specification querySpecification = getChartOfAccountsDataSpecification(status);

    Page<HObGLAccount> chartOfAccountsPage =
        chartOfAccountsGeneralModelRep.findAll(querySpecification, pageRequest);
    List<HObGLAccount> chartOfAccounts = chartOfAccountsPage.getContent();
    return serializeViewAllResponse(chartOfAccounts, chartOfAccountsPage.getTotalElements());
  }

  @RequestMapping(value = "/parents/{level}", method = RequestMethod.GET)
  @ResponseBody
  public String getparents(@PathVariable String level) throws Exception {
    authorizationManager.authorizeAction(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);

    String parentLevel = String.valueOf(Long.parseLong(level) - 1);
    List<HObChartOfAccountsGeneralModel> hObChartOfAccountsGeneralModels =
        chartOfAccountsGeneralModelRep.findAllByLevel(parentLevel);
    return serializeRESTSuccessResponse(hObChartOfAccountsGeneralModels);
  }

  @RequestMapping(value = "/leafs", method = RequestMethod.GET)
  @ResponseBody
  public String getLeafs() throws Exception {
    checkIfReadPermissionIsAllowed(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);
    List<HObChartOfAccountsGeneralModel> hObLeafGLAccountsGeneralModel =
        chartOfAccountsGeneralModelRep.findByCurrentStateNotContainsAndLevel(
            IN_ACTIVE_STATE, IHObChartOfAccounts.LEAF);
    return serializeRESTSuccessResponse(hObLeafGLAccountsGeneralModel);
  }

  @RequestMapping(value = "/limitedleafs", method = RequestMethod.GET)
  @ResponseBody
  public String readAllLeafsLimited() throws Exception {
    checkIfReadPermissionIsAllowed(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL_LIMITED);
    List<HObChartOfAccountsGeneralModel> hObLeafGLAccountsGeneralModel =
        chartOfAccountsGeneralModelRep.findByCurrentStateNotContainsAndLevel(
            IN_ACTIVE_STATE, IHObChartOfAccounts.LEAF);
    return serializeRESTSuccessResponse(hObLeafGLAccountsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/levels")
  public @ResponseBody String readAccountLevel() throws Exception {
    return serializeLevelResponse();
  }

  private String serializeLevelResponse() {
    JsonArray jsonArray = new JsonArray();
    for (Level level : Level.values()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.add(level.name(), getGson().toJsonTree(level.toString()));
      jsonArray.add(jsonObject);
    }
    return serializeRESTSuccessResponse(jsonArray);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/creditDebit")
  public @ResponseBody String readCreditDebit() throws Exception {
    return serializeCreditDebitResponse();
  }

  private String serializeCreditDebitResponse() {
    JsonArray jsonArray = new JsonArray();
    for (AccountingEntry accountingEntry : AccountingEntry.values()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.add(accountingEntry.name(), getGson().toJsonTree(accountingEntry.toString()));
      jsonArray.add(jsonObject);
    }
    return serializeRESTSuccessResponse(jsonArray);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/accounttypes")
  public @ResponseBody String readAccountType() throws Exception {
    return serializeAccountTypeResponse();
  }

  private String serializeAccountTypeResponse() {
    JsonArray jsonArray = new JsonArray();
    for (AccountType accountType : AccountType.values()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.add(accountType.name(), getGson().toJsonTree(accountType.toString()));
      jsonArray.add(jsonObject);
    }
    return serializeRESTSuccessResponse(jsonArray);
  }

  @RequestMapping(value = "/subaccounts/{ledger}", method = RequestMethod.GET)
  @ResponseBody
  public String getSubAccounts(@PathVariable String ledger) throws Exception {
    checkIfReadPermissionIsAllowed(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);
    List<SubAccountGeneralModel> subAccountGeneralModelList =
        subAccountGeneralModelRep.findAllByLedger(ledger);
    return serializeRESTSuccessResponse(subAccountGeneralModelList);
  }

  private Specification getChartOfAccountsDataSpecification(Status status) {

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.USER_CODE);

    SimpleStringFieldSpecification parentCodeNameSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.PARENT_CODE_NAME);

    SimpleStringFieldSpecification accountTypeSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.ACCOUNT_TYPE);

    SimpleStringFieldSpecification debitCreditSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.CREDIT_DEBIT);

    JsonFieldSpecification accountNameSpecification =
        getJsonContainsSpecification(status, IHObChartOfAccountsGeneralModel.ACCOUNT_NAME);

    SimpleStringFieldSpecification accountLevelSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.LEVEL);

    SimpleStringFieldSpecification accountEnLevelSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.EN_LEVEL);

    SimpleStringFieldSpecification accountArLevelSpecification =
        getSimpleStringSpecification(status, IHObChartOfAccountsGeneralModel.AR_LEVEL);

    CurrentStatesFieldSpecification accountStateSpecification =
        getCurrentStatesFieldSpecification(
            status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

    Specification querySpecification =
        Specification.where(userCodeSpecification)
            .and(accountNameSpecification)
            .and(accountTypeSpecification)
            .and(accountLevelSpecification)
            .and(parentCodeNameSpecification)
            .and(debitCreditSpecification)
            .and(accountEnLevelSpecification)
            .and(accountArLevelSpecification)
            .and(accountStateSpecification);

    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/mapped-accounts")
  public @ResponseBody String readAccountsConfiguration() {
    List<LObGlobalGLAccountConfig> existingConfig = globalAccountConfigRep.findAll();
    return serializeConfigurationResponse(existingConfig);
  }

  private String serializeConfigurationResponse(List<LObGlobalGLAccountConfig> existingConfig) {
    JsonArray jsonArray = new JsonArray();

    for (MappedAccountsEnum mappedAccounts : MappedAccountsEnum.values()) {
      boolean doesConfigExist = false;
      for (LObGlobalGLAccountConfig config : existingConfig) {
        if (mappedAccounts.name().equals(config.getUserCode())) {
          doesConfigExist = true;
          break;
        }
      }
      if (!doesConfigExist) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(mappedAccounts.name(), getGson().toJsonTree(mappedAccounts.toString()));
        jsonArray.add(jsonObject);
      }
    }
    return serializeRESTSuccessResponse(jsonArray);
  }
}
