package com.ebs.dda.rest.inventory.goodsreceipt;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.shiro.annotations.EBSAuthorizationHandler;
import com.ebs.dac.security.utils.AuthorizationUtils;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptActionNames;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecord;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.masterdata.specifications.ItemVendorRecordSpecification;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingAccountingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.inventory.IObInventoryCompanyGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping(value = "/services/goodsreceipt")
public class DObGoodsReceiptQueryController extends BasicQueryController {

  private static final String ITEM_QUANTITIES = "quantities";
  private static final String BATCHES_KEY = "batches";
  private static final String RESULT_KEY = "result";
  protected final String GR_PO_INVENTORY_DOCUMENT_TYPE = "GR_PO";
  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SUCCESSFUL_CODE = "Gen-msg-11";
  private final EBSAuthorizationHandler authorizationHandler;
  @Autowired private GoodsReceiptItemDataGeneralModelRep goodsReceiptItemDataGeneralModelRep;

  @Autowired
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      goodsReceiptPurchaseOrderDataGeneralModelRep;

  @Autowired private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  @Autowired private ItemUnitOfEntriesRep itemUnitOfEntriesRep;

  @Autowired
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep;

  @Autowired
  private IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
      goodsReceiptBatchedReceivedItemsBatchesGeneralModelRep;

  @Autowired
  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
      iObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep;

  @Autowired
  private IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
      iObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;

  @Autowired
  @Qualifier("goodsReceiptAllowedActionService")
  private AllowedActionService allowedActionService;

  @Autowired private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  @Autowired private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;

  @Autowired
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;

  @Autowired private MObItemGeneralModelRep itemGeneralModelRep;
  @Autowired private IObInventoryCompanyGeneralModelRep goodsReceiptCompanyGeneralModelRep;

  @Autowired
  private IObGoodsReceiptSalesReturnDataGeneralModelRep goodsReceiptSalesReturnDataGeneralModelRep;

  @Autowired private CObGoodsReceiptRep cObGoodsReceiptRep;

  @Autowired
  private IObCostingAccountingDetailsGeneralModelRep
          iObCostingAccountingDetailsGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep
      iObAccountingDocumentActivationDetailsGeneralModelRep;

  @Autowired
  private IObCostingDetailsGeneralModelRep
          iObCostingGeneralModelRep;

  public DObGoodsReceiptQueryController() {
    authorizationHandler = new EBSAuthorizationHandler();
  }

  private RequiresPermissions getReadGoodsReceiptPermissions(String actionName) {
    return AuthorizationUtils.createClassPermission(IDObGoodsReceipt.SYS_NAME, actionName)
        .getPermission();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/state/{code}")
  public @ResponseBody String readGoodsReceiptStateByCode(@PathVariable String code)
      throws Exception {

    authorizationHandler.authorizeAction(
        getReadGoodsReceiptPermissions(IGoodsReceiptActionNames.VIEW_HEADER_SECTION));

    DObGoodsReceiptGeneralModel goodsReceipt = goodsReceiptGeneralModelRep.findOneByUserCode(code);
    String goodsReceiptState = goodsReceipt.getCurrentState();
    return serializeRESTSuccessResponse(goodsReceiptState);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    String readAllPermission = IActionsNames.READ_ALL;
    authorizationManager.authorizeAction(IDObGoodsReceipt.SYS_NAME, readAllPermission);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Direction.DESC, IDObGoodsReceiptGeneralModel.USER_CODE));

    Page<DObGoodsReceiptGeneralModel> goodsReceiptPage = readGoodsReceiptData(conditions, status, pageRequest);

    List<DObGoodsReceiptGeneralModel> goodsReceipts = goodsReceiptPage.getContent();

    return serializeViewAllResponse(goodsReceipts, goodsReceiptPage.getTotalElements());
  }

  private Page<DObGoodsReceiptGeneralModel> readGoodsReceiptData(Set<String> conditions, Status status, Pageable pagable) {
    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObGoodsReceiptGeneralModel.USER_CODE);
    SimpleStringFieldSpecification currentStateSpecification =
        getSimpleStringSpecification(status, IDObGoodsReceiptGeneralModel.STATE);
    JsonFieldSpecification typeSpecification =
        getJsonContainsSpecification(status, IDObGoodsReceiptGeneralModel.TYPE_NAME);
    JsonFieldSpecification storehouseNameSpecification =
        getJsonContainsSpecification(status, IDObGoodsReceiptGeneralModel.STOREHOUSE_NAME);
    DateContainsSpecification postingDateContainsSpecification =
        getDateContainsSpecification(status, IDObGoodsReceiptGeneralModel.ACTIVATION_DATE);
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObGoodsReceiptGeneralModel.PURCHASING_UNIT_NAME, conditions);
    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IDObGoodsReceiptGeneralModel.PURCHASING_UNIT_NAME_EN);

    SimpleStringFieldSpecification businessPartnerCodeSpecification =
        getCustomStringContainsSpecification(
            status, IDObGoodsReceiptGeneralModel.BUSINESS_PARTNER, CODE);

    JsonFieldSpecification businessPartnerNameSpecification =
        getCustomJsonContainsSpecification(
            status, IDObGoodsReceiptGeneralModel.BUSINESS_PARTNER, NAME);

    return goodsReceiptGeneralModelRep
        .findAll(
            Specification.where(userCodeSpecification)
                .and(purchaseUnitConditionSpecification)
                .and(storehouseNameSpecification)
                .and(postingDateContainsSpecification)
                .and(currentStateSpecification)
                .and(typeSpecification)
                .and(businessPartnerCodeSpecification.or(businessPartnerNameSpecification)),
            pagable);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsReceiptCode}/refdocumentdata")
  public @ResponseBody String readRefDocumentData(@PathVariable String goodsReceiptCode)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      if (goodsReceiptGeneralModel.getInventoryDocumentType().equals("GR_PO")) {
        return readPurchaseOrderData(goodsReceiptCode, goodsReceiptGeneralModel);
      } else {
        return readSalesReturnData(goodsReceiptCode, goodsReceiptGeneralModel);
      }
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  private String readSalesReturnData(
      String goodsReceiptCode, DObGoodsReceiptGeneralModel goodsReceiptGeneralModel)
      throws Exception {
    authorizationManager.authorizeAction(
        goodsReceiptGeneralModel, IGoodsReceiptActionNames.VIEW_SALES_RETURN_DATA);
    IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnGeneralModel =
        goodsReceiptSalesReturnDataGeneralModelRep.findOneByGoodsReceiptCode(goodsReceiptCode);
    return serializeRESTSuccessResponse(goodsReceiptSalesReturnGeneralModel);
  }

  private String readPurchaseOrderData(
      String goodsReceiptCode, DObGoodsReceiptGeneralModel goodsReceiptGeneralModel)
      throws Exception {
    authorizationManager.authorizeAction(
        goodsReceiptGeneralModel, IGoodsReceiptActionNames.VIEW_PURCHASE_ORDER_SECTION);
    IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPurchaseOrderData =
        goodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(goodsReceiptCode);
    return serializeRESTSuccessResponse(goodsReceiptPurchaseOrderData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsReceiptCode}/company")
  public @ResponseBody String readCompany(@PathVariable String goodsReceiptCode) throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      authorizationManager.authorizeAction(
          goodsReceiptGeneralModel, IGoodsReceiptActionNames.READ_COMPANY);
      IObInventoryCompanyGeneralModel goodsReceiptCompanyGeneralModel =
          goodsReceiptCompanyGeneralModelRep.findOneByInventoryDocumentCodeAndInventoryDocumentType(
              goodsReceiptCode, goodsReceiptGeneralModel.getInventoryDocumentType());
      return serializeRESTSuccessResponse(goodsReceiptCompanyGeneralModel);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  private List<String> getPurchaseUnitNameByAuthorizationCondition(Set<String> conditions) {
    return getConditionAttributeValue(
        IDObGoodsReceiptGeneralModel.PURCHASING_UNIT_NAME, conditions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsReceiptCode}/generaldata")
  public @ResponseBody String readGoodsReceiptGeneralData(@PathVariable String goodsReceiptCode)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      authorizationManager.authorizeAction(
          goodsReceiptGeneralModel, IGoodsReceiptActionNames.VIEW_HEADER_SECTION);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
    return serializeRESTSuccessResponse(goodsReceiptGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsReceiptCode}/items")
  public @ResponseBody String readIObGoodsReceiptReceivedItems(
      @PathVariable String goodsReceiptCode) throws Exception {

    authorizeActionOnGoodsReceiptIfEntityIsExist(
        goodsReceiptCode, IGoodsReceiptActionNames.VIEW_RECEIVED_ITEMS_SECTION);
    List<IObGoodsReceiptReceivedItemsGeneralModel> goodsReceiptReceivedItemsGeneralModel =
        goodsReceiptReceivedItemsGeneralModelRep.findByGoodsReceiptCodeOrderByGrItemIdDesc(
            goodsReceiptCode);

    JsonArray receivedItemsJsonArray = new JsonArray();

    for (IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel :
        goodsReceiptReceivedItemsGeneralModel) {

      JsonObject receivedItem = serializeReceivedItems(receivedItemGeneralModel);
      receivedItemsJsonArray.add(receivedItem);
    }

    return serializeGoodsReceiptReceivedItems(receivedItemsJsonArray);
  }

  private JsonObject serializeReceivedItems(
      IObGoodsReceiptReceivedItemsGeneralModel generalReceivedItemGeneralModel) {
    JsonObject receivedItem =
        getGson().toJsonTree(generalReceivedItemGeneralModel).getAsJsonObject();

    if (generalReceivedItemGeneralModel
        .getObjectTypeCode()
        .equals(IIObGoodsReceiptReceivedItemsData.BATCHED_ITEMS_OBJECT_TYPE_CODE)) {

      serializeBatchedReceivedItems(generalReceivedItemGeneralModel, receivedItem);
    } else {
      serializeOrdinaryReceivedItems(generalReceivedItemGeneralModel, receivedItem);
    }

    return receivedItem;
  }

  private void serializeBatchedReceivedItems(
      IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel, JsonObject receivedItem) {

    List<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel>
        goodsReceiptBatchedReceivedItemsGeneralModel =
            goodsReceiptBatchedReceivedItemsBatchesGeneralModelRep.findByBatchedItemIdOrderByIdDesc(
                receivedItemGeneralModel.getGRItemId());

    receivedItem
        .getAsJsonObject()
        .add(
            BATCHES_KEY,
            getGson().toJsonTree(goodsReceiptBatchedReceivedItemsGeneralModel).getAsJsonArray());
  }

  private void serializeOrdinaryReceivedItems(
      IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel, JsonObject receivedItem) {

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(
            receivedItemGeneralModel.getGoodsReceiptCode());

    if (goodsReceiptGeneralModel.getInventoryDocumentType().equals(GR_PO_INVENTORY_DOCUMENT_TYPE)) {
      List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
          goodsReceiptPurchaseOrderOrdinaryReceivedItemsGeneralModel =
              iObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
                  .findByOrOrdinaryItemIdOrderByIdDesc(receivedItemGeneralModel.getGRItemId());

      receivedItem
          .getAsJsonObject()
          .add(
              ITEM_QUANTITIES,
              getGson()
                  .toJsonTree(goodsReceiptPurchaseOrderOrdinaryReceivedItemsGeneralModel)
                  .getAsJsonArray());
    } else {
      List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
          goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelList =
              iObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
                  .findByOrdinaryItemIdOrderByIdAsc(receivedItemGeneralModel.getGRItemId());

      receivedItem
          .getAsJsonObject()
          .add(
              ITEM_QUANTITIES,
              getGson()
                  .toJsonTree(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelList)
                  .getAsJsonArray());
    }
  }

  private String serializeGoodsReceiptReceivedItems(JsonArray receivedItemsJsonArray) {

    JsonObject successResponse = new JsonObject();

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESSFUL_CODE);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.add(RESULT_KEY, receivedItemsJsonArray);

    return successResponse.toString();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/alloweditems/{goodsReceiptCode}")
  public @ResponseBody String readItemsOptions(@PathVariable String goodsReceiptCode) {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    List<GoodsReceiptItemDataGeneralModel> authorizedItemsList = getItemList(goodsReceiptCode);
    List<IObGoodsReceiptReceivedItemsGeneralModel> savedGoodsReceiptItems =
        goodsReceiptReceivedItemsGeneralModelRep.findByGoodsReceiptCode(goodsReceiptCode);
    List<GoodsReceiptItemDataGeneralModel> excludedItems =
        excludeGoodsReceiptSavedItems(authorizedItemsList, savedGoodsReceiptItems);
    return serializeRESTSuccessResponse(excludedItems);
  }

  private List<GoodsReceiptItemDataGeneralModel> excludeGoodsReceiptSavedItems(
      List<GoodsReceiptItemDataGeneralModel> authorizedItemsList,
      List<IObGoodsReceiptReceivedItemsGeneralModel> savedGoodsReceiptItems) {
    Set<String> savedItemsCodes = new HashSet<>();
    List<GoodsReceiptItemDataGeneralModel> finalAuthorizedItems = new ArrayList<>();
    for (IObGoodsReceiptReceivedItemsGeneralModel savedItem : savedGoodsReceiptItems) {
      savedItemsCodes.add(savedItem.getItemCode());
    }
    for (GoodsReceiptItemDataGeneralModel authorizedItem : authorizedItemsList) {
      if (!savedItemsCodes.contains(authorizedItem.getItemCode())) {
        finalAuthorizedItems.add(authorizedItem);
      }
    }
    return finalAuthorizedItems;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/stocktypes")
  public @ResponseBody String getStockTypes() {
    authorizationManager.authorizeAction(StockTypeEnum.SYS_NAME, IActionsNames.READ_ALL);
    return serializeRESTSuccessResponse(StockTypeEnum.StockType.values());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/itemqtydn/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String getQuantityInDNPerItem(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    IObGoodsReceiptPurchaseOrderDataGeneralModel purchaseOrderData =
        goodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(goodsReceiptCode);
    BigDecimal qtyInDN = null;
    boolean existInPurchaseOrder = false;
    LocalizedString unitSymbol = null;
    if (purchaseOrderData != null) {
      String purchaseOrderCode = purchaseOrderData.getPurchaseOrderCode();
      IObOrderLineDetailsGeneralModel orderItem =
          orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(purchaseOrderCode, itemCode);
      if (orderItem != null) {
        qtyInDN = orderItem.getQtyInDn();
        unitSymbol = orderItem.getUnitSymbol();
        existInPurchaseOrder = true;
      }
    }
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("qtyInDN", qtyInDN);
    jsonObject.addProperty("existInPO", existInPurchaseOrder);
    jsonObject.add("unitSymbol", getGson().toJsonTree(unitSymbol));
    return serializeRESTSuccessResponse(jsonObject);
  }

  private List<GoodsReceiptItemDataGeneralModel> getItemList(String goodsReceiptCode) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> allowedPurchaseUnitNames =
        getConditionAttributeValue(IItemVendorRecord.PURCHASING_UNIT_NAME, conditions);
    List<GoodsReceiptItemDataGeneralModel> allowedItems = new ArrayList<>();
    if (allowedPurchaseUnitNames.contains(goodsReceiptGeneralModel.getPurchaseUnitNameEn())
        || allowedPurchaseUnitNames.contains(WILD_CARD_CONDITION)) {
      JsonListContainsSpecification ivrPurchaseUnitsAuthorizationSpecification =
          new JsonListContainsSpecification(
              IItemVendorRecord.PURCHASING_UNIT_NAME,
              Arrays.asList(goodsReceiptGeneralModel.getPurchaseUnitNameEn()));
      allowedItems =
          goodsReceiptItemDataGeneralModelRep.findAll(
              Specification.where(ivrPurchaseUnitsAuthorizationSpecification)
                  .and(
                      ItemVendorRecordSpecification.withVendorCode(
                          goodsReceiptGeneralModel.getBusinessPartnerCode())));
    }
    return allowedItems;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/item/unitofmeasures/{itemCode}")
  public @ResponseBody String getGoodsReceiptItemUnitOfMeasure(@PathVariable String itemCode) {

    List<ItemUnitOfEntries> unitOfMeasures = itemUnitOfEntriesRep.findAllByItemCode(itemCode);

    return serializeRESTSuccessResponse(unitOfMeasures);
  }

  private void authorizeActionOnGoodsReceiptIfEntityIsExist(
      String goodsReceiptCode, String permission) throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      authorizationManager.authorizeAction(goodsReceiptGeneralModel, permission);
    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(DObGoodsReceiptGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{goodsReceiptCode}")
  public @ResponseBody String readActions(@PathVariable String goodsReceiptCode) throws Exception {

    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    if (goodsReceipt == null) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions = allowedActionService.getObjectScreenAllowedActions(goodsReceipt);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    if (goodsReceipt.isGoodsReceiptPurchaseOrder()) {
      allowedActions.remove(IGoodsReceiptActionNames.VIEW_SALES_RETURN_DATA);
    } else {
      allowedActions.remove(IGoodsReceiptActionNames.VIEW_PURCHASE_ORDER_SECTION);
      allowedActions.remove(IGoodsReceiptActionNames.CALCULATE_ACTUAL_COST);
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/navigationdata/{goodsReceiptCode}")
  public @ResponseBody String getItemNavigationData(@PathVariable String goodsReceiptCode)
      throws Exception {
    authorizeActionOnGoodsReceiptIfEntityIsExist(goodsReceiptCode, IActionsNames.READ_ALL);
    boolean nextCodeExists = false, previousCodeExists = false;
    try {
      getNextGoodsReceiptCode(goodsReceiptCode);
      nextCodeExists = true;
    } catch (DataNavigationException e) {
    }

    try {
      getPreviousGoodsReceiptCode(goodsReceiptCode);
      previousCodeExists = true;
    } catch (DataNavigationException e) {
    }

    return serializeObjectScreenNavigationData(nextCodeExists, previousCodeExists);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/next/{goodsReceiptCode}")
  public @ResponseBody String readNextCode(@PathVariable String goodsReceiptCode) throws Exception {
    authorizeActionOnGoodsReceiptIfEntityIsExist(goodsReceiptCode, IActionsNames.READ_ALL);
    String nextGoodsReceiptCode = getNextGoodsReceiptCode(goodsReceiptCode);
    return serializeReadNextResponse(nextGoodsReceiptCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/previous/{goodsReceiptCode}")
  public @ResponseBody String readPreviousCode(@PathVariable String goodsReceiptCode)
      throws Exception {
    authorizeActionOnGoodsReceiptIfEntityIsExist(goodsReceiptCode, IActionsNames.READ_ALL);
    String previousGoodsReceiptCode = getPreviousGoodsReceiptCode(goodsReceiptCode);
    return serializeReadPreviousResponse(previousGoodsReceiptCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/item/detail/unitofmeasures/{itemCode}/{vendorCode}/{purchaseUnitCode}")
  public @ResponseBody String readPurchaseOrderItems(
      @PathVariable String itemCode,
      @PathVariable String vendorCode,
      @PathVariable String purchaseUnitCode)
      throws Exception {

    try {
      checkIfReadPermissionIsAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

      List<ItemVendorRecordUnitOfMeasuresGeneralModel> unitOfMeasuresList =
          itemVendorRecordUnitOfMeasuresGeneralModelRep
              .findAllByItemCodeAndVendorCodeAndPurchasingUnitCodeOrderById(
                  itemCode, vendorCode, purchaseUnitCode);

      ItemVendorRecordUnitOfMeasuresGeneralModel basicUnitOfMeasure =
          getItemBasicUnitOfMeasure(itemCode);

      ItemVendorRecordGeneralModel ivrUoMRecord =
          itemVendorRecordGeneralModelRep
              .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                  itemCode,
                  vendorCode,
                  basicUnitOfMeasure.getAlternativeUnitOfMeasureCode(),
                  purchaseUnitCode);

      if (ivrUoMRecord != null) {
        basicUnitOfMeasure.setItemCodeAtVendor(ivrUoMRecord.getItemVendorCode());
        unitOfMeasuresList.add(0, basicUnitOfMeasure);
      }
      return serializeRESTSuccessResponse(unitOfMeasuresList);
    } catch (ResourceAccessException ex) {
      throw new AuthorizationException();
    }
  }

  private ItemVendorRecordUnitOfMeasuresGeneralModel getItemBasicUnitOfMeasure(
      @PathVariable String itemCode) {
    ItemVendorRecordUnitOfMeasuresGeneralModel basicUnitOfMeasure =
        new ItemVendorRecordUnitOfMeasuresGeneralModel();
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    basicUnitOfMeasure.setConversionFactor(1.0f);
    basicUnitOfMeasure.setAlternativeUnitOfMeasureCode(item.getBasicUnitOfMeasureCode());
    basicUnitOfMeasure.setAlternativeUnitOfMeasureName(item.getBasicUnitOfMeasureSymbol());
    return basicUnitOfMeasure;
  }

  private String getNextGoodsReceiptCode(String currentGoodsReceiptCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    DObGoodsReceiptGeneralModel nextGoodsReceipt = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      nextGoodsReceipt =
          goodsReceiptGeneralModelRep.findNextByUserCode(
              Long.valueOf(currentGoodsReceiptCode), purchaseUnitNames);
    } else {
      nextGoodsReceipt =
          goodsReceiptGeneralModelRep.findNextByUserCodeUnconditionalRead(
              Long.valueOf(currentGoodsReceiptCode));
    }
    return getTargetGoodsReceiptCode(nextGoodsReceipt);
  }

  private String getPreviousGoodsReceiptCode(String currentGoodsReceiptCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    DObGoodsReceiptGeneralModel prevGoodsReceipt = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      prevGoodsReceipt =
          goodsReceiptGeneralModelRep.findPreviousByUserCode(
              Long.valueOf(currentGoodsReceiptCode), purchaseUnitNames);
    } else {
      prevGoodsReceipt =
          goodsReceiptGeneralModelRep.findPreviousByUserCodeUnconditionalRead(
              Long.valueOf(currentGoodsReceiptCode));
    }
    return getTargetGoodsReceiptCode(prevGoodsReceipt);
  }

  private String getTargetGoodsReceiptCode(DObGoodsReceiptGeneralModel goodsReceipt)
      throws Exception {
    if (goodsReceipt == null) {
      throw new DataNavigationException();
    }
    return goodsReceipt.getUserCode();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readType() {

    authorizationManager.authorizeAction(ICObGoodsReceipt.SYS_NAME, IActionsNames.READ_ALL);

    List<CObGoodsReceipt> cObGoodsReceipts = cObGoodsReceiptRep.findAllByOrderByIdDesc();

    return serializeRESTSuccessResponse(cObGoodsReceipts);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsReceiptCode}/accountingdetails")
  public @ResponseBody String readCostingdetails(@PathVariable String goodsReceiptCode)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      authorizationManager.authorizeAction(
          goodsReceiptGeneralModel, IGoodsReceiptActionNames.READ_ACCOUNTING_DETAILS);

      List<IObCostingAccountingDetailsGeneralModel>
          iObCostingAccountingDetailsGeneralModel =
              iObCostingAccountingDetailsGeneralModelRep.findByGoodsReceiptCodeOrderByAccountingEntryDesc(
                  goodsReceiptCode);

      return serializeRESTSuccessResponse(iObCostingAccountingDetailsGeneralModel);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{goodsReceiptCode}/costing/activationdetails")
  public @ResponseBody String readCostingActivationDetails(
      @PathVariable String goodsReceiptCode) throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    try {
      authorizationManager.authorizeAction(
          goodsReceiptGeneralModel,
          IGoodsReceiptActionNames.READ_ACCOUNTING_DOCUMENT_ACTIVATION_DETAILS);

      IObCostingDetailsGeneralModel
          iObCostingDetailsGeneralModel =
              iObCostingGeneralModelRep.findOneByGoodsReceiptCode(
                  goodsReceiptCode);
      //TODO: Remove the null check when we implement the accounting document creation
      IObAccountingDocumentActivationDetailsGeneralModel
              iObAccountingDocumentActivationDetailsGeneralModel = null;
      if (iObCostingDetailsGeneralModel != null)
          iObAccountingDocumentActivationDetailsGeneralModel =
              iObAccountingDocumentActivationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
                  iObCostingDetailsGeneralModel.getUserCode(),
                  IDocumentTypes.COSTING);
      return serializeRESTSuccessResponse(iObAccountingDocumentActivationDetailsGeneralModel);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }
}
