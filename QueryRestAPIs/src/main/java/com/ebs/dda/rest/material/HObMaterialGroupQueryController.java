package com.ebs.dda.rest.material;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;
import com.ebs.dda.jpa.masterdata.itemgroup.IHObItemGroup;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/material")
public class HObMaterialGroupQueryController extends BasicQueryController {

  private static final Set<String> itemGroupObjectScreenAllowedActions =
      new HashSet<>(Arrays.asList(IActionsNames.READ_ALL, IActionsNames.DELETE));
  private final String CODE = "userCode";
  @Autowired private CObMaterialGroupGeneralModelRep materialGroupGeneralModelRep;

  @Autowired
  @Qualifier("itemGroupAllowedActionService")
  private AllowedActionService allowedActionService;

  @Autowired private AuthorizationService authorizationService;

  @RequestMapping(method = RequestMethod.GET, value = "/getall")
  public @ResponseBody String readAll() throws Exception {
    checkIfReadPermissionIsAllowed(IHObItemGroup.SYS_NAME, IActionsNames.READ_ALL);
    HierarchyLevelEnumType.HierarchyLevelEnum hierarchyLevelEnum =
        HierarchyLevelEnumType.HierarchyLevelEnum.LEAF;
    HierarchyLevelEnumType hierarchyLevelEnumType = new HierarchyLevelEnumType();
    hierarchyLevelEnumType.setId(hierarchyLevelEnum);

    List<CObMaterialGroupGeneralModel> materialGroupGeneralModels =
        materialGroupGeneralModelRep.findByLevel(hierarchyLevelEnumType);

    return serializeRESTSuccessResponse(materialGroupGeneralModels);
  }

  @RequestMapping(value = "/allHiererchy", method = RequestMethod.GET)
  @ResponseBody
  public String getAllHiererchyItemGroups() throws Exception {
    HierarchyLevelEnumType level = new HierarchyLevelEnumType();
    level.setId(HierarchyLevelEnumType.HierarchyLevelEnum.LEAF);
    return serializeRESTSuccessResponse(materialGroupGeneralModelRep.findAllByLevelNot(level));
  }

  private Page<CObMaterialGroupGeneralModel> readItemGroupsData(Pageable pagable) {
    return materialGroupGeneralModelRep.findAll(pagable);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filters}")
  public @ResponseBody String itemGoupsReadAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filters,
      HttpServletRequest request) {
    authorizationManager.authorizeAction(IHObItemGroup.SYS_NAME, IActionsNames.READ_ALL);
    PageRequest pageRequest = PageRequest.of(pageNum, rowsNum, Sort.by(Sort.Direction.DESC, CODE));
    Page<CObMaterialGroupGeneralModel> itemGroupsPage = readItemGroupsData(pageRequest);
    List<CObMaterialGroupGeneralModel> items = itemGroupsPage.getContent();
    return serializeViewAllResponse(items, itemGroupsPage.getTotalElements());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions = getItemGroupAllowedActions();
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{itemGroupCode}")
  public @ResponseBody String readActions(@PathVariable String itemGroupCode) throws Exception {

    CObMaterialGroupGeneralModel materialGroup =
        materialGroupGeneralModelRep.findOneByUserCode(itemGroupCode);
    if (materialGroup == null) {
      throw new InstanceNotExistException();
    }
    Set<String> currentUserAllowedActions =
        authorizationService.getCurrentUserAuthorizedActions(materialGroup);

    Set<String> itemGroupAllowedActions = getItemGroupAllowedActions();

    if (currentUserAllowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    if (isWildCardPermission(currentUserAllowedActions)) {
      return serializeRESTSuccessResponse(itemGroupAllowedActions);
    }
    currentUserAllowedActions.retainAll(itemGroupAllowedActions);

    return serializeRESTSuccessResponse(currentUserAllowedActions);
  }

  private Set<String> getItemGroupAllowedActions() {
    Set<String> allowedActions = new HashSet<>();
    allowedActions.add("ReadAll");
    allowedActions.add("Delete");
    allowedActions.add("Create");
    return allowedActions;
  }

  private boolean isWildCardPermission(Set<String> authorizedActions) {
    return authorizedActions.contains(AllowedActionService.WILD_CARD_PERMISSION);
  }
}
