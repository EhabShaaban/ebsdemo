package com.ebs.dda.rest.accounting.settlement;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.dbo.jpa.entities.businessobject.enums.SettlementTypeEnum;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementActionNames;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.*;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/accounting/settlement")
public class DObSettlementQueryController extends BasicQueryController implements InitializingBean {

  @Autowired
  @Qualifier("settlementAllowedActionService")
  private AllowedActionService settlementAllowedActionService;

  @Autowired private IUserAccountSecurityService userAccountSecurityService;
  @Autowired private DObSettlementGeneralModelRep settlementGeneralModelRep;
  @Autowired private IObSettlementCompanyDataGeneralModelRep companyDataGeneralModelRep;
  @Autowired private IObSettlementDetailsGeneralModelRep settlementDetailsGMRep;

  @Autowired
  private IObSettlementAccountDetailsGeneralModelRep settlementAccountDetailsGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  @Autowired private IObSettlementSummaryGeneralModelRep summaryGeneralModelRep;

  @GetMapping("/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        settlementAllowedActionService.getHomeScreenAllowedActions(DObSettlement.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{settlementcode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String settlementcode)
      throws Exception {
    DObSettlementGeneralModel settlement =
        settlementGeneralModelRep.findOneByUserCode(settlementcode);
    checkIfEntityIsNull(settlement);

    Set<String> allowedActions =
        settlementAllowedActionService.getObjectScreenAllowedActions(settlement);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObSettlement.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObSettlementGeneralModel.USER_CODE));

    Specification querySpecification = getSettlementDataSpecification(conditions, status);
    Page<DObSettlementGeneralModel> settlementsPage =
        settlementGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObSettlementGeneralModel> settlements = settlementsPage.getContent();
    return serializeViewAllResponse(settlements, settlementsPage.getTotalElements());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readSettlementTypes() {
    List<SettlementTypeEnum.SettlementType> settlementType =
        new ArrayList<>(Arrays.asList(SettlementTypeEnum.SettlementType.values()));
    return serializeRESTSuccessResponse(settlementType);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{settlementCode}/accountingdetails")
  public @ResponseBody String readAccountingDetailsData(@PathVariable String settlementCode)
      throws Exception {
    checkExsitanceAndAuthority(
        settlementCode, IDObSettlementActionNames.READ_SETTLEMENT_ACCOUNTING_DETAILS);

    List<IObSettlementAccountDetailsGeneralModel> accountDetailsGeneralModel =
        settlementAccountDetailsGeneralModelRep.findByDocumentCodeOrderByAccountingEntryDesc(
            settlementCode);

    return serializeRESTSuccessResponse(accountDetailsGeneralModel);
  }

  private Specification getSettlementDataSpecification(Set<String> conditions, Status status) {

    Specification querySpecification;

    List<String> businessUnitNames =
        getConditionAttributeValue(IDObSettlementGeneralModel.PURCHASING_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObSettlementGeneralModel.PURCHASING_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.USER_CODE);

    SimpleStringFieldSpecification settlementStateSpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.CURRENT_STATES);

    SimpleStringFieldSpecification activatedBySpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.ACTIVATED_BY);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.SETTLEMENT_TYPE);

    SimpleStringFieldSpecification businessUnitSpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.PURCHASE_UNIT_CODE);

    SimpleStringFieldSpecification companyNameEnSpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.COMPANY_NAME_EN);

    JsonFieldSpecification companyNameSpecification =
        getJsonContainsSpecification(status, IDObSettlementGeneralModel.COMPANY_NAME);

    SimpleStringFieldSpecification currencySpecification =
        getSimpleStringSpecification(status, IDObSettlementGeneralModel.CURRENCY_ISO);

    DateContainsSpecification creationDateSpecification =
        getDateContainsSpecification(status, IDObSettlementGeneralModel.CREATION_DATE);

    querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(businessUnitSpecification)
            .and(settlementStateSpecification)
            .and(userCodeSpecification)
            .and(activatedBySpecification)
            .and(typeSpecification)
            .and(currencySpecification)
            .and(creationDateSpecification)
            .and(companyNameSpecification)
            .and(companyNameEnSpecification);

    querySpecification = applyCreatedByConditionSpecification(querySpecification, conditions);
    return querySpecification;
  }

  private Specification applyCreatedByConditionSpecification(
      Specification querySpecification, Set<String> conditions) {
    List<String> creationNames =
        getConditionAttributeValue(IDObSettlementGeneralModel.CREATION_INFO, conditions);
    if (!creationNames.isEmpty()
        && creationNames.get(0).contains(DObSettlementQueryControllerUtils.LOGGED_IN_USER)) {
      String loggedinUserInfo = getLoggedInUserName();
      CreatedBySpecification createdByAuthorizationSpecification =
          new CreatedBySpecification(loggedinUserInfo);
      querySpecification = querySpecification.and(createdByAuthorizationSpecification);
    }
    return querySpecification;
  }

  private String getLoggedInUserName() {
    return userAccountSecurityService.getLoggedInUser().toString();
  }

  @GetMapping("/{settlementCode}/details")
  public @ResponseBody String readSettlementDetails(@PathVariable String settlementCode)
      throws Exception {

    checkExsitanceAndAuthority(settlementCode, IDObSettlementActionNames.READ_SETTLEMENT_DETAILS);

    IObSettlementDetailsGeneralModel detailsGM =
        settlementDetailsGMRep.findOneBySettlementCode(settlementCode);

    return serializeRESTSuccessResponse(detailsGM);
  }

  private DObSettlementGeneralModel checkExsitanceAndAuthority(String settlementCode, String action)
      throws Exception {
    DObSettlementGeneralModel settlementGM =
        settlementGeneralModelRep.findOneByUserCode(settlementCode);
    checkIfEntityIsNull(settlementGM);
    try {
      this.authorizationManager.authorizeAction(settlementGM, action);
      return settlementGM;
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  @GetMapping(value = "/{settlementCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String settlementCode)
      throws Exception {

    DObSettlementGeneralModel generalData =
        checkExsitanceAndAuthority(settlementCode, IActionsNames.READ_GENERAL_DATA);

    return serializeRESTSuccessResponse(generalData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{settlementCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String settlementCode)
      throws Exception {
    checkExsitanceAndAuthority(
        settlementCode, IDObSettlementActionNames.READ_SETTLEMENT_COMPANY_DATA);

    IObSettlementCompanyDataGeneralModel companyDataGeneralModel =
        companyDataGeneralModelRep.findOneByDocumentCode(settlementCode);

    return serializeRESTSuccessResponse(companyDataGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{settlementCode}/postingdetails")
  public @ResponseBody String readActivationDetails(@PathVariable String settlementCode)
      throws Exception {
    checkExsitanceAndAuthority(
        settlementCode, IDObSettlementActionNames.READ_SETTLEMENT_ACTIVATION_DETAILS);
    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            settlementCode, IDocumentTypes.SETTLEMENT);
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{settlementCode}/summary")
  public @ResponseBody String readSummary(@PathVariable String settlementCode) throws Exception {
    checkExsitanceAndAuthority(settlementCode, IDObSettlementActionNames.READ_SETTLEMENT_SUMMARY);
    IObSettlementSummaryGeneralModel summaryGeneralModel =
        summaryGeneralModelRep.findOneBySettlementCode(settlementCode);
    return serializeRESTSuccessResponse(summaryGeneralModel);
  }

  @Override
  public void afterPropertiesSet() throws Exception {}
}
