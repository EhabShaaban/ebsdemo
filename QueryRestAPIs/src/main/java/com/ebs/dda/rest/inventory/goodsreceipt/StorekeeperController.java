package com.ebs.dda.rest.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.rest.BasicQueryController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 21, 2018 10:01:40 AM
 */
@RestController
@RequestMapping(value = "/services/storekeepers")
public class StorekeeperController extends BasicQueryController {

  @Autowired private StoreKeeperRep storeKeeperRep;

  @RequestMapping(method = RequestMethod.GET)
  public @ResponseBody String readStoreKeepers() {

    List<StoreKeeper> storeKeepers = storeKeeperRep.findAll();
    return serializeRESTSuccessResponse(storeKeepers);
  }
}
