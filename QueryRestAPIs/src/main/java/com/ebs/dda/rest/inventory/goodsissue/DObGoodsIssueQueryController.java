package com.ebs.dda.rest.inventory.goodsissue;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObGoodsIssueAuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.inventory.goodsissue.apis.IDObGoodsIssueActionNames;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.inventory.goodsissue.*;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DateContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.IObInventoryCompanyGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.CObGoodsIssueRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.QueryControllerUtils;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/goodsissue")
public class DObGoodsIssueQueryController extends BasicQueryController {
  protected final String Customer = "customer";

  @Autowired
  @Qualifier("goodsIssueAllowedActionService")
  private AllowedActionService goodsIssueAllowedActionService;

  @Autowired private DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep;

  @Autowired private IObInventoryCompanyGeneralModelRep goodsIssueCompanyGeneralModelRep;

  @Autowired
  private IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep;

  @Autowired private IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep;

  @Autowired private CObGoodsIssueRep cobGoodsIssueRep;

  @Autowired private DObGoodsIssueAuthorizationManager goodsIssueAuthorizationManager;

  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        goodsIssueAllowedActionService.getHomeScreenAllowedActions(DObGoodsIssueGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    AuthorizedPermissionsConfig dropDownPermissionsConfig = constructDropDownConfiguration();
    Set<String> authorizedDropDownReads = getAuthorizedReads(dropDownPermissionsConfig);
    allowedActions.addAll(authorizedDropDownReads);
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{goodsIssueCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String goodsIssueCode)
      throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);

    if (goodsIssueGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions =
        goodsIssueAllowedActionService.getObjectScreenAllowedActions(goodsIssueGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    if (goodsIssueGeneralModel.isGoodsIssueSalesInvoice()) {
      allowedActions.remove(IDObGoodsIssueActionNames.READ_SALES_ORDER_DATA);
    } else {
      allowedActions.remove(IDObGoodsIssueActionNames.READ_SALES_INVOICE_DATA);
      allowedActions.remove(IDObGoodsIssueActionNames.UPDATE_SALES_INVOICE_DATA);
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/goodsissuetypes")
  public @ResponseBody String readGoodsIssueTypes() throws Exception {
    try {
      checkIfReadPermissionIsAllowed(ICObGoodsIssue.SYS_NAME, READ_ALL);

      List<CObGoodsIssue> cobGoodsIssueList = cobGoodsIssueRep.findAll();
      return serializeRESTSuccessResponse(cobGoodsIssueList);
    } catch (ResourceAccessException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsIssueCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String goodsIssueCode)
      throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);
    QueryControllerUtils.checkIfEntityIsNull(goodsIssueGeneralModel);
    goodsIssueAuthorizationManager.authorizeActionOnSection(
        goodsIssueGeneralModel, IDObGoodsIssueActionNames.READ_GENERAL_DATA);
    return serializeRESTSuccessResponse(goodsIssueGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsIssueCode}/company")
  public @ResponseBody String readCompany(@PathVariable String goodsIssueCode) throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);
    QueryControllerUtils.checkIfEntityIsNull(goodsIssueGeneralModel);
    goodsIssueAuthorizationManager.authorizeActionOnSection(
        goodsIssueGeneralModel, IDObGoodsIssueActionNames.READ_COMPANY);
    IObInventoryCompanyGeneralModel goodsIssueCompanyGeneralModel =
        goodsIssueCompanyGeneralModelRep.findOneByInventoryDocumentCodeAndInventoryDocumentType(
            goodsIssueCode, goodsIssueGeneralModel.getInventoryDocumentType());
    return serializeRESTSuccessResponse(goodsIssueCompanyGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsIssueCode}/refdocumentdata")
  public @ResponseBody String readRefDocumentData(@PathVariable String goodsIssueCode)
      throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);
    QueryControllerUtils.checkIfEntityIsNull(goodsIssueGeneralModel);
    String actionName =
        getActionNameBaseOnGoodsIssueType(goodsIssueGeneralModel.getInventoryDocumentType());
    goodsIssueAuthorizationManager.authorizeActionOnSection(goodsIssueGeneralModel, actionName);
    IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
        goodsIssueRefDocumentDataGeneralModelRep.findOneByGoodsIssueCode(goodsIssueCode);
    return serializeRESTSuccessResponse(goodsIssueRefDocumentDataGeneralModel);
  }

  private String getActionNameBaseOnGoodsIssueType(String inventoryDocumentType) {

    if (inventoryDocumentType.equals("GI_SI")) {
      return IDObGoodsIssueActionNames.READ_SALES_INVOICE_DATA;
    } else {
      return IDObGoodsIssueActionNames.READ_SALES_ORDER_DATA;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsIssueCode}/items")
  public @ResponseBody String readItems(@PathVariable String goodsIssueCode) throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);
    QueryControllerUtils.checkIfEntityIsNull(goodsIssueGeneralModel);
    goodsIssueAuthorizationManager.authorizeActionOnSection(
        goodsIssueGeneralModel, IDObGoodsIssueActionNames.READ_ITEMS);
    List<IObGoodsIssueItemGeneralModel> GoodsIssueItemGeneralModels =
        goodsIssueItemGeneralModelRep.findByGoodsIssueCodeOrderByIdDesc(goodsIssueCode);
    return serializeRESTSuccessResponse(GoodsIssueItemGeneralModels);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}/salesinvoices")
  public @ResponseBody String readReadyForDeliveringSalesInvoices(
      @PathVariable String businessUnitCode) {
    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL);
    List<DObSalesInvoiceGeneralModel> salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findAllByPurchaseUnitCodeAndCurrentStatesLikeOrderByIdDesc(
            businessUnitCode,
            '%' + DObSalesInvoiceWithoutReferenceStateMachine.READY_FOR_DELIVERING_STATE + '%');
    return serializeRESTSuccessResponse(salesInvoiceGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObGoodsIssue.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObGoodsIssueGeneralModel.USER_CODE));

    Specification querySpecification = getGoodsIssueDataSpecification(conditions, status);
    Page<DObGoodsIssueGeneralModel> goodsIssuePage =
        goodsIssueGeneralModelRep.findAll(querySpecification, pageRequest);
    List goodsIssues = goodsIssuePage.getContent();

    return serializeViewAllResponse(goodsIssues, goodsIssuePage.getTotalElements());
  }

  private Specification getGoodsIssueDataSpecification(Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME, conditions);
    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObGoodsIssueGeneralModel.USER_CODE);

    SimpleStringFieldSpecification goodsIssueStateSpecification =
        getSimpleStringSpecification(status, IDObGoodsIssueGeneralModel.CURRENT_STATES);

    JsonFieldSpecification goodsIssueStoreSpecification =
        getJsonContainsSpecification(status, IDObGoodsIssueGeneralModel.STORE_HOUSE_NAME);

    DateContainsSpecification creationDateContainsSpecification =
        getDateContainsSpecification(status, IDObGoodsIssueGeneralModel.CREATION_DATE);

    SimpleStringFieldSpecification customerCodeSpecification =
        getCustomStringContainsSpecification(status, Customer, CODE);

    JsonFieldSpecification customerNameSpecification =
        getCustomJsonContainsSpecification(status, Customer, NAME);

    JsonFieldSpecification purchaseUnitNameSpecification =
        getJsonContainsSpecification(status, IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME);

    JsonFieldSpecification goodsIssueTypeNameSpecification =
        getJsonContainsSpecification(status, IDObGoodsIssueGeneralModel.TYPE_NAME);

    Specification querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(goodsIssueStateSpecification)
            .and(userCodeSpecification)
            .and(goodsIssueStoreSpecification)
            .and(creationDateContainsSpecification)
            .and(customerCodeSpecification.or(customerNameSpecification))
            .and(goodsIssueTypeNameSpecification)
            .and(purchaseUnitNameSpecification);
    return querySpecification;
  }

  //  TODO : should be abstract function in basic query controller after all query controllers have
  // implemented it.
  protected AuthorizedPermissionsConfig constructDropDownConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
        ICObGoodsIssue.SYS_NAME, IActionsNames.READ_ALL, "ReadGoodsIssueTypes");
    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnits");
    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME,
        IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER,
        "ReadBusinessUnits");
    return configuration;
  }
}
