package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.accounting.landedcost.CObLandedCostType;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCostType;
import com.ebs.dda.jpa.inventory.goodsissue.CObGoodsIssue;
import com.ebs.dda.repositories.accounting.landedcost.CObLandedCostTypeRep;
import com.ebs.dda.repositories.inventory.goodsissue.CObGoodsIssueRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/accounting/landedcost")
public class CObLandedCostQueryController extends BasicQueryController {
    @Autowired
    private CObLandedCostTypeRep landedCostTypeRep;

    @RequestMapping(method = RequestMethod.GET, value = "/types")
    public @ResponseBody
    String readGoodsIssueTypes() throws Exception {
        try {
            checkIfReadPermissionIsAllowed(ICObLandedCost.SYS_NAME, READ_ALL);

            List<CObLandedCostType> landedCostList = landedCostTypeRep.findAll();
            return serializeRESTSuccessResponse(landedCostList);
        } catch (ResourceAccessException ex) {
            AuthorizationException authorizationException = new AuthorizationException(ex);
            throw authorizationException;
        }
    }


}
