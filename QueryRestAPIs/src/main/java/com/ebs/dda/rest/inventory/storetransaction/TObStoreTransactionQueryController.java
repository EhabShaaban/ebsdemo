package com.ebs.dda.rest.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.ITObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.inventory.storetransaction.ReadStoreTransactionSpecification;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/storetransaction")
public class TObStoreTransactionQueryController extends BasicQueryController {

    @Autowired
    private TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep;

    @RequestMapping(method = RequestMethod.GET, value = "/transactiontypes")
    public @ResponseBody
    String getTransactionTypes() {
        authorizationManager.authorizeAction(TransactionTypeEnum.SYS_NAME, READ_ALL);
        return serializeRESTSuccessResponse(TransactionTypeEnum.TransactionType.values());
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
    public @ResponseBody
    String readAll(
            @PathVariable int pageNum,
            @PathVariable int rowsNum,
            @PathVariable String filter,
            HttpServletRequest request) {

        authorizationManager.authorizeAction(ITObStoreTransactionGeneralModel.SYS_NAME, READ_ALL);
        Set<String> conditions = authorizationManager.getPermissionConditions();

        Status status = constructStatus(pageNum, rowsNum, filter, request);
        PageRequest pageRequest =
                PageRequest.of(
                        pageNum,
                        rowsNum,
                        Sort.by(Sort.Direction.DESC, ITObStoreTransactionGeneralModel.ST_CODE));

        Specification querySpecification = getStoreTransactionDataSpecification(status, conditions);

        Page <TObStoreTransactionGeneralModel>  storeTransactionPage = storeTransactionGeneralModelRep.findAll(querySpecification, pageRequest);
        List<TObStoreTransactionGeneralModel> storeTransactions = storeTransactionPage.getContent();

        return serializeViewAllResponse(storeTransactions, storeTransactionPage.getTotalElements());
    }

    private Specification getStoreTransactionDataSpecification(
            Status status, Set<String> conditions) {

        List<String> purchaseUnitNames =
                getConditionAttributeValue(ITObStoreTransactionGeneralModel.PURCHASEUNIT_NAME, conditions);

        SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
                new SinglePurchaseUnitNamesConditionSpecification(
                        purchaseUnitNames, ITObStoreTransactionGeneralModel.PURCHASEUNIT_NAME_EN);

        SimpleStringFieldSpecification userCodeSpecification =
                getSimpleStringSpecification(status, ITObStoreTransactionGeneralModel.ST_CODE);
        DateContainsSpecification originalAddingDateContainsSpecification =
                getDateContainsSpecification(status, ITObStoreTransactionGeneralModel.ORIGINAL_ADDING_DATE);
        SimpleStringFieldSpecification transactionTypeSpecification =
                getSimpleStringSpecification(status, ITObStoreTransactionGeneralModel.TRANSACTION_OPERATION);
        SimpleStringFieldSpecification companyCodeSpecification =
                getSimpleStringSpecification(status, ITObStoreTransactionGeneralModel.COMPANY_CODE);
        JsonFieldSpecification plantNameSpecification =
                getJsonContainsSpecification(status, ITObStoreTransactionGeneralModel.PLANT_NAME);
        JsonFieldSpecification storehouseNameSpecification =
                getJsonContainsSpecification(status, ITObStoreTransactionGeneralModel.STOREHOUSE_NAME);
        SimpleStringFieldSpecification purchaseUnitCodeSpecification =
                getSimpleStringSpecification(status, ITObStoreTransactionGeneralModel.PURCHASEUNIT_CODE);
        SimpleStringFieldSpecification stockTypeCodeSpecification =
                getSimpleStringSpecification(status, ITObStoreTransactionGeneralModel.STOCK_TYPE);

        SimpleStringFieldSpecification itemCodeSpecification =
                getCustomStringContainsSpecification(status, ITObStoreTransactionGeneralModel.ITEM, "Code");
        JsonFieldSpecification itemNameSpecification =
                getCustomJsonContainsSpecification(status, ITObStoreTransactionGeneralModel.ITEM, "Name");
        JsonFieldSpecification unitOfMeasureNameSpecification =
                getJsonContainsSpecification(status, ITObStoreTransactionGeneralModel.UOM_NAME);

        BigDecimalFieldSpecification quantitySpecification =
                getBigDecimalFieldSpecification(status, ITObStoreTransactionGeneralModel.QUANTITY);
        BigDecimalFieldSpecification estimateCostSpecification =
                getBigDecimalFieldSpecification(status, ITObStoreTransactionGeneralModel.ESTIMATE_COST);
        BigDecimalFieldSpecification actualCostSpecification =
                getBigDecimalFieldSpecification(status, ITObStoreTransactionGeneralModel.ACTUAL_COST);

        SimpleStringFieldSpecification refDocumentCodeSpecification =
                getCustomStringContainsSpecification(
                        status, ITObStoreTransactionGeneralModel.REF_DOCUMENT, "Code");
        JsonFieldSpecification refDocumentTypeSpecification =
                getCustomJsonContainsSpecification(
                        status, ITObStoreTransactionGeneralModel.REF_DOCUMENT, "Type");
        BigDecimalFieldSpecification remainingQuantitySpecification =
                getBigDecimalFieldSpecification(status, ITObStoreTransactionGeneralModel.REMAINING_QUANTITY);

        Specification querySpecification =
                Specification.where(userCodeSpecification)
                        .and(purchaseUnitConditionSpecification)
                        .and(transactionTypeSpecification)
                        .and(originalAddingDateContainsSpecification)
                        .and(companyCodeSpecification)
                        .and(plantNameSpecification)
                        .and(storehouseNameSpecification)
                        .and(purchaseUnitCodeSpecification)
                        .and(stockTypeCodeSpecification)
                        .and(itemCodeSpecification.or(itemNameSpecification))
                        .and(unitOfMeasureNameSpecification)
                        .and(quantitySpecification)
                        .and(estimateCostSpecification)
                        .and(actualCostSpecification)
                        .and(refDocumentCodeSpecification.or(refDocumentTypeSpecification))
                        .and(remainingQuantitySpecification);

        return querySpecification;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/readfilteredstoretransactions/{businessUnitCode}")
    public @ResponseBody
    String getFilteredStoreTransactions(@PathVariable String businessUnitCode) throws Exception {

        ReadStoreTransactionSpecification readStoreTransactionSpecification =
                new ReadStoreTransactionSpecification();

        checkIfReadPermissionIsAllowed(ITObStoreTransactionGeneralModel.SYS_NAME, READ_ALL);

        List<TObStoreTransactionGeneralModel> storeTransactions =
                storeTransactionGeneralModelRep.findAll(
                        Specification.where(readStoreTransactionSpecification.isAllowedStoreTransaction(businessUnitCode)),
                        Sort.by(Sort.Direction.DESC, ITObStoreTransactionGeneralModel.ST_CODE));

        return serializeRESTSuccessResponse(storeTransactions);
    }
}
