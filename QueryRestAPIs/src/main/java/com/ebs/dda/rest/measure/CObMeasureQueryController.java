package com.ebs.dda.rest.measure;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

// Created By Hossam Hassan @ 10/22/18
@RestController
@RequestMapping(value = "/services/measure")
public class CObMeasureQueryController extends BasicQueryController {
  @Autowired private CObUoMGeneralModelRep uoMGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/getall")
  public @ResponseBody String readAll() throws Exception {
    checkIfReadPermissionIsAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    List<CObUoMGeneralModel> uoMGeneralModels = uoMGeneralModelRep.findAll();

    return serializeRESTSuccessResponse(uoMGeneralModels);
  }
}
