package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dda.jpa.masterdata.collectionresponsible.CObCollectionResponsible;
import com.ebs.dda.repositories.masterdata.collectionresponsible.CObCollectionResponsibleRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/collectionresponsible")
public class CObCollectionResponsibleQueryController extends BasicQueryController{
   @Autowired private CObCollectionResponsibleRep collectionResponsibleRep;

    @RequestMapping(method = RequestMethod.GET, value = "/getall")
    public @ResponseBody  String readAll() {
        List<CObCollectionResponsible> collectionResponsibles = collectionResponsibleRep.findAll();
        return serializeRESTSuccessResponse(collectionResponsibles);
    }
}
