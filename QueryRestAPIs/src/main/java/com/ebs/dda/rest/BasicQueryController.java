package com.ebs.dda.rest;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dac.common.utils.json.adapters.BigDecimalAdapter;
import com.ebs.dac.common.utils.json.adapters.JodaTimeJsonAdapter;
import com.ebs.dac.common.utils.services.rest.BasicRESTResponse;
import com.ebs.dac.dbo.api.json.ExcludeAnnotationExclusionStrategy;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public abstract class BasicQueryController {

  protected final String WILD_CARD_CONDITION = "*";
  protected final String VENDOR = "vendor";
  protected final String NAME = "Name";
  protected final String CODE = "Code";
  protected final String ID_KEY = "id";
  private static final Logger log = LoggerFactory.getLogger(BasicQueryController.class);

  @Autowired public AuthorizationManager authorizationManager;

  public Gson getGson() {
    return initGsonBuilder().create();
  }

  protected String serializeRESTSuccessResponse(Object data) {

    Gson gson = getGson();
    BasicRESTResponse response = new BasicRESTResponse(Locale.ENGLISH);
    response.success(data);
    return gson.toJson(response);
  }

  protected String serializeRESTSuccessResponse(List<Object> data) {

    Gson gson = getGson();
    BasicRESTResponse response = new BasicRESTResponse(Locale.ENGLISH);
    response.success(data);
    return gson.toJson(response);
  }

  protected String serializeReadNextResponse(String nextCode) {
    JsonObject response = new JsonObject();
    response.addProperty("nextCode", nextCode);
    return response.toString();
  }

  protected String serializeReadPreviousResponse(String previousCode) {
    JsonObject response = new JsonObject();
    response.addProperty("previousCode", previousCode);
    return response.toString();
  }

  protected String serializeObjectScreenNavigationData(
      boolean nextCodeExists, boolean previousCodeExists) {
    JsonObject response = new JsonObject();
    response.addProperty("nextExists", nextCodeExists);
    response.addProperty("previousExists", previousCodeExists);
    return response.toString();
  }

  protected List<String> getConditionAttributeValue(String attribute, Set<String> conditions) {
    List<String> attributeValuesList = new ArrayList<>();

    for (String conditionValue : conditions) {
      if (!conditionValue.equals(WILD_CARD_CONDITION)) {
        int startIndex = conditionValue.indexOf(attribute);
        if (startIndex > -1) {
          int operationIndex = conditionValue.indexOf("]", startIndex);
          String attributeValue =
                  conditionValue.substring(startIndex + attribute.length() + 2, operationIndex - 1);
          attributeValuesList.add(attributeValue);
        }
      } else {
        attributeValuesList.add(WILD_CARD_CONDITION);
      }
    }

    return attributeValuesList;
  }

  protected void checkIfReadPermissionIsAllowed(String sysName, String readPermission)
      throws Exception {
    try {
      authorizationManager.authorizeAction(sysName, readPermission);
    } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      throw new ResourceAccessException();
    }
  }

  public String serializeViewAllResponse(Object data, long totalNumberOfRecords) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("status", "SUCCESS");
    jsonObject.add("data", getGson().toJsonTree(data));
    jsonObject.addProperty("totalNumberOfRecords", totalNumberOfRecords);
    return jsonObject.toString();
  }

  private GsonBuilder initGsonBuilder() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.addSerializationExclusionStrategy(new ExcludeAnnotationExclusionStrategy());
    gsonBuilder.setDateFormat(DateFormats.DATE_TIME_WITH_TIMEZONE);
    gsonBuilder.registerTypeAdapter(DateTime.class, new JodaTimeJsonAdapter());
    gsonBuilder.registerTypeAdapter(BigDecimal.class, new BigDecimalAdapter());
    gsonBuilder.enableComplexMapKeySerialization().setPrettyPrinting();

    return gsonBuilder;
  }

  protected DoubleFieldSpecification getDoubleFieldSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria criteriaFilter = status.getFilterCriteria(key);
      return new DoubleFieldSpecification(criteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new DoubleFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected BigDecimalFieldSpecification getBigDecimalFieldSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria criteriaFilter = status.getFilterCriteria(key);
      return new BigDecimalFieldSpecification(criteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new BigDecimalFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected SimpleStringFieldSpecification getSimpleStringSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria criteriaFilter = status.getFilterCriteria(key);
      return new SimpleStringFieldSpecification(criteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new SimpleStringFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected CurrentStatesFieldSpecification getCurrentStatesFieldSpecification(
      Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria criteriaFilter = status.getFilterCriteria(key);
      return new CurrentStatesFieldSpecification(criteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new CurrentStatesFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected DateContainsSpecification getDateContainsSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria criteriaFilter = status.getFilterCriteria(key);
      return new DateContainsSpecification(criteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new DateContainsSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected JsonFieldSpecification getJsonContainsSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria CriteriaFilter = status.getFilterCriteria(key);
      return new JsonFieldSpecification(CriteriaFilter, status.getCurrentLang());
    }
    Status emptyStatus = new Status("");
    return new JsonFieldSpecification(emptyStatus.getFilterCriteria(key), status.getCurrentLang());
  }

  protected JsonFieldSpecification getJsonEqualsSpecification(Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria CriteriaFilter = status.getFilterCriteria(key);
      return new JsonFieldSpecification(CriteriaFilter, status.getCurrentLang());
    }
    Status emptyStatus = new Status("");
    return new JsonFieldSpecification(emptyStatus.getFilterCriteria(key), status.getCurrentLang());
  }

  protected SimpleBooleanFieldSpecification getBooleanEqualsSpecification(
      Status status, String key) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria CriteriaFilter = status.getFilterCriteria(key);
      return new SimpleBooleanFieldSpecification(CriteriaFilter);
    }
    Status emptyStatus = new Status("");
    return new SimpleBooleanFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected JsonListContainsSpecification getCustomPurchaseUnitJsonListSpecification(
      String key, Status status) {
    if (status.getFilterCriteria(key) != null) {
      String value = status.getFilterCriteria(key).getValue();
      return new JsonListContainsSpecification(key, Arrays.asList(value));
    }
    return new JsonListContainsSpecification(key, new ArrayList<>());
  }

  protected SimpleStringFieldSpecification getCustomStringContainsSpecification(
      Status status, String key, String customKeyValue) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria filterCriteria = createCustomFilterCriteria(status, key, customKeyValue);
      return new SimpleStringFieldSpecification(filterCriteria);
    }
    Status emptyStatus = new Status("");
    return new SimpleStringFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected SimpleStringFieldSpecification getCustomStringContainsOperationSpecification(
          Status status, String key, String customKeyValue) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria filterCriteria = createCustomContainsFilterCriteria(status, key, customKeyValue);
      return new SimpleStringFieldSpecification(filterCriteria);
    }
    Status emptyStatus = new Status("");
    return new SimpleStringFieldSpecification(emptyStatus.getFilterCriteria(key));
  }
  protected DoubleFieldSpecification getCustomDoubleEqualsSpecification(
      Status status, String key, String customKeyValue) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria filterCriteria = createCustomFilterCriteria(status, key, customKeyValue);
      return new DoubleFieldSpecification(filterCriteria);
    }
    Status emptyStatus = new Status("");
    return new DoubleFieldSpecification(emptyStatus.getFilterCriteria(key));
  }

  protected SimpleStringFieldSpecification getSimpleStringContainsSpecification(
      String key, String customKeyValue) {
    FilterCriteria filterCriteria = new FilterCriteria(key, "Contains", customKeyValue);
    return new SimpleStringFieldSpecification(filterCriteria);
  }

  protected SimpleStringFieldSpecification getSimpleStringEqualsSpecification(
      String key, String customKeyValue) {
    FilterCriteria filterCriteria = new FilterCriteria(key, "Equals", customKeyValue);
    return new SimpleStringFieldSpecification(filterCriteria);
  }

  protected JsonFieldSpecification getCustomJsonContainsSpecification(
      Status status, String key, String customKeyValue) {
    if (status.getFilterCriteria(key) != null) {
      FilterCriteria filterCriteria = createCustomFilterCriteria(status, key, customKeyValue);
      return new JsonFieldSpecification(filterCriteria, status.getCurrentLang());
    }
    Status emptyStatus = new Status("");
    return new JsonFieldSpecification(emptyStatus.getFilterCriteria(key), status.getCurrentLang());
  }

  public Status constructStatus(
      int pageNum, int rowsNum, String filter, HttpServletRequest request) {
    String decodedFilter = decodeStatus(filter);
    Status status = new Status(decodedFilter);
    status.setCurrentLang(request.getHeader("Accept-Language"));
    status.setPageNumber(pageNum);
    status.setPageSize(rowsNum);
    return status;
  }

  private String decodeStatus(String filter) {
    String decodedFilter = null;
    try {
      decodedFilter = URLDecoder.decode(filter, StandardCharsets.UTF_8.name());
    } catch (UnsupportedEncodingException e) {
      log.error("Failed to decode filter");
    }
    return decodedFilter;
  }

  private FilterCriteria createCustomFilterCriteria(
      Status status, String key, String customKeyValue) {
    String originalKey = status.getFilterCriteria(key).getKey() + customKeyValue;
    String operation = status.getFilterCriteria(key).getOperation();
    String value = status.getFilterCriteria(key).getValue();
    return new FilterCriteria(originalKey, operation, value);
  }

  private FilterCriteria createCustomContainsFilterCriteria(
      Status status, String key, String customKeyValue) {
    String originalKey = status.getFilterCriteria(key).getKey() + customKeyValue;
    String operation = "Contains";
    String value = status.getFilterCriteria(key).getValue();
    return new FilterCriteria(originalKey, operation, value);
  }

  public boolean isReadPermissionAllowed(String sysName, String readPermission) {
    boolean isReadPermissionAllowed = false;
    try {
      authorizationManager.authorizeAction(sysName, readPermission);
      isReadPermissionAllowed = true;
    } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      return isReadPermissionAllowed;
    }

    return isReadPermissionAllowed;
  }

  public void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }

  public Set<String> getAuthorizedReads(AuthorizedPermissionsConfig authorizedPermissionsConfig) {
    List<AuthorizedPermissionsConfig.AuthorizedPermissionConfig> permissionsConfig =
            authorizedPermissionsConfig.getPermissionsConfig();
    List<String> authorizedReads = new ArrayList<>();
    for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config : permissionsConfig) {
      try {
        authorizationManager.authorizeAction(config.getEntitySysName(),
                config.getPermissionToCheck());
        authorizedReads.add(config.getPermissionToReturn());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      }
    }
    return new HashSet<>(authorizedReads);
  }

}
