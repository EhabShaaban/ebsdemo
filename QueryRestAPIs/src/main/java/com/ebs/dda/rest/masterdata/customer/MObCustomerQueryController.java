package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.customer.*;
import com.ebs.dda.jpa.masterdata.lookups.LObIssueFrom;
import com.ebs.dda.jpa.masterdata.lookups.LObTaxAdministrative;
import com.ebs.dda.masterdata.customer.ICustomerActionNames;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.CurrentStatesFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonListContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/masterdataobjects/customers")
public class MObCustomerQueryController extends BasicQueryController implements InitializingBean {
  @Autowired private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  @Autowired private IObCustomerAddressGeneralModelRep customerAddressGeneralModelRep;
  @Autowired private MObCustomerGeneralModelRep customerGeneralModelRep;
  @Autowired private LObIssueFromRep issueFromRep;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private IObCustomerContactPersonGeneralModelRep customerContactPersonGeneralModelRep;
  @Autowired private LObTaxAdministrativeRep taxAdministrativeRep;

  @Autowired
  @Qualifier("customerAllowedActionService")
  private AllowedActionService customerAllowedActionService;

  private MObCustomerQueryControllerUtils utils;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IMObCustomer.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IMObCustomerGeneralModel.USER_CODE));

    Specification<MObCustomerGeneralModel> querySpecification =
        getMObCustomerSpecification(conditions, status);

    Page<MObCustomerGeneralModel> customersDataPage =
        customerGeneralModelRep.findAll(querySpecification, pageRequest);
    List<MObCustomerGeneralModel> customersData = customersDataPage.getContent();

    return serializeViewAllResponse(customersData, customersDataPage.getTotalElements());
  }

  private Specification<MObCustomerGeneralModel> getMObCustomerSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(IMObCustomerGeneralModel.PURCHASE_UNIT_NAME, conditions);
    JsonListContainsSpecification customerPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(
            IMObCustomerGeneralModel.BUSINESS_UNIT_NAMES, businessUnitNames);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IMObCustomerGeneralModel.USER_CODE);

    JsonFieldSpecification marketNameSpecification =
        getJsonContainsSpecification(status, IMObCustomerGeneralModel.MARKET_NAME);

    JsonFieldSpecification legalNameSpecification =
        getJsonContainsSpecification(status, IMObCustomerGeneralModel.LEGAL_NAME);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringSpecification(status, IMObCustomerGeneralModel.TYPE);

    JsonListContainsSpecification businessUnitNameSpecification =
        getCustomPurchaseUnitJsonListSpecification(
            IMObCustomerGeneralModel.BUSINESS_UNIT_NAMES, status);

    SimpleStringFieldSpecification oldCustomerNumberSpecification =
            getSimpleStringSpecification(status, IMObCustomerGeneralModel.OLD_CUSTOMER_NUMBER);

    CurrentStatesFieldSpecification stateSpecification =
        getCurrentStatesFieldSpecification(status, IMObCustomerGeneralModel.CURRENT_STATES);

    Specification<MObCustomerGeneralModel> querySpecification =
        Specification.where(customerPurchaseUnitsAuthorizationSpecification)
            .and(userCodeSpecification)
            .and(typeSpecification)
            .and(marketNameSpecification)
            .and(legalNameSpecification)
            .and(businessUnitNameSpecification)
            .and(oldCustomerNumberSpecification)
            .and(stateSpecification);
    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String getCustomerTypes() {
    authorizationManager.authorizeAction(IMObCustomer.SYS_NAME, IActionsNames.READ_ALL);
    List<CustomerTypeEnum> customerTypes = Arrays.asList(CustomerTypeEnum.values());
    return serializeRESTSuccessResponse(customerTypes);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/issuedfrom")
  public @ResponseBody String getCustomerIssuedFrom() {
    authorizationManager.authorizeAction(
        IMObCustomer.SYS_NAME, ICustomerActionNames.READ_ISSUE_FROM);

    List<LObIssueFrom> issuedFromList = issueFromRep.findAllByOrderByUserCodeDesc();
    return serializeRESTSuccessResponse(issuedFromList);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/taxadministration")
  public @ResponseBody String getCustomerTaxAdministration() {
    authorizationManager.authorizeAction(
        IMObCustomer.SYS_NAME, ICustomerActionNames.READ_TAX_ADMINISTRATION);
    List<LObTaxAdministrative> taxAdministrations =
        taxAdministrativeRep.findAllByOrderByUserCodeDesc();
    return serializeRESTSuccessResponse(taxAdministrations);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/bybusinessunitcode/{businessUnitCode}")
  public @ResponseBody String readCustomersByBusinessUnit(@PathVariable String businessUnitCode) {
    List<IObCustomerBusinessUnitGeneralModel> customers =
        customerBusinessUnitGeneralModelRep.findAllByBusinessUnitcodeOrderByUserCodeDesc(
            businessUnitCode);
    return serializeRESTSuccessResponse(customers);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{customerCode}/addresses")
  public @ResponseBody String getCustomerAddresses(@PathVariable String customerCode) {

    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);

    utils.authorizeActionOnCustomer(customerBusinessUnitsList, IActionsNames.READ_ADDRESS);

    List<IObCustomerAddressGeneralModel> addresses =
        customerAddressGeneralModelRep.findAllByUserCodeOrderByIdDesc(customerCode);

    return serializeRESTSuccessResponse(addresses);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{customerCode}/contactpersons")
  public @ResponseBody String getCustomerContactPersons(@PathVariable String customerCode) {

    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);

    utils.authorizeActionOnCustomer(customerBusinessUnitsList, IActionsNames.READ_CONTACT_PERSON);

    List<IObCustomerContactPersonGeneralModel> contactPersons =
        customerContactPersonGeneralModelRep.findAllByUserCodeOrderByIdAsc(customerCode);

    return serializeRESTSuccessResponse(contactPersons);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/")
  public @ResponseBody String getAllowedActionsHomeScreen() throws Exception {
    Set<String> allowedActions =
        customerAllowedActionService.getHomeScreenAllowedActions(MObCustomer.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{customerCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String customerCode)
      throws Exception {
    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnits =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);
    if (customerBusinessUnits.size() == 0) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions = new HashSet<>();
    for (IObCustomerBusinessUnitGeneralModel customerBusinessUnit : customerBusinessUnits) {
      allowedActions.addAll(
          customerAllowedActionService.getObjectScreenAllowedActions(customerBusinessUnit));
    }
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    utils = new MObCustomerQueryControllerUtils(authorizationManager);
  }
}
