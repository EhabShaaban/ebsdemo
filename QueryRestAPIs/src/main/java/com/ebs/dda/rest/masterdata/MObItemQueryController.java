package com.ebs.dda.rest.masterdata;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.item.*;
import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonListContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleBooleanFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.masterdata.utils.MObItemQueryControllerUtils;
import com.ebs.dda.rest.masterdata.utils.MasterDataQueryControllerUtils;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/masterdataobjects/items")
public class MObItemQueryController extends BasicQueryController implements InitializingBean {

  @Autowired private MObItemGeneralModelRep itemsRep;
  @Autowired private IObItemAccountingInfoGeneralModelRep accountingInfoRep;

  @Autowired private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;

  @Autowired
  @Qualifier("itemAllowedActionService")
  private AllowedActionService allowedActionService;

  @Autowired private IObAlternativeUoMGeneralModelRep alternativeUoMsGeneralModelRep;

  @Autowired private CObUoMGeneralModelRep uoMGeneralModelRep;

  private MasterDataQueryControllerUtils masterDataQueryControllerUtils;
  private MObItemQueryControllerUtils itemQueryControllerUtils;

  @Override
  public void afterPropertiesSet() {
    masterDataQueryControllerUtils = new MasterDataQueryControllerUtils(authorizationManager);
    masterDataQueryControllerUtils.setItemsRep(itemsRep);
    itemQueryControllerUtils = new MObItemQueryControllerUtils(authorizationManager);
    itemQueryControllerUtils.setItemAuthorizationGeneralModelRep(itemAuthorizationGeneralModelRep);
    itemQueryControllerUtils.setUoMGeneralModelRep(uoMGeneralModelRep);
    itemQueryControllerUtils.setItemsRep(itemsRep);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unitofmeasure/{itemCode}")
  public @ResponseBody String readItemAlternativeUOMs(@PathVariable String itemCode)
      throws Exception {

    masterDataQueryControllerUtils.checkIfItemExists(itemCode);
    try {
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
          itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
      String actionName = IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION;
      masterDataQueryControllerUtils.applyAuthorizationOnItemByAction(
          itemPurchaseUnits, actionName);
      List<IObAlternativeUoMGeneralModel> unitOfMeasuresList =
          alternativeUoMsGeneralModelRep.findAllByItemCodeOrderByIdDesc(itemCode);
      return serializeRESTSuccessResponse(unitOfMeasuresList);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException();
      authorizationException.initCause(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/alternativeunitofmeasure/{itemCode}/{unitOfMeasureCode}")
  public @ResponseBody String readItemBaseUnitAndConversionFactor(
      @PathVariable String itemCode, @PathVariable String unitOfMeasureCode) throws Exception {

    itemQueryControllerUtils.checkPermissionsForReadingItemBaseUnitAndConversionFactor();

    try {
      itemQueryControllerUtils.checkIfItemExists(itemCode);
      itemQueryControllerUtils.checkIfUnitOfMeasureExists(unitOfMeasureCode);
    } catch (Exception e) {
      throw new ArgumentViolationSecurityException();
    }
    IObAlternativeUoMGeneralModel alternativeUoMGeneralModel =
        alternativeUoMsGeneralModelRep.findByItemCodeAndAlternativeUnitOfMeasureCode(
            itemCode, unitOfMeasureCode);

    if (alternativeUoMGeneralModel != null) {
      JsonObject baseUnitAndConversion =
          createConversionAndBaseUnitJsonObject(alternativeUoMGeneralModel);
      return serializeRESTSuccessResponse(baseUnitAndConversion);
    }

    MObItemGeneralModel itemGeneralModel =
        itemsRep.findByUserCodeAndBasicUnitOfMeasureCode(itemCode, unitOfMeasureCode);

    JsonObject baseUnitAndConversion = createConversionAndBaseUnitJsonObject(itemGeneralModel);
    return serializeRESTSuccessResponse(baseUnitAndConversion);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/baseandalternativeunitofmeasures/{itemCode}")
  public @ResponseBody String readItemUOMs(@PathVariable String itemCode) throws Exception {

    masterDataQueryControllerUtils.checkIfItemExists(itemCode);
    try {
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
          itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
      String actionName = IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION;
      masterDataQueryControllerUtils.applyAuthorizationOnItemByAction(
          itemPurchaseUnits, actionName);
      JsonArray unitOfMeasures = addItemUnitOfMeasuresToJsonObject(itemCode);

      return serializeRESTSuccessResponse(unitOfMeasures);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filters}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filters,
      HttpServletRequest request) {
    String readAllPermission = IActionsNames.READ_ALL;
    authorizationManager.authorizeAction(MObItem.SYS_NAME, readAllPermission);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    Status status = constructStatus(pageNum, rowsNum, filters, request);
    PageRequest pageRequest =
        PageRequest.of(pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IMObItemGeneralModel.CODE));
    Page<MObItemGeneralModel> itemsPage = readItemsData(status, pageRequest, conditions);
    List<MObItemGeneralModel> items = itemsPage.getContent();
    return serializeViewAllResponse(items, itemsPage.getTotalElements());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/readauthorizeditems")
  public @ResponseBody String readAuthorizedItems() {
    try {
      String readAllPermission = IActionsNames.READ_ALL;
      authorizationManager.authorizeAction(MObItem.SYS_NAME, readAllPermission);
      Set<String> conditions = authorizationManager.getPermissionConditions();
      List<MObItemGeneralModel> items = readAuthorizedItems(conditions);
      return serializeRESTSuccessResponse(items);
    } catch (AuthorizationException | ConditionalAuthorizationException e) {
      if (e.equals(new ConditionalAuthorizationException(""))) {
        throw new AuthorizationException();
      }
      throw new AuthorizationException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}")
  public @ResponseBody String readAuthorizedItemsByBusinessUnit(
      @PathVariable String businessUnitCode) {
    try {
      authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.READ_ALL);
      Set<String> conditions = authorizationManager.getPermissionConditions();
      List<MObItemGeneralModel> items =
          readAuthorizedItemsByBusinessUnit(conditions, businessUnitCode);
      return serializeRESTSuccessResponse(items);
    } catch (Exception e) {
      throw new AuthorizationException(e);
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/type/{type}")
  public @ResponseBody String readItemsByType(@PathVariable String type) {
    try {
      authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.READ_ALL);
      Set<String> conditions = authorizationManager.getPermissionConditions();
      List<MObItemGeneralModel> items = readAuthorizedItemsByType(conditions, type);
      return serializeRESTSuccessResponse(items);
    } catch (AuthorizationException | ConditionalAuthorizationException e) {
      if (e.equals(new ConditionalAuthorizationException(""))) {
        throw new AuthorizationException();
      }
      throw new AuthorizationException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{itemCode}")
  public @ResponseBody String readActions(@PathVariable String itemCode) throws Exception {

    List<MObItemAuthorizationGeneralModel> items =
        itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
    if (items.size() == 0) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions = new HashSet<>();
    for (MObItemAuthorizationGeneralModel item : items) {
      allowedActions.addAll(allowedActionService.getObjectScreenAllowedActions(item));
    }
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    try {
      authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.CREATE);
      if (allowedActions.contains("UpdateUoMData")) allowedActions.add("CreateMeasure");
    } catch (AuthorizationException e) {
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(MObItemAuthorizationGeneralModel.class);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/generaldata/{itemCode}")
  public @ResponseBody String readMObItemGeneralData(@PathVariable String itemCode)
      throws Exception {

    itemQueryControllerUtils.authorizeUserOnItem(
        itemCode, IMObItemActionNames.READ_ITEM_GENERAL_DATA);
    MObItemGeneralModel itemGeneralModel = itemsRep.findByUserCode(itemCode);

    return itemQueryControllerUtils.serializeItemGeneralDataSuccessResponse(
        getGson().toJsonTree(itemGeneralModel));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/accountinginfo/{itemCode}")
  public @ResponseBody String readMObItemAccoutningInfo(@PathVariable String itemCode)
      throws Exception {
    itemQueryControllerUtils.authorizeUserOnItem(itemCode, IActionsNames.READ_ACCOUNTING_DETAILS);

    IObItemAccountingInfoGeneralModel accountingInfoGeneralModel =
        accountingInfoRep.findByItemCode(itemCode);
    return itemQueryControllerUtils.serializeItemGeneralDataSuccessResponse(
        getGson().toJsonTree(accountingInfoGeneralModel));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/navigationdata/{itemCode}")
  public @ResponseBody String getItemNavigationData(@PathVariable String itemCode)
      throws Exception {
    itemQueryControllerUtils.authorizeItemNavigationPermission(itemCode);
    boolean nextCodeExists = false, previousCodeExists = false;
    try {
      getNextItemCode(itemCode);
      nextCodeExists = true;
    } catch (DataNavigationException e) {
    }

    try {
      getPreviousItemCode(itemCode);
      previousCodeExists = true;
    } catch (DataNavigationException e) {
    }

    return serializeObjectScreenNavigationData(nextCodeExists, previousCodeExists);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/next/{itemCode}")
  public @ResponseBody String readNextCode(@PathVariable String itemCode) throws Exception {
    itemQueryControllerUtils.authorizeItemNavigationPermission(itemCode);
    String nextItemCode = getNextItemCode(itemCode);
    return serializeReadNextResponse(nextItemCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/previous/{itemCode}")
  public @ResponseBody String readPreviousCode(@PathVariable String itemCode) throws Exception {
    itemQueryControllerUtils.authorizeItemNavigationPermission(itemCode);
    String previousItemCode = getPreviousItemCode(itemCode);
    return serializeReadPreviousResponse(previousItemCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/getitemuom/{itemCode}")
  public @ResponseBody String readItemUnitOfMeasure(@PathVariable String itemCode)
      throws Exception {
    checkIfReadPermissionIsAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    List<CObUoMGeneralModel> uoMGeneralModels = uoMGeneralModelRep.findAllByOrderByUserCodeDesc();

    MObItemGeneralModel itemGeneralModel = itemsRep.findByUserCode(itemCode);

    int baseUnitIndex = -1;

    for (CObUoMGeneralModel uoMGeneralModel : uoMGeneralModels) {
      if (uoMGeneralModel.getUserCode().equals(itemGeneralModel.getBasicUnitOfMeasureCode())) {
        baseUnitIndex = uoMGeneralModels.indexOf(uoMGeneralModel);
        break;
      }
    }
    uoMGeneralModels.remove(baseUnitIndex);
    return serializeRESTSuccessResponse(uoMGeneralModels);
  }

  private String getNextItemCode(String currentItemCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    MObItemGeneralModel nextItem = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      nextItem =
          itemsRep.findNextByUserCode(
              StringUtils.join(purchaseUnitNames, ','), Long.valueOf(currentItemCode));
    } else {
      nextItem = itemsRep.findNextByUserCodeUnconditionalRead(Long.valueOf(currentItemCode));
    }
    return itemQueryControllerUtils.getTargetItemCode(nextItem);
  }

  private String getPreviousItemCode(String currentItemCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    MObItemGeneralModel prevItem = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      prevItem =
          itemsRep.findPreviousByUserCode(
              StringUtils.join(purchaseUnitNames, ','), Long.valueOf(currentItemCode));
    } else {
      prevItem = itemsRep.findPreviousByUserCodeUnconditionalRead(Long.valueOf(currentItemCode));
    }
    return itemQueryControllerUtils.getTargetItemCode(prevItem);
  }

  private List<String> getPurchaseUnitNameByAuthorizationCondition(Set<String> conditions) {
    return getConditionAttributeValue(ICObPurchasingUnit.PURCHASING_UNIT_NAME, conditions);
  }

  private JsonObject createConversionAndBaseUnitJsonObject(Object itemData) {

    JsonObject baseUnitAndConversion = new JsonObject();
    if (itemData instanceof IObAlternativeUoMGeneralModel) {
      baseUnitAndConversion.addProperty(
          "conversionFactor", ((IObAlternativeUoMGeneralModel) itemData).getConversionFactor());
      baseUnitAndConversion.addProperty(
          "basicUnitOfMeasureCode",
          ((IObAlternativeUoMGeneralModel) itemData).getBasicUnitOfMeasureCode());
      baseUnitAndConversion.add(
          "basicUnitOfMeasureSymbol",
          getUnitOfMeasureSymbol(
              ((IObAlternativeUoMGeneralModel) itemData).getBasicUnitOfMeasureSymbol()));
    } else if (itemData instanceof MObItemGeneralModel) {
      baseUnitAndConversion.addProperty("conversionFactor", 1f);
      baseUnitAndConversion.addProperty(
          "basicUnitOfMeasureCode", ((MObItemGeneralModel) itemData).getBasicUnitOfMeasureCode());
      baseUnitAndConversion.add(
          "basicUnitOfMeasureSymbol",
          getUnitOfMeasureSymbol(((MObItemGeneralModel) itemData).getBasicUnitOfMeasureSymbol()));
    }
    return baseUnitAndConversion;
  }

  private void addBasicUnitOfMeasureToJsonObject(
      MObItemGeneralModel item, JsonObject basicUOMJsonObject) {
    basicUOMJsonObject.addProperty("userCode", item.getBasicUnitOfMeasureCode());
    basicUOMJsonObject.add("symbol", getUnitOfMeasureSymbol(item.getBasicUnitOfMeasureSymbol()));
  }

  private void addAlternativeUnitOfMeasureToJsonObject(
      IObAlternativeUoMGeneralModel alternativeUoMGeneralModel,
      JsonObject alternativeUOMJsonObject) {
    alternativeUOMJsonObject.addProperty(
        "userCode", alternativeUoMGeneralModel.getAlternativeUnitOfMeasureCode());
    alternativeUOMJsonObject.add(
        "symbol",
        getUnitOfMeasureSymbol(alternativeUoMGeneralModel.getAlternativeUnitOfMeasureSymbol()));
  }

  private JsonElement getUnitOfMeasureSymbol(LocalizedString basicUnitOfMeasureSymbol) {
    return getGson().toJsonTree(basicUnitOfMeasureSymbol);
  }

  private Page<MObItemGeneralModel> readItemsData(
      Status status, Pageable pageable, Set<String> conditions) {

    JsonListContainsSpecification itemPurchaseUnitsAuthorizationSpecification =
        getItemPurchaseUnitAuthorizationSpecification(conditions);

    JsonListContainsSpecification itemPurchaseUnitsSpecification =
        getCustomPurchaseUnitJsonListSpecification(
            IMObItemGeneralModel.PURCHASING_UNIT_NAMES, status);

    SimpleStringFieldSpecification codeSpecification =
        getSimpleStringSpecification(status, IMObItemGeneralModel.CODE);

    JsonFieldSpecification itemNameSpecification =
        getJsonContainsSpecification(status, IMObItemGeneralModel.ITEM_NAME);

    JsonFieldSpecification itemGroupNameSpecification =
        getJsonContainsSpecification(status, IMObItemGeneralModel.ITEM_GROUP_NAME);

    JsonFieldSpecification basicUnitOfMeasureNameSpecification =
        getJsonContainsSpecification(status, IMObItemGeneralModel.BASIC_UOM_NAME);

    SimpleBooleanFieldSpecification isBatchManagedSpecification =
        getBooleanEqualsSpecification(status, IMObItemGeneralModel.IS_BATCH_MANAGED);

    JsonFieldSpecification itemTypeSpecification =
        getJsonEqualsSpecification(status, IMObItemGeneralModel.ITEM_TYPE_NAME);

    return itemsRep.findAll(
        Specification.where(codeSpecification)
            .and(itemNameSpecification)
            .and(itemGroupNameSpecification)
            .and(basicUnitOfMeasureNameSpecification)
            .and(isBatchManagedSpecification)
            .and(itemPurchaseUnitsSpecification)
            .and(itemPurchaseUnitsAuthorizationSpecification)
            .and(itemTypeSpecification),
        pageable);
  }

  private JsonArray addItemUnitOfMeasuresToJsonObject(String itemCode) {
    List<IObAlternativeUoMGeneralModel> unitOfMeasuresList =
        alternativeUoMsGeneralModelRep.findAllByItemCodeOrderByIdDesc(itemCode);
    MObItemGeneralModel item = itemsRep.findByUserCode(itemCode);

    JsonArray unitOfMeasures = new JsonArray();
    JsonObject basicUnitOfMeasure = new JsonObject();
    addBasicUnitOfMeasureToJsonObject(item, basicUnitOfMeasure);
    unitOfMeasures.add(basicUnitOfMeasure);
    for (IObAlternativeUoMGeneralModel alternativeUoMGeneralModel : unitOfMeasuresList) {
      JsonObject alternativeUnitOfMeasure = new JsonObject();
      addAlternativeUnitOfMeasureToJsonObject(alternativeUoMGeneralModel, alternativeUnitOfMeasure);
      unitOfMeasures.add(alternativeUnitOfMeasure);
    }
    return unitOfMeasures;
  }

  private JsonListContainsSpecification getItemPurchaseUnitAuthorizationSpecification(
      Set<String> conditions) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IMObItemGeneralModel.PURCHASING_UNIT_NAME, conditions);

    JsonListContainsSpecification itemPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(
            IMObItemGeneralModel.PURCHASING_UNIT_NAMES, purchaseUnitNames);
    return itemPurchaseUnitsAuthorizationSpecification;
  }

  private List<MObItemGeneralModel> readAuthorizedItems(Set<String> conditions) {

    JsonListContainsSpecification itemPurchaseUnitsAuthorizationSpecification =
        getItemPurchaseUnitAuthorizationSpecification(conditions);

    PageRequest pageRequest =
        PageRequest.of(
            0, Integer.MAX_VALUE, Sort.by(Sort.Direction.DESC, IMObItemGeneralModel.CODE));

    return itemsRep
        .findAll(Specification.where(itemPurchaseUnitsAuthorizationSpecification), pageRequest)
        .getContent();
  }

  private List<MObItemGeneralModel> readAuthorizedItemsByBusinessUnit(
      Set<String> conditions, String businessUnitCode) {

    JsonListContainsSpecification authorizationSpecification =
        getItemPurchaseUnitAuthorizationSpecification(conditions);

    SimpleStringFieldSpecification businessUnitSpecification =
        getSimpleStringContainsSpecification(
            IMObItemGeneralModel.PURCHASE_UNIT_CODES, businessUnitCode);

    PageRequest pageRequest =
        PageRequest.of(
            0, Integer.MAX_VALUE, Sort.by(Sort.Direction.DESC, IMObItemGeneralModel.CODE));

    return itemsRep
        .findAll(
            Specification.where(authorizationSpecification).and(businessUnitSpecification),
            pageRequest)
        .getContent();
  }

  private List<MObItemGeneralModel> readAuthorizedItemsByType(Set<String> conditions, String type) {

    JsonListContainsSpecification authorizationSpecification =
        getItemPurchaseUnitAuthorizationSpecification(conditions);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringEqualsSpecification(IMObItemGeneralModel.ITEM_TYPE_CODE, type);

    PageRequest pageRequest =
        PageRequest.of(
            0, Integer.MAX_VALUE, Sort.by(Sort.Direction.DESC, IMObItemGeneralModel.CODE));

    return itemsRep
        .findAll(
            Specification.where(authorizationSpecification).and(typeSpecification), pageRequest)
        .getContent();
  }
}
