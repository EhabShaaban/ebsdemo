package com.ebs.dda.rest.order.purchases;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemUOMGeneralModel;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderTaxGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemUOMGeneralModelRep;
import com.ebs.dda.rest.masterdata.utils.MasterDataQueryControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DObPurchaseOrderQueryControllerUtils {

  private MObItemUOMGeneralModelRep itemUOMGeneralModelRep;
  private IObOrderItemGeneralModelRep orderItemGeneralModelRep;

  private MasterDataQueryControllerUtils masterDataQueryControllerUtils;

  private AllowedActionService purchaseOrderAllowedActionService;
  private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;
  private AuthorizationManager authorizationManager;

  public DObPurchaseOrderQueryControllerUtils(AuthorizationManager authorizationManager) {
    masterDataQueryControllerUtils = new MasterDataQueryControllerUtils(authorizationManager);
    this.authorizationManager = authorizationManager;
  }

  public static void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }

  public static JsonObject constructItemSectionResponse(
      List<IObOrderItemGeneralModel> orderItems,
      List<IObOrderTaxGeneralModel> taxes,
      IObOrderItemSummaryGeneralModel itemSummary,
      Gson gson) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("Items", gson.toJson(orderItems));
    jsonObject.addProperty("Taxes", gson.toJson(taxes));
    jsonObject.addProperty("ItemSummary", gson.toJson(itemSummary));
    return jsonObject;
  }

  public List<JsonObject> getServicePurchaseOrderItemUOMsJsonObjects(
      String purchaseOrderCode, String itemCode, Gson gson) {

    List<MObItemUOMGeneralModel> itemUOMGeneralModels =
        itemUOMGeneralModelRep.findAllByItemCodeOrderByUserCodeDesc(itemCode);

    List<IObOrderItemGeneralModel> orderItemGeneralModels =
        orderItemGeneralModelRep.findAllByOrderCodeAndItemCode(purchaseOrderCode, itemCode);

    removeOrderUnitFromDropdownThatAlreadyExistsInOrder(
        itemUOMGeneralModels, orderItemGeneralModels);

    List<JsonObject> jsonObjects = constructOrderUOMsJsonObjects(itemUOMGeneralModels, gson);
    return jsonObjects;
  }

  private void removeOrderUnitFromDropdownThatAlreadyExistsInOrder(
      List<MObItemUOMGeneralModel> itemUOMGeneralModels,
      List<IObOrderItemGeneralModel> orderItemGeneralModels) {
    for (IObOrderItemGeneralModel orderItem : orderItemGeneralModels) {
      itemUOMGeneralModels.removeIf(
          itemUOM -> itemUOM.getUserCode().equals(orderItem.getOrderUnitCode()));
    }
  }

  private List<JsonObject> constructOrderUOMsJsonObjects(
      List<MObItemUOMGeneralModel> itemUOMGeneralModels, Gson gson) {
    List<JsonObject> itemUOMDetailsList = new ArrayList<>();
    for (MObItemUOMGeneralModel itemUOMGeneralModel : itemUOMGeneralModels) {
      JsonObject orderItemUOMData = new JsonObject();
      orderItemUOMData.addProperty("UnitOfMeasure", gson.toJson(itemUOMGeneralModel));
      itemUOMDetailsList.add(orderItemUOMData);
    }
    return itemUOMDetailsList;
  }

  public void checkIfActionAllowedInCurrentState(DObPurchaseOrderGeneralModel orderGeneralModel)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(orderGeneralModel);
    purchaseOrderAllowedActionService.setStateMachine(purchaseOrderStateMachine);

    Set<String> allowedActions =
        purchaseOrderAllowedActionService.getObjectScreenAllowedActions(orderGeneralModel);
    if (!allowedActions.contains(IPurchaseOrderActionNames.UPDATE_ITEM)) {
      throw new AuthorizationException();
    }
  }

  public void applyAuthorizationOnItemBusinessUnits(String itemCode) throws Exception {
    List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
        itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
    try {
      if (itemPurchaseUnits.size() != 0) {
        masterDataQueryControllerUtils.applyAuthorizationOnItemByAction(
            itemPurchaseUnits, IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION);
      }
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void checkIfUserIsAuthorizedOnAction(DObPurchaseOrderGeneralModel orderGeneralModel)
      throws Exception {
    DObPurchaseOrderQueryControllerUtils.checkIfEntityIsNull(orderGeneralModel);
    try {
      authorizationManager.authorizeAction(
          orderGeneralModel, IPurchaseOrderActionNames.UPDATE_ITEM);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void setItemUOMGeneralModelRep(MObItemUOMGeneralModelRep itemUOMGeneralModelRep) {
    this.itemUOMGeneralModelRep = itemUOMGeneralModelRep;
  }

  public void setOrderItemGeneralModelRep(IObOrderItemGeneralModelRep orderItemGeneralModelRep) {
    this.orderItemGeneralModelRep = orderItemGeneralModelRep;
  }

  public void setPurchaseOrderAllowedActionService(
      AllowedActionService purchaseOrderAllowedActionService) {
    this.purchaseOrderAllowedActionService = purchaseOrderAllowedActionService;
  }

  public void setItemAuthorizationGeneralModelRep(
      MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep) {
    this.itemAuthorizationGeneralModelRep = itemAuthorizationGeneralModelRep;
  }
}
