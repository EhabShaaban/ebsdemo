package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dda.jpa.masterdata.salesresponsible.CObSalesResponsible;
import com.ebs.dda.repositories.masterdata.salesresponsible.CObSalesResponsibleRep;
import com.ebs.dda.rest.BasicQueryController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/services/enterprise/salesresponsibles")
public class CObSalesResponsibleQueryController extends BasicQueryController {

  @Autowired private CObSalesResponsibleRep salesResponsibleRep;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String readAll() {
    List<CObSalesResponsible> salesReponsibles = salesResponsibleRep.findAllByOrderByIdDesc();
    return serializeRESTSuccessResponse(salesReponsibles);
  }
}
