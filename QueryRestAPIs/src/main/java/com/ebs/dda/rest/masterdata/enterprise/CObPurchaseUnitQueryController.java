package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizedPurchaseUnitService;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/purchaseunits")
public class CObPurchaseUnitQueryController extends BasicQueryController {

    @Autowired
    private AuthorizedPurchaseUnitService authorizedPurchaseUnitsService;

    @RequestMapping(method = RequestMethod.GET, value = "/readforpurchaseorder")
    public @ResponseBody
    String readForPurchaseOrder() throws Exception {
        String readAllPermission = IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER;
        return getAllowedPurchaseUnitsBasedOnUserPermissionResponse(readAllPermission);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/readall")
    public @ResponseBody
    String readAllPurchaseUnits() throws Exception {
        String readAllPermission = IActionsNames.READ_ALL;
        return getAllowedPurchaseUnitsBasedOnUserPermissionResponse(readAllPermission);
    }

    private String getAllowedPurchaseUnitsBasedOnUserPermissionResponse(String readAllPermission)
            throws Exception {
        List<CObPurchasingUnitGeneralModel> allowedPurchaseUnitsBasedOnUserPermission =
                authorizedPurchaseUnitsService.getAllowedPurchaseUnitsBasedOnUserPermission(readAllPermission);
        return serializeRESTSuccessResponse(allowedPurchaseUnitsBasedOnUserPermission);
    }

}
