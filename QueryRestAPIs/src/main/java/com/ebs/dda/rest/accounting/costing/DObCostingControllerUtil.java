package com.ebs.dda.rest.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;

import java.math.BigDecimal;
import java.util.List;

public class DObCostingControllerUtil {
  protected List<IObCostingItemGeneralModel> setItemTotalCost(
      List<IObCostingItemGeneralModel> costingItemsGeneralModel) {
    for (IObCostingItemGeneralModel costingItemGeneralModel : costingItemsGeneralModel) {
      BigDecimal estimateUnitLandedCost = costingItemGeneralModel.getEstimateUnitLandedCost();
      BigDecimal actualUnitLandedCost = costingItemGeneralModel.getActualUnitLandedCost();
      if (estimateUnitLandedCost != null) {
        setEstimateItemTotalCost(costingItemGeneralModel, estimateUnitLandedCost);
      }
      if (actualUnitLandedCost != null) {
        setActualItemTotalCost(costingItemGeneralModel, actualUnitLandedCost);
      }
    }
    return costingItemsGeneralModel;
  }

  private void setEstimateItemTotalCost(
      IObCostingItemGeneralModel costingItemGeneralModel, BigDecimal estimateUnitLandedCost) {
    BigDecimal estimateUnitTotalCost =
        costingItemGeneralModel.getEstimateUnitPrice().add(estimateUnitLandedCost);
    costingItemGeneralModel.setEstimateItemTotalCost(
        estimateUnitTotalCost.multiply(costingItemGeneralModel.getQuantity()));
  }

  private void setActualItemTotalCost(
      IObCostingItemGeneralModel costingItemGeneralModel, BigDecimal actualUnitLandedCost) {
    BigDecimal actualUnitTotalCost =
        costingItemGeneralModel.getActualUnitPrice().add(actualUnitLandedCost);
    costingItemGeneralModel.setActualItemTotalCost(
        actualUnitTotalCost.multiply(costingItemGeneralModel.getQuantity()));
  }
}
