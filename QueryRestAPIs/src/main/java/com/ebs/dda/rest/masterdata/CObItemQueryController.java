package com.ebs.dda.rest.masterdata;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.item.CObItem;
import com.ebs.dda.jpa.masterdata.item.ICObItem;
import com.ebs.dda.repositories.masterdata.item.CObItemRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/masterdataobjects/items")
public class CObItemQueryController extends BasicQueryController {

  @Autowired
  private CObItemRep cObItemRep;

  @RequestMapping(method = RequestMethod.GET, value = "/gettypes")
  public @ResponseBody
  String readTypes() {

    authorizationManager.authorizeAction(ICObItem.SYS_NAME, IActionsNames.READ_ALL);

    List<CObItem> cObItems = cObItemRep.findAllByOrderByIdAsc();
    return serializeRESTSuccessResponse(cObItems);
  }
}
