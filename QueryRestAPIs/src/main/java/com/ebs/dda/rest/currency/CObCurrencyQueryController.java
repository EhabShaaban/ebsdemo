package com.ebs.dda.rest.currency;

import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/currency")
public class CObCurrencyQueryController extends BasicQueryController {

    @Autowired
    private CObCurrencyRep cObCurrencyRep;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public @ResponseBody
    String readCurrencies() {
        List<CObCurrency> currencies = cObCurrencyRep.findAll();
        return serializeRESTSuccessResponse(currencies);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{iso}")
    public @ResponseBody
    String readCurrenciesExceptSpecificIso(@PathVariable String iso) {
        List<CObCurrency> currencies = cObCurrencyRep.findAllByIsoIsNot(iso);
        return serializeRESTSuccessResponse(currencies);
    }
}
