package com.ebs.dda.rest.masterdata;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.lookups.ILObReason;
import com.ebs.dda.jpa.masterdata.lookups.LObReason;
import com.ebs.dda.repositories.masterdata.lookups.LObReasonRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/services/masterdataobjects/reason")
public class LObReasonQueryController extends BasicQueryController {

  private final String SRO_REASON = "SRO_REASON";
  @Autowired private LObReasonRep lobReasonRep;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String readAll() throws Exception {

    LObReason reason = lobReasonRep.findTopByType(SRO_REASON);
    authorizationManager.authorizeAction(reason, IActionsNames.READ_ALL);

    List<LObReason> lObReasons = lobReasonRep.findAllByTypeOrderByUserCodeDesc(SRO_REASON);

    return serializeRESTSuccessResponse(lObReasons);
  }
}
