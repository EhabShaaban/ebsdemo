package com.ebs.dda.rest.masterdata.utils;

import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;

public class MObItemQueryControllerUtils extends MasterDataQueryControllerUtils {

  public final String RESULT_KEY = "result";
  public final String RESPONSE_STATUS = "status";
  public final String CODE_RESPONSE_KEY = "code";
  public final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  public final String SUCCESSFUL_CODE = "Gen-msg-11";

  private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;
  private CObUoMGeneralModelRep uoMGeneralModelRep;

  public MObItemQueryControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void checkPermissionsForReadingItemBaseUnitAndConversionFactor() {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IMObItem.SYS_NAME, IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION);
  }

  public void checkIfUnitOfMeasureExists(String unitOfMeasureCode)
      throws InstanceNotExistException {
    CObUoMGeneralModel uoMGeneralModel = uoMGeneralModelRep.findOneByUserCode(unitOfMeasureCode);
    if (uoMGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }

  public String serializeItemGeneralDataSuccessResponse(JsonElement generalModel) {
    JsonObject successResponse = new JsonObject();

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESSFUL_CODE);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.add(RESULT_KEY, generalModel);

    return successResponse.toString();
  }

  private void applyAuthorization(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    if (itemPurchaseUnits == null || itemPurchaseUnits.isEmpty()) {
      throw new InstanceNotExistException();
    }
    applyAuthorizationAction(itemPurchaseUnits, actionName);
  }

  private void applyAuthorizationAction(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObItemAuthorizationGeneralModel itemPurchaseUnit : itemPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(itemPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) throw conditionalAuthorizationException;
  }

  public void authorizeUserOnItem(String itemCode, String actionName) throws Exception {
    try {
      List<MObItemAuthorizationGeneralModel> itemAuthorizationGeneralModels =
          itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
      applyAuthorization(itemAuthorizationGeneralModels, actionName);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException();
      authorizationException.initCause(ex);
      throw authorizationException;
    }
  }

  public void authorizeItemNavigationPermission(String itemCode) throws Exception {
    List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
        itemAuthorizationGeneralModelRep.findAllByUserCode(itemCode);
    applyAuthorization(itemPurchaseUnits, IActionsNames.READ_ALL);
  }

  public String getTargetItemCode(MObItemGeneralModel item) throws Exception {
    if (item == null) {
      throw new DataNavigationException();
    }
    return item.getUserCode();
  }

  public void setItemAuthorizationGeneralModelRep(
      MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep) {
    this.itemAuthorizationGeneralModelRep = itemAuthorizationGeneralModelRep;
  }

  public void setUoMGeneralModelRep(CObUoMGeneralModelRep uoMGeneralModelRep) {
    this.uoMGeneralModelRep = uoMGeneralModelRep;
  }
}
