package com.ebs.dda.rest.masterdata.utils;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.rest.utils.QueryControllerUtils;
import java.util.List;

public class MasterDataQueryControllerUtils extends QueryControllerUtils {

  protected AuthorizationManager authorizationManager;
  private MObItemGeneralModelRep itemsRep;

  public MasterDataQueryControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void setItemsRep(MObItemGeneralModelRep itemsRep) {
    this.itemsRep = itemsRep;
  }

  public void applyAuthorizationOnItemByAction(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObItemAuthorizationGeneralModel itemPurchaseUnit : itemPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(itemPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) {
      throw conditionalAuthorizationException;
    }
  }

  public void checkIfItemExists(String itemCode) throws InstanceNotExistException {
    MObItemGeneralModel itemGeneralModel = itemsRep.findByUserCode(itemCode);
    if (itemGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }
}
