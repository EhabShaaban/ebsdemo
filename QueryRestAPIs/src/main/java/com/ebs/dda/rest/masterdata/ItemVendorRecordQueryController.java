package com.ebs.dda.rest.masterdata;

import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecord;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorCommonPurchaseUnitsGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.masterdata.ivr.apis.IItemVendorRecordActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DoubleFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonListContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorCommonPurchaseUnitsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.masterdata.utils.MasterDataQueryControllerUtils;
import com.ebs.dda.rest.utils.Status;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/masterdataobjects/itemvendor")
public class ItemVendorRecordQueryController extends BasicQueryController
    implements InitializingBean {

  private static final String IVR_CODE = "userCode";
  private static final String IVR_UOM = "uomCode";
  private static final String IVR_IVC = "itemVendorCode";
  private static final String IVR_PRICE = "price";
  private static final String IVR_PURCHASE_UNIT = "purchaseUnitName";
  private static final String ITEM = "item";
  private static final String OLD_ITEM_REFERENCE = "oldItemNumber";

  private static final Set<String> itemVendorRecordHomeScreenAllowedActions =
      new HashSet<>(Arrays.asList(IActionsNames.CREATE, IActionsNames.READ_ALL));

  private static final Set<String> itemVendorRecordObjectScreenAllowedActions =
      new HashSet<>(
          Arrays.asList(
              IActionsNames.READ_ALL,
              IItemVendorRecordActionNames.VIEW_GENERAL_DATA,
              IItemVendorRecordActionNames.EDIT_GENERAL_DATA,
              IActionsNames.DELETE));

  @Autowired private ItemVendorRecordGeneralModelRep itemVendorGeneralModelRep;

  @Autowired
  private ItemVendorCommonPurchaseUnitsGeneralModelRep itemVendorCommonPurchaseUnitsGeneralModelRep;

  @Autowired private MObItemGeneralModelRep itemsRep;

  @Autowired
  @Qualifier("itemVendorRecordAllowedActionService")
  private AllowedActionService allowedActionService;

  private MasterDataQueryControllerUtils masterDataQueryControllerUtils;

  @Override
  public void afterPropertiesSet() {
    masterDataQueryControllerUtils = new MasterDataQueryControllerUtils(authorizationManager);
    masterDataQueryControllerUtils.setItemsRep(itemsRep);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filters}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filters,
      HttpServletRequest request) {

    String readAllPermission = IActionsNames.READ_ALL;

    authorizationManager.authorizeAction(IItemVendorRecord.SYS_NAME, readAllPermission);

    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filters, request);

    PageRequest pageRequest =
        PageRequest.of(pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IVR_CODE));

    Page<ItemVendorRecordGeneralModel> ivrsPage = readIVRsData(status, pageRequest, conditions);

    List<ItemVendorRecordGeneralModel> ivrs = ivrsPage.getContent();

    return serializeViewAllResponse(ivrs, ivrsPage.getTotalElements());
  }

  private Page<ItemVendorRecordGeneralModel> readIVRsData(Status status, Pageable pagable, Set<String> conditions) {

    List<String> purchaseUnitNames =
        getConditionAttributeValue(IItemVendorRecord.PURCHASING_UNIT_NAME, conditions);

    JsonListContainsSpecification ivrPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(IVR_PURCHASE_UNIT, purchaseUnitNames);
    JsonListContainsSpecification ivrPurchaseUnitsSpecification =
        getCustomPurchaseUnitJsonListSpecification(IVR_PURCHASE_UNIT, status);

    SimpleStringFieldSpecification ivrVendorCodeSpecification =
        getCustomStringContainsSpecification(status, VENDOR, CODE);
    SimpleStringFieldSpecification ivrItemCodeSpecification =
        getCustomStringContainsSpecification(status, ITEM, CODE);

    JsonFieldSpecification ivrVendorNameSpecification =
        getCustomJsonContainsSpecification(status, VENDOR, NAME);
    JsonFieldSpecification ivrItemNameSpecification =
        getCustomJsonContainsSpecification(status, ITEM, NAME);

    SimpleStringFieldSpecification ivrCodeSpecification =
        getSimpleStringSpecification(status, IVR_CODE);

    SimpleStringFieldSpecification oldItemReferenceSpecification =
        getSimpleStringSpecification(status, OLD_ITEM_REFERENCE);

    DoubleFieldSpecification ivrPriceSpecification = getDoubleFieldSpecification(status, IVR_PRICE);
    SimpleStringFieldSpecification ivrItemCodeAtVendorSpecification =
        getSimpleStringSpecification(status, IVR_IVC);

    SimpleStringFieldSpecification ivrUomSymbolSpecification =
        getSimpleStringSpecification(status, IVR_UOM);

    return itemVendorGeneralModelRep.findAll(
        Specification.where(ivrCodeSpecification)
            .and(ivrVendorCodeSpecification.or(ivrVendorNameSpecification))
            .and(ivrItemCodeSpecification.or(ivrItemNameSpecification))
            .and(ivrUomSymbolSpecification)
            .and(ivrItemCodeAtVendorSpecification)
            .and(ivrPriceSpecification)
            .and(ivrPurchaseUnitsSpecification)
            .and(oldItemReferenceSpecification)
            .and(ivrPurchaseUnitsAuthorizationSpecification),
        pagable);
  }

  private List<ItemVendorRecordGeneralModel> readPage(
      int pageNum, int rowsNum, Set<String> conditions) {

    Page<ItemVendorRecordGeneralModel> listPage = null;
    if (!conditions.contains(WILD_CARD_CONDITION)) {
      listPage = filterItemsAccordingToCondition(pageNum, rowsNum, conditions);
    } else {
      listPage = itemVendorGeneralModelRep.findAll(PageRequest.of(pageNum, rowsNum));
    }
    return listPage.getContent();
  }

  private Page<ItemVendorRecordGeneralModel> filterItemsAccordingToCondition(
      int pageNum, int rowsNum, Set<String> conditions) {
    List<String> purchaseUnitNames = getPurchaseUnitNamesWithConditions(conditions);
    return itemVendorGeneralModelRep.findByPurchasingUnitNames(
        StringUtils.join(purchaseUnitNames, ','), PageRequest.of(pageNum, rowsNum));
  }

  private List<String> getPurchaseUnitNamesWithConditions(Set<String> conditions) {
    return getConditionAttributeValue(
        IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{itemVendorRecordCode}")
  public @ResponseBody String readActions(@PathVariable String itemVendorRecordCode)
      throws Exception {
    ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
        itemVendorGeneralModelRep.findByUserCode(itemVendorRecordCode);
    if (itemVendorRecordGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions = new HashSet<>();
    allowedActions.addAll(
        allowedActionService.getObjectScreenAllowedActions(itemVendorRecordGeneralModel));
    if (isWildCardPermission(allowedActions)) {
      allowedActions = itemVendorRecordObjectScreenAllowedActions;
    }
    allowedActions.retainAll(itemVendorRecordObjectScreenAllowedActions);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(ItemVendorRecordGeneralModel.class);
    if (isWildCardPermission(allowedActions)) {
      allowedActions = itemVendorRecordHomeScreenAllowedActions;
    }
    allowedActions.retainAll(itemVendorRecordHomeScreenAllowedActions);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/commonpurchaseunits/{itemCode}/{vendorCode}")
  public @ResponseBody String readItemAndVendorCommonPurchaseUnits(
      @PathVariable String itemCode, @PathVariable String vendorCode) throws Exception {
    return getAllowedPurchaseUnitsBasedOnUserPermission(
        IActionsNames.READ_ALL, itemCode, vendorCode);
  }

  private String getAllowedPurchaseUnitsBasedOnUserPermission(
      String readPermission, String itemCode, String vendorCode) throws Exception {
    checkIfReadPermissionIsAllowed(ICObPurchasingUnit.SYS_NAME, readPermission);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<ItemVendorCommonPurchaseUnitsGeneralModel> purchasingUnits =
        filterPurchaseUnitAccordingToCondition(conditions, itemCode, vendorCode);
    return serializeRESTSuccessResponse(purchasingUnits);
  }

  private List<ItemVendorCommonPurchaseUnitsGeneralModel> filterPurchaseUnitAccordingToCondition(
      Set<String> conditions, String itemCode, String vendorCode) {
    List<ItemVendorCommonPurchaseUnitsGeneralModel> itemVendorCommonPurchaseUnitsGeneralModels =
        null;

    if (!conditions.contains(WILD_CARD_CONDITION)) {
      List<String> purchaseUnitNames =
          getConditionAttributeValue(ICObPurchasingUnit.PURCHASING_UNIT_NAME, conditions);
      itemVendorCommonPurchaseUnitsGeneralModels =
          itemVendorCommonPurchaseUnitsGeneralModelRep.findByPurchaseUnitNameInOrderByIdDesc(
              purchaseUnitNames);
    } else {
      itemVendorCommonPurchaseUnitsGeneralModels =
          itemVendorCommonPurchaseUnitsGeneralModelRep.findAllByVendorCodeAndItemCode(
              vendorCode, itemCode);
    }

    return itemVendorCommonPurchaseUnitsGeneralModels;
  }

  private boolean isWildCardPermission(Set<String> authorizedActions) {
    return authorizedActions.contains(AllowedActionService.WILD_CARD_PERMISSION);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/generaldata/{ivrCode}")
  public @ResponseBody String readItemVendorRecordGeneralData(@PathVariable String ivrCode)
      throws Exception {
    try {
      authorizeReadActionOnItemVendorRecord(
          ivrCode, IItemVendorRecordActionNames.VIEW_GENERAL_DATA);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    ItemVendorRecordGeneralModel ivrRecord = itemVendorGeneralModelRep.findByUserCode(ivrCode);
    return serializeRESTSuccessResponse(ivrRecord);
  }

  private void authorizeReadActionOnItemVendorRecord(String itemVendorRecordCode, String permission)
      throws Exception {
    ItemVendorRecordGeneralModel ivrRecord =
        itemVendorGeneralModelRep.findByUserCode(itemVendorRecordCode);
    checkIfEntityIsNull(ivrRecord);
    try {
      authorizationManager.authorizeAction(ivrRecord, permission);
    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/next/{ivrCode}")
  public @ResponseBody String readNextCode(@PathVariable String ivrCode) throws Exception {
    authorizeActionOnIVR(ivrCode, IActionsNames.READ_ALL);
    String nextIVRCode = getNextIVRCode(ivrCode);
    return serializeReadNextResponse(nextIVRCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/previous/{ivrCode}")
  public @ResponseBody String readPreviousCode(@PathVariable String ivrCode) throws Exception {
    authorizeActionOnIVR(ivrCode, IActionsNames.READ_ALL);
    String previousIVRCode = getPreviousIVRCode(ivrCode);
    return serializeReadPreviousResponse(previousIVRCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/navigationdata/{ivrCode}")
  public @ResponseBody String getIVRNavigationData(@PathVariable String ivrCode) throws Exception {
    authorizeActionOnIVR(ivrCode, IActionsNames.READ_ALL);
    boolean nextCodeExists = false, previousCodeExists = false;
    try {
      getNextIVRCode(ivrCode);
      nextCodeExists = true;
    } catch (DataNavigationException e) {
    }

    try {
      getPreviousIVRCode(ivrCode);
      previousCodeExists = true;
    } catch (DataNavigationException e) {
    }

    return serializeObjectScreenNavigationData(nextCodeExists, previousCodeExists);
  }

  private void authorizeActionOnIVR(String ivrCode, String permission) throws Exception {
    ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
        itemVendorGeneralModelRep.findByUserCode(ivrCode);
    checkIfEntityIsNull(itemVendorRecordGeneralModel);
    try {
      authorizationManager.authorizeAction(itemVendorRecordGeneralModel, permission);
    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException();
    }
  }

  private String getNextIVRCode(String currentIVRCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    ItemVendorRecordGeneralModel nextItemVendorRecord = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      nextItemVendorRecord =
          itemVendorGeneralModelRep.findNextByUserCode(
              Long.valueOf(currentIVRCode), purchaseUnitNames);
    } else {
      nextItemVendorRecord =
          itemVendorGeneralModelRep.findNextByUserCodeUnconditionalRead(
              Long.valueOf(currentIVRCode));
    }
    return getTargetIVRCode(nextItemVendorRecord);
  }

  private String getPreviousIVRCode(String currentIVRCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    ItemVendorRecordGeneralModel prevItemVendorRecord = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      prevItemVendorRecord =
          itemVendorGeneralModelRep.findPreviousByUserCode(
              Long.valueOf(currentIVRCode), purchaseUnitNames);
    } else {
      prevItemVendorRecord =
          itemVendorGeneralModelRep.findPreviousByUserCodeUnconditionalRead(
              Long.valueOf(currentIVRCode));
    }
    return getTargetIVRCode(prevItemVendorRecord);
  }

  private List<String> getPurchaseUnitNameByAuthorizationCondition(Set<String> conditions) {
    return getConditionAttributeValue(IItemVendorRecord.PURCHASING_UNIT_NAME, conditions);
  }

  private String getTargetIVRCode(ItemVendorRecordGeneralModel itemVendorRecordGeneralModel)
      throws Exception {
    if (itemVendorRecordGeneralModel == null) {
      throw new DataNavigationException();
    }
    return itemVendorRecordGeneralModel.getUserCode();
  }
}
