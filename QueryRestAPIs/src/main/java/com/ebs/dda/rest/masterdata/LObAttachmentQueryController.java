package com.ebs.dda.rest.masterdata;

import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.generalmodels.LObAttachmentTypeGeneralModel;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/services/masterdataobjects/attachment")
public class LObAttachmentQueryController extends BasicQueryController {

  @Autowired private LObAttachmentTypeGeneralModelRep attachmentTypeGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readAll() {

    List<LObAttachmentTypeGeneralModel> attachmentTypes = attachmentTypeGeneralModelRep.findAll();
    return serializeRESTSuccessResponse(attachmentTypes);
  }
}
