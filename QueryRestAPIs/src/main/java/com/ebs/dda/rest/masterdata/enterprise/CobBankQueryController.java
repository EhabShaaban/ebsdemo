package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.utils.AuthorizationUtils;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping(value = "/services/enterprise/banks")
public class CobBankQueryController extends BasicQueryController {
  @Autowired private IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/banksAndAccounts/{companyCode}")
  public @ResponseBody String findBanksDetailsByCompanyCode(@PathVariable String companyCode) throws Exception {
    applyAuthorizationForBank();
    List<IObCompanyBankDetailsGeneralModel> banks =
        companyBankDetailsGeneralModelRep.findByCompanyCode(companyCode);
    return serializeRESTSuccessResponse(banks);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{companyCode}/{bankCode}")
  public @ResponseBody String findCompanyBankAccounts(
      @PathVariable String companyCode, @PathVariable String bankCode) throws Exception {
    applyAuthorizationForCompany();
    applyAuthorizationForBank();
    List<IObCompanyBankDetailsGeneralModel> banks =
        companyBankDetailsGeneralModelRep.findByCodeAndCompanyCode(bankCode, companyCode);
    return serializeRESTSuccessResponse(banks);
  }

  private RequiresPermissions createPermissionForCompany(String sysName, String readAllActionName) {
    return AuthorizationUtils.createClassPermission(sysName, readAllActionName).getPermission();
  }

  private void applyAuthorizationForCompany() throws Exception {
    CObCompany company = new CObCompany();
    checkIfReadPermissionIsAllowed(company.getSysName(), IActionsNames.READ_ALL);
  }

  private void applyAuthorizationForBank() throws Exception {
    CObCompany company = new CObCompany();
    checkIfReadPermissionIsAllowed(company.getSysName(), IActionsNames.READ_BANK_DETAILS);
  }
}
