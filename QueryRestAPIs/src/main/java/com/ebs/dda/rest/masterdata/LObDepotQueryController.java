package com.ebs.dda.rest.masterdata;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.lookups.ILObDepot;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/masterdataobjects/depot")
public class LObDepotQueryController extends BasicQueryController {

  @Autowired private LObDepotRep lObDepotRep;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String readAll() throws Exception {

    authorizationManager.authorizeAction(ILObDepot.SYS_NAME, IActionsNames.READ_ALL);

    List<LObDepot> depotList = lObDepotRep.findAllByOrderByUserCodeDesc();

    return serializeRESTSuccessResponse(depotList);
  }
}
