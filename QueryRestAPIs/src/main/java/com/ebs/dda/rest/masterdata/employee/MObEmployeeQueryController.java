package com.ebs.dda.rest.masterdata.employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ebs.dda.jpa.masterdata.customer.MObEmployee;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;
import com.ebs.dda.rest.BasicQueryController;

@RestController
@RequestMapping(value = "/services/masterdataobjects/employees")
public class MObEmployeeQueryController extends BasicQueryController {

	@Autowired
	private MObEmployeeRep employeeRep;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public @ResponseBody String readEmployees() {
		List<MObEmployee> employees = employeeRep.findAllByOrderByUserCodeDesc();
		return serializeRESTSuccessResponse(employees);
	}

}
