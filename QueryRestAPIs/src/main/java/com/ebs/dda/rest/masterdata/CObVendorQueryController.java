package com.ebs.dda.rest.masterdata;

import com.ebs.dda.jpa.masterdata.vendor.CObVendor;
import com.ebs.dda.jpa.masterdata.vendor.ICObVendor;
import com.ebs.dda.masterdata.vendor.IMObVendorActionNames;
import com.ebs.dda.repositories.masterdata.vendor.CObVendorRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/masterdataobjects/vendors")
public class CObVendorQueryController extends BasicQueryController {

  @Autowired
  private CObVendorRep cObVendorRep;

  @RequestMapping(method = RequestMethod.GET, value = "/gettypes")
  public @ResponseBody
  String readVendorTypes() {

    authorizationManager.authorizeAction(ICObVendor.SYS_NAME, IMObVendorActionNames.READ_ALL);

    List<CObVendor> cObVendors = cObVendorRep.findAllByOrderByIdAsc();
    return serializeRESTSuccessResponse(cObVendors);
  }
}
