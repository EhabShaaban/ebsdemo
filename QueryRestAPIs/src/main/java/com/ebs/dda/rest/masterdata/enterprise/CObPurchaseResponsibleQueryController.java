package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.ICObPurchasingResponsible;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/purchasereponsibles")
public class CObPurchaseResponsibleQueryController extends BasicQueryController {

  @Autowired private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/all")
  public @ResponseBody String readAll() throws Exception {
    checkIfReadPermissionIsAllowed(ICObPurchasingResponsible.SYS_NAME, IActionsNames.READ_ALL);
    List<CObPurchasingResponsibleGeneralModel> purchasingResponsibles =
        purchasingResponsibleGeneralModelRep.findAllByOrderByIdAsc();
    return serializeRESTSuccessResponse(purchasingResponsibles);
  }
}
