package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.dbo.jpa.repositories.specifications.ReadSalesOrderSpecification;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/accounting/salesinvoice")
public class DObSalesInvoiceQueryController extends BasicQueryController
    implements InitializingBean {

  @Autowired
  @Qualifier("salesInvoiceAllowedActionService")
  private AllowedActionService salesInvoiceAllowedActionService;

  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private DObSalesInvoiceBasicGeneralModelRep salesInvoiceBasicGeneralModelRep;
  @Autowired private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  @Autowired private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;

  @Autowired
  private IObSalesInvoiceBusinessPartnerGeneralModelRep
      iObSalesInvoiceBusinessPartnerGeneralModelRep;

  @Autowired
  private IObSalesInvoiceTaxesGeneralModelRep salesInvoiceCompanyTaxesDetailsGeneralModelRep;

  @Autowired private IObSalesInvoiceItemsGeneralModelRep iObSalesInvoiceItemsGeneralModelRep;

  @Autowired private IObInvoiceSummaryGeneralModelRep salesInvoiceItemSummaryGeneralModelRep;

  @Autowired private MObItemGeneralModelRep itemGeneralModelRep;

  @Autowired private IObAlternativeUoMGeneralModelRep alternativeUOMRep;

  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  @Autowired private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;
  @Autowired private IUserAccountSecurityService userAccountSecurityService;

  private DObSalesInvoiceQueryControllerUtils utils;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        salesInvoiceAllowedActionService.getHomeScreenAllowedActions(DObSalesInvoice.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{salesInvoiceCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    DObSalesInvoiceFactory salesInvoiceFactory = new DObSalesInvoiceFactory();
    AbstractStateMachine salesInvoiceStateMachine =
        salesInvoiceFactory.createSalesInvoiceStateMachine(salesInvoiceGeneralModel);
    salesInvoiceAllowedActionService.setStateMachine(salesInvoiceStateMachine);
    Set<String> allowedActions =
        salesInvoiceAllowedActionService.getObjectScreenAllowedActions(salesInvoiceGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_GENERAL_DATA);
    return serializeRESTSuccessResponse(salesInvoiceGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_COMPANY);

    return serializeRESTSuccessResponse(salesInvoiceGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/taxes")
  public @ResponseBody String readTaxes(@PathVariable String salesInvoiceCode) throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_TAXES);
    List<IObSalesInvoiceTaxesGeneralModel> salesInvoiceCompanyTaxesDetailsGeneralModelList =
        salesInvoiceCompanyTaxesDetailsGeneralModelRep.findByInvoiceCodeOrderByTaxCodeAsc(
            salesInvoiceCode);
    return serializeRESTSuccessResponse(salesInvoiceCompanyTaxesDetailsGeneralModelList);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/summary")
  public @ResponseBody String readSummaryData(@PathVariable String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_SUMMARY);
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
            salesInvoiceCode, IDocumentTypes.SALES_INVOICE);
    return serializeRESTSuccessResponse(invoiceSummaryGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObSalesInvoiceGeneralModel.INVOICE_CODE));

    Specification querySpecification = getSalesInvoiceDataSpecification(status, conditions);

    Page<DObSalesInvoiceBasicGeneralModel> salesInvoicesPage =
        salesInvoiceBasicGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObSalesInvoiceBasicGeneralModel> salesInvoices = salesInvoicesPage.getContent();
    return serializeViewAllResponse(salesInvoices, salesInvoicesPage.getTotalElements());
  }

  private Specification getSalesInvoiceDataSpecification(Status status, Set<String> conditions) {

    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObSalesInvoiceGeneralModel.PURCHASE_UNIT, conditions);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObSalesInvoiceGeneralModel.INVOICE_CODE);

    DateContainsSpecification creationDateSpecification =
        getDateContainsSpecification(status, IDObSalesInvoiceGeneralModel.CREATION_DATE);

    JsonFieldSpecification invoiceTypeNameSpecification =
        getJsonContainsSpecification(status, IDObSalesInvoiceGeneralModel.INVOICE_TYPE);

    JsonFieldSpecification purchaseUnitNamesSpecification =
        getJsonContainsSpecification(status, IDObSalesInvoiceBasicGeneralModel.PURCHASE_UNIT_NAME);

    SimpleStringFieldSpecification customerNameSpecification =
        getSimpleStringSpecification(status, IDObSalesInvoiceGeneralModel.CUSTOMER_CODE_NAME);

    JsonFieldSpecification companyNameSpecification =
        getJsonContainsSpecification(status, IDObSalesInvoiceGeneralModel.COMPANY);

    SimpleStringFieldSpecification salesOrderCodeSpecification =
        getSimpleStringSpecification(status, IDObSalesInvoiceGeneralModel.SALES_ORDER);

    SimpleStringFieldSpecification currentStateSpecification =
        getSimpleStringSpecification(status, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);

    Specification querySpecification;
    List<String> documentOwnerNames =
        getConditionAttributeValue(IDObSalesInvoiceGeneralModel.DOCUMENT_OWNER, conditions);
    if (!documentOwnerNames.isEmpty()
        && documentOwnerNames.get(0).contains(IDObSalesInvoiceGeneralModel.LOGGED_IN_USER)) {
      String loggedinUserInfo = getLoggedInUserName();
      DocumentOwnerSpecification documentOwnerSpecification =
          new DocumentOwnerSpecification(loggedinUserInfo);
      querySpecification =
          Specification.where(userCodeSpecification)
              .and(purchaseUnitConditionSpecification)
              .and(invoiceTypeNameSpecification)
              .and(purchaseUnitNamesSpecification)
              .and(creationDateSpecification)
              .and(customerNameSpecification)
              .and(companyNameSpecification)
              .and(currentStateSpecification)
              .and(salesOrderCodeSpecification)
              .and(documentOwnerSpecification);
    } else {
      querySpecification =
          Specification.where(userCodeSpecification)
              .and(purchaseUnitConditionSpecification)
              .and(invoiceTypeNameSpecification)
              .and(purchaseUnitNamesSpecification)
              .and(creationDateSpecification)
              .and(customerNameSpecification)
              .and(companyNameSpecification)
              .and(currentStateSpecification)
              .and(salesOrderCodeSpecification);
    }
    return querySpecification;
  }

  private String getLoggedInUserName() {
    return userAccountSecurityService.getLoggedInUser().toString();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/businesspartner")
  public @ResponseBody String readBusinessPartner(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    utils.checkIfEntityIsNull(salesInvoiceGeneralModel);

    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
        iObSalesInvoiceBusinessPartnerGeneralModelRep.findOneByCode(salesInvoiceCode);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_BUSINESS_PARTNER);
    return serializeRESTSuccessResponse(salesInvoiceBusinessPartnerGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/items")
  public @ResponseBody String readInvoiceItems(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel invoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    utils.checkIfEntityIsNull(invoice);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        invoice, IDObSalesInvoiceActionNames.READ_ITEMS_SECTION);

    List<IObSalesInvoiceItemsGeneralModel> invoiceItemsGeneralModel =
        iObSalesInvoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdDesc(salesInvoiceCode);
    return serializeRESTSuccessResponse(invoiceItemsGeneralModel);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/businesspartneritems/{salesInvoiceCode}/{customerCode}/{purchaseUnitCode}")
  public @ResponseBody String readItemsByVendorCodeAndPurchaseUnitCode(
          @PathVariable String salesInvoiceCode, @PathVariable String customerCode, @PathVariable String purchaseUnitCode) {
    try {
      authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.READ_ALL);
      List<MObItemGeneralModel> items =
          itemGeneralModelRep.findAllByPurchasingUnitCodesContainsOrderByUserCodeDesc(
              purchaseUnitCode);
      List<Map<String, Object>> itemResponseData = getItemResponseData(items);
      return serializeRESTSuccessResponse(itemResponseData);
    } catch (Exception e) {
      throw new AuthorizationException(e);
    }
  }

  private List<Map<String, Object>> getItemResponseData(
      List<MObItemGeneralModel> itemGeneralModels) {
    List<Map<String, Object>> itemResponseData = new ArrayList<>();
    for (MObItemGeneralModel itemGeneralModel : itemGeneralModels) {
      Map<String, Object> itemData = new HashMap<>();
      itemData.put(IIObInvoiceItemsGeneralModel.ITEM_NAME, itemGeneralModel.getItemName());
      itemData.put(IIObInvoiceItemsGeneralModel.ITEM_CODE, itemGeneralModel.getUserCode());
      itemData.put(
          IIObInvoiceItemsGeneralModel.BASE_UNIT_SYMBOL,
          itemGeneralModel.getBasicUnitOfMeasureSymbol());
      itemData.put(
          IIObInvoiceItemsGeneralModel.BASE_UNIT_CODE,
          itemGeneralModel.getBasicUnitOfMeasureCode());
      itemResponseData.add(itemData);
    }
    return itemResponseData;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/businesspartneritemsunits/{salesInvoiceCode}/{itemCode}/{customerCode}/{purchaseUnitCode}")
  public @ResponseBody String readPurchaseOrderItems(
      @PathVariable String salesInvoiceCode,
      @PathVariable String itemCode,
      @PathVariable String customerCode,
      @PathVariable String purchaseUnitCode)
      throws Exception {
    try {
      checkIfReadPermissionIsAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
      List<IObAlternativeUoMGeneralModel> alternativeUoMGeneralModels =
          alternativeUOMRep.findAllByItemCode(itemCode);
      MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
      IObAlternativeUoMGeneralModel alternativeUoMGeneralModel =
          new IObAlternativeUoMGeneralModel();
      alternativeUoMGeneralModel.setAlternativeUnitOfMeasureCode(item.getBasicUnitOfMeasureCode());
      alternativeUoMGeneralModel.setAlternativeUnitOfMeasureName(
          item.getBasicUnitOfMeasureSymbol());
      alternativeUoMGeneralModels.add(alternativeUoMGeneralModel);
      return serializeRESTSuccessResponse(alternativeUoMGeneralModels);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/postingdetails")
  public @ResponseBody String readPostingDetailsData(@PathVariable String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    utils.checkIfEntityIsNull(salesInvoiceGeneralModel);
    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.READ_POSTING_DETAILS);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            salesInvoiceCode, IDocumentTypes.SALES_INVOICE);
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @GetMapping("/sales-orders")
  public @ResponseBody String readApprovedAndGoodsIssuedSalesOrders(
      @RequestParam("business-unit-code") String businessUnitCode) {

    authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL);

    List<DObSalesOrderGeneralModel> salesOrders = new ArrayList<DObSalesOrderGeneralModel>();

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    ReadSalesOrderSpecification salesOrderSpecification = new ReadSalesOrderSpecification();
    Specification<DObSalesOrderGeneralModel> authorizedPurchaseUnit =
        salesOrderSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);
    salesOrders =
        salesOrderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(salesOrderSpecification.isAllowedSalesOrder(businessUnitCode)),
            Sort.by(Sort.Direction.DESC, IDObSalesOrderGeneralModel.USER_CODE));
    return serializeRESTSuccessResponse(salesOrders);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    utils = new DObSalesInvoiceQueryControllerUtils();
  }
}
