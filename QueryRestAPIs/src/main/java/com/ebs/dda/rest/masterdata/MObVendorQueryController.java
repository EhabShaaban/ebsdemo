package com.ebs.dda.rest.masterdata;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.vendor.*;
import com.ebs.dda.masterdata.vendor.IMObVendorActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonListContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/masterdataobjects/vendors")
public class MObVendorQueryController extends BasicQueryController {

  public static final String ACTIVE_STATE = "Active";
  private static final String RESULT_KEY = "result";
  private static final String ACCOUNT_WITH_VENDOR = "accountWithVendor";
  private static final String OLD_VENDOR_NUMBER = "oldVendorNumber";
  private static final String VENDOR_NAME = "vendorName";
  private static final String VENDOR_TYPE_NAME = "typeName";
  private static final String PURCHASE_RESPONSIBLE_NAME = "purchaseResponsibleName";
  private static final String VENDOR_PURCHASE_UNIT_NAMES = "purchasingUnitNames";
  private final String CODE = "userCode";
  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SUCCESSFUL_CODE = "Gen-msg-01";

  @Autowired private MObVendorGeneralModelRep vendorGeneralModelRep;

  @Autowired private AuthorizationManager authorizationManager;

  @Autowired private MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep;

  @Autowired private IObVendorAccountingInfoGeneralModelRep accountingInfoRep;

  @Autowired
  @Qualifier("vendorAllowedActionService")
  private AllowedActionService allowedActionService;

  @RequestMapping(method = RequestMethod.GET, value = "/readauthorizedvendors")
  public @ResponseBody String readAuthorizedVendors() throws Exception {
    try {
      String readAllPermission = IActionsNames.READ_ALL;
      authorizationManager.authorizeAction(MObVendor.SYS_NAME, readAllPermission);
      Set<String> conditions = authorizationManager.getPermissionConditions();
      List<MObVendorGeneralModel> vendors = readAuthorizedVendors(conditions);
      return serializeRESTSuccessResponse(vendors);
    } catch (AuthorizationException | ConditionalAuthorizationException e) {
      throw new ResourceAccessException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseUnitCode}")
  public @ResponseBody String findVendorByPurchaseUnitCodeAndAccountLeadger(
      @PathVariable String purchaseUnitCode) throws Exception {

    try {
      String readAllPermission = IActionsNames.READ_ALL;
      authorizationManager.authorizeAction(MObVendor.SYS_NAME, readAllPermission);
      List<MObVendorGeneralModel> vendors =
          vendorGeneralModelRep.findAllByPurchasingUnitCodesContainsOrderByUserCodeDesc(
              purchaseUnitCode);
      return serializeRESTSuccessResponse(vendors);
    } catch (AuthorizationException | ConditionalAuthorizationException e) {
      throw new ResourceAccessException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/getactive")
  public @ResponseBody String readAll() {

    List<MObVendorGeneralModel> vendors =
        vendorGeneralModelRep.findByCurrentStatesLike("%" + ACTIVE_STATE + "%");

    return serializeRESTSuccessResponse(vendors);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filters}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filters,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(MObVendor.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filters, request);
    PageRequest pageRequest = PageRequest.of(pageNum, rowsNum, Sort.by(Direction.DESC, CODE));

    Page <MObVendorGeneralModel> vendorsPage = readVendorData(conditions, status, pageRequest);
    List <MObVendorGeneralModel>vendors = vendorsPage.getContent();
    return serializeViewAllResponse(vendors, vendorsPage.getTotalElements());
  }

  private Page<MObVendorGeneralModel> readVendorData(Set<String> conditions, Status status, Pageable pagable) {
    JsonListContainsSpecification vendorPurchaseUnitsAuthorizationSpecification =
        getVendorPurchchaseUnitAuthorizationSpecification(conditions);

    JsonListContainsSpecification vendorPurchaseUnitsSpecification =
        getCustomPurchaseUnitJsonListSpecification(VENDOR_PURCHASE_UNIT_NAMES, status);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, CODE);

    JsonFieldSpecification vendorNameSpecification =
        getJsonContainsSpecification(status, VENDOR_NAME);

    JsonFieldSpecification vendorTypeNameSpecification =
        getJsonEqualsSpecification(status, VENDOR_TYPE_NAME);

    JsonFieldSpecification purchaseResponsibleNameSpecification =
        getJsonContainsSpecification(status, PURCHASE_RESPONSIBLE_NAME);

    SimpleStringFieldSpecification oldVendorNumberSpecification =
        getSimpleStringSpecification(status, OLD_VENDOR_NUMBER);

    SimpleStringFieldSpecification accountWithVendorSpecification =
        getSimpleStringSpecification(status, ACCOUNT_WITH_VENDOR);

    return vendorGeneralModelRep
        .findAll(
            Specification.where(vendorPurchaseUnitsAuthorizationSpecification)
                .and(userCodeSpecification)
                .and(vendorTypeNameSpecification)
                .and(accountWithVendorSpecification)
                .and(oldVendorNumberSpecification)
                .and(vendorNameSpecification)
                .and(purchaseResponsibleNameSpecification)
                .and(vendorPurchaseUnitsSpecification),
            pagable);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{vendorCode}")
  public @ResponseBody String readActions(@PathVariable String vendorCode) throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendors =
        vendorPurchaseUnitGeneralModelRep.findAllByUserCode(vendorCode);
    if (vendors.size() == 0) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions = new HashSet<>();
    for (MObVendorPurchaseUnitGeneralModel vendor : vendors) {
      allowedActions.addAll(allowedActionService.getObjectScreenAllowedActions(vendor));
    }

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(MObVendorPurchaseUnitGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/generaldata/{vendorCode}")
  public @ResponseBody String readMObVendorGeneralData(@PathVariable String vendorCode)
      throws Exception {

    try {
      List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
          vendorPurchaseUnitGeneralModelRep.findAllByUserCode(vendorCode);

      authorizeActionOnVendor(vendorPurchaseUnits, IMObVendorActionNames.VIEW_GENERAL_DATA_SECTION);

    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }

    MObVendorGeneralModel vendorGeneralModel = vendorGeneralModelRep.findOneByUserCode(vendorCode);

    if (vendorGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    return serializeVendorGeneralDataSuccessResponse(vendorGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/navigationdata/{vendorCode}")
  public @ResponseBody String getVendorNavigationData(@PathVariable String vendorCode)
      throws Exception {
    authorizeVendorNavigationPermission(vendorCode);
    boolean nextCodeExists = false, previousCodeExists = false;
    try {
      getNextVendorCode(vendorCode);
      nextCodeExists = true;
    } catch (DataNavigationException e) {
    }

    try {
      getPreviousVendorCode(vendorCode);
      previousCodeExists = true;
    } catch (DataNavigationException e) {
    }

    return serializeObjectScreenNavigationData(nextCodeExists, previousCodeExists);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/next/{vendorCode}")
  public @ResponseBody String readNextCode(@PathVariable String vendorCode) throws Exception {
    authorizeVendorNavigationPermission(vendorCode);
    String nextVendorCode = getNextVendorCode(vendorCode);
    return serializeReadNextResponse(nextVendorCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/previous/{vendorCode}")
  public @ResponseBody String readPreviousCode(@PathVariable String vendorCode) throws Exception {
    authorizeVendorNavigationPermission(vendorCode);
    String previousVendorCode = getPreviousVendorCode(vendorCode);
    return serializeReadPreviousResponse(previousVendorCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/accountinginfo/{vendorCode}")
  public @ResponseBody String readMObVendorAccoutningInfo(@PathVariable String vendorCode)
      throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
        vendorPurchaseUnitGeneralModelRep.findAllByUserCode(vendorCode);
    try {
      authorizeActionOnVendor(vendorPurchaseUnits, IActionsNames.READ_ACCOUNTING_DETAILS);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }

    IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel =
        accountingInfoRep.findByVendorCode(vendorCode);
    return serializeVendorGeneralDataSuccessResponse(accountingInfoGeneralModel);
  }

  private List<String> getPurchaseUnitNameByAuthorizationCondition(Set<String> conditions) {
    return getConditionAttributeValue(ICObPurchasingUnit.PURCHASING_UNIT_NAME, conditions);
  }

  private void authorizeVendorNavigationPermission(String vendorCode) throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
        vendorPurchaseUnitGeneralModelRep.findAllByUserCode(vendorCode);
    authorizeActionOnVendor(vendorPurchaseUnits, IActionsNames.READ_ALL);
  }

  private String getNextVendorCode(String currentVendorCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    MObVendorGeneralModel nextVendor = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      nextVendor =
          vendorGeneralModelRep.findNextByUserCode(
              Long.valueOf(currentVendorCode), purchaseUnitNames);
    } else {
      nextVendor =
          vendorGeneralModelRep.findNextByUserCodeUnconditionalRead(
              Long.valueOf(currentVendorCode));
    }
    return getTargetVendorCode(nextVendor);
  }

  private String getPreviousVendorCode(String currentVendorCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    MObVendorGeneralModel prevVendor = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      prevVendor =
          vendorGeneralModelRep.findPreviousByUserCode(
              Long.valueOf(currentVendorCode), purchaseUnitNames);
    } else {
      prevVendor =
          vendorGeneralModelRep.findPreviousByUserCodeUnconditionalRead(
              Long.valueOf(currentVendorCode));
    }
    return getTargetVendorCode(prevVendor);
  }

  private String getTargetVendorCode(MObVendorGeneralModel vendor) throws Exception {
    if (vendor == null) {
      throw new DataNavigationException();
    }
    return vendor.getUserCode();
  }

  private List<String> getPurchaseUnitNamesWithConditions(Set<String> conditions) {
    return getConditionAttributeValue(
        IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);
  }

  private void authorizeActionOnVendor(
      List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits, String actionName)
      throws Exception {
    if (vendorPurchaseUnits == null || vendorPurchaseUnits.isEmpty()) {
      throw new InstanceNotExistException();
    }

    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObVendorPurchaseUnitGeneralModel vendorPurchaseUnit : vendorPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(vendorPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) {
      throw conditionalAuthorizationException;
    }
  }

  private String serializeVendorGeneralDataSuccessResponse(BusinessObject vendorGeneralModel) {
    JsonObject successResponse = new JsonObject();

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESSFUL_CODE);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.add(RESULT_KEY, getGson().toJsonTree(vendorGeneralModel));

    return successResponse.toString();
  }

  private List readAuthorizedVendors(Set<String> conditions) {

    JsonListContainsSpecification vendorPurchaseUnitsAuthorizationSpecification =
        getVendorPurchchaseUnitAuthorizationSpecification(conditions);
    PageRequest pageRequest =
        PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.DESC, IMObVendorGeneralModel.USER_CODE));

    return vendorGeneralModelRep
        .findAll(Specification.where(vendorPurchaseUnitsAuthorizationSpecification), pageRequest)
        .getContent();
  }

  private JsonListContainsSpecification getVendorPurchchaseUnitAuthorizationSpecification(
      Set<String> conditions) {
    List<String> purchaseUnitNamesFromCondition = getPurchaseUnitNamesWithConditions(conditions);

    JsonListContainsSpecification vendorPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(
            VENDOR_PURCHASE_UNIT_NAMES, purchaseUnitNamesFromCondition);

    return vendorPurchaseUnitsAuthorizationSpecification;
  }
}
