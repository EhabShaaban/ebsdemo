package com.ebs.dda.rest.accounting.salesinvoice;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.accounting.salesinvoice.CObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.ICObSalesInvoice;
import com.ebs.dda.repositories.accounting.salesinvoice.CObSalesInvoiceRep;
import com.ebs.dda.rest.BasicQueryController;

@RestController
@RequestMapping(value = "/services/enterprise/salesInvoiceTypes")
public class CObSalesInvoiceQueryController extends BasicQueryController {
  @Autowired
  private CObSalesInvoiceRep cObSalesInvoiceRep;

  @RequestMapping(method = RequestMethod.GET, value = "/getall")
  public @ResponseBody String readAll() throws Exception {
    try {
      checkIfReadPermissionIsAllowed(ICObSalesInvoice.SYS_NAME, READ_ALL);

      List<CObSalesInvoice> salesInvoices = cObSalesInvoiceRep.findAll();
      return serializeRESTSuccessResponse(salesInvoices);
    } catch (ResourceAccessException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }
}
