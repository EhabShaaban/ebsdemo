package com.ebs.dda.rest.utils;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class QueryControllerUtils {

    public QueryControllerUtils(){};

    public static void checkIfEntityIsNull(Object object) throws Exception {
        if (object == null) {
            throw new InstanceNotExistException();
        }
    }
}
