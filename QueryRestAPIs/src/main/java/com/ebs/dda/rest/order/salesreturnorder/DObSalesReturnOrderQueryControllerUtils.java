package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class DObSalesReturnOrderQueryControllerUtils {

  public DObSalesReturnOrderQueryControllerUtils() {}

  public static void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }
}
