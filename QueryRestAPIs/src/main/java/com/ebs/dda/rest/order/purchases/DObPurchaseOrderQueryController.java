package com.ebs.dda.rest.order.purchases;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.DataNavigationException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.lookups.LObContainerRequirement;
import com.ebs.dda.jpa.masterdata.lookups.LObIncoterms;
import com.ebs.dda.jpa.masterdata.lookups.LObPort;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.LObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.RObAttachment;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis.ILObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.RObAttachmentRep;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemUOMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObContainerRequirementRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIncotermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObPortRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.LObModeOfTransport;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.CObShipping;
import com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups.LObModeOfTransportRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.CObShippingRep;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/services/purchase/orders")
public class DObPurchaseOrderQueryController extends BasicQueryController
    implements InitializingBean {

  private final String DATA_KEY = "data";
  private final String USER_CODE = "userCode";
  private final String REF_DOCUMENT_CODE = "refDocumentCode";
  private final String REF_DOCUMENT_TYPE = "refDocumentType";
  private final String ORDER_ITEMS_KEY = "items";
  private final String ORDER_CURRENCY_KEY = "currency";

  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SUCCESSFUL_CODE = "Gen-msg-11";
  private final String OBJECT_TYPE_CODE = "objectTypeCode";
  private final String PURCHASE_UNIT_NAME = "purchaseUnitName";
  private final String STATE = "currentStates";

  private final Logger logger = LoggerFactory.getLogger(DObPurchaseOrderQueryController.class);

  @Autowired private DObPurchaseOrderRep orderRep;

  @Autowired private DObPurchaseOrderGeneralModelRep purchaseOrderGenaralModelRep;
  @Autowired private IObPurchaseOrderVendorGeneralModelRep purchaseOrderVendorGenaralModelRep;
  @Autowired private IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep;

  @Autowired private IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep;

  @Autowired private DObPurchaseOrderConsigneeGeneralModelRep purchaseOrderConsigneeGeneralModelRep;

  @Autowired private IObOrderItemGeneralModelRep orderItemGeneralModelRep;

  @Autowired private IObOrderTaxGeneralModelRep orderTaxGeneralModelRep;

  @Autowired private IObOrderItemSummaryGeneralModelRep orderItemSummaryGeneralModelRep;

  @Autowired private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;

  @Autowired
  private IObOrderLineDetailsQuantitiesGeneralModelRep orderLineDetailsQuantitiesGeneralModelRep;

  @Autowired
  @Qualifier("purchaseOrderAllowedActionService")
  private AllowedActionService purchaseOrderAllowedActionService;

  @Autowired private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;

  @Autowired private DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep;

  @Autowired private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;

  @Autowired private MObVendorGeneralModelRep vendorGeneralModelRep;

  @Autowired
  private IObOrderDocumentRequiredDocumentsGeneralModelRep requiredDocumentsGeneralModelRep;

  @Autowired private LObIncotermsRep lObIncotermsRep;

  @Autowired private CObCurrencyRep cObCurrencyRep;

  @Autowired private CObPaymentTermsRep cObPaymentTermsRep;

  @Autowired private CObShippingRep cObShippingRep;

  @Autowired private LObContainerRequirementRep lObContainerRequirementRep;

  @Autowired private LObModeOfTransportRep lObModeOfTransportRep;

  @Autowired private LObPortRep lObPortRep;

  @Autowired private RObAttachmentRep attachmentRep;

  @Autowired private LObAttachmentTypeRep attachmentTypeRep;

  @Autowired private DObPurchaseOrderAttachmentGeneralModelRep orderAttachmentGeneralModelRep;

  @Autowired private IObOrderApprovalCycleGeneralModelRep approvalCycleGeneralModelRep;
  @Autowired private IObOrderApproverGeneralModelRep approverGeneralModelRep;
  @Autowired private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  @Autowired private MObItemGeneralModelRep itemGeneralModelRep;

  @Autowired
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;

  @Autowired private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;
  @Autowired private MObItemUOMGeneralModelRep itemUOMGeneralModelRep;

  private DObPurchaseOrderQueryControllerUtils utils;

  @Override
  public void afterPropertiesSet() {
    utils = new DObPurchaseOrderQueryControllerUtils(authorizationManager);
    utils.setItemUOMGeneralModelRep(itemUOMGeneralModelRep);
    utils.setOrderItemGeneralModelRep(orderItemGeneralModelRep);
    utils.setPurchaseOrderAllowedActionService(purchaseOrderAllowedActionService);
    utils.setItemAuthorizationGeneralModelRep(itemAuthorizationGeneralModelRep);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value =
          "/itembyvendorandpurchaseunitcode/{purchaseOrderCode}/{vendorCode}/{purchaseUnitCode}")
  public @ResponseBody String readItemsByVendorCodeAndPurchaseUnitCode(
      @PathVariable String purchaseOrderCode,
      @PathVariable String vendorCode,
      @PathVariable String purchaseUnitCode) {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> ivrItemCodes = getIVRItemCodes(vendorCode, purchaseUnitCode);
    Set<String> poItemCodes = getPOItemCodes(purchaseOrderCode);
    Set<String> itemCodes = getItemCodesInIVRNotInPO(ivrItemCodes, poItemCodes);
    List<MObItemGeneralModel> itemGeneralModels = getItemsByItemCodes(itemCodes);
    List<Map<String, Object>> itemResponseData = getItemResponseData(itemGeneralModels);
    return serializeRESTSuccessResponse(itemResponseData);
  }

  private Set<String> getItemCodesInIVRNotInPO(Set<String> ivrItemCodes, Set<String> poItemCodes) {
    return ivrItemCodes.removeAll(poItemCodes) ? ivrItemCodes : ivrItemCodes;
  }

  private Set<String> getPOItemCodes(String purchaseOrderCode) {
    Set<String> itemCodes = new HashSet<>();
    List<IObOrderLineDetailsGeneralModel> POItems =
        orderLineDetailsGeneralModelRep.findByOrderCode(purchaseOrderCode);
    for (IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel : POItems) {
      String itemCode = orderLineDetailsGeneralModel.getItemCode();
      itemCodes.add(itemCode);
    }
    return itemCodes;
  }

  private List<MObItemGeneralModel> getItemsByItemCodes(Set<String> itemCodes) {

    if (itemCodes.isEmpty()) {
      return new ArrayList<>();
    }

    return itemGeneralModelRep.findByUserCodeIn(itemCodes);
  }

  private List<Map<String, Object>> getItemResponseData(
      List<MObItemGeneralModel> itemGeneralModels) {
    List<Map<String, Object>> itemResponseData = new ArrayList<>();
    for (MObItemGeneralModel itemGeneralModel : itemGeneralModels) {
      Map<String, Object> itemData = new HashMap<>();
      itemData.put("itemName", itemGeneralModel.getItemName());
      itemData.put("itemCode", itemGeneralModel.getUserCode());
      itemData.put("baseUnitSymbol", itemGeneralModel.getBasicUnitOfMeasureSymbol());
      itemData.put("baseUnitCode", itemGeneralModel.getBasicUnitOfMeasureCode());
      itemResponseData.add(itemData);
    }
    return itemResponseData;
  }

  private Set<String> getIVRItemCodes(String vendorCode, String purchaseUnitCode) {
    List<ItemVendorRecordGeneralModel> itemVendorRecordGeneralModels =
        itemVendorRecordGeneralModelRep.findByVendorCodeAndPurchasingUnitCode(
            vendorCode, purchaseUnitCode);
    Set<String> itemCodes = new HashSet<>();
    for (ItemVendorRecordGeneralModel itemVendorRecordGeneralModel :
        itemVendorRecordGeneralModels) {
      String itemCode = itemVendorRecordGeneralModel.getItemCode();
      itemCodes.add(itemCode);
    }
    return itemCodes;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest = PageRequest.of(pageNum, rowsNum, Sort.by(Direction.DESC, USER_CODE));
    Page<DObPurchaseOrderGeneralModel> purchaseOrderPage =
        readPurchaseOrderData(conditions, status, pageRequest);
    List purchaseOrders = purchaseOrderPage.getContent();
    return serializeViewAllResponse(purchaseOrders, purchaseOrderPage.getTotalElements());
  }

  @GetMapping(value = "/purchaseordersforgoodsreceipt")
  public @ResponseBody String readPurchaseOrdersForGoodsReceipt() {

    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    ReadPurchaseOrderForGoodsReceiptSpecification specification =
        new ReadPurchaseOrderForGoodsReceiptSpecification();

    List<DObPurchaseOrderConsigneeGeneralModel> purchaseOrders =
        purchaseOrderConsigneeGeneralModelRep.findAll(
            specification
                .isAuthorizedPurchaseUnit(purchaseUnitNames)
                .and(specification.isValidPurchaseOrder()),
            Sort.by(Direction.DESC, USER_CODE));

    return serializeRESTSuccessResponse(purchaseOrders);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readTypes() {
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(Arrays.asList(OrderTypeEnum.values()));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/purchasingUnits")
  public @ResponseBody String readPurchasingUnit() {

    List<CObPurchasingUnitGeneralModel> purchasingUnits = purchasingUnitGeneralModelRep.findAll();

    return serializeRESTSuccessResponse(purchasingUnits);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/purchasingResponsibles")
  public @ResponseBody String readPurchasingResponsible() {

    List<CObPurchasingResponsibleGeneralModel> purchasingResponsibles =
        purchasingResponsibleGeneralModelRep.findAll();

    return serializeRESTSuccessResponse(purchasingResponsibles);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/vendors")
  public @ResponseBody String readvendor() {

    List<MObVendorGeneralModel> vendors = vendorGeneralModelRep.findAll();

    return serializeRESTSuccessResponse(vendors);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/incoterms")
  public @ResponseBody String readIncoterms() {

    List<LObIncoterms> incoterms = lObIncotermsRep.findAll();
    return serializeRESTSuccessResponse(incoterms);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/currencies")
  public @ResponseBody String readCurrencies() {

    List<CObCurrency> currencies = cObCurrencyRep.findAllByOrderByUserCodeAsc();
    return serializeRESTSuccessResponse(currencies);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/paymentterms")
  public @ResponseBody String readPaymentTerms() {

    List<CObPaymentTerms> paymentTerms = cObPaymentTermsRep.findAll();
    return serializeRESTSuccessResponse(paymentTerms);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/shippinginstructions")
  public @ResponseBody String readShippingInstructions() {

    List<CObShipping> shippingInstructions = cObShippingRep.findAll();
    return serializeRESTSuccessResponse(shippingInstructions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/containertypes")
  public @ResponseBody String readContainerTypes() {

    List<LObContainerRequirement> containerTypes = lObContainerRequirementRep.findAll();
    return serializeRESTSuccessResponse(containerTypes);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/transportmodes")
  public @ResponseBody String readTransportModes() {

    List<LObModeOfTransport> transportModes = lObModeOfTransportRep.findAll();
    return serializeRESTSuccessResponse(transportModes);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/ports")
  public @ResponseBody String readPorts() {

    List<LObPort> ports = lObPortRep.findAll();
    return serializeRESTSuccessResponse(ports);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String purchaseOrderCode)
      throws Exception {

    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizeReadActionOnPurchaseOrder(purchaseOrderGeneralModel, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(purchaseOrderGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/vendor")
  public @ResponseBody String readVendorData(@PathVariable String purchaseOrderCode)
      throws Exception {

    IObPurchaseOrderVendorGeneralModel purchaseOrderVendorGeneralModel =
        purchaseOrderVendorGenaralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizeReadActionOnPurchaseOrder(
        purchaseOrderVendorGeneralModel, IPurchaseOrderActionNames.VIEW_VENDOR_SECTION);

    return serializeRESTSuccessResponse(purchaseOrderVendorGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/paymentanddelivery/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderPaymentAndDelivery(
      @PathVariable String purchaseOrderCode) throws Exception {

    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.VIEW_PAYMENT_AND_DELIVERY_SECTION);

    DObPaymentAndDeliveryGeneralModel purchaseOrderPaymentAndDelivery =
        paymentAndDeliveryGeneralModelRep.findByUserCode(purchaseOrderCode);

    return serializeRESTSuccessResponse(purchaseOrderPaymentAndDelivery);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/paymentdetails")
  public @ResponseBody String readPurchaseOrderPaymentDetails(
      @PathVariable String purchaseOrderCode) throws Exception {

    IObOrderPaymentDetailsGeneralModel purchaseOrderPaymentDetailsGeneralModel =
        orderPaymentDetailsGeneralModelRep.findOneByOrderCode(purchaseOrderCode);

    authorizeOnPurchaseOrderIOb(
        purchaseOrderCode,
        purchaseOrderPaymentDetailsGeneralModel,
        IPurchaseOrderActionNames.VIEW_PAYMENT_TERM_SECTION);

    return serializeRESTSuccessResponse(purchaseOrderPaymentDetailsGeneralModel);
  }

  private void authorizeOnPurchaseOrderIOb(
      String purchaseOrderCode, InformationObject informationObject, String readAction)
      throws Exception {
    if (informationObject != null) {
      authorizeReadActionOnPurchaseOrder(informationObject, readAction);
    } else {
      authorizeReadActionOnPurchaseOrder(purchaseOrderCode, readAction);
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/attachment/{attachmentId}")
  public ResponseEntity<byte[]> getAttachment(@PathVariable String attachmentId) {
    Optional<RObAttachment> optionalRObAttachment =
        attachmentRep.findById(Long.parseLong(attachmentId));
    if (optionalRObAttachment.isPresent()) {
      RObAttachment attachment = optionalRObAttachment.get();
      return ResponseEntity.ok()
          .header(
              HttpHeaders.CONTENT_DISPOSITION,
              "attachment; filename=\"" + attachment.getName() + "\"")
          .body(attachment.getContent());
    }
    return ResponseEntity.status(404).body(null);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readAuthorizedAction() throws Exception {
    Set<String> allowedActions =
        purchaseOrderAllowedActionService.getHomeScreenAllowedActions(IDObPurchaseOrder.SYS_NAME);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/company")
  public @ResponseBody String readPurchaseOrderCompanyData(@PathVariable String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkIfEntityIsNull(purchaseOrderGeneralModel);

    authorizationManager.authorizeActionOnObject(
        purchaseOrderGeneralModel, IPurchaseOrderActionNames.READ_COMPANY_DATA);
    IObOrderCompanyGeneralModel purchaseOrderCompanySection =
        orderCompanyGeneralModelRep.findByUserCodeAndPartyRole(purchaseOrderCode, "BILL_TO");

    return serializeRESTSuccessResponse(purchaseOrderCompanySection);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/consignee/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderConsignee(@PathVariable String purchaseOrderCode)
      throws Exception {

    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.VIEW_CONSIGNEE_SECTION);
    DObPurchaseOrderConsigneeGeneralModel purchaseOrderConsignee =
        purchaseOrderConsigneeGeneralModelRep.findByUserCode(purchaseOrderCode);

    return serializeRESTSuccessResponse(purchaseOrderConsignee);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/items")
  public @ResponseBody String readOrderItems(@PathVariable String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkIfEntityIsNull(purchaseOrderGeneralModel);

    authorizationManager.authorizeActionOnObject(
        purchaseOrderGeneralModel, IPurchaseOrderActionNames.READ_ITEMS_SECTION_WITH_PRICES);

    List<IObOrderItemGeneralModel> orderItems =
        orderItemGeneralModelRep.findByOrderCodeOrderByIdDesc(purchaseOrderCode);

    List<IObOrderTaxGeneralModel> taxes =
        orderTaxGeneralModelRep.findByOrderCodeOrderByCodeAsc(purchaseOrderCode);

    IObOrderItemSummaryGeneralModel itemSummary =
        orderItemSummaryGeneralModelRep.findOneByOrderCode(purchaseOrderCode);

    JsonObject response =
        utils.constructItemSectionResponse(orderItems, taxes, itemSummary, getGson());

    return serializeRESTSuccessResponse(response);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/goodspurchaseorders")
  public @ResponseBody String readGoodsPurchaseOrders() {
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    ReadGoodsPurchaseOrderForServicePOSpecification orderSpecification =
        new ReadGoodsPurchaseOrderForServicePOSpecification();

    List<String> purchaseUnitNamesFromCondition =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    JsonListContainsSpecification goodsPOPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(PURCHASE_UNIT_NAME, purchaseUnitNamesFromCondition);

    List<DObPurchaseOrderGeneralModel> orders =
        purchaseOrderGenaralModelRep.findAll(
            Specification.where(goodsPOPurchaseUnitsAuthorizationSpecification)
                .and(orderSpecification.isAllowedDobOrder()),
            Sort.by(Direction.DESC, "id"));
    return serializeRESTSuccessResponse(orders);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{purchaseOrderCode}/items/unitofmeasures/{itemCode}")
  public @ResponseBody String readOrderItemsUOMs(
      @PathVariable String purchaseOrderCode, @PathVariable String itemCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkIfUserIsAuthorizedOnAction(orderGeneralModel);

    utils.checkIfActionAllowedInCurrentState(orderGeneralModel);

    utils.applyAuthorizationOnItemBusinessUnits(itemCode);

    List<JsonObject> itemUOMDetailsList =
        utils.getServicePurchaseOrderItemUOMsJsonObjects(purchaseOrderCode, itemCode, getGson());

    return serializeRESTSuccessResponse(itemUOMDetailsList);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/itemsview/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderItems(@PathVariable String purchaseOrderCode)
      throws Exception {
    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.READ_ITEMS_SECTION_WITH_PRICES);
    List<IObOrderLineDetailsGeneralModel> orderLineDetailsGeneralModels =
        orderLineDetailsGeneralModelRep.findByOrderCodeOrderByIdDesc(purchaseOrderCode);
    JsonObject orderItemsAndCurrency = new JsonObject();
    for (IObOrderLineDetailsGeneralModel orderLineItem : orderLineDetailsGeneralModels) {
      List<IObOrderLineDetailsQuantitiesGeneralModel> orderLineDetailsQuantitiesGeneralModel =
          orderLineDetailsQuantitiesGeneralModelRep.findByRefInstanceIdOrderByIdDesc(
              orderLineItem.getId());
      orderLineItem.setQuantities(orderLineDetailsQuantitiesGeneralModel);
    }
    String currencyISO = getCurrencyInitialsForPurchaseOrder(purchaseOrderCode);
    if (currencyISO != null) {
      orderItemsAndCurrency.addProperty(ORDER_CURRENCY_KEY, currencyISO);
    }
    orderItemsAndCurrency.add(ORDER_ITEMS_KEY, getGson().toJsonTree(orderLineDetailsGeneralModels));

    return serializeOrderItems(orderItemsAndCurrency);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value =
          "/item/quantity/unitofmeasures/{purchaseOrderCode}/{itemCode}/{vendorCode}/{purchaseUnitCode}")
  public @ResponseBody String readPurchaseOrderItemQuantities(
      @PathVariable String purchaseOrderCode,
      @PathVariable String itemCode,
      @PathVariable String vendorCode,
      @PathVariable String purchaseUnitCode)
      throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkIfUserIsAuthorizedOnAction(orderGeneralModel);

    try {
      checkIfReadPermissionIsAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

      List<ItemVendorRecordUnitOfMeasuresGeneralModel> unitOfMeasuresList =
          itemVendorRecordUnitOfMeasuresGeneralModelRep
              .findAllByItemCodeAndVendorCodeAndPurchasingUnitCodeOrderById(
                  itemCode, vendorCode, purchaseUnitCode);

      ItemVendorRecordUnitOfMeasuresGeneralModel basicUnitOfMeasure =
          getItemBasicUnitOfMeasure(itemCode);

      ItemVendorRecordGeneralModel ivrUoMRecord =
          itemVendorRecordGeneralModelRep
              .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                  itemCode,
                  vendorCode,
                  basicUnitOfMeasure.getAlternativeUnitOfMeasureCode(),
                  purchaseUnitCode);

      if (ivrUoMRecord != null) {
        unitOfMeasuresList.add(0, basicUnitOfMeasure);
      }
      filterOrderUnitsThatAlreadyExistInItemInPurchaseOrder(
          unitOfMeasuresList, purchaseOrderCode, itemCode);
      return serializeRESTSuccessResponse(unitOfMeasuresList);
    } catch (ResourceAccessException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  private void filterOrderUnitsThatAlreadyExistInItemInPurchaseOrder(
      List<ItemVendorRecordUnitOfMeasuresGeneralModel> unitOfMeasuresList,
      String purchaseOrderCode,
      String itemCode) {
    List<IObOrderLineDetailsQuantitiesGeneralModel> orderUnitsInItemInPO =
        getOrderUnitsInItemInPurchaseOrder(purchaseOrderCode, itemCode);
    List<ItemVendorRecordUnitOfMeasuresGeneralModel> filterableUnitOfMeasures =
        getAllIVRUnitOfMeasuresInItemInPurchaseOrder(unitOfMeasuresList, orderUnitsInItemInPO);
    unitOfMeasuresList.removeAll(filterableUnitOfMeasures);
  }

  private List<ItemVendorRecordUnitOfMeasuresGeneralModel>
      getAllIVRUnitOfMeasuresInItemInPurchaseOrder(
          List<ItemVendorRecordUnitOfMeasuresGeneralModel> unitOfMeasuresList,
          List<IObOrderLineDetailsQuantitiesGeneralModel> orderUnitsInItemInPO) {
    List<ItemVendorRecordUnitOfMeasuresGeneralModel> filterableUnitOfMeasures = new ArrayList<>();
    for (ItemVendorRecordUnitOfMeasuresGeneralModel unitOfMeasure : unitOfMeasuresList) {
      for (IObOrderLineDetailsQuantitiesGeneralModel quantityInItemInPO : orderUnitsInItemInPO) {
        if (unitOfMeasure
            .getAlternativeUnitOfMeasureCode()
            .equals(quantityInItemInPO.getOrderUnitCode())) {
          filterableUnitOfMeasures.add(unitOfMeasure);
        }
      }
    }
    return filterableUnitOfMeasures;
  }

  private List<IObOrderLineDetailsQuantitiesGeneralModel> getOrderUnitsInItemInPurchaseOrder(
      String purchaseOrderCode, String itemCode) {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(purchaseOrderCode, itemCode);
    return orderLineDetailsQuantitiesGeneralModelRep.findByRefInstanceIdOrderByIdDesc(
        orderLineDetailsGeneralModel.getId());
  }

  private ItemVendorRecordUnitOfMeasuresGeneralModel getItemBasicUnitOfMeasure(
      @PathVariable String itemCode) {
    ItemVendorRecordUnitOfMeasuresGeneralModel basicUnitOfMeasure =
        new ItemVendorRecordUnitOfMeasuresGeneralModel();
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    basicUnitOfMeasure.setConversionFactor(1.0f);
    basicUnitOfMeasure.setAlternativeUnitOfMeasureCode(item.getBasicUnitOfMeasureCode());
    basicUnitOfMeasure.setAlternativeUnitOfMeasureName(item.getBasicUnitOfMeasureSymbol());
    return basicUnitOfMeasure;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/itemsviewwithoutprices/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderItemsWithoutPrices(
      @PathVariable String purchaseOrderCode) throws Exception {
    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.VIEW_ITEMS_SECTION_WITHOUT_PRICES);
    List<IObOrderLineDetailsGeneralModel> orderLineDetailsGeneralModels =
        orderLineDetailsGeneralModelRep.findByOrderCodeOrderByIdDesc(purchaseOrderCode);
    JsonObject orderItems = new JsonObject();
    for (IObOrderLineDetailsGeneralModel orderLineItem : orderLineDetailsGeneralModels) {
      List<IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel>
          orderLineDetailsQuantitiesGeneralModel =
              orderLineDetailsQuantitiesGeneralModelRep
                  .findOrderQuantitiesWithoutPricesGeneralModelByRefInstanceId(
                      orderLineItem.getId());

      orderLineItem.setQuantitiesWithoutPrices(orderLineDetailsQuantitiesGeneralModel);
    }

    orderItems.add(ORDER_ITEMS_KEY, getGson().toJsonTree(orderLineDetailsGeneralModels));

    return serializeOrderItems(orderItems);
  }

  private String getCurrencyInitialsForPurchaseOrder(String purchaseOrderCode) {
    DObPaymentAndDeliveryGeneralModel purchaseOrderPaymentAndDelivery =
        paymentAndDeliveryGeneralModelRep.findByUserCode(purchaseOrderCode);
    if (purchaseOrderPaymentAndDelivery == null) {
      return null;
    }
    String currencyCode = purchaseOrderPaymentAndDelivery.getCurrencyCode();
    if (currencyCode == null) {
      return null;
    }
    CObCurrency cObCurrency = cObCurrencyRep.findOneByUserCode(currencyCode);
    if (cObCurrency == null) {
      return null;
    }
    return cObCurrency.getIso();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{purchaseOrderCode}")
  public @ResponseBody String readActions(@PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkIfEntityIsNull(orderGeneralModel);

    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();

    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(orderGeneralModel);
    purchaseOrderAllowedActionService.setStateMachine(purchaseOrderStateMachine);

    Set<String> allowedActions =
        purchaseOrderAllowedActionService.getObjectScreenAllowedActions(orderGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/requiredDocument/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderRequiredDocuments(
      @PathVariable String purchaseOrderCode) throws Exception {

    String viewRequiredDocuments = IPurchaseOrderActionNames.VIEW_REQUIRED_DOCUMENTS;

    authorizeReadActionOnPurchaseOrder(purchaseOrderCode, viewRequiredDocuments);

    List<IObOrderDocumentRequiredDocumentsGeneralModel> purchaseOrderRequiredDocuments =
        new ArrayList<>();
    purchaseOrderRequiredDocuments.addAll(
        requiredDocumentsGeneralModelRep.findByUserCodeOrderById(purchaseOrderCode));

    return serializeRESTSuccessResponse(purchaseOrderRequiredDocuments);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/requiredDocuments")
  public @ResponseBody String readRequiredDocuments() throws Exception {
    try {
      authorizationManager.authorizeAction(ILObAttachmentType.SYS_NAME, IActionsNames.READ_ALL);
      List<LObAttachmentType> attachmentTypes = attachmentTypeRep.findAll();
      return serializeRESTSuccessResponse(attachmentTypes);
    } catch (AuthorizationException | ConditionalAuthorizationException e) {
      logger.error("This action is not authorized to the logged in user ", e);
      throw new ResourceAccessException();
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/attachments/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderAttachments(@PathVariable String purchaseOrderCode)
      throws Exception {

    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.VIEW_ATTACHMENT_SECTION);
    DObPurchaseOrder order = orderRep.findOneByUserCode(purchaseOrderCode);
    if (order == null) {
      throw new InstanceNotExistException();
    }

    List<DObPurchaseOrderAttachmentGeneralModel> orderAttachments =
        orderAttachmentGeneralModelRep.findAllByUserCodeOrderByIdAsc(purchaseOrderCode);

    return serializeRESTSuccessResponse(orderAttachments);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/navigationdata/{purchaseOrderCode}")
  public @ResponseBody String getVendorNavigationData(@PathVariable String purchaseOrderCode)
      throws Exception {
    authorizeActionOnPurchaseOrder(purchaseOrderCode, IActionsNames.READ_ALL);
    boolean nextCodeExists = false, previousCodeExists = false;
    try {
      getNextPurchaseOrderCode(purchaseOrderCode);
      nextCodeExists = true;
    } catch (DataNavigationException e) {
    }

    try {
      getPreviousPurchaseOrderCode(purchaseOrderCode);
      previousCodeExists = true;
    } catch (DataNavigationException e) {
    }

    return serializeObjectScreenNavigationData(nextCodeExists, previousCodeExists);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/next/{purchaseOrderCode}")
  public @ResponseBody String readNextCode(@PathVariable String purchaseOrderCode)
      throws Exception {
    authorizeActionOnPurchaseOrder(purchaseOrderCode, IActionsNames.READ_ALL);
    String nextPurchaseOrderCode = getNextPurchaseOrderCode(purchaseOrderCode);
    return serializeReadNextResponse(nextPurchaseOrderCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/previous/{purchaseOrderCode}")
  public @ResponseBody String readPreviousCode(@PathVariable String purchaseOrderCode)
      throws Exception {
    authorizeActionOnPurchaseOrder(purchaseOrderCode, IActionsNames.READ_ALL);
    String previousPurchaseOrderCode = getPreviousPurchaseOrderCode(purchaseOrderCode);
    return serializeReadPreviousResponse(previousPurchaseOrderCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/approvalcycles/{purchaseOrderCode}")
  public @ResponseBody String readPurchaseOrderApprovalCycles(
      @PathVariable String purchaseOrderCode) throws Exception {

    authorizeReadActionOnPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.VIEW_APPROVAL_CYCLES);
    List<IObOrderApprovalCycleGeneralModel> approvalCycles =
        approvalCycleGeneralModelRep.findByPurchaseOrderCodeOrderByApprovalCycleNumDesc(
            purchaseOrderCode);
    for (IObOrderApprovalCycleGeneralModel approvalCycle : approvalCycles) {
      List<IObOrderApproverGeneralModel> approvers =
          approverGeneralModelRep.findByPurchaseOrderCodeAndApprovalCycleNumOrderByControlPointCode(
              purchaseOrderCode, approvalCycle.getApprovalCycleNum());
      approvalCycle.setApprovers(approvers);
    }
    return serializeRESTSuccessResponse(approvalCycles);
  }

  private Page<DObPurchaseOrderGeneralModel> readPurchaseOrderData(
      Set<String> conditions, Status status, Pageable pagable) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, USER_CODE);

    SimpleStringFieldSpecification orderStateSpecification =
        getSimpleStringSpecification(status, STATE);

    SimpleStringFieldSpecification orderTypeSpecification =
        getSimpleStringSpecification(status, OBJECT_TYPE_CODE);

    JsonFieldSpecification purchaseUnitNameSpecification =
        getJsonContainsSpecification(status, PURCHASE_UNIT_NAME);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(purchaseUnitNames, "purchaseUnitNameEn");

    SimpleStringFieldSpecification vendorCodeSpecification =
        getCustomStringContainsSpecification(status, VENDOR, CODE);

    JsonFieldSpecification vendorNameSpecification =
        getCustomJsonContainsSpecification(status, VENDOR, NAME);

    SimpleStringFieldSpecification refDocumentCodeSpecification =
        getCustomStringContainsSpecification(
            status, IDObPurchaseOrderGeneralModel.REF_DOCUMENT, CODE);

    SimpleStringFieldSpecification refDocumentTypeSpecification =
        getCustomStringContainsSpecification(
            status, IDObPurchaseOrderGeneralModel.REF_DOCUMENT, IDObPurchaseOrderGeneralModel.TYPE);
    return purchaseOrderGenaralModelRep.findAll(
        Specification.where(userCodeSpecification)
            .and(orderStateSpecification)
            .and(purchaseUnitConditionSpecification)
            .and(orderTypeSpecification)
            .and(purchaseUnitNameSpecification)
            .and(vendorCodeSpecification.or(vendorNameSpecification))
            .and(refDocumentCodeSpecification.or(refDocumentTypeSpecification)),
        pagable);
  }

  private List<String> getPurchaseUnitNameByAuthorizationCondition(Set<String> conditions) {
    return getConditionAttributeValue(
        IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);
  }

  private String serializeOrderItems(JsonObject orderItemsJsonArray) {

    JsonObject successResponse = new JsonObject();

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESSFUL_CODE);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.add(DATA_KEY, orderItemsJsonArray);

    return successResponse.toString();
  }

  private void authorizeActionOnPurchaseOrder(String purchaseOrderCode, String permission)
      throws Exception {

    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);
    checkIfEntityIsNull(purchaseOrderGeneralModel);
    authorizationManager.authorizeAction(purchaseOrderGeneralModel, permission);
  }

  private String getNextPurchaseOrderCode(String purchaseOrderCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    DObPurchaseOrderGeneralModel nextPurchaseOrder = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      nextPurchaseOrder =
          purchaseOrderGenaralModelRep.findNextByUserCode(
              Long.valueOf(purchaseOrderCode), purchaseUnitNames);
    } else {
      nextPurchaseOrder =
          purchaseOrderGenaralModelRep.findNextByUserCodeUnconditionalRead(
              Long.valueOf(purchaseOrderCode));
    }
    return getTargetPurchaseOrderCode(nextPurchaseOrder);
  }

  private String getPreviousPurchaseOrderCode(String purchaseOrderCode) throws Exception {
    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames = getPurchaseUnitNameByAuthorizationCondition(conditions);
    DObPurchaseOrderGeneralModel prevPurchaseOrder = null;
    if (!conditions.contains(WILD_CARD_CONDITION) && !purchaseUnitNames.isEmpty()) {
      prevPurchaseOrder =
          purchaseOrderGenaralModelRep.findPreviousByUserCode(
              Long.valueOf(purchaseOrderCode), purchaseUnitNames);
    } else {
      prevPurchaseOrder =
          purchaseOrderGenaralModelRep.findPreviousByUserCodeUnconditionalRead(
              Long.valueOf(purchaseOrderCode));
    }
    return getTargetPurchaseOrderCode(prevPurchaseOrder);
  }

  private String getTargetPurchaseOrderCode(DObPurchaseOrderGeneralModel purchaseOrder)
      throws Exception {
    if (purchaseOrder == null) {
      throw new DataNavigationException();
    }
    return purchaseOrder.getUserCode();
  }

  private void authorizeReadActionOnPurchaseOrder(String purchaseOrderCode, String permission)
      throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGenaralModelRep.findOneByUserCode(purchaseOrderCode);
    checkIfEntityIsNull(purchaseOrderGeneralModel);
    try {
      authorizationManager.authorizeAction(purchaseOrderGeneralModel, permission);
    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException();
    }
  }

  private void authorizeReadActionOnPurchaseOrder(
      BusinessObject purchaseOrderGeneralModel, String permission) throws Exception {
    checkIfEntityIsNull(purchaseOrderGeneralModel);
    try {
      authorizationManager.authorizeAction(purchaseOrderGeneralModel, permission);
    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException();
    }
  }
}
