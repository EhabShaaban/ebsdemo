package com.ebs.dda.rest.accounting.initialbalanceupload;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUpload;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUploadGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.DObInitialBalanceUploadGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/initialbalanceupload")
public class DObInitialBalanceUploadQueryController extends BasicQueryController {

  @Autowired DObInitialBalanceUploadGeneralModelRep initialBalanceUploadGeneralModelRep;

  @Autowired
  @Qualifier("initialBalanceUploadAllowedActionService")
  private AllowedActionService allowedActionService;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObInitialBalanceUpload.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObInitialBalanceUploadGeneralModel.USER_CODE));

    Specification querySpecification = getInitialBalanceUploadDataSpecification(conditions, status);
    Page initialBalanceUploadsPage =
        initialBalanceUploadGeneralModelRep.findAll(querySpecification, pageRequest);
    List initialBalanceUploads = initialBalanceUploadsPage.getContent();
    return serializeViewAllResponse(
        initialBalanceUploads, initialBalanceUploadsPage.getTotalElements());
  }

  private Specification getInitialBalanceUploadDataSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(
            IDObInitialBalanceUploadGeneralModel.PURCHASING_UNIT_NAME, conditions);
    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObInitialBalanceUploadGeneralModel.PURCHASING_UNIT_NAME_EN);

    Specification querySpecification = Specification.where(businessUnitAuthorizationSpecification);
    return querySpecification;
  }

  @GetMapping("/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(IDObInitialBalanceUpload.SYS_NAME);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }
}
