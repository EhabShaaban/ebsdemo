package com.ebs.dda.rest.utils;

import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;

import java.util.HashMap;
import java.util.Map;

public class Status {
  public  static final String FILTER_VALUE_SEPARATOR = "&separator&";
  public  static final String FILTER_PART_SEPARATOR = "-";

  private Map<String, FilterCriteria> filters;
  private int pageNumber;
  private int pageSize;
  private String currentLang;

  public Status(String status) {
    filters = new HashMap<>();
    fillFiltersData(status);
  }

  public FilterCriteria getFilterCriteria(String key) {
    return filters.getOrDefault(key, null);
  }

  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public String getCurrentLang() {
    return currentLang;
  }

  public void setCurrentLang(String locale) {
    if (locale != null) {
      this.currentLang = locale.split("-")[0];
    }
  }

  private void fillFiltersData(String status) {
    if (status.isEmpty()) {
      return;
    }

    String[] filtersParts = status.split(FILTER_VALUE_SEPARATOR);
    for (String filter : filtersParts) {
      FilterCriteria conditionFields = getFilterCondition(filter);
      filters.put(conditionFields.getKey(), conditionFields);
    }
  }

  private FilterCriteria getFilterCondition(String filter) {
    String[] filterParts = filter.split(FILTER_PART_SEPARATOR, 3);
    String key = filterParts[0];
    String operation = filterParts[1];
    String value = filterParts[2];
    return new FilterCriteria(key, operation, handleFilterSpecialCharacters(operation, value));
  }

  private String handleFilterSpecialCharacters(String operation, String filter) {
    if (operation.equalsIgnoreCase("contains")) {
      filter = filter.replace("_","\\_");
    }
    filter = filter.replace("semicolon",";");
    filter = filter.replace("backslash","\\");
    filter = filter.replace("forwardslash","/");

    return filter;
  }
}
