package com.ebs.dda.rest.masterdata.assetmasterdata;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.assetmasterdata.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonListContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.repositories.masterdata.assetmasterdata.CObAssetTypeRep;
import com.ebs.dda.repositories.masterdata.assetmasterdata.MObAssetGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/masterdataobjects/asset")
public class MObAssetQueryController extends BasicQueryController {

  @Autowired
  @Qualifier("assetMasterDataAllowedActionService")
  private AllowedActionService assetMasterDataAllowedActionService;

  @Autowired private CObAssetTypeRep assetTypeRep;

  @Autowired private MObAssetGeneralModelRep assetGeneralModelRep;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        assetMasterDataAllowedActionService.getHomeScreenAllowedActions(MObAsset.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IMObAsset.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IMObAssetGeneralModel.USER_CODE));

    Specification<MObAssetGeneralModel> querySpecification =
        getMObAssetSpecification(conditions, status);

    Page<MObAssetGeneralModel> assetsDataPage =
        assetGeneralModelRep.findAll(querySpecification, pageRequest);
    List<MObAssetGeneralModel> assetsData = assetsDataPage.getContent();
    return serializeViewAllResponse(assetsData, assetsDataPage.getTotalElements());
  }

  private Specification<MObAssetGeneralModel> getMObAssetSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(IMObAssetGeneralModel.PURCHASE_UNIT_NAME, conditions);
    JsonListContainsSpecification customerPurchaseUnitsAuthorizationSpecification =
        new JsonListContainsSpecification(
            IMObAssetGeneralModel.BUSINESS_UNIT_NAMES, businessUnitNames);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IMObAssetGeneralModel.USER_CODE);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringSpecification(status, IMObAssetGeneralModel.ASSET_TYPE_CODE);

    JsonFieldSpecification assetNameSpecification =
        getJsonContainsSpecification(status, IMObAssetGeneralModel.ASSET_NAME);

    JsonListContainsSpecification businessUnitNameSpecification =
        getCustomPurchaseUnitJsonListSpecification(
            IMObAssetGeneralModel.BUSINESS_UNIT_NAMES, status);

    SimpleStringFieldSpecification oldNumberSpecification =
        getSimpleStringSpecification(status, IMObAssetGeneralModel.OLD_NUMBER);

    Specification<MObAssetGeneralModel> querySpecification =
        Specification.where(customerPurchaseUnitsAuthorizationSpecification)
            .and(userCodeSpecification)
            .and(typeSpecification)
            .and(assetNameSpecification)
            .and(businessUnitNameSpecification)
            .and(oldNumberSpecification);
    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/assettypes")
  public @ResponseBody String readAssetTypes() {
    authorizationManager.authorizeAction(ICObAssetType.SYS_NAME, IActionsNames.READ_ALL);
    List<CObAssetType> types = assetTypeRep.findAllByOrderByIdDesc();
    return serializeRESTSuccessResponse(types);
  }
}
