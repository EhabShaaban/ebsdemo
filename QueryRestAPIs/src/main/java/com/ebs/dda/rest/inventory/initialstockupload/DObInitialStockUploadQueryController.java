package com.ebs.dda.rest.inventory.initialstockupload;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadGeneralModel;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUpload;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUploadGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.inventory.initialstockupload.DObInitialStockUploadGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/initialstockupload")
public class DObInitialStockUploadQueryController extends BasicQueryController {

  @Autowired DObInitialStockUploadGeneralModelRep initialStockUploadGeneralModelRep;

  @Autowired
  @Qualifier("initialStockUploadAllowedActionService")
  private AllowedActionService allowedActionService;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObInitialStockUpload.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObInitialStockUploadGeneralModel.USER_CODE));

    Specification querySpecification = getInitialStockUploadDataSpecification(conditions, status);
    Page <DObInitialStockUploadGeneralModel>  goodsIssuePage = initialStockUploadGeneralModelRep.findAll(querySpecification, pageRequest);
    List goodsIssues = goodsIssuePage.getContent();
    return serializeViewAllResponse(goodsIssues, goodsIssuePage.getTotalElements());
  }

  @GetMapping("/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        allowedActionService.getHomeScreenAllowedActions(IDObInitialStockUpload.SYS_NAME);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{initialStockUploadCode}")
  public @ResponseBody String readActions(@PathVariable String initialStockUploadCode)
      throws Exception {

    DObInitialStockUploadGeneralModel initialStockUploadGeneralModel =
        initialStockUploadGeneralModelRep.findOneByUserCode(initialStockUploadCode);
    if (initialStockUploadGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    Set<String> allowedActions =
        allowedActionService.getObjectScreenAllowedActions(initialStockUploadGeneralModel);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  private Specification getInitialStockUploadDataSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(
            IDObInitialStockUploadGeneralModel.PURCHASING_UNIT_NAME, conditions);
    SinglePurchaseUnitNamesConditionSpecification businessUnitAuthorizationSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            businessUnitNames, IDObInitialStockUploadGeneralModel.PURCHASING_UNIT_NAME_EN);

    Specification querySpecification = Specification.where(businessUnitAuthorizationSpecification);
    return querySpecification;
  }
}
