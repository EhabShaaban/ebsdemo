package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.plant.CObPlant;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouse;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 21, 2018 9:50:57 AM
 */
@RestController
@RequestMapping(value = "/services/enterprise/storehouses")
public class CObStorehouseQueryController extends BasicQueryController {

  @Autowired private AuthorizationManager authorizationManager;

  @Autowired private CObStorehouseGeneralModelRep storehouseGeneralModelRep;
  @Autowired private CObCompanyGeneralModelRep companyGeneralModelRep;
  @Autowired private CObPlantRep plantRep;

  @RequestMapping(method = RequestMethod.GET, value = "/byPlantCode/{plantCode}")
  public @ResponseBody String readStorehousesByPlant(@PathVariable String plantCode)
      throws Exception {
    applyAuthorizationForStorehouse();
    List<CObStorehouseGeneralModel> storehouses =
        storehouseGeneralModelRep.findByPlantCode(plantCode);
    return serializeRESTSuccessResponse(storehouses);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/byCompanyCode/{companyCode}")
  public @ResponseBody String readStorehousesByCompany(@PathVariable String companyCode) {

    authorizationManager.authorizeAction(ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL);

    CObCompanyGeneralModel company = companyGeneralModelRep.findOneByUserCode(companyCode);
    CObPlant plant = plantRep.findOneByCompanyId(company.getId());

    List<CObStorehouseGeneralModel> storehouses = new ArrayList<>();
    if (plant != null) {
      storehouses = storehouseGeneralModelRep.findByPlantCode(plant.getUserCode());
    }

    return serializeRESTSuccessResponse(storehouses);
  }

  private void applyAuthorizationForStorehouse() throws Exception {
    CObStorehouse storehouse = new CObStorehouse();
    checkIfReadPermissionIsAllowed(storehouse.getSysName(), IActionsNames.READ_ALL);
  }
}
