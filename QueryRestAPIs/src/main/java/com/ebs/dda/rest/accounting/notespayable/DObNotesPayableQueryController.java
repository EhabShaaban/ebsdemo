package com.ebs.dda.rest.accounting.notespayable;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesPayableGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesPayableGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesPayableGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/accounting/notespayable")
public class DObNotesPayableQueryController extends BasicQueryController {

  @Autowired private DObNotesPayableGeneralModelRep notesPayableGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_PAYABLE_SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObNotesPayableGeneralModel.USER_CODE));

    Specification<DObNotesPayableGeneralModel> querySpecification =
        getNotesPayableDataSpecification(conditions, status);

    Page<DObNotesPayableGeneralModel> notesPayablePage =
        notesPayableGeneralModelRep.findAll(querySpecification, pageRequest);

    List<DObNotesPayableGeneralModel> notesPayable = notesPayablePage.getContent();
    return serializeViewAllResponse(notesPayable, notesPayablePage.getTotalElements());
  }

  private Specification<DObNotesPayableGeneralModel> getNotesPayableDataSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(
            IDObMonetaryNotes.PURCHASE_UNIT_NAME_ATTR, conditions);

    SinglePurchaseUnitNamesConditionSpecification<DObNotesPayableGeneralModel>
        businessUnitAuthorizationSpecification =
            new SinglePurchaseUnitNamesConditionSpecification<>(
                businessUnitNames, IDObNotesPayableGeneralModel.PURCHASE_UNIT_NAME_EN);

    Specification<DObNotesPayableGeneralModel> querySpecification =
        Specification.where(businessUnitAuthorizationSpecification);
    return querySpecification;
  }
}
