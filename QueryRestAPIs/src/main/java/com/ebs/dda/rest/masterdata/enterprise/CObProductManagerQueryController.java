package com.ebs.dda.rest.masterdata.enterprise;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.productmanager.CObProductManager;
import com.ebs.dda.jpa.masterdata.productmanager.ICObProductManager;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/enterprise/productmanagers")
public class CObProductManagerQueryController extends BasicQueryController {

  @Autowired private CObProductManagerRep productManagerRep;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String readAllProductMangers() {
    authorizationManager.authorizeAction(ICObProductManager.SYS_NAME, IActionsNames.READ_ALL);
    List<CObProductManager> productManager = productManagerRep.findAllByOrderByIdDesc();
    return serializeRESTSuccessResponse(productManager);
  }
}
