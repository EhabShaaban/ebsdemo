package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dda.repositories.accounting.salesinvoice.ItemsLastSalesPriceGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.sales.dbo.jpa.entities.GeneralModels.ItemsLastSalesPriceGeneralModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping(value = "/services/accounting/itemslastsalesprice")
public class ItemsLastSalesPriceQueryController extends BasicQueryController {
  @Autowired private ItemsLastSalesPriceGeneralModelRep itemsLastSalesPriceGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{itemCode}/{uomCode}/{purchaseUnitCode}/{customerCode}")
  public @ResponseBody String readGeneralData(
      @PathVariable String itemCode,
      @PathVariable String uomCode,
      @PathVariable String purchaseUnitCode,
      @PathVariable String customerCode) {
    List<ItemsLastSalesPriceGeneralModel> itemsLastSalesPriceGeneralModels =
        itemsLastSalesPriceGeneralModelRep
            .findByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCode(
                itemCode, uomCode, purchaseUnitCode, customerCode);
    ItemsLastSalesPriceGeneralModel itemsLastSalesPrice =
        Collections.max(
            itemsLastSalesPriceGeneralModels,
            Comparator.comparing(ItemsLastSalesPriceGeneralModel::getSalesInvoicePostingDate));
    return serializeRESTSuccessResponse(itemsLastSalesPrice);
  }
}
