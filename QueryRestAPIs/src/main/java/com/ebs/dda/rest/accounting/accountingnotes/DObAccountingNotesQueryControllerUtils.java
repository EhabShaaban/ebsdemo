package com.ebs.dda.rest.accounting.accountingnotes;

import com.ebs.dda.jpa.accounting.accountingnotes.CreditNoteTypeEnum;
import com.ebs.dda.jpa.accounting.accountingnotes.DebitNoteTypeEnum;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DObAccountingNotesQueryControllerUtils {

  public DObAccountingNotesQueryControllerUtils() {}

  public List getAccountingNoteTypesBasedOnNoteTypeInput(String noteType) {
    if (noteType.equals(AccountingEntry.DEBIT.name())) {
      return Arrays.asList(DebitNoteTypeEnum.values());
    } else if (noteType.equals(AccountingEntry.CREDIT.name())) {
      return Arrays.asList(CreditNoteTypeEnum.values());
    }
    return new ArrayList<>();
  }
}
