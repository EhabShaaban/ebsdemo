package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class DObSalesOrderQueryControllerUtils {

  public static final String LOGGED_IN_USER = "LoggedInUser";
  public static final String UNRESTRICTED_STOCK_TYPE = "UNRESTRICTED_USE";

  public DObSalesOrderQueryControllerUtils() {}

  public static void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }
}
