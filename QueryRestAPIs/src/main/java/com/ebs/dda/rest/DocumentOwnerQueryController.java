package com.ebs.dda.rest;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.IBusinessObjectNames;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUpload;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCost;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/services/documentowners")
public class DocumentOwnerQueryController extends BasicQueryController {
    @Autowired
    private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

    @RequestMapping(method = RequestMethod.GET, value = "/{businessObjectName}")
    public @ResponseBody
    String readBusinessObjectDocumentOwners(
            @PathVariable String businessObjectName) {
        authorizationManager.authorizeAction(IDocumentOwnerGeneralModel.SYS_NAME,
                IActionsNames.READ_ALL);

        String businessObjectSysName = getBusinessObjectSysName(businessObjectName);
        List<DocumentOwnerGeneralModel> documentOwners = documentOwnerGeneralModelRep
                .findAllByObjectNameOrderByUserNameAsc(businessObjectSysName);

        return serializeRESTSuccessResponse(documentOwners);
    }

    private String getBusinessObjectSysName(String businessObjectName) {
        switch (businessObjectName) {
            case IBusinessObjectNames.PURCHASE_ORDER:
                return IDObPurchaseOrder.SYS_NAME;
            case IBusinessObjectNames.ACCOUNTING_NOTE:
                return IDObAccountingNote.SYS_NAME;
            case IBusinessObjectNames.SALES_RETURN:
                return IDObSalesReturnOrder.SYS_NAME;
            case IBusinessObjectNames.SALES_INVOICE:
                return IDObSalesInvoice.SYS_NAME;
            case IBusinessObjectNames.SETTLEMENT:
                return IDObSettlement.SYS_NAME;
            case IBusinessObjectNames.COLLECTION:
                return IDObCollectionGeneralModel.SYS_NAME;
            case IBusinessObjectNames.INITIAL_BALANCE_UPLOAD:
                return IDObInitialBalanceUpload.SYS_NAME;
            case IBusinessObjectNames.NOTES_RECEIVABLE:
                return IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME;
            case IBusinessObjectNames.PAYMENT_REQUEST:
                return IDObPaymentRequest.SYS_NAME;
            case IBusinessObjectNames.LANDED_COST:
                return IDObLandedCost.SYS_NAME;
            case IBusinessObjectNames.VENDOR_INVOICE:
                return IDObVendorInvoice.SYS_NAME;
            default:
                return null;
        }
    }
}