package com.ebs.dda.rest.accounting.accountingnotes;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.accountingnote.statemachines.DObAccountingNoteStateMachine;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/services/accounting/accountingnotes")
public class DObAccountingNotesQueryController extends BasicQueryController
    implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep;
  @Autowired private DocumentOwnerGeneralModelRep documentOwnerRep;

  private DObAccountingNotesQueryControllerUtils accountingNotesQueryControllerUtils;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObAccountingNoteGeneralModel.USER_CODE));

    Specification<DObAccountingNoteGeneralModel> querySpecification =
        getAccountingNoteDataSpecification(conditions, status);

    Page<DObAccountingNoteGeneralModel> accountingNotesPage =
        accountingNoteGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObAccountingNoteGeneralModel> accountingNotes = accountingNotesPage.getContent();
    return serializeViewAllResponse(accountingNotes, accountingNotesPage.getTotalElements());
  }

  private Specification<DObAccountingNoteGeneralModel> getAccountingNoteDataSpecification(
      Set<String> conditions, Status status) {
    List<String> businessUnitNames =
        getConditionAttributeValue(IDObAccountingNoteGeneralModel.PURCHASE_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification<DObAccountingNoteGeneralModel>
        businessUnitAuthorizationSpecification =
            new SinglePurchaseUnitNamesConditionSpecification<>(
                businessUnitNames, IDObAccountingNoteGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObAccountingNoteGeneralModel.USER_CODE);

    SimpleStringFieldSpecification accountingNoteTypeSpecification =
        getSimpleStringSpecification(status, IDObAccountingNoteGeneralModel.OBJECT_TYPE_CODE);

    JsonFieldSpecification businessPartnerTypeSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObAccountingNoteGeneralModel.BUSINESS_PARTNER,
            IDObAccountingNoteGeneralModel.BP_TYPE);

    JsonFieldSpecification businessPartnerNameSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObAccountingNoteGeneralModel.BUSINESS_PARTNER,
            IDObAccountingNoteGeneralModel.BP_NAME);

    SimpleStringFieldSpecification businessPartnerCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObAccountingNoteGeneralModel.BUSINESS_PARTNER,
            IDObAccountingNoteGeneralModel.BP_CODE);

    SimpleStringFieldSpecification documentOwnerNameSpecification =
        getSimpleStringSpecification(status, IDObAccountingNoteGeneralModel.DOCUMENT_OWNER_NAME);

    SimpleStringFieldSpecification referenceDocumentCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObAccountingNoteGeneralModel.REFERENCE_DOCUMENT,
            IDObAccountingNoteGeneralModel.RD_CODE);

    JsonFieldSpecification referenceDocumentTypeSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObAccountingNoteGeneralModel.REFERENCE_DOCUMENT,
            IDObAccountingNoteGeneralModel.RD_Type);

    DoubleFieldSpecification amountSpecification =
        getDoubleFieldSpecification(status, IDObAccountingNoteGeneralModel.AMOUNT);

    SimpleStringFieldSpecification businessUnitCodeSpecification =
        getSimpleStringSpecification(status, IDObAccountingNoteGeneralModel.PURCHASE_UNIT_CODE);

    CurrentStatesFieldSpecification stateSpecification =
        getCurrentStatesFieldSpecification(status, IDObAccountingNoteGeneralModel.CURRENT_STATES);

    Specification<DObAccountingNoteGeneralModel> querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(userCodeSpecification)
            .and(accountingNoteTypeSpecification)
            .and(
                businessPartnerTypeSpecification
                    .or(businessPartnerNameSpecification)
                    .or(businessPartnerCodeSpecification))
            .and(documentOwnerNameSpecification)
            .and(referenceDocumentCodeSpecification.or(referenceDocumentTypeSpecification))
            .and(amountSpecification)
            .and(businessUnitCodeSpecification)
            .and(stateSpecification);
    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{businessUnitCode}/getActiveDebitNotes")
  public @ResponseBody String readPostedDebitNotes(@PathVariable String businessUnitCode) {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();

    List<DObAccountingNoteGeneralModel> accountingNoteGeneralModelList = new ArrayList<>();

    String DEBIT_NOTE = "DEBIT_NOTE";
    accountingNoteGeneralModelList =
        accountingNoteGeneralModelRep
            .findAllByPurchaseUnitCodeAndObjectTypeCodeLikeAndCurrentStatesLikeAndRemainingGreaterThanOrderByUserCodeDesc(
                businessUnitCode,
                "%" + DEBIT_NOTE + "%",
                "%" + DObAccountingNoteStateMachine.POSTED_STATE + "%",
                (double) 0);

    return serializeRESTSuccessResponse(accountingNoteGeneralModelList);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/notetypes")
  public @ResponseBody String readNoteTypes() {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(Arrays.asList(AccountingEntry.values()));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types/{noteType}")
  public @ResponseBody String readTypes(@PathVariable String noteType) {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);
    return serializeRESTSuccessResponse(
        accountingNotesQueryControllerUtils.getAccountingNoteTypesBasedOnNoteTypeInput(noteType));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/creditNotes")
  public @ResponseBody String readPostedCreditNotes() {
    authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObPaymentRequestGeneralModel.PURCHASING_UNIT_NAME, conditions);

    ReadCreditNotesSpecification creditNotesSpecification = new ReadCreditNotesSpecification();

    Specification<DObAccountingNoteGeneralModel> authorizedPurchaseUnit =
        creditNotesSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObAccountingNoteGeneralModel> creditNotes =
        accountingNoteGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(creditNotesSpecification.creditNoteInAllowedState())
                .and(creditNotesSpecification.creditNotesAllowedRemaining())
                .and(creditNotesSpecification.accountingNoteIsCredit()));

    creditNotes.sort(Comparator.comparing(DObAccountingNoteGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(creditNotes, creditNotes.size());
  }

  @Override
  public void afterPropertiesSet() {
    accountingNotesQueryControllerUtils = new DObAccountingNotesQueryControllerUtils();
  }
}
