package com.ebs.dda.rest.inventory.stocktransformation;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationReasonRep;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationTypeRep;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/stocktransformation")
public class DObStockTransformationQueryController extends BasicQueryController {

    @Autowired
    CObStockTransformationReasonRep stockTransformationReasonRep;
    @Autowired
    CObStockTransformationTypeRep stockTransformationTypeRep;
    @Autowired
    DObStockTransformationGeneralModelRep dObStockTransformationGeneralModelRep;

    @Autowired
    @Qualifier("stockTransformationAllowedActionService")
    private AllowedActionService stockTransformationAllowedActionService;

    @RequestMapping(
            method = RequestMethod.GET,
            value =
                    "/{stockTransformationTypeCode}/stocktransformationreasons/{fromStockType}/{toStockType}")
    public @ResponseBody
    String getStockTransformationReasons(
            @PathVariable String stockTransformationTypeCode,
            @PathVariable String fromStockType,
            @PathVariable String toStockType)
            throws Exception {
        try {
            checkIfReadPermissionIsAllowed(
                    ICObStockTransformationReason.SYS_NAME, READ_ALL);
            CObStockTransformationType stockTransformationType =
                    stockTransformationTypeRep.findOneByUserCode(stockTransformationTypeCode);
            List<CObStockTransformationReason> stockTransformationReasons =
                    stockTransformationReasonRep.findByTypeIdAndFromStockTypeAndToStockType(
                            stockTransformationType.getId(),
                            fromStockType,
                            toStockType,
                            Sort.by(Sort.Direction.DESC, IDObGoodsReceiptGeneralModel.USER_CODE));
            return serializeRESTSuccessResponse(stockTransformationReasons);
        } catch (ResourceAccessException ex) {
            AuthorizationException authorizationException = new AuthorizationException(ex);
            throw authorizationException;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stocktransformationtypes")
    public @ResponseBody
    String getStockTransformationTypes() throws Exception {
        try {
            checkIfReadPermissionIsAllowed(
                    ICObStockTransformationType.SYS_NAME, READ_ALL);
            List<CObStockTransformationType> stockTransformationTypes =
                    stockTransformationTypeRep.findAll(
                            Sort.by(Sort.Direction.DESC, IDObGoodsReceiptGeneralModel.USER_CODE));
            return serializeRESTSuccessResponse(stockTransformationTypes);
        } catch (ResourceAccessException ex) {
            AuthorizationException authorizationException = new AuthorizationException(ex);
            throw authorizationException;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
    public @ResponseBody
    String readHomeScreenActions() throws Exception {
        Set<String> allowedActions =
                stockTransformationAllowedActionService.getHomeScreenAllowedActions(
                        DObStockTransformation.class);
        if (allowedActions.size() == 0) {
            throw new ResourceAccessException();
        }
        return serializeRESTSuccessResponse(allowedActions);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
    public @ResponseBody
    String readAll(
            @PathVariable int pageNum,
            @PathVariable int rowsNum,
            @PathVariable String filter,
            HttpServletRequest request) {

        authorizationManager.authorizeAction(IDObStockTransformationGeneralModel.SYS_NAME, READ_ALL);
        Set<String> conditions = authorizationManager.getPermissionConditions();

        Status status = constructStatus(pageNum, rowsNum, filter, request);
        PageRequest pageRequest =
                PageRequest.of(
                        pageNum,
                        rowsNum,
                        Sort.by(
                                Sort.Direction.DESC,
                                IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_CODE));

        Specification querySpecification = getStockTransformationDataSpecification(status, conditions);

        Page <DObStockTransformationGeneralModel>  stockTransformationPage = dObStockTransformationGeneralModelRep.findAll(querySpecification, pageRequest);
        List<DObStockTransformationGeneralModel> stockTransformations = stockTransformationPage.getContent();

        return serializeViewAllResponse(stockTransformations, stockTransformationPage.getTotalElements());
    }

    private Specification getStockTransformationDataSpecification(
            Status status, Set<String> conditions) {

        List<String> purchaseUnitNames =
                getConditionAttributeValue(
                        IDObStockTransformationGeneralModel.PURCHASEUNIT_NAME, conditions);
        SinglePurchaseUnitNamesConditionSpecification purchaseUnitAuthorizationSpecification =
                new SinglePurchaseUnitNamesConditionSpecification(
                        purchaseUnitNames, IDObStockTransformationGeneralModel.PURCHASEUNIT_NAME_EN);

        SimpleStringFieldSpecification userCodeSpecification =
                getSimpleStringSpecification(
                        status, IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_CODE);

        SimpleStringFieldSpecification stockTransformationTypeSpecification =
                getSimpleStringSpecification(
                        status, IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_TYPE_CODE);

        SimpleStringFieldSpecification refStoreTransactionSpecification =
                getSimpleStringSpecification(
                        status, IDObStockTransformationGeneralModel.REF_STORE_TRANSACTION_CODE);

        SimpleStringFieldSpecification newStoreTransactionSpecification =
                getSimpleStringSpecification(
                        status, IDObStockTransformationGeneralModel.NEW_STORE_TRANSACTION_CODE);

        SimpleStringFieldSpecification stockTransformationStateSpecification =
                getSimpleStringSpecification(status, IDObStockTransformationGeneralModel.CURRENT_STATES);

        SimpleStringFieldSpecification purchaseUnitCodeSpecification =
                getSimpleStringSpecification(status, IDObStockTransformationGeneralModel.PURCHASEUNIT_CODE);

        Specification querySpecification =
                Specification.where(purchaseUnitAuthorizationSpecification)
                        .and(userCodeSpecification)
                        .and(stockTransformationTypeSpecification)
                        .and(refStoreTransactionSpecification)
                        .and(newStoreTransactionSpecification)
                        .and(stockTransformationStateSpecification)
                        .and(purchaseUnitCodeSpecification);

        return querySpecification;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/authorizedactions/{stockTransformationCode}")
    public @ResponseBody
    String readObjectScreenActions(@PathVariable String stockTransformationCode)
            throws Exception {
        DObStockTransformationGeneralModel dObStockTransformationGeneralModel =
                dObStockTransformationGeneralModelRep.findOneByUserCode(stockTransformationCode);

        if (dObStockTransformationGeneralModel == null) {
            throw new InstanceNotExistException();
        }
        Set<String> allowedActions =
                stockTransformationAllowedActionService.getObjectScreenAllowedActions(
                        dObStockTransformationGeneralModel);

        if (allowedActions.size() == 0) {
            throw new ResourceAccessException();
        }

        return serializeRESTSuccessResponse(allowedActions);
    }
}
