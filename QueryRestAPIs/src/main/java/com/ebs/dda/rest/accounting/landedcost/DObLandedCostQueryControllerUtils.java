package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;

public class DObLandedCostQueryControllerUtils {

  public void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }
}
