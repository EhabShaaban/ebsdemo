package com.ebs.dda.rest.fiscalyear;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.ICObFiscalYear;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.rest.BasicQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/services/fiscalyear")
public class CObFiscalYearQueryController extends BasicQueryController {
  @Autowired private CObFiscalYearRep fiscalYearRep;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String readAllFiscalYears() {
    authorizationManager.authorizeAction(ICObFiscalYear.SYS_NAME, IActionsNames.READ_ALL);
    List<CObFiscalYear> fiscalYears = fiscalYearRep.findAll(Sort.by(Sort.Direction.DESC, ICObFiscalYear.CODE));
    return serializeRESTSuccessResponse(fiscalYears);
  }
  @RequestMapping(method = RequestMethod.GET, value = "/activefiscalyear")
  public @ResponseBody String readActiveFiscalYear() {
    authorizationManager.authorizeAction(ICObFiscalYear.SYS_NAME, IActionsNames.READ_ALL);
    //TODO handle case when multiple fiscal years available by removing this controller and using v2
    CObFiscalYear activeFiscalYear = fiscalYearRep.findByCurrentStatesLike("%Active%").get(0);
    return serializeRESTSuccessResponse(activeFiscalYear);
  }
  @RequestMapping(method = RequestMethod.GET, value = "/activefiscalyear/v2")
  public @ResponseBody String readActiveFiscalYearList() {
    authorizationManager.authorizeAction(ICObFiscalYear.SYS_NAME, IActionsNames.READ_ALL);
    List<CObFiscalYear> activeFiscalYears = fiscalYearRep.findByCurrentStatesLike("%Active%");
    return serializeRESTSuccessResponse(activeFiscalYears);
  }
}
