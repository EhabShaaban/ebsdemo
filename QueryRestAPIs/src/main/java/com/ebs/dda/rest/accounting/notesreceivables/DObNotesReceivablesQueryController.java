package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesActionNames;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/accounting/notesreceivables")
public class DObNotesReceivablesQueryController extends BasicQueryController
    implements InitializingBean {

  @Autowired private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;

  @Autowired
  private IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;

  @Autowired
  @Qualifier("notesReceivablesAllowedActionService")
  private AllowedActionService notesReceivablesAllowedActionService;

  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  @Autowired private IObMonetaryNotesDetailsGeneralModelRep notesDetailsGeneralModelRep;

  @Autowired
  private IObNotesReceivableAccountingDetailsGeneralModelRep
      notesReceivableAccountingDetailsGeneralModelRep;

  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  @Autowired
  private IObAccountingDocumentActivationDetailsGeneralModelRep activationDetailsGeneralModelRep;

  private DObNotesReceivablesQueryControllerUtils utils;

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        notesReceivablesAllowedActionService.getHomeScreenAllowedActions(
            DObNotesReceivablesGeneralModel.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{notesReceivableCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String notesReceivableCode)
      throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    Set<String> allowedActions =
        notesReceivablesAllowedActionService.getObjectScreenAllowedActions(
            notesReceivablesGeneralModel);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/notesreceivablesRefDocument/{businessUnit}/{customer}")
  public @ResponseBody String getNotesReceivablesRefDocument(
      @PathVariable String businessUnit, @PathVariable String customer) {
    List<DObSalesInvoiceGeneralModel> salesInvoices =
        salesInvoiceGeneralModelRep
            .findAllByPurchaseUnitCodeAndCustomerCodeAndCurrentStatesLikeAndCurrentStatesLikeOrderByUserCodeDesc(
                businessUnit,
                customer,
                '%' + DObSalesInvoiceStateMachine.OPENED + '%',
                '%' + DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE + '%');
    return serializeRESTSuccessResponse(salesInvoices);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObNotesReceivablesGeneralModel.USER_CODE));

    Specification<DObNotesReceivablesGeneralModel> querySpecification =
        getNotesReceivablesDataSpecification(conditions, status);

    Page<DObNotesReceivablesGeneralModel> notesReceivablesPage =
        notesReceivablesGeneralModelRep.findAll(querySpecification, pageRequest);

    List<DObNotesReceivablesGeneralModel> notesReceivables = notesReceivablesPage.getContent();
    return serializeViewAllResponse(notesReceivables, notesReceivablesPage.getTotalElements());
  }

  private Specification<DObNotesReceivablesGeneralModel> getNotesReceivablesDataSpecification(
      Set<String> conditions, Status status) {

    List<String> businessUnitNames =
        getConditionAttributeValue(
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR, conditions);

    SinglePurchaseUnitNamesConditionSpecification<DObNotesReceivablesGeneralModel>
        businessUnitAuthorizationSpecification =
            new SinglePurchaseUnitNamesConditionSpecification<>(
                businessUnitNames, IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObNotesReceivablesGeneralModel.USER_CODE);

    SimpleStringFieldSpecification typeSpecification =
        getSimpleStringSpecification(status, IDObNotesReceivablesGeneralModel.OBJECT_TYPE_CODE);

    SimpleStringFieldSpecification businessPartnerCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER,
            IDObNotesReceivablesGeneralModel.CODE);

    JsonFieldSpecification businessPartnerNameSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER,
            IDObNotesReceivablesGeneralModel.NAME);

    SimpleStringFieldSpecification depotCodeSpecification =
        getCustomStringContainsSpecification(
            status, IDObNotesReceivablesGeneralModel.DEPOT, IDObNotesReceivablesGeneralModel.CODE);

    JsonFieldSpecification depotNameSpecification =
        getCustomJsonContainsSpecification(
            status, IDObNotesReceivablesGeneralModel.DEPOT, IDObNotesReceivablesGeneralModel.NAME);

    DateContainsSpecification dateSpecification =
        getDateContainsSpecification(status, IDObNotesReceivablesGeneralModel.DUE_DATE);

    DoubleFieldSpecification amountSpecification =
        getDoubleFieldSpecification(status, IDObNotesReceivablesGeneralModel.AMOUNT);

    DoubleFieldSpecification remainingSpecification =
        getDoubleFieldSpecification(status, IDObNotesReceivablesGeneralModel.REMAINING);

    SimpleStringFieldSpecification businessUnitCodeSpecification =
        getSimpleStringSpecification(status, IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE);

    CurrentStatesFieldSpecification stateSpecification =
        getCurrentStatesFieldSpecification(status, IDObNotesReceivablesGeneralModel.CURRENT_STATES);

    Specification<DObNotesReceivablesGeneralModel> querySpecification =
        Specification.where(businessUnitAuthorizationSpecification)
            .and(userCodeSpecification)
            .and(typeSpecification)
            .and(businessPartnerCodeSpecification.or(businessPartnerNameSpecification))
            .and(depotCodeSpecification.or(depotNameSpecification))
            .and(amountSpecification)
            .and(remainingSpecification)
            .and(dateSpecification)
            .and(businessUnitCodeSpecification)
            .and(stateSpecification);
    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String notesReceivableCode)
      throws Exception {

    DObNotesReceivablesGeneralModel generalData =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);
    authorizationManager.authorizeActionOnObject(generalData, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(generalData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/types")
  public @ResponseBody String readNotesReceivableTypes() {
    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(Arrays.asList(NotesReceivableTypeEnum.values()));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/forms")
  public @ResponseBody String readMonetaryNoteForms() {
    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.READ_ALL);

    return serializeRESTSuccessResponse(Arrays.asList(MonetaryNoteFormEnum.values()));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String notesReceivableCode)
      throws Exception {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);
    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.READ_COMPANY_DATA);

    return serializeRESTSuccessResponse(notesReceivablesGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/notesdetails")
  public @ResponseBody String readNotesDetails(@PathVariable String notesReceivableCode)
      throws Exception {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel,
        IDObNotesReceivablesActionNames.READ_NOTES_RECEIVABLE_DETAILS);

    IObMonetaryNotesDetailsGeneralModel notesDetailsGeneralModel =
        notesDetailsGeneralModelRep.findOneByRefInstanceId(notesReceivablesGeneralModel.getId());

    return serializeRESTSuccessResponse(notesDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/accountingdetails")
  public @ResponseBody String readAccountingDetailsData(@PathVariable String notesReceivableCode)
      throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.READ_ACCOUNTING_DETAILS);

    List<IObNotesReceivableAccountingDetailsGeneralModel>
        notesReceivableAccountingDetailsGeneralModelList =
            notesReceivableAccountingDetailsGeneralModelRep
                .findByDocumentCodeOrderByAccountingEntryDesc(notesReceivableCode);

    return serializeRESTSuccessResponse(notesReceivableAccountingDetailsGeneralModelList);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/referencedocuments")
  public @ResponseBody String ReadReferenceDocuments(@PathVariable String notesReceivableCode)
      throws Exception {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.READ_REFERENCE_DOCUMENTS);

    JsonObject notesReceivableReferenceDocumentsSectionData = new JsonObject();
    utils.constructNotesReceivableReferenceDocumentsSectionData(
        notesReceivableCode, notesReceivableReferenceDocumentsSectionData);
    return serializeRESTSuccessResponse(notesReceivableReferenceDocumentsSectionData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/referencedocumenttypes")
  public @ResponseBody String readReferenceDocumentTypes() {
    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME,
        IDObNotesReceivablesActionNames.ADD_REFERENCE_DOCUMENT);

    return serializeRESTSuccessResponse(
        Arrays.asList(NotesReceivableReferenceDocumentsTypeEnum.values()));
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/salesorders")
  public @ResponseBody String readReferenceDocumentSalesOrders(
      @PathVariable String notesReceivableCode) {

    authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL);

    List<DObSalesOrderGeneralModel> validSalesOrders =
        utils.getValidSalesOrders(notesReceivableCode);

    utils.constructSalesOrderList(notesReceivableCode, validSalesOrders);

    return serializeViewAllResponse(validSalesOrders, validSalesOrders.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/notesreceivables")
  public @ResponseBody String readReferenceDocumentNotesReceivables(
      @PathVariable String notesReceivableCode) {

    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.READ_ALL);

    List<DObNotesReceivablesGeneralModel> validNotesReceivables =
        utils.getValidNotesReceivables(notesReceivableCode);
    utils.constructNotesReceivableList(notesReceivableCode, validNotesReceivables);

    return serializeViewAllResponse(validNotesReceivables, validNotesReceivables.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/salesinvoices")
  public @ResponseBody String readReferenceDocumentSalesInvoices(
      @PathVariable String notesReceivableCode) {

    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL);

    List<DObSalesInvoiceGeneralModel> validsalesInvoices =
        utils.getValidsalesInvoices(notesReceivableCode);
    utils.constructsalesInvoiceList(notesReceivableCode, validsalesInvoices);
    return serializeViewAllResponse(validsalesInvoices, validsalesInvoices.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/postingdetails")
  public @ResponseBody String readActivationDetails(@PathVariable String notesReceivableCode)
      throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.READ_ACTIVATION_DETAILS);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetailsGeneralModel =
        activationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            notesReceivableCode, IDocumentTypes.NOTES_RECEIVABLE_AS_COLLECTION);
    return serializeRESTSuccessResponse(activationDetailsGeneralModel);
  }

  @Override
  public void afterPropertiesSet() {
    utils = new DObNotesReceivablesQueryControllerUtils(authorizationManager);
    utils.setIObNotesReceivablesReferenceDocumentGeneralModelRep(
        notesReceivablesReferenceDocumentGeneralModelRep);
    utils.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    utils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    utils.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
  }
}
