package com.ebs.dda.rest.accounting.journalentry;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.DateContainsSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.accounting.journalentry.DObJournalEntryGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/accounting/journalentry")
public class DObJournalEntryQueryController extends BasicQueryController {

  @Autowired private DObJournalEntryGeneralModelRep journalEntryGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObJournalEntry.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum, rowsNum, Sort.by(Sort.Direction.DESC, IDObJournalEntryGeneralModel.CODE));
    Specification querySpecification = getJournalEntryDataSpecification(status, conditions);

    Page<DObJournalEntryGeneralModel> journalEntriesPage =
        journalEntryGeneralModelRep.findAll(querySpecification, pageRequest);
    List<DObJournalEntryGeneralModel> journalEntries = journalEntriesPage.getContent();

    return serializeViewAllResponse(journalEntries, journalEntriesPage.getTotalElements());
  }

  private Specification getJournalEntryDataSpecification(Status status, Set<String> conditions) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObJournalEntryGeneralModel.PURCHASE_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, IDObJournalEntryGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObJournalEntryGeneralModel.CODE);

    DateContainsSpecification creationDateSpecification =
        getDateContainsSpecification(status, IDObJournalEntryGeneralModel.CREATION_DATE);
    SimpleStringFieldSpecification createdBySpecification =
        getSimpleStringSpecification(status, IDObJournalEntryGeneralModel.CREATED_BY);
    DateContainsSpecification dueDateSpecification =
        getDateContainsSpecification(status, IDObJournalEntryGeneralModel.DUE_DATE);
    SimpleStringFieldSpecification referenceDocumentCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObJournalEntryGeneralModel.DOCUMENT_REFERENCE,
            IDObJournalEntryGeneralModel.DOCUMENT_CODE);
    SimpleStringFieldSpecification referenceDocumentTypeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObJournalEntryGeneralModel.DOCUMENT_REFERENCE,
            IDObJournalEntryGeneralModel.DOCUMENT_TYPE);
    JsonFieldSpecification companySpecification =
        getJsonContainsSpecification(status, IDObJournalEntryGeneralModel.COMPANY_NAME);
    JsonFieldSpecification businessUnitSpecification =
        getJsonContainsSpecification(status, IDObJournalEntryGeneralModel.PURCHASE_UNIT_NAME);
    SimpleStringFieldSpecification fiscalPeriodSpecification =
        getSimpleStringSpecification(status, IDObJournalEntryGeneralModel.FISCAL_PERIOD);

    Specification querySpecification =
        Specification.where(purchaseUnitConditionSpecification)
            .and(userCodeSpecification)
            .and(creationDateSpecification)
            .and(createdBySpecification)
            .and(dueDateSpecification)
            .and(referenceDocumentCodeSpecification.or(referenceDocumentTypeSpecification))
            .and(companySpecification)
            .and(businessUnitSpecification)
            .and(fiscalPeriodSpecification);
    return querySpecification;
  }
}
