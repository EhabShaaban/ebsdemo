package com.ebs.dda.rest.inventory.stockAvaliability;

import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.ICObStockAvailabilitiyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.BigDecimalFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.JsonFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SimpleStringFieldSpecification;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.SinglePurchaseUnitNamesConditionSpecification;
import com.ebs.dda.repositories.inventory.stockavailability.DObStockAvailabilityGeneralModelRep;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

import static com.ebs.dac.foundation.realization.statemachine.IActionsNames.READ_ALL;

@RestController
@RequestMapping(value = "/services/stockavailability")
public class StockAvailabilityQueryController extends BasicQueryController {

  @Autowired private DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(ICObStockAvailabilitiyGeneralModel.SYS_NAME, READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(
                Sort.Direction.DESC,
                ICObStockAvailabilitiyGeneralModel.ITEM_CODE,
                ICObStockAvailabilitiyGeneralModel.UOM_CODE,
                ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_CODE,
                ICObStockAvailabilitiyGeneralModel.STOREHOUSE_CODE,
                ICObStockAvailabilitiyGeneralModel.COMPANY_CODE,
                ICObStockAvailabilitiyGeneralModel.PLANT_CODE,
                ICObStockAvailabilitiyGeneralModel.STOCK_TYPE));

    Specification querySpecification = getStoreTransactionDataSpecification(status, conditions);

    Page<CObStockAvailabilitiesGeneralModel> stockAvailabilityPage =
        stockAvailabilityGeneralModelRep.findAll(querySpecification, pageRequest);
    List<CObStockAvailabilitiesGeneralModel> stockAvailabilities =
        stockAvailabilityPage.getContent();

    return serializeViewAllResponse(stockAvailabilities, stockAvailabilityPage.getTotalElements());
  }

  private Specification getStoreTransactionDataSpecification(
      Status status, Set<String> conditions) {

    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification purchaseUnitConditionSpecification =
        new SinglePurchaseUnitNamesConditionSpecification(
            purchaseUnitNames, ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_NAME_EN);

    SimpleStringFieldSpecification itemCodeSpecification =
        getCustomStringContainsSpecification(
            status, ICObStockAvailabilitiyGeneralModel.ITEM, "Code");
    JsonFieldSpecification itemNameSpecification =
        getCustomJsonContainsSpecification(status, ICObStockAvailabilitiyGeneralModel.ITEM, "Name");

    JsonFieldSpecification unitOfMeasureNameSpecification =
        getJsonContainsSpecification(status, ICObStockAvailabilitiyGeneralModel.UOM_NAME);

    BigDecimalFieldSpecification totalQuantitySpecification =
        getBigDecimalFieldSpecification(
            status, ICObStockAvailabilitiyGeneralModel.AVAILABLE_QUANTITY);

    JsonFieldSpecification storehouseNameSpecification =
        getJsonContainsSpecification(status, ICObStockAvailabilitiyGeneralModel.STOREHOUSE_NAME);

    SimpleStringFieldSpecification purchaseUnitNameSpecification =
        getSimpleStringSpecification(status, ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_CODE);

    SimpleStringFieldSpecification companyCodeSpecification =
        getSimpleStringSpecification(status, ICObStockAvailabilitiyGeneralModel.COMPANY_CODE);

    JsonFieldSpecification plantNameSpecification =
        getJsonContainsSpecification(status, ICObStockAvailabilitiyGeneralModel.PLANT_NAME);
    SimpleStringFieldSpecification stockTypeCodeSpecification =
        getSimpleStringSpecification(status, ICObStockAvailabilitiyGeneralModel.STOCK_TYPE);

    Specification querySpecification =
        Specification.where(totalQuantitySpecification)
            .and(purchaseUnitConditionSpecification)
            .and(companyCodeSpecification)
            .and(plantNameSpecification)
            .and(storehouseNameSpecification)
            .and(purchaseUnitNameSpecification)
            .and(stockTypeCodeSpecification)
            .and(itemCodeSpecification.or(itemNameSpecification))
            .and(unitOfMeasureNameSpecification);
    return querySpecification;
  }
}
