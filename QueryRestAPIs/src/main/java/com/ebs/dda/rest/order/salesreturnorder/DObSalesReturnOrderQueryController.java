package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dac.security.services.DObSalesReturnOrderAuthorizationManager;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.order.salesreturnorder.*;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.specifications.*;
import com.ebs.dda.repositories.order.salesreturnorder.*;
import com.ebs.dda.rest.BasicQueryController;
import com.ebs.dda.rest.utils.Status;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/services/order/salesreturnorder")
public class DObSalesReturnOrderQueryController extends BasicQueryController {

  @Autowired private DObSalesReturnOrderGeneralModelRep dObSalesReturnOrderGeneralModelRep;
  @Autowired private IObSalesReturnOrderCompanyDataGeneralModelRep companyDataGeneralModelRep;
  @Autowired private IObSalesReturnOrderDetailsGeneralModelRep returnDetailsGeneralModelRep;
  @Autowired private IObSalesReturnOrderItemGeneralModelRep itemGeneralModelRep;
  @Autowired private IObSalesReturnOrderTaxGeneralModelRep taxGeneralModelRep;
  @Autowired private CObSalesReturnOrderTypeRep salesReturnOrderTypeRep;
  @Autowired private DObSalesReturnOrderAuthorizationManager salesReturnOrderAuthorizationManager;

  @Autowired
  @Qualifier("salesReturnOrderAllowedActionService")
  private AllowedActionService salesReturnOrderAllowedActionService;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/page={pageNum}/rows={rowsNum}/filters={filter}")
  public @ResponseBody String readAll(
      @PathVariable int pageNum,
      @PathVariable int rowsNum,
      @PathVariable String filter,
      HttpServletRequest request) {

    authorizationManager.authorizeAction(IDObSalesReturnOrder.SYS_NAME, IActionsNames.READ_ALL);
    Set<String> conditions = authorizationManager.getPermissionConditions();

    Status status = constructStatus(pageNum, rowsNum, filter, request);
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            rowsNum,
            Sort.by(Sort.Direction.DESC, IDObSalesReturnOrderGeneralModel.USER_CODE));

    Specification<DObSalesReturnOrderGeneralModel> querySpecification =
        getSalesReturnOrderDataSpecification(conditions, status);

    Page<DObSalesReturnOrderGeneralModel> salesReturnOrderPage =
        dObSalesReturnOrderGeneralModelRep.findAll(querySpecification, pageRequest);

    List<DObSalesReturnOrderGeneralModel> salesReturnOrders = salesReturnOrderPage.getContent();

    return serializeViewAllResponse(salesReturnOrders, salesReturnOrderPage.getTotalElements());
  }

  private Specification<DObSalesReturnOrderGeneralModel> getSalesReturnOrderDataSpecification(
      Set<String> conditions, Status status) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_NAME, conditions);

    SinglePurchaseUnitNamesConditionSpecification<DObSalesReturnOrderGeneralModel>
        businessUnitConditionSpecification =
            new SinglePurchaseUnitNamesConditionSpecification<>(
                purchaseUnitNames, IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_NAME_EN);

    SimpleStringFieldSpecification userCodeSpecification =
        getSimpleStringSpecification(status, IDObSalesReturnOrderGeneralModel.USER_CODE);

    SimpleStringFieldSpecification typeCodeSpecification =
        getSimpleStringSpecification(
            status, IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_CODE);

    DateContainsSpecification dateSpecification =
        getDateContainsSpecification(status, IDObSalesReturnOrderGeneralModel.CREATION_DATE);

    SimpleStringFieldSpecification businessUnitCodeSpecification =
        getSimpleStringSpecification(status, IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_CODE);

    SimpleStringFieldSpecification customerCodeSpecification =
        getCustomStringContainsSpecification(
            status,
            IDObSalesReturnOrderGeneralModel.CUSTOMER,
            IDObSalesReturnOrderGeneralModel.CODE);

    JsonFieldSpecification customerNameSpecification =
        getCustomJsonContainsSpecification(
            status,
            IDObSalesReturnOrderGeneralModel.CUSTOMER,
            IDObSalesReturnOrderGeneralModel.NAME);

    SimpleStringFieldSpecification salesInvoiceCodeSpecification =
        getSimpleStringSpecification(status, IDObSalesReturnOrderGeneralModel.SALES_INVOICE_CODE);

    CurrentStatesFieldSpecification currentStateSpecification =
        getCurrentStatesFieldSpecification(status, IDObSalesReturnOrderGeneralModel.CURRENT_STATES);

    Specification<DObSalesReturnOrderGeneralModel> querySpecification =
        Specification.where(businessUnitConditionSpecification)
            .and(userCodeSpecification)
            .and(typeCodeSpecification)
            .and(dateSpecification)
            .and(businessUnitCodeSpecification)
            .and(customerCodeSpecification.or(customerNameSpecification))
            .and(salesInvoiceCodeSpecification)
            .and(currentStateSpecification);
    return querySpecification;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/salesreturnordertypes")
  public @ResponseBody String readSalesReturnOrderTypes() {
    authorizationManager.authorizeAction(ICObSalesReturnOrderType.SYS_NAME, IActionsNames.READ_ALL);
    List<CObSalesReturnOrderType> types = salesReturnOrderTypeRep.findAll();
    return serializeRESTSuccessResponse(types);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesReturnOrderCode}/generaldata")
  public @ResponseBody String readGeneralData(@PathVariable String salesReturnOrderCode)
      throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturnOrderGeneralModel);

    salesReturnOrderAuthorizationManager.authorizeActionOnSection(
        salesReturnOrderGeneralModel, IActionsNames.READ_ALL);
    return serializeRESTSuccessResponse(salesReturnOrderGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesReturnOrderCode}/companydata")
  public @ResponseBody String readCompanyData(@PathVariable String salesReturnOrderCode)
      throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturnOrderGeneralModel);

    salesReturnOrderAuthorizationManager.authorizeActionOnSection(
        salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.READ_COMPANY_DATA);

    IObSalesReturnOrderCompanyDataGeneralModel companyDataGeneralModel =
        companyDataGeneralModelRep.findOneBySalesReturnOrderCode(salesReturnOrderCode);

    return serializeRESTSuccessResponse(companyDataGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesReturnOrderCode}/returndetails")
  public @ResponseBody String readReturnDetails(@PathVariable String salesReturnOrderCode)
      throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturnOrderGeneralModel);

    salesReturnOrderAuthorizationManager.authorizeActionOnSection(
        salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.READ_RETURN_DETAILS);

    IObSalesReturnOrderDetailsGeneralModel returnDetailsGeneralModel =
        returnDetailsGeneralModelRep.findOneBySalesReturnOrderCode(salesReturnOrderCode);
    return serializeRESTSuccessResponse(returnDetailsGeneralModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesReturnOrderCode}/items")
  public @ResponseBody String readItemsData(@PathVariable String salesReturnOrderCode)
      throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturnOrderGeneralModel);

    salesReturnOrderAuthorizationManager.authorizeActionOnSection(
        salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.READ_ITEMS);

    JsonObject salesReturnOrderItemData = new JsonObject();

    constructSalesReturnOrderItemData(salesReturnOrderCode, salesReturnOrderItemData);

    return serializeRESTSuccessResponse(salesReturnOrderItemData);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions")
  public @ResponseBody String readHomeScreenActions() throws Exception {
    Set<String> allowedActions =
        salesReturnOrderAllowedActionService.getHomeScreenAllowedActions(DObSalesReturnOrder.class);
    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }
    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/authorizedactions/{salesReturnOrderCode}")
  public @ResponseBody String readObjectScreenActions(@PathVariable String salesReturnOrderCode)
      throws Exception {
    DObSalesReturnOrderGeneralModel salesReturn =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturn);

    Set<String> allowedActions =
        salesReturnOrderAllowedActionService.getObjectScreenAllowedActions(salesReturn);

    if (allowedActions.size() == 0) {
      throw new ResourceAccessException();
    }

    return serializeRESTSuccessResponse(allowedActions);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/salesreturnordersforgoodsreceipt")
  public @ResponseBody String readShippedSalesReturnOrders() {
    authorizationManager.authorizeAction(IDObSalesReturnOrder.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    ReadSalesReturnForGoodsReceiptSpecification salesReturnForGoodsReceiptSpecification =
        new ReadSalesReturnForGoodsReceiptSpecification();
    Specification<DObSalesReturnOrderGeneralModel> authorizedPurchaseUnit =
        salesReturnForGoodsReceiptSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesReturnOrderGeneralModel> salesReturnOrders =
        dObSalesReturnOrderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(salesReturnForGoodsReceiptSpecification.salesReturnsInAllowedState()));

    salesReturnOrders.sort(
        Comparator.comparing(DObSalesReturnOrderGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(salesReturnOrders, salesReturnOrders.size());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/salesreturnordersforcreditnote")
  public @ResponseBody String readShippedAndGoodsReceiptActivatedSalesReturnOrders() {
    authorizationManager.authorizeAction(IDObSalesReturnOrder.SYS_NAME, IActionsNames.READ_ALL);

    Set<String> conditions = authorizationManager.getPermissionConditions();
    List<String> purchaseUnitNames =
        getConditionAttributeValue(
            IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME, conditions);

    ReadSalesReturnForCreditNoteSpecification salesReturnForCreditNoteSpecification =
        new ReadSalesReturnForCreditNoteSpecification();
    Specification<DObSalesReturnOrderGeneralModel> authorizedPurchaseUnit =
        salesReturnForCreditNoteSpecification.isAuthorizedPurchaseUnit(purchaseUnitNames);

    List<DObSalesReturnOrderGeneralModel> salesReturnOrders =
        dObSalesReturnOrderGeneralModelRep.findAll(
            Specification.where(authorizedPurchaseUnit)
                .and(salesReturnForCreditNoteSpecification.salesReturnsInAllowedState()));

    salesReturnOrders.sort(
        Comparator.comparing(DObSalesReturnOrderGeneralModel::getUserCode).reversed());

    return serializeViewAllResponse(salesReturnOrders, salesReturnOrders.size());
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/itemorderunits/{salesReturnOrderCode}/{itemCode}")
  public @ResponseBody String readSalesReturnItemOrderUnits(
      @PathVariable String salesReturnOrderCode, @PathVariable String itemCode) throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dObSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    DObSalesReturnOrderQueryControllerUtils.checkIfEntityIsNull(salesReturnOrderGeneralModel);

    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

    List<IObSalesReturnOrderItemGeneralModel> salesReturnOrderItemGeneralModelList =
        itemGeneralModelRep.findAllBySalesReturnOrderCodeAndItemCodeOrderByOrderUnitCodeAsc(
            salesReturnOrderCode, itemCode);

    return serializeRESTSuccessResponse(salesReturnOrderItemGeneralModelList);
  }

  private void constructSalesReturnOrderItemData(
      String salesReturnOrderCode, JsonObject salesReturnOrderItemData) {

    Gson gson = getGson();
    JsonObject itemSummary = new JsonObject();

    List<IObSalesReturnOrderItemGeneralModel> itemsGeneralModel =
        itemGeneralModelRep.findBySalesReturnOrderCodeOrderByIdDesc(salesReturnOrderCode);

    List<IObSalesReturnOrderTaxGeneralModel> taxesList =
        taxGeneralModelRep.findBySalesReturnOrderCodeOrderByCodeAsc(salesReturnOrderCode);

    String itemsTotalAmount = getItemsTotalAmount(itemsGeneralModel);
    String totalTaxesAmount = calculateTotalTaxesAmount(itemsTotalAmount, taxesList);
    String totalAmountAfterTaxes = getTotalAmountAfterTaxes(itemsTotalAmount, totalTaxesAmount);

    salesReturnOrderItemData.addProperty("Items", gson.toJson(itemsGeneralModel));
    salesReturnOrderItemData.addProperty("Taxes", gson.toJson(taxesList));

    itemSummary.addProperty("totalAmountBeforeTaxes", itemsTotalAmount);

    itemSummary.addProperty("totalTaxes", totalTaxesAmount);

    itemSummary.addProperty("totalAmountAfterTaxes", totalAmountAfterTaxes);

    salesReturnOrderItemData.addProperty("ItemSummary", gson.toJson(itemSummary));
  }

  private String calculateTotalTaxesAmount(
      String itemsTotalAmount,
      List<IObSalesReturnOrderTaxGeneralModel> salesReturnOrderCompanyTaxesGeneralModels) {

    if (itemsTotalAmount == null) {
      return null;
    }

    BigDecimal totalTaxesAmount = BigDecimal.ZERO;
    for (IObSalesReturnOrderTaxGeneralModel tax : salesReturnOrderCompanyTaxesGeneralModels) {
      BigDecimal taxTotalAmount =
          tax.getPercentage()
              .multiply(new BigDecimal(itemsTotalAmount))
              .divide(new BigDecimal("100.0"));
      tax.setAmount(taxTotalAmount);
      totalTaxesAmount = totalTaxesAmount.add(taxTotalAmount);
    }
    return totalTaxesAmount.toString();
  }

  private String getItemsTotalAmount(List<IObSalesReturnOrderItemGeneralModel> itemsList) {

    if (itemsList.size() == 0) {
      return null;
    }

    BigDecimal itemsTotalAmount = BigDecimal.ZERO;

    for (IObSalesReturnOrderItemGeneralModel item : itemsList) {
      BigDecimal itemTotalAmount = item.getTotalAmount();
      itemsTotalAmount = itemsTotalAmount.add(itemTotalAmount);
    }

    return itemsTotalAmount.toString();
  }

  private String getTotalAmountAfterTaxes(String itemsTotalAmount, String totalTaxesAmount) {

    BigDecimal total = null;

    if (itemsTotalAmount != null && totalTaxesAmount != null) {
      total = new BigDecimal(itemsTotalAmount).add(new BigDecimal(totalTaxesAmount));
    }

    return total != null ? total.toString() : null;
  }
}
