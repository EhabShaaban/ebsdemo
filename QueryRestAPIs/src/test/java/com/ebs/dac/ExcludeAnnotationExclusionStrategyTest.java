package com.ebs.dac;

import com.ebs.dda.rest.BasicQueryController;
import com.ebs.entities.fakeEntityWithCustomeExclusionAnnotaion;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class ExcludeAnnotationExclusionStrategyTest {

  @Test
  public void shouldExcludePropertyWithCustomeExclusionAnnotaionWhenSerializedAsJsonObject() {
    BasicQueryController testableQueryController = new BasicQueryControllerTestable();
    fakeEntityWithCustomeExclusionAnnotaion fakeEntityWithCustomeExclusionAnnotaion = new fakeEntityWithCustomeExclusionAnnotaion();
    fakeEntityWithCustomeExclusionAnnotaion.setNotAnnotatedString("notAnnotatedStringValue");
    fakeEntityWithCustomeExclusionAnnotaion.setAnnotatedString("annotatedStringValue");
    String response = testableQueryController.serializeViewAllResponse(Collections.singletonList(fakeEntityWithCustomeExclusionAnnotaion), 1);
    Assert.assertFalse(response.contains("annotatedString"));
  }

  class BasicQueryControllerTestable extends BasicQueryController {}
}
