# EBS Logging Machine

## Table of contents:

1. [Add Application Name](#step1)
2. [Add logback-access.xml under resources directory](#step2)
3. [Configure logback-access.xml](#step3)
4. [Prepare Logging Machine](#step4)
5. [Notes](#step5)

## 1. Modify application-EBS.properties {#step1}

- Added new property named spring.application.name which holds the value of project name.

    ```properties
    spring.application.name = 'testing'
    ```

## 2. Add logback-access.xml {#step2}

- Set your working directory to ApplicationLauncher/src/main/resources & ApplicationLauncher 
- Add new XML file that is responsible for sending logs to a destination port
    
    ```xml
    <destination>172.31.32.34:4560</destination>
    ```
    > We are sending logs to port **4560** on our logging machine with private ip **172.31.32.34** 

- Add new property named logging.config in application-EBS.properties with value logback-access.xml

    ```properties
    logging.config = logback-access.xml
    ``` 

## 3. Configure logback-access.xml {#step3}

- Add new property named logcontext and assign it with spring.application.name

    ```xml
    <springProperty name="logcontext" scope="context" source="spring.application.name"/>
    ```
 
- Add pattern tag with an object having two properties appName and appVersion

    ```xml
    <pattern>
      {
        "appName": "${logcontext}",
        "appVersion": "1.0"
      }
    </pattern>
    ```
  
## 4. Prepare Logging Machine {#step4}

- Pull latest elk image from docker hub
 
    ```shell script
    sudo docker pull sebp/elk:661
    ```
  
- Update maximum map count system property
 
    ```shell script
    sudo sysctl -w vm.max_map_count=262144
    ```
  
- Run a container with exposing 8080:5601, 4560:4560, 9200:9200, 5044:5044

    ```
    sudo docker run -p 80:5601 -p 9200:9200 -p 5044:5044 -p 4560:4560 --ulimit nofile=65536:65536 -d --name logging-container sebp/elk
    ```
  
- Open **logging-container** in /bin/bash

    ```shell script
    sudo docker exec -it logging-container /bin/bash
    ```
 
- Open logstash.yml in edit

    ```shell script
    sudo vim opt/logstash/config/logstash.yml
    ```
 
- Add config.string in opt/logstash/config/logstash.yml

    ```shell script
    config.string: input { tcp { port => 4560 codec => json_lines } } output { elasticsearch { hosts => "172.31.32.34" index => "%{logcontext}" } }
    ```
   
    > Make sure that config.string is not indented with any amount of space 
  
## 5. Notes {#step5}
   
- logging-container has to be manually restarted if the log machine went down at any time
    
    ```shell script
    sudo docker start logging-container
    ```

- log machine url is [ebs.log.kites-software.io:8080](http://ebs.log.kites-software.io:8080)