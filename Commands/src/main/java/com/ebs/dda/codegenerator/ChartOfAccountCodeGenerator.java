package com.ebs.dda.codegenerator;

import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountCreateValueObject;

import java.util.Optional;

public class ChartOfAccountCodeGenerator {
    private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;

    public ChartOfAccountCodeGenerator() {
    }

    public String generateUserCode(
            HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject) {
        String userCode = "";
        if (!chartOfAccountCreateValueObject.getLevel().equals(IHObChartOfAccounts.ROOT)) {
            userCode =
                    generateChartOfAccountCodeAsIntermedite(
                            Long.parseLong(chartOfAccountCreateValueObject.getParentCode()));
        } else {
            userCode = generateItemGroupCodeAsRoot();
        }
        return userCode;
    }

    private String generateItemGroupCodeAsRoot() {
        Optional<String> code =
                chartOfAccountsGeneralModelRep.findLastChartOfAccountCodeAsRootWithMaxId();
        Long newCode = Long.valueOf(code.orElse("0")) + 1;

        return newCode.toString();
    }

    private String generateChartOfAccountCodeAsIntermedite(Long parentCode) {
        Optional<Long> maxAsHierarchyId =
                chartOfAccountsGeneralModelRep.findChartOfAccountMaxIdAsHierarchy(parentCode.toString());
        Optional<String> code = null;
        Long newGeneratedCode = null;
        if (!maxAsHierarchyId.isPresent()) {
            code = Optional.of(parentCode.toString() + "01");
            newGeneratedCode = Long.valueOf(code.orElse("000000"));
        } else {
            code =
                    chartOfAccountsGeneralModelRep.findChartOfAccountCodeAsHierarchyWithMaxId(
                            maxAsHierarchyId.orElse(0L));
            newGeneratedCode = Long.valueOf(code.orElse("000000")) + 1;
        }

        return newGeneratedCode.toString();
    }

    public void setChartOfAccountsGeneralModelRep(HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
        this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
    }
}
