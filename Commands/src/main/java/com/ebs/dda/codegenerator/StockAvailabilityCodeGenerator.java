package com.ebs.dda.codegenerator;

import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;

public class StockAvailabilityCodeGenerator {
  public synchronized String generateUserCode(StockAvailabilityCodeGenerationValueObject valueObject) {
    StringBuilder userCodeBuilder =
        new StringBuilder()
            .append(valueObject.getStockType())
            .append(valueObject.getBusinessUnitCode())
            .append(valueObject.getCompanyCode())
            .append(valueObject.getPlantCode())
            .append(valueObject.getStorehouseCode())
            .append(valueObject.getItemCode())
            .append(valueObject.getUnitOfMeasureCode());
    return userCodeBuilder.toString();
  }
}