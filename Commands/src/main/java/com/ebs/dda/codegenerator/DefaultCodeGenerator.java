package com.ebs.dda.codegenerator;

import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;

public class DefaultCodeGenerator {

  private int numberOfDigits;
  private long code;

  public DefaultCodeGenerator(int numberOfDigits) {
    this.numberOfDigits = numberOfDigits;
    this.code = 0L;
  }

  public String generateCode() {
    this.code++;
    String numberOFDigitsInCode = String.valueOf(this.code);
    StringBuffer buffer = new StringBuffer(numberOFDigitsInCode);
    for (int i = numberOFDigitsInCode.length(); i < this.numberOfDigits; i++) {
      buffer.insert(0, '0');
    }
    return buffer.toString();
  }

  public void setLastCode(String lastCode) {
    code = Long.parseLong(lastCode);
  }

  public void readLastCode(CodeDBRetrievalGateway codeDBRetrievalGateway) {
    this.code = readCode(codeDBRetrievalGateway.retrieveEntityLastCode());
  }

  private long readCode(EntityUserCode entityUserCode) {
    return Long.parseLong(entityUserCode.getCode());
  }
}
