package com.ebs.dda.codegenerator;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;
import com.ebs.dac.dbo.jpa.repositories.usercodes.EntityUserCodeRep;

// Created By Hossam Hassan @ 10/10/18
public class MasterDataCodeGenrator {
  private EntityUserCodeRep entityUserCodeRep;

  public synchronized <T extends BusinessObject> String generateUserCode(Class<T> entityClass) {
    EntityUserCode entityUserCode = entityUserCodeRep.findOneByType(entityClass.getSimpleName());

    String newEntityUserCode = null;
    if (entityUserCode == null) {
      newEntityUserCode = "000001";
      createEntityUserCode(newEntityUserCode, entityClass.getSimpleName());
    } else {
      String code = entityUserCode.getCode();
      Integer serialNumber = Integer.valueOf(code);
      serialNumber++;

      newEntityUserCode = getFormattedSerialNumber(serialNumber);
      entityUserCode.setCode(newEntityUserCode);
      entityUserCodeRep.save(entityUserCode);
    }

    return newEntityUserCode;
  }

  private String getFormattedSerialNumber(int serialNumber) {
    String serialNumberStr = String.valueOf(serialNumber);
    while (serialNumberStr.length() < 6) {
      serialNumberStr = "0" + serialNumberStr;
    }
    return serialNumberStr;
  }

  private void createEntityUserCode(String userCode, String entityClassName) {
    EntityUserCode entityUserCode = new EntityUserCode(entityClassName, userCode);
    entityUserCodeRep.save(entityUserCode);
  }

  public void setEntityUserCodeRep(EntityUserCodeRep entityUserCodeRep) {
    this.entityUserCodeRep = entityUserCodeRep;
  }
}
