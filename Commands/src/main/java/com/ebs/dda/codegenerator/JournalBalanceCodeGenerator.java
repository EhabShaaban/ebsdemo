package com.ebs.dda.codegenerator;

import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum.JournalBalanceType;

public class JournalBalanceCodeGenerator {

  public String generateUserCode(JournalBalanceCodeGenerationValueObject valueObject) {

    StringBuilder userCodeBuilder = new StringBuilder()
        .append(valueObject.getObjectTypeCode())
        .append(valueObject.getBusinessUnitCode())
        .append(valueObject.getCompanyCode())
        .append(valueObject.getCurrencyCode());

    if (valueObject.getObjectTypeCode().equals(JournalBalanceType.TREASURY.name())) {
      userCodeBuilder.append(valueObject.getTreasuryCode());
    } else {
      userCodeBuilder.append(valueObject.getBankCode());
      userCodeBuilder.append(valueObject.getBankAccountNumber());
    }

    return userCodeBuilder.toString();
  }
}
