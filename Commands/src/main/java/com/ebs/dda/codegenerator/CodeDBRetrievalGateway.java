package com.ebs.dda.codegenerator;

import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;
import com.ebs.dac.dbo.jpa.repositories.usercodes.EntityUserCodeRep;

public class CodeDBRetrievalGateway {

  private EntityUserCodeRep entityUserCodeRep;
  private String entityType;

  public CodeDBRetrievalGateway(EntityUserCodeRep entityUserCodeRep, String entityType) {
    this.entityUserCodeRep = entityUserCodeRep;
    this.entityType = entityType;
  }

  public EntityUserCode retrieveEntityLastCode() {
    return this.entityUserCodeRep.findOneByType(this.entityType);
  }
}
