package com.ebs.dda.codegenerator;

import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;
import com.ebs.dac.dbo.jpa.repositories.usercodes.EntityUserCodeRep;
import org.joda.time.DateTime;

public class DocumentObjectCodeGenerator {

    private EntityUserCodeRep entityUserCodeRep;

    public synchronized String generateUserCode(String entityName) {
        EntityUserCode entityUserCode = entityUserCodeRep.findOneByType(entityName);

        int currentYear = new DateTime().getYear();
        String newEntityUserCode = String.valueOf(currentYear);
        if (entityUserCode == null) {
            newEntityUserCode += "000001";
            createEntityUserCode(entityName, newEntityUserCode);
        } else {
            String code = entityUserCode.getCode();
            String lastReservedCodeYear = code.substring(0, 4);
            Integer serialNumber = Integer.valueOf(code.substring(4));
            if (currentYear == Integer.valueOf(lastReservedCodeYear)) {
                serialNumber++;
            } else {
                serialNumber = 1;
            }
            newEntityUserCode += getFormattedSerialNumber(serialNumber);
            entityUserCode.setCode(newEntityUserCode);
            entityUserCodeRep.save(entityUserCode);
        }

        return newEntityUserCode;
    }

    private String getFormattedSerialNumber(int serialNumber) {
        String serialNumberStr = String.valueOf(serialNumber);
        while (serialNumberStr.length() < 6) {
            serialNumberStr = "0" + serialNumberStr;
        }
        return serialNumberStr;
    }

    private void createEntityUserCode(String entityName, String userCode) {
        EntityUserCode entityUserCode = new EntityUserCode(entityName, userCode);
        entityUserCodeRep.save(entityUserCode);
    }

    public void setEntityUserCodeRep(EntityUserCodeRep entityUserCodeRep) {
        this.entityUserCodeRep = entityUserCodeRep;
    }


}
