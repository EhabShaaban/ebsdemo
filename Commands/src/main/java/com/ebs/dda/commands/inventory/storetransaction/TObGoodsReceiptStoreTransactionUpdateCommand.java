package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionCreateStockTransformationValueObject;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class TObGoodsReceiptStoreTransactionUpdateCommand
    implements ICommand<TObStoreTransactionCreateStockTransformationValueObject> {

  private CommandUtils commandUtils;
  private DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep;
  private TObStoreTransactionRep storeTransactionRep;

  public TObGoodsReceiptStoreTransactionUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      TObStoreTransactionCreateStockTransformationValueObject valueObject) throws Exception {

    DObStockTransformationGeneralModel stockTransformationGeneralModel =
        stockTransformationGeneralModelRep.findOneByUserCode(
            valueObject.getStockTransformationCode());

    TObStoreTransaction storeTransaction =
        storeTransactionRep.findOneByUserCode(
            stockTransformationGeneralModel.getRefStoreTransactionCode());

    BigDecimal remainingQty = calculateRemainingQty(stockTransformationGeneralModel, storeTransaction);

    storeTransaction.setRemainingQuantity(remainingQty);
    commandUtils.setModificationInfoAndModificationDate(storeTransaction);
    storeTransactionRep.update(storeTransaction);
    return Observable.just(storeTransaction.getUserCode());
  }

  private BigDecimal calculateRemainingQty(
      DObStockTransformationGeneralModel stockTransformationGeneralModel,
      TObStoreTransaction storeTransaction) {
    BigDecimal transformedQty = BigDecimal.valueOf(stockTransformationGeneralModel.getTransformedQty());
    return storeTransaction.getRemainingQuantity().subtract(transformedQty);
  }

    public void setStoreTransactionRep(TObStoreTransactionRep storeTransactionRep) {
        this.storeTransactionRep = storeTransactionRep;
    }

  public void setStockTransformationGeneralModelRep(
      DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep) {
    this.stockTransformationGeneralModelRep = stockTransformationGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
