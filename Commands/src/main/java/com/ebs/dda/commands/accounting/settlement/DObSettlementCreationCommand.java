package com.ebs.dda.commands.accounting.settlement;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.settlements.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;

import io.reactivex.Observable;

public class DObSettlementCreationCommand implements ICommand<DObSettlementCreationValueObject> {

	private DObSettlementRep settlementRep;
	private CObPurchasingUnitRep purchasingUnitRep;
	private CObCompanyRep companyRep;
	private CObCurrencyRep currencyRep;
	private DocumentObjectCodeGenerator codeGenerator;
	private CommandUtils commandUtils;
	private DObSettlementStateMachine stateMachine;

	public DObSettlementCreationCommand() throws Exception {
		commandUtils = new CommandUtils();
		stateMachine = new DObSettlementStateMachine();
	}

	@Override
	public Observable<String> executeCommand(DObSettlementCreationValueObject valueObject)
					throws Exception {
		DObSettlement settlement = new DObSettlement();
		setGeneralData(settlement, valueObject);
		setCompanyData(settlement, valueObject);
		setDetailsData(settlement, valueObject);
		settlementRep.create(settlement);
		return Observable.just(settlement.getUserCode());
	}

	private void setDetailsData(DObSettlement settlement,
					DObSettlementCreationValueObject valueObject) {
		IObSettlementDetails settlementDetails = new IObSettlementDetails();
		CObCurrency currency = currencyRep.findOneByUserCode(valueObject.getCurrencyCode());
		settlementDetails.setCurrencyId(currency.getId());
		commandUtils.setCreationAndModificationInfoAndDate(settlementDetails);
		settlement.setHeaderDetail(IDObSettlement.detailsAttr, settlementDetails);

	}

	private void setCompanyData(DObSettlement settlement,
					DObSettlementCreationValueObject valueObject) {
		IObSettlementCompanyData companyData = new IObSettlementCompanyData();
		CObCompany company = companyRep.findOneByUserCode(valueObject.getCompanyCode());
		CObPurchasingUnit businessUnit = purchasingUnitRep
						.findOneByUserCode(valueObject.getBusinessUnitCode());
		companyData.setCompanyId(company.getId());
		companyData.setPurchaseUnitId(businessUnit.getId());
		commandUtils.setCreationAndModificationInfoAndDate(companyData);

		settlement.setHeaderDetail(IDObSettlement.companyDataAttr, companyData);
	}

	private void setGeneralData(DObSettlement settlement,
					DObSettlementCreationValueObject valueObject) throws Exception {
		String userCode = codeGenerator.generateUserCode(DObSettlement.class.getSimpleName());
		settlement.setUserCode(userCode);
		stateMachine.initObjectState(settlement);
		commandUtils.setCreationAndModificationInfoAndDate(settlement);
		settlement.setSettlementType(valueObject.getType());
		settlement.setDocumentOwnerId(valueObject.getDocumentOwnerId());
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setSettlementRep(DObSettlementRep settlementRep) {
		this.settlementRep = settlementRep;
	}

	public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
		this.purchasingUnitRep = purchasingUnitRep;
	}

	public void setCompanyRep(CObCompanyRep companyRep) {
		this.companyRep = companyRep;
	}

	public void setCurrencyRep(CObCurrencyRep currencyRep) {
		this.currencyRep = currencyRep;
	}

	public void setCodeGenerator(DocumentObjectCodeGenerator codeGenerator) {
		this.codeGenerator = codeGenerator;
	}
}
