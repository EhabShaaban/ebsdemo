package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import io.reactivex.Observable;

public class DObPurchaseOrderPostGoodsReceiptCommand
    implements ICommand<DObInventoryDocumentActivateValueObject> {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep dObPurchaseOrderRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      iObGoodsReceiptPurchaseOrderDataGeneralModelRep;

  public DObPurchaseOrderPostGoodsReceiptCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObInventoryDocumentActivateValueObject valueObject)
      throws Exception {
    IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(
            valueObject.getUserCode());
    DObPurchaseOrder dObPurchaseOrder =
        dObPurchaseOrderRep.findOneByUserCode(
            iObGoodsReceiptPurchaseOrderDataGeneralModel.getPurchaseOrderCode());
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(dObPurchaseOrder);
    boolean isPostActionAllowed = checkIfPostActionAllowed(purchaseOrderStateMachine);
    setPurchaseOrderTargetState(purchaseOrderStateMachine);

    if (isPostActionAllowed) {
      commandUtils.setModificationInfoAndModificationDate(dObPurchaseOrder);
      dObPurchaseOrderRep.update(dObPurchaseOrder);
    }
    return Observable.just(valueObject.getUserCode());
  }

  private boolean checkIfPostActionAllowed(AbstractStateMachine purchaseOrderStateMachine)
      throws Exception {
    try {
      purchaseOrderStateMachine.isAllowedAction(IPurchaseOrderActionNames.POST_GOODS_RECEIPT);
    } catch (ActionNotAllowedPerStateException e) {
      return false;
    }
    return true;
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void setPurchaseOrderTargetState(AbstractStateMachine purchaseOrderStateMachine)
      throws Exception {
    String actionName = IPurchaseOrderActionNames.POST_GOODS_RECEIPT;
    purchaseOrderStateMachine.isAllowedAction(actionName);
    fireStateMachineEvent(purchaseOrderStateMachine, actionName);
  }

  private void fireStateMachineEvent(
      AbstractStateMachine purchaseOrderStateMachine, String actionName) {
    if (purchaseOrderStateMachine.fireEvent(actionName)) {
      purchaseOrderStateMachine.save();
    }
  }

  public void setdObOrderRep(DObPurchaseOrderRep dObPurchaseOrderRep) {
    this.dObPurchaseOrderRep = dObPurchaseOrderRep;
  }

  public void setiObGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.iObGoodsReceiptPurchaseOrderDataGeneralModelRep =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
