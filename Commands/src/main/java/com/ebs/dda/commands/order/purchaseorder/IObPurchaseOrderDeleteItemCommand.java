package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.jpa.order.IObPurchaseOrderDeleteItemValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderItem;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderItemRep;
import io.reactivex.Observable;

public class IObPurchaseOrderDeleteItemCommand
    implements ICommand<IObPurchaseOrderDeleteItemValueObject> {

  private final CommandUtils commandUtils;
  private IObOrderItemRep iobOrderItemRep;
  private DObOrderDocumentRep orderDocumentRep;

  public IObPurchaseOrderDeleteItemCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObPurchaseOrderDeleteItemValueObject valueObject)
      throws Exception {
    IObOrderItem iobOrderItem = iobOrderItemRep.findOneById(valueObject.getItemId());
    iobOrderItemRep.delete(iobOrderItem);

    DObOrderDocument purchaseOrder =
        orderDocumentRep.findOneByUserCode(valueObject.getPurchaseOrderCode());
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    orderDocumentRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  public void setIObOrderItemRep(IObOrderItemRep iobOrderItemRep) {
    this.iobOrderItemRep = iobOrderItemRep;
  }

  public void setOrderDocumentRep(DObOrderDocumentRep orderDocumentRep) {
    this.orderDocumentRep = orderDocumentRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
