package com.ebs.dda.commands.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.costing.services.CostingExchangeRateService;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.costing.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObCostingUpdateAfterRecostingCommand
    implements ICommand<DObCostingDocumentRecostingValueObject> {
  private DObCostingRep costingRep;
  private IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  private CostingExchangeRateService exchangeRateService;
  private IObCostingDetailsRep costingDetailsRep;
  private CommandUtils commandUtils;
  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private IObCostingItemRep costingItemRep;
  private CreateCostingItemsSectionService createCostingItemsSectionService;

  public DObCostingUpdateAfterRecostingCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public <T> Observable<T> executeCommand(DObCostingDocumentRecostingValueObject valueObject)
      throws Exception {
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        this.costingDetailsGeneralModelRep.findOneByGoodsReceiptCode(
            valueObject.getGoodsReceiptCode());
    DObCosting costing =
        this.costingRep.findOneByUserCode(costingDetailsGeneralModel.getUserCode());
    IObCostingDetails costingDetails = updateCostingDetails(costingDetailsGeneralModel);
    List<IObCostingItem> costingItems = costingItemRep.findByRefInstanceId(costing.getId());
    updateCostingItems(costingItems, costingDetails.getCurrencyPriceActualTime(), valueObject);

    setCostingDetails(costing, costingDetails);
    costing.setLinesDetails(IDObCosting.accountingDocumentItemsAttr, costingItems);
    this.commandUtils.setModificationInfoAndModificationDate(costing);
    this.costingRep.update(costing);
    return Observable.empty();
  }

  private void updateCostingItems(
      List<IObCostingItem> costingItems,
      BigDecimal currencyPriceActualTime,
      DObCostingDocumentRecostingValueObject valueObject) {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        vendorInvoiceGeneralModelRep.findOneByPurchaseOrderCode(valueObject.getPurchaseOrderCode());
    IObCostingItemsCreationValueObject costingItemsCreationValueObject =
        new IObCostingItemsCreationValueObject(
            vendorInvoiceGeneralModel.getUserCode(),
            valueObject.getGoodsReceiptCode(),
            valueObject.getLandedCostCode(),
            currencyPriceActualTime,
            true);
    for (IObCostingItem costingItem : costingItems) {
      createCostingItemsSectionService.setCostingItemCostFields(
          costingItem, costingItemsCreationValueObject);
      this.commandUtils.setModificationInfoAndModificationDate(costingItem);
    }
  }

  private void setCostingDetails(DObCosting costing, IObCostingDetails costingDetails) {
    costing.setHeaderDetail(IDObCosting.costingDetailsAttr, costingDetails);
  }

  private IObCostingDetails updateCostingDetails(
      IObCostingDetailsGeneralModel costingDetailsGeneralModel) {
    BigDecimal actualExchangeRate =
        this.exchangeRateService.calculate(costingDetailsGeneralModel.getGoodsReceiptCode());
    IObCostingDetails costingDetails =
        this.costingDetailsRep.findOneById(costingDetailsGeneralModel.getId());
    costingDetails.setCurrencyPriceActualTime(actualExchangeRate);
    this.commandUtils.setModificationInfoAndModificationDate(costingDetails);
    return costingDetails;
  }

  public void setCostingRep(DObCostingRep costingRep) {
    this.costingRep = costingRep;
  }

  public void setCostingDetailsGeneralModelRep(
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
  }

  public void setCostingDetailsRep(IObCostingDetailsRep costingDetailsRep) {
    this.costingDetailsRep = costingDetailsRep;
  }

  public void setExchangeRateService(CostingExchangeRateService exchangeRateService) {
    this.exchangeRateService = exchangeRateService;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCostingItemRep(IObCostingItemRep costingItemRep) {
    this.costingItemRep = costingItemRep;
  }

  public void setCreateCostingItemsSectionService(
      CreateCostingItemsSectionService createCostingItemsSectionService) {
    this.createCostingItemsSectionService = createCostingItemsSectionService;
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }
}
