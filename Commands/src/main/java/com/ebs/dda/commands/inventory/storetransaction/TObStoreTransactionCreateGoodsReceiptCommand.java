package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderCreateStoreTransactionService;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TObStoreTransactionCreateGoodsReceiptCommand
        implements ICommand<DObInventoryDocumentActivateValueObject> {
    private final String GR_PO = "GR_PO";
    private TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep;
    private IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
            goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;
    private IObGoodsReceiptSalesReturnDataGeneralModelRep goodsReceiptSalesReturnDataGeneralModelRep;
    private IObSalesReturnOrderDetailsGeneralModelRep salesReturnOrderDetailsGeneralModelRep;
    private IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep;
    private IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep;
    private TObGoodsIssueStoreTransactionGeneralModelRep goodsIssueStoreTransactionGeneralModelRep;
    private TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep;
    private DObGoodsReceiptDataGeneralModelRep goodsReceiptDataGeneralModelRep;
    private MObItemGeneralModelRep itemGeneralModelRep;
    private CObMeasureRep measureRep;
    private CommandUtils commandUtils;
    private DocumentObjectCodeGenerator documentObjectCodeGenerator;

    private ArrayList<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionsList;
    private GoodsReceiptPurchaseOrderCreateStoreTransactionService
            goodsReceiptPurchaseOrderCreateStoreTransactionService;
    private GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
            goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;

    public TObStoreTransactionCreateGoodsReceiptCommand(
            DocumentObjectCodeGenerator documentObjectCodeGenerator) {
        commandUtils = new CommandUtils();
        this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    }

    @Override
    public Observable<String> executeCommand(
            DObInventoryDocumentActivateValueObject goodsReceiptValueObject) throws Exception {

        String goodsReceiptCode = goodsReceiptValueObject.getUserCode();

        if (goodsReceiptValueObject.getInventoryType().equals(GR_PO)) {
            handleGoodsReceiptPurchaseOrderStoreTransactionEntity(goodsReceiptCode);
        } else {
            handleGoodsReceiptSalesReturnStoreTransactionEntity(goodsReceiptCode);
        }

        return Observable.just(goodsReceiptCode);
    }

    private void handleGoodsReceiptPurchaseOrderStoreTransactionEntity(String goodsReceiptCode) {

        List<TObGoodsReceiptStoreTransaction> transactions =
                goodsReceiptPurchaseOrderCreateStoreTransactionService
                        .getGoodsReceiptPurchaseOrderStoreTransaction(goodsReceiptCode);

        goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
                .setGoodsReceiptPurchaseOrderStoreTransactionCosts(goodsReceiptCode, transactions);

        goodsReceiptStoreTransactionRep.saveAll(transactions);
    }

    private void handleGoodsReceiptSalesReturnStoreTransactionEntity(String goodsReceiptCode) {
        DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel =
                getGoodsReceiptDataGeneralModel(goodsReceiptCode);

        IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel =
                getGoodsReceiptSalesReturnDataGeneralModel(goodsReceiptCode);

        IObSalesReturnOrderDetailsGeneralModel salesReturnOrderDetailsGeneralModel =
                getSalesReturnOrderDetailsGeneralModel(goodsReceiptSalesReturnDataGeneralModel);

        IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
                getSalesInvoiceBusinessPartnerGeneralModel(salesReturnOrderDetailsGeneralModel);

        IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
                getGoodsIssueRefDocumentDataGeneralModel(salesInvoiceBusinessPartnerGeneralModel);

        goodsReceiptStoreTransactionsList = new ArrayList<>();

        List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelList =
                getGoodsReceiptSalesReturnItemQuantities(goodsReceiptCode);

        HashMap<String, BigDecimal> storeTransactionsRemainingsMap = new HashMap<>();

        for (IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel :
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelList) {

            List<TObGoodsIssueStoreTransactionGeneralModel> goodsIssueStoreTransactionGeneralModelList =
                    getOldGoodsIssueStoreTransactions(
                            goodsIssueRefDocumentDataGeneralModel,
                            goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
                            goodsReceiptDataGeneralModel);

            storeTransactionsRemainingsMap =
                    getStoreTransactionRemainingsMap(
                            storeTransactionsRemainingsMap, goodsIssueStoreTransactionGeneralModelList);

            if (goodsIssueStoreTransactionGeneralModelList.size() > 1) {
                handleQuantityIssuedOnMultipleStoreTransactions(
                        goodsIssueStoreTransactionGeneralModelList,
                        goodsReceiptDataGeneralModel,
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
                        storeTransactionsRemainingsMap);
            } else {
                handleQuantityIssuedOnSingleStoreTransaction(
                        goodsReceiptDataGeneralModel,
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
                        goodsIssueStoreTransactionGeneralModelList,
                        storeTransactionsRemainingsMap);
            }
        }

        goodsReceiptStoreTransactionRep.saveAll(goodsReceiptStoreTransactionsList);
    }

    private HashMap<String, BigDecimal> getStoreTransactionRemainingsMap(
            HashMap<String, BigDecimal> storeTransactionsRemainings,
            List<TObGoodsIssueStoreTransactionGeneralModel> goodsIssueStoreTransactionGeneralModelList) {
        for (TObGoodsIssueStoreTransactionGeneralModel goodsIssueStoreTransactionGeneralModel :
                goodsIssueStoreTransactionGeneralModelList) {
            if (!storeTransactionsRemainings.containsKey(
                    goodsIssueStoreTransactionGeneralModel.getUserCode()))
                storeTransactionsRemainings.put(
                        goodsIssueStoreTransactionGeneralModel.getUserCode(),
                        goodsIssueStoreTransactionGeneralModel.getQuantity());
        }
        return storeTransactionsRemainings;
    }

    private void handleQuantityIssuedOnMultipleStoreTransactions(
            List<TObGoodsIssueStoreTransactionGeneralModel> goodsIssueStoreTransactionGeneralModelList,
            DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
            HashMap<String, BigDecimal> storeTransactionsRemainingsMap) {

        BigDecimal requiredReturnedQuantity =
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE();

        for (int i = 0;
             i < goodsIssueStoreTransactionGeneralModelList.size()
                     && requiredReturnedQuantity.compareTo(BigDecimal.ZERO) > 0;
             i++) {

            String storeTransactionCode = goodsIssueStoreTransactionGeneralModelList.get(i).getUserCode();

            if (storeTransactionsRemainingsMap.get(storeTransactionCode).compareTo(BigDecimal.ZERO)
                    == 0) {
                continue;
            }
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction =
                    setMutualDataCaseMultipleStoreTransactions(
                            goodsReceiptDataGeneralModel,
                            goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel);
            if (storeTransactionsRemainingsMap
                    .get(storeTransactionCode)
                    .compareTo(requiredReturnedQuantity)
                    >= 0) {
                setDataFromOldMultipleStoreTransactions(
                        requiredReturnedQuantity,
                        goodsReceiptStoreTransaction,
                        goodsIssueStoreTransactionGeneralModelList.get(i));

                storeTransactionsRemainingsMap.put(
                        storeTransactionCode,
                        storeTransactionsRemainingsMap
                                .get(storeTransactionCode)
                                .subtract(requiredReturnedQuantity));

                requiredReturnedQuantity = BigDecimal.ZERO;
            } else {

                setDataFromOldMultipleStoreTransactions(
                        storeTransactionsRemainingsMap.get(storeTransactionCode),
                        goodsReceiptStoreTransaction,
                        goodsIssueStoreTransactionGeneralModelList.get(i));

                requiredReturnedQuantity =
                        requiredReturnedQuantity.subtract(
                                storeTransactionsRemainingsMap.get(storeTransactionCode));

                storeTransactionsRemainingsMap.put(storeTransactionCode, BigDecimal.ZERO);
            }

            goodsReceiptStoreTransactionsList.add(goodsReceiptStoreTransaction);
        }
    }

    private TObGoodsReceiptStoreTransaction setMutualDataCaseMultipleStoreTransactions(
            DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel) {
        TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction =
                new TObGoodsReceiptStoreTransaction();

        addCommonDataForGoodsReceiptSalesReturnStoreTransaction(
                goodsReceiptStoreTransaction, goodsReceiptDataGeneralModel);
        addItemDataToStoreTransaction(
                goodsReceiptStoreTransaction, goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel);

        setTransactionOperation(goodsReceiptStoreTransaction);

        setGoodsReceiptStoreTransactionCreationData(goodsReceiptStoreTransaction);

        setStoreTransactionUserCode(goodsReceiptStoreTransaction);
        return goodsReceiptStoreTransaction;
    }

    private void handleQuantityIssuedOnSingleStoreTransaction(
            DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
            List<TObGoodsIssueStoreTransactionGeneralModel> goodsIssueStoreTransactionGeneralModelList,
            HashMap<String, BigDecimal> storeTransactionsRemainings) {
        TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction =
                new TObGoodsReceiptStoreTransaction();

        addCommonDataForGoodsReceiptSalesReturnStoreTransaction(
                goodsReceiptStoreTransaction, goodsReceiptDataGeneralModel);
        addItemDataToStoreTransaction(
                goodsReceiptStoreTransaction, goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel);

        setDataFromOldSingleStoreTransaction(
                goodsReceiptStoreTransaction,
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
                goodsIssueStoreTransactionGeneralModelList.get(0));

        setTransactionOperation(goodsReceiptStoreTransaction);

        setGoodsReceiptStoreTransactionCreationData(goodsReceiptStoreTransaction);

        setStoreTransactionUserCode(goodsReceiptStoreTransaction);

        String storeTransactionCode = goodsIssueStoreTransactionGeneralModelList.get(0).getUserCode();
        BigDecimal returnedQty =
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE();

        storeTransactionsRemainings.put(
                storeTransactionCode,
                storeTransactionsRemainings.get(storeTransactionCode).subtract(returnedQty));

        goodsReceiptStoreTransactionsList.add(goodsReceiptStoreTransaction);
    }

    private void setTransactionOperation(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {
        TransactionTypeEnum.TransactionType transactionOperationType =
                TransactionTypeEnum.TransactionType.ADD;
        TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
        transactionOperationEnum.setId(transactionOperationType);
        goodsReceiptStoreTransaction.setTransactionOperation(transactionOperationEnum);
    }

    private void setGoodsReceiptStoreTransactionCreationData(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {
        commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptStoreTransaction);
    }

    private void setStoreTransactionUserCode(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {
        String userCode =
                documentObjectCodeGenerator.generateUserCode(TObStoreTransaction.class.getSimpleName());
        goodsReceiptStoreTransaction.setUserCode(userCode);
    }

    private void setDataFromOldMultipleStoreTransactions(
            BigDecimal requiredReturnedQuantity,
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
            TObGoodsIssueStoreTransactionGeneralModel goodsIssueStoreTransactionGeneralModel) {

        goodsReceiptStoreTransaction.setQuantity(requiredReturnedQuantity);
        goodsReceiptStoreTransaction.setRemainingQuantity(requiredReturnedQuantity);
        goodsReceiptStoreTransaction.setRefTransactionId(
                goodsIssueStoreTransactionGeneralModel.getId());
        setDataFromOriginalAddingTransaction(
                goodsReceiptStoreTransaction, goodsIssueStoreTransactionGeneralModel);
    }

    private void setDataFromOldSingleStoreTransaction(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
            TObGoodsIssueStoreTransactionGeneralModel goodsIssueStoreTransactionGeneralModel) {

        goodsReceiptStoreTransaction.setQuantity(
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE());
        goodsReceiptStoreTransaction.setRemainingQuantity(
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE());
        goodsReceiptStoreTransaction.setRefTransactionId(
                goodsIssueStoreTransactionGeneralModel.getId());
        setDataFromOriginalAddingTransaction(
                goodsReceiptStoreTransaction, goodsIssueStoreTransactionGeneralModel);
    }

    private void setDataFromOriginalAddingTransaction(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
            TObGoodsIssueStoreTransactionGeneralModel tObGoodsIssueStoreTransactionGeneralModel) {

        TObStoreTransactionGeneralModel oldGoodsReceiptStoreTransaction =
                storeTransactionGeneralModelRep.findOneByUserCode(
                        tObGoodsIssueStoreTransactionGeneralModel.getRefTransactionCode());
        goodsReceiptStoreTransaction.setOriginalAddingDate(
                oldGoodsReceiptStoreTransaction.getOriginalAddingDate());
        goodsReceiptStoreTransaction.setEstimateCost(oldGoodsReceiptStoreTransaction.getEstimateCost());
        goodsReceiptStoreTransaction.setActualCost(oldGoodsReceiptStoreTransaction.getActualCost());
    }

    private void addItemDataToStoreTransaction(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel) {

        MObItemGeneralModel itemGeneralModel =
                getItemGeneralModel(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel);
        goodsReceiptStoreTransaction.setItemId(itemGeneralModel.getId());

        CObMeasure cObMeasure = getMeasure(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel);
        goodsReceiptStoreTransaction.setUnitOfMeasureId(cObMeasure.getId());

        StockTypeEnum stockType =
                getStockType(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getStockType());
        goodsReceiptStoreTransaction.setStockType(stockType);
    }

    private void addCommonDataForGoodsReceiptSalesReturnStoreTransaction(
            TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
            DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel) {

        goodsReceiptStoreTransaction.setDocumentReferenceId(goodsReceiptDataGeneralModel.getId());

        goodsReceiptStoreTransaction.setCompanyId(goodsReceiptDataGeneralModel.getCompanyId());

        goodsReceiptStoreTransaction.setPlantId(goodsReceiptDataGeneralModel.getPlantId());

        goodsReceiptStoreTransaction.setStorehouseId(goodsReceiptDataGeneralModel.getStorehouseId());

        goodsReceiptStoreTransaction.setPurchaseUnitId(
                goodsReceiptDataGeneralModel.getPurchaseUnitId());
    }

    private List<TObGoodsIssueStoreTransactionGeneralModel> getOldGoodsIssueStoreTransactions(
            IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel,
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel,
            DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel) {
        return goodsIssueStoreTransactionGeneralModelRep
                .findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
                        goodsIssueRefDocumentDataGeneralModel.getGoodsIssueCode(),
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode(),
                        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode(),
                        goodsReceiptDataGeneralModel.getCompanyCode(),
                        goodsReceiptDataGeneralModel.getStorehouseCode(),
                        goodsReceiptDataGeneralModel.getPlantCode());
    }

    private List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
    getGoodsReceiptSalesReturnItemQuantities(String goodsReceiptCode) {
        return goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
                .findAllByGoodsReceiptCodeOrderByIdAsc(goodsReceiptCode);
    }

    private IObGoodsIssueRefDocumentDataGeneralModel getGoodsIssueRefDocumentDataGeneralModel(
            IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel) {
        return goodsIssueRefDocumentDataGeneralModelRep.findOneByRefDocumentAndGoodsIssueStateContains(
                salesInvoiceBusinessPartnerGeneralModel.getSalesOrder(),
                DObGoodsIssueStateMachine.ACTIVE_STATE);
    }

    private IObSalesInvoiceBusinessPartnerGeneralModel getSalesInvoiceBusinessPartnerGeneralModel(
            IObSalesReturnOrderDetailsGeneralModel salesReturnOrderDetailsGeneralModel) {
        return salesInvoiceBusinessPartnerGeneralModelRep.findOneByCode(
                salesReturnOrderDetailsGeneralModel.getSalesInvoiceCode());
    }

    private IObSalesReturnOrderDetailsGeneralModel getSalesReturnOrderDetailsGeneralModel(
            IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel) {
        return salesReturnOrderDetailsGeneralModelRep.findOneBySalesReturnOrderCode(
                goodsReceiptSalesReturnDataGeneralModel.getSalesReturnOrderCode());
    }

    private IObGoodsReceiptSalesReturnDataGeneralModel getGoodsReceiptSalesReturnDataGeneralModel(
            String goodsReceiptCode) {
        return goodsReceiptSalesReturnDataGeneralModelRep.findOneByGoodsReceiptCode(goodsReceiptCode);
    }

    private DObGoodsReceiptDataGeneralModel getGoodsReceiptDataGeneralModel(String goodsReceiptCode) {
        return goodsReceiptDataGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    }

    private StockTypeEnum getStockType(String stockTypeCode) {
        StockTypeEnum.StockType stockTypeEnumObject = StockTypeEnum.StockType.valueOf(stockTypeCode);
        StockTypeEnum stockTypeEnum = new StockTypeEnum();
        stockTypeEnum.setId(stockTypeEnumObject);
        return stockTypeEnum;
    }

    private CObMeasure getMeasure(
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel) {
        return measureRep.findOneByUserCode(
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode());
    }

    private MObItemGeneralModel getItemGeneralModel(
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel) {
        return itemGeneralModelRep.findByUserCode(
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode());
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setGoodsReceiptStoreTransactionRep(
            TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep) {
        this.goodsReceiptStoreTransactionRep = goodsReceiptStoreTransactionRep;
    }

    public void setGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep(
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
                    goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep) {
        this.goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep =
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;
    }

    public void setGoodsReceiptSalesReturnDataGeneralModelRep(
            IObGoodsReceiptSalesReturnDataGeneralModelRep goodsReceiptSalesReturnDataGeneralModelRep) {
        this.goodsReceiptSalesReturnDataGeneralModelRep = goodsReceiptSalesReturnDataGeneralModelRep;
    }

    public void setSalesReturnOrderDetailsGeneralModelRep(
            IObSalesReturnOrderDetailsGeneralModelRep salesReturnOrderDetailsGeneralModelRep) {
        this.salesReturnOrderDetailsGeneralModelRep = salesReturnOrderDetailsGeneralModelRep;
    }

    public void setSalesInvoiceBusinessPartnerGeneralModelRep(
            IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep) {
        this.salesInvoiceBusinessPartnerGeneralModelRep = salesInvoiceBusinessPartnerGeneralModelRep;
    }

    public void setGoodsIssueRefDocumentDataGeneralModelRep(
            IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep) {
        this.goodsIssueRefDocumentDataGeneralModelRep = goodsIssueRefDocumentDataGeneralModelRep;
    }

    public void setGoodsIssueStoreTransactionGeneralModelRep(
            TObGoodsIssueStoreTransactionGeneralModelRep goodsIssueStoreTransactionGeneralModelRep) {
        this.goodsIssueStoreTransactionGeneralModelRep = goodsIssueStoreTransactionGeneralModelRep;
    }

    public void setStoreTransactionGeneralModelRep(
            TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep) {
        this.storeTransactionGeneralModelRep = storeTransactionGeneralModelRep;
    }

    public void setGoodsReceiptDataGeneralModelRep(
            DObGoodsReceiptDataGeneralModelRep goodsReceiptDataGeneralModelRep) {
        this.goodsReceiptDataGeneralModelRep = goodsReceiptDataGeneralModelRep;
    }

    public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
        this.itemGeneralModelRep = itemGeneralModelRep;
    }

    public void setMeasureRep(CObMeasureRep measureRep) {
        this.measureRep = measureRep;
    }

    public void setGoodsReceiptPurchaseOrderCreateStoreTransactionService(
            GoodsReceiptPurchaseOrderCreateStoreTransactionService
                    goodsReceiptPurchaseOrderCreateStoreTransactionService) {
        this.goodsReceiptPurchaseOrderCreateStoreTransactionService =
                goodsReceiptPurchaseOrderCreateStoreTransactionService;
    }

    public void setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
            GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
                    goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService) {
        this.goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService =
                goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
    }
}
