package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.dbo.jpa.valueobjects.IObBusinessPartnerValueObject;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartner;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerRep;
import io.reactivex.Observable;
import javax.transaction.Transactional;

public class IObSalesInvoiceBusinessPartnerSaveCommand implements ICommand<IObBusinessPartnerValueObject> {
	private IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep;
	private DObSalesInvoiceRep salesInvoiceRep;
	private CObPaymentTermsRep paymentTermRep;
	private CObCurrencyRep currencyRep;
	private CommandUtils commandUtils;

	public IObSalesInvoiceBusinessPartnerSaveCommand() {
		commandUtils = new CommandUtils();
	}

	@Transactional
	@Override
	public <T> Observable<T> executeCommand(IObBusinessPartnerValueObject valueObject)
					throws Exception {

		DObSalesInvoice salesInvoice = salesInvoiceRep
						.findOneByUserCode(valueObject.getSalesInvoiceCode());

		IObSalesInvoiceBusinessPartner salesInvoiceBusinessPartner = salesInvoiceBusinessPartnerRep
						.findOneByRefInstanceId(salesInvoice.getId());

		setBusinessPartnerData(salesInvoiceBusinessPartner, valueObject);

		salesInvoice.setHeaderDetail(IDObSalesInvoice.businessPartner, salesInvoiceBusinessPartner);

		commandUtils.setCreationAndModificationInfoAndDate(salesInvoiceBusinessPartner);
		commandUtils.setModificationInfoAndModificationDate(salesInvoice);
		salesInvoiceRep.updateAndClear(salesInvoice);
		return Observable.empty();
	}

	private void setBusinessPartnerData(IObSalesInvoiceBusinessPartner salesInvoiceBusinessPartner,
					IObBusinessPartnerValueObject businessPartnerValueObject) {
		setPaymentTerm(salesInvoiceBusinessPartner,
						businessPartnerValueObject.getPaymentTermCode());
		setCurrencyData(salesInvoiceBusinessPartner, businessPartnerValueObject.getCurrencyCode());
	}

	private void setCurrencyData(IObSalesInvoiceBusinessPartner salesInvoiceBusinessPartner,
					String currencyCode) {
		if (currencyCode != null) {
			CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
			salesInvoiceBusinessPartner.setCurrencyId(currency.getId());
		} else {
			salesInvoiceBusinessPartner.setCurrencyId(null);
		}
	}

	private void setPaymentTerm(IObSalesInvoiceBusinessPartner salesInvoiceBusinessPartner,
					String paymentTermCode) {
		CObPaymentTerms paymentTerms = paymentTermRep.findOneByUserCode(paymentTermCode);
		Long paymentTermsId = null;
		if (paymentTerms != null) {
			paymentTermsId = paymentTerms.getId();
		}
		salesInvoiceBusinessPartner.setPaymentTermId(paymentTermsId);
	}

	public void setSalesInvoiceBusinessPartnerRep(
					IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep) {
		this.salesInvoiceBusinessPartnerRep = salesInvoiceBusinessPartnerRep;
	}

	public void setSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
		this.salesInvoiceRep = salesInvoiceRep;
	}

	public void setPaymentTermRep(CObPaymentTermsRep paymentTermRep) {
		this.paymentTermRep = paymentTermRep;
	}

	public void setCurrencyRep(CObCurrencyRep currencyRep) {
		this.currencyRep = currencyRep;
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}
}
