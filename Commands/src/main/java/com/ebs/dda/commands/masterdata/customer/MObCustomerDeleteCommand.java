package com.ebs.dda.commands.masterdata.customer;

import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerDeletionValueObject;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import io.reactivex.Observable;
import org.springframework.transaction.TransactionSystemException;

public class MObCustomerDeleteCommand implements ICommand<MObCustomerDeletionValueObject> {

  private MObCustomerRep customerRep;

  @Override
  public Observable<String> executeCommand(MObCustomerDeletionValueObject valueObject)
      throws Exception {
    try {
      MObCustomer customer = readEntityByCode(valueObject.getCustomerCode());
      customerRep.delete(customer);
    } catch (TransactionSystemException e) {
      throw new DependentInstanceException();
    }
    return Observable.just("SUCCESS");
  }

  public void checkIfInstanceExists(String userCode) throws InstanceNotExistException {
    MObCustomer customer = readEntityByCode(userCode);
    if (customer == null) {
      throw new InstanceNotExistException();
    }
  }

  private MObCustomer readEntityByCode(String userCode) {
    return customerRep.findOneByUserCode(userCode);
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }
}
