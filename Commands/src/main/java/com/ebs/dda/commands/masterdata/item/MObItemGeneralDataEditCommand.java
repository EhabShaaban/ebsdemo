package com.ebs.dda.commands.masterdata.item;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.IObItemPurchaseUnit;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.productmanager.CObProductManager;
import com.ebs.dda.masterdata.item.IMObItemSectionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.IObItemPurchaseUnitRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import io.reactivex.Observable;

import java.util.List;
import java.util.Locale;

public class MObItemGeneralDataEditCommand implements ICommand<MObItemGeneralDataValueObject> {

  private final CommandUtils commandUtils;
  private MObItemRep itemRep;
  private CObProductManagerRep productManagerRep;

  private EntityLockCommand entityLockCommand;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private IObItemPurchaseUnitRep itemPurchaseUnitRep;

  public MObItemGeneralDataEditCommand() {
    commandUtils = new CommandUtils();
  }

  public void lock(String resourceCode) throws Exception {
    entityLockCommand.lockSection(resourceCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
  }

  public void unlock(String itemCode) throws Exception {
    entityLockCommand.unlockSection(itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  @Override
  public Observable<String> executeCommand(MObItemGeneralDataValueObject valueObject)
      throws Exception {
    String itemCode = valueObject.getItemCode();
    String itemName = valueObject.getItemName();
    String itemMarketName = valueObject.getMarketName();
    String itemProductManagerCode = valueObject.getProductManagerCode();
    List<String> purchaseUnitList = valueObject.getPurchasingUnitCodes();

    MObItem item = itemRep.findOneByUserCode(itemCode);

    setItemName(item, itemName);
    setItemMarketName(item, itemMarketName);
    setItemPurchaseUnitsData(item, purchaseUnitList);
    setItemProductManagerData(item, itemProductManagerCode);

    commandUtils.setModificationInfoAndModificationDate(item);
    itemRep.updateAndClear(item);
    return Observable.just("SUCCESS");
  }

  private void setItemProductManagerData(MObItem item, String itemProductManagerCode) {
    if (itemProductManagerCode == null) {
      item.setProductManagerId(null);
      return;
    }
    CObProductManager productManager = productManagerRep.findOneByUserCode(itemProductManagerCode);
    item.setProductManagerId(productManager.getId());
  }

  private void setItemName(MObItem item, String itemName) {
    LocalizedString localizedItemName = new LocalizedString();
    localizedItemName.setValue(new Locale("ar"), itemName);
    localizedItemName.setValue(new Locale("en"), itemName);
    item.setName(localizedItemName);
  }

  private void setItemMarketName(MObItem item, String itemMarketName) {
    item.setMarketName(itemMarketName);
  }

  private void setItemPurchaseUnitsData(MObItem item, List<String> purchaseUnitList)
      throws Exception {
    for (String purchaseUnitCode : purchaseUnitList) {

      Long purchaseUnitId = getPurchaseUnitId(purchaseUnitCode);
      IObItemPurchaseUnit itemPurchaseUnit =
          itemPurchaseUnitRep.findByRefInstanceIdAndPurchaseUnitId(item.getId(), purchaseUnitId);
      if (itemPurchaseUnit == null) {
        itemPurchaseUnit = new IObItemPurchaseUnit();
        itemPurchaseUnit.setRefInstanceId(item.getId());
        itemPurchaseUnit.setPurchaseUnitId(purchaseUnitId);
        commandUtils.setCreationAndModificationInfoAndDate(itemPurchaseUnit);
      }
      item.addMultipleDetail(IMObItem.purchseUnitAttr, itemPurchaseUnit);
    }
  }

  private Long getPurchaseUnitId(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchaseUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    return purchaseUnitGeneralModel.getId();
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setProductManagerRep(CObProductManagerRep productManagerRep) {
    this.productManagerRep = productManagerRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setItemPurchaseUnitRep(IObItemPurchaseUnitRep itemPurchaseUnitRep) {
    this.itemPurchaseUnitRep = itemPurchaseUnitRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
