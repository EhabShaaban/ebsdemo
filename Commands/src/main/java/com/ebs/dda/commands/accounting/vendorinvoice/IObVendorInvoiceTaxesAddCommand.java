package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.taxes.LObVendorTaxes;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxes;
import com.ebs.dda.repositories.accounting.taxes.LObVendorTaxesRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class IObVendorInvoiceTaxesAddCommand implements ICommand<DObVendorInvoice> {
	private CommandUtils commandUtils;
	private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
	private IObVendorInvoiceTaxesRep taxesRep;
	private LObVendorTaxesRep lObVendorTaxesRep;
	private IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep;

	public IObVendorInvoiceTaxesAddCommand() {
		commandUtils = new CommandUtils();
	}

	@Transactional
	@Override
	public Observable<String> executeCommand(DObVendorInvoice vendorInvoice) throws Exception {

		List<IObVendorInvoiceTaxes> vendorInvoiceTaxes = addVendorTaxesToVendorInvoice(
						vendorInvoice);
		taxesRep.saveAll(vendorInvoiceTaxes);
		return Observable.just("SUCCESS");
	}

  private List<IObVendorInvoiceTaxes> addVendorTaxesToVendorInvoice(
    DObVendorInvoice vendorInvoice) {

    IObVendorInvoiceDetails vendorInvoiceDetails =
      vendorInvoiceDetailsRep.findOneByRefInstanceId(vendorInvoice.getId());

    return lObVendorTaxesRep
      .findAllByVendorId(vendorInvoiceDetails.getVendorId())
      .stream()
      .map(tax -> createTaxEntity(vendorInvoice,tax))
      .collect(Collectors.toList());
	}

	private IObVendorInvoiceTaxes createTaxEntity(DObVendorInvoice vendorInvoice,
       LObVendorTaxes vendorTax) {

    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems = vendorInvoiceItemsGeneralModelRep
      .findByInvoiceCode(vendorInvoice.getUserCode());
    BigDecimal totalItemsAmount = getTotalItemsAmount(vendorInvoiceItems);

    IObVendorInvoiceTaxes vendorInvoiceTax = new IObVendorInvoiceTaxes();
		commandUtils.setCreationAndModificationInfoAndDate(vendorInvoiceTax);
		vendorInvoiceTax.setRefInstanceId(vendorInvoice.getId());

		vendorInvoiceTax.setName(vendorTax.getName());
		vendorInvoiceTax.setCode(vendorTax.getUserCode());

		BigDecimal taxAmount = vendorTax.getPercentage().multiply(totalItemsAmount).divide(BigDecimal.valueOf(100));
		vendorInvoiceTax.setAmount(taxAmount);
		return vendorInvoiceTax;
	}

	private BigDecimal getTotalItemsAmount(List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems) {
		return vendorInvoiceItems
			.stream()
			.map(item ->
				item.getQuantityInOrderUnit()
					.multiply(item.getPrice())
					.multiply(item.getConversionFactorToBase())
			)
			.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setVendorInvoiceItemsGeneralModelRep(
					IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
		this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
	}

	public void setTaxesRep(IObVendorInvoiceTaxesRep taxesRep) {
		this.taxesRep = taxesRep;
	}

	public void setlObVendorTaxesRep(LObVendorTaxesRep lObVendorTaxesRep) {
		this.lObVendorTaxesRep = lObVendorTaxesRep;
	}

	public void setVendorInvoiceDetailsRep(IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep) {
		this.vendorInvoiceDetailsRep = vendorInvoiceDetailsRep;
	}
}
