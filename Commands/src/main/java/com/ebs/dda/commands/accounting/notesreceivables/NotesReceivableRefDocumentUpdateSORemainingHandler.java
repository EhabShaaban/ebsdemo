package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;

import java.math.BigDecimal;

public class NotesReceivableRefDocumentUpdateSORemainingHandler
        extends NotesReceivableRefDocumentUpdateRemainingHandler {

    private DObSalesOrderRep salesOrderRep;
    private IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep;

    public void updateRemaining(String refDocumentCode, BigDecimal amountToCollect, String nrCurrencyIso) throws Exception {
        DObSalesOrder salesOrder = getSalesOrderRefDocument(refDocumentCode);
        String soCurrencyIso = getSalesOrderCurrencyIso(salesOrder.getId());
        BigDecimal amount = getAmountToCollectWithExchangeRate(nrCurrencyIso, soCurrencyIso, amountToCollect);
        subtractAmountToCollectFromSalesOrderRemaining(salesOrder, amount);
    }

    private DObSalesOrder getSalesOrderRefDocument(String refDocumentCode) {
        return salesOrderRep.findOneByUserCode(refDocumentCode);
    }

    private String getSalesOrderCurrencyIso(Long salesOrderId) {
        return salesOrderDataGeneralModelRep.findOneByRefInstanceId(salesOrderId).getCurrencyIso();
    }

    private void subtractAmountToCollectFromSalesOrderRemaining(
            DObSalesOrder salesOrder, BigDecimal amountToCollect) {
        salesOrder.setRemaining(salesOrder.getRemaining().subtract(amountToCollect));
        commandUtils.setModificationInfoAndModificationDate(salesOrder);
        salesOrderRep.save(salesOrder);
    }

    public void setDObSalesOrderRep(DObSalesOrderRep salesOrderRep) {
        this.salesOrderRep = salesOrderRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setSalesOrderDataGeneralModelRep(IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep) {
        this.salesOrderDataGeneralModelRep = salesOrderDataGeneralModelRep;
    }
}
