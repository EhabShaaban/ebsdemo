package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentBankAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentVendorAccountingDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;


public class DObBankPaymentForVendorAccountDeterminationCommand extends DObPaymentRequestAccountDeterminationCommand {
    private MObVendorRep vendorRep;
    private IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep;
    private LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;
    private HObChartOfAccountRep chartOfAccountRep;

    @Override
    protected void determineCreditAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
        String creditAccount = ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT;
        LObGlobalGLAccountConfigGeneralModel creditGlobalGLAccountAccount = globalAccountConfigGeneralModelRep
                .findOneByUserCode(creditAccount);

        IObAccountingDocumentBankAccountingDetails creditAccountingDetails = new IObAccountingDocumentBankAccountingDetails();
        creditAccountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
        creditAccountingDetails.setGlAccountId(creditGlobalGLAccountAccount.getGlAccountId());
        creditAccountingDetails.setObjectTypeCode(IDocumentTypes.PAYMENT_REQUEST);
        creditAccountingDetails.setGlSubAccountId(paymentDetails.getBankAccountId());
        creditAccountingDetails.setAmount(paymentDetails.getNetAmount());

        commandUtils.setCreationAndModificationInfoAndDate(creditAccountingDetails);
        paymentRequest.addLineDetail(IDObPaymentRequest.accountingBankDetailsAttr, creditAccountingDetails);
    }

    @Override
    protected void determineDebitAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
        IObVendorAccountingInfoGeneralModel vendorAccountingInfo = vendorAccountingInfoRep
                .findByVendorCode(paymentDetails.getBusinessPartnerCode());
        MObVendor vendor = vendorRep.findOneByUserCode(paymentDetails.getBusinessPartnerCode());

        IObAccountingDocumentVendorAccountingDetails debitAccountingDetails = new IObAccountingDocumentVendorAccountingDetails();
        debitAccountingDetails.setObjectTypeCode(IDocumentTypes.PAYMENT_REQUEST);
        setAccountingDetailsDataIfVendorAccountingNotNull(vendorAccountingInfo, debitAccountingDetails);
        debitAccountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
        debitAccountingDetails.setGlSubAccountId(vendor.getId());
        debitAccountingDetails.setAmount(paymentDetails.getNetAmount());

        commandUtils.setCreationAndModificationInfoAndDate(debitAccountingDetails);
        paymentRequest.addLineDetail(IDObPaymentRequest.accountingVendorDetailsAttr, debitAccountingDetails);
    }

    private void setAccountingDetailsDataIfVendorAccountingNotNull(
            IObVendorAccountingInfoGeneralModel vendorAccountingInfo,
            IObAccountingDocumentVendorAccountingDetails accountingDetails) {
        if (vendorAccountingInfo != null) {
            String accountCode = vendorAccountingInfo.getAccountCode();
            HObGLAccount account = chartOfAccountRep.findOneByUserCode(accountCode);
            accountingDetails.setGlAccountId(account.getId());
            accountingDetails.setGlSubAccountId(vendorAccountingInfo.getRefInstanceId());
        }
    }

    public void setVendorRep(MObVendorRep vendorRep) {
        this.vendorRep = vendorRep;
    }

    public void setVendorAccountingInfoRep(IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep) {
        this.vendorAccountingInfoRep = vendorAccountingInfoRep;
    }

    public void setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
        this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
    }

    public void setChartOfAccountRep(HObChartOfAccountRep chartOfAccountRep) {
        this.chartOfAccountRep = chartOfAccountRep;
    }
}
