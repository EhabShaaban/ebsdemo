package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import io.reactivex.Observable;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class DObSalesInvoicePdfGeneratorCommand implements ICommand {
  public String SALES_INVOICE_CODE = "salesInvoiceCode";
  public String ITEMS_NUMBER = "itemsNumber";
  private DataSource dataSource;
  private IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep;

  @Override
  public Observable executeCommand(Object salesInvoiceCode) throws Exception {

    List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItems =
        salesInvoiceItemsGeneralModelRep.findByInvoiceCode(salesInvoiceCode.toString());

    InputStream jrxmlFile =
        DObSalesInvoicePdfGeneratorCommand.class.getResourceAsStream("/salesInvoice.jrxml");
    HashMap reportParameters = new HashMap();
    reportParameters.put(SALES_INVOICE_CODE, salesInvoiceCode);
    reportParameters.put(ITEMS_NUMBER, salesInvoiceItems.size());

    JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFile);

    JasperPrint report =
        JasperFillManager.fillReport(jasperReport, reportParameters, dataSource.getConnection());
    report.addStyle(prepareNormalStyle());

    byte[] pdf = JasperExportManager.exportReportToPdf(report);
    return Observable.just(new String(Base64.encodeBase64(pdf), "UTF-8"));
  }

  JRDesignStyle prepareNormalStyle() {
    JRDesignStyle jrDesignStyle = new JRDesignStyle();
    jrDesignStyle.setDefault(true);
    jrDesignStyle.setPdfFontName("/arial.ttf");
    jrDesignStyle.setPdfEncoding("Identity-H");
    return jrDesignStyle;
  }

  public void setSalesInvoiceItemsGeneralModelRep(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
    this.salesInvoiceItemsGeneralModelRep = salesInvoiceItemsGeneralModelRep;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
}
