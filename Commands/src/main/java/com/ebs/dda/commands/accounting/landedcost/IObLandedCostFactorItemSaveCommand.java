package com.ebs.dda.commands.accounting.landedcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.IObAddLandedCostFactorItemValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItems;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;

public class IObLandedCostFactorItemSaveCommand implements ICommand<IObAddLandedCostFactorItemValueObject> {
  private CommandUtils commandUtils;
  private DObLandedCostRep landedCostRep;
  private MObItemGeneralModelRep mObItemGeneralModelRep;

  public IObLandedCostFactorItemSaveCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(IObAddLandedCostFactorItemValueObject landedCostFactorItemValueObject)
      throws Exception {
    DObLandedCost  landedCost=
            landedCostRep.findOneByUserCode(landedCostFactorItemValueObject.getLandedCostCode());
    IObLandedCostFactorItems landedCostItem = getLandedCostItemInstance(landedCostFactorItemValueObject);
    prepareLandedCostItem(landedCost, landedCostItem);

    commandUtils.setModificationInfoAndModificationDate(landedCost);
    landedCostRep.update(landedCost);
    return Observable.just(landedCost.getUserCode());
  }

  private IObLandedCostFactorItems getLandedCostItemInstance(
          IObAddLandedCostFactorItemValueObject landedCostFactorItemValueObject) {
    IObLandedCostFactorItems landedCostItem = new IObLandedCostFactorItems();

    MObItemGeneralModel item =
        mObItemGeneralModelRep.findByUserCode(landedCostFactorItemValueObject.getItemCode());

    landedCostItem.setCostItemId(item.getId());
    landedCostItem.setEstimateValue(landedCostFactorItemValueObject.getValue());
    landedCostItem.setActualValue(null);
    return landedCostItem;
  }

  private void prepareLandedCostItem(
      DObLandedCost landedCost, IObLandedCostFactorItems landedCostItem) {
    landedCostItem.setRefInstanceId(landedCost.getId());
    commandUtils.setCreationAndModificationInfoAndDate(landedCostItem);
    landedCost.addLineDetail(IDObLandedCost.costFactorItems, landedCostItem);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setmObItemGeneralModelRep(MObItemGeneralModelRep mObItemGeneralModelRep) {
    this.mObItemGeneralModelRep = mObItemGeneralModelRep;
  }

  public void setLandedCostRep(DObLandedCostRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }
}
