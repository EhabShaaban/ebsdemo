package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import io.reactivex.Observable;

public class PurchaseOrderDeleteCommand implements ICommand {

  private DObOrderDocumentRep orderDocumentRep;

  @Override
  public Observable<String> executeCommand(Object object) throws Exception {
    String userCode = (String) object;
    DObOrderDocument purchaseOrder = orderDocumentRep.findOneByUserCode(userCode);
    orderDocumentRep.delete(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  public void setOrderDocumentRep(DObOrderDocumentRep orderDocumentRep) {
    this.orderDocumentRep = orderDocumentRep;
  }
}
