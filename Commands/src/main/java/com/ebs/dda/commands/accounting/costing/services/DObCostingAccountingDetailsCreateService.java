package com.ebs.dda.commands.accounting.costing.services;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.factories.IObAccountingDocumentAccountingDetailsFactory;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class DObCostingAccountingDetailsCreateService {

  private final CommandUtils commandUtils;

  private final IObLandedCostFactorItemsSummaryGeneralModelRep
      landedCostFactorItemsSummaryGeneralModelRep;
  private final LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;
  private final IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
  private final IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep;
  private final IObAccountingDocumentActivationDetailsGeneralModelRep
      accountingDocumentActivationDetailsGeneralModelRep;
  private final IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;
  private final DObLandedCostGeneralModelRep landedCostGeneralModelRep;

  public DObCostingAccountingDetailsCreateService(
      IObLandedCostFactorItemsSummaryGeneralModelRep landedCostFactorItemsSummaryGeneralModelRep,
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
      IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
      IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep,
      IObAccountingDocumentActivationDetailsGeneralModelRep
          accountingDocumentActivationDetailsGeneralModelRep,
      IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils = new CommandUtils();
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    this.landedCostFactorItemsSummaryGeneralModelRep = landedCostFactorItemsSummaryGeneralModelRep;
    this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
    this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
    this.itemAccountingInfoGeneralModelRep = itemAccountingInfoGeneralModelRep;
    this.accountingDocumentActivationDetailsGeneralModelRep =
        accountingDocumentActivationDetailsGeneralModelRep;
    this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public List<IObAccountingDocumentAccountingDetails> getCostingAccountingDetails(
      DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel,
      BigDecimal actualCostingCurrencyPrice,
      String purchaseOrderCode) {
    String vendorInvoiceCode = vendorInvoiceGeneralModel.getUserCode();
    BigDecimal invoiceSummaryTotalAmount = getInvoiceSummaryTotalAmount(vendorInvoiceGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesNotLike(
            purchaseOrderCode, "%" + DObLandedCostStateMachine.DRAFT_STATE + "%");

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        landedCostFactorItemsSummaryGeneralModelRep.findOneByCode(
            landedCostGeneralModel.getUserCode());

    List<IObVendorInvoiceItemsGeneralModel> invoiceItemsGeneralModels =
        vendorInvoiceItemsGeneralModelRep.findByInvoiceCode(vendorInvoiceCode);

    IObAccountingDocumentActivationDetailsGeneralModel vendorInvoiceActivationDetailsGeneralModel =
        accountingDocumentActivationDetailsGeneralModelRep.findOneByCodeAndObjectTypeCode(
            vendorInvoiceCode, "VendorInvoice");

    BigDecimal realizedExChangeRate =
        vendorInvoiceActivationDetailsGeneralModel
            .getCurrencyPrice()
            .subtract(actualCostingCurrencyPrice);

    boolean isNotZeroRealizedExchangeRate = realizedExChangeRate.compareTo(BigDecimal.ZERO) != 0;

    List<IObAccountingDocumentAccountingDetails> costingAccountingDetails = new ArrayList<>();

    IObAccountingDocumentAccountingDetails purchasingAccountingDetail =
        getPurchasingAccountingDetail(
            landedCostFactorItemsSummary.getActualTotalAmount(),
            invoiceSummaryTotalAmount,
            actualCostingCurrencyPrice);

    IObAccountingDocumentAccountingDetails purchaseOrderAccountingDetail =
        getPurchaseOrderAccountingDetail(
            landedCostFactorItemsSummary.getActualTotalAmount(),
            landedCostGeneralModel.getPurchaseOrderId());

    List<IObAccountingDocumentAccountingDetails> costingItemsAccountingDetails = new ArrayList<>();

    for (IObVendorInvoiceItemsGeneralModel invoiceItemGeneralModel : invoiceItemsGeneralModels) {
      IObItemAccountingInfoGeneralModel itemAccountingInfoGeneralModel =
          itemAccountingInfoGeneralModelRep.findByItemCode(invoiceItemGeneralModel.getItemCode());
      BigDecimal itemTotalPriceInLocalCurrencyWithCostingExchangeRate =
          getItemTotalPriceInLocalCurrency(invoiceItemGeneralModel, actualCostingCurrencyPrice);

      IObAccountingDocumentAccountingDetails itemAccountingDetail =
          getItemAccountingDetail(itemAccountingInfoGeneralModel, landedCostGeneralModel);
      setAccountingDetailAmountAndEntry(
          itemAccountingDetail, itemTotalPriceInLocalCurrencyWithCostingExchangeRate);
      costingItemsAccountingDetails.add(itemAccountingDetail);
      if (isNotZeroRealizedExchangeRate) {
        BigDecimal itemTotalPriceInLocalCurrencyWithRealizedExchangeRate =
            getItemTotalPriceInLocalCurrency(invoiceItemGeneralModel, realizedExChangeRate);

        IObAccountingDocumentAccountingDetails itemRealizedAccountingDetail =
            getItemAccountingDetail(itemAccountingInfoGeneralModel, landedCostGeneralModel);
        setAccountingDetailAmountAndEntry(
            itemRealizedAccountingDetail, itemTotalPriceInLocalCurrencyWithRealizedExchangeRate);
        costingItemsAccountingDetails.add(itemRealizedAccountingDetail);
      }
    }

    if (isNotZeroRealizedExchangeRate) {
      BigDecimal vendorInvoiceTotalAmountInLocalCurrencyWithRealizedExchangeRate =
          invoiceSummaryTotalAmount.multiply(realizedExChangeRate);

      IObAccountingDocumentAccountingDetails realizedExchangeAccountingDetail =
          getRealizedExchangeRateAccountingDetail();
      setRealizedAccountingDetailAmountAndEntry(
          realizedExchangeAccountingDetail,
          vendorInvoiceTotalAmountInLocalCurrencyWithRealizedExchangeRate);
      costingAccountingDetails.add(realizedExchangeAccountingDetail);
    }

    costingAccountingDetails.add(purchasingAccountingDetail);
    costingAccountingDetails.add(purchaseOrderAccountingDetail);
    costingAccountingDetails.addAll(costingItemsAccountingDetails);
    return costingAccountingDetails;
  }

  private BigDecimal getInvoiceSummaryTotalAmount(
      DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel) {
    IObVendorInvoiceSummary vendorInvoiceSummary =
        vendorInvoiceSummaryRep.findOneByRefInstanceId(vendorInvoiceGeneralModel.getId());
    return vendorInvoiceSummary.getTotalAmount();
  }

  private IObAccountingDocumentAccountingDetails getPurchasingAccountingDetail(
      BigDecimal landedCostActualTotalAmount, BigDecimal totalAmount, BigDecimal currencyPrice) {
    LObGlobalGLAccountConfigGeneralModel purchasingAccountConfigGeneralModel =
        getGLAccountConfigGeneralModel(ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT);

    BigDecimal vendorInvoiceTotalAmountInLocalCurrency = totalAmount.multiply(currencyPrice);
    BigDecimal totalPurchasingAmount =
        vendorInvoiceTotalAmountInLocalCurrency.add(landedCostActualTotalAmount);

    IObAccountingDocumentAccountingDetails purchasingDetail =
        IObAccountingDocumentAccountingDetailsFactory.initAccountingDetails(null);
    commandUtils.setCreationAndModificationInfoAndDate(purchasingDetail);
    purchasingDetail.setObjectTypeCode(IDocumentTypes.COSTING);
    purchasingDetail.setAccountingEntry(AccountingEntry.DEBIT.name());
    purchasingDetail.setGlAccountId(purchasingAccountConfigGeneralModel.getGlAccountId());
    purchasingDetail.setAmount(totalPurchasingAmount);
    return purchasingDetail;
  }

  private IObAccountingDocumentAccountingDetails getPurchaseOrderAccountingDetail(
      BigDecimal landedCostActualTotalAmount, Long purchaseOrderId) {
    LObGlobalGLAccountConfigGeneralModel purchaseOrderAccountConfigGeneralModel =
        getGLAccountConfigGeneralModel(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);

    IObAccountingDocumentAccountingDetails purchaseOrderDetail =
        IObAccountingDocumentAccountingDetailsFactory.initAccountingDetails(
            SubLedgers.PO.toString());
    commandUtils.setCreationAndModificationInfoAndDate(purchaseOrderDetail);
    purchaseOrderDetail.setObjectTypeCode(IDocumentTypes.COSTING);
    purchaseOrderDetail.setAccountingEntry(AccountingEntry.CREDIT.name());
    purchaseOrderDetail.setGlAccountId(purchaseOrderAccountConfigGeneralModel.getGlAccountId());
    purchaseOrderDetail.setGlSubAccountId(purchaseOrderId);
    purchaseOrderDetail.setAmount(landedCostActualTotalAmount);
    return purchaseOrderDetail;
  }

  private IObAccountingDocumentAccountingDetails getRealizedExchangeRateAccountingDetail() {
    LObGlobalGLAccountConfigGeneralModel realizedAccountConfigGeneralModel =
        getGLAccountConfigGeneralModel(ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);

    IObAccountingDocumentAccountingDetails realizedExchangeRateDetail =
        IObAccountingDocumentAccountingDetailsFactory.initAccountingDetails(null);
    commandUtils.setCreationAndModificationInfoAndDate(realizedExchangeRateDetail);
    realizedExchangeRateDetail.setObjectTypeCode(IDocumentTypes.COSTING);
    realizedExchangeRateDetail.setGlAccountId(realizedAccountConfigGeneralModel.getGlAccountId());
    return realizedExchangeRateDetail;
  }

  private BigDecimal getItemTotalPriceInLocalCurrency(
      IObVendorInvoiceItemsGeneralModel invoiceItemGeneralModel, BigDecimal currencyPrice) {
    return invoiceItemGeneralModel.getTotalAmount().multiply(currencyPrice);
  }

  private IObAccountingDocumentAccountingDetails getItemAccountingDetail(
      IObItemAccountingInfoGeneralModel itemAccountingInfoGeneralModel,
      DObLandedCostGeneralModel landedCostGeneralModel) {

    IObAccountingDocumentAccountingDetails itemAccountingDetail =
        IObAccountingDocumentAccountingDetailsFactory.initAccountingDetails(
            SubLedgers.PO.toString());
    commandUtils.setCreationAndModificationInfoAndDate(itemAccountingDetail);
    itemAccountingDetail.setObjectTypeCode(IDocumentTypes.COSTING);
    itemAccountingDetail.setGlAccountId(itemAccountingInfoGeneralModel.getAccountId());
    itemAccountingDetail.setGlSubAccountId(landedCostGeneralModel.getPurchaseOrderId());
    return itemAccountingDetail;
  }

  void setAccountingDetailAmountAndEntry(
      IObAccountingDocumentAccountingDetails accountingDetail,
      BigDecimal totalPriceInLocalCurrency) {
    if (totalPriceInLocalCurrency.compareTo(BigDecimal.ZERO) > 0) {
      accountingDetail.setAccountingEntry(AccountingEntry.CREDIT.name());
      accountingDetail.setAmount(totalPriceInLocalCurrency);
    } else {
      accountingDetail.setAccountingEntry(AccountingEntry.DEBIT.name());
      accountingDetail.setAmount(totalPriceInLocalCurrency.abs());
    }
  }

  void setRealizedAccountingDetailAmountAndEntry(
      IObAccountingDocumentAccountingDetails accountingDetail,
      BigDecimal totalPriceInLocalCurrency) {
    if (totalPriceInLocalCurrency.compareTo(BigDecimal.ZERO) > 0) {
      accountingDetail.setAccountingEntry(AccountingEntry.DEBIT.name());
      accountingDetail.setAmount(totalPriceInLocalCurrency);
    } else {
      accountingDetail.setAccountingEntry(AccountingEntry.CREDIT.name());
      accountingDetail.setAmount(totalPriceInLocalCurrency.abs());
    }
  }

  private LObGlobalGLAccountConfigGeneralModel getGLAccountConfigGeneralModel(String userCode) {
    return globalAccountConfigGeneralModelRep.findOneByUserCode(userCode);
  }
}
