package com.ebs.dda.commands.masterdata.itemvendorrecord;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.masterdata.ivr.IItemVendorRecordSectionNames;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;
import io.reactivex.Observable;

// Created By Niveen Magdy @ 02/04/19
public class ItemVendorRecoredGeneralDataEditCommand implements ICommand<IValueObject> {

  private EntityLockCommand<ItemVendorRecord> entityLockCommand;

  @Override
  public Observable<String> executeCommand(IValueObject object)
      throws Exception {
    return Observable.just("SUCCESS");
  }

  public LockDetails lockSection(String resourceCode) throws Exception {
    LockDetails lockDetails =
        entityLockCommand.lockSection(
            resourceCode, IItemVendorRecordSectionNames.GENERAL_DATA_SECTION);
    return lockDetails;
  }

  public void unlockSection(String vendorCode) throws Exception {
    entityLockCommand.unlockSection(vendorCode, IItemVendorRecordSectionNames.GENERAL_DATA_SECTION);
  }

  public void setEntityLockCommand(EntityLockCommand<ItemVendorRecord> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }
}
