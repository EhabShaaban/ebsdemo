package com.ebs.dda.commands.masterdata.itemvendorrecord;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import io.reactivex.Observable;
import org.springframework.transaction.TransactionSystemException;

public class ItemVendorRecordDeleteCommand implements ICommand {

  private ItemVendorRecordRep itemVendorRecordRep;
  private EntityLockCommand<ItemVendorRecord> entityLockCommand;

  @Override
  public Observable<String> executeCommand(Object itemVendorRecordCode) throws Exception {
    LockDetails lockDetails = null;
    String userCode = (String) itemVendorRecordCode;
    try {
      lockDetails = entityLockCommand.lockAllSections(userCode);
      ItemVendorRecord itemVendorRecord = itemVendorRecordRep.findOneByUserCode(userCode);
      itemVendorRecordRep.delete(itemVendorRecord);
      entityLockCommand.unlockAllSections(userCode);
    } catch (TransactionSystemException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw new DependentInstanceException();
    }
    return Observable.just("SUCCESS");
  }

  public void setItemVendorRecordRep(ItemVendorRecordRep itemVendorRecordRep) {
    this.itemVendorRecordRep = itemVendorRecordRep;
  }

  public void setEntityLockCommand(EntityLockCommand<ItemVendorRecord> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }
}
