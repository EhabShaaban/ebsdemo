package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderCommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DObPurchaseOrderOpenForUpdatesCommand implements ICommand<DObPurchaseOrder> {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderApprovalCycleRep approvalCycleRep;

  public DObPurchaseOrderOpenForUpdatesCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setApprovalCycleRep(IObOrderApprovalCycleRep approvalCycleRep) {
    this.approvalCycleRep = approvalCycleRep;
  }

  @Override
  public Observable<String> executeCommand(DObPurchaseOrder purchaseOrder) throws Exception {
    if (purchaseOrder
        .getCurrentStates()
        .contains(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL)) {
      IObOrderApprovalCycle latestApprovalCycle = updateLatestApprovalCycle(purchaseOrder);
      commandUtils.setModificationInfoAndModificationDate(latestApprovalCycle);
      purchaseOrder.addLineDetail(IDObPurchaseOrder.approvalCyclesAttr, latestApprovalCycle);
    }
    DObPurchaseOrderCommandUtils.setPurchaseOrderTargetState(
        purchaseOrder, IPurchaseOrderActionNames.OPEN_FOR_UPDATES);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private IObOrderApprovalCycle updateLatestApprovalCycle(DObPurchaseOrder purchaseOrder) {
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());
    currentUndecidedCycle.setFinalDecision(ApprovalDecision.CANCELLED);
    currentUndecidedCycle.setEndDate(new DateTime(DateTimeZone.UTC));
    return currentUndecidedCycle;
  }
}
