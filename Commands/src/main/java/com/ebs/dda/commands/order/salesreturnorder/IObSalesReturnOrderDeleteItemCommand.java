package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDeleteItemValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItem;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemRep;
import io.reactivex.Observable;

public class IObSalesReturnOrderDeleteItemCommand
    implements ICommand<IObSalesReturnOrderDeleteItemValueObject> {

  private IObSalesReturnOrderItemRep salesReturnOrderItemRep;
  private DObSalesReturnOrderRep salesReturnOrderRep;
  private CommandUtils commandUtils;

  public IObSalesReturnOrderDeleteItemCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObSalesReturnOrderDeleteItemValueObject valueObject)
      throws Exception {

    IObSalesReturnOrderItem salesReturnOrderItem =
        salesReturnOrderItemRep.findOneById(valueObject.getItemId());
    salesReturnOrderItemRep.delete(salesReturnOrderItem);

    DObSalesReturnOrder salesReturnOrder =
        salesReturnOrderRep.findOneByUserCode(valueObject.getSalesReturnOrderCode());
    this.commandUtils.setModificationInfoAndModificationDate(salesReturnOrder);
    salesReturnOrderRep.update(salesReturnOrder);

    return Observable.just("SUCCESS");
  }

  public void setSalesReturnOrderItemRep(IObSalesReturnOrderItemRep salesReturnOrderItemRep) {
    this.salesReturnOrderItemRep = salesReturnOrderItemRep;
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
