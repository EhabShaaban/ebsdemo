package com.ebs.dda.commands.inventory.stockavailability;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;
import com.ebs.dda.repositories.inventory.stockavailability.CObStockAvailabilityRep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class UpdateStockAvailabilityService {
  private CObStockAvailabilityRep stockAvailabilityRep;
  private StockAvailabilityCodeGenerator codeGenerator;
  private final CommandUtils commandUtils;
  private final Logger logger = LoggerFactory.getLogger(getClass());

  public UpdateStockAvailabilityService() {
    this.commandUtils = new CommandUtils();
  }

  private void createStockAvailabilityIfNoStockAvailable(
      CObStockAvailability stockAvailability,
      IObItemQuantityGeneralModel item,
      String stockAvailabilityCode)
      throws Exception {
    if (stockAvailability == null) {
      stockAvailability = mapItemQuantityToStockAvailability(item, stockAvailabilityCode);
      stockAvailabilityRep.create(stockAvailability);
    }
  }

  private void updateStockAvailability(
      CObStockAvailability stockAvailability, BigDecimal itemQuantity) throws Exception {
    if (stockAvailability != null) {
      BigDecimal newValue = stockAvailability.getAvailableQuantity().add(itemQuantity);
      stockAvailability.setAvailableQuantity(newValue);
      commandUtils.setModificationInfoAndModificationDate(stockAvailability);
      stockAvailabilityRep.update(stockAvailability);
    }
  }

  public void updateOrCreateStockAvailabilityForItem(IObItemQuantityGeneralModel item, BigDecimal quantity) {
    StockAvailabilityCodeGenerationValueObject stockAvailabilityCodeGenerationValueObject =
        new StockAvailabilityCodeGenerationValueObject().fromItemQuantityGeneralModel(item);
    String stockAvailabilityCode =
        codeGenerator.generateUserCode(stockAvailabilityCodeGenerationValueObject);
    CObStockAvailability stockAvailability =
        stockAvailabilityRep.findOneByUserCode(stockAvailabilityCode);
    try {
      updateStockAvailability(stockAvailability, quantity);
      createStockAvailabilityIfNoStockAvailable(stockAvailability, item, stockAvailabilityCode);
    } catch (Exception ex) {
      logger.error("Error creating or updating stock availability", ex);
    }
  }

  private CObStockAvailability mapItemQuantityToStockAvailability(
      IObItemQuantityGeneralModel item, String stockAvailabilityCode) {
    CObStockAvailability stockAvailability;
    stockAvailability = new CObStockAvailability();
    commandUtils.setCreationAndModificationInfoAndDate(stockAvailability);
    stockAvailability.setUserCode(stockAvailabilityCode);
    stockAvailability.setAvailableQuantity(item.getQuantity());
    stockAvailability.setStockType(item.getStockType());
    stockAvailability.setCompanyId(item.getCompanyId());
    stockAvailability.setStorehouseId(item.getStorehouseId());
    stockAvailability.setPlantId(item.getPlantId());
    stockAvailability.setItemId(item.getItemId());
    stockAvailability.setBusinessUnitId(item.getPurchaseUnitId());
    stockAvailability.setUnitOfMeasureId(item.getUnitOfMeasureId());
    return stockAvailability;
  }

  public void setStockAvailabilityRep(CObStockAvailabilityRep stockAvailabilityRep) {
    this.stockAvailabilityRep = stockAvailabilityRep;
  }

  public void setCodeGenerator(StockAvailabilityCodeGenerator codeGenerator) {
    this.codeGenerator = codeGenerator;
  }

  public void setUserAccountSecurityService(IUserAccountSecurityService service) {
    this.commandUtils.setUserAccountSecurityService(service);
  }
}
