package com.ebs.dda.commands.inventory.inventorydocument;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.inventory.inventorydocument.DObInventoryDocumentStateMachineFactory;
import com.ebs.dda.inventory.inventorydocument.IDObInventoryActionNames;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;
import com.ebs.dda.jpa.inventory.inventorydocument.IDObInventoryDocument;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryActivationDetails;
import com.ebs.dda.repositories.inventory.DObInventoryDocumentRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class DObInventoryDocumentActivateCommand implements ICommand<DObInventoryDocumentActivateValueObject> {

  private DObInventoryDocumentRep<DObInventoryDocument> dObInventoryDocumentRep;
  private CommandUtils commandUtils;

  public DObInventoryDocumentActivateCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable executeCommand(DObInventoryDocumentActivateValueObject valueObject) throws Exception {
    DObInventoryDocument dObInventoryDocument =
        dObInventoryDocumentRep.findOneByUserCodeAndInventoryDocumentType(
            valueObject.getUserCode(), valueObject.getInventoryType());

    IObInventoryActivationDetails activationDetails = createActivationDetails(dObInventoryDocument);
    updateInventoryDocument(dObInventoryDocument, activationDetails);

    return Observable.just(valueObject.getUserCode());
  }

  private IObInventoryActivationDetails createActivationDetails(
      DObInventoryDocument dObInventoryDocument) {

    IObInventoryActivationDetails activationDetails = new IObInventoryActivationDetails();
    String loggedInUser = commandUtils.getLoggedInUser();
    activationDetails.setActivatedBy(loggedInUser);
    activationDetails.setActivationDate(new DateTime());
    activationDetails.setRefInstanceId(dObInventoryDocument.getId());
    commandUtils.setCreationAndModificationInfoAndDate(activationDetails);
    return activationDetails;
  }

  private void updateInventoryDocument(
      DObInventoryDocument dObInventoryDocument,
      IObInventoryActivationDetails iObInventoryActivationDetails)
      throws Exception {
    this.commandUtils.setModificationInfoAndModificationDate(dObInventoryDocument);
    DObInventoryDocumentStateMachineFactory inventoryDocumentStateMachineFactory =
        new DObInventoryDocumentStateMachineFactory();
    AbstractStateMachine inventoryDocumentStateMachine =
        inventoryDocumentStateMachineFactory.createStateMachine(dObInventoryDocument);
    setInventoryActiveState(inventoryDocumentStateMachine);
    dObInventoryDocument.setHeaderDetail(
        IDObInventoryDocument.activationDetailsAttr, iObInventoryActivationDetails);
    dObInventoryDocumentRep.update(dObInventoryDocument);
  }

  private void setInventoryActiveState(AbstractStateMachine abstractStateMachine) {
    if (abstractStateMachine.canFireEvent(IDObInventoryActionNames.ACTIVATE)) {
      abstractStateMachine.fireEvent(IDObInventoryActionNames.ACTIVATE);
      abstractStateMachine.save();
    }
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setDObInventoryDocumentRep(DObInventoryDocumentRep dObInventoryDocumentRep) {
    this.dObInventoryDocumentRep = dObInventoryDocumentRep;
  }
}
