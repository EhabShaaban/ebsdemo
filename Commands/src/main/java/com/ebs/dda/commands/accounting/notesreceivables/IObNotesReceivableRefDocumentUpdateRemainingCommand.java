package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dda.commands.accounting.notesreceivables.factory.NotesReceivablesReferenceDocumentUpdateRemainingFactory;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class IObNotesReceivableRefDocumentUpdateRemainingCommand
    implements ICommand<DObNotesReceivableUpdateRefDocumentRemainingValueObject> {

  NotesReceivablesReferenceDocumentUpdateRemainingFactory
      notesReceivablesReferenceDocumentUpdateRemainingFactory;
  private IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;

  @Override
  public Observable<String> executeCommand(
      DObNotesReceivableUpdateRefDocumentRemainingValueObject valueObject) throws Exception {

    String notesReceivableCode = valueObject.getNotesReceivableCode();

    List<IObNotesReceivablesReferenceDocumentGeneralModel>
        notesReceivablesReferenceDocumentGeneralModelList =
            getNotesReceivableReferenceDocuments(notesReceivableCode);

    for (IObNotesReceivablesReferenceDocumentGeneralModel notesReceivableRefDocument :
        notesReceivablesReferenceDocumentGeneralModelList) {

      updateNotesReceivablesReferenceDocumentRemaining(notesReceivableRefDocument);
    }
    return Observable.just(notesReceivableCode);
  }

  private List<IObNotesReceivablesReferenceDocumentGeneralModel>
      getNotesReceivableReferenceDocuments(String notesReceivableCode) {
    return notesReceivablesReferenceDocumentGeneralModelRep.findByNotesReceivableCodeOrderByIdAsc(
        notesReceivableCode);
  }

  private void updateNotesReceivablesReferenceDocumentRemaining(
      IObNotesReceivablesReferenceDocumentGeneralModel notesReceivableRefDocument)
      throws Exception {

    BigDecimal amountToCollect = notesReceivableRefDocument.getAmountToCollect();
    String refDocumentCode = notesReceivableRefDocument.getRefDocumentCode();
    String refDocumentType = notesReceivableRefDocument.getDocumentType();
    String notesReceivableCurrencyIso = notesReceivableRefDocument.getCurrencyIso();

    NotesReceivableRefDocumentUpdateRemainingHandler
        notesReceivableRefDocumentUpdateRemainingHandler =
            notesReceivablesReferenceDocumentUpdateRemainingFactory
                .createNotesReceivableReferenceDocumentUpdateRemaining(refDocumentType);
    notesReceivableRefDocumentUpdateRemainingHandler.updateRemaining(
        refDocumentCode, amountToCollect, notesReceivableCurrencyIso);
  }

  public void setNotesReceivablesReferenceDocumentGeneralModelRep(
      IObNotesReceivablesReferenceDocumentGeneralModelRep
          notesReceivablesReferenceDocumentGeneralModelRep) {
    this.notesReceivablesReferenceDocumentGeneralModelRep =
        notesReceivablesReferenceDocumentGeneralModelRep;
  }

  public void setNotesReceivablesReferenceDocumentUpdateRemainingFactory(
      NotesReceivablesReferenceDocumentUpdateRemainingFactory
          notesReceivablesReferenceDocumentUpdateRemainingFactory) {
    this.notesReceivablesReferenceDocumentUpdateRemainingFactory =
        notesReceivablesReferenceDocumentUpdateRemainingFactory;
  }
}
