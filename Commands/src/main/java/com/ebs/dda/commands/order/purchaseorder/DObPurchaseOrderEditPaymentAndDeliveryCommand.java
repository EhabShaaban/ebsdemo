package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.item.LObMaterial;
import com.ebs.dda.jpa.masterdata.lookups.LObContainerRequirement;
import com.ebs.dda.jpa.masterdata.lookups.LObIncoterms;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderContainersDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderDeliveryDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPaymentTermsDetails;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderContainersDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderDeliveryDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPaymentTermsDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.lookups.LObContainerRequirementRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIncotermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObPortRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.LObModeOfTransport;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.CObShipping;
import com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups.LObModeOfTransportRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.CObShippingRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DObPurchaseOrderEditPaymentAndDeliveryCommand
    implements ICommand<DObPurchaseOrderPaymentAndDeliveryValueObject> {
  private final CommandUtils commandUtils;
  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private IObOrderPaymentTermsDetailsRep iObOrderPaymentTermsDetailsRep;
  private IObOrderDeliveryDetailsRep iObOrderDeliveryDetailsRep;
  private IObOrderContainersDetailsRep iObOrderContainersDetailsRep;

  private DObPurchaseOrderRep purchaseOrderRep;
  private CObPaymentTermsRep cObPaymentTermsRep;
  private CObCurrencyRep cObCurrencyRep;
  private CObShippingRep cObShippingRep;
  private LObIncotermsRep lObIncotermsRep;
  private LObModeOfTransportRep lObModeOfTransportRep;
  private LObPortRep lObPortRep;
  private LObContainerRequirementRep lObContainerRequirementRep;

  public DObPurchaseOrderEditPaymentAndDeliveryCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setIObOrderPaymentTermsDetailsRep(
      IObOrderPaymentTermsDetailsRep iObOrderPaymentTermsDetailsRep) {
    this.iObOrderPaymentTermsDetailsRep = iObOrderPaymentTermsDetailsRep;
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setCobPaymentTermsRep(CObPaymentTermsRep cobPaymentTermsRep) {
    this.cObPaymentTermsRep = cobPaymentTermsRep;
  }

  public void setLObIncotermsRep(LObIncotermsRep lObIncotermsRep) {
    this.lObIncotermsRep = lObIncotermsRep;
  }

  public void setIObOrderDeliveryDetailsRep(IObOrderDeliveryDetailsRep iObOrderDeliveryDetailsRep) {
    this.iObOrderDeliveryDetailsRep = iObOrderDeliveryDetailsRep;
  }

  public void setLObModeOfTransportRep(LObModeOfTransportRep lObModeOfTransportRep) {
    this.lObModeOfTransportRep = lObModeOfTransportRep;
  }

  public void setCObShippingRep(CObShippingRep cObShippingRep) {
    this.cObShippingRep = cObShippingRep;
  }

  public void setLObPortRep(LObPortRep lObPortRep) {
    this.lObPortRep = lObPortRep;
  }

  public void setCObCurrencyRep(CObCurrencyRep cObCurrencyRep) {
    this.cObCurrencyRep = cObCurrencyRep;
  }

  public void setLObContainerRequirementRep(LObContainerRequirementRep lObContainerRequirementRep) {
    this.lObContainerRequirementRep = lObContainerRequirementRep;
  }

  public void setIObOrderContainersDetailsRep(
      IObOrderContainersDetailsRep iObOrderContainersDetailsRep) {
    this.iObOrderContainersDetailsRep = iObOrderContainersDetailsRep;
  }

  @Override
  public Observable<String> executeCommand(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject) throws Exception {

    DObPurchaseOrder purchaseOrder =
        purchaseOrderRep.findOneByUserCode(valueObject.getPurchaseOrderCode());

    preparePaymentTermsData(valueObject, purchaseOrder);
    prepareDeliveryData(valueObject, purchaseOrder);
    prepareContainerData(valueObject, purchaseOrder);

    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void preparePaymentTermsData(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject, DObPurchaseOrder purchaseOrder) {

    IObOrderPaymentTermsDetails iObOrderPaymentTermsDetails =
        iObOrderPaymentTermsDetailsRep.findOneByRefInstanceId(purchaseOrder.getId());

    if (valueObject.isPaymentTermDetailsEmpty() && iObOrderPaymentTermsDetails == null) {

      return;
    } else if (valueObject.isPaymentTermDetailsEmpty()) {

      iObOrderPaymentTermsDetailsRep.deleteById(iObOrderPaymentTermsDetails.getId());
      commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
      return;
    }

    iObOrderPaymentTermsDetails =
        addOrderPaymentTermsDetails(valueObject, iObOrderPaymentTermsDetails, purchaseOrder);

    commandUtils.setCreationAndModificationInfoAndDate(iObOrderPaymentTermsDetails);

    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.paymentTermsAttr, iObOrderPaymentTermsDetails);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }

  private void prepareDeliveryData(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject, DObPurchaseOrder purchaseOrder) {

    IObOrderDeliveryDetails iObOrderDeliveryDetails =
        iObOrderDeliveryDetailsRep.findOneByRefInstanceId(purchaseOrder.getId());

    if (valueObject.isDeliveryDetailsEmpty() && iObOrderDeliveryDetails == null) {

      return;
    } else if (valueObject.isDeliveryDetailsEmpty()) {

      iObOrderDeliveryDetailsRep.deleteById(iObOrderDeliveryDetails.getId());
      commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
      return;
    }

    iObOrderDeliveryDetails =
        addOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails, purchaseOrder);

    commandUtils.setCreationAndModificationInfoAndDate(iObOrderDeliveryDetails);

    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.deliveryAttr, iObOrderDeliveryDetails);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }

  public void prepareContainerData(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject, DObPurchaseOrder purchaseOrder)
      throws Exception {

    IObOrderContainersDetails iObOrderContainersDetails =
        iObOrderContainersDetailsRep.findOneByRefInstanceId(purchaseOrder.getId());

    if (valueObject.isContainerDetailsEmpty() && iObOrderContainersDetails == null) {

      return;
    } else if (valueObject.isContainerDetailsEmpty()) {

      iObOrderContainersDetailsRep.deleteById(iObOrderContainersDetails.getId());
      commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
      return;
    }

    iObOrderContainersDetails =
        addOrderContainerDetails(valueObject, iObOrderContainersDetails, purchaseOrder);

    commandUtils.setCreationAndModificationInfoAndDate(iObOrderContainersDetails);

    purchaseOrder.addLineDetail(IDObPurchaseOrder.containersAttr, iObOrderContainersDetails);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }

  private void populateIncoTermIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setIncotermName(null);
    iObOrderDeliveryDetails.setIncotermRef(null);

    LObIncoterms lObIncoterms = lObIncotermsRep.findOneByUserCode(valueObject.getIncoterm());
    if (lObIncoterms != null) {
      iObOrderDeliveryDetails.setIncotermName(lObIncoterms.getName());
      iObOrderDeliveryDetails.setIncotermRef(lObIncoterms.getId());
    }
  }

  private void populatePaymentTermIntoPaymentTermsDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderPaymentTermsDetails iObOrderPaymentTermsDetails) {

    iObOrderPaymentTermsDetails.setPaymentTermName(new LocalizedString());
    iObOrderPaymentTermsDetails.setPaymentTermRef(null);

    CObPaymentTerms cObPaymentTerms =
        cObPaymentTermsRep.findOneByUserCode(valueObject.getPaymentTerms());
    if (cObPaymentTerms != null) {
      iObOrderPaymentTermsDetails.setPaymentTermName(cObPaymentTerms.getName());
      iObOrderPaymentTermsDetails.setPaymentTermRef(cObPaymentTerms.getId());
    }
  }

  private void populateCurrencyIntoPaymentTermsDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderPaymentTermsDetails iObOrderPaymentTermsDetails) {

    iObOrderPaymentTermsDetails.setCurrencyName(new LocalizedString());
    iObOrderPaymentTermsDetails.setCurrencyRef(null);

    CObCurrency cObCurrency = cObCurrencyRep.findOneByUserCode(valueObject.getCurrency());

    if (cObCurrency != null) {
      iObOrderPaymentTermsDetails.setCurrencyName(cObCurrency.getName());
      iObOrderPaymentTermsDetails.setCurrencyRef(cObCurrency.getId());
    }
  }

  private void populateTransportModeIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setModeOfTransportName(new LocalizedString());
    iObOrderDeliveryDetails.setModeOfTransportRef(null);
    LObModeOfTransport lObModeOfTransport =
        lObModeOfTransportRep.findOneByUserCode(valueObject.getModeOfTransport());
    if (lObModeOfTransport != null) {
      iObOrderDeliveryDetails.setModeOfTransportName(lObModeOfTransport.getName());
      iObOrderDeliveryDetails.setModeOfTransportRef(lObModeOfTransport.getId());
    }
  }

  private void populateShippingInstructionIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setShippingInstructionsName(new LocalizedString());
    iObOrderDeliveryDetails.setShippingInstructionsRef(null);
    CObShipping cObShipping =
        cObShippingRep.findOneByUserCode(valueObject.getShippingInstructions());
    if (cObShipping != null) {
      iObOrderDeliveryDetails.setShippingInstructionsName(cObShipping.getName());
      iObOrderDeliveryDetails.setShippingInstructionsRef(cObShipping.getId());
    }
  }

  private void populateLoadingPortIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setLoadingPortName(new LocalizedString());
    iObOrderDeliveryDetails.setLoadingPortRef(null);
    LObMaterial loadingPort = lObPortRep.findOneByUserCode(valueObject.getLoadingPort());
    if (loadingPort != null) {
      iObOrderDeliveryDetails.setLoadingPortName(loadingPort.getName());
      iObOrderDeliveryDetails.setLoadingPortRef(loadingPort.getId());
    }
  }

  private void populateDischargePortIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setDischargePortName(new LocalizedString());
    iObOrderDeliveryDetails.setDischargePortRef(null);
    LObMaterial dischargePort = lObPortRep.findOneByUserCode(valueObject.getDischargePort());
    if (dischargePort != null) {
      iObOrderDeliveryDetails.setDischargePortName(dischargePort.getName());
      iObOrderDeliveryDetails.setDischargePortRef(dischargePort.getId());
    }
  }

  private void populateContainerTypeIntoOrderContainerDetails(
      IObOrderContainersDetails iObOrderContainersDetails, LObContainerRequirement container) {

    iObOrderContainersDetails.setContainerType(null);

    if (container != null) {
      iObOrderContainersDetails.setContainerType(container.getId());
    }
  }

  private void populateContainerNumberIntoOrderContainerDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderContainersDetails iObOrderContainersDetails) {

    iObOrderContainersDetails.setQuantity(null);

    if (valueObject.getContainersNo() != null && !valueObject.getContainersNo().equals("")) {
      iObOrderContainersDetails.setQuantity(Integer.parseInt(valueObject.getContainersNo()));
    }
  }

  private void populateCollectionDateIntoOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails) {

    iObOrderDeliveryDetails.setCollectionDate(null);
    iObOrderDeliveryDetails.setNumberOfDays(null);

    if (valueObject.getCollectionDate() != null && !valueObject.getCollectionDate().equals("")) {

      DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
      DateTime collectionDate = formatter.parseDateTime(valueObject.getCollectionDate());
      iObOrderDeliveryDetails.setCollectionDate(collectionDate);

    } else if (valueObject.getNumberOfDays() != null && !valueObject.getNumberOfDays().equals("")) {
      iObOrderDeliveryDetails.setNumberOfDays(Integer.parseInt(valueObject.getNumberOfDays()));
    }
  }

  private IObOrderPaymentTermsDetails addOrderPaymentTermsDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderPaymentTermsDetails iObOrderPaymentTermsDetails,
      DObPurchaseOrder purchaseOrder) {

    if (iObOrderPaymentTermsDetails == null) {

      iObOrderPaymentTermsDetails = new IObOrderPaymentTermsDetails();
      setOrderPaymentAndDeliveryDetailSystemColumns(iObOrderPaymentTermsDetails, purchaseOrder);
    }

    populatePaymentTermIntoPaymentTermsDetails(valueObject, iObOrderPaymentTermsDetails);
    populateCurrencyIntoPaymentTermsDetails(valueObject, iObOrderPaymentTermsDetails);

    return iObOrderPaymentTermsDetails;
  }

  private IObOrderDeliveryDetails addOrderDeliveryDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderDeliveryDetails iObOrderDeliveryDetails,
      DObPurchaseOrder purchaseOrder) {
    if (iObOrderDeliveryDetails == null) {

      iObOrderDeliveryDetails = new IObOrderDeliveryDetails();
      setOrderPaymentAndDeliveryDetailSystemColumns(iObOrderDeliveryDetails, purchaseOrder);
    }

    populateIncoTermIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);
    populateTransportModeIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);
    populateShippingInstructionIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);
    populateLoadingPortIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);
    populateDischargePortIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);
    populateCollectionDateIntoOrderDeliveryDetails(valueObject, iObOrderDeliveryDetails);

    return iObOrderDeliveryDetails;
  }

  private IObOrderContainersDetails addOrderContainerDetails(
      DObPurchaseOrderPaymentAndDeliveryValueObject valueObject,
      IObOrderContainersDetails iObOrderContainersDetails,
      DObPurchaseOrder purchaseOrder) {
    if (iObOrderContainersDetails == null) {

      iObOrderContainersDetails = new IObOrderContainersDetails();
      setOrderPaymentAndDeliveryDetailSystemColumns(iObOrderContainersDetails, purchaseOrder);
    }

    LObContainerRequirement container =
        lObContainerRequirementRep.findOneByUserCode(valueObject.getContainersType());

    populateContainerTypeIntoOrderContainerDetails(iObOrderContainersDetails, container);
    populateContainerNumberIntoOrderContainerDetails(valueObject, iObOrderContainersDetails);

    return iObOrderContainersDetails;
  }

  private void setOrderPaymentAndDeliveryDetailSystemColumns(
      InformationObject informationObject, DObPurchaseOrder purchaseOrder) {
    informationObject.setRefInstanceId(purchaseOrder.getId());
  }

  public void unlockSection(String purchaseOrderCode) throws Exception {
    entityLockCommand.unlockSection(
        purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
