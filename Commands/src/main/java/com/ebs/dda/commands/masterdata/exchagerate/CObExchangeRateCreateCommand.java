package com.ebs.dda.commands.masterdata.exchagerate;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import org.joda.time.DateTime;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateType;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateValueObject;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateType;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateTypeRep;
import io.reactivex.Observable;

public class CObExchangeRateCreateCommand implements ICommand<CObExchangeRateValueObject> {

  private final CommandUtils commandUtils;
  private CObExchangeRateRep exchangeRep;
  private CObCurrencyRep currencyRep;
  private final DocumentObjectCodeGenerator codeGenerator;
  private CObExchangeRateTypeRep exchangeRateTypeRep;

  public CObExchangeRateCreateCommand(DocumentObjectCodeGenerator codeGenerator){
    commandUtils = new CommandUtils();
    this.codeGenerator = codeGenerator;
  }

  @Override
  public Observable<String> executeCommand(CObExchangeRateValueObject exchangeRateValueObject)
      throws Exception {
    String userCode =
        codeGenerator.generateUserCode(CObExchangeRate.class.getSimpleName());
    CObExchangeRate exchangeRate = new CObExchangeRate();
    commandUtils.setCreationAndModificationInfoAndDate(exchangeRate);
    setExchangeRateData(exchangeRateValueObject, exchangeRate, userCode);
    exchangeRep.create(exchangeRate);
    return Observable.just(userCode);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  private void setExchangeRateData(CObExchangeRateValueObject exchangeRateValueObject,
      CObExchangeRate exchangeRate, String userCode) {

    DateTime dateTime = new DateTime();

    exchangeRate.setFirstCurrencyId(
        getUsedCurrency(exchangeRateValueObject.getFirstCurrencyCode()).getId());
    exchangeRate.setSecondCurrencyId(1L);
    exchangeRate.setName(setLocalizedName(exchangeRateValueObject));
    exchangeRate.setSecondValue(exchangeRateValueObject.getSecondValue());
    exchangeRate.setFirstValue(BigDecimal.ONE);
    exchangeRate.setCurrentStates(addState());
    exchangeRate.setCreationDate(dateTime);
    exchangeRate.setModifiedDate(dateTime);
    exchangeRate.setValidFrom(dateTime);
    exchangeRate.setTypeId(getUserDailyRateType());
    exchangeRate.setUserCode(userCode);
  }

  private Long getUserDailyRateType() {
    CObExchangeRateType exchangeRateType =
        exchangeRateTypeRep.findOneByUserCode(ICObExchangeRateType.USER_DAILY_RATE);
    return exchangeRateType.getId();
  }

  private LocalizedString setLocalizedName(CObExchangeRateValueObject exchangeRateValueObject) {

    String name = getUsedCurrency(exchangeRateValueObject.getFirstCurrencyCode()).getIso() + "EGP";
    LocalizedString localizedItemName = new LocalizedString();
    localizedItemName.setValue(new Locale("ar"), name);
    localizedItemName.setValue(new Locale("en"), name);
    return localizedItemName;
  }

  private CObCurrency getUsedCurrency(String currencyCode) {
    CObCurrency firstCurrency = currencyRep.findOneByUserCode(currencyCode);
    return firstCurrency;
  }

  private Set<String> addState() {
    Set<String> currentStates = new HashSet<>();
    currentStates.add("Active");
    return currentStates;
  }

  public void setExchangeRep(CObExchangeRateRep exchangeRep) {
    this.exchangeRep = exchangeRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setExchangeRateTypeRep(CObExchangeRateTypeRep exchangeRateTypeRep) {
    this.exchangeRateTypeRep = exchangeRateTypeRep;
  }
}
