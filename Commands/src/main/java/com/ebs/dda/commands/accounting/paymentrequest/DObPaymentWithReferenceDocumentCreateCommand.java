package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceDocumentGeneralModelRep;

public abstract class DObPaymentWithReferenceDocumentCreateCommand<RefDocument extends DObPaymentReferenceDocumentGeneralModel>
				extends DObPaymentRequestCreateCommand {

	private DObPaymentReferenceDocumentGeneralModelRep refDocumentRep;
	private RefDocument refDocument;

	public DObPaymentWithReferenceDocumentCreateCommand(DObPaymentRequestStateMachine stateMachine) {
		super(stateMachine);
	}
	@Override
	protected void findReferenceDocument(
					DObPaymentRequestCreateValueObject paymentRequestValueObject) {
		String refDocumentCode = paymentRequestValueObject.getDueDocumentCode();
		refDocument = (RefDocument) refDocumentRep.findOneByCode(refDocumentCode);

	}

	protected RefDocument getRefDocument() {
		return refDocument;
	}

	public void setRefDocumentRep(DObPaymentReferenceDocumentGeneralModelRep refDocumentRep) {
		this.refDocumentRep = refDocumentRep;
	}
}
