package com.ebs.dda.commands.accounting.salesinvoice;

import javax.transaction.Transactional;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.salesinvoice.typestrategy.DObSalesInvoiceCreationHandler;
import com.ebs.dda.commands.accounting.salesinvoice.typestrategy.DObSalesInvoiceTypeStrategyContext;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceCreationValueObject;
import com.ebs.dda.repositories.accounting.salesinvoice.CObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.collectionresponsible.CObCollectionResponsibleRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.*;
import io.reactivex.Observable;

public class DObSalesInvoiceCreateCommand implements ICommand<DObSalesInvoiceCreationValueObject> {

  private DObSalesInvoiceRep dObSalesInvoiceRep;
  private DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep;
  private CObSalesInvoiceRep cObSalesInvoiceRep;
  private CObCompanyRep cObCompanyRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private CObCollectionResponsibleRep cObCollectionResponsibleRep;
  private MObCustomerRep mObCustomerRep;
  private LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep;
  private IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep;
  private CommandUtils commandUtils;
  private IUserAccountSecurityService userAccountSecurityService;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;

  private IObSalesOrderDataRep iObSalesOrderDataRep;
  private IObSalesOrderCompanyStoreDataRep iObSalesOrderCompanyRep;
  private IObSalesOrderCompanyAndStoreDataGeneralModelRep iObSalesOrderCompanyGeneralModelRep;
  private IObSalesOrderItemsRep salesOrderItemsRep;
  private MObItemRep itemRep;
  private IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep;

  public DObSalesInvoiceCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  @Transactional
  public Observable<String> executeCommand(DObSalesInvoiceCreationValueObject valueObject)
      throws Exception {

    DObSalesInvoiceTypeStrategyContext typeStrategyContext =
        new DObSalesInvoiceTypeStrategyContext();

    DObSalesInvoiceCreationHandler creationHandler =
        typeStrategyContext.getContext(valueObject.getInvoiceTypeCode());

    buildHandlerDependencies(creationHandler);

    DObSalesInvoice salesInvoice = creationHandler.createSalesInvoice(valueObject);

    dObSalesInvoiceRep.create(salesInvoice);

    return Observable.just(salesInvoice.getUserCode());
  }

  private void buildHandlerDependencies(DObSalesInvoiceCreationHandler creationHandler) {

    creationHandler.setcObSalesInvoiceRep(cObSalesInvoiceRep);
    creationHandler.setcObCompanyRep(cObCompanyRep);
    creationHandler.setPurchasingUnitRep(purchasingUnitRep);
    creationHandler.setcObCollectionResponsibleRep(cObCollectionResponsibleRep);
    creationHandler.setmObCustomerRep(mObCustomerRep);
    creationHandler.setCommandUtils(commandUtils);
    creationHandler.setUserAccountSecurityService(userAccountSecurityService);
    creationHandler.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
    creationHandler.setCompanyTaxesGeneralModelRep(companyTaxesGeneralModelRep);
    creationHandler.setSalesOrderTaxGeneralModelRep(salesOrderTaxGeneralModelRep);
    creationHandler.setdObSalesOrderGeneralModelRep(dObSalesOrderGeneralModelRep);
    creationHandler.setiObSalesOrderDataRep(iObSalesOrderDataRep);
    creationHandler.setiObSalesOrderCompanyRep(iObSalesOrderCompanyRep);
    creationHandler.setiObSalesOrderCompanyGeneralModelRep(iObSalesOrderCompanyGeneralModelRep);
    creationHandler.setSalesOrderItemsRep(salesOrderItemsRep);
    creationHandler.setItemRep(itemRep);
    creationHandler.setAlternativeUoMGeneralModelRep(alternativeUoMGeneralModelRep);
  }


  public void setdObSalesInvoiceRep(DObSalesInvoiceRep dObSalesInvoiceRep) {
    this.dObSalesInvoiceRep = dObSalesInvoiceRep;
  }

  public void setcObSalesInvoiceRep(CObSalesInvoiceRep cObSalesInvoiceRep) {
    this.cObSalesInvoiceRep = cObSalesInvoiceRep;
  }

  public void setcObCompanyRep(CObCompanyRep cObCompanyRep) {
    this.cObCompanyRep = cObCompanyRep;
  }

  public CObPurchasingUnitRep getPurchasingUnitRep() {
    return purchasingUnitRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setcObCollectionResponsibleRep(
      CObCollectionResponsibleRep cObCollectionResponsibleRep) {
    this.cObCollectionResponsibleRep = cObCollectionResponsibleRep;
  }

  public void setmObCustomerRep(MObCustomerRep mObCustomerRep) {
    this.mObCustomerRep = mObCustomerRep;
  }

  public CommandUtils getCommandUtils() {
    return commandUtils;
  }

  public void setCommandUtils(CommandUtils commandUtils) {
    this.commandUtils = commandUtils;
  }

  public IUserAccountSecurityService getUserAccountSecurityService() {
    return userAccountSecurityService;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public DocumentObjectCodeGenerator getDocumentObjectCodeGenerator() {
    return documentObjectCodeGenerator;
  }

  public void setDocumentObjectCodeGenerator(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  public void setCompanyTaxesGeneralModelRep(
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep) {
    this.companyTaxesGeneralModelRep = companyTaxesGeneralModelRep;
  }

  public void setdObSalesOrderGeneralModelRep(
      DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep) {
    this.dObSalesOrderGeneralModelRep = dObSalesOrderGeneralModelRep;
  }

  public void setiObSalesOrderDataRep(IObSalesOrderDataRep iObSalesOrderDataRep) {
    this.iObSalesOrderDataRep = iObSalesOrderDataRep;
  }

  public void setiObSalesOrderCompanyRep(IObSalesOrderCompanyStoreDataRep iObSalesOrderCompanyRep) {
    this.iObSalesOrderCompanyRep = iObSalesOrderCompanyRep;
  }

  public void setiObSalesOrderCompanyGeneralModelRep(
      IObSalesOrderCompanyAndStoreDataGeneralModelRep iObSalesOrderCompanyGeneralModelRep) {
    this.iObSalesOrderCompanyGeneralModelRep = iObSalesOrderCompanyGeneralModelRep;
  }

  public void setSalesOrderItemsRep(IObSalesOrderItemsRep salesOrderItemsRep) {
    this.salesOrderItemsRep = salesOrderItemsRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setAlternativeUoMGeneralModelRep(IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep) {
    this.alternativeUoMGeneralModelRep = alternativeUoMGeneralModelRep;
  }

  public void setSalesOrderTaxGeneralModelRep(IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep) {
    this.salesOrderTaxGeneralModelRep = salesOrderTaxGeneralModelRep;
  }
}
