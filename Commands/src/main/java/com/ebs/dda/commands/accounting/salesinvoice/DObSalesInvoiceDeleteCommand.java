package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import io.reactivex.Observable;

public class DObSalesInvoiceDeleteCommand implements ICommand {


  private DObSalesInvoiceRep dObSalesInvoiceRep;

  public DObSalesInvoiceDeleteCommand() {}

  @Override
  public Observable executeCommand(Object salesInvoiceCode) throws Exception {
    dObSalesInvoiceRep.delete(readEntityByCode((String) salesInvoiceCode));
    return Observable.empty();
  }

  public void checkIfInstanceExists(String salesInvoiceCode) throws InstanceNotExistException {
    DObSalesInvoice salesInvoice = readEntityByCode(salesInvoiceCode);
    if (salesInvoice == null) {
      throw new InstanceNotExistException();
    }
  }

  private DObSalesInvoice readEntityByCode(String salesInvoiceCode) {
    return dObSalesInvoiceRep.findOneByUserCode(salesInvoiceCode);
  }

  public void setdObSalesInvoiceRep(DObSalesInvoiceRep dObSalesInvoiceRep) {
    this.dObSalesInvoiceRep = dObSalesInvoiceRep;
  }

}
