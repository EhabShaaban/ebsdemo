package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItem;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoicePurchaseOrder;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderItem;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderItemRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class DObPurchaseServiceLocalVendorInvoiceCreateCommand
    extends DObVendorInvoiceCreateCommand {
  private IObOrderItemRep orderItemRep;
  private MObItemRep itemRep;
  private IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep;

  public DObPurchaseServiceLocalVendorInvoiceCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) throws Exception {
    super(documentObjectCodeGenerator);
  }

  @Override
  protected void setVendorInvoiceItems(DObPurchaseOrder order, DObVendorInvoice dObInvoice) {

    List<IObVendorInvoiceItem> invoiceItems = new LinkedList<>();
    List<IObOrderItem> orderLineDetailsList = orderItemRep.findByRefInstanceId(order.getId());
    for (IObOrderItem orderItem : orderLineDetailsList) {
      BigDecimal itemPrice = orderItem.getPrice();
      IObVendorInvoiceItem item = new IObVendorInvoiceItem();
      commandUtils.setCreationAndModificationInfo(item);
      Optional<MObItem> masterDataItem = itemRep.findById(orderItem.getItemId());
      Long basicUnitOfMeasure = masterDataItem.get().getBasicUnitOfMeasure();
      item.setBaseUnitOfMeasureId(basicUnitOfMeasure);
      item.setItemId(orderItem.getItemId());
      item.setOrderUnitOfMeasureId(orderItem.getOrderUnitId());
      item.setPrice(itemPrice);
      item.setQunatityInOrderUnit(orderItem.getQuantity());
      invoiceItems.add(item);
    }
    Collections.reverse(invoiceItems);
    dObInvoice.setLinesDetails(IDObVendorInvoice.invoiceItemsAttr, invoiceItems);
  }

  @Override
  protected void setVendorInvoiceReferenceOrderPaymentProperties(
      IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder, DObPurchaseOrder order) {

    IObOrderPaymentDetailsGeneralModel orderPaymentDetailsGeneralModel =
        orderPaymentDetailsGeneralModelRep.findOneByOrderCode(order.getUserCode());

    setCurrency(iObInvoicePurchaseOrder, orderPaymentDetailsGeneralModel.getCurrencyCode());
    setPaymentTerm(iObInvoicePurchaseOrder, orderPaymentDetailsGeneralModel.getPaymentTermCode());
  }

  public void setOrderPaymentDetailsGeneralModelRep(
      IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep) {
    this.orderPaymentDetailsGeneralModelRep = orderPaymentDetailsGeneralModelRep;
  }

  public void setOrderItemRep(IObOrderItemRep orderItemRep) {
    this.orderItemRep = orderItemRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }
}
