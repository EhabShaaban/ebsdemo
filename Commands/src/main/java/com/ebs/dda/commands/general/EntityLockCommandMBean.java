package com.ebs.dda.commands.general;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;

public interface EntityLockCommandMBean {

  // DO NOT CHANGE METHODS NAMES -- USED IN JMX
  LockDetails getLockForCurrentUserOfResource(
      IBDKUser user, String resourceName, String resourceCode);

  void unlockAllSections(String resourceCode) throws Exception;
}
