package com.ebs.dda.commands.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.factories.IObJournalEntryItemFactory;
import com.ebs.dda.jpa.accounting.journalentry.DObCostingJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingAccountingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObCostingJournalEntryRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DObCostingClosingJournalCreateCommand
    implements ICommand<DObCostingActivateValueObject> {
  private final CommandUtils commandUtils;
  private final String ACTIVE_STATE = "Active";
  private final DocumentObjectCodeGenerator codeGenerator;
  private IObCostingCompanyDataGeneralModelRep costingCompanyDataGeneralModelRep;
  private IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  private DObCostingJournalEntryRep costingJournalEntryRep;
  private IObCostingAccountingDetailsGeneralModelRep costingAccountingDetailsGeneralModelRep;
  private CObFiscalYearRep fiscalYearRep;
  private CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep;

  public DObCostingClosingJournalCreateCommand(DocumentObjectCodeGenerator codeGenerator) {
    this.codeGenerator = codeGenerator;
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObCostingActivateValueObject valueObject)
      throws Exception {

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        costingDetailsGeneralModelRep.findOneByUserCode(valueObject.getUserCode());

    List<IObCostingAccountingDetailsGeneralModel> costingAccountingDetails =
        costingAccountingDetailsGeneralModelRep.findByRefInstanceId(
            costingDetailsGeneralModel.getRefInstanceId());

    if (!isCostingAccountingDetailsEmpty(costingAccountingDetails)) {
      DObCostingJournalEntry journalEntry =
          setJournalEntry(costingDetailsGeneralModel, valueObject);
      setJournalEntryDetails(journalEntry, costingAccountingDetails);
      costingJournalEntryRep.create(journalEntry);
      return Observable.just(journalEntry.getUserCode());
    }
    return Observable.empty();
  }

  private boolean isCostingAccountingDetailsEmpty(
      List<IObCostingAccountingDetailsGeneralModel> costingAccountingDetails) {
    return costingAccountingDetails.isEmpty();
  }

  private void setJournalEntryDetails(
      DObCostingJournalEntry journalEntry,
      List<IObCostingAccountingDetailsGeneralModel> costingAccountingDetails) {

    String companyCurrencyIso = costingAccountingDetails.get(0).getCurrencyISO();

    CObExchangeRateGeneralModel exchangeRate =
        exchangeRateGeneralModelRep.findLatestDefaultExchangeRate(
            companyCurrencyIso, companyCurrencyIso);

    List<IObJournalEntryItem> journalEntryItems =
        costingAccountingDetails.stream()
            .map(accountingDetail -> setJournalEntryItem(exchangeRate, accountingDetail))
            .collect(Collectors.toList());
    journalEntry.setLinesDetails(IDObJournalEntry.journalItemAttr, journalEntryItems);
  }

  private IObJournalEntryItem setJournalEntryItem(
      CObExchangeRateGeneralModel exchangeRate,
      IObCostingAccountingDetailsGeneralModel accountingDetail) {
    IObJournalEntryItem journalEntryItem =
        IObJournalEntryItemFactory.initJournalEntryItemInstance(accountingDetail.getSubLedger());

    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
    journalEntryItem.setAccountId(accountingDetail.getGlAccountId());
    journalEntryItem.setSubAccountId(accountingDetail.getGlSubAccountId());
    journalEntryItem.setDocumentCurrencyId(exchangeRate.getFirstCurrencyId());
    journalEntryItem.setCompanyCurrencyId(exchangeRate.getSecondCurrencyId());
    journalEntryItem.setCompanyCurrencyPrice(exchangeRate.getSecondValue());
    journalEntryItem.setCompanyExchangeRateId(exchangeRate.getId());
    if (accountingDetail.getAccountingEntry().equals(AccountingEntry.DEBIT.name())) {
      journalEntryItem.setDebit(accountingDetail.getAmount());
      journalEntryItem.setCredit(BigDecimal.ZERO);
    }
    if (accountingDetail.getAccountingEntry().equals(AccountingEntry.CREDIT.name())) {
      journalEntryItem.setCredit(accountingDetail.getAmount());
      journalEntryItem.setDebit(BigDecimal.ZERO);
    }
    return journalEntryItem;
  }

  private DObCostingJournalEntry setJournalEntry(
      IObCostingDetailsGeneralModel costingDetailsGeneralModel,
      DObCostingActivateValueObject valueObject) {

    IObCostingCompanyDataGeneralModel costingCompanyDataGeneralModel =
        costingCompanyDataGeneralModelRep.findOneByDocumentCode(
            costingDetailsGeneralModel.getUserCode());

    DateTime dueDate = commandUtils.getDateTime(valueObject.getDueDate());
    CObFiscalYear activeFiscalYear = getActiveFiscalYear(dueDate);

    String userCode = codeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName());

    DObCostingJournalEntry journalEntry = new DObCostingJournalEntry();
    commandUtils.setCreationAndModificationInfoAndDate(journalEntry);
    journalEntry.setCompanyId(costingCompanyDataGeneralModel.getCompanyId());
    journalEntry.setPurchaseUnitId(costingCompanyDataGeneralModel.getPurchaseUnitId());
    journalEntry.setUserCode(userCode);
    journalEntry.setDocumentReferenceId(costingDetailsGeneralModel.getRefInstanceId());
    journalEntry.setFiscalPeriodId(activeFiscalYear.getId());
    journalEntry.setDueDate(dueDate);
    setJournalEntryActiveState(journalEntry);
    return journalEntry;
  }

  private CObFiscalYear getActiveFiscalYear(DateTime dueDate) {
    List<CObFiscalYear> activeFiscalYears =
        fiscalYearRep.findByCurrentStatesLike(("%" + ACTIVE_STATE + "%"));
    return activeFiscalYears.stream()
        .filter(year -> year.getFromDate().isBefore(dueDate) && year.getToDate().isAfter(dueDate))
        .findFirst()
        .orElse(null);
  }

  private void setJournalEntryActiveState(DObCostingJournalEntry costingJournalEntry) {
    Set<String> currentStates = new HashSet<>();
    currentStates.add(ACTIVE_STATE);
    costingJournalEntry.setCurrentStates(currentStates);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCostingDetailsGeneralModelRep(
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
  }

  public void setCostingCompanyDataGeneralModelRep(
      IObCostingCompanyDataGeneralModelRep costingCompanyDataGeneralModelRep) {
    this.costingCompanyDataGeneralModelRep = costingCompanyDataGeneralModelRep;
  }

  public void setCostingJournalEntryRep(DObCostingJournalEntryRep costingJournalEntryRep) {
    this.costingJournalEntryRep = costingJournalEntryRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  public void setCostingAccountingDetailsGeneralModelRep(
      IObCostingAccountingDetailsGeneralModelRep costingAccountingDetailsGeneralModelRep) {
    this.costingAccountingDetailsGeneralModelRep = costingAccountingDetailsGeneralModelRep;
  }

  public void setExchangeRateGeneralModelRep(
      CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep) {
    this.exchangeRateGeneralModelRep = exchangeRateGeneralModelRep;
  }
}
