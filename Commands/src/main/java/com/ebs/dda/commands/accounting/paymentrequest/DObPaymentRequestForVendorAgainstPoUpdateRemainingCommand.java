package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand
    implements ICommand<DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject> {

  private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  private DObPurchaseOrderRep purchaseOrderRep;

  @Override
  public Observable<String> executeCommand(
      DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject valueObject) throws Exception {

    String paymentRequestCode = valueObject.getPaymentRequestCode();
    BigDecimal amountValue = valueObject.getAmountValue();

    DObPurchaseOrder purchaseOrder = getPurchaseOrder(paymentRequestCode);
    updatePurchaseOrderRemaining(purchaseOrder, amountValue);
    return Observable.just(paymentRequestCode);
  }

  private DObPurchaseOrder getPurchaseOrder(String paymentRequestCode) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    String purchaseOrderCode = paymentRequestGeneralModel.getReferenceDocumentCode();
    return purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
  }

  private void updatePurchaseOrderRemaining(
      DObPurchaseOrder purchaseOrder, BigDecimal amountValue) {
    purchaseOrder.setRemaining(purchaseOrder.getRemaining().subtract(amountValue));
  }

  public void setDObPaymentRequestGeneralModelRep(
      DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep) {
    this.paymentRequestGeneralModelRep = paymentRequestGeneralModelRep;
  }

  public void setDObPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }
}
