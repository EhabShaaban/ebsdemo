package com.ebs.dda.commands.accounting.notesreceivables.factory;

import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateNRRemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateRemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateSIRemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateSORemainingHandler;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;

public class NotesReceivablesReferenceDocumentUpdateRemainingFactory {

  private NotesReceivableRefDocumentUpdateNRRemainingHandler
      notesReceivableRefDocumentUpdateNRRemainingHandler;

  private NotesReceivableRefDocumentUpdateSORemainingHandler
      notesReceivableRefDocumentUpdateSORemainingHandler;

  private NotesReceivableRefDocumentUpdateSIRemainingHandler
      notesReceivableRefDocumentUpdateSIRemainingHandler;

  public void setNotesReceivableRefDocumentUpdateNRRemainingHandler(
      NotesReceivableRefDocumentUpdateNRRemainingHandler
          notesReceivableRefDocumentUpdateNRRemainingHandler) {
    this.notesReceivableRefDocumentUpdateNRRemainingHandler =
        notesReceivableRefDocumentUpdateNRRemainingHandler;
  }

  public void setNotesReceivableRefDocumentUpdateSORemainingHandler(
      NotesReceivableRefDocumentUpdateSORemainingHandler
          notesReceivableRefDocumentUpdateSORemainingHandler) {
    this.notesReceivableRefDocumentUpdateSORemainingHandler =
        notesReceivableRefDocumentUpdateSORemainingHandler;
  }

  public void setNotesReceivableRefDocumentUpdateSIRemainingHandler(
      NotesReceivableRefDocumentUpdateSIRemainingHandler
          notesReceivableRefDocumentUpdateSIRemainingHandler) {
    this.notesReceivableRefDocumentUpdateSIRemainingHandler =
        notesReceivableRefDocumentUpdateSIRemainingHandler;
  }

  public NotesReceivableRefDocumentUpdateRemainingHandler
      createNotesReceivableReferenceDocumentUpdateRemaining(String type) throws Exception {

    if (isNotesReceivable(type)) {
      return notesReceivableRefDocumentUpdateNRRemainingHandler;
    } else if (isSalesOrder(type)) {
      return notesReceivableRefDocumentUpdateSORemainingHandler;
    } else if (isSalesInvoice(type)) {
      return notesReceivableRefDocumentUpdateSIRemainingHandler;
    }
    throw new UnsupportedOperationException("Not supported ReferenceDocument type");
  }

  private boolean isNotesReceivable(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name());
  }

  private boolean isSalesOrder(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name());
  }

  private boolean isSalesInvoice(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name());
  }
}
