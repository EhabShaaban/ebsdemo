package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.CObControlPoint;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderApproveAndRejectUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderAssignMeToApproveValueObject;
import io.reactivex.Observable;

public class DObPurchaseOrderAssignMeToApproveCommand extends DObPurchaseOrderApproveAndRejectUtils
    implements ICommand<DObPurchaseOrderAssignMeToApproveValueObject> {

  private final CommandUtils commandUtils;

  public DObPurchaseOrderAssignMeToApproveCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      DObPurchaseOrderAssignMeToApproveValueObject assignMeToApproveValueObject) throws Exception {
    String purchaseOrderCode = assignMeToApproveValueObject.getPurchaseOrderCode();
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    IBDKUser loggedInUser = commandUtils.getLoggedInUserObject();
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());

    Long approvalCycleId = currentUndecidedCycle.getId();
    String controlPointCode = assignMeToApproveValueObject.getControlPointCode();
    CObControlPoint controlPoint = controlPointRep.findOneByUserCode(controlPointCode);

    IObOrderApprover controlPointCurrentApprover =
        getControlPointCurrentApprover(approvalCycleId, controlPoint.getId());

    controlPointCurrentApprover.setApproverId(loggedInUser.getUserId());

    commandUtils.setModificationInfoAndModificationDate(controlPointCurrentApprover);
    commandUtils.setModificationInfoAndModificationDate(currentUndecidedCycle);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    updateDatabase(controlPointCurrentApprover, currentUndecidedCycle, purchaseOrder);
    return Observable.just("SUCCESS");
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
