package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObVendorInvoiceJournalEntryRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import io.reactivex.Observable;

public class DObJournalEntryCreateVendorInvoiceCommand
        implements ICommand<DObJournalEntryCreateValueObject> {

    private DObVendorInvoiceJournalEntryInitializer journalEntryInitializer;
    private IObVendorInvoiceJournalEntryDetailsInitializer vendorInvoiceJournalEntryDetailsInitializer;
    private DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep;
    private DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep;
    private final CommandUtils commandUtils;

    private DObVendorInvoiceRep vendorInvoiceRep;

    public DObJournalEntryCreateVendorInvoiceCommand() {
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(
            DObJournalEntryCreateValueObject vendorInvoiceValueObject) throws Exception {

        String vendorInvoiceCode = vendorInvoiceValueObject.getUserCode();
        DObVendorInvoice vendorInvoice = vendorInvoiceRep.findOneByUserCode(vendorInvoiceCode);

        DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry = journalEntryInitializer.initializeJournalEntry(vendorInvoice, vendorInvoiceValueObject);
        DObVendorInvoiceJournalDetailPreparationGeneralModel journalDetailPreparationGeneralModel = journalDetailPreparationGeneralModelٌRep.findOneByCodeAndObjectTypeCode(vendorInvoice.getUserCode(), vendorInvoice.getDiscriminatorValue());

        vendorInvoiceJournalEntryDetailsInitializer.addCredit(vendorInvoiceJournalEntry, journalDetailPreparationGeneralModel).addDebit(vendorInvoiceJournalEntry, vendorInvoice, journalDetailPreparationGeneralModel);

        vendorInvoiceJournalEntryRep.create(vendorInvoiceJournalEntry);

        return Observable.just(vendorInvoiceJournalEntry.getUserCode());
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setVendorInvoiceJournalEntryRep(
            DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep) {
        this.vendorInvoiceJournalEntryRep = vendorInvoiceJournalEntryRep;
    }

    public void setJournalEntryInitializer(DObVendorInvoiceJournalEntryInitializer journalEntryInitializer) {
        this.journalEntryInitializer = journalEntryInitializer;
    }

    public void setVendorInvoiceRep(DObVendorInvoiceRep vendorInvoiceRep) {
        this.vendorInvoiceRep = vendorInvoiceRep;
    }

    public void setVendorInvoiceJournalEntryDetailsInitializer(IObVendorInvoiceJournalEntryDetailsInitializer vendorInvoiceJournalEntryDetailsInitializer) {
        this.vendorInvoiceJournalEntryDetailsInitializer = vendorInvoiceJournalEntryDetailsInitializer;
    }

    public void setJournalDetailPreparationGeneralModelٌRep(DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep) {
        this.journalDetailPreparationGeneralModelٌRep = journalDetailPreparationGeneralModelٌRep;
    }

}
