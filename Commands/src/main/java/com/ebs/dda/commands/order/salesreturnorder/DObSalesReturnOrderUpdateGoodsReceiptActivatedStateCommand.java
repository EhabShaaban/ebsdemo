package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderActionNames;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import io.reactivex.Observable;

public class DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
    implements ICommand<DObInventoryDocumentActivateValueObject> {

  private final CommandUtils commandUtils;
  private DObSalesReturnOrderRep salesReturnOrderRep;

  public DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObInventoryDocumentActivateValueObject valueObject)
      throws Exception {

    DObSalesReturnOrder salesReturnOrder =
        salesReturnOrderRep.findOneByUserCode(valueObject.getRefDocumentCode());
    commandUtils.setModificationInfoAndModificationDate(salesReturnOrder);

    updateSalesReturnOrderState(getSalesReturnOrderStateMachine(salesReturnOrder));

    salesReturnOrderRep.update(salesReturnOrder);

    return Observable.just(salesReturnOrder.getUserCode());
  }

  private void updateSalesReturnOrderState(AbstractStateMachine salesReturnOrderStateMachine) {
    if (salesReturnOrderStateMachine.canFireEvent(
        IDObSalesReturnOrderActionNames.ACTIVATE_GOODS_RECEIPT)) {
      salesReturnOrderStateMachine.fireEvent(
          IDObSalesReturnOrderActionNames.ACTIVATE_GOODS_RECEIPT);
      salesReturnOrderStateMachine.save();
    }
  }

  private DObSalesReturnOrderStateMachine getSalesReturnOrderStateMachine(
      DObSalesReturnOrder salesReturnOrder) throws Exception {
    DObSalesReturnOrderStateMachine salesReturnOrderStateMachine =
        new DObSalesReturnOrderStateMachine();
    salesReturnOrderStateMachine.initObjectState(salesReturnOrder);
    return salesReturnOrderStateMachine;
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
