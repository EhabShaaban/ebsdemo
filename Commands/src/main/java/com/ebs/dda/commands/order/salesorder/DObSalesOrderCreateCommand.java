package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import com.ebs.dda.jpa.order.salesorder.CObSalesOrderType;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyStoreData;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderData;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderTax;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.salesresponsible.CObSalesResponsibleRep;
import com.ebs.dda.repositories.order.salesorder.CObSalesOrderTypeRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

public class DObSalesOrderCreateCommand implements ICommand<DObSalesOrderCreateValueObject> {

  private DObSalesOrderStateMachine stateMachine;
  private CommandUtils commandUtils;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;

  private CObPurchasingUnitRep businessUnitRep;
  private CObSalesOrderTypeRep salesOrderTypeRep;
  private CObSalesResponsibleRep salesResponsibleRep;
  private MObCustomerRep customerRep;
  private CObCompanyRep companyRep;
  private DObSalesOrderRep salesOrderRep;
  private LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep;

  public DObSalesOrderCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    stateMachine = new DObSalesOrderStateMachine();
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  @Transactional
  public Observable<String> executeCommand(
      DObSalesOrderCreateValueObject salesOrderCreateValueObject) throws Exception {

    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObSalesOrder.class.getSimpleName());

    DObSalesOrder salesOrder = new DObSalesOrder();
    IObSalesOrderCompanyStoreData companyStoreData = new IObSalesOrderCompanyStoreData();
    IObSalesOrderData salesOrderData = new IObSalesOrderData();
    List<IObSalesOrderTax> salesOrderTaxes = new ArrayList<>();

    setDObSalesOrderData(salesOrderCreateValueObject, salesOrder, userCode);
    setCompanyStoreData(salesOrderCreateValueObject, companyStoreData);
    setIObSalesOrderData(salesOrderCreateValueObject, salesOrderData);
    setIObSalesOrderTax(salesOrderCreateValueObject, salesOrderTaxes);

    executeTransaction(salesOrder, salesOrderData, companyStoreData, salesOrderTaxes);

    return Observable.just(userCode);
  }

  private void setDObSalesOrderData(
      DObSalesOrderCreateValueObject salesOrderCreateValueObject,
      DObSalesOrder salesOrder,
      String userCode)
      throws Exception {

    commandUtils.setCreationAndModificationInfoAndDate(salesOrder);
    stateMachine.initObjectState(salesOrder);

    salesOrder.setUserCode(userCode);
    salesOrder.setSalesOrderTypeId(getTypeId(salesOrderCreateValueObject));
    salesOrder.setBusinessUnitId(getPurchaseUnitId(salesOrderCreateValueObject));
    salesOrder.setSalesResponsibleId(getSalesResponsibleId(salesOrderCreateValueObject));
  }

  private void setCompanyStoreData(
      DObSalesOrderCreateValueObject salesOrderCreateValueObject,
      IObSalesOrderCompanyStoreData companyStoreData) {
    commandUtils.setCreationAndModificationInfoAndDate(companyStoreData);
    companyStoreData.setCompanyId(getCompanyIdByCode(salesOrderCreateValueObject.getCompanyCode()));
  }

  private void setIObSalesOrderData(
      DObSalesOrderCreateValueObject salesOrderCreateValueObject,
      IObSalesOrderData salesOrderData) {
    commandUtils.setCreationAndModificationInfoAndDate(salesOrderData);
    salesOrderData.setCustomerId(getCustomerId(salesOrderCreateValueObject));
  }

  private void setIObSalesOrderTax(
      DObSalesOrderCreateValueObject valueObject, List<IObSalesOrderTax> salesOrderTaxes) {
    List<LObCompanyTaxesGeneralModel> companyTaxes =
        companyTaxesGeneralModelRep.findByCompanyCodeOrderByUserCodeAsc(
            valueObject.getCompanyCode());
    for (LObCompanyTaxesGeneralModel companyTax : companyTaxes) {
      addCompanyTaxToSalesOrderTaxes(companyTax, salesOrderTaxes);
    }
  }

  private void addCompanyTaxToSalesOrderTaxes(
      LObCompanyTaxesGeneralModel companyTax, List<IObSalesOrderTax> salesOrderTaxes) {
    IObSalesOrderTax tax = new IObSalesOrderTax();
    commandUtils.setCreationAndModificationInfoAndDate(tax);
    tax.setCode(companyTax.getUserCode());
    tax.setName(companyTax.getName());
    tax.setPercentage(companyTax.getPercentage());
    salesOrderTaxes.add(tax);
  }

  private Long getCompanyIdByCode(String companyCode) {
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private void executeTransaction(
      DObSalesOrder salesOrder,
      IObSalesOrderData salesOrderData,
      IObSalesOrderCompanyStoreData companyStoreData,
      List<IObSalesOrderTax> salesOrderTaxes)
      throws Exception {
    salesOrder.setHeaderDetail(IDObSalesOrder.companyStoreDataAttr, companyStoreData);
    salesOrder.setHeaderDetail(IDObSalesOrder.salesOrderDataAttr, salesOrderData);
    addTaxesToSalesOrder(salesOrder, salesOrderTaxes);
    salesOrderRep.create(salesOrder);
  }

  private void addTaxesToSalesOrder(
      DObSalesOrder salesOrder, List<IObSalesOrderTax> salesOrderTaxes) {
    for (IObSalesOrderTax tax : salesOrderTaxes) {
      salesOrder.addLineDetail(IDObSalesOrder.salesOrderTaxesAttr, tax);
    }
  }

  private Long getCustomerId(DObSalesOrderCreateValueObject salesOrderCreateValueObject) {
    String customerCode = salesOrderCreateValueObject.getCustomerCode();
    MObCustomer customer = customerRep.findOneByUserCode(customerCode);
    return customer.getId();
  }

  private Long getSalesResponsibleId(DObSalesOrderCreateValueObject salesOrderCreateValueObject) {
    String salesResponsibleCode = salesOrderCreateValueObject.getSalesResponsibleCode();
    CObEnterprise salesResponsible = salesResponsibleRep.findOneByUserCode(salesResponsibleCode);
    return salesResponsible.getId();
  }

  private Long getTypeId(DObSalesOrderCreateValueObject salesOrderCreateValueObject) {
    String typeCode = salesOrderCreateValueObject.getTypeCode();
    CObSalesOrderType salesOrdertype = salesOrderTypeRep.findOneByUserCode(typeCode);
    return salesOrdertype.getId();
  }

  private Long getPurchaseUnitId(DObSalesOrderCreateValueObject salesOrderCreateValueObject) {
    String businessUnitCode = salesOrderCreateValueObject.getBusinessUnitCode();
    CObPurchasingUnit businessUnit = businessUnitRep.findOneByUserCode(businessUnitCode);
    return businessUnit.getId();
  }

  public void setBusinessUnitRep(CObPurchasingUnitRep businessUnitRep) {
    this.businessUnitRep = businessUnitRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setSalesOrderTypeRep(CObSalesOrderTypeRep salesOrderTypeRep) {
    this.salesOrderTypeRep = salesOrderTypeRep;
  }

  public void setSalesResponsibleRep(CObSalesResponsibleRep salesResponsibleRep) {
    this.salesResponsibleRep = salesResponsibleRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  public void setCompanyTaxesGeneralModelRep(
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep) {
    this.companyTaxesGeneralModelRep = companyTaxesGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
