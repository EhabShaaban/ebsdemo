package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;

public abstract class DObPaymentToVendorCreateCommand<RefDocument extends DObPaymentReferenceDocumentGeneralModel> extends DObPaymentWithReferenceDocumentCreateCommand<RefDocument> {

	private MObVendorRep vendorRep;

	public DObPaymentToVendorCreateCommand(DObPaymentRequestStateMachine stateMachine){
		super(stateMachine);
	}

	@Override
	protected void setBusinessPartner(DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		setVendor(paymentRequestValueObject, paymentDetails);
	}

	private void setVendor(
					DObPaymentRequestCreateValueObject valueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		MObVendor vendor = vendorRep.findOneByUserCode(valueObject.getBusinessPartnerCode());
		paymentDetails.setBusinessPartnerId(vendor.getId());
		paymentDetails.setDueDocumentType(valueObject.getDueDocumentType().name());
	}

	public void setVendorRep(MObVendorRep vendorRep) {
		this.vendorRep = vendorRep;
	}
}
