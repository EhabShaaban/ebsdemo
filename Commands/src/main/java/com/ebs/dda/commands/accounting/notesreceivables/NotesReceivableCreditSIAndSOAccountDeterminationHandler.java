package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCustomerAccountingDetails;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerGeneralModel;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.CUSTOMERS_GL_ACCOUNT;

public class NotesReceivableCreditSIAndSOAccountDeterminationHandler
    extends NotesReceivableCreditAccountDeterminationHandler {

  private MObCustomerGeneralModelRep customerGeneralModelRep;

  public void setCreditAccountingDetailRecord(
      IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel,
      DObMonetaryNotes notesReceivable,
      IObMonetaryNotesDetailsGeneralModel notesDetails) {

    IObAccountingDocumentCustomerAccountingDetails creditAccountingDetailForCustomer =
        createCreditAccountingDetailForCustomer(referenceDocumentGeneralModel, notesDetails);

    commandUtils.setCreationAndModificationInfoAndDate(creditAccountingDetailForCustomer);
    notesReceivable.addLineDetail(
        IDObAccountingDocument.accountingCustomerDetailsAttr, creditAccountingDetailForCustomer);
  }

  private IObAccountingDocumentCustomerAccountingDetails createCreditAccountingDetailForCustomer(
      IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel,
      IObMonetaryNotesDetailsGeneralModel notesDetails) {

    IObAccountingDocumentCustomerAccountingDetails customerAccountingDetails =
        new IObAccountingDocumentCustomerAccountingDetails();

    customerAccountingDetails.setObjectTypeCode(IDocumentTypes.NOTES_RECEIVABLE);
    customerAccountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
    customerAccountingDetails.setAmount(referenceDocumentGeneralModel.getAmountToCollect());

    LObGlobalGLAccountConfigGeneralModel customerGlobalGLAccount =
        globalAccountConfigGeneralModelRep.findOneByUserCode(CUSTOMERS_GL_ACCOUNT);
    customerAccountingDetails.setGlAccountId(customerGlobalGLAccount.getGlAccountId());

    MObCustomerGeneralModel customer =
        customerGeneralModelRep.findOneByUserCode(notesDetails.getBusinessPartnerCode());
    customerAccountingDetails.setGlSubAccountId(customer.getId());

    return customerAccountingDetails;
  }

  public void setCustomerGeneralModelRep(MObCustomerGeneralModelRep customerGeneralModelRep) {
    this.customerGeneralModelRep = customerGeneralModelRep;
  }
}
