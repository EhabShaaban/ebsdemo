package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import io.reactivex.Observable;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class DObPurchaseOrderSaveItemQuantityCommand
    implements ICommand<IObOrderLineDetailsQuantitiesValueObject> {

  private final CommandUtils commandUtils;
  private IObOrderLineDetailsQuantitiesRep quantitiesRep;
  private IObOrderLineDetailsRep orderLineDetailsRep;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  private DObPurchaseOrderRep purchaseOrderRep;
  private CObMeasureRep measureRep;
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;
  private DObPurchaseOrderGeneralModelRep orderHeaderGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;

  private PlatformTransactionManager transactionManager;

  public DObPurchaseOrderSaveItemQuantityCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject) throws Exception {
    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            try {
              DObPurchaseOrder purchaseOrder =
                  purchaseOrderRep.findOneByUserCode(quantitiesValueObject.getOrderCode());
              IObOrderLineDetails orderLineDetail = getOrderLineDetail(quantitiesValueObject);
              IObOrderLineDetailsQuantities itemQuantity =
                  updateAndGetItemQuantity(quantitiesValueObject, orderLineDetail);

              orderLineDetail.addLineDetail(IIObOrderLineDetails.itemQuantitiesAttr, itemQuantity);
              purchaseOrder.addLineDetail(IDObPurchaseOrder.lineAttr, orderLineDetail);
              commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
              purchaseOrderRep.update(purchaseOrder);
            } catch (Exception e) {
              status.setRollbackOnly();
              throw new RuntimeException("Save Quantity failed !");
            }
          }
        });
    return Observable.just("SUCCESS");
  }

  private IObOrderLineDetails getOrderLineDetail(
      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject) {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(
            quantitiesValueObject.getOrderCode(), quantitiesValueObject.getItemCode());
    IObOrderLineDetails orderLineDetail =
        orderLineDetailsRep.findOneById(orderLineDetailsGeneralModel.getId());
    commandUtils.setModificationInfoAndModificationDate(orderLineDetail);
    return orderLineDetail;
  }

  private IObOrderLineDetailsQuantities updateAndGetItemQuantity(
      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject,
      IObOrderLineDetails orderLineDetails) {
    IObOrderLineDetailsQuantities quantity;
    if (quantitiesValueObject.getQuantityId() == null) {
      quantity = new IObOrderLineDetailsQuantities();
      quantity.setRefInstanceId(orderLineDetails.getId());
      commandUtils.setCreationAndModificationInfoAndDate(quantity);
    } else {
      quantity = quantitiesRep.findOneById(quantitiesValueObject.getQuantityId());
      commandUtils.setModificationInfoAndModificationDate(quantity);
    }
    setQuantityValues(quantitiesValueObject, quantity);
    return quantity;
  }

  private void setQuantityValues(
      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject,
      IObOrderLineDetailsQuantities quantity) {
    CObMeasure measure = measureRep.findOneByUserCode(quantitiesValueObject.getOrderUnitCode());
    DObPurchaseOrderGeneralModel order =
        orderHeaderGeneralModelRep.findOneByUserCode(quantitiesValueObject.getOrderCode());

    quantity.setPrice(quantitiesValueObject.getPrice());
    quantity.setDiscountPercentage(quantitiesValueObject.getDiscountPercentage());
    quantity.setQuantity(quantitiesValueObject.getQuantity());
    quantity.setOrderUnitId(measure.getId());
    quantity.setItemVendorRecordId(getItemVendorRecordId(order, quantitiesValueObject));
  }

  private Long getItemVendorRecordId(
      DObPurchaseOrderGeneralModel order,
      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject) {
    ItemVendorRecordUnitOfMeasuresGeneralModel itemVendorRecordUnitOfMeasures =
        itemVendorRecordUnitOfMeasuresGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndPurchasingUnitCodeAndAlternativeUnitOfMeasureCode(
                quantitiesValueObject.getItemCode(),
                order.getVendorCode(),
                order.getPurchaseUnitCode(),
                quantitiesValueObject.getOrderUnitCode());

    if (itemVendorRecordUnitOfMeasures == null) {
      ItemVendorRecordGeneralModel itemVendorRecord =
          itemVendorRecordGeneralModelRep
              .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                  quantitiesValueObject.getItemCode(),
                  order.getVendorCode(),
                  quantitiesValueObject.getOrderUnitCode(),
                  order.getPurchaseUnitCode());
      return itemVendorRecord.getId();
    }
    return itemVendorRecordUnitOfMeasures.getItemvendorRecordId();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setQuantitiesRep(IObOrderLineDetailsQuantitiesRep quantitiesRep) {
    this.quantitiesRep = quantitiesRep;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setOrderLineDetailsRep(IObOrderLineDetailsRep orderLineDetailsRep) {
    this.orderLineDetailsRep = orderLineDetailsRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setOrderLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
  }

  public void setItemVendorRecordUnitOfMeasuresGeneralModelRep(
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep) {
    this.itemVendorRecordUnitOfMeasuresGeneralModelRep =
        itemVendorRecordUnitOfMeasuresGeneralModelRep;
  }

  public void setOrderHeaderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep orderHeaderGeneralModelRep) {
    this.orderHeaderGeneralModelRep = orderHeaderGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }
}
