package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum.DueDocumentType;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyData;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestWithoutReferencePaymentDetails;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.customer.MObEmployee;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;

public class DObUnderSettlementPaymentCreateCommand extends DObPaymentRequestCreateCommand {

	private MObEmployeeRep employeeRep;
	private CObCompanyRep companyRep;

	public DObUnderSettlementPaymentCreateCommand(DObPaymentRequestStateMachine stateMachine) {
		super(stateMachine);
	}

	@Override
	protected void setCompany(IObPaymentRequestCompanyData paymentRequestCompanyData,
					DObPaymentRequestCreateValueObject paymentRequestValueObject) {
		CObCompany company = companyRep
						.findOneByUserCode(paymentRequestValueObject.getCompanyCode());
		paymentRequestCompanyData.setCompanyId(company.getId());
	}

	@Override
	protected void findReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject) {

	}

	@Override
	protected void setReferenceDocumentCurrency(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {

	}

	@Override
	protected void setReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {

	}

	@Override
	protected void setBusinessPartner(DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		setEmployeeIfPaymentTypeIsUnderSettlement(paymentRequestValueObject, paymentDetails);
	}

	private void setEmployeeIfPaymentTypeIsUnderSettlement(
					DObPaymentRequestCreateValueObject valueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		MObEmployee employee = employeeRep.findOneByUserCode(valueObject.getBusinessPartnerCode());
		paymentDetails.setBusinessPartnerId(employee.getId());
	}

	@Override
	protected IObPaymentRequestPaymentDetails createPaymentDetailsInstance(
					DueDocumentType documentType) {
		return new IObPaymentRequestWithoutReferencePaymentDetails();
	}

	public void setCompanyRep(CObCompanyRep companyRep) {
		this.companyRep = companyRep;
	}

	public void setEmployeeRep(MObEmployeeRep employeeRep) {
		this.employeeRep = employeeRep;
	}
}
