package com.ebs.dda.commands.accounting.notesreceivables.factory;

import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableCreditAccountDeterminationHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableCreditNRAccountDeterminationHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableCreditSIAndSOAccountDeterminationHandler;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;

public class NotesReceivablesCreditAccountDeterminationFactory {

  private NotesReceivableCreditNRAccountDeterminationHandler
      notesReceivableCreditNRAccountDeterminationHandler;
  private NotesReceivableCreditSIAndSOAccountDeterminationHandler
      notesReceivableCreditSIAndSOAccountDeterminationHandler;

  public NotesReceivableCreditAccountDeterminationHandler
      createNotesReceivableCreditAccountDeterminationHandler(String type) throws Exception {

    if (isNotesReceivable(type)) {
      return notesReceivableCreditNRAccountDeterminationHandler;
    } else if (isSalesInvoice(type) || isSalesOrder(type)) {
      return notesReceivableCreditSIAndSOAccountDeterminationHandler;
    }
    throw new UnsupportedOperationException("Not supported ReferenceDocument type");
  }

  private boolean isNotesReceivable(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name());
  }

  private boolean isSalesOrder(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name());
  }

  private boolean isSalesInvoice(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name());
  }

  public void setNotesReceivableCreditNRAccountDeterminationHandler(
      NotesReceivableCreditNRAccountDeterminationHandler
          notesReceivableCreditNRAccountDeterminationHandler) {
    this.notesReceivableCreditNRAccountDeterminationHandler =
        notesReceivableCreditNRAccountDeterminationHandler;
  }

  public void setNotesReceivableCreditSIAndSOAccountDeterminationHandler(
      NotesReceivableCreditSIAndSOAccountDeterminationHandler
          notesReceivableCreditSIAndSOAccountDeterminationHandler) {
    this.notesReceivableCreditSIAndSOAccountDeterminationHandler =
        notesReceivableCreditSIAndSOAccountDeterminationHandler;
  }
}
