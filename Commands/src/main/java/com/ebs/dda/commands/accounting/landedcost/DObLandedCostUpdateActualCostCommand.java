package com.ebs.dda.commands.accounting.landedcost;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import io.reactivex.Observable;

public class DObLandedCostUpdateActualCostCommand
    implements ICommand<DObLandedCostUpdateActualCostValueObject> {

  private CommandUtils commandUtils;
  private DObLandedCostRep landedCostRep;
  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  private IObLandedCostFactorItemsRep landedCostFactorItemsRep;
  private MObItemRep itemRep;

  public DObLandedCostUpdateActualCostCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObLandedCostUpdateActualCostValueObject valueObject)
      throws Exception {

    DObLandedCostGeneralModel landedCost = landedCostGeneralModelRep
        .findByPurchaseOrderCodeAndCurrentStatesLike(valueObject.getPurchaseOrderCode(),
            "%" + DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED + "%");

    Map<String, BigDecimal> itemsCodeAmount = valueObject.getItemsCodeAmount();
    
    for (String itemCode : itemsCodeAmount.keySet()) {
      BigDecimal itemAmount = itemsCodeAmount.get(itemCode);
      
      Long mobItemId = itemRep.findOneByUserCode(itemCode).getId();

      Optional<IObLandedCostFactorItems> presistedItem =
          getCostFactorItem(valueObject, landedCost.getId(), mobItemId);

      IObLandedCostFactorItems updatedItem = null;
      if (presistedItem.isPresent()) {
        updatedItem = updateLandedCostItemActualCost(presistedItem.get(), itemAmount);
      } else {
        updatedItem = createLandedCostFactorItem(landedCost.getId(), mobItemId, itemAmount);
      }

      IObLandedCostFactorItemsDetails itemsDetails =
          createLandedCostFactorItemDetails(valueObject.getRefDocumentId(), itemAmount);

      try {
        updatedItem.addLineDetail(IIObLandedCostFactorItems.costFactorItemsDetails, itemsDetails);
        landedCostFactorItemsRep.update(updatedItem);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    updateLandedCostInfo(landedCost);
    return Observable.just(landedCost.getUserCode());
  }

  private void updateLandedCostInfo(DObLandedCostGeneralModel landedCostGeneralModel) throws Exception {
    String userCode = landedCostGeneralModel.getUserCode();
    DObLandedCost landedCost = landedCostRep.findOneByUserCode(userCode);
    commandUtils.setModificationInfoAndModificationDate(landedCost);
    landedCostRep.update(landedCost);
  }

  private Optional<IObLandedCostFactorItems> getCostFactorItem(
      DObLandedCostUpdateActualCostValueObject valueObject, Long landedCostId, Long mobItemId) {

    List<IObLandedCostFactorItems> costFactorItems =
        landedCostFactorItemsRep.findByRefInstanceId(landedCostId);

    Optional<IObLandedCostFactorItems> costFactorItem =
        costFactorItems.stream().filter(item -> item.getCostItemId().equals(mobItemId)).findAny()
            .map(item -> Optional.of(item)).orElse(Optional.empty());

    return costFactorItem;
  }

  private IObLandedCostFactorItems updateLandedCostItemActualCost(
      IObLandedCostFactorItems costFactorItem, BigDecimal amount) {

    if(costFactorItem.getActualValue() != null) {
      amount = amount.add(costFactorItem.getActualValue());
    }
    
    costFactorItem.setActualValue(amount);
    this.commandUtils.setModificationInfoAndModificationDate(costFactorItem);

    return costFactorItem;
  }

  private IObLandedCostFactorItems createLandedCostFactorItem(Long landedCostId,
      Long mobItemId, BigDecimal amount) {

    IObLandedCostFactorItems costFactorItem = new IObLandedCostFactorItems();

    costFactorItem.setCostItemId(mobItemId);
    costFactorItem.setRefInstanceId(landedCostId);

    costFactorItem.setEstimateValue(BigDecimal.ZERO);
    costFactorItem.setActualValue(amount);

    this.commandUtils.setCreationAndModificationInfoAndDate(costFactorItem);

    return costFactorItem;
  }

  private IObLandedCostFactorItemsDetails createLandedCostFactorItemDetails(Long refDocumentId,
      BigDecimal actualValue) {

    IObLandedCostFactorItemsDetails details = new IObLandedCostFactorItemsDetails();

    details.setDocumentInfoId(refDocumentId);
    details.setActualValue(actualValue);

    this.commandUtils.setCreationAndModificationInfoAndDate(details);
    return details;
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public void setLandedCostRep(DObLandedCostRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setLandedCostFactorItemsRep(IObLandedCostFactorItemsRep landedCostFactorItemsRep) {
    this.landedCostFactorItemsRep = landedCostFactorItemsRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
