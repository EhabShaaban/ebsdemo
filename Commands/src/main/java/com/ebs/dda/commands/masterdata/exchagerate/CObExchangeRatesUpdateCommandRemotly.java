package com.ebs.dda.commands.masterdata.exchagerate;

import static com.ebs.dac.foundation.apis.exchangerates.ICurrencyISO.EGP;
import static com.ebs.dac.foundation.realization.statemachine.PrimitiveStateMachine.ACTIVE_STATE;
import static com.ebs.dac.i18n.locales.ISysDefaultLocales.ARABIC_LOCALE;
import static com.ebs.dac.i18n.locales.ISysDefaultLocales.ENGLISH_LOCALE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.realization.exchangerates.CBEExchangeRateClient;
import com.ebs.dac.foundation.realization.exchangerates.CBEExchangeRateDTO;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateType;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateType;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateTypeRep;
import io.reactivex.Observable;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 21, 2020 9:14:47 AM
 */

public class CObExchangeRatesUpdateCommandRemotly implements ICommand<Long> {

  private static final String SYSTEM_USER = "System";

  private static final Logger LOGGER =
      LoggerFactory.getLogger(CObExchangeRatesUpdateCommandRemotly.class);

  private MasterDataCodeGenrator codeGenrator;
  private CBEExchangeRateClient cbeClient;

  private CObCurrencyRep currencyRep;
  private CObExchangeRateRep exchangeRateRep;
  private CObExchangeRateTypeRep exchangeRateTypeRep;

  public CObExchangeRatesUpdateCommandRemotly() {}

  @Override
  public <T> Observable<T> executeCommand(Long object) throws Exception {

    List<CObExchangeRate> exRates = Stream.of(cbeClient.getLatestExchangeRates())
        .map(dto -> convertDtoToEntity(dto)).filter(Objects::nonNull).collect(Collectors.toList());

    exchangeRateRep.saveAll(exRates);

    return Observable.empty();
  }

  private CObExchangeRate convertDtoToEntity(CBEExchangeRateDTO dto) {

    CObCurrency firstCurrency = getCurrencyEntityByISO(dto.getCurrencyISO());
    if (firstCurrency == null) {
      return null;
    }
    CObCurrency secondcurrency = getCurrencyEntityByISO(EGP);

    CObExchangeRate exRate = new CObExchangeRate();

    exRate.setFirstCurrencyId(firstCurrency.getId());
    exRate.setFirstValue(BigDecimal.ONE);

    exRate.setSecondCurrencyId(secondcurrency.getId());
    exRate.setSecondValue(BigDecimal.valueOf(dto.getSell()));

    exRate.setName(constructCurrencyName(firstCurrency.getIso(), secondcurrency.getIso()));
    exRate.setCurrentStates(new HashSet<String>(Arrays.asList(ACTIVE_STATE)));

    exRate.setTypeId(getCBEExRateTypeId());
    exRate.setUserCode(codeGenrator.generateUserCode(CObExchangeRate.class));

    exRate.setCreationInfo(SYSTEM_USER);
    exRate.setModificationInfo(SYSTEM_USER);

    DateTime currentDate = new DateTime();
    exRate.setModifiedDate(currentDate);
    exRate.setCreationDate(currentDate);
    exRate.setValidFrom(currentDate);

    return exRate;
  }

  private Long getCBEExRateTypeId() {
    CObExchangeRateType exchangeRateType =
        exchangeRateTypeRep.findOneByUserCode(ICObExchangeRateType.CENTRAL_BANK_OF_EGYPT);
    return exchangeRateType.getId();
  }

  private LocalizedString constructCurrencyName(String firstCurrencyISO, String secondCurrencyISO) {
    String name = firstCurrencyISO + "-" + secondCurrencyISO;
    LocalizedString localizedItemName = new LocalizedString();
    localizedItemName.setValue(ARABIC_LOCALE, name);
    localizedItemName.setValue(ENGLISH_LOCALE, name);
    return localizedItemName;
  }

  private CObCurrency getCurrencyEntityByISO(String currencyISO) {
    CObCurrency currency = currencyRep.findOneByIso(currencyISO);
    if (currency == null) {
      LOGGER.info("Currency ISO '" + currencyISO + "' is not exist");
    }
    return currency;
  }

  public void setCodeGenrator(MasterDataCodeGenrator codeGenrator) {
    this.codeGenrator = codeGenrator;
  }

  public void setCbeClient(CBEExchangeRateClient cbeClient) {
    this.cbeClient = cbeClient;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setExchangeRateRep(CObExchangeRateRep exchangeRateRep) {
    this.exchangeRateRep = exchangeRateRep;
  }

  public void setExchangeRateTypeRep(CObExchangeRateTypeRep exchangeRateTypeRep) {
    this.exchangeRateTypeRep = exchangeRateTypeRep;
  }

}
