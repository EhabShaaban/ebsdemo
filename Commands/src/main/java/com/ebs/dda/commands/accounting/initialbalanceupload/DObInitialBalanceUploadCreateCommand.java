package com.ebs.dda.commands.accounting.initialbalanceupload;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.initialbalanceupload.statemachines.DObInitialBalanceUploadStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.initialbalanceupload.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class DObInitialBalanceUploadCreateCommand
    implements ICommand<DObInitialBalanceUploadCreateValueObject> {
  private final CommandUtils commandUtils;
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private final DObInitialBalanceUploadStateMachine stateMachine;
  private CObPurchasingUnitRep purchasingUnitRep;
  private CObCompanyRep companyRep;
  private CObFiscalYearRep fiscalYearRep;
  private DObInitialBalanceUploadRep initialBalanceUploadRep;
  private IUserAccountSecurityService userAccountSecurityService;
  private MObVendorGeneralModelRep mObVendorGeneralModelRep;
  private HObChartOfAccountsGeneralModelRep hObChartOfAccountsGeneralModelRep;
  private CObCurrencyRep currencyRep;

  public DObInitialBalanceUploadCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) throws Exception {
    commandUtils = new CommandUtils();
    stateMachine = new DObInitialBalanceUploadStateMachine();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(DObInitialBalanceUploadCreateValueObject valueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObInitialBalanceUpload.class.getSimpleName());
    DObInitialBalanceUpload initialBalanceUpload = new DObInitialBalanceUpload();
    setInitialBalanceUploadData(userCode, initialBalanceUpload, valueObject);

    IObInitialBalanceUploadCompanyData initialBalanceUploadCompanyDetails =
        new IObInitialBalanceUploadCompanyData();
    setInitialBalanceUploadCompany(valueObject, initialBalanceUploadCompanyDetails);
    initialBalanceUpload.setHeaderDetail(
        IDObInitialBalanceUpload.initialBalanceUploadCompanyDetails,
        initialBalanceUploadCompanyDetails);

    List<IObInitialBalanceUploadItem> itemsList =
        commandUtils.convertCSVFileToEntityList(
            valueObject.getAttachment(), IObInitialBalanceUploadItem.class);
    setInitialBalanceUploadItemInfo(initialBalanceUpload, itemsList);

    initialBalanceUploadRep.create(initialBalanceUpload);

    return Observable.just(userCode);
  }

  private void setInitialBalanceUploadItemInfo(
      DObInitialBalanceUpload initialBalanceUpload, List<IObInitialBalanceUploadItem> itemsList) {
    List<IObInitialBalanceUploadItem> initialBalanceUploadItems = new ArrayList<>();

    for (IObInitialBalanceUploadItem item : itemsList) {
      CObCurrency currency = currencyRep.findOneByUserCode(item.getCurrencyCode());
      HObChartOfAccountsGeneralModel glAccountGeneralModel =
          hObChartOfAccountsGeneralModelRep.findOneByOldAccountCode(item.getOldGlAccountCode());
      if (glAccountGeneralModel.getLeadger().equals("PO")) {

        addIObInitialBalanceUploadOrderItem(
            initialBalanceUploadItems, item, currency, glAccountGeneralModel);
      } else {
        addIObInitialBalanceUploadVendorItem(
            initialBalanceUploadItems, item, currency, glAccountGeneralModel);
      }
    }
    initialBalanceUpload.setLinesDetails(
        IDObInitialBalanceUpload.itemsAttr, initialBalanceUploadItems);
  }

  private void addIObInitialBalanceUploadVendorItem(
      List<IObInitialBalanceUploadItem> itemsList,
      IObInitialBalanceUploadItem item,
      CObCurrency currency,
      HObChartOfAccountsGeneralModel glAccountGeneralModel) {
    IObInitialBalanceUploadVendorItem initialBalanceUploadVendorItem =
        getIObInitialBalanceUploadVendorItem(item, currency, glAccountGeneralModel);
    commandUtils.setCreationAndModificationInfoAndDate(initialBalanceUploadVendorItem);
    itemsList.add(initialBalanceUploadVendorItem);
  }

  private IObInitialBalanceUploadVendorItem getIObInitialBalanceUploadVendorItem(
      IObInitialBalanceUploadItem item,
      CObCurrency currency,
      HObChartOfAccountsGeneralModel glAccountGeneralModel) {
    IObInitialBalanceUploadVendorItem initialBalanceUploadVendorItem =
        new IObInitialBalanceUploadVendorItem();
    initialBalanceUploadVendorItem.setGlAccountId(glAccountGeneralModel.getId());
    initialBalanceUploadVendorItem.setDebit(item.getDebit());
    initialBalanceUploadVendorItem.setCredit(item.getCredit());
    initialBalanceUploadVendorItem.setCurrencyId(currency.getId());
    initialBalanceUploadVendorItem.setCurrencyPrice(item.getCurrencyPrice());
    MObVendorGeneralModel vendorGeneralModel =
        mObVendorGeneralModelRep.findOneByOldVendorNumber(item.getOldSubAccount());
    initialBalanceUploadVendorItem.setVendorId(vendorGeneralModel.getId());
    return initialBalanceUploadVendorItem;
  }

  private void addIObInitialBalanceUploadOrderItem(
      List<IObInitialBalanceUploadItem> itemsList,
      IObInitialBalanceUploadItem item,
      CObCurrency currency,
      HObChartOfAccountsGeneralModel glAccountGeneralModel) {

    IObInitialBalanceUploadDocumentOrderItem initialBalanceUploadDocumentOrderItem =
        getIObInitialBalanceUploadDocumentOrderItem(item, currency, glAccountGeneralModel);

    commandUtils.setCreationAndModificationInfoAndDate(initialBalanceUploadDocumentOrderItem);
    itemsList.add(initialBalanceUploadDocumentOrderItem);
  }

  private IObInitialBalanceUploadDocumentOrderItem getIObInitialBalanceUploadDocumentOrderItem(
      IObInitialBalanceUploadItem item,
      CObCurrency currency,
      HObChartOfAccountsGeneralModel glAccountGeneralModel) {
    IObInitialBalanceUploadDocumentOrderItem initialBalanceUploadDocumentOrderItem =
        new IObInitialBalanceUploadDocumentOrderItem();
    initialBalanceUploadDocumentOrderItem.setGlAccountId(glAccountGeneralModel.getId());
    initialBalanceUploadDocumentOrderItem.setDebit(item.getDebit());
    initialBalanceUploadDocumentOrderItem.setCredit(item.getCredit());
    initialBalanceUploadDocumentOrderItem.setCurrencyId(currency.getId());
    initialBalanceUploadDocumentOrderItem.setCurrencyPrice(item.getCurrencyPrice());
    return initialBalanceUploadDocumentOrderItem;
  }

  private void setInitialBalanceUploadData(
      String userCode,
      DObInitialBalanceUpload initialBalanceUpload,
      DObInitialBalanceUploadCreateValueObject valueObject)
      throws Exception {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setCreationAndModificationInfoAndDate(initialBalanceUpload);
    stateMachine.initObjectState(initialBalanceUpload);
    initialBalanceUpload.setUserCode(userCode);
    CObFiscalYear fiscalPeriod = getFiscalPeriodByUserCode(valueObject);
    initialBalanceUpload.setFiscalPeriodId(fiscalPeriod.getId());
    initialBalanceUpload.setDocumentOwnerId(valueObject.getDocumentOwnerId());
  }

  private CObFiscalYear getFiscalPeriodByUserCode(
      DObInitialBalanceUploadCreateValueObject valueObject) {
    return fiscalYearRep.findOneByUserCode(valueObject.getFiscalPeriodCode());
  }

  private void setInitialBalanceUploadCompany(
      DObInitialBalanceUploadCreateValueObject initialBalanceUploadCreateValueObject,
      IObInitialBalanceUploadCompanyData iObInitialBalanceUploadCompanyData) {
    commandUtils.setCreationAndModificationInfoAndDate(iObInitialBalanceUploadCompanyData);
    CObCompany company =
        companyRep.findOneByUserCode(initialBalanceUploadCreateValueObject.getCompanyCode());
    iObInitialBalanceUploadCompanyData.setCompanyId(company.getId());

    CObPurchasingUnit purchaseUnit =
        purchasingUnitRep.findOneByUserCode(
            initialBalanceUploadCreateValueObject.getPurchaseUnitCode());
    iObInitialBalanceUploadCompanyData.setPurchaseUnitId(purchaseUnit.getId());
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  public void setInitialBalanceUploadRep(DObInitialBalanceUploadRep initialBalanceUploadRep) {
    this.initialBalanceUploadRep = initialBalanceUploadRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setMObVendorGeneralModelRep(MObVendorGeneralModelRep mObVendorGeneralModelRep) {
    this.mObVendorGeneralModelRep = mObVendorGeneralModelRep;
  }

  public void sethObChartOfAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep hObChartOfAccountsGeneralModelRep) {
    this.hObChartOfAccountsGeneralModelRep = hObChartOfAccountsGeneralModelRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }
}
