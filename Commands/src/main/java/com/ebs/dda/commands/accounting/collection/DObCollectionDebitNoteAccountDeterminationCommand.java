package com.ebs.dda.commands.accounting.collection;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentVendorAccountingDetails;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;

public class DObCollectionDebitNoteAccountDeterminationCommand
    extends DObCollectionAccountDeterminationCommand {

  private IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep;
  private HObChartOfAccountRep chartOfAccountRep;

  @Override
  protected void getCreditAccountingDetailRecord(
      IObCollectionDetailsGeneralModel collectionDetailsGM, DObCollection collection) {

    IObAccountingDocumentVendorAccountingDetails accountingDetail =
        getCreditAccountingDetailForVendor(collectionDetailsGM);

    commandUtils.setCreationAndModificationInfoAndDate(accountingDetail);
    collection.addLineDetail(IDObAccountingDocument.accountingVendorDetailsAttr, accountingDetail);
  }

  private IObAccountingDocumentVendorAccountingDetails getCreditAccountingDetailForVendor(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {
    IObAccountingDocumentVendorAccountingDetails accountingDetails =
        new IObAccountingDocumentVendorAccountingDetails();

    accountingDetails.setObjectTypeCode(IDocumentTypes.COLLECTION);
    accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
    accountingDetails.setAmount(collectionDetailsGM.getAmount());

    IObVendorAccountingInfoGeneralModel vendorAccountingInfo =
        vendorAccountingInfoRep.findByVendorCode(collectionDetailsGM.getBusinessPartnerCode());
    accountingDetails.setGlSubAccountId(vendorAccountingInfo.getRefInstanceId());

    HObGLAccount account =
        chartOfAccountRep.findOneByUserCode(vendorAccountingInfo.getAccountCode());
    accountingDetails.setGlAccountId(account.getId());

    return accountingDetails;
  }

  public void setVendorAccountingInfoRep(
      IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep) {
    this.vendorAccountingInfoRep = vendorAccountingInfoRep;
  }

  public void setChartOfAccountRep(HObChartOfAccountRep chartOfAccountRep) {
    this.chartOfAccountRep = chartOfAccountRep;
  }


}
