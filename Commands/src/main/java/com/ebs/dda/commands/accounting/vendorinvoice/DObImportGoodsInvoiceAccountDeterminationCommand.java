package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;

import java.math.BigDecimal;
import java.util.List;

public class DObImportGoodsInvoiceAccountDeterminationCommand extends DObVendorInvoiceAccountDeterminationCommand{

    @Override
    protected Long getOrderId(List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems) {
        return groupedItems.get(0).getOrderId();
    }

    @Override
    protected BigDecimal getGroupedItemsAmount(String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems) {
        return groupedItems.stream().map(DObVendorInvoiceItemAccountsPreparationGeneralModel::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
