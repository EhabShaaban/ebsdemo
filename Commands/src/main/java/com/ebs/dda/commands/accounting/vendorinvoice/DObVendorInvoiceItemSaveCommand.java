package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.DObInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

public class DObVendorInvoiceItemSaveCommand implements ICommand<DObInvoiceItemValueObject> {
  private CommandUtils commandUtils;
  private DObVendorInvoiceRep invoiceRep;
  private MObItemGeneralModelRep mObItemGeneralModelRep;
  private CObMeasureRep cObMeasureRep;
  private IObVendorInvoiceItemRep itemsRep;
  private IObVendorInvoiceSummaryUpdateCommand SummaryUpdateCommand;
  private IObVendorInvoiceTaxesUpdateCommand taxesUpdateCommand;

  public DObVendorInvoiceItemSaveCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<DObVendorInvoice> executeCommand(DObInvoiceItemValueObject invoiceItemValueObject)
      throws Exception {
    DObVendorInvoice invoice = invoiceRep.findOneByUserCode(invoiceItemValueObject.getInvoiceCode());
    IObVendorInvoiceItem invoiceItem;
    if (invoiceItemValueObject.getItemId() != null) {
      invoiceItem = getInvoiceItemById(invoiceItemValueObject);
    } else {
      invoiceItem = getInvoiceItemInstance(invoiceItemValueObject);
    }
    prepareInvoiceItem(invoice, invoiceItem);
    commandUtils.setModificationInfo(invoice);
    invoiceRep.update(invoice);
    taxesUpdateCommand.executeCommand(invoice);
    SummaryUpdateCommand.executeCommand(invoice.getUserCode());
    return Observable.just(invoice);
  }

  private IObVendorInvoiceItem getInvoiceItemInstance(
      DObInvoiceItemValueObject invoiceItemValueObject) {
    IObVendorInvoiceItem invoiceItem = new IObVendorInvoiceItem();

    MObItemGeneralModel item =
        mObItemGeneralModelRep.findByUserCode(invoiceItemValueObject.getItemCode());
    CObMeasure cObMeasure =
        cObMeasureRep.findOneByUserCode(invoiceItemValueObject.getOrderUnitCode());

    invoiceItem.setItemId(item.getId());
    invoiceItem.setBaseUnitOfMeasureId(item.getBasicUnitOfMeasure());
    invoiceItem.setOrderUnitOfMeasureId(cObMeasure.getId());
    invoiceItem.setPrice(invoiceItemValueObject.getUnitPrice());
    invoiceItem.setQunatityInOrderUnit(invoiceItemValueObject.getQuantityInOrderUnit());

    return invoiceItem;
  }

  private IObVendorInvoiceItem getInvoiceItemById(
      DObInvoiceItemValueObject invoiceItemValueObject) {
    IObVendorInvoiceItem item = itemsRep.findOneById(invoiceItemValueObject.getItemId());
    item.setPrice(invoiceItemValueObject.getUnitPrice());
    item.setQunatityInOrderUnit(invoiceItemValueObject.getQuantityInOrderUnit());
    return item;
  }

  private void prepareInvoiceItem(DObVendorInvoice invoice, IObVendorInvoiceItem invoiceItem) {
    setInvoiceItemSysInfo(invoiceItem, invoice);
    invoice.addLineDetail(IDObVendorInvoice.invoiceItemsAttr, invoiceItem);
  }

  private void setInvoiceItemSysInfo(IObVendorInvoiceItem invoiceItem, DObVendorInvoice invoice) {
    invoiceItem.setRefInstanceId(invoice.getId());
    commandUtils.setCreationAndModificationInfoAndDate(invoiceItem);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setmObItemGeneralModelRep(MObItemGeneralModelRep mObItemGeneralModelRep) {
    this.mObItemGeneralModelRep = mObItemGeneralModelRep;
  }

  public void setcObMeasureRep(CObMeasureRep cObMeasureRep) {
    this.cObMeasureRep = cObMeasureRep;
  }

  public void setInvoiceRep(DObVendorInvoiceRep invoiceRep) {
    this.invoiceRep = invoiceRep;
  }

  public void setItemsRep(IObVendorInvoiceItemRep itemsRep) {
    this.itemsRep = itemsRep;
  }

  public void setSummaryUpdateCommand(IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand) {
    SummaryUpdateCommand = summaryUpdateCommand;
  }

  public void setTaxesUpdateCommand(IObVendorInvoiceTaxesUpdateCommand taxesUpdateCommand) {
    this.taxesUpdateCommand = taxesUpdateCommand;
  }
}
