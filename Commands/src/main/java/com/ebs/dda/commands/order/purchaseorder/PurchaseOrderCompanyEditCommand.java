package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderBillToCompany;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObPurchaseOrderBillToCompanyRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCompanyValueObject;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import io.reactivex.Observable;

public class PurchaseOrderCompanyEditCommand
    implements ICommand<DObPurchaseOrderCompanyValueObject> {

  private final CommandUtils commandUtils;
  private CObCompanyGeneralModelRep cObCompanyGeneralModelRep;
  private IObPurchaseOrderBillToCompanyRep orderCompanyRep;

  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private DObPurchaseOrderRep purchaseOrderRep;

  public PurchaseOrderCompanyEditCommand() {
    commandUtils = new CommandUtils();
  }

  public void unlockSection(String purchaseOrderCode) throws Exception {
    entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);
  }

  @Override
  public Observable<String> executeCommand(DObPurchaseOrderCompanyValueObject valueObject)
      throws Exception {
    DObPurchaseOrder purchaseOrder =
        purchaseOrderRep.findOneByUserCode(valueObject.getPurchaseOrderCode());

    setCompanyData(valueObject, purchaseOrder);

    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);

    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void setCompanyData(
      DObPurchaseOrderCompanyValueObject valueObject, DObPurchaseOrder purchaseOrder) {
    IObPurchaseOrderBillToCompany billToCompany =
        orderCompanyRep.findOneByRefInstanceId(purchaseOrder.getId());
    if (!purchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name())) {
      billToCompany.setCompanyId(getCompanyId(valueObject.getCompanyCode()));
    }
    billToCompany.setBankAccountId(valueObject.getBankAccountId());

    commandUtils.setModificationInfoAndModificationDate(billToCompany);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.companyAttr, billToCompany);
  }

  private Long getCompanyId(String companyCode) {
    CObCompanyGeneralModel company = cObCompanyGeneralModelRep.findOneByUserCode(companyCode);
    if (company != null) return company.getId();
    return null;
  }

  public void setcObCompanyGeneralModelRep(CObCompanyGeneralModelRep cObCompanyGeneralModelRep) {
    this.cObCompanyGeneralModelRep = cObCompanyGeneralModelRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setOrderCompanyRep(IObPurchaseOrderBillToCompanyRep orderCompanyRep) {
    this.orderCompanyRep = orderCompanyRep;
  }

  public void setUserAccountSecurityService(IUserAccountSecurityService securityService) {
    commandUtils.setUserAccountSecurityService(securityService);
  }
}
