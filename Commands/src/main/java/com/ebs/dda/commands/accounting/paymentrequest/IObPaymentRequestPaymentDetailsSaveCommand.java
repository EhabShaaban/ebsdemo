package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestSectionNames;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetails;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import io.reactivex.Observable;

public class IObPaymentRequestPaymentDetailsSaveCommand
    implements ICommand<IObPaymentRequestPaymentDetailsValueObject> {
  private DObPaymentRequestRep paymentRequestRep;
  private IObPaymentRequestPaymentDetailsRep paymentDetailsRep;
  private EntityLockCommand<DObPaymentRequest> entityLockCommand;
  private CObCurrencyRep currencyRep;
  private IObCompanyBankDetailsRep companyBankDetailsRep;
  private CommandUtils commandUtils;
  private Long treasuryId;
  private MObItemGeneralModelRep itemGMRep;
  private CObTreasuryRep treasuryRep;

  public IObPaymentRequestPaymentDetailsSaveCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public <T> Observable<T> executeCommand(IObPaymentRequestPaymentDetailsValueObject valueObject)
      throws Exception {
    DObPaymentRequest paymentRequest =
        paymentRequestRep.findOneByUserCode(valueObject.getPaymentRequestCode());
    IObPaymentRequestPaymentDetails paymentDetails =
        paymentDetailsRep.findOneByRefInstanceId(paymentRequest.getId());
    commandUtils.setCreationAndModificationInfoAndDate(paymentDetails);
    setPaymentDetailsData(paymentDetails,paymentRequest, valueObject);
    setBankAccountData(paymentDetails, valueObject);
    setTreasuryData(paymentDetails, valueObject);
    commandUtils.setModificationInfoAndModificationDate(paymentRequest);
    paymentRequest.setHeaderDetail(IDObPaymentRequest.paymentdetailsAttr, paymentDetails);
    setCostFactorItem(paymentRequest, paymentDetails, valueObject);
    paymentRequestRep.updateAndClear(paymentRequest);
    return Observable.empty();
  }

  private void setTreasuryData(IObPaymentRequestPaymentDetails paymentDetails,
      IObPaymentRequestPaymentDetailsValueObject valueObject) {
    if (valueObject.getTreasuryCode() != null) {
      CObTreasury treasury = treasuryRep.findOneByUserCode(valueObject.getTreasuryCode());
      treasuryId = treasury.getId();
      paymentDetails.setTreasuryId(treasuryId);
    }
  }

  private void setPaymentDetailsData(IObPaymentRequestPaymentDetails paymentDetails,
                                     DObPaymentRequest paymentRequest,
      IObPaymentRequestPaymentDetailsValueObject valueObject) {
    paymentDetails.setNetAmount(valueObject.getNetAmount());
    paymentDetails.setDescription(valueObject.getDescription());
    setCurrencyData(paymentDetails, paymentRequest, valueObject.getCurrencyCode());
  }
  private void setCurrencyData(IObPaymentRequestPaymentDetails paymentDetails, DObPaymentRequest paymentRequest,
      String currencyCode) {
		if (paymentRequest.getPaymentType().equals(PaymentType.UNDER_SETTLEMENT.toString())
						|| paymentRequest.getPaymentType()
										.equals(PaymentType.OTHER_PARTY_FOR_PURCHASE.toString())) {
      if (currencyCode != null) {
        CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
        paymentDetails.setCurrencyId(currency.getId());
      } else {
        paymentDetails.setCurrencyId(null);
      }
    }
  }

  private void setBankAccountData(IObPaymentRequestPaymentDetails paymentRequestPaymentDetails,
      IObPaymentRequestPaymentDetailsValueObject valueObject) {
    if (valueObject.getCompanyBankId() != null) {
      IObCompanyBankDetails companyBankDetails =
          companyBankDetailsRep.findOneById(valueObject.getCompanyBankId());
      paymentRequestPaymentDetails.setBankAccountId(companyBankDetails.getId());
    } else {
      paymentRequestPaymentDetails.setBankAccountId(null);
    }
  }

  public void unlockSection(String paymentRequestCode) throws Exception {
    entityLockCommand.unlockSection(paymentRequestCode,
        IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION);
  }

  private void setCostFactorItem(DObPaymentRequest paymentRequest,
      IObPaymentRequestPaymentDetails paymentDetails,
      IObPaymentRequestPaymentDetailsValueObject valueObject) {

    if (valueObject.getCostFactorItemCode() == null) {
      paymentDetails.setCostFactorItemId(null);
      return;
    }
    if (paymentRequest.getPaymentType().equals(PaymentType.OTHER_PARTY_FOR_PURCHASE.toString())) {
      MObItemGeneralModel item = itemGMRep.findByUserCode(valueObject.getCostFactorItemCode());
      paymentDetails.setCostFactorItemId(item.getId());
    }
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }

  public void setPaymentDetailsRep(IObPaymentRequestPaymentDetailsRep paymentDetailsRep) {
    this.paymentDetailsRep = paymentDetailsRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObPaymentRequest> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCompanyBankDetailsRep(IObCompanyBankDetailsRep companyBankDetailsRep) {
    this.companyBankDetailsRep = companyBankDetailsRep;
  }
  public void setItemGMRep(MObItemGeneralModelRep itemGMRep) {
    this.itemGMRep = itemGMRep;
  }

  public void setTreasuryRep(CObTreasuryRep treasuryRep) {
    this.treasuryRep = treasuryRep;
  }
}
