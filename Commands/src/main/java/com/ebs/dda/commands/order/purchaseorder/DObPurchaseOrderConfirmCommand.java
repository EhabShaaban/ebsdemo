package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemSummaryGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderNewStateMachineFactory;
import org.joda.time.DateTime;

public class DObPurchaseOrderConfirmCommand {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderCycleDatesRep orderCycleDatesRep;
  private IObOrderItemSummaryGeneralModelRep orderSummaryGeneralModelRep;
  private DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory;

  public DObPurchaseOrderConfirmCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void executeCommand(
      String purchaseOrderCode, DObPurchaseOrderConfirmValueObject purchaseOrderConfirmValueObject)
      throws Exception {
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);

    setPurchaseOrderConfirmedState(purchaseOrder);
    IObOrderCycleDates orderCycleDates =
        setOrderConfirmationDate(
            purchaseOrder, purchaseOrderConfirmValueObject.getConfirmationDateTime());

    setRemainingValueIfService(purchaseOrderCode, purchaseOrder);

    commandUtils.setModificationInfoAndModificationDate(orderCycleDates);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.cycleDatesAttr, orderCycleDates);
    purchaseOrderRep.update(purchaseOrder);
  }

  private void setRemainingValueIfService(
      String purchaseOrderCode, DObPurchaseOrder purchaseOrder) {
    if (isServicePO(purchaseOrder)) {
      IObOrderItemSummaryGeneralModel summary =
          orderSummaryGeneralModelRep.findOneByOrderCode(purchaseOrderCode);
      purchaseOrder.setRemaining(summary.getTotalAmountAfterTaxes());
    }
  }

  private boolean isServicePO(DObPurchaseOrder purchaseOrder) {
    return purchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name());
  }

  private void setPurchaseOrderConfirmedState(DObPurchaseOrder purchaseOrder) throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);

    if (purchaseOrderStateMachine.fireEvent(IPurchaseOrderActionNames.MARK_AS_CONFIRMED)) {
      purchaseOrderStateMachine.save();
    }
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private IObOrderCycleDates setOrderConfirmationDate(
      DObPurchaseOrder purchaseOrder, DateTime confirmationDateTime) {

    IObOrderCycleDates orderCycleDates =
        orderCycleDatesRep.findOneByRefInstanceId(purchaseOrder.getId());

    if (orderCycleDates == null) {
      orderCycleDates = new IObOrderCycleDates();
      commandUtils.setCreationAndModificationInfoAndDate(orderCycleDates);
    }

    orderCycleDates.setActualConfirmationDate(confirmationDateTime);
    return orderCycleDates;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep orderRep) {
    this.purchaseOrderRep = orderRep;
  }

  public void setOrderCycleDatesRep(IObOrderCycleDatesRep orderCycleDatesRep) {
    this.orderCycleDatesRep = orderCycleDatesRep;
  }

  public void setOrderSummaryGeneralModelRep(
      IObOrderItemSummaryGeneralModelRep orderSummaryGeneralModelRep) {
    this.orderSummaryGeneralModelRep = orderSummaryGeneralModelRep;
  }

  public void setPurchaseOrderFactory(DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory) {
    this.purchaseOrderFactory = purchaseOrderFactory;
  }
}
