package com.ebs.dda.commands.inventory.storetransaction.services;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModelRep;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

@Service
public class GoodsReceiptPurchaseOrderCreateStoreTransactionService {

  private final CommandUtils commandUtils;
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;

  private final TObGoodsReceiptStoreTransactionDataGeneralModelRep
      goodsReceiptStoreTransactionDataGeneralModelRep;

  public GoodsReceiptPurchaseOrderCreateStoreTransactionService(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      TObGoodsReceiptStoreTransactionDataGeneralModelRep
          goodsReceiptStoreTransactionDataGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    this.goodsReceiptStoreTransactionDataGeneralModelRep =
        goodsReceiptStoreTransactionDataGeneralModelRep;
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public List<TObGoodsReceiptStoreTransaction> getGoodsReceiptPurchaseOrderStoreTransaction(
      String goodsReceiptCode) {

    ArrayList<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionsList =
        new ArrayList<>();

    List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItemsData =
        goodsReceiptStoreTransactionDataGeneralModelRep.findByUserCode(goodsReceiptCode);

    for (TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow :
        goodsReceiptItemsData) {

      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction =
          new TObGoodsReceiptStoreTransaction();

      commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptStoreTransaction);

      addCommonDataForGoodsReceiptPurchaseOrderStoreTransaction(
          goodsReceiptItemDataRow, goodsReceiptStoreTransaction);

      setStoreTransactionUserCode(goodsReceiptStoreTransaction);

      if (goodsReceiptItemDataRow.getBatchNo() != null) {
        addDataForBatchedItemGoodsReceiptStoreTransaction(
            goodsReceiptItemDataRow, goodsReceiptStoreTransaction);
      } else {
        addDataForUnBatchedItemGoodsReceiptStoreTransaction(
            goodsReceiptItemDataRow, goodsReceiptStoreTransaction);
      }

      goodsReceiptStoreTransactionsList.add(goodsReceiptStoreTransaction);
    }
    return goodsReceiptStoreTransactionsList;
  }

  private void addDataForUnBatchedItemGoodsReceiptStoreTransaction(
      TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow,
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {

    Long uoeId = goodsReceiptItemDataRow.getReceivedUoeId();
    goodsReceiptStoreTransaction.setUnitOfMeasureId(uoeId);

    BigDecimal qty =goodsReceiptItemDataRow.getReceivedQty();

    String stockType = goodsReceiptItemDataRow.getReceivedStockType();
    setStoreTransactionStockData(goodsReceiptStoreTransaction, qty, stockType);
  }

  private void setStoreTransactionStockData(
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
      BigDecimal qty,
      String stockType) {
    StockTypeEnum.StockType stockTypeEnumObject = StockTypeEnum.StockType.valueOf(stockType);
    StockTypeEnum stockTypeEnum = new StockTypeEnum();
    stockTypeEnum.setId(stockTypeEnumObject);
    goodsReceiptStoreTransaction.setStockType(stockTypeEnum);

    setTransactionOperation(goodsReceiptStoreTransaction);

    goodsReceiptStoreTransaction.setQuantity(qty);
    goodsReceiptStoreTransaction.setRemainingQuantity(qty);
  }

  private void addCommonDataForGoodsReceiptPurchaseOrderStoreTransaction(
      TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow,
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {

    goodsReceiptStoreTransaction.setDocumentReferenceId(
        goodsReceiptItemDataRow.getGoodsReceiptId());
    Long itemId = goodsReceiptItemDataRow.getItemId();
    goodsReceiptStoreTransaction.setItemId(itemId);

    Long companyId = goodsReceiptItemDataRow.getCompanyId();
    goodsReceiptStoreTransaction.setCompanyId(companyId);

    Long plantId = goodsReceiptItemDataRow.getPlantId();
    goodsReceiptStoreTransaction.setPlantId(plantId);

    Long storehouseId = goodsReceiptItemDataRow.getStorehouseId();
    goodsReceiptStoreTransaction.setStorehouseId(storehouseId);

    Long purchaseUnitId = goodsReceiptItemDataRow.getPurchaseUnitId();
    goodsReceiptStoreTransaction.setPurchaseUnitId(purchaseUnitId);

    goodsReceiptStoreTransaction.setOriginalAddingDate(new DateTime());
  }

  private void addDataForBatchedItemGoodsReceiptStoreTransaction(
      TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow,
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {

    Long uoeId = goodsReceiptItemDataRow.getBatchUoeId();
    goodsReceiptStoreTransaction.setUnitOfMeasureId(uoeId);

    String batchNo = goodsReceiptItemDataRow.getBatchNo();
    goodsReceiptStoreTransaction.setBatchNo(batchNo);

    BigDecimal qty =goodsReceiptItemDataRow.getBatchQty();

    String stockType = goodsReceiptItemDataRow.getBatchStockType();
    setStoreTransactionStockData(goodsReceiptStoreTransaction, qty, stockType);
  }

  private void setTransactionOperation(
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {
    TransactionTypeEnum.TransactionType transactionOperationType =
        TransactionTypeEnum.TransactionType.ADD;
    TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    transactionOperationEnum.setId(transactionOperationType);
    goodsReceiptStoreTransaction.setTransactionOperation(transactionOperationEnum);
  }

  private void setStoreTransactionUserCode(
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(TObStoreTransaction.class.getSimpleName());
    goodsReceiptStoreTransaction.setUserCode(userCode);
  }
}
