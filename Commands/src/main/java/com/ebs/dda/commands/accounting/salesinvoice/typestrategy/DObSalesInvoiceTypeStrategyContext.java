package com.ebs.dda.commands.accounting.salesinvoice.typestrategy;

import com.ebs.dda.jpa.accounting.salesinvoice.SalesInvoiceTypeEnum;

public class DObSalesInvoiceTypeStrategyContext {

  private static boolean isSalesInvoiceForSalesOrder(String typeCode) {
    return typeCode.equals(
        SalesInvoiceTypeEnum.SalesInvoiceType.SALES_INVOICE_FOR_SALES_ORDER.toString());
  }

  private static boolean isSalesInvoiceWithoutReference(String typeCode) {
    return typeCode.equals(
        SalesInvoiceTypeEnum.SalesInvoiceType.SALES_INVOICE_WITHOUT_REFERENCE.toString());
  }

  public DObSalesInvoiceCreationHandler getContext(String salesInvoiceTypeCode) throws Exception {

    if (isSalesInvoiceForSalesOrder(salesInvoiceTypeCode)) {
      return new DObSalesInvoiceForSalesOrderCreationHandler();
    }

    if (isSalesInvoiceWithoutReference(salesInvoiceTypeCode)) {
      return new DObSalesInvoiceWithoutReferenceCreationHandler();
    }

    throw new UnsupportedOperationException("Not supported sales invoice type");
  }
}
