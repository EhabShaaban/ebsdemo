package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoicePurchaseOrder;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsDownPaymentRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoicePurchaseOrdeRep;
import io.reactivex.Observable;
import java.util.List;
import javax.transaction.Transactional;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DObVendorInvoiceDetailsEditCommand implements ICommand<IObVendorInvoiceDetailsValueObject> {

  private IUserAccountSecurityService userAccountSecurityService;
  private DObVendorInvoiceRep invoiceRep;
  private IObVendorInvoiceDetailsRep invoiceDetailsRep;
  private IObVendorInvoicePurchaseOrdeRep invoicePurchaseOrderRep;

  private CObCurrencyRep currencyRep;
  private CObPaymentTermsRep paymentTermsRep;

  @Transactional
  @Override
  public Observable<String> executeCommand(IObVendorInvoiceDetailsValueObject valueObject)
      throws Exception {
    DObVendorInvoice invoice = invoiceRep.findOneByUserCode(valueObject.getInvoiceCode());

    IObVendorInvoiceDetails invoiceDetails = invoiceDetailsRep.findOneByRefInstanceId(invoice.getId());

    IObVendorInvoicePurchaseOrder invoicePurchaseOrder =
        invoicePurchaseOrderRep.findOneByRefInstanceId(invoice.getId());

    setInvoiceDetailsData(valueObject, invoiceDetails);

    setInvoicePurchaseOrderData(valueObject, invoicePurchaseOrder);

    setModificationInfo(invoiceDetails);
    setModificationInfo(invoice);

    invoice.setHeaderDetail(IDObVendorInvoice.purchaseOrderAttr, invoicePurchaseOrder);

    invoice.setHeaderDetail(IDObVendorInvoice.invoiceDetailsAttr, invoiceDetails);
    invoiceRep.updateAndClear(invoice);
    return Observable.just("SUCCESS");
  }

  private void setInvoicePurchaseOrderData(
          IObVendorInvoiceDetailsValueObject valueObject, IObVendorInvoicePurchaseOrder invoicePurchaseOrder) {

    setCurrency(valueObject, invoicePurchaseOrder);

    setPaymentTerms(valueObject, invoicePurchaseOrder);
  }

  private void setInvoiceDetailsData(
          IObVendorInvoiceDetailsValueObject valueObject, IObVendorInvoiceDetails invoiceDetails) {
    invoiceDetails.setInvoiceNumber(valueObject.getInvoiceNumber());
    if (valueObject.getInvoiceDate() != null) {
      invoiceDetails.setInvoiceDate(getDateTime(valueObject.getInvoiceDate()));
    } else {
      invoiceDetails.setInvoiceDate(null);
    }
  }

  private void setPaymentTerms(
          IObVendorInvoiceDetailsValueObject valueObject, IObVendorInvoicePurchaseOrder invoicePurchaseOrder) {
    if (null != valueObject.getPaymentTermCode()) {
      CObPaymentTerms paymentTerms =
          paymentTermsRep.findOneByUserCode(valueObject.getPaymentTermCode());
      invoicePurchaseOrder.setPaymentTermId(paymentTerms.getId());
    } else {
      invoicePurchaseOrder.setPaymentTermId(null);
    }
  }

  private void setCurrency(
          IObVendorInvoiceDetailsValueObject valueObject, IObVendorInvoicePurchaseOrder invoicePurchaseOrder) {
    if (null != valueObject.getCurrencyCode()) {
      CObCurrency currency = currencyRep.findOneByUserCode(valueObject.getCurrencyCode());
      invoicePurchaseOrder.setCurrencyId(currency.getId());
    } else {
      invoicePurchaseOrder.setCurrencyId(null);
    }
  }



  private void setModificationInfo(BusinessObject businessObject) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    businessObject.setModificationInfo(loggedinUserInfo);
    DateTime currentDate = new DateTime();
    businessObject.setModifiedDate(currentDate);
  }


  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setInvoiceRep(DObVendorInvoiceRep invoiceRep) {
    this.invoiceRep = invoiceRep;
  }

  public void setInvoiceDetailsRep(IObVendorInvoiceDetailsRep invoiceDetailsRep) {
    this.invoiceDetailsRep = invoiceDetailsRep;
  }

  public void setInvoicePurchaseOrderRep(IObVendorInvoicePurchaseOrdeRep invoicePurchaseOrderRep) {
    this.invoicePurchaseOrderRep = invoicePurchaseOrderRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setPaymentTermsRep(CObPaymentTermsRep paymenttermsRep) {
    this.paymentTermsRep = paymenttermsRep;
  }


  public DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }
}
