package com.ebs.dda.commands.inventory.stockavailability.strategies;

import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModelRep;

import java.util.List;

public class UpdateStockAvailabilityByGoodsIssueStrategy
    implements IUpdateStockAvailabilityStrategy {
  private IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep;
  private UpdateStockAvailabilityService stockAvailabilityUpdateService;

  public void updateStockAvailability(String userCode) {
    List<? extends IObItemQuantityGeneralModel> items = getItemQuantityList(userCode);
    items.forEach(
        item ->
            stockAvailabilityUpdateService.updateOrCreateStockAvailabilityForItem(
                item, item.getQuantity().negate()));
  }

  private List<? extends IObItemQuantityGeneralModel> getItemQuantityList(String userCode) {
    return goodsIssueItemQuantityGeneralModelRep.findAllByUserCode(userCode);
  }

  public void setGoodsIssueItemQuantityGeneralModelRep(
      IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep) {
    this.goodsIssueItemQuantityGeneralModelRep = goodsIssueItemQuantityGeneralModelRep;
  }

  public void setStockAvailabilityUpdateService(
      UpdateStockAvailabilityService stockAvailabilityUpdateService) {
    this.stockAvailabilityUpdateService = stockAvailabilityUpdateService;
  }
}
