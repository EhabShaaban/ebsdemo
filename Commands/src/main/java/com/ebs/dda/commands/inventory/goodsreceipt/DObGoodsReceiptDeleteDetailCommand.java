package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;

public class DObGoodsReceiptDeleteDetailCommand {

  private final String GR_PO = "PURCHASE_ORDER";
  private IObGoodsReceiptItemBatchesRep batchesRep;
  private IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderQuantitiesRep;
  private IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep;
  private DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep;
  private DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep;
  private IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptPurchaseOrderReceivedItemsRep;
  private IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
      goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep;
  private IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep;

  private PlatformTransactionManager transactionManager;

  private IUserAccountSecurityService userAccountSecurityService;
  private CommandUtils commandUtils;

  public DObGoodsReceiptDeleteDetailCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void executeCommand(String goodsReceiptCode, String grType, String itemCode, Long detailId)
          throws Exception {
    IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel =
            receivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(goodsReceiptCode, itemCode);
    Boolean isBatched = receivedItemGeneralModel.getIsBatchManaged();
    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);

    if (grType.equals(GR_PO)) {
      handleDeleteItemDetailInGoodsReceiptPurchaseOrder(
              goodsReceiptCode, detailId, receivedItemGeneralModel, isBatched, transactionTemplate);
    } else {
      handleDeleteItemDetailInGoodsReceiptSalesReturn(
              goodsReceiptCode, detailId, receivedItemGeneralModel, transactionTemplate);
    }
  }

  private void handleDeleteItemDetailInGoodsReceiptSalesReturn(
          String goodsReceiptCode,
          Long detailId,
          IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel,
          TransactionTemplate transactionTemplate)
          throws Exception {
    IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData
            goodsReceiptSalesReturnOrdinaryReceivedItemsData =
            goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep.findOneById(
                    receivedItemGeneralModel.getGRItemId());

    checkIfGoodsReceiptSalesReturnItemDetailExists(detailId);
    transactionTemplate.execute(
            new TransactionCallbackWithoutResult() {
              @Override
              protected void doInTransactionWithoutResult(TransactionStatus status) {
                deleteGoodsReceiptSalesReturnQuantity(detailId);
                commandUtils.setModificationInfoAndModificationDate(
                        goodsReceiptSalesReturnOrdinaryReceivedItemsData);
                try {
                  updateGoodsReceiptSalesReturn(
                          goodsReceiptCode, goodsReceiptSalesReturnOrdinaryReceivedItemsData);
                } catch (Exception e) {
                  status.setRollbackOnly();
                  throw new RuntimeException("Delete Detail failed !");
                }
              }
            });
  }

  private void handleDeleteItemDetailInGoodsReceiptPurchaseOrder(
          String goodsReceiptCode,
          Long detailId,
          IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel,
          Boolean isBatched,
          TransactionTemplate transactionTemplate)
          throws Exception {
    IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptPurchaseOrderReceivedItem =
            goodsReceiptPurchaseOrderReceivedItemsRep.findOneById(receivedItemGeneralModel.getGRItemId());
    checkIfGoodsReceiptPurchaseOrderItemDetailExists(detailId, isBatched);
    transactionTemplate.execute(
            new TransactionCallbackWithoutResult() {
              @Override
              protected void doInTransactionWithoutResult(TransactionStatus status) {
                if (isBatched) {
                  deleteBatch(detailId);
                } else {
                  deleteGoodsReceiptPurchaseOrderQuantity(detailId);
                }
                commandUtils.setModificationInfoAndModificationDate(
                        goodsReceiptPurchaseOrderReceivedItem);
                try {
                  updateGoodsReceiptPurchaseOrder(
                          goodsReceiptCode, goodsReceiptPurchaseOrderReceivedItem);
                } catch (Exception e) {
                  status.setRollbackOnly();
                  throw new RuntimeException("Delete Detail failed !");
                }
              }
            });
  }

  private void checkIfGoodsReceiptPurchaseOrderItemDetailExists(Long detailId, Boolean isBatched)
          throws Exception {
    if (isBatched) {
      checkIfBatchExists(detailId);

    } else {
      checkIfGoodsReceiptPurchaseOrderQuantityExists(detailId);
    }
  }

  private void checkIfGoodsReceiptSalesReturnItemDetailExists(Long detailId) throws Exception {
    checkIfGoodsReceiptSalesReturnQuantityExists(detailId);
  }

  private void checkIfGoodsReceiptPurchaseOrderQuantityExists(Long detailId) throws Exception {
    Optional<IObGoodsReceiptPurchaseOrderItemQuantities> quantity =
        goodsReceiptPurchaseOrderQuantitiesRep.findById(detailId);

    if (quantity.equals(Optional.empty())) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptPurchaseOrderItemQuantities.class.getName());
    }
  }

  private void checkIfGoodsReceiptSalesReturnQuantityExists(Long detailId) throws Exception {
    Optional<IObGoodsReceiptSalesReturnItemQuantities> quantity =
        goodsReceiptSalesReturnItemQuantitiesRep.findById(detailId);

    if (quantity.equals(Optional.empty())) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptSalesReturnItemQuantities.class.getName());
    }
  }

  private void checkIfBatchExists(Long detailId) throws Exception {
    Optional<IObGoodsReceiptItemBatches> batch = batchesRep.findById(detailId);

    if (batch.equals(Optional.empty())) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptReceivedItemsGeneralModel.class.getName());
    }
  }

  private void updateGoodsReceiptPurchaseOrder(
          String goodsReceiptCode,
          IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptPurchaseOrderReceivedItemsData)
          throws Exception {
    DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
            goodsReceiptPurchaseOrderRep.findOneByUserCode(goodsReceiptCode);
    dObGoodsReceiptPurchaseOrder.addLineDetail(
            IDObGoodsReceiptPurchaseOrder.iObGoodsReceiptPurchaseOrderItems,
            goodsReceiptPurchaseOrderReceivedItemsData);
    commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptPurchaseOrder);
    goodsReceiptPurchaseOrderRep.update(dObGoodsReceiptPurchaseOrder);
  }

  private void updateGoodsReceiptSalesReturn(
          String goodsReceiptCode,
          IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData
                  goodsReceiptSalesReturnOrdinaryReceivedItemsData)
          throws Exception {
    DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn =
            goodsReceiptSalesReturnRep.findOneByUserCode(goodsReceiptCode);
    dObGoodsReceiptSalesReturn.addLineDetail(
            IDObGoodsReceiptSalesReturn.salesReturnOrdinaryItems,
            goodsReceiptSalesReturnOrdinaryReceivedItemsData);
    commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptSalesReturn);
    goodsReceiptSalesReturnRep.update(dObGoodsReceiptSalesReturn);
  }

  private void deleteBatch(Long detailId) {
    batchesRep.deleteById(detailId);
  }

  private void deleteGoodsReceiptPurchaseOrderQuantity(Long detailId) {
    goodsReceiptPurchaseOrderQuantitiesRep.deleteById(detailId);
  }

  private void deleteGoodsReceiptSalesReturnQuantity(Long detailId) {
    IObGoodsReceiptSalesReturnItemQuantities goodsReceiptSalesReturnItemQuantity =
            goodsReceiptSalesReturnItemQuantitiesRep.findOneById(detailId);
    goodsReceiptSalesReturnItemQuantitiesRep.delete(goodsReceiptSalesReturnItemQuantity);
  }

  public void setBatchesRep(IObGoodsReceiptItemBatchesRep batchesRep) {
    this.batchesRep = batchesRep;
  }

  public void setGoodsReceiptPurchaseOrderQuantitiesRep(
      IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderQuantitiesRep) {
    this.goodsReceiptPurchaseOrderQuantitiesRep = goodsReceiptPurchaseOrderQuantitiesRep;
  }

  public void setGoodsReceiptPurchaseOrderReceivedItemsRep(
      IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptPurchaseOrderReceivedItemsRep) {
    this.goodsReceiptPurchaseOrderReceivedItemsRep = goodsReceiptPurchaseOrderReceivedItemsRep;
  }

  public void setGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep(
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep) {
    this.goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep =
        goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setReceivedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep) {
    this.receivedItemsGeneralModelRep = receivedItemsGeneralModelRep;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setGoodsReceiptSalesReturnItemQuantitiesRep(
      IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep) {
    this.goodsReceiptSalesReturnItemQuantitiesRep = goodsReceiptSalesReturnItemQuantitiesRep;
  }

  public void setGoodsReceiptPurchaseOrderRep(
      DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep) {
    this.goodsReceiptPurchaseOrderRep = goodsReceiptPurchaseOrderRep;
  }

  public void setGoodsReceiptSalesReturnRep(
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep) {
    this.goodsReceiptSalesReturnRep = goodsReceiptSalesReturnRep;
  }
}
