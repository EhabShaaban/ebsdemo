package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsClearedValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsClearedCommand {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep orderRep;
  private IObOrderCycleDatesRep orderCycleDatesRep;

  public DObPurchaseOrderMarkAsClearedCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }

  public void setOrderCycleDatesRep(IObOrderCycleDatesRep orderCycleDatesRep) {
    this.orderCycleDatesRep = orderCycleDatesRep;
  }

  public void executeCommand(
      String purchaseOrderCode,
      DobPurchaseOrderMarkAsClearedValueObject purchaseOrderClearValueObject)
      throws Exception {

    DObPurchaseOrder purchaseOrder = orderRep.findOneByUserCode(purchaseOrderCode);

    setPurchaseOrderClearedState(purchaseOrder);
    IObOrderCycleDates orderCycleDates =
        setOrderClearanceDate(purchaseOrder, purchaseOrderClearValueObject.getClearanceDateTime());

    commandUtils.setModificationInfoAndModificationDate(orderCycleDates);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.cycleDatesAttr, orderCycleDates);
    orderRep.update(purchaseOrder);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void setPurchaseOrderClearedState(DObPurchaseOrder purchaseOrder) throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);

    if (purchaseOrderStateMachine.fireEvent(IPurchaseOrderActionNames.MARK_AS_CLEARED)) {
      purchaseOrderStateMachine.save();
    }
  }

  private IObOrderCycleDates setOrderClearanceDate(
      DObPurchaseOrder purchaseOrder, DateTime clearanceDateTime) {
    IObOrderCycleDates orderCycleDates =
        orderCycleDatesRep.findOneByRefInstanceId(purchaseOrder.getId());
    orderCycleDates.setActualClearanceDate(clearanceDateTime);
    return orderCycleDates;
  }
}
