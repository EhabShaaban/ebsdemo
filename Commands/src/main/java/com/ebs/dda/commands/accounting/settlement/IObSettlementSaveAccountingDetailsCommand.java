package com.ebs.dda.commands.accounting.settlement;

import java.math.BigDecimal;
import javax.transaction.Transactional;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.factories.IObAccountingDocumentAccountingDetailsFactory;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.SubAccountGeneralModel;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import io.reactivex.Observable;

/**
 * @auther Ahmed Ali Elfeky
 * @since Mar 2, 2021
 */
public class IObSettlementSaveAccountingDetailsCommand
    implements ICommand<IObSettlementAddAccountingDetailsValueObject> {

  private CommandUtils commandUtils;
  private DObSettlementRep settlementRep;
  private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
  private SubAccountGeneralModelRep subAccountGeneralModelRep;

  public IObSettlementSaveAccountingDetailsCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(
      IObSettlementAddAccountingDetailsValueObject accDetailsValueObj) throws Exception {

    DObSettlement settlement =
        settlementRep.findOneByUserCode(accDetailsValueObj.getSettlementCode());

    IObAccountingDocumentAccountingDetails accountingDetails =
        constructAccountingDetailsInstance(accDetailsValueObj);

    attachAccountingDetailsToSettlement(settlement, accountingDetails);

    commandUtils.setModificationInfoAndModificationDate(settlement);
    settlementRep.update(settlement);

    return Observable.just(settlement.getUserCode());
  }

  private IObAccountingDocumentAccountingDetails constructAccountingDetailsInstance(
      IObSettlementAddAccountingDetailsValueObject valueObj) {

    HObChartOfAccountsGeneralModel glAccount =
        chartOfAccountsGeneralModelRep.findOneByUserCode(valueObj.getGlAccountCode());

    IObAccountingDocumentAccountingDetails accountingDetails =
        initAccDetailsByLedger(glAccount.getLeadger());

    accountingDetails.setAccountingEntry(valueObj.getAccountType());
    accountingDetails.setAmount(valueObj.getAmount());

    accountingDetails.setGlAccountId(glAccount.getId());
    accountingDetails.setObjectTypeCode(IDObSettlement.SYS_NAME);

    if (glAccount.getLeadger() != null) {
      SubAccountGeneralModel glSubAccount = subAccountGeneralModelRep
          .findOneByCodeAndLedger(valueObj.getGlSubAccountCode(), glAccount.getLeadger());
      accountingDetails.setGlSubAccountId(glSubAccount.getSubaccountid());
    }

    commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);
    return accountingDetails;
  }

  private IObAccountingDocumentAccountingDetails initAccDetailsByLedger(String ledger) {
    return IObAccountingDocumentAccountingDetailsFactory.initAccountingDetails(ledger);
  }

  private void attachAccountingDetailsToSettlement(DObSettlement settlement,
      IObAccountingDocumentAccountingDetails accDetails) {
    accDetails.setRefInstanceId(settlement.getId());
    settlement.addLineDetail(IDObSettlement.accountingDetailsAttr, accDetails);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setSettlementRep(DObSettlementRep settlementRep) {
    this.settlementRep = settlementRep;
  }

  public void setChartOfAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
    this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
  }

  public void setSubAccountGeneralModelRep(SubAccountGeneralModelRep subAccountGeneralModelRep) {
    this.subAccountGeneralModelRep = subAccountGeneralModelRep;
  }

}
