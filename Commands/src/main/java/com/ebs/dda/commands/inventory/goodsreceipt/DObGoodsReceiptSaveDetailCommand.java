package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class DObGoodsReceiptSaveDetailCommand
    implements ICommand<IObGoodsReceiptItemDetailValueObject> {
  private final String GR_PO = "PURCHASE_ORDER";
  private IObGoodsReceiptItemBatchesRep batchesRep;
  private IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderQuantitiesRep;
  private IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep;
  private CObMeasureRep measureRep;
  private DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep;
  private DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep;
  private IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptPurchaseOrderReceivedItemsRep;
  private IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
      goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep;
  private IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      iObGoodsReceiptPurchaseOrderDataGeneralModelRep;
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private IUserAccountSecurityService userAccountSecurityService;
  private CommandUtils commandUtils;

  public DObGoodsReceiptSaveDetailCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObGoodsReceiptItemDetailValueObject valueObject)
      throws Exception {

    IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel =
        receivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
            valueObject.getGoodsReceiptCode(), valueObject.getItemCode());
    if (valueObject.getGoodsReceiptType().equals(GR_PO)) {
      DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
          goodsReceiptPurchaseOrderRep.findOneByUserCode(valueObject.getGoodsReceiptCode());

      List<IObGoodsReceiptPurchaseOrderReceivedItemsData>
          goodsReceiptPurchaseOrderReceivedItemsList =
              goodsReceiptPurchaseOrderReceivedItemsRep.findAllByRefInstanceIdAndIdNot(
                  dObGoodsReceiptPurchaseOrder.getId(), receivedItemGeneralModel.getGRItemId());

      IObGoodsReceiptPurchaseOrderReceivedItemsData receivedItemData =
          goodsReceiptPurchaseOrderReceivedItemsRep.findOneById(
              receivedItemGeneralModel.getGRItemId());

      String detailType = valueObject.getDetailType();

      if (detailType.equals("Batch")) {
        IObGoodsReceiptItemBatches goodsReceiptItemBatches = new IObGoodsReceiptItemBatches();
        if (valueObject.getId() != null) {
          goodsReceiptItemBatches = saveItemBatch(valueObject);
          receivedItemData.addLineDetail(
              IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptItemBatches,
              goodsReceiptItemBatches);
        } else {
          goodsReceiptItemBatches = addItemBatch(receivedItemData, valueObject);
          receivedItemData.addLineDetail(
              IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptItemBatches,
              goodsReceiptItemBatches);
        }
      } else {
        IObGoodsReceiptPurchaseOrderItemQuantities goodsReceiptPurchaseOrderItemQuantities =
            new IObGoodsReceiptPurchaseOrderItemQuantities();

        if (valueObject.getId() != null) {
          goodsReceiptPurchaseOrderItemQuantities = saveItemQuantity(valueObject);
          receivedItemData.addLineDetail(
              IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptPurchaseOrderItemQuantities,
              goodsReceiptPurchaseOrderItemQuantities);
        } else {
          goodsReceiptPurchaseOrderItemQuantities =
              addGoodsReceiptPurchaseOrderItemQuantity(receivedItemData, valueObject);
          receivedItemData.addLineDetail(
              IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptPurchaseOrderItemQuantities,
              goodsReceiptPurchaseOrderItemQuantities);
        }
      }
      updateItem(receivedItemData);
      goodsReceiptPurchaseOrderReceivedItemsList.add(receivedItemData);
      updateGoodsReceipt(dObGoodsReceiptPurchaseOrder);
      dObGoodsReceiptPurchaseOrder.setLinesDetails(
          IDObGoodsReceiptPurchaseOrder.iObGoodsReceiptPurchaseOrderItems,
          goodsReceiptPurchaseOrderReceivedItemsList);
      goodsReceiptPurchaseOrderRep.updateAndClear(dObGoodsReceiptPurchaseOrder);

    } else {
      DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn =
          goodsReceiptSalesReturnRep.findOneByUserCode(valueObject.getGoodsReceiptCode());

      List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData>
          goodsReceiptSalesReturnOrdinaryReceivedItemsList =
              goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep.findAllByRefInstanceIdAndIdNot(
                  dObGoodsReceiptSalesReturn.getId(), receivedItemGeneralModel.getGRItemId());

      IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData receivedItemData =
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep.findOneById(
              receivedItemGeneralModel.getGRItemId());

      if (valueObject.getId() == null) {

        IObGoodsReceiptSalesReturnItemQuantities goodsReceiptSalesReturnItemQuantities =
            new IObGoodsReceiptSalesReturnItemQuantities();

        setGoodsReceiptSalesReturnItemQuantity(valueObject, goodsReceiptSalesReturnItemQuantities);
        commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptSalesReturnItemQuantities);

        receivedItemData.addLineDetail(
            IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptSalesReturnItemQuantities,
            goodsReceiptSalesReturnItemQuantities);
      } else {

        IObGoodsReceiptSalesReturnItemQuantities goodsReceiptSalesReturnItemQuantities =
            goodsReceiptSalesReturnItemQuantitiesRep.findOneById(valueObject.getId());

        setGoodsReceiptSalesReturnItemQuantity(valueObject, goodsReceiptSalesReturnItemQuantities);
        commandUtils.setModificationInfoAndModificationDate(goodsReceiptSalesReturnItemQuantities);

        receivedItemData.addLineDetail(
            IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptSalesReturnItemQuantities,
            goodsReceiptSalesReturnItemQuantities);
      }
      goodsReceiptSalesReturnOrdinaryReceivedItemsList.add(receivedItemData);

      commandUtils.setModificationInfoAndModificationDate(receivedItemData);
      commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptSalesReturn);

      dObGoodsReceiptSalesReturn.setLinesDetails(
          IDObGoodsReceiptSalesReturn.salesReturnOrdinaryItems,
          goodsReceiptSalesReturnOrdinaryReceivedItemsList);
      goodsReceiptSalesReturnRep.updateAndClear(dObGoodsReceiptSalesReturn);
    }
    return Observable.just("SUCCESS");
  }

  private void updateItem(IObGoodsReceiptPurchaseOrderReceivedItemsData receivedItemData) {
    commandUtils.setModificationInfoAndModificationDate(receivedItemData);
  }

  private void updateGoodsReceipt(DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder) {
    commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptPurchaseOrder);
  }

  IObGoodsReceiptPurchaseOrderItemQuantities saveItemQuantity(
      IObGoodsReceiptItemDetailValueObject valueObject) {
    IObGoodsReceiptItemQuantityValueObject quantityValueObject =
        (IObGoodsReceiptItemQuantityValueObject) valueObject;
    IObGoodsReceiptPurchaseOrderItemQuantities quantity =
        goodsReceiptPurchaseOrderQuantitiesRep.findOneById(valueObject.getId());

    setItemDetailCommonProperties(quantityValueObject, quantity);
    return quantity;
  }

  IObGoodsReceiptPurchaseOrderItemQuantities addGoodsReceiptPurchaseOrderItemQuantity(
      IObGoodsReceiptPurchaseOrderReceivedItemsData recievedItemData,
      IObGoodsReceiptItemDetailValueObject valueObject)
      throws Exception {
    IObGoodsReceiptItemQuantityValueObject quantityValueObject =
        (IObGoodsReceiptItemQuantityValueObject) valueObject;
    IObGoodsReceiptPurchaseOrderItemQuantities quantity =
        new IObGoodsReceiptPurchaseOrderItemQuantities();

    setItemDetailCommonProperties(quantityValueObject, quantity);
    setQuantitySystemProperties(recievedItemData, quantity);

    return quantity;
  }

  private void setQuantitySystemProperties(
      IObGoodsReceiptPurchaseOrderReceivedItemsData recievedItemData,
      IObGoodsReceiptPurchaseOrderItemQuantities quantity)
      throws Exception {

    quantity.setRefInstanceId(recievedItemData.getId());
    commandUtils.setCreationAndModificationInfoAndDate(quantity);
  }

  IObGoodsReceiptItemBatches saveItemBatch(IObGoodsReceiptItemDetailValueObject valueObject) {
    IObGoodsReceiptItemBatchValueObject batchValueObject =
        (IObGoodsReceiptItemBatchValueObject) valueObject;
    IObGoodsReceiptItemBatches batch = batchesRep.findOneById(valueObject.getId());

    batch.setBatchCode(batchValueObject.getBatchCode());

    batch.setExpirationDate(getDateTime(batchValueObject.getExpirationDate()));
    batch.setProductionDate(getDateTime(batchValueObject.getProductionDate()));

    setItemDetailCommonProperties(batchValueObject, batch);
    return batch;
  }

  IObGoodsReceiptItemBatches addItemBatch(
      IObGoodsReceiptPurchaseOrderReceivedItemsData recievedItemData,
      IObGoodsReceiptItemDetailValueObject valueObject)
      throws Exception {
    IObGoodsReceiptItemBatchValueObject batchValueObject =
        (IObGoodsReceiptItemBatchValueObject) valueObject;
    IObGoodsReceiptItemBatches batch = new IObGoodsReceiptItemBatches();

    batch.setBatchCode(batchValueObject.getBatchCode());
    batch.setExpirationDate(getDateTime(batchValueObject.getExpirationDate()));
    batch.setProductionDate(getDateTime(batchValueObject.getProductionDate()));

    setItemDetailCommonProperties(batchValueObject, batch);
    setBatchSystemProperties(recievedItemData, batch);
    return batch;
  }

  private void setBatchSystemProperties(
      IObGoodsReceiptPurchaseOrderReceivedItemsData recievedItemData,
      IObGoodsReceiptItemBatches batch)
      throws Exception {
    batch.setRefInstanceId(recievedItemData.getId());
    commandUtils.setCreationAndModificationInfoAndDate(batch);
  }

  void setItemDetailCommonProperties(
      IObGoodsReceiptItemDetailValueObject valueObject, IObGoodsReceiptItemDetail itemDetail) {
    itemDetail.setReceivedQtyUoE(valueObject.getReceivedQtyUoE());
    itemDetail.setUnitOfEntryId(getUnitOfEntryIdFromCode(valueObject));

    StockTypeEnum stockTypeEnum = new StockTypeEnum();
    stockTypeEnum.setId(valueObject.getStockType());

    itemDetail.setStockType(stockTypeEnum);
    itemDetail.setNotes(valueObject.getNotes());
    itemDetail.setItemVendorRecordId(getItemVendorRecordId(valueObject));
    commandUtils.setModificationInfoAndModificationDate(itemDetail);
  }

  void setGoodsReceiptSalesReturnItemQuantity(
      IObGoodsReceiptItemDetailValueObject valueObject,
      IObGoodsReceiptSalesReturnItemQuantities itemDetail) {

    itemDetail.setReceivedQtyUoE(valueObject.getReceivedQtyUoE());
    itemDetail.setUnitOfEntryId(getUnitOfEntryIdFromCode(valueObject));

    StockTypeEnum stockTypeEnum = new StockTypeEnum();
    stockTypeEnum.setId(valueObject.getStockType());
    itemDetail.setStockType(stockTypeEnum);
    itemDetail.setNotes(valueObject.getNotes());
  }

  private Long getItemVendorRecordId(IObGoodsReceiptItemDetailValueObject valueObject) {
    IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(
            valueObject.getGoodsReceiptCode());

    ItemVendorRecordUnitOfMeasuresGeneralModel itemVendorRecordUoM =
        itemVendorRecordUnitOfMeasuresGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndPurchasingUnitCodeAndAlternativeUnitOfMeasureCode(
                valueObject.getItemCode(),
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getVendorCode(),
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getPurchaseUnitCode(),
                valueObject.getUnitOfEntryCode());

    if (itemVendorRecordUoM == null) {
      ItemVendorRecordGeneralModel itemVendorRecord =
          itemVendorRecordGeneralModelRep
              .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                  valueObject.getItemCode(),
                  iObGoodsReceiptPurchaseOrderDataGeneralModel.getVendorCode(),
                  valueObject.getUnitOfEntryCode(),
                  iObGoodsReceiptPurchaseOrderDataGeneralModel.getPurchaseUnitCode());
      return itemVendorRecord.getId();
    }
    return itemVendorRecordUoM.getItemvendorRecordId();
  }

  private DateTime getDateTime(String currentDate) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
    return formatter.parseDateTime(currentDate);
  }

  private Long getUnitOfEntryIdFromCode(IObGoodsReceiptItemDetailValueObject valueObject) {
    CObMeasure cObMeasure = measureRep.findOneByUserCode(valueObject.getUnitOfEntryCode());
    return cObMeasure.getId();
  }

  public void setBatchesRep(IObGoodsReceiptItemBatchesRep batchesRep) {
    this.batchesRep = batchesRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setGoodsReceiptPurchaseOrderRep(
      DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep) {
    this.goodsReceiptPurchaseOrderRep = goodsReceiptPurchaseOrderRep;
  }

  public void setGoodsReceiptPurchaseOrderReceivedItemsRep(
      IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptPurchaseOrderReceivedItemsRep) {
    this.goodsReceiptPurchaseOrderReceivedItemsRep = goodsReceiptPurchaseOrderReceivedItemsRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setReceivedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep) {
    this.receivedItemsGeneralModelRep = receivedItemsGeneralModelRep;
  }

  public void setIObGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.iObGoodsReceiptPurchaseOrderDataGeneralModelRep =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setItemVendorRecordUnitOfMeasuresGeneralModelRep(
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep) {
    this.itemVendorRecordUnitOfMeasuresGeneralModelRep =
        itemVendorRecordUnitOfMeasuresGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep(
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep) {
    this.goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep =
        goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep;
  }

  public void setGoodsReceiptSalesReturnRep(
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep) {
    this.goodsReceiptSalesReturnRep = goodsReceiptSalesReturnRep;
  }

  public void setGoodsReceiptPurchaseOrderQuantitiesRep(
      IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderQuantitiesRep) {
    this.goodsReceiptPurchaseOrderQuantitiesRep = goodsReceiptPurchaseOrderQuantitiesRep;
  }

  public void setGoodsReceiptSalesReturnItemQuantitiesRep(
      IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep) {
    this.goodsReceiptSalesReturnItemQuantitiesRep = goodsReceiptSalesReturnItemQuantitiesRep;
  }
}
