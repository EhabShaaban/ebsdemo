package com.ebs.dda.commands.masterdata.item;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserInSameSessionException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import io.reactivex.Observable;
import org.springframework.transaction.TransactionSystemException;

public class MObItemDeleteCommand implements ICommand {

  EntityLockCommand<MObItem> entityLockCommand;
  private MObItemRep mObItemRep;

  public void setmObItemRep(MObItemRep mObItemRep) {
    this.mObItemRep = mObItemRep;
  }

  public void setEntityLockCommand(EntityLockCommand<MObItem> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  @Override
  public Observable<String> executeCommand(Object object) throws Exception {
    String userCode = (String) object;
    LockDetails lockDetails = null;
    try {
      lockDetails = entityLockCommand.lockAllSections(userCode);
      MObItem item = readEntityByCode(userCode);
      mObItemRep.delete(item);
      entityLockCommand.unlockAllSections(userCode);

    } catch (TransactionSystemException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw new DependentInstanceException();

    } catch (ResourceAlreadyLockedBySameUserException ex) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw new ResourceAlreadyLockedBySameUserInSameSessionException();
    } catch (DeleteNotAllowedPerStateException | InstanceNotExistException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw e;
    }
    return Observable.just("SUCCESS");
  }

  public void checkIfInstanceExists(String vendorCode) throws Exception {
    MObItem item = readEntityByCode(vendorCode);
    if (item == null) {
      throw new InstanceNotExistException();
    }
  }

  private MObItem readEntityByCode(String userCode) throws InstanceNotExistException {
    MObItem item = mObItemRep.findOneByUserCode(userCode);
    return item;
  }
}
