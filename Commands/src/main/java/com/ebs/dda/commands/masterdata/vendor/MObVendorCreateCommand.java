package com.ebs.dda.commands.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.infrastructure.tm.TransactionCallback;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.*;
import com.ebs.dda.masterdata.dbo.jpa.entities.events.CreatedMObVendorEvent;
import com.ebs.dda.masterdata.dbo.jpa.repositories.events.MObVendorEventRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.CObVendorRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

// Created By Niveen Magdy @ 21/10/18
public class MObVendorCreateCommand implements ICommand<MObVendorCreationValueObject> {

  private String COMMERCIAL_VENDOR_TYPE = "COMMERCIAL";
  private MObVendorRep vendorRep;
  private CObVendorRep cObVendorRep;
  private MObVendorEventRep vendorEventRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;
  private MasterDataCodeGenrator masterDataCodeGenrator;
  private TransactionManagerDelegate transactionManagerDelegate;
  private CommandUtils commandUtils;

  public MObVendorCreateCommand(MasterDataCodeGenrator masterDataCodeGenrator) {
    this.masterDataCodeGenrator = masterDataCodeGenrator;
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(MObVendorCreationValueObject vendorValueObject)
          throws Exception {
    String userCode = masterDataCodeGenrator.generateUserCode(MObVendor.class);
    MObVendor vendor = new MObVendor();
    setVendorData(vendor, vendorValueObject, userCode);
    setVendorPurchaseUnitsData(vendor, vendorValueObject.getPurchaseUnits());
    vendorRep.create(vendor);
    return Observable.just(userCode);
  }

  private void executeTransaction(
          MObVendor vendor, MObVendorCreationValueObject vendorValueObject) {
    transactionManagerDelegate.executeTransaction(
            new TransactionCallback() {
              protected void doTransaction() throws Exception {
                vendorRep.create(vendor);
                CreatedMObVendorEvent event = new CreatedMObVendorEvent();
                event.setCommandName("MObVendorCreateCommand");
                event.setVendorCode(vendor.getUserCode());
                event.setVendorCreationValueObject(vendorValueObject);
                vendorEventRep.save(event);
              }
            });
  }

  private void setVendorData(
          MObVendor vendor, MObVendorCreationValueObject vendorValueObject, String userCode) {
    vendor.setUserCode(userCode);
    setVendorType(vendor, vendorValueObject);
    Set<String> currentStates = new HashSet<>();
    currentStates.add("Active");
    vendor.setCurrentStates(currentStates);
    setVendorName(vendor, vendorValueObject);
    setPurchaseResponsible(vendor, vendorValueObject);
    vendor.setAccountWithVendor(vendorValueObject.getAccountWithVendor());
    vendor.setOldVendorNumber(vendorValueObject.getOldVendorReference());
    this.commandUtils.setCreationAndModificationInfoAndDate(vendor);
  }

  private void setPurchaseResponsible(MObVendor vendor, MObVendorCreationValueObject vendorValueObject) {
    if (vendorValueObject.getVendorType().equals(COMMERCIAL_VENDOR_TYPE)) {
      CObPurchasingResponsibleGeneralModel purchasingResponsible =
              purchasingResponsibleGeneralModelRep.findOneByUserCode(
                      vendorValueObject.getPurchaseResponsibleCode());

      vendor.setPurchaseResponsibleId(purchasingResponsible.getId());
    }
  }

  private void setVendorName(MObVendor vendor, MObVendorCreationValueObject vendorValueObject) {
    LocalizedString vendorName = new LocalizedString();
    vendorName.setValue(new Locale("ar"), vendorValueObject.getVendorName());
    vendorName.setValue(new Locale("en"), vendorValueObject.getVendorName());

    vendor.setName(vendorName);
  }

  private void setVendorType(MObVendor vendor, MObVendorCreationValueObject vendorValueObject) {
    CObVendor cObVendor = cObVendorRep.findOneByUserCode(vendorValueObject.getVendorType());
    vendor.setTypeId(cObVendor.getId());
  }

  private void setVendorPurchaseUnitsData(
          MObVendor vendor, List<IObVendorPurchaseUnitValueObject> purchaseUnits) throws Exception {
    for (IObVendorPurchaseUnitValueObject purchaseUnit : purchaseUnits) {

      IObVendorPurchaseUnit vendorPurchaseUnit = new IObVendorPurchaseUnit();

      CObPurchasingUnitGeneralModel purchaseUnitGeneralModel =
              purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnit.getPurchaseUnitCode());
      vendorPurchaseUnit.setPurchaseUnitId(purchaseUnitGeneralModel.getId());
      vendorPurchaseUnit.setPurchaseUnitName(purchaseUnitGeneralModel.getPurchaseUnitName());
      this.commandUtils.setCreationAndModificationInfoAndDate(vendorPurchaseUnit);

      vendor.addMultipleDetail(IMObVendor.purchseUnitAttr, vendorPurchaseUnit);
    }
  }

  public void setMObVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchasingResponsibleGeneralModelRep(
      CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep) {
    this.purchasingResponsibleGeneralModelRep = purchasingResponsibleGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setTransactionManagerDelegate(TransactionManagerDelegate transactionManagerDelegate) {
    this.transactionManagerDelegate = transactionManagerDelegate;
  }

  public void setVendorEventRep(MObVendorEventRep vendorEventRep) {
    this.vendorEventRep = vendorEventRep;
  }

  public void setcObVendorRep(CObVendorRep cObVendorRep) {
    this.cObVendorRep = cObVendorRep;
  }
}
