package com.ebs.dda.commands.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.costing.*;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import io.reactivex.Observable;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public class DObCostingCreateCommand implements ICommand<DObInventoryDocumentActivateValueObject> {
  private final CommandUtils commandUtils;
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private final DObCostingStateMachine dObCostingStateMachine;

  private DObCostingRep costingRep;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory;
  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;

  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  private BigDecimal calculatedCurrencyPrice;
  private CreateCostingItemsSectionService createCostingItemsSectionService;

  public DObCostingCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      DObCostingStateMachine dObCostingStateMachine) {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    this.dObCostingStateMachine = dObCostingStateMachine;
  }

  @Override
  @Transactional
  public <T> Observable<T> executeCommand(DObInventoryDocumentActivateValueObject object)
      throws Exception {
    DObCosting costingDocument = createDObCosting();

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        getGoodsReceiptGeneralModel(object.getUserCode());

    DObVendorInvoiceGeneralModel vendorInvoice =
        getVendorInvoiceGeneralModel(goodsReceiptGeneralModel);

    IObVendorInvoiceSummary invoiceSummary = getVendorInvoiceSummary(vendorInvoice);

    ICalculateCostingExchangeRateStrategy strategy =
        getCalculateCurrencyPriceStrategy(invoiceSummary);
    calculatedCurrencyPrice = strategy.calculate(goodsReceiptGeneralModel);

    IObCostingCompanyData companyData = createCostingCompany(goodsReceiptGeneralModel);

    IObCostingDetails costingDetails = createCostingDetails(goodsReceiptGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesNotLike(
            goodsReceiptGeneralModel.getRefDocumentCode(),
            "%" + DObLandedCostStateMachine.DRAFT_STATE + "%");

    boolean isLandedCostActual =
        landedCostGeneralModel
            .getCurrentStates()
            .contains(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED);

    if (isLandedCostActual) {
      setDetailsCurrencyPriceActualTime(calculatedCurrencyPrice, costingDetails);
    } else {
      setDetailsCurrencyPriceEstimateTime(calculatedCurrencyPrice, costingDetails);
    }

    IObCostingItemsCreationValueObject costingItemsCreationValueObject =
        new IObCostingItemsCreationValueObject(
            vendorInvoice.getUserCode(),
            goodsReceiptGeneralModel.getUserCode(),
            landedCostGeneralModel.getUserCode(),
            calculatedCurrencyPrice,
            isLandedCostActual);

    List<IObCostingItem> costingItems =
        createCostingItemsSectionService.getCostingItems(costingItemsCreationValueObject);

    costingDocument.setHeaderDetail(IDObCosting.companyDataAttr, companyData);
    costingDocument.setHeaderDetail(IDObCosting.costingDetailsAttr, costingDetails);
    costingDocument.setLinesDetails(IDObCosting.accountingDocumentItemsAttr, costingItems);
    costingRep.create(costingDocument);

    return Observable.empty();
  }

  private ICalculateCostingExchangeRateStrategy getCalculateCurrencyPriceStrategy(
      IObVendorInvoiceSummary invoiceSummary) {
    return costingExchangeRateStrategyFactory.getStrategy(invoiceSummary);
  }

  private IObVendorInvoiceSummary getVendorInvoiceSummary(
      DObVendorInvoiceGeneralModel vendorInvoice) {
    return vendorInvoiceSummaryRep.findOneByRefInstanceId(vendorInvoice.getId());
  }

  private DObVendorInvoiceGeneralModel getVendorInvoiceGeneralModel(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    return vendorInvoiceGeneralModelRep.findOneByPurchaseOrderCode(
        goodsReceiptGeneralModel.getRefDocumentCode());
  }

  private IObCostingCompanyData createCostingCompany(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    IObCostingCompanyData companyData = new IObCostingCompanyData();

    companyData.setCompanyId(goodsReceiptGeneralModel.getCompanyId());
    companyData.setPurchaseUnitId(goodsReceiptGeneralModel.getPurchaseUnitId());

    commandUtils.setCreationAndModificationInfoAndDate(companyData);
    return companyData;
  }

  private IObCostingDetails createCostingDetails(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    IObCostingDetails costingDetails = new IObCostingDetails();
    costingDetails.setGoodsReceiptId(goodsReceiptGeneralModel.getId());
    commandUtils.setCreationAndModificationInfoAndDate(costingDetails);
    return costingDetails;
  }

  private DObCosting createDObCosting() throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObCosting.class.getSimpleName());
    DObCosting costing = new DObCosting();
    dObCostingStateMachine.initObjectState(costing);
    costing.setUserCode(userCode);
    setDocumentOwnerId(costing);
    commandUtils.setCreationAndModificationInfoAndDate(costing);
    return costing;
  }

  private void setDetailsCurrencyPriceActualTime(
      BigDecimal calculatedCurrencyPrice, IObCostingDetails costingDetails) {
    costingDetails.setCurrencyPriceActualTime(calculatedCurrencyPrice);
  }

  private void setDetailsCurrencyPriceEstimateTime(
      BigDecimal calculatedCurrencyPrice, IObCostingDetails costingDetails) {
    costingDetails.setCurrencyPriceEstimateTime(calculatedCurrencyPrice);
  }

  private DObGoodsReceiptGeneralModel getGoodsReceiptGeneralModel(String goodsReceiptCode) {
    return goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  private void setDocumentOwnerId(DObCosting costing) {
    Long loggedInUserId = commandUtils.getLoggedInUserId();
    costing.setDocumentOwnerId(loggedInUserId);
  }

  public void setCostingRep(DObCostingRep costingRep) {
    this.costingRep = costingRep;
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }

  public void setCostingExchangeRateStrategyFactory(
      CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory) {
    this.costingExchangeRateStrategyFactory = costingExchangeRateStrategyFactory;
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }

  public void setVendorInvoiceSummaryRep(IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep) {
    this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public void setCreateCostingItemsSectionService(
      CreateCostingItemsSectionService createCostingItemsSectionService) {
    this.createCostingItemsSectionService = createCostingItemsSectionService;
  }
}
