package com.ebs.dda.commands.masterdata.chartofaccount;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.codegenerator.ChartOfAccountCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.LObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.*;
import com.ebs.dda.masterdata.chartofaccounts.IGLAccountSubleadgerMapping;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObIntermediateInChartOfAccountRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObRootInChartOfAccountRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

public class HObChartOfAccountCreateCommand
    implements ICommand<HObChartOfAccountCreateValueObject> {
  public static final String ACTIVE_STATE = "Active";
  private CommandUtils commandUtils;
  private HObIntermediateInChartOfAccountRep intermediateInChartOfAccountRep;
  private HObLeafInChartOfAccountsRep leafInChartOfAccountsRep;
  private HObRootInChartOfAccountRep rootInChartOfAccountRep;
  private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
  private LObGlobalAccountConfigRep globalAccountConfigRep;
  private ChartOfAccountCodeGenerator chartOfAccountCodeGenerator;

  public HObChartOfAccountCreateCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject) throws Exception {

    String userCode = chartOfAccountCodeGenerator.generateUserCode(chartOfAccountCreateValueObject);
    if (chartOfAccountCreateValueObject.getLevel().equals(IHObChartOfAccounts.LEAF)) {
      HObLeafGLAccount createdLeafGlAccount =
          createLeafGLAccount(chartOfAccountCreateValueObject, userCode);
      updateAccountsConfiguration(createdLeafGlAccount, chartOfAccountCreateValueObject);
    } else if (chartOfAccountCreateValueObject
        .getLevel()
        .equals(IHObChartOfAccounts.INTERMEDIATE)) {
      createIntermediateGLAccount(chartOfAccountCreateValueObject, userCode);
    } else if (chartOfAccountCreateValueObject.getLevel().equals(IHObChartOfAccounts.ROOT)) {
      createRootGLAccount(chartOfAccountCreateValueObject, userCode);
    }
    return Observable.just(userCode);
  }

  private void updateAccountsConfiguration(
      HObLeafGLAccount createdLeafGlAccount,
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject)
      throws Exception {
    LObGlobalGLAccountConfig lObGlobalGLAccountConfig = new LObGlobalGLAccountConfig();
    lObGlobalGLAccountConfig.setAccountId(createdLeafGlAccount.getId());
    lObGlobalGLAccountConfig.setUserCode(chartOfAccountCreateValueObject.getMappedAccount());
    lObGlobalGLAccountConfig.setSysFlag(Boolean.FALSE);
    commandUtils.setCreationAndModificationInfoAndDate(lObGlobalGLAccountConfig);
    globalAccountConfigRep.create(lObGlobalGLAccountConfig);
  }

  private void createRootGLAccount(
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject, String userCode)
      throws Exception {
    HObRootGLAccount rootGLAccount = new HObRootGLAccount();
    setGeneralGlAccountData(rootGLAccount, chartOfAccountCreateValueObject, userCode);
    rootInChartOfAccountRep.create(rootGLAccount);
  }

  private void createIntermediateGLAccount(
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject, String userCode)
      throws Exception {
    HObIntermadiatGLAccount intermediateGLAccount = new HObIntermadiatGLAccount();
    setGeneralGlAccountData(intermediateGLAccount, chartOfAccountCreateValueObject, userCode);
    setParentAccount(intermediateGLAccount, chartOfAccountCreateValueObject);
    intermediateInChartOfAccountRep.create(intermediateGLAccount);
  }

  private HObLeafGLAccount createLeafGLAccount(
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject, String userCode)
      throws Exception {
    HObLeafGLAccount leafGlAccount = new HObLeafGLAccount();
    setGeneralGlAccountData(leafGlAccount, chartOfAccountCreateValueObject, userCode);
    leafGlAccount.setAccountType(String.valueOf(chartOfAccountCreateValueObject.getAccountType()));
    leafGlAccount.setCreditDebit(
        String.valueOf(chartOfAccountCreateValueObject.getAccountingEntry()));
    setParentAccount(leafGlAccount, chartOfAccountCreateValueObject);
    leafGlAccount.setMappedAccount(chartOfAccountCreateValueObject.getMappedAccount());
    setSubLedger(leafGlAccount, chartOfAccountCreateValueObject.getMappedAccount());
    HObLeafGLAccount createdLeafGlAccount = leafInChartOfAccountsRep.create(leafGlAccount);
    return createdLeafGlAccount;
  }

  private void setSubLedger(HObLeafGLAccount leafGlAccount, String mappedAccount) {
    String subleadger = IGLAccountSubleadgerMapping.subLeadgerMap.get(mappedAccount);
    leafGlAccount.setLeadger(subleadger);
  }

  private void setParentAccount(
      HObGLAccount glAccount, HObChartOfAccountCreateValueObject valueObject) {
    HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel =
        chartOfAccountsGeneralModelRep.findOneByUserCode(valueObject.getParentCode());
    glAccount.setParentId(chartOfAccountsGeneralModel.getId());
    this.commandUtils.setModificationInfoAndModificationDate(glAccount);
  }

  private void setGeneralGlAccountData(
      HObGLAccount glAccount, HObChartOfAccountCreateValueObject valueObject, String userCode) {
    Set<String> currentStates = new HashSet<>();
    currentStates.add(ACTIVE_STATE);
    LocalizedString name = new LocalizedString();
    name.setValue(ISysDefaultLocales.ARABIC_LOCALE, valueObject.getAccountName());
    name.setValue(ISysDefaultLocales.ENGLISH_LOCALE, valueObject.getAccountName());
    glAccount.setName(name);
    glAccount.setCurrentStates(currentStates);
    glAccount.setUserCode(userCode);
    glAccount.setOldAccountCode(valueObject.getOldAccountCode());
    commandUtils.setCreationAndModificationInfoAndDate(glAccount);
  }

  public void setIntermediateInChartOfAccountRep(
      HObIntermediateInChartOfAccountRep intermediateInChartOfAccountRep) {
    this.intermediateInChartOfAccountRep = intermediateInChartOfAccountRep;
  }

  public void setLeafInChartOfAccountsRep(HObLeafInChartOfAccountsRep leafInChartOfAccountsRep) {
    this.leafInChartOfAccountsRep = leafInChartOfAccountsRep;
  }

  public void setChartOfAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
    this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setChartOfAccountCodeGenerator(
      ChartOfAccountCodeGenerator chartOfAccountCodeGenerator) {
    this.chartOfAccountCodeGenerator = chartOfAccountCodeGenerator;
  }

  public void setGlobalAccountConfigRep(LObGlobalAccountConfigRep globalAccountConfigRep) {
    this.globalAccountConfigRep = globalAccountConfigRep;
  }

  public void setRootInChartOfAccountRep(HObRootInChartOfAccountRep rootInChartOfAccountRep) {
    this.rootInChartOfAccountRep = rootInChartOfAccountRep;
  }
}
