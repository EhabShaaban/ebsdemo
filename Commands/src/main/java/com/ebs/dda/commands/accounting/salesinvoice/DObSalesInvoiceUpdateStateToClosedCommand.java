package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.RefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class DObSalesInvoiceUpdateStateToClosedCommand
    implements ICommand<RefDocumentRemainingValueObject> {

  private DObSalesInvoiceRep salesInvoiceRep;
  private IObInvoiceSummaryGeneralModelRep salesInvoiceItemSummaryGeneralModelRep;
  private CommandUtils commandUtils;

  public DObSalesInvoiceUpdateStateToClosedCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(RefDocumentRemainingValueObject object)
      throws Exception {
    String salesInvoiceCode = object.getRefDocumentCode();
    IObInvoiceSummaryGeneralModel itemSummary =
        salesInvoiceItemSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
            salesInvoiceCode, IDocumentTypes.SALES_INVOICE);
    if (new BigDecimal(itemSummary.getTotalRemaining()).compareTo(BigDecimal.ZERO) == 0) {
      DObSalesInvoice salesInvoice = salesInvoiceRep.findOneByUserCode(salesInvoiceCode);
      updateSalesInvoiceStateToClosed(getSalesInvoiceStateMachine(salesInvoice));
      commandUtils.setModificationInfoAndModificationDate(salesInvoice);
      salesInvoiceRep.update(salesInvoice);
    }
    if (object instanceof DObNotesReceivablesUpdateRefDocumentRemainingValueObject) {
      return Observable.just(
          ((DObNotesReceivablesUpdateRefDocumentRemainingValueObject) object)
              .getNotesReceivablesCode());
    } else {
      return Observable.just(
          ((DObCollectionUpdateRefDocumentRemainingValueObject) object).getCollectionCode());
    }
  }

  private void updateSalesInvoiceStateToClosed(AbstractStateMachine salesInvoiceStateMachine)
      throws Exception {
    if (salesInvoiceStateMachine.canFireEvent(IDObSalesInvoiceActionNames.CLOSE)) {
      salesInvoiceStateMachine.fireEvent(IDObSalesInvoiceActionNames.CLOSE);
      salesInvoiceStateMachine.save();
    }
  }

  private AbstractStateMachine getSalesInvoiceStateMachine(DObSalesInvoice salesInvoice)
      throws Exception {
    DObSalesInvoiceFactory salesInvoiceStateMachine = new DObSalesInvoiceFactory();
    AbstractStateMachine invoiceStateMachine =
        salesInvoiceStateMachine.createSalesInvoiceStateMachine(salesInvoice);
    return invoiceStateMachine;
  }

  public void setSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
    this.salesInvoiceRep = salesInvoiceRep;
  }

  public void setSalesInvoiceItemSummaryGeneralModelRep(
      IObInvoiceSummaryGeneralModelRep salesInvoiceItemSummaryGeneralModelRep) {
    this.salesInvoiceItemSummaryGeneralModelRep = salesInvoiceItemSummaryGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
