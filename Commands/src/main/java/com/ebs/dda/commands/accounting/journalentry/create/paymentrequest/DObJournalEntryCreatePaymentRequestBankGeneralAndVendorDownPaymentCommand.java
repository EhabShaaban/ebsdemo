package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObLeafGLAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public abstract class DObJournalEntryCreatePaymentRequestBankGeneralAndVendorDownPaymentCommand
    extends DObJournalEntryCreatePaymentRequestCommand {

  private HObLeafInChartOfAccountsRep leafInChartOfAccountsRep;
  private HObLeafGLAccount leafGLAccount;

  protected DObJournalEntryCreatePaymentRequestBankGeneralAndVendorDownPaymentCommand() {
  }

  protected Observable<String> executeBankGeneralAndVendorDownPaymentCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    executeParentCommand(paymentRequestValueObject);
    initGeneralTypeEntities();
    setJournalItemsData(journalEntryItems);
    executeTransaction(paymentRequestJournalEntry, journalEntryItems);
    return Observable.just(paymentRequestJournalEntry.getUserCode());
  }

  private void initGeneralTypeEntities() {
    Long glAccountId = paymentRequestAccountingDetailsDebit.getGlAccountId();
    leafGLAccount = leafInChartOfAccountsRep.findOneById(glAccountId);
  }

  private void setJournalItemsData(List<IObJournalEntryItem> journalEntryItems) {
    setJournalItemGeneralTypeData(journalEntryItems);
    setJournalItemBankData(journalEntryItems);
  }

  private void setJournalItemBankData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount =
        new IObJournalEntryItemBankSubAccount();
    setAccountAndSubAccountForBankType(journalEntryItemBankSubAccount);
    setDetailsForBankType(journalEntryItemBankSubAccount);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemBankSubAccount);
    journalEntryItems.add(journalEntryItemBankSubAccount);
  }

  private void setDetailsForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItemBankSubAccount.setCredit(paymentDetails.getNetAmount());
    journalEntryItemBankSubAccount.setDebit(BigDecimal.ZERO);
    journalEntryItemBankSubAccount.setDocumentCurrencyId(currencyId);
    journalEntryItemBankSubAccount.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItemBankSubAccount.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItemBankSubAccount.setCompanyExchangeRateId(exchangeRateId);
  }

  private void setAccountAndSubAccountForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount) {

    journalEntryItemBankSubAccount.setAccountId(
        paymentRequestAccountingDetailsCredit.getGlAccountId()
    );
    journalEntryItemBankSubAccount.setSubAccountId(
        paymentRequestAccountingDetailsCredit.getGlSubAccountId()
    );
  }

  private void setJournalItemGeneralTypeData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItem journalEntryItem = createJournalEntryAndSetSubAccountBasedOnLedger();
    Long accountId = paymentRequestAccountingDetailsDebit.getGlAccountId();
    journalEntryItem.setAccountId(accountId);
    setJournalItemDetailsForGeneralType(journalEntryItem);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
    journalEntryItems.add(journalEntryItem);
  }

  private IObJournalEntryItem createJournalEntryAndSetSubAccountBasedOnLedger() {
    String ledger = leafGLAccount.getLeadger();
    IObJournalEntryItem journalEntryItem;
    if (ledger != null && ledger.equals(SubLedgers.PO.name())) {
      journalEntryItem = new IObJournalEntryItemPurchaseOrderSubAccount();
      addSubAccountForPOType(journalEntryItem);
    } else if (ledger != null && isLedgerVendor(ledger)) {
      journalEntryItem = new IObJournalEntryItemVendorSubAccount();
      addSubAccountAndLedgerForVendorType(journalEntryItem);
    } else {
      journalEntryItem = new IObJournalEntryItemGeneral();
    }
    return journalEntryItem;
  }

  private boolean isLedgerVendor(String ledger) {
    return ledger.equals(SubLedgers.Local_Vendors.name())
        || ledger.equals(SubLedgers.Import_Vendors.name());
  }

  private void addSubAccountAndLedgerForVendorType(IObJournalEntryItem journalEntryItem) {
    Long subAccountId = paymentRequestAccountingDetailsDebit.getGlSubAccountId();
    String subLedger = paymentRequestAccountingDetailsDebit.getSubLedger();
    ((IObJournalEntryItemVendorSubAccount) journalEntryItem).setSubAccountId(subAccountId);
    ((IObJournalEntryItemVendorSubAccount) journalEntryItem).setType(subLedger);
  }

  private void addSubAccountForPOType(IObJournalEntryItem journalEntryItem) {
    Long subAccountId = paymentRequestAccountingDetailsDebit.getGlSubAccountId();
    ((IObJournalEntryItemPurchaseOrderSubAccount) journalEntryItem).setSubAccountId(subAccountId);
  }

  private void setJournalItemDetailsForGeneralType(IObJournalEntryItem journalEntryItem) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItem.setCredit(BigDecimal.ZERO);
    journalEntryItem.setDebit(paymentDetails.getNetAmount());
    journalEntryItem.setDocumentCurrencyId(currencyId);
    journalEntryItem.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItem.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItem.setCompanyExchangeRateId(exchangeRateId);
  }

  public void setLeafInChartOfAccountsRep(HObLeafInChartOfAccountsRep leafInChartOfAccountsRep) {
    this.leafInChartOfAccountsRep = leafInChartOfAccountsRep;
  }
}
