package com.ebs.dda.commands.masterdata.item;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.item.*;
import com.ebs.dda.jpa.masterdata.itemgroup.CObItemGroup;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.item.CObItemRep;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class MObItemCreateCommand implements ICommand<MObItemCreationValueObject> {

  private final CommandUtils commandUtils;
  private final MasterDataCodeGenrator masterDataCodeGenrator;
  private String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";
  private CObMeasureRep measureRep;
  private CObMaterialGroupRep materialGroupRep;
  private MObItemRep itemRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private CObItemRep cObItemRep;

  public MObItemCreateCommand(MasterDataCodeGenrator masterDataCodeGenrator) {
    this.masterDataCodeGenrator = masterDataCodeGenrator;
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(MObItemCreationValueObject itemCreationValueObject)
      throws Exception {
    String userCode = masterDataCodeGenrator.generateUserCode(MObItem.class);
    MObItem item = new MObItem();
    setItemData(item, itemCreationValueObject, userCode);
    setItemPurchaseUnitsData(item, itemCreationValueObject.getPurchaseUnits());
    commandUtils.setModificationInfoAndModificationDate(item);
    itemRep.create(item);
    return Observable.just(userCode);
  }

  private void setItemData(
      MObItem item, MObItemCreationValueObject itemCreationValueObject, String userCode) {
    item.setUserCode(userCode);

    Set<String> currentStates = new HashSet<>();
    currentStates.add("Active");
    item.setCurrentStates(currentStates);

    LocalizedString itemName = new LocalizedString();
    itemName.setValue(new Locale("ar"), itemCreationValueObject.getItemName());
    itemName.setValue(new Locale("en"), itemCreationValueObject.getItemName());

    item.setName(itemName);
    CObItem cObItem = cObItemRep.findOneByUserCode(itemCreationValueObject.getItemType());
    item.setTypeId(cObItem.getId());

    item.setMarketName(itemCreationValueObject.getMarketName());

    Long basicUnitOfMeasureId =
        measureRep.findOneByUserCode(itemCreationValueObject.getBaseUnitCode()).getId();
    item.setBasicUnitOfMeasure(basicUnitOfMeasureId);

    if (itemCreationValueObject.getItemType().equals(COMMERCIAL_ITEM_TYPE)) {
      CObItemGroup itemGroup =
          materialGroupRep.findOneByUserCode(itemCreationValueObject.getItemGroupCode());
      Long itemGroupId = itemGroup.getId();
      item.setItemGroup(itemGroupId);

      item.setBatchManagementRequired(itemCreationValueObject.getBatchManaged());
    }
    item.setOldItemNumber(itemCreationValueObject.getItemOldNumber());

    commandUtils.setCreationAndModificationInfoAndDate(item);
  }

  private void setItemPurchaseUnitsData(
      MObItem item, List<IObItemPurchaseUnitValueObject> purchaseUnits) throws Exception {
    for (IObItemPurchaseUnitValueObject purchaseUnit : purchaseUnits) {

      IObItemPurchaseUnit itemPurchaseUnit = new IObItemPurchaseUnit();
      Long purchaseUnitId =
          purchasingUnitRep.findOneByUserCode(purchaseUnit.getPurchaseUnitCode()).getId();
      itemPurchaseUnit.setPurchaseUnitId(purchaseUnitId);

      commandUtils.setCreationAndModificationInfoAndDate(itemPurchaseUnit);
      item.addMultipleDetail(IMObItem.purchseUnitAttr, itemPurchaseUnit);
    }
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setMaterialGroupRep(CObMaterialGroupRep materialGroupRep) {
    this.materialGroupRep = materialGroupRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setcObItemRep(CObItemRep cObItemRep) {
    this.cObItemRep = cObItemRep;
  }
}
