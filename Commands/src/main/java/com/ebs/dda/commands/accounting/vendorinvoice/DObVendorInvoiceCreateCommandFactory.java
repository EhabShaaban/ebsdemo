package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;

public class DObVendorInvoiceCreateCommandFactory {

    private DObGoodsInvoiceCreateCommand goodsInvoiceCreateCommand;
    private DObPurchaseServiceLocalVendorInvoiceCreateCommand purchaseServiceLocalVendorInvoiceCreateCommand;

    public DObVendorInvoiceCreateCommand getVendorInvoiceCreateCommandInstance(String vendorInvoiceType) {
        switch (vendorInvoiceType) {
            case VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE:
            case VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE:
                return goodsInvoiceCreateCommand;
            case VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE:
            case VendorInvoiceTypeEnum.Values.SHIPMENT_INVOICE:
            case VendorInvoiceTypeEnum.Values.INSURANCE_INVOICE:
                return purchaseServiceLocalVendorInvoiceCreateCommand;

        }
        throw new IllegalArgumentException("Unsupported Vendor Invoice Type");
    }

    public void setGoodsInvoiceCreateCommand(DObGoodsInvoiceCreateCommand goodsInvoiceCreateCommand) {
        this.goodsInvoiceCreateCommand = goodsInvoiceCreateCommand;
    }

    public void setPurchaseServiceLocalVendorInvoiceCreateCommand(DObPurchaseServiceLocalVendorInvoiceCreateCommand purchaseServiceLocalVendorInvoiceCreateCommand) {
        this.purchaseServiceLocalVendorInvoiceCreateCommand = purchaseServiceLocalVendorInvoiceCreateCommand;
    }
}
