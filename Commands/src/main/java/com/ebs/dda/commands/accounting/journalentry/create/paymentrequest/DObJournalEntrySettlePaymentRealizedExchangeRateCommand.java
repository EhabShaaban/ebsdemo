package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;


public class DObJournalEntrySettlePaymentRealizedExchangeRateCommand implements ICommand<DObJournalEntryCreateValueObject> {

    private LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep;
    private DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep exchangeRateDifferenceGeneralModelRep;
    private DObPaymentRequestJournalEntryRep journalEntryRep;
    private CommandUtils commandUtils;

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommand(){
        commandUtils = new CommandUtils();
    }
    @Override
    public Observable<String> executeCommand(
            DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
        String paymentCode = paymentRequestValueObject.getUserCode();
        DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel exchangeRateDifferenceGeneralModel = exchangeRateDifferenceGeneralModelRep.findOneByPaymentCode(paymentCode);
        BigDecimal realizedDifferenceOnExchangeValue = calculateRealizedDifferenceOnExchange(exchangeRateDifferenceGeneralModel);
        DObPaymentRequestJournalEntry journalEntry = journalEntryRep.findOneByUserCode(paymentRequestValueObject.getJournalEntryCode());
        setRealizedDifferenceOnExchange(journalEntry, realizedDifferenceOnExchangeValue, exchangeRateDifferenceGeneralModel);
        journalEntryRep.update(journalEntry);
        return Observable.just(paymentRequestValueObject.getJournalEntryCode());
    }

    private void setRealizedDifferenceOnExchange(DObPaymentRequestJournalEntry journalEntry, BigDecimal realizedDifferenceOnExchange, DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel exchangeRateDifferenceGeneralModel) {
        if (realizedDifferenceOnExchange.signum() == 0)
            return;
        IObJournalEntryItemVendorSubAccount vendorJournalEntryItem = getCommonVendorJournalEntryItemData(exchangeRateDifferenceGeneralModel);
        IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem = getCommonRealizedDifferenceJournalEntryItemData(exchangeRateDifferenceGeneralModel);
        if (realizedDifferenceOnExchange.signum() < 0) {
            debitRealizedExchangeRate(realizedDifferenceJournalEntryItem, realizedDifferenceOnExchange);
            creditVendor(vendorJournalEntryItem, realizedDifferenceOnExchange);
        } else {
            creditRealizedExchangeRate(realizedDifferenceJournalEntryItem, realizedDifferenceOnExchange);
            debitVendor(vendorJournalEntryItem, realizedDifferenceOnExchange);
        }
        journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, vendorJournalEntryItem);
        journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, realizedDifferenceJournalEntryItem);
    }

    private void debitVendor(IObJournalEntryItemVendorSubAccount vendorJournalEntryItem, BigDecimal realizedDifferenceOnExchange) {
        vendorJournalEntryItem.setCredit(BigDecimal.ZERO);
        vendorJournalEntryItem.setDebit(realizedDifferenceOnExchange.abs());
    }

    private void creditRealizedExchangeRate(IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem, BigDecimal realizedDifferenceOnExchange) {
        realizedDifferenceJournalEntryItem.setDebit(BigDecimal.ZERO);
        realizedDifferenceJournalEntryItem.setCredit(realizedDifferenceOnExchange.abs());
    }

    private IObJournalEntryItemVendorSubAccount getCommonVendorJournalEntryItemData(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel exchangeRateDifferenceGeneralModel) {
        IObJournalEntryItemVendorSubAccount vendorJournalEntryItem = new IObJournalEntryItemVendorSubAccount();
        Long companyCurrencyId = exchangeRateDifferenceGeneralModel.getLocalCurrencyId();
        vendorJournalEntryItem.setAccountId(exchangeRateDifferenceGeneralModel.getGlAccountId());
        vendorJournalEntryItem.setSubAccountId(exchangeRateDifferenceGeneralModel.getGlSubAccountId());
        vendorJournalEntryItem.setType(exchangeRateDifferenceGeneralModel.getSubLedger());
        vendorJournalEntryItem.setCompanyCurrencyId(companyCurrencyId);
        vendorJournalEntryItem.setDocumentCurrencyId(companyCurrencyId);
        vendorJournalEntryItem.setCompanyCurrencyPrice(BigDecimal.ONE);
        commandUtils.setCreationAndModificationInfoAndDate(vendorJournalEntryItem);
        return vendorJournalEntryItem;
    }

    private IObJournalEntryItemGeneral getCommonRealizedDifferenceJournalEntryItemData(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel exchangeRateDifferenceGeneralModel) {
        IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem = new IObJournalEntryItemGeneral();
        LObGlobalGLAccountConfigGeneralModel globalGLAccountConfigGeneralModel =
                accountConfigGeneralModelRep
                        .findOneByUserCode(ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);
        Long companyCurrencyId = exchangeRateDifferenceGeneralModel.getLocalCurrencyId();
        realizedDifferenceJournalEntryItem.setAccountId(globalGLAccountConfigGeneralModel.getGlAccountId());
        realizedDifferenceJournalEntryItem.setCompanyCurrencyId(companyCurrencyId);
        realizedDifferenceJournalEntryItem.setDocumentCurrencyId(companyCurrencyId);
        realizedDifferenceJournalEntryItem.setCompanyCurrencyPrice(BigDecimal.ONE);
        commandUtils.setCreationAndModificationInfoAndDate(realizedDifferenceJournalEntryItem);
        return realizedDifferenceJournalEntryItem;
    }

    private void creditVendor(IObJournalEntryItemVendorSubAccount vendorJournalEntryItem, BigDecimal realizedDifferenceOnExchange) {
        vendorJournalEntryItem.setCredit(realizedDifferenceOnExchange.abs());
        vendorJournalEntryItem.setDebit(BigDecimal.ZERO);
    }

    private void debitRealizedExchangeRate(IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem, BigDecimal realizedDifferenceOnExchange) {
        realizedDifferenceJournalEntryItem.setDebit(realizedDifferenceOnExchange.abs());
        realizedDifferenceJournalEntryItem.setCredit(BigDecimal.ZERO);
    }

    private BigDecimal calculateRealizedDifferenceOnExchange(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel exchangeRateDifferenceGeneralModel) {
        BigDecimal currentCurrencyPrice = exchangeRateDifferenceGeneralModel.getPaymentCurrencyPrice();
        BigDecimal vendorInvoiceCurrencyPrice =  exchangeRateDifferenceGeneralModel.getVendorInvoiceCurrencyPrice();
        BigDecimal paymentAmount = exchangeRateDifferenceGeneralModel.getPaymentAmount();
        return ((paymentAmount).multiply(vendorInvoiceCurrencyPrice)).subtract(paymentAmount.multiply(currentCurrencyPrice));
    }

    public void setAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep) {
        this.accountConfigGeneralModelRep = accountConfigGeneralModelRep;
    }

    public void setJournalEntryRep(DObPaymentRequestJournalEntryRep journalEntryRep) {
        this.journalEntryRep = journalEntryRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setExchangeRateDifferenceGeneralModelRep(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep exchangeRateDifferenceGeneralModelRep) {
        this.exchangeRateDifferenceGeneralModelRep = exchangeRateDifferenceGeneralModelRep;
    }
}
