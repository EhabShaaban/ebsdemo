package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemBankSubAccount;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand
    extends DObJournalEntryCreatePaymentRequestVendorInvoiceCommand {

  public DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand() {}

  @Override
  public Observable<String> executeCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    super.executeCommand(paymentRequestValueObject);
    setJournalItemsData(journalEntryItems);
    executeTransaction(paymentRequestJournalEntry, journalEntryItems);
    return Observable.just(paymentRequestJournalEntry.getUserCode());
  }

  private void setJournalItemsData(List<IObJournalEntryItem> journalEntryItems) {
    setJournalItemVendorInvoiceData(journalEntryItems);
    setJournalItemBankData(journalEntryItems);
  }

  private void setJournalItemBankData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount =
        new IObJournalEntryItemBankSubAccount();
    setAccountAndSubAccountForBankType(journalEntryItemBankSubAccount);
    setDetailsForBankType(journalEntryItemBankSubAccount);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemBankSubAccount);
    journalEntryItems.add(journalEntryItemBankSubAccount);
  }

  private void setDetailsForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItemBankSubAccount.setCredit(paymentDetails.getNetAmount());
    journalEntryItemBankSubAccount.setDebit(BigDecimal.ZERO);
    journalEntryItemBankSubAccount.setDocumentCurrencyId(currencyId);
    journalEntryItemBankSubAccount.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItemBankSubAccount.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItemBankSubAccount.setCompanyExchangeRateId(exchangeRateId);
  }

  private void setAccountAndSubAccountForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount) {

    journalEntryItemBankSubAccount.setAccountId(
        paymentRequestAccountingDetailsCredit.getGlAccountId()
    );
    journalEntryItemBankSubAccount.setSubAccountId(
        paymentRequestAccountingDetailsCredit.getGlSubAccountId()
    );
  }
}
