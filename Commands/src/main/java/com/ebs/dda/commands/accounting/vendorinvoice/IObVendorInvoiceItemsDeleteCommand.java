package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsDeletionValueObject;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import javax.transaction.Transactional;

public class IObVendorInvoiceItemsDeleteCommand implements ICommand<IObVendorInvoiceItemsDeletionValueObject> {

  private IObVendorInvoiceItemRep invoicesItemsRep;
  private DObVendorInvoiceRep invoiceRep;
  private IObVendorInvoiceSummaryUpdateCommand SummaryUpdateCommand;
  private IUserAccountSecurityService userAccountSecurityService;

  public IObVendorInvoiceItemsDeleteCommand() {}

  @Transactional
  @Override
  public Observable<DObVendorInvoice> executeCommand(
      IObVendorInvoiceItemsDeletionValueObject itemsDeletionValueObject) throws Exception {

    Long invoiceItemId = itemsDeletionValueObject.getItemId();
    String invoiceCode = itemsDeletionValueObject.getInvoiceCode();

    invoicesItemsRep.deleteById(invoiceItemId);
    DObVendorInvoice invoice = invoiceRep.findOneByUserCode(invoiceCode);
    setModificationInfo(invoice);
    SummaryUpdateCommand.executeCommand(invoice.getUserCode());
    return Observable.just(invoice);
  }

  public void setInvoicesItemsRep(IObVendorInvoiceItemRep invoicesItemsRep) {
    this.invoicesItemsRep = invoicesItemsRep;
  }

  public void setInvoiceRep(DObVendorInvoiceRep invoiceRep) {
    this.invoiceRep = invoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  private void setModificationInfo(DObVendorInvoice dObInvoice) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    dObInvoice.setModificationInfo(loggedinUserInfo);
    dObInvoice.setModifiedDate(new DateTime());
    invoiceRep.save(dObInvoice);
  }

  public void setSummaryUpdateCommand(IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand) {
    SummaryUpdateCommand = summaryUpdateCommand;
  }
}
