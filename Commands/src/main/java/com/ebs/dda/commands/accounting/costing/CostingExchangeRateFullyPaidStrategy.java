package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CostingExchangeRateFullyPaidStrategy implements ICalculateCostingExchangeRateStrategy {
  private CostingExchangeRateUtils costingExchangeRateUtils;

  public CostingExchangeRateFullyPaidStrategy(CostingExchangeRateUtils costingExchangeRateUtils) {
    this.costingExchangeRateUtils = costingExchangeRateUtils;
  }

  @Override
  public BigDecimal calculate(DObGoodsReceiptGeneralModel goodsReceipt) {

    DObVendorInvoiceGeneralModel vendorInvoice =
        costingExchangeRateUtils.getVendorInvoice(goodsReceipt.getRefDocumentCode());

    List<DObPaymentRequestCurrencyPriceGeneralModel> payments =
        costingExchangeRateUtils.constructDObPaymentRequestCurrencyPriceGeneralModels(
            goodsReceipt, vendorInvoice);

    BigDecimal totalAmountInLocalCurrency =
        costingExchangeRateUtils.getPaymentsTotalAmountInLocalCurrency(payments);
    BigDecimal totalAmount = costingExchangeRateUtils.getVendorInvoiceTotalAmount();

    return CommandUtils.getDivideResult(totalAmountInLocalCurrency, totalAmount);
  }

  @Override
  public void setVendorInvoiceSummary(IObVendorInvoiceSummary vendorInvoiceSummary) {
    this.costingExchangeRateUtils.setVendorInvoiceSummary(vendorInvoiceSummary);
  }
}
