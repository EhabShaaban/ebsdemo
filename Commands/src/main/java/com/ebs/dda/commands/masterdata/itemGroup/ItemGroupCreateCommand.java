package com.ebs.dda.commands.masterdata.itemGroup;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;
import com.ebs.dda.jpa.masterdata.itemgroup.CObItemGroup;
import com.ebs.dda.jpa.masterdata.itemgroup.HObItemGroupValueObject;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupRep;
import io.reactivex.Observable;

import java.util.Locale;
import java.util.Optional;

public class ItemGroupCreateCommand implements ICommand<HObItemGroupValueObject> {

  private CObMaterialGroupRep itemGroupRep;
  private CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep;
  private final CommandUtils commandUtils;

  public ItemGroupCreateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(HObItemGroupValueObject itemGroupValueObject)
      throws Exception {
    String userCode;
    if (!itemGroupValueObject
        .getType()
        .equals(HierarchyLevelEnumType.HierarchyLevelEnum.ROOT.toString())) {
      userCode =
          generateItemGroupCodeAsIntermediate(Long.parseLong(itemGroupValueObject.getParentCode()));
    } else {
      userCode = generateItemGroupCodeAsRoot();
    }
    CObItemGroup itemGroup = new CObItemGroup();
    setItemData(itemGroup, itemGroupValueObject, userCode);
    commandUtils.setCreationAndModificationInfoAndDate(itemGroup);
    itemGroupRep.create(itemGroup);
    return Observable.just(userCode);
  }

  private void setItemData(
      CObItemGroup itemGroup,
      HObItemGroupValueObject itemGroupCreationValueObject,
      String userCode) {

    itemGroup.setUserCode(userCode);

    if (itemGroupCreationValueObject.getParentCode() != null) {
      Long parent =
          itemGroupGeneralModelRep.findParentItemGroupId(
              itemGroupCreationValueObject.getParentCode());
      itemGroup.setParentGroup(parent);
    }

    HierarchyLevelEnumType level = new HierarchyLevelEnumType();
    HierarchyLevelEnumType.HierarchyLevelEnum levelId =
        HierarchyLevelEnumType.HierarchyLevelEnum.valueOf(itemGroupCreationValueObject.getType());
    level.setId(levelId);
    itemGroup.setLevel(level);

    LocalizedString name = new LocalizedString();
    name.setValue(new Locale("en"), itemGroupCreationValueObject.getName());
    name.setValue(new Locale("ar"), itemGroupCreationValueObject.getName());
    itemGroup.setName(name);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setItemRep(CObMaterialGroupRep itemGroupRep) {
    this.itemGroupRep = itemGroupRep;
  }

  public void setItemGroupGeneralModelRep(
      CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep) {
    this.itemGroupGeneralModelRep = itemGroupGeneralModelRep;
  }

  private String generateItemGroupCodeAsRoot() {
    Optional<Long> maxId = itemGroupGeneralModelRep.findItemGroupMaxIdAsRoot();
    Optional<String> code =
        itemGroupGeneralModelRep.findItemGroupCodeAsRootWithMaxId(maxId.orElse(0L));
    long newCode = Long.parseLong(code.orElse("000000")) + 1;

    return padding(Long.toString(newCode), 6);
  }

  private String generateItemGroupCodeAsIntermediate(Long parentCode) {
    Optional<Long> maxAsHierarchyId =
        itemGroupGeneralModelRep.findItemGroupMaxIdAsHierarchy(padding(parentCode.toString(), 6));
    Optional<String> code;
    long lastItemGroupCode;
    if (!maxAsHierarchyId.isPresent()) {
      code = Optional.of(parentCode + "1");
      lastItemGroupCode = Long.parseLong(code.orElse("000000"));
    } else {
      code =
          itemGroupGeneralModelRep.findItemGroupCodeAsHierarchyWithMaxId(
              maxAsHierarchyId.orElse(0L));
      lastItemGroupCode =
          Long.parseLong(generateNewChildUserCode(code.orElse("000000"), parentCode.toString()));
    }

    return padding(Long.toString(lastItemGroupCode), 6);
  }

  private String generateNewChildUserCode(String code, String parentCode) {
    String[] parts = code.split(parentCode, 2);
    String part2 = parts[1];
    long newChildGeneratedCode = Long.parseLong(part2) + 1;
    String newCode = parentCode + newChildGeneratedCode;
    return newCode;
  }

  private String padding(String s, int padLength) {
    StringBuilder sb = new StringBuilder(s);
    for (int i = sb.length(); i < padLength; i++) {
      sb.insert(0, "0");
    }
    return sb.toString();
  }
}
