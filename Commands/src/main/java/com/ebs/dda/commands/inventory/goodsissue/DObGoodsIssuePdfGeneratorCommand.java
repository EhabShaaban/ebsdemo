package com.ebs.dda.commands.inventory.goodsissue;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemGeneralModelRep;
import io.reactivex.Observable;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class DObGoodsIssuePdfGeneratorCommand implements ICommand {
  private IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep;
  private DataSource dataSource;

  public void setGoodsIssueItemGeneralModelRep(
          IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep) {
    this.goodsIssueItemGeneralModelRep = goodsIssueItemGeneralModelRep;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Observable executeCommand(Object goodsIssueCode) throws Exception {

    List<IObGoodsIssueItemGeneralModel> goodsIssueItems =
            goodsIssueItemGeneralModelRep.findByGoodsIssueCode(goodsIssueCode.toString());

    InputStream jrxmlFile =
            DObGoodsIssuePdfGeneratorCommand.class.getResourceAsStream("/goodsIssue_blue.jrxml");
    HashMap reportParameters = new HashMap();
    reportParameters.put("Parameter1", goodsIssueCode);
    reportParameters.put("itemsNumber", goodsIssueItems.size());

    JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFile);

    JasperPrint report =
            JasperFillManager.fillReport(jasperReport, reportParameters, dataSource.getConnection());
    report.addStyle(prepareNormalStyle());
    // Export pdf file
    byte[] pdf = JasperExportManager.exportReportToPdf(report);
    return Observable.just(new String(Base64.encodeBase64(pdf), "UTF-8"));
  }

  JRDesignStyle prepareNormalStyle() {
    JRDesignStyle jrDesignStyle = new JRDesignStyle();
    jrDesignStyle.setDefault(true);
    jrDesignStyle.setPdfFontName("/arial.ttf");
    jrDesignStyle.setPdfEncoding("Identity-H");
    return jrDesignStyle;
  }
}
