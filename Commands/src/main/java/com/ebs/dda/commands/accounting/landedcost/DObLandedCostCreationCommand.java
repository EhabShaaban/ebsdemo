package com.ebs.dda.commands.accounting.landedcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachineFactory;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.CObLandedCostTypeRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;

public class DObLandedCostCreationCommand implements ICommand<DObLandedCostCreationValueObject> {

  private CommandUtils commandUtils;
  private DObLandedCostStateMachineFactory stateMachineFactory;

  private DocumentObjectCodeGenerator codeGenerator;

  private CObLandedCostTypeRep landedCostTypeRep;
  private DObLandedCostRep landedCostRep;

    private IObOrderCompanyGeneralModelRep poCompanyGMRep;
  private CObCompanyRep companyRep;

  private DObPurchaseOrderGeneralModelRep purchaseOrderGMRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;

  public DObLandedCostCreationCommand() {
    commandUtils = new CommandUtils();
    stateMachineFactory = new DObLandedCostStateMachineFactory();
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional
  public Observable<String> executeCommand(DObLandedCostCreationValueObject valueObject)
      throws Exception {

    DObPurchaseOrderGeneralModel purchaseOrderGM =
        purchaseOrderGMRep.findOneByUserCode(valueObject.getPurchaseOrderCode());

    DObLandedCost landedCost = createLandedCost(valueObject);
    IObLandedCostCompanyData companyData = createCompanyData(purchaseOrderGM);
    IObLandedCostDetails details = createDetails(purchaseOrderGM);

    landedCost.setHeaderDetail(IDObLandedCost.companyData, companyData);
    landedCost.setHeaderDetail(IDObLandedCost.landedCostDetails, details);

    landedCostRep.create(landedCost);

    return Observable.just(landedCost.getUserCode());
  }

  private IObLandedCostDetails createDetails(DObPurchaseOrderGeneralModel purchaseOrderGM) {
    IObLandedCostDetails details = new IObLandedCostDetails();
    commandUtils.setCreationAndModificationInfoAndDate(details);
    details.setPurchaseOrderId(purchaseOrderGM.getId());
    details.setVendorInvoiceId(getVendorInvoiceId(purchaseOrderGM.getUserCode() , purchaseOrderGM.getObjectTypeCode()));
    details.setDamageStock(false);
    return details;
  }

  private Long getVendorInvoiceId(String poCode, String poType) {
  String invoiceType =  getVendorInvoiceType(poType);
    DObVendorInvoiceGeneralModel vendorInvoice = vendorInvoiceGeneralModelRep
        .findOneByPurchaseOrderCodeAndInvoiceTypeAndAndCurrentStatesLike(poCode,
                invoiceType,
            "%" + DObVendorInvoiceStateMachine.POSTED_STATE + "%");
    if (vendorInvoice == null) {
      return null;
    }
    return vendorInvoice.getId();
  }

  private String getVendorInvoiceType(String poType) {
    if(poType.equals(OrderTypeEnum.Values.IMPORT_PO))
      return VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE;
    if(poType.equals(OrderTypeEnum.Values.LOCAL_PO))
      return VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE;
    throw new IllegalArgumentException("invalid Purchase Order Type");
  }

  private DObLandedCost createLandedCost(DObLandedCostCreationValueObject valueObject)
      throws Exception {
    DObLandedCost landedCost = new DObLandedCost();
    String userCode = codeGenerator.generateUserCode(DObLandedCost.class.getSimpleName());
    landedCost.setUserCode(userCode);
    commandUtils.setCreationAndModificationInfoAndDate(landedCost);
    stateMachineFactory.createStateMachine(landedCost);
    landedCost.setTypeId(getLandedCostTypeId(valueObject.getLandedCostType()));
    landedCost.setDocumentOwnerId(valueObject.getDocumentOwnerId());
    return landedCost;
  }

  private Long getLandedCostTypeId(String typeCode) {
    CObLandedCostType landedCostType = landedCostTypeRep.findOneByUserCode(typeCode);
    return landedCostType.getId();
  }

  private IObLandedCostCompanyData createCompanyData(DObPurchaseOrderGeneralModel purchaseOrderGM) {
    IObLandedCostCompanyData landedCostCompany = new IObLandedCostCompanyData();
    commandUtils.setCreationAndModificationInfoAndDate(landedCostCompany);
    landedCostCompany.setCompanyId(getCompanyId(purchaseOrderGM.getUserCode()));
    landedCostCompany.setPurchaseUnitId(getBusinessUnitId(purchaseOrderGM));
    return landedCostCompany;
  }

  private Long getBusinessUnitId(DObPurchaseOrderGeneralModel purchaseOrderGeneralModel) {
    CObPurchasingUnit purchasingUnit =
        purchasingUnitRep.findOneByUserCode(purchaseOrderGeneralModel.getPurchaseUnitCode());
    return purchasingUnit.getId();
  }

  private Long getCompanyId(String poCode) {
      IObOrderCompanyGeneralModel poCompanyGM = poCompanyGMRep.findByUserCode(poCode);
    CObCompany company = companyRep.findOneByUserCode(poCompanyGM.getCompanyCode());
    return company.getId();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCodeGenerator(DocumentObjectCodeGenerator codeGenerator) {
    this.codeGenerator = codeGenerator;
  }

  public void setLandedCostTypeRep(CObLandedCostTypeRep landedCostTypeRep) {
    this.landedCostTypeRep = landedCostTypeRep;
  }

  public void setLandedCostRep(DObLandedCostRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }

    public void setPoCompanyGMRep(IObOrderCompanyGeneralModelRep poCompanyGMRep) {
    this.poCompanyGMRep = poCompanyGMRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setPurchaseOrderGMRep(DObPurchaseOrderGeneralModelRep purchaseOrderGMRep) {
    this.purchaseOrderGMRep = purchaseOrderGMRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }

}
