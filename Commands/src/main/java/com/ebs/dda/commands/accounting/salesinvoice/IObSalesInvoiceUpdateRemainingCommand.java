package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class IObSalesInvoiceUpdateRemainingCommand
    implements ICommand<DObCollectionUpdateRefDocumentRemainingValueObject> {

  private DObSalesInvoiceRep salesInvoiceRep;
  private IObSalesInvoiceSummaryRep summaryRep;
  private DObCollectionGeneralModelRep collectionGMRep;
  private CommandUtils commandUtils;

  public IObSalesInvoiceUpdateRemainingCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      DObCollectionUpdateRefDocumentRemainingValueObject valueObject) throws Exception {

    String salesInvoiceCode = valueObject.getRefDocumentCode();
    DObSalesInvoice salesInvoice = salesInvoiceRep.findOneByUserCode(salesInvoiceCode);
    IObSalesInvoiceSummary summary = summaryRep.findOneByRefInstanceId(salesInvoice.getId());

    BigDecimal collectionAmount = getCollectionAmount(valueObject.getCollectionCode());
    summary.setRemaining(summary.getRemaining().subtract(collectionAmount));

    commandUtils.setModificationInfoAndModificationDate(summary);
    commandUtils.setModificationInfoAndModificationDate(salesInvoice);

    salesInvoice.setHeaderDetail(IDObSalesInvoice.summary, summary);

    salesInvoiceRep.update(salesInvoice);

    return Observable.just(salesInvoiceCode);
  }

  private BigDecimal getCollectionAmount(String collectionCode) {
    DObCollectionGeneralModel collection = collectionGMRep.findOneByUserCode(collectionCode);
    return collection.getAmount();
  }

  public void setSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
    this.salesInvoiceRep = salesInvoiceRep;
  }

  public void setSummaryRep(IObSalesInvoiceSummaryRep summaryRep) {
    this.summaryRep = summaryRep;
  }

  public void setCollectionGMRep(DObCollectionGeneralModelRep collectionGMRep) {
    this.collectionGMRep = collectionGMRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }


}
