package com.ebs.dda.commands.accounting.electronicinvoice;

import com.ebs.dda.accounting.electronicinvoice.EInvoiceData;
import com.ebs.dda.accounting.electronicinvoice.EInvoiceDataFactory;
import com.ebs.dda.accounting.electronicinvoice.EInvoiceTaxAuthorityClient;
import com.ebs.dda.commands.apis.ICommand;
import com.google.gson.Gson;
import io.reactivex.Observable;
import lombok.Setter;

@Setter
public class EInvoiceSubmitCommand<T extends EInvoiceData> implements ICommand<String> {

    private final Class<T> eInvoiceClass;
    private EInvoiceDataFactory eInvoiceDataFactory;
    private EInvoiceTaxAuthorityClient taxAuthorityClient;

    public EInvoiceSubmitCommand(Class<T> eInvoiceClass) {
        this.eInvoiceClass = eInvoiceClass;
    }

    @Override
    public Observable executeCommand(String userCode) throws Exception {

        EInvoiceData eInvoiceData = createEInvoice(userCode);
        String eInvoiceJSON = toJson(eInvoiceData);
        // TODO: Sign eInvoiceData json by Dongle client
        taxAuthorityClient.submit(eInvoiceJSON, eInvoiceClass);// TODO: submit eInvoiceData json by TaxAuthority client

        return Observable.just(userCode);
    }

    private EInvoiceData createEInvoice(String userCode) {
        return eInvoiceDataFactory.createInvoiceData(userCode);
    }

    private String toJson(EInvoiceData eInvoiceData) {
        Gson gson = new Gson();
        return gson.toJson(eInvoiceData, eInvoiceClass);
    }

}
