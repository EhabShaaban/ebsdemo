package com.ebs.dda.commands.masterdata.itemvendorrecord;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordValueObject;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;

public class ItemVendorRecordCreateCommand implements ICommand<ItemVendorRecordValueObject> {
  private MObItemRep itemRep;
  private MObVendorRep vendorRep;
  private CObCurrencyRep currencyRep;
  private ItemVendorRecordRep itemVendorRecordRep;
  private CObPurchasingUnitRep purchaseUnitRep;
  private CObMeasureRep measureRep;
  private IObAlternativeUoMGeneralModelRep alternativeUoMRep;
  private final MasterDataCodeGenrator masterDataCodeGenrator;
  private final CommandUtils commandUtils;

  public ItemVendorRecordCreateCommand(MasterDataCodeGenrator masterDataCodeGenrator){
    this.masterDataCodeGenrator = masterDataCodeGenrator;
    this.commandUtils = new CommandUtils();
  }

  public Observable<String> executeCommand(
      ItemVendorRecordValueObject itemVendorRecordValueObject) throws Exception {
    String userCode = masterDataCodeGenrator.generateUserCode(ItemVendorRecord.class);
    MObItem item = itemRep.findOneByUserCode(itemVendorRecordValueObject.getItemCode());
    Long vendorId =
        vendorRep.findOneByUserCode(itemVendorRecordValueObject.getVendorCode()).getId();
    Long purchaseUnitId =
        purchaseUnitRep
            .findOneByUserCode(itemVendorRecordValueObject.getPurchaseUnitCode())
            .getId();
    ItemVendorRecord itemVendorRecord = new ItemVendorRecord();
    itemVendorRecord.setUserCode(userCode);
    itemVendorRecord.setItemId(item.getId());
    itemVendorRecord.setVendorId(vendorId);
    setCurrencyId(itemVendorRecord, itemVendorRecordValueObject.getCurrencyCode());
    itemVendorRecord.setItemVendorCode(itemVendorRecordValueObject.getItemVendorCode());
    itemVendorRecord.setPrice(itemVendorRecordValueObject.getPrice());
    itemVendorRecord.setPurchaseUnitId(purchaseUnitId);
    Long unitOfMeasureId = getUnitOfMeasureId(itemVendorRecordValueObject);
    itemVendorRecord.setUomId(unitOfMeasureId);
    if (!unitOfMeasureId.equals(item.getBasicUnitOfMeasure())) {
      itemVendorRecord.setAlternativeUomId(
          getAlternativeUnitOfMeasureId(itemVendorRecordValueObject));
    }
    itemVendorRecord.setOldItemNumber(itemVendorRecordValueObject.getOldItemNumber());
    this.commandUtils.setCreationAndModificationInfoAndDate(itemVendorRecord);

    itemVendorRecordRep.create(itemVendorRecord);
    return Observable.just(userCode);
  }

  private Long getUnitOfMeasureId(ItemVendorRecordValueObject itemVendorRecordValueObject) {
    CObMeasure measure =
        measureRep.findOneByUserCode(itemVendorRecordValueObject.getUnitOfMeasureCode());
    return measure.getId();
  }

  private Long getAlternativeUnitOfMeasureId(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {
    IObAlternativeUoMGeneralModel alternativeUoM =
        alternativeUoMRep.findByItemCodeAndAlternativeUnitOfMeasureCode(
            itemVendorRecordValueObject.getItemCode(),
            itemVendorRecordValueObject.getUnitOfMeasureCode());
    return alternativeUoM.getId();
  }

  private void setCurrencyId(ItemVendorRecord itemVendorRecord, String currencyCode) {
    if (currencyCode != null) {
      Long currencyId = currencyRep.findOneByUserCode(currencyCode).getId();
      itemVendorRecord.setCurrencyId(currencyId);
    }
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setItemVendorRecordRep(ItemVendorRecordRep itemVendorRecordRep) {
    this.itemVendorRecordRep = itemVendorRecordRep;
  }

  public void setPurchaseUnitRep(CObPurchasingUnitRep purchaseUnitRep) {
    this.purchaseUnitRep = purchaseUnitRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setAlternativeUoMRep(IObAlternativeUoMGeneralModelRep alternativeUoMRep) {
    this.alternativeUoMRep = alternativeUoMRep;
  }
}
