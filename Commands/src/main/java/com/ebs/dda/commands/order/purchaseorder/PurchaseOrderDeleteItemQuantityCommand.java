package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import lombok.SneakyThrows;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class PurchaseOrderDeleteItemQuantityCommand {

  private final CommandUtils commandUtils;
  private IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  private IObOrderLineDetailsRep orderLineDetailsRep;
  private DObPurchaseOrderRep orderRep;
  private PlatformTransactionManager transactionManager;

  public PurchaseOrderDeleteItemQuantityCommand() {
    commandUtils = new CommandUtils();
  }

  public void setOrderLineDetailsQuantitiesRep(
      IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep) {
    this.orderLineDetailsQuantitiesRep = orderLineDetailsQuantitiesRep;
  }

  public void setOrderLineDetailsRep(IObOrderLineDetailsRep orderLineDetailsRep) {
    this.orderLineDetailsRep = orderLineDetailsRep;
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }

  public void setOrderLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void executeCommand(String orderCode, String itemCode, Long quantityId) throws Exception {
    checkIfItemExists(orderCode, itemCode);
    checkIfQuantityExists(quantityId);
    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @SneakyThrows
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            IObOrderLineDetailsQuantities quantity =
                orderLineDetailsQuantitiesRep.findOneById(quantityId);
            Long orderLineId = quantity.getRefInstanceId();
            orderLineDetailsQuantitiesRep.delete(quantity);

            updateItemsAndPurchaseOrder(orderLineId, orderCode);
          }
        });
  }

  private void updateItemsAndPurchaseOrder(Long orderLineId, String orderCode) throws Exception {
    IObOrderLineDetails orderLineDetails = orderLineDetailsRep.findOneById(orderLineId);
    commandUtils.setModificationInfoAndModificationDate(orderLineDetails);

    DObPurchaseOrder purchaseOrder = orderRep.findOneByUserCode(orderCode);
    purchaseOrder.addLineDetail(IDObPurchaseOrder.lineAttr, orderLineDetails);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);

    orderRep.update(purchaseOrder);
  }

  private void checkIfQuantityExists(Long quantityId) throws RecordOfInstanceNotExistException {

    IObOrderLineDetailsQuantities orderLineDetailsQuantities =
        orderLineDetailsQuantitiesRep.findOneById(quantityId);
    if (orderLineDetailsQuantities == null)
      throw new RecordOfInstanceNotExistException(IObOrderLineDetailsQuantities.class.getName());
  }

  private void checkIfItemExists(String orderCode, String itemCode)
      throws RecordOfInstanceNotExistException {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(orderCode, itemCode);
    if (orderLineDetailsGeneralModel == null)
      throw new RecordOfInstanceNotExistException(IObOrderLineDetailsGeneralModel.class.getName());
  }
}
