package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCustomerAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentTaxAccountingDetails;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObSalesInvoiceAccountDeterminationCommand implements ICommand<DObJournalEntryCreateValueObject> {

    private DObSalesInvoiceRep salesInvoiceRep;
    private DObSalesInvoiceJournalEntryPreparationGeneralModelRep salesInvoicePreparationRep;
    private DObSalesInvoiceJournalEntryPreparationGeneralModel salesInvoicePreparation;
    private IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep salesInvoiceItemsPreparationRep;
    private List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> salesInvoiceItemsPreparation;
    private final CommandUtils commandUtils;

    public DObSalesInvoiceAccountDeterminationCommand() {
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(DObJournalEntryCreateValueObject valueObject) throws Exception {
        String salesInvoiceCode = valueObject.getUserCode();
        DObSalesInvoice salesInvoice = salesInvoiceRep.findOneByUserCode(salesInvoiceCode);
        initInvoiceEntities(salesInvoiceCode);
        determineCreditAccount(salesInvoice);
        determineDebitAccount(salesInvoice);
        determineTaxesAccount(salesInvoice);
        salesInvoiceRep.update(salesInvoice);
        return Observable.just(salesInvoiceCode);
    }

    private void initInvoiceEntities(String salesInvoiceCode) {
        salesInvoicePreparation = salesInvoicePreparationRep.findOneByCode(salesInvoiceCode);
        salesInvoiceItemsPreparation = salesInvoiceItemsPreparationRep.findByCode(salesInvoiceCode);
    }

    private void determineDebitAccount(DObSalesInvoice salesInvoice) {
        IObAccountingDocumentCustomerAccountingDetails accountingDetails = new IObAccountingDocumentCustomerAccountingDetails();
        accountingDetails.setObjectTypeCode(IDocumentTypes.SALES_INVOICE);
        accountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
        accountingDetails.setAmount(salesInvoicePreparation.getAmountAfterTaxes());
        accountingDetails.setGlAccountId(salesInvoicePreparation.getCustomerAccountId());
        accountingDetails.setGlSubAccountId(salesInvoicePreparation.getCustomerId());

        commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);
        salesInvoice.addLineDetail(IDObAccountingDocument.accountingCustomerDetailsAttr, accountingDetails);
    }

    private void determineCreditAccount(DObSalesInvoice salesInvoice) {
        IObAccountingDocumentAccountingDetails accountingDetails = new IObAccountingDocumentAccountingDetails();
        accountingDetails.setObjectTypeCode(IDocumentTypes.SALES_INVOICE);
        accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
        accountingDetails.setAmount(salesInvoicePreparation.getAmountBeforeTaxes());
        accountingDetails.setGlAccountId(salesInvoicePreparation.getSalesAccountId());

        commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);
        salesInvoice.addLineDetail(IDObAccountingDocument.accountingDetailsAttr, accountingDetails);
    }

    private void determineTaxesAccount(DObSalesInvoice salesInvoice) {
        for (IObSalesInvoiceJournalEntryItemsPreparationGeneralModel salesInvoiceTaxes : salesInvoiceItemsPreparation) {
            IObAccountingDocumentTaxAccountingDetails accountingDetails = new IObAccountingDocumentTaxAccountingDetails();
            accountingDetails.setObjectTypeCode(IDocumentTypes.SALES_INVOICE);
            accountingDetails.setGlAccountId(salesInvoiceTaxes.getTaxesAccountId());
            accountingDetails.setGlSubAccountId(salesInvoiceTaxes.getTaxId());
            commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);

            if (salesInvoiceTaxes.getTaxAmount().compareTo(BigDecimal.ZERO) < 0) {
                accountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
                accountingDetails.setAmount(salesInvoiceTaxes.getTaxAmount().abs());

            } else {
                accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
                accountingDetails.setAmount(salesInvoiceTaxes.getTaxAmount());
            }
            salesInvoice.addLineDetail(IDObAccountingDocument.accountingDetailsAttr, accountingDetails);
        }
    }

    public void setSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
        this.salesInvoiceRep = salesInvoiceRep;
    }

    public void setSalesInvoicePreparationRep(DObSalesInvoiceJournalEntryPreparationGeneralModelRep salesInvoicePreparationRep) {
        this.salesInvoicePreparationRep = salesInvoicePreparationRep;
    }

    public void setSalesInvoiceItemsPreparationRep(IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep salesInvoiceItemsPreparationRep) {
        this.salesInvoiceItemsPreparationRep = salesInvoiceItemsPreparationRep;
    }

    public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
}
