package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentOrderAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentTreasuryAccountingDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;


public class DObCashPaymentForPurchaseOrderAccountDeterminationCommand extends DObPaymentRequestAccountDeterminationCommand {
    private DObPurchaseOrderRep purchaseOrderRep;
    private LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;


    @Override
    protected void determineCreditAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
        String creditAccount = ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT;
        LObGlobalGLAccountConfigGeneralModel creditGlobalGLAccountAccount = globalAccountConfigGeneralModelRep
                .findOneByUserCode(creditAccount);

        IObAccountingDocumentTreasuryAccountingDetails creditAccountingDetails = new IObAccountingDocumentTreasuryAccountingDetails();
        creditAccountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
        creditAccountingDetails.setGlAccountId(creditGlobalGLAccountAccount.getGlAccountId());
        creditAccountingDetails.setObjectTypeCode(IDocumentTypes.PAYMENT_REQUEST);
        creditAccountingDetails.setGlSubAccountId(paymentDetails.getTreasuryAccountId());
        creditAccountingDetails.setAmount(paymentDetails.getNetAmount());

        commandUtils.setCreationAndModificationInfoAndDate(creditAccountingDetails);
        paymentRequest.addLineDetail(IDObPaymentRequest.accountingDetailsAttr, creditAccountingDetails);
    }

    @Override
    protected void determineDebitAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
        DObPurchaseOrder order = purchaseOrderRep.findOneByUserCode(paymentDetails.getDueDocumentCode());
        LObGlobalGLAccountConfigGeneralModel purchaseOrderAccount = globalAccountConfigGeneralModelRep
                .findOneByUserCode(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);

        IObAccountingDocumentOrderAccountingDetails debitAccountingDetails = new IObAccountingDocumentOrderAccountingDetails();
        debitAccountingDetails.setObjectTypeCode(IDocumentTypes.PAYMENT_REQUEST);
        debitAccountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
        debitAccountingDetails.setGlAccountId(purchaseOrderAccount.getGlAccountId());
        debitAccountingDetails.setGlSubAccountId(order.getId());
        debitAccountingDetails.setAmount(paymentDetails.getNetAmount());

        commandUtils.setCreationAndModificationInfoAndDate(debitAccountingDetails);
        paymentRequest.addLineDetail(IDObPaymentRequest.accountingDetailsAttr, debitAccountingDetails);
    }

    public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
        this.purchaseOrderRep = purchaseOrderRep;
    }

    public void setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
        this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
    }
}
