package com.ebs.dda.commands.accounting.paymentrequest;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;

@Component
public class DObPaymentCreateCommandFactory implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	public DObPaymentRequestCreateCommand getCommandInstance(
					DueDocumentTypeEnum.DueDocumentType dueDocumentType,
					PaymentTypeEnum.PaymentType paymentType) throws Exception {
		if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR)) {
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE)) {
				return applicationContext
								.getBean(DObPaymentAgainstVendorInvoiceCreateCommand.class);
			}
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)) {
				return applicationContext.getBean(DObDownPaymentCreateCommand.class);
			}
		}
		if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)) {
			return applicationContext.getBean(DObGeneralPaymentCreateCommand.class);
		}
		if (paymentType.equals(PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT)) {
			return applicationContext.getBean(DObUnderSettlementPaymentCreateCommand.class);
		}
		if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE)) {
			return applicationContext.getBean(DObPaymentAgainstCreditNoteCreateCommand.class);
		}
		throw new ArgumentViolationSecurityException();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
