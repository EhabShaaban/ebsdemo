package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPaymentDetails;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPaymentDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObServicePurchaseOrderPaymentDetailsValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import io.reactivex.Observable;

public class IObServicePurchaseOrderPaymentDetailsUpdateCommand
    implements ICommand<IObServicePurchaseOrderPaymentDetailsValueObject> {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderPaymentDetailsRep paymentDetailsRep;
  private CObCurrencyRep currencyRep;
  private CObPaymentTermsRep paymentTermsRep;

  public IObServicePurchaseOrderPaymentDetailsUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObServicePurchaseOrderPaymentDetailsValueObject valueObject) throws Exception {

    DObPurchaseOrder purchaseOrder =
        purchaseOrderRep.findOneByUserCode(valueObject.getPurchaseOrderCode());
    IObOrderPaymentDetails paymentDetails =
        getIObServicePurchaseOrderPaymentDetailsWithUpdatedValues(
            purchaseOrder.getId(), valueObject);

    commandUtils.setModificationInfoAndModificationDate(paymentDetails);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.paymentDetailsAttr, paymentDetails);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("Success");
  }

  private IObOrderPaymentDetails getIObServicePurchaseOrderPaymentDetailsWithUpdatedValues(
      Long purchaseOrderId, IObServicePurchaseOrderPaymentDetailsValueObject valueObject) {
    IObOrderPaymentDetails paymentDetails =
        paymentDetailsRep.findOneByRefInstanceId(purchaseOrderId);
    if (paymentDetails == null) {
      paymentDetails = new IObOrderPaymentDetails();
      commandUtils.setCreationAndModificationInfoAndDate(paymentDetails);
    }
    setPaymentTermId(paymentDetails, valueObject.getPaymentTermCode());
    setCurrencyId(paymentDetails, valueObject.getCurrencyCode());
    return paymentDetails;
  }

  private void setPaymentTermId(IObOrderPaymentDetails paymentDetails, String paymentTermCode) {
    if (paymentTermCode == null) {
      paymentDetails.setPaymentTermId(null);
      return;
    }
    CObPaymentTerms paymentTerm = paymentTermsRep.findOneByUserCode(paymentTermCode);
    paymentDetails.setPaymentTermId(paymentTerm.getId());
  }

  private void setCurrencyId(IObOrderPaymentDetails paymentDetails, String currencyCode) {
    if (currencyCode == null) {
      paymentDetails.setCurrencyId(null);
      return;
    }
    CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
    paymentDetails.setCurrencyId(currency.getId());
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setPaymentDetailsRep(IObOrderPaymentDetailsRep paymentDetailsRep) {
    this.paymentDetailsRep = paymentDetailsRep;
  }

  public void setPaymentTermsRep(CObPaymentTermsRep paymentTermsRep) {
    this.paymentTermsRep = paymentTermsRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
