package com.ebs.dda.commands.inventory.stockavailability.strategies;


public interface IUpdateStockAvailabilityStrategy {

  public void updateStockAvailability(String userCode);

}
