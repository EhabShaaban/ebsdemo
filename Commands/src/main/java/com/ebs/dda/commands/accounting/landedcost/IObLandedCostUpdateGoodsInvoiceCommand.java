package com.ebs.dda.commands.accounting.landedcost;

import java.util.List;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import io.reactivex.Observable;

public class IObLandedCostUpdateGoodsInvoiceCommand implements ICommand<String> {

  private IObVendorInvoiceDetailsGeneralModelRep vendorInvoiceRep;
  private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
  private IObLandedCostDetailsRep landedCostDetailsRep;
  private DObLandedCostRep landedCostRep;
  private CommandUtils commandUtils;

  public IObLandedCostUpdateGoodsInvoiceCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(String vendorInvoiceCode) throws Exception {

    IObVendorInvoiceDetailsGeneralModel vendorInvoice =
        vendorInvoiceRep.findByInvoiceCode(vendorInvoiceCode);

    List<IObLandedCostDetailsGeneralModel> landedCostDetailsGeneralModel =
            landedCostDetailsGeneralModelRep.findByPurchaseOrderCode(vendorInvoice.getPurchaseOrderCode());

    for (IObLandedCostDetailsGeneralModel landedCostDetailGeneralModel : landedCostDetailsGeneralModel) {
      IObLandedCostDetails lcDetails =
              landedCostDetailsRep.findOneById(landedCostDetailGeneralModel.getId());
      lcDetails.setVendorInvoiceId(vendorInvoice.getId());
      DObLandedCost landedCost =  landedCostRep.getOne(lcDetails.getRefInstanceId());
      commandUtils.setModificationInfoAndModificationDate(landedCost);
      commandUtils.setModificationInfoAndModificationDate(lcDetails);
      landedCost.setHeaderDetail(IDObLandedCost.landedCostDetails, lcDetails);
      landedCostRep.save(landedCost);
    }

    return Observable.empty();
  }

  public void setVendorInvoiceRep(IObVendorInvoiceDetailsGeneralModelRep vendorInvoiceRep) {
    this.vendorInvoiceRep = vendorInvoiceRep;
  }

  public void setLandedCostDetailsGeneralModelRep(IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep) {
    this.landedCostDetailsGeneralModelRep = landedCostDetailsGeneralModelRep;
  }

  public void setLandedCostRep(DObLandedCostRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }

  public void setLandedCostDetailsRep(IObLandedCostDetailsRep landedCostDetailsRep) {
    this.landedCostDetailsRep = landedCostDetailsRep;
  }

  public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
