package com.ebs.dda.commands.inventory.goodsissue;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.*;
import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import com.ebs.dda.jpa.inventory.inventorydocument.IDObInventoryDocument;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompany;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerAddress;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerContactPerson;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderData;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.CObGoodsIssueRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueSalesInvoiceRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueSalesOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsGeneralModelRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class DObGoodsIssueCreateCommand implements ICommand<DObGoodsIssueCreateValueObject> {

  private DObGoodsIssueSalesInvoiceRep goodsIssueSalesInvoiceRep;
  private DObGoodsIssueSalesOrderRep goodsIssueSalesOrderRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private DObGoodsIssueStateMachine stateMachine;
  private CObGoodsIssueRep cobGoodsIssueRep;
  private MObCustomerRep customerRep;
  private StoreKeeperRep storeKeeperRep;
  private CObStorehouseGeneralModelRep storehouseRep;
  private CObPlantGeneralModelRep plantRep;
  private CObCompanyRep companyRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private DObSalesOrderRep salesOrderRep;
  private IObSalesOrderDataRep salesOrderDataRep;
  private IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep;
  private IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private CObMeasureRep measureRep;
  private IObCustomerContactPersonRep customerContactPersonRep;
  private IObCustomerAddressRep customerAddressRep;

  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private IUserAccountSecurityService userAccountSecurityService;
  private CommandUtils commandUtils;
  private String GI_SI = "DELIVERY_TO_CUSTOMER";
  private String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";

  public DObGoodsIssueCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
          throws Exception {
    commandUtils = new CommandUtils();
    stateMachine = new DObGoodsIssueStateMachine();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject) throws Exception {
    String userCode =
            documentObjectCodeGenerator.generateUserCode(DObGoodsIssue.class.getSimpleName());
    if (goodsIssueCreateValueObject.getTypeCode().equals(GI_SI)) {
      handleGoodsIssueSalesInvoice(goodsIssueCreateValueObject, userCode);
    } else {
      handleGoodsIssueSalesOrder(goodsIssueCreateValueObject, userCode);
    }
    return Observable.just(userCode);
  }

  private void handleGoodsIssueSalesOrder(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject, String userCode)
          throws Exception {

    DObGoodsIssueSalesOrder goodsIssueSalesOrder = new DObGoodsIssueSalesOrder();
    setGoodsIssueSalesOrder(goodsIssueCreateValueObject, goodsIssueSalesOrder, userCode);
    IObGoodsIssueSalesOrderData goodsIssueSalesOrderData = new IObGoodsIssueSalesOrderData();
    List<IObGoodsIssueSalesOrderItem> goodsIssueSalesOrderItems = new ArrayList<>();
    DObSalesOrderGeneralModel salesOrderGeneralModel =
            salesOrderGeneralModelRep.findOneByUserCode(
                    goodsIssueCreateValueObject.getRefDocumentCode());

    IObInventoryCompany goodsIssueSalesOrderCompany = new IObInventoryCompany();
    setGoodsIssueSalesOrderDataSection(salesOrderGeneralModel, goodsIssueSalesOrderData);
    setGoodsIssueCompany(goodsIssueCreateValueObject, goodsIssueSalesOrderCompany);
    setGoodsIssueSalesOrderItems(salesOrderGeneralModel, goodsIssueSalesOrderItems);

    goodsIssueSalesOrder.setHeaderDetail(
            IDObGoodsIssueSalesOrder.salesOrderRefDocumentAttr, goodsIssueSalesOrderData);
    goodsIssueSalesOrder.setHeaderDetail(
            IDObInventoryDocument.companyAttr, goodsIssueSalesOrderCompany);
    goodsIssueSalesOrder.setLinesDetails(
            IDObGoodsIssueSalesOrder.salesOrderItemsAttr, goodsIssueSalesOrderItems);
    goodsIssueSalesOrderRep.create(goodsIssueSalesOrder);
  }

  private void handleGoodsIssueSalesInvoice(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject, String userCode)
          throws Exception {
    DObGoodsIssueSalesInvoice goodsIssueSalesInvoice = new DObGoodsIssueSalesInvoice();
    setGoodsIssueSalesInvoice(goodsIssueCreateValueObject, goodsIssueSalesInvoice, userCode);
    IObGoodsIssueSalesInvoiceData goodsIssueSalesInvoiceData = new IObGoodsIssueSalesInvoiceData();
    List<IObGoodsIssueSalesInvoiceItem> goodsIssueSalesInvoiceItems = new ArrayList<>();
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
            salesInvoiceGeneralModelRep.findOneByUserCode(
                    goodsIssueCreateValueObject.getRefDocumentCode());

    IObInventoryCompany goodsIssueSalesInvoiceCompany = new IObInventoryCompany();
    setGoodsIssueSalesInvoiceDataSection(salesInvoiceGeneralModel, goodsIssueSalesInvoiceData);
    setGoodsIssueCompany(goodsIssueCreateValueObject, goodsIssueSalesInvoiceCompany);
    setGoodsIssueSalesInvoiceItems(salesInvoiceGeneralModel, goodsIssueSalesInvoiceItems);

    goodsIssueSalesInvoice.setHeaderDetail(
            IDObGoodsIssueSalesInvoice.salesInvoiceRefDocumentAttr, goodsIssueSalesInvoiceData);
    goodsIssueSalesInvoice.setHeaderDetail(
            IDObInventoryDocument.companyAttr, goodsIssueSalesInvoiceCompany);
    goodsIssueSalesInvoice.setLinesDetails(
            IDObGoodsIssueSalesInvoice.salesInvoiceItemsAttr, goodsIssueSalesInvoiceItems);
    goodsIssueSalesInvoiceRep.create(goodsIssueSalesInvoice);
  }

  private void setGoodsIssueSalesInvoice(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject,
          DObGoodsIssueSalesInvoice dObGoodsIssueSalesInvoice,
          String userCode)
          throws Exception {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setCreationAndModificationInfoAndDate(dObGoodsIssueSalesInvoice);
    stateMachine.initObjectState(dObGoodsIssueSalesInvoice);
    dObGoodsIssueSalesInvoice.setUserCode(userCode);
    setPurchaseUnit(dObGoodsIssueSalesInvoice, goodsIssueCreateValueObject.getPurchaseUnitCode());
    setGoodsIssueSalesInvoiceType(
            dObGoodsIssueSalesInvoice, goodsIssueCreateValueObject.getTypeCode());
  }

  private void setGoodsIssueSalesOrder(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject,
          DObGoodsIssueSalesOrder goodsIssueSalesOrder,
          String userCode)
          throws Exception {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setCreationAndModificationInfoAndDate(goodsIssueSalesOrder);
    stateMachine.initObjectState(goodsIssueSalesOrder);
    goodsIssueSalesOrder.setUserCode(userCode);
    setPurchaseUnit(goodsIssueSalesOrder, goodsIssueCreateValueObject.getPurchaseUnitCode());
    setGoodsIssueSalesOrderType(goodsIssueSalesOrder, goodsIssueCreateValueObject.getTypeCode());
  }

  private void setGoodsIssueCompany(
          DObGoodsIssueCreateValueObject goodsIssueCreateValueObject,
          IObInventoryCompany goodsIssueCompany) {
    commandUtils.setCreationAndModificationInfoAndDate(goodsIssueCompany);

    StoreKeeper storeKeeper =
            storeKeeperRep.findOneByCode(goodsIssueCreateValueObject.getStoreKeeperCode());
    goodsIssueCompany.setStoreKeeperId(storeKeeper.getId());

    CObCompany company = companyRep.findOneByUserCode(goodsIssueCreateValueObject.getCompanyCode());
    goodsIssueCompany.setCompanyId(company.getId());

    CObPlantGeneralModel plant =
            plantRep.findOneByCompanyCode(goodsIssueCreateValueObject.getCompanyCode());
    goodsIssueCompany.setPlantId(plant.getId());

    CObStorehouseGeneralModel storehouse =
            storehouseRep.findByUserCodeAndPlantCode(
                    goodsIssueCreateValueObject.getStoreHouseCode(), plant.getUserCode());
    goodsIssueCompany.setStorehouseId(storehouse.getId());
  }

  private void setGoodsIssueSalesInvoiceItems(
          DObSalesInvoiceGeneralModel salesInvoiceGeneralModel,
          List<IObGoodsIssueSalesInvoiceItem> goodsIssueItems) {
    List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItemsGeneralModels =
            salesInvoiceItemsGeneralModelRep.findByInvoiceCode(salesInvoiceGeneralModel.getUserCode());
    for (IObSalesInvoiceItemsGeneralModel salesInvoiceItemsGeneralModel :
            salesInvoiceItemsGeneralModels) {
      IObGoodsIssueSalesInvoiceItem goodsIssueItem = new IObGoodsIssueSalesInvoiceItem();
      commandUtils.setCreationAndModificationInfoAndDate(goodsIssueItem);
      MObItemGeneralModel item =
              itemGeneralModelRep.findByUserCode(salesInvoiceItemsGeneralModel.getItemCode());
      goodsIssueItem.setItemId(item.getId());
      CObMeasure measure =
              measureRep.findOneByUserCode(salesInvoiceItemsGeneralModel.getOrderUnitCode());
      goodsIssueItem.setUnitOfMeasureId(measure.getId());
      goodsIssueItem.setPrice(salesInvoiceItemsGeneralModel.getOrderUnitPrice());
      goodsIssueItem.setQuantity(salesInvoiceItemsGeneralModel.getQuantityInOrderUnit());

      goodsIssueItems.add(goodsIssueItem);
    }
  }

  private void setGoodsIssueSalesOrderItems(
          DObSalesOrderGeneralModel salesOrderGeneralModel,
          List<IObGoodsIssueSalesOrderItem> goodsIssueItems) {
    List<IObSalesOrderItemGeneralModel> salesOrderItemGeneralModels =
            salesOrderItemsGeneralModelRep.findBySalesOrderCodeOrderByIdDesc(
                    salesOrderGeneralModel.getUserCode());
    for (IObSalesOrderItemGeneralModel salesOrderItemGeneralModel : salesOrderItemGeneralModels) {

      if (salesOrderItemGeneralModel.getItemTypeCode().equals(COMMERCIAL_ITEM_TYPE)) {
        IObGoodsIssueSalesOrderItem goodsIssueItem = new IObGoodsIssueSalesOrderItem();
        commandUtils.setCreationAndModificationInfoAndDate(goodsIssueItem);
        MObItemGeneralModel item =
                itemGeneralModelRep.findByUserCode(salesOrderItemGeneralModel.getItemCode());
        goodsIssueItem.setItemId(item.getId());
        CObMeasure measure =
                measureRep.findOneByUserCode(salesOrderItemGeneralModel.getUnitOfMeasureCode());
        goodsIssueItem.setUnitOfMeasureId(measure.getId());
        goodsIssueItem.setPrice(salesOrderItemGeneralModel.getSalesPrice());
        goodsIssueItem.setQuantity(salesOrderItemGeneralModel.getQuantity());

        goodsIssueItems.add(goodsIssueItem);
      }
    }
  }

  private void setGoodsIssueSalesInvoiceDataSection(
          DObSalesInvoiceGeneralModel salesInvoiceGeneralModel,
          IObGoodsIssueSalesInvoiceData goodsIssueSalesInvoiceData) {
    commandUtils.setCreationAndModificationInfoAndDate(goodsIssueSalesInvoiceData);
    IBDKUser loggedInUserInfo = userAccountSecurityService.getLoggedInUser();
    String customerCode = salesInvoiceGeneralModel.getCustomerCode();
    MObCustomer customer = customerRep.findOneByUserCode(customerCode);
    IObCustomerContactPerson customerContactPerson =
            customerContactPersonRep.findOneByRefInstanceId(customer.getId());

    IObCustomerAddress customerAddress =
            customerAddressRep.findOneByRefInstanceId(customer.getId());
    goodsIssueSalesInvoiceData.setCustomerId(customer.getId());
    goodsIssueSalesInvoiceData.setSalesRepresentativeId(loggedInUserInfo.getUserId());
    goodsIssueSalesInvoiceData.setContactPersonId(customerContactPerson.getContactPersonId());
    goodsIssueSalesInvoiceData.setAddressId(customerAddress.getAddressId());
    goodsIssueSalesInvoiceData.setSalesInvoiceId(salesInvoiceGeneralModel.getId());
  }

  private void setGoodsIssueSalesOrderDataSection(
          DObSalesOrderGeneralModel salesOrderGeneralModel,
          IObGoodsIssueSalesOrderData goodsIssueSalesOrderData) {
    IObSalesOrderData salesOrderData =
            salesOrderDataRep.findOneByRefInstanceId(salesOrderGeneralModel.getId());
    DObSalesOrder salesOrder =
            salesOrderRep.findOneByUserCode(salesOrderGeneralModel.getUserCode());
    commandUtils.setCreationAndModificationInfoAndDate(goodsIssueSalesOrderData);
    String customerCode = salesOrderGeneralModel.getCustomerCode();
    MObCustomer customer = customerRep.findOneByUserCode(customerCode);
    IObCustomerContactPerson customerContactPerson =
            customerContactPersonRep.findOneById(salesOrderData.getContactPersonId());

    IObCustomerAddress customerAddress =
            customerAddressRep.findOneById(salesOrderData.getCustomerAddressId());
    goodsIssueSalesOrderData.setCustomerId(customer.getId());
    goodsIssueSalesOrderData.setSalesRepresentativeId(salesOrder.getSalesResponsibleId());
    goodsIssueSalesOrderData.setContactPersonId(customerContactPerson.getContactPersonId());
    goodsIssueSalesOrderData.setAddressId(customerAddress.getAddressId());
    goodsIssueSalesOrderData.setSalesOrderId(salesOrderGeneralModel.getId());
  }

  private void setPurchaseUnit(DObGoodsIssue goodsIssue, String purchaseUnitCode) {
    CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
    goodsIssue.setPurchaseUnitId(purchaseUnit.getId());
  }

  private void setGoodsIssueSalesInvoiceType(
          DObGoodsIssueSalesInvoice dObGoodsIssueSalesInvoice, String goodsIssueTypeCode) {
    CObGoodsIssue CobGoodsIssue = cobGoodsIssueRep.findOneByUserCode(goodsIssueTypeCode);
    dObGoodsIssueSalesInvoice.setTypeId(CobGoodsIssue.getId());
  }

  private void setGoodsIssueSalesOrderType(
          DObGoodsIssueSalesOrder goodsIssueSalesOrder, String goodsIssueTypeCode) {
    CObGoodsIssue CobGoodsIssue = cobGoodsIssueRep.findOneByUserCode(goodsIssueTypeCode);
    goodsIssueSalesOrder.setTypeId(CobGoodsIssue.getId());
  }

  public void setGoodsIssueSalesInvoiceRep(DObGoodsIssueSalesInvoiceRep goodsIssueSalesInvoiceRep) {
    this.goodsIssueSalesInvoiceRep = goodsIssueSalesInvoiceRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setCobGoodsIssueRep(CObGoodsIssueRep cobGoodsIssueRep) {
    this.cobGoodsIssueRep = cobGoodsIssueRep;
  }

  public void setMObCustomerRep(MObCustomerRep mObCustomerRep) {
    this.customerRep = mObCustomerRep;
  }

  public void setStoreKeeperRep(StoreKeeperRep storeKeeperRep) {
    this.storeKeeperRep = storeKeeperRep;
  }

  public void setPlantRep(CObPlantGeneralModelRep plantRep) {
    this.plantRep = plantRep;
  }

  public void setStorehouseRep(CObStorehouseGeneralModelRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setSalesInvoiceGeneralModelRep(
          DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setSalesInvoiceItemsGeneralModelRep(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
    this.salesInvoiceItemsGeneralModelRep = salesInvoiceItemsGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setCustomerContactPersonRep(IObCustomerContactPersonRep customerContactPersonRep) {
    this.customerContactPersonRep = customerContactPersonRep;
  }

  public void setCustomerAddressRep(IObCustomerAddressRep customerAddressRep) {
    this.customerAddressRep = customerAddressRep;
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setSalesOrderItemsGeneralModelRep(
          IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep) {
    this.salesOrderItemsGeneralModelRep = salesOrderItemsGeneralModelRep;
  }

  public void setGoodsIssueSalesOrderRep(DObGoodsIssueSalesOrderRep goodsIssueSalesOrderRep) {
    this.goodsIssueSalesOrderRep = goodsIssueSalesOrderRep;
  }

  public void setSalesOrderDataRep(IObSalesOrderDataRep salesOrderDataRep) {
    this.salesOrderDataRep = salesOrderDataRep;
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }
}
