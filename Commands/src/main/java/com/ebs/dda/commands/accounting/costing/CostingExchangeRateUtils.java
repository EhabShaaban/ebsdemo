package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CostingExchangeRateUtils {

  private final DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private final DObPaymentRequestCurrencyPriceGeneralModelRep paymentCurrencyPriceGeneralModelRep;
  private final CObCompanyGeneralModelRep companyGeneralModelRep;
  private final CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep;
  private IObVendorInvoiceSummary vendorInvoiceSummary;

  public CostingExchangeRateUtils(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
      DObPaymentRequestCurrencyPriceGeneralModelRep paymentCurrencyPriceGeneralModelRep,
      CObCompanyGeneralModelRep companyGeneralModelRep,
      CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
    this.exchangeRateGeneralModelRep = exchangeRateGeneralModelRep;
    this.companyGeneralModelRep = companyGeneralModelRep;
    this.paymentCurrencyPriceGeneralModelRep = paymentCurrencyPriceGeneralModelRep;
  }

  DObVendorInvoiceGeneralModel getVendorInvoice(String vendorInvoiceCode) {
    return vendorInvoiceGeneralModelRep.findOneByPurchaseOrderCode(vendorInvoiceCode);
  }

  List<DObPaymentRequestCurrencyPriceGeneralModel>
      constructDObPaymentRequestCurrencyPriceGeneralModels(
          DObGoodsReceiptGeneralModel goodsReceipt, DObVendorInvoiceGeneralModel vendorInvoice) {

    List<DObPaymentRequestCurrencyPriceGeneralModel> payments = new ArrayList<>();
    payments.addAll(getPurchaseOrderPayments(goodsReceipt));
    payments.addAll(getVendorInvoicePayments(vendorInvoice));
    return payments;
  }

  private List<DObPaymentRequestCurrencyPriceGeneralModel> getVendorInvoicePayments(
      DObVendorInvoiceGeneralModel vendorInvoice) {
    return paymentCurrencyPriceGeneralModelRep.findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
        vendorInvoice.getUserCode(), DueDocumentTypeEnum.DueDocumentType.INVOICE.name(), PaymentType.OTHER_PARTY_FOR_PURCHASE.name());
  }

  private CObCompanyGeneralModel getCompany(String companyCode) {
    return companyGeneralModelRep.findOneByUserCode(companyCode);
  }

  private List<DObPaymentRequestCurrencyPriceGeneralModel> getPurchaseOrderPayments(
      DObGoodsReceiptGeneralModel goodsReceipt) {
    return paymentCurrencyPriceGeneralModelRep.findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
        goodsReceipt.getRefDocumentCode(),
        DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name(),PaymentType.OTHER_PARTY_FOR_PURCHASE.name());
  }

  BigDecimal getPaymentsTotalAmountInLocalCurrency(
      List<DObPaymentRequestCurrencyPriceGeneralModel> payments) {
    return payments.stream()
        .map(payment -> payment.getNetAmount().multiply(payment.getCurrencyPrice()))
        .reduce(BigDecimal::add)
        .get();
  }

  BigDecimal getLatestExchangeRate(DObVendorInvoiceGeneralModel vendorInvoice, String companyCode) {
    CObCompanyGeneralModel companyGeneralModel = getCompany(companyCode);

    CObExchangeRateGeneralModel exchangeRate =
        getExchangeRateModel(vendorInvoice.getCurrencyISO(), companyGeneralModel.getCurrencyIso());
    return exchangeRate.getSecondValue();
  }

  private CObExchangeRateGeneralModel getExchangeRateModel(
      String vendorInvoiceCurrencyISO, String companyCurrencyISO) {
    return exchangeRateGeneralModelRep.findLatestDefaultExchangeRate(
        vendorInvoiceCurrencyISO, companyCurrencyISO);
  }

  BigDecimal getVendorInvoiceTotalAmount() {
    return vendorInvoiceSummary.getTotalAmount();
  }

  BigDecimal getVendorInvoiceRemaining() {
    return vendorInvoiceSummary.getRemaining();
  }

  public void setVendorInvoiceSummary(IObVendorInvoiceSummary vendorInvoiceSummary) {
    this.vendorInvoiceSummary = vendorInvoiceSummary;
  }
}
