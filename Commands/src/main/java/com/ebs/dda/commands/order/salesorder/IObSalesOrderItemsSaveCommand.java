package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItem;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemValueObject;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;

public class IObSalesOrderItemsSaveCommand implements ICommand<IObSalesOrderItemValueObject> {

  private CommandUtils commandUtils;
  private DObSalesOrderRep salesOrderRep;
  private MObItemRep itemRep;
  private CObMeasureRep measureRep;

  public IObSalesOrderItemsSaveCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObSalesOrderItemValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());

    IObSalesOrderItem salesOrderItem = prepareSalesOrderItem(valueObject);

    updateObjectsInfo(salesOrder, salesOrderItem);

    salesOrder.addLineDetail(IDObSalesOrder.salesOrderItemsAttr, salesOrderItem);
    salesOrderRep.update(salesOrder);

    return Observable.just("Success");
  }

  private IObSalesOrderItem prepareSalesOrderItem(IObSalesOrderItemValueObject valueObject) {
    IObSalesOrderItem salesOrderItem = new IObSalesOrderItem();

    salesOrderItem.setItemId(getItemId(valueObject));
    salesOrderItem.setUnitOfMeasureId(getUnitOfMeasureId(valueObject));
    salesOrderItem.setQuantity(valueObject.getQuantity());
    salesOrderItem.setSalesPrice(valueObject.getSalesPrice());

    return salesOrderItem;
  }

  private void updateObjectsInfo(DObSalesOrder salesOrder, IObSalesOrderItem salesOrderItem) {
    commandUtils.setCreationAndModificationInfoAndDate(salesOrderItem);
    commandUtils.setModificationInfoAndModificationDate(salesOrder);
  }

  private Long getUnitOfMeasureId(IObSalesOrderItemValueObject valueObject) {
    String uomCode = valueObject.getUnitOfMeasureCode();
    return measureRep.findOneByUserCode(uomCode).getId();
  }

  private Long getItemId(IObSalesOrderItemValueObject valueObject) {
    String itemCode = valueObject.getItemCode();
    return itemRep.findOneByUserCode(itemCode).getId();
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
