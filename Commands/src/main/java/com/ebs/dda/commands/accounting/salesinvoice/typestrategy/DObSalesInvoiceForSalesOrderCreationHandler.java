package com.ebs.dda.commands.accounting.salesinvoice.typestrategy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.IObInvoiceItem;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.order.salesorder.*;

class DObSalesInvoiceForSalesOrderCreationHandler extends DObSalesInvoiceCreationHandler {

    private Long salesOrderId;

    public DObSalesInvoiceForSalesOrderCreationHandler() throws Exception {
        super();
    }

    @Override
    public DObSalesInvoice createSalesInvoice(DObSalesInvoiceCreationValueObject valueObject)
            throws Exception {

        salesOrderId = getSalesOrderIdIfExist(valueObject);
        DObSalesInvoice salesInvoice = super.createSalesInvoice(valueObject);

        List<IObSalesInvoiceItem> salesInvoiceItems =
                copySalesOrderItemsIntoSalesInvoice(valueObject, salesOrderId);
        salesInvoice.setLinesDetails(IDObSalesInvoice.salesInvoiceItemsAttr, salesInvoiceItems);

        IObSalesInvoiceSummary summary = getSummary(salesInvoice.getAllLines(IDObSalesInvoice.salesInvoiceItemsAttr),
                salesInvoice.getAllLines(IDObSalesInvoice.taxesData));
        salesInvoice.setHeaderDetail(IDObSalesInvoice.summary, summary);

        return salesInvoice;
    }

    @Override
    protected String getCompanyCode(DObSalesInvoiceCreationValueObject valueObject) {
        return iObSalesOrderCompanyGeneralModelRep.findOneByRefInstanceId(salesOrderId)
                .getCompanyCode();
    }

    @Override
    protected Long getCompanyId(DObSalesInvoiceCreationValueObject valueObject) {
        IObSalesOrderCompanyStoreData companyStoreData =
                iObSalesOrderCompanyRep.findOneByRefInstanceId(salesOrderId);
        return companyStoreData.getCompanyId();
    }

    @Override
    protected IObSalesInvoiceBusinessPartner getBusinessPartner(
            DObSalesInvoiceCreationValueObject valueObject) {

        IObSalesOrderData salesOrderData = iObSalesOrderDataRep.findOneByRefInstanceId(salesOrderId);

        IObSalesInvoiceCustomerBusinessPartner businessPartner =
                new IObSalesInvoiceCustomerBusinessPartner();
        businessPartner.setSalesOrderid(salesOrderId);
        businessPartner.setCurrencyId(salesOrderData.getCurrencyId());
        businessPartner.setPaymentTermId(salesOrderData.getPaymentTermId());
        businessPartner.setCustomerId(salesOrderData.getCustomerId());
        commandUtils.setCreationAndModificationInfoAndDate(businessPartner);

        return businessPartner;
    }

    @Override
    protected List<IObSalesInvoiceTaxes> getSalesInvoiceTaxes(
            DObSalesInvoiceCreationValueObject valueObject) {

        List<IObSalesInvoiceTaxes> salesInvoiceSalesOrderTaxesDetailsList = new ArrayList<>();

        List<IObSalesOrderTaxGeneralModel> salesOrderTaxsGeneralModel =
                salesOrderTaxGeneralModelRep.findBySalesOrderCodeOrderByCodeAsc(valueObject.getSalesOrderCode());

        for (int i = 0; i < salesOrderTaxsGeneralModel.size(); i++) {
            IObSalesInvoiceTaxes salesInvoiceCompanyTaxesDetails = new IObSalesInvoiceTaxes();
            commandUtils.setCreationAndModificationInfoAndDate(salesInvoiceCompanyTaxesDetails);

            salesInvoiceCompanyTaxesDetails.setName(salesOrderTaxsGeneralModel.get(i).getName());
            salesInvoiceCompanyTaxesDetails.setCode(salesOrderTaxsGeneralModel.get(i).getCode());
            salesInvoiceCompanyTaxesDetails.setAmount(BigDecimal.ZERO);
            salesInvoiceCompanyTaxesDetails.setPercentage(salesOrderTaxsGeneralModel.get(i).getPercentage());
            salesInvoiceSalesOrderTaxesDetailsList.add(salesInvoiceCompanyTaxesDetails);
        }
        return salesInvoiceSalesOrderTaxesDetailsList;

    }

    private Long getSalesOrderIdIfExist(DObSalesInvoiceCreationValueObject valueObject) {
        DObSalesOrderGeneralModel salesOrder =
                dObSalesOrderGeneralModelRep.findOneByUserCode(valueObject.getSalesOrderCode());
        Long salesOrderId = null;
        if (salesOrder != null) {
            salesOrderId = salesOrder.getId();
        }
        return salesOrderId;
    }

    private List<IObSalesInvoiceItem> copySalesOrderItemsIntoSalesInvoice(
            DObSalesInvoiceCreationValueObject valueObject, Long salesOrderId) {

        List<IObSalesOrderItem> salesOrderItems = salesOrderItemsRep.findByRefInstanceId(salesOrderId);

        List<IObSalesInvoiceItem> salesInvoiceItems = new ArrayList<IObSalesInvoiceItem>();
        for (IObSalesOrderItem soItem : salesOrderItems) {
            IObSalesInvoiceItem siItem = new IObSalesInvoiceItem();
            MObItem item = itemRep.findById(soItem.getItemId()).get();
            IObAlternativeUoMGeneralModel alternativeUoMGeneralModel =
                    alternativeUoMGeneralModelRep.findByItemCodeAndAlternativeUnitOfMeasureId(
                            item.getUserCode(), soItem.getUnitOfMeasureId());
            siItem.setItemId(soItem.getItemId());
            siItem.setOrderUnitOfMeasureId(soItem.getUnitOfMeasureId());
            siItem.setQunatityInOrderUnit(soItem.getQuantity());
            siItem.setPrice(soItem.getSalesPrice());
            siItem.setBaseUnitOfMeasureId(item.getBasicUnitOfMeasure());
            siItem.setReturnedQuantity(BigDecimal.ZERO);
            commandUtils.setCreationAndModificationInfoAndDate(siItem);

            salesInvoiceItems.add(siItem);
        }

        return salesInvoiceItems;
    }


    @Override
    protected IObSalesInvoiceSummary getSummary(Collection items, Collection taxes) {
        if (items != null && !items.isEmpty()) {
            IObSalesInvoiceSummary summary = new IObSalesInvoiceSummary();
            commandUtils.setCreationAndModificationInfoAndDate(summary);
            List<IObSalesInvoiceItem> salesInvoiceItems = new ArrayList<>(items);
            BigDecimal remaining = BigDecimal.ZERO;
            for (IObInvoiceItem item : salesInvoiceItems) {
                BigDecimal itemTotal = item.getPrice().multiply(item.getQunatityInOrderUnit());
                remaining = remaining.add(itemTotal);
            }
            remaining = getTotalRemainingAfterTaxes(remaining, taxes);
            summary.setRemaining(remaining);
            return summary;
        }
        return null;
    }

    private BigDecimal getTotalRemainingAfterTaxes(BigDecimal remaining, Collection taxes) {
        BigDecimal total = remaining;
        List<IObSalesInvoiceTaxes> salesInvoiceTaxes = new ArrayList<>(taxes);
        for (IObSalesInvoiceTaxes tax : salesInvoiceTaxes) {
            total = total.add((tax.getPercentage().multiply(remaining)).divide(BigDecimal.valueOf(100)));
        }
        return total;
    }
}
