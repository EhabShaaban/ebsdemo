package com.ebs.dda.commands.accounting.salesinvoice.typestrategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.accounting.salesinvoice.CObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.collectionresponsible.CObCollectionResponsibleRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.*;

public abstract class DObSalesInvoiceCreationHandler {

  protected DObSalesInvoiceRep dObSalesInvoiceRep;
  protected DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep;
  protected CObSalesInvoiceRep cObSalesInvoiceRep;
  protected CObCompanyRep cObCompanyRep;
  protected CObPurchasingUnitRep purchasingUnitRep;
  protected CObCollectionResponsibleRep cObCollectionResponsibleRep;
  protected MObCustomerRep mObCustomerRep;
  protected LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep;
  protected IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep;
  protected final DObSalesInvoiceFactory salesInvoiceFactory;
  protected CommandUtils commandUtils;
  protected IUserAccountSecurityService userAccountSecurityService;
  protected DocumentObjectCodeGenerator documentObjectCodeGenerator;

  protected IObSalesOrderDataRep iObSalesOrderDataRep;
  protected IObSalesOrderCompanyStoreDataRep iObSalesOrderCompanyRep;
  protected IObSalesOrderCompanyAndStoreDataGeneralModelRep iObSalesOrderCompanyGeneralModelRep;
  protected IObSalesOrderItemsRep salesOrderItemsRep;
  protected MObItemRep itemRep;
  protected IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep;


  public DObSalesInvoiceCreationHandler() throws Exception {
    salesInvoiceFactory = new DObSalesInvoiceFactory();
    commandUtils = new CommandUtils();
  }


  public DObSalesInvoice createSalesInvoice(DObSalesInvoiceCreationValueObject valueObject)
      throws Exception {
    DObSalesInvoice salesInvoice = new DObSalesInvoice();

    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObSalesInvoice.class.getSimpleName());

    commandUtils.setCreationAndModificationInfoAndDate(salesInvoice);
    salesInvoice.setTypeId(getInvoiceTypeId(valueObject.getInvoiceTypeCode()));
    salesInvoiceFactory.createSalesInvoiceStateMachine(salesInvoice);

    salesInvoice.setUserCode(userCode);
    salesInvoice.setDocumentOwnerId(valueObject.getDocumentOwnerId());

    IObSalesInvoiceBusinessPartner businessPartner = getBusinessPartner(valueObject);
    IObSalesInvoiceCompanyData companyData = getCompanyData(valueObject);
    List<IObSalesInvoiceTaxes> salesInvoiceTaxes =
        getSalesInvoiceTaxes(valueObject);
    IObSalesInvoiceSummary summary = getSummary(salesInvoice.getAllLines(IDObSalesInvoice.salesInvoiceItemsAttr),
            salesInvoice.getAllLines(IDObSalesInvoice.taxesData));

    salesInvoice.setHeaderDetail(IDObSalesInvoice.businessPartner, businessPartner);
    salesInvoice.setHeaderDetail(IDObSalesInvoice.companyData, companyData);
    salesInvoice.setLinesDetails(IDObSalesInvoice.taxesData, salesInvoiceTaxes);
    salesInvoice.setHeaderDetail(IDObSalesInvoice.summary, summary);

    return salesInvoice;
  }

  protected abstract Long getCompanyId(DObSalesInvoiceCreationValueObject valueObject);

  protected abstract String getCompanyCode(DObSalesInvoiceCreationValueObject valueObject);

  protected abstract List<IObSalesInvoiceTaxes> getSalesInvoiceTaxes(DObSalesInvoiceCreationValueObject valueObject);

  protected abstract IObSalesInvoiceBusinessPartner getBusinessPartner(
      DObSalesInvoiceCreationValueObject valueObject);

  protected IObSalesInvoiceCompanyData getCompanyData(
      DObSalesInvoiceCreationValueObject valueObject) {

    Long businessUnitId = getBusinessUnitId(valueObject.getBusinessUnitCode());
    Long companyId = getCompanyId(valueObject);

    IObSalesInvoiceCompanyData salesInvoiceCompanyData = new IObSalesInvoiceCompanyData();
    salesInvoiceCompanyData.setPurchaseUnitId(businessUnitId);
    salesInvoiceCompanyData.setCompanyId(companyId);
    commandUtils.setCreationAndModificationInfoAndDate(salesInvoiceCompanyData);


    return salesInvoiceCompanyData;
  }

  protected abstract IObSalesInvoiceSummary getSummary(Collection items, Collection texes);

  private Long getBusinessUnitId(String businessUnitCode) {
    CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(businessUnitCode);
    return purchaseUnit.getId();
  }

  private Long getInvoiceTypeId(String invoiceTypeCode) {
    CObSalesInvoice cObSalesInvoice = cObSalesInvoiceRep.findOneByUserCode(invoiceTypeCode);

    return cObSalesInvoice.getId();
  }

  public void setcObSalesInvoiceRep(CObSalesInvoiceRep cObSalesInvoiceRep) {
    this.cObSalesInvoiceRep = cObSalesInvoiceRep;
  }

  public void setcObCompanyRep(CObCompanyRep cObCompanyRep) {
    this.cObCompanyRep = cObCompanyRep;
  }

  public CObPurchasingUnitRep getPurchasingUnitRep() {
    return purchasingUnitRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setcObCollectionResponsibleRep(
      CObCollectionResponsibleRep cObCollectionResponsibleRep) {
    this.cObCollectionResponsibleRep = cObCollectionResponsibleRep;
  }

  public void setmObCustomerRep(MObCustomerRep mObCustomerRep) {
    this.mObCustomerRep = mObCustomerRep;
  }

  public void setCommandUtils(CommandUtils commandUtils) {
    this.commandUtils = commandUtils;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setDocumentObjectCodeGenerator(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  public void setCompanyTaxesGeneralModelRep(
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep) {
    this.companyTaxesGeneralModelRep = companyTaxesGeneralModelRep;
  }

  public void setdObSalesOrderGeneralModelRep(
      DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep) {
    this.dObSalesOrderGeneralModelRep = dObSalesOrderGeneralModelRep;
  }

  public void setiObSalesOrderDataRep(IObSalesOrderDataRep iObSalesOrderDataRep) {
    this.iObSalesOrderDataRep = iObSalesOrderDataRep;
  }

  public void setiObSalesOrderCompanyRep(IObSalesOrderCompanyStoreDataRep iObSalesOrderCompanyRep) {
    this.iObSalesOrderCompanyRep = iObSalesOrderCompanyRep;
  }

  public void setiObSalesOrderCompanyGeneralModelRep(
      IObSalesOrderCompanyAndStoreDataGeneralModelRep iObSalesOrderCompanyGeneralModelRep) {
    this.iObSalesOrderCompanyGeneralModelRep = iObSalesOrderCompanyGeneralModelRep;
  }

  public void setSalesOrderItemsRep(IObSalesOrderItemsRep salesOrderItemsRep) {
    this.salesOrderItemsRep = salesOrderItemsRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setAlternativeUoMGeneralModelRep(
      IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep) {
    this.alternativeUoMGeneralModelRep = alternativeUoMGeneralModelRep;
  }

  public void setSalesOrderTaxGeneralModelRep(IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep) {
    this.salesOrderTaxGeneralModelRep = salesOrderTaxGeneralModelRep;
  }
}
