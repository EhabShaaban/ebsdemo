package com.ebs.dda.commands.inventory.stockavailability.strategies;

import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModelRep;

import java.util.List;

public class UpdateStockAvailabilityByGoodsReceiptStrategy
    implements IUpdateStockAvailabilityStrategy {
  private IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep;
  private UpdateStockAvailabilityService stockAvailabilityUpdateService;

  public void updateStockAvailability(String userCode) {
    List<? extends IObItemQuantityGeneralModel> items = getItemQuantityList(userCode);
    items.forEach(
        item ->
            stockAvailabilityUpdateService.updateOrCreateStockAvailabilityForItem(
                item, item.getQuantity()));
  }

  private List<? extends IObItemQuantityGeneralModel> getItemQuantityList(String userCode) {
    return goodsReceiptItemQuantityGeneralModelRep.findAllByUserCode(userCode);
  }

  public void setGoodsReceiptItemQuantityGeneralModelRep(
      IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep) {
    this.goodsReceiptItemQuantityGeneralModelRep = goodsReceiptItemQuantityGeneralModelRep;
  }

  public void setStockAvailabilityUpdateService(
      UpdateStockAvailabilityService stockAvailabilityUpdateService) {
    this.stockAvailabilityUpdateService = stockAvailabilityUpdateService;
  }
}
