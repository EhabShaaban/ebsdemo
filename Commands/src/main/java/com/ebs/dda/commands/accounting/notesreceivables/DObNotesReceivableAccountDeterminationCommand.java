package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.notesreceivables.factory.NotesReceivablesCreditAccountDeterminationFactory;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentNotesReceivableAccountingDetails;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import io.reactivex.Observable;

import java.util.List;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.NOTES_RECEIVABLES_GL_ACCOUNT;

public class DObNotesReceivableAccountDeterminationCommand implements ICommand<String> {

  private CommandUtils commandUtils;
  private LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;
  private DObMonetaryNotesRep monetaryNotesRep;
  private IObNotesReceivablesReferenceDocumentGeneralModelRep referenceDGMRep;
  private IObMonetaryNotesDetailsGeneralModelRep notesDetailsGMRep;
  private NotesReceivablesCreditAccountDeterminationFactory accountDeterminationFactory;

  public DObNotesReceivableAccountDeterminationCommand() {
    commandUtils = new CommandUtils();
  }

  @Override public Observable<String> executeCommand(String notesReceivableCode) throws Exception {

    DObMonetaryNotes notesReceivable = monetaryNotesRep.findOneByUserCode(notesReceivableCode);
    handleNotesReceivableAccountDetermination(notesReceivable);
    setNotesReceivableData(notesReceivable);
    monetaryNotesRep.update(notesReceivable);

    return Observable.just(notesReceivableCode);
  }

  protected void setNotesReceivableData(DObMonetaryNotes notesReceivable) {
    commandUtils.setModificationInfoAndModificationDate(notesReceivable);
  }

  protected void handleNotesReceivableAccountDetermination(DObMonetaryNotes notesReceivable)
    throws Exception {

    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocGMs =
      referenceDGMRep.findByNotesReceivableCodeOrderByIdAsc(notesReceivable.getUserCode());

    IObMonetaryNotesDetailsGeneralModel notesDetails =
      notesDetailsGMRep.findOneByRefInstanceId(notesReceivable.getId());

    setDebitAccountingDetailRecord(notesReceivable, notesDetails);

    for (IObNotesReceivablesReferenceDocumentGeneralModel referenceDocGM : referenceDocGMs) {
      NotesReceivableCreditAccountDeterminationHandler accountDeterminationHandler =
        accountDeterminationFactory
          .createNotesReceivableCreditAccountDeterminationHandler(referenceDocGM.getDocumentType());
      accountDeterminationHandler
        .setCreditAccountingDetailRecord(referenceDocGM, notesReceivable, notesDetails);
    }
  }

  private void setDebitAccountingDetailRecord(DObMonetaryNotes notesReceivable,
    IObMonetaryNotesDetailsGeneralModel notesDetails) {

    IObAccountingDocumentNotesReceivableAccountingDetails debitAccountingDetail =
      createDebitAccountingDetail(notesDetails);

    commandUtils.setCreationAndModificationInfoAndDate(debitAccountingDetail);
    notesReceivable.addLineDetail(IDObAccountingDocument.accountingNotesReceivableDetailsAttr,
      debitAccountingDetail);
  }

  private IObAccountingDocumentNotesReceivableAccountingDetails createDebitAccountingDetail(
    IObMonetaryNotesDetailsGeneralModel notesDetailsGeneralModel) {
    IObAccountingDocumentNotesReceivableAccountingDetails accountingDetails =
      new IObAccountingDocumentNotesReceivableAccountingDetails();

    accountingDetails.setObjectTypeCode(IDocumentTypes.NOTES_RECEIVABLE);
    accountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
    accountingDetails.setAmount(notesDetailsGeneralModel.getAmount());

    LObGlobalGLAccountConfigGeneralModel globalGLAccount =
      globalAccountConfigGeneralModelRep.findOneByUserCode(NOTES_RECEIVABLES_GL_ACCOUNT);
    accountingDetails.setGlAccountId(globalGLAccount.getGlAccountId());
    accountingDetails.setGlSubAccountId(notesDetailsGeneralModel.getRefInstanceId());

    return accountingDetails;
  }

  public void setUserAccountSecurityService(
    IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setGlobalAccountConfigGeneralModelRep(
    LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
    this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setReferenceDocumentGeneralModelRep(
    IObNotesReceivablesReferenceDocumentGeneralModelRep referenceDocumentGeneralModelRep) {
    this.referenceDGMRep = referenceDocumentGeneralModelRep;
  }

  public void setNotesDetailsGeneralModelRep(
    IObMonetaryNotesDetailsGeneralModelRep notesDetailsGeneralModelRep) {
    this.notesDetailsGMRep = notesDetailsGeneralModelRep;
  }

  public void setNotesReceivablesCreditAccountDeterminationFactory(
    NotesReceivablesCreditAccountDeterminationFactory notesReceivablesCreditAccountDeterminationFactory) {
    this.accountDeterminationFactory = notesReceivablesCreditAccountDeterminationFactory;
  }
}
