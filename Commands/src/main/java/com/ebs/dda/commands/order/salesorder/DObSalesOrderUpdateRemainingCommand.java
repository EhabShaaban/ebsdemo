package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.DocumentObjectUpdateRemainingValueObject;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class DObSalesOrderUpdateRemainingCommand implements ICommand<DocumentObjectUpdateRemainingValueObject> {
    DObSalesOrderRep salesOrderRep;
    CommandUtils commandUtils;

    public DObSalesOrderUpdateRemainingCommand (){
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(DocumentObjectUpdateRemainingValueObject salesOrderUpdateRemainingValueObject) throws Exception {
        DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(salesOrderUpdateRemainingValueObject.getUserCode());
        BigDecimal salesOrderRemaining = salesOrder.getRemaining().subtract(salesOrderUpdateRemainingValueObject.getAmount());
        salesOrder.setRemaining(salesOrderRemaining);
        commandUtils.setModificationInfoAndModificationDate(salesOrder);
        salesOrderRep.update(salesOrder);
        return Observable.just(salesOrder.getUserCode());
    }

    public DObSalesOrderRep getSalesOrderRep() {
        return salesOrderRep;
    }

    public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
        this.salesOrderRep = salesOrderRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
}
