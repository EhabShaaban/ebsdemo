package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderMarkAsArrivedValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsArrivedCommand {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderCycleDatesRep orderCycleDatesRep;

  public DObPurchaseOrderMarkAsArrivedCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setOrderCycleDatesRep(IObOrderCycleDatesRep orderCycleDatesRep) {
    this.orderCycleDatesRep = orderCycleDatesRep;
  }

  public void executeCommand(
      DObPurchaseOrderMarkAsArrivedValueObject purchaseOrderMarkAsArrivedValueObject)
      throws Exception {

    String purchaseOrderCode = purchaseOrderMarkAsArrivedValueObject.getPurchaseOrderCode();

    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    checkIfAllowedAction(purchaseOrder, IPurchaseOrderActionNames.MARK_AS_ARRIVED);

    setPurchaseOrderArrivalState(purchaseOrder);

    IObOrderCycleDates orderCycleDates =
        setOrderArrivalDate(
            purchaseOrder, purchaseOrderMarkAsArrivedValueObject.getArrivalDateTime());

    commandUtils.setModificationInfoAndModificationDate(orderCycleDates);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.cycleDatesAttr, orderCycleDates);
    purchaseOrderRep.update(purchaseOrder);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void setPurchaseOrderArrivalState(DObPurchaseOrder purchaseOrder) throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    if (purchaseOrderStateMachine.fireEvent(IPurchaseOrderActionNames.MARK_AS_ARRIVED)) {
      purchaseOrderStateMachine.save();
    }
  }

  protected void checkIfAllowedAction(DObPurchaseOrder entity, String allowedAction)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(entity);
    purchaseOrderStateMachine.isAllowedAction(allowedAction);
  }

  private IObOrderCycleDates setOrderArrivalDate(
      DObPurchaseOrder purchaseOrder, DateTime arrivalDateTime) {
    IObOrderCycleDates orderCycleDates =
        orderCycleDatesRep.findOneByRefInstanceId(purchaseOrder.getId());
    orderCycleDates.setActualArrivalDate(arrivalDateTime);
    return orderCycleDates;
  }
}
