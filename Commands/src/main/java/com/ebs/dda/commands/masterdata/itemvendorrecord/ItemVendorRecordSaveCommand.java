package com.ebs.dda.commands.masterdata.itemvendorrecord;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import io.reactivex.Observable;

public class ItemVendorRecordSaveCommand
    implements ICommand<ItemVendorRecordGeneralDataValueObject> {

  private ItemVendorRecordRep itemVendorRecordRep;
  private CObCurrencyRep currencyRep;
  private String ivrCode;
  private final CommandUtils commandUtils;

  public ItemVendorRecordSaveCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserCode(String userCode) {
    this.ivrCode = userCode;
  }

  @Override
  public Observable<String> executeCommand(
      ItemVendorRecordGeneralDataValueObject generalDataValueObject) throws Exception {

    ItemVendorRecord itemVendorRecord = constructIVRObject(generalDataValueObject);
    itemVendorRecordRep.updateAndClear(itemVendorRecord);
    return Observable.just("SUCCESS");
  }

  public void setItemVendorRecordRep(ItemVendorRecordRep itemVendorRecordRep) {
    this.itemVendorRecordRep = itemVendorRecordRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  private ItemVendorRecord constructIVRObject(
      ItemVendorRecordGeneralDataValueObject generalDataValueObject) {
    ItemVendorRecord itemVendorRecord = itemVendorRecordRep.findOneByUserCode(this.ivrCode);
    itemVendorRecord.setItemVendorCode(generalDataValueObject.getItemCodeAtVendor());
    addCurrencyFromJson(generalDataValueObject, itemVendorRecord);
    itemVendorRecord.setPrice(generalDataValueObject.getPrice());
    this.commandUtils.setModificationInfoAndModificationDate(itemVendorRecord);
    return itemVendorRecord;
  }

  private void addCurrencyFromJson(
      ItemVendorRecordGeneralDataValueObject generalDataValueObject,
      ItemVendorRecord itemVendorRecord) {
    CObCurrency currency = currencyRep.findOneByUserCode(generalDataValueObject.getCurrencyCode());
    if (currency != null) {
      itemVendorRecord.setCurrencyId(currency.getId());
    } else {
      itemVendorRecord.setCurrencyId(null);
    }
  }
}
