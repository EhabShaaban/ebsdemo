package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;

import java.math.BigDecimal;

public interface ICalculateCostingExchangeRateStrategy {
  BigDecimal calculate(DObGoodsReceiptGeneralModel goodsReceipt);
  void setVendorInvoiceSummary(IObVendorInvoiceSummary vendorInvoiceSummary);
}
