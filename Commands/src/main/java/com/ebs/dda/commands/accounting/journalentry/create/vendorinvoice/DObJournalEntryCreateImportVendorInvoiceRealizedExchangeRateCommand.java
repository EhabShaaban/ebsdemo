package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObVendorInvoiceJournalEntryRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand
    implements ICommand<DObJournalEntryCreateValueObject> {

  private final CommandUtils commandUtils;
  private LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;
  private DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep;
  private DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep
      vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep;

  public DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObJournalEntryCreateValueObject valueObject)
      throws Exception {

    List<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel>
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModels =
            vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep.findByVendorInvoiceCodeAndPaymentType(
                valueObject.getUserCode(), PaymentTypeEnum.PaymentType.VENDOR.name());

    DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry =
        vendorInvoiceJournalEntryRep.findOneByUserCode(valueObject.getJournalEntryCode());

    BigDecimal realizedDifferenceOnExchangeRate =
        getRealizedDifferenceOnExchangeRate(
            vendorInvoiceRealizedExchangeRateDifferenceGeneralModels);

    setRealizedDifferenceOnExchangeRate(
        vendorInvoiceJournalEntry,
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModels,
        realizedDifferenceOnExchangeRate);

    vendorInvoiceJournalEntryRep.update(vendorInvoiceJournalEntry);

    return Observable.just(valueObject.getJournalEntryCode());
  }

  private void setRealizedDifferenceOnExchangeRate(
      DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry,
      List<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel>
          vendorInvoiceRealizedExchangeRateDifferenceGeneralModels,
      BigDecimal realizedDifferenceOnExchangeRate) {

    if (realizedDifferenceOnExchangeRate.signum() == 0) return;

    IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount =
        getCommonVendorJournalEntryItemData(
            vendorInvoiceRealizedExchangeRateDifferenceGeneralModels.get(0));
    IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem =
        getCommonRealizedDifferenceJournalEntryItemData(
            vendorInvoiceRealizedExchangeRateDifferenceGeneralModels.get(0));

    if (realizedDifferenceOnExchangeRate.signum() > 0) {
      setCreditForRealizedExchangeRate(
          realizedDifferenceJournalEntryItem, realizedDifferenceOnExchangeRate);
      setDebitForVendor(journalEntryItemVendorSubAccount, realizedDifferenceOnExchangeRate);
    } else {
      setDebitForRealizedExchangeRate(
          realizedDifferenceJournalEntryItem, realizedDifferenceOnExchangeRate);
      setCreditForVendor(journalEntryItemVendorSubAccount, realizedDifferenceOnExchangeRate);
    }

    vendorInvoiceJournalEntry.addLineDetail(
        IDObJournalEntry.journalItemAttr, journalEntryItemVendorSubAccount);

    vendorInvoiceJournalEntry.addLineDetail(
        IDObJournalEntry.journalItemAttr, realizedDifferenceJournalEntryItem);
  }

  private void setDebitForVendor(
      IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount,
      BigDecimal realizedDifferenceOnExchangeRate) {
    journalEntryItemVendorSubAccount.setCredit(BigDecimal.ZERO);
    journalEntryItemVendorSubAccount.setDebit(realizedDifferenceOnExchangeRate.abs());
  }

  private void setCreditForVendor(
      IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount,
      BigDecimal realizedDifferenceOnExchangeRate) {
    journalEntryItemVendorSubAccount.setDebit(BigDecimal.ZERO);
    journalEntryItemVendorSubAccount.setCredit(realizedDifferenceOnExchangeRate.abs());
  }

  private void setCreditForRealizedExchangeRate(
      IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem,
      BigDecimal vendorInvoiceJournalEntry) {

    realizedDifferenceJournalEntryItem.setCredit(vendorInvoiceJournalEntry.abs());
    realizedDifferenceJournalEntryItem.setDebit(BigDecimal.ZERO);
  }

  private void setDebitForRealizedExchangeRate(
      IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem,
      BigDecimal vendorInvoiceJournalEntry) {

    realizedDifferenceJournalEntryItem.setDebit(vendorInvoiceJournalEntry.abs());
    realizedDifferenceJournalEntryItem.setCredit(BigDecimal.ZERO);
  }

  private BigDecimal getRealizedDifferenceOnExchangeRate(
      List<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel>
          vendorInvoiceRealizedExchangeRateDifferenceGeneralModels) {
    BigDecimal creditValue =
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModels.stream()
            .map(
                item ->
                    item.getPaymentRequestNetAmount()
                        .multiply(item.getVendorInvoiceCurrencyPrice()))
            .reduce(BigDecimal.ZERO, BigDecimal::add);

    BigDecimal debitValue =
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModels.stream()
            .map(item -> item.getPaymentRequestNetAmount().multiply(item.getPaymentCurrencyPrice()))
            .reduce(BigDecimal.ZERO, BigDecimal::add);

    return creditValue.subtract(debitValue);
  }

  private IObJournalEntryItemVendorSubAccount getCommonVendorJournalEntryItemData(
      DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel
          vendorInvoiceRealizedExchangeRateDifferenceGeneralModel) {

    IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount =
        new IObJournalEntryItemVendorSubAccount();
    journalEntryItemVendorSubAccount.setAccountId(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModel.getVendorInvoiceGlAccountId());
    journalEntryItemVendorSubAccount.setDocumentCurrencyId(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModel.getCompanyCurrencyId());
    journalEntryItemVendorSubAccount.setCompanyCurrencyId(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModel.getCompanyCurrencyId());
    journalEntryItemVendorSubAccount.setCompanyCurrencyPrice(BigDecimal.ONE);
    journalEntryItemVendorSubAccount.setSubAccountId(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModel.getVendorInvoiceGlSubAccountId());
    journalEntryItemVendorSubAccount.setType(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModel.getVendorInvoiceAccountLedger());
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemVendorSubAccount);

    return journalEntryItemVendorSubAccount;
  }

  private IObJournalEntryItemGeneral getCommonRealizedDifferenceJournalEntryItemData(
      DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel
          journalDetailPreparationGeneralModel) {

    IObJournalEntryItemGeneral realizedDifferenceJournalEntryItem =
        new IObJournalEntryItemGeneral();
    LObGlobalGLAccountConfigGeneralModel globalGLAccountConfigGeneralModel =
        globalAccountConfigGeneralModelRep.findOneByUserCode(
            ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);
    realizedDifferenceJournalEntryItem.setAccountId(
        globalGLAccountConfigGeneralModel.getGlAccountId());
    realizedDifferenceJournalEntryItem.setCompanyCurrencyPrice(BigDecimal.ONE);
    realizedDifferenceJournalEntryItem.setDocumentCurrencyId(
        journalDetailPreparationGeneralModel.getCompanyCurrencyId());
    realizedDifferenceJournalEntryItem.setCompanyCurrencyId(
        journalDetailPreparationGeneralModel.getCompanyCurrencyId());

    commandUtils.setCreationAndModificationInfoAndDate(realizedDifferenceJournalEntryItem);

    return realizedDifferenceJournalEntryItem;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setGlobalAccountConfigGeneralModelRep(
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
    this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
  }

  public void setVendorInvoiceJournalEntryRep(
      DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep) {
    this.vendorInvoiceJournalEntryRep = vendorInvoiceJournalEntryRep;
  }

  public void setVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep(
      DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep
          vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep) {
    this.vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep =
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep;
  }
}
