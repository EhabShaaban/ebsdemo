package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.DObAccountingJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.repositories.accounting.DObAccountingJournalEntryPreparationGeneralModelRep;

import java.util.HashSet;
import java.util.Set;

public class DObVendorInvoiceJournalEntryInitializer implements IDObJournalEntryInitializer<DObVendorInvoice> {

    private static final String ACTIVE_STATE = "Active";
    private final CommandUtils commandUtils;
    private DObAccountingJournalEntryPreparationGeneralModelRep accountingJournalEntryPreparationGeneralModelRep;
    public DObVendorInvoiceJournalEntryInitializer() {
        commandUtils = new CommandUtils();
    }

    @Override
    public DObVendorInvoiceJournalEntry initializeJournalEntry(DObVendorInvoice vendorInvoice, DObJournalEntryCreateValueObject vendorInvoiceValueObject) {
        DObVendorInvoiceJournalEntry journalEntry = new DObVendorInvoiceJournalEntry();
        setJournalEntryUserCode(journalEntry, vendorInvoiceValueObject);
        setJournalEntryActiveState(journalEntry);
        setVendorInvoiceData(vendorInvoice, journalEntry);
        commandUtils.setCreationAndModificationInfoAndDate(journalEntry);
        return journalEntry;
    }

    private void setJournalEntryActiveState(DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry) {
        Set<String> currentStates = new HashSet<>();
        currentStates.add(ACTIVE_STATE);
        vendorInvoiceJournalEntry.setCurrentStates(currentStates);
    }

    private void setJournalEntryUserCode(DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry, DObJournalEntryCreateValueObject vendorInvoiceValueObject) {
        String userCode =
                vendorInvoiceValueObject.getJournalEntryCode();
        vendorInvoiceJournalEntry.setUserCode(userCode);
    }

    private void setVendorInvoiceData(DObVendorInvoice vendorInvoice, DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry) {
        String vendorInvoiceType = vendorInvoice.getDiscriminatorValue();
        DObAccountingJournalEntryPreparationGeneralModel preparationGeneralModel = accountingJournalEntryPreparationGeneralModelRep.findOneByCodeAndObjectTypeCode(vendorInvoice.getUserCode(), vendorInvoiceType);
        vendorInvoiceJournalEntry.setDueDate(preparationGeneralModel.getDueDate());
        vendorInvoiceJournalEntry.setCompanyId(preparationGeneralModel.getCompanyId());
        vendorInvoiceJournalEntry.setPurchaseUnitId(preparationGeneralModel.getBusinessUnitId());
        vendorInvoiceJournalEntry.setDocumentReferenceId(preparationGeneralModel.getId());
        vendorInvoiceJournalEntry.setFiscalPeriodId(preparationGeneralModel.getFiscalPeriodId());
    }

    @Override
    public void initializeJournalEntryDetails(IObJournalEntryItem journalEntryDetails) {

    }

    public void setAccountingJournalEntryPreparationGeneralModelRep(DObAccountingJournalEntryPreparationGeneralModelRep accountingJournalEntryPreparationGeneralModelRep) {
        this.accountingJournalEntryPreparationGeneralModelRep = accountingJournalEntryPreparationGeneralModelRep;
    }

    public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
}
