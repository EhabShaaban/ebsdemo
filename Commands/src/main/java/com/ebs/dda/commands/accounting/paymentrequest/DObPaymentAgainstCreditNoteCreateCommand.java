package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.jpa.accounting.accountingnotes.IObAccountingNoteCompanyGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum.DueDocumentType;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyData;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCreditNotePaymentDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteRep;
import com.ebs.dda.repositories.accounting.accountingnote.IObAccountingNoteCompanyGeneralModelRep;

public class DObPaymentAgainstCreditNoteCreateCommand extends DObPaymentToVendorCreateCommand {

	private DObAccountingNoteRep accountingNoteRep;
	private IObAccountingNoteCompanyGeneralModelRep accountingNoteCompanyGeneralModelRep;

	public DObPaymentAgainstCreditNoteCreateCommand(DObPaymentRequestStateMachine stateMachine) {
		super(stateMachine);
	}

	//TODO: Remove when implement credit/note payment
	@Override
	protected void findReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject){}

	@Override
	protected void setCompany(IObPaymentRequestCompanyData paymentRequestCompanyData,
					DObPaymentRequestCreateValueObject paymentRequestValueObject) {
		String accountingNoteCode = paymentRequestValueObject.getDueDocumentCode();
		IObAccountingNoteCompanyGeneralModel accountingNoteCompany = accountingNoteCompanyGeneralModelRep.findOneByDocumentCode(accountingNoteCode);
		paymentRequestCompanyData.setCompanyId(accountingNoteCompany.getCompanyId());
	}

	@Override
	protected void setReferenceDocumentCurrency(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {

	}

	@Override
	protected void setReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {
		String accountingNoteCode = paymentRequestValueObject.getDueDocumentCode();
		DObAccountingNote accountingNote = accountingNoteRep.findOneByUserCode(accountingNoteCode);
		((IObPaymentRequestCreditNotePaymentDetails)paymentDetails).setDueDocumentId(accountingNote.getId());

	}

	@Override
	protected IObPaymentRequestPaymentDetails createPaymentDetailsInstance(
					DueDocumentType documentType) {
		return new IObPaymentRequestCreditNotePaymentDetails();
	}

	public void setAccountingNoteRep(DObAccountingNoteRep accountingNoteRep) {
		this.accountingNoteRep = accountingNoteRep;
	}

	public void setAccountingNoteCompanyGeneralModelRep(IObAccountingNoteCompanyGeneralModelRep accountingNoteCompanyGeneralModelRep) {
		this.accountingNoteCompanyGeneralModelRep = accountingNoteCompanyGeneralModelRep;
	}
}
