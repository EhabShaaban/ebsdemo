package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.*;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsIssueStoreTransactionRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TObStoreTransactionCreateGoodsIssueCommand
        implements ICommand<DObInventoryDocumentActivateValueObject> {

    private final String UNRESTRICTED_STOCK_TYPE = "UNRESTRICTED_USE";
    private final BigDecimal nonRemainingValue = BigDecimal.ZERO;
    private DocumentObjectCodeGenerator documentObjectCodeGenerator;
    private TObGoodsIssueStoreTransactionRep goodsIssueStoreTransactionRep;
    private TObStoreTransactionRep tObStoreTransactionRep;
    private TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep;
    private DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep;
    private CommandUtils commandUtils;
    private List<TObGoodsIssueStoreTransaction> goodsIssueStoreTransactionsList;
  private List<TObStoreTransactionGeneralModel> availableStoreTransactionsList;

    public TObStoreTransactionCreateGoodsIssueCommand(
            DocumentObjectCodeGenerator documentObjectCodeGenerator) {
        commandUtils = new CommandUtils();
        this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    }

    @Override
    public Observable<String> executeCommand(
            DObInventoryDocumentActivateValueObject goodsIssueValueObject) throws Exception {

        String goodsIssueCode = goodsIssueValueObject.getUserCode();

        initGoodsIssueStoreTransactionEntity(goodsIssueCode);

        return Observable.just(goodsIssueCode);
    }

    private void initGoodsIssueStoreTransactionEntity(String goodsIssueCode)
            throws Exception {
        List<DObGoodsIssueDataGeneralModel> goodsIssueItemsData =
                goodsIssueDataGeneralModelRep.findAllByUserCode(goodsIssueCode);
        goodsIssueStoreTransactionsList = new ArrayList<TObGoodsIssueStoreTransaction>();
        for (DObGoodsIssueDataGeneralModel goodsIssueItemDataRow : goodsIssueItemsData) {

            availableStoreTransactionsList = new ArrayList<TObStoreTransactionGeneralModel>();

            getAvailableStoreTransactions(goodsIssueItemDataRow);
            createGoodsIssueStoreTransactions(goodsIssueItemDataRow);
        }
    goodsIssueStoreTransactionRep.saveAll(goodsIssueStoreTransactionsList);
  }

  private void getAvailableStoreTransactions(DObGoodsIssueDataGeneralModel goodsIssueItemDataRow) {
    String companyCode = goodsIssueItemDataRow.getCompanyCode();
    String plantCode = goodsIssueItemDataRow.getPlantCode();
    String storeHouseCode = goodsIssueItemDataRow.getStoreHouseCode();
    String itemCode = goodsIssueItemDataRow.getItemCode();
    String unitOfMeasureCode = goodsIssueItemDataRow.getUnitOfMeasureCode();
    String batchNo = goodsIssueItemDataRow.getBatchNo();
    String purchaseUnitCode = goodsIssueItemDataRow.getPurchaseUnitCode();
      availableStoreTransactionsList =
              tObStoreTransactionGeneralModelRep
                      .findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCodeAndBatchNoAndRemainingQuantityGreaterThanOrderByOriginalAddingDateAsc(
                              itemCode,
                              companyCode,
                              plantCode,
                              storeHouseCode,
                              purchaseUnitCode,
                              UNRESTRICTED_STOCK_TYPE,
                              unitOfMeasureCode,
                              batchNo,
                              nonRemainingValue);
  }

    private void createGoodsIssueStoreTransactions(
            DObGoodsIssueDataGeneralModel goodsIssueItemDataRow) throws Exception {
        BigDecimal goodsIssueRequiredQuantity = goodsIssueItemDataRow.getQuantity();
        BigDecimal storeTransactionRemainingQty;
        BigDecimal quantityValue;
        BigDecimal remainingValue;
        String storeTransactionCode;
        Long storeTransactionId;
        for (int i = 0;
             i < availableStoreTransactionsList.size() && goodsIssueRequiredQuantity.compareTo(BigDecimal.ZERO) > 0;
             i++) {
            storeTransactionRemainingQty = availableStoreTransactionsList.get(i).getRemainingQuantity();
            storeTransactionCode = availableStoreTransactionsList.get(i).getUserCode();
            storeTransactionId = availableStoreTransactionsList.get(i).getId();
            if (storeTransactionRemainingQty.compareTo(goodsIssueRequiredQuantity) >= 0) {
                quantityValue = goodsIssueRequiredQuantity;
                remainingValue = storeTransactionRemainingQty.subtract(goodsIssueRequiredQuantity);
                goodsIssueRequiredQuantity = BigDecimal.ZERO;
            } else {
                quantityValue = storeTransactionRemainingQty;
                remainingValue = nonRemainingValue;
        goodsIssueRequiredQuantity =  goodsIssueRequiredQuantity.subtract(storeTransactionRemainingQty);
      }
            setGoodsIssueStoreTransactionCommonData(
                    quantityValue, storeTransactionId, goodsIssueItemDataRow);
            updateStoreTransactionRecord(remainingValue, storeTransactionCode);
    }
  }

    private void updateStoreTransactionRecord(BigDecimal RemainingQty, String storeTransactionCode)
            throws Exception {
        TObStoreTransaction updatedRecord =
                tObStoreTransactionRep.findOneByUserCode(storeTransactionCode);
        updatedRecord.setRemainingQuantity(RemainingQty);
        tObStoreTransactionRep.update(updatedRecord);
    }

    private void setGoodsIssueStoreTransactionCommonData(
            BigDecimal quantity,
            Long storeTransactionId,
            DObGoodsIssueDataGeneralModel goodsIssueItemDataRow) {

        TObGoodsIssueStoreTransaction goodsIssueStoreTransaction = new TObGoodsIssueStoreTransaction();
        goodsIssueStoreTransaction.setQuantity(quantity);

        StockTypeEnum.StockType stockTypeEnumObject =
                StockTypeEnum.StockType.valueOf(UNRESTRICTED_STOCK_TYPE);
        StockTypeEnum stockTypeEnum = new StockTypeEnum();
        stockTypeEnum.setId(stockTypeEnumObject);
        goodsIssueStoreTransaction.setStockType(stockTypeEnum);

    goodsIssueStoreTransaction.setRefTransactionId(storeTransactionId);

    goodsIssueStoreTransaction.setItemId(goodsIssueItemDataRow.getItemId());

    goodsIssueStoreTransaction.setUnitOfMeasureId(goodsIssueItemDataRow.getUnitOfMeasureId());
    goodsIssueStoreTransaction.setBatchNo(goodsIssueItemDataRow.getBatchNo());
    goodsIssueStoreTransaction.setCompanyId(goodsIssueItemDataRow.getCompanyId());
    goodsIssueStoreTransaction.setPlantId(goodsIssueItemDataRow.getPlantId());
    goodsIssueStoreTransaction.setStorehouseId(goodsIssueItemDataRow.getStoreHouseId());
        goodsIssueStoreTransaction.setPurchaseUnitId(goodsIssueItemDataRow.getPurchaseUnitId());
        goodsIssueStoreTransaction.setDocumentReferenceId(goodsIssueItemDataRow.getGoodsIssueId());
        goodsIssueStoreTransaction.setRemainingQuantity(nonRemainingValue);

    setTransactionOperation(goodsIssueStoreTransaction);

    setGoodsIssueStoreTransactionCreationData(goodsIssueStoreTransaction);
    setStoreTransactionUserCode(goodsIssueStoreTransaction);

    goodsIssueStoreTransactionsList.add(goodsIssueStoreTransaction);
  }

  private void setTransactionOperation(TObGoodsIssueStoreTransaction goodsIssueStoreTransaction) {
      TransactionTypeEnum.TransactionType transactionOperationType =
              TransactionTypeEnum.TransactionType.TAKE;
      TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    transactionOperationEnum.setId(transactionOperationType);
    goodsIssueStoreTransaction.setTransactionOperation(transactionOperationEnum);
  }

    private void setGoodsIssueStoreTransactionCreationData(
            TObGoodsIssueStoreTransaction goodsIssueStoreTransaction) {
        commandUtils.setCreationAndModificationInfoAndDate(goodsIssueStoreTransaction);
    }

    private void setStoreTransactionUserCode(
            TObGoodsIssueStoreTransaction goodsIssueStoreTransaction) {
        String userCode =
                documentObjectCodeGenerator.generateUserCode(TObStoreTransaction.class.getSimpleName());
        goodsIssueStoreTransaction.setUserCode(userCode);
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setGoodsIssueStoreTransactionRep(
            TObGoodsIssueStoreTransactionRep goodsIssueStoreTransactionRep) {
        this.goodsIssueStoreTransactionRep = goodsIssueStoreTransactionRep;
    }

    public void setGoodsIssueDataGeneralModelRep(
            DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep) {
        this.goodsIssueDataGeneralModelRep = goodsIssueDataGeneralModelRep;
    }

  public void settObStoreTransactionRep(TObStoreTransactionRep tObStoreTransactionRep) {
    this.tObStoreTransactionRep = tObStoreTransactionRep;
  }

    public void settObStoreTransactionGeneralModelRep(
            TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep) {
        this.tObStoreTransactionGeneralModelRep = tObStoreTransactionGeneralModelRep;
    }
}
