package com.ebs.dda.commands.accounting.accountingnote;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.jpa.DocumentObjectUpdateRemainingValueObject;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteRep;
import io.reactivex.Observable;

public class DObDebitNoteUpdateRemainingCommand
    implements ICommand<DocumentObjectUpdateRemainingValueObject> {
  private DObAccountingNoteRep accountingNoteRep;
  private CommandUtils commandUtils;

  public DObDebitNoteUpdateRemainingCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DocumentObjectUpdateRemainingValueObject valueObject)
      throws Exception {
    DObAccountingNote debitNote = accountingNoteRep.findOneByUserCode(valueObject.getUserCode());
    Double remaining = debitNote.getRemaining() - valueObject.getAmount().doubleValue();
    debitNote.setRemaining(remaining);
    commandUtils.setModificationInfoAndModificationDate(debitNote);
    accountingNoteRep.update(debitNote);
    return Observable.just(debitNote.getUserCode());
  }

  public void setAccountingNoteRep(DObAccountingNoteRep accountingNoteRep) {
    this.accountingNoteRep = accountingNoteRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
