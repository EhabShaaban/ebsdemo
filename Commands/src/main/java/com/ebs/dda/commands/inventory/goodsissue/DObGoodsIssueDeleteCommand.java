package com.ebs.dda.commands.inventory.goodsissue;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueRep;
import io.reactivex.Observable;
import org.springframework.transaction.TransactionSystemException;

public class DObGoodsIssueDeleteCommand implements ICommand {

  private DObGoodsIssueRep goodsIssueRep;
  private EntityLockCommand<DObGoodsIssue> entityLockCommand;

  @Override
  public Observable<String> executeCommand(Object goodsIssueCode) throws Exception {
    LockDetails lockDetails = null;
    String userCode = (String) goodsIssueCode;
    try {
      lockDetails = entityLockCommand.lockAllSections(userCode);
      DObGoodsIssue goodsIssue = readEntityByCode(userCode);
      checkIfAllowedAction(goodsIssue, IActionsNames.DELETE);
      goodsIssueRep.delete(goodsIssue);
      entityLockCommand.unlockAllSections(userCode);
    } catch (TransactionSystemException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw new DependentInstanceException();
    }
    return Observable.just("SUCCESS");
  }

  protected void checkIfAllowedAction(DObGoodsIssue entity, String allowedAction) throws Exception {
    DObGoodsIssueStateMachine goodsIssueStateMachine = new DObGoodsIssueStateMachine();
    goodsIssueStateMachine.initObjectState(entity);
    try {
      goodsIssueStateMachine.isAllowedAction(allowedAction);
    } catch (ActionNotAllowedPerStateException e) {
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException();
      deleteNotAllowedPerStateException.initCause(e);
      throw deleteNotAllowedPerStateException;
    }
  }

  private DObGoodsIssue readEntityByCode(String userCode)
      throws InstanceNotExistException {
    DObGoodsIssue goodsIssue = goodsIssueRep.findOneByUserCode(userCode);
    if (goodsIssue == null) {
      throw new InstanceNotExistException();
    }
    return goodsIssue;
  }

  public void setGoodsIssueRep(DObGoodsIssueRep goodsIssueRep) {
    this.goodsIssueRep = goodsIssueRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObGoodsIssue> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }
}
