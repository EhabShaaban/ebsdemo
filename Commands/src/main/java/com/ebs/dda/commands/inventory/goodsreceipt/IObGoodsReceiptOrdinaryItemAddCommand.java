package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptActionNames;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.util.List;

public class IObGoodsReceiptOrdinaryItemAddCommand
        implements ICommand<IObGoodsReceiptReceivedItemsDataValueObject> {

    private DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep;
    private EntityLockCommand entityLockCommand;
    private MObItemGeneralModelRep itemGeneralModelRep;
    private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep ordinaryReceivedItemsDataRep;
    private IUserAccountSecurityService userAccountSecurityService;
    private CObMeasureRep measureRep;
    private IObGoodsReceiptReceivedItemsGeneralModelRep receiptReceivedItemsGeneralModelRep;

    private CommandUtils commandUtils;

    public IObGoodsReceiptOrdinaryItemAddCommand() {
        this.commandUtils = new CommandUtils();
    }
    public void setMeasureRep(CObMeasureRep measureRep) {
        this.measureRep = measureRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
    public void setOrdinaryReceivedItemsDataRep(
            IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep ordinaryReceivedItemsDataRep) {
        this.ordinaryReceivedItemsDataRep = ordinaryReceivedItemsDataRep;
    }

    public void setReceiptReceivedItemsGeneralModelRep(
            IObGoodsReceiptReceivedItemsGeneralModelRep receiptReceivedItemsGeneralModelRep) {
        this.receiptReceivedItemsGeneralModelRep = receiptReceivedItemsGeneralModelRep;
    }

    @Override
    public Observable<String> executeCommand(
            IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptOrdinaryReceivedItemsDataValueObject)
            throws Exception {

        IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData iObGoodsReceiptOrdinaryRecievedItemsData =
                new IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData();

        String goodsReceiptCode =
                goodsReceiptOrdinaryReceivedItemsDataValueObject.getGoodsReceiptCode();

        checkItemSectionLocked(goodsReceiptCode);

        DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
                getGoodsReceiptByCode(goodsReceiptCode);

        prepareGoodsReceiptOrdinaryReceivedItemDataToPersistFromValueObject(
                iObGoodsReceiptOrdinaryRecievedItemsData,
                goodsReceiptOrdinaryReceivedItemsDataValueObject,
                dObGoodsReceiptPurchaseOrder);

        ordinaryReceivedItemsDataRep.updateAndClear(iObGoodsReceiptOrdinaryRecievedItemsData);

        commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptPurchaseOrder);

        dObGoodsReceiptPurchaseOrderRep.update(dObGoodsReceiptPurchaseOrder);
        return Observable.just("SUCCESS");
    }

    private Long getUnitOfEntryIdByCode(String unitOfEntryCode) {
        CObMeasure unitOfMeasure = measureRep.findOneByUserCode(unitOfEntryCode);
        return unitOfMeasure.getId();
    }

    public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
        this.itemGeneralModelRep = itemGeneralModelRep;
    }

    public void setdObGoodsReceiptPurchaseOrderRep(
            DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep) {
        this.dObGoodsReceiptPurchaseOrderRep = dObGoodsReceiptPurchaseOrderRep;
    }

    public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
        this.entityLockCommand = entityLockCommand;
    }

    public LockDetails lock(String resourceCode) throws Exception {
        LockDetails lockDetails =
                entityLockCommand.lockSection(resourceCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        DObGoodsReceipt goodsReceipt = dObGoodsReceiptPurchaseOrderRep.findOneByUserCode(resourceCode);
        try {
            checkIfAllowedAction(goodsReceipt);
        } catch (ActionNotAllowedPerStateException e) {
            entityLockCommand.unlockSection(resourceCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
            EditNotAllowedPerStateException editNotAllowedPerStateException =
                    new EditNotAllowedPerStateException();
            editNotAllowedPerStateException.initCause(e);
            throw editNotAllowedPerStateException;
        }
        return lockDetails;
  }

  private void checkItemSectionLocked(String goodsReceiptCode) throws Exception {
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
  }



  private void prepareGoodsReceiptOrdinaryReceivedItemDataToPersistFromValueObject(
          IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItem,
          IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptOrdinaryReceivedItemsDataValueObject,
          DObGoodsReceipt goodsReceipt)
      throws Exception {
    setGoodsReceiptOrdinaryRecievedItemSystemData(goodsReceiptOrdinaryReceivedItem, goodsReceipt);
    setGoodsReceiptOrdinaryRecievedItemValueObject(
        goodsReceiptOrdinaryReceivedItem,
        goodsReceiptOrdinaryReceivedItemsDataValueObject,
        goodsReceipt);
    if (goodsReceiptOrdinaryReceivedItemsDataValueObject.getQuantities() != null) {
      addQuantitesToGoodsReceiptOrdinaryItemFromValueObject(
          goodsReceiptOrdinaryReceivedItem, goodsReceiptOrdinaryReceivedItemsDataValueObject);
    }
  }

  private void setGoodsReceiptOrdinaryRecievedItemSystemData(
          IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItem,
          DObGoodsReceipt goodsReceipt)
      throws Exception {

    goodsReceiptOrdinaryReceivedItem.setRefInstanceId(goodsReceipt.getId());
    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptOrdinaryReceivedItem);
  }

    private void setGoodsReceiptOrdinaryRecievedItemValueObject(
            IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItem,
            IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptOrdinaryReceivedItemsDataValueObject,
            DObGoodsReceipt goodsReceipt) {

        String itemCode = goodsReceiptOrdinaryReceivedItemsDataValueObject.getItemCode();

        IObGoodsReceiptReceivedItemsGeneralModel receiptRecievedItemsGeneralModel =
                receiptReceivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
                        goodsReceipt.getUserCode(), itemCode);
        if (receiptRecievedItemsGeneralModel != null) {
            goodsReceiptOrdinaryReceivedItem.setId(receiptRecievedItemsGeneralModel.getGRItemId());
        }

    Long itemId = getItemIdByCode(itemCode);
    goodsReceiptOrdinaryReceivedItem.setItemId(itemId);

    goodsReceiptOrdinaryReceivedItem.setDifferenceReason("");

    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptOrdinaryReceivedItem);
  }

  private void addQuantitesToGoodsReceiptOrdinaryItemFromValueObject(
          IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItem,
          IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptOrdinaryReceivedItemsDataValueObject)
      throws Exception {

    List<IObGoodsReceiptItemQuantityValueObject> goodsReceiptItemQuantitiesValueObjectList =
        goodsReceiptOrdinaryReceivedItemsDataValueObject.getQuantities();

    for (IObGoodsReceiptItemQuantityValueObject goodsReceiptItemQuantityValueObject :
        goodsReceiptItemQuantitiesValueObjectList) {

        IObGoodsReceiptPurchaseOrderItemQuantities goodsReceiptItemQuantity =
                new IObGoodsReceiptPurchaseOrderItemQuantities();

        if (goodsReceiptItemQuantityValueObject.getId() != null) {
            goodsReceiptItemQuantity.setId(goodsReceiptItemQuantityValueObject.getId());
        }
        goodsReceiptItemQuantity.setReceivedQtyUoE(
                goodsReceiptItemQuantityValueObject.getReceivedQtyUoE());
        goodsReceiptItemQuantity.setNotes(goodsReceiptItemQuantityValueObject.getNotes());

        StockTypeEnum defectStockTypeEnum = new StockTypeEnum();
        defectStockTypeEnum.setId(goodsReceiptItemQuantityValueObject.getStockType());
        goodsReceiptItemQuantity.setStockType(defectStockTypeEnum);

        Long unitOfEntryId =
                getUnitOfEntryIdByCode(goodsReceiptItemQuantityValueObject.getUnitOfEntryCode());
        goodsReceiptItemQuantity.setUnitOfEntryId(unitOfEntryId);

        goodsReceiptItemQuantity.setRefInstanceId(goodsReceiptOrdinaryReceivedItem.getId());

        commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptItemQuantity);

        goodsReceiptOrdinaryReceivedItem.addLineDetail(
                IIObGoodsReceiptReceivedItemsData.iObGoodsReceiptPurchaseOrderItemQuantities,
                goodsReceiptItemQuantity);
    }
  }

    private DObGoodsReceiptPurchaseOrder getGoodsReceiptByCode(String goodsReceiptCode) {
        return dObGoodsReceiptPurchaseOrderRep.findOneByUserCode(goodsReceiptCode);
    }

    private Long getItemIdByCode(String itemCode) {
        MObItemGeneralModel itemGeneralModelRepByCode = itemGeneralModelRep.findByUserCode(itemCode);
        return itemGeneralModelRepByCode.getId();
    }

    private void checkIfAllowedAction(DObGoodsReceipt entity) throws Exception {
        DObGoodsReceiptStateMachine goodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
        goodsReceiptStateMachine.initObjectState(entity);
        goodsReceiptStateMachine.isAllowedAction(IGoodsReceiptActionNames.UPDATE_ITEM);
    }


}
