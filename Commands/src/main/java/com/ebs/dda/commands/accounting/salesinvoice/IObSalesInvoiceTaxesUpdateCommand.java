package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class IObSalesInvoiceTaxesUpdateCommand implements ICommand<IObInvoiceTaxesValueObject> {
	private CommandUtils commandUtils;
	private IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep;
	private IObSalesInvoiceTaxesRep taxesRep;
	private IObSalesInvoiceTaxesGeneralModelRep taxesGeneralModelRep;

	public IObSalesInvoiceTaxesUpdateCommand() {
		commandUtils = new CommandUtils();
	}

	@Transactional
	@Override
	public Observable<String> executeCommand(IObInvoiceTaxesValueObject taxesValueObject)
					throws Exception {

		String invoiceCode = taxesValueObject.getInvoiceCode();

		List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItems = salesInvoiceItemsGeneralModelRep
						.findByInvoiceCode(invoiceCode);

		List<IObSalesInvoiceTaxes> salesInvoiceTaxes = taxesGeneralModelRep
			.findByInvoiceCode(invoiceCode)
			.stream()
			.map(taxGM -> createTaxEntity(salesInvoiceItems, taxGM))
			.collect(Collectors.toList());

		taxesRep.saveAll(salesInvoiceTaxes);
		return Observable.just("SUCCESS");
	}

	private IObSalesInvoiceTaxes createTaxEntity(
		List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItems,
		IObSalesInvoiceTaxesGeneralModel taxGeneralModel) {

		IObSalesInvoiceTaxes tax = new IObSalesInvoiceTaxes();
		tax.setId(taxGeneralModel.getId());
		tax.setCode(taxGeneralModel.getTaxCode());
		tax.setName(taxGeneralModel.getTaxName());
		tax.setRefInstanceId(taxGeneralModel.getRefInstanceId());
		tax.setCreationDate(taxGeneralModel.getCreationDate());
		tax.setCreationInfo(taxGeneralModel.getCreationInfo());

		BigDecimal taxTotalAmount = taxGeneralModel
			.getTaxPercentage()
			.multiply(getTotalItemsAmount(salesInvoiceItems))
			.divide(BigDecimal.valueOf(100));

		tax.setAmount(taxTotalAmount);
		commandUtils.setModificationInfoAndModificationDate(tax);
		return tax;
	}

	private BigDecimal getTotalItemsAmount(List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItems) {
		return salesInvoiceItems
      .stream()
      .map(item ->
        item.getQuantityInOrderUnit()
				.multiply(item.getPrice())
				.multiply(item.getConversionFactorToBase())
      )
      .reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setSalesInvoiceItemsGeneralModelRep(
					IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
		this.salesInvoiceItemsGeneralModelRep = salesInvoiceItemsGeneralModelRep;
	}

	public void setTaxesRep(IObSalesInvoiceTaxesRep taxesRep) {
		this.taxesRep = taxesRep;
	}

	public void setTaxesGeneralModelRep(IObSalesInvoiceTaxesGeneralModelRep taxesGeneralModelRep) {
		this.taxesGeneralModelRep = taxesGeneralModelRep;
	}
}
