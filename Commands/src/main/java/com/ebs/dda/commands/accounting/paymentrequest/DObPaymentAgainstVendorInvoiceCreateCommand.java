package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum.DueDocumentType;

public class DObPaymentAgainstVendorInvoiceCreateCommand extends
				DObPaymentToVendorCreateCommand<DObPaymentReferenceVendorInvoiceGeneralModel> {

	public DObPaymentAgainstVendorInvoiceCreateCommand(DObPaymentRequestStateMachine stateMachine){
		super(stateMachine);
	}

	@Override
	protected void setCompany(IObPaymentRequestCompanyData paymentRequestCompanyData,
					DObPaymentRequestCreateValueObject paymentRequestValueObject) {
		DObPaymentReferenceVendorInvoiceGeneralModel vendorInvoice = getRefDocument();
		paymentRequestCompanyData.setCompanyId(vendorInvoice.getCompanyId());
	}

	@Override
	protected void setReferenceDocumentCurrency(
					DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		DObPaymentReferenceVendorInvoiceGeneralModel vendorInvoice = getRefDocument();
		paymentDetails.setCurrencyId(vendorInvoice.getCurrencyId());

	}

	@Override
	protected void setReferenceDocument(
					DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		DObPaymentReferenceVendorInvoiceGeneralModel vendorInvoice = getRefDocument();
		((IObPaymentRequestInvoicePaymentDetails) paymentDetails)
						.setDueDocumentId(vendorInvoice.getId());
	}

	@Override
	protected IObPaymentRequestPaymentDetails createPaymentDetailsInstance(
					DueDocumentType documentType) {
		return new IObPaymentRequestInvoicePaymentDetails();
	}
}
