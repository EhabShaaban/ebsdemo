package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderConsigneeEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPlantEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderStorehouseEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderConsigneeEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPlantEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderStorehouseEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConsigneeValueObject;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import io.reactivex.Observable;

public class DObPurchaseOrderEditConsigneeCommand
    implements ICommand<DObPurchaseOrderConsigneeValueObject> {
  private final CommandUtils commandUtils;

  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderConsigneeEnterpriseDataRep iObOrderConsigneeEnterpriseDataRep;
  private IObOrderPlantEnterpriseDataRep iObOrderPlantEnterpriseDataRep;
  private IObOrderStorehouseEnterpriseDataRep iObOrderStorehouseEnterpriseDataRep;
  private CObCompanyGeneralModelRep cObCompanyGeneralModelRep;
  private CObPlantGeneralModelRep cObPlantGeneralModelRep;
  private CObStorehouseGeneralModelRep cObStorehouseGeneralModelRep;

  public DObPurchaseOrderEditConsigneeCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObPurchaseOrderConsigneeValueObject valueObject)
      throws Exception {
    String purchaseOrderCode = valueObject.getPurchaseOrderCode();
    String consigneeCode = valueObject.getConsigneeCode();
    String plantCode = valueObject.getPlantCode();
    String storehouseCode = valueObject.getStorehouseCode();

    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);

    Long purchaseOrderId = purchaseOrder.getId();

    prepareConsigneeData(consigneeCode, purchaseOrderId, purchaseOrder);
    preparePlantData(plantCode, purchaseOrderId, purchaseOrder);
    prepareStorehouseData(storehouseCode, plantCode, purchaseOrderId, purchaseOrder);

    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void prepareStorehouseData(
      String storehouseCode,
      String plantCode,
      Long purchaseOrderId,
      DObPurchaseOrder purchaseOrder) {

    IObOrderStorehouseEnterpriseData storehouseEnterpriseData =
        iObOrderStorehouseEnterpriseDataRep.findOneByRefInstanceId(purchaseOrderId);

    if (storehouseCode != null && !storehouseCode.isEmpty()) {
      if (storehouseEnterpriseData == null) {
        storehouseEnterpriseData = new IObOrderStorehouseEnterpriseData();
        commandUtils.setCreationAndModificationInfoAndDate(storehouseEnterpriseData);
      } else {
        commandUtils.setModificationInfoAndModificationDate(storehouseEnterpriseData);
      }

      CObStorehouseGeneralModel storehouse =
          cObStorehouseGeneralModelRep.findByUserCodeAndPlantCode(storehouseCode, plantCode);
      storehouseEnterpriseData.setEnterpriseName(storehouse.getName());
      storehouseEnterpriseData.setEnterpriseId(storehouse.getId());

      purchaseOrder.setHeaderDetail(IDObPurchaseOrder.storehouseDataAttr, storehouseEnterpriseData);

    } else if (storehouseEnterpriseData != null) {
      iObOrderStorehouseEnterpriseDataRep.delete(storehouseEnterpriseData);
    }
  }

  private void preparePlantData(
      String plantCode, Long purchaseOrderId, DObPurchaseOrder purchaseOrder) {

    IObOrderPlantEnterpriseData plantEnterpriseData =
        iObOrderPlantEnterpriseDataRep.findOneByRefInstanceId(purchaseOrderId);

    if (plantCode != null && !plantCode.isEmpty()) {
      if (plantEnterpriseData == null) {
        plantEnterpriseData = new IObOrderPlantEnterpriseData();
        commandUtils.setCreationAndModificationInfoAndDate(plantEnterpriseData);
      } else {
        commandUtils.setModificationInfoAndModificationDate(plantEnterpriseData);
      }

      CObPlantGeneralModel plant = cObPlantGeneralModelRep.findOneByUserCode(plantCode);
      plantEnterpriseData.setEnterpriseName(plant.getName());
      plantEnterpriseData.setEnterpriseId(plant.getId());

      purchaseOrder.setHeaderDetail(IDObPurchaseOrder.plantDataAttr, plantEnterpriseData);

    } else if (plantEnterpriseData != null) {
      iObOrderPlantEnterpriseDataRep.delete(plantEnterpriseData);
    }
  }

  private void prepareConsigneeData(
      String consigneeCode, Long purchaseOrderId, DObPurchaseOrder purchaseOrder) {

    IObOrderConsigneeEnterpriseData consigneeEnterpriseData =
        iObOrderConsigneeEnterpriseDataRep.findOneByRefInstanceId(purchaseOrderId);

    if (consigneeCode != null && !consigneeCode.isEmpty()) {
      if (consigneeEnterpriseData == null) {
        consigneeEnterpriseData = new IObOrderConsigneeEnterpriseData();
        commandUtils.setCreationAndModificationInfoAndDate(consigneeEnterpriseData);
      } else {
        commandUtils.setModificationInfoAndModificationDate(consigneeEnterpriseData);
      }

      CObCompanyGeneralModel consignee = cObCompanyGeneralModelRep.findOneByUserCode(consigneeCode);
      consigneeEnterpriseData.setEnterpriseName(consignee.getCompanyName());
      consigneeEnterpriseData.setEnterpriseId(consignee.getId());

      purchaseOrder.setHeaderDetail(IDObPurchaseOrder.consigneeDataAttr, consigneeEnterpriseData);

    } else if (consigneeEnterpriseData != null) {
      iObOrderConsigneeEnterpriseDataRep.delete(consigneeEnterpriseData);
    }
  }

  public void setOrderConsigneeEnterpriseDataRep(
      IObOrderConsigneeEnterpriseDataRep iObOrderConsigneeEnterpriseDataRep) {
    this.iObOrderConsigneeEnterpriseDataRep = iObOrderConsigneeEnterpriseDataRep;
  }

  public void setOrderPlantEnterpriseDataRep(
      IObOrderPlantEnterpriseDataRep iObOrderPlantEnterpriseDataRep) {
    this.iObOrderPlantEnterpriseDataRep = iObOrderPlantEnterpriseDataRep;
  }

  public void setOrderStorehouseEnterpriseDataRep(
      IObOrderStorehouseEnterpriseDataRep iObOrderStorehouseEnterpriseDataRep) {
    this.iObOrderStorehouseEnterpriseDataRep = iObOrderStorehouseEnterpriseDataRep;
  }

  public void setCObCompanyGeneralModelRep(CObCompanyGeneralModelRep cObCompanyGeneralModelRep) {
    this.cObCompanyGeneralModelRep = cObCompanyGeneralModelRep;
  }

  public void setCObPlantGeneralModelRep(CObPlantGeneralModelRep cObPlantGeneralModelRep) {
    this.cObPlantGeneralModelRep = cObPlantGeneralModelRep;
  }

  public void setCObStorehouseGeneralModelRep(
      CObStorehouseGeneralModelRep cObStorehouseGeneralModelRep) {
    this.cObStorehouseGeneralModelRep = cObStorehouseGeneralModelRep;
  }

  public void unlockSection(String purchaseOrderCode) throws Exception {
    entityLockCommand.unlockSection(
        purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);
  }

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setIUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
