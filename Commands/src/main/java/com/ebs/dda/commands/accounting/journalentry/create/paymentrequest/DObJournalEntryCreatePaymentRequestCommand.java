package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRep;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class DObJournalEntryCreatePaymentRequestCommand
    implements ICommand<DObJournalEntryCreateValueObject> {

  protected static final String ACTIVE_STATE = "Active";

  protected DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep;
  protected IObPaymentRequestAccountDetailsGeneralModelRep
      paymentRequestAccountingDetailsGeneralModelRep;
  protected IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
      paymentRequestPaymentDetailsRep;
  protected IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep;
  protected DObPaymentRequestRep paymentRequestRep;
  protected IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep;
  protected IObPaymentRequestPaymentDetailsGeneralModelRep
      paymentRequestPaymentDetailsGeneralModelRep;
  protected CommandUtils commandUtils;

  protected DObPaymentRequest paymentRequest;
  protected IObPaymentRequestCompanyData paymentRequestCompanyData;
  protected IObPaymentRequestAccountDetailsGeneralModel paymentRequestAccountingDetailsDebit;
  protected IObPaymentRequestAccountDetailsGeneralModel paymentRequestAccountingDetailsCredit;
  protected List<IObPaymentRequestAccountDetailsGeneralModel>
      paymentRequestAccountDetailsGeneralModelList;
  protected IObPaymentRequestPaymentDetailsGeneralModel paymentRequestPaymentDetailsGeneralModel;
  protected IObPaymentRequestPaymentDetails paymentDetails;
  protected IObPaymentRequestPostingDetails paymentPostingDetails;

  protected DObPaymentRequestJournalEntry paymentRequestJournalEntry;
  protected List<IObJournalEntryItem> journalEntryItems;

  protected IObAccountingDocumentActivationDetailsGeneralModelRep
          accountingDocumentActivationDetailsGeneralModelRep;
  IObAccountingDocumentActivationDetailsGeneralModel paymentRequestActivationDetailsGeneralModel;

  public DObJournalEntryCreatePaymentRequestCommand() {
    commandUtils = new CommandUtils();
  }

  protected void executeParentCommand(DObJournalEntryCreateValueObject paymentRequestValueObject) {
    paymentRequestJournalEntry = new DObPaymentRequestJournalEntry();
    journalEntryItems = new ArrayList<>();
    String paymentRequestCode = paymentRequestValueObject.getUserCode();
    initBasicEntities(paymentRequestCode);
    setJournalEntryData(paymentRequestJournalEntry, paymentRequestValueObject);
  }

  protected void executeTransaction(
      DObPaymentRequestJournalEntry paymentRequestJournalEntry,
      List<IObJournalEntryItem> journalEntryItems)
      throws Exception {
    paymentRequestJournalEntry.setLinesDetails(IDObJournalEntry.journalItemAttr, journalEntryItems);
    paymentRequestJournalEntryRep.create(paymentRequestJournalEntry);
  }

  private void initBasicEntities(String paymentRequestCode) {

    paymentRequest = paymentRequestRep.findOneByUserCode(paymentRequestCode);
    paymentRequestCompanyData =
        paymentRequestCompanyDataRep.findOneByRefInstanceId(paymentRequest.getId());
    paymentDetails =
        paymentRequestPaymentDetailsRep.findOneByRefInstanceId(paymentRequest.getId());

    paymentRequestAccountingDetailsDebit =
        paymentRequestAccountingDetailsGeneralModelRep
            .findOneByRefInstanceIdAndAccountingEntry(
                paymentRequest.getId(), AccountingEntry.DEBIT.name());
    paymentRequestAccountingDetailsCredit =
            paymentRequestAccountingDetailsGeneralModelRep
            .findOneByRefInstanceIdAndAccountingEntry(
                paymentRequest.getId(), AccountingEntry.CREDIT.name());

    paymentPostingDetails =
        paymentRequestPostingDetailsRep.findOneByRefInstanceId(paymentRequest.getId());
    paymentRequestAccountDetailsGeneralModelList =
        paymentRequestAccountingDetailsGeneralModelRep
            .findByDocumentCodeOrderByAccountingEntryDesc(paymentRequestCode);
    paymentRequestPaymentDetailsGeneralModel =
        paymentRequestPaymentDetailsGeneralModelRep.findOneByUserCode(paymentRequestCode);

    paymentRequestActivationDetailsGeneralModel =
            accountingDocumentActivationDetailsGeneralModelRep.findOneByCodeAndDocumentType(
                    paymentRequestCode, IDocumentTypes.PAYMENT_REQUEST);
  }

  private void setJournalEntryData(DObPaymentRequestJournalEntry paymentRequestJournalEntry, DObJournalEntryCreateValueObject paymentRequestValueObject) {
    setJournalEntryUserCode(paymentRequestJournalEntry, paymentRequestValueObject);
    setJournalEntryActiveState(paymentRequestJournalEntry);
    commandUtils.setCreationAndModificationInfoAndDate(paymentRequestJournalEntry);
    setPaymentRequestRequestData(paymentRequestJournalEntry);
  }

  private void setJournalEntryActiveState(
      DObPaymentRequestJournalEntry paymentRequestJournalEntry) {
    Set<String> currentStates = new HashSet<>();
    currentStates.add(ACTIVE_STATE);
    paymentRequestJournalEntry.setCurrentStates(currentStates);
  }

  private void setJournalEntryUserCode(DObPaymentRequestJournalEntry paymentRequestJournalEntry, DObJournalEntryCreateValueObject paymentRequestValueObject) {
    String userCode =
            paymentRequestValueObject.getJournalEntryCode();
    paymentRequestJournalEntry.setUserCode(userCode);
  }

  private void setPaymentRequestRequestData(
      DObPaymentRequestJournalEntry paymentRequestJournalEntry) {
    setGeneralData(paymentRequestJournalEntry);
    setCompanyData(paymentRequestJournalEntry);
    setPostingDetailsData(paymentRequestJournalEntry);
  }

  private void setPostingDetailsData(DObPaymentRequestJournalEntry paymentRequestJournalEntry) {
    DateTime dueDate = paymentPostingDetails.getDueDate();
    paymentRequestJournalEntry.setDueDate(dueDate);
    paymentRequestJournalEntry.setFiscalPeriodId(paymentRequestActivationDetailsGeneralModel.getFiscalPeriodId());
  }

  private void setCompanyData(DObPaymentRequestJournalEntry paymentRequestJournalEntry) {
    paymentRequestJournalEntry.setPurchaseUnitId(paymentRequestCompanyData.getPurchaseUnitId());
    paymentRequestJournalEntry.setCompanyId(paymentRequestCompanyData.getCompanyId());
  }

  private void setGeneralData(DObPaymentRequestJournalEntry paymentRequestJournalEntry) {
    paymentRequestJournalEntry.setDocumentReferenceId(paymentRequest.getId());
  }

  public void setPaymentRequestJournalEntryRep(
      DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep) {
    this.paymentRequestJournalEntryRep = paymentRequestJournalEntryRep;
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }

  public void setPaymentRequestCompanyDataRep(
      IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep) {
    this.paymentRequestCompanyDataRep = paymentRequestCompanyDataRep;
  }

  public void setPaymentRequestPostingDetailsRep(
      IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep) {
    this.paymentRequestPostingDetailsRep = paymentRequestPostingDetailsRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPaymentRequestPaymentDetailsRep(
      IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
          paymentRequestPaymentDetailsRep) {
    this.paymentRequestPaymentDetailsRep = paymentRequestPaymentDetailsRep;
  }


  public void setPaymentRequestAccountingDetailsGeneralModelRep(
      IObPaymentRequestAccountDetailsGeneralModelRep
          paymentRequestAccountingDetailsGeneralModelRep) {
    this.paymentRequestAccountingDetailsGeneralModelRep =
        paymentRequestAccountingDetailsGeneralModelRep;
  }

  public void setPaymentRequestPaymentDetailsGeneralModelRep(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep) {
    this.paymentRequestPaymentDetailsGeneralModelRep = paymentRequestPaymentDetailsGeneralModelRep;
  }

  public void setAccountingDocumentActivationDetailsGeneralModelRep(
          IObAccountingDocumentActivationDetailsGeneralModelRep
                  accountingDocumentActivationDetailsGeneralModelRep) {
    this.accountingDocumentActivationDetailsGeneralModelRep =
            accountingDocumentActivationDetailsGeneralModelRep;
  }
}
