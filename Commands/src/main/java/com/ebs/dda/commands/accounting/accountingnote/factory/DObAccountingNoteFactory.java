package com.ebs.dda.commands.accounting.accountingnote.factory;

import com.ebs.dda.jpa.accounting.accountingnotes.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;

import javax.transaction.NotSupportedException;

public class DObAccountingNoteFactory {

  public DObAccountingNote createAccountingNote(String type, String typeCode) throws Exception {

    if (isCommissionDebitNote(type, typeCode)) {
      return new DObDebitNoteCommission();
    } else if (isCreditNoteBasedOnSalesReturn(type, typeCode)) {
      return new DObCreditNoteSalesReturn();
    }

    throw new NotSupportedException("Not supported Accounting Note type");
  }

  private boolean isCommissionDebitNote(String type, String typeCode) {
    return type.equals(AccountingEntry.DEBIT.name())
        && typeCode.equals(DebitNoteTypeEnum.DEBIT_NOTE_BASED_ON_COMMISSION.name());
  }

  private boolean isCreditNoteBasedOnSalesReturn(String type, String typeCode) {
    return type.equals(AccountingEntry.CREDIT.name())
        && typeCode.equals(CreditNoteTypeEnum.CREDIT_NOTE_BASED_ON_SALES_RETURN.name());
  }
}
