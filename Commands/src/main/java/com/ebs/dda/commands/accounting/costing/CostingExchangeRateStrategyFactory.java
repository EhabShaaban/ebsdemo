package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CostingExchangeRateStrategyFactory {

  private final ApplicationContext applicationContext;

  public CostingExchangeRateStrategyFactory(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public ICalculateCostingExchangeRateStrategy getStrategy(
      IObVendorInvoiceSummary vendorInvoiceSummary) {
    ICalculateCostingExchangeRateStrategy strategy = null;

    if (isRemainingEqualToZero(vendorInvoiceSummary)) {
      strategy = applicationContext.getBean(CostingExchangeRateFullyPaidStrategy.class);
    }
    else if (isRemainingEqualToTotal(vendorInvoiceSummary)) {
      strategy = applicationContext.getBean(CostingExchangeRateNotPaidStrategy.class);
    }
    else if (isaRemainingGreaterThanZero(vendorInvoiceSummary)) {
      strategy = applicationContext.getBean(CostingExchangeRatePartiallyPaidStrategy.class);
    }
    strategy.setVendorInvoiceSummary(vendorInvoiceSummary);
    return strategy;
  }

  private boolean isaRemainingGreaterThanZero(IObVendorInvoiceSummary vendorInvoiceSummary) {
    return vendorInvoiceSummary.getRemaining().compareTo(BigDecimal.ZERO) > 0;
  }

  private boolean isRemainingEqualToZero(IObVendorInvoiceSummary vendorInvoiceSummary) {
    return vendorInvoiceSummary.getRemaining().compareTo(BigDecimal.ZERO) == 0;
  }

  private boolean isRemainingEqualToTotal(IObVendorInvoiceSummary vendorInvoiceSummary) {
    return vendorInvoiceSummary.getRemaining().compareTo(vendorInvoiceSummary.getTotalAmount()) == 0;
  }
}
