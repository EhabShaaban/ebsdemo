package com.ebs.dda.commands.accounting.vendorinvoice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxes;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRep;

import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesRep;
import io.reactivex.Observable;

public class IObVendorInvoiceTaxesUpdateCommand implements ICommand<DObVendorInvoice> {
	private CommandUtils commandUtils;
	private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
	private IObVendorInvoiceTaxesGeneralModelRep taxesGeneralModelRep;
	private IObVendorInvoiceTaxesRep taxesRep;

	public IObVendorInvoiceTaxesUpdateCommand() {
		commandUtils = new CommandUtils();
	}

	@Transactional
	@Override
	public Observable<String> executeCommand(DObVendorInvoice vendorInvoice) throws Exception {

		List<IObVendorInvoiceTaxes> vendorInvoiceTaxes = addVendorTaxesToVendorInvoice(
						vendorInvoice);
		taxesRep.saveAll(vendorInvoiceTaxes);
		return Observable.just("SUCCESS");
	}

	private List<IObVendorInvoiceTaxes> addVendorTaxesToVendorInvoice(
            DObVendorInvoice vendorInvoice) {

		return taxesGeneralModelRep
			.findByRefInstanceId(vendorInvoice.getId())
			.stream()
			.map(tax -> createTaxEntity(vendorInvoice, tax))
			.collect(Collectors.toList());
	}

	private IObVendorInvoiceTaxes createTaxEntity(DObVendorInvoice vendorInvoice,
		IObVendorInvoiceTaxesGeneralModel vendorTax) {

		List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems = vendorInvoiceItemsGeneralModelRep
			.findByInvoiceCode(vendorInvoice.getUserCode());
		BigDecimal totalItemsAmount = getTotalItemsAmount(vendorInvoiceItems);

		IObVendorInvoiceTaxes tax = new IObVendorInvoiceTaxes();
		commandUtils.setModificationInfoAndModificationDate(tax);
		tax.setId(vendorTax.getId());
		tax.setRefInstanceId(vendorTax.getRefInstanceId());

		tax.setName(vendorTax.getTaxName());
		tax.setCode(vendorTax.getTaxCode());
		tax.setCreationDate(vendorTax.getCreationDate());
		tax.setCreationInfo(vendorTax.getCreationInfo());

		BigDecimal taxTotalAmount = vendorTax
			.getTaxPercentage()
			.multiply(totalItemsAmount)
			.divide(BigDecimal.valueOf(100));
		tax.setAmount(taxTotalAmount);

		return tax;
	}

	private BigDecimal getTotalItemsAmount(List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems) {
		return vendorInvoiceItems
			.stream()
			.map(item ->
				item.getQuantityInOrderUnit()
					.multiply(item.getPrice())
					.multiply(item.getConversionFactorToBase())
			)
			.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setVendorInvoiceItemsGeneralModelRep(
					IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
		this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
	}

	public void setTaxesGeneralModelRep(IObVendorInvoiceTaxesGeneralModelRep taxesGeneralModelRep) {
		this.taxesGeneralModelRep = taxesGeneralModelRep;
	}

	public void setTaxesRep(IObVendorInvoiceTaxesRep taxesRep) {
		this.taxesRep = taxesRep;
	}
}
