package com.ebs.dda.commands.accounting.journalentry.create.salesinvoice;

import java.math.BigDecimal;

import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemCustomerSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemTaxesSubAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;

public class DObJournalEntrySalesInvoiceInstanceFactory {

    private DObJournalEntrySalesInvoiceInstanceFactory(){}

	public static IObJournalEntryItem getJournalItemInstance(String accountingEntry, String subLedger) {
		if (subLedger.equals(SubLedgers.Taxes.name())) {
			return new IObJournalEntryItemTaxesSubAccount();
		}
		if (accountingEntry.equals(AccountingEntry.DEBIT.name())) {
			IObJournalEntryItemCustomerSubAccount customerSubAccount = new IObJournalEntryItemCustomerSubAccount();
			customerSubAccount.setType(subLedger);
			return customerSubAccount;
		}
		return new IObJournalEntryItem();
	}

	public static void setAmount(String accountingEntry, IObJournalEntryItem journalEntryItem,
					BigDecimal amount) {
		if (accountingEntry.equals(AccountingEntry.CREDIT.name())) {
			setCreditAmount(journalEntryItem, amount);
		} else {
			journalEntryItem.setDebit(amount);
			journalEntryItem.setCredit(BigDecimal.ZERO);

		}
	}

	private static void setCreditAmount(IObJournalEntryItem journalEntryItem, BigDecimal amount) {
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			journalEntryItem.setDebit(amount.abs());
			journalEntryItem.setCredit(BigDecimal.ZERO);
		} else {
			journalEntryItem.setDebit(BigDecimal.ZERO);
			journalEntryItem.setCredit(amount);
		}
	}
}
