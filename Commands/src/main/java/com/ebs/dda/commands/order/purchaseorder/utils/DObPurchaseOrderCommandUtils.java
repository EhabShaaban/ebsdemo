package com.ebs.dda.commands.order.purchaseorder.utils;

import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;

public class DObPurchaseOrderCommandUtils {

  public static void setPurchaseOrderTargetState(DObPurchaseOrder purchaseOrder, String actionName)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    if (purchaseOrderStateMachine.fireEvent(actionName)) {
      purchaseOrderStateMachine.save();
    }
  }

  private static AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }
}
