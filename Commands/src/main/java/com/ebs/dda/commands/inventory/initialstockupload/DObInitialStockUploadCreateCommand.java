package com.ebs.dda.commands.inventory.initialstockupload;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.inventory.initialstockupload.statemachines.DObInitialStockUploadStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUpload;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadCreateValueObject;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUpload;
import com.ebs.dda.jpa.inventory.initialstockupload.IObInitialStockUploadItem;
import com.ebs.dda.jpa.inventory.inventorydocument.IDObInventoryDocument;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompany;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.inventory.initialstockupload.DObInitialStockUploadRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import io.reactivex.Observable;

import java.util.List;

public class DObInitialStockUploadCreateCommand
    implements ICommand<DObInitialStockUploadCreateValueObject> {
  private final CommandUtils commandUtils;
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private final DObInitialStockUploadStateMachine stateMachine;
  private CObPurchasingUnitRep purchasingUnitRep;
  private CObStorehouseGeneralModelRep storehouseRep;
  private CObCompanyRep companyRep;
  private CObPlantGeneralModelRep plantRep;
  private StoreKeeperRep storeKeeperRep;
  private DObInitialStockUploadRep initialStockUploadRep;
  private ItemVendorRecordRep itemVendorRecordRep;
  private IUserAccountSecurityService userAccountSecurityService;

  public DObInitialStockUploadCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    commandUtils = new CommandUtils();
    stateMachine = new DObInitialStockUploadStateMachine();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(DObInitialStockUploadCreateValueObject valueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObInitialStockUpload.class.getSimpleName());
    DObInitialStockUpload initialStockUpload = new DObInitialStockUpload();
    setInitialStockUploadData(userCode, initialStockUpload, valueObject);

    IObInventoryCompany inventoryCompany = new IObInventoryCompany();
    setInventoryCompany(valueObject, inventoryCompany);
    initialStockUpload.setHeaderDetail(IDObInventoryDocument.companyAttr, inventoryCompany);

    List<IObInitialStockUploadItem> itemsList =
        commandUtils.convertCSVFileToEntityList(
            valueObject.getAttachment(), IObInitialStockUploadItem.class);
    setInitialStockUploadItemInfo(itemsList);
    initialStockUpload.setLinesDetails(IDObInitialStockUpload.itemsAttr, itemsList);

    initialStockUploadRep.create(initialStockUpload);

    return Observable.just(userCode);
  }

  private void setInitialStockUploadItemInfo(List<IObInitialStockUploadItem> itemsList) {
    for (IObInitialStockUploadItem item : itemsList) {
      setItemUOMIdAndItemID(item);
      commandUtils.setCreationAndModificationInfoAndDate(item);
    }
  }

  private void setItemUOMIdAndItemID(IObInitialStockUploadItem item) {
    ItemVendorRecord itemVendorRecord =
        itemVendorRecordRep.findOneByOldItemNumber(item.getOldItemNumber());
    item.setItemId(itemVendorRecord.getItemId());
    item.setUnitOfMeasureId(itemVendorRecord.getUomId());
  }

  private void setInitialStockUploadData(
      String userCode,
      DObInitialStockUpload dObInitialStockUpload,
      DObInitialStockUploadCreateValueObject dObInitialStockUploadCreateValueObject)
      throws Exception {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setCreationAndModificationInfoAndDate(dObInitialStockUpload);
    stateMachine.initObjectState(dObInitialStockUpload);

    dObInitialStockUpload.setUserCode(userCode);

    setPurchaseUnit(
        dObInitialStockUpload, dObInitialStockUploadCreateValueObject.getPurchaseUnitCode());
  }

  private void setInventoryCompany(
      DObInitialStockUploadCreateValueObject initialStockUploadCreateValueObject,
      IObInventoryCompany inventoryCompany) {
    commandUtils.setCreationAndModificationInfoAndDate(inventoryCompany);
    StoreKeeper storeKeeper =
        storeKeeperRep.findOneByCode(initialStockUploadCreateValueObject.getStoreKeeperCode());
    inventoryCompany.setStoreKeeperId(storeKeeper.getId());

    CObCompany company =
        companyRep.findOneByUserCode(initialStockUploadCreateValueObject.getCompanyCode());
    inventoryCompany.setCompanyId(company.getId());

    CObPlantGeneralModel plant =
        plantRep.findOneByCompanyCode(initialStockUploadCreateValueObject.getCompanyCode());
    inventoryCompany.setPlantId(plant.getId());
    CObStorehouseGeneralModel storehouse =
        storehouseRep.findByUserCodeAndPlantCode(
            initialStockUploadCreateValueObject.getStoreHouseCode(), plant.getUserCode());
    inventoryCompany.setStorehouseId(storehouse.getId());
  }

  private void setPurchaseUnit(DObInitialStockUpload initialStockUpload, String purchaseUnitCode) {
    CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
    initialStockUpload.setPurchaseUnitId(purchaseUnit.getId());
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setStorehouseRep(CObStorehouseGeneralModelRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setPlantRep(CObPlantGeneralModelRep plantRep) {
    this.plantRep = plantRep;
  }

  public void setInitialStockUploadRep(DObInitialStockUploadRep initialStockUploadRep) {
    this.initialStockUploadRep = initialStockUploadRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setStoreKeeperRep(StoreKeeperRep storeKeeperRep) {
    this.storeKeeperRep = storeKeeperRep;
  }

  public void setItemVendorRecordRep(ItemVendorRecordRep itemVendorRecordRep) {
    this.itemVendorRecordRep = itemVendorRecordRep;
  }
}
