package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import io.reactivex.Observable;

public class IObNoteReceivablesDetailsUpdateCommand
    implements ICommand<IObMonetaryNotesDetailsValueObject> {

  private CommandUtils commandUtils;
  private DObMonetaryNotesRep monetaryNotesRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private IObMonetaryNotesDetailsRep monetaryNotesDetailsRep;
  private LObDepotRep depotRep;

  public IObNoteReceivablesDetailsUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObMonetaryNotesDetailsValueObject valueObject)
      throws Exception {

    String notesReceivableCode = valueObject.getNotesReceivableCode();
    DObMonetaryNotes monetaryNote = monetaryNotesRep.findOneByUserCode(notesReceivableCode);
    IObMonetaryNotesDetails noteDetails = updateNoteDetails(valueObject, notesReceivableCode);
    updateModificationInfoAndDate(monetaryNote, noteDetails);

    monetaryNote.setHeaderDetail(IDObMonetaryNotes.detailsAttr, noteDetails);

    monetaryNotesRep.update(monetaryNote);

    return Observable.just(notesReceivableCode);
  }

  private IObMonetaryNotesDetails updateNoteDetails(
      IObMonetaryNotesDetailsValueObject valueObject, String notesReceivableCode) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);
    IObMonetaryNotesDetails noteDetails =
        monetaryNotesDetailsRep.findOneByRefInstanceId(notesReceivablesGeneralModel.getId());
    noteDetails.setNoteNumber(valueObject.getNoteNumber());
    noteDetails.setNoteBank(valueObject.getNoteBank());
    setDueDate(noteDetails, valueObject.getDueDate());
    setDepotId(valueObject.getDepotCode(), noteDetails);
    return noteDetails;
  }

  private void setDepotId(String depotCode, IObMonetaryNotesDetails NoteDetails) {
    if (depotCode != null) {
      LObDepot depot = depotRep.findOneByUserCode(depotCode);
      NoteDetails.setDepotId(depot.getId());
      return;
    }
    NoteDetails.setDepotId(null);
  }

  private void setDueDate(IObMonetaryNotesDetails NoteDetails, String dueDate) {
    if (dueDate == null) {
      NoteDetails.setDueDate(null);
      return;
    }
    NoteDetails.setDueDate(commandUtils.getDateTime(dueDate));
  }

  private void updateModificationInfoAndDate(
      DObMonetaryNotes monetaryNotes, IObMonetaryNotesDetails noteDetails) {
    commandUtils.setModificationInfoAndModificationDate(noteDetails);
    commandUtils.setModificationInfoAndModificationDate(monetaryNotes);
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setMonetaryNotesDetailsRep(IObMonetaryNotesDetailsRep monetaryNotesDetailsRep) {
    this.monetaryNotesDetailsRep = monetaryNotesDetailsRep;
  }

  public void setDepotRep(LObDepotRep depotRep) {
    this.depotRep = depotRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
