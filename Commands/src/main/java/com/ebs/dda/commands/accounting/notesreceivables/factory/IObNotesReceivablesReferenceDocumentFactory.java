package com.ebs.dda.commands.accounting.notesreceivables.factory;

import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

import javax.transaction.NotSupportedException;

public class IObNotesReceivablesReferenceDocumentFactory {

  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public IObMonetaryNotesReferenceDocuments createNotesReceivableReferenceDocument(String type)
      throws Exception {

    if (isNotesReceivable(type)) {
      return new IObNotesReceivableReferenceDocumentNotesReceivable();
    } else if (isSalesOrder(type)) {
      return new IObNotesReceivableReferenceDocumentSalesOrder();
    } else if (isSalesInvoice(type)) {
      return new IObNotesReceivableReferenceDocumentSalesInvoice();
    }

    throw new NotSupportedException("Not supported ReferenceDocument type");
  }

  public void setReferenceDocumentId(
      IObMonetaryNotesReferenceDocumentsValueObject valueObject,
      IObMonetaryNotesReferenceDocuments notesReceivableReferenceDocument){

    if (isNotesReceivable(valueObject.getDocumentType())) {
      ((IObNotesReceivableReferenceDocumentNotesReceivable) notesReceivableReferenceDocument)
          .setDocumentId(getNotesReceivsbleDocumentId(valueObject.getRefDocumentCode()));
    } else if (isSalesOrder(valueObject.getDocumentType())) {
      ((IObNotesReceivableReferenceDocumentSalesOrder) notesReceivableReferenceDocument)
          .setDocumentId(getSalesOrderDocumentId(valueObject.getRefDocumentCode()));
    } else if (isSalesInvoice(valueObject.getDocumentType())) {
      ((IObNotesReceivableReferenceDocumentSalesInvoice) notesReceivableReferenceDocument)
          .setDocumentId(getSalesInvoiceDocumentId(valueObject.getRefDocumentCode()));
    }
  }

  private Long getSalesInvoiceDocumentId(String refDocumentCode) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(refDocumentCode);
    return salesInvoiceGeneralModel.getId();
  }

  private Long getSalesOrderDocumentId(String refDocumentCode) {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(refDocumentCode);
    return salesOrderGeneralModel.getId();
  }

  private Long getNotesReceivsbleDocumentId(String refDocumentCode) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(refDocumentCode);
    return notesReceivablesGeneralModel.getId();
  }

  private boolean isNotesReceivable(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name());
  }

  private boolean isSalesOrder(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name());
  }

  private boolean isSalesInvoice(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name());
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }
}
