package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;

import io.reactivex.Observable;

public abstract class DObPaymentRequestCreateCommand
				implements ICommand<DObPaymentRequestCreateValueObject> {

	private final DObPaymentRequestStateMachine stateMachine;
	private final CommandUtils commandUtils;
	private DObPaymentRequestRep paymentRequestRep;
	private CObPurchasingUnitRep purchasingUnitRep;
	private DocumentObjectCodeGenerator documentObjectCodeGenerator;
	public DObPaymentRequestCreateCommand(DObPaymentRequestStateMachine stateMachine){
		this.stateMachine = stateMachine;
		commandUtils = new CommandUtils();
	}

	@Override
	public Observable<String> executeCommand(
					DObPaymentRequestCreateValueObject paymentRequestValueObject) throws Exception {
		String userCode = documentObjectCodeGenerator
						.generateUserCode(DObPaymentRequest.class.getSimpleName());
		DObPaymentRequest paymentRequest = new DObPaymentRequest();
		IObPaymentRequestPaymentDetails paymentDetails = createPaymentDetailsInstance(
						paymentRequestValueObject.getDueDocumentType());
		IObPaymentRequestCompanyData paymentRequestCompanyData = new IObPaymentRequestCompanyData();
		findReferenceDocument(paymentRequestValueObject);
		setPaymentRequestData(paymentRequestValueObject, paymentRequest, userCode);
		setPaymentRequestCompanyData(paymentRequest, paymentRequestValueObject,
						paymentRequestCompanyData);
		setPaymentDetailsData(paymentRequest, paymentRequestValueObject, paymentDetails);
		paymentRequestRep.create(paymentRequest);

		return Observable.just(userCode);
	}

	private void setPaymentRequestCompanyData(DObPaymentRequest paymentRequest,
					DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestCompanyData paymentRequestCompanyData) {
		commandUtils.setCreationAndModificationInfoAndDate(paymentRequestCompanyData);
		setPurchaseUnit(paymentRequestCompanyData, paymentRequestValueObject.getBusinessUnitCode());
		setCompany(paymentRequestCompanyData, paymentRequestValueObject);
		paymentRequest.setHeaderDetail(IDObPaymentRequest.companyDataAttr,
						paymentRequestCompanyData);
	}

	protected abstract void setCompany(IObPaymentRequestCompanyData paymentRequestCompanyData,
					DObPaymentRequestCreateValueObject paymentRequestValueObject);

	private void setPaymentDetailsData(DObPaymentRequest paymentRequest,
					DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {
		commandUtils.setCreationAndModificationInfoAndDate(paymentDetails);
		paymentDetails.setPaymentForm(String.valueOf(paymentRequestValueObject.getPaymentForm()));
		setBusinessPartner(paymentRequestValueObject, paymentDetails);
		setReferenceDocument(paymentRequestValueObject, paymentDetails);
		setReferenceDocumentCurrency(paymentRequestValueObject, paymentDetails);
		paymentRequest.setHeaderDetail(IDObPaymentRequest.paymentdetailsAttr, paymentDetails);
	}

	protected abstract void findReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject);

	protected abstract void setReferenceDocumentCurrency(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails);

	protected abstract void setReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails  paymentDetails);

	protected abstract void setBusinessPartner(
					DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails);

	protected abstract IObPaymentRequestPaymentDetails createPaymentDetailsInstance(
					DueDocumentTypeEnum.DueDocumentType documentType);

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	private void setPaymentRequestData(DObPaymentRequestCreateValueObject paymentRequestValueObject,
					DObPaymentRequest paymentRequest, String userCode) throws Exception {
		commandUtils.setCreationAndModificationInfoAndDate(paymentRequest);
		stateMachine.initObjectState(paymentRequest);
		paymentRequest.setPaymentType(String.valueOf(paymentRequestValueObject.getPaymentType()));
		paymentRequest.setUserCode(userCode);
		paymentRequest.setDocumentOwnerId(paymentRequestValueObject.getDocumentOwnerId());
	}

	private void setPurchaseUnit(IObPaymentRequestCompanyData paymentRequestCompanyData,
					String purchaseUnitCode) {
		CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
		paymentRequestCompanyData.setPurchaseUnitId(purchaseUnit.getId());
	}

	public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
		this.paymentRequestRep = paymentRequestRep;
	}

	public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
		this.purchasingUnitRep = purchasingUnitRep;
	}

	public void setDocumentObjectCodeGenerator(
					DocumentObjectCodeGenerator documentObjectCodeGenerator) {
		this.documentObjectCodeGenerator = documentObjectCodeGenerator;
	}

}
