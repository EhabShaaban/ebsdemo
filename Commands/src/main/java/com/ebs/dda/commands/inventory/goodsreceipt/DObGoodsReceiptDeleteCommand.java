package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptRep;
import io.reactivex.Observable;

public class DObGoodsReceiptDeleteCommand implements ICommand {

  DObGoodsReceiptRep dObGoodsReceiptRep;
  EntityLockCommand entityLockCommand;

  public void setdObGoodsReceiptRep(DObGoodsReceiptRep dObGoodsReceiptRep) {
    this.dObGoodsReceiptRep = dObGoodsReceiptRep;
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  @Override
  public Observable<String> executeCommand(Object object) throws Exception {
    String userCode = (String) object;
    LockDetails lockDetails = null;

    try {
      lockDetails = entityLockCommand.lockAllSections(userCode);
      DObGoodsReceipt dObGoodsReceipt = readEntityByCode(userCode);
      checkIfAllowedAction(dObGoodsReceipt, IActionsNames.DELETE);
      dObGoodsReceiptRep.delete(dObGoodsReceipt);
      entityLockCommand.unlockAllSections(userCode);
      return Observable.just("SUCCESS");

    } catch (DeleteNotAllowedPerStateException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(userCode);
      }
      throw e;
    }
  }

  protected void checkIfAllowedAction(DObGoodsReceipt entity, String allowedAction)
      throws Exception {
    DObGoodsReceiptStateMachine goodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
    goodsReceiptStateMachine.initObjectState(entity);
    try {
      goodsReceiptStateMachine.isAllowedAction(allowedAction);
    } catch (ActionNotAllowedPerStateException e) {
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException();
      deleteNotAllowedPerStateException.initCause(e);
      throw deleteNotAllowedPerStateException;
    }
  }

  private DObGoodsReceipt readEntityByCode(String userCode) throws InstanceNotExistException {
    DObGoodsReceipt dObGoodsReceipt = dObGoodsReceiptRep.findOneByUserCode(userCode);
    if (dObGoodsReceipt == null) {
      throw new InstanceNotExistException();
    }
    return dObGoodsReceipt;
  }
}
