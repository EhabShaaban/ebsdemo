package com.ebs.dda.commands.accounting.journalentry.create;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.factories.IObJournalEntryItemFactory;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObInitialBalanceUploadJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObInitialBalanceUploadJournalEntryRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DObJournalEntryCreateInintialBalanceUploadCommand implements ICommand<String> {

  public static final String LOCAL_VENDORS = SubLedgers.Local_Vendors.toString();
  protected static final String ACTIVE_STATE = "Active";
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private final CommandUtils commandUtils;
  private DObInitialBalanceUploadJournalEntryRep journalEntryRep;
  private DObInitialBalanceUploadJournalEntryPreparationGeneralModelRep
      journalEntryPreparationGeneralModelRep;
  private DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep
      journalEntryItemPreparationGeneralModelRep;

  public DObJournalEntryCreateInintialBalanceUploadCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(String initialBalanceUploadCode) throws Exception {
    DObInitialBalanceUploadJournalEntryPreparationGeneralModel preparationGeneralModel =
        journalEntryPreparationGeneralModelRep.findOneByCode(initialBalanceUploadCode);

    DObInitialBalanceUploadJournalEntry journalEntry =
        prepareJournalEntryData(preparationGeneralModel);
    prepareJournalEntryItems(journalEntry, preparationGeneralModel);
    journalEntryRep.create(journalEntry);
    return Observable.just(journalEntry.getUserCode());
  }

  private void prepareJournalEntryItems(
      DObInitialBalanceUploadJournalEntry journalEntry,
      DObInitialBalanceUploadJournalEntryPreparationGeneralModel preparationGeneralModel) {
    List<DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel>
        initialBalanceUploadJournalEntryItemPreparationGeneralModels =
            journalEntryItemPreparationGeneralModelRep.findByCode(
                preparationGeneralModel.getCode());
    for (DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel item :
        initialBalanceUploadJournalEntryItemPreparationGeneralModels) {
      IObJournalEntryItem journalEntryItem =
          IObJournalEntryItemFactory.initJournalEntryItemInstance(item.getSubledger());
      commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
      journalEntryItem.setAccountId(item.getGlAccountId());
      setAmount(item, journalEntryItem);
      journalEntryItem.setCompanyCurrencyPrice(item.getCurrencyPrice());
      journalEntryItem.setDocumentCurrencyId(item.getCurrencyId());
      journalEntryItem.setCompanyCurrencyId(preparationGeneralModel.getCompanyCurrencyId());
      if (item.getSubledger().equals(LOCAL_VENDORS))
        journalEntryItem.setSubAccountId(item.getVendorId());

      journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, journalEntryItem);
    }
  }

  private void setAmount(
      DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel item,
      IObJournalEntryItem journalEntryItem) {
    if (item.getCredit() != null) {
      journalEntryItem.setCredit(new BigDecimal(item.getCredit()));
      journalEntryItem.setDebit(BigDecimal.ZERO);
    } else {
      journalEntryItem.setDebit(new BigDecimal(item.getDebit()));
      journalEntryItem.setCredit(BigDecimal.ZERO);
    }
  }

  private DObInitialBalanceUploadJournalEntry prepareJournalEntryData(
      DObInitialBalanceUploadJournalEntryPreparationGeneralModel preparationGeneralModel) {
    DObInitialBalanceUploadJournalEntry initialBalanceUploadJournalEntry =
        new DObInitialBalanceUploadJournalEntry();
    commandUtils.setCreationAndModificationInfoAndDate(initialBalanceUploadJournalEntry);
    initialBalanceUploadJournalEntry.setDocumentReferenceId(preparationGeneralModel.getId());
    initialBalanceUploadJournalEntry.setCompanyId(preparationGeneralModel.getCompanyId());
    initialBalanceUploadJournalEntry.setPurchaseUnitId(preparationGeneralModel.getBusinessUnitId());
    initialBalanceUploadJournalEntry.setDueDate(new DateTime());
    initialBalanceUploadJournalEntry.setFiscalPeriodId(preparationGeneralModel.getFiscalPeriodId());
    setCurrentStates(initialBalanceUploadJournalEntry);
    setUserCode(initialBalanceUploadJournalEntry);
    return initialBalanceUploadJournalEntry;
  }

  private void setUserCode(DObInitialBalanceUploadJournalEntry initialBalanceUploadJournalEntry) {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName());
    initialBalanceUploadJournalEntry.setUserCode(userCode);
  }

  private void setCurrentStates(
      DObInitialBalanceUploadJournalEntry intialBalanceUploadJournalEntry) {
    Set<String> currentStates = new HashSet<>();
    currentStates.add(ACTIVE_STATE);
    intialBalanceUploadJournalEntry.setCurrentStates(currentStates);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setJournalEntryRep(DObInitialBalanceUploadJournalEntryRep journalEntryRep) {
    this.journalEntryRep = journalEntryRep;
  }

  public void setJournalEntryPreparationGeneralModelRep(
      DObInitialBalanceUploadJournalEntryPreparationGeneralModelRep
          journalEntryPreparationGeneralModelRep) {
    this.journalEntryPreparationGeneralModelRep = journalEntryPreparationGeneralModelRep;
  }

  public void setJournalEntryItemPreparationGeneralModelRep(
      DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep
          journalEntryItemPreparationGeneralModelRep) {
    this.journalEntryItemPreparationGeneralModelRep = journalEntryItemPreparationGeneralModelRep;
  }
}
