package com.ebs.dda.commands.inventory.stockavailability.factory;

import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsIssueStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsReceiptStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByInitialStockUploadStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.IUpdateStockAvailabilityStrategy;

public class UpdateStockAvailabilityStrategyFactory {
  private UpdateStockAvailabilityByInitialStockUploadStrategy byInitialStockUploadStrategy;
  private UpdateStockAvailabilityByGoodsIssueStrategy byGoodsIssueStrategy;
  private UpdateStockAvailabilityByGoodsReceiptStrategy byGoodsReceiptStrategy;

  public IUpdateStockAvailabilityStrategy getStockAvailabilityUpdateStrategyByType(
      String type) {
    if (isPartOf("GI", type)) {
      return byGoodsIssueStrategy;
    } else if (isPartOf("GR", type)) {
      return byGoodsReceiptStrategy;
    } else if (isPartOf("ISU", type)) {
      return byInitialStockUploadStrategy;
    }
    throw new IllegalArgumentException("Unsupported Inventory Type");
  }

  private boolean isPartOf(String type, String inventoryType) {
    if (inventoryType != null) {
      return inventoryType.contains(type);
    }
    return false;
  }

  public void setByInitialStockUploadStrategy(UpdateStockAvailabilityByInitialStockUploadStrategy byInitialStockUploadStrategy) {
    this.byInitialStockUploadStrategy = byInitialStockUploadStrategy;
  }

  public void setByGoodsIssueStrategy(UpdateStockAvailabilityByGoodsIssueStrategy byGoodsIssueStrategy) {
    this.byGoodsIssueStrategy = byGoodsIssueStrategy;
  }

  public void setByGoodsReceiptStrategy(UpdateStockAvailabilityByGoodsReceiptStrategy byGoodsReceiptStrategy) {
    this.byGoodsReceiptStrategy = byGoodsReceiptStrategy;
  }
}
