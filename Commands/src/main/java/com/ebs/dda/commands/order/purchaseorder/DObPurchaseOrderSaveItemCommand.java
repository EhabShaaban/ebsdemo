package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsValueObject;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import io.reactivex.Observable;

public class DObPurchaseOrderSaveItemCommand implements ICommand<IObOrderLineDetailsValueObject> {
  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private EntityLockCommand<DObOrderDocument> entityLockCommand;

  public DObPurchaseOrderSaveItemCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObOrderLineDetailsValueObject orderLineDetailsValueObject) throws Exception {
    String purchaseOrderCode = orderLineDetailsValueObject.getPurchaseOrderCode();
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    IObOrderLineDetails orderLineDetails = new IObOrderLineDetails();
    prepareOrderLineDetail(purchaseOrder, orderLineDetails, orderLineDetailsValueObject);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void prepareOrderLineDetail(
      DObPurchaseOrder purchaseOrder,
      IObOrderLineDetails orderLineDetails,
      IObOrderLineDetailsValueObject orderLineDetailsValueObject) {
    setOrderLineDetailsSysInfo(orderLineDetails, purchaseOrder);
    setOrderLineDetailsData(orderLineDetails, orderLineDetailsValueObject);
    purchaseOrder.addLineDetail(IDObPurchaseOrder.lineAttr, orderLineDetails);
  }

  private void setOrderLineDetailsData(
      IObOrderLineDetails orderLineDetails,
      IObOrderLineDetailsValueObject orderLineDetailsValueObject) {
    MObItemGeneralModel itemGeneralModel =
        itemGeneralModelRep.findByUserCode(orderLineDetailsValueObject.getItemCode());
    orderLineDetails.setItemId(itemGeneralModel.getId());
    orderLineDetails.setUnitOfMeasureId(itemGeneralModel.getBasicUnitOfMeasure());
    orderLineDetails.setQuantityInDn(orderLineDetailsValueObject.getQtyInDn());
  }

  private void setOrderLineDetailsSysInfo(
      IObOrderLineDetails orderLineDetails, DObPurchaseOrder purchaseOrder) {

    orderLineDetails.setRefInstanceId(purchaseOrder.getId());
    commandUtils.setCreationAndModificationInfoAndDate(orderLineDetails);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
