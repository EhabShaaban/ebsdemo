package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderData;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataRep;
import io.reactivex.Observable;

public class IObSalesOrderDataUpdateCommand implements ICommand<IObSalesOrderDataValueObject> {

  private CommandUtils commandUtils;
  private DObSalesOrderRep salesOrderRep;
  private IObSalesOrderDataRep salesOrderDataRep;
  private CObCurrencyRep currencyRep;
  private CObPaymentTermsRep paymentTermsRep;

  public IObSalesOrderDataUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObSalesOrderDataValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    IObSalesOrderData salesOrderData =
        getIObSalesOrderDataWithUpdatedValues(salesOrder.getId(), valueObject);

    commandUtils.setModificationInfoAndModificationDate(salesOrderData);
    commandUtils.setModificationInfoAndModificationDate(salesOrder);
    salesOrder.setHeaderDetail(IDObSalesOrder.salesOrderDataAttr, salesOrderData);
    salesOrderRep.update(salesOrder);
    return Observable.just("Success");
  }

  private IObSalesOrderData getIObSalesOrderDataWithUpdatedValues(
      Long salesOrderId, IObSalesOrderDataValueObject valueObject) {
    IObSalesOrderData salesOrderData = salesOrderDataRep.findOneByRefInstanceId(salesOrderId);
    setPaymentTermId(salesOrderData, valueObject.getPaymentTermCode());
    setCurrencyId(salesOrderData, valueObject.getCurrencyCode());
    salesOrderData.setCustomerAddressId(valueObject.getCustomerAddressId());
    salesOrderData.setContactPersonId(valueObject.getContactPersonId());
    setExpectedDeliveryDate(salesOrderData, valueObject.getExpectedDeliveryDate());
    setNotes(salesOrderData, valueObject.getNotes());
    return salesOrderData;
  }

  private void setNotes(IObSalesOrderData salesOrderData, String notes) {
    if (notes != null && notes.trim().isEmpty()) {
      salesOrderData.setNotes(null);
    } else {
      salesOrderData.setNotes(notes);
    }
  }

  private void setPaymentTermId(IObSalesOrderData salesOrderData, String paymentTermCode) {
    if (paymentTermCode == null) {
      salesOrderData.setPaymentTermId(null);
      return;
    }
    CObPaymentTerms paymentTerm = paymentTermsRep.findOneByUserCode(paymentTermCode);
    salesOrderData.setPaymentTermId(paymentTerm.getId());
  }

  private void setCurrencyId(IObSalesOrderData salesOrderData, String currencyCode) {
    if (currencyCode == null) {
      salesOrderData.setCurrencyId(null);
      return;
    }
    CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
    salesOrderData.setCurrencyId(currency.getId());
  }

  private void setExpectedDeliveryDate(
      IObSalesOrderData salesOrderData, String expectedDeliveryDate) {
    if (expectedDeliveryDate == null) {
      salesOrderData.setExpectedDeliveryDate(null);
      return;
    }
    salesOrderData.setExpectedDeliveryDate(commandUtils.getDateTime(expectedDeliveryDate));
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setPaymentTermsRep(CObPaymentTermsRep paymentTermsRep) {
    this.paymentTermsRep = paymentTermsRep;
  }

  public void setSalesOrderDataRep(IObSalesOrderDataRep salesOrderDataRep) {
    this.salesOrderDataRep = salesOrderDataRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
