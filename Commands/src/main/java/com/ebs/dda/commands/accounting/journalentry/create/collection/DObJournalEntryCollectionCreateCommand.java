package com.ebs.dda.commands.accounting.journalentry.create.collection;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObJournalEntryCreateCollectionValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObCollectionJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObCollectionJournalEntryRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class DObJournalEntryCollectionCreateCommand
    implements ICommand<DObJournalEntryCreateCollectionValueObject> {

  private static final String ACTIVE_STATE = "Active";
  IObAccountingDocumentActivationDetailsGeneralModel collectionActivationDetailsGeneralModel;
  IObAccountingDocumentActivationDetailsGeneralModelRep
      accountingDocumentActivationDetailsGeneralModelRep;
  DObCollectionGeneralModelRep dObCollectionGeneralModelRep;
  DObCollectionGeneralModel collectionGeneralModel;
  CObCompanyGeneralModelRep companyGeneralModelRep;
  DocumentObjectCodeGenerator documentObjectCodeGenerator;
  CObCompanyGeneralModel companyGeneralModel;
  CommandUtils commandUtils;
  CObPurchasingUnitGeneralModel purchasingUnitGeneralModel;
  CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  DObCollectionJournalEntryRep collectionJournalEntryRep;

  public DObJournalEntryCollectionCreateCommand() {
    commandUtils = new CommandUtils();
  }

  protected void executeTransaction(
      DObCollectionJournalEntry collectionJournalEntry, List<IObJournalEntryItem> journalEntryItems)
      throws Exception {
    collectionJournalEntry.setLinesDetails(IDObJournalEntry.journalItemAttr, journalEntryItems);

    collectionJournalEntryRep.create(collectionJournalEntry);
  }

  public DObCollectionJournalEntry executeParentCommand() {
    DObCollectionJournalEntry collectionJournalEntry = new DObCollectionJournalEntry();
    setJournalEntryData(collectionJournalEntry);
    return collectionJournalEntry;
  }

  protected void initGeneralCollectionVariables(String collectionCode) {
    collectionGeneralModel = dObCollectionGeneralModelRep.findOneByUserCode(collectionCode);
    collectionActivationDetailsGeneralModel =
        accountingDocumentActivationDetailsGeneralModelRep.findOneByCodeAndDocumentType(
            collectionCode, collectionGeneralModel.getCollectionDocumentType());
    String companyCode = collectionGeneralModel.getCompanyCode();
    companyGeneralModel = companyGeneralModelRep.findOneByUserCode(companyCode);
    String purchaseUnitCode = collectionGeneralModel.getPurchaseUnitCode();
    purchasingUnitGeneralModel = purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
  }

  private void setJournalEntryData(DObCollectionJournalEntry collectionJournalEntry) {
    setJournalEntryUserCode(collectionJournalEntry);
    setJournalEntryActiveState(collectionJournalEntry);
    commandUtils.setCreationAndModificationInfoAndDate(collectionJournalEntry);
    setCollectionDetailsData(collectionJournalEntry);
  }

  private void setCollectionDetailsData(DObCollectionJournalEntry collectionJournalEntry) {
    collectionJournalEntry.setDueDate(collectionActivationDetailsGeneralModel.getDueDate());
    collectionJournalEntry.setFiscalPeriodId(collectionActivationDetailsGeneralModel.getFiscalPeriodId());
    collectionJournalEntry.setCompanyId(companyGeneralModel.getId());
    collectionJournalEntry.setPurchaseUnitId(purchasingUnitGeneralModel.getId());
    collectionJournalEntry.setDocumentReferenceId(collectionGeneralModel.getId());
  }

  private void setJournalEntryActiveState(DObCollectionJournalEntry collectionJournalEntry) {
    Set<String> currentStates = new HashSet<>();
    currentStates.add(ACTIVE_STATE);
    collectionJournalEntry.setCurrentStates(currentStates);
  }

  private void setJournalEntryUserCode(DObCollectionJournalEntry collectionJournalEntry) {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName());
    collectionJournalEntry.setUserCode(userCode);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCompanyGeneralModelRep(CObCompanyGeneralModelRep companyGeneralModelRep) {
    this.companyGeneralModelRep = companyGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setDocumentObjectCodeGenerator(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  public void setAccountingDocumentActivationDetailsGeneralModelRep(
      IObAccountingDocumentActivationDetailsGeneralModelRep
          accountingDocumentActivationDetailsGeneralModelRep) {
    this.accountingDocumentActivationDetailsGeneralModelRep =
        accountingDocumentActivationDetailsGeneralModelRep;
  }

  public void setdObCollectionGeneralModelRep(
      DObCollectionGeneralModelRep dObCollectionGeneralModelRep) {
    this.dObCollectionGeneralModelRep = dObCollectionGeneralModelRep;
  }

  public void setCollectionJournalEntryRep(DObCollectionJournalEntryRep collectionJournalEntryRep) {
    this.collectionJournalEntryRep = collectionJournalEntryRep;
  }
}
