package com.ebs.dda.commands.order.purchaseorder.factory;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObImportPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObLocalPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import javax.transaction.NotSupportedException;

public class DObPurchaseOrderFactory {

  public DObPurchaseOrder createPurchaseOrder(String type) throws Exception {

    if (isPurchaseOrderLocal(type)) {
      return new DObLocalPurchaseOrder();
    }

    if (isPurchaseOrderImport(type)) {
      return new DObImportPurchaseOrder();
    }

    if (isPurchaseOrderService(type)) {
      return new DObServicePurchaseOrder();
    }

    throw new NotSupportedException("Not supported purchase order type");
  }

  private boolean isPurchaseOrderLocal(String typeCode) {
    return typeCode.equals(OrderTypeEnum.LOCAL_PO.name());
  }

  private boolean isPurchaseOrderImport(String typeCode) {
    return typeCode.equals(OrderTypeEnum.IMPORT_PO.name());
  }

  private boolean isPurchaseOrderService(String typeCode) {
    return typeCode.equals(OrderTypeEnum.SERVICE_PO.name());
  }
}
