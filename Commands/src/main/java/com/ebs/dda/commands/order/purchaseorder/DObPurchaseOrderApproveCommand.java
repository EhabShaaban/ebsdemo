package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderApproveAndRejectUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderApproveValueObject;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;

public class DObPurchaseOrderApproveCommand extends DObPurchaseOrderApproveAndRejectUtils
    implements ICommand<DObPurchaseOrderApproveValueObject> {

  private final CommandUtils commandUtils;
  private boolean isLastApprover;

  public DObPurchaseOrderApproveCommand() {
    this.commandUtils = new CommandUtils();
  }

  public Observable<String> executeCommand(DObPurchaseOrderApproveValueObject approveValueObject)
      throws Exception {
    String purchaseOrderCode = approveValueObject.getPurchaseOrderCode();
    entityLockCommand.lockAllSections(purchaseOrderCode);
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    IBDKUser loggedInUser = commandUtils.getLoggedInUserObject();
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());
    DateTime dicisionDatetime = new DateTime(DateTimeZone.UTC);
    ApprovalDecision decision = ApprovalDecision.APPROVED;
    IObOrderApprover currentApprover =
        getAndUpdateCurrentApprover(
            approveValueObject.getNotes(),
            loggedInUser,
            currentUndecidedCycle.getId(),
            decision,
            dicisionDatetime);

    if (isLastApprover(currentUndecidedCycle.getId(), loggedInUser.getUserId())) {
      updateCycleDecisionAndEndDate(currentUndecidedCycle, dicisionDatetime, decision);
      commandUtils.setModificationInfoAndModificationDate(currentUndecidedCycle);

      setPurchaseOrderTargetState(purchaseOrder, IPurchaseOrderActionNames.APPROVE_PO);
      setRemainingValue(purchaseOrder);
    }

    commandUtils.setModificationInfoAndModificationDate(currentApprover);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    updateDatabaseAndUnlock(currentApprover, currentUndecidedCycle, purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void setRemainingValue(DObPurchaseOrder purchaseOrder) {
    IObOrderLineDetailsSummaryGeneralModel summary =
        orderLineDetailsSummaryGeneralModelRep.findOneByUserCode(purchaseOrder.getUserCode());
    purchaseOrder.setRemaining(summary.getTotalAmountAfterDiscount());
  }

  private boolean isLastApprover(Long approvalCycleId, Long approverId) {
    List<IObOrderApprover> approversList =
        approverRep.findByRefInstanceIdOrderByIdAsc(approvalCycleId);
    isLastApprover = approversList.get(approversList.size() - 1).getApproverId() == approverId;
    return isLastApprover;
  }

  public boolean isLastApprover() {
    return isLastApprover;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
