package com.ebs.dda.commands.accounting.costing.services;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.IObInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemsCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CreateCostingItemsSectionService {
  private final CommandUtils commandUtils;
  private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
      goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private IObLandedCostFactorItemsSummaryGeneralModelRep
      landedCostFactorItemsSummaryGeneralModelRep;
  private IObLandedCostFactorItemsSummary landedCostFactorItemsSummary;

  public CreateCostingItemsSectionService(
      IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep,
      IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
      IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
          goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      IObLandedCostFactorItemsSummaryGeneralModelRep landedCostFactorItemsSummaryGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    this.invoiceSummaryGeneralModelRep = invoiceSummaryGeneralModelRep;
    this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
    this.goodsReceiptReceivedItemsGeneralModelRep = goodsReceiptReceivedItemsGeneralModelRep;
    this.goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep =
        goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep;
    this.itemGeneralModelRep = itemGeneralModelRep;
    this.landedCostFactorItemsSummaryGeneralModelRep = landedCostFactorItemsSummaryGeneralModelRep;
    this.commandUtils = new CommandUtils();
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public List<IObCostingItem> getCostingItems(
      IObCostingItemsCreationValueObject costingItemsCreationValueObject) {

    List<IObCostingItem> costingItems = new ArrayList<>();

    List<IObGoodsReceiptReceivedItemsGeneralModel> goodsReceiptReceivedItemsGeneralModels =
        getGoodsReceiptReceivedItemsGeneralModelsByGRCode(
            costingItemsCreationValueObject.getGoodsReceiptCode());

    for (IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptReceivedItemsGeneralModel :
        goodsReceiptReceivedItemsGeneralModels) {

      List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
          goodsReceiptPurchaseOrderReceivedItemsData =
              getGoodsReceiptPurchaseOrderReceivedItemsData(goodsReceiptReceivedItemsGeneralModel);

      setCostingItemsData(
          costingItems,
          costingItemsCreationValueObject,
          goodsReceiptPurchaseOrderReceivedItemsData);
    }
    return costingItems;
  }

  private BigDecimal getTotalAmountBeforeTaxes(String vendorInvoiceCode) {
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
            vendorInvoiceCode, IDocumentTypes.VENDOR_INVOICE);

    return new BigDecimal(invoiceSummaryGeneralModel.getTotalAmountBeforeTaxes());
  }

  private void setCostingItemsData(
      List<IObCostingItem> costingItems,
      IObCostingItemsCreationValueObject costingItemsCreationValueObject,
      List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
          goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModels) {

    for (IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel
        goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel :
            goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModels) {
      IObCostingItem costingItem =
          setCostingItem(
              costingItemsCreationValueObject,
              goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel);
      costingItems.add(costingItem);
    }
  }

  private IObCostingItem setCostingItem(
      IObCostingItemsCreationValueObject costingItemsCreationValueObject,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel
          goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel) {

    BigDecimal receivedQtyUoE =
        goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE();

    MObItemGeneralModel itemGeneralModel =
        getItem(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel.getItemCode());
    Long itemId = itemGeneralModel.getId();

    IObCostingItem costingItem = new IObCostingItem();
    commandUtils.setCreationAndModificationInfoAndDate(costingItem);
    costingItem.setItemId(itemId);

    costingItem.setUomId(
        goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel.getUnitOfEntryId());
    costingItem.setStockType(
        goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel.getStockType());
    costingItem.setQuantity(receivedQtyUoE);
    setCostingItemCostFields(costingItem, costingItemsCreationValueObject);
    return costingItem;
  }

  public void setCostingItemCostFields(
      IObCostingItem costingItem,
      IObCostingItemsCreationValueObject costingItemsCreationValueObject) {

    IObVendorInvoiceItemsGeneralModel invoiceItemGeneralModel =
        getVendorInvoiceItemGeneralModel(
            costingItemsCreationValueObject.getVendorInvoiceCode(),
            costingItem.getItemId(),
            costingItem.getUomId());

    BigDecimal totalAmountBeforeTaxes =
        getTotalAmountBeforeTaxes(invoiceItemGeneralModel.getInvoiceCode());

    BigDecimal itemTotalAmount = calculateItemTotalAmount(invoiceItemGeneralModel);

    setItemWeightIfNotExist(costingItem, totalAmountBeforeTaxes, itemTotalAmount);

    landedCostFactorItemsSummary =
        landedCostFactorItemsSummaryGeneralModelRep.findOneByCode(
            costingItemsCreationValueObject.getLandedCostCode());

    setItemUnitPrice(costingItem, itemTotalAmount, costingItemsCreationValueObject);

    setUnitLandedCost(costingItem, costingItemsCreationValueObject);
  }

  private void setItemWeightIfNotExist(
      IObCostingItem costingItem, BigDecimal totalAmountBeforeTaxes, BigDecimal itemTotalAmount) {
    if (costingItem.getId() == null) {
      BigDecimal itemWeight =
          getItemWeight(costingItem.getStockType(), totalAmountBeforeTaxes, itemTotalAmount);
      costingItem.setToBeConsideredInCostingPer(itemWeight);
    }
  }

  private BigDecimal calculateItemTotalAmount(IObInvoiceItemsGeneralModel invoiceItemGeneralModel) {
    return invoiceItemGeneralModel
        .getOrderUnitPrice()
        .multiply(invoiceItemGeneralModel.getQuantityInOrderUnit());
  }

  private MObItemGeneralModel getItem(String itemCode) {
    return itemGeneralModelRep.findByUserCode(itemCode);
  }

  private BigDecimal getItemWeight(
      String stockType, BigDecimal totalAmountBeforeTaxes, BigDecimal itemTotalAmount) {
    BigDecimal itemWeight = BigDecimal.ZERO;
    if (stockType.equals(StockTypeEnum.StockType.UNRESTRICTED_USE.toString())) {
      itemWeight = CommandUtils.getDivideResult(itemTotalAmount, totalAmountBeforeTaxes);
    }
    return itemWeight;
  }

  private void setItemUnitPrice(
      IObCostingItem costingItem,
      BigDecimal itemTotalAmount,
      IObCostingItemsCreationValueObject costingItemsCreationValueObject) {
    BigDecimal unitPrice = BigDecimal.ZERO;
    if (costingItem.getStockType().equals(StockTypeEnum.StockType.UNRESTRICTED_USE.toString())) {
      unitPrice =
          (CommandUtils.getDivideResult(itemTotalAmount, costingItem.getQuantity()))
              .multiply(costingItemsCreationValueObject.getCurrencyPrice());
    }

    boolean isLandedCostActual = costingItemsCreationValueObject.isLandedCostActual();
    if (isLandedCostActual) {
      costingItem.setActualUnitPrice(unitPrice);
    } else {
      costingItem.setEstimateUnitPrice(unitPrice);
    }
  }

  private void setUnitLandedCost(
      IObCostingItem costingItem,
      IObCostingItemsCreationValueObject costingItemsCreationValueObject) {
    boolean isLandedCostActual = costingItemsCreationValueObject.isLandedCostActual();
    if (isLandedCostActual) {
      setActualUnitLandedCost(
          costingItem, costingItem.getQuantity(), costingItem.getToBeConsideredInCostingPer());
    } else {
      setEstimateUnitLandedCost(
          costingItem, costingItem.getQuantity(), costingItem.getToBeConsideredInCostingPer());
    }
  }

  private BigDecimal calculateUnitLandedCost(
      BigDecimal quantity, BigDecimal itemWeight, BigDecimal totalAmount) {
    return CommandUtils.getDivideResult(totalAmount.multiply(itemWeight), quantity);
  }

  private void setActualUnitLandedCost(
      IObCostingItem costingItem, BigDecimal quantity, BigDecimal itemWeight) {
    BigDecimal unitLandedCost =
        calculateUnitLandedCost(
            quantity, itemWeight, landedCostFactorItemsSummary.getActualTotalAmount());
    costingItem.setActualUnitLandedCost(unitLandedCost);
  }

  private void setEstimateUnitLandedCost(
      IObCostingItem costingItem, BigDecimal quantity, BigDecimal itemWeight) {
    BigDecimal unitLandedCost =
        calculateUnitLandedCost(
            quantity, itemWeight, landedCostFactorItemsSummary.getEstimateTotalAmount());
    costingItem.setEstimateUnitLandedCost(unitLandedCost);
  }

  private List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
      getGoodsReceiptPurchaseOrderReceivedItemsData(
          IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptReceivedItemsGeneralModel) {
    return goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
        .findByGoodsReceiptCodeAndItemCode(
            goodsReceiptReceivedItemsGeneralModel.getGoodsReceiptCode(),
            goodsReceiptReceivedItemsGeneralModel.getItemCode());
  }

  private List<IObGoodsReceiptReceivedItemsGeneralModel>
      getGoodsReceiptReceivedItemsGeneralModelsByGRCode(String goodsReceiptCode) {
    List<IObGoodsReceiptReceivedItemsGeneralModel> goodsReceiptReceivedItemsGeneralModels =
        getGoodsReceiptReceivedItemsGeneralModels(goodsReceiptCode);
    return goodsReceiptReceivedItemsGeneralModels;
  }

  private IObVendorInvoiceItemsGeneralModel getVendorInvoiceItemGeneralModel(
      String vendorInvoiceCode, Long itemId, Long orderUnitId) {
    return vendorInvoiceItemsGeneralModelRep.findOneByInvoiceCodeAndItemIdAndOrderUnitId(
        vendorInvoiceCode, itemId, orderUnitId);
  }

  private List<IObGoodsReceiptReceivedItemsGeneralModel> getGoodsReceiptReceivedItemsGeneralModels(
      String userCode) {
    return goodsReceiptReceivedItemsGeneralModelRep.findByGoodsReceiptCode(userCode);
  }
}
