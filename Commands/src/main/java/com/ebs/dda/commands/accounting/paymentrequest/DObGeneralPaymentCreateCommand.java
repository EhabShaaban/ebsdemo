package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum.DueDocumentType;

public class DObGeneralPaymentCreateCommand extends DObPaymentWithReferenceDocumentCreateCommand<DObPaymentReferenceOrderGeneralModel> {

	public DObGeneralPaymentCreateCommand(DObPaymentRequestStateMachine stateMachine){
		super(stateMachine);
	}

	@Override
	protected void setCompany(IObPaymentRequestCompanyData paymentRequestCompanyData,
					DObPaymentRequestCreateValueObject paymentRequestValueObject) {
		DObPaymentReferenceOrderGeneralModel order = getRefDocument();
		paymentRequestCompanyData.setCompanyId(order.getCompanyId());
	}
	@Override
	protected void setReferenceDocumentCurrency(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {

	}

	@Override
	protected void setReferenceDocument(DObPaymentRequestCreateValueObject paymentRequestValueObject, IObPaymentRequestPaymentDetails paymentDetails) {
		DObPaymentReferenceOrderGeneralModel order = getRefDocument();
		((IObPaymentRequestPurchaseOrderPaymentDetails)paymentDetails).setDueDocumentId(order.getId());
	}

	@Override
	protected void setBusinessPartner(DObPaymentRequestCreateValueObject paymentRequestValueObject,
					IObPaymentRequestPaymentDetails paymentDetails) {

	}

	@Override
	protected IObPaymentRequestPaymentDetails createPaymentDetailsInstance(
					DueDocumentType documentType) {
		return new IObPaymentRequestPurchaseOrderPaymentDetails();
	}

}
