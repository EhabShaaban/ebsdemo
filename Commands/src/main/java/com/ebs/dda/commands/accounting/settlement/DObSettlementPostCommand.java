package com.ebs.dda.commands.accounting.settlement;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.infrastructure.tm.TransactionCallback;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementActionNames;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.settlements.*;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementActivationPreparationRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class DObSettlementPostCommand implements ICommand<DObSettlementActivateValueObject> {

  private DObSettlementRep settlementRep;
  private CommandUtils commandUtils;
  private DObSettlementStateMachine stateMachine;
  private TransactionManagerDelegate transactionManagerDelegate;
  private DObSettlementActivationPreparationRep settlementActivationPreparationRep;
  private CObFiscalYearRep fiscalYearRep;
  private static final String ACTIVE_STATE = "Active";


  public DObSettlementPostCommand() throws Exception {
    commandUtils = new CommandUtils();
    stateMachine = new DObSettlementStateMachine();
  }

  @Override
  public Observable<DObSettlement> executeCommand(DObSettlementActivateValueObject settlementActivateValueObject) throws Exception {
    DObSettlement settlement = settlementRep.findOneByUserCode(settlementActivateValueObject.getSettlementCode());
    transactionManagerDelegate.executeTransaction(
        new TransactionCallback() {
          protected void doTransaction() throws Exception {
            IObSettlementPostingDetails postingDetails = createPostingDetails(settlement.getId(), settlementActivateValueObject);
            updateSettlement(settlement, postingDetails);
          }
        });
    return Observable.just(settlement);
  }

  private void updateSettlement(
      DObSettlement settlement, IObSettlementPostingDetails postingDetails) throws Exception {
    this.commandUtils.setModificationInfoAndModificationDate(settlement);
    setSettlementPostState(createSettlementStateMachine(settlement));
    settlement.setHeaderDetail(IDObSettlement.postingDetailsAttr, postingDetails);
    settlementRep.updateAndClear(settlement);
  }

  private void setSettlementPostState(AbstractStateMachine settlementStateMachine)
      throws Exception {
    if (settlementStateMachine.canFireEvent(IDObSettlementActionNames.POST)) {
      settlementStateMachine.fireEvent(IDObSettlementActionNames.POST);
      settlementStateMachine.save();
    }
  }

  private IObSettlementPostingDetails createPostingDetails(Long settlementId, DObSettlementActivateValueObject settlementActivateValueObject) {
    Optional<DObSettlementActivationPreparation> settlementActivationPreparation =
        settlementActivationPreparationRep.findById(settlementId);
    IObSettlementPostingDetails postingDetails = new IObSettlementPostingDetails();
    String loggedInUser = commandUtils.getLoggedInUser();
    postingDetails.setAccountant(loggedInUser);

    DateTime journalEntryDateTime = commandUtils.getDateTime(settlementActivateValueObject.getJournalEntryDate());
    postingDetails.setDueDate(journalEntryDateTime);

    postingDetails.setActivationDate(new DateTime());
    postingDetails.setCurrencyPrice(settlementActivationPreparation.get().getCurrencyPrice());
    postingDetails.setExchangeRateId(settlementActivationPreparation.get().getExchangeRateId());
    postingDetails.setRefInstanceId(settlementId);
    postingDetails.setFiscalPeriodId(getActiveFiscalPeriodId(journalEntryDateTime));
    commandUtils.setCreationAndModificationInfoAndDate(postingDetails);
    return postingDetails;
  }

  private Long getActiveFiscalPeriodId(DateTime journalEntryDateTime) {

    List<CObFiscalYear> fiscalYears =
            fiscalYearRep.findByCurrentStatesLike("%" + ACTIVE_STATE + "%");
    for (CObFiscalYear fiscalYear : fiscalYears) {
      if (journalEntryDateTime.isAfter(fiscalYear.getFromDate())
              && journalEntryDateTime.isBefore(fiscalYear.getToDate())) {
        return fiscalYear.getId();
      }
    }
    throw new MissingSystemDataException("There is no active fiscal year");
  }

  private DObSettlementStateMachine createSettlementStateMachine(DObSettlement settlement)
      throws Exception {
    DObSettlementStateMachine settlementStateMachine = new DObSettlementStateMachine();
    settlementStateMachine.initObjectState(settlement);
    return settlementStateMachine;
  }

  public void setTransactionManagerDelegate(TransactionManagerDelegate transactionManagerDelegate) {
    this.transactionManagerDelegate = transactionManagerDelegate;
  }

  public void setSettlementRep(DObSettlementRep settlementRep) {
    this.settlementRep = settlementRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setSettlementActivationPreparationRep(
      DObSettlementActivationPreparationRep settlementActivationPreparationRep) {
    this.settlementActivationPreparationRep = settlementActivationPreparationRep;
  }
  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

}
