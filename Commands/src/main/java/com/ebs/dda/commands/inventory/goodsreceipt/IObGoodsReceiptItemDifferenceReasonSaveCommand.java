package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrder;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsData;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsDifferenceReasonValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class IObGoodsReceiptItemDifferenceReasonSaveCommand
    implements ICommand<IObGoodsReceiptReceivedItemsDifferenceReasonValueObject> {

  private PlatformTransactionManager transactionManager;
  private IUserAccountSecurityService userAccountSecurityService;
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptRecievedItemsGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptRecievedItemsRep;
  private DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep;
  private CommandUtils commandUtils;

  public IObGoodsReceiptItemDifferenceReasonSaveCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setDObGoodsReceiptPurchaseOrderRep(
      DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep) {
    this.dObGoodsReceiptPurchaseOrderRep = dObGoodsReceiptPurchaseOrderRep;
  }

  public void setGoodsReceiptRecievedItemsRep(
      IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptRecievedItemsRep) {
    this.goodsReceiptRecievedItemsRep = goodsReceiptRecievedItemsRep;
  }

  public void setGoodsReceiptRecievedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptRecievedItemsGeneralModelRep) {
    this.goodsReceiptRecievedItemsGeneralModelRep = goodsReceiptRecievedItemsGeneralModelRep;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  @Override
  public Observable<String> executeCommand(
      IObGoodsReceiptReceivedItemsDifferenceReasonValueObject valueObject) throws Exception {
    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptRecievedItemsData =
                getRecievedItemsDataByGoodsReceiptCodeAndItemCode(
                    valueObject.getGoodsReceiptCode(), valueObject.getItemCode());
            updateGoodsReceiptRecievedItemsData(goodsReceiptRecievedItemsData, valueObject);
            updateGoodsReceipt(valueObject.getGoodsReceiptCode());
          }
        });
    return Observable.just("SUCCESS");
  }

  private IObGoodsReceiptPurchaseOrderReceivedItemsData
      getRecievedItemsDataByGoodsReceiptCodeAndItemCode(String orderCode, String itemCode) {
    IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptRecievedItemsGeneralModel =
        goodsReceiptRecievedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
            orderCode, itemCode);
    return goodsReceiptRecievedItemsRep.findOneById(
        goodsReceiptRecievedItemsGeneralModel.getGRItemId());
  }

  private void updateGoodsReceiptRecievedItemsData(
      IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptRecievedItemsData,
      IObGoodsReceiptReceivedItemsDifferenceReasonValueObject
          goodsReceiptReceivedItemsDifferenceReasonValueObject) {

    goodsReceiptRecievedItemsData.setDifferenceReason(
        goodsReceiptReceivedItemsDifferenceReasonValueObject.getDifferenceReason());
    commandUtils.setModificationInfoAndModificationDate(goodsReceiptRecievedItemsData);
    goodsReceiptRecievedItemsRep.save(goodsReceiptRecievedItemsData);
  }

  private void updateGoodsReceipt(String grCode) {
    DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
        dObGoodsReceiptPurchaseOrderRep.findOneByUserCode(grCode);
    commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptPurchaseOrder);
    dObGoodsReceiptPurchaseOrderRep.save(dObGoodsReceiptPurchaseOrder);
  }
}
