package com.ebs.dda.commands.accounting.actualcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.costing.*;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPostingDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemRep;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPostingDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import io.reactivex.Observable;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ActualCostCalculateCommand {

  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep goodsReceiptPORep;
  private IObVendorInvoiceRemainingGeneralModelRep invoiceRemainingRep;
  private IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestRep;
  private IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep;
  private IObJournalEntryItemsGeneralModelRep journalEntryItemsRep;
  private LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep;
  private IObInvoiceSummaryGeneralModelRep invoiceSummaryRep;
  private IObCostingItemGeneralModelRep costingItemGeneralModelRep;
  private IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  private IObVendorInvoiceItemsGeneralModelRep invoiceItemsRep;
  private IObCostingItemRep costingItemRep;
  private DObCostingRep costingRep;
  private IUserAccountSecurityService userAccountSecurityService;
  private CommandUtils commandUtils;

  @Transactional
  public Observable<String> executeCommand(String goodsReceiptsCode) throws Exception {
    commandUtils = new CommandUtils();
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPOGeneralModel =
        goodsReceiptPORep.findByGoodsReceiptCode(goodsReceiptsCode);
    IObVendorInvoiceRemainingGeneralModel invoiceRemainingGeneralModel =
        invoiceRemainingRep.findOneByPurchaseOrderCodeAndInvoiceTypeIsIn(
            goodsReceiptPOGeneralModel.getPurchaseOrderCode(),
            new String[] {
              VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE,
              VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE
            });
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        invoiceSummaryRep.findOneByInvoiceCodeAndInvoiceType(
            invoiceRemainingGeneralModel.getUserCode(), IDocumentTypes.VENDOR_INVOICE);
    IObCostingDetailsGeneralModel grAccountingDocumentDetailsGeneralModel =
        costingDetailsGeneralModelRep.findOneByGoodsReceiptCode(goodsReceiptsCode);
    List<IObCostingItemGeneralModel> accountingDocumentItemList =
        costingItemGeneralModelRep.findByUserCode(
            grAccountingDocumentDetailsGeneralModel.getUserCode());
    List<IObVendorInvoiceItemsGeneralModel> invoiceItemsList =
        invoiceItemsRep.findByInvoiceCode(invoiceSummaryGeneralModel.getInvoiceCode());

    BigDecimal currencyPrice =
        getCurrencyPrice(goodsReceiptPOGeneralModel, invoiceRemainingGeneralModel);
    BigDecimal goodsInvoiceTotal =
        currencyPrice.multiply(new BigDecimal(invoiceSummaryGeneralModel.getTotalAmountBeforeTaxes()));
    BigDecimal landedCost =
        getLandedCost(goodsReceiptPOGeneralModel.getPurchaseOrderCode(), goodsInvoiceTotal);
    List<IObCostingItem> items =
        calculate(
            currencyPrice,
            landedCost,
            new BigDecimal(invoiceSummaryGeneralModel.getTotalAmountBeforeTaxes()),
            accountingDocumentItemList,
            invoiceItemsList);

    Optional<DObCosting> accountingDocument = costingRep.findById(items.get(0).getRefInstanceId());
    commandUtils.setModificationInfoAndModificationDate(accountingDocument.get());
    accountingDocument.get().setLinesDetails(IDObCosting.accountingDocumentItemsAttr, items);
    costingRep.save(accountingDocument.get());
    return Observable.just(accountingDocument.get().getUserCode());
  }

  private BigDecimal getCurrencyPrice(
      IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPOGeneralModel,
      IObVendorInvoiceRemainingGeneralModel invoiceRemainingGeneralModel) {
    boolean isFullyPaid = isFullyPaid(invoiceRemainingGeneralModel);
    if (isFullyPaid) {
      return getAveragePrice(goodsReceiptPOGeneralModel, invoiceRemainingGeneralModel);
    } else {
      // TODO: refactor remaining amount
      return BigDecimal.ZERO;
    }
  }

  private boolean isFullyPaid(IObVendorInvoiceRemainingGeneralModel invoiceRemainingGeneralModel) {
    if (invoiceRemainingGeneralModel.getRemaining().compareTo(BigDecimal.ZERO) == 0) {
      return true;
    } else {
      return false;
    }
  }

  private BigDecimal getAveragePrice(
      IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPOGeneralModel,
      IObVendorInvoiceRemainingGeneralModel invoiceRemainingGeneralModel) {
    BigDecimal averagePrice = BigDecimal.ZERO;
    List<IObPaymentRequestPaymentDetailsGeneralModel> downPaymentPaymentRequests =
        paymentRequestRep.findByDueDocumentCodeAndDueDocumentType(
            goodsReceiptPOGeneralModel.getPurchaseOrderCode(),
            DueDocumentTypeEnum.PURCHASEORDER.toString());
    List<IObPaymentRequestPaymentDetailsGeneralModel> invoicePaymentRequests =
        paymentRequestRep.findByDueDocumentCodeAndDueDocumentType(
            invoiceRemainingGeneralModel.getUserCode(), DueDocumentTypeEnum.INVOICE.toString());
    List<IObPaymentRequestPaymentDetailsGeneralModel> paymentRequests = new LinkedList<>();
    paymentRequests.addAll(downPaymentPaymentRequests);
    paymentRequests.addAll(invoicePaymentRequests);
    for (IObPaymentRequestPaymentDetailsGeneralModel paymentRequest : paymentRequests) {
      IObPaymentRequestPostingDetails paymentRequestPostingDetails =
          paymentRequestPostingDetailsRep.findOneByRefInstanceId(paymentRequest.getRefInstanceId());
      averagePrice = averagePrice.add(paymentRequestPostingDetails.getCurrencyPrice());
    }
    return averagePrice.divide(BigDecimal.valueOf(paymentRequests.size()));
  }

  private BigDecimal getLandedCost(String purchaseOrderCode, BigDecimal goodsInvoiceTotal) {
    BigDecimal landedCost = BigDecimal.ZERO;
    LObGlobalGLAccountConfigGeneralModel accountConfigGeneralModel =
        accountConfigGeneralModelRep.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);
    String accountCode = accountConfigGeneralModel.getAccountCode();
    List<IObJournalEntryItemGeneralModel> journalEntryItems =
        journalEntryItemsRep.findAllByAccountCodeAndSubAccountCode(accountCode, purchaseOrderCode);
    for (IObJournalEntryItemGeneralModel item : journalEntryItems) {
      landedCost = landedCost.add(new BigDecimal(item.getDebit()).multiply(new BigDecimal(item.getCompanyCurrencyPrice())));
    }
    return landedCost.subtract(goodsInvoiceTotal);
  }

  private List<IObCostingItem> calculate(
      BigDecimal currencyPrice,
      BigDecimal landedCost,
      BigDecimal totalAmount,
      List<IObCostingItemGeneralModel> accountingDocumentItemList,
      List<IObVendorInvoiceItemsGeneralModel> invoiceItemsList) {
    List<IObCostingItem> results = new LinkedList<>();
    for (IObCostingItemGeneralModel item : accountingDocumentItemList) {
      if (item.getToBeConsideredInCostingPer().compareTo(BigDecimal.ZERO) == 0) {
        continue;
      }
      BigDecimal receivedQuantity = item.getQuantity();
      Optional<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItem =
          invoiceItemsList.stream()
              .filter(
                  selectedItem ->
                      (selectedItem.getItemCode().equals(item.getItemCode()))
                          && selectedItem.getOrderUnitCode().equals(item.getUomCode()))
              .findFirst();
      BigDecimal quantity = vendorInvoiceItem.get().getQuantityInOrderUnit();
      BigDecimal price = vendorInvoiceItem.get().getPrice();
      BigDecimal factor = vendorInvoiceItem.get().getConversionFactorToBase();
      BigDecimal total = quantity.multiply(price).multiply(factor);
      BigDecimal itemAverageWeight = total.divide(totalAmount);
      BigDecimal itemUnitPrice = quantity.multiply(price).divide(receivedQuantity);
      BigDecimal totalItemLandedCost = landedCost.multiply(itemAverageWeight);
      BigDecimal unitLandedCost = totalItemLandedCost.divide((receivedQuantity.multiply(factor)));
      BigDecimal unitPrice = itemUnitPrice.multiply(currencyPrice);
      IObCostingItem updatedItem = costingItemRep.findOneById(item.getId());
      updatedItem.setActualUnitPrice(unitPrice);
      updatedItem.setActualUnitLandedCost(unitLandedCost);
      commandUtils.setModificationInfoAndModificationDate(updatedItem);
      results.add(updatedItem);
    }
    return results;
  }

  public void setGoodsReceiptPORep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep goodsReceiptPORep) {
    this.goodsReceiptPORep = goodsReceiptPORep;
  }

  public void setInvoiceRemainingRep(IObVendorInvoiceRemainingGeneralModelRep invoiceRemainingRep) {
    this.invoiceRemainingRep = invoiceRemainingRep;
  }

  public void setPaymentRequestRep(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }

  public void setPaymentRequestPostingDetailsRep(
      IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep) {
    this.paymentRequestPostingDetailsRep = paymentRequestPostingDetailsRep;
  }

  public void setJournalEntryItemsRep(IObJournalEntryItemsGeneralModelRep journalEntryItemsRep) {
    this.journalEntryItemsRep = journalEntryItemsRep;
  }

  public void setAccountConfigGeneralModelRep(
      LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep) {
    this.accountConfigGeneralModelRep = accountConfigGeneralModelRep;
  }

  public void setInvoiceSummaryRep(IObInvoiceSummaryGeneralModelRep invoiceSummaryRep) {
    this.invoiceSummaryRep = invoiceSummaryRep;
  }

  public void setCostingItemGeneralModelRep(
      IObCostingItemGeneralModelRep costingItemGeneralModelRep) {
    this.costingItemGeneralModelRep = costingItemGeneralModelRep;
  }

  public void setCostingDetailsGeneralModelRep(
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
  }

  public void setInvoiceItemsRep(IObVendorInvoiceItemsGeneralModelRep invoiceItemsRep) {
    this.invoiceItemsRep = invoiceItemsRep;
  }

  public void setCostingItemRep(IObCostingItemRep costingItemRep) {
    this.costingItemRep = costingItemRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setCostingRep(DObCostingRep costingRep) {
    this.costingRep = costingRep;
  }
}
