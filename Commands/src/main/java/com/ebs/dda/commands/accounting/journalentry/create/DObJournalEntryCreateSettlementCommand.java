package com.ebs.dda.commands.accounting.journalentry.create;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.factories.IObJournalEntryItemFactory;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObSettlementJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.journalentry.DObSettlementJournalEntryRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DObJournalEntryCreateSettlementCommand implements ICommand<DObJournalEntryCreateValueObject> {

	private CommandUtils commandUtils;
	private DObSettlementJournalEntryRep journalEntryRep;
	private DObSettlementJournalEntryPreparationGeneralModelRep journalEntryPreparationGeneralModelRep;
	private IObSettlementAccountDetailsGeneralModelRep accountDetailsGeneralModelRep;
	protected static final String ACTIVE_STATE = "Active";

	public DObJournalEntryCreateSettlementCommand() {
		commandUtils = new CommandUtils();
	}

	@Override
	public Observable<String> executeCommand(DObJournalEntryCreateValueObject valueObject) throws Exception {
		DObSettlementJournalEntryPreparationGeneralModel preparationGeneralModel = journalEntryPreparationGeneralModelRep
				.findOneByCode(valueObject.getUserCode());

		DObSettlementJournalEntry journalEntry = prepareJournalEntryData(preparationGeneralModel);
		journalEntry.setUserCode(valueObject.getJournalEntryCode());

		prepareJournalEntryItems(journalEntry, preparationGeneralModel);
		journalEntryRep.create(journalEntry);
		return Observable.just(journalEntry.getUserCode());
	}

	private void prepareJournalEntryItems(DObSettlementJournalEntry journalEntry, DObSettlementJournalEntryPreparationGeneralModel preparationGeneralModel) {
		List<IObSettlementAccountDetailsGeneralModel> accountDetailsGeneralModel = accountDetailsGeneralModelRep
						.findAllByDocumentCode(preparationGeneralModel.getCode());
		for (IObSettlementAccountDetailsGeneralModel account : accountDetailsGeneralModel) {
			IObJournalEntryItem journalEntryItem = IObJournalEntryItemFactory
							.initJournalEntryItemInstance(account.getSubLedger());
			commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
			journalEntryItem.setAccountId(account.getGlAccountId());
			setAmount(account, journalEntryItem);
			journalEntryItem.setCompanyCurrencyPrice(preparationGeneralModel.getCurrencyPrice());
			journalEntryItem.setCompanyExchangeRateId(preparationGeneralModel.getExchangeRateId());
			journalEntryItem.setDocumentCurrencyId(preparationGeneralModel.getDocumentCurrencyId());
			journalEntryItem.setCompanyCurrencyId(preparationGeneralModel.getCompanyCurrencyId());
			journalEntryItem.setSubAccountId(account.getGlSubAccountId());

			journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, journalEntryItem);
		}
	}

	private void setAmount(IObSettlementAccountDetailsGeneralModel account, IObJournalEntryItem journalEntryItem) {
		if (account.getAccountingEntry().equals(AccountingEntry.CREDIT.name())) {
			journalEntryItem.setCredit(account.getAmount());
			journalEntryItem.setDebit(BigDecimal.ZERO);
		} else if (account.getAccountingEntry().equals(AccountingEntry.DEBIT.name())) {
			journalEntryItem.setDebit(account.getAmount());
			journalEntryItem.setCredit(BigDecimal.ZERO);
		}
	}

	private DObSettlementJournalEntry prepareJournalEntryData(DObSettlementJournalEntryPreparationGeneralModel preparationGeneralModel) {
		DObSettlementJournalEntry settlementJournalEntry = new DObSettlementJournalEntry();
		commandUtils.setCreationAndModificationInfoAndDate(settlementJournalEntry);
		settlementJournalEntry.setDocumentReferenceId(preparationGeneralModel.getId());
		settlementJournalEntry.setCompanyId(preparationGeneralModel.getCompanyId());
		settlementJournalEntry.setPurchaseUnitId(preparationGeneralModel.getBusinessUnitId());
		settlementJournalEntry.setDueDate(preparationGeneralModel.getDueDate());
		settlementJournalEntry.setFiscalPeriodId(preparationGeneralModel.getFiscalPeriodId());
		setCurrentStates(settlementJournalEntry);
		return settlementJournalEntry;
	}

	private void setCurrentStates(DObSettlementJournalEntry settlementJournalEntry) {
		Set<String> currentStates = new HashSet<>();
		currentStates.add(ACTIVE_STATE);
		settlementJournalEntry.setCurrentStates(currentStates);
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setJournalEntryRep(DObSettlementJournalEntryRep journalEntryRep) {
		this.journalEntryRep = journalEntryRep;
	}

	public void setJournalEntryPreparationGeneralModelRep(
					DObSettlementJournalEntryPreparationGeneralModelRep journalEntryPreparationGeneralModelRep) {
		this.journalEntryPreparationGeneralModelRep = journalEntryPreparationGeneralModelRep;
	}

	public void setAccountDetailsGeneralModelRep(
					IObSettlementAccountDetailsGeneralModelRep accountDetailsGeneralModelRep) {
		this.accountDetailsGeneralModelRep = accountDetailsGeneralModelRep;
	}
}
