package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceReadyForDeliveringValueObject;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import io.reactivex.Observable;

public class DObSalesInvoiceMarkAsReadyForDeliveringCommand implements
    ICommand<DObSalesInvoiceReadyForDeliveringValueObject> {

  private DObSalesInvoiceRep dObSalesInvoiceRep;
  private CommandUtils commandUtils;

  public DObSalesInvoiceMarkAsReadyForDeliveringCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObSalesInvoiceReadyForDeliveringValueObject valueObject)
      throws Exception {
    DObSalesInvoice salesInvoice =
        dObSalesInvoiceRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
    setSalesInvoiceReadyForDeliveringState(getSalesInvoiceStateMachine(salesInvoice));
    commandUtils.setModificationInfoAndModificationDate(salesInvoice);
    dObSalesInvoiceRep.update(salesInvoice);
    return Observable.empty();
  }

  private void setSalesInvoiceReadyForDeliveringState(
      AbstractStateMachine salesInvoiceStateMachine) {
    if (salesInvoiceStateMachine
        .canFireEvent(IDObSalesInvoiceActionNames.MARK_READY_FOR_DELIVERING)) {
      salesInvoiceStateMachine.fireEvent(IDObSalesInvoiceActionNames.MARK_READY_FOR_DELIVERING);
      salesInvoiceStateMachine.save();
    }
  }

  private AbstractStateMachine getSalesInvoiceStateMachine(DObSalesInvoice salesInvoice)
      throws Exception {
    DObSalesInvoiceFactory salesInvoiceStateMachine = new DObSalesInvoiceFactory();
    AbstractStateMachine invoiceStateMachine = salesInvoiceStateMachine.createSalesInvoiceStateMachine(salesInvoice);
    return invoiceStateMachine;
  }

  public void setdObSalesInvoiceRep(DObSalesInvoiceRep dObSalesInvoiceRep) {
    this.dObSalesInvoiceRep = dObSalesInvoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

}
