package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderDeletionValueObject;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCycleDates;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObSalesOrderSummaryGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderCycleDatesRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DObSalesOrderApproveCommand implements ICommand<DObSalesOrderDeletionValueObject> {

  private DObSalesOrderRep salesOrderRep;
  private IObSalesOrderCycleDatesRep cycleDatesRep;
  private IObSalesOrderSummaryGeneralModelRep salesOrderSummaryGeneralModelRep;
  private CommandUtils commandUtils;

  @Autowired
  public DObSalesOrderApproveCommand(
      DObSalesOrderRep salesOrderRep,
      IObSalesOrderCycleDatesRep cycleDatesRep,
      IObSalesOrderSummaryGeneralModelRep salesOrderSummaryGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    this.salesOrderRep = salesOrderRep;
    this.cycleDatesRep = cycleDatesRep;
    this.salesOrderSummaryGeneralModelRep = salesOrderSummaryGeneralModelRep;
    this.commandUtils = new CommandUtils();
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  @Override
  public Observable<String> executeCommand(DObSalesOrderDeletionValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    commandUtils.setModificationInfoAndModificationDate(salesOrder);

    updateSalesOrderState(getSalesOrderStateMachine(salesOrder));
    updateSalesOrderCycleDates(salesOrder);
    updateSalesOrderRemainingAndTotalAmount(salesOrder);

    salesOrderRep.update(salesOrder);

    return Observable.just("SUCCESS");
  }

  private void updateSalesOrderRemainingAndTotalAmount(DObSalesOrder salesOrder) {
    IObSalesOrderSummaryGeneralModel orderSummaryGeneralModel =
        salesOrderSummaryGeneralModelRep.findOneByOrderCode(salesOrder.getUserCode());

    salesOrder.setTotalAmount(orderSummaryGeneralModel.getTotalAmountAfterTaxes());
    salesOrder.setRemaining(orderSummaryGeneralModel.getTotalAmountAfterTaxes());
  }

  private void updateSalesOrderCycleDates(DObSalesOrder salesOrder) {
    IObSalesOrderCycleDates cycleDates = cycleDatesRep.findOneByRefInstanceId(salesOrder.getId());

    if (cycleDates == null) {
      cycleDates = new IObSalesOrderCycleDates();
      commandUtils.setCreationAndModificationInfoAndDate(cycleDates);
      cycleDates.setApprovalDate(new DateTime());
    }

    salesOrder.setHeaderDetail(IDObSalesOrder.cycleDatesAttr, cycleDates);
  }

  private void updateSalesOrderState(AbstractStateMachine salesOrderStateMachine) {
    salesOrderStateMachine.fireEvent(IDObSalesOrderActionNames.APPROVE);
    salesOrderStateMachine.save();
  }

  private DObSalesOrderStateMachine getSalesOrderStateMachine(DObSalesOrder salesOrder)
      throws Exception {
    DObSalesOrderStateMachine salesOrderStateMachine = new DObSalesOrderStateMachine();
    salesOrderStateMachine.initObjectState(salesOrder);
    return salesOrderStateMachine;
  }
}
