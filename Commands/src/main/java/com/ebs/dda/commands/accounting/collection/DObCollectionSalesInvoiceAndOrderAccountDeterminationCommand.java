package com.ebs.dda.commands.accounting.collection;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCustomerAccountingDetails;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerGeneralModel;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.CUSTOMERS_GL_ACCOUNT;

public class DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand
    extends DObCollectionAccountDeterminationCommand {

  @Override
  protected void getCreditAccountingDetailRecord(
      IObCollectionDetailsGeneralModel collectionDetailsGM, DObCollection collection) {

    IObAccountingDocumentCustomerAccountingDetails creditAccDetailForCustomer =
        getCreditAccountingDetailForCustomer(collectionDetailsGM);

    commandUtils.setCreationAndModificationInfoAndDate(creditAccDetailForCustomer);
    collection.addLineDetail(IDObAccountingDocument.accountingCustomerDetailsAttr,
        creditAccDetailForCustomer);
  }

  private IObAccountingDocumentCustomerAccountingDetails getCreditAccountingDetailForCustomer(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {

    IObAccountingDocumentCustomerAccountingDetails customerAccDetails =
        new IObAccountingDocumentCustomerAccountingDetails();

    customerAccDetails.setObjectTypeCode(IDocumentTypes.COLLECTION);
    customerAccDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
    customerAccDetails.setAmount(collectionDetailsGM.getAmount());

    LObGlobalGLAccountConfigGeneralModel customerGlobalGLAccount =
        globalAccountConfigGMRep.findOneByUserCode(CUSTOMERS_GL_ACCOUNT);
    customerAccDetails.setGlAccountId(customerGlobalGLAccount.getGlAccountId());

    MObCustomerGeneralModel customerGM =
        customerGeneralModelRep.findOneByUserCode(collectionDetailsGM.getBusinessPartnerCode());
    customerAccDetails.setGlSubAccountId(customerGM.getId());

    return customerAccDetails;
  }


}
