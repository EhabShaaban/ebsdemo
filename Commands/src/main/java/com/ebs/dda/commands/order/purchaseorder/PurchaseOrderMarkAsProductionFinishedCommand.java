package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsProductionFinishedValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class PurchaseOrderMarkAsProductionFinishedCommand
    implements ICommand<DobPurchaseOrderMarkAsProductionFinishedValueObject> {
  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderCycleDatesRep orderCycleDatesRep;

  public PurchaseOrderMarkAsProductionFinishedCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setOrderCycleDatesRep(IObOrderCycleDatesRep orderCycleDatesRep) {
    this.orderCycleDatesRep = orderCycleDatesRep;
  }

  @Override
  public Observable<String> executeCommand(
      DobPurchaseOrderMarkAsProductionFinishedValueObject MarkAsProductionFinishedValueObject)
      throws Exception {
    String purchaseOrderCode = MarkAsProductionFinishedValueObject.getPurchaseOrderCode();
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);

    setPurchaseOrderMarkAsProductionFinishedState(purchaseOrder);

    IObOrderCycleDates orderCycleDates =
        setOrderProductionFinishedDate(
            purchaseOrder, MarkAsProductionFinishedValueObject.getProductionFinishedDate());

    commandUtils.setModificationInfoAndModificationDate(orderCycleDates);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.cycleDatesAttr, orderCycleDates);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void setPurchaseOrderMarkAsProductionFinishedState(DObPurchaseOrder purchaseOrder)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    if (purchaseOrderStateMachine.fireEvent(
        IPurchaseOrderActionNames.MARK_AS_PRODUCTION_FINISHED)) {
      purchaseOrderStateMachine.save();
    }
  }

  private IObOrderCycleDates setOrderProductionFinishedDate(
      DObPurchaseOrder purchaseOrder, DateTime productionFinishedDateTime) {
    IObOrderCycleDates orderCycleDates =
        orderCycleDatesRep.findOneByRefInstanceId(purchaseOrder.getId());
    orderCycleDates.setActualProductionFinishedDate(productionFinishedDateTime);
    return orderCycleDates;
  }
}
