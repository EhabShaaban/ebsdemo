package com.ebs.dda.commands.accounting.journalbalance;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceUpdateValueObject;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceRep;
import io.reactivex.Observable;

public class CObJournalBalanceIncreaseBalanceCommand
    implements ICommand<JournalBalanceUpdateValueObject> {

  private CObJournalBalanceRep journalBalanceRep;
  private final CommandUtils commandUtils;

  public CObJournalBalanceIncreaseBalanceCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(JournalBalanceUpdateValueObject valueObject)
      throws Exception {

    CObJournalBalance journalBalance =
        journalBalanceRep.findOneByUserCode(valueObject.getJournalBalanceCode());
    journalBalance.setBalance(journalBalance.getBalance().add(valueObject.getAmount()));
    commandUtils.setModificationInfoAndModificationDate(journalBalance);
    journalBalanceRep.save(journalBalance);
    return Observable.just(journalBalance.getUserCode());
  }

  public void setJournalBalanceRep(CObJournalBalanceRep journalBalanceRep) {
    this.journalBalanceRep = journalBalanceRep;
  }
  public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
