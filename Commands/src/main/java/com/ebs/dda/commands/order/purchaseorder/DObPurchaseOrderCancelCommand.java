package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderCommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import io.reactivex.Observable;

public class DObPurchaseOrderCancelCommand implements ICommand {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;

  public DObPurchaseOrderCancelCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  @Override
  public Observable<String> executeCommand(Object purchaseOrderCode) throws Exception {
    String purchaseOrderCodeStr = purchaseOrderCode.toString();
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCodeStr);
    DObPurchaseOrderCommandUtils.setPurchaseOrderTargetState(
        purchaseOrder, IPurchaseOrderActionNames.CANCEL);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }
}
