package com.ebs.dda.commands.accounting.landedcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemDeleteValueObject;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsRep;

import io.reactivex.Observable;

public class IObLandedCostFactorItemDeleteCommand
				implements ICommand<IObLandedCostFactorItemDeleteValueObject> {

	private IObLandedCostFactorItemsRep itemsRep;
	private final CommandUtils commandUtils;
	private DObLandedCostRep landedCostRep;

	public IObLandedCostFactorItemDeleteCommand() {
		commandUtils = new CommandUtils();
	}

	@Override
	public Observable executeCommand(IObLandedCostFactorItemDeleteValueObject valueObject)
					throws Exception {
		itemsRep.deleteById(valueObject.getItemId());
		updateLandedCostInfo(valueObject);
		return Observable.empty();
	}

	private void updateLandedCostInfo(IObLandedCostFactorItemDeleteValueObject valueObject) throws Exception {
		String landedCostCode = valueObject.getLandedCostCode();
		DObLandedCost landedCost = landedCostRep.findOneByUserCode(landedCostCode);
		commandUtils.setModificationInfoAndModificationDate(landedCost);
		landedCostRep.update(landedCost);
	}

	public void setItemsRep(IObLandedCostFactorItemsRep itemsRep) {
		this.itemsRep = itemsRep;
	}
	public void setUserAccountSecurityService(
			IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setLandedCostRep(DObLandedCostRep landedCostRep) {
		this.landedCostRep = landedCostRep;
	}
}
