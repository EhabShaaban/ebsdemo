package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesDeletionValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesRep;
import io.reactivex.Observable;
import javax.transaction.Transactional;
import org.joda.time.DateTime;

public class IObVendorInvoiceTaxesDeleteCommand
    implements ICommand<IObVendorInvoiceTaxesDeletionValueObject> {

  private IObVendorInvoiceTaxesRep vendorInvoiceTaxesRep;
  private IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep;
  private DObVendorInvoiceRep vendorInvoiceRep;
  private IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand;
  private IUserAccountSecurityService userAccountSecurityService;

  @Override
  @Transactional
  public Observable<String> executeCommand(
      IObVendorInvoiceTaxesDeletionValueObject taxesDeletionValueObject)
      throws Exception {
    String invoiceCode = taxesDeletionValueObject.getVendorInvoiceCode();
    String taxCode = taxesDeletionValueObject.getTaxCode();
    IObVendorInvoiceTaxesGeneralModel invoiceTaxesGeneralModel =
        vendorInvoiceTaxesGeneralModelRep.findOneByInvoiceCodeAndTaxCode(invoiceCode, taxCode);

    vendorInvoiceTaxesRep.deleteById(invoiceTaxesGeneralModel.getId());

    DObVendorInvoice invoice = vendorInvoiceRep.findOneByUserCode(invoiceCode);
    setModificationInfo(invoice);
    summaryUpdateCommand.executeCommand(invoice.getUserCode());
    return Observable.just("SUCCESS");
  }

  public void setVendorInvoiceTaxesRep(
      IObVendorInvoiceTaxesRep vendorInvoiceTaxesRep) {
    this.vendorInvoiceTaxesRep = vendorInvoiceTaxesRep;
  }

  public void setVendorInvoiceTaxesGeneralModelRep(
      IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep) {
    this.vendorInvoiceTaxesGeneralModelRep = vendorInvoiceTaxesGeneralModelRep;
  }

  public void setVendorInvoiceRep(
      DObVendorInvoiceRep vendorInvoiceRep) {
    this.vendorInvoiceRep = vendorInvoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  private void setModificationInfo(DObVendorInvoice dObInvoice) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    dObInvoice.setModificationInfo(loggedinUserInfo);
    dObInvoice.setModifiedDate(new DateTime());
    vendorInvoiceRep.save(dObInvoice);
  }

  public void setSummaryUpdateCommand(IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand) {
    this.summaryUpdateCommand = summaryUpdateCommand;
  }
}
