package com.ebs.dda.commands.masterdata.itemGroup;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupRep;
import io.reactivex.Observable;

public class ItemGroupDeleteCommand implements ICommand {
    private CObMaterialGroupRep cObMaterialGroupRep;
    private CObMaterialGroupGeneralModelRep cObMaterialGroupGeneralModelRep;

    public void setcObMaterialGroupRep(CObMaterialGroupRep cObMaterialGroupRep) {
        this.cObMaterialGroupRep = cObMaterialGroupRep;
    }

    public void setcObMaterialGroupGeneralModelRep(CObMaterialGroupGeneralModelRep cObMaterialGroupGeneralModelRep) {
        this.cObMaterialGroupGeneralModelRep = cObMaterialGroupGeneralModelRep;
    }

    @Override
    public Observable<String> executeCommand(Object object) throws Exception {

        String code = (String) object;
        CObMaterialGroupGeneralModel cObMaterialGroupGeneralModel =
                cObMaterialGroupGeneralModelRep.findOneByUserCode(code);
        cObMaterialGroupRep.deleteById(cObMaterialGroupGeneralModel.getId());
        return Observable.just("SUCCESS");
    }

    public void checkIfInstanceExists(String itemGroupCode) throws Exception {
        CObMaterialGroupGeneralModel materialGroupGeneralModel = readEntityByUserCode(itemGroupCode);
        if (materialGroupGeneralModel == null) {
            throw new InstanceNotExistException();
        }
    }

    private CObMaterialGroupGeneralModel readEntityByUserCode(String userCode) throws InstanceNotExistException {
        CObMaterialGroupGeneralModel materialGroupGeneralModel = cObMaterialGroupGeneralModelRep.findOneByUserCode(userCode);
        return materialGroupGeneralModel;
    }
}
