package com.ebs.dda.commands.accounting.settlement;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDeleteAccountingDetailValueObject;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;

import com.ebs.dda.repositories.accounting.settlement.DObSettlementRep;
import io.reactivex.Observable;

public class IObSettlementAccountingDetailDeleteCommand
				implements ICommand<IObSettlementDeleteAccountingDetailValueObject> {

	private IObAccountingDocumentAccountingDetailsRep accountingDetailsRep;
	private DObSettlementRep settlementRep;
	private final CommandUtils commandUtils;

	public IObSettlementAccountingDetailDeleteCommand() {
		commandUtils = new CommandUtils();
	}

	@Override
	public Observable executeCommand(IObSettlementDeleteAccountingDetailValueObject valueObject)
					throws Exception {
		accountingDetailsRep.deleteById(valueObject.getAccountingDetailId());
		updateSettlementInfo(valueObject);
		return Observable.empty();
	}

	private void updateSettlementInfo(IObSettlementDeleteAccountingDetailValueObject valueObject) throws Exception {
		String settlementCode = valueObject.getSettlementCode();
		DObSettlement settlement = settlementRep.findOneByUserCode(settlementCode);
		commandUtils.setModificationInfoAndModificationDate(settlement);
		settlementRep.update(settlement);
	}

	public void setAccountingDetailsRep(
					IObAccountingDocumentAccountingDetailsRep accountingDetailsRep) {
		this.accountingDetailsRep = accountingDetailsRep;
	}

	public void setSettlementRep(DObSettlementRep settlementRep) {
		this.settlementRep = settlementRep;
	}
	public void setUserAccountSecurityService(
			IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

}
