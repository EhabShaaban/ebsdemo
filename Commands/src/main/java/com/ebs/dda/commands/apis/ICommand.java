package com.ebs.dda.commands.apis;

import io.reactivex.Observable;

public interface ICommand<IValueObject> {
  <T> Observable<T> executeCommand(IValueObject object) throws Exception;
}
