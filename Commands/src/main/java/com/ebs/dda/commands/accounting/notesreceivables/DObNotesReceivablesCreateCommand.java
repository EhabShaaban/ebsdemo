package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.notesreceivables.factory.DObMonetaryNoteFactory;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;
import com.ebs.dda.jpa.accounting.accountingnotes.IObAccountingNoteCompanyData;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import io.reactivex.Observable;
import javax.transaction.Transactional;

public class DObNotesReceivablesCreateCommand
    implements ICommand<DObNotesReceivablesCreateValueObject> {

  private CObPurchasingUnitRep purchasingUnitRep;
  private DObMonetaryNotesRep monetaryNotesRep;
  private MObCustomerRep customerRep;
  private CObCompanyRep companyRep;
  private CObCurrencyRep currencyRep;

  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private CommandUtils commandUtils;
  private DObNotesReceivablesStateMachine stateMachine;
  private DObMonetaryNoteFactory monetaryNoteFactory;

  public DObNotesReceivablesCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    stateMachine = new DObNotesReceivablesStateMachine();
    monetaryNoteFactory = new DObMonetaryNoteFactory();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(DObNotesReceivablesCreateValueObject valueObject)
      throws Exception {

    String userCode = getUserCodeByMonetaryNoteType(valueObject.getType().name());

    DObMonetaryNotes monetaryNote =
        monetaryNoteFactory.createMonetaryNote(valueObject.getType().name());

    commandUtils.setCreationAndModificationInfoAndDate(monetaryNote);

    setGeneralData(valueObject, userCode, monetaryNote);

    setCompanyData(valueObject, monetaryNote);
    setDetailsData(valueObject, monetaryNote);

    monetaryNotesRep.create(monetaryNote);
    return Observable.just(userCode);
  }

  private String getUserCodeByMonetaryNoteType(String monetaryNoteType) {
    String userCode = null;
    if (isNotesReceivable(monetaryNoteType)) {
      userCode = documentObjectCodeGenerator.generateUserCode("DObNotesReceivable");
    }
    return userCode;
  }

  private void setCompanyData(
      DObNotesReceivablesCreateValueObject valueObject, DObMonetaryNotes monetaryNote) {

    IObNotesReceivableCompanyData companyData = new IObNotesReceivableCompanyData();
    commandUtils.setCreationAndModificationInfoAndDate(companyData);
    companyData.setCompanyId(getCompanyIdByCode(valueObject.getCompanyCode()));
    companyData.setPurchaseUnitId(getPurchaseUnitIdByCode(valueObject.getBusinessUnitCode()));
    monetaryNote.setHeaderDetail(IDObMonetaryNotes.companyAttr, companyData);
  }

  private void setDetailsData(
      DObNotesReceivablesCreateValueObject valueObject, DObMonetaryNotes monetaryNote) {
    IObMonetaryNotesDetails detailsData = new IObMonetaryNotesDetails();
    commandUtils.setCreationAndModificationInfoAndDate(detailsData);

    detailsData.setBusinessPartnerId(getBusinessPartnerIdBasedOnMonetaryNoteType(valueObject));
    detailsData.setCurrencyId(getCurrencyIdByCurrencyIso(valueObject.getCurrencyIso()));
    detailsData.setAmount(valueObject.getAmount());
    detailsData.setRemaining(valueObject.getAmount());
    detailsData.setNoteForm(valueObject.getNoteForm().name());
    monetaryNote.setHeaderDetail(IDObMonetaryNotes.detailsAttr, detailsData);
  }

  private void setGeneralData(
      DObNotesReceivablesCreateValueObject valueObject,
      String userCode,
      DObMonetaryNotes monetaryNote)
      throws Exception {
    monetaryNote.setUserCode(userCode);
    monetaryNote.setDocumentOwnerId(valueObject.getDocumentOwnerId());
    stateMachine.initObjectState(monetaryNote);
  }

  private Long getPurchaseUnitIdByCode(String purchaseUnitCode) {
    CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
    return purchaseUnit.getId();
  }

  private Long getCustomerIdByCode(String customerCode) {
    return customerRep.findOneByUserCode(customerCode).getId();
  }

  private Long getCompanyIdByCode(String companyCode) {
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private Long getCurrencyIdByCurrencyIso(String currencyIso) {
    return currencyRep.findOneByIso(currencyIso).getId();
  }

  private Long getBusinessPartnerIdBasedOnMonetaryNoteType(
      DObNotesReceivablesCreateValueObject valueObject) {
    Long businessPartnerId = null;
    String businessPartnerCode = valueObject.getBusinessPartnerCode();
    if (isNotesReceivable(valueObject.getType().name())) {
      businessPartnerId = getCustomerIdByCode(businessPartnerCode);
    }
    return businessPartnerId;
  }

  private boolean isNotesReceivable(String monetaryNoteType) {
    return monetaryNoteType.equals(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION.name());
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setMObCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
