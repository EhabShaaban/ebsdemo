package com.ebs.dda.commands.inventory.storetransaction.services;

import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModelRep;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

@Service
public class GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService {
  private final TObGoodsReceiptStoreTransactionDataGeneralModelRep
      goodsReceiptStoreTransactionDataGeneralModelRep;
  private final IObCostingItemGeneralModelRep costingItemGeneralModelRep;
  private final IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;

  public GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
      IObCostingItemGeneralModelRep costingItemGeneralModelRep,
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep,
      TObGoodsReceiptStoreTransactionDataGeneralModelRep
          goodsReceiptStoreTransactionDataGeneralModelRep) {
    this.costingItemGeneralModelRep = costingItemGeneralModelRep;
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
    this.goodsReceiptStoreTransactionDataGeneralModelRep =
        goodsReceiptStoreTransactionDataGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderStoreTransactionCosts(
      String goodsReceiptCode,
      List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactions) {

    List<IObCostingItemGeneralModel> costingItems = getCostingItemGeneralModels(goodsReceiptCode);

    List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItemsData =
        goodsReceiptStoreTransactionDataGeneralModelRep.findByUserCode(goodsReceiptCode);

    for (TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction :
        goodsReceiptStoreTransactions) {

      TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow =
          findGoodsReceiptItemByStoreTransaction(
              goodsReceiptItemsData, goodsReceiptStoreTransaction);

      IObCostingItemGeneralModel costingItem =
          findCostingItemByGoodsReceiptItem(costingItems, goodsReceiptItemDataRow);

      setEstimatedOrActualCost(goodsReceiptStoreTransaction, costingItem);
    }
  }

  private IObCostingItemGeneralModel findCostingItemByGoodsReceiptItem(
      List<IObCostingItemGeneralModel> costingItems,
      TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptItemDataRow) {

    return costingItems.stream()
        .filter(
            item ->
                item.getItemCode().equals(goodsReceiptItemDataRow.getItemCode())
                    && item.getUomCode().equals(goodsReceiptItemDataRow.getReceivedUoeCode())
                    && item.getStockType().equals(goodsReceiptItemDataRow.getReceivedStockType()))
        .findFirst()
        .get();
  }

  private TObGoodsReceiptStoreTransactionDataGeneralModel findGoodsReceiptItemByStoreTransaction(
      List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItemsData,
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction) {

    String storeTransactionStockType =
        goodsReceiptStoreTransaction.getStockType().getId().toString();

    return goodsReceiptItemsData.stream()
        .filter(
            itemDataRow ->
                itemDataRow.getItemId().equals(goodsReceiptStoreTransaction.getItemId())
                    && itemDataRow.getReceivedStockType().equals(storeTransactionStockType)
                    && itemDataRow
                        .getReceivedUoeId()
                        .equals(goodsReceiptStoreTransaction.getUnitOfMeasureId()))
        .findFirst()
        .get();
  }

  private List<IObCostingItemGeneralModel> getCostingItemGeneralModels(String goodsReceiptCode) {
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        costingDetailsGeneralModelRep.findOneByGoodsReceiptCode(goodsReceiptCode);
    String costingDocumentCode = costingDetailsGeneralModel.getUserCode();
    return costingItemGeneralModelRep.findByUserCodeOrderByItemCodeAsc(costingDocumentCode);
  }

  private void setEstimatedOrActualCost(
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
      IObCostingItemGeneralModel costingItem) {

    if (isEstimatedCost(costingItem)) {
      goodsReceiptStoreTransaction.setEstimateCost(
          costingItem.getEstimateUnitPrice().add(costingItem.getEstimateUnitLandedCost()));
    }

    if (isActualCost(costingItem)) {
      goodsReceiptStoreTransaction.setActualCost(
          costingItem.getActualUnitPrice().add(costingItem.getActualUnitLandedCost()));
      goodsReceiptStoreTransaction.setUpdatingActualCostDate(DateTime.now());
    }
  }

  private boolean isActualCost(IObCostingItemGeneralModel costingItem) {
    return costingItem.getActualUnitPrice() != null;
  }

  private boolean isEstimatedCost(IObCostingItemGeneralModel costingItem) {
    return costingItem.getEstimateUnitPrice() != null;
  }
}
