package com.ebs.dda.commands.accounting.collection;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.collection.apis.IDObCollectionActionNames;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.collection.*;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class DObCollectionActivateCommand implements ICommand<DObCollectionActivateValueObject> {

  private static final String ACTIVE_STATE = "Active";
  private IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;
  private CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep;
  private DObCollectionRep<DObCollection> collectionRep;
  private CObFiscalYearRep fiscalYearRep;
  private CommandUtils commandUtils;

  public DObCollectionActivateCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  @Transactional
  public Observable<String> executeCommand(DObCollectionActivateValueObject valueObject)
      throws Exception {
    DObCollection collection = collectionRep.findOneByUserCode(valueObject.getCollectionCode());
    handleActivatingCollection(collection, valueObject);
    collectionRep.update(collection);
    return Observable.just(valueObject.getCollectionCode());
  }

  private <T extends DObCollection> void handleActivatingCollection(
      T collection, DObCollectionActivateValueObject valueObject) throws Exception {
    setCollectionActiveState(getCollectionStateMachine(collection));
    IObCollectionActivationDetails activationDetails = setActivationDetails(valueObject);

    collection.setHeaderDetail(IDObCollection.activationDetails, activationDetails);
    commandUtils.setModificationInfoAndModificationDate(collection);
  }

  private void setCollectionActiveState(AbstractStateMachine collectionStateMachine)
      throws Exception {
    if (collectionStateMachine.canFireEvent(IDObCollectionActionNames.ACTIVATE)) {
      collectionStateMachine.fireEvent(IDObCollectionActionNames.ACTIVATE);
      collectionStateMachine.save();
    }
  }

  private DObCollectionStateMachine getCollectionStateMachine(DObCollection collection)
      throws Exception {
    DObCollectionStateMachine collectionStateMachine = new DObCollectionStateMachine();
    collectionStateMachine.initObjectState(collection);
    return collectionStateMachine;
  }

  private IObCollectionActivationDetails setActivationDetails(
      DObCollectionActivateValueObject valueObject) {
    IObCollectionActivationDetails activationDetails = new IObCollectionActivationDetails();
    commandUtils.setCreationAndModificationInfoAndDate(activationDetails);
    String loggedInUser = commandUtils.getLoggedInUser();
    activationDetails.setAccountant(loggedInUser);
    DateTime dueDateTime = commandUtils.getDateTime(valueObject.getJournalEntryDate());
    activationDetails.setDueDate(dueDateTime);
    CObExchangeRateGeneralModel exchangeRateGeneralModel =
        getExchangeRate(valueObject.getCollectionCode());
    activationDetails.setCurrencyPrice(exchangeRateGeneralModel.getSecondValue());
    activationDetails.setExchangeRateId(exchangeRateGeneralModel.getId());
    activationDetails.setActivationDate(activationDetails.getModifiedDate());

    activationDetails.setFiscalPeriodId(getActiveFiscalPeriodId(valueObject.getJournalEntryDate()));
    return activationDetails;
  }

  private Long getActiveFiscalPeriodId(String journalEntryDate) {
    DateTime journalDate = getDateTime(journalEntryDate);
    List<CObFiscalYear> fiscalYears =
        fiscalYearRep.findByCurrentStatesLike("%" + ACTIVE_STATE + "%");
    for (CObFiscalYear fiscalYear : fiscalYears) {
      if (journalDate.isAfter(fiscalYear.getFromDate())
          && journalDate.isBefore(fiscalYear.getToDate())) {
        return fiscalYear.getId();
      }
    }
    return null;
  }

  private CObExchangeRateGeneralModel getExchangeRate(String collectionCode) {
    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode);
    String firstCurrencyIso = collectionDetailsGeneralModel.getCurrencyISO();
    String secondCurrencyIso = collectionDetailsGeneralModel.getCompanyCurrencyISO();
    CObExchangeRateGeneralModel exchangeRateGeneralModel =
        exchangeRateGeneralModelRep.findLatestDefaultExchangeRate(
            firstCurrencyIso, secondCurrencyIso);
    return exchangeRateGeneralModel;
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCollectionDetailsGeneralModelRep(
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep) {
    this.collectionDetailsGeneralModelRep = collectionDetailsGeneralModelRep;
  }

  public void setCollectionRep(DObCollectionRep<DObCollection> collectionRep) {
    this.collectionRep = collectionRep;
  }

  public void setExchangeRateGeneralModelRep(
      CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep) {
    this.exchangeRateGeneralModelRep = exchangeRateGeneralModelRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

}
