package com.ebs.dda.commands.inventory.stocktransformation;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.IObStockTransformationDetails;
import com.ebs.dda.jpa.inventory.storetransaction.TObStockTransformationStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionCreateStockTransformationValueObject;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.repositories.inventory.stocktransformation.IObStockTransformationDetailsRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStockTransformationStoreTransactionRep;
import io.reactivex.Observable;

public class DObStockTransformationUpdateCommand
    implements ICommand<TObStoreTransactionCreateStockTransformationValueObject> {

  private CommandUtils commandUtils;
  private DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep;
  private IObStockTransformationDetailsRep stockTransformationDetailsRep;
  private TObStockTransformationStoreTransactionRep stockTransformationStoreTransactionRep;

  public DObStockTransformationUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      TObStoreTransactionCreateStockTransformationValueObject valueObject) throws Exception {
      DObStockTransformationGeneralModel stockTransformationGeneralModel =
              stockTransformationGeneralModelRep.findOneByUserCode(
                      valueObject.getStockTransformationCode());

      Long newStoreTransactionId = getAddStoreTransactionId(stockTransformationGeneralModel);

      IObStockTransformationDetails stockTransformationDetails =
              stockTransformationDetailsRep.findOneByRefInstanceId(
                      stockTransformationGeneralModel.getId());

      stockTransformationDetails.setNewStoreTransactionId(newStoreTransactionId);

      commandUtils.setModificationInfoAndModificationDate(stockTransformationDetails);

      stockTransformationDetailsRep.update(stockTransformationDetails);
      return Observable.just("Success");
  }

    private Long getAddStoreTransactionId(
            DObStockTransformationGeneralModel stockTransformationGeneralModel) {

        TObStockTransformationStoreTransaction stockTransformationStoreTransaction =
                stockTransformationStoreTransactionRep.findOneByDocumentReferenceIdAndTransactionOperation(
                        stockTransformationGeneralModel.getId(), TransactionTypeEnum.TransactionType.ADD);
        return stockTransformationStoreTransaction.getId();
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setStockTransformationGeneralModelRep(
      DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep) {
    this.stockTransformationGeneralModelRep = stockTransformationGeneralModelRep;
  }

  public void setStockTransformationDetailsRep(
      IObStockTransformationDetailsRep stockTransformationDetailsRep) {
    this.stockTransformationDetailsRep = stockTransformationDetailsRep;
  }

  public void setStockTransformationStoreTransactionRep(
      TObStockTransformationStoreTransactionRep stockTransformationStoreTransactionRep) {
    this.stockTransformationStoreTransactionRep = stockTransformationStoreTransactionRep;
  }
}
