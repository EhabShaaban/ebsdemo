package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TObGoodsReceiptStoreTransactionRecostingCommand implements ICommand<String> {
  private TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep;
  private CommandUtils commandUtils;
  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private static final String GR_PO = "GR_PO";
  private GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
      goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
  private TObStoreTransactionRep storeTransactionRep;

  public TObGoodsReceiptStoreTransactionRecostingCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable executeCommand(String landedCostCode) throws Exception {
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    String purchaseOrderCode = landedCostGeneralModel.getPurchaseOrderCode();
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep
            .findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
                purchaseOrderCode, GR_PO, "%" + DObGoodsReceiptStateMachine.ACTIVE_STATE + "%");
    List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList =
        getTransactionsToBeUpdated(goodsReceiptGeneralModel);
    String goodsReceiptGeneralModelUserCode = goodsReceiptGeneralModel.getUserCode();
    goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
        .setGoodsReceiptPurchaseOrderStoreTransactionCosts(
            goodsReceiptGeneralModelUserCode, goodsReceiptStoreTransactionList);
    setModificationInfoForStoreTransactions(goodsReceiptStoreTransactionList);
    goodsReceiptStoreTransactionRep.saveAll(goodsReceiptStoreTransactionList);
    return Observable.empty();
  }

  private void setModificationInfoForStoreTransactions(
      List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList) {
    goodsReceiptStoreTransactionList.stream()
        .forEach(
            storeTransaction ->
                this.commandUtils.setModificationInfoAndModificationDate(storeTransaction));
  }

  private List<TObGoodsReceiptStoreTransaction> getTransactionsToBeUpdated(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList =
        getStoretransactionsForGoodsReceipt(goodsReceiptGeneralModel);
    List<TObGoodsReceiptStoreTransaction> childrenTransactions =
        getChildrenTransactionsFromTransactionList(goodsReceiptStoreTransactionList);
    Stream<TObGoodsReceiptStoreTransaction> addedStream =
        Stream.concat(goodsReceiptStoreTransactionList.stream(), childrenTransactions.stream());
    return addedStream.collect(Collectors.toList());
  }

  private List<TObGoodsReceiptStoreTransaction> getChildrenTransactionsFromTransactionList(
      List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList) {
    List<Long> childrenTransactionIds = new ArrayList<>();
    goodsReceiptStoreTransactionList.forEach(
        goodsReceiptStoreTransaction -> {
          List<Long> storeTransactionChildren =
              storeTransactionRep.findAllChildrenByRefInstanceIdAndAddTransactionOperation(
                  goodsReceiptStoreTransaction.getId());
          childrenTransactionIds.addAll(storeTransactionChildren);
        });
    List<TObGoodsReceiptStoreTransaction> childGoodsReceiptStoreTransactions =
        goodsReceiptStoreTransactionRep.findAllById(childrenTransactionIds);
    return childGoodsReceiptStoreTransactions;
  }

  private List<TObGoodsReceiptStoreTransaction> getStoretransactionsForGoodsReceipt(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    Long goodsReceiptId = goodsReceiptGeneralModel.getId();
    List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList =
        goodsReceiptStoreTransactionRep.findByDocumentReferenceId(goodsReceiptId);
    return goodsReceiptStoreTransactionList;
  }

  public void setGoodsReceiptStoreTransactionRep(
      TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep) {
    this.goodsReceiptStoreTransactionRep = goodsReceiptStoreTransactionRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
      GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
          goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService) {
    this.goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService =
        goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
  }

  public void setStoreTransactionRep(TObStoreTransactionRep storeTransactionRep) {
    this.storeTransactionRep = storeTransactionRep;
  }
}
