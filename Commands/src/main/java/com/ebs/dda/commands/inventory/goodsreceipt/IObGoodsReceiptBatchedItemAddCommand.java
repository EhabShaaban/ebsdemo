package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptActionNames;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptBatchedRecievedItemsRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import io.reactivex.Observable;

public class IObGoodsReceiptBatchedItemAddCommand
    implements ICommand<IObGoodsReceiptReceivedItemsDataValueObject> {

  private DObGoodsReceiptPurchaseOrderRep goodsReceiptRep;
  private EntityLockCommand entityLockCommand;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private IObGoodsReceiptBatchedRecievedItemsRep batchedRecievedItemsDataRep;
  private IUserAccountSecurityService userAccountSecurityService;
  private IObGoodsReceiptReceivedItemsGeneralModelRep receiptRecievedItemsGeneralModelRep;
  private CommandUtils commandUtils;

  public IObGoodsReceiptBatchedItemAddCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptReceivedItemsDataValueObject)
      throws Exception {
    IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedReceivedItem =
        new IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData();

    String goodsReceiptCode = goodsReceiptReceivedItemsDataValueObject.getGoodsReceiptCode();

    checkItemSectionLocked(goodsReceiptCode);

    DObGoodsReceiptPurchaseOrder goodsReceiptPurchaseOrder =
        getGoodsReceiptByCode(goodsReceiptCode);

    prepareGoodsReceiptBatchedReceivedItemDataToPersistFromValueObject(
        goodsReceiptBatchedReceivedItem,
        goodsReceiptReceivedItemsDataValueObject,
        goodsReceiptPurchaseOrder);

    batchedRecievedItemsDataRep.updateAndClear(goodsReceiptBatchedReceivedItem);

    commandUtils.setModificationInfoAndModificationDate(goodsReceiptPurchaseOrder);

    goodsReceiptRep.update(goodsReceiptPurchaseOrder);
    return Observable.just("SUCCESS");
  }

  public void setBatchedReceivedItemsDataRep(
      IObGoodsReceiptBatchedRecievedItemsRep batchedRecievedItemsDataRep) {
    this.batchedRecievedItemsDataRep = batchedRecievedItemsDataRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setGoodsReceiptRep(DObGoodsReceiptPurchaseOrderRep goodsReceiptRep) {
    this.goodsReceiptRep = goodsReceiptRep;
  }

  public void setReceiptRecievedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep receiptRecievedItemsGeneralModelRep) {
    this.receiptRecievedItemsGeneralModelRep = receiptRecievedItemsGeneralModelRep;
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  private LockDetails lock(String resourceCode) throws Exception {
    LockDetails lockDetails =
        entityLockCommand.lockSection(resourceCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
    DObGoodsReceipt goodsReceipt = goodsReceiptRep.findOneByUserCode(resourceCode);
    try {
      checkIfAllowedAction(goodsReceipt);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(resourceCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
    return lockDetails;
  }

  private void checkItemSectionLocked(String goodsReceiptCode) throws Exception {
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
  }

  private DObGoodsReceiptPurchaseOrder getGoodsReceiptByCode(String goodsReceiptCode) {
    return goodsReceiptRep.findOneByUserCode(goodsReceiptCode);
  }

  private void prepareGoodsReceiptBatchedReceivedItemDataToPersistFromValueObject(
      IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedReceivedItem,
      IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptReceivedItemsDataValueObject,
      DObGoodsReceipt goodsReceipt)
      throws Exception {
    setGoodsReceiptBatchedRecievedItemSystemData(goodsReceiptBatchedReceivedItem, goodsReceipt);
    setGoodsReceiptBatchedRecievedItemValueObject(
        goodsReceiptBatchedReceivedItem, goodsReceiptReceivedItemsDataValueObject, goodsReceipt);
  }

  private void setGoodsReceiptBatchedRecievedItemSystemData(
      IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedReceivedItem,
      DObGoodsReceipt goodsReceipt)
      throws Exception {

    goodsReceiptBatchedReceivedItem.setRefInstanceId(goodsReceipt.getId());
    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptBatchedReceivedItem);
  }

  private void setGoodsReceiptBatchedRecievedItemValueObject(
      IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedReceivedItem,
      IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptReceivedItemsDataValueObject,
      DObGoodsReceipt goodsReceipt) {

    String itemCode = goodsReceiptReceivedItemsDataValueObject.getItemCode();

    IObGoodsReceiptReceivedItemsGeneralModel receiptRecievedItemsGeneralModel =
        receiptRecievedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
            goodsReceipt.getUserCode(), itemCode);
    if (receiptRecievedItemsGeneralModel != null) {
      goodsReceiptBatchedReceivedItem.setId(receiptRecievedItemsGeneralModel.getGRItemId());
    }

    Long itemId = getItemIdByCode(itemCode);

    goodsReceiptBatchedReceivedItem.setItemId(itemId);
    goodsReceiptBatchedReceivedItem.setDifferenceReason("");
  }

  private Long getItemIdByCode(String itemCode) {
    MObItemGeneralModel itemGeneralModelRepByCode = itemGeneralModelRep.findByUserCode(itemCode);
    return itemGeneralModelRepByCode.getId();
  }

  private void checkIfAllowedAction(DObGoodsReceipt entity) throws Exception {
    DObGoodsReceiptStateMachine goodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
    goodsReceiptStateMachine.initObjectState(entity);
    goodsReceiptStateMachine.isAllowedAction(IGoodsReceiptActionNames.UPDATE_ITEM);
  }
}
