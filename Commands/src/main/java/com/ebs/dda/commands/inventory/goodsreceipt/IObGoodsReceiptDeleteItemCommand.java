package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrder;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsData;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import org.joda.time.DateTime;

import java.util.Optional;

public class IObGoodsReceiptDeleteItemCommand {

    DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep;
    IObGoodsReceiptPurchaseOrderReceivedItemsRep iObGoodsReceiptRecievedItemsDataRep;
    EntityLockCommand entityLockCommand;
    IObGoodsReceiptReceivedItemsGeneralModelRep iObGoodsReceiptReceivedItemsGeneralModelRep;
    private String sectionName = IGoodsReceiptSectionNames.ITEMS_SECTION;
    private CommandUtils commandUtils;

    public IObGoodsReceiptDeleteItemCommand(){
        commandUtils = new CommandUtils();
    }
    public void setGoodsReceiptRep(DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep) {
        this.dObGoodsReceiptPurchaseOrderRep = dObGoodsReceiptPurchaseOrderRep;
    }

    public void setGoodsReceiptRecievedItemsDataRep(
            IObGoodsReceiptPurchaseOrderReceivedItemsRep iObGoodsReceiptRecievedItemsDataRep) {
        this.iObGoodsReceiptRecievedItemsDataRep = iObGoodsReceiptRecievedItemsDataRep;
    }

    public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
        this.entityLockCommand = entityLockCommand;
    }

    public void setGoodsReceiptRecievedItemsGeneralModelRep(
            IObGoodsReceiptReceivedItemsGeneralModelRep iObGoodsReceiptReceivedItemsGeneralModelRep) {
        this.iObGoodsReceiptReceivedItemsGeneralModelRep = iObGoodsReceiptReceivedItemsGeneralModelRep;
    }

  public void executeCommand(String itemCode, String goodsReciptCode) throws Exception {

    LockDetails lockDetails = null;

    try {
        lockDetails = entityLockCommand.lockSection(goodsReciptCode, sectionName);
        IObGoodsReceiptReceivedItemsGeneralModel iObGoodsReceiptRecievedItemsGeneralModel =
                readReceivedItemByItemCodeAndGoodsReceiptCode(itemCode, goodsReciptCode);
        Optional<IObGoodsReceiptPurchaseOrderReceivedItemsData> iObGoodsReceiptRecievedItemsData =
                readReceivedItemByItemID(iObGoodsReceiptRecievedItemsGeneralModel);
        DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
                readGoodsReceiptByCode(goodsReciptCode);
        checkIfAllowedAction(dObGoodsReceiptPurchaseOrder, IActionsNames.DELETE);
        iObGoodsReceiptRecievedItemsDataRep.deleteById(
                iObGoodsReceiptRecievedItemsData.get().getId());
        commandUtils.setModificationInfoAndModificationDate(dObGoodsReceiptPurchaseOrder);
        dObGoodsReceiptPurchaseOrderRep.save(dObGoodsReceiptPurchaseOrder);
        entityLockCommand.unlockSection(goodsReciptCode, sectionName);
        return;

    } catch (RecordOfInstanceNotExistException | RecordDeleteNotAllowedPerStateException e) {
      if (lockDetails
          != null) { // remove InstanceNotExistException as the check is done in lock function
        entityLockCommand.unlockSection(goodsReciptCode, sectionName);
      }
      throw e;
    }
  }

    private IObGoodsReceiptReceivedItemsGeneralModel readReceivedItemByItemCodeAndGoodsReceiptCode(
            String itemCode, String goodsReceiptCode) throws RecordOfInstanceNotExistException {
        IObGoodsReceiptReceivedItemsGeneralModel iObGoodsReceiptRecievedItemsGeneralModel =
                iObGoodsReceiptReceivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
                        goodsReceiptCode, itemCode);
        if (iObGoodsReceiptRecievedItemsGeneralModel == null) {
            throw new RecordOfInstanceNotExistException(
                    IObGoodsReceiptReceivedItemsGeneralModel.class.getName());
        }
        return iObGoodsReceiptRecievedItemsGeneralModel;
    }

    private Optional<IObGoodsReceiptPurchaseOrderReceivedItemsData> readReceivedItemByItemID(
            IObGoodsReceiptReceivedItemsGeneralModel iObGoodsReceiptRecievedItemsGeneralModel)
            throws RecordOfInstanceNotExistException {
        Optional<IObGoodsReceiptPurchaseOrderReceivedItemsData> iObGoodsReceiptRecievedItemsData =
                iObGoodsReceiptRecievedItemsDataRep.findById(
                        iObGoodsReceiptRecievedItemsGeneralModel.getGRItemId());
        if (iObGoodsReceiptRecievedItemsData == null) {
            throw new RecordOfInstanceNotExistException(IObGoodsReceiptPurchaseOrderReceivedItemsData.class.getName());
        }
        return iObGoodsReceiptRecievedItemsData;
    }

    private DObGoodsReceiptPurchaseOrder readGoodsReceiptByCode(String goodsReceiptCode)
            throws InstanceNotExistException {
        DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder =
                dObGoodsReceiptPurchaseOrderRep.findOneByUserCode(goodsReceiptCode);
        if (dObGoodsReceiptPurchaseOrder == null) {
            throw new InstanceNotExistException();
        }
        return dObGoodsReceiptPurchaseOrder;
    }

  private void checkIfAllowedAction(DObGoodsReceipt entity, String allowedAction) throws Exception {
    DObGoodsReceiptStateMachine goodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
    goodsReceiptStateMachine.initObjectState(entity);
    try {
      goodsReceiptStateMachine.isAllowedAction(allowedAction);
    } catch (ActionNotAllowedPerStateException e) {
      RecordDeleteNotAllowedPerStateException recordDeleteNotAllowedPerStateException =
          new RecordDeleteNotAllowedPerStateException();
      recordDeleteNotAllowedPerStateException.initCause(e);
      throw recordDeleteNotAllowedPerStateException;
    }
  }

  public void setUserAccountInfoService(IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
}
