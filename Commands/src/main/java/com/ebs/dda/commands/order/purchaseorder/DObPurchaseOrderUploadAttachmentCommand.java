package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.LObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.RObAttachment;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.RObAttachmentRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderAttachment;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderAttachmentValueObject;
import io.reactivex.Observable;

public class DObPurchaseOrderUploadAttachmentCommand
    implements ICommand<DObPurchaseOrderAttachmentValueObject> {
  private final CommandUtils commandUtils;
  private RObAttachmentRep robAttachmentRep;
  private LObAttachmentTypeRep lObAttachmentTypeRep;
  private DObPurchaseOrderRep orderRep;
  private EntityLockCommand<DObOrderDocument> entityLockCommand;

  public DObPurchaseOrderUploadAttachmentCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      DObPurchaseOrderAttachmentValueObject attachmentValueObject) throws Exception {

    RObAttachment attachment = saveAttachment(attachmentValueObject);
    DObPurchaseOrder order =
        orderRep.findOneByUserCode(attachmentValueObject.getPurchaseOrderCode());
    prepareIObOrderAttachment(attachmentValueObject, attachment, order);
    orderRep.update(order);
    return Observable.just("SUCCESS");
  }

  private void prepareIObOrderAttachment(
      DObPurchaseOrderAttachmentValueObject attachmentValueObject,
      RObAttachment attachment,
      DObPurchaseOrder purchaseOrder) {
    LObAttachmentType lObAttachment =
        lObAttachmentTypeRep.findOneByUserCode(attachmentValueObject.getAttachmentTypeCode());
    IObOrderAttachment iObOrderAttachment = new IObOrderAttachment();
    iObOrderAttachment.setLobAttachmentId(lObAttachment.getId());
    iObOrderAttachment.setRobAttachmentId(attachment.getId());
    iObOrderAttachment.setRefInstanceId(purchaseOrder.getId());
    commandUtils.setCreationAndModificationInfoAndDate(iObOrderAttachment);
    purchaseOrder.addLineDetail(IDObPurchaseOrder.attachmentDataAttr, iObOrderAttachment);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }

  private RObAttachment saveAttachment(
      DObPurchaseOrderAttachmentValueObject attachmentValueObject) {
    RObAttachment attachment = new RObAttachment();
    attachment.setContent(attachmentValueObject.getAttachmentContent());
    attachment.setFormat(attachmentValueObject.getAttachmentFormat());
    attachment.setName(attachmentValueObject.getAttachmentName());
    commandUtils.setCreationAndModificationInfoAndDate(attachment);
    RObAttachment savedAttachment = robAttachmentRep.save(attachment);
    return savedAttachment;
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setRobAttachmentRep(RObAttachmentRep robAttachmentRep) {
    this.robAttachmentRep = robAttachmentRep;
  }

  public void setlObAttachmentTypeRep(LObAttachmentTypeRep lObAttachmentTypeRep) {
    this.lObAttachmentTypeRep = lObAttachmentTypeRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
