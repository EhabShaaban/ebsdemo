package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouse;
import com.ebs.dda.jpa.order.salesorder.*;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderCompanyStoreDataRep;
import io.reactivex.Observable;

public class IObSalesOrderCompanyStoreUpdateCommand
    implements ICommand<IObSalesOrderCompanyAndStoreValueObject> {

  private CommandUtils commandUtils;
  private DObSalesOrderRep salesOrderRep;
  private IObSalesOrderCompanyStoreDataRep companyStoreDataRep;
  private CObCompanyRep companyRep;
  private CObStorehouseRep storehouseRep;

  public IObSalesOrderCompanyStoreUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObSalesOrderCompanyAndStoreValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    IObSalesOrderCompanyStoreData companyStoreData =
        getIObSalesOrderCompanyStoreDataWithUpdatedValues(salesOrder.getId(), valueObject);

    commandUtils.setModificationInfoAndModificationDate(companyStoreData);
    commandUtils.setModificationInfoAndModificationDate(salesOrder);
    salesOrder.setHeaderDetail(IDObSalesOrder.companyStoreDataAttr, companyStoreData);
    salesOrderRep.update(salesOrder);
    return Observable.just("Success");
  }

  private IObSalesOrderCompanyStoreData getIObSalesOrderCompanyStoreDataWithUpdatedValues(
      Long salesOrderId, IObSalesOrderCompanyAndStoreValueObject valueObject) {
    IObSalesOrderCompanyStoreData companyStoreData =
        companyStoreDataRep.findOneByRefInstanceId(salesOrderId);
    if (companyStoreData == null) {
      companyStoreData = new IObSalesOrderCompanyStoreData();
      commandUtils.setCreationAndModificationInfoAndDate(companyStoreData);
    }
    setStoreId(companyStoreData, valueObject.getStoreCode());
    return companyStoreData;
  }

  private void setStoreId(IObSalesOrderCompanyStoreData companyStoreData, String storeCode) {
    if (storeCode == null) {
      companyStoreData.setStoreId(null);
      return;
    }
    CObStorehouse storehouse = storehouseRep.findOneByUserCode(storeCode);
    companyStoreData.setStoreId(storehouse.getId());
  }

  private void setExpectedDeliveryDate(
      IObSalesOrderData salesOrderData, String expectedDeliveryDate) {
    if (expectedDeliveryDate == null) {
      salesOrderData.setExpectedDeliveryDate(null);
      return;
    }
    salesOrderData.setExpectedDeliveryDate(commandUtils.getDateTime(expectedDeliveryDate));
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setStorehouseRep(CObStorehouseRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setCompanyStoreDataRep(IObSalesOrderCompanyStoreDataRep companyStoreDataRep) {
    this.companyStoreDataRep = companyStoreDataRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
