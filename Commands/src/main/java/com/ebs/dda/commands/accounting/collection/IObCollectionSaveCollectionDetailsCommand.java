package com.ebs.dda.commands.accounting.collection;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.collection.*;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import io.reactivex.Observable;

public class IObCollectionSaveCollectionDetailsCommand
    implements ICommand<IObCollectionDetailsValueObject> {
  private DObCollectionRep collectionRep;
  private DObCollectionGeneralModelRep collectionGeneralModelRep;
  private IObCollectionDetailsRep collectionDetailsRep;
  private IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep;
  private CObTreasuryRep treasuryRep;
  private CommandUtils commandUtils;

  public IObCollectionSaveCollectionDetailsCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObCollectionDetailsValueObject collectionDetailsValueObject) throws Exception {
    String collectionCode = collectionDetailsValueObject.getCollectionCode();
    DObCollection collection = (DObCollection) collectionRep.findOneByUserCode(collectionCode);
    IObCollectionDetails collectionDetails =
        UpdateCollectionDetails(collectionDetailsValueObject, collectionCode);
    updateModificationInfoAndDate(collection, collectionDetails);

    collection.setHeaderDetail(IDObCollection.collectionDetails, collectionDetails);

    collectionRep.update(collection);
    return Observable.just(collectionCode);
  }

  private IObCollectionDetails UpdateCollectionDetails(
      IObCollectionDetailsValueObject collectionDetailsValueObject, String collectionCode) {
    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(collectionCode);
    IObCollectionDetails collectionDetails =
        collectionDetailsRep.findOneByRefInstanceId(collectionGeneralModel.getId());
    collectionDetails.setAmount(collectionDetailsValueObject.getAmount());
    if (collectionDetails
        .getCollectionMethod()
        .equals(CollectionMethodEnum.CollectionMethod.BANK.name())) {
      String bankAccountNumber = collectionDetailsValueObject.getBankAccountNumber();
      if (bankAccountNumber != null) {
        IObCompanyBankDetailsGeneralModel companyBankDetailsGeneralModel =
            companyBankDetailsGeneralModelRep.findOneByCompanyCodeAndAccountNumber(
                collectionGeneralModel.getCompanyCode(), bankAccountNumber);
        collectionDetails.setBankAccountId(companyBankDetailsGeneralModel.getId());
      }else {
        collectionDetails.setBankAccountId(null);
      }
    } else {
      String treasuryCode = collectionDetailsValueObject.getTreasuryCode();
      if (treasuryCode != null) {
        CObTreasury treasury = treasuryRep.findOneByUserCode(treasuryCode);
        collectionDetails.setTreasuryId(treasury.getId());
      }else {
        collectionDetails.setTreasuryId(null);
      }
    }
    return collectionDetails;
  }

  private void updateModificationInfoAndDate(
      DObCollection collection, IObCollectionDetails collectionDetails) {
    commandUtils.setModificationInfoAndModificationDate(collectionDetails);
    commandUtils.setModificationInfoAndModificationDate(collection);
  }

  public void setCollectionRep(DObCollectionRep collectionRep) {
    this.collectionRep = collectionRep;
  }

  public void setCollectionGeneralModelRep(DObCollectionGeneralModelRep collectionGeneralModelRep) {
    this.collectionGeneralModelRep = collectionGeneralModelRep;
  }

  public void setCollectionDetailsRep(IObCollectionDetailsRep collectionDetailsRep) {
    this.collectionDetailsRep = collectionDetailsRep;
  }

  public void setCompanyBankDetailsGeneralModelRep(
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep) {
    this.companyBankDetailsGeneralModelRep = companyBankDetailsGeneralModelRep;
  }

  public void setTreasuryRep(CObTreasuryRep treasuryRep) {
    this.treasuryRep = treasuryRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
