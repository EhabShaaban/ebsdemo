package com.ebs.dda.commands.accounting.collection;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentNotesReceivableAccountingDetails;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.NOTES_RECEIVABLES_GL_ACCOUNT;

public class DObCollectionMonetaryNoteAccountDeterminationCommand
    extends DObCollectionAccountDeterminationCommand {

  private DObNotesReceivablesGeneralModelRep notesReceivablesGMRep;

  @Override
  protected void getCreditAccountingDetailRecord(
      IObCollectionDetailsGeneralModel collectionDetailsGM, DObCollection collection) {

    IObAccountingDocumentNotesReceivableAccountingDetails creditAccountingDetail =
        getCreditAccountingDetail(collectionDetailsGM);

    commandUtils.setCreationAndModificationInfoAndDate(creditAccountingDetail);
    collection.addLineDetail(IDObAccountingDocument.accountingNotesReceivableDetailsAttr,
        creditAccountingDetail);
  }

  private IObAccountingDocumentNotesReceivableAccountingDetails getCreditAccountingDetail(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {
    IObAccountingDocumentNotesReceivableAccountingDetails accountingDetails =
        new IObAccountingDocumentNotesReceivableAccountingDetails();

    accountingDetails.setObjectTypeCode(IDocumentTypes.COLLECTION);
    accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
    accountingDetails.setAmount(collectionDetailsGM.getAmount());

    LObGlobalGLAccountConfigGeneralModel globalGLAccount =
        globalAccountConfigGMRep.findOneByUserCode(NOTES_RECEIVABLES_GL_ACCOUNT);
    accountingDetails.setGlAccountId(globalGLAccount.getGlAccountId());

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGMRep.findOneByUserCode(collectionDetailsGM.getRefDocumentCode());
    accountingDetails.setGlSubAccountId(notesReceivablesGeneralModel.getId());

    return accountingDetails;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGMRep = notesReceivablesGeneralModelRep;
  }


}
