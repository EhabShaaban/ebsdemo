package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.DObInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;

public class DObSalesInvoiceItemSaveCommand implements ICommand<DObInvoiceItemValueObject> {
  private CommandUtils commandUtils;
  private DObSalesInvoiceRep salesInvoiceRep;
  private MObItemGeneralModelRep mObItemGeneralModelRep;
  private CObMeasureRep cObMeasureRep;

  public DObSalesInvoiceItemSaveCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(DObInvoiceItemValueObject invoiceItemValueObject)
      throws Exception {
    DObSalesInvoice salesInvoice =
        salesInvoiceRep.findOneByUserCode(invoiceItemValueObject.getInvoiceCode());
    IObSalesInvoiceItem salesInvoiceItem = getSalesInvoiceItemInstance(invoiceItemValueObject);
    prepareSalesInvoiceItem(salesInvoice, salesInvoiceItem);

    commandUtils.setModificationInfoAndModificationDate(salesInvoice);
    salesInvoiceRep.update(salesInvoice);
    return Observable.just(salesInvoice.getUserCode());
  }

  private IObSalesInvoiceItem getSalesInvoiceItemInstance(
          DObInvoiceItemValueObject invoiceItemValueObject) {
    IObSalesInvoiceItem invoiceItem = new IObSalesInvoiceItem();

    MObItemGeneralModel item =
        mObItemGeneralModelRep.findByUserCode(invoiceItemValueObject.getItemCode());
    CObMeasure orderUnitMeasure =
        cObMeasureRep.findOneByUserCode(invoiceItemValueObject.getOrderUnitCode());

    invoiceItem.setItemId(item.getId());
    invoiceItem.setOrderUnitOfMeasureId(orderUnitMeasure.getId());
    invoiceItem.setQunatityInOrderUnit(invoiceItemValueObject.getQuantityInOrderUnit());
    invoiceItem.setPrice(invoiceItemValueObject.getUnitPrice());
    invoiceItem.setBaseUnitOfMeasureId(item.getBasicUnitOfMeasure());
    invoiceItem.setReturnedQuantity(BigDecimal.ZERO);
    return invoiceItem;
  }

  private void prepareSalesInvoiceItem(
      DObSalesInvoice salesInvoice, IObSalesInvoiceItem invoiceItem) {
    invoiceItem.setRefInstanceId(salesInvoice.getId());
    commandUtils.setCreationAndModificationInfoAndDate(invoiceItem);
    salesInvoice.addLineDetail(IDObSalesInvoice.salesInvoiceItemsAttr, invoiceItem);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setmObItemGeneralModelRep(MObItemGeneralModelRep mObItemGeneralModelRep) {
    this.mObItemGeneralModelRep = mObItemGeneralModelRep;
  }

  public void setcObMeasureRep(CObMeasureRep cObMeasureRep) {
    this.cObMeasureRep = cObMeasureRep;
  }

  public void setSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
    this.salesInvoiceRep = salesInvoiceRep;
  }
}
