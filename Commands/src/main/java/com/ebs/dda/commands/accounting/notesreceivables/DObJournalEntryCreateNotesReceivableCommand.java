package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObNotesReceivableJournalEntryRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry.CREDIT;
import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry.DEBIT;
import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers.Local_Customer;
import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers.Notes_Receivables;

public class DObJournalEntryCreateNotesReceivableCommand
  implements ICommand<DObNotesReceivableActivateValueObject> {

  private CommandUtils commandUtils;

  private DocumentObjectCodeGenerator codeGenerator;
  private DObNotesReceivableJournalEntryRep notesReceivableJournalEntryRep;
  private DObNotesReceivableJournalEntryPreparationGeneralModelRep journalEntryPreparationGMRep;
  private IObNotesReceivableAccountingDetailsGeneralModelRep accountingDetailsGMRep;

  public DObJournalEntryCreateNotesReceivableCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObNotesReceivableActivateValueObject valueObject)
    throws Exception {

    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM =
      journalEntryPreparationGMRep.findOneByCode(valueObject.getNotesReceivableCode());

    DObNotesReceivableJournalEntry journalEntry =
      createJournalEntry(preparationGM, valueObject.getJournalEntryDate());

    List<IObJournalEntryItem> journalEntryItems = createJournalEntryItems(preparationGM);

    journalEntry.setLinesDetails(IDObJournalEntry.journalItemAttr, journalEntryItems);
    notesReceivableJournalEntryRep.create(journalEntry);

    return Observable.just(journalEntry.getUserCode());
  }

  private DObNotesReceivableJournalEntry createJournalEntry(
    DObNotesReceivableJournalEntryPreparationGeneralModel journalEntryPreparationGM,
    String journalEntryDate) {

    DObNotesReceivableJournalEntry journalEntry = new DObNotesReceivableJournalEntry();
    commandUtils.setCreationAndModificationInfoAndDate(journalEntry);

    journalEntry.setDocumentReferenceId(journalEntryPreparationGM.getId());
    journalEntry.setUserCode(generateUserCode());

    journalEntry.setCurrentStates(commandUtils.initCurrenctStates(BasicStateMachine.ACTIVE_STATE));
    journalEntry.setDueDate(commandUtils.getDateTime(journalEntryDate));

    journalEntry.setCompanyId(journalEntryPreparationGM.getCompanyId());
    journalEntry.setPurchaseUnitId(journalEntryPreparationGM.getBusinessUnitId());
    journalEntry.setFiscalPeriodId(journalEntryPreparationGM.getFiscalPeriodId());

    return journalEntry;
  }

  private List<IObJournalEntryItem> createJournalEntryItems(
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM) {

    Map<String, List<IObNotesReceivableAccountingDetailsGeneralModel>> accDetailsItems =
      accountingDetailsGMRep
        .findByDocumentCodeOrderByAccountingEntryDesc(preparationGM.getCode())
        .stream()
        .collect(
          Collectors.groupingBy(IObNotesReceivableAccountingDetailsGeneralModel::getAccountingEntry)
        );

    return Stream
      .of(
        Arrays.asList(createNRDebitJEItem(accDetailsItems.get(DEBIT.name()).get(0), preparationGM)),
        createCreditJEItems(accDetailsItems.get(CREDIT.name()), preparationGM)
      )
      .flatMap(Collection::stream)
      .collect(Collectors.toList());
  }

  private IObJournalEntryItemNotesReceivableSubAccount createNRDebitJEItem(
    IObNotesReceivableAccountingDetailsGeneralModel debitAccDetails,
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM) {

    IObJournalEntryItemNotesReceivableSubAccount subAccount =
      new IObJournalEntryItemNotesReceivableSubAccount();
    commandUtils.setCreationAndModificationInfoAndDate(subAccount);

    subAccount.setAccountId(debitAccDetails.getGlAccountId());
    subAccount.setSubAccountId(debitAccDetails.getGlSubAccountId());

    subAccount.setDebit(debitAccDetails.getAmount());
    subAccount.setCredit(BigDecimal.ZERO);

    subAccount.setCompanyCurrencyId(preparationGM.getCompanyCurrencyId());
    subAccount.setDocumentCurrencyId(preparationGM.getDocumentCurrencyId());
    subAccount.setCompanyCurrencyPrice(preparationGM.getCurrencyPrice());
    subAccount.setCompanyExchangeRateId(preparationGM.getExchangeRateId());

    return subAccount;
  }

  private List<IObJournalEntryItem> createCreditJEItems(
    List<IObNotesReceivableAccountingDetailsGeneralModel> creditAccDetailsItems,
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM) {

    Map<String, List<IObNotesReceivableAccountingDetailsGeneralModel>> groupedAccDetailsItems =
      creditAccDetailsItems
        .stream()
        .collect(
          Collectors.groupingBy(IObNotesReceivableAccountingDetailsGeneralModel::getSubLedger)
        );

    return Stream
    .of(
      createCustomerJEItems(groupedAccDetailsItems.get(Local_Customer.name()), preparationGM),
      createCreditNRJEItems(groupedAccDetailsItems.get(Notes_Receivables.name()), preparationGM)
    )
    .flatMap(Collection::stream)
    .collect(Collectors.toList());
  }

  private List<IObJournalEntryItemCustomerSubAccount> createCustomerJEItems(
    List<IObNotesReceivableAccountingDetailsGeneralModel> creditAccDetailsItems,
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM) {

    if (creditAccDetailsItems == null || creditAccDetailsItems.isEmpty()) {
      return Collections.emptyList();
    }

    return creditAccDetailsItems
      .stream()
      .map(creditItem -> createCustomerJEItem(preparationGM, creditItem))
      .collect(Collectors.toList());
  }

  private IObJournalEntryItemCustomerSubAccount createCustomerJEItem(
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM,
    IObNotesReceivableAccountingDetailsGeneralModel creditItem) {

    IObJournalEntryItemCustomerSubAccount customerSubAccount =
      new IObJournalEntryItemCustomerSubAccount();
    customerSubAccount.setType(Local_Customer.name());

    return (IObJournalEntryItemCustomerSubAccount) createCreditJEItem(
      preparationGM,
      creditItem,
      customerSubAccount
    );
  }

  private List<IObJournalEntryItemNotesReceivableSubAccount> createCreditNRJEItems(
    List<IObNotesReceivableAccountingDetailsGeneralModel> creditAccDetailsItems,
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM) {

    if (creditAccDetailsItems == null || creditAccDetailsItems.isEmpty()) {
      return Collections.emptyList();
    }

    return creditAccDetailsItems
      .stream()
      .map(creditItem -> createCreditNRJEItem(preparationGM, creditItem))
      .collect(Collectors.toList());
  }

  private IObJournalEntryItemNotesReceivableSubAccount createCreditNRJEItem(
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM,
    IObNotesReceivableAccountingDetailsGeneralModel creditItem) {

    return (IObJournalEntryItemNotesReceivableSubAccount) createCreditJEItem(
      preparationGM,
      creditItem,
      new IObJournalEntryItemNotesReceivableSubAccount()
    );
  }

  private IObJournalEntryItem createCreditJEItem(
    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGM,
    IObNotesReceivableAccountingDetailsGeneralModel creditItem,
    IObJournalEntryItem subAccount) {

    commandUtils.setCreationAndModificationInfoAndDate(subAccount);

    subAccount.setAccountId(creditItem.getGlAccountId());
    subAccount.setSubAccountId(creditItem.getGlSubAccountId());

    subAccount.setDebit(BigDecimal.ZERO);
    subAccount.setCredit(creditItem.getAmount());

    subAccount.setCompanyCurrencyId(preparationGM.getCompanyCurrencyId());
    subAccount.setDocumentCurrencyId(preparationGM.getDocumentCurrencyId());
    subAccount.setCompanyCurrencyPrice(preparationGM.getCurrencyPrice());
    subAccount.setCompanyExchangeRateId(preparationGM.getExchangeRateId());

    return subAccount;
  }

  private String generateUserCode() {
    return codeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName());
  }

  public void setCodeGenerator(DocumentObjectCodeGenerator codeGenerator) {
    this.codeGenerator = codeGenerator;
  }

  public void setNotesReceivableJournalEntryRep(
    DObNotesReceivableJournalEntryRep notesReceivableJournalEntryRep) {
    this.notesReceivableJournalEntryRep = notesReceivableJournalEntryRep;
  }

  public void setJournalEntryPreparationGMRep(
    DObNotesReceivableJournalEntryPreparationGeneralModelRep journalEntryPreparationGMRep) {
    this.journalEntryPreparationGMRep = journalEntryPreparationGMRep;
  }

  public void setAccountingDetailsGMRep(
    IObNotesReceivableAccountingDetailsGeneralModelRep accountingDetailsGMRep) {
    this.accountingDetailsGMRep = accountingDetailsGMRep;
  }

  public void setUserAccountSecurityService(
    IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
