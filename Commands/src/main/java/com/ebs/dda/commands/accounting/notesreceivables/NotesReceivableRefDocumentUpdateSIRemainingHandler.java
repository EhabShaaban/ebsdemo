package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;

import java.math.BigDecimal;

public class NotesReceivableRefDocumentUpdateSIRemainingHandler
    extends NotesReceivableRefDocumentUpdateRemainingHandler {

  private DObSalesInvoiceRep salesInvoiceRep;
  private IObSalesInvoiceSummaryRep salesInvoiceSummaryRep;
  private IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerRep;

  public void updateRemaining(String refDocumentCode, BigDecimal amountToCollect, String nrCurrencyIso) throws Exception {
    DObSalesInvoice salesInvoice = getSalesInvoiceRefDocumentId(refDocumentCode);
    IObSalesInvoiceSummary salesInvoiceSummary = getSalesInvoiceSummaryBySiId(salesInvoice.getId());
    String siCurrencyIso = getSalesInvoiceCurrencyIso(salesInvoice.getId());
    BigDecimal amount = getAmountToCollectWithExchangeRate(nrCurrencyIso, siCurrencyIso, amountToCollect);
    subtractAmountToCollectFromSalesInvoiceRemaining(salesInvoice, salesInvoiceSummary, amount);
    salesInvoiceRep.update(salesInvoice);
  }

  private DObSalesInvoice getSalesInvoiceRefDocumentId(String refDocumentCode) {
    DObSalesInvoice salesInvoice = salesInvoiceRep.findOneByUserCode(refDocumentCode);
    return salesInvoice;
  }
  private IObSalesInvoiceSummary getSalesInvoiceSummaryBySiId(Long salesInvoiceId) {
    return salesInvoiceSummaryRep.findOneByRefInstanceId(salesInvoiceId);
  }

  private String getSalesInvoiceCurrencyIso(Long salesInvoiceId) {
    return salesInvoiceBusinessPartnerRep.findOneByRefInstanceId(salesInvoiceId).getCurrencyIso();
  }

  private void subtractAmountToCollectFromSalesInvoiceRemaining(DObSalesInvoice salesInvoice,
          IObSalesInvoiceSummary salesInvoiceSummary, BigDecimal amountToCollect) {
    salesInvoiceSummary.setRemaining(
        salesInvoiceSummary.getRemaining().subtract(amountToCollect));
    commandUtils.setModificationInfoAndModificationDate(salesInvoiceSummary);
    commandUtils.setModificationInfoAndModificationDate(salesInvoice);
    salesInvoice.setHeaderDetail(IDObSalesInvoice.summary, salesInvoiceSummary);
  }

  public void setIObSalesInvoiceSummaryRep(IObSalesInvoiceSummaryRep salesInvoiceSummaryRep) {
    this.salesInvoiceSummaryRep = salesInvoiceSummaryRep;
  }
  public void setDObSalesInvoiceRep(DObSalesInvoiceRep salesInvoiceRep) {
    this.salesInvoiceRep = salesInvoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setSalesInvoiceBusinessPartnerRep(IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerRep) {
    this.salesInvoiceBusinessPartnerRep = salesInvoiceBusinessPartnerRep;
  }
}
