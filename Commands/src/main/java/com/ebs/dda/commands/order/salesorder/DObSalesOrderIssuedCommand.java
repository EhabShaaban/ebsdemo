package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderUpdateStateValueObject;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;

public class DObSalesOrderIssuedCommand
    implements ICommand<DObSalesOrderUpdateStateValueObject> {

  private final CommandUtils commandUtils;
  private DObSalesOrderRep salesOrderRep;

  public DObSalesOrderIssuedCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObSalesOrderUpdateStateValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    commandUtils.setModificationInfoAndModificationDate(salesOrder);

    updateSalesOrderState(getSalesOrderStateMachine(salesOrder));

    salesOrderRep.update(salesOrder);

    return Observable.just(salesOrder.getUserCode());
  }

  private void updateSalesOrderState(AbstractStateMachine salesOrderStateMachine) {
    if (salesOrderStateMachine.canFireEvent(IDObSalesOrderActionNames.POST_GOODS_ISSUE)) {
      salesOrderStateMachine.fireEvent(IDObSalesOrderActionNames.POST_GOODS_ISSUE);
      salesOrderStateMachine.save();
    }
  }

  private DObSalesOrderStateMachine getSalesOrderStateMachine(DObSalesOrder salesOrder)
      throws Exception {
    DObSalesOrderStateMachine salesOrderStateMachine = new DObSalesOrderStateMachine();
    salesOrderStateMachine.initObjectState(salesOrder);
    return salesOrderStateMachine;
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
