package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceActionNames;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoicePostPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

public class DObVendorInvoiceActivateCommand implements ICommand<DObVendorInvoiceActivateValueObject> {

  private DObVendorInvoiceRep invoiceRep;
  private DObVendorInvoicePostPreparationGeneralModelRep
      vendorInvoicePostPreparationGeneralModelRep;

  private final CommandUtils commandUtils;

  private CObFiscalYearRep cObFiscalYearRep;


  public DObVendorInvoiceActivateCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      DObVendorInvoiceActivateValueObject invoiceActivateValueObject) throws Exception {

    DObVendorInvoice invoice =
        invoiceRep.findOneByUserCode(invoiceActivateValueObject.getVendorInvoiceCode());

           setPostingDetails(invoice, invoiceActivateValueObject);
           setVendorInvoiceData(invoice);

         invoiceRep.updateAndClear(invoice);

    return Observable.just(invoice.getUserCode());
  }

  private void setPostingDetails(
      DObVendorInvoice invoice, DObVendorInvoiceActivateValueObject invoicePostValueObject) {

    DObVendorInvoicePostPreparationGeneralModel vendorInvoicePostPreparationGeneralModel =
        vendorInvoicePostPreparationGeneralModelRep.findOneByCode(
            invoicePostValueObject.getVendorInvoiceCode());

    IObVendorInvoicePostingDetails postingDetails = new IObVendorInvoicePostingDetails();
    setAccountant(postingDetails);
    postingDetails.setCurrencyPrice(new BigDecimal(vendorInvoicePostPreparationGeneralModel.getCurrencyprice()));
    setDueDate(invoicePostValueObject, postingDetails);
    postingDetails.setActivationDate(new DateTime());
    postingDetails.setExchangeRateId(
              vendorInvoicePostPreparationGeneralModel.getExchangerateid());
    setFiscalYear(invoicePostValueObject, postingDetails);
    commandUtils.setCreationAndModificationInfoAndDate(postingDetails);
    invoice.setHeaderDetail(IDObVendorInvoice.postingDetailsAttr, postingDetails);
  }

  private void setDueDate(DObVendorInvoiceActivateValueObject invoicePostValueObject, IObVendorInvoicePostingDetails postingDetails) {
    DateTime dueDate = commandUtils.getDateTime(invoicePostValueObject.getJournalDate());
    postingDetails.setDueDate(dueDate);
  }

  private void setAccountant(IObVendorInvoicePostingDetails postingDetails) {
    String loggedInUser = commandUtils.getLoggedInUser();
    postingDetails.setAccountant(loggedInUser);
  }

  private void setFiscalYear(DObVendorInvoiceActivateValueObject invoicePostValueObject, IObVendorInvoicePostingDetails postingDetails) {
    String journalDateStr = invoicePostValueObject.getJournalDate();
    DateTime journalDate = commandUtils.getDateTime(journalDateStr);
    String dueDateYear = Integer.toString(journalDate.getYear());
    List<CObFiscalYear> activeFiscalYears = cObFiscalYearRep.findByCurrentStatesLike("%Active%");
    CObFiscalYear fiscalYear = activeFiscalYears.stream()
            .filter(activeFiscalYear -> dueDateYear.equals(activeFiscalYear.getName().getValue(ISysDefaultLocales.ENGLISH_LANG_KEY)))
            .findAny()
            .orElse(null);
    postingDetails.setFiscalPeriodId(fiscalYear.getId());
  }

  private void setVendorInvoiceData(DObVendorInvoice invoice) throws Exception {
    this.commandUtils.setModificationInfoAndModificationDate(invoice);
    setInvoicePostState(createInvoiceStateMachine(invoice));
  }

  private void setInvoicePostState(AbstractStateMachine invoiceStateMachine) throws Exception {
    if (invoiceStateMachine.canFireEvent(IDObInvoiceActionNames.POST_INVOICE)) {
      invoiceStateMachine.fireEvent(IDObInvoiceActionNames.POST_INVOICE);
      invoiceStateMachine.save();
    }
  }

  private DObVendorInvoiceStateMachine createInvoiceStateMachine(DObVendorInvoice invoice)
      throws Exception {
    DObVendorInvoiceStateMachine dObInvoiceStateMachine = new DObVendorInvoiceStateMachine();
    dObInvoiceStateMachine.initObjectState(invoice);
    return dObInvoiceStateMachine;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setInvoiceRep(DObVendorInvoiceRep invoiceRep) {
    this.invoiceRep = invoiceRep;
  }

  public void setVendorInvoicePostPreparationGeneralModelRep(
      DObVendorInvoicePostPreparationGeneralModelRep vendorInvoicePostPreparationGeneralModelRep) {
    this.vendorInvoicePostPreparationGeneralModelRep = vendorInvoicePostPreparationGeneralModelRep;
  }

  public void setcObFiscalYearRep(CObFiscalYearRep cObFiscalYearRep) {
    this.cObFiscalYearRep = cObFiscalYearRep;
  }
}
