package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderDeletionValueObject;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import io.reactivex.Observable;

public class DObSalesReturnOrderDeleteCommand
    implements ICommand<DObSalesReturnOrderDeletionValueObject> {

  private DObSalesReturnOrderRep salesReturnOrderRep;

  @Override
  public Observable<String> executeCommand(DObSalesReturnOrderDeletionValueObject valueObject)
      throws Exception {

    DObSalesReturnOrder salesReturnOrder =
        salesReturnOrderRep.findOneByUserCode(valueObject.getSalesReturnOrderCode());
    salesReturnOrderRep.delete(salesReturnOrder);

    return Observable.just("SUCCESS");
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }
}
