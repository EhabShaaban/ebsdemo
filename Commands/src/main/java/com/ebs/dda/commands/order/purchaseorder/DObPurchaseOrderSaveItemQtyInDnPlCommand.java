package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsValueObject;
import io.reactivex.Observable;
import lombok.SneakyThrows;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class DObPurchaseOrderSaveItemQtyInDnPlCommand
    implements ICommand<IObOrderLineDetailsValueObject> {

  private final CommandUtils commandUtils;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  private IObOrderLineDetailsRep orderLineDetailsRep;
  private DObPurchaseOrderRep orderRep;
  private PlatformTransactionManager transactionManager;

  public DObPurchaseOrderSaveItemQtyInDnPlCommand() {
    commandUtils = new CommandUtils();
  }

  public void setOrderLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
  }

  public void setOrderLineDetailsRep(IObOrderLineDetailsRep orderLineDetailsRep) {
    this.orderLineDetailsRep = orderLineDetailsRep;
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  @Override
  public Observable<String> executeCommand(
      IObOrderLineDetailsValueObject orderLineDetailsValueObject) {
    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @SneakyThrows
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            IObOrderLineDetails orderLineDetails =
                getOrderLineDetailsByOrderCodeAndItemCode(
                    orderLineDetailsValueObject.getPurchaseOrderCode(),
                    orderLineDetailsValueObject.getItemCode());

            updateItem(orderLineDetails, orderLineDetailsValueObject);

            updatePurchaseOrder(orderLineDetails, orderLineDetailsValueObject);
          }
        });
    return Observable.just("SUCCESS");
  }

  private void updateItem(
      IObOrderLineDetails orderLineDetails,
      IObOrderLineDetailsValueObject orderLineDetailsValueObject) {
    orderLineDetails.setQuantityInDn(orderLineDetailsValueObject.getQtyInDn());
    commandUtils.setModificationInfoAndModificationDate(orderLineDetails);
  }

  private void updatePurchaseOrder(
      IObOrderLineDetails orderLineDetails,
      IObOrderLineDetailsValueObject orderLineDetailsValueObject)
      throws Exception {
    DObPurchaseOrder purchaseOrder =
        orderRep.findOneByUserCode(orderLineDetailsValueObject.getPurchaseOrderCode());
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.addLineDetail(IDObPurchaseOrder.lineAttr, orderLineDetails);
    orderRep.update(purchaseOrder);
  }

  private IObOrderLineDetails getOrderLineDetailsByOrderCodeAndItemCode(
      String orderCode, String itemCode) {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(orderCode, itemCode);
    return orderLineDetailsRep.findOneById(orderLineDetailsGeneralModel.getId());
  }
}
