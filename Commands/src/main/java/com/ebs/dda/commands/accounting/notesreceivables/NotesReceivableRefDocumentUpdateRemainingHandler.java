package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class NotesReceivableRefDocumentUpdateRemainingHandler {
  protected CommandUtils commandUtils = new CommandUtils();
  protected CObExchangeRateGeneralModelRep exchangeRateRep;

  public abstract void updateRemaining(String refDocumentCode, BigDecimal amountToCollect, String nrCurrencyIso) throws Exception;

  public BigDecimal getAmountToCollectWithExchangeRate(String nrCurrencyIso, String refDocCurrencyIso, BigDecimal amountToCollect) {
    CObExchangeRateGeneralModel exchangeRate = exchangeRateRep.findLatestDefaultExchangeRate(nrCurrencyIso, refDocCurrencyIso);
    if (exchangeRate != null) {
      return amountToCollect.multiply(exchangeRate.getSecondValue());
    } else {
      exchangeRate = exchangeRateRep.findLatestDefaultExchangeRate(refDocCurrencyIso, nrCurrencyIso);
      return amountToCollect.divide(exchangeRate.getSecondValue(), 10, RoundingMode.FLOOR);
    }
  }

  public void setExchangeRateRep(CObExchangeRateGeneralModelRep exchangeRateRep) {
    this.exchangeRateRep = exchangeRateRep;
  }
}
