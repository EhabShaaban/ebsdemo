package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.factory.DObPurchaseOrderFactory;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderTax;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderBillToCompany;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderFulFillerVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderCreateCommand implements ICommand<DObPurchaseOrderCreateValueObject> {

  private final CommandUtils commandUtils;
  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObPurchaseOrderFactory purchaseOrderFactory;
  private final DObPurchaseOrderStateMachineFactory stateMachineFactory;

  private DObPurchaseOrderRep purchaseOrderRep;
  private CObEnterpriseRep<CObPurchasingUnit> enterpriseRep;
  private IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep;
  private LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep;
  private MObVendorGeneralModelRep vendorGeneralModelRep;
  private IObOrderCompanyGeneralModel orderCompany;

  public PurchaseOrderCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      DObPurchaseOrderStateMachineFactory stateMachineFactory) {
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    this.stateMachineFactory = stateMachineFactory;
    commandUtils = new CommandUtils();
    purchaseOrderFactory = new DObPurchaseOrderFactory();
  }

  @Override
  public Observable<String> executeCommand(DObPurchaseOrderCreateValueObject valueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObPurchaseOrder.class.getSimpleName());

    DObPurchaseOrder purchaseOrder =
        purchaseOrderFactory.createPurchaseOrder(valueObject.getType().name());

    commandUtils.setCreationAndModificationInfoAndDate(purchaseOrder);

    setGeneralData(valueObject, userCode, purchaseOrder);
    setVendorData(valueObject, purchaseOrder);

    setCompanyData(valueObject, purchaseOrder);
    setCompanyTaxesDataIfService(valueObject, purchaseOrder);

    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrderRep.create(purchaseOrder);
    return Observable.just(userCode);
  }

  private void setGeneralData(
      DObPurchaseOrderCreateValueObject valueObject,
      String userCode,
      DObPurchaseOrder purchaseOrder)
      throws Exception {
    purchaseOrder.setUserCode(userCode);
    purchaseOrder.setDocumentOwnerId(valueObject.getDocumentOwnerId());
    stateMachineFactory.createPurchaseOrderStateMachine(purchaseOrder);
  }

  private void setCompanyData(
      DObPurchaseOrderCreateValueObject valueObject, DObPurchaseOrder purchaseOrder) {
    IObPurchaseOrderBillToCompany billToCompany = new IObPurchaseOrderBillToCompany();
    commandUtils.setCreationAndModificationInfoAndDate(billToCompany);

    setCompanyDataBusinessUnitId(valueObject, billToCompany);
    setCompanyDataCompanyIdIfService(valueObject, billToCompany);

    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.companyAttr, billToCompany);
  }

  private void setCompanyDataCompanyIdIfService(
      DObPurchaseOrderCreateValueObject valueObject, IObPurchaseOrderBillToCompany billToCompany) {
    if (isServicePO(valueObject)) {
      orderCompany =
          orderCompanyGeneralModelRep.findByUserCode(valueObject.getReferencePurchaseOrderCode());
      billToCompany.setCompanyId(orderCompany == null ? null : orderCompany.getCompanyId());
    }
  }

  private void setCompanyDataBusinessUnitId(
      DObPurchaseOrderCreateValueObject valueObject, IObPurchaseOrderBillToCompany billToCompany) {
    Long businessUnitId = getBusinessUnitIdBasedOnPurchaseOrderType(valueObject);
    billToCompany.setBusinessUnitId(businessUnitId);
  }

  private void setVendorData(
      DObPurchaseOrderCreateValueObject valueObject, DObPurchaseOrder purchaseOrder) {
    IObPurchaseOrderFulFillerVendor fulFillerVendor = new IObPurchaseOrderFulFillerVendor();
    commandUtils.setCreationAndModificationInfoAndDate(fulFillerVendor);
    MObVendorGeneralModel vendorGeneralModel =
        vendorGeneralModelRep.findOneByUserCode(valueObject.getVendorCode());
    fulFillerVendor.setVendorId(vendorGeneralModel.getId());
    setReferencePurchaseOrderIdIfService(valueObject, fulFillerVendor);

    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.vendorAttr, fulFillerVendor);
  }

  private void setReferencePurchaseOrderIdIfService(
      DObPurchaseOrderCreateValueObject valueObject,
      IObPurchaseOrderFulFillerVendor fulFillerVendor) {
    if (isServicePO(valueObject)) {
      Long referencePoId =
          purchaseOrderRep.findOneByUserCode(valueObject.getReferencePurchaseOrderCode()).getId();
      fulFillerVendor.setReferencePoId(referencePoId);
    }
  }

  private void setCompanyTaxesDataIfService(
      DObPurchaseOrderCreateValueObject valueObject, DObPurchaseOrder purchaseOrder) {
    List<IObOrderTax> orderTaxes = new ArrayList<>();
    if (isServicePO(valueObject) && orderCompany != null) {
      setOrderCompanyTaxes(orderTaxes);
    }
    purchaseOrder.setLinesDetails(IDObPurchaseOrder.taxAttr, orderTaxes);
  }

  private void setOrderCompanyTaxes(List<IObOrderTax> orderTaxes) {
    List<LObCompanyTaxesGeneralModel> taxes =
        companyTaxesGeneralModelRep.findAllByCompanyCode(orderCompany.getCompanyCode());
    taxes.forEach(tax -> setOrderTaxData(orderTaxes, tax));
  }

  private void setOrderTaxData(List<IObOrderTax> orderTaxes, LObCompanyTaxesGeneralModel tax) {
    IObOrderTax orderTax = new IObOrderTax();
    commandUtils.setCreationAndModificationInfoAndDate(orderTax);
    orderTax.setCode(tax.getUserCode());
    orderTax.setName(tax.getName());
    orderTax.setPercentage(tax.getPercentage());
    orderTaxes.add(orderTax);
  }

  private Long getBusinessUnitIdFromCobEnterprise(DObPurchaseOrderCreateValueObject valueObject) {
    Long businessUnitId;
    CObPurchasingUnit businessUnit =
        enterpriseRep.findOneByUserCode(valueObject.getBusinessUnitCode());
    businessUnitId = businessUnit.getId();
    return businessUnitId;
  }

  private Long getBusinessUnitIdFromRefPurchaseOrder(
      DObPurchaseOrderCreateValueObject valueObject) {
    Long businessUnitId;
    IObOrderCompanyGeneralModel refPurchaseOrderCompany =
        orderCompanyGeneralModelRep.findByUserCode(valueObject.getReferencePurchaseOrderCode());
    businessUnitId = refPurchaseOrderCompany.getPurchaseUnitId();
    return businessUnitId;
  }

  private Long getBusinessUnitIdBasedOnPurchaseOrderType(
      DObPurchaseOrderCreateValueObject valueObject) {
    Long businessUnitId;
    if (isServicePO(valueObject)) {
      businessUnitId = getBusinessUnitIdFromRefPurchaseOrder(valueObject);
    } else {
      businessUnitId = getBusinessUnitIdFromCobEnterprise(valueObject);
    }
    return businessUnitId;
  }

  private boolean isServicePO(DObPurchaseOrderCreateValueObject valueObject) {
    return valueObject.getType().name().equals(OrderTypeEnum.SERVICE_PO.name());
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setVendorGeneralModelRep(MObVendorGeneralModelRep vendorGeneralModelRep) {
    this.vendorGeneralModelRep = vendorGeneralModelRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setEnterpriseRep(CObEnterpriseRep<CObPurchasingUnit> enterpriseRep) {
    this.enterpriseRep = enterpriseRep;
  }

  public void setCompanyTaxesGeneralModelRep(
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep) {
    this.companyTaxesGeneralModelRep = companyTaxesGeneralModelRep;
  }

  public void setOrderCompanyGeneralModelRep(
      IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep) {
    this.orderCompanyGeneralModelRep = orderCompanyGeneralModelRep;
  }

  public void setPurchaseOrderFactory(DObPurchaseOrderFactory purchaseOrderFactory) {
    this.purchaseOrderFactory = purchaseOrderFactory;
  }
}
