package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItem;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemValueObject;
import com.ebs.dda.repositories.masterdata.lookups.LObReasonRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemRep;
import io.reactivex.Observable;

public class IObSalesReturnOrderSaveItemCommand
    implements ICommand<IObSalesReturnOrderItemValueObject> {

  private final String RETURN_REASON_TYPE = "SRO_REASON";
  private DObSalesReturnOrderRep salesReturnOrderRep;
  private IObSalesReturnOrderItemRep salesReturnOrderItemRep;
  private LObReasonRep reasonRep;
  private CommandUtils commandUtils;

  public IObSalesReturnOrderSaveItemCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<DObSalesReturnOrder> executeCommand(
      IObSalesReturnOrderItemValueObject valueObject) throws Exception {
    DObSalesReturnOrder salesReturnOrder =
        salesReturnOrderRep.findOneByUserCode(valueObject.getSalesReturnOrderCode());
    IObSalesReturnOrderItem salesReturnOrderItem = prepareSalesReturnOrderItem(valueObject);
    updateObjectsInfo(salesReturnOrder, salesReturnOrderItem);

    salesReturnOrder.addLineDetail(IDObSalesReturnOrder.itemsAttr, salesReturnOrderItem);
    salesReturnOrderRep.update(salesReturnOrder);
    return Observable.just(salesReturnOrder);
  }

  private void updateObjectsInfo(
      DObSalesReturnOrder salesReturnOrder, IObSalesReturnOrderItem salesReturnOrderItem) {
    commandUtils.setCreationAndModificationInfoAndDate(salesReturnOrderItem);
    commandUtils.setModificationInfoAndModificationDate(salesReturnOrder);
  }

  private IObSalesReturnOrderItem prepareSalesReturnOrderItem(
      IObSalesReturnOrderItemValueObject valueObject) {
    IObSalesReturnOrderItem salesReturnOrderItem =
        salesReturnOrderItemRep.findOneById(valueObject.getItemId());
    salesReturnOrderItem.setReturnQuantity(valueObject.getReturnQuantity());
    salesReturnOrderItem.setReturnReasonId(
        getSalesReturnReasonId(valueObject.getReturnReasonCode()));
    return salesReturnOrderItem;
  }

  private Long getSalesReturnReasonId(String returnReasonCode) {
    if(returnReasonCode == null)
      return null;

    return reasonRep.findOneByUserCodeAndType(returnReasonCode, RETURN_REASON_TYPE).getId();
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setSalesReturnOrderItemRep(IObSalesReturnOrderItemRep salesReturnOrderItemRep) {
    this.salesReturnOrderItemRep = salesReturnOrderItemRep;
  }

  public void setReasonRep(LObReasonRep reasonRep) {
    this.reasonRep = reasonRep;
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
