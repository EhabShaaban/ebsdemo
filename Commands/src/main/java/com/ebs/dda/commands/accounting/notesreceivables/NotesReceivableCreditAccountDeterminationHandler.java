package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;

public abstract class NotesReceivableCreditAccountDeterminationHandler {
  protected CommandUtils commandUtils = new CommandUtils();
  protected LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;

  public abstract void setCreditAccountingDetailRecord(
      IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel,
      DObMonetaryNotes notesReceivable,
      IObMonetaryNotesDetailsGeneralModel notesDetails);

  public void setGlobalAccountConfigGeneralModelRep(
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
    this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
