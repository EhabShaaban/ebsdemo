package com.ebs.dda.commands.accounting.invoice;

import java.util.LinkedList;
import java.util.List;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesValueObject;
import com.ebs.dda.jpa.accounting.IObTaxes;
import com.ebs.dda.jpa.accounting.taxes.LObTaxInfo;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesRep;
import com.ebs.dda.repositories.accounting.taxes.LObTaxInfoRep;
import io.reactivex.Observable;

public class IObInvoiceTaxCopyCommand<M extends DObAccountingDocument, D extends IObTaxes>
    implements ICommand<IObInvoiceTaxesValueObject> {

  private IObInvoiceTaxesRep<D> invoiceTaxesRep;

  private LObTaxInfoRep<? extends LObTaxInfo> taxesInfoRep;

  @Override
  public Observable<String> executeCommand(IObInvoiceTaxesValueObject valueObject)
          throws Exception {

    M invoice = (M) valueObject.getInvoice();

    List<D> taxObjs = invoiceTaxesRep.findByRefInstanceId(invoice.getId());
    List<? extends LObTaxInfo> allTaxesInfo = taxesInfoRep.findAll();
    List<D> updatedTaxInfoList = new LinkedList<>();

    for (D taxObj : taxObjs) {
      LObTaxInfo taxInfo = allTaxesInfo.stream()
              .filter(tax -> taxObj.getCode().equals(tax.getUserCode()))
              .findFirst()
              .orElse(null);
      taxObj.setPercentage(taxInfo.getPercentage());
      updatedTaxInfoList.add(taxObj);
    }
    invoiceTaxesRep.saveAll(updatedTaxInfoList);
    return Observable.just("SUCCESS");
  }

  public void setInvoiceTaxesRep(IObInvoiceTaxesRep<D> invoiceTaxesRep) {
    this.invoiceTaxesRep = invoiceTaxesRep;
  }

  public void setTaxesInfoRep(LObTaxInfoRep<? extends LObTaxInfo> taxesInfoRep) {
    this.taxesInfoRep = taxesInfoRep;
  }

}
