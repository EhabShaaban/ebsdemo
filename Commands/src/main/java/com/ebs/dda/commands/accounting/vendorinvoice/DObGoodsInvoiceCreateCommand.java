package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItem;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoicePurchaseOrder;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPaymentAndDeliveryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPaymentAndDeliveryGeneralModelRep;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DObGoodsInvoiceCreateCommand extends DObVendorInvoiceCreateCommand {

  private DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep;
  private IObOrderLineDetailsRep orderLineDetailsRep;
  private IObOrderLineDetailsQuantitiesRep quantitiesRep;

  public DObGoodsInvoiceCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    super(documentObjectCodeGenerator);
  }

  @Override
  protected void setVendorInvoiceItems(DObPurchaseOrder order, DObVendorInvoice dObInvoice) {

    List<IObVendorInvoiceItem> invoiceItems = new LinkedList<>();
    List<IObOrderLineDetails> orderLineDetailsList =
        orderLineDetailsRep.findByRefInstanceId(order.getId());
    for (IObOrderLineDetails orderLine : orderLineDetailsList) {
      List<IObOrderLineDetailsQuantities> orderLineQuantities =
          quantitiesRep.findByRefInstanceId(orderLine.getId());
      for (IObOrderLineDetailsQuantities quantity : orderLineQuantities) {
        BigDecimal itemPrice =
            quantity.getPrice().subtract((quantity.getPrice()
                    .multiply(quantity.getDiscountPercentage())
                    .divide(BigDecimal.valueOf(100))));
        IObVendorInvoiceItem item = new IObVendorInvoiceItem();
        commandUtils.setCreationAndModificationInfoAndDate(item);
        item.setBaseUnitOfMeasureId(orderLine.getUnitOfMeasureId());
        item.setItemId(orderLine.getItemId());
        item.setOrderUnitOfMeasureId(quantity.getOrderUnitId());
        item.setPrice(itemPrice);
        item.setQunatityInOrderUnit(quantity.getQuantity());
        invoiceItems.add(item);
      }
    }
    Collections.reverse(invoiceItems);
    dObInvoice.setLinesDetails(IDObVendorInvoice.invoiceItemsAttr, invoiceItems);
  }

  @Override
  protected void setVendorInvoiceReferenceOrderPaymentProperties(
      IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder, DObPurchaseOrder order) {

    DObPaymentAndDeliveryGeneralModel dObPaymentAndDeliveryGeneralModel =
        paymentAndDeliveryGeneralModelRep.findByUserCode(order.getUserCode());

    setCurrency(iObInvoicePurchaseOrder, dObPaymentAndDeliveryGeneralModel.getCurrencyCode());
    setPaymentTerm(iObInvoicePurchaseOrder, dObPaymentAndDeliveryGeneralModel.getPaymentTermCode());
  }

  public void setPaymentAndDeliveryGeneralModelRep(
      DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep) {
    this.paymentAndDeliveryGeneralModelRep = paymentAndDeliveryGeneralModelRep;
  }

  public void setOrderLineDetailsRep(IObOrderLineDetailsRep orderLineDetailsRep) {
    this.orderLineDetailsRep = orderLineDetailsRep;
  }

  public void setQuantitiesRep(IObOrderLineDetailsQuantitiesRep quantitiesRep) {
    this.quantitiesRep = quantitiesRep;
  }
}
