package com.ebs.dda.commands.accounting.collection;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentBankAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentTreasuryAccountingDetails;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionAccountDetermineValueObject;
import com.ebs.dda.jpa.accounting.collection.IObCollectionCompanyDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionCompanyDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import io.reactivex.Observable;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT;
import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT;
import static com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum.CollectionMethod.CASH;

public abstract class DObCollectionAccountDeterminationCommand
    implements ICommand<DObCollectionAccountDetermineValueObject> {

  private DObCollectionRep collectionRep;

  private IObCollectionDetailsGeneralModelRep collectionDetailsGMRep;
  private CObTreasuryRep treasuryRep;
  private IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep;
  private IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGMRep;

  protected MObCustomerGeneralModelRep customerGeneralModelRep;
  protected LObGlobalAccountConfigGeneralModelRep globalAccountConfigGMRep;
  protected CommandUtils commandUtils;

  public DObCollectionAccountDeterminationCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObCollectionAccountDetermineValueObject valueObject)
      throws Exception {

    DObCollection collection =
        (DObCollection) collectionRep.findOneByUserCode(valueObject.getCollectionCode());
    handleCollectionAccountDetermination(valueObject, collection);
    setCollectionData(collection);
    collectionRep.update(collection);

    return Observable.just(valueObject.getCollectionCode());
  }

  protected void setCollectionData(DObCollection collection) {
    commandUtils.setModificationInfoAndModificationDate(collection);
  }

  protected void handleCollectionAccountDetermination(
      DObCollectionAccountDetermineValueObject valueObject, DObCollection collection)
      throws Exception {

    IObCollectionDetailsGeneralModel collectionDetailsGM =
        collectionDetailsGMRep.findByCollectionCode(valueObject.getCollectionCode());

    getDebitAccountingDetailRecord(collectionDetailsGM, collection);

    getCreditAccountingDetailRecord(collectionDetailsGM, collection);
  }

  private void getDebitAccountingDetailRecord(IObCollectionDetailsGeneralModel collectionDetailsGM,
      DObCollection collection) {
    if (isCashMethod(collectionDetailsGM.getCollectionMethod())) {
      createCashDebitAccountingDetails(collectionDetailsGM, collection);
    } else {
      createBankAccountingDetails(collectionDetailsGM, collection);
    }
  }

  private void createCashDebitAccountingDetails(
      IObCollectionDetailsGeneralModel collectionDetailsGM, DObCollection collection) {
    IObAccountingDocumentTreasuryAccountingDetails debitAccountingDetail =
        getDebitAccountingDetailForCash(collectionDetailsGM);

    commandUtils.setCreationAndModificationInfoAndDate(debitAccountingDetail);
    collection.addLineDetail(IDObAccountingDocument.accountingCashDetailsAttr,
        debitAccountingDetail);
  }

  private void createBankAccountingDetails(IObCollectionDetailsGeneralModel collectionDetailsGM,
      DObCollection collection) {
    IObAccountingDocumentBankAccountingDetails debitAccountingDetail =
        getDebitAccountingDetailForBank(collectionDetailsGM);

    commandUtils.setCreationAndModificationInfoAndDate(debitAccountingDetail);

    collection.addLineDetail(IDObAccountingDocument.accountingBankDetailsAttr,
        debitAccountingDetail);
  }

  private boolean isCashMethod(String collectionMethod) {
    return CASH.name().equals(collectionMethod);
  }

  private IObAccountingDocumentBankAccountingDetails getDebitAccountingDetailForBank(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {

    IObAccountingDocumentBankAccountingDetails bankAccDetails =
        new IObAccountingDocumentBankAccountingDetails();

    bankAccDetails.setObjectTypeCode(IDocumentTypes.COLLECTION);
    bankAccDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
    bankAccDetails.setAmount(collectionDetailsGM.getAmount());

    LObGlobalGLAccountConfigGeneralModel bankDebitGlobalGLAccount =
        globalAccountConfigGMRep.findOneByUserCode(BANK_GL_ACCOUNT);
    bankAccDetails.setGlAccountId(bankDebitGlobalGLAccount.getGlAccountId());

    IObCompanyBankDetailsGeneralModel companyBankDetailsGM =
        getCompanyBankDetails(collectionDetailsGM);
    bankAccDetails.setGlSubAccountId(companyBankDetailsGM.getId());
    return bankAccDetails;
  }

  private IObCompanyBankDetailsGeneralModel getCompanyBankDetails(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {

    IObCollectionCompanyDetailsGeneralModel collectionCompanyDetailsGM =
        collectionCompanyDetailsGMRep
            .findOneByCollectionCode(collectionDetailsGM.getCollectionCode());

    IObCompanyBankDetailsGeneralModel companyBankDetailsGM =
        companyBankDetailsGeneralModelRep.findOneByCodeAndCompanyCodeAndAccountNumber(
            collectionDetailsGM.getBankAccountCode(), collectionCompanyDetailsGM.getCompanyCode(),
            collectionDetailsGM.getBankAccountNumber());
    return companyBankDetailsGM;
  }

  private IObAccountingDocumentTreasuryAccountingDetails getDebitAccountingDetailForCash(
      IObCollectionDetailsGeneralModel collectionDetailsGM) {

    IObAccountingDocumentTreasuryAccountingDetails treasuryAccDetails =
        new IObAccountingDocumentTreasuryAccountingDetails();

    treasuryAccDetails.setObjectTypeCode(IDocumentTypes.COLLECTION);
    treasuryAccDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
    treasuryAccDetails.setAmount(collectionDetailsGM.getAmount());

    LObGlobalGLAccountConfigGeneralModel cashDebitGlobalGLAccount =
        globalAccountConfigGMRep.findOneByUserCode(CASH_GL_ACCOUNT);
    treasuryAccDetails.setGlAccountId(cashDebitGlobalGLAccount.getGlAccountId());

    CObTreasury treasury = treasuryRep.findOneByUserCode(collectionDetailsGM.getTreasuryCode());
    treasuryAccDetails.setGlSubAccountId(treasury.getId());
    return treasuryAccDetails;
  }

  protected abstract void getCreditAccountingDetailRecord(
      IObCollectionDetailsGeneralModel collectionDetailsGM, DObCollection collection);

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCollectionRep(DObCollectionRep collectionRep) {
    this.collectionRep = collectionRep;
  }

  public void setCollectionDetailsGeneralModelRep(
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep) {
    this.collectionDetailsGMRep = collectionDetailsGeneralModelRep;
  }

  public void setCustomerGeneralModelRep(MObCustomerGeneralModelRep customerGeneralModelRep) {
    this.customerGeneralModelRep = customerGeneralModelRep;
  }

  public void setTreasuryRep(CObTreasuryRep treasuryRep) {
    this.treasuryRep = treasuryRep;
  }

  public void setCompanyBankDetailsGeneralModelRep(
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep) {
    this.companyBankDetailsGeneralModelRep = companyBankDetailsGeneralModelRep;
  }

  public void setCollectionCompanyDetailsGeneralModelRep(
      IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGeneralModelRep) {
    this.collectionCompanyDetailsGMRep = collectionCompanyDetailsGeneralModelRep;
  }

  public void setGlobalAccountConfigGeneralModelRep(
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
    this.globalAccountConfigGMRep = globalAccountConfigGeneralModelRep;
  }

}
