package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import io.reactivex.Observable;

public class DObVendorInvoiceDeleteCommand implements ICommand {

  private DObVendorInvoiceRep vendorInvoiceRep;

  public DObVendorInvoiceDeleteCommand() {}

  @Override
  public Observable executeCommand(Object vendorInvoiceCode) throws Exception {
    vendorInvoiceRep.delete(readEntityByCode((String) vendorInvoiceCode));
    return Observable.empty();
  }

  private DObVendorInvoice readEntityByCode(String vendorInvoiceCode) {
    return vendorInvoiceRep.findOneByUserCode(vendorInvoiceCode);
  }

  public void setVendorInvoiceRep(DObVendorInvoiceRep vendorInvoiceRep) {
    this.vendorInvoiceRep = vendorInvoiceRep;
  }
}