package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxes;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxes;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceCompanyDataRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class IObLocalGoodsInvoiceTaxesAddCommand implements ICommand<DObVendorInvoice> {
	private CommandUtils commandUtils;
	private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
	private IObVendorInvoiceTaxesRep taxesRep;
	private LObCompanyTaxesRep lObCompanyTaxesRep;
	private IObVendorInvoiceCompanyDataRep vendorInvoiceCompanyDataRep;

	public IObLocalGoodsInvoiceTaxesAddCommand() {
		commandUtils = new CommandUtils();
	}

	@Transactional
	@Override
	public Observable<String> executeCommand(DObVendorInvoice vendorInvoice) throws Exception {

		IObAccountingDocumentCompanyData companyData = vendorInvoiceCompanyDataRep
			.findOneByRefInstanceId(vendorInvoice.getId());

		List<IObVendorInvoiceTaxes> vendorInvoiceTaxes = lObCompanyTaxesRep
			.findAllByCompanyId(companyData.getCompanyId())
			.stream()
			.map(taxGM -> createTaxEntity(vendorInvoice, taxGM))
			.collect(Collectors.toList());

		taxesRep.saveAll(vendorInvoiceTaxes);
		return Observable.just("SUCCESS");
	}

	private IObVendorInvoiceTaxes createTaxEntity(
		DObVendorInvoice vendorInvoice,
		LObCompanyTaxes vendorCompany) {

		IObVendorInvoiceTaxes vendorInvoiceTax = new IObVendorInvoiceTaxes();
		commandUtils.setCreationAndModificationInfoAndDate(vendorInvoiceTax);
		vendorInvoiceTax.setRefInstanceId(vendorInvoice.getId());

		vendorInvoiceTax.setName(vendorCompany.getName());
		vendorInvoiceTax.setCode(vendorCompany.getUserCode());
		BigDecimal taxAmount = vendorCompany
			.getPercentage()
			.multiply(getTotalItemsAmount(vendorInvoice.getUserCode()))
			.divide(BigDecimal.valueOf(100));

		vendorInvoiceTax.setAmount(taxAmount);
		return vendorInvoiceTax;
	}

	private BigDecimal getTotalItemsAmount(String vendorInvoiceCode) {
		return vendorInvoiceItemsGeneralModelRep
			.findByInvoiceCode(vendorInvoiceCode)
			.stream()
			.map(item ->
				item.getQuantityInOrderUnit()
				.multiply(item.getPrice())
				.multiply(item.getConversionFactorToBase())
			)
			.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setVendorInvoiceItemsGeneralModelRep(
					IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
		this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
	}

	public void setTaxesRep(IObVendorInvoiceTaxesRep taxesRep) {
		this.taxesRep = taxesRep;
	}

	public void setlObCompanyTaxesRep(LObCompanyTaxesRep lObCompanyTaxesRep) {
		this.lObCompanyTaxesRep = lObCompanyTaxesRep;
	}

	public void setVendorInvoiceCompanyDataRep(IObVendorInvoiceCompanyDataRep vendorInvoiceCompanyDataRep) {
		this.vendorInvoiceCompanyDataRep = vendorInvoiceCompanyDataRep;
	}
}
