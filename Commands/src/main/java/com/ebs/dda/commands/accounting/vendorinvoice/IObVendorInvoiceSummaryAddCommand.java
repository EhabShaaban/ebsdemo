package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.repositories.accounting.vendorinvoice.*;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

public class IObVendorInvoiceSummaryAddCommand implements ICommand<DObVendorInvoice> {
  private CommandUtils commandUtils;
  private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;
  private IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep;
  private IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;

  private IObVendorInvoiceSummary vendorInvoiceSummary;
  private IUserAccountSecurityService userAccountSecurityService;

  public IObVendorInvoiceSummaryAddCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(DObVendorInvoice vendorInvoice) throws Exception {
    vendorInvoiceSummary = new IObVendorInvoiceSummary();

    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setCreationAndModificationInfoAndDate(vendorInvoiceSummary);
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItemsGeneralModels =
        vendorInvoiceItemsGeneralModelRep.findByInvoiceCode(vendorInvoice.getUserCode());
    List<IObVendorInvoiceTaxesGeneralModel> vendorInvoiceTaxesGeneralModels =
        vendorInvoiceTaxesGeneralModelRep.findByInvoiceCode(vendorInvoice.getUserCode());
    BigDecimal totalAmountWithoutTaxes = vendorInvoiceItemsGeneralModels.stream()
            .map(item -> item.getTotalAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
    BigDecimal totalTaxAmount =
        vendorInvoiceTaxesGeneralModels.stream()
            .map(tax -> tax.getTaxAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
    vendorInvoiceSummary.setRefInstanceId(vendorInvoice.getId());
    vendorInvoiceSummary.setDownPayment(BigDecimal.ZERO);
    vendorInvoiceSummary.setRemaining(totalAmountWithoutTaxes.add(totalTaxAmount));
    vendorInvoiceSummary.setTotalAmount(totalAmountWithoutTaxes.add(totalTaxAmount));
    vendorInvoiceSummaryRep.create(vendorInvoiceSummary);
    return Observable.just("SUCCESS");
  }

  public void setVendorInvoiceItemsGeneralModelRep(IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
    this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
  }

  public void setVendorInvoiceTaxesGeneralModelRep(IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep) {
    this.vendorInvoiceTaxesGeneralModelRep = vendorInvoiceTaxesGeneralModelRep;
  }

  public void setVendorInvoiceSummaryRep(IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep) {
    this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }
}
