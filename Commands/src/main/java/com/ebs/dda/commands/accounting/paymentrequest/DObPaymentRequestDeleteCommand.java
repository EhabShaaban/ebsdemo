package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import io.reactivex.Observable;

public class DObPaymentRequestDeleteCommand implements ICommand {

  private DObPaymentRequestRep paymentRequestRep;

  @Override
  public Observable<String> executeCommand(Object paymentRequestCode) throws Exception {

    paymentRequestRep.delete(readEntityByCode((String) paymentRequestCode));

    return Observable.empty();
  }

  private DObPaymentRequest readEntityByCode(String paymentRequestCode) {
    return paymentRequestRep.findOneByUserCode(paymentRequestCode);
  }

  public void checkIfAllowedAction(DObPaymentRequestGeneralModel entity, String allowedAction)
          throws Exception {
    DObPaymentRequestStateMachine paymentRequestStateMachine = new DObPaymentRequestStateMachine();
    paymentRequestStateMachine.initObjectState(entity);
    paymentRequestStateMachine.isAllowedAction(allowedAction);
  }

  public DObPaymentRequest checkIfInstanceExists(String userCode) throws InstanceNotExistException {
    DObPaymentRequest paymentRequest = paymentRequestRep.findOneByUserCode(userCode);
    if (paymentRequest == null) {
      throw new InstanceNotExistException();
    }
    return paymentRequest;
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }
}
