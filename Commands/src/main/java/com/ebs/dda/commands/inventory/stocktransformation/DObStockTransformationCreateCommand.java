package com.ebs.dda.commands.inventory.stocktransformation;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.inventory.stocktransformation.statemachines.DObStockTransformationStateMachine;
import com.ebs.dda.jpa.inventory.stocktransformation.*;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationReasonRep;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationTypeRep;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import io.reactivex.Observable;

public class DObStockTransformationCreateCommand
        implements ICommand<DObStockTransformationCreateValueObject> {

    private CommandUtils commandUtils;
    private DObStockTransformationStateMachine stateMachine;
    private DocumentObjectCodeGenerator documentObjectCodeGenerator;
    private IUserAccountSecurityService userAccountSecurityService;

    private DObStockTransformationRep dObStockTransformationRep;
    private CObPurchasingUnitRep purchasingUnitRep;
    private TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep;
    private CObStockTransformationTypeRep cObStockTransformationTypeRep;
    private CObStockTransformationReasonRep cObStockTransformationReasonRep;

    public DObStockTransformationCreateCommand(
            DocumentObjectCodeGenerator documentObjectCodeGenerator) throws Exception {
        commandUtils = new CommandUtils();
        stateMachine = new DObStockTransformationStateMachine();
        this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    }

    @Override
    public Observable<String> executeCommand(
            DObStockTransformationCreateValueObject dObStockTransformationCreateValueObject)
            throws Exception {
        String userCode =
                documentObjectCodeGenerator.generateUserCode(DObStockTransformation.class.getSimpleName());
        DObStockTransformation dObStockTransformation = new DObStockTransformation();
        IObStockTransformationDetails iObStockTransformationDetails =
                new IObStockTransformationDetails();

        setStockTransformationData(
                userCode, dObStockTransformation, dObStockTransformationCreateValueObject);

        setStockTransformationDetailsData(
                iObStockTransformationDetails, dObStockTransformationCreateValueObject);

        dObStockTransformation.setHeaderDetail(
                IDObStockTransformation.stockTransformationDetailsAttr, iObStockTransformationDetails);
        dObStockTransformationRep.create(dObStockTransformation);
        return Observable.just(userCode);
    }

    private void setStockTransformationData(
            String userCode,
            DObStockTransformation dObStockTransformation,
            DObStockTransformationCreateValueObject dObStockTransformationCreateValueObject)
            throws Exception {

        setCreationAndModificationData(dObStockTransformation);
        stateMachine.initObjectState(dObStockTransformation);

        dObStockTransformation.setUserCode(userCode);

        setStockTransformationPurchaseUnit(
                dObStockTransformation, dObStockTransformationCreateValueObject.getPurchaseUnitCode());

        setStockTransformationType(
                dObStockTransformation,
                dObStockTransformationCreateValueObject.getTransformationTypeCode());
    }

    private void setStockTransformationDetailsData(
            IObStockTransformationDetails iObStockTransformationDetails,
            DObStockTransformationCreateValueObject dObStockTransformationCreateValueObject) {

        commandUtils.setCreationAndModificationInfoAndDate(iObStockTransformationDetails);

        iObStockTransformationDetails.setNewStockType(
                dObStockTransformationCreateValueObject.getNewStockType());

        iObStockTransformationDetails.setTransformedQty(
                dObStockTransformationCreateValueObject.getTransformedQty());

        setStockTransformationStoreTransaction(
                iObStockTransformationDetails,
                dObStockTransformationCreateValueObject.getStoreTransactionCode());

        setStockTransformationReason(
                iObStockTransformationDetails,
                dObStockTransformationCreateValueObject.getTransformationReasonCode());
    }

    private void setCreationAndModificationData(DObStockTransformation dObStockTransformation) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
        commandUtils.setCreationAndModificationInfoAndDate(dObStockTransformation);
    }

    private void setStockTransformationPurchaseUnit(
            DObStockTransformation dObStockTransformation, String purchaseUnitCode) {
        CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
        dObStockTransformation.setPurchaseUnitId(purchaseUnit.getId());
    }

    private void setStockTransformationStoreTransaction(
            IObStockTransformationDetails iObStockTransformationDetails, String storeTransactionCode) {
        TObStoreTransactionGeneralModel tObStoreTransactionGeneralModel =
                tObStoreTransactionGeneralModelRep.findOneByUserCode(storeTransactionCode);
        iObStockTransformationDetails.setStoreTransactionId(tObStoreTransactionGeneralModel.getId());
    }

    private void setStockTransformationType(
            DObStockTransformation dObStockTransformation, String transformationTypeCode) {
        CObStockTransformationType cObStockTransformationType =
                cObStockTransformationTypeRep.findOneByUserCode(transformationTypeCode);
        dObStockTransformation.setTransformationTypeId(cObStockTransformationType.getId());
    }

    private void setStockTransformationReason(
            IObStockTransformationDetails iObStockTransformationDetails,
            String transformationReasonCode) {
        CObStockTransformationReason cObStockTransformationReason =
                cObStockTransformationReasonRep.findOneByUserCode(transformationReasonCode);
        iObStockTransformationDetails.setTransformationReasonId(cObStockTransformationReason.getId());
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        this.userAccountSecurityService = userAccountSecurityService;
    }

    public void setDocumentObjectCodeGenerator(
            DocumentObjectCodeGenerator documentObjectCodeGenerator) {
        this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    }

    public void setdObStockTransformationRep(DObStockTransformationRep dObStockTransformationRep) {
        this.dObStockTransformationRep = dObStockTransformationRep;
    }

    public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
        this.purchasingUnitRep = purchasingUnitRep;
    }

    public void settObStoreTransactionGeneralModelRep(
            TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep) {
        this.tObStoreTransactionGeneralModelRep = tObStoreTransactionGeneralModelRep;
    }

    public void setcObStockTransformationTypeRep(
            CObStockTransformationTypeRep cObStockTransformationTypeRep) {
        this.cObStockTransformationTypeRep = cObStockTransformationTypeRep;
    }

    public void setcObStockTransformationReasonRep(
            CObStockTransformationReasonRep cObStockTransformationReasonRep) {
        this.cObStockTransformationReasonRep = cObStockTransformationReasonRep;
    }
}
