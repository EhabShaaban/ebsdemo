package com.ebs.dda.commands.accounting.landedcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.infrastructure.tm.TransactionCallback;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import io.reactivex.Observable;

public class DObLandedCostConvertToEstimateCommand implements ICommand<String> {

    private DObLandedCostRep landedCostRep;
    private CommandUtils commandUtils;

    public DObLandedCostConvertToEstimateCommand() {
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(String landedCostCode)
            throws Exception {

        DObLandedCost landedCost = landedCostRep.findOneByUserCode(landedCostCode);
        updateLandedCost(landedCost);
        return Observable.just(landedCost.getUserCode());
    }


    private void updateLandedCost(DObLandedCost landedCost)
            throws Exception {
        this.commandUtils.setModificationInfoAndModificationDate(landedCost);
        setLandedCostConvertToEstimateState(createLandedCostStateMachine(landedCost));
        landedCostRep.updateAndClear(landedCost);
    }

    private void setLandedCostConvertToEstimateState(AbstractStateMachine landedCostStateMachine) throws Exception {
        if (landedCostStateMachine.canFireEvent(IDObLandedCostActionNames.CONVERT_TO_ESTIMATE)) {
            landedCostStateMachine.fireEvent(IDObLandedCostActionNames.CONVERT_TO_ESTIMATE);
            landedCostStateMachine.save();
        }
    }

    private DObLandedCostStateMachine createLandedCostStateMachine(DObLandedCost landedCost) throws Exception {
        DObLandedCostStateMachine landedCostStateMachine = new DObLandedCostStateMachine();
        landedCostStateMachine.initObjectState(landedCost);
        return landedCostStateMachine;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }


    public void setLandedCostRep(DObLandedCostRep landedCostRep) {
        this.landedCostRep = landedCostRep;
    }
}
