package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import io.reactivex.Observable;

public class DObJournalEntryCreatePaymentRequestBankGeneralCommand
    extends DObJournalEntryCreatePaymentRequestBankGeneralAndVendorDownPaymentCommand {

  public DObJournalEntryCreatePaymentRequestBankGeneralCommand() {
  }

  @Override
  public Observable<String> executeCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    return executeBankGeneralAndVendorDownPaymentCommand(paymentRequestValueObject);
  }

}
