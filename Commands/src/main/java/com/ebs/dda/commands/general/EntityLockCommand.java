package com.ebs.dda.commands.general;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyLockedException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.locking.AlreadyLockedException;
import com.ebs.dac.foundation.exceptions.operational.locking.AlreadyLockedSectionException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityLockCommand<T extends BusinessObject> implements EntityLockCommandMBean {

  private static final Logger logger = LoggerFactory.getLogger(EntityLockCommand.class);
  private IConcurrentDataAccessManager concurrentDataAccessManager;
  private IUserAccountSecurityService userAccountSecurityService;
  private CodedBusinessObjectRep repository;

  public IConcurrentDataAccessManager getConcurrentDataAccessManager() {
    return concurrentDataAccessManager;
  }

  public void setConcurrentDataAccessManager(
      IConcurrentDataAccessManager concurrentDataAccessManager) {
    this.concurrentDataAccessManager = concurrentDataAccessManager;
  }

  public IUserAccountSecurityService getUserAccountSecurityService() {
    return userAccountSecurityService;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setRepository(CodedBusinessObjectRep repository) {
    this.repository = repository;
  }

  public LockDetails lockAllSections(String resourceCode) throws Exception {
    LockDetails lock = null;
    try {
      lock = lockSection(resourceCode, "*");
      return lock;
    } catch (AlreadyLockedSectionException e) {
      throw new AlreadyLockedException(lock);
    }
  }

  @Override
  public void unlockAllSections(String resourceCode) throws Exception {
    unlockSection(resourceCode, "*");
  }

  // for refresh client side action
  public void unlockAllResourceLockedSectionsByUser(String resourceCode,
      List<String> lockedSections) throws Exception {
    if (!lockedSections.isEmpty()) {
      IBDKUser user = userAccountSecurityService.getLoggedInUser();
      List<LockDetails> allUserLocks =
          concurrentDataAccessManager.listLocksAcquiredForResourceByUser(resourceCode, user);
      for (LockDetails lockDetails : allUserLocks) {
        if (lockedSections.contains(lockDetails.getResourceName())) {
          concurrentDataAccessManager.unlock(lockDetails.getTokenId());
        }
      }
    }
  }

  public void unlockSection(String resourceCode, String resourceName) throws Exception {
    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    checkIfResourceIsLockedByLoggedInUser(resourceCode, resourceName);
    getConcurrentDataAccessManager().unlock(resourceName, resourceCode, user);
  }

  public LockDetails lockSection(String resourceCode, String resourceName) throws Exception {
    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    LockDetails lock = null;
    try {
      lock = getConcurrentDataAccessManager().lock(resourceName, resourceCode, user);
      checkIfEntityExists(resourceCode);
    } catch (ResourceAlreadyLockedException e) {
      throw new AlreadyLockedSectionException(lock);
    } catch (InstanceNotExistException e) {
      unlockSection(resourceCode, resourceName);
      throw e;
    }
    return lock;
  }

  public LockDetails lockAllSectionsOfNonExistingEntity(String resourceCode) throws Exception {
    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    LockDetails lock = null;
    try {
      lock = getConcurrentDataAccessManager().lock("*", resourceCode, user);
    } catch (ResourceAlreadyLockedException e) {
      throw new AlreadyLockedSectionException(lock);
    }
    return lock;
  }

  @Override
  public LockDetails getLockForCurrentUserOfResource(IBDKUser user, String resourceName,
      String resourceCode) {
    List<LockDetails> lockDetailsList = getConcurrentDataAccessManager().listAllLocks();
    for (LockDetails lockDetails : lockDetailsList) {

      boolean resourceNameExist = lockDetails.getResourceName().equals(resourceName);
      boolean resourceCodeExist = lockDetails.getResourceCode().equals(resourceCode);
      boolean userExist = lockDetails.getIdentity().equals(user);
      if (resourceCodeExist && userExist && resourceNameExist) {
        return lockDetails;
      }
    }
    return null;
  }

  public void checkIfResourceIsLockedByLoggedInUser(String resourceCode, String resourceName)
      throws Exception {
    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    getConcurrentDataAccessManager().checkIfResourceIsLockedByUser(resourceName, resourceCode,
        user);
  }

  public void checkIfEntityExists(String entityUserCode) throws InstanceNotExistException {
    boolean entityExists = repository.existsByUserCode(entityUserCode);
    if (!entityExists) {
      throw new InstanceNotExistException();
    }
  }

  @Deprecated
  public boolean isAllSectionsLocked(String userCode) {
    List<String> lockedSectionNames =
        getConcurrentDataAccessManager().listResourceNameLocksAcquiredForResource(userCode);
    return lockedSectionNames.stream().anyMatch(ele -> ele.equals("*"));
  }
}
