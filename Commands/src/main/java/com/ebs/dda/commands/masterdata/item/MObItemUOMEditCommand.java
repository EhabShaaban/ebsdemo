package com.ebs.dda.commands.masterdata.item;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoM;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemUOMValueObject;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.measure.UnitOfMeasureValueObject;
import com.ebs.dda.masterdata.item.IMObItemSectionNames;
import com.ebs.dda.repositories.masterdata.item.IObItemAlternativeUOMRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class MObItemUOMEditCommand implements ICommand<MObItemUOMValueObject> {

  private final CommandUtils commandUtils;
  private EntityLockCommand<MObItem> entityLockCommand;
  private MObItemRep itemRep;
  private CObMeasureRep measureRep;
  private IObItemAlternativeUOMRep itemAlternativeUOMRep;

  public MObItemUOMEditCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(MObItemUOMValueObject valueObject) throws Exception {
    String itemCode = valueObject.getItemCode();
    MObItem item = itemRep.findOneByUserCode(itemCode);
    setItemUnitOfMeasuresData(item, valueObject.getItemUnitOfMeasures());
    commandUtils.setModificationInfoAndModificationDate(item);
    itemRep.updateAndClear(item);
    return Observable.just("SUCCESS");
  }

  private void setItemUnitOfMeasuresData(MObItem item, List<UnitOfMeasureValueObject> valueObjects)
      throws Exception {
    if (valueObjects.isEmpty()) {
      item.setAllMultipleDetails(IMObItem.alternativeUOMAttr, new ArrayList<>());
    }
    for (UnitOfMeasureValueObject valueObject : valueObjects) {
      IObAlternativeUoM uomFromDB = getUomFromDB(valueObject, item);
      if (uomFromDB != null) {
        item.addMultipleDetail(IMObItem.alternativeUOMAttr, uomFromDB);
      } else {
        IObAlternativeUoM alternativeUoM = new IObAlternativeUoM();
        prepareUnitOfMeasureData(valueObject, alternativeUoM);
        commandUtils.setCreationAndModificationInfoAndDate(alternativeUoM);
        item.addMultipleDetail(IMObItem.alternativeUOMAttr, alternativeUoM);
      }
    }
  }

  private IObAlternativeUoM getUomFromDB(UnitOfMeasureValueObject valueObject, MObItem item) {
    CObMeasure measure = measureRep.findOneByUserCode(valueObject.getAlternativeUnitCode());
    return itemAlternativeUOMRep
        .findOneByRefInstanceIdAndAlternativeUnitOfMeasureIdAndConversionFactor(
            item.getId(), measure.getId(), valueObject.getConversionFactor());
  }

  private void prepareUnitOfMeasureData(
      UnitOfMeasureValueObject valueObject, IObAlternativeUoM alternativeUoM) {
    String alternativeUnitCode = valueObject.getAlternativeUnitCode();
    Float conversionFactor = valueObject.getConversionFactor();
    String oldItemNumber = valueObject.getOldItemNumber();

    CObMeasure measure = measureRep.findOneByUserCode(alternativeUnitCode);
    alternativeUoM.setAlternativeUnitOfMeasureId(measure.getId());
    alternativeUoM.setConversionFactor(conversionFactor);
    alternativeUoM.setOldItemNumber(oldItemNumber);
  }

  public void lock(String resourceCode) throws Exception {
    entityLockCommand.lockSection(resourceCode, IMObItemSectionNames.UOM_SECTION);
  }

  public void unlockSection(String itemCode) throws Exception {
    entityLockCommand.unlockSection(itemCode, IMObItemSectionNames.UOM_SECTION);
  }

  public void setEntityLockCommand(EntityLockCommand<MObItem> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setMObItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setCObMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setItemAlternativeUOMRep(IObItemAlternativeUOMRep itemAlternativeUOMRep) {
    this.itemAlternativeUOMRep = itemAlternativeUOMRep;
  }
}
