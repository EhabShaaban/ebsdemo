package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.IObPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderItem;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;

public class IObPurchaseOrderItemsSaveCommand implements ICommand<IObPurchaseOrderItemValueObject> {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private MObItemRep itemRep;
  private CObMeasureRep measureRep;

  public IObPurchaseOrderItemsSaveCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObPurchaseOrderItemValueObject valueObject)
      throws Exception {

    DObPurchaseOrder purchaseOrder =
        purchaseOrderRep.findOneByUserCode(valueObject.getPurchaseOrderCode());

    IObOrderItem iobOrderItem = preparePurchaseOrderItem(valueObject);

    updateObjectsInfo(purchaseOrder, iobOrderItem);

    purchaseOrder.addLineDetail(IDObPurchaseOrder.purchaseOrderItemsAttr, iobOrderItem);
    purchaseOrderRep.update(purchaseOrder);

    return Observable.just("Success");
  }

  private IObOrderItem preparePurchaseOrderItem(IObPurchaseOrderItemValueObject valueObject) {
    IObOrderItem iobOrderItem = new IObOrderItem();

    iobOrderItem.setItemId(getItemId(valueObject));
    iobOrderItem.setOrderUnitId(getOrderUnitId(valueObject));
    iobOrderItem.setQuantity(valueObject.getQuantity());
    iobOrderItem.setPrice(valueObject.getPrice());

    return iobOrderItem;
  }

  private void updateObjectsInfo(DObPurchaseOrder purchaseOrder, IObOrderItem iobOrderItem) {
    commandUtils.setCreationAndModificationInfoAndDate(iobOrderItem);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }

  private Long getOrderUnitId(IObPurchaseOrderItemValueObject valueObject) {
    String uomCode = valueObject.getUnitOfMeasureCode();
    return measureRep.findOneByUserCode(uomCode).getId();
  }

  private Long getItemId(IObPurchaseOrderItemValueObject valueObject) {
    String itemCode = valueObject.getItemCode();
    return itemRep.findOneByUserCode(itemCode).getId();
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
