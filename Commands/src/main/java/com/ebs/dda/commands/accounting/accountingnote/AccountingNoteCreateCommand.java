package com.ebs.dda.commands.accounting.accountingnote;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.accountingnote.statemachines.DObAccountingNoteStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.accountingnote.factory.DObAccountingNoteFactory;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;
import com.ebs.dda.jpa.accounting.accountingnotes.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class AccountingNoteCreateCommand implements ICommand<AccountingNoteCreateValueObject> {

  private final CommandUtils commandUtils;
  private DObAccountingNoteRep accountingNoteRep;
  private CObCompanyRep companyRep;
  private CObPurchasingUnitRep businessUnitRep;
  private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  private MObVendorRep vendorRep;
  private MObCustomerRep customerRep;
  private CObCurrencyRep currencyRep;

  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObAccountingNoteFactory accountingNoteFactory;
  private DObAccountingNoteStateMachine stateMachine;

  public AccountingNoteCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
    commandUtils = new CommandUtils();
    accountingNoteFactory = new DObAccountingNoteFactory();
    stateMachine = new DObAccountingNoteStateMachine();
  }

  @Override
  public Observable<String> executeCommand(AccountingNoteCreateValueObject valueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObAccountingNote.class.getSimpleName());

    DObAccountingNote accountingNote =
        accountingNoteFactory.createAccountingNote(
            valueObject.getNoteType().name(), valueObject.getType());

    commandUtils.setCreationAndModificationInfoAndDate(accountingNote);

    setGeneralData(valueObject, userCode, accountingNote);
    setCompanyData(valueObject, accountingNote);
    setDetailsData(valueObject, accountingNote);
    accountingNoteRep.create(accountingNote);
    return Observable.just(userCode);
  }

  private void setGeneralData(
      AccountingNoteCreateValueObject valueObject,
      String userCode,
      DObAccountingNote accountingNote)
      throws Exception {
    accountingNote.setUserCode(userCode);
    accountingNote.setDocumentOwnerId(valueObject.getDocumentOwnerId());
    accountingNote.setType(valueObject.getNoteType().name());
    stateMachine.initObjectState(accountingNote);
  }

  private void setCompanyData(
      AccountingNoteCreateValueObject valueObject, DObAccountingNote accountingNote) {
    IObAccountingDocumentCompanyData companyData = new IObAccountingNoteCompanyData();
    commandUtils.setCreationAndModificationInfoAndDate(companyData);
    companyData.setCompanyId(getCompanyIdBasedOnAccountingNoteType(valueObject));
    companyData.setPurchaseUnitId(getBusinessUnitIdBasedOnAccountingNoteType(valueObject));
    accountingNote.setHeaderDetail(IDObAccountingNote.companyAttr, companyData);
  }

  private void setDetailsData(
      AccountingNoteCreateValueObject valueObject, DObAccountingNote accountingNote) {
    IObAccountingNoteDetails detailsData = new IObAccountingNoteDetails();
    commandUtils.setCreationAndModificationInfoAndDate(detailsData);
    if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      detailsData.setRefDocumentId(getSalesReturnId(valueObject.getReferenceDocumentCode()));
    }
    detailsData.setBusinessPartnerId(getBusinessPartnerIdBasedOnAccountingNoteType(valueObject));
    detailsData.setCurrencyId(getCurrencyIdBasedOnAccountingNoteType(valueObject));
    detailsData.setAmount(getAmountBasedOnAccountingNoteType(valueObject).doubleValue());
    accountingNote.setHeaderDetail(IDObAccountingNote.detailsAttr, detailsData);
  }

  private Long getSalesReturnId(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    return salesReturn.getId();
  }

  private Long getBusinessPartnerIdBasedOnAccountingNoteType(
      AccountingNoteCreateValueObject valueObject) {
    Long businessPartnerId = null;
    String businessPartnerCode = valueObject.getBusinessPartnerCode();
    if (isDebitNoteBasedOnCommission(valueObject)) {
      businessPartnerId = getVendorBusinessPartnerIdByCode(businessPartnerCode);
    } else if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      businessPartnerId = getBusinessPartnerIdBySalesReturn(valueObject.getReferenceDocumentCode());
    }
    return businessPartnerId;
  }

  private Long getBusinessPartnerIdBySalesReturn(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    String businessPartnerCode = salesReturn.getCustomerCode();
    Long businessPartnerId = getCustomerBusinessPartnerIdByCode(businessPartnerCode);
    return businessPartnerId;
  }

  private Long getVendorBusinessPartnerIdByCode(String businessPartnerCode) {
    return vendorRep.findOneByUserCode(businessPartnerCode).getId();
  }

  private Long getCustomerBusinessPartnerIdByCode(String businessPartnerCode) {
    return customerRep.findOneByUserCode(businessPartnerCode).getId();
  }

  private Long getPurchaseUnitId(String businessUnitCode) {
    CObPurchasingUnit businessUnit = businessUnitRep.findOneByUserCode(businessUnitCode);
    return businessUnit.getId();
  }

  private Long getBusinessUnitIdBasedOnAccountingNoteType(
      AccountingNoteCreateValueObject valueObject) {

    Long businessUnitId = null;

    if (isDebitNoteBasedOnCommission(valueObject)) {
      businessUnitId = getPurchaseUnitId(valueObject.getBusinessUnitCode());
    } else if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      businessUnitId = getBusinessUnitIdBySalesReturn(valueObject.getReferenceDocumentCode());
    }

    return businessUnitId;
  }

  private Long getBusinessUnitIdBySalesReturn(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    String businessUnitCode = salesReturn.getPurchaseUnitCode();
    Long businessUnitId = getPurchaseUnitId(businessUnitCode);
    return businessUnitId;
  }

  private Long getCurrencyIdBasedOnAccountingNoteType(AccountingNoteCreateValueObject valueObject) {

    Long currencyId = null;

    if (isDebitNoteBasedOnCommission(valueObject)) {
      currencyId = getCurrencyId(valueObject.getCurrencyIso());
    } else if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      currencyId = getCurrencyIdBySalesReturn(valueObject.getReferenceDocumentCode());
    }

    return currencyId;
  }

  private Long getCurrencyIdBySalesReturn(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    String currencyIso = salesReturn.getCurrencyIso();
    Long currencyId = getCurrencyId(currencyIso);
    return currencyId;
  }

  private Long getCurrencyId(String currencyIso) {
    return currencyRep.findOneByIso(currencyIso).getId();
  }

  private Long getCompanyIdBasedOnAccountingNoteType(AccountingNoteCreateValueObject valueObject) {

    Long companyId = null;

    if (isDebitNoteBasedOnCommission(valueObject)) {
      companyId = getCompanyIdByCode(valueObject.getCompanyCode());
    } else if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      companyId = getCompanyIdBySalesReturn(valueObject.getReferenceDocumentCode());
    }

    return companyId;
  }

  private Long getCompanyIdByCode(String companyCode) {
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private Long getCompanyIdBySalesReturn(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    String companyCode = salesReturn.getCompanyCode();
    Long companyId = getCompanyIdByCode(companyCode);
    return companyId;
  }

  private BigDecimal getAmountBasedOnAccountingNoteType(
      AccountingNoteCreateValueObject valueObject) {

    BigDecimal amount = BigDecimal.ZERO;

    if (isDebitNoteBasedOnCommission(valueObject)) {
      amount = BigDecimal.valueOf(valueObject.getAmount());
    } else if (isCreditNoteBasedOnSalesReturn(valueObject)) {
      amount = getAmountBySalesReturn(valueObject.getReferenceDocumentCode());
    }

    return amount;
  }

  private BigDecimal getAmountBySalesReturn(String salesReturnCode) {
    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);
    BigDecimal amount = salesReturn.getAmount();
    return amount;
  }

  private boolean isCreditNoteBasedOnSalesReturn(AccountingNoteCreateValueObject valueObject) {
    return valueObject
        .getType()
        .equals(CreditNoteTypeEnum.CREDIT_NOTE_BASED_ON_SALES_RETURN.name());
  }

  private boolean isDebitNoteBasedOnCommission(AccountingNoteCreateValueObject valueObject) {
    return valueObject.getType().equals(DebitNoteTypeEnum.DEBIT_NOTE_BASED_ON_COMMISSION.name());
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setBusinessUnitRep(CObPurchasingUnitRep businessUnitRep) {
    this.businessUnitRep = businessUnitRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setAccountingNoteRep(DObAccountingNoteRep accountingNoteRep) {
    this.accountingNoteRep = accountingNoteRep;
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }
}
