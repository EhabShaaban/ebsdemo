package com.ebs.dda.commands.order.salesorder;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderDeletionValueObject;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;

public class DObSalesOrderDeleteCommand implements ICommand<DObSalesOrderDeletionValueObject> {

  private DObSalesOrderRep salesOrderRep;

  @Override
  public Observable<String> executeCommand(DObSalesOrderDeletionValueObject valueObject)
      throws Exception {

    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    salesOrderRep.delete(salesOrder);

    return Observable.just("SUCCESS");
  }

  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }
}
