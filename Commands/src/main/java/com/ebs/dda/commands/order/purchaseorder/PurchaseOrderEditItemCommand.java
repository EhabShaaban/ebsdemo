package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;

public class PurchaseOrderEditItemCommand {

  private EntityLockCommand<DObOrderDocument> entityLockCommand;

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void unlockSection(String purchaseOrderCode) throws Exception {
    entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
  }

}
