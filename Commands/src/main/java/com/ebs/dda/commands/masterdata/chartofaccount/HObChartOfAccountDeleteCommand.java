package com.ebs.dda.commands.masterdata.chartofaccount;

import javax.transaction.Transactional;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountDeleteValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;

import io.reactivex.Observable;

public class HObChartOfAccountDeleteCommand
				implements ICommand<HObChartOfAccountDeleteValueObject> {
	private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
	private LObGlobalAccountConfigRep globalAccountConfigRep;
	private HObChartOfAccountRep glAccountRep;

	@Transactional
	@Override
	public Observable<String> executeCommand(HObChartOfAccountDeleteValueObject valueObject)
					throws Exception {
		String glAccountCode = valueObject.getGlAccountCode();
		HObChartOfAccountsGeneralModel glAccount = chartOfAccountsGeneralModelRep
						.findOneByUserCode(glAccountCode);
		String mappedAccountCode = glAccount.getMappedAccount();
		globalAccountConfigRep.deleteByUserCode(mappedAccountCode);
		glAccountRep.deleteByUserCode(glAccountCode);
		return Observable.just(glAccountCode);
	}

	public void setChartOfAccountsGeneralModelRep(
					HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
		this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
	}

	public void setGlobalAccountConfigRep(LObGlobalAccountConfigRep globalAccountConfigRep) {
		this.globalAccountConfigRep = globalAccountConfigRep;
	}

	public void setGlAccountRep(HObChartOfAccountRep glAccountRep) {
		this.glAccountRep = glAccountRep;
	}
}
