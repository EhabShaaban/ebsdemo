package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableReferenceDocumentDeletionValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentNotesReceivableRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentSalesOrderRep;
import io.reactivex.Observable;

public class IObNotesReceivableReferenceDocumentDeleteCommand
    implements ICommand<IObNotesReceivableReferenceDocumentDeletionValueObject> {

  private IObNotesReceivableReferenceDocumentSalesInvoiceRep
      notesReceivableReferenceDocumentSalesInvoiceRep;
  private IObNotesReceivableReferenceDocumentNotesReceivableRep
      notesReceivableReferenceDocumentNotesReceivableRep;
  private IObNotesReceivableReferenceDocumentSalesOrderRep
      notesReceivableReferenceDocumentSalesOrderRep;
  private DObMonetaryNotesRep monetaryNotesRep;
  private CommandUtils commandUtils;

  public IObNotesReceivableReferenceDocumentDeleteCommand() { commandUtils = new CommandUtils(); }

  @Override
  public Observable<String> executeCommand(
      IObNotesReceivableReferenceDocumentDeletionValueObject valueObject) throws Exception {

    String refDocumentType = valueObject.getReferenceDocumentType();
    if (refDocumentType.equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name())) {
      notesReceivableReferenceDocumentNotesReceivableRep.deleteById(
          valueObject.getReferenceDocumentId());
    } else if (refDocumentType.equals(
        NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name())) {
      notesReceivableReferenceDocumentSalesOrderRep.deleteById(
          valueObject.getReferenceDocumentId());
    } else if (refDocumentType.equals(
        NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name())) {
      notesReceivableReferenceDocumentSalesInvoiceRep.deleteById(
          valueObject.getReferenceDocumentId());
    }
    DObMonetaryNotes monetaryNotes = monetaryNotesRep.findOneByUserCode(valueObject.getNotesReceivableCode());
    commandUtils.setModificationInfoAndModificationDate(monetaryNotes);
    monetaryNotesRep.update(monetaryNotes);
    return Observable.just("SUCCESS");
  }

  public void setNotesReceivableReferenceDocumentSalesInvoiceRep(
      IObNotesReceivableReferenceDocumentSalesInvoiceRep
          notesReceivableReferenceDocumentSalesInvoiceRep) {
    this.notesReceivableReferenceDocumentSalesInvoiceRep =
        notesReceivableReferenceDocumentSalesInvoiceRep;
  }

  public void setNotesReceivableReferenceDocumentNotesReceivableRep(
      IObNotesReceivableReferenceDocumentNotesReceivableRep
          notesReceivableReferenceDocumentNotesReceivableRep) {
    this.notesReceivableReferenceDocumentNotesReceivableRep =
        notesReceivableReferenceDocumentNotesReceivableRep;
  }

  public void setNotesReceivableReferenceDocumentSalesOrderRep(
      IObNotesReceivableReferenceDocumentSalesOrderRep
          notesReceivableReferenceDocumentSalesOrderRep) {
    this.notesReceivableReferenceDocumentSalesOrderRep =
        notesReceivableReferenceDocumentSalesOrderRep;
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
