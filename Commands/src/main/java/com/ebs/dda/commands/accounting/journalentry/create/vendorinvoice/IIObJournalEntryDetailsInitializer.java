package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dda.jpa.accounting.DObAccountingDocumentJournalDetailPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public interface IIObJournalEntryDetailsInitializer<JournalDetail extends DObAccountingDocumentJournalDetailPreparationGeneralModel> {

    Object addCredit(DObJournalEntry journalEntry, JournalDetail journalDetailPreparationGeneralModel);
    Object addDebit(DObJournalEntry journalEntry, DObVendorInvoice vendorInvoice, JournalDetail journalDetailPreparationGeneralModel);
}
