package com.ebs.dda.commands.order.salesorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemDeletionValueObject;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsRep;
import io.reactivex.Observable;

public class IObSalesOrderItemsDeleteCommand
    implements ICommand<IObSalesOrderItemDeletionValueObject> {

  private IObSalesOrderItemsRep salesOrderItemRep;
  private DObSalesOrderRep dObSalesOrderRep;
  private CommandUtils commandUtils;

  public IObSalesOrderItemsDeleteCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(IObSalesOrderItemDeletionValueObject valueObject)
      throws Exception {

    salesOrderItemRep.deleteById(valueObject.getSalesOrderItemId());
    DObSalesOrder salesOrder = this.dObSalesOrderRep.findOneByUserCode(valueObject.getSalesOrderCode());
    this.commandUtils.setModificationInfoAndModificationDate(salesOrder);
    this.dObSalesOrderRep.update(salesOrder);
    return Observable.just("SUCCESS");
  }

  public void setSalesOrderItemRep(IObSalesOrderItemsRep salesOrderItemRep) {
    this.salesOrderItemRep = salesOrderItemRep;
  }

  public void setdObSalesOrderRep(DObSalesOrderRep dObSalesOrderRep) {
    this.dObSalesOrderRep = dObSalesOrderRep;
  }
  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
