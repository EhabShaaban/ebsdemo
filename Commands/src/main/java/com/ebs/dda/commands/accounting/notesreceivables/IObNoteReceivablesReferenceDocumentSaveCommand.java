package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.notesreceivables.factory.IObNotesReceivablesReferenceDocumentFactory;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesReferenceDocuments;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesReferenceDocumentsValueObject;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import io.reactivex.Observable;

public class IObNoteReceivablesReferenceDocumentSaveCommand
    implements ICommand<IObMonetaryNotesReferenceDocumentsValueObject> {

  private CommandUtils commandUtils;
  private IObNotesReceivablesReferenceDocumentFactory referenceDocumentFactory;
  private DObMonetaryNotesRep monetaryNotesRep;

  public IObNoteReceivablesReferenceDocumentSaveCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObMonetaryNotesReferenceDocumentsValueObject valueObject) throws Exception {

    String notesReceivableCode = valueObject.getNotesReceivableCode();
    DObMonetaryNotes monetaryNote = monetaryNotesRep.findOneByUserCode(notesReceivableCode);
    IObMonetaryNotesReferenceDocuments notesReceivableReferenceDocument =
        prepareNotesReceivableReferenceDocument(valueObject);
    updateModificationInfoAndDate(monetaryNote, notesReceivableReferenceDocument);

    monetaryNote.addLineDetail(
        IDObMonetaryNotes.referenceDocumentsAttr, notesReceivableReferenceDocument);

    monetaryNotesRep.update(monetaryNote);

    return Observable.just(notesReceivableCode);
  }

  private IObMonetaryNotesReferenceDocuments prepareNotesReceivableReferenceDocument(
      IObMonetaryNotesReferenceDocumentsValueObject valueObject) throws Exception {
    IObMonetaryNotesReferenceDocuments referenceDocument =
        referenceDocumentFactory.createNotesReceivableReferenceDocument(
            valueObject.getDocumentType());
    referenceDocument.setAmountToCollect(valueObject.getAmountToCollect());
    referenceDocumentFactory.setReferenceDocumentId(valueObject, referenceDocument);
    return referenceDocument;
  }

  private void updateModificationInfoAndDate(
      DObMonetaryNotes monetaryNotes, IObMonetaryNotesReferenceDocuments referenceDocuments) {
    commandUtils.setCreationAndModificationInfoAndDate(referenceDocuments);
    commandUtils.setModificationInfoAndModificationDate(monetaryNotes);
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setReferenceDocumentFactory(IObNotesReceivablesReferenceDocumentFactory referenceDocumentFactory) {
    this.referenceDocumentFactory = referenceDocumentFactory;
  }
}
