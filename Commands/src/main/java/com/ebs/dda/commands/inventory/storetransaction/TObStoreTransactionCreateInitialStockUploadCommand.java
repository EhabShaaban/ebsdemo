package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.*;
import com.ebs.dda.repositories.inventory.storetransaction.TObInitialStockUploadStoreTransactionDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObInitialStockUploadStoreTransactionRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class TObStoreTransactionCreateInitialStockUploadCommand
    implements ICommand<TObStoreTransactionCreateInitialStockUploadValueObject> {

  private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private final CommandUtils commandUtils;
  private TObInitialStockUploadStoreTransactionDataGeneralModelRep
      initialStockUploadStoreTransactionDataGeneralModelRep;
  private List<TObInitialStockUploadStoreTransaction> initialStockUploadStoreTransactionList;
  private TObInitialStockUploadStoreTransactionRep initialStockUploadStoreTransactionRep;

  public TObStoreTransactionCreateInitialStockUploadCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(
      TObStoreTransactionCreateInitialStockUploadValueObject initialStockUploadValueObject)
      throws Exception {

    String initialStockUploadCode = initialStockUploadValueObject.getInitialStockUploadCode();

    initInitialStockUploadStoreTransactionEntity(
        initialStockUploadCode, initialStockUploadValueObject.getInitialStockUploadId());

    return Observable.just(initialStockUploadCode);
  }

  private void initInitialStockUploadStoreTransactionEntity(
      String initialStockUploadCode, Long initialStockUploadId) {
    List<TObInitialStockUploadStoreTransactionDataGeneralModel> initialStockUploadItemsData =
        initialStockUploadStoreTransactionDataGeneralModelRep.findByUserCode(
            initialStockUploadCode);
    initialStockUploadStoreTransactionList = new ArrayList<>();
    for (TObInitialStockUploadStoreTransactionDataGeneralModel initialStockUploadItemDataRow :
        initialStockUploadItemsData) {
      createInitialStockUploadStoreTransactions(
          initialStockUploadItemDataRow, initialStockUploadId);
    }
    initialStockUploadStoreTransactionRep.saveAll(initialStockUploadStoreTransactionList);
  }

  private void createInitialStockUploadStoreTransactions(
      TObInitialStockUploadStoreTransactionDataGeneralModel initialStockUploadItemDataRow,
      Long initialStockUploadId) {
    TObInitialStockUploadStoreTransaction initialStockUploadStoreTransaction =
        new TObInitialStockUploadStoreTransaction();
    initialStockUploadStoreTransaction.setOriginalAddingDate(
        initialStockUploadItemDataRow.getOriginalAddingDate());
    initialStockUploadStoreTransaction.setDocumentReferenceId(initialStockUploadId);
    initialStockUploadStoreTransaction.setItemId(initialStockUploadItemDataRow.getItemId());
    initialStockUploadStoreTransaction.setUnitOfMeasureId(
        initialStockUploadItemDataRow.getUnitOfMeasureId());

    StockTypeEnum stockTypeEnum = getStockTypeEnum(initialStockUploadItemDataRow.getStockType());
    initialStockUploadStoreTransaction.setStockType(stockTypeEnum);

    initialStockUploadStoreTransaction.setActualCost(initialStockUploadItemDataRow.getActualCost());
    initialStockUploadStoreTransaction.setEstimateCost(
        initialStockUploadItemDataRow.getEstimateCost());
    initialStockUploadStoreTransaction.setQuantity(
        initialStockUploadItemDataRow.getQuantity());
    initialStockUploadStoreTransaction.setRemainingQuantity(initialStockUploadItemDataRow.getRemainingQuantity());
    setTransactionOperation(initialStockUploadStoreTransaction);
    initialStockUploadStoreTransaction.setCompanyId(initialStockUploadItemDataRow.getCompanyId());
    initialStockUploadStoreTransaction.setPlantId(initialStockUploadItemDataRow.getPlantId());
    initialStockUploadStoreTransaction.setStorehouseId(
        initialStockUploadItemDataRow.getStorehouseId());
    initialStockUploadStoreTransaction.setPurchaseUnitId(
        initialStockUploadItemDataRow.getPurchaseunitId());
    setInitialStockUploadStoreTransactionCreationData(initialStockUploadStoreTransaction);
    setStoreTransactionUserCode(initialStockUploadStoreTransaction);
    initialStockUploadStoreTransactionList.add(initialStockUploadStoreTransaction);
  }

  private StockTypeEnum getStockTypeEnum(String stockType) {
    StockTypeEnum.StockType stockTypeEnumObject = StockTypeEnum.StockType.valueOf(stockType);
    StockTypeEnum stockTypeEnum = new StockTypeEnum();
    stockTypeEnum.setId(stockTypeEnumObject);
    return stockTypeEnum;
  }

  private void setTransactionOperation(
      TObInitialStockUploadStoreTransaction initialStockUploadStoreTransaction) {
    TransactionTypeEnum.TransactionType transactionOperationType =
        TransactionTypeEnum.TransactionType.ADD;
    TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    transactionOperationEnum.setId(transactionOperationType);
    initialStockUploadStoreTransaction.setTransactionOperation(transactionOperationEnum);
  }

  private void setInitialStockUploadStoreTransactionCreationData(
      TObInitialStockUploadStoreTransaction initialStockUploadStoreTransaction) {
    commandUtils.setCreationAndModificationInfoAndDate(initialStockUploadStoreTransaction);
  }

  private void setStoreTransactionUserCode(
      TObInitialStockUploadStoreTransaction initialStockUploadStoreTransaction) {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(TObStoreTransaction.class.getSimpleName());
    initialStockUploadStoreTransaction.setUserCode(userCode);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setInitialStockUploadStoreTransactionDataGeneralModelRep(
      TObInitialStockUploadStoreTransactionDataGeneralModelRep
          initialStockUploadStoreTransactionDataGeneralModelRep) {
    this.initialStockUploadStoreTransactionDataGeneralModelRep =
        initialStockUploadStoreTransactionDataGeneralModelRep;
  }

  public void setInitialStockUploadStoreTransactionRep(
      TObInitialStockUploadStoreTransactionRep initialStockUploadStoreTransactionRep) {
    this.initialStockUploadStoreTransactionRep = initialStockUploadStoreTransactionRep;
  }
}
