package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.jpa.order.IDObOrderDocument;
import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocuments;
import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.LObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderDocumentRequiredDocumentsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderDocumentRequiredDocumentsValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObPurchaseOrderDocumentValueObject;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class IObOrderDocumentRequiredDocumentsUpdateCommand
    implements ICommand<IObOrderDocumentRequiredDocumentsValueObject> {

  private final CommandUtils commandUtils;
  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private DObOrderDocumentRep orderDocumentRep;
  private IObOrderDocumentRequiredDocumentsGeneralModelRep
      orderDocumentRequiredDocumentsGeneralModelRep;
  private LObAttachmentTypeRep attachmentTypeRep;

  public IObOrderDocumentRequiredDocumentsUpdateCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      IObOrderDocumentRequiredDocumentsValueObject requiredDocumentsValueObject) throws Exception {

    String purchaseOrderCode = requiredDocumentsValueObject.getPurchaseOrderCode();
    DObOrderDocument orderDocument = orderDocumentRep.findOneByUserCode(purchaseOrderCode);
    prepareRequiredDocumentsDataDetails(requiredDocumentsValueObject, orderDocument);
    commandUtils.setModificationInfoAndModificationDate(orderDocument);
    orderDocumentRep.updateAndClear(orderDocument);
    return Observable.just("SUCCESS");
  }

  public void unlockSection(String purchaseOrderCode) throws Exception {
    entityLockCommand.unlockSection(
        purchaseOrderCode, IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION);
  }

  public void setOrderDocumentRep(DObOrderDocumentRep orderDocumentRep) {
    this.orderDocumentRep = orderDocumentRep;
  }

  public void setOrderRequiredDocumentsGeneralModelRep(
      IObOrderDocumentRequiredDocumentsGeneralModelRep orderRequiredDocumentsGeneralModelRep) {
    this.orderDocumentRequiredDocumentsGeneralModelRep = orderRequiredDocumentsGeneralModelRep;
  }

  public void setAttachmentTypeRep(LObAttachmentTypeRep attachmentTypeRep) {
    this.attachmentTypeRep = attachmentTypeRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  private void prepareRequiredDocumentsDataDetails(
      IObOrderDocumentRequiredDocumentsValueObject requiredDocumentsValueObject,
      DObOrderDocument purchaseOrder) {

    List<IObPurchaseOrderDocumentValueObject> purchaseOrderRequiredDocuments =
        requiredDocumentsValueObject.getPurchaseOrderDocuments();
    updateOrderDocumentRequiredDocumentsRecords(purchaseOrder, purchaseOrderRequiredDocuments);
  }

  private void updateOrderDocumentRequiredDocumentsRecords(
      DObOrderDocument purchaseOrder,
      List<IObPurchaseOrderDocumentValueObject> purchaseOrderRequiredDocuments) {

    String purchaseOrderCode = purchaseOrder.getUserCode();
    purchaseOrder.setLinesDetails(IDObOrderDocument.requiredDocumentsAttr, new ArrayList<>());
    for (IObPurchaseOrderDocumentValueObject document : purchaseOrderRequiredDocuments) {

      IObOrderDocumentRequiredDocuments requiredDocument = new IObOrderDocumentRequiredDocuments();

      String attachmentTypeCode = document.getDocumentTypeCode();
      Integer numberOfCopies = document.getNumberOfCopies();

      LObAttachmentType attachment = attachmentTypeRep.findOneByUserCode(attachmentTypeCode);

      IObOrderDocumentRequiredDocumentsGeneralModel orderRequiredDocumentsGeneralModel =
          orderDocumentRequiredDocumentsGeneralModelRep.findByUserCodeAndAttachmentCode(
              purchaseOrderCode, attachmentTypeCode);

      if (orderRequiredDocumentsGeneralModel != null) {
        Long orderRequiredDocumentsGeneralModelId = orderRequiredDocumentsGeneralModel.getId();
        requiredDocument.setId(orderRequiredDocumentsGeneralModelId);
      }
      requiredDocument.setAttachmentTypeId(attachment.getId());
      requiredDocument.setNumberOfCopies(numberOfCopies);
      setPurchaseOrderRequiredDocumentSystemData(requiredDocument, purchaseOrder);
      purchaseOrder.addLineDetail(IDObOrderDocument.requiredDocumentsAttr, requiredDocument);
    }
  }

  private void setPurchaseOrderRequiredDocumentSystemData(
      IObOrderDocumentRequiredDocuments purchaseOrderRequiredDocument,
      DObOrderDocument purchaseOrder) {

    purchaseOrderRequiredDocument.setRefInstanceId(purchaseOrder.getId());
    commandUtils.setCreationAndModificationInfoAndDate(purchaseOrderRequiredDocument);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
