package com.ebs.dda.commands.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.inventory.inventorydocument.IDObInventoryDocument;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompany;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouse;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderCompanyDataGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPlantEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPlantEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemRep;
import io.reactivex.Observable;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;

public class DObGoodsReceiptCreateCommand implements ICommand<DObGoodsReceiptCreationValueObject> {

  public static final String PURCHASE_ORDER = "PURCHASE_ORDER";
  public static final String SALES_RETURN = "SALES_RETURN";
  public static final String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";

  private DObPurchaseOrderGeneralModelRep purchasingOrderGeneralModelRep;
  private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  private IObSalesReturnOrderCompanyDataGeneralModelRep
      iObSalesReturnOrderCompanyDataGeneralModelRep;
  private StoreKeeperRep storeKeeperRep;
  private DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep;
  private DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep;
  private DObGoodsReceiptStateMachine stateMachine;
  private CObStorehouseRep storehouseRep;
  private CObCompanyRep cObCompanyRep;
  private IObGoodsReceiptBatchedRecievedItemsRep goodsReceiptBatchedReceivedItemsRep;
  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep
      goodsReceiptOrdinaryReceivedItemsRep;
  private IObOrderLineDetailsGeneralModelRep purchaseOrderItemsGeneralModelRep;

  private MObVendorRep vendorRep;
  private CObPurchasingResponsibleRep purchasingResponsibleRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private IObOrderPlantEnterpriseDataRep orderPlantEnterpriseDataRep;
  private PlatformTransactionManager transactionManager;
  private TransactionTemplate transactionTemplate;
  private MObItemRep itemRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private CObGoodsReceiptRep cObGoodsReceiptRep;
  private CObPlantGeneralModelRep plantRep;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private CommandUtils commandUtils;
  private MObCustomerRep customerRep;
  private DocumentOwnerGeneralModelRep salesReturnOrderDocumentOwnerRep;
  private IObSalesReturnOrderItemRep salesReturnOrderItemRep;

  public DObGoodsReceiptCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    commandUtils = new CommandUtils();
    stateMachine = new DObGoodsReceiptStateMachine();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(DObGoodsReceiptCreationValueObject creationValueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObGoodsReceipt.class.getSimpleName());

    IObInventoryCompany iObInventoryCompany = new IObInventoryCompany();

    if (creationValueObject.getType().equals(PURCHASE_ORDER)) {
      handleCreateGoodsReceiptPurchseOrder(creationValueObject, userCode, iObInventoryCompany);
    } else {
      handleCreateGoodsReceiptSalesReturn(creationValueObject, userCode, iObInventoryCompany);
    }
    return Observable.just(userCode);
  }

  public void handleCreateGoodsReceiptSalesReturn(
      DObGoodsReceiptCreationValueObject creationValueObject,
      String userCode,
      IObInventoryCompany iObInventoryCompany)
      throws Exception {
    DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn = initGoodsReceiptSalesReturn(userCode);

    addGoodsReceiptSalesReturnCompanyHeader(
        creationValueObject, iObInventoryCompany, dObGoodsReceiptSalesReturn);

    addSalesReturnData(creationValueObject, dObGoodsReceiptSalesReturn);

    addGoodsReceiptSalesReturnItems(dObGoodsReceiptSalesReturn, creationValueObject);

    goodsReceiptSalesReturnRep.create(dObGoodsReceiptSalesReturn);
  }

  private void addGoodsReceiptSalesReturnItems(
      DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn,
      DObGoodsReceiptCreationValueObject creationValueObject) {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(creationValueObject.getRefDocumentCode());

    List<Long> salesReturnOrderItemsIds =
        salesReturnOrderItemRep.findDistinctSalesReturnItems(salesReturnOrderGeneralModel.getId());
    List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData>
        goodsReceiptSalesReturnOrdinaryReceivedItemsDataList = new ArrayList<>();
    for (Long salesReturnOrderItemId : salesReturnOrderItemsIds) {
      MObItemGeneralModel mObItemGeneralModel =
          itemGeneralModelRep.findById(salesReturnOrderItemId).get();
      if (mObItemGeneralModel.getTypeCode().equals(COMMERCIAL_ITEM_TYPE)) {

        IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData
            iObGoodsReceiptSalesReturnOrdinaryReceivedItemsData =
                new IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData();

        iObGoodsReceiptSalesReturnOrdinaryReceivedItemsData.setItemId(salesReturnOrderItemId);
        commandUtils.setCreationAndModificationInfoAndDate(
            iObGoodsReceiptSalesReturnOrdinaryReceivedItemsData);

        goodsReceiptSalesReturnOrdinaryReceivedItemsDataList.add(
            iObGoodsReceiptSalesReturnOrdinaryReceivedItemsData);
      }
      dObGoodsReceiptSalesReturn.setLinesDetails(
          IDObGoodsReceiptSalesReturn.salesReturnOrdinaryItems,
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataList);
    }
  }

  public void addSalesReturnData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn) {
    IObGoodsReceiptSalesReturnData goodsReceiptSalesReturnData =
        new IObGoodsReceiptSalesReturnData();

    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptSalesReturnData);

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(creationValueObject.getRefDocumentCode());

    goodsReceiptSalesReturnData.setSalesReturnId(salesReturnOrderGeneralModel.getId());

    setSalesReturnCustomerId(goodsReceiptSalesReturnData, salesReturnOrderGeneralModel);

    setSalesRepresentativeId(goodsReceiptSalesReturnData, salesReturnOrderGeneralModel);

    dObGoodsReceiptSalesReturn.setHeaderDetail(
        IDObGoodsReceiptSalesReturn.salesReturnRefDocumentAttr, goodsReceiptSalesReturnData);

    Long businessUnitId = getBusinessUnitId(salesReturnOrderGeneralModel.getPurchaseUnitCode());
    dObGoodsReceiptSalesReturn.setPurchaseUnitId(businessUnitId);
  }

  public Long getBusinessUnitId(String purchaseUnitCode) {
    return purchasingUnitRep.findOneByUserCode(purchaseUnitCode).getId();
  }

  public void setSalesRepresentativeId(
      IObGoodsReceiptSalesReturnData goodsReceiptSalesReturnData,
      DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel) {
    Long salesRepresentativeId = salesReturnOrderGeneralModel.getDocumentOwnerId();
    DocumentOwnerGeneralModel salesRepresentative =
        salesReturnOrderDocumentOwnerRep.findOneByUserIdAndObjectName(
            salesRepresentativeId, IDObSalesReturnOrder.SYS_NAME);
    goodsReceiptSalesReturnData.setSalesRepresentativeId(salesRepresentative.getUserId());
  }

  public void setSalesReturnCustomerId(
      IObGoodsReceiptSalesReturnData goodsReceiptSalesReturnData,
      DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel) {
    String customerCode = salesReturnOrderGeneralModel.getCustomerCode();
    MObCustomer mObCustomer = customerRep.findOneByUserCode(customerCode);
    goodsReceiptSalesReturnData.setCustomerId(mObCustomer.getId());
  }

  public void handleCreateGoodsReceiptPurchseOrder(
      DObGoodsReceiptCreationValueObject creationValueObject,
      String userCode,
      IObInventoryCompany iObInventoryCompany)
      throws Exception {
    DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder = initPOGoodsReceipt(userCode);

    Long purchaseOrderId = getPurchaseOrderId(creationValueObject);

    initGRPurchaseOrderType(purchaseOrderId, dObGoodsReceiptPurchaseOrder);

    addGoodsReceiptPurchaseOrderCompanyHaeder(
        creationValueObject, iObInventoryCompany, dObGoodsReceiptPurchaseOrder, purchaseOrderId);

    createGoodsReceiptPurchaseOrderWithItems(
        dObGoodsReceiptPurchaseOrder, creationValueObject.getRefDocumentCode());
  }

  public void addGoodsReceiptSalesReturnCompanyHeader(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany,
      DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn) {
    setGRSalesReturnInventoryCompanyData(creationValueObject, iObInventoryCompany);
    dObGoodsReceiptSalesReturn.setHeaderDetail(
        IDObInventoryDocument.companyAttr, iObInventoryCompany);
  }

  public void addGoodsReceiptPurchaseOrderCompanyHaeder(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany,
      DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder,
      Long purchaseOrderId) {
    setGRPurchaseOrderInventoryCompanyData(
        creationValueObject, purchaseOrderId, iObInventoryCompany);
    dObGoodsReceiptPurchaseOrder.setHeaderDetail(
        IDObInventoryDocument.companyAttr, iObInventoryCompany);
  }

  private void setGRPurchaseOrderInventoryCompanyData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      Long purchaseOrderId,
      IObInventoryCompany iObInventoryCompany) {

    setInventoryCompanyData(creationValueObject, iObInventoryCompany);
    setPlantData(purchaseOrderId, iObInventoryCompany);
  }

  private void setGRSalesReturnInventoryCompanyData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany) {

    setInventoryCompanyData(creationValueObject, iObInventoryCompany);
    iObInventoryCompany.setPlantId(getSalesReturnPlantId(creationValueObject.getCompanyCode()));
  }

  private void setInventoryCompanyData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany) {
    commandUtils.setCreationAndModificationInfoAndDate(iObInventoryCompany);

    setCompanyData(creationValueObject, iObInventoryCompany);
    setStorehouseData(creationValueObject, iObInventoryCompany);
    setStorekeeperData(creationValueObject, iObInventoryCompany);
  }

  private void setCompanyData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany) {
    CObCompany company = cObCompanyRep.findOneByUserCode(creationValueObject.getCompanyCode());
    iObInventoryCompany.setCompanyId(company.getId());
  }

  private Long getPurchaseOrderId(DObGoodsReceiptCreationValueObject creationValueObject) {
    String purchaseOrderCode = creationValueObject.getRefDocumentCode();
    return getPurchaseOrderIdByCode(purchaseOrderCode);
  }

  private void createGoodsReceiptPurchaseOrderWithItems(
      DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder, String purchaseOrderCode)
      throws Exception {

    transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            try {
              DObGoodsReceiptPurchaseOrder createdGR =
                  goodsReceiptPurchaseOrderRep.create(dObGoodsReceiptPurchaseOrder);
              addPOItemsToGoodsReceipt(createdGR, purchaseOrderCode);
            } catch (Exception e) {
              status.setRollbackOnly();
              throw new RuntimeException("Create Goods Receipt failed !");
            }
          }
        });
  }

  private Long getPurchaseOrderIdByCode(String purchaseOrderCode) {
    DObPurchaseOrderGeneralModel purchasingOrder =
        purchasingOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    return purchasingOrder.getId();
  }

  private IObSalesReturnOrderCompanyDataGeneralModel getSalesReturnCompany(String salesReturnCode) {
    IObSalesReturnOrderCompanyDataGeneralModel salesReturnOrderCompanyDataGeneralModel =
        iObSalesReturnOrderCompanyDataGeneralModelRep.findOneBySalesReturnOrderCode(
            salesReturnCode);
    return salesReturnOrderCompanyDataGeneralModel;
  }

  private void addPOItemsToGoodsReceipt(DObGoodsReceipt goodsReceipt, String purchaseOrderCode)
      throws Exception {

    List<IObOrderLineDetailsGeneralModel> purchaseOrderItemsGeneralModel =
        purchaseOrderItemsGeneralModelRep.findByOrderCode(purchaseOrderCode);
    for (IObOrderLineDetailsGeneralModel purchaseOrderItemGeneralModel :
        purchaseOrderItemsGeneralModel) {
      MObItem item = itemRep.findOneByUserCode(purchaseOrderItemGeneralModel.getItemCode());
      if (item.getBatchManagementRequired()) {
        IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedRecievedItem =
            createGoodsReceiptBatchedReceivedItem(goodsReceipt, item);
        goodsReceiptBatchedReceivedItemsRep.create(goodsReceiptBatchedRecievedItem);

      } else {
        IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItem =
            createGoodsReceiptOrdinaryReceivedItem(goodsReceipt, item);
        goodsReceiptOrdinaryReceivedItemsRep.create(goodsReceiptOrdinaryReceivedItem);
      }
    }
  }

  private IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData
      createGoodsReceiptBatchedReceivedItem(DObGoodsReceipt goodsReceipt, MObItem item)
          throws Exception {

    IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData goodsReceiptBatchedRecievedItemsData =
        new IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData();

    goodsReceiptBatchedRecievedItemsData.setRefInstanceId(goodsReceipt.getId());
    goodsReceiptBatchedRecievedItemsData.setItemId(item.getId());

    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptBatchedRecievedItemsData);

    return goodsReceiptBatchedRecievedItemsData;
  }

  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData
      createGoodsReceiptOrdinaryReceivedItem(DObGoodsReceipt goodsReceipt, MObItem item) {

    IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData goodsReceiptOrdinaryReceivedItemsData =
        new IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData();

    goodsReceiptOrdinaryReceivedItemsData.setRefInstanceId(goodsReceipt.getId());
    goodsReceiptOrdinaryReceivedItemsData.setItemId(item.getId());

    commandUtils.setCreationAndModificationInfoAndDate(goodsReceiptOrdinaryReceivedItemsData);

    return goodsReceiptOrdinaryReceivedItemsData;
  }

  private void initGRPurchaseOrderType(
      Long purchaseOrderId, DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder)
      throws Exception {
    DObPurchaseOrderGeneralModel purchasingOrderGeneralModel =
        purchasingOrderGeneralModelRep.findById(purchaseOrderId).get();
    createGRPurchaseOrderData(purchasingOrderGeneralModel, dObGoodsReceiptPurchaseOrder);
  }

  private void setPlantData(Long purchaseOrderId, IObInventoryCompany iObInventoryCompany) {
    Long plantId = getPurchaseOrderPlantId(purchaseOrderId);
    iObInventoryCompany.setPlantId(plantId);
  }

  private Long getPurchaseOrderPlantId(Long purchaseOrderId) {
    IObOrderPlantEnterpriseData orderPlant =
        orderPlantEnterpriseDataRep.findOneByRefInstanceId(purchaseOrderId);
    return orderPlant.getEnterpriseId();
  }

  private Long getSalesReturnPlantId(String companyCode) {
    CObPlantGeneralModel plant = plantRep.findOneByCompanyCode(companyCode);
    return plant.getId();
  }

  private void createGRPurchaseOrderData(
      DObPurchaseOrderGeneralModel purchasingOrderGeneralModel,
      DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder) {

    IObGoodsReceiptPurchaseOrderData purchaseOrderData = new IObGoodsReceiptPurchaseOrderData();

    purchaseOrderData.setPurchaseOrderId(purchasingOrderGeneralModel.getId());

    setVendorData(purchasingOrderGeneralModel, purchaseOrderData);
    setPurchaseResponsibleData(purchasingOrderGeneralModel, purchaseOrderData);
    setPurchaseUnitData(purchasingOrderGeneralModel, dObGoodsReceiptPurchaseOrder);

    commandUtils.setCreationAndModificationInfoAndDate(purchaseOrderData);

    dObGoodsReceiptPurchaseOrder.setHeaderDetail(
        IDObGoodsReceiptPurchaseOrder.IOB_PURCHASING_DATA, purchaseOrderData);
  }

  private DObGoodsReceiptPurchaseOrder initPOGoodsReceipt(String userCode) throws Exception {

    DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder = new DObGoodsReceiptPurchaseOrder();

    commandUtils.setCreationAndModificationInfoAndDate(dObGoodsReceiptPurchaseOrder);
    stateMachine.initObjectState(dObGoodsReceiptPurchaseOrder);

    dObGoodsReceiptPurchaseOrder.setUserCode(userCode);

    setGoodsReceiptPurchaseOrderType(dObGoodsReceiptPurchaseOrder);
    return dObGoodsReceiptPurchaseOrder;
  }

  private DObGoodsReceiptSalesReturn initGoodsReceiptSalesReturn(String userCode) throws Exception {

    DObGoodsReceiptSalesReturn dObGoodsReceiptSalesReturn = new DObGoodsReceiptSalesReturn();

    commandUtils.setCreationAndModificationInfoAndDate(dObGoodsReceiptSalesReturn);
    stateMachine.initObjectState(dObGoodsReceiptSalesReturn);

    dObGoodsReceiptSalesReturn.setUserCode(userCode);

    setGoodsReceiptSalesReturnType(dObGoodsReceiptSalesReturn);
    return dObGoodsReceiptSalesReturn;
  }

  private void setGoodsReceiptPurchaseOrderType(
      DObGoodsReceiptPurchaseOrder goodsReceiptPurchaseOrder) {
    CObGoodsReceipt cObGoodsReceipt =
        cObGoodsReceiptRep.findOneByUserCode(DObGoodsReceiptCreateCommand.PURCHASE_ORDER);
    goodsReceiptPurchaseOrder.setTypeId(cObGoodsReceipt.getId());
  }

  private void setGoodsReceiptSalesReturnType(DObGoodsReceiptSalesReturn goodsReceiptSalesReturn) {
    CObGoodsReceipt cObGoodsReceipt =
        cObGoodsReceiptRep.findOneByUserCode(DObGoodsReceiptCreateCommand.SALES_RETURN);
    goodsReceiptSalesReturn.setTypeId(cObGoodsReceipt.getId());
  }

  private void setStorekeeperData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany) {
    String storeKeeperCode = creationValueObject.getStoreKeeperCode();
    if (storeKeeperCode != null) {
      StoreKeeper storeKeeper = storeKeeperRep.findOneByCode(storeKeeperCode);
      iObInventoryCompany.setStoreKeeperId(storeKeeper.getId());
    }
  }

  private void setStorehouseData(
      DObGoodsReceiptCreationValueObject creationValueObject,
      IObInventoryCompany iObInventoryCompany) {
    String storehouseCode = creationValueObject.getStorehouseCode();
    Long plantId;
    if (creationValueObject.getType().equals(IDObGoodsReceiptCreationValueObject.PURCHASE_ORDER)) {
      Long purchaseOrderId = getPurchaseOrderId(creationValueObject);
      plantId = getPurchaseOrderPlantId(purchaseOrderId);
    } else {
      IObSalesReturnOrderCompanyDataGeneralModel salesReturnOrderCompanyDataGeneralModel =
          getSalesReturnCompany(creationValueObject.getRefDocumentCode());
      plantId = getSalesReturnPlantId(salesReturnOrderCompanyDataGeneralModel.getCompanyCode());
    }
    if (storehouseCode != null) {
      CObStorehouse storeHouse = storehouseRep.findOneByUserCodeAndPlantId(storehouseCode, plantId);
      iObInventoryCompany.setStorehouseId(storeHouse.getId());
    }
  }

  private void setPurchaseUnitData(
      DObPurchaseOrderGeneralModel purchasingOrderGeneralModel,
      DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder) {
    String purchaseUnitCode = purchasingOrderGeneralModel.getPurchaseUnitCode();
    Long purchasingUnitId = getBusinessUnitId(purchaseUnitCode);
    dObGoodsReceiptPurchaseOrder.setPurchaseUnitId(purchasingUnitId);
  }

  private void setPurchaseResponsibleData(
      DObPurchaseOrderGeneralModel purchasingOrderGeneralModel,
      IObGoodsReceiptPurchaseOrderData purchaseOrderData) {
    purchaseOrderData.setPurchaseResponsibleId(purchasingOrderGeneralModel.getDocumentOwnerId());
    purchaseOrderData.setPurchaseResponsibleName(
        purchasingOrderGeneralModel.getDocumentOwnerName());
  }

  private void setVendorData(
      DObPurchaseOrderGeneralModel purchasingOrderGeneralModel,
      IObGoodsReceiptPurchaseOrderData purchaseOrderData) {
    String vendorCode = purchasingOrderGeneralModel.getVendorCode();
    Long vendorId = vendorRep.findOneByUserCode(vendorCode).getId();
    purchaseOrderData.setVendorId(vendorId);
    purchaseOrderData.setVendorName(purchasingOrderGeneralModel.getVendorName());
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setStorehouseRep(CObStorehouseRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setGoodsReceiptPurchaseOrderRep(
      DObGoodsReceiptPurchaseOrderRep goodsReceiptPurchaseOrderRep) {
    this.goodsReceiptPurchaseOrderRep = goodsReceiptPurchaseOrderRep;
  }

  public void setStoreKeeperRep(StoreKeeperRep storeKeeperRep) {
    this.storeKeeperRep = storeKeeperRep;
  }

  public void setPurchasingOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchasingOrderGeneralModelRep) {
    this.purchasingOrderGeneralModelRep = purchasingOrderGeneralModelRep;
  }

  public void setPurchaseOrderItemsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep purchaseORderItemsGeneralModelRep) {
    this.purchaseOrderItemsGeneralModelRep = purchaseORderItemsGeneralModelRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setPurchasingResponsibleRep(CObPurchasingResponsibleRep purchasingResponsibleRep) {
    this.purchasingResponsibleRep = purchasingResponsibleRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setGoodsReceiptBatchedReceivedItemsRep(
      IObGoodsReceiptBatchedRecievedItemsRep goodsReceiptBatchedReceivedItemsRep) {
    this.goodsReceiptBatchedReceivedItemsRep = goodsReceiptBatchedReceivedItemsRep;
  }

  public void setGoodsReceiptOrdinaryReceivedItemsRep(
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep
          goodsReceiptOrdinaryReceivedItemsRep) {
    this.goodsReceiptOrdinaryReceivedItemsRep = goodsReceiptOrdinaryReceivedItemsRep;
  }

  public void setOrderPlantEnterpriseDataRep(
      IObOrderPlantEnterpriseDataRep orderPlantEnterpriseDataRep) {
    this.orderPlantEnterpriseDataRep = orderPlantEnterpriseDataRep;
  }

  public void setCObCompanyRep(CObCompanyRep cObCompanyRep) {
    this.cObCompanyRep = cObCompanyRep;
  }

  public void setCObGoodsReceiptRep(CObGoodsReceiptRep cObGoodsReceiptRep) {
    this.cObGoodsReceiptRep = cObGoodsReceiptRep;
  }

  public void setGoodsReceiptSalesReturnRep(
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep) {
    this.goodsReceiptSalesReturnRep = goodsReceiptSalesReturnRep;
  }

  public void setPlantRep(CObPlantGeneralModelRep plantRep) {
    this.plantRep = plantRep;
  }

  public void setIObSalesReturnOrderCompanyDataGeneralModelRep(
      IObSalesReturnOrderCompanyDataGeneralModelRep iObSalesReturnOrderCompanyDataGeneralModelRep) {
    this.iObSalesReturnOrderCompanyDataGeneralModelRep =
        iObSalesReturnOrderCompanyDataGeneralModelRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }

  public void setSalesReturnOrderDocumentOwnerRep(
      DocumentOwnerGeneralModelRep salesReturnOrderDocumentOwnerRep) {
    this.salesReturnOrderDocumentOwnerRep = salesReturnOrderDocumentOwnerRep;
  }

  public void setSalesReturnOrderItemRep(IObSalesReturnOrderItemRep salesReturnOrderItemRep) {
    this.salesReturnOrderItemRep = salesReturnOrderItemRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }
}
