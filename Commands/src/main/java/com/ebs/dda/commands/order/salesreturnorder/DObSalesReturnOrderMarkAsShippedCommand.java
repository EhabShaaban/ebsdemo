package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderMarkAsShippedValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderCycleDates;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderActionNames;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderCycleDatesRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class DObSalesReturnOrderMarkAsShippedCommand
    implements ICommand<DObSalesReturnOrderMarkAsShippedValueObject> {

  private DObSalesReturnOrderRep salesReturnOrderRep;
  private IObSalesReturnOrderCycleDatesRep cycleDatesRep;
  private CommandUtils commandUtils;

  public DObSalesReturnOrderMarkAsShippedCommand() {
    this.commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(DObSalesReturnOrderMarkAsShippedValueObject valueObject)
      throws Exception {

    DObSalesReturnOrder salesRetrunOrder =
        salesReturnOrderRep.findOneByUserCode(valueObject.getSalesReturnOrderCode());
    commandUtils.setModificationInfoAndModificationDate(salesRetrunOrder);

    updateSalesReturnOrderState(getSalesReturnOrderStateMachine(salesRetrunOrder));
    updateSalesReturnOrderCycleDates(salesRetrunOrder);

    salesReturnOrderRep.update(salesRetrunOrder);
    return Observable.just(salesRetrunOrder.getUserCode());
  }

  private void updateSalesReturnOrderCycleDates(DObSalesReturnOrder salesReturnOrder) {
    IObSalesReturnOrderCycleDates cycleDates =
        cycleDatesRep.findOneByRefInstanceId(salesReturnOrder.getId());

    if (cycleDates == null) {
      cycleDates = new IObSalesReturnOrderCycleDates();
      commandUtils.setCreationAndModificationInfoAndDate(cycleDates);
      cycleDates.setShippingDate(new DateTime());
    }

    salesReturnOrder.setHeaderDetail(IDObSalesReturnOrder.cycleDatesAttr, cycleDates);
  }

  private void updateSalesReturnOrderState(AbstractStateMachine salesReturnOrderStateMachine) {
    salesReturnOrderStateMachine.fireEvent(IDObSalesReturnOrderActionNames.MARK_AS_SHIPPED);
    salesReturnOrderStateMachine.save();
  }

  private DObSalesReturnOrderStateMachine getSalesReturnOrderStateMachine(
      DObSalesReturnOrder salesReturnOrder) throws Exception {
    DObSalesReturnOrderStateMachine salesReturnOrderStateMachine =
        new DObSalesReturnOrderStateMachine();
    salesReturnOrderStateMachine.initObjectState(salesReturnOrder);
    return salesReturnOrderStateMachine;
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setCycleDatesRep(IObSalesReturnOrderCycleDatesRep cycleDatesRep) {
    this.cycleDatesRep = cycleDatesRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
