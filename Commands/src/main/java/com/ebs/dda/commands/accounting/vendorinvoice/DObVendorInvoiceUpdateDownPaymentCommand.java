package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsDownPayment;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObVendorInvoiceUpdateDownPaymentCommand implements ICommand<String> {

    private final CommandUtils commandUtils;

    private IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep;
    private IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep;
    private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;


    public DObVendorInvoiceUpdateDownPaymentCommand() {
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(
            String vendorInvoiceCode) throws Exception {

        IObVendorInvoiceDetailsGeneralModel  invoiceDetailsGeneralModel=
                invoiceDetailsRep.findByInvoiceCode(vendorInvoiceCode);
        IObVendorInvoiceDetails vendorInvoiceDetails = vendorInvoiceDetailsRep.findOneByRefInstanceId(invoiceDetailsGeneralModel.getRefInstanceId());

        setVendorInvoiceDetailsDownPayment(invoiceDetailsGeneralModel , vendorInvoiceDetails);
        vendorInvoiceDetailsRep.updateAndClear(vendorInvoiceDetails);
        return Observable.just("SUCCESS");
    }

    private void setVendorInvoiceDetailsDownPayment(IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel, IObVendorInvoiceDetails invoiceDetails) throws Exception {
        List<DObPaymentRequestGeneralModel> invoiceDownPayments =
                paymentRequestGeneralModelRep
                .findAllByPaymentTypeAndReferenceDocumentCodeAndDueDocumentTypeAndCurrentStatesLike(
                 PaymentTypeEnum.PaymentType.VENDOR.name()
                 ,invoiceDetailsGeneralModel.getPurchaseOrderCode()
                 ,DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name()
                 ,"%"+DObPaymentRequestStateMachine.PAYMENT_DONE_STATE+"%");

        if (invoiceDownPayments != null) {
            for (DObPaymentRequestGeneralModel downPayment : invoiceDownPayments) {
                setDownPaymentData(invoiceDetails, downPayment);
            }
            commandUtils.setModificationInfoAndModificationDate(invoiceDetails);
        }
    }

    private void setDownPaymentData(IObVendorInvoiceDetails invoiceDetails, DObPaymentRequestGeneralModel downPayment) throws Exception {
        IObVendorInvoiceDetailsDownPayment invoiceDetailsDownPayment = new IObVendorInvoiceDetailsDownPayment();
        invoiceDetailsDownPayment.setDownPaymentId(downPayment.getId());
        invoiceDetailsDownPayment.setDownPaymentAmount(downPayment.getAmountValue());
        commandUtils.setCreationAndModificationInfoAndDate(invoiceDetailsDownPayment);
        invoiceDetails.addLineDetail(IDObVendorInvoice.downPaymentAttr, invoiceDetailsDownPayment);
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setInvoiceDetailsRep(IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep) {
        this.invoiceDetailsRep = invoiceDetailsRep;
    }

    public void setPaymentRequestGeneralModelRep(DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep) {
        this.paymentRequestGeneralModelRep = paymentRequestGeneralModelRep;
    }

    public void setVendorInvoiceDetailsRep(IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep) {
        this.vendorInvoiceDetailsRep = vendorInvoiceDetailsRep;
    }
}
