package com.ebs.dda.commands.masterdata.item;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfo;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemAccountingInfoValueObject;
import com.ebs.dda.masterdata.item.IMObItemSectionNames;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import io.reactivex.Observable;

public class MObItemAccountingDetailsEditCommand
    implements ICommand<MObItemAccountingInfoValueObject> {
  private final CommandUtils commandUtils;
  private EntityLockCommand<MObItem> entityLockCommand;
  private MObItemRep itemRep;
  private IObItemAccountingInfoRep accountingInfoRep;
  private HObChartOfAccountsGeneralModelRep accountsGeneralModelRep;

  public MObItemAccountingDetailsEditCommand() {
    commandUtils = new CommandUtils();
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setAccountingInfoRep(IObItemAccountingInfoRep accountingInfoRep) {
    this.accountingInfoRep = accountingInfoRep;
  }

  public void setUserAccountSecurityService(IUserAccountSecurityService securityService) {
    this.commandUtils.setUserAccountSecurityService(securityService);
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep accountsGeneralModelRep) {
    this.accountsGeneralModelRep = accountsGeneralModelRep;
  }

  public LockDetails lock(String resourceCode) throws Exception {
    LockDetails lockDetails =
        entityLockCommand.lockSection(
            resourceCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);
    return lockDetails;
  }

  public void unlockSection(String vendorCode) throws Exception {
    entityLockCommand.unlockSection(vendorCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);
  }

  @Override
  public Observable<String> executeCommand(MObItemAccountingInfoValueObject valueObject)
      throws Exception {
    MObItem item = itemRep.findOneByUserCode(valueObject.getItemCode());
    prepareAccountingDetailsData(valueObject, item);
    commandUtils.setModificationInfoAndModificationDate(item);
    itemRep.updateAndClear(item);
    return Observable.just("SUCCESS");
  }

  private void prepareAccountingDetailsData(
      MObItemAccountingInfoValueObject valueObject, MObItem item) throws Exception {
    IObItemAccountingInfo itemAccountingInfo =
        accountingInfoRep.findOneByRefInstanceId(item.getId());
    if (itemAccountingInfo == null) {
      itemAccountingInfo = new IObItemAccountingInfo();
      commandUtils.setCreationAndModificationInfoAndDate(itemAccountingInfo);
    }
    HObChartOfAccountsGeneralModel account = getAccount(valueObject);
    itemAccountingInfo.setRefInstanceId(item.getId());
    itemAccountingInfo.setChartOfAccountId(account.getId());
    itemAccountingInfo.setCostFactor(valueObject.getCostFactor());

    commandUtils.setModificationInfoAndModificationDate(itemAccountingInfo);

    item.setSingleDetail(IMObItem.itemAccountInfoAttr, itemAccountingInfo);
  }

  private HObChartOfAccountsGeneralModel getAccount(MObItemAccountingInfoValueObject valueObject) {
    return accountsGeneralModelRep.findOneByUserCode(valueObject.getAccountCode());
  }
}
