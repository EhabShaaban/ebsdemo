package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoicePostPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class DObSalesInvoicePostCommand implements ICommand<DObSalesInvoicePostValueObject> {

	private DObSalesInvoiceRep dObSalesInvoiceRep;
	private DObSalesInvoicePostPreparationGeneralModelRep postPreparationGeneralModelRep;
	private IObSalesInvoiceBusinessPartnerRep businessPartnerRep;
	private DObSalesOrderRep salesOrderRep;
	private DObCollectionGeneralModelRep collectionRep;
	private IObNotesReceivablesReferenceDocumentGeneralModelRep notesReceivablesReferenceDocumentRep;
	private IObSalesInvoiceSummaryRep summaryRep;
	private CommandUtils commandUtils;
	private CObFiscalYearRep cObFiscalYearRep;

	public DObSalesInvoicePostCommand() {
		commandUtils = new CommandUtils();
	}

	@Override
	@Transactional
	public Observable<DObSalesInvoice> executeCommand(DObSalesInvoicePostValueObject valueObject)
					throws Exception {
		DObSalesInvoice salesInvoice = dObSalesInvoiceRep
						.findOneByUserCode(valueObject.getSalesInvoiceCode());
		setSalesInvoicePostState(getSalesInvoiceStateMachine(salesInvoice));
		IObSalesInvoicePostingDetails postingDetails = createPostingDetails(valueObject,
						salesInvoice);
		salesInvoice.setHeaderDetail(IDObSalesInvoice.postingDetails, postingDetails);
		commandUtils.setModificationInfoAndModificationDate(salesInvoice);
		updateDownPaymentAndRemaining(salesInvoice);
		dObSalesInvoiceRep.update(salesInvoice);
		return Observable.just(salesInvoice);
	}

	private void setSalesInvoicePostState(AbstractStateMachine salesInvoiceStateMachine)
					throws Exception {
		if (salesInvoiceStateMachine
						.canFireEvent(IDObSalesInvoiceActionNames.DELIVER_TO_CUSTOMER)) {
			salesInvoiceStateMachine.fireEvent(IDObSalesInvoiceActionNames.DELIVER_TO_CUSTOMER);
			salesInvoiceStateMachine.save();
		}
	}

	private AbstractStateMachine getSalesInvoiceStateMachine(DObSalesInvoice salesInvoice)
					throws Exception {
		DObSalesInvoiceFactory salesInvoiceStateMachine = new DObSalesInvoiceFactory();
		AbstractStateMachine invoiceStateMachine = salesInvoiceStateMachine
						.createSalesInvoiceStateMachine(salesInvoice);
		return invoiceStateMachine;
	}

	private IObSalesInvoicePostingDetails createPostingDetails(
					DObSalesInvoicePostValueObject salesInvoicePostValueObject,
					DObSalesInvoice salesInvoice) {
		DObSalesInvoicePostPreparationGeneralModel latestExchangeRate = postPreparationGeneralModelRep
						.findOneByCode(salesInvoice.getUserCode());

		IObSalesInvoicePostingDetails postingDetails = new IObSalesInvoicePostingDetails();
		DateTime journalDate = commandUtils.getDateTime(salesInvoicePostValueObject.getJournalEntryDate());
		commandUtils.setCreationAndModificationInfoAndDate(postingDetails);
		String loggedInUser = commandUtils.getLoggedInUser();

		postingDetails.setDueDate(journalDate);
		postingDetails.setAccountant(loggedInUser);
		postingDetails.setActivationDate(new DateTime());
		postingDetails.setExchangeRateId(latestExchangeRate.getExchangeRateId());
		postingDetails.setCurrencyPrice(latestExchangeRate.getCurrencyPrice());
		postingDetails.setFiscalPeriodId(getFiscalYear(journalDate).getId());

		return postingDetails;
	}

	private CObFiscalYear getFiscalYear(DateTime journalDate) {
		return cObFiscalYearRep
			.findByCurrentStatesLike("%" + BasicStateMachine.ACTIVE_STATE + "%")
			.stream()
			.filter(fiscalYear -> journalDate.isAfter(fiscalYear.getFromDate())
												 && journalDate.isBefore(fiscalYear.getToDate()))
			.findFirst()
			.get();
	}

	private void updateDownPaymentAndRemaining(DObSalesInvoice salesInvoice) {
		IObSalesInvoiceBusinessPartner businessPartner = businessPartnerRep.findOneByRefInstanceId(salesInvoice.getId());
		List<DObCollectionGeneralModel> collections = getCollections(businessPartner.getSalesOrderid());
		List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivables = getNotesReceivables(businessPartner.getSalesOrderid());

		BigDecimal collectionTotalAmount = BigDecimal.ZERO;
		BigDecimal notesReceivablesTotalAmount = BigDecimal.ZERO;

		if (collections != null && !collections.isEmpty()) {
			collectionTotalAmount = collections.stream().map(DObCollectionGeneralModel::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
		}

		if (notesReceivables != null && !notesReceivables.isEmpty()) {
			notesReceivablesTotalAmount = notesReceivables.stream().map(IObNotesReceivablesReferenceDocumentGeneralModel::getAmountToCollect).reduce(BigDecimal.ZERO, BigDecimal::add);
		}

		BigDecimal totalDownPayment = collectionTotalAmount.add(notesReceivablesTotalAmount);
		if (totalDownPayment.compareTo(BigDecimal.ZERO) > 0) {
			updateDownPayment(businessPartner, totalDownPayment);
			updateRemaining(salesInvoice.getId(), totalDownPayment);
		}
	}

	private List<DObCollectionGeneralModel> getCollections(Long salesOrderId) {
		List<DObCollectionGeneralModel> collections = null;
		if (salesOrderId != null) {
			Optional<DObSalesOrder> salesOrder = salesOrderRep.findById(salesOrderId);
			collections = collectionRep.findAllByRefDocumentCodeAndRefDocumentTypeCodeAndCurrentStatesLike(
					salesOrder.get().getUserCode(), RefDocumentTypeEnum.SALES_ORDER, "%"+ DObCollectionStateMachine.ACTIVE + "%");
		}
		return collections;
	}

	private List<IObNotesReceivablesReferenceDocumentGeneralModel> getNotesReceivables(Long salesOrderId) {
		List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivables = null;
		if (salesOrderId != null) {
			Optional<DObSalesOrder> salesOrder = salesOrderRep.findById(salesOrderId);
			notesReceivables = notesReceivablesReferenceDocumentRep.findByRefDocumentCodeAndDocumentTypeAndCurrentStatesLike(
					salesOrder.get().getUserCode(), RefDocumentTypeEnum.SALES_ORDER, "%" + DObNotesReceivablesStateMachine.ACTIVE_STATE + "%" );
		}
		return notesReceivables;
	}

	private void updateDownPayment(IObSalesInvoiceBusinessPartner businessPartner, BigDecimal totalAmount) {
		businessPartner.setDownPayment(totalAmount);
		businessPartnerRep.save(businessPartner);
	}

	private void updateRemaining(Long salesInvoiceId, BigDecimal totalAmount) {
		IObSalesInvoiceSummary summary = summaryRep.findOneByRefInstanceId(salesInvoiceId);
		summary.setRemaining(summary.getRemaining().subtract(totalAmount));
		summaryRep.save(summary);
	}

	public void setdObSalesInvoiceRep(DObSalesInvoiceRep dObSalesInvoiceRep) {
		this.dObSalesInvoiceRep = dObSalesInvoiceRep;
	}

	public void setPostPreparationGeneralModelRep(
					DObSalesInvoicePostPreparationGeneralModelRep postPreparationGeneralModelRep) {
		this.postPreparationGeneralModelRep = postPreparationGeneralModelRep;
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setcObFiscalYearRep(CObFiscalYearRep cObFiscalYearRep) {
		this.cObFiscalYearRep = cObFiscalYearRep;
	}

	public void setBusinessPartnerRep(IObSalesInvoiceBusinessPartnerRep businessPartnerRep) {
		this.businessPartnerRep = businessPartnerRep;
	}

	public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
		this.salesOrderRep = salesOrderRep;
	}

	public void setCollectionRep(DObCollectionGeneralModelRep collectionRep) {
		this.collectionRep = collectionRep;
	}

	public void setSummaryRep(IObSalesInvoiceSummaryRep summaryRep) {
		this.summaryRep = summaryRep;
	}

	public void setNotesReceivablesReferenceDocumentRep(IObNotesReceivablesReferenceDocumentGeneralModelRep notesReceivablesReferenceDocumentRep) {
		this.notesReceivablesReferenceDocumentRep = notesReceivablesReferenceDocumentRep;
	}
}
