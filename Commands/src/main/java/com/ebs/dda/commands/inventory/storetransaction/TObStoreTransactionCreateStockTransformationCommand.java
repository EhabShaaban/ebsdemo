package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStockTransformationStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionCreateStockTransformationValueObject;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStockTransformationStoreTransactionRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TObStoreTransactionCreateStockTransformationCommand
    implements ICommand<TObStoreTransactionCreateStockTransformationValueObject> {

  public static final String ADD_TRANSACTION_OPERATION = "ADD";
  private CommandUtils commandUtils;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep;
  private TObStockTransformationStoreTransactionRep stockTransformationStoreTransactionRep;
  private TObStoreTransactionRep storeTransactionRep;

  private List<TObStockTransformationStoreTransaction> stockTransformationStoreTransactionsList;

  public TObStoreTransactionCreateStockTransformationCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  public Observable<String> executeCommand(
      TObStoreTransactionCreateStockTransformationValueObject stockTransformationValueObject)
      throws Exception {
    String stockTransformationCode = stockTransformationValueObject.getStockTransformationCode();
    initStockTransformationStoreTransactionEntity(stockTransformationCode);
    return Observable.just("SUCCESS");
  }

  private void initStockTransformationStoreTransactionEntity(String stockTransformationCode) {
    DObStockTransformationGeneralModel stockTransformationGeneralModel =
        stockTransformationGeneralModelRep.findOneByUserCode(stockTransformationCode);
    stockTransformationStoreTransactionsList =
        new ArrayList<TObStockTransformationStoreTransaction>();

    TObStoreTransaction storeTransaction = getStoreTransaction(stockTransformationGeneralModel);

    createTakeStockTransformationStoreTransactions(
        stockTransformationGeneralModel, storeTransaction);

    createAddStockTransformationStoreTransactions(
        stockTransformationGeneralModel, storeTransaction);
    stockTransformationStoreTransactionRep.saveAll(stockTransformationStoreTransactionsList);
  }

  private void createAddStockTransformationStoreTransactions(
      DObStockTransformationGeneralModel stockTransformationGeneralModel,
      TObStoreTransaction storeTransaction) {
    TObStockTransformationStoreTransaction addStockTransformationStoreTransaction =
        new TObStockTransformationStoreTransaction();
    setAddTransactionOperation(addStockTransformationStoreTransaction);

    setCommonStockTransformationStoreTransactionsDate(
        addStockTransformationStoreTransaction, stockTransformationGeneralModel, storeTransaction);

    setAddStockType(stockTransformationGeneralModel, addStockTransformationStoreTransaction);

    addStockTransformationStoreTransaction.setOriginalAddingDate(
        storeTransaction.getCreationDate());

    addStockTransformationStoreTransaction.setRemainingQuantity(
        BigDecimal.valueOf(stockTransformationGeneralModel.getTransformedQty()));

    stockTransformationStoreTransactionsList.add(addStockTransformationStoreTransaction);
  }

  private void createTakeStockTransformationStoreTransactions(
      DObStockTransformationGeneralModel stockTransformationGeneralModel,
      TObStoreTransaction storeTransaction) {
    TObStockTransformationStoreTransaction takeStockTransformationStoreTransaction =
        new TObStockTransformationStoreTransaction();
    setTakeTransactionOperation(takeStockTransformationStoreTransaction);

    setCommonStockTransformationStoreTransactionsDate(
        takeStockTransformationStoreTransaction, stockTransformationGeneralModel, storeTransaction);

    takeStockTransformationStoreTransaction.setStockType(storeTransaction.getStockType());

    takeStockTransformationStoreTransaction.setRemainingQuantity(BigDecimal.ZERO);

    stockTransformationStoreTransactionsList.add(takeStockTransformationStoreTransaction);
  }

  private TObStoreTransaction getStoreTransaction(
      DObStockTransformationGeneralModel stockTransformationGeneralModel) {
    return storeTransactionRep.findOneByUserCode(
        stockTransformationGeneralModel.getRefStoreTransactionCode());
  }

  private void setCommonStockTransformationStoreTransactionsDate(
      TObStockTransformationStoreTransaction stockTransformationStoreTransaction,
      DObStockTransformationGeneralModel stockTransformationGeneralModel,
      TObStoreTransaction storeTransaction) {

    setStockTransformationStoreTransactionCreationData(stockTransformationStoreTransaction);
    setStoreTransactionUserCode(stockTransformationStoreTransaction);

    stockTransformationStoreTransaction.setCompanyId(storeTransaction.getCompanyId());
    stockTransformationStoreTransaction.setPlantId(storeTransaction.getPlantId());
    stockTransformationStoreTransaction.setStorehouseId(storeTransaction.getStorehouseId());
    stockTransformationStoreTransaction.setPurchaseUnitId(storeTransaction.getPurchaseUnitId());
    stockTransformationStoreTransaction.setItemId(storeTransaction.getItemId());
    stockTransformationStoreTransaction.setUnitOfMeasureId(storeTransaction.getUnitOfMeasureId());
    stockTransformationStoreTransaction.setQuantity(
        BigDecimal.valueOf(stockTransformationGeneralModel.getTransformedQty()));
    if (stockTransformationStoreTransaction
        .getTransactionOperation()
        .getId()
        .toString()
        .equals(ADD_TRANSACTION_OPERATION)) {
      stockTransformationStoreTransaction.setEstimateCost(storeTransaction.getEstimateCost());
      stockTransformationStoreTransaction.setActualCost(storeTransaction.getActualCost());
    }
    stockTransformationStoreTransaction.setDocumentReferenceId(
        stockTransformationGeneralModel.getId());
    stockTransformationStoreTransaction.setRefTransactionId(storeTransaction.getId());
  }

  private void setAddTransactionOperation(
      TObStockTransformationStoreTransaction stockTransformationStoreTransaction) {
    TransactionTypeEnum.TransactionType transactionOperationType =
        TransactionTypeEnum.TransactionType.ADD;
    TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    transactionOperationEnum.setId(transactionOperationType);
    stockTransformationStoreTransaction.setTransactionOperation(transactionOperationEnum);
  }

  private void setTakeTransactionOperation(
      TObStockTransformationStoreTransaction stockTransformationStoreTransaction) {
    TransactionTypeEnum.TransactionType transactionOperationType =
        TransactionTypeEnum.TransactionType.TAKE;
    TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    transactionOperationEnum.setId(transactionOperationType);
    stockTransformationStoreTransaction.setTransactionOperation(transactionOperationEnum);
  }

  private void setAddStockType(
      DObStockTransformationGeneralModel stockTransformationGeneralModel,
      TObStockTransformationStoreTransaction addStockTransformationStoreTransaction) {
    StockTypeEnum.StockType stockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(stockTransformationGeneralModel.getNewStockType());
    StockTypeEnum stockTypeEnum = new StockTypeEnum();
    stockTypeEnum.setId(stockTypeEnumObject);
    addStockTransformationStoreTransaction.setStockType(stockTypeEnum);
  }

  private void setStockTransformationStoreTransactionCreationData(
      TObStockTransformationStoreTransaction stockTransformationStoreTransaction) {
    commandUtils.setCreationAndModificationInfoAndDate(stockTransformationStoreTransaction);
  }

  private void setStoreTransactionUserCode(
      TObStockTransformationStoreTransaction stockTransformationStoreTransaction) {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(TObStoreTransaction.class.getSimpleName());
    stockTransformationStoreTransaction.setUserCode(userCode);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setTObStockTransformationStoreTransactionRep(
      TObStockTransformationStoreTransactionRep stockTransformationStoreTransactionRep) {
    this.stockTransformationStoreTransactionRep = stockTransformationStoreTransactionRep;
  }

  public void setStoreTransactionRep(TObStoreTransactionRep storeTransactionRep) {
    this.storeTransactionRep = storeTransactionRep;
  }

  public void setStockTransformationGeneralModelRep(
      DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep) {
    this.stockTransformationGeneralModelRep = stockTransformationGeneralModelRep;
  }
}
