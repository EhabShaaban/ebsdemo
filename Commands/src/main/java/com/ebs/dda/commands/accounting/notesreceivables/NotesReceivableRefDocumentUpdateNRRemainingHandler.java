package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetails;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;

import java.math.BigDecimal;

public class NotesReceivableRefDocumentUpdateNRRemainingHandler
    extends NotesReceivableRefDocumentUpdateRemainingHandler {

  private DObMonetaryNotesRep notesReceivablesRep;
  private IObMonetaryNotesDetailsRep notesDetailsRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;


  public void updateRemaining(String refDocumentCode, BigDecimal amountToCollect, String nrCurrencyIso) throws Exception {
    DObMonetaryNotes notesReceivable = getNotesReceivableRefDocumentId(refDocumentCode);
    IObMonetaryNotesDetails notesReceivableDetails =
            getNotesReceivableDetailsByNrId(notesReceivable.getId());
    String refNRCurrencyIso = getNotesReceivableCurrencyIso(notesReceivable.getId());
    BigDecimal amount = getAmountToCollectWithExchangeRate(nrCurrencyIso, refNRCurrencyIso, amountToCollect);
    subtractAmountToCollectFromNrRemaining(notesReceivable, notesReceivableDetails, amount);
    notesReceivablesRep.update(notesReceivable);
  }

  private DObMonetaryNotes getNotesReceivableRefDocumentId(String refDocumentCode) {
    DObMonetaryNotes notesReceivable = notesReceivablesRep.findOneByUserCode(refDocumentCode);
    return notesReceivable;
  }
  private IObMonetaryNotesDetails getNotesReceivableDetailsByNrId(Long notesReceivableId) {
    return notesDetailsRep.findOneByRefInstanceId(notesReceivableId);
  }

  private String getNotesReceivableCurrencyIso(Long notesReceivableId) {
    return notesReceivablesGeneralModelRep.findById(notesReceivableId).get().getCurrencyIso();
  }

  private void subtractAmountToCollectFromNrRemaining(DObMonetaryNotes notesReceivable,
          IObMonetaryNotesDetails notesReceivableDetails, BigDecimal amountToCollect) {
    notesReceivableDetails.setRemaining(
            notesReceivableDetails.getRemaining().subtract(amountToCollect));
    commandUtils.setModificationInfoAndModificationDate(notesReceivableDetails);
    commandUtils.setModificationInfoAndModificationDate(notesReceivable);
    notesReceivable.setHeaderDetail(IDObMonetaryNotes.detailsAttr, notesReceivableDetails);
  }
  public void setDObMonetaryNotesRep(DObMonetaryNotesRep notesReceivablesRep) {
    this.notesReceivablesRep = notesReceivablesRep;
  }

  public void setIObMonetaryNotesDetailsRep(IObMonetaryNotesDetailsRep notesDetailsRep) {
    this.notesDetailsRep = notesDetailsRep;
  }
  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setNotesReceivablesGeneralModelRep(DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }
}
