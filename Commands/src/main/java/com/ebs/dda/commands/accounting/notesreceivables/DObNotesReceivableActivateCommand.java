package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesActionNames;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.util.List;

public class DObNotesReceivableActivateCommand
  implements ICommand<DObNotesReceivableActivateValueObject> {

  private static final String ACTIVE_STATE = "Active";

  private CommandUtils commandUtils;
  private DObMonetaryNotesRep notesReceivablesRep;
  private IObMonetaryNotesDetailsGeneralModelRep nrDetailsGMRep;
  private CObExchangeRateGeneralModelRep exchangeRateGMRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObNotesReceivableActivateCommand() {
    commandUtils = new CommandUtils();
  }

  @Override public Observable<String> executeCommand(
    DObNotesReceivableActivateValueObject notesReceivablesValueObject) throws Exception {

    String nrCode = notesReceivablesValueObject.getNotesReceivableCode();
    DObMonetaryNotes notesReceivable = notesReceivablesRep.findOneByUserCode(nrCode);

    setNotesReceivableActiveState(getNotesReceivablesMachine(notesReceivable));

    IObNotesReceivableActivationDetails notesReceivablesActivationDetails =
      constructActivationDetails(notesReceivablesValueObject, notesReceivable.getId());

    notesReceivable
      .setHeaderDetail(IDObMonetaryNotes.activationDetails, notesReceivablesActivationDetails);

    commandUtils.setModificationInfoAndModificationDate(notesReceivable);

    notesReceivablesRep.update(notesReceivable);
    return Observable.just(notesReceivable.getUserCode());
  }

  private IObNotesReceivableActivationDetails constructActivationDetails(
    DObNotesReceivableActivateValueObject valueObject, Long nrId) {
    IObNotesReceivableActivationDetails activationDetails =
      new IObNotesReceivableActivationDetails();

    commandUtils.setCreationAndModificationInfoAndDate(activationDetails);
    activationDetails.setActivationDate(activationDetails.getModifiedDate());

    String loggedInUser = commandUtils.getLoggedInUser();
    activationDetails.setAccountant(loggedInUser);

    DateTime journalEntryDateTime = commandUtils.getDateTime(valueObject.getJournalEntryDate());
    activationDetails.setDueDate(journalEntryDateTime);

    CObExchangeRateGeneralModel exchangeRateGeneralModel = getExchangeRate(nrId);
    activationDetails.setCurrencyPrice(exchangeRateGeneralModel.getSecondValue());
    activationDetails.setExchangeRateId(exchangeRateGeneralModel.getId());
    activationDetails.setFiscalPeriodId(getActiveFiscalPeriodId(journalEntryDateTime));

    return activationDetails;
  }

  private CObExchangeRateGeneralModel getExchangeRate(Long nrId) {
    IObMonetaryNotesDetailsGeneralModel nrDetails = nrDetailsGMRep.findOneByRefInstanceId(nrId);
    String firstCurrencyIso = nrDetails.getCurrencyIso();
    String secondCurrencyIso = nrDetails.getCompanyCurrencyIso();
    CObExchangeRateGeneralModel exchangeRateGeneralModel =
      exchangeRateGMRep.findLatestDefaultExchangeRate(firstCurrencyIso, secondCurrencyIso);
    return exchangeRateGeneralModel;
  }

  private Long getActiveFiscalPeriodId(DateTime journalEntryDateTime) {

    List<CObFiscalYear> fiscalYears =
            fiscalYearRep.findByCurrentStatesLike("%" + ACTIVE_STATE + "%");
    for (CObFiscalYear fiscalYear : fiscalYears) {
      if (journalEntryDateTime.isAfter(fiscalYear.getFromDate())
              && journalEntryDateTime.isBefore(fiscalYear.getToDate())) {
        return fiscalYear.getId();
      }
    }
    throw new MissingSystemDataException("There is no active fiscal year");
  }

  private void setNotesReceivableActiveState(AbstractStateMachine notesReceivablesStateMachine)
    throws Exception {
    if (notesReceivablesStateMachine.canFireEvent(IDObNotesReceivablesActionNames.ACTIVATE)) {
      notesReceivablesStateMachine.fireEvent(IDObNotesReceivablesActionNames.ACTIVATE);
      notesReceivablesStateMachine.save();
    }
  }

  private DObNotesReceivablesStateMachine getNotesReceivablesMachine(
    DObMonetaryNotes notesReceivables) throws Exception {
    DObNotesReceivablesStateMachine stateMachine = new DObNotesReceivablesStateMachine();
    stateMachine.initObjectState(notesReceivables);
    return stateMachine;
  }

  public void setDObNotesReceivablesRep(DObMonetaryNotesRep notesReceivablesRep) {
    this.notesReceivablesRep = notesReceivablesRep;
  }

  public void setNrDetailsGMRep(IObMonetaryNotesDetailsGeneralModelRep nrDetailsGMRep) {
    this.nrDetailsGMRep = nrDetailsGMRep;
  }

  public void setExchangeRateGMRep(CObExchangeRateGeneralModelRep exchangeRateGMRep) {
    this.exchangeRateGMRep = exchangeRateGMRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  public void setUserAccountSecurityService(
    IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
