package com.ebs.dda.commands.masterdata.customer;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.customer.*;
import com.ebs.dda.masterdata.customer.MObCustomerStateMachine;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;
import io.reactivex.Observable;
import org.apache.commons.scxml2.model.ModelException;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class MObCustomerCreateCommand implements ICommand<MObCustomerCreateValueObject> {

  private MasterDataCodeGenrator masterDataCodeGenerator;
  private CObPurchasingUnitRep purchasingUnitRep;
  private MObCustomerRep customerRep;
  private CObCurrencyRep currencyRep;
  private LObIssueFromRep issueFromRep;
  private LObTaxAdministrativeRep taxAdministrativeRep;

  private CommandUtils commandUtils;
  private MObCustomerStateMachine stateMachine;

  public MObCustomerCreateCommand() throws ModelException {
    commandUtils = new CommandUtils();
    stateMachine = new MObCustomerStateMachine();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  @Override
  public Observable<String> executeCommand(MObCustomerCreateValueObject valueObject)
      throws Exception {
    String userCode = masterDataCodeGenerator.generateUserCode(MObCustomer.class);
    MObCustomer customer = new MObCustomer();
    commandUtils.setCreationAndModificationInfoAndDate(customer);
    setCustomerGeneralData(valueObject, userCode, customer);
    setCustomerBusinessUnitsData(valueObject, customer);
    setCustomerLegalRegistrationData(valueObject, customer);
    customerRep.create(customer);
    return Observable.just(userCode);
  }

  private void setCustomerLegalRegistrationData(
      MObCustomerCreateValueObject valueObject, MObCustomer customer) throws Exception {
    IObCustomerLegalRegistrationData legalRegistrationData = new IObCustomerLegalRegistrationData();
    commandUtils.setCreationAndModificationInfoAndDate(legalRegistrationData);
    legalRegistrationData.setRegistrationNumber(valueObject.getRegistrationNumber());
    setIssueFromId(legalRegistrationData, valueObject.getIssuedFromCode());
    legalRegistrationData.setTaxCardNumber(valueObject.getTaxCardNumber());
    setTaxAdministrativeId(legalRegistrationData, valueObject.getTaxAdministrationCode());
    legalRegistrationData.setTaxFileNumber(valueObject.getTaxFileNumber());
    customer.setSingleDetail(IMObCustomer.legalRegistrationDataAttr, legalRegistrationData);
  }

  private void setCustomerGeneralData(
      MObCustomerCreateValueObject valueObject, String userCode, MObCustomer customer)
      throws Exception {
    customer.setUserCode(userCode);
    setLegalName(valueObject, customer);
    customer.setCreditLimit(valueObject.getCreditLimit());
    customer.setCurrencyId(getCurrencyIdByIso(valueObject.getCurrencyIso()));
    setMarketName(valueObject, customer);
    customer.setOldCustomerNumber(valueObject.getOldCustomerNumber());
    stateMachine.initObjectState(customer);
  }

  private Long getCurrencyIdByIso(String currencyIso) {
    return currencyRep.findOneByIso(currencyIso).getId();
  }

  private void setIssueFromId(
      IObCustomerLegalRegistrationData legalRegistrationData, String issuedFromCode) {
    Long issueFromId = issueFromRep.findOneByUserCode(issuedFromCode).getId();
    legalRegistrationData.setIssueFromId(issueFromId);
  }

  private void setTaxAdministrativeId(
      IObCustomerLegalRegistrationData legalRegistrationData, String taxAdministrativeCode) {
    Long taxAdministrativeId =
        taxAdministrativeRep.findOneByUserCode(taxAdministrativeCode).getId();
    legalRegistrationData.setTaxAdministrativeId(taxAdministrativeId);
  }

  private void setCustomerBusinessUnitsData(
     MObCustomerCreateValueObject valueObject,  MObCustomer customer) throws Exception {
    List<IObCustomerBusinessUnit> businessUnits = new LinkedList<>();
    for (String businessUnit : valueObject.getBusinessUnitCodes()) {
      IObCustomerBusinessUnit customerBusinessUnit = new IObCustomerBusinessUnit();
      CObPurchasingUnit purchasingUnit = getPurchaseUnit(businessUnit);
      customerBusinessUnit.setBusinessUnit(purchasingUnit.getId());
      commandUtils.setCreationAndModificationInfoAndDate(customerBusinessUnit);
      businessUnits.add(customerBusinessUnit);
    }
    customer.setAllMultipleDetails(IMObCustomer.purchaseUnitAttr, businessUnits);
  }

  private CObPurchasingUnit getPurchaseUnit(String businessUnitCode) {
    return purchasingUnitRep.findOneByUserCode(businessUnitCode);
  }

  private void setMarketName(
      MObCustomerCreateValueObject valueObject, MObCustomer customer) {
    LocalizedString customerMarketName = new LocalizedString();
    customerMarketName.setValue(new Locale("en"), valueObject.getMarketName());
    customerMarketName.setValue(new Locale("ar"), valueObject.getMarketName());
    customer.setMarketName(customerMarketName);
  }

  private void setLegalName(
      MObCustomerCreateValueObject valueObject, MObCustomer customer) {
    LocalizedString customerName = new LocalizedString();
    customerName.setValue(new Locale("en"), valueObject.getLegalName());
    customerName.setValue(new Locale("ar"), valueObject.getLegalName());
    customer.setName(customerName);
  }

  public void setMasterDataCodeGenerator(MasterDataCodeGenrator masterDataCodeGenerator) {
    this.masterDataCodeGenerator = masterDataCodeGenerator;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setIssueFromRep(LObIssueFromRep issueFromRep) {
    this.issueFromRep = issueFromRep;
  }

  public void setTaxAdministrativeRep(LObTaxAdministrativeRep taxAdministrativeRep) {
    this.taxAdministrativeRep = taxAdministrativeRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }
}
