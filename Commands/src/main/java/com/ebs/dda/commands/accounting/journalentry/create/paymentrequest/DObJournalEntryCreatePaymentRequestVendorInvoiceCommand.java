package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemVendorSubAccount;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetails;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public abstract class DObJournalEntryCreatePaymentRequestVendorInvoiceCommand
        extends DObJournalEntryCreatePaymentRequestCommand {

    protected IObAccountingDocumentActivationDetailsGeneralModel invoicePostingDetailsGeneralModel;
    protected IObPaymentRequestInvoicePaymentDetails paymentInvoiceDetails;

    public DObJournalEntryCreatePaymentRequestVendorInvoiceCommand() {
    }
    @Override
    public Observable<String> executeCommand(
            DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
        executeParentCommand(paymentRequestValueObject);
        initVendorInvoiceEntities();
        return Observable.just("SUCCESS");
    }
    private void initVendorInvoiceEntities() {
        paymentInvoiceDetails =
                (IObPaymentRequestInvoicePaymentDetails)
                        paymentRequestPaymentDetailsRep.findOneByRefInstanceId(paymentRequest.getId());
        Long invoiceId = paymentInvoiceDetails.getDueDocumentId();
        invoicePostingDetailsGeneralModel =
                accountingDocumentActivationDetailsGeneralModelRep.findOneByRefInstanceIdAndObjectTypeCode(invoiceId, IDocumentTypes.VENDOR_INVOICE);
    }
    protected BigDecimal getInvoiceCurrencyPrice() {
        return invoicePostingDetailsGeneralModel.getCurrencyPrice();
    }

    protected void setJournalItemVendorInvoiceData(List<IObJournalEntryItem> journalEntryItems) {
        IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount =
                new IObJournalEntryItemVendorSubAccount();
        setAccountAndSubAccountForVendorType(journalEntryItemVendorSubAccount);
        setJournalItemDetailsForVendorInvoiceType(journalEntryItemVendorSubAccount);
        commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemVendorSubAccount);
        journalEntryItems.add(journalEntryItemVendorSubAccount);
    }

    private void setAccountAndSubAccountForVendorType(
            IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount) {
        Long accountId = paymentRequestAccountingDetailsDebit.getGlAccountId();
        Long subAccountId = paymentRequestAccountingDetailsDebit.getGlSubAccountId();
        String subLedger = paymentRequestAccountingDetailsDebit.getSubLedger();
        journalEntryItemVendorSubAccount.setAccountId(accountId);
        journalEntryItemVendorSubAccount.setSubAccountId(subAccountId);
        journalEntryItemVendorSubAccount.setType(subLedger);
    }

    private void setJournalItemDetailsForVendorInvoiceType(
            IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount) {
        Long currencyId = paymentInvoiceDetails.getCurrencyId();
        Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
        BigDecimal invoiceCurrencyPrice = paymentPostingDetails.getCurrencyPrice();
        Long invoiceExchangeRateId = paymentPostingDetails.getExchangeRateId();
        journalEntryItemVendorSubAccount.setCredit(BigDecimal.ZERO);
        journalEntryItemVendorSubAccount.setDebit(paymentInvoiceDetails.getNetAmount());
        journalEntryItemVendorSubAccount.setDocumentCurrencyId(currencyId);
        journalEntryItemVendorSubAccount.setCompanyCurrencyId(companyCurrencyId);
        journalEntryItemVendorSubAccount.setCompanyCurrencyPrice(invoiceCurrencyPrice);
        journalEntryItemVendorSubAccount.setCompanyExchangeRateId(invoiceExchangeRateId);
    }
}
