package com.ebs.dda.commands.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingActivationDetails;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.journalentry.DObJournalEntryGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public class DObCostingSetActivationDetailsCommand implements ICommand<String> {

  private final CommandUtils commandUtils;
  private DObCostingRep costingRep;
  private DObJournalEntryGeneralModelRep journalEntryGeneralModelRep;
  private IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep;

  public DObCostingSetActivationDetailsCommand() {
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(String journalEntryCode) throws Exception {

    DObJournalEntryGeneralModel journalEntry =
        journalEntryGeneralModelRep.findOneByUserCode(journalEntryCode);

    IObJournalEntryItemGeneralModel journalEntryItem =
        journalEntryItemsGeneralModelRep.findFirstByCode(journalEntryCode);

    DObCosting costing = costingRep.findOneByUserCode(journalEntry.getDocumentReferenceCode());

    IObCostingActivationDetails activationDetails = new IObCostingActivationDetails();
    commandUtils.setCreationAndModificationInfoAndDate(activationDetails);

    activationDetails.setRefInstanceId(costing.getId());
    activationDetails.setAccountant(commandUtils.getLoggedInUser());
    activationDetails.setActivationDate(DateTime.now());
    activationDetails.setDueDate(journalEntry.getDueDate());
    activationDetails.setExchangeRateId(journalEntryItem.getCompanyExchangeRateId());
    activationDetails.setCurrencyPrice(new BigDecimal(journalEntryItem.getCompanyCurrencyPrice()));
    activationDetails.setFiscalPeriodId(journalEntry.getFiscalPeriodId());

    commandUtils.setModificationInfoAndModificationDate(costing);
    costing.setHeaderDetail(IDObCosting.accountingActivationDetailsAttr, activationDetails);
    costingRep.update(costing);

    return Observable.empty();
  }

  public void setCostingRep(DObCostingRep costingRep) {
    this.costingRep = costingRep;
  }

  public void setJournalEntryGeneralModelRep(
      DObJournalEntryGeneralModelRep journalEntryGeneralModelRep) {
    this.journalEntryGeneralModelRep = journalEntryGeneralModelRep;
  }

  public void setJournalEntryItemsGeneralModelRep(
      IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep) {
    this.journalEntryItemsGeneralModelRep = journalEntryItemsGeneralModelRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
