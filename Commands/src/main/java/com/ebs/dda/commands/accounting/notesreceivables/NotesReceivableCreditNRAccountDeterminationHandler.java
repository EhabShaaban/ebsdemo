package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentNotesReceivableAccountingDetails;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;

import static com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig.NOTES_RECEIVABLES_GL_ACCOUNT;

public class NotesReceivableCreditNRAccountDeterminationHandler
    extends NotesReceivableCreditAccountDeterminationHandler {

  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;

  public void setCreditAccountingDetailRecord(
      IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel,
      DObMonetaryNotes notesReceivable,
      IObMonetaryNotesDetailsGeneralModel notesDetails) {

    IObAccountingDocumentNotesReceivableAccountingDetails creditAccountingDetail =
        createCreditAccountingDetail(referenceDocumentGeneralModel);

    commandUtils.setCreationAndModificationInfoAndDate(creditAccountingDetail);
    notesReceivable.addLineDetail(
        IDObAccountingDocument.accountingNotesReceivableDetailsAttr, creditAccountingDetail);
  }

  private IObAccountingDocumentNotesReceivableAccountingDetails createCreditAccountingDetail(
      IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel) {
    IObAccountingDocumentNotesReceivableAccountingDetails accountingDetails =
        new IObAccountingDocumentNotesReceivableAccountingDetails();

    accountingDetails.setObjectTypeCode(IDocumentTypes.NOTES_RECEIVABLE);
    accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
    accountingDetails.setAmount(referenceDocumentGeneralModel.getAmountToCollect());

    LObGlobalGLAccountConfigGeneralModel globalGLAccount =
        globalAccountConfigGeneralModelRep.findOneByUserCode(NOTES_RECEIVABLES_GL_ACCOUNT);
    accountingDetails.setGlAccountId(globalGLAccount.getGlAccountId());

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(
            referenceDocumentGeneralModel.getRefDocumentCode());
    accountingDetails.setGlSubAccountId(notesReceivablesGeneralModel.getId());

    return accountingDetails;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }
}
