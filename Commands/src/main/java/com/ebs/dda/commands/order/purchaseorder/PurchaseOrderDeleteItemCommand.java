package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;

public class PurchaseOrderDeleteItemCommand {

  private final CommandUtils commandUtils;
  private IObOrderLineDetailsGeneralModelRep itemOrderGeneralModelRep;
  private IObOrderLineDetailsRep itemOrderRep;
  private DObOrderDocumentRep orderDocumentRep;

  public PurchaseOrderDeleteItemCommand() {
    commandUtils = new CommandUtils();
  }

  public void setItemOrderGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep itemOrderGeneralModelRep) {
    this.itemOrderGeneralModelRep = itemOrderGeneralModelRep;
  }

  public void setItemOrderRep(IObOrderLineDetailsRep itemOrderRep) {
    this.itemOrderRep = itemOrderRep;
  }

  public void executeCommand(String purchaseOrderCode, String itemCode) throws Exception {
    IObOrderLineDetails itemOrder = readEntityByPOAndItemCode(purchaseOrderCode, itemCode);
    itemOrderRep.delete(itemOrder);

    DObOrderDocument purchaseOrder = orderDocumentRep.findOneByUserCode(purchaseOrderCode);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    orderDocumentRep.update(purchaseOrder);
  }

  private IObOrderLineDetails readEntityByPOAndItemCode(String purchaseOrderCode, String itemCode)
      throws RecordOfInstanceNotExistException {
    IObOrderLineDetailsGeneralModel itemOrderGeneralModel =
        itemOrderGeneralModelRep.findByOrderCodeAndItemCode(purchaseOrderCode, itemCode);
    if (itemOrderGeneralModel != null) {
      return itemOrderRep.findOneById(itemOrderGeneralModel.getId());
    } else {
      throw new RecordOfInstanceNotExistException(IObOrderLineDetailsQuantities.class.getName());
    }
  }

  public void setOrderDocumentRep(DObOrderDocumentRep orderDocumentRep) {
    this.orderDocumentRep = orderDocumentRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
