package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestActionNames;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class DObPaymentRequestActivationCommand implements ICommand<DObPaymentRequestActivateValueObject> {

  private static final String ACTIVE_STATE = "Active";
  private DObPaymentRequestRep paymentRequestRep;
  private CommandUtils commandUtils;
  private CObExchangeRateRep cObExchangeRateRep;
  private DObPaymentRequestActivatePreparationGeneralModelRep paymentActivatePreparationGMRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObPaymentRequestActivationCommand() {
    commandUtils = new CommandUtils();
  }


  @Override
  public Observable<String> executeCommand(DObPaymentRequestActivateValueObject valueObject)
      throws Exception {
    DObPaymentRequest paymentRequest =
        paymentRequestRep.findOneByUserCode(valueObject.getPaymentRequestCode());

    DObPaymentRequestActivatePreparationGeneralModel preparationGM = paymentActivatePreparationGMRep
      .findOneByPaymentRequestCode(valueObject.getPaymentRequestCode());

    IObPaymentRequestPostingDetails postingDetails =
                createPostingDetails(paymentRequest, preparationGM, valueObject.getDueDate());

    updatePaymentRequest(paymentRequest, postingDetails);

    return Observable.just(paymentRequest.getUserCode());
  }

  private IObPaymentRequestPostingDetails createPostingDetails(
    DObPaymentRequest paymentRequest,
    DObPaymentRequestActivatePreparationGeneralModel preparationGM,
    String dueDate) {

    IObPaymentRequestPostingDetails postingDetails = new IObPaymentRequestPostingDetails();

    String loggedInUser = commandUtils.getLoggedInUser();
    Long exchangeRateId = getExchangeRateIdByCode(preparationGM.getExchangeRate());

    postingDetails.setAccountant(loggedInUser);
    postingDetails.setCurrencyPrice((preparationGM.getCurrencyPrice()));
    postingDetails.setExchangeRateId(exchangeRateId);
    postingDetails.setDueDate(commandUtils.getDateTime(dueDate));
    postingDetails.setActivationDate(new DateTime());
    postingDetails.setRefInstanceId(paymentRequest.getId());
    postingDetails.setFiscalPeriodId(getActiveFiscalPeriodId(dueDate));

    commandUtils.setCreationAndModificationInfoAndDate(postingDetails);

    return postingDetails;
  }

  private Long getActiveFiscalPeriodId(String journalEntryDate) {
    DateTime journalDate = getDateTime(journalEntryDate);
    List<CObFiscalYear> fiscalYears =
            fiscalYearRep.findByCurrentStatesLike("%" + ACTIVE_STATE + "%");
    for (CObFiscalYear fiscalYear : fiscalYears) {
      if (journalDate.isAfter(fiscalYear.getFromDate())
              && journalDate.isBefore(fiscalYear.getToDate())) {
        return fiscalYear.getId();
      }
    }
    return null;
  }
  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }
  private Long getExchangeRateIdByCode(String exchangeRateCode) {
    CObExchangeRate cObExchangeRate = cObExchangeRateRep.findOneByUserCode(exchangeRateCode);
    return cObExchangeRate.getId();
  }

  private void updatePaymentRequest(
      DObPaymentRequest paymentRequest,
      IObPaymentRequestPostingDetails postingDetails)
      throws Exception {
    this.commandUtils.setModificationInfoAndModificationDate(paymentRequest);
    updatePaymentRequestState(getPaymentRequestStateMachine(paymentRequest));
    paymentRequest.setHeaderDetail(IDObPaymentRequest.postingDetailsDeAttr, postingDetails);
    paymentRequestRep.updateAndClear(paymentRequest);
  }

  private void updatePaymentRequestState(DObPaymentRequestStateMachine paymentRequestStateMachine) {
    if (paymentRequestStateMachine.canFireEvent(IDObPaymentRequestActionNames.ACTIVATE)) {
      paymentRequestStateMachine.fireEvent(IDObPaymentRequestActionNames.ACTIVATE);
      paymentRequestStateMachine.save();
    }
  }

  private DObPaymentRequestStateMachine getPaymentRequestStateMachine(
      DObPaymentRequest paymentRequest) throws Exception {
    DObPaymentRequestStateMachine paymentRequestStateMachine = new DObPaymentRequestStateMachine();
    paymentRequestStateMachine.initObjectState(paymentRequest);
    return paymentRequestStateMachine;
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }
  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setcObExchangeRateRep(CObExchangeRateRep cObExchangeRateRep) {
    this.cObExchangeRateRep = cObExchangeRateRep;
  }

  public void setPaymentActivatePreparationGMRep(
    DObPaymentRequestActivatePreparationGeneralModelRep paymentActivatePreparationGMRep) {
    this.paymentActivatePreparationGMRep = paymentActivatePreparationGMRep;
  }
  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

}
