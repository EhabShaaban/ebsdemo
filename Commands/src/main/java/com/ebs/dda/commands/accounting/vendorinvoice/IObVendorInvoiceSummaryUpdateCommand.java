package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsDownPaymentRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

public class IObVendorInvoiceSummaryUpdateCommand implements ICommand<String> {
  private CommandUtils commandUtils;

  private IObVendorInvoiceDetailsDownPaymentRep VendorInvoiceDetailsDownPaymentRep;
  private IObPaymentRequestInvoicePaymentDetailsRep paymentRequestInvoicePaymentDetailsRep;
  private IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;
  private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  private DObVendorInvoiceRep vendorInvoiceRep;
  private IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep;

  private IObVendorInvoiceSummary vendorInvoiceSummary;
  private IObInvoiceSummaryGeneralModel invoiceSummaryGM;
  private IUserAccountSecurityService userAccountSecurityService;

  public IObVendorInvoiceSummaryUpdateCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(String vendorInvoiceCode) throws Exception {
    BigDecimal totalDownPayment = BigDecimal.ZERO;
    BigDecimal totalInstallments = BigDecimal.ZERO;
    DObVendorInvoice vendorInvoice = vendorInvoiceRep.findOneByUserCode(vendorInvoiceCode);
    IObVendorInvoiceDetails vendorInvoiceDetails = vendorInvoiceDetailsRep.findOneByRefInstanceId(vendorInvoice.getId());
    vendorInvoiceSummary = vendorInvoiceSummaryRep.findOneByRefInstanceId(vendorInvoice.getId());
    invoiceSummaryGM = invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(vendorInvoice.getUserCode(), IDocumentTypes.VENDOR_INVOICE);

    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    commandUtils.setModificationInfoAndModificationDate(vendorInvoiceSummary);
    commandUtils.setModificationInfoAndModificationDate(vendorInvoice);

    List<IObVendorInvoiceDetailsDownPayment> vendorInvoiceDetailsDownPayments =
        VendorInvoiceDetailsDownPaymentRep.findByRefInstanceId(vendorInvoiceDetails.getId());
    if (vendorInvoiceDetailsDownPayments != null) {
      totalDownPayment =
          vendorInvoiceDetailsDownPayments.stream().map(downpayment -> downpayment.getDownPaymentAmount())
                  .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    List<IObPaymentRequestInvoicePaymentDetails> paymentRequestInvoicePaymentDetails =
        paymentRequestInvoicePaymentDetailsRep.findOneByDueDocumentId(vendorInvoice.getId());
    if (paymentRequestInvoicePaymentDetails != null) {
      totalInstallments = paymentRequestInvoicePaymentDetails
        .stream()
        .map(downpayment -> downpayment.getNetAmount())
        .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    BigDecimal totalRemaining = new BigDecimal(invoiceSummaryGM.getTotalAmountBeforeTaxes())
                    .add(new BigDecimal(invoiceSummaryGM.getTaxAmount()))
                    .subtract(totalDownPayment)
                    .subtract(totalInstallments);
    vendorInvoiceSummary.setRemaining(totalRemaining);
    vendorInvoiceSummary.setDownPayment(totalDownPayment);
    vendorInvoice.setHeaderDetail(IDObVendorInvoice.summaryAttr, vendorInvoiceSummary);
    vendorInvoiceRep.update(vendorInvoice);
    return Observable.just("SUCCESS");
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setVendorInvoiceDetailsDownPaymentRep(IObVendorInvoiceDetailsDownPaymentRep vendorInvoiceDetailsDownPaymentRep) {
    VendorInvoiceDetailsDownPaymentRep = vendorInvoiceDetailsDownPaymentRep;
  }

  public void setPaymentRequestInvoicePaymentDetailsRep(IObPaymentRequestInvoicePaymentDetailsRep paymentRequestInvoicePaymentDetailsRep) {
    this.paymentRequestInvoicePaymentDetailsRep = paymentRequestInvoicePaymentDetailsRep;
  }

  public void setVendorInvoiceSummaryRep(IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep) {
    this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
  }

  public void setVendorInvoiceRep(DObVendorInvoiceRep vendorInvoiceRep) {
    this.vendorInvoiceRep = vendorInvoiceRep;
  }

  public void setVendorInvoiceDetailsRep(IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep) {
    this.vendorInvoiceDetailsRep = vendorInvoiceDetailsRep;
  }

  public void setInvoiceSummaryGeneralModelRep(IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep) {
    this.invoiceSummaryGeneralModelRep = invoiceSummaryGeneralModelRep;
  }
}
