package com.ebs.dda.commands.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfo;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorAccountingInfoValueObject;
import com.ebs.dda.masterdata.vendor.IMObVendorSectionNames;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class MObVendorAccountingDetailsEditCommand
    implements ICommand<MObVendorAccountingInfoValueObject> {

  private EntityLockCommand<MObVendor> entityLockCommand;
  private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
  private MObVendorRep vendorRep;
  private IObVendorAccountingInfoRep vendorAccountingInfoRep;
  private CommandUtils commandUtils;

  public MObVendorAccountingDetailsEditCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setChartOfAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
    this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setVendorAccountingInfoRep(IObVendorAccountingInfoRep vendorAccountingInfoRep) {
    this.vendorAccountingInfoRep = vendorAccountingInfoRep;
  }

  public LockDetails lock(String resourceCode) throws Exception {
    LockDetails lockDetails =
        entityLockCommand.lockSection(
            resourceCode, IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);
    return lockDetails;
  }

  public void unlockSection(String vendorCode) throws Exception {
    entityLockCommand.unlockSection(vendorCode, IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);
  }

  @Override
  public Observable<String> executeCommand(MObVendorAccountingInfoValueObject valueObject)
      throws Exception {
    MObVendor vendor = vendorRep.findOneByUserCode(valueObject.getVendorCode());
    HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel =
        chartOfAccountsGeneralModelRep.findOneByUserCode(valueObject.getAccountCode());
    setVendorAccountingInfo(vendor, chartOfAccountsGeneralModel.getId());
    this.commandUtils.setModificationInfoAndModificationDate(vendor);
    vendorRep.update(vendor);
    return Observable.just("SUCCESS");
  }

  private void setVendorAccountingInfo(MObVendor vendor, Long accountId) throws Exception {
    IObVendorAccountingInfo vendorAccountingInfo =
        vendorAccountingInfoRep.findByRefInstanceId(vendor.getId());
    if (vendorAccountingInfo == null) {
      vendorAccountingInfo = new IObVendorAccountingInfo();
      this.commandUtils.setCreationAndModificationInfoAndDate(vendorAccountingInfo);
    }
    prepareVendorAccountingInfo(vendor.getId(), accountId, vendorAccountingInfo);
    this.commandUtils.setModificationInfoAndModificationDate(vendorAccountingInfo);
    vendor.setSingleDetail(IMObVendor.accountingDetailsAttr, vendorAccountingInfo);
  }

  private void prepareVendorAccountingInfo(
      Long vendorId, Long accountId, IObVendorAccountingInfo vendorAccountingInfo) {
    vendorAccountingInfo.setRefInstanceId(vendorId);
    vendorAccountingInfo.setChartOfAccountId(accountId);
  }
}
