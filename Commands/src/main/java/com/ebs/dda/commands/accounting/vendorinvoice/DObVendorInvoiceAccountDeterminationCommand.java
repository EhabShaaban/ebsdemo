package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentOrderAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentVendorAccountingDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class DObVendorInvoiceAccountDeterminationCommand implements ICommand<DObVendorInvoiceActivateValueObject> {

    private DObVendorInvoiceRep vendorInvoiceRep;
    private DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep;
    private DObVendorInvoiceItemAccountsPreparationGeneralModelRep itemAccountsPreparationGeneralModelRep;
    private final CommandUtils commandUtils;

    public DObVendorInvoiceAccountDeterminationCommand() {
        commandUtils = new CommandUtils();
    }

    @Override
    public Observable<String> executeCommand(DObVendorInvoiceActivateValueObject valueObject) throws Exception {
        String vendorInvoiceCode = valueObject.getVendorInvoiceCode();
       DObVendorInvoice vendorInvoice = vendorInvoiceRep.findOneByUserCode(vendorInvoiceCode);
        determineCreditAccount(vendorInvoice);
        determineDebitAccount(vendorInvoice);
        vendorInvoiceRep.update(vendorInvoice);
        return Observable.just(vendorInvoiceCode);
    }

    private void determineDebitAccount(DObVendorInvoice vendorInvoice) {
        List<DObVendorInvoiceItemAccountsPreparationGeneralModel> itemAccountsPreparationGeneralModel = itemAccountsPreparationGeneralModelRep.findAllByCode(vendorInvoice.getUserCode());
        Map<Long, List<DObVendorInvoiceItemAccountsPreparationGeneralModel>> groupedItemsMap = itemAccountsPreparationGeneralModel.stream()
                .collect(Collectors.groupingBy(DObVendorInvoiceItemAccountsPreparationGeneralModel::getGlAccountId));
        for (Map.Entry<Long, List<DObVendorInvoiceItemAccountsPreparationGeneralModel>> entry : groupedItemsMap.entrySet()) {
            IObAccountingDocumentOrderAccountingDetails accountingDetails = new IObAccountingDocumentOrderAccountingDetails();
            accountingDetails.setGlAccountId(entry.getKey());
            accountingDetails.setObjectTypeCode(IDocumentTypes.VENDOR_INVOICE);
            accountingDetails.setAccountingEntry(AccountingEntry.DEBIT.name());
            BigDecimal amount = getGroupedItemsAmount(vendorInvoice.getUserCode(), entry.getValue());
            accountingDetails.setAmount(amount);
            Long orderId = getOrderId(entry.getValue());
            accountingDetails.setGlSubAccountId(orderId);
            commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);
            vendorInvoice.addLineDetail(IDObAccountingDocument.accountingOrderDetailsAttr, accountingDetails);
        }
        }

    protected abstract Long getOrderId(List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems);

    protected abstract BigDecimal getGroupedItemsAmount(String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems);

    private void determineCreditAccount(DObVendorInvoice vendorInvoice) {
        DObVendorInvoiceJournalDetailPreparationGeneralModel creditPreparationGeneralModel = journalDetailPreparationGeneralModelٌRep.findOneByCode(vendorInvoice.getUserCode());
        IObAccountingDocumentVendorAccountingDetails accountingDetails = new IObAccountingDocumentVendorAccountingDetails();
        accountingDetails.setGlAccountId(creditPreparationGeneralModel.getGlAccountId());
        accountingDetails.setAccountingEntry(AccountingEntry.CREDIT.name());
        accountingDetails.setAmount(creditPreparationGeneralModel.getAmount());
        accountingDetails.setGlSubAccountId(creditPreparationGeneralModel.getGlSubAccountId());
        accountingDetails.setObjectTypeCode(IDocumentTypes.VENDOR_INVOICE);
        commandUtils.setCreationAndModificationInfoAndDate(accountingDetails);
        vendorInvoice.addLineDetail(IDObAccountingDocument.accountingVendorDetailsAttr, accountingDetails);
    }

    public void setJournalDetailPreparationGeneralModelٌRep(DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep) {
        this.journalDetailPreparationGeneralModelٌRep = journalDetailPreparationGeneralModelٌRep;
    }

    public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setVendorInvoiceRep(DObVendorInvoiceRep vendorInvoiceRep) {
        this.vendorInvoiceRep = vendorInvoiceRep;
    }

    public void setItemAccountsPreparationGeneralModelRep(DObVendorInvoiceItemAccountsPreparationGeneralModelRep itemAccountsPreparationGeneralModelRep) {
        this.itemAccountsPreparationGeneralModelRep = itemAccountsPreparationGeneralModelRep;
    }
}
