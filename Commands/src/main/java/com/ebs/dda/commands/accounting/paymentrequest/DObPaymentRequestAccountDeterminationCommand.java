package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestAccountDeterminationValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import io.reactivex.Observable;

public abstract class DObPaymentRequestAccountDeterminationCommand implements ICommand<DObPaymentRequestAccountDeterminationValueObject> {

    private DObPaymentRequestRep paymentRequestRep;
    protected CommandUtils commandUtils;
    protected IObAccountingDocumentAccountingDetailsRep accountingDetailsRep;
    private IObPaymentRequestPaymentDetailsGeneralModelRep
            paymentRequestPaymentDetailsGeneralModelRep;

    public DObPaymentRequestAccountDeterminationCommand() {
        commandUtils = new CommandUtils();
    }


    @Override
    public Observable<String> executeCommand(DObPaymentRequestAccountDeterminationValueObject valueObject)
            throws Exception {

        DObPaymentRequest paymentRequest =
                paymentRequestRep.findOneByUserCode(valueObject.getCode());
        IObPaymentRequestPaymentDetailsGeneralModel paymentDetails =
                paymentRequestPaymentDetailsGeneralModelRep.findOneByRefInstanceId(paymentRequest.getId());

        handlePaymentAccountDetermination(paymentRequest, paymentDetails);

        paymentRequestRep.update(paymentRequest);
        return Observable.just(paymentRequest.getUserCode());
    }

    private void handlePaymentAccountDetermination(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
        determineCreditAccount(paymentRequest, paymentDetails);
        determineDebitAccount(paymentRequest, paymentDetails);
    }

    protected abstract void determineCreditAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails);

    protected abstract void determineDebitAccount(DObPaymentRequest paymentRequest, IObPaymentRequestPaymentDetailsGeneralModel paymentDetails);

    public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
        this.paymentRequestRep = paymentRequestRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setAccountingDetailsRep(
            IObAccountingDocumentAccountingDetailsRep accountingDetailsRep) {
        this.accountingDetailsRep = accountingDetailsRep;
    }

    public void setPaymentRequestPaymentDetailsGeneralModelRep(
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep) {
        this.paymentRequestPaymentDetailsGeneralModelRep = paymentRequestPaymentDetailsGeneralModelRep;
    }

}
