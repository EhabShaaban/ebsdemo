package com.ebs.dda.commands.accounting.notesreceivables;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetails;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class DObNotesReceivableUpdateRemainingCommand
  implements ICommand<DObCollectionUpdateRefDocumentRemainingValueObject> {

  private DObMonetaryNotesRep monetaryNotesRep;
  private IObMonetaryNotesDetailsRep monetaryNotesDetailsRep;
  private DObCollectionGeneralModelRep collectionGMRep;
  private CommandUtils commandUtils;

  public DObNotesReceivableUpdateRemainingCommand() {
    commandUtils = new CommandUtils();
  }

  @Override public Observable<String> executeCommand(
    DObCollectionUpdateRefDocumentRemainingValueObject valueObject) throws Exception {

    String nrCode = valueObject.getRefDocumentCode();
    DObMonetaryNotes notesReceivable = monetaryNotesRep.findOneByUserCode(nrCode);
    IObMonetaryNotesDetails details =
      monetaryNotesDetailsRep.findOneByRefInstanceId(notesReceivable.getId());

    BigDecimal collectionAmount = getCollectionAmount(valueObject.getCollectionCode());
    details.setRemaining(details.getRemaining().subtract(collectionAmount));

    commandUtils.setModificationInfoAndModificationDate(details);
    commandUtils.setModificationInfoAndModificationDate(notesReceivable);
    notesReceivable.setHeaderDetail(IDObMonetaryNotes.detailsAttr, details);
    monetaryNotesRep.update(notesReceivable);

    return Observable.just(nrCode);
  }

  private BigDecimal getCollectionAmount(String collectionCode) {
    DObCollectionGeneralModel collection = collectionGMRep.findOneByUserCode(collectionCode);
    return collection.getAmount();
  }

  public void setMonetaryNotesRep(DObMonetaryNotesRep monetaryNotesRep) {
    this.monetaryNotesRep = monetaryNotesRep;
  }

  public void setMonetaryNotesDetailsRep(IObMonetaryNotesDetailsRep monetaryNotesDetailsRep) {
    this.monetaryNotesDetailsRep = monetaryNotesDetailsRep;
  }

  public void setCollectionGMRep(DObCollectionGeneralModelRep collectionGMRep) {
    this.collectionGMRep = collectionGMRep;
  }

  public void setUserAccountSecurityService(
    IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }


}
