package com.ebs.dda.commands.accounting.collection;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.collection.CObCollectionType;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionDebitNote;
import com.ebs.dda.jpa.accounting.collection.DObCollectionNotesReceivable;
import com.ebs.dda.jpa.accounting.collection.DObCollectionPaymentRequest;
import com.ebs.dda.jpa.accounting.collection.DObCollectionSalesInvoice;
import com.ebs.dda.jpa.accounting.collection.DObCollectionSalesOrder;
import com.ebs.dda.jpa.accounting.collection.IDObCollection;
import com.ebs.dda.jpa.accounting.collection.IObCollectionCompanyDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDebitNoteDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionNotesReceivableDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionPaymentRequestDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionSalesInvoiceDetails;
import com.ebs.dda.jpa.accounting.collection.IObCollectionSalesOrderDetails;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartner;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceCompanyData;
import com.ebs.dda.jpa.masterdata.buisnesspartner.MObBusinessPartner;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.CObCollectionTypeRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceCompanyDataRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import io.reactivex.Observable;
import org.springframework.transaction.annotation.Transactional;

public class DObCollectionCreateCommand implements ICommand<DObCollectionCreateValueObject> {

  public static final String SALES_INVOICE = "SALES_INVOICE";
  public static final String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
  public static final String SALES_ORDER = "SALES_ORDER";
  public static final String DEBIT_NOTE = "DEBIT_NOTE";
  public static final String PAYMENT_REQUEST = "PAYMENT_REQUEST";
  public static final String CUSTOMER = "CUSTOMER";
  public static final String VENDOR = "VENDOR";
  public static final String EMPLOYEE = "EMPLOYEE";
  private DObCollectionStateMachine stateMachine;
  private CommandUtils commandUtils;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObCollectionRep collectionRep;
  private CObPurchasingUnitRep businessUnitRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private IObSalesInvoiceCompanyDataRep salesInvoiceCompanyRep;
  private IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep;
  private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  private IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private CObCurrencyRep currencyRep;
  private CObCompanyRep companyRep;
  private MObCustomerRep customerRep;
  private MObVendorRep vendorRep;
  private MObEmployeeRep employeeRep;
  private CObCollectionTypeRep collectionTypeRep;
  private IObPaymentRequestCompanyDataGeneralModelRep paymentRequestCompanyDataGeneralModelRep;

  public DObCollectionCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    stateMachine = new DObCollectionStateMachine();
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  @Transactional
  public Observable<String> executeCommand(DObCollectionCreateValueObject collectionValueObject)
      throws Exception {
    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObCollection.class.getSimpleName());
    DObCollection collection = createCollectionInstance(collectionValueObject.getCollectionType());
    IObCollectionDetails collectionDetails =
        createCollectionDetailsInstance(collectionValueObject.getCollectionType());
    IObCollectionCompanyDetails companyDetails = new IObCollectionCompanyDetails();

    setCollectionData(collectionValueObject, collection, userCode);
    setCollectionCompanyDetails(collectionValueObject, companyDetails);

    setCollectionDetailsData(collectionValueObject, collectionDetails);
    setRefDocumentData(collectionDetails, collectionValueObject);

    executeTransaction(collection, collectionDetails, companyDetails);
    return Observable.just(userCode);
  }

  private void setRefDocumentData(
      IObCollectionDetails collectionDetails,
      DObCollectionCreateValueObject collectionValueObject) {

    if (isSalesInvoiceRefDocumentType(collectionValueObject)) {
      ((IObCollectionSalesInvoiceDetails) collectionDetails)
          .setSalesInvoiceId(getSalesInvoiceId(collectionValueObject));
    }
    if (isNotesReceivablesRefDocumentType(collectionValueObject)) {
      ((IObCollectionNotesReceivableDetails) collectionDetails)
          .setNotesReceivableId(getNotesReceivableId(collectionValueObject));
    }
    if (isSalesOrderRefDocumentType(collectionValueObject)) {
      ((IObCollectionSalesOrderDetails) collectionDetails)
          .setSalesOrderId(getSalesOrderId(collectionValueObject));
    }
    if (isDebitNoteRefDocumentType(collectionValueObject)) {
      ((IObCollectionDebitNoteDetails) collectionDetails)
          .setDebitNoteId(getDebitNoteId(collectionValueObject));
    }
    if (isPaymentRequestRefDocumentType(collectionValueObject)) {
      ((IObCollectionPaymentRequestDetails) collectionDetails)
          .setPaymentRequestId(getPaymentRequestId(collectionValueObject));
    }
  }

  private DObCollection createCollectionInstance(String collectionType) {
    if (collectionType.equals(SALES_INVOICE)) {
      return new DObCollectionSalesInvoice();
    } else if (collectionType.equals(NOTES_RECEIVABLE)) {
      return new DObCollectionNotesReceivable();
    } else if (collectionType.equals(SALES_ORDER)) {
      return new DObCollectionSalesOrder();
    } else if (collectionType.equals(DEBIT_NOTE)) {
      return new DObCollectionDebitNote();
    } else {
      return new DObCollectionPaymentRequest();
    }
  }

  private void setCollectionCompanyDetails(
      DObCollectionCreateValueObject collectionValueObject,
      IObCollectionCompanyDetails companyDetails) {
    commandUtils.setCreationAndModificationInfoAndDate(companyDetails);
    companyDetails.setPurchaseUnitId(getBusinessUnitId(collectionValueObject));
    companyDetails.setCompanyId(getCompanyId(collectionValueObject));
  }

  private Long getBusinessUnitId(DObCollectionCreateValueObject collectionValueObject) {
    String businessUnitCode = null;
    if (isSalesInvoiceRefDocumentType(collectionValueObject)) {
      businessUnitCode = getSalesInvoiceBusinessUnitCode(collectionValueObject);

    } else if (isNotesReceivablesRefDocumentType(collectionValueObject)) {
      businessUnitCode = getNotesReceivableBusinessUnitCode(collectionValueObject);
    } else if (isSalesOrderRefDocumentType(collectionValueObject)) {
      businessUnitCode = getSalesOrderBusinessUnitCode(collectionValueObject);
    } else if (isDebitNoteRefDocumentType(collectionValueObject)) {
      businessUnitCode = getDebitNoteBusinessUnitCode(collectionValueObject);
    } else if (isPaymentRequestRefDocumentType(collectionValueObject)) {
      businessUnitCode = getPaymentRequestBusinessUnitCode(collectionValueObject);
    }
    if (null != businessUnitCode) {
      return getBusinessUnit(businessUnitCode).getId();
    }
    return null;
  }

  private String getSalesOrderBusinessUnitCode(
      DObCollectionCreateValueObject collectionValueObject) {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return salesOrderGeneralModel.getPurchaseUnitCode();
  }

  private String getDebitNoteBusinessUnitCode(
      DObCollectionCreateValueObject collectionValueObject) {
    DObAccountingNoteGeneralModel accountingNoteGeneralModel =
        accountingNoteGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return accountingNoteGeneralModel.getPurchaseUnitCode();
  }

  private String getPaymentRequestBusinessUnitCode(
      DObCollectionCreateValueObject collectionValueObject) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return paymentRequestGeneralModel.getPurchaseUnitCode();
  }

  private String getNotesReceivableBusinessUnitCode(
      DObCollectionCreateValueObject collectionValueObject) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        getDObNotesReceivablesGeneralModel(collectionValueObject);
    return notesReceivablesGeneralModel.getPurchaseUnitCode();
  }

  private String getSalesInvoiceBusinessUnitCode(
      DObCollectionCreateValueObject collectionValueObject) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        getDObSalesInvoiceGeneralModel(collectionValueObject);
    return salesInvoiceGeneralModel.getPurchaseUnitCode();
  }

  private CObPurchasingUnit getBusinessUnit(String businessUnitCode) {
    return businessUnitRep.findOneByUserCode(businessUnitCode);
  }

  private void setCollectionDetailsData(
      DObCollectionCreateValueObject collectionValueObject,
      IObCollectionDetails collectionDetails) {
    commandUtils.setCreationAndModificationInfoAndDate(collectionDetails);
    collectionDetails.setCollectionMethod(collectionValueObject.getCollectionMethod());
    collectionDetails.setCurrencyId(getCurrencyId(collectionValueObject));
    collectionDetails.setRefDocumentTypeId(getRefDocumentTypeID(collectionValueObject));
    collectionDetails.setBusinessPartnerId(getBusinessPartnerId(collectionValueObject));
  }

  private Long getRefDocumentTypeID(DObCollectionCreateValueObject collectionValueObject) {
    CObCollectionType collectionType =
        collectionTypeRep.findOneByUserCode(collectionValueObject.getCollectionType());
    return collectionType.getId();
  }

  private boolean isSalesInvoiceRefDocumentType(
      DObCollectionCreateValueObject collectionValueObject) {
    return SALES_INVOICE.equals(collectionValueObject.getCollectionType());
  }

  private boolean isNotesReceivablesRefDocumentType(
      DObCollectionCreateValueObject collectionValueObject) {
    return NOTES_RECEIVABLE.equals(collectionValueObject.getCollectionType());
  }

  private boolean isSalesOrderRefDocumentType(
      DObCollectionCreateValueObject collectionValueObject) {
    return SALES_ORDER.equals(collectionValueObject.getCollectionType());
  }

  private boolean isDebitNoteRefDocumentType(DObCollectionCreateValueObject collectionValueObject) {
    return DEBIT_NOTE.equals(collectionValueObject.getCollectionType());
  }

  private boolean isPaymentRequestRefDocumentType(
      DObCollectionCreateValueObject collectionValueObject) {
    return PAYMENT_REQUEST.equals(collectionValueObject.getCollectionType());
  }

  private Long getNotesReceivableId(DObCollectionCreateValueObject collectionValueObject) {
    DObNotesReceivablesGeneralModel notesReceivables =
            notesReceivablesGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return notesReceivables.getId();
  }

  private Long getSalesOrderId(DObCollectionCreateValueObject collectionValueObject) {
    DObSalesOrderGeneralModel salesOrder =
        salesOrderGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return salesOrder.getId();
  }

  private Long getDebitNoteId(DObCollectionCreateValueObject collectionValueObject) {
    DObAccountingNoteGeneralModel creditDebitNoteGeneralModel =
        accountingNoteGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    return creditDebitNoteGeneralModel.getId();
  }

  private Long getPaymentRequestId(DObCollectionCreateValueObject collectionValueObject) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        getPaymentRequestGeneralModel(collectionValueObject);
    return paymentRequestGeneralModel.getId();
  }

  private Long getSalesInvoiceId(DObCollectionCreateValueObject collectionValueObject) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        getDObSalesInvoiceGeneralModel(collectionValueObject);
    return salesInvoiceGeneralModel.getId();
  }

  private void setCollectionData(
      DObCollectionCreateValueObject collectionValueObject,
      DObCollection collection,
      String userCode)
      throws Exception {
    commandUtils.setCreationAndModificationInfoAndDate(collection);
    stateMachine.initObjectState(collection);
    if (collectionValueObject.getCollectionType().equals(DEBIT_NOTE)) {
      collection.setCollectionType(VENDOR);
    } else if (collectionValueObject.getCollectionType().equals(PAYMENT_REQUEST)) {
      collection.setCollectionType(EMPLOYEE);
    } else {
      collection.setCollectionType(CUSTOMER);
    }
    collection.setDocumentOwnerId(collectionValueObject.getDocumentOwnerId());
    collection.setUserCode(userCode);
  }

  private Long getCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    Long companyId = null;

    if (isSalesInvoiceRefDocumentType(collectionValueObject)) {
      companyId = getSalesInvoiceCompanyId(collectionValueObject);
    }

    if (isNotesReceivablesRefDocumentType(collectionValueObject)) {
      companyId = getNotesReceivableCompanyId(collectionValueObject);
    }
    if (isSalesOrderRefDocumentType(collectionValueObject)) {
      companyId = getSalesOrderCompanyId(collectionValueObject);
    }
    if (isDebitNoteRefDocumentType(collectionValueObject)) {
      companyId = getDebitNoteCompanyId(collectionValueObject);
    }
    if (isPaymentRequestRefDocumentType(collectionValueObject)) {
      companyId = getPaymentRequestCompanyId(collectionValueObject);
    }
    return companyId;
  }

  private Long getCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    Long currencyId = null;
    if (isSalesInvoiceRefDocumentType(collectionValueObject)) {
      currencyId = getSalesInvoiceCurrencyId(collectionValueObject);
    }
    if (isNotesReceivablesRefDocumentType(collectionValueObject)) {
      currencyId = getNotesReceivableCurrencyId(collectionValueObject);
    }
    if (isSalesOrderRefDocumentType(collectionValueObject)) {
      currencyId = getSalesOrderCurrencyId(collectionValueObject);
    }
    if (isDebitNoteRefDocumentType(collectionValueObject)) {
      currencyId = getDebitNoteCurrencyId(collectionValueObject);
    }
    if (isPaymentRequestRefDocumentType(collectionValueObject)) {
      currencyId = getPaymentRequestCurrencyId(collectionValueObject);
    }
    return currencyId;
  }

  private Long getBusinessPartnerId(DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode = null;

    if (isSalesInvoiceRefDocumentType(collectionValueObject)) {
      businessPartnerCode = getSalesInvoiceBusinessPartnerCode(collectionValueObject);
    }
    if (isNotesReceivablesRefDocumentType(collectionValueObject)) {
      businessPartnerCode = getNotesReceivableBusinessPartnerCode(collectionValueObject);
    }
    if (isSalesOrderRefDocumentType(collectionValueObject)) {
      businessPartnerCode = getSalesOrderBusinessPartnerCode(collectionValueObject);
    }
    if (isDebitNoteRefDocumentType(collectionValueObject)) {
      businessPartnerCode = getDebitNoteBusinessPartnerCode(collectionValueObject);
    }
    if (isPaymentRequestRefDocumentType(collectionValueObject)) {
      businessPartnerCode = getPaymentBusinessPartnerCode(collectionValueObject);
    }
    MObBusinessPartner businessPartner =
        getBusinessPartnerIdByCode(businessPartnerCode, collectionValueObject.getCollectionType());
    return businessPartner.getId();
  }

  private String getDebitNoteBusinessPartnerCode(
      DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode;
    DObAccountingNoteGeneralModel accountingNoteGeneralModel =
        getAccountingNoteGeneralModel(collectionValueObject);
    businessPartnerCode = accountingNoteGeneralModel.getBusinessPartnerCode();
    return businessPartnerCode;
  }

  private String getPaymentBusinessPartnerCode(
      DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode;
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        getPaymentRequestGeneralModel(collectionValueObject);
    businessPartnerCode = paymentRequestGeneralModel.getBusinessPartnerCode();
    return businessPartnerCode;
  }

  private String getSalesOrderBusinessPartnerCode(
      DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode;
    IObSalesOrderDataGeneralModel salesOrderDataGeneralModel =
        getIObSalesOrderDataGeneralModel(collectionValueObject);
    businessPartnerCode = salesOrderDataGeneralModel.getCustomerCode();
    return businessPartnerCode;
  }

  private String getNotesReceivableBusinessPartnerCode(
      DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode;
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        getDObNotesReceivablesGeneralModel(collectionValueObject);
    businessPartnerCode = notesReceivablesGeneralModel.getBusinessPartnerCode();
    return businessPartnerCode;
  }

  private String getSalesInvoiceBusinessPartnerCode(
      DObCollectionCreateValueObject collectionValueObject) {
    String businessPartnerCode;
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        getDObSalesInvoiceGeneralModel(collectionValueObject);

    businessPartnerCode = salesInvoiceGeneralModel.getCustomerCode();
    return businessPartnerCode;
  }

  private MObBusinessPartner getBusinessPartnerIdByCode(
      String businessPartnerCode, String collectionType) {
    if (collectionType.equals(DEBIT_NOTE)) {
      return vendorRep.findOneByUserCode(businessPartnerCode);
    } else if (collectionType.equals(PAYMENT_REQUEST)){
      return employeeRep.findOneByUserCode(businessPartnerCode);
    }else {
      return customerRep.findOneByUserCode(businessPartnerCode);
    }
  }

  private Long getSalesInvoiceCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    Long currencyId;
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        getDObSalesInvoiceGeneralModel(collectionValueObject);

    IObSalesInvoiceBusinessPartner salesInvoiceBusinessPartner =
        salesInvoiceBusinessPartnerRep.findOneByRefInstanceId(salesInvoiceGeneralModel.getId());
    currencyId = salesInvoiceBusinessPartner.getCurrencyId();
    return currencyId;
  }

  private Long getSalesInvoiceCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    Long companyId;
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        getDObSalesInvoiceGeneralModel(collectionValueObject);
    IObSalesInvoiceCompanyData salesInvoiceCompanyData =
        salesInvoiceCompanyRep.findOneByRefInstanceId(salesInvoiceGeneralModel.getId());
    companyId = salesInvoiceCompanyData.getCompanyId();
    return companyId;
  }

  private Long getNotesReceivableCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        getDObNotesReceivablesGeneralModel(collectionValueObject);

    CObCurrency currency = currencyRep.findOneByIso(notesReceivablesGeneralModel.getCurrencyIso());
    return currency.getId();
  }

  private Long getSalesOrderCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    IObSalesOrderDataGeneralModel salesOrderDataGeneralModel =
        getIObSalesOrderDataGeneralModel(collectionValueObject);

    CObCurrency currency = currencyRep.findOneByIso(salesOrderDataGeneralModel.getCurrencyIso());
    return currency.getId();
  }

  private Long getDebitNoteCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    DObAccountingNoteGeneralModel accountingNoteGeneralModel =
        getAccountingNoteGeneralModel(collectionValueObject);

    CObCurrency currency = currencyRep.findOneByIso(accountingNoteGeneralModel.getCurrencyIso());
    return currency.getId();
  }

  private Long getPaymentRequestCurrencyId(DObCollectionCreateValueObject collectionValueObject) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        getPaymentRequestGeneralModel(collectionValueObject);

    CObCurrency currency = currencyRep.findOneByIso(paymentRequestGeneralModel.getAmountCurrency());
    return currency.getId();
  }

  private Long getNotesReceivableCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        getDObNotesReceivablesGeneralModel(collectionValueObject);
    String codeName = notesReceivablesGeneralModel.getCompanyCodeName();
    String companyCode = codeName.split("-")[0];
    CObCompany company = companyRep.findOneByUserCode(companyCode.trim());
    return company.getId();
  }

  private Long getSalesOrderCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    String companyCode = salesOrderGeneralModel.getCompanyCode();
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private Long getDebitNoteCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    DObAccountingNoteGeneralModel accountingNoteGeneralModel =
        accountingNoteGeneralModelRep.findOneByUserCode(collectionValueObject.getRefDocumentCode());
    String companyCode = accountingNoteGeneralModel.getCompanyCode();
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private Long getPaymentRequestCompanyId(DObCollectionCreateValueObject collectionValueObject) {
    IObPaymentRequestCompanyDataGeneralModel paymentRequestCompanyDataGeneralModel =
        paymentRequestCompanyDataGeneralModelRep.findOneByDocumentCode(
            collectionValueObject.getRefDocumentCode());
    String companyCode = paymentRequestCompanyDataGeneralModel.getCompanyCode();
    CObCompany company = companyRep.findOneByUserCode(companyCode);
    return company.getId();
  }

  private IObCollectionDetails createCollectionDetailsInstance(String documentType) {
    if (documentType.equals(SALES_INVOICE)) {
      return new IObCollectionSalesInvoiceDetails();
    } else if (documentType.equals(NOTES_RECEIVABLE)) {
      return new IObCollectionNotesReceivableDetails();
    } else if (documentType.equals(SALES_ORDER)) {
      return new IObCollectionSalesOrderDetails();
    } else if (documentType.equals(DEBIT_NOTE)) {
      return new IObCollectionDebitNoteDetails();
    } else {
      return new IObCollectionPaymentRequestDetails();
    }
  }

  private void executeTransaction(
      DObCollection collection,
      IObCollectionDetails collectionDetails,
      IObCollectionCompanyDetails companyDetails)
      throws Exception {
    collection.setHeaderDetail(IDObCollection.collectionDetails, collectionDetails);

    collection.setHeaderDetail(IDObCollection.collectionCompanyDetails, companyDetails);
    collectionRep.create(collection);
  }

  private DObNotesReceivablesGeneralModel getDObNotesReceivablesGeneralModel(
      DObCollectionCreateValueObject collectionValueObject) {
    return notesReceivablesGeneralModelRep.findOneByUserCode(
        collectionValueObject.getRefDocumentCode());
  }

  private DObSalesInvoiceGeneralModel getDObSalesInvoiceGeneralModel(
      DObCollectionCreateValueObject collectionValueObject) {
    return salesInvoiceGeneralModelRep.findOneByUserCode(
        collectionValueObject.getRefDocumentCode());
  }

  private IObSalesOrderDataGeneralModel getIObSalesOrderDataGeneralModel(
      DObCollectionCreateValueObject collectionValueObject) {
    return salesOrderDataGeneralModelRep.findOneBySalesOrderCode(
        collectionValueObject.getRefDocumentCode());
  }

  private DObAccountingNoteGeneralModel getAccountingNoteGeneralModel(
      DObCollectionCreateValueObject collectionValueObject) {
    return accountingNoteGeneralModelRep.findOneByUserCode(
        collectionValueObject.getRefDocumentCode());
  }

  private DObPaymentRequestGeneralModel getPaymentRequestGeneralModel(
      DObCollectionCreateValueObject collectionValueObject) {
    return paymentRequestGeneralModelRep.findOneByUserCode(
        collectionValueObject.getRefDocumentCode());
  }

  public void setCollectionRep(DObCollectionRep collectionRep) {
    this.collectionRep = collectionRep;
  }

  public void setBusinessUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.businessUnitRep = purchasingUnitRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setSalesInvoiceCompanyRep(IObSalesInvoiceCompanyDataRep salesInvoiceCompanyRep) {
    this.salesInvoiceCompanyRep = salesInvoiceCompanyRep;
  }

  public void setSalesInvoiceBusinessPartnerRep(
      IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep) {
    this.salesInvoiceBusinessPartnerRep = salesInvoiceBusinessPartnerRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setSalesOrderDataGeneralModelRep(
      IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep) {
    this.salesOrderDataGeneralModelRep = salesOrderDataGeneralModelRep;
  }

  public void setCollectionTypeRep(CObCollectionTypeRep collectionTypeRep) {
    this.collectionTypeRep = collectionTypeRep;
  }

  public void setAccountingNoteGeneralModelRep(
      DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep) {
    this.accountingNoteGeneralModelRep = accountingNoteGeneralModelRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setPaymentRequestGeneralModelRep(
      DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep) {
    this.paymentRequestGeneralModelRep = paymentRequestGeneralModelRep;
  }

  public void setPaymentRequestCompanyDataGeneralModelRep(
      IObPaymentRequestCompanyDataGeneralModelRep paymentRequestCompanyDataGeneralModelRep) {
    this.paymentRequestCompanyDataGeneralModelRep = paymentRequestCompanyDataGeneralModelRep;
  }

  public void setEmployeeRep(MObEmployeeRep employeeRep) {
    this.employeeRep = employeeRep;
  }
}
