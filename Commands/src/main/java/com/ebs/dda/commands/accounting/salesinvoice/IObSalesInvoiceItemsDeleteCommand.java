package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsDeletionValueObject;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class IObSalesInvoiceItemsDeleteCommand
    implements ICommand<IObSalesInvoiceItemsDeletionValueObject> {

  private IObSalesInvoiceItemsRep salesInvoiceItemsRep;
  private DObSalesInvoiceRep salesInvoiceRep;
  private IUserAccountSecurityService userAccountSecurityService;

  @Override
  public Observable<String> executeCommand(
      IObSalesInvoiceItemsDeletionValueObject iObSalesInvoiceItemsDeletionValueObject)
      throws Exception {

    Long invoiceItemId = iObSalesInvoiceItemsDeletionValueObject.getItemId();
    String invoiceCode = iObSalesInvoiceItemsDeletionValueObject.getSalesInvoiceCode();

    salesInvoiceItemsRep.deleteById(invoiceItemId);
    DObSalesInvoice invoice = salesInvoiceRep.findOneByUserCode(invoiceCode);
    setModificationInfo(invoice);
    return Observable.just("SUCCESS");
  }

  public void setSalesInvoiceItemsRep(IObSalesInvoiceItemsRep invoiceItemsRep) {
    this.salesInvoiceItemsRep = invoiceItemsRep;
  }

  public void setSalesInvoiceRep(DObSalesInvoiceRep invoiceRep) {
    this.salesInvoiceRep = invoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  private void setModificationInfo(DObSalesInvoice dObInvoice) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    dObInvoice.setModificationInfo(loggedinUserInfo);
    dObInvoice.setModifiedDate(new DateTime());
    salesInvoiceRep.save(dObInvoice);
  }
}
