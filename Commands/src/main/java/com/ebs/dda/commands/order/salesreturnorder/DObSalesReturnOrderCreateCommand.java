package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.order.salesreturnorder.*;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.order.salesreturnorder.CObSalesReturnOrderTypeRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import io.reactivex.Observable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class DObSalesReturnOrderCreateCommand
    implements ICommand<DObSalesReturnOrderCreateValueObject> {

  private final CommandUtils commandUtils;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObSalesReturnOrderStateMachine stateMachine;
  private DObSalesReturnOrderRep salesReturnOrderRep;
  private CObSalesReturnOrderTypeRep salesReturnOrderTypeRep;

  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private CObCompanyGeneralModelRep companyGeneralModelRep;
  private CObCurrencyRep currencyRep;
  private CObMeasureRep measureRep;
  private IObInvoiceTaxesGeneralModelRep<IObSalesInvoiceTaxesGeneralModel>
      salesInvoiceTaxesGeneralModelRep;
  private IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private MObCustomerGeneralModelRep customerGeneralModelRep;

  public DObSalesReturnOrderCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
      throws Exception {
    stateMachine = new DObSalesReturnOrderStateMachine();
    commandUtils = new CommandUtils();
    this.documentObjectCodeGenerator = documentObjectCodeGenerator;
  }

  @Override
  @Transactional
  public Observable<String> executeCommand(DObSalesReturnOrderCreateValueObject valueObject)
      throws Exception {

    String userCode =
        documentObjectCodeGenerator.generateUserCode(DObSalesReturnOrder.class.getSimpleName());

    DObSalesReturnOrder salesReturnOrder = new DObSalesReturnOrder();
    commandUtils.setCreationAndModificationInfoAndDate(salesReturnOrder);

    setDObSalesReturnOrderData(valueObject, salesReturnOrder, userCode);
    setIObSalesReturnOrderCompanyData(valueObject, salesReturnOrder);
    setIObSalesReturnOrderReturnDetailsData(valueObject, salesReturnOrder);
    setIObSalesReturnOrderItemsData(valueObject, salesReturnOrder);
    setIObSalesReturnOrderTaxesData(valueObject, salesReturnOrder);

    commandUtils.setModificationInfoAndModificationDate(salesReturnOrder);

    salesReturnOrderRep.create(salesReturnOrder);
    return Observable.just(userCode);
  }

  private void setIObSalesReturnOrderCompanyData(
      DObSalesReturnOrderCreateValueObject valueObject, DObSalesReturnOrder salesReturnOrder) {
    IObSalesReturnOrderCompanyData companyData = new IObSalesReturnOrderCompanyData();
    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
    commandUtils.setCreationAndModificationInfoAndDate(companyData);
    companyData.setBusinessUnitId(getBusinessUnitIdFromSalesInvoice(salesInvoice));
    companyData.setCompanyId(getCompanyIdFromSalesInvoice(salesInvoice));
    salesReturnOrder.setHeaderDetail(IDObSalesReturnOrder.companyDataAttr, companyData);
  }

  private void setIObSalesReturnOrderReturnDetailsData(
      DObSalesReturnOrderCreateValueObject valueObject, DObSalesReturnOrder salesReturnOrder) {
    IObSalesReturnOrderDetails returnDetails = new IObSalesReturnOrderDetails();
    commandUtils.setCreationAndModificationInfoAndDate(returnDetails);
    returnDetails.setSalesInvoiceId(getSalesInvoiceIdByCode(valueObject));
    returnDetails.setCustomerId(getCustomerIdFromSalesInvoice(valueObject));
    returnDetails.setCurrencyId(getCurrencyIdFromSalesInvoice(valueObject));
    salesReturnOrder.setHeaderDetail(IDObSalesReturnOrder.returnDetailsAttr, returnDetails);
  }

  private void setIObSalesReturnOrderItemsData(
      DObSalesReturnOrderCreateValueObject valueObject, DObSalesReturnOrder salesReturnOrder) {

    List<IObSalesInvoiceItemsGeneralModel> salesInvoiceItems =
        salesInvoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdAsc(
            valueObject.getSalesInvoiceCode());

    for (IObSalesInvoiceItemsGeneralModel item : salesInvoiceItems) {
      if (isItemQuantityInOrderUnitGreaterThanReturnedQuantityInOrderUnit(item)) {
        addItemToSalesReturnOrder(salesReturnOrder, item);
      }
    }
  }

  private boolean isItemQuantityInOrderUnitGreaterThanReturnedQuantityInOrderUnit(
      IObSalesInvoiceItemsGeneralModel item) {
    return item.getReturnedQuantity().compareTo(item.getQuantityInOrderUnit()) < 0;
  }

  private void addItemToSalesReturnOrder(
      DObSalesReturnOrder salesReturnOrder, IObSalesInvoiceItemsGeneralModel item) {
    IObSalesReturnOrderItem salesReturnOrderItem = new IObSalesReturnOrderItem();
    commandUtils.setCreationAndModificationInfoAndDate(salesReturnOrderItem);
    salesReturnOrderItem.setItemId(getItemIdByCode(item.getItemCode()));
    salesReturnOrderItem.setOrderUnitId(getOrderUnitIdByCode(item.getOrderUnitCode()));
    salesReturnOrderItem.setReturnQuantity(
        item.getQuantityInOrderUnit().subtract(item.getReturnedQuantity()));
    salesReturnOrderItem.setSalesPrice(item.getOrderUnitPrice());
    salesReturnOrder.addLineDetail(IDObSalesReturnOrder.itemsAttr, salesReturnOrderItem);
  }

  private void setIObSalesReturnOrderTaxesData(
      DObSalesReturnOrderCreateValueObject valueObject, DObSalesReturnOrder salesReturnOrder) {

    List<IObSalesInvoiceTaxesGeneralModel> salesInvoiceTaxes =
        salesInvoiceTaxesGeneralModelRep.findByInvoiceCodeOrderByTaxCodeAsc(
            valueObject.getSalesInvoiceCode());
    for (IObSalesInvoiceTaxesGeneralModel salesInvoiceTax : salesInvoiceTaxes) {
      addTaxToSalesReturnOrder(salesReturnOrder, salesInvoiceTax);
    }
  }

  private void addTaxToSalesReturnOrder(
      DObSalesReturnOrder salesReturnOrder, IObSalesInvoiceTaxesGeneralModel salesInvoiceTax) {
    IObSalesReturnOrderTax salesReturnOrderTax = new IObSalesReturnOrderTax();
    commandUtils.setCreationAndModificationInfoAndDate(salesReturnOrderTax);

    salesReturnOrderTax.setCode(salesInvoiceTax.getTaxCode());
    salesReturnOrderTax.setName(salesInvoiceTax.getTaxName());
    salesReturnOrderTax.setPercentage(salesInvoiceTax.getTaxPercentage());

    salesReturnOrder.addLineDetail(IDObSalesReturnOrder.taxesAttr, salesReturnOrderTax);
  }

  private Long getItemIdByCode(String itemCode) {
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    return item.getId();
  }

  private Long getOrderUnitIdByCode(String orderUnitCode) {
    CObMeasure orderUnit = measureRep.findOneByUserCode(orderUnitCode);
    return orderUnit.getId();
  }

  private Long getCurrencyIdFromSalesInvoice(DObSalesReturnOrderCreateValueObject valueObject) {
    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoice =
        salesInvoiceBusinessPartnerGeneralModelRep.findOneByCode(valueObject.getSalesInvoiceCode());
    CObCurrency currency = currencyRep.findOneByUserCode(salesInvoice.getCurrencyCode());
    return currency.getId();
  }

  private Long getCustomerIdFromSalesInvoice(DObSalesReturnOrderCreateValueObject valueObject) {
    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
    MObCustomerGeneralModel customer =
        customerGeneralModelRep.findOneByUserCode(salesInvoice.getCustomerCode());
    return customer.getId();
  }

  private Long getSalesInvoiceIdByCode(DObSalesReturnOrderCreateValueObject valueObject) {
    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
    return salesInvoice.getId();
  }

  private Long getBusinessUnitIdFromSalesInvoice(DObSalesInvoiceGeneralModel salesInvoice) {
    CObPurchasingUnitGeneralModel purchasingUnit =
        purchasingUnitGeneralModelRep.findOneByUserCode(salesInvoice.getPurchaseUnitCode());
    return purchasingUnit.getId();
  }

  private Long getCompanyIdFromSalesInvoice(DObSalesInvoiceGeneralModel salesInvoice) {
    CObCompanyGeneralModel company =
        companyGeneralModelRep.findOneByUserCode(salesInvoice.getCompanyCode());
    return company.getId();
  }

  private void setDObSalesReturnOrderData(
      DObSalesReturnOrderCreateValueObject salesReturnOrderCreateValueObject,
      DObSalesReturnOrder salesReturnOrder,
      String userCode)
      throws Exception {

    stateMachine.initObjectState(salesReturnOrder);
    salesReturnOrder.setUserCode(userCode);
    salesReturnOrder.setSalesReturnOrderTypeId(getTypeId(salesReturnOrderCreateValueObject));
    salesReturnOrder.setDocumentOwnerId(salesReturnOrderCreateValueObject.getDocumentOwnerId());
  }

  private Long getTypeId(DObSalesReturnOrderCreateValueObject salesReturnOrderCreateValueObject) {
    String typeCode = salesReturnOrderCreateValueObject.getTypeCode();
    CObSalesReturnOrderType salesReturnOrderType =
        salesReturnOrderTypeRep.findOneByUserCode(typeCode);
    return salesReturnOrderType.getId();
  }

  public void setSalesReturnOrderTypeRep(CObSalesReturnOrderTypeRep salesReturnOrderTypeRep) {
    this.salesReturnOrderTypeRep = salesReturnOrderTypeRep;
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setCompanyGeneralModelRep(CObCompanyGeneralModelRep companyGeneralModelRep) {
    this.companyGeneralModelRep = companyGeneralModelRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setSalesInvoiceBusinessPartnerGeneralModelRep(
      IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep) {
    this.salesInvoiceBusinessPartnerGeneralModelRep = salesInvoiceBusinessPartnerGeneralModelRep;
  }

  public void setCustomerGeneralModelRep(MObCustomerGeneralModelRep customerGeneralModelRep) {
    this.customerGeneralModelRep = customerGeneralModelRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setSalesInvoiceItemsGeneralModelRep(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
    this.salesInvoiceItemsGeneralModelRep = salesInvoiceItemsGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setSalesInvoiceTaxesGeneralModelRep(
      IObInvoiceTaxesGeneralModelRep<IObSalesInvoiceTaxesGeneralModel>
          salesInvoiceTaxesGeneralModelRep) {
    this.salesInvoiceTaxesGeneralModelRep = salesInvoiceTaxesGeneralModelRep;
  }
}
