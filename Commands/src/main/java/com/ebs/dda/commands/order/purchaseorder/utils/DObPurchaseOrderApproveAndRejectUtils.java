package com.ebs.dda.commands.order.purchaseorder.utils;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObControlPointRep;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApproverRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsSummaryGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderNewStateMachineFactory;
import org.joda.time.DateTime;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

public class DObPurchaseOrderApproveAndRejectUtils {

  protected EntityLockCommand<DObOrderDocument> entityLockCommand;
  protected IObOrderApprovalCycleRep approvalCycleRep;
  protected IObOrderApproverRep approverRep;
  protected IObOrderLineDetailsSummaryGeneralModelRep orderLineDetailsSummaryGeneralModelRep;
  protected DObPurchaseOrderRep purchaseOrderRep;
  protected CObControlPointRep controlPointRep;
  protected PlatformTransactionManager transactionManager;
  protected TransactionTemplate transactionTemplate;

  private DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory;

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setApprovalCycleRep(IObOrderApprovalCycleRep approvalCycleRep) {
    this.approvalCycleRep = approvalCycleRep;
  }

  public void setApproverRep(IObOrderApproverRep approverRep) {
    this.approverRep = approverRep;
  }

  public void setOrderLineDetailsSummaryGeneralModelRep(
      IObOrderLineDetailsSummaryGeneralModelRep orderLineDetailsSummaryGeneralModelRep) {
    this.orderLineDetailsSummaryGeneralModelRep = orderLineDetailsSummaryGeneralModelRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setControlPointRep(CObControlPointRep controlPointRep) {
    this.controlPointRep = controlPointRep;
  }

  public void setPurchaseOrderFactory(DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory) {
    this.purchaseOrderFactory = purchaseOrderFactory;
  }

  protected void updateCycleDecisionAndEndDate(
      IObOrderApprovalCycle currentUndecidedCycle,
      DateTime dicisionDatetime,
      ApprovalDecision decision) {
    currentUndecidedCycle.setFinalDecision(decision);
    currentUndecidedCycle.setEndDate(dicisionDatetime);
  }

  protected void updateDatabaseAndUnlock(
      IObOrderApprover currentApprover,
      IObOrderApprovalCycle currentUndecidedCycle,
      DObPurchaseOrder purchaseOrder)
      throws Exception {
    try {
      updateDatabase(currentApprover, currentUndecidedCycle, purchaseOrder);
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrder.getUserCode());
    }
  }

  protected void updateDatabase(
      IObOrderApprover currentApprover,
      IObOrderApprovalCycle currentUndecidedCycle,
      DObPurchaseOrder purchaseOrder) {
    transactionTemplate = new TransactionTemplate(transactionManager);
    transactionTemplate.execute(
        new TransactionCallbackWithoutResult() {
          @Override
          protected void doInTransactionWithoutResult(TransactionStatus status) {
            try {
              approverRep.update(currentApprover);
              approvalCycleRep.update(currentUndecidedCycle);
              purchaseOrderRep.update(purchaseOrder);
            } catch (Exception e) {
              status.setRollbackOnly();
              throw new RuntimeException();
            }
          }
        });
  }

  protected void setPurchaseOrderTargetState(DObPurchaseOrder purchaseOrder, String actionName)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    if (purchaseOrderStateMachine.fireEvent(actionName)) {
      purchaseOrderStateMachine.save();
    }
  }

  protected AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private IObOrderApprover getCurrentApprover(Long approvalCycleId, IBDKUser loggedInUser) {

    List<IObOrderApprover> approversList =
        approverRep.findByRefInstanceIdOrderByIdAsc(approvalCycleId);

    for (IObOrderApprover approver : approversList) {
      if (approver.getDecision() == null) {
        if (approver.getApproverId().equals(loggedInUser.getUserId())) {
          return approver;
        }
      }
    }
    return null;
  }

  protected IObOrderApprover getAndUpdateCurrentApprover(
      String notes,
      IBDKUser loggedInUser,
      Long approvalCycleId,
      ApprovalDecision decision,
      DateTime dicisionDatetime) {
    IObOrderApprover currentApprover = getCurrentApprover(approvalCycleId, loggedInUser);
    currentApprover.setDecision(decision);
    currentApprover.setDecisionDatetime(dicisionDatetime);
    currentApprover.setNotes(notes);
    currentApprover.setModifiedDate(dicisionDatetime);
    currentApprover.setModificationInfo(loggedInUser.getUsername());
    return currentApprover;
  }

  protected IObOrderApprover getControlPointCurrentApprover(
      Long approvalCycleId, Long controlPointId) {

    List<IObOrderApprover> approversList =
        approverRep.findByRefInstanceIdOrderByIdAsc(approvalCycleId);

    for (IObOrderApprover approver : approversList) {
      if (approver.getDecision() == null && approver.getControlPointId().equals(controlPointId)) {
        return approver;
      }
    }
    return null;
  }
}
