package com.ebs.dda.commands.accounting.initialbalanceupload;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.initialbalanceupload.apis.IDObInitialBalanceUploadActionNames;
import com.ebs.dda.accounting.initialbalanceupload.statemachines.DObInitialBalanceUploadStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUpload;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUpload;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IObInitialBalanceUploadActivationDetails;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class DObInitialBalanceUploadActivateCommand implements ICommand<String> {

    private DObInitialBalanceUploadRep initialBalanceUploadRep;
    private CommandUtils commandUtils;
    private IUserAccountSecurityService userAccountSecurityService;
    private static final String ACTIVE_STATE = "Active";

    public DObInitialBalanceUploadActivateCommand(){
        commandUtils = new CommandUtils();
    }
    @Override
    public Observable<String> executeCommand(String initialBalanceUploadCode) throws Exception {

        DObInitialBalanceUpload initialBalanceUpload = initialBalanceUploadRep.findOneByUserCode(initialBalanceUploadCode);
        IObInitialBalanceUploadActivationDetails initialBalanceUploadActivationDetails = createActivationDetails(initialBalanceUpload);
        setInitialBalanceUploadActiveState(getInitialBalanceUploadStateMachine(initialBalanceUpload));
        initialBalanceUpload.setHeaderDetail(IDObInitialBalanceUpload.activationDetails, initialBalanceUploadActivationDetails);
        initialBalanceUploadRep.update(initialBalanceUpload);
        return Observable.just(initialBalanceUploadCode);
    }

    private IObInitialBalanceUploadActivationDetails createActivationDetails(
            DObInitialBalanceUpload initialBalanceUpload) {

        IObInitialBalanceUploadActivationDetails activationDetails = new IObInitialBalanceUploadActivationDetails();
        String loggedInUser = commandUtils.getLoggedInUser();
        activationDetails.setAccountant(loggedInUser);
        activationDetails.setActivationDate(new DateTime());
        activationDetails.setRefInstanceId(initialBalanceUpload.getId());
        commandUtils.setCreationAndModificationInfoAndDate(activationDetails);
        activationDetails.setFiscalPeriodId(initialBalanceUpload.getFiscalPeriodId());
        DateTime dueDateTime = new DateTime();
        activationDetails.setDueDate(dueDateTime);
        return activationDetails;
    }

    private void setInitialBalanceUploadActiveState(AbstractStateMachine initialBalanceUploadStateMachine) {
        if (initialBalanceUploadStateMachine.canFireEvent(IDObInitialBalanceUploadActionNames.ACTIVATE)) {
            initialBalanceUploadStateMachine.fireEvent(IDObInitialBalanceUploadActionNames.ACTIVATE);
            initialBalanceUploadStateMachine.save();
        }
    }

    private DObInitialBalanceUploadStateMachine getInitialBalanceUploadStateMachine(DObInitialBalanceUpload initialBalanceUpload)
            throws Exception {
        DObInitialBalanceUploadStateMachine initialBalanceUploadStateMachine = new DObInitialBalanceUploadStateMachine();
        initialBalanceUploadStateMachine.initObjectState(initialBalanceUpload);
        return initialBalanceUploadStateMachine;
    }

    public void setInitialBalanceUploadRep(DObInitialBalanceUploadRep initialBalanceUploadRep) {
        this.initialBalanceUploadRep = initialBalanceUploadRep;
    }

    public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
        this.userAccountSecurityService = userAccountSecurityService;
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }
}
