package com.ebs.dda.commands.accounting.paymentrequest;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import io.reactivex.Observable;

import java.math.BigDecimal;

public class DObPaymentUpdateVendorInvoiceRemainingCommand implements ICommand<DObPaymentRequestActivateValueObject> {
    private CommandUtils commandUtils;
    private IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep;
    private DObVendorInvoiceRep vendorInvoiceRep;
    private IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;

    public DObPaymentUpdateVendorInvoiceRemainingCommand(){
        commandUtils = new CommandUtils();
    }
    @Override
    public <T> Observable<T> executeCommand(DObPaymentRequestActivateValueObject valueObject) throws Exception {
        IObPaymentRequestPaymentDetailsGeneralModel paymentDetails = getPaymentDetails(valueObject);

        String vendorInvoiceCode = paymentDetails.getDueDocumentCode();

        DObVendorInvoice vendorInvoice = getVendorInvoiceId(vendorInvoiceCode);
        IObVendorInvoiceSummary vendorInvoiceSummary =
          updateVendorInvoiceRemaining(paymentDetails.getNetAmount(), vendorInvoice.getId());
        commandUtils.setModificationInfoAndModificationDate(vendorInvoice);
        vendorInvoice.setHeaderDetail(IDObVendorInvoice.summaryAttr, vendorInvoiceSummary);
        vendorInvoiceRep.update(vendorInvoice);

        return Observable.empty();
    }

    private IObPaymentRequestPaymentDetailsGeneralModel getPaymentDetails(DObPaymentRequestActivateValueObject valueObject) {
        String paymentCode = valueObject.getPaymentRequestCode();
        IObPaymentRequestPaymentDetailsGeneralModel paymentRequestPaymentDetails = paymentRequestPaymentDetailsGeneralModelRep.findOneByUserCode(paymentCode);
        return paymentRequestPaymentDetails;
    }

    private IObVendorInvoiceSummary updateVendorInvoiceRemaining(BigDecimal paymentAmount, Long vendorInvoiceId) {
        IObVendorInvoiceSummary vendorInvoiceSummary = vendorInvoiceSummaryRep.findOneByRefInstanceId(vendorInvoiceId);
        BigDecimal remaining = vendorInvoiceSummary.getRemaining();
        BigDecimal updatedRemaining = remaining.subtract(paymentAmount);
        vendorInvoiceSummary.setRemaining(updatedRemaining);
        commandUtils.setModificationInfoAndModificationDate(vendorInvoiceSummary);
        return vendorInvoiceSummary;
    }

    private DObVendorInvoice getVendorInvoiceId(String vendorInvoiceCode) {
        DObVendorInvoice vendorInvoice = vendorInvoiceRep.findOneByUserCode(vendorInvoiceCode);
        return vendorInvoice;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setPaymentRequestPaymentDetailsGeneralModelRep(IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep) {
        this.paymentRequestPaymentDetailsGeneralModelRep = paymentRequestPaymentDetailsGeneralModelRep;
    }

    public void setVendorInvoiceRep(DObVendorInvoiceRep vendorInvoiceRep) {
        this.vendorInvoiceRep = vendorInvoiceRep;
    }

    public void setVendorInvoiceSummaryRep(IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep) {
        this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
    }
}
