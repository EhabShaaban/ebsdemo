package com.ebs.dda.commands.accounting.journalentry.create.collection;

import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.collection.*;
import com.ebs.dda.jpa.accounting.journalentry.DObCollectionJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemBankSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemTreasurySubAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseBasicData;
import com.ebs.dda.repositories.accounting.collection.*;
import com.ebs.dda.repositories.masterdata.enterprise.IObEnterpriseBasicDataRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DObJournalEntryItemsCollectionCreateCommand
    extends DObJournalEntryCollectionCreateCommand {

  protected LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep;
  private IObCollectionAccountingDetailsGeneralModelRep collectionAccountingDetailsGeneralModelRep;
  private IObCollectionAccountingDetailsGeneralModel collectionDebitAccountingDetails;
  private IObCollectionAccountingDetailsGeneralModel collectionCustomerAccountingDetails;
  private IObCollectionCompanyDetailsRep collectionCompanyDetailsRep;
  private IObCollectionCompanyDetails collectionCompanyDetails;
  private IObEnterpriseBasicDataRep companyBasicDataRep;
  private IObEnterpriseBasicData companyBasicData;
  private IObCollectionDetailsRep<IObCollectionDetails> collectionDetailsRep;
  private IObCollectionDetails collectionDetails;

  public DObJournalEntryItemsCollectionCreateCommand() {
    super();
  }

  public void setGlobalAccountConfigGeneralModelRep(
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
    this.globalAccountConfigGeneralModelRep = globalAccountConfigGeneralModelRep;
  }

  @Override
  public Observable<String> executeCommand(
      DObJournalEntryCreateCollectionValueObject collectionValueObject) throws Exception {
    initCollectionVariables(collectionValueObject);
    LObGlobalGLAccountConfigGeneralModel bankAccount =
        globalAccountConfigGeneralModelRep.findOneByUserCode(
            ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT);
    LObGlobalGLAccountConfigGeneralModel cashAccount =
        globalAccountConfigGeneralModelRep.findOneByUserCode(
            ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT);

    List<IObJournalEntryItem> journalEntryItems = new ArrayList<>();
    DObCollectionJournalEntry collectionJournalEntry = executeParentCommand();
    setJournalItemsData(journalEntryItems, bankAccount, cashAccount);
    executeTransaction(collectionJournalEntry, journalEntryItems);
    return Observable.just(collectionJournalEntry.getUserCode());
  }

  private void initCollectionVariables(
      DObJournalEntryCreateCollectionValueObject collectionValueObject) {
    String collectionCode = collectionValueObject.getCollectionCode();
    initGeneralCollectionVariables(collectionCode);
    initCollectionConfigurationVariables(collectionCode);
    collectionDetails =
        collectionDetailsRep.findOneByRefInstanceId(
            collectionDebitAccountingDetails.getRefInstanceId());
  }

  private void initCollectionConfigurationVariables(String collectionCode) {
    collectionDebitAccountingDetails =
        collectionAccountingDetailsGeneralModelRep.findOneByDocumentCodeAndAccountingEntry(
            collectionCode, AccountingEntry.DEBIT.name());
    collectionCustomerAccountingDetails =
        collectionAccountingDetailsGeneralModelRep.findOneByDocumentCodeAndAccountingEntry(
            collectionCode, AccountingEntry.CREDIT.name());
    collectionCompanyDetails =
        collectionCompanyDetailsRep.findOneByRefInstanceId(
            collectionDebitAccountingDetails.getRefInstanceId());
    companyBasicData =
        companyBasicDataRep.findOneByRefInstanceId(collectionCompanyDetails.getCompanyId());
  }

  private void setJournalItemsData(
      List<IObJournalEntryItem> journalEntryItems,
      LObGlobalGLAccountConfigGeneralModel bankAccount,
      LObGlobalGLAccountConfigGeneralModel cashAccount) {
    setJournalItemsBasedOnCashOrBank(journalEntryItems, bankAccount, cashAccount);
    setJournalItemBasedOnRefDocumentAccountData(journalEntryItems);
  }

  private void setJournalItemsBasedOnCashOrBank(
      List<IObJournalEntryItem> journalEntryItems,
      LObGlobalGLAccountConfigGeneralModel bankAccount,
      LObGlobalGLAccountConfigGeneralModel cashAccount) {
    String accountCode = collectionDebitAccountingDetails.getGlAccountCode();

    if (!accountCode.equals(bankAccount.getAccountCode())) {
      setJournalItemTreasuryAccountData(journalEntryItems, cashAccount.getGlAccountId());
    } else {
      setJournalItemBankData(journalEntryItems, bankAccount.getGlAccountId());
    }
  }

  private void setJournalItemBankData(
      List<IObJournalEntryItem> journalEntryItems, Long bankAccountId) {
    IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount =
        new IObJournalEntryItemBankSubAccount();
    setAccountAndSubAccountForBankType(journalEntryItemBankSubAccount, bankAccountId);
    setDetailsForBankType(journalEntryItemBankSubAccount);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemBankSubAccount);
    journalEntryItems.add(journalEntryItemBankSubAccount);
  }

  private void setAccountAndSubAccountForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount, Long bankAccountId) {
    Long bankAccountAccountId = collectionDebitAccountingDetails.getGlSubAccountId();
    journalEntryItemBankSubAccount.setAccountId(bankAccountId);
    journalEntryItemBankSubAccount.setSubAccountId(bankAccountAccountId);
  }

  private void setDetailsForBankType(
      IObJournalEntryItemBankSubAccount journalEntryItemBankSubAccount) {
    journalEntryItemBankSubAccount.setAccountId(collectionDebitAccountingDetails.getGlAccountId());
    journalEntryItemBankSubAccount.setDocumentCurrencyId(collectionDetails.getCurrencyId());
    journalEntryItemBankSubAccount.setCompanyCurrencyId(companyBasicData.getCurrencyId());
    journalEntryItemBankSubAccount.setCompanyCurrencyPrice(
        collectionActivationDetailsGeneralModel.getCurrencyPrice());
    journalEntryItemBankSubAccount.setCompanyExchangeRateId(
        collectionActivationDetailsGeneralModel.getExchangeRateId());
    journalEntryItemBankSubAccount.setCredit(BigDecimal.ZERO);
    journalEntryItemBankSubAccount.setDebit(collectionDebitAccountingDetails.getAmount());
  }

  private void setJournalItemBasedOnRefDocumentAccountData(
      List<IObJournalEntryItem> journalEntryItems) {
    JournalItemCollectionFactory journalItemCollectionFactory =
        new JournalItemCollectionFactory(collectionGeneralModel);
    IObJournalEntryItem iObJournalEntryItem =
        journalItemCollectionFactory.getJournalEntryItemByCollectionType(
            collectionCustomerAccountingDetails);
    iObJournalEntryItem.setAccountId(collectionCustomerAccountingDetails.getGlAccountId());
    iObJournalEntryItem.setDocumentCurrencyId(collectionDetails.getCurrencyId());
    iObJournalEntryItem.setCompanyCurrencyId(companyBasicData.getCurrencyId());
    iObJournalEntryItem.setCompanyCurrencyPrice(
            collectionActivationDetailsGeneralModel.getCurrencyPrice());
    iObJournalEntryItem.setCompanyExchangeRateId(
        collectionActivationDetailsGeneralModel.getExchangeRateId());
    iObJournalEntryItem.setCredit(collectionCustomerAccountingDetails.getAmount());
    iObJournalEntryItem.setDebit(BigDecimal.ZERO);
    commandUtils.setCreationAndModificationInfoAndDate(iObJournalEntryItem);
    journalEntryItems.add(iObJournalEntryItem);
  }

  private void setJournalItemTreasuryAccountData(
      List<IObJournalEntryItem> journalEntryItems, Long cashAccountId) {
    IObJournalEntryItemTreasurySubAccount journalEntryItemTreasurySubAccount =
        new IObJournalEntryItemTreasurySubAccount();
    setAccountAndSubAccountForCashType(journalEntryItemTreasurySubAccount, cashAccountId);
    setDetailsForCashType(journalEntryItemTreasurySubAccount);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemTreasurySubAccount);
    journalEntryItems.add(journalEntryItemTreasurySubAccount);
  }

  private void setDetailsForCashType(
      IObJournalEntryItemTreasurySubAccount journalEntryItemTreasurySubAccount) {
    journalEntryItemTreasurySubAccount.setDocumentCurrencyId(collectionDetails.getCurrencyId());
    journalEntryItemTreasurySubAccount.setCompanyCurrencyId(companyBasicData.getCurrencyId());
    journalEntryItemTreasurySubAccount.setCompanyCurrencyPrice(
            collectionActivationDetailsGeneralModel.getCurrencyPrice());
    journalEntryItemTreasurySubAccount.setCompanyExchangeRateId(
        collectionActivationDetailsGeneralModel.getExchangeRateId());
    journalEntryItemTreasurySubAccount.setCredit(BigDecimal.ZERO);
    journalEntryItemTreasurySubAccount.setDebit(collectionDebitAccountingDetails.getAmount());
  }

  private void setAccountAndSubAccountForCashType(
      IObJournalEntryItemTreasurySubAccount journalEntryItemTreasurySubAccount,
      Long cashAccountId) {
    Long cashAccountAccountId = collectionDebitAccountingDetails.getGlSubAccountId();
    journalEntryItemTreasurySubAccount.setAccountId(cashAccountId);
    journalEntryItemTreasurySubAccount.setSubAccountId(cashAccountAccountId);
  }

  public void setCollectionAccountingDetailsGeneralModelRep(
      IObCollectionAccountingDetailsGeneralModelRep collectionAccountingDetailsGeneralModelRep) {
    this.collectionAccountingDetailsGeneralModelRep = collectionAccountingDetailsGeneralModelRep;
  }

  public void setCollectionCompanyDetailsRep(
      IObCollectionCompanyDetailsRep collectionCompanyDetailsRep) {
    this.collectionCompanyDetailsRep = collectionCompanyDetailsRep;
  }

  public void setCompanyBasicDataRep(IObEnterpriseBasicDataRep companyBasicDataRep) {
    this.companyBasicDataRep = companyBasicDataRep;
  }

  public void setCollectionDetailsRep(IObCollectionDetailsRep<IObCollectionDetails> collectionDetailsRep) {
    this.collectionDetailsRep = collectionDetailsRep;
  }
}
