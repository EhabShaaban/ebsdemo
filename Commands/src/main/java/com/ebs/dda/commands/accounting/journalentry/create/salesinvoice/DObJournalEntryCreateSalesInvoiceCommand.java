package com.ebs.dda.commands.accounting.journalentry.create.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObSalesInvoiceJournalEntryRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModelRep;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DObJournalEntryCreateSalesInvoiceCommand
				implements ICommand<DObJournalEntryCreateValueObject> {

	private DocumentObjectCodeGenerator documentObjectCodeGenerator;
	private DObSalesInvoiceJournalEntryPreparationGeneralModelRep salesInvoiceJournalEntryPreparationGeneralModelRep;
	private DObSalesInvoiceJournalEntryPreparationGeneralModel salesInvoiceJournalEntryPreparationGeneralModel;
    private IObSalesInvoiceAccountingDetailsGeneralModelRep accountingDetailsGeneralModelRep;

	private CommandUtils commandUtils;

	private DObSalesInvoiceJournalEntryRep salesInvoiceJournalEntryRep;

	public DObJournalEntryCreateSalesInvoiceCommand(
					DocumentObjectCodeGenerator documentObjectCodeGenerator) {
		commandUtils = new CommandUtils();
		this.documentObjectCodeGenerator = documentObjectCodeGenerator;
	}

	@Override
	public Observable<String> executeCommand(
					DObJournalEntryCreateValueObject salesInvoiceValueObject) throws Exception {

		String salesInvoiceCode = salesInvoiceValueObject.getUserCode();

		DObSalesInvoiceJournalEntry salesInvoiceJournalEntry = new DObSalesInvoiceJournalEntry();
		List<IObJournalEntryItem> journalEntryItems = new ArrayList<>();

		initInvoiceEntities(salesInvoiceCode);

		setJournalEntryData(salesInvoiceJournalEntry);
		setJournalEntryFiscalYear(salesInvoiceJournalEntry);
		setJournalItemsData(journalEntryItems, salesInvoiceCode);

		salesInvoiceJournalEntry.setLinesDetails(IDObJournalEntry.journalItemAttr,
						journalEntryItems);
		setJournalEntryUserCode(salesInvoiceJournalEntry);
		salesInvoiceJournalEntryRep.create(salesInvoiceJournalEntry);

		return Observable.just(salesInvoiceJournalEntry.getUserCode());
	}

	private void setJournalEntryFiscalYear(DObSalesInvoiceJournalEntry salesInvoiceJournalEntry) {
		salesInvoiceJournalEntry.setFiscalPeriodId(salesInvoiceJournalEntryPreparationGeneralModel.getFiscalYearId());
	}

	private void initInvoiceEntities(String salesInvoiceCode) {
		salesInvoiceJournalEntryPreparationGeneralModel = salesInvoiceJournalEntryPreparationGeneralModelRep
						.findOneByCode(salesInvoiceCode);

	}

	private void setJournalEntryData(DObSalesInvoiceJournalEntry salesInvoiceJournalEntry) {
		setJournalEntryActiveState(salesInvoiceJournalEntry);
		commandUtils.setCreationAndModificationInfoAndDate(salesInvoiceJournalEntry);
		setSalesInvoiceData(salesInvoiceJournalEntry);
	}

	private void setSalesInvoiceData(DObSalesInvoiceJournalEntry salesInvoiceJournalEntry) {
		salesInvoiceJournalEntry
						.setDueDate(salesInvoiceJournalEntryPreparationGeneralModel.getDueDate());
		salesInvoiceJournalEntry.setCompanyId(
						salesInvoiceJournalEntryPreparationGeneralModel.getCompanyId());
		salesInvoiceJournalEntry.setPurchaseUnitId(
						salesInvoiceJournalEntryPreparationGeneralModel.getPurchaseUnitId());
		salesInvoiceJournalEntry.setDocumentReferenceId(
						salesInvoiceJournalEntryPreparationGeneralModel.getId());
	}

	private void setJournalEntryActiveState(DObSalesInvoiceJournalEntry salesInvoiceJournalEntry) {
		Set<String> currentStates = new HashSet<>();
		currentStates.add(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE);
		salesInvoiceJournalEntry.setCurrentStates(currentStates);
	}

	private void setJournalEntryUserCode(DObSalesInvoiceJournalEntry salesInvoiceJournalEntry) {
		String userCode = documentObjectCodeGenerator
						.generateUserCode(DObJournalEntry.class.getSimpleName());
		salesInvoiceJournalEntry.setUserCode(userCode);
	}

	private void setJournalItemsData(List<IObJournalEntryItem> journalEntryItems,
					String salesInvoiceCode) {
		List<IObSalesInvoiceAccountingDetailsGeneralModel> accounts = accountingDetailsGeneralModelRep.findByDocumentCodeOrderByAccountingEntryDesc(salesInvoiceCode);

		for(IObSalesInvoiceAccountingDetailsGeneralModel account: accounts){
			IObJournalEntryItem journalItem = DObJournalEntrySalesInvoiceInstanceFactory.getJournalItemInstance(account.getAccountingEntry(), account.getSubLedger());
			journalItem.setAccountId(
					account.getGlAccountId());
			journalItem.setSubAccountId(
					account.getGlSubAccountId());
			journalItem.setDocumentCurrencyId(
					salesInvoiceJournalEntryPreparationGeneralModel.getCustomerCurrencyId());
			journalItem.setCompanyCurrencyId(
					salesInvoiceJournalEntryPreparationGeneralModel.getCompanyCurrencyId());
			journalItem.setCompanyCurrencyPrice(
					salesInvoiceJournalEntryPreparationGeneralModel.getCurrencyPrice());
			DObJournalEntrySalesInvoiceInstanceFactory.setAmount(account.getAccountingEntry(), journalItem, account.getAmount());
			journalItem.setCompanyExchangeRateId(
					salesInvoiceJournalEntryPreparationGeneralModel.getExchangeRateId());	journalItem.setCompanyExchangeRateId(
					salesInvoiceJournalEntryPreparationGeneralModel.getExchangeRateId());
			commandUtils.setCreationAndModificationInfoAndDate(journalItem);
			journalEntryItems.add(journalItem);
		}
	}

	public void setUserAccountSecurityService(
					IUserAccountSecurityService userAccountSecurityService) {
		this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
	}

	public void setDocumentObjectCodeGenerator(
					DocumentObjectCodeGenerator documentObjectCodeGenerator) {
		this.documentObjectCodeGenerator = documentObjectCodeGenerator;
	}

	public void setSalesInvoiceJournalEntryRep(
					DObSalesInvoiceJournalEntryRep salesInvoiceJournalEntryRep) {
		this.salesInvoiceJournalEntryRep = salesInvoiceJournalEntryRep;
	}

	public void setSalesInvoiceJournalEntryPreparationGeneralModelRep(
					DObSalesInvoiceJournalEntryPreparationGeneralModelRep salesInvoiceJournalEntryPreparationGeneralModelRep) {
		this.salesInvoiceJournalEntryPreparationGeneralModelRep = salesInvoiceJournalEntryPreparationGeneralModelRep;
	}
	public void setAccountingDetailsGeneralModelRep(IObSalesInvoiceAccountingDetailsGeneralModelRep accountingDetailsGeneralModelRep) {
		this.accountingDetailsGeneralModelRep = accountingDetailsGeneralModelRep;
	}
}
