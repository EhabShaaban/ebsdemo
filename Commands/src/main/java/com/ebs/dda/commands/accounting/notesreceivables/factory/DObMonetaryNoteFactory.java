package com.ebs.dda.commands.accounting.notesreceivables.factory;

import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableAsCollection;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableTypeEnum;
import javax.transaction.NotSupportedException;

public class DObMonetaryNoteFactory {

  public DObMonetaryNotes createMonetaryNote(String type) throws Exception {

    if (isNotesReceivableAsCollection(type)) {
      return new DObNotesReceivableAsCollection();
    }

    throw new NotSupportedException("Not supported MonetaryNote type");
  }

  private boolean isNotesReceivableAsCollection(String type) {
    return type.equals(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION.name());
  }

}
