package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsPIRequestedValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import io.reactivex.Observable;
import org.joda.time.DateTime;

public class PurchaseOrderMarkAsPIRequestedCommand
    implements ICommand<DobPurchaseOrderMarkAsPIRequestedValueObject> {

  private final CommandUtils commandUtils;
  private DObPurchaseOrderRep purchaseOrderRep;
  private IObOrderCycleDatesRep orderCycleDatesRep;

  public PurchaseOrderMarkAsPIRequestedCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setOrderCycleDatesRep(IObOrderCycleDatesRep orderCycleDatesRep) {
    this.orderCycleDatesRep = orderCycleDatesRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  @Override
  public Observable<String> executeCommand(
      DobPurchaseOrderMarkAsPIRequestedValueObject asPIRequestedValueObject) throws Exception {
    DObPurchaseOrder purchaseOrder =
        readEntityByCode(asPIRequestedValueObject.getPurchaseOrderCode());
    setPurchaseOrderMarkAsPIRequestState(getPurchaseOrderStateMachine(purchaseOrder));
    IObOrderCycleDates orderCycleDates = getOrderCycleDates(purchaseOrder);
    DateTime collectionDate = asPIRequestedValueObject.getpIRequestedDate();
    orderCycleDates.setActualPIRequestedDate(collectionDate);

    commandUtils.setModificationInfoAndModificationDate(orderCycleDates);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    purchaseOrder.setHeaderDetail(IDObPurchaseOrder.cycleDatesAttr, orderCycleDates);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private IObOrderCycleDates getOrderCycleDates(DObPurchaseOrder purchaseOrder) {
    IObOrderCycleDates orderCycleDates =
        orderCycleDatesRep.findOneByRefInstanceId(purchaseOrder.getId());
    if (orderCycleDates != null) {
      return orderCycleDates;
    } else {
      orderCycleDates = new IObOrderCycleDates();
      commandUtils.setCreationAndModificationInfoAndDate(orderCycleDates);
    }
    return orderCycleDates;
  }

  private void setPurchaseOrderMarkAsPIRequestState(
      AbstractStateMachine purchaseOrderStateMachine) {
    if (purchaseOrderStateMachine.fireEvent(IPurchaseOrderActionNames.MARK_AS_PI_REQUESTED)) {
      purchaseOrderStateMachine.save();
    }
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private DObPurchaseOrder readEntityByCode(String userCode) {
    return purchaseOrderRep.findOneByUserCode(userCode);
  }
}
