package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesDeletionValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesRep;
import io.reactivex.Observable;
import javax.transaction.Transactional;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public class IObSalesInvoiceTaxesDeleteCommand
    implements ICommand<IObSalesInvoiceTaxesDeletionValueObject> {

  private IObSalesInvoiceTaxesRep salesInvoiceTaxesRep;
  private IObSalesInvoiceTaxesGeneralModelRep salesInvoiceTaxesGeneralModelRep;
  private DObSalesInvoiceRep salesInvoiceRep;
  private IUserAccountSecurityService userAccountSecurityService;
  private IObSalesInvoiceSummaryRep summaryRep;
  private EntityLockCommand<DObSalesInvoice> entityLockCommand;

  @Override
  @Transactional
  public Observable<String> executeCommand(
      IObSalesInvoiceTaxesDeletionValueObject taxesDeletionValueObject)
      throws Exception {
    String invoiceCode = taxesDeletionValueObject.getSalesInvoiceCode();
    String taxCode = taxesDeletionValueObject.getTaxCode();
    IObSalesInvoiceTaxesGeneralModel invoiceTaxesGeneralModel =
        salesInvoiceTaxesGeneralModelRep.findOneByInvoiceCodeAndTaxCode(invoiceCode, taxCode);
    BigDecimal taxTotalAmount = invoiceTaxesGeneralModel.getTaxAmount();

    salesInvoiceTaxesRep.deleteById(invoiceTaxesGeneralModel.getId());

    DObSalesInvoice invoice = salesInvoiceRep.findOneByUserCode(invoiceCode);
    updateSummarySection(invoice, taxTotalAmount);
    setModificationInfo(invoice);
    return Observable.just("SUCCESS");
  }

  public void setSalesInvoiceTaxesRep(
      IObSalesInvoiceTaxesRep salesInvoiceTaxesRep) {
    this.salesInvoiceTaxesRep = salesInvoiceTaxesRep;
  }

  public void setSalesInvoiceTaxesGeneralModelRep(
      IObSalesInvoiceTaxesGeneralModelRep salesInvoiceTaxesGeneralModelRep) {
    this.salesInvoiceTaxesGeneralModelRep = salesInvoiceTaxesGeneralModelRep;
  }

  public void setSalesInvoiceRep(DObSalesInvoiceRep invoiceRep) {
    this.salesInvoiceRep = invoiceRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  private void setModificationInfo(DObSalesInvoice dObInvoice) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    dObInvoice.setModificationInfo(loggedinUserInfo);
    dObInvoice.setModifiedDate(new DateTime());
    salesInvoiceRep.save(dObInvoice);
  }

  private void updateSummarySection(DObSalesInvoice salesInvoice, BigDecimal taxTotalAmount) throws Exception {
    entityLockCommand.lockSection(salesInvoice.getUserCode(), IDObSalesInvoiceSectionNames.SUMMARY_SECTION);
    IObSalesInvoiceSummary summary = summaryRep.findOneByRefInstanceId(salesInvoice.getId());
    summary.setRemaining(summary.getRemaining().subtract(taxTotalAmount));

    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    summary.setModificationInfo(loggedinUserInfo);
    summary.setModifiedDate(new DateTime());
    summaryRep.save(summary);
    entityLockCommand.unlockSection(salesInvoice.getUserCode(), IDObSalesInvoiceSectionNames.SUMMARY_SECTION);
  }

  public void setSummaryRep(IObSalesInvoiceSummaryRep summaryRep) {
    this.summaryRep = summaryRep;
  }

  public void setEntityLockCommand(EntityLockCommand<DObSalesInvoice> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }
}
