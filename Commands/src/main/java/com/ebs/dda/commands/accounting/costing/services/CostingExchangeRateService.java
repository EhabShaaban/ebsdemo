package com.ebs.dda.commands.accounting.costing.services;

import com.ebs.dda.commands.accounting.costing.CostingExchangeRateStrategyFactory;
import com.ebs.dda.commands.accounting.costing.ICalculateCostingExchangeRateStrategy;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CostingExchangeRateService {
  private final DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private final DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private final IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep;
  private final CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory;

  public CostingExchangeRateService(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
      IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
      CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
    this.vendorInvoiceSummaryRep = vendorInvoiceSummaryRep;
    this.costingExchangeRateStrategyFactory = costingExchangeRateStrategyFactory;
  }

  public BigDecimal calculate(String goodsReceiptCode) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        this.goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        this.vendorInvoiceGeneralModelRep.findOneByPurchaseOrderCode(
            goodsReceiptGeneralModel.getRefDocumentCode());
    IObVendorInvoiceSummary vendorInvoiceSummary =
        this.vendorInvoiceSummaryRep.findOneByRefInstanceId(vendorInvoiceGeneralModel.getId());
    ICalculateCostingExchangeRateStrategy strategy =
        this.costingExchangeRateStrategyFactory.getStrategy(vendorInvoiceSummary);
    return strategy.calculate(goodsReceiptGeneralModel);
  }
}
