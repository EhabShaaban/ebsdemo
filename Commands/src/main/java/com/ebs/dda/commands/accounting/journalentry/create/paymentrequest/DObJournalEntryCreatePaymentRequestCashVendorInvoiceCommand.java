package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemTreasurySubAccount;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand
    extends DObJournalEntryCreatePaymentRequestVendorInvoiceCommand {

  public DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand() {}

  @Override
  public Observable<String> executeCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    super.executeCommand(paymentRequestValueObject);
    setJournalItemsData(journalEntryItems);
    executeTransaction(paymentRequestJournalEntry, journalEntryItems);
    return Observable.just(paymentRequestJournalEntry.getUserCode());
  }

  private void setJournalItemsData(List<IObJournalEntryItem> journalEntryItems) {
    setJournalItemVendorInvoiceData(journalEntryItems);
    setJournalItemCashData(journalEntryItems);
  }

  private void setJournalItemCashData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItemTreasurySubAccount journalEntryItem =
        new IObJournalEntryItemTreasurySubAccount();
    setAccountAndSubAccountForCashType(journalEntryItem);
    setDetailsForCashType(journalEntryItem);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
    journalEntryItems.add(journalEntryItem);
  }

  private void setDetailsForCashType(IObJournalEntryItem journalEntryItem) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItem.setCredit(paymentDetails.getNetAmount());
    journalEntryItem.setDebit(BigDecimal.ZERO);
    journalEntryItem.setDocumentCurrencyId(currencyId);
    journalEntryItem.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItem.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItem.setCompanyExchangeRateId(exchangeRateId);
  }

  private void setAccountAndSubAccountForCashType(
      IObJournalEntryItemTreasurySubAccount journalEntryItem) {
    journalEntryItem.setAccountId(paymentRequestAccountingDetailsCredit.getGlAccountId());
    journalEntryItem.setSubAccountId(paymentRequestAccountingDetailsCredit.getGlSubAccountId());
  }
}
