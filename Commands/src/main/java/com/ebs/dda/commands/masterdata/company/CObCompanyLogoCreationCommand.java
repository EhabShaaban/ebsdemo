package com.ebs.dda.commands.masterdata.company;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.masterdata.company.utils.CObCompanyLogoCreationUtils;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.company.CObCompanyLogoCreationValueObject;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.company.IObCompanyLogoDetails;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyLogoDetailsRep;
import org.joda.time.DateTime;

public class CObCompanyLogoCreationCommand extends CObCompanyLogoCreationUtils {

  private IUserAccountSecurityService userAccountSecurityService;
  private CObCompanyRep companyRep;
  private IObCompanyLogoDetailsRep companyLogoDetailsRep;

  public void setIUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setCObCompanyRep(CObCompanyRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setIObCompanyLogoDetailsRep(IObCompanyLogoDetailsRep companyLogoDetailsRep) {
    this.companyLogoDetailsRep = companyLogoDetailsRep;
  }

  public void executeCommand(CObCompanyLogoCreationValueObject logoCreationValueObject)
      throws Exception {

    CObCompany company = getCompany(logoCreationValueObject);
    IObCompanyLogoDetails companyLogoDetails = getIObCompanyLogoDetails(company.getId());
    setCompanyLogoDetails(companyLogoDetails, logoCreationValueObject, company);
    companyRep.update(company);
  }

  private CObCompany getCompany(CObCompanyLogoCreationValueObject logoCreationValueObject) {
    String companyCode = logoCreationValueObject.getCompanyCode();
    return companyRep.findOneByUserCode(companyCode);
  }

  private IObCompanyLogoDetails getIObCompanyLogoDetails(Long companyId) {
    IObCompanyLogoDetails companyLogoDetails =
        companyLogoDetailsRep.findOneByRefInstanceId(companyId);
    if (companyLogoDetails == null) {
      companyLogoDetails = new IObCompanyLogoDetails();
    }
    return companyLogoDetails;
  }

  private void setCompanyLogoDetails(
      IObCompanyLogoDetails companyLogoDetails,
      CObCompanyLogoCreationValueObject logoCreationValueObject,
      CObCompany company)
      throws Exception {
    byte[] logoByteArray =
        convertFilePathContentToByteArray(logoCreationValueObject.getLogoFilePath());
    companyLogoDetails.setLogo(logoByteArray);
    setInformationObjectDetails(companyLogoDetails, company);
    company.setSingleDetail(ICObCompany.companyLogoAttr, companyLogoDetails);
    companyLogoDetails.setRefInstanceId(company.getId());
  }

  private void setInformationObjectDetails(
      IObCompanyLogoDetails companyLogoDetails, CObCompany company) throws Exception {
    setCreationAndModificationInfo(companyLogoDetails);
  }

  private void setCreationAndModificationInfo(BusinessObject businessObject) {
    String loggedinUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    businessObject.setCreationInfo(loggedinUserInfo);
    businessObject.setModificationInfo(loggedinUserInfo);
    DateTime currentDate = new DateTime();
    businessObject.setModifiedDate(currentDate);
    businessObject.setCreationDate(currentDate);
  }

}
