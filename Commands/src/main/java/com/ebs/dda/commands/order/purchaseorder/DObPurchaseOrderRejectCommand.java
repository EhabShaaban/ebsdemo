package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderApproveAndRejectUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderApproveValueObject;
import io.reactivex.Observable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DObPurchaseOrderRejectCommand extends DObPurchaseOrderApproveAndRejectUtils
    implements ICommand<DObPurchaseOrderApproveValueObject> {

  private final CommandUtils commandUtils;

  public DObPurchaseOrderRejectCommand() {
    this.commandUtils = new CommandUtils();
  }

  public Observable<String> executeCommand(DObPurchaseOrderApproveValueObject approveValueObject)
      throws Exception {
    String purchaseOrderCode = approveValueObject.getPurchaseOrderCode();
    entityLockCommand.lockAllSections(purchaseOrderCode);
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    IBDKUser loggedInUser = commandUtils.getLoggedInUserObject();
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());
    DateTime dicisionDatetime = new DateTime(DateTimeZone.UTC);
    ApprovalDecision decision = ApprovalDecision.REJECTED;
    IObOrderApprover currentApprover =
        getAndUpdateCurrentApprover(
            approveValueObject.getNotes(),
            loggedInUser,
            currentUndecidedCycle.getId(),
            decision,
            dicisionDatetime);

    updateCycleDecisionAndEndDate(currentUndecidedCycle, dicisionDatetime, decision);
    setPurchaseOrderTargetState(purchaseOrder, IPurchaseOrderActionNames.REJECT_PO);

    commandUtils.setModificationInfoAndModificationDate(currentApprover);
    commandUtils.setModificationInfoAndModificationDate(currentUndecidedCycle);
    commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
    updateDatabaseAndUnlock(currentApprover, currentUndecidedCycle, purchaseOrder);
    return Observable.just("SUCCESS");
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }
}
