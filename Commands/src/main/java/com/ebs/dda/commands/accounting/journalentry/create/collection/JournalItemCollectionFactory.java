package com.ebs.dda.commands.accounting.journalentry.create.collection;

import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemCustomerSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemNotesReceivableSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemVendorSubAccount;

public class JournalItemCollectionFactory {

  private static final String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
  private static final String SALES_INVOICE = "SALES_INVOICE";
  private static final String SALES_ORDER = "SALES_ORDER";
  private static final String LOCAL_CUSTOMER = "Local_Customer";
  private DObCollectionGeneralModel collectionGeneralModel;

  public JournalItemCollectionFactory(DObCollectionGeneralModel collectionGeneralModel) {
    this.collectionGeneralModel = collectionGeneralModel;
  }

  public IObJournalEntryItem getJournalEntryItemByCollectionType(
      IObCollectionAccountingDetailsGeneralModel collectionCustomerAccountingDetails) {
    String refDocumentTypeCode = collectionGeneralModel.getRefDocumentTypeCode();
    Long subAccountId = collectionCustomerAccountingDetails.getGlSubAccountId();
    if (refDocumentTypeCode.equals(NOTES_RECEIVABLE)) {
      return constructJournalEntryItemNotesReceivable(subAccountId);
    } else if (refDocumentTypeCode.equals(SALES_INVOICE)
        || refDocumentTypeCode.equals(SALES_ORDER)) {
      return constructJournalEntryItemCustomer(subAccountId);
    } else if (refDocumentTypeCode.equals(RefDocumentTypeEnum.DEBIT_NOTE)) {
      return constructJournalEntryItemVendor(subAccountId, collectionCustomerAccountingDetails.getSubLedger());
    }
    return new IObJournalEntryItem();
  }

  private IObJournalEntryItem constructJournalEntryItemCustomer(Long subAccountId) {
    IObJournalEntryItemCustomerSubAccount journalEntryItemCustomerSubAccount =
        new IObJournalEntryItemCustomerSubAccount();
    journalEntryItemCustomerSubAccount.setType(LOCAL_CUSTOMER);
    journalEntryItemCustomerSubAccount.setSubAccountId(subAccountId);
    return journalEntryItemCustomerSubAccount;
  }

  private IObJournalEntryItemNotesReceivableSubAccount constructJournalEntryItemNotesReceivable(
      Long subAccountId) {
    IObJournalEntryItemNotesReceivableSubAccount journalEntryItemNotesReceivableSubAccount =
        new IObJournalEntryItemNotesReceivableSubAccount();
    journalEntryItemNotesReceivableSubAccount.setSubAccountId(subAccountId);
    return journalEntryItemNotesReceivableSubAccount;
  }

  private IObJournalEntryItemVendorSubAccount constructJournalEntryItemVendor(Long subAccountId, String subLedger) {
    IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount =
        new IObJournalEntryItemVendorSubAccount();
    journalEntryItemVendorSubAccount.setSubAccountId(subAccountId);
    journalEntryItemVendorSubAccount.setType(subLedger);
    return journalEntryItemVendorSubAccount;
  }
}
