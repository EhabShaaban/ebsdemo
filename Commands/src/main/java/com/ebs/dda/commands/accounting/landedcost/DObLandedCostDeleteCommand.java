package com.ebs.dda.commands.accounting.landedcost;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import io.reactivex.Observable;

public class DObLandedCostDeleteCommand implements ICommand {

  private DObLandedCostRep landedCostRep;

  public DObLandedCostDeleteCommand() {}

  @Override
  public Observable executeCommand(Object landedCostCode) throws Exception {
    landedCostRep.delete(readEntityByCode((String) landedCostCode));
    return Observable.empty();
  }

  private DObLandedCost readEntityByCode(String landedCostCode) {
    return landedCostRep.findOneByUserCode(landedCostCode);
  }

  public void setLandedCostRep(DObLandedCostRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }
}
