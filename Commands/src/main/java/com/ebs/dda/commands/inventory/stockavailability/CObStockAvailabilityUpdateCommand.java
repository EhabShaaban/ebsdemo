package com.ebs.dda.commands.inventory.stockavailability;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.inventory.stockavailability.factory.UpdateStockAvailabilityStrategyFactory;
import com.ebs.dda.commands.inventory.stockavailability.strategies.IUpdateStockAvailabilityStrategy;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import io.reactivex.Observable;

public class CObStockAvailabilityUpdateCommand
    implements ICommand<DObInventoryDocumentActivateValueObject> {
  private UpdateStockAvailabilityStrategyFactory stockAvailabilityUpdateFactory;

  @Override
  public Observable executeCommand(DObInventoryDocumentActivateValueObject valueObject) {
    IUpdateStockAvailabilityStrategy updateStrategy =
        stockAvailabilityUpdateFactory.getStockAvailabilityUpdateStrategyByType(
            valueObject.getInventoryType());
    updateStrategy.updateStockAvailability(valueObject.getUserCode());
    return Observable.empty();
  }

  public void setStockAvailabilityUpdateFactory(
      UpdateStockAvailabilityStrategyFactory stockAvailabilityUpdateFactory) {
    this.stockAvailabilityUpdateFactory = stockAvailabilityUpdateFactory;
  }
}
