package com.ebs.dda.commands.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsible;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorPurchaseUnit;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralDataValueObject;
import com.ebs.dda.masterdata.vendor.IMObVendorSectionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorPurchaseUnitRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Locale;

// Created By Niveen Magdy @ 16/12/18
public class MObVendorGeneralDataEditCommand implements ICommand<MObVendorGeneralDataValueObject> {

  private EntityLockCommand<MObVendor> entityLockCommand;
  private MObVendorRep vendorRep;
  private CObPurchasingResponsibleRep purchasingResponsibleRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private IObVendorPurchaseUnitRep vendorPurchaseUnitRep;
  private CommandUtils commandUtils;

  public MObVendorGeneralDataEditCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setPurchasingResponsibleRep(CObPurchasingResponsibleRep purchasingResponsibleRep) {
    this.purchasingResponsibleRep = purchasingResponsibleRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public LockDetails lock(String resourceCode) throws Exception {
    LockDetails lockDetails =
        entityLockCommand.lockSection(resourceCode, IMObVendorSectionNames.GENERAL_DATA_SECTION);
    return lockDetails;
  }

  public void unlockSection(String vendorCode) throws Exception {
    entityLockCommand.unlockSection(vendorCode, IMObVendorSectionNames.GENERAL_DATA_SECTION);
  }

  @Override
  public Observable<String> executeCommand(MObVendorGeneralDataValueObject valueObject)
      throws Exception {
    String vendorCode = valueObject.getVendorCode();
    String vendorName = valueObject.getVendorName();
    String purchaseResponsibleCode = valueObject.getPurchaseResponsibleCode();
    String accountWithVendor = valueObject.getAccountWithVendor();
    List<String> purchaseUnitList = valueObject.getPurchaseUnits();

    MObVendor vendor = vendorRep.findOneByUserCode(vendorCode);

    vendor.setAccountWithVendor(accountWithVendor);
    setVendorName(vendor, vendorName);
    if (purchaseResponsibleCode != null) {
      setPurchaseResponsible(vendor, purchaseResponsibleCode);
    }
    setVendorPurchaseUnitsData(vendor, purchaseUnitList);
    this.commandUtils.setModificationInfoAndModificationDate(vendor);

    vendorRep.updateAndClear(vendor);
    return Observable.just("SUCCESS");
  }

  private void setPurchaseResponsible(MObVendor vendor, String purchaseResponsibleCode) {
    CObPurchasingResponsible purchasingResponsible =
        purchasingResponsibleRep.findOneByUserCode(purchaseResponsibleCode);
    vendor.setPurchaseResponsibleId(purchasingResponsible.getId());
  }

  private void setVendorName(MObVendor vendor, String vendorName) {
    LocalizedString localizedVendorName = new LocalizedString();
    localizedVendorName.setValue(new Locale("ar"), vendorName);
    localizedVendorName.setValue(new Locale("en"), vendorName);
    vendor.setName(localizedVendorName);
  }

  private void setVendorPurchaseUnitsData(MObVendor vendor, List<String> purchaseUnitList)
      throws Exception {
    for (String purchaseUnitCode : purchaseUnitList) {
      Long purchaseUnitId = getPurchaseUnitId(purchaseUnitCode);
      IObVendorPurchaseUnit vendorPurchaseUnit =
          vendorPurchaseUnitRep.findByRefInstanceIdAndPurchaseUnitId(
              vendor.getId(), purchaseUnitId);
      if (vendorPurchaseUnit == null) {
        vendorPurchaseUnit = new IObVendorPurchaseUnit();
        setPurchaseUnit(vendorPurchaseUnit, purchaseUnitCode);
        this.commandUtils.setCreationAndModificationInfoAndDate(vendorPurchaseUnit);
      }
      vendor.addMultipleDetail(IMObVendor.purchseUnitAttr, vendorPurchaseUnit);
    }
  }

  private Long getPurchaseUnitId(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchaseUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    return purchaseUnitGeneralModel.getId();
  }

  private void setPurchaseUnit(IObVendorPurchaseUnit vendorPurchaseUnit, String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchaseUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    vendorPurchaseUnit.setPurchaseUnitId(purchaseUnitGeneralModel.getId());
    vendorPurchaseUnit.setPurchaseUnitName(purchaseUnitGeneralModel.getPurchaseUnitName());
  }

  public void setUserAccountSecurityService(
          IUserAccountSecurityService userAccountSecurityService) {
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setVendorPurchaseUnitRep(IObVendorPurchaseUnitRep vendorPurchaseUnitRep) {
    this.vendorPurchaseUnitRep = vendorPurchaseUnitRep;
  }
}
