package com.ebs.dda.commands.order.salesreturnorder;

import com.ebs.dda.commands.apis.ICommand;
import io.reactivex.Observable;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.imageio.ImageIO;
import javax.sql.DataSource;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.HashMap;

public class DObSalesReturnPdfGeneratorCommand implements ICommand {
  public String SALES_Return_CODE = "salesReturnCode";
  public String CALENDER_LOGO = "calenderLogo";
  private DataSource dataSource;

  @Override
  public Observable executeCommand(Object salesReturnCode) throws Exception {

    InputStream jrxmlFile =
        DObSalesReturnPdfGeneratorCommand.class.getResourceAsStream("/salesReturnPdf.jrxml");
    HashMap reportParameters = new HashMap();
    BufferedImage calenderLogo = ImageIO.read(getClass().getResource("/calendar.png"));

    reportParameters.put(SALES_Return_CODE, salesReturnCode);
    reportParameters.put(CALENDER_LOGO, calenderLogo);

    JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFile);

    JasperPrint report =
        JasperFillManager.fillReport(jasperReport, reportParameters, dataSource.getConnection());
    report.addStyle(prepareNormalStyle());

    byte[] pdf = JasperExportManager.exportReportToPdf(report);
    return Observable.just(new String(Base64.encodeBase64(pdf), "UTF-8"));
  }

  JRDesignStyle prepareNormalStyle() {
    JRDesignStyle jrDesignStyle = new JRDesignStyle();
    jrDesignStyle.setDefault(true);
    jrDesignStyle.setPdfFontName("/arial.ttf");
    jrDesignStyle.setPdfEncoding("Identity-H");
    return jrDesignStyle;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
}
