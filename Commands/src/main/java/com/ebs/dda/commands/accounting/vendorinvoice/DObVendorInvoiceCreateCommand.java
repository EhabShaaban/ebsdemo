package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.vendorinvoice.DObVendorInvoiceInstanceFactory;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;

public abstract class DObVendorInvoiceCreateCommand
        implements ICommand<DObVendorInvoiceCreationValueObject> {

    private MObVendorRep mObVendorRep;
    private DObPurchaseOrderGeneralModelRep dObPurchaseOrderHeaderGeneralModelRep;
    private IObOrderCompanyGeneralModelRep dObPurchaseOrderCompanySectionGeneralModelRep;
    private CObPaymentTermsRep cObPaymentTermsRep;
    private DObVendorInvoiceRep dObInvoiceRep;
    private CObCompanyRep cObCompanyRep;
    private CObCurrencyRep cObCurrencyRep;
    private CObPurchasingUnitRep purchasingUnitRep;
    private final DObVendorInvoiceStateMachine dObInvoiceStateMachine;
    private final DocumentObjectCodeGenerator documentObjectCodeGenerator;
    private final DObVendorInvoiceInstanceFactory instanceFactory;
    private DObPurchaseOrderRep dObPurchaseOrderRep;
    protected CommandUtils commandUtils;

    public DObVendorInvoiceCreateCommand(DocumentObjectCodeGenerator documentObjectCodeGenerator)
            throws Exception {
        dObInvoiceStateMachine = new DObVendorInvoiceStateMachine();
        commandUtils = new CommandUtils();
        this.documentObjectCodeGenerator = documentObjectCodeGenerator;
        instanceFactory = new DObVendorInvoiceInstanceFactory();
    }

    protected abstract void setVendorInvoiceItems(DObPurchaseOrder orderCode, DObVendorInvoice dObInvoice);

    protected abstract void setVendorInvoiceReferenceOrderPaymentProperties(IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder,
                                                                            DObPurchaseOrder orderCode);

    @Override
    public Observable<DObVendorInvoice> executeCommand(
            DObVendorInvoiceCreationValueObject invoiceCreationValueObject) throws Exception {

        DObVendorInvoice dObInvoice = instanceFactory.createVendorInvoiceInstance(invoiceCreationValueObject.getInvoiceType());

        setReferenceOrderData(invoiceCreationValueObject, dObInvoice);
        setVendorInvoiceDetailsData(invoiceCreationValueObject, dObInvoice);
        setVendorInvoiceGeneralData(dObInvoice, invoiceCreationValueObject);

        dObInvoiceRep.create(dObInvoice);

        return Observable.just(dObInvoice);
    }

    private void setVendorInvoiceGeneralData(DObVendorInvoice dObInvoice, DObVendorInvoiceCreationValueObject invoiceCreationValueObject) throws Exception {
        dObInvoiceStateMachine.initObjectState(dObInvoice);
        commandUtils.setCreationAndModificationInfoAndDate(dObInvoice);
        dObInvoice.setDocumentOwnerId(invoiceCreationValueObject.getDocumentOwnerId());
        generateCode(dObInvoice);
    }

    private void setVendorInvoiceDetailsData(DObVendorInvoiceCreationValueObject invoiceCreationValueObject, DObVendorInvoice dObInvoice) {
        IObVendorInvoiceDetails invoiceDetails = new IObVendorInvoiceDetails();
        commandUtils.setCreationAndModificationInfoAndDate(invoiceDetails);
        setVendor(invoiceDetails, invoiceCreationValueObject.getVendorCode());
        dObInvoice.setHeaderDetail(IDObVendorInvoice.invoiceDetailsAttr, invoiceDetails);
    }

    private void setReferenceOrderData(DObVendorInvoiceCreationValueObject invoiceCreationValueObject, DObVendorInvoice dObInvoice) {
        String orderCode = invoiceCreationValueObject.getPurchaseOrderCode();
        DObPurchaseOrder order =  getOrderByCode(orderCode);
        setVendorInvoiceOrderGeneralData(dObInvoice, order);
        setVendorInvoiceCompanyData(dObInvoice, order);
        setVendorInvoiceItems(order, dObInvoice);
    }

    private DObPurchaseOrder getOrderByCode(String orderCode) {
        return dObPurchaseOrderRep.findOneByUserCode(orderCode);
    }

    private void setVendorInvoiceOrderGeneralData(DObVendorInvoice dObInvoice, DObPurchaseOrder order) {
        IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder = new IObVendorInvoicePurchaseOrder();
        setPurchaseOrder(order, iObInvoicePurchaseOrder);
        setVendorInvoiceReferenceOrderPaymentProperties(iObInvoicePurchaseOrder, order);
        commandUtils.setCreationAndModificationInfoAndDate(iObInvoicePurchaseOrder);
        dObInvoice.setHeaderDetail(IDObVendorInvoice.purchaseOrderAttr, iObInvoicePurchaseOrder);
    }

    private void setVendorInvoiceCompanyData(DObVendorInvoice dObInvoice, DObPurchaseOrder orderCode) {
        IObVendorInvoiceCompanyData companyData = new IObVendorInvoiceCompanyData();
        setReferenceOrderCompanyData(companyData, orderCode);
        commandUtils.setCreationAndModificationInfoAndDate(companyData);
        dObInvoice.setHeaderDetail(IDObVendorInvoice.companyData, companyData);
    }

    private void generateCode(DObVendorInvoice dObInvoice) {
        String userCode =
                documentObjectCodeGenerator.generateUserCode(DObVendorInvoice.class.getSimpleName());
        dObInvoice.setUserCode(userCode);
    }

    private void setReferenceOrderCompanyData(IObVendorInvoiceCompanyData companyData, DObPurchaseOrder orderCode) {
        setPurchaseUnit(companyData, orderCode);
        setCompany(companyData, orderCode);
    }

    private void setPurchaseUnit(IObVendorInvoiceCompanyData companyData, DObPurchaseOrder order) {
        DObPurchaseOrderGeneralModel poHeaderGM =
                dObPurchaseOrderHeaderGeneralModelRep.findOneByUserCode(order.getUserCode());
        CObPurchasingUnit purUnit =
                purchasingUnitRep.findOneByUserCode(poHeaderGM.getPurchaseUnitCode());
        companyData.setPurchaseUnitId(purUnit.getId());
    }

    private void setCompany(IObVendorInvoiceCompanyData companyData, DObPurchaseOrder order) {
        IObOrderCompanyGeneralModel poCompanySectionGM =
                dObPurchaseOrderCompanySectionGeneralModelRep.findByUserCode(order.getUserCode());
        CObCompany cObCompany = cObCompanyRep.findOneByUserCode(poCompanySectionGM.getCompanyCode());
        companyData.setCompanyId(cObCompany.getId());
    }


    private void setPurchaseOrder(DObPurchaseOrder order,
                                  IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder) {
        iObInvoicePurchaseOrder.setPurchaseOrderId(order.getId());
    }

    private void setVendor(IObVendorInvoiceDetails invoiceDetails, String orderCode) {
        MObVendor mObVendor = mObVendorRep.findOneByUserCode(orderCode);
        invoiceDetails.setVendorId(mObVendor.getId());
    }

    protected void setPaymentTerm(IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder,
                                  String paymentTermCode) {
        CObPaymentTerms cObPaymentTerms = cObPaymentTermsRep.findOneByUserCode(paymentTermCode);
        iObInvoicePurchaseOrder.setPaymentTermId(cObPaymentTerms.getId());
    }

    protected void setCurrency(IObVendorInvoicePurchaseOrder iObInvoicePurchaseOrder,
                               String currencyCode) {
        CObCurrency cObCurrency = cObCurrencyRep.findOneByUserCode(currencyCode);
        iObInvoicePurchaseOrder.setCurrencyId(cObCurrency.getId());
    }

    public void setOrderRep(DObPurchaseOrderRep dObPurchaseOrderRep) {
        this.dObPurchaseOrderRep = dObPurchaseOrderRep;
    }

    public void setVendorRep(MObVendorRep mObVendorRep) {
        this.mObVendorRep = mObVendorRep;
    }

    public void setPurchaseOrderHeaderGeneralModelRep(
            DObPurchaseOrderGeneralModelRep dObPurchaseOrderHeaderGeneralModelRep) {
        this.dObPurchaseOrderHeaderGeneralModelRep = dObPurchaseOrderHeaderGeneralModelRep;
    }

    public void setPurchaseOrderCompanySectionGeneralModelRep(
            IObOrderCompanyGeneralModelRep dObPurchaseOrderCompanySectionGeneralModelRep) {
        this.dObPurchaseOrderCompanySectionGeneralModelRep =
                dObPurchaseOrderCompanySectionGeneralModelRep;
    }

    public void setPaymentTermsRep(CObPaymentTermsRep cObPaymentTermsRep) {
        this.cObPaymentTermsRep = cObPaymentTermsRep;
    }

    public void setVendorInvoiceRep(DObVendorInvoiceRep dObInvoiceRep) {
        this.dObInvoiceRep = dObInvoiceRep;
    }

    public void setCompanyRep(CObCompanyRep cObCompanyRep) {
        this.cObCompanyRep = cObCompanyRep;
    }

    public void setCurrencyRep(CObCurrencyRep cObCurrencyRep) {
        this.cObCurrencyRep = cObCurrencyRep;
    }

    public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
        this.purchasingUnitRep = purchasingUnitRep;
    }

    public void setUserAccountSecurityService(
            IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

}

