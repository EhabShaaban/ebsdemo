package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemPurchaseOrderSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemVendorSubAccount;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;

import java.math.BigDecimal;
import java.util.List;

public class IObVendorInvoiceJournalEntryDetailsInitializer implements IIObJournalEntryDetailsInitializer<DObVendorInvoiceJournalDetailPreparationGeneralModel> {

    private IObAccountingDocumentAccountingDetailsRep accountingDocumentAccountingDetailsRep;
    private final CommandUtils commandUtils;

    public IObVendorInvoiceJournalEntryDetailsInitializer() {
        commandUtils = new CommandUtils();
    }

    @Override
    public IObVendorInvoiceJournalEntryDetailsInitializer addCredit(DObJournalEntry journalEntry, DObVendorInvoiceJournalDetailPreparationGeneralModel journalDetailPreparationGeneralModel) {
        IObAccountingDocumentAccountingDetails creditAccount = accountingDocumentAccountingDetailsRep.findOneByRefInstanceIdAndAccountingEntryAndObjectTypeCode(journalDetailPreparationGeneralModel.getId(), AccountingEntry.CREDIT.name(), IDocumentTypes.VENDOR_INVOICE);
        IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount =
                new IObJournalEntryItemVendorSubAccount();
        journalEntryItemVendorSubAccount.setAccountId(creditAccount.getGlAccountId());
        journalEntryItemVendorSubAccount.setDocumentCurrencyId(journalDetailPreparationGeneralModel.getDocumentCurrencyId());
        journalEntryItemVendorSubAccount.setCompanyCurrencyId(journalDetailPreparationGeneralModel.getCompanyCurrencyId());
        journalEntryItemVendorSubAccount
                .setCompanyCurrencyPrice(new BigDecimal(journalDetailPreparationGeneralModel.getCurrencyPrice()));
        journalEntryItemVendorSubAccount.setCredit(creditAccount.getAmount());
        journalEntryItemVendorSubAccount.setDebit(BigDecimal.ZERO);
        journalEntryItemVendorSubAccount
                .setCompanyExchangeRateId(journalDetailPreparationGeneralModel.getExchangeRateId());
        journalEntryItemVendorSubAccount.setType(journalDetailPreparationGeneralModel.getAccountLedger());
        journalEntryItemVendorSubAccount
                .setSubAccountId(creditAccount.getGlSubAccountId());

        commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemVendorSubAccount);
        journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, journalEntryItemVendorSubAccount);
        return this;
    }

    @Override
    public IObVendorInvoiceJournalEntryDetailsInitializer addDebit(DObJournalEntry journalEntry, DObVendorInvoice vendorInvoice, DObVendorInvoiceJournalDetailPreparationGeneralModel journalDetailPreparationGeneralModel) {
        List<IObAccountingDocumentAccountingDetails> debitAccounts = accountingDocumentAccountingDetailsRep.findByRefInstanceIdAndAccountingEntryAndObjectTypeCode(journalDetailPreparationGeneralModel.getId(), AccountingEntry.DEBIT.name(), IDocumentTypes.VENDOR_INVOICE);

        for (IObAccountingDocumentAccountingDetails debitAccount : debitAccounts) {
            IObJournalEntryItemPurchaseOrderSubAccount journalEntryItemPurchaseOrderSubAccount = new IObJournalEntryItemPurchaseOrderSubAccount();
            journalEntryItemPurchaseOrderSubAccount.setAccountId(debitAccount.getGlAccountId());
            journalEntryItemPurchaseOrderSubAccount.setDocumentCurrencyId(journalDetailPreparationGeneralModel.getDocumentCurrencyId());
            journalEntryItemPurchaseOrderSubAccount.setCompanyCurrencyId(journalDetailPreparationGeneralModel.getCompanyCurrencyId());
            journalEntryItemPurchaseOrderSubAccount.setCompanyCurrencyPrice(new BigDecimal(journalDetailPreparationGeneralModel.getCurrencyPrice()));
            journalEntryItemPurchaseOrderSubAccount.setCompanyExchangeRateId(journalDetailPreparationGeneralModel.getExchangeRateId());
            journalEntryItemPurchaseOrderSubAccount.setSubAccountId(debitAccount.getGlSubAccountId());
            journalEntryItemPurchaseOrderSubAccount.setCredit(BigDecimal.ZERO);
            journalEntryItemPurchaseOrderSubAccount.setDebit(debitAccount.getAmount());
            commandUtils.setCreationAndModificationInfoAndDate(journalEntryItemPurchaseOrderSubAccount);
            journalEntry.addLineDetail(IDObJournalEntry.journalItemAttr, journalEntryItemPurchaseOrderSubAccount);
        }
        return this;
    }

    public void setUserAccountSecurityService(IUserAccountSecurityService userAccountSecurityService) {
        commandUtils.setUserAccountSecurityService(userAccountSecurityService);
    }

    public void setAccountingDocumentAccountingDetailsRep(IObAccountingDocumentAccountingDetailsRep accountingDocumentAccountingDetailsRep) {
        this.accountingDocumentAccountingDetailsRep = accountingDocumentAccountingDetailsRep;
    }
}
