package com.ebs.dda.commands;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommandUtils {

  private static final int DEFAULT_SCALE_FACTOR = 10;
  private IUserAccountSecurityService userAccountSecurityService;

  public static BigDecimal getDivideResult(BigDecimal firstNumber, BigDecimal secondNumber) {
    try {
      return firstNumber.divide(secondNumber);
    } catch (ArithmeticException ex) {
      return firstNumber.divide(secondNumber, DEFAULT_SCALE_FACTOR, RoundingMode.FLOOR);
    }
  }

  @Deprecated
  // TODO change it from public to private
  public void setCreationAndModificationInfo(BusinessObject businessObject) {
    String loggedInUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    businessObject.setCreationInfo(loggedInUserInfo);
    businessObject.setModificationInfo(loggedInUserInfo);
  }

  public void setCreationAndModificationInfoAndDate(BusinessObject businessObject) {
    setCreationAndModificationInfo(businessObject);
    DateTime currentDate = new DateTime();
    businessObject.setModifiedDate(currentDate);
    businessObject.setCreationDate(currentDate);
  }

  public void setModificationInfo(BusinessObject businessObject) {
    String loggedInUserInfo = userAccountSecurityService.getLoggedInUser().toString();
    businessObject.setModificationInfo(loggedInUserInfo);
  }

  public void setModificationInfoAndModificationDate(BusinessObject businessObject) {
    DateTime currentDateTime = new DateTime();
    String loggedInUser = userAccountSecurityService.getLoggedInUser().toString();
    businessObject.setModifiedDate(currentDateTime);
    businessObject.setModificationInfo(loggedInUser);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  public String getLoggedInUser() {
    return this.userAccountSecurityService.getLoggedInUser().toString();
  }

  public IBDKUser getLoggedInUserObject() {
    return this.userAccountSecurityService.getLoggedInUser();
  }

  public Long getLoggedInUserId() {
    return this.userAccountSecurityService.getLoggedInUser().getUserId();
  }

  public <T extends InformationObject> List<T> convertCSVFileToEntityList(
      MultipartFile attachment, Class<T> entityClass) throws Exception {
    HeaderColumnNameMappingStrategy<T> strategy = getMappingStrategy(entityClass);
    File attachmentFile = convertMultiPartToFile(attachment);
    CsvToBean<T> csvToBean = getCSVToBean(strategy, attachmentFile);
    deleteTempAttachmentFile(attachmentFile);
    return csvToBean.parse();
  }

  private <T extends InformationObject> CsvToBean<T> getCSVToBean(
      HeaderColumnNameMappingStrategy<T> strategy, File attachmentFile)
      throws FileNotFoundException {
    CSVReader csvReader = new CSVReader(new FileReader(attachmentFile));
    CsvToBean<T> csvToBean = new CsvToBean<>();
    csvToBean.setCsvReader(csvReader);
    csvToBean.setMappingStrategy(strategy);
    return csvToBean;
  }

  private void deleteTempAttachmentFile(File attachmentFile) {
    File f = new File(attachmentFile.toURI());
    f.delete();
  }

  private <T extends InformationObject> HeaderColumnNameMappingStrategy<T> getMappingStrategy(
      Class<T> entityClass) {
    HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(entityClass);
    return strategy;
  }

  private File convertMultiPartToFile(MultipartFile attachment) throws Exception {
    String fileName = attachment.getOriginalFilename();
    String prefix = fileName.substring(fileName.lastIndexOf("."));
    File file = File.createTempFile(fileName, prefix);
    attachment.transferTo(file);
    return file;
  }

  public Set<String> initCurrenctStates(String... activeStates) {
    Set<String> states = new HashSet<>(Arrays.asList(activeStates));
    return states;
  }
}
