package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dac.dbo.exceptions.NotSupportedPOTypeException;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.condition.adapter.ConditionManager;
import com.ebs.dac.foundation.realization.condition.resolvers.EntityAttributeResolverContext;
import com.ebs.dac.foundation.realization.processing.DataObjectProcessor;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.CObApprovalPolicy;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentTypeEnum;
import com.ebs.dda.approval.dbo.jpa.entities.generalmodel.ApprovalPolicyControlPointUsersGeneralModel;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObApprovalPolicyRep;
import com.ebs.dda.approval.dbo.jpa.repositories.generalmodel.ApprovalPolicyControlPointUsersGeneralModelRep;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.order.purchaseorder.utils.DObPurchaseOrderCommandUtils;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderApprovalCycles;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.exceptions.MoreThanOneMatchedApprovalPolicyException;
import com.ebs.dda.purchases.exceptions.NoMatchingApprovalPolicyException;
import io.reactivex.Observable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DObPurchaseOrderSubmitForApprovalCommand implements ICommand {

  private IUserAccountSecurityService userAccountSecurityService;
  private CommandUtils commandUtils;
  private CObApprovalPolicyRep approvalPolicyRep;
  private IObOrderApprovalCycleRep approvalCycleRep;
  private DObPurchaseOrderRep purchaseOrderRep;
  private ApprovalPolicyControlPointUsersGeneralModelRep approvalPolicyControlPointUsersRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;

  public DObPurchaseOrderSubmitForApprovalCommand() {
    this.commandUtils = new CommandUtils();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
    this.commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setApprovalPolicyRep(CObApprovalPolicyRep approvalPolicyRep) {
    this.approvalPolicyRep = approvalPolicyRep;
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setApprovalPolicyControlPointUsersRep(
      ApprovalPolicyControlPointUsersGeneralModelRep approvalPolicyControlPointUsersRep) {
    this.approvalPolicyControlPointUsersRep = approvalPolicyControlPointUsersRep;
  }

  public void setApprovalCycleRep(IObOrderApprovalCycleRep approvalCycleRep) {
    this.approvalCycleRep = approvalCycleRep;
  }

  @Override
  public Observable<String> executeCommand(Object purchaseOrderCode) throws Exception {
    String purchaseOrderCodeStr = purchaseOrderCode.toString();
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCodeStr);

    DocumentTypeEnum documentType = getDocumentType(purchaseOrder.getObjectTypeCode());

    String matchedApprovalPolicyCode = getMatchedApprovalPolicy(purchaseOrderCodeStr, documentType);
    addPurchaseOrderApprovalCycleDetails(purchaseOrder, matchedApprovalPolicyCode);

    DObPurchaseOrderCommandUtils.setPurchaseOrderTargetState(
        purchaseOrder, IPurchaseOrderActionNames.SUBMIT_FOR_APPROVAL);
    updatePurchaseOrderModificationData(purchaseOrder);
    purchaseOrderRep.update(purchaseOrder);
    return Observable.just("SUCCESS");
  }

  private void addPurchaseOrderApprovalCycleDetails(
      DObOrderDocument purchaseOrder, String matchedApprovalPolicyCode) throws Exception {
    List<ApprovalPolicyControlPointUsersGeneralModel> mainUsers =
        approvalPolicyControlPointUsersRep
            .findByApprovalPolicyCodeAndIsMainTrueOrderByControlPointCodeAsc(
                matchedApprovalPolicyCode);
    IObOrderApprovalCycle approvalCycle = createNewApprovalCycle(purchaseOrder);
    for (ApprovalPolicyControlPointUsersGeneralModel mainUser : mainUsers) {
      IObOrderApprover orderApprover = new IObOrderApprover();
      orderApprover.setControlPointId(mainUser.getControlPointId());
      orderApprover.setApproverId(mainUser.getUserId());
      this.commandUtils.setCreationAndModificationInfoAndDate(orderApprover);
      approvalCycle.addLineDetail(IIObOrderApprovalCycles.approversAttr, orderApprover);
    }
    purchaseOrder.addLineDetail(IDObPurchaseOrder.approvalCyclesAttr, approvalCycle);
  }

  private IObOrderApprovalCycle createNewApprovalCycle(DObOrderDocument purchaseOrder)
      throws Exception {
    IObOrderApprovalCycle approvalCycle = new IObOrderApprovalCycle();
    approvalCycle.setStartDate(new DateTime(DateTimeZone.UTC));

    this.commandUtils.setCreationAndModificationInfoAndDate(approvalCycle);
    IObOrderApprovalCycle lastApprovalCycle =
        approvalCycleRep.findTopByRefInstanceIdOrderByIdDesc(purchaseOrder.getId());
    Long approvalCycleNum =
        lastApprovalCycle == null ? 1L : (lastApprovalCycle.getApprovalCycleNum() + 1);
    approvalCycle.setApprovalCycleNum(approvalCycleNum);
    return approvalCycle;
  }

  private boolean isValidCondition(String condition, BusinessObject entity) throws Exception {
    ConditionManager conditionManager = new ConditionManager();
    Set<String> conditionVariables = conditionManager.getConditionVariables(condition);
    Map<String, Object> objectAttributeMap = buildObjectAttributeMap(conditionVariables, entity);
    return conditionManager.executeCondition(objectAttributeMap, condition);
  }

  private Map<String, Object> buildObjectAttributeMap(
      Set<String> conditionVariables, BusinessObject entity) throws Exception {
    DataObjectProcessor processor = new DataObjectProcessor(entity);
    Map<String, Object> attributesValuesMap =
        processor.getAttributesValuesMap(entity, conditionVariables);
    EntityAttributeResolverContext attributesResolver = new EntityAttributeResolverContext();
    Map<String, Object> resolvedMap = attributesResolver.resolveAttributes(attributesValuesMap);
    return resolvedMap;
  }

  private String getMatchedApprovalPolicy(String purchaseOrderCode, DocumentTypeEnum documentType)
      throws Exception {
    List<CObApprovalPolicy> approvalPolicies = approvalPolicyRep.findByDocumentType(documentType);
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    String matchedApprovalPolicyCode = null;
    for (CObApprovalPolicy approvalPolicy : approvalPolicies) {
      boolean validCondition =
          isValidCondition(approvalPolicy.getCondition(), purchaseOrderGeneralModel);
      if (validCondition && (matchedApprovalPolicyCode != null)) {
        throw new MoreThanOneMatchedApprovalPolicyException();
      }
      if (validCondition) {
        matchedApprovalPolicyCode = approvalPolicy.getUserCode();
      }
    }
    if (matchedApprovalPolicyCode == null) {
      throw new NoMatchingApprovalPolicyException();
    }
    return matchedApprovalPolicyCode;
  }

  private DocumentTypeEnum getDocumentType(String orderTypeCode) {
    if (orderTypeCode.equals(OrderTypeEnum.LOCAL_PO.name())) {
      return DocumentTypeEnum.LOCAL_PO;
    } else if (orderTypeCode.equals(OrderTypeEnum.IMPORT_PO.name())) {
      return DocumentTypeEnum.IMPORT_PO;
    }
    throw new NotSupportedPOTypeException();
  }

  protected void updatePurchaseOrderModificationData(DObOrderDocument purchaseOrder) {
    this.commandUtils.setModificationInfoAndModificationDate(purchaseOrder);
  }
}
