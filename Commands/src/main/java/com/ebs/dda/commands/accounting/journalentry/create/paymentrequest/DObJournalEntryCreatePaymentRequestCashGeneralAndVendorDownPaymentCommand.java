package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObLeafGLAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public abstract class DObJournalEntryCreatePaymentRequestCashGeneralAndVendorDownPaymentCommand
    extends DObJournalEntryCreatePaymentRequestCommand {

  private HObLeafInChartOfAccountsRep leafInChartOfAccountsRep;
  private HObLeafGLAccount leafGLAccount;

  public DObJournalEntryCreatePaymentRequestCashGeneralAndVendorDownPaymentCommand() {
  }

  protected Observable<String> executeCashGeneralAndVendorDownPaymentCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    executeParentCommand(paymentRequestValueObject);
    initGeneralTypeEntities();
    setJournalItemsData(journalEntryItems);
    executeTransaction(paymentRequestJournalEntry, journalEntryItems);
    return Observable.just(paymentRequestJournalEntry.getUserCode());
  }

  private void initGeneralTypeEntities() {
    Long glAccountId = paymentRequestAccountingDetailsDebit.getGlAccountId();
    leafGLAccount = leafInChartOfAccountsRep.findOneById(glAccountId);
  }

  private void setJournalItemsData(List<IObJournalEntryItem> journalEntryItems) {
    setJournalItemGeneralTypeData(journalEntryItems);
    setJournalItemCashData(journalEntryItems);
  }

  private void setJournalItemCashData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItemTreasurySubAccount journalEntryItem = new IObJournalEntryItemTreasurySubAccount();
    setAccountAndSubAccountForCashType(journalEntryItem);
    setDetailsForCashType(journalEntryItem);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
    journalEntryItems.add(journalEntryItem);
  }

  private void setDetailsForCashType(IObJournalEntryItem journalEntryItem) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItem.setCredit(paymentDetails.getNetAmount());
    journalEntryItem.setDebit(BigDecimal.ZERO);
    journalEntryItem.setDocumentCurrencyId(currencyId);
    journalEntryItem.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItem.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItem.setCompanyExchangeRateId(exchangeRateId);
  }

  private void setAccountAndSubAccountForCashType(IObJournalEntryItemTreasurySubAccount journalEntryItem) {
    journalEntryItem.setAccountId(paymentRequestAccountingDetailsCredit.getGlAccountId());
    journalEntryItem.setSubAccountId(paymentRequestAccountingDetailsCredit.getGlSubAccountId());
  }

  private void setJournalItemGeneralTypeData(List<IObJournalEntryItem> journalEntryItems) {
    IObJournalEntryItem journalEntryItem = createJournalEntryAndSetSubAccountBasedOnLedger();
    Long accountId = paymentRequestAccountingDetailsDebit.getGlAccountId();
    journalEntryItem.setAccountId(accountId);
    setJournalItemDetailsForGeneralType(journalEntryItem);
    commandUtils.setCreationAndModificationInfoAndDate(journalEntryItem);
    journalEntryItems.add(journalEntryItem);
  }

  private IObJournalEntryItem createJournalEntryAndSetSubAccountBasedOnLedger() {
    String ledger = leafGLAccount.getLeadger();
    if (ledger == null) {
      return new IObJournalEntryItemGeneral();
    } else {
      return createJournalEntryAndSetSubAccountIfLedgerExists(ledger);
    }
  }

  private IObJournalEntryItem createJournalEntryAndSetSubAccountIfLedgerExists(String ledger) {
    IObJournalEntryItem journalEntryItem;
    if (ledger.equals(SubLedgers.PO.name())) {
      journalEntryItem = new IObJournalEntryItemPurchaseOrderSubAccount();
      addSubAccountForPOType(journalEntryItem);
    } else if (isLedgerVendor(ledger)) {
      journalEntryItem = new IObJournalEntryItemVendorSubAccount();
      addSubAccountAndLedgerForVendorType(journalEntryItem);
    } else {
      journalEntryItem = new IObJournalEntryItemGeneral();
    }
    return journalEntryItem;
  }

  private boolean isLedgerVendor(String ledger) {
    return ledger.equals(SubLedgers.Local_Vendors.name())
        || ledger.equals(SubLedgers.Import_Vendors.name());
  }

  private void addSubAccountAndLedgerForVendorType(IObJournalEntryItem journalEntryItem) {
    Long subAccountId = paymentRequestAccountingDetailsDebit.getGlSubAccountId();
    String subLedger = paymentRequestAccountingDetailsDebit.getSubLedger();
    ((IObJournalEntryItemVendorSubAccount) journalEntryItem).setSubAccountId(subAccountId);
    ((IObJournalEntryItemVendorSubAccount) journalEntryItem).setType(subLedger);
  }

  private void addSubAccountForPOType(IObJournalEntryItem journalEntryItem) {
    Long subAccountId = paymentRequestAccountingDetailsDebit.getGlSubAccountId();
    ((IObJournalEntryItemPurchaseOrderSubAccount) journalEntryItem).setSubAccountId(subAccountId);
  }

  private void setJournalItemDetailsForGeneralType(IObJournalEntryItem journalEntryItem) {
    BigDecimal currencyPrice = paymentPostingDetails.getCurrencyPrice();
    Long currencyId = paymentDetails.getCurrencyId();
    Long companyCurrencyId = paymentRequestPaymentDetailsGeneralModel.getCompanyCurrencyId();
    Long exchangeRateId = paymentPostingDetails.getExchangeRateId();
    journalEntryItem.setCredit(BigDecimal.ZERO);
    journalEntryItem.setDebit(paymentDetails.getNetAmount());
    journalEntryItem.setDocumentCurrencyId(currencyId);
    journalEntryItem.setCompanyCurrencyId(companyCurrencyId);
    journalEntryItem.setCompanyCurrencyPrice(currencyPrice);
    journalEntryItem.setCompanyExchangeRateId(exchangeRateId);
  }

  public void setLeafInChartOfAccountsRep(HObLeafInChartOfAccountsRep leafInChartOfAccountsRep) {
    this.leafInChartOfAccountsRep = leafInChartOfAccountsRep;
  }
}
