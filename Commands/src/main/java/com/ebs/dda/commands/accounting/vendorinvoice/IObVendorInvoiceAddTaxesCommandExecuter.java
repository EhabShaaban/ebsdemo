package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import io.reactivex.Observable;

public class IObVendorInvoiceAddTaxesCommandExecuter {
    private IObVendorInvoiceTaxesAddCommand vendorInvoiceTaxesAddCommand;
    private IObLocalGoodsInvoiceTaxesAddCommand localGoodsInvoiceTaxesAddCommand;

    public Observable<String> executeCommand(DObVendorInvoice vendorInvoice) throws Exception {
        String vendorInvoiceType = vendorInvoice.getDiscriminatorValue();
        switch (vendorInvoiceType) {
            case VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE:
            case VendorInvoiceTypeEnum.Values.SHIPMENT_INVOICE:
            case VendorInvoiceTypeEnum.Values.INSURANCE_INVOICE:
                return vendorInvoiceTaxesAddCommand.executeCommand(vendorInvoice);
            case VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE:
                return localGoodsInvoiceTaxesAddCommand.executeCommand(vendorInvoice);
            case VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE:
                return Observable.just("SUCCESS");
        }
        throw new IllegalArgumentException("Unsupported Vendor Invoice Type");
    }

    public void setVendorInvoiceTaxesAddCommand(IObVendorInvoiceTaxesAddCommand vendorInvoiceTaxesAddCommand) {
        this.vendorInvoiceTaxesAddCommand = vendorInvoiceTaxesAddCommand;
    }

    public void setLocalGoodsInvoiceTaxesAddCommand(IObLocalGoodsInvoiceTaxesAddCommand localGoodsInvoiceTaxesAddCommand) {
        this.localGoodsInvoiceTaxesAddCommand = localGoodsInvoiceTaxesAddCommand;
    }
}
