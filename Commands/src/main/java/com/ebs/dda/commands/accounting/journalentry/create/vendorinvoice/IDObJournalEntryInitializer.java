package com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice;

import com.ebs.dda.jpa.accounting.DObAccountingDocument;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public interface IDObJournalEntryInitializer<AccountingDocumentType extends DObAccountingDocument> {
    DObJournalEntry initializeJournalEntry(DObVendorInvoice vendorInvoice, DObJournalEntryCreateValueObject vendorInvoiceValueObject);
    void initializeJournalEntryDetails(IObJournalEntryItem journalEntryDetails);
}
