package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemValueObject;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import io.reactivex.Observable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class IObSalesInvoiceUpdateReturnedItemsCommand
    implements ICommand<IObSalesReturnOrderItemValueObject> {
  private CommandUtils commandUtils;
  private IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemRep;
  private DObSalesReturnOrderGeneralModelRep salesReturnOrderRep;
  private IObSalesInvoiceItemsRep salesInvoiceItemsRep;
  private IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep;

  public IObSalesInvoiceUpdateReturnedItemsCommand() {
    commandUtils = new CommandUtils();
  }

  @Transactional
  @Override
  public Observable<String> executeCommand(
      IObSalesReturnOrderItemValueObject returnOrderItemValueObject) throws Exception {
    String salesReturnOrderCode = returnOrderItemValueObject.getSalesReturnOrderCode();
    List<IObSalesReturnOrderItemGeneralModel> salesReturnOrderItems =
        salesReturnOrderItemRep.findBySalesReturnOrderCodeOrderByIdDesc(salesReturnOrderCode);
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderRep.findOneByUserCode(salesReturnOrderCode);
    String salesInvoiceCode = salesReturnOrderGeneralModel.getSalesInvoiceCode();
    List<IObSalesInvoiceItemsGeneralModel> invoiceItems =
        salesInvoiceItemsGeneralModelRep.findByInvoiceCode(salesInvoiceCode);
    List<IObSalesInvoiceItem> salesInvoiceItems = new LinkedList<>();

    for (IObSalesReturnOrderItemGeneralModel salesReturnOrderItem : salesReturnOrderItems) {
      String returnedItemCode = salesReturnOrderItem.getItemCode();
      String returnedItemOrderUnit = salesReturnOrderItem.getOrderUnitCode();
      BigDecimal returnedQuantity = salesReturnOrderItem.getReturnQuantity();
      Long invoiceItemId = getItemId(invoiceItems, returnedItemCode, returnedItemOrderUnit);
      IObSalesInvoiceItem salesInvoiceItem = salesInvoiceItemsRep.findOneById(invoiceItemId);
      BigDecimal invoiceItemReturnedQuantity =
          salesInvoiceItem.getReturnedQuantity().add(returnedQuantity);
      salesInvoiceItem.setReturnedQuantity(invoiceItemReturnedQuantity);
      commandUtils.setModificationInfoAndModificationDate(salesInvoiceItem);
      salesInvoiceItems.add(salesInvoiceItem);
    }

    salesInvoiceItemsRep.saveAll(salesInvoiceItems);
    return Observable.just("SUCCESS");
  }

  private Long getItemId(
      List<IObSalesInvoiceItemsGeneralModel> invoiceItems, String itemCode, String orderUnit) {
    List<IObSalesInvoiceItemsGeneralModel> filteredInvoiceItems =
        invoiceItems.stream()
            .filter(
                item ->
                    (item.getItemCode().equals(itemCode)
                        && item.getOrderUnitCode().equals(orderUnit)))
            .collect(Collectors.toList());
    return filteredInvoiceItems.get(0).getId();
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setSalesReturnOrderItemRep(
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemRep) {
    this.salesReturnOrderItemRep = salesReturnOrderItemRep;
  }

  public void setSalesReturnOrderRep(DObSalesReturnOrderGeneralModelRep salesReturnOrderRep) {
    this.salesReturnOrderRep = salesReturnOrderRep;
  }

  public void setSalesInvoiceItemsRep(IObSalesInvoiceItemsRep salesInvoiceItemsRep) {
    this.salesInvoiceItemsRep = salesInvoiceItemsRep;
  }

  public void setSalesInvoiceItemsGeneralModelRep(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
    this.salesInvoiceItemsGeneralModelRep = salesInvoiceItemsGeneralModelRep;
  }
}
