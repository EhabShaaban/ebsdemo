package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRep;

import java.math.BigDecimal;
import java.util.List;

public class DObLocalGoodsInvoiceAccountDeterminationCommand extends DObVendorInvoiceAccountDeterminationCommand {

    private IObVendorInvoiceTaxesGeneralModelRep taxesRep;

    @Override
    protected Long getOrderId(List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems) {
        return groupedItems.get(0).getOrderId();
    }

    @Override
    protected BigDecimal getGroupedItemsAmount(String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> groupedItems) {
        BigDecimal totalTaxPercentage = getTotalTaxPercentage(vendorInvoiceCode);
        BigDecimal amount = groupedItems.stream().map(DObVendorInvoiceItemAccountsPreparationGeneralModel::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        return getItemAmountAfterTaxes(totalTaxPercentage, amount);
    }

    private BigDecimal getItemAmountAfterTaxes(BigDecimal totalTaxPercentage, BigDecimal amount) {
        return amount.add((amount.multiply(totalTaxPercentage)).divide(BigDecimal.valueOf(100)));
    }

    private BigDecimal getTotalTaxPercentage(String vendorInvoiceCode) {
        List<IObVendorInvoiceTaxesGeneralModel> taxes = taxesRep.findAllByInvoiceCode(vendorInvoiceCode);
        return taxes.stream().map(IObVendorInvoiceTaxesGeneralModel::getTaxPercentage).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void setTaxesRep(IObVendorInvoiceTaxesGeneralModelRep taxesRep) {
        this.taxesRep = taxesRep;
    }
}
