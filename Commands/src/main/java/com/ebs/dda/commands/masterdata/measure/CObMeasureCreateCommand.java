package com.ebs.dda.commands.masterdata.measure;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.ConfigurationObjectCodeGenrator;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.measure.CObMeasureCreationValueObject;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import io.reactivex.Observable;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class CObMeasureCreateCommand implements ICommand<CObMeasureCreationValueObject> {

  private ConfigurationObjectCodeGenrator configurationObjectCodeGenrator;
  private IUserAccountSecurityService userAccountSecurityService;
  private CObMeasureRep measureRep;
  private CommandUtils commandUtils;

  public CObMeasureCreateCommand(ConfigurationObjectCodeGenrator configurationObjectCodeGenrator) {
    this.configurationObjectCodeGenrator = configurationObjectCodeGenrator;
    commandUtils = new CommandUtils();
  }

  @Override
  public Observable<String> executeCommand(
      CObMeasureCreationValueObject unitOfMeasureCreationValueObject) throws Exception {
    String userCode = configurationObjectCodeGenrator.generateUserCode(CObMeasure.class);
    CObMeasure unitOfMeasure = new CObMeasure();
    setUnitOfMeasureData(unitOfMeasure, unitOfMeasureCreationValueObject, userCode);
    measureRep.create(unitOfMeasure);
    return Observable.just(userCode);
  }

  private void setUnitOfMeasureData(
      CObMeasure unitOfMeasure,
      CObMeasureCreationValueObject unitOfMeasureCreationValueObject,
      String userCode) {
    unitOfMeasure.setUserCode(userCode);

    Set<String> currentStates = new HashSet<>();
    currentStates.add("Active");
    unitOfMeasure.setCurrentStates(currentStates);

    LocalizedString unitOfMeasureName = new LocalizedString();
    unitOfMeasureName.setValue(
        new Locale("ar"), unitOfMeasureCreationValueObject.getUnitOfMeasureName());
    unitOfMeasureName.setValue(
        new Locale("en"), unitOfMeasureCreationValueObject.getUnitOfMeasureName());

    unitOfMeasure.setName(unitOfMeasureName);

    unitOfMeasure.setSymbol(unitOfMeasureName);

    unitOfMeasure.setStandard(unitOfMeasureCreationValueObject.getIsStandard());

    commandUtils.setCreationAndModificationInfoAndDate(unitOfMeasure);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }
}
