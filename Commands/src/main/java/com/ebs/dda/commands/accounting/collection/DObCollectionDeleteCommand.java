package com.ebs.dda.commands.accounting.collection;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import io.reactivex.Observable;

public class DObCollectionDeleteCommand implements ICommand {

  private DObCollectionRep<DObCollection> collectionRep;

  public DObCollectionDeleteCommand() {
  }

  @Override
  public Observable executeCommand(Object collectionCode) throws Exception {
    DObCollection collection = getCollection(collectionCode.toString());
    collectionRep.delete(collection);
    return Observable.empty();
  }

  private DObCollection getCollection(String collectionCode) {
    return collectionRep.findOneByUserCode(collectionCode);
  }

  public void setCollectionRep(DObCollectionRep<DObCollection> collectionRep) {
    this.collectionRep = collectionRep;
  }
}
