package com.ebs.dda.commands.accounting.journalentry.create.paymentrequest;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import io.reactivex.Observable;

public class DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand
    extends DObJournalEntryCreatePaymentRequestCashGeneralAndVendorDownPaymentCommand {

  public DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand() {
  }

  @Override
  public Observable<String> executeCommand(
      DObJournalEntryCreateValueObject paymentRequestValueObject) throws Exception {
    return executeCashGeneralAndVendorDownPaymentCommand(paymentRequestValueObject);
  }

}
