package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CostingExchangeRateNotPaidStrategy implements ICalculateCostingExchangeRateStrategy {

  private final CostingExchangeRateUtils costingExchangeRateUtils;

  public CostingExchangeRateNotPaidStrategy(CostingExchangeRateUtils costingExchangeRateUtils) {
    this.costingExchangeRateUtils = costingExchangeRateUtils;
  }

  @Override
  public BigDecimal calculate(DObGoodsReceiptGeneralModel goodsReceipt) {
    DObVendorInvoiceGeneralModel vendorInvoice =
        costingExchangeRateUtils.getVendorInvoice(goodsReceipt.getRefDocumentCode());

    return costingExchangeRateUtils.getLatestExchangeRate(
        vendorInvoice, goodsReceipt.getCompanyCode());
  }

  @Override
  public void setVendorInvoiceSummary(IObVendorInvoiceSummary vendorInvoiceSummary) {
    this.costingExchangeRateUtils.setVendorInvoiceSummary(vendorInvoiceSummary);
  }
}
