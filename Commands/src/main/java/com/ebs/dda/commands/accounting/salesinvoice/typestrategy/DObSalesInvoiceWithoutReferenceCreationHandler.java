package com.ebs.dda.commands.accounting.salesinvoice.typestrategy;

import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompany;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class DObSalesInvoiceWithoutReferenceCreationHandler extends DObSalesInvoiceCreationHandler {

  public DObSalesInvoiceWithoutReferenceCreationHandler() throws Exception {
    super();
  }

  @Override
  protected Long getCompanyId(DObSalesInvoiceCreationValueObject valueObject) {
    CObCompany cObCompany = cObCompanyRep.findOneByUserCode(valueObject.getCompanyCode());
    return cObCompany.getId();
  }

  @Override
  protected String getCompanyCode(DObSalesInvoiceCreationValueObject valueObject) {
    return valueObject.getCompanyCode();
  }

  @Override
  protected List<IObSalesInvoiceTaxes> getSalesInvoiceTaxes(
      DObSalesInvoiceCreationValueObject valueObject) {

    List<IObSalesInvoiceTaxes> salesInvoiceCompanyTaxesDetailsList = new ArrayList<>();

    List<LObCompanyTaxesGeneralModel> companyTaxDetailsList =
        companyTaxesGeneralModelRep.findAllByCompanyCode(getCompanyCode(valueObject));

    for (int i = 0; i < companyTaxDetailsList.size(); i++) {
      IObSalesInvoiceTaxes salesInvoiceCompanyTaxesDetails = new IObSalesInvoiceTaxes();
      commandUtils.setCreationAndModificationInfoAndDate(salesInvoiceCompanyTaxesDetails);

      salesInvoiceCompanyTaxesDetails.setName(companyTaxDetailsList.get(i).getName());
      salesInvoiceCompanyTaxesDetails.setCode(companyTaxDetailsList.get(i).getUserCode());
      salesInvoiceCompanyTaxesDetails.setAmount(BigDecimal.ZERO);
      salesInvoiceCompanyTaxesDetailsList.add(salesInvoiceCompanyTaxesDetails);
    }
    return salesInvoiceCompanyTaxesDetailsList;

  }

  @Override
  protected IObSalesInvoiceBusinessPartner getBusinessPartner(
      DObSalesInvoiceCreationValueObject valueObject) {

    IObSalesInvoiceCustomerBusinessPartner businessPartner =
        new IObSalesInvoiceCustomerBusinessPartner();
    commandUtils.setCreationAndModificationInfoAndDate(businessPartner);

    Long customerId = mObCustomerRep.findOneByUserCode(valueObject.getCustomerCode()).getId();
    ((IObSalesInvoiceCustomerBusinessPartner) businessPartner).setCustomerId(customerId);
    return businessPartner;
  }

  @Override
  protected IObSalesInvoiceSummary getSummary (Collection items, Collection taxes) {
    IObSalesInvoiceSummary summary = new IObSalesInvoiceSummary();
    commandUtils.setCreationAndModificationInfoAndDate(summary);
    summary.setRemaining(null);
    return summary;
  }
}
