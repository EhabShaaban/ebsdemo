package com.ebs.dda.commands.masterdata.vendor;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import io.reactivex.Observable;
import org.springframework.transaction.TransactionSystemException;

public class MObVendorDeleteCommand implements ICommand {

  private MObVendorRep vendorRep;
  private EntityLockCommand entityLockCommand;

  public void setMObVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  @Override
  public Observable<String> executeCommand(Object vendorCode) throws Exception {
    LockDetails lockDetails = null;
    try {
      lockDetails = entityLockCommand.lockAllSections((String) vendorCode);
      vendorRep.delete(readEntityByCode((String) vendorCode));
      entityLockCommand.unlockAllSections((String) vendorCode);
    } catch (TransactionSystemException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections((String) vendorCode);
      }
      throw new DependentInstanceException();
    }
    return Observable.just("SUCCESS");
  }

  public void checkIfInstanceExists(String vendorCode) throws Exception {
    MObVendor vendor = readEntityByCode(vendorCode);
    if (vendor == null) {
      throw new InstanceNotExistException();
    }
  }

  public void setEntityLockCommand(EntityLockCommand entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  private MObVendor readEntityByCode(String vendorCode) {
    return vendorRep.findOneByUserCode(vendorCode);
  }
}
