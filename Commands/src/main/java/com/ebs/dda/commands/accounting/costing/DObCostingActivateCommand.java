package com.ebs.dda.commands.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.costing.apis.IDObCostingActionNames;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.commands.accounting.costing.services.DObCostingAccountingDetailsCreateService;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import io.reactivex.Observable;

import java.math.BigDecimal;
import java.util.List;

public class DObCostingActivateCommand implements ICommand<DObCostingActivateValueObject> {

  private DObCostingRep costingRep;

  private final CommandUtils commandUtils;
  private DObCostingStateMachine costingStateMachine;
  private IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private DObCostingAccountingDetailsCreateService costingAccountingDetailsCreateService;

  public DObCostingActivateCommand(DObCostingStateMachine costingStateMachine) {
    commandUtils = new CommandUtils();
    this.costingStateMachine = costingStateMachine;
  }

  @Override
  public Observable<String> executeCommand(DObCostingActivateValueObject costingActivateValueObject)
      throws Exception {

    DObCosting costing = costingRep.findOneByUserCode(costingActivateValueObject.getUserCode());
    setCostingAccountingDetails(costing);
    setCostingData(costing);
    this.commandUtils.setModificationInfoAndModificationDate(costing);
    costingRep.update(costing);

    return Observable.just(costing.getUserCode());
  }

  private void setCostingAccountingDetails(DObCosting costing) {
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        costingDetailsGeneralModelRep.findOneByUserCode(costing.getUserCode());
    String purchaseOrderCode = costingDetailsGeneralModel.getPurchaseOrderCode();
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        vendorInvoiceGeneralModelRep.findOneByPurchaseOrderCode(purchaseOrderCode);
    BigDecimal actualCostingCurrencyPrice = costingDetailsGeneralModel.getCurrencyPriceActualTime();
    List<IObAccountingDocumentAccountingDetails> accountingDetailsList =
        costingAccountingDetailsCreateService.getCostingAccountingDetails(
            vendorInvoiceGeneralModel, actualCostingCurrencyPrice, purchaseOrderCode);
    costing.setLinesDetails(IDObCosting.accountingDetailsAttr, accountingDetailsList);
  }

  private void setCostingData(DObCosting costing) throws Exception {
    createCostingStateMachine(costing);
    setCostingActiveState();
  }

  private void setCostingActiveState() throws Exception {
    if (costingStateMachine.canFireEvent(IDObCostingActionNames.ACTIVATE)) {
      costingStateMachine.fireEvent(IDObCostingActionNames.ACTIVATE);
      costingStateMachine.save();
    }
  }

  private void createCostingStateMachine(DObCosting costing) throws Exception {
    this.costingStateMachine.initObjectState(costing);
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    commandUtils.setUserAccountSecurityService(userAccountSecurityService);
  }

  public void setCostingRep(DObCostingRep costingRep) {
    this.costingRep = costingRep;
  }

  public void setCostingDetailsGeneralModelRep(
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }

  public void setCostingAccountingDetailsCreateService(
      DObCostingAccountingDetailsCreateService costingAccountingDetailsCreateService) {
    this.costingAccountingDetailsCreateService = costingAccountingDetailsCreateService;
  }
}
