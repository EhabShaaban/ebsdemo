package com.ebs.dda.utils;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import org.mockito.Mockito;

public class DocumentObjectCodeGeneratorMockUtils {

  private static DocumentObjectCodeGenerator codeGenerator =
    Mockito.mock(DocumentObjectCodeGenerator.class);

  public static void mockGenerateCode(String sysName, String userCode){
    Mockito.doReturn(userCode).when(codeGenerator).generateUserCode(sysName);
  }

  public static DocumentObjectCodeGenerator getCodeGenerator() {
    return codeGenerator;
  }
}
