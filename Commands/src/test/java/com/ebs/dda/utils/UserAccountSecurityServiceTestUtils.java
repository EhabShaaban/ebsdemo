package com.ebs.dda.utils;

import org.mockito.Mockito;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;

public class UserAccountSecurityServiceTestUtils {

  public static IUserAccountSecurityService setup() {
    IBDKUser user = Mockito.mock(IBDKUser.class);
    Mockito.doReturn("Admin").when(user).toString();

    IUserAccountSecurityService userAccountSecurityService =
        Mockito.mock(IUserAccountSecurityService.class);
    Mockito.doReturn(user).when(userAccountSecurityService).getLoggedInUser();

    return userAccountSecurityService;
  }

}
