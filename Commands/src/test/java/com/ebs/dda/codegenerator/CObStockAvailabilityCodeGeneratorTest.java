package com.ebs.dda.codegenerator;

import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CObStockAvailabilityCodeGeneratorTest {

  private StockAvailabilityCodeGenerationValueObject stockAvailabilityCodeGenerationValueObject;

  @Before
  public void setup() {
    stockAvailabilityCodeGenerationValueObject = new StockAvailabilityCodeGenerationValueObject();
    stockAvailabilityCodeGenerationValueObject.setStockType(StockTypeEnum.StockType.UNRESTRICTED_USE.name());
    stockAvailabilityCodeGenerationValueObject.setBusinessUnitCode("0001");
    stockAvailabilityCodeGenerationValueObject.setCompanyCode("0002");
    stockAvailabilityCodeGenerationValueObject.setPlantCode("0003");
    stockAvailabilityCodeGenerationValueObject.setStorehouseCode("0004");
    stockAvailabilityCodeGenerationValueObject.setItemCode("000005");
    stockAvailabilityCodeGenerationValueObject.setUnitOfMeasureCode("0006");

  }

  @Test
  public void testStockAvailabilityCodeGeneration() {
    // Setup
    StockAvailabilityCodeGenerator codeGenerator = new StockAvailabilityCodeGenerator();
    String expectedUserCode = "UNRESTRICTED_USE00010002000300040000050006";

    // act
    String code = codeGenerator.generateUserCode(stockAvailabilityCodeGenerationValueObject);

    // assert
    Assertions.assertEquals(
        expectedUserCode, code, "StockAvailability Code Generated should be " + expectedUserCode);
  }
}
