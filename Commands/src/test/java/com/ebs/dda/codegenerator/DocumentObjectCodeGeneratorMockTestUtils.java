package com.ebs.dda.codegenerator;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

public class DocumentObjectCodeGeneratorMockTestUtils {

	private static final DocumentObjectCodeGenerator generator = Mockito
					.mock(DocumentObjectCodeGenerator.class);

	public static void mockGenerateUserCode(String userCode) {
		Mockito.doReturn(userCode).when(generator).generateUserCode(ArgumentMatchers.anyString());
	}

	public static DocumentObjectCodeGenerator getGenerator() {
		return generator;
	}
}
