package com.ebs.dda.codegenerator;

import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;
import com.ebs.dac.dbo.jpa.repositories.usercodes.EntityUserCodeRep;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

public class DefaultCodeGeneratorTest {

  private EntityUserCodeRep entityUserCodeRep;
  private String entityType = "TestCoddedType";


  @Before
  public void setup() {
    entityUserCodeRep = Mockito.mock(EntityUserCodeRep.class);
    EntityUserCode entityUserCode = new EntityUserCode();
    entityUserCode.setCode("0007");
    Mockito.doReturn(entityUserCode).when(entityUserCodeRep).findOneByType(entityType);
  }


  @Test
  public void testGenerateInitialCodeSuccessfullyWith6Digits() {
    //Setup
    DefaultCodeGenerator defaultCodeGenerator = new DefaultCodeGenerator(6);

    //act
    String code = defaultCodeGenerator.generateCode();

    //assert
    Assertions.assertEquals("000001", code, "Initial Code Generated must be 000001");

  }


  @Test
  public void testGenerateInitialCodeSuccessfullyWith4Digits() {
    //Setup
    DefaultCodeGenerator defaultCodeGenerator = new DefaultCodeGenerator(4);

    //act
    String code = defaultCodeGenerator.generateCode();

    //assert
    Assertions.assertEquals("0001", code, "Initial Code Generated must be 0001");

  }


  @Test
  public void testGenerateCodeSuccessfullyWith4DigitsFromLastCode() {
    //Setup
    DefaultCodeGenerator defaultCodeGenerator = new DefaultCodeGenerator(4);
    defaultCodeGenerator.setLastCode("0007");

    //act
    String code = defaultCodeGenerator.generateCode();

    //assert
    Assertions.assertEquals("0008", code, "The Code Generated must be 0008");

  }


  @Test
  public void testGenerateCodeWith4DigitsFromLastCodeOfEntityCodeReader() {
    //Setup
    DefaultCodeGenerator defaultCodeGenerator = new DefaultCodeGenerator(4);

    CodeDBRetrievalGateway codeDBRetrievalGateway = new CodeDBRetrievalGateway(entityUserCodeRep,
        entityType);

    defaultCodeGenerator.readLastCode(codeDBRetrievalGateway);

    //act
    String code = defaultCodeGenerator.generateCode();

    //assert
    Assertions.assertEquals("0008", code, "The Code Generated must be 0008");

  }


}
