package com.ebs.dda.codegenerator;

import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class JournalBalanceCodeGeneratorTest {

  private JournalBalanceCodeGenerationValueObject bankValueObject;
  private JournalBalanceCodeGenerationValueObject treasuryValueObject;

  @Before
  public void setup() {
    bankValueObject = new JournalBalanceCodeGenerationValueObject();
    bankValueObject.setCompanyCode("0002");
    bankValueObject.setBusinessUnitCode("0002");
    bankValueObject.setCurrencyCode("0002");
    bankValueObject.setObjectTypeCode(JournalBalanceTypeEnum.JournalBalanceType.BANK.name());
    bankValueObject.setBankCode("0001");
    bankValueObject.setBankAccountNumber("1234567893499");

    treasuryValueObject = new JournalBalanceCodeGenerationValueObject();
    treasuryValueObject.setCompanyCode("0002");
    treasuryValueObject.setBusinessUnitCode("0002");
    treasuryValueObject.setCurrencyCode("0002");
    treasuryValueObject.setObjectTypeCode(JournalBalanceTypeEnum.JournalBalanceType.TREASURY.name());
    treasuryValueObject.setTreasuryCode("0002");
  }

  @Test
  public void testJournalBalanceCodeGenerationForBank() {
    // Setup
    JournalBalanceCodeGenerator codeGenerator = new JournalBalanceCodeGenerator();
    String expectedUserCode = "BANK00020002000200011234567893499";

    // act
    String code = codeGenerator.generateUserCode(bankValueObject);

    // assert
    Assertions.assertEquals(
        expectedUserCode, code, "JournalBalance Code Generated should be " + expectedUserCode);
  }

  @Test
  public void testJournalBalanceCodeGenerationForTreasury() {
    // Setup
    JournalBalanceCodeGenerator codeGenerator = new JournalBalanceCodeGenerator();
    String expectedUserCode = "TREASURY0002000200020002";

    // act
    String code = codeGenerator.generateUserCode(treasuryValueObject);

    // assert
    Assertions.assertEquals(
            expectedUserCode, code, "JournalBalance Code Generated should be " + expectedUserCode);
  }
}
