package com.ebs.dda.commands.accounting.salesinvoice.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.journalentry.create.salesinvoice.DObJournalEntryCreateSalesInvoiceCommand;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.entities.accounting.journalentry.DObSalesInvoiceJournalEntryMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModelMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils;
import com.ebs.repositories.accounting.journalentry.DObSalesInvoiceJournalEntryRepMockUtils;
import com.ebs.repositories.accounting.salesInvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils;
import com.ebs.repositories.accounting.salesInvoice.IObSalesInvoiceAccountingDetailsGeneralModelRepUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DObJournalEntryCreateSalesInvoiceCommandTestUtils {

  private final List<IObSalesInvoiceAccountingDetailsGeneralModel> accountingDetailsGeneralModels =
      new ArrayList<>();
  private DObJournalEntryCreateValueObject valueObject;
  private DObJournalEntryCreateSalesInvoiceCommand command;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils() {
    DObSalesInvoiceJournalEntryMockUtils.resetSalesInvoiceJournalEntry();
    DObSalesInvoiceJournalEntryRepMockUtils.resetRepository();
    documentObjectCodeGenerator = Mockito.mock(DocumentObjectCodeGenerator.class);
    valueObject = new DObJournalEntryCreateValueObject();
    command = new DObJournalEntryCreateSalesInvoiceCommand(documentObjectCodeGenerator);
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils withSalesInvoice(
      String salesInvoiceCode) {
    valueObject.setUserCode(salesInvoiceCode);
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withSalesInvoiceCode(
        salesInvoiceCode);
    IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.withSalesInvoiceCode(
        salesInvoiceCode);
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils withCustomerAccountId(
      Long customerAccountId) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withCustomerAccountId(
        customerAccountId);
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils withCustomer(Long customerAccountId, Long customerId, BigDecimal amount) {
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withGlAccountId(
            customerAccountId);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withGlSubAccountId(
            customerId);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAccountingEntry(AccountingEntry.DEBIT.name());
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withSubLedger(SubLedgers.Local_Customer.name());
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAmount(amount);
    accountingDetailsGeneralModels.add(IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.buildEntity());
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils withSalesAccount(Long salesAccountId, BigDecimal amount) {
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withGlAccountId(
            salesAccountId);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAmount(amount);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAccountingEntry(AccountingEntry.CREDIT.name());
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withSubLedger(SubLedgers.NONE.name());
    accountingDetailsGeneralModels.add(IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.buildEntity());
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils withTax(
      Long taxesAccountId, Long taxId, BigDecimal taxAmount) {
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withGlAccountId(
        taxesAccountId);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withGlSubAccountId(taxId);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAmount(taxAmount);
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withAccountingEntry(AccountingEntry.CREDIT.name());
    IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.withSubLedger(SubLedgers.Taxes.name());
    accountingDetailsGeneralModels.add(IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.buildEntity());
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils WithJournalEntryCode(
      String journalEntryCode) {
    valueObject.setJournalEntryCode(journalEntryCode);
    Mockito.when(
            documentObjectCodeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName()))
        .thenReturn(journalEntryCode);
    DObSalesInvoiceJournalEntryMockUtils.withUserCode(journalEntryCode);
    return this;
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils whenExecute() throws Exception {
    prepareUserAccountSecurityService();
    prepareCreditDebitAccounts();
    prepareSalesInvoiceTaxes(valueObject.getUserCode(), accountingDetailsGeneralModels);
    prepareSalesInvoiceJournalEntryRepUtils();
    command.executeCommand(valueObject);
    return this;
  }

  private void prepareUserAccountSecurityService() {
    CommandTestUtils commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("admin");
    IUserAccountSecurityService userAccountSecurityService =
        commandTestUtils.getUserAccountSecurityService();
    command.setUserAccountSecurityService(userAccountSecurityService);
  }

  private void prepareSalesInvoiceJournalEntryRepUtils() {
    command.setSalesInvoiceJournalEntryRep(DObSalesInvoiceJournalEntryRepMockUtils.getRepository());
  }

  private void prepareCreditDebitAccounts() {
    DObSalesInvoiceJournalEntryPreparationGeneralModel creditPreparationGeneralModel =
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.buildEntity();
    DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.findOneByCode(
        creditPreparationGeneralModel);

    command.setSalesInvoiceJournalEntryPreparationGeneralModelRep(
        DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.getRepository());
  }

  private void prepareSalesInvoiceTaxes(
      String salesInvoiceCode,
      List<IObSalesInvoiceAccountingDetailsGeneralModel> accountingDetailsGeneralModels) {
    IObSalesInvoiceAccountingDetailsGeneralModelRepUtils.findByDocumentCodeOrderByAccountingEntryDesc(
            accountingDetailsGeneralModels, salesInvoiceCode);
    command.setAccountingDetailsGeneralModelRep(
        IObSalesInvoiceAccountingDetailsGeneralModelRepUtils.getRepository());
  }

  public DObJournalEntryCreateSalesInvoiceCommandTestUtils assertThatJournalEntriesIsUpdated()
      throws Exception {
    ArgumentCaptor<DObSalesInvoiceJournalEntry> journalEntryArgumentCaptor =
        ArgumentCaptor.forClass(DObSalesInvoiceJournalEntry.class);

    Mockito.verify(DObSalesInvoiceJournalEntryRepMockUtils.getRepository(), Mockito.times(1))
        .create(journalEntryArgumentCaptor.capture());

    return this;
  }

  public DObSalesInvoiceJournalEntry verifyThatCreateIsCalled() throws Exception {
    ArgumentCaptor<DObSalesInvoiceJournalEntry> argumentCaptor =
            ArgumentCaptor.forClass(DObSalesInvoiceJournalEntry.class);

    Mockito.verify(DObSalesInvoiceJournalEntryRepMockUtils.getRepository())
            .create(argumentCaptor.capture());

    return argumentCaptor.getValue();
  }

  public void assertJournalEntryItems(DObSalesInvoiceJournalEntry createdJE,Long glAccountId, BigDecimal value, String accountingEntry) {

    List<IObJournalEntryItem> journalEntryItems = createdJE
            .getAllLines(IDObJournalEntry.journalItemAttr)
            .stream()
            .collect(Collectors.toList());
    IObJournalEntryItem actualJournalItem =
            journalEntryItems.stream()
                    .filter(
                            item ->
                                    glAccountId.equals(item.getAccountId())
                                            && ((accountingEntry.equals(AccountingEntry.CREDIT.name())
                                            && value.compareTo(item.getCredit()) == 0)
                                            || (accountingEntry.equals(AccountingEntry.DEBIT.name())
                                            && value.compareTo(item.getDebit()) == 0)))
                    .findAny()
                    .orElse(null);
    Assert.assertNotNull(actualJournalItem);
  }
}
