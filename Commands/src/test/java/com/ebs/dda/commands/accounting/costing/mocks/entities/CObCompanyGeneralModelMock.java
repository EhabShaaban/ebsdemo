package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;

public class CObCompanyGeneralModelMock extends CObCompanyGeneralModel {
  private String currencyIso;

  public CObCompanyGeneralModelMock(String currencyIso) {
    this.currencyIso = currencyIso;
  }

  @Override
  public String getCurrencyIso() {
    return currencyIso;
  }
}
