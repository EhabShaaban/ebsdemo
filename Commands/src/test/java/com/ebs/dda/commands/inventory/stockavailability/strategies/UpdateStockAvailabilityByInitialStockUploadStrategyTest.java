package com.ebs.dda.commands.inventory.stockavailability.strategies;

import com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils.initialStockUploadItem;

@RunWith(MockitoJUnitRunner.class)
public class UpdateStockAvailabilityByInitialStockUploadStrategyTest {

  private UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils utils;

  @Before
  public void init() {
    utils = new UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils();
  }

  @Test
  public void Should_FetchItemsFromRepositoryAndCallUpdateServiceWithItemsList() {
    utils
        .withUserCode("2021000001")
        .withInitialStockUploadItems(
            initialStockUploadItem(
                "UNRESTRICTED_USE",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("10.0")),
            initialStockUploadItem(
                "DAMAGED_STOCK",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("5.0")))
        .whenUpdate()
        .thenAssertRepositoryWasCalledWithCode("2021000001")
        .thenAssertUpdateServiceWasCalledWithItemsList();
  }
}
