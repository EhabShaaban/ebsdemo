package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;

import java.math.BigDecimal;

public class IObVendorInvoiceSummaryMock extends IObVendorInvoiceSummary {
  private BigDecimal totalAmount;
  private BigDecimal remaining;

  public IObVendorInvoiceSummaryMock(BigDecimal totalAmount, BigDecimal remaining) {
    this.totalAmount = totalAmount;
    this.remaining = remaining;
  }

  @Override
  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  @Override
  public BigDecimal getRemaining() {
    return remaining;
  }
}
