package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceRepMockUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObPaymentUpdateVendorInvoiceRemainingCommandTests {
  private DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils utils;

  @Before
  public void setUp() {
    utils = new DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils();
  }

  @Test
  public void testUpdateVendorInvoiceRemainingWithPaymentAmount() throws Exception {

    utils
        .withPaymentCode("2019000038")
        .withAmount(BigDecimal.valueOf(50))
        .withVendorInvoiceCode("2019000108")
        .withVendorInvoiceId(3L)
        .withVendorInvoiceRemaining(BigDecimal.valueOf(200))
        .whenExecute()
        .assertThatRemainingIsUpdatedTo(BigDecimal.valueOf(150));
  }

  @After
  public void tearDown() throws Exception {
    DObVendorInvoiceRepMockUtils.resetRepository();
  }
}
