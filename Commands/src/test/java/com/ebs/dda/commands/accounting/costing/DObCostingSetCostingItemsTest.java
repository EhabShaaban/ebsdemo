package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemsCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.entities.*;
import com.ebs.repositories.GoodsReceiptGeneralModelRepUtils;
import com.ebs.repositories.LandedCostGeneralModelRepMockUtils;
import com.ebs.repositories.VendorInvoiceGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
public class DObCostingSetCostingItemsTest {
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObCostingStateMachine costingStateMachine;

  private DObCostingCreateCommand command;
  private CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory;
  private CostingExchangeRateFullyPaidStrategy costingExchangeRateFullyPaidStrategy;
  private CommandTestUtils commandTestUtils;

  @MockBean private DObCostingRep costingRep;
  @MockBean private CreateCostingItemsSectionService createCostingItemsSectionService;

  @Before
  public void init() {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    documentObjectCodeGenerator = Mockito.mock(DocumentObjectCodeGenerator.class);
    costingStateMachine = Mockito.mock(DObCostingStateMachine.class);

    costingExchangeRateStrategyFactory = Mockito.mock(CostingExchangeRateStrategyFactory.class);
    costingExchangeRateFullyPaidStrategy = Mockito.mock(CostingExchangeRateFullyPaidStrategy.class);

    command = new DObCostingCreateCommand(documentObjectCodeGenerator, costingStateMachine);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setGoodsReceiptGeneralModelRep(GoodsReceiptGeneralModelRepUtils.getRepository());
    command.setVendorInvoiceGeneralModelRep(VendorInvoiceGeneralModelRepMockUtils.getRepository());
    command.setVendorInvoiceSummaryRep(VendorInvoiceSummaryRepUtils.getRepository());
    command.setCostingRep(costingRep);
    command.setCostingExchangeRateStrategyFactory(costingExchangeRateStrategyFactory);
    command.setLandedCostGeneralModelRep(LandedCostGeneralModelRepMockUtils.getRepository());
    command.setCreateCostingItemsSectionService(createCostingItemsSectionService);

    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @After
  public void after() {
    LandedCostGeneralModelRepMockUtils.resetRepository();
  }

  @Test
  public void shouldCreateCostingItemsForEstimateLandedCost() throws Exception {

    String goodsReceiptCode = "2021000001";
    String purchaseOrderCode = "2021000001";
    String vendorInvoiceCode = "2021000001";

    Long goodsReceiptId = 1L;
    Long vendorInvoiceId = 65L;
    Long purchaseOrderId = 22L;
    Long firstItemId = 33L;
    Long secondItemId = 34L;
    Long uomId = 23L;

    BigDecimal firstUnrestrictedItemQty = new BigDecimal("90.0");
    BigDecimal firstDamagedItemQty = new BigDecimal("10.0");
    BigDecimal secondUnrestrictedItemQty = new BigDecimal("50.0");

    BigDecimal remaining = new BigDecimal("2500");

    String unrestrictedStockType = "UNRESTRICTED_USE";
    String damagedStockType = "DAMAGED_STOCK";

    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .id(goodsReceiptId)
            .userCode(goodsReceiptCode)
            .refDocumentCode(purchaseOrderCode)
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(vendorInvoiceId)
            .userCode(vendorInvoiceCode)
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(remaining)
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .purchaseOrderId(purchaseOrderId)
            .state(DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED)
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    Mockito.when(costingExchangeRateStrategyFactory.getStrategy(Mockito.any()))
        .thenReturn(costingExchangeRateFullyPaidStrategy);
    Mockito.when(costingExchangeRateFullyPaidStrategy.calculate(Mockito.any()))
        .thenReturn(new BigDecimal("16.8"));

    IObCostingItem expectedUnrestrictedCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(firstItemId)
            .uomId(uomId)
            .quantity(firstUnrestrictedItemQty)
            .stockType(unrestrictedStockType)
            .toBeConsideredInCostingPer(new BigDecimal("0.4"))
            .estimateUnitPrice(new BigDecimal("186.66666666648"))
            .actualUnitPrice(null)
            .estimateUnitLandedCost(new BigDecimal("8.0888888888"))
            .actualUnitLandedCost(null)
            .build();
    IObCostingItem expectedDamagedCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(firstItemId)
            .uomId(uomId)
            .quantity(firstDamagedItemQty)
            .stockType(damagedStockType)
            .toBeConsideredInCostingPer(BigDecimal.ZERO)
            .estimateUnitPrice(BigDecimal.ZERO)
            .actualUnitPrice(null)
            .estimateUnitLandedCost(BigDecimal.ZERO)
            .actualUnitLandedCost(null)
            .build();
    IObCostingItem expectedSecondCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(secondItemId)
            .uomId(uomId)
            .quantity(secondUnrestrictedItemQty)
            .stockType(unrestrictedStockType)
            .toBeConsideredInCostingPer(new BigDecimal("0.6"))
            .estimateUnitPrice(new BigDecimal("504.0"))
            .actualUnitPrice(null)
            .estimateUnitLandedCost(new BigDecimal("21.84"))
            .actualUnitLandedCost(null)
            .build();

    List<IObCostingItem> expectedCostingItems =
        Arrays.asList(
            expectedUnrestrictedCostingItem, expectedDamagedCostingItem, expectedSecondCostingItem);
    Mockito.when(
            createCostingItemsSectionService.getCostingItems(
                Mockito.any(IObCostingItemsCreationValueObject.class)))
        .thenReturn(expectedCostingItems);

    // act
    command.executeCommand(valueObject);

    // assert
    ArgumentCaptor<DObCosting> createdObjectCaptor = ArgumentCaptor.forClass(DObCosting.class);
    Mockito.verify(costingRep, Mockito.times(1)).create(createdObjectCaptor.capture());
    DObCosting createdObject = createdObjectCaptor.getValue();
    List<IObCostingItem> actualCostingItems =
        (List<IObCostingItem>) createdObject.getAllLines(IDObCosting.accountingDocumentItemsAttr);

    Assert.assertNotNull(actualCostingItems);
    Assert.assertEquals(actualCostingItems.size(), 3);

    assertThatExpectedAndActualItemsAreEqual(expectedCostingItems, actualCostingItems);
  }

  @Test
  public void shouldCreateCostingItemsForActualLandedCost() throws Exception {

    String goodsReceiptCode = "2021000001";
    String purchaseOrderCode = "2021000001";
    String vendorInvoiceCode = "2021000001";

    Long goodsReceiptId = 1L;
    Long vendorInvoiceId = 65L;
    Long purchaseOrderId = 22L;
    Long firstItemId = 33L;
    Long secondItemId = 34L;
    Long uomId = 23L;

    BigDecimal firstUnrestrictedItemQty = new BigDecimal("90.0");
    BigDecimal firstDamagedItemQty = new BigDecimal("10.0");
    BigDecimal secondUnrestrictedItemQty = new BigDecimal("50.0");

    BigDecimal remaining = new BigDecimal("2500");

    String unrestrictedStockType = "UNRESTRICTED_USE";
    String damagedStockType = "DAMAGED_STOCK";

    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .id(goodsReceiptId)
            .userCode(goodsReceiptCode)
            .refDocumentCode(purchaseOrderCode)
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(vendorInvoiceId)
            .userCode(vendorInvoiceCode)
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(remaining)
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .purchaseOrderId(purchaseOrderId)
            .state(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED)
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    Mockito.when(costingExchangeRateStrategyFactory.getStrategy(Mockito.any()))
        .thenReturn(costingExchangeRateFullyPaidStrategy);
    Mockito.when(costingExchangeRateFullyPaidStrategy.calculate(Mockito.any()))
        .thenReturn(new BigDecimal("16.392"));

    IObCostingItem expectedUnrestrictedCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(firstItemId)
            .uomId(uomId)
            .quantity(firstUnrestrictedItemQty)
            .stockType(unrestrictedStockType)
            .toBeConsideredInCostingPer(new BigDecimal("0.4"))
            .estimateUnitPrice(null)
            .actualUnitPrice(new BigDecimal("182.1333333331512"))
            .estimateUnitLandedCost(null)
            .actualUnitLandedCost(new BigDecimal("8.0888888888"))
            .build();
    IObCostingItem expectedDamagedCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(firstItemId)
            .uomId(uomId)
            .quantity(firstDamagedItemQty)
            .stockType(damagedStockType)
            .toBeConsideredInCostingPer(BigDecimal.ZERO)
            .estimateUnitPrice(null)
            .actualUnitPrice(BigDecimal.ZERO)
            .estimateUnitLandedCost(null)
            .actualUnitLandedCost(BigDecimal.ZERO)
            .build();
    IObCostingItem expectedSecondCostingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(secondItemId)
            .uomId(uomId)
            .quantity(secondUnrestrictedItemQty)
            .stockType(unrestrictedStockType)
            .toBeConsideredInCostingPer(new BigDecimal("0.6"))
            .estimateUnitPrice(null)
            .actualUnitPrice(new BigDecimal("491.760"))
            .estimateUnitLandedCost(null)
            .actualUnitLandedCost(new BigDecimal("21.84"))
            .build();

    List<IObCostingItem> expectedCostingItems =
        Arrays.asList(
            expectedUnrestrictedCostingItem, expectedDamagedCostingItem, expectedSecondCostingItem);

    Mockito.when(
            createCostingItemsSectionService.getCostingItems(
                Mockito.any(IObCostingItemsCreationValueObject.class)))
        .thenReturn(expectedCostingItems);

    // act
    command.executeCommand(valueObject);

    // assert
    ArgumentCaptor<DObCosting> createdObjectCaptor = ArgumentCaptor.forClass(DObCosting.class);
    Mockito.verify(costingRep, Mockito.times(1)).create(createdObjectCaptor.capture());
    DObCosting createdObject = createdObjectCaptor.getValue();
    List<IObCostingItem> actualCostingItems =
        (List<IObCostingItem>) createdObject.getAllLines(IDObCosting.accountingDocumentItemsAttr);

    Assert.assertNotNull(actualCostingItems);
    Assert.assertEquals(actualCostingItems.size(), 3);

    assertThatExpectedAndActualItemsAreEqual(expectedCostingItems, actualCostingItems);
  }

  private void assertThatExpectedAndActualItemsAreEqual(
      List<IObCostingItem> expectedCostingItems, List<IObCostingItem> actualCostingItems) {
    for (int i = 0; i < expectedCostingItems.size(); i++) {
      Assert.assertEquals(
          expectedCostingItems.get(i).getItemId(), actualCostingItems.get(i).getItemId());
      Assert.assertEquals(
          expectedCostingItems.get(i).getUomId(), actualCostingItems.get(i).getUomId());
      Assert.assertEquals(
          expectedCostingItems.get(i).getQuantity(), actualCostingItems.get(i).getQuantity());
      Assert.assertEquals(
          expectedCostingItems.get(i).getStockType(), actualCostingItems.get(i).getStockType());

      Assert.assertEquals(
          "expected ToBeConsideredInCostingPer not equals actual "
              + expectedCostingItems.get(i).getToBeConsideredInCostingPer()
              + " != "
              + actualCostingItems.get(i).getToBeConsideredInCostingPer(),
          0,
          expectedCostingItems
              .get(i)
              .getToBeConsideredInCostingPer()
              .compareTo(actualCostingItems.get(i).getToBeConsideredInCostingPer()));

      if (expectedCostingItems.get(i).getEstimateUnitLandedCost() != null) {
        Assert.assertEquals(
            "expected EstimateUnitLandedCost not equals actual "
                + expectedCostingItems.get(i).getEstimateUnitLandedCost()
                + " != "
                + actualCostingItems.get(i).getEstimateUnitLandedCost(),
            0,
            expectedCostingItems
                .get(i)
                .getEstimateUnitLandedCost()
                .compareTo(actualCostingItems.get(i).getEstimateUnitLandedCost()));
        Assert.assertEquals(
            "expected EstimateUnitPrice not equals actual "
                + expectedCostingItems.get(i).getEstimateUnitPrice()
                + " != "
                + actualCostingItems.get(i).getEstimateUnitPrice(),
            0,
            expectedCostingItems
                .get(i)
                .getEstimateUnitPrice()
                .compareTo(actualCostingItems.get(i).getEstimateUnitPrice()));
      } else {
        Assert.assertNull(actualCostingItems.get(i).getEstimateUnitLandedCost());
        Assert.assertNull(actualCostingItems.get(i).getEstimateUnitPrice());
      }

      if (expectedCostingItems.get(i).getActualUnitLandedCost() != null) {
        Assert.assertEquals(
            "expected ActualUnitLandedCost not equals actual "
                + expectedCostingItems.get(i).getActualUnitLandedCost()
                + " != "
                + actualCostingItems.get(i).getActualUnitLandedCost(),
            0,
            expectedCostingItems
                .get(i)
                .getActualUnitLandedCost()
                .compareTo(actualCostingItems.get(i).getActualUnitLandedCost()));

        Assert.assertEquals(
            "expected ActualUnitPrice not equals actual "
                + expectedCostingItems.get(i).getActualUnitPrice()
                + " != "
                + actualCostingItems.get(i).getActualUnitPrice(),
            0,
            expectedCostingItems
                .get(i)
                .getActualUnitPrice()
                .compareTo(actualCostingItems.get(i).getActualUnitPrice()));
      } else {
        Assert.assertNull(actualCostingItems.get(i).getActualUnitLandedCost());
        Assert.assertNull(actualCostingItems.get(i).getActualUnitPrice());
      }
    }
  }
}
