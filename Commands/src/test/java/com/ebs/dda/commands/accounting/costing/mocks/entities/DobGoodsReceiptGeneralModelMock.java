package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;

public class DobGoodsReceiptGeneralModelMock extends DObGoodsReceiptGeneralModel {
  private Long mockCompanyId;
  private Long mockBusinessUnitId;
  private String userCode;
  private String refDocumentCode;
  private String companyCode;

  public DobGoodsReceiptGeneralModelMock(Long goodsReceiptId, Long companyId, Long businessUnitId,
                                         String userCode, String refDocumentCode) {
    setId(goodsReceiptId);
    this.mockCompanyId = companyId;
    this.mockBusinessUnitId = businessUnitId;
    this.userCode = userCode;
    this.refDocumentCode = refDocumentCode;
  }

  public DobGoodsReceiptGeneralModelMock(String userCode, String refDocumentCode) {
    this.userCode = userCode;
    this.refDocumentCode = refDocumentCode;
  }

  public DobGoodsReceiptGeneralModelMock(
      String userCode, String refDocumentCode, String companyCode) {
    this.userCode = userCode;
    this.refDocumentCode = refDocumentCode;
    this.companyCode = companyCode;
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  @Override
  public long getCompanyId() {
    return this.mockCompanyId;
  }

  @Override
  public long getPurchaseUnitId() {
    return this.mockBusinessUnitId;
  }

  @Override
  public String getSysName() {
    return "DObGoodsReceipt";
  }
}
