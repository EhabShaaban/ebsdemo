package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingActivationDetails;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.entities.AccountingDocumentActivationDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingMockUtils;
import com.ebs.entities.accounting.journalentry.DObJournalEntryMockUtils;
import com.ebs.entities.accounting.journalentry.IObJournalEntryItemMockUtils;
import com.ebs.repositories.accounting.costing.CostingRepUtils;
import com.ebs.repositories.accounting.journalentry.DObJournalEntryGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.journalentry.IObJournalEntryItemGeneralModelRepMockUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObCostingSetActivationDetailsCommandTest {

  private DObCostingSetActivationDetailsCommand command;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    command = new DObCostingSetActivationDetailsCommand();
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setCostingRep(CostingRepUtils.getRepository());
    command.setJournalEntryGeneralModelRep(DObJournalEntryGeneralModelRepMockUtils.getRepository());
    command.setJournalEntryItemsGeneralModelRep(
        IObJournalEntryItemGeneralModelRepMockUtils.getRepository());

    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @After
  public void tearDown() {
    DObJournalEntryGeneralModelRepMockUtils.resetRepository();
    IObJournalEntryItemGeneralModelRepMockUtils.resetRepository();
    CostingRepUtils.resetRepository();
  }

  @Test
  public void shouldSetCostingAccountingDocumentActivationDetails() throws Exception {
    // Arrange
    String costingDocumentCode = "2021000001";
    Long costingDocumentId = 2L;
    String journalEntryCode = "2021000002";
    Long exchangeRateId = 1L;
    String currencyPrice = "1.0";
    String dueDate = "08-Jul-2021 04:30 PM";
    Long fiscalPeriodId = 3L;
    String ACCOUNTANT = "Admin";

    DObJournalEntryGeneralModel journalEntry =
        DObJournalEntryMockUtils.mockGeneralModel()
            .userCode(journalEntryCode)
            .documentReferenceCode(costingDocumentCode)
            .dueDate(commandTestUtils.getDateTime(dueDate))
            .fiscalPeriodId(fiscalPeriodId)
            .build();
    DObJournalEntryGeneralModelRepMockUtils.findOneByUserCode(journalEntry);

    IObJournalEntryItemGeneralModel journalEntryItem =
        IObJournalEntryItemMockUtils.mockGeneralModel()
            .journalEntryCode(journalEntryCode)
            .exchangeRateId(exchangeRateId)
            .currencyPrice(currencyPrice)
            .build();
    IObJournalEntryItemGeneralModelRepMockUtils.findFirstByCode(journalEntryItem);

    DObCosting costing =
        CostingMockUtils.mockEntity().id(costingDocumentId).userCode(costingDocumentCode).build();
    CostingRepUtils.findOneByUserCode(costing);

    IObAccountingDocumentActivationDetailsGeneralModel expectedActivationDetails =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .refInstanceId(costingDocumentId)
            .accountant(ACCOUNTANT)
            .activationDate(DateTime.now())
            .dueDate(commandTestUtils.getDateTime(dueDate))
            .exchangeRateId(exchangeRateId)
            .currencyPrice(new BigDecimal(currencyPrice))
            .fiscalPeriodId(fiscalPeriodId)
            .build();

    // Act
    command.executeCommand(journalEntryCode);

    // Assert
    Mockito.verify(DObJournalEntryGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(journalEntryCode);

    Mockito.verify(IObJournalEntryItemGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findFirstByCode(journalEntryCode);

    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(journalEntry.getDocumentReferenceCode());

    ArgumentCaptor<IObCostingActivationDetails> createdObjectCaptor =
        ArgumentCaptor.forClass(IObCostingActivationDetails.class);

    Mockito.verify(costing, Mockito.times(1))
        .setHeaderDetail(
            Mockito.eq(IDObCosting.accountingActivationDetailsAttr), createdObjectCaptor.capture());

    IObAccountingDocumentActivationDetails actualCostingActivationDetail =
        createdObjectCaptor.getValue();

    assertThatActualActivationDetailsMatchesExpected(
        expectedActivationDetails, actualCostingActivationDetail);

    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1)).update(costing);
  }

  private void assertThatActualActivationDetailsMatchesExpected(
      IObAccountingDocumentActivationDetailsGeneralModel expectedActivationDetails,
      IObAccountingDocumentActivationDetails actualCostingActivationDetail) {

    Assert.assertEquals(
        expectedActivationDetails.getRefInstanceId(),
        actualCostingActivationDetail.getRefInstanceId());
    Assert.assertEquals(
        expectedActivationDetails.getAccountant(), actualCostingActivationDetail.getAccountant());
    Assert.assertEquals(
        expectedActivationDetails.getActivationDate(),
        actualCostingActivationDetail.getActivationDate());
    Assert.assertEquals(
        expectedActivationDetails.getDueDate(), actualCostingActivationDetail.getDueDate());
    Assert.assertEquals(
        expectedActivationDetails.getExchangeRateId(),
        actualCostingActivationDetail.getExchangeRateId());
    Assert.assertEquals(
        expectedActivationDetails.getCurrencyPrice(),
        actualCostingActivationDetail.getCurrencyPrice());
    Assert.assertEquals(
        expectedActivationDetails.getFiscalPeriodId(),
        actualCostingActivationDetail.getFiscalPeriodId());
  }
}
