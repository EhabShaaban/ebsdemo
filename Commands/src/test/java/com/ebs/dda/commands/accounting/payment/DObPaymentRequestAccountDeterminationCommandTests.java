package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObPaymentRequestAccountDeterminationCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class DObPaymentRequestAccountDeterminationCommandTests {

    private DObPaymentRequestAccountDeterminationCommandTestUtils utils;

    @Before
    public void setup() {
        utils = new DObPaymentRequestAccountDeterminationCommandTestUtils();
    }

    @Test
    public void testDetermineGeneralPaymentForPOAccounts() throws Exception {
        utils.withPaymentRequest(1L, "2021000001", "OTHER_PARTY_FOR_PURCHASE")
                .withDueDocumentType("PURCHASEORDER")
                .withPaymentForm("BANK")
                .withDueDocumentCode("2021000001")
                .withBusinessPartnerCode(null)
                .withBankAccountId(17L)
                .withTreasuryAccountId(null)
                .withAmount(BigDecimal.valueOf(1000))
                .withOrder(1L, "2021000001")
                .whenExecute()
                .saveMethodIsCalledSuccessfully()
                .numberOfAccountsDeterminedIs(2);
    }

    @Test
    public void testDeterminePaymentToVendorForPOAccounts() throws Exception {
        utils.withPaymentRequest(2L, "2021000002", "VENDOR")
                .withDueDocumentType("PURCHASEORDER")
                .withPaymentForm("CASH")
                .withDueDocumentCode("2021000002")
                .withBusinessPartnerCode(null)
                .withBankAccountId(null)
                .withTreasuryAccountId(2L)
                .withAmount(BigDecimal.valueOf(1000))
                .withOrder(2L, "2021000002")
                .whenExecute()
                .saveMethodIsCalledSuccessfully()
                .numberOfAccountsDeterminedIs(2);
    }

    @Test
    public void testDeterminePaymentToVendorForInvoiceAccounts() throws Exception {
        utils.withPaymentRequest(3L, "2021000003", "VENDOR")
                .withDueDocumentType("INVOICE")
                .withPaymentForm("BANK")
                .withDueDocumentCode("2021000003")
                .withBusinessPartnerCode("000002")
                .withBankAccountId(17L)
                .withTreasuryAccountId(null)
                .withAmount(BigDecimal.valueOf(1000))
                .withVendor("000002", "10221")
                .whenExecute()
                .saveMethodIsCalledSuccessfully()
                .numberOfAccountsDeterminedIs(2);
    }
}
