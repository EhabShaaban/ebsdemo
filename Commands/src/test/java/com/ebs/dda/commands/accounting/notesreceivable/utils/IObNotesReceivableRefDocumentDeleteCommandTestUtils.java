package com.ebs.dda.commands.accounting.notesreceivable.utils;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.notesreceivables.IObNotesReceivableReferenceDocumentDeleteCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableReferenceDocumentDeletionValueObject;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentNotesReceivableRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableReferenceDocumentSalesOrderRep;
import com.ebs.entities.accounting.monetarynotes.DObNotesReceivableMockUtils;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.mockito.Mockito;

public class IObNotesReceivableRefDocumentDeleteCommandTestUtils {
  private IObNotesReceivableReferenceDocumentDeleteCommand command;

  private IObNotesReceivableReferenceDocumentSalesInvoiceRep
      notesReceivableReferenceDocumentSalesInvoiceRep;
  private IObNotesReceivableReferenceDocumentNotesReceivableRep
      notesReceivableReferenceDocumentNotesReceivableRep;
  private IObNotesReceivableReferenceDocumentSalesOrderRep
      notesReceivableReferenceDocumentSalesOrderRep;
  private DObMonetaryNotesRep monetaryNotesRep;
  private CommandTestUtils commandTestUtils;

  public IObNotesReceivableRefDocumentDeleteCommandTestUtils() throws Exception {
    mockDependencies();
    initCommand();
    prepareNotesReceivable(monetaryNotesRep);
  }

  private void mockDependencies() {
    notesReceivableReferenceDocumentSalesInvoiceRep =
        Mockito.mock(IObNotesReceivableReferenceDocumentSalesInvoiceRep.class);
    notesReceivableReferenceDocumentNotesReceivableRep =
        Mockito.mock(IObNotesReceivableReferenceDocumentNotesReceivableRep.class);
    notesReceivableReferenceDocumentSalesOrderRep =
        Mockito.mock(IObNotesReceivableReferenceDocumentSalesOrderRep.class);
    monetaryNotesRep = Mockito.mock(DObMonetaryNotesRep.class);
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
  }

  private void initCommand() {
    command = new IObNotesReceivableReferenceDocumentDeleteCommand();
    command.setNotesReceivableReferenceDocumentNotesReceivableRep(
        notesReceivableReferenceDocumentNotesReceivableRep);
    command.setNotesReceivableReferenceDocumentSalesInvoiceRep(
        notesReceivableReferenceDocumentSalesInvoiceRep);
    command.setNotesReceivableReferenceDocumentSalesOrderRep(
        notesReceivableReferenceDocumentSalesOrderRep);
    command.setMonetaryNotesRep(monetaryNotesRep);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
  }

  public IObNotesReceivableRefDocumentDeleteCommandTestUtils
      whenExecuteReferenceDocumentDeleteCommand(
          String notesReceivableCode, Long refDocumentId, String documentType) {

    IObNotesReceivableReferenceDocumentDeletionValueObject valueObject =
        setNotesReceivableReferenceDocumentDeletionValueObject(
            notesReceivableCode, refDocumentId, documentType);
    try {
      command.executeCommand(valueObject);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
    return this;
  }

  private IObNotesReceivableReferenceDocumentDeletionValueObject
      setNotesReceivableReferenceDocumentDeletionValueObject(
          String notesReceivableCode, Long refDocumentId, String documentType) {
    IObNotesReceivableReferenceDocumentDeletionValueObject valueObject =
        new IObNotesReceivableReferenceDocumentDeletionValueObject();
    valueObject.setNotesReceivableCode(notesReceivableCode);
    valueObject.setReferenceDocumentType(documentType);
    valueObject.setReferenceDocumentId(refDocumentId);
    return valueObject;
  }

  public void thenAssertNotesReceivableRefDocumentDeleted() {
    Mockito.verify(notesReceivableReferenceDocumentNotesReceivableRep, Mockito.times(1))
        .deleteById(1L);
  }

  public void thenAssertSalesOrderRefDocumentDeleted() {
    Mockito.verify(notesReceivableReferenceDocumentSalesOrderRep, Mockito.times(1)).deleteById(1L);
  }

  public void thenAssertSalesInvoiceRefDocumentDeleted() {
    Mockito.verify(notesReceivableReferenceDocumentSalesInvoiceRep, Mockito.times(1))
        .deleteById(1L);
  }

  private void prepareNotesReceivable(DObMonetaryNotesRep notesReceivablesRep) {
    DObMonetaryNotes monetaryNotes = DObNotesReceivableMockUtils.mockEntity().code("2021000001").build();
    Mockito.when(notesReceivablesRep.findOneByUserCode("2021000001")).thenReturn(monetaryNotes);
  }
}
