package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;

import java.math.BigDecimal;

public class CObExchangeRateGeneralModelMock extends CObExchangeRateGeneralModel {

  private BigDecimal secondValue;

  public CObExchangeRateGeneralModelMock(BigDecimal secondValue) {
    this.secondValue = secondValue;
  }

  @Override
  public BigDecimal getSecondValue() {
    return secondValue;
  }
}
