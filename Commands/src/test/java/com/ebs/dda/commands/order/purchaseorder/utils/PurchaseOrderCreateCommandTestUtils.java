package com.ebs.dda.commands.order.purchaseorder.utils;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.order.purchaseorder.factory.DObPurchaseOrderFactory;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderTax;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderBillToCompany;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderFulFillerVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.entities.purchases.CompanyTaxesMockUtils;
import com.ebs.entities.purchases.ServicePurchaseOrderTestUtils;
import com.ebs.repositories.purchases.CompanyTaxesGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.PurchaseOrderRepTestUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PurchaseOrderCreateCommandTestUtils {

  public void prepareUserCodeToBeGenerated(
      DocumentObjectCodeGenerator documentObjectCodeGenerator) {
    Mockito.when(documentObjectCodeGenerator.generateUserCode(Mockito.any()))
        .thenReturn("20210000001");
  }

  public void prepareCompanyTaxes(int size) {
    List<LObCompanyTaxesGeneralModel> taxes =
        IntStream.range(0, size)
            .mapToObj(PurchaseOrderCreateCommandTestUtils::getSingleTaxByIndex)
            .collect(Collectors.toList());
    CompanyTaxesGeneralModelRepTestUtils.findOneByUserCode(taxes, Mockito.anyString());
  }

  private static LObCompanyTaxesGeneralModel getSingleTaxByIndex(int index) {
    Map<String, String> nameValues = new HashMap<>();
    nameValues.put("ar", ("name" + index));
    nameValues.put("en", ("name" + index));
    return CompanyTaxesMockUtils.mockGeneralModel()
        .userCode("000" + index)
        .name(new LocalizedString(nameValues))
        .percentage(Integer.toString(10 * index))
        .build();
  }

  public DObPurchaseOrderCreateValueObject prepareValueObject(OrderTypeEnum orderType) {
    DObPurchaseOrderCreateValueObject valueObject = new DObPurchaseOrderCreateValueObject();
    valueObject.setReferencePurchaseOrderCode("123");
    valueObject.setType(orderType);
    valueObject.setBusinessUnitCode("002");
    valueObject.setVendorCode("0001");
    valueObject.setDocumentOwnerId(1L);
    return valueObject;
  }

  public void assertPurchaseOrderValuesSet(DObServicePurchaseOrder purchaseOrder) throws Exception {
    Mockito.verify(purchaseOrder, Mockito.times(1)).setUserCode("20210000001");
    Mockito.verify(purchaseOrder, Mockito.times(1)).setDocumentOwnerId(1L);
    assertCompanyAndVendor(purchaseOrder);
    assertCompanyTaxes(purchaseOrder);
  }

  private void assertCompanyTaxes(DObServicePurchaseOrder purchaseOrder) {
    ArgumentCaptor<List<IObOrderTax>> listArgumentCaptor = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<CompositeAttribute> compositeAttributeArgumentCaptor =
        ArgumentCaptor.forClass(CompositeAttribute.class);
    Mockito.verify(purchaseOrder, Mockito.times(1))
        .setLinesDetails(compositeAttributeArgumentCaptor.capture(), listArgumentCaptor.capture());
    Assert.assertEquals(listArgumentCaptor.getValue().size(), 5);
  }

  private void assertCompanyAndVendor(DObServicePurchaseOrder purchaseOrder) {
    ArgumentCaptor<DocumentHeader> documentHeaderArgumentCaptor =
        ArgumentCaptor.forClass(DocumentHeader.class);
    Mockito.verify(purchaseOrder, Mockito.times(2))
        .setHeaderDetail(Mockito.any(), documentHeaderArgumentCaptor.capture());
    documentHeaderArgumentCaptor.getAllValues().stream()
        .forEach(
            documentHeader -> {
              assertCompany(documentHeader);
              assertVendor(documentHeader);
            });
  }

  private void assertVendor(DocumentHeader documentHeader) {
    if (documentHeader instanceof IObPurchaseOrderFulFillerVendor) {
      IObPurchaseOrderFulFillerVendor vendor = (IObPurchaseOrderFulFillerVendor) documentHeader;
      long vendorId = vendor.getVendorId();
      long referencePoId = vendor.getReferencePoId();
      Assert.assertEquals(5L, vendorId);
      Assert.assertEquals(10L, referencePoId);
    }
  }

  private void assertCompany(DocumentHeader documentHeader) {
    if (documentHeader instanceof IObPurchaseOrderBillToCompany) {
      IObPurchaseOrderBillToCompany purchaseOrderBillToCompany =
          (IObPurchaseOrderBillToCompany) documentHeader;
      long actualCompanyId = purchaseOrderBillToCompany.getCompanyId();
      long businessUnitId = purchaseOrderBillToCompany.getBusinessUnitId();
      Assert.assertEquals(20L, actualCompanyId);
      Assert.assertEquals(15L, businessUnitId);
    }
  }

  public void prepareStateMachine(DObPurchaseOrderStateMachineFactory stateMachineFactory) {
    try {
      Mockito.when(
              stateMachineFactory.createPurchaseOrderStateMachine((DObPurchaseOrder) Mockito.any()))
          .thenReturn(null);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }

  public void assertStateMachineCalled(DObPurchaseOrderStateMachineFactory stateMachineFactory)
      throws Exception {
    Mockito.verify(stateMachineFactory, Mockito.times(1))
        .createPurchaseOrderStateMachine((DObPurchaseOrder) Mockito.any());
  }

  public DObServicePurchaseOrder preparePurchaseOrderFactory(
      DObPurchaseOrderFactory purchaseOrderFactory) {
    try {
      DObServicePurchaseOrder purchaseOrder = ServicePurchaseOrderTestUtils.newEntity().build();
      Mockito.when(purchaseOrderFactory.createPurchaseOrder(Mockito.any()))
          .thenReturn(purchaseOrder);
      return purchaseOrder;
    } catch (Exception e) {
      Assert.fail(e.getMessage());
      return null;
    }
  }

  public void assertPurchaseOrderCreated() throws Exception {
    Mockito.verify(PurchaseOrderRepTestUtils.getRepository(), Mockito.times(1))
        .create(Mockito.any());
  }
}
