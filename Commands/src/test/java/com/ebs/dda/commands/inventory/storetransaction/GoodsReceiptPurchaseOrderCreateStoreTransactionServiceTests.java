package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderCreateStoreTransactionService;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum.StockType;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionDataMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionMockUtils;
import com.ebs.repositories.inventory.storetransaction.GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GoodsReceiptPurchaseOrderCreateStoreTransactionServiceTests {

  private CommandTestUtils commandTestUtils;
  private GoodsReceiptPurchaseOrderCreateStoreTransactionService service;

  @Mock private DocumentObjectCodeGenerator documentObjectCodeGenerator;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    service =
        new GoodsReceiptPurchaseOrderCreateStoreTransactionService(
            documentObjectCodeGenerator,
            GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.getRepository(),
            commandTestUtils.getUserAccountSecurityService());

    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @Test
  public void shouldSetEstimatedOrActualCostInStoreTransactions() throws Exception {

    // Arrange
    String goodsReceiptCode = "2021000001";
    String costingUserCode = "2021000001";

    String firstItemCode = "000001";
    String firstItemUomCode = "0019";
    String firstItemStockType = StockType.UNRESTRICTED_USE.name();

    String secondItemCode = "000002";
    String secondItemUomCode = "0019";
    String secondItemStockType = StockType.UNRESTRICTED_USE.name();

    String thirdItemStockType = StockType.DAMAGED_STOCK.name();

    Long goodsReceiptId = 1L;
    Long companyId = 3L;
    Long plantId = 4L;
    Long storehouseId = 5L;
    Long purchaseUnitId = 6L;

    BigDecimal firstItemQuantity = new BigDecimal("120");
    BigDecimal secondItemQuantity = new BigDecimal("100");
    BigDecimal thirdItemQuantity = new BigDecimal("20");

    Long firstItemId = 2L;
    Long secondItemId = 3L;

    Long firstItemReceivedUnitOfEntryId = 7L;
    Long secondItemReceivedUnitOfEntryId = 7L;

    // GoodsReceipt Items
    TObGoodsReceiptStoreTransactionDataGeneralModel firstGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .goodsReceiptId(goodsReceiptId)
            .itemId(firstItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .receivedUnitOfEntryId(firstItemReceivedUnitOfEntryId)
            .receivedQty(firstItemQuantity)
            .stockType(firstItemStockType)
            .itemCode(firstItemCode)
            .receivedUnitOfEntryCode(firstItemUomCode)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel secondGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .goodsReceiptId(goodsReceiptId)
            .itemId(secondItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .receivedUnitOfEntryId(secondItemReceivedUnitOfEntryId)
            .receivedQty(secondItemQuantity)
            .stockType(secondItemStockType)
            .itemCode(secondItemCode)
            .receivedUnitOfEntryCode(secondItemUomCode)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel thirdGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .goodsReceiptId(goodsReceiptId)
            .itemId(secondItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .receivedUnitOfEntryId(secondItemReceivedUnitOfEntryId)
            .receivedQty(thirdItemQuantity)
            .stockType(thirdItemStockType)
            .itemCode(secondItemCode)
            .receivedUnitOfEntryCode(secondItemUomCode)
            .build();

    List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItems = new ArrayList<>();
    goodsReceiptItems.add(firstGoodsReceiptItem);
    goodsReceiptItems.add(secondGoodsReceiptItem);
    goodsReceiptItems.add(thirdGoodsReceiptItem);

    GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.findByUserCode(goodsReceiptItems);

    // Expecting Store Transactions
    StockTypeEnum.StockType unrestrictedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(firstItemStockType);
    StockTypeEnum unrestrictedStockTypeEnum = new StockTypeEnum();
    unrestrictedStockTypeEnum.setId(unrestrictedStockTypeEnumObject);

    StockTypeEnum.StockType damagedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(thirdItemStockType);
    StockTypeEnum damagedStockTypeEnum = new StockTypeEnum();
    damagedStockTypeEnum.setId(damagedStockTypeEnumObject);

    TransactionTypeEnum transactionOperationEnum = new TransactionTypeEnum();
    TransactionTypeEnum.TransactionType transactionOperationType =
        TransactionTypeEnum.TransactionType.ADD;
    transactionOperationEnum.setId(transactionOperationType);

    TObGoodsReceiptStoreTransaction firstStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000005")
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .documentReferenceId(goodsReceiptId)
            .itemId(firstItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .originalAddingDate(DateTime.now())
            .unitOfMeasureId(firstItemReceivedUnitOfEntryId)
            .stockType(unrestrictedStockTypeEnum)
            .transactionOperation(transactionOperationEnum)
            .quantity(firstItemQuantity)
            .remainingQuantity(firstItemQuantity)
            .build();

    TObGoodsReceiptStoreTransaction secondStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000006")
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .documentReferenceId(goodsReceiptId)
            .itemId(secondItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .originalAddingDate(DateTime.now())
            .unitOfMeasureId(secondItemReceivedUnitOfEntryId)
            .stockType(unrestrictedStockTypeEnum)
            .transactionOperation(transactionOperationEnum)
            .quantity(secondItemQuantity)
            .remainingQuantity(secondItemQuantity)
            .build();

    TObGoodsReceiptStoreTransaction thirdStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000007")
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .documentReferenceId(goodsReceiptId)
            .itemId(secondItemId)
            .companyId(companyId)
            .plantId(plantId)
            .storehouseId(storehouseId)
            .purchaseUnitId(purchaseUnitId)
            .originalAddingDate(DateTime.now())
            .unitOfMeasureId(secondItemReceivedUnitOfEntryId)
            .stockType(damagedStockTypeEnum)
            .transactionOperation(transactionOperationEnum)
            .quantity(thirdItemQuantity)
            .remainingQuantity(thirdItemQuantity)
            .build();

    List<TObGoodsReceiptStoreTransaction> expectedGoodsReceiptStoreTransactions = new ArrayList<>();
    expectedGoodsReceiptStoreTransactions.add(firstStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(secondStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(thirdStoreTransaction);

    // Act
    List<TObGoodsReceiptStoreTransaction> actualStoreTransactions =
        service.getGoodsReceiptPurchaseOrderStoreTransaction(goodsReceiptCode);

    // Assert
    Mockito.verify(
            GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.getRepository(),
            Mockito.times(1))
        .findByUserCode(goodsReceiptCode);

    assertThatExpectedStoreTransactionsMatchesActual(
        expectedGoodsReceiptStoreTransactions, actualStoreTransactions);
  }

  private void assertThatExpectedStoreTransactionsMatchesActual(
      List<TObGoodsReceiptStoreTransaction> expectedGoodsReceiptStoreTransactions,
      List<TObGoodsReceiptStoreTransaction> actualStoreTransactions) {

    Assert.assertEquals(
        expectedGoodsReceiptStoreTransactions.size(), actualStoreTransactions.size());

    for (TObGoodsReceiptStoreTransaction expected : expectedGoodsReceiptStoreTransactions) {

      TObGoodsReceiptStoreTransaction transaction =
          actualStoreTransactions.stream()
              .filter(actual -> expected.getCreationInfo().equals(actual.getCreationInfo()))
              .filter(actual -> expected.getModificationInfo().equals(actual.getModificationInfo()))
              .filter(actual -> expected.getCreationDate().equals(actual.getCreationDate()))
              .filter(actual -> expected.getModifiedDate().equals(actual.getModifiedDate()))
              .filter(
                  actual ->
                      expected.getDocumentReferenceId().equals(actual.getDocumentReferenceId()))
              .filter(actual -> expected.getItemId().equals(actual.getItemId()))
              .filter(actual -> expected.getCompanyId().equals(actual.getCompanyId()))
              .filter(actual -> expected.getPlantId().equals(actual.getPlantId()))
              .filter(actual -> expected.getStorehouseId().equals(actual.getStorehouseId()))
              .filter(actual -> expected.getPurchaseUnitId().equals(actual.getPurchaseUnitId()))
              .filter(
                  actual -> expected.getOriginalAddingDate().equals(actual.getOriginalAddingDate()))
              .filter(actual -> expected.getUnitOfMeasureId().equals(actual.getUnitOfMeasureId()))
              .filter(
                  actual -> expected.getStockType().getId().equals(actual.getStockType().getId()))
              .filter(
                  actual ->
                      expected
                          .getTransactionOperation()
                          .getId()
                          .equals(actual.getTransactionOperation().getId()))
              .filter(actual -> expected.getQuantity().compareTo(actual.getQuantity()) == 0)
              .filter(
                  actual ->
                      expected.getRemainingQuantity().compareTo(actual.getRemainingQuantity()) == 0)
              .findAny()
              .get();

      Assert.assertNotNull(transaction);
    }
  }
}
