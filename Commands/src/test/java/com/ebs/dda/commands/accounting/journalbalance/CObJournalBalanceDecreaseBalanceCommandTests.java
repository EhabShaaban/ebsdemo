package com.ebs.dda.commands.accounting.journalbalance;

import com.ebs.dda.commands.accounting.journalbalance.utils.CObJournalBalanceDecreaseBalanceCommandTestUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CObJournalBalanceDecreaseBalanceCommandTests {
    private CObJournalBalanceDecreaseBalanceCommandTestUtils utils;

    @Before
    public void setUp() {
        utils = new CObJournalBalanceDecreaseBalanceCommandTestUtils();
    }

    @Test
    public void testDecreaseBalanceWithPaymentAmount() throws Exception {
        utils.withBalanceCode("TREASURY0001000100020001")
                .withBalanceAmount(BigDecimal.valueOf(1000000))
                .withPaymentAmount(BigDecimal.valueOf(500))
                .whenExecute()
                .assertThatBalanceDecreasedTo(BigDecimal.valueOf(999500));
    }
}
