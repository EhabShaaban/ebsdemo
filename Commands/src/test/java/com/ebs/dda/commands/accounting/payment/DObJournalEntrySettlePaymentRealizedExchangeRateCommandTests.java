package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObJournalEntrySettlePaymentRealizedExchangeRateCommandTests {

    private DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils utils;

    @Before
    public void setup() {
        utils = new DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils();
    }

    @Test
    public void shouldCreditRealizedDifferenceAndDebitVendorWhenExchangeRateIsDecreased() throws Exception {
        utils.withPaymentCode("2021000001")
                .withJournalEntryCode("2021000010")
                .withVendorId(45L)
                .withLocalCurrencyId(1L)
                .withVendorSubLedger("Import_Vendors")
                .withGlVendorAccountId(21L)
                .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(16))
                .withCurrentCurrencyPrice(BigDecimal.valueOf(12.5))
                .withPaymentAmount(BigDecimal.valueOf(300))
                .whenExecute()
                .assertThatJournalEntriesIsUpdated()
                .assertThatJournalEntryIsRecorded(21L,BigDecimal.valueOf(1050), AccountingEntry.DEBIT.name())
                .assertThatJournalEntryIsRecorded(14L, BigDecimal.valueOf(1050), AccountingEntry.CREDIT.name());
    }

    @Test
    public void shouldDebitRealizedDifferenceAndCreditVendorWhenExchangeRateIsIncreased() throws Exception {
        utils.withPaymentCode("2021000001")
                .withJournalEntryCode("2021000010")
                .withVendorId(45L)
                .withLocalCurrencyId(1L)
                .withVendorSubLedger("Import_Vendors")
                .withGlVendorAccountId(21L)
                .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(12.5))
                .withCurrentCurrencyPrice(BigDecimal.valueOf(16))
                .withPaymentAmount(BigDecimal.valueOf(300))
                .whenExecute()
                .assertThatJournalEntriesIsUpdated()
                .assertThatJournalEntryIsRecorded(21L,BigDecimal.valueOf(1050), AccountingEntry.CREDIT.name())
                .assertThatJournalEntryIsRecorded(14L, BigDecimal.valueOf(1050), AccountingEntry.DEBIT.name());
    }

    @Test
    public void shouldNotCreateRealizedDifferenceWhenExchangeRateNotChanged() throws Exception {
        utils.withPaymentCode("2021000001")
                .withJournalEntryCode("2021000010")
                .withVendorId(45L)
                .withLocalCurrencyId(1L)
                .withVendorSubLedger("Import_Vendors")
                .withGlVendorAccountId(21L)
                .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(12.5))
                .withCurrentCurrencyPrice(BigDecimal.valueOf(12.5))
                .withPaymentAmount(BigDecimal.valueOf(300))
                .whenExecute()
                .assertThatNoJournalEntryIsAdded();
    }
}
