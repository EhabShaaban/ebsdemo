package com.ebs.dda.commands.accounting.notesreceivable.validation.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidator;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidatorFactory;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesCreateValueObject;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;

public class DObNotesReceivableCreationSchemaTestUtils {

  public static final Object MISSING_KEY_MARKER = "N/A";
  private JsonObject requestBody;

  public DObNotesReceivableCreationSchemaTestUtils() {
    requestBody = new JsonObject();
  }

  public DObNotesReceivableCreationSchemaTestUtils noteForm(Object value) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.NOTE_FORM, value);
  }

  public DObNotesReceivableCreationSchemaTestUtils type(Object value) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.TYPE, value);
  }

  public DObNotesReceivableCreationSchemaTestUtils withBusinessUnitCode(Object value) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.BUSINESS_UNIT_CODE, value);
  }

  public DObNotesReceivableCreationSchemaTestUtils withCompanyCode(Object companyCode) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.COMPANY_CODE, companyCode);
  }

  public DObNotesReceivableCreationSchemaTestUtils withBusinessPartnerCode(Object businessPartner) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.BUSINESS_PARTNER_CODE,
      businessPartner);
  }

  public DObNotesReceivableCreationSchemaTestUtils withDocumentOwnerId(Object documentOwnerId) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.DOCUMENT_OWNER_ID, documentOwnerId);
  }

  public DObNotesReceivableCreationSchemaTestUtils withAmount(Object amount) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.AMOUNT, amount);
  }

  public DObNotesReceivableCreationSchemaTestUtils withCurrencyIso(Object currencyIso) {
    return setFieldValue(IDObNotesReceivablesCreateValueObject.CURRENCY_ISO, currencyIso);
  }

  private DObNotesReceivableCreationSchemaTestUtils setFieldValue(String field, Object value) {
    if (value == null) {
      requestBody.add(field, JsonNull.INSTANCE);
    }
    if (value instanceof String) {
      if (value.equals(MISSING_KEY_MARKER)) {
        return this;
      }
      requestBody.addProperty(field, (String) value);
    }
    if (value instanceof Number) {
      requestBody.addProperty(field, (Number) value);
    }
    return this;
  }

  public void assertThrowsArgumentSecurityException() {
    Assertions.assertThrows(ArgumentViolationSecurityException.class, this::validateJsonSchema);
  }

  public void assertValidateSuccessfully() throws Exception {
    validateJsonSchema();
  }

  private void validateJsonSchema() throws Exception {
    JsonSchemaValidator schemaValidator = new JsonSchemaValidator().jsonSchemaFactory(
      new JsonSchemaValidatorFactory(IDObNotesReceivableJsonSchema.CREATE_SCHEMA));
    schemaValidator.validateSchemaForString(requestBody.toString());
  }

}
