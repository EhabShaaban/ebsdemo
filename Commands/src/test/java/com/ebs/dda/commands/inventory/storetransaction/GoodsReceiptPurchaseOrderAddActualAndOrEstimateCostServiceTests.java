package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum.StockType;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingItemMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionDataMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingItemGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.storetransaction.GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostServiceTests {

  private GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService service;

  @Before
  public void setUp() throws Exception {
    service =
        new GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
            CostingItemGeneralModelRepMockUtils.getRepository(),
            CostingDetailsGeneralModelRepMockUtils.getRepository(),
            GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.getRepository());
  }

  @Test
  public void shouldCalculateAndSetEstimatedCostForStoreTransactionsIfCostIsEstimated() {

    // Arrange
    String goodsReceiptCode = "2021000001";
    String costingUserCode = "2021000001";

    String firstCostingItemCode = "000001";
    String firstCostingItemUomCode = "0019";
    String firstCostingItemStockType = StockType.UNRESTRICTED_USE.name();

    String secondCostingItemCode = "000002";
    String secondCostingItemUomCode = "0019";
    String secondCostingItemStockType = StockType.UNRESTRICTED_USE.name();

    String thirdCostingItemCode = "000002";
    String thirdCostingItemUomCode = "0019";
    String thirdCostingItemStockType = StockType.DAMAGED_STOCK.name();

    // GoodsReceipt Items
    TObGoodsReceiptStoreTransactionDataGeneralModel firstGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(1L)
            .itemCode(firstCostingItemCode)
            .receivedUnitOfEntryId(1L)
            .receivedUnitOfEntryCode(firstCostingItemUomCode)
            .stockType(firstCostingItemStockType)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel secondGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(2L)
            .itemCode(secondCostingItemCode)
            .receivedUnitOfEntryId(2L)
            .receivedUnitOfEntryCode(secondCostingItemUomCode)
            .stockType(secondCostingItemStockType)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel thirdGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(2L)
            .itemCode(thirdCostingItemCode)
            .receivedUnitOfEntryId(2L)
            .receivedUnitOfEntryCode(thirdCostingItemUomCode)
            .stockType(thirdCostingItemStockType)
            .build();

    List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItems = new ArrayList<>();
    goodsReceiptItems.add(firstGoodsReceiptItem);
    goodsReceiptItems.add(secondGoodsReceiptItem);
    goodsReceiptItems.add(thirdGoodsReceiptItem);

    GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.findByUserCode(goodsReceiptItems);

    // Costing Details
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .goodsReceiptUserCode(goodsReceiptCode)
            .build();
    CostingDetailsGeneralModelRepMockUtils.findOneByGoodsReceiptCode(costingDetailsGeneralModel);

    // Costing Items
    IObCostingItemGeneralModel firstCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(firstCostingItemCode)
            .uomCode(firstCostingItemUomCode)
            .stockType(firstCostingItemStockType)
            .estimateUnitPrice(new BigDecimal("3"))
            .estimateUnitLandedCost(new BigDecimal("2"))
            .build();

    IObCostingItemGeneralModel secondCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(secondCostingItemCode)
            .uomCode(secondCostingItemUomCode)
            .stockType(secondCostingItemStockType)
            .estimateUnitPrice(new BigDecimal("4"))
            .estimateUnitLandedCost(new BigDecimal("6"))
            .build();

    IObCostingItemGeneralModel thirdCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(thirdCostingItemCode)
            .uomCode(thirdCostingItemUomCode)
            .stockType(thirdCostingItemStockType)
            .estimateUnitPrice(BigDecimal.ZERO)
            .estimateUnitLandedCost(BigDecimal.ZERO)
            .build();

    List<IObCostingItemGeneralModel> costingItems = new ArrayList<>();
    costingItems.add(firstCostingItemsGeneralModel);
    costingItems.add(secondCostingItemsGeneralModel);
    costingItems.add(thirdCostingItemsGeneralModel);

    CostingItemGeneralModelRepMockUtils.findByUserCodeOrderByItemCodeAsc(costingItems);

    // Existing Store Transactions without costs
    StockTypeEnum.StockType unrestrictedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(firstCostingItemStockType);
    StockTypeEnum unrestrictedStockTypeEnum = new StockTypeEnum();
    unrestrictedStockTypeEnum.setId(unrestrictedStockTypeEnumObject);

    StockTypeEnum.StockType damagedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(thirdCostingItemStockType);
    StockTypeEnum damagedStockTypeEnum = new StockTypeEnum();
    damagedStockTypeEnum.setId(damagedStockTypeEnumObject);

    TObGoodsReceiptStoreTransaction firstStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000005")
            .itemId(1L)
            .unitOfMeasureId(1L)
            .stockType(unrestrictedStockTypeEnum)
            .build();

    TObGoodsReceiptStoreTransaction secondStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000006")
            .itemId(2L)
            .unitOfMeasureId(2L)
            .stockType(unrestrictedStockTypeEnum)
            .build();

    TObGoodsReceiptStoreTransaction thirdStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000007")
            .itemId(2L)
            .unitOfMeasureId(2L)
            .stockType(damagedStockTypeEnum)
            .build();

    List<TObGoodsReceiptStoreTransaction> expectedGoodsReceiptStoreTransactions = new ArrayList<>();
    expectedGoodsReceiptStoreTransactions.add(firstStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(secondStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(thirdStoreTransaction);

    // Act
    service.setGoodsReceiptPurchaseOrderStoreTransactionCosts(
        goodsReceiptCode, expectedGoodsReceiptStoreTransactions);

    // Assert

    BigDecimal firstItemExpectedEstimatedCost = new BigDecimal("5");
    BigDecimal secondItemExpectedEstimatedCost = BigDecimal.TEN;
    BigDecimal thirdItemExpectedEstimatedCost = BigDecimal.ZERO;

    Mockito.verify(firstStoreTransaction, Mockito.times(1))
        .setEstimateCost(firstItemExpectedEstimatedCost);
    Mockito.verify(firstStoreTransaction, Mockito.times(0))
        .setActualCost(firstItemExpectedEstimatedCost);
    Mockito.verify(firstStoreTransaction, Mockito.never()).setUpdatingActualCostDate(Mockito.any());

    Mockito.verify(secondStoreTransaction, Mockito.times(1))
        .setEstimateCost(secondItemExpectedEstimatedCost);
    Mockito.verify(secondStoreTransaction, Mockito.times(0))
        .setActualCost(secondItemExpectedEstimatedCost);
    Mockito.verify(secondStoreTransaction, Mockito.never())
        .setUpdatingActualCostDate(Mockito.any());

    Mockito.verify(thirdStoreTransaction, Mockito.times(1))
        .setEstimateCost(thirdItemExpectedEstimatedCost);
    Mockito.verify(thirdStoreTransaction, Mockito.times(0))
        .setActualCost(thirdItemExpectedEstimatedCost);
    Mockito.verify(thirdStoreTransaction, Mockito.never()).setUpdatingActualCostDate(Mockito.any());
  }

  @Test
  public void shouldCalculateAndSetActualCostForStoreTransactionsIfCostIsActual() {
    DateTimeUtils.setCurrentMillisFixed(DateTime.now().getMillis());
    // Arrange
    String goodsReceiptCode = "2021000001";
    String costingUserCode = "2021000001";

    String firstCostingItemCode = "000001";
    String firstCostingItemUomCode = "0019";
    String firstCostingItemStockType = StockType.UNRESTRICTED_USE.name();

    String secondCostingItemCode = "000002";
    String secondCostingItemUomCode = "0019";
    String secondCostingItemStockType = StockType.UNRESTRICTED_USE.name();

    String thirdCostingItemCode = "000002";
    String thirdCostingItemUomCode = "0019";
    String thirdCostingItemStockType = StockType.DAMAGED_STOCK.name();

    // GoodsReceipt Items
    TObGoodsReceiptStoreTransactionDataGeneralModel firstGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(1L)
            .itemCode(firstCostingItemCode)
            .receivedUnitOfEntryId(1L)
            .receivedUnitOfEntryCode(firstCostingItemUomCode)
            .stockType(firstCostingItemStockType)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel secondGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(2L)
            .itemCode(secondCostingItemCode)
            .receivedUnitOfEntryId(2L)
            .receivedUnitOfEntryCode(secondCostingItemUomCode)
            .stockType(secondCostingItemStockType)
            .build();

    TObGoodsReceiptStoreTransactionDataGeneralModel thirdGoodsReceiptItem =
        GoodsReceiptStoreTransactionDataMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemId(2L)
            .itemCode(thirdCostingItemCode)
            .receivedUnitOfEntryId(2L)
            .receivedUnitOfEntryCode(thirdCostingItemUomCode)
            .stockType(thirdCostingItemStockType)
            .build();

    List<TObGoodsReceiptStoreTransactionDataGeneralModel> goodsReceiptItems = new ArrayList<>();
    goodsReceiptItems.add(firstGoodsReceiptItem);
    goodsReceiptItems.add(secondGoodsReceiptItem);
    goodsReceiptItems.add(thirdGoodsReceiptItem);

    GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.findByUserCode(goodsReceiptItems);

    // Costing Details
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .goodsReceiptUserCode(goodsReceiptCode)
            .build();
    CostingDetailsGeneralModelRepMockUtils.findOneByGoodsReceiptCode(costingDetailsGeneralModel);

    // Costing Items
    IObCostingItemGeneralModel firstCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(firstCostingItemCode)
            .uomCode(firstCostingItemUomCode)
            .stockType(firstCostingItemStockType)
            .actualUnitPrice(new BigDecimal("3"))
            .actualUnitLandedCost(new BigDecimal("2"))
            .build();

    IObCostingItemGeneralModel secondCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(secondCostingItemCode)
            .uomCode(secondCostingItemUomCode)
            .stockType(secondCostingItemStockType)
            .actualUnitPrice(new BigDecimal("4"))
            .actualUnitLandedCost(new BigDecimal("6"))
            .build();

    IObCostingItemGeneralModel thirdCostingItemsGeneralModel =
        CostingItemMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .itemCode(thirdCostingItemCode)
            .uomCode(thirdCostingItemUomCode)
            .stockType(thirdCostingItemStockType)
            .actualUnitPrice(BigDecimal.ZERO)
            .actualUnitLandedCost(BigDecimal.ZERO)
            .build();

    List<IObCostingItemGeneralModel> costingItems = new ArrayList<>();
    costingItems.add(firstCostingItemsGeneralModel);
    costingItems.add(secondCostingItemsGeneralModel);
    costingItems.add(thirdCostingItemsGeneralModel);

    CostingItemGeneralModelRepMockUtils.findByUserCodeOrderByItemCodeAsc(costingItems);

    // Existing Store Transactions without costs
    StockTypeEnum.StockType unrestrictedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(firstCostingItemStockType);
    StockTypeEnum unrestrictedStockTypeEnum = new StockTypeEnum();
    unrestrictedStockTypeEnum.setId(unrestrictedStockTypeEnumObject);

    StockTypeEnum.StockType damagedStockTypeEnumObject =
        StockTypeEnum.StockType.valueOf(thirdCostingItemStockType);
    StockTypeEnum damagedStockTypeEnum = new StockTypeEnum();
    damagedStockTypeEnum.setId(damagedStockTypeEnumObject);

    TObGoodsReceiptStoreTransaction firstStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000005")
            .itemId(1L)
            .unitOfMeasureId(1L)
            .stockType(unrestrictedStockTypeEnum)
            .build();

    TObGoodsReceiptStoreTransaction secondStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000006")
            .itemId(2L)
            .unitOfMeasureId(2L)
            .stockType(unrestrictedStockTypeEnum)
            .build();

    TObGoodsReceiptStoreTransaction thirdStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000007")
            .itemId(2L)
            .unitOfMeasureId(2L)
            .stockType(damagedStockTypeEnum)
            .build();

    List<TObGoodsReceiptStoreTransaction> expectedGoodsReceiptStoreTransactions = new ArrayList<>();
    expectedGoodsReceiptStoreTransactions.add(firstStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(secondStoreTransaction);
    expectedGoodsReceiptStoreTransactions.add(thirdStoreTransaction);

    // Act
    service.setGoodsReceiptPurchaseOrderStoreTransactionCosts(
        goodsReceiptCode, expectedGoodsReceiptStoreTransactions);

    // Assert
    BigDecimal firstItemExpectedEstimatedCost = new BigDecimal("5");
    BigDecimal secondItemExpectedEstimatedCost = BigDecimal.TEN;
    BigDecimal thirdItemExpectedEstimatedCost = BigDecimal.ZERO;

    Mockito.verify(firstStoreTransaction, Mockito.times(0))
        .setEstimateCost(firstItemExpectedEstimatedCost);
    Mockito.verify(firstStoreTransaction, Mockito.times(1))
        .setActualCost(firstItemExpectedEstimatedCost);
    Mockito.verify(firstStoreTransaction, Mockito.times(1))
        .setUpdatingActualCostDate(DateTime.now());

    Mockito.verify(secondStoreTransaction, Mockito.times(0))
        .setEstimateCost(secondItemExpectedEstimatedCost);
    Mockito.verify(secondStoreTransaction, Mockito.times(1))
        .setActualCost(secondItemExpectedEstimatedCost);
    Mockito.verify(secondStoreTransaction, Mockito.times(1))
            .setUpdatingActualCostDate(DateTime.now());

    Mockito.verify(thirdStoreTransaction, Mockito.times(0))
        .setEstimateCost(thirdItemExpectedEstimatedCost);
    Mockito.verify(thirdStoreTransaction, Mockito.times(1))
        .setActualCost(thirdItemExpectedEstimatedCost);
    Mockito.verify(thirdStoreTransaction, Mockito.times(1))
            .setUpdatingActualCostDate(DateTime.now());
  }

  @After
  public void tearDown() {
    CostingItemGeneralModelRepMockUtils.resetRepository();
    CostingDetailsGeneralModelRepMockUtils.resetRepository();
    GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils.resetRepository();
    DateTimeUtils.setCurrentMillisSystem();
  }
}
