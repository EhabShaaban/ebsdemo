package com.ebs.dda.commands.accounting.notesreceivable;

import com.ebs.dda.commands.accounting.notesreceivable.utils.IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class IObNotesReceivableRefDocumentUpdateRemainingCommandTest {
  IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils utils;

  @Before
  public void init() throws Exception {
    utils = new IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils();
  }

  @Test
  public void Should_UpdateNRRefDocumentRemaining() {
    utils
        .withNotesReceivableCode("2021000001")
        .withNotesReceivableCurrencyIso("EGP")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withUser("Ahmed.Hamdi")
        .withExchangeRateData("EGP", "EGP", new BigDecimal(1))
        .whenExecute()
        .thenAssertNotesReceivableRemainingIsUpdatedAs(new BigDecimal(1000));
  }

  @Test
  public void Should_UpdateSORefDocumentRemaining() {
    utils
        .withNotesReceivableCode("2021000001")
        .withNotesReceivableCurrencyIso("EGP")
        .withReferenceDocumentData("2021000001", "2021000001", new BigDecimal(5000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withUser("Ahmed.Hamdi")
        .withExchangeRateData("USD", "EGP", new BigDecimal(10))
        .whenExecute()
        .thenAssertSalesOrderRemainingIsUpdatedAs(new BigDecimal(1000));
  }
  @Test
  public void Should_UpdateSIRefDocumentRemaining() {
    utils
        .withNotesReceivableCode("2021000001")
        .withNotesReceivableCurrencyIso("USD")
        .withReferenceDocumentData("2021000001", "2021000001", new BigDecimal(500), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withUser("Ahmed.Hamdi")
        .withExchangeRateData("USD", "EGP", new BigDecimal(10))
        .whenExecute()
        .thenAssertSalesInvoiceRemainingIsUpdatedAs(new BigDecimal(100));
  }
}
