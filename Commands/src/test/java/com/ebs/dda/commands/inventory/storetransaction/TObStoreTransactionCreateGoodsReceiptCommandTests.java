package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderCreateStoreTransactionService;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionRep;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionMockUtils;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class TObStoreTransactionCreateGoodsReceiptCommandTests {
  private TObStoreTransactionCreateGoodsReceiptCommand command;
  private CommandTestUtils commandTestUtils;

  @Mock private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  @Mock private TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep;

  @Mock
  private GoodsReceiptPurchaseOrderCreateStoreTransactionService
      goodsReceiptPurchaseOrderCreateStoreTransactionService;

  @Mock
  private GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
      goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;

  @Before
  public void before() {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    command = new TObStoreTransactionCreateGoodsReceiptCommand(documentObjectCodeGenerator);
    command.setGoodsReceiptPurchaseOrderCreateStoreTransactionService(
        goodsReceiptPurchaseOrderCreateStoreTransactionService);
    command.setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
        goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService);
    command.setGoodsReceiptStoreTransactionRep(goodsReceiptStoreTransactionRep);
  }

  @Test
  public void shouldCallGoodsReceiptPurchaseOrderStoreTransactionServiceAndAddActualAndOrEstimateCostService() throws Exception {
    // arrange
    String goodsReceiptCode = "2021000001";
    String GR_PO_Type = "GR_PO";

    List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactions = new ArrayList<>();

    goodsReceiptStoreTransactions.add(
        GoodsReceiptStoreTransactionMockUtils.mockEntity().userCode("2021000005").build());

    goodsReceiptStoreTransactions.add(
        GoodsReceiptStoreTransactionMockUtils.mockEntity().userCode("2021000006").build());

    Mockito.doReturn(goodsReceiptStoreTransactions)
        .when(goodsReceiptPurchaseOrderCreateStoreTransactionService)
        .getGoodsReceiptPurchaseOrderStoreTransaction(goodsReceiptCode);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptCode);
    valueObject.setInventoryType(GR_PO_Type);

    // act
    command.executeCommand(valueObject);

    // assert
    Mockito.verify(goodsReceiptPurchaseOrderCreateStoreTransactionService, Mockito.times(1))
        .getGoodsReceiptPurchaseOrderStoreTransaction(goodsReceiptCode);

    Mockito.verify(goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService, Mockito.times(1))
        .setGoodsReceiptPurchaseOrderStoreTransactionCosts(goodsReceiptCode, goodsReceiptStoreTransactions);

    ArgumentCaptor<List<TObGoodsReceiptStoreTransaction>> createdObjectCaptor =
        ArgumentCaptor.forClass(List.class);

    Mockito.verify(goodsReceiptStoreTransactionRep, Mockito.times(1))
        .saveAll(createdObjectCaptor.capture());

    List<TObGoodsReceiptStoreTransaction> createdObject = createdObjectCaptor.getValue();

    Assert.assertNotNull(createdObject);
    Assert.assertFalse("Created Store Transactions should not be empty", createdObject.isEmpty());
  }
}
