package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObCostingJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObCostingJournalEntryRep;
import com.ebs.entities.ExchangeRateMockUtils;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.costing.CostingAccountingDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingCompanyMockUtils;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.repositories.ExchangeRateRepMockUtils;
import com.ebs.repositories.FiscalYearRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingAccountingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingCompanyDataGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(MockitoJUnitRunner.class)
public class DObCostingClosingJournalCreateCommandTests {
  public static final String GR_PO = "GR_PO";
  private final String ACTIVE_STATE = "Active";
  private final String dueDate = "06-Sep-2021 9:30 AM";

  @Mock private DocumentObjectCodeGenerator codeGenerator;
  @Mock private DObCostingJournalEntryRep costingJournalEntryRep;

  private DObCostingClosingJournalCreateCommand command;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    command = new DObCostingClosingJournalCreateCommand(codeGenerator);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setCostingCompanyDataGeneralModelRep(
        CostingCompanyDataGeneralModelRepMockUtils.getRepository());
    command.setCostingDetailsGeneralModelRep(
        CostingDetailsGeneralModelRepMockUtils.getRepository());
    command.setCostingJournalEntryRep(costingJournalEntryRep);
    command.setFiscalYearRep(FiscalYearRepMockUtils.getRepository());
    command.setCostingAccountingDetailsGeneralModelRep(
        CostingAccountingDetailsGeneralModelRepMockUtils.getRepository());
    command.setExchangeRateGeneralModelRep(ExchangeRateRepMockUtils.getRepository());
    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @After
  public void tearDown() {
    CostingCompanyDataGeneralModelRepMockUtils.resetRepository();
    CostingDetailsGeneralModelRepMockUtils.resetRepository();
    FiscalYearRepMockUtils.resetRepository();
    CostingAccountingDetailsGeneralModelRepMockUtils.resetRepository();
    ExchangeRateRepMockUtils.resetRepository();
    Mockito.reset(codeGenerator);
    Mockito.reset(costingJournalEntryRep);
  }

  @Test
  public void shouldCreateClosingJournalEntryForCostingDocument() throws Exception {
    //    Arrange
    String journalEntryCode = "2021000001";
    String goodsReceiptCode = "2021000001";
    String costingUserCode = "2021000001";
    Long companyId = 1L;
    Long purchaseUnitId = 2L;
    Long costingId = 3L;
    Long fiscalYear2020Id = 3L;
    Long fiscalYear2021Id = 4L;
    String activeState = "Active";
    String companyCurrencyISO = "EGP";
    Long companyCurrencyId = 5L;
    BigDecimal exchangeRatePriceValue = new BigDecimal("16.5");
    Long exchangeRateId = 6L;

    Mockito.when(codeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName()))
        .thenReturn(journalEntryCode);

    DObCostingActivateValueObject valueObject = new DObCostingActivateValueObject();
    valueObject.setUserCode(costingUserCode);
    valueObject.setDueDate(dueDate);

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .refInstanceId(costingId)
            .userCode(costingUserCode)
            .goodsReceiptUserCode(goodsReceiptCode)
            .build();
    CostingDetailsGeneralModelRepMockUtils.findOneByUserCode(costingDetailsGeneralModel);

    IObCostingCompanyDataGeneralModel costingCompanyDataGeneralModel =
        CostingCompanyMockUtils.mockGeneralModel()
            .companyId(companyId)
            .documentCode(costingUserCode)
            .purchaseUnitId(purchaseUnitId)
            .build();
    CostingCompanyDataGeneralModelRepMockUtils.findOneByDocumentCode(
        costingCompanyDataGeneralModel);

    CObFiscalYear fiscalYear2020 =
        FiscalYearMockUtils.mockEntity()
            .id(fiscalYear2020Id)
            .currentState(activeState)
            .fromDate(
                new DateTime(
                    commandTestUtils.getDateTimeInMilliSecondsFormat("01-JAN-2020 00:00 AM")))
            .toDate(
                new DateTime(
                    commandTestUtils.getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
            .build();
    CObFiscalYear fiscalYear2021 =
        FiscalYearMockUtils.mockEntity()
            .id(fiscalYear2021Id)
            .currentState(activeState)
            .fromDate(
                new DateTime(
                    commandTestUtils.getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
            .toDate(
                new DateTime(
                    commandTestUtils.getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
            .build();
    FiscalYearRepMockUtils.mockFindActiveFiscalYears(fiscalYear2020, fiscalYear2021);

    commandTestUtils.mockUserAccount("Admin");

    mockAccountingDetailsList(costingId, companyCurrencyISO);

    CObExchangeRateGeneralModel exchangeRate =
        ExchangeRateMockUtils.mockGeneralModel()
            .id(exchangeRateId)
            .secondValue(exchangeRatePriceValue)
            .firstCurrencyId(companyCurrencyId)
            .secondCurrencyId(companyCurrencyId)
            .build();
    ExchangeRateRepMockUtils.findLatestDefaultExchangeRate(
        exchangeRate, companyCurrencyISO, companyCurrencyISO);

    //    Act
    command.executeCommand(valueObject);

    //    Assert
    Mockito.verify(codeGenerator, Mockito.times(1))
        .generateUserCode(DObJournalEntry.class.getSimpleName());

    Mockito.verify(CostingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(valueObject.getUserCode());

    Mockito.verify(CostingCompanyDataGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByDocumentCode(costingUserCode);

    Mockito.verify(FiscalYearRepMockUtils.getRepository(), Mockito.times(1))
        .findByCurrentStatesLike("%" + activeState + "%");

    Mockito.verify(
            CostingAccountingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findByRefInstanceId(costingId);

    Mockito.verify(ExchangeRateRepMockUtils.getRepository(), Mockito.times(1))
        .findLatestDefaultExchangeRate(companyCurrencyISO, companyCurrencyISO);

    ArgumentCaptor<DObCostingJournalEntry> journalEntryArgumentCaptor =
        ArgumentCaptor.forClass(DObCostingJournalEntry.class);

    Mockito.verify(costingJournalEntryRep, Mockito.times(1))
        .create(journalEntryArgumentCaptor.capture());

    DObCostingJournalEntry journalEntry = journalEntryArgumentCaptor.getValue();
    assertJournalEntry(
        journalEntryCode, companyId, purchaseUnitId, costingId, fiscalYear2021Id, journalEntry);

    Collection<IObJournalEntryItem> journalEntryItems =
        journalEntry.getAllLines(IDObJournalEntry.journalItemAttr);

    assertJournalEntryItems(
        companyCurrencyId, exchangeRatePriceValue, exchangeRateId, journalEntryItems);
  }

  @Test
  public void shouldNotCreateClosingJournalEntryWhenAccountingDetailsIsEmpty() throws Exception {
    //    Arrange
    String goodsReceiptCode = "2021000001";
    String costingUserCode = "2021000001";
    Long companyId = 1L;
    Long purchaseUnitId = 2L;
    Long costingId = 3L;
    String companyCurrencyISO = "EGP";

    DObCostingActivateValueObject valueObject = new DObCostingActivateValueObject();
    valueObject.setUserCode(costingUserCode);
    valueObject.setDueDate(dueDate);

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .refInstanceId(costingId)
            .userCode(costingUserCode)
            .goodsReceiptUserCode(goodsReceiptCode)
            .build();
    CostingDetailsGeneralModelRepMockUtils.findOneByUserCode(costingDetailsGeneralModel);

    IObCostingCompanyDataGeneralModel costingCompanyDataGeneralModel =
        CostingCompanyMockUtils.mockGeneralModel()
            .companyId(companyId)
            .documentCode(costingUserCode)
            .purchaseUnitId(purchaseUnitId)
            .build();
    CostingCompanyDataGeneralModelRepMockUtils.findOneByDocumentCode(
        costingCompanyDataGeneralModel);

    mockAccountingDetailsList(costingId, companyCurrencyISO);
    List<IObCostingAccountingDetailsGeneralModel> accountingDetails = new ArrayList<>();
    CostingAccountingDetailsGeneralModelRepMockUtils.findByRefInstanceId(
        accountingDetails, costingId);

    //    Act
    command.executeCommand(valueObject);

    //    Assert
    Mockito.verify(codeGenerator, Mockito.times(0))
        .generateUserCode(DObJournalEntry.class.getSimpleName());

    Mockito.verify(CostingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(valueObject.getUserCode());

    Mockito.verify(CostingCompanyDataGeneralModelRepMockUtils.getRepository(), Mockito.times(0))
        .findOneByDocumentCode(costingUserCode);

    Mockito.verify(FiscalYearRepMockUtils.getRepository(), Mockito.times(0))
        .findByCurrentStatesLike("%" + ACTIVE_STATE + "%");

    Mockito.verify(
            CostingAccountingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findByRefInstanceId(costingId);

    Mockito.verify(ExchangeRateRepMockUtils.getRepository(), Mockito.times(0))
        .findLatestDefaultExchangeRate(companyCurrencyISO, companyCurrencyISO);

    Mockito.verify(costingJournalEntryRep, Mockito.times(0)).create(Mockito.any());
  }

  private void assertJournalEntryItems(
      Long companyCurrencyId,
      BigDecimal exchangeRatePriceValue,
      Long exchangeRateId,
      Collection<IObJournalEntryItem> journalEntryItems) {
    Assert.assertEquals(5, journalEntryItems.size());
    journalEntryItems.stream()
        .forEach(
            journalEntryItem -> {
              long index = journalEntryItem.getAccountId() - 1;
              Assert.assertEquals((index + 2L), (long) journalEntryItem.getSubAccountId());
              if (isDebit(index)) {
                Assert.assertEquals(
                    0, journalEntryItem.getDebit().compareTo(BigDecimal.valueOf(index * 100)));
                Assert.assertEquals(0, journalEntryItem.getCredit().compareTo(BigDecimal.ZERO));
              }
              if (isCredit(index)) {
                Assert.assertEquals(
                    0, journalEntryItem.getCredit().compareTo(BigDecimal.valueOf(index * 100)));
                Assert.assertEquals(0, journalEntryItem.getDebit().compareTo(BigDecimal.ZERO));
              }
              Assert.assertEquals(exchangeRateId, journalEntryItem.getCompanyExchangeRateId());
              Assert.assertEquals(companyCurrencyId, journalEntryItem.getDocumentCurrencyId());
              Assert.assertEquals(companyCurrencyId, journalEntryItem.getCompanyCurrencyId());
              Assert.assertEquals(exchangeRatePriceValue, journalEntryItem.getCompanyCurrencyPrice());
            });
  }

  private void assertJournalEntry(
      String journalEntryCode,
      Long companyId,
      Long purchaseUnitId,
      Long costingId,
      Long fiscalYearId,
      DObCostingJournalEntry journalEntry) {
    Assert.assertEquals(journalEntryCode, journalEntry.getUserCode());
    Assert.assertEquals(purchaseUnitId, journalEntry.getPurchaseUnitId());
    Assert.assertEquals(companyId, journalEntry.getCompanyId());
    Assert.assertEquals(costingId, journalEntry.getDocumentReferenceId());
    Assert.assertEquals(fiscalYearId, journalEntry.getFiscalPeriodId());
    Assert.assertEquals(commandTestUtils.getDateTime(dueDate), journalEntry.getDueDate());
    Assert.assertEquals(
        commandTestUtils.getDateTime(dueDate).year().getAsText(),
        journalEntry.getDueDate().year().getAsText());
    Assert.assertEquals(ACTIVE_STATE, journalEntry.getCurrentState());
  }

  private void mockAccountingDetailsList(Long costingId, String companyCurrencyISO) {
    List<IObCostingAccountingDetailsGeneralModel> accountingDetails =
        IntStream.range(0, 5)
            .mapToObj(
                index ->
                    mockAccountingDetailItem(Long.valueOf(index), costingId, companyCurrencyISO))
            .collect(Collectors.toList());

    CostingAccountingDetailsGeneralModelRepMockUtils.findByRefInstanceId(
        accountingDetails, costingId);
  }

  private IObCostingAccountingDetailsGeneralModel mockAccountingDetailItem(
      Long index, Long costingId, String companyCurrencyISO) {
    return CostingAccountingDetailsMockUtils.mockGeneralModel()
        .glAccountId(index + 1L)
        .glSubAccountId(index + 2L)
        .accountingEntry(
            isDebit(index) ? AccountingEntry.DEBIT.name() : AccountingEntry.CREDIT.name())
        .amount(String.valueOf(index * 100))
        .currencyISO(companyCurrencyISO)
        .build();
  }

  private boolean isCredit(long index) {
    return index % 2 != 0;
  }

  private boolean isDebit(long index) {
    return index % 2 == 0;
  }
}
