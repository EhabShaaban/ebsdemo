package com.ebs.dda.commands.accounting.notesreceivable;

import com.ebs.dda.commands.accounting.notesreceivable.utils.DObJournalEntryCreateNotesReceivableCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DObJournalEntryCreateNotesReceivableCommandTest {

  private DObJournalEntryCreateNotesReceivableCommandTestUtils utils;

  @Before
  public void setup() {
    utils = new DObJournalEntryCreateNotesReceivableCommandTestUtils();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneSORefDoc() throws Exception {
    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSORefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneSIRefDoc() throws Exception {
    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSIRefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneNRRefDoc() throws Exception {
    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneNRRefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneSIAndSORefDoc() throws Exception {

    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSORefDoc()
      .withOneSIRefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneSIAndNRRefDoc() throws Exception {

    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSIRefDoc()
      .withOneNRRefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasMultipleSORefDoc() throws Exception {
    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSORefDoc()
      .withOneSORefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

  @Test
  public void Should_CreateJournalEntrySuccessfully_WhenNRHasOneSIAndSOAndNRRefDoc() throws Exception {

    utils
      .notesReceivableCode("2021000001")
      .journalEntryDate("08-Jul-2021 04:30 PM")
      .withOneSORefDoc()
      .withOneSIRefDoc()
      .withOneNRRefDoc()
      .assertThatJournalEntryCreatedSuccessfully();
  }

}
