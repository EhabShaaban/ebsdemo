package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.order.purchaseorder.DObPurchaseOrderMockUtils;
import com.ebs.repositories.payment.PaymentRequestRepUtils;
import com.ebs.repositories.purchases.PurchaseOrderRepTestUtils;
import org.eclipse.persistence.jpa.jpql.Assert;

import java.math.BigDecimal;

public class DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils {

  private DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand command;
  private DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject valueObject;

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils() {
    valueObject = new DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject();
  }

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils withPaymentCode(
      String paymentCode) {
    PaymentUtils.withPaymentCode(paymentCode);
    valueObject.setPaymentRequestCode(paymentCode);
    return this;
  }

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils withAmount(
      BigDecimal paymentAmount) {
    PaymentUtils.withAmount(paymentAmount);
    valueObject.setAmountValue(paymentAmount);
    return this;
  }

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils withPurchaseOrderCode(
      String purchaseOrderCode) {
    PaymentUtils.withDueDocumentCode(purchaseOrderCode);
    DObPurchaseOrderMockUtils.withpoCode(purchaseOrderCode);
    return this;
  }

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils withPoRemaining(
      BigDecimal remaining) {
    DObPurchaseOrderMockUtils.withRemaining(remaining);
    return this;
  }

  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils whenExecute()
      throws Exception {
    prepareCommandDependencies();
    command.executeCommand(valueObject);
    return this;
  }

  private void prepareCommandDependencies() {
    command = new DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand();
    preparePurchaseOrder();
    preparePaymentRequest();
  }

  private void preparePaymentRequest() {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        PaymentUtils.buildPaymentRequestGeneralModel();
    PaymentRequestRepUtils.findPaymentRequestGeneralModelByPaymentCode(paymentRequestGeneralModel);
    command.setDObPaymentRequestGeneralModelRep(
        PaymentRequestRepUtils.getPaymentRequestGeneralModelRep());
  }

  private void preparePurchaseOrder() {
    DObPurchaseOrder purchaseOrder = DObPurchaseOrderMockUtils.buildPurchaseOrder();
    PurchaseOrderRepTestUtils.findOneByUserCode(purchaseOrder, "2019000108");
    command.setDObPurchaseOrderRep(PurchaseOrderRepTestUtils.getRepository());
  }

  public void assertThatRemainingIsUpdatedTo(BigDecimal expectedRemaining) {
    DObPurchaseOrder purchaseOrder =
        PurchaseOrderRepTestUtils.getRepository().findOneByUserCode("2019000108");
    BigDecimal remaining = purchaseOrder.getRemaining();
    Assert.isEqual(
        0,
        remaining.compareTo(expectedRemaining),
        "PurchaseOrder Remaining is not updated correctly");
  }
}
