package com.ebs.dda.commands.inventory.stockavailability.strategies;

import com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils.goodsReceiptItem;

@RunWith(MockitoJUnitRunner.class)
public class UpdateStockAvailabilityByGoodsReceiptStrategyTest {

  private UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils utils;

  @Before
  public void init() {
    utils = new UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils();
  }

  @Test
  public void Should_FetchItemsFromRepositoryAndCallUpdateServiceWithItemsList() {
    utils
        .withUserCode("2021000001")
        .withGoodsReceiptItems(
            goodsReceiptItem(
                "UNRESTRICTED_USE",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                BigDecimal.TEN),
            goodsReceiptItem(
                "DAMAGED_STOCK",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("5.0")))
        .whenUpdate()
        .thenAssertRepositoryWasCalledWithCode("2021000001")
        .thenAssertUpdateServiceWasCalledWithItemsList();
  }
}
