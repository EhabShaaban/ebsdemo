package com.ebs.dda.commands.accounting.salesinvoice.utils;

import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceAccountDeterminationCommand;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCustomerAccountingDetails;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils;
import com.ebs.repositories.accounting.salesInvoice.DObSalesInvoiceRepMockUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DObSalesInvoiceAccountDeterminationCommandTestUtils {

  private final DObJournalEntryCreateValueObject valueObject;
  private final List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> taxes;

  public DObSalesInvoiceAccountDeterminationCommandTestUtils() {
    valueObject = new DObJournalEntryCreateValueObject();
    taxes = new ArrayList<>();
    DObSalesInvoiceMockUtils.resetValue();
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withSalesInvoice(
      Long salesInvoiceId, String salesInvoiceCode) {
    valueObject.setUserCode(salesInvoiceCode);
    DObSalesInvoiceMockUtils.withSalesInvoiceId(salesInvoiceId);
    DObSalesInvoiceMockUtils.withSalesInvoiceCode(salesInvoiceCode);
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withSalesInvoiceCode(
        salesInvoiceCode);
    IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.withSalesInvoiceCode(
        salesInvoiceCode);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withCustomerAccountId(
      Long customerAccountId) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withCustomerAccountId(
        customerAccountId);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withCustomerId(Long customerId) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withCustomerId(customerId);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withSalesAccountId(
      Long salesAccountId) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withSalesAccountId(salesAccountId);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withAmountBeforeTaxes(
      BigDecimal amountBeforeTaxes) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withAmountBeforeTaxes(
        amountBeforeTaxes);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withAmountAfterTaxes(
      BigDecimal amountAfterTaxes) {
    DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.withAmountAfterTaxes(
        amountAfterTaxes);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils withTax(
      Long taxesAccountId, Long taxId, BigDecimal taxAmount) {
    IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.withTaxesAccountId(
        taxesAccountId);
    IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.withTaxId(taxId);
    IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.withTaxAmount(taxAmount);
    taxes.add(IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.buildEntity());
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils whenExecute() throws Exception {
    DObSalesInvoiceAccountDeterminationCommand command =
        DObSalesInvoiceAccountDeterminationCommandTestFactory.createCommandInstance(
            valueObject.getUserCode(), taxes);
    command.executeCommand(valueObject);
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils saveMethodIsCalledSuccessfully()
      throws Exception {
    Mockito.verify(DObSalesInvoiceRepMockUtils.getDObSalesInvoiceRep(), Mockito.times(1))
        .update(DObSalesInvoiceMockUtils.getSalesInvoice());
    return this;
  }

  public DObSalesInvoiceAccountDeterminationCommandTestUtils numberOfAccountsDeterminedIs(int accountsNum) {
    ArgumentCaptor<IObAccountingDocumentAccountingDetails> argumentCaptor =
        ArgumentCaptor.forClass(IObAccountingDocumentAccountingDetails.class);
    Mockito.verify(DObSalesInvoiceMockUtils.getSalesInvoice(), Mockito.times(accountsNum))
        .addLineDetail(ArgumentMatchers.any(), argumentCaptor.capture());
    return this;
  }
  public DObSalesInvoiceAccountDeterminationCommandTestUtils assertThatCustomerAccountingDetailsIsRecorded(
      Long glAccountId, BigDecimal value, String accountingEntry, int accountsNum) {

    ArgumentCaptor<IObAccountingDocumentCustomerAccountingDetails> argumentCaptor =
        ArgumentCaptor.forClass(IObAccountingDocumentCustomerAccountingDetails.class);
    Mockito.verify(DObSalesInvoiceMockUtils.getSalesInvoice(), Mockito.times(accountsNum))
        .addLineDetail(Mockito.eq(IDObAccountingDocument.accountingCustomerDetailsAttr), argumentCaptor.capture());
    List<IObAccountingDocumentCustomerAccountingDetails> accountingDetailsList =
        argumentCaptor.getAllValues();
    IObAccountingDocumentAccountingDetails accountingDetails =
        accountingDetailsList.stream()
            .filter(
                item ->
                    glAccountId.equals(item.getGlAccountId())
                        && ((accountingEntry.equals(AccountingEntry.CREDIT.name())
                                && value.compareTo(item.getAmount()) == 0)
                            || (accountingEntry.equals(AccountingEntry.DEBIT.name())
                                && value.compareTo(item.getAmount()) == 0)))
            .findAny()
            .orElse(null);
    Assert.assertNotNull(accountingDetails);
    return this;
  }
  public DObSalesInvoiceAccountDeterminationCommandTestUtils assertThatAccountingDetailsIsRecorded(
      Long glAccountId, BigDecimal value, String accountingEntry, int accountsNum) {

    ArgumentCaptor<IObAccountingDocumentAccountingDetails> argumentCaptor =
        ArgumentCaptor.forClass(IObAccountingDocumentAccountingDetails.class);
    Mockito.verify(DObSalesInvoiceMockUtils.getSalesInvoice(), Mockito.times(accountsNum))
        .addLineDetail(Mockito.eq(IDObAccountingDocument.accountingDetailsAttr), argumentCaptor.capture());
    List<IObAccountingDocumentAccountingDetails> accountingDetailsList =
        argumentCaptor.getAllValues();
    IObAccountingDocumentAccountingDetails accountingDetails =
        accountingDetailsList.stream()
            .filter(
                item ->
                    glAccountId.equals(item.getGlAccountId())
                        && ((accountingEntry.equals(AccountingEntry.CREDIT.name())
                                && value.compareTo(item.getAmount()) == 0)
                            || (accountingEntry.equals(AccountingEntry.DEBIT.name())
                                && value.compareTo(item.getAmount()) == 0)))
            .findAny()
            .orElse(null);
    Assert.assertNotNull(accountingDetails);
    return this;
  }
}
