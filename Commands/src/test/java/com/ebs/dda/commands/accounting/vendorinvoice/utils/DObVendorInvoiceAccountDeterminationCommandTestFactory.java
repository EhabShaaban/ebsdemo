package com.ebs.dda.commands.accounting.vendorinvoice.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.vendorinvoice.DObImportGoodsInvoiceAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.DObLocalGoodsInvoiceAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.DObPurchaseServiceLocalInvoiceAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceAccountDeterminationCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.entities.accounting.VendorInvoiceUtils;
import com.ebs.entities.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils;
import com.ebs.entities.order.purchaseorder.IObPurchaseOrderFulFillerVendorMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceRepMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRepUtils;
import com.ebs.repositories.order.purchaseorder.IObPurchaseOrderFulFillerVendorRepMockUtils;

import java.util.List;

public class DObVendorInvoiceAccountDeterminationCommandTestFactory {
    private static DObVendorInvoiceAccountDeterminationCommand command;

    public static DObVendorInvoiceAccountDeterminationCommand createCommandInstance(String vendorInvoiceType, String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> items, List<IObVendorInvoiceTaxesGeneralModel> taxes) {
        switch (vendorInvoiceType) {
            case VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE: {
                command = new DObImportGoodsInvoiceAccountDeterminationCommand();
                prepareGeneralCommandDependencies(vendorInvoiceCode, items);
                return command;
            }
            case VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE: {
                command = new DObLocalGoodsInvoiceAccountDeterminationCommand();
                prepareGeneralCommandDependencies(vendorInvoiceCode, items);
                prepareLocalGoodsCommandDependencies((DObLocalGoodsInvoiceAccountDeterminationCommand) command, vendorInvoiceCode, taxes);
                return command;
            }
            case VendorInvoiceTypeEnum.Values.SHIPMENT_INVOICE:
            case VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE:
            case VendorInvoiceTypeEnum.Values.INSURANCE_INVOICE:
            {
                command = new DObPurchaseServiceLocalInvoiceAccountDeterminationCommand();
                prepareGeneralCommandDependencies(vendorInvoiceCode, items);
                preparePurchaseServiceLocalCommandDependencies((DObPurchaseServiceLocalInvoiceAccountDeterminationCommand) command, vendorInvoiceCode, taxes);
                return command;
            }
        }
        throw new IllegalArgumentException("Invalid VendorInvoice Type");
    }

    private static void preparePurchaseServiceLocalCommandDependencies(DObPurchaseServiceLocalInvoiceAccountDeterminationCommand command, String vendorInvoiceCode, List<IObVendorInvoiceTaxesGeneralModel> taxes) {
        IObVendorInvoiceTaxesGeneralModelRepUtils.findAllByVendorInvoiceCode(taxes, vendorInvoiceCode);
        command.setTaxesRep(IObVendorInvoiceTaxesGeneralModelRepUtils.getRepository());

        IObPurchaseOrderFulFillerVendorRepMockUtils.findOneByRefInstanceId(IObPurchaseOrderFulFillerVendorMockUtils.buildEntity());
        command.setPurchaseOrderFulFillerVendorRep(IObPurchaseOrderFulFillerVendorRepMockUtils.getRepository());
    }

    private static void prepareLocalGoodsCommandDependencies(DObLocalGoodsInvoiceAccountDeterminationCommand command, String vendorInvoiceCode, List<IObVendorInvoiceTaxesGeneralModel> taxes) {
        IObVendorInvoiceTaxesGeneralModelRepUtils.findAllByVendorInvoiceCode(taxes, vendorInvoiceCode);
        command.setTaxesRep(IObVendorInvoiceTaxesGeneralModelRepUtils.getRepository());
    }

    private static void prepareGeneralCommandDependencies(String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> items) {
        prepareCreditAccount();
        prepareUserAccountSecurityService();
        prepareVendorInvoice();
        prepareVendorInvoiceItems(vendorInvoiceCode, items);
    }

    private static void prepareVendorInvoiceItems(String vendorInvoiceCode, List<DObVendorInvoiceItemAccountsPreparationGeneralModel> items) {
        DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils.findAllByVendorInvoiceCode(items, vendorInvoiceCode);
        command.setItemAccountsPreparationGeneralModelRep(DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils.getRepository());
    }

    private static void prepareUserAccountSecurityService() {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }

    private static void prepareVendorInvoice() {
        DObVendorInvoice vendorInvoice = VendorInvoiceUtils.buildVendorInvoice();
        DObVendorInvoiceRepMockUtils.findOneByVendorInvoiceCode(vendorInvoice);
        command.setVendorInvoiceRep(DObVendorInvoiceRepMockUtils.getRepository());
    }

    private static void prepareCreditAccount() {
        DObVendorInvoiceJournalDetailPreparationGeneralModel creditPreparationGeneralModel = DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.buildEntity();
        DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils.findOneByVendorInvoiceCode(creditPreparationGeneralModel);
        command.setJournalDetailPreparationGeneralModelٌRep(DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils.getRepository());
    }
}
