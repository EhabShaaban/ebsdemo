package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.order.purchaseorder.mocks.entities.EntityLockCommandMockUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderApproveValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObLocalPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderNewStateMachineFactory;
import com.ebs.entities.PlatformTransactionManagerMockUtils;
import com.ebs.entities.order.purchaseorder.DObImportPurchaseOrderMockUtils;
import com.ebs.entities.order.purchaseorder.DObLocalPurchaseOrderMockUtils;
import com.ebs.entities.order.purchaseorder.IObOrderApprovalCycleMockUtils;
import com.ebs.entities.order.purchaseorder.IObOrderApproverMockUtils;
import com.ebs.entities.order.purchaseorder.IObOrderLineDetailsSummaryGeneralModelMockUtils;
import com.ebs.repositories.order.purchaseorder.DObPurchaseOrderRepMockUtils;
import com.ebs.repositories.order.purchaseorder.IObOrderApprovalCycleRepMockUtils;
import com.ebs.repositories.order.purchaseorder.IObOrderApproverRepMockUtils;
import com.ebs.repositories.order.purchaseorder.IObOrderLineDetailsSummaryGeneralModelRepMockUtils;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DObImportLocalPurchaseOrderApproveCommandTest {

  @Mock private DObImportPurchaseOrderStateMachine importPurchaseOrderStateMachine;
  @Mock private DObLocalPurchaseOrderStateMachine localPurchaseOrderStateMachine;
  @Mock private DObPurchaseOrderNewStateMachineFactory mockedFactory;

  private DObPurchaseOrderApproveCommand approveCommand;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {

    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");

    approveCommand = new DObPurchaseOrderApproveCommand();
    approveCommand.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    approveCommand.setPurchaseOrderRep(DObPurchaseOrderRepMockUtils.getRepository());
    approveCommand.setApprovalCycleRep(IObOrderApprovalCycleRepMockUtils.getRepository());
    approveCommand.setEntityLockCommand(EntityLockCommandMockUtils.newEntity().build());
    approveCommand.setApproverRep(IObOrderApproverRepMockUtils.getRepository());
    approveCommand.setTransactionManager(PlatformTransactionManagerMockUtils.mockEntity().build());
    approveCommand.setOrderLineDetailsSummaryGeneralModelRep(
        IObOrderLineDetailsSummaryGeneralModelRepMockUtils.getRepository());

    approveCommand.setPurchaseOrderFactory(mockedFactory);
  }

  @Test
  public void shouldSetRemainingForImportPurchaseOrderAfterApprovedState() throws Exception {
    //    Arrange
    String purchaseOrderCode = "2018000015";
    String totalAmountAfterTaxes = "14025.123456";

    DObPurchaseOrderApproveValueObject valueObject = new DObPurchaseOrderApproveValueObject();
    valueObject.setPurchaseOrderCode(purchaseOrderCode);

    DObPurchaseOrder purchaseOrder =
        DObImportPurchaseOrderMockUtils.mockEntity().id(1L).userCode(purchaseOrderCode).build();
    DObPurchaseOrderRepMockUtils.findOneByUserCode(purchaseOrder);

    IObOrderApprovalCycle orderApprovalCycle =
        IObOrderApprovalCycleMockUtils.mockEntity().id(1L).build();
    IObOrderApprovalCycleRepMockUtils.findByRefInstanceIdAndFinalDecisionIsNull(orderApprovalCycle);

    IObOrderApprover orderApprover = IObOrderApproverMockUtils.mockEntity().build();
    IObOrderApproverRepMockUtils.findByRefInstanceIdOrderByIdAsc(Arrays.asList(orderApprover));

    Mockito.doReturn(importPurchaseOrderStateMachine)
        .when(mockedFactory)
        .createPurchaseOrderStateMachine(Mockito.any(DObPurchaseOrder.class));

    IObOrderLineDetailsSummaryGeneralModel summary =
        IObOrderLineDetailsSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(purchaseOrderCode)
            .totalAmountAfterDiscount(new BigDecimal(totalAmountAfterTaxes))
            .build();
    IObOrderLineDetailsSummaryGeneralModelRepMockUtils.findOneByUserCode(summary);

    //    Act
    approveCommand.executeCommand(valueObject);

    //    Assert
    Mockito.verify(purchaseOrder, Mockito.times(1))
        .setRemaining(new BigDecimal(totalAmountAfterTaxes));
  }

  @Test
  public void shouldSetRemainingForLocalPurchaseOrderAfterApprovedState() throws Exception {
    //    Arrange
    String purchaseOrderCode = "2018000015";
    String totalAmountAfterTaxes = "14025.123456";

    DObPurchaseOrderApproveValueObject valueObject = new DObPurchaseOrderApproveValueObject();
    valueObject.setPurchaseOrderCode(purchaseOrderCode);

    DObPurchaseOrder purchaseOrder =
        DObLocalPurchaseOrderMockUtils.mockEntity().id(1L).userCode(purchaseOrderCode).build();
    DObPurchaseOrderRepMockUtils.findOneByUserCode(purchaseOrder);

    IObOrderApprovalCycle orderApprovalCycle =
        IObOrderApprovalCycleMockUtils.mockEntity().id(1L).build();
    IObOrderApprovalCycleRepMockUtils.findByRefInstanceIdAndFinalDecisionIsNull(orderApprovalCycle);

    IObOrderApprover orderApprover = IObOrderApproverMockUtils.mockEntity().build();
    IObOrderApproverRepMockUtils.findByRefInstanceIdOrderByIdAsc(Arrays.asList(orderApprover));

    Mockito.doReturn(localPurchaseOrderStateMachine)
        .when(mockedFactory)
        .createPurchaseOrderStateMachine(Mockito.any(DObPurchaseOrder.class));

    IObOrderLineDetailsSummaryGeneralModel summary =
        IObOrderLineDetailsSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(purchaseOrderCode)
            .totalAmountAfterDiscount(new BigDecimal(totalAmountAfterTaxes))
            .build();
    IObOrderLineDetailsSummaryGeneralModelRepMockUtils.findOneByUserCode(summary);

    //    Act
    approveCommand.executeCommand(valueObject);

    //    Assert
    Mockito.verify(purchaseOrder, Mockito.times(1))
        .setRemaining(new BigDecimal(totalAmountAfterTaxes));
  }
}
