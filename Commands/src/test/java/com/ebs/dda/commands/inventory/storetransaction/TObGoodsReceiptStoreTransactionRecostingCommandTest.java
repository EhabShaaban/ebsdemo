package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.entities.GoodsReceiptMockUtils;
import com.ebs.entities.LandedCostGeneralModelMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionMockUtils;
import com.ebs.repositories.GoodsReceiptGeneralModelRepUtils;
import com.ebs.repositories.LandedCostGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.storetransaction.GoodsReceiptStoreTransactionRepUtils;
import com.ebs.repositories.inventory.storetransaction.StoreTransactionRepUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
class TObGoodsReceiptStoreTransactionRecostingCommandTest {
  private TObGoodsReceiptStoreTransactionRecostingCommand command;

  private GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
      goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
  private CommandTestUtils commandTestUtils;

  @BeforeEach
  void setUp() {
    goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService =
        Mockito.mock(GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService.class);
    commandTestUtils = new CommandTestUtils();
    command = new TObGoodsReceiptStoreTransactionRecostingCommand();
    command.setLandedCostGeneralModelRep(LandedCostGeneralModelRepMockUtils.getRepository());
    command.setGoodsReceiptGeneralModelRep(GoodsReceiptGeneralModelRepUtils.getRepository());
    command.setGoodsReceiptStoreTransactionRep(
        GoodsReceiptStoreTransactionRepUtils.getRepository());
    command.setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(
        goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService);
    command.setStoreTransactionRep(StoreTransactionRepUtils.getRepository());
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    commandTestUtils.mockUserAccount("ADMIN");
  }

  @AfterEach
  void tearDown() {
    LandedCostGeneralModelRepMockUtils.resetRepository();
    GoodsReceiptGeneralModelRepUtils.resetRepository();
    GoodsReceiptStoreTransactionRepUtils.resetRepository();
    StoreTransactionRepUtils.resetRepository();
  }

  @Test
  void should_AddActualCostsForItemsBasedOnCostingItemCosts() throws Exception {
    // Prepare
    String landedCostUserCode = "20210000004";
    String purchaseOrderCode = "20210000001";
    String goodsReceiptUserCode = "20210000005";
    long goodsReceiptId = 5L;

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostUserCode)
            .purchaseOrderCode(purchaseOrderCode)
            .build();
    LandedCostGeneralModelRepMockUtils.findOneByUserCode(landedCostGeneralModel);

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .refDocumentCode(purchaseOrderCode)
            .inventoryDocumentType("GR_PO")
            .userCode(goodsReceiptUserCode)
            .id(goodsReceiptId)
            .build();
    GoodsReceiptGeneralModelRepUtils
        .findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
            goodsReceiptGeneralModel, ("%" + DObGoodsReceiptStateMachine.ACTIVE_STATE + "%"));

    List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactionList =
        GoodsReceiptStoreTransactionMockUtils.mockEntityList()
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(1L)
                    .id(1L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(2L)
                    .id(2L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(3L)
                    .id(3L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .id(4L)
                    .itemId(4L)
                    .build())
            .build();
    GoodsReceiptStoreTransactionRepUtils.findByDocumentReferenceId(
        goodsReceiptStoreTransactionList);

    List<Long> listOfChildrenIds = new ArrayList<>();

    goodsReceiptStoreTransactionList.stream()
        .forEach(
            tObGoodsReceiptStoreTransaction -> {
              List<Long> returnedList = new ArrayList<>();
              returnedList.add(tObGoodsReceiptStoreTransaction.getId() + 4);
              listOfChildrenIds.addAll(returnedList);
              StoreTransactionRepUtils.findAllChildrenByRefInstanceIdAndAddTransactionOperation(
                  tObGoodsReceiptStoreTransaction.getId(), returnedList);
            });

    List<TObGoodsReceiptStoreTransaction> addedTransactionList =
        GoodsReceiptStoreTransactionMockUtils.mockEntityList()
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(1L)
                    .id(5L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(2L)
                    .id(6L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(3L)
                    .id(7L)
                    .build())
            .goodsReceiptStoreTransaction(
                GoodsReceiptStoreTransactionMockUtils.mockEntity()
                    .userCode(goodsReceiptUserCode)
                    .documentReferenceId(goodsReceiptId)
                    .itemId(3L)
                    .id(8L)
                    .build())
            .build();

    GoodsReceiptStoreTransactionRepUtils.findAllById(addedTransactionList, listOfChildrenIds);

    // Execute
    command.executeCommand(landedCostUserCode);

    // Assert
    Mockito.verify(LandedCostGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(landedCostUserCode);
    Mockito.verify(GoodsReceiptGeneralModelRepUtils.getRepository(), Mockito.times(1))
        .findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
            purchaseOrderCode, "GR_PO", ("%" + DObGoodsReceiptStateMachine.ACTIVE_STATE + "%"));
    Mockito.verify(GoodsReceiptStoreTransactionRepUtils.getRepository(), Mockito.times(1))
        .findByDocumentReferenceId(goodsReceiptId);
    Mockito.verify(
            StoreTransactionRepUtils.getRepository(),
            Mockito.times(goodsReceiptStoreTransactionList.size()))
        .findAllChildrenByRefInstanceIdAndAddTransactionOperation(Mockito.anyLong());

    ArgumentCaptor<List<TObGoodsReceiptStoreTransaction>> storeTransactionsCaptor =
        ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<String> goodsReceiptCodeCaptor = ArgumentCaptor.forClass(String.class);
    Mockito.verify(goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService, Mockito.times(1))
        .setGoodsReceiptPurchaseOrderStoreTransactionCosts(
            goodsReceiptCodeCaptor.capture(), storeTransactionsCaptor.capture());
    List<TObGoodsReceiptStoreTransaction> capturedList = storeTransactionsCaptor.getValue();
    Assertions.assertEquals(capturedList.size(), 8);
    Assertions.assertEquals(goodsReceiptCodeCaptor.getValue(), goodsReceiptUserCode);
    Mockito.verify(commandTestUtils.getUserAccountSecurityService(), Mockito.times(8))
        .getLoggedInUser();
    Mockito.verify(GoodsReceiptStoreTransactionRepUtils.getRepository(), Mockito.times(1))
        .saveAll(storeTransactionsCaptor.capture());
    capturedList = storeTransactionsCaptor.getValue();
    Assertions.assertEquals(capturedList.size(), 8);
  }
}
