package com.ebs.dda.commands.inventory.stockavailability.mocks.entities;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModel;

import java.math.BigDecimal;

public class IObGoodsIssueItemQuantityGeneralModelMock
    extends IObGoodsIssueItemQuantityGeneralModel {
  private String userCode;
  private String itemCode;
  private String unitOfMeasureCode;
  private String purchaseUnitCode;
  private String companyCode;
  private String storeHouseCode;
  private String plantCode;

  public IObGoodsIssueItemQuantityGeneralModelMock(
      String userCode,
      String itemCode,
      String plantCode,
      String storehouseCode,
      String companyCode,
      String purchaseUnitCode,
      String unitOfMeasureCode,
      BigDecimal quantity) {
    this.userCode = userCode;
    this.itemCode = itemCode;
    this.plantCode = plantCode;
    this.storeHouseCode = storehouseCode;
    this.companyCode = companyCode;
    this.purchaseUnitCode = purchaseUnitCode;
    this.unitOfMeasureCode = unitOfMeasureCode;
    setQuantity(quantity);
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getItemCode() {
    return itemCode;
  }

  @Override
  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  @Override
  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  @Override
  public String getCompanyCode() {
    return companyCode;
  }

  @Override
  public String getStoreHouseCode() {
    return storeHouseCode;
  }

  @Override
  public String getPlantCode() {
    return plantCode;
  }
}
