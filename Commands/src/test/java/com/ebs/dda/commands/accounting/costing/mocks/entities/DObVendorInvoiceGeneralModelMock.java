package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;

public class DObVendorInvoiceGeneralModelMock extends DObVendorInvoiceGeneralModel {
  private String userCode;
  private String purchaseOrderCode;
  private String currencyISO;

  public DObVendorInvoiceGeneralModelMock(String userCode, String purchaseOrderCode) {
    this.userCode = userCode;
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public DObVendorInvoiceGeneralModelMock(
      String userCode, String purchaseOrderCode, String currencyISO) {
    this.userCode = userCode;
    this.purchaseOrderCode = purchaseOrderCode;
    this.currencyISO = currencyISO;
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  @Override
  public String getCurrencyISO() {
    return currencyISO;
  }
}
