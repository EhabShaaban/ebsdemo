package com.ebs.dda.commands.inventory.stockavailability.utils;

import com.ebs.dda.commands.inventory.stockavailability.CObStockAvailabilityUpdateCommand;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsIssueStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsReceiptStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByInitialStockUploadStrategy;
import com.ebs.dda.commands.inventory.stockavailability.factory.UpdateStockAvailabilityStrategyFactory;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import org.mockito.Mockito;

public class CObStockAvailabilityUpdateCommandTestUtils {

  private CObStockAvailabilityUpdateCommand command;
  private UpdateStockAvailabilityByGoodsReceiptStrategy byGoodsReceiptStrategy;
  private UpdateStockAvailabilityByGoodsIssueStrategy byGoodsIssueStrategy;
  private UpdateStockAvailabilityByInitialStockUploadStrategy byInitialStockUploadStrategy;
  private UpdateStockAvailabilityStrategyFactory factory;
  private DObInventoryDocumentActivateValueObject activateValueObject;

  public CObStockAvailabilityUpdateCommandTestUtils() {
    mockDependencies();
    initCommand();
  }

  private void initCommand() {
    command = new CObStockAvailabilityUpdateCommand();
    command.setStockAvailabilityUpdateFactory(factory);
  }

  private void mockDependencies() {
    byGoodsIssueStrategy = Mockito.mock(UpdateStockAvailabilityByGoodsIssueStrategy.class);
    byGoodsReceiptStrategy = Mockito.mock(UpdateStockAvailabilityByGoodsReceiptStrategy.class);
    byInitialStockUploadStrategy =
        Mockito.mock(UpdateStockAvailabilityByInitialStockUploadStrategy.class);
    factory = new UpdateStockAvailabilityStrategyFactory();
    factory.setByGoodsIssueStrategy(byGoodsIssueStrategy);
    factory.setByGoodsReceiptStrategy(byGoodsReceiptStrategy);
    factory.setByInitialStockUploadStrategy(byInitialStockUploadStrategy);
  }

  public CObStockAvailabilityUpdateCommandTestUtils whenActivate() {
    command.executeCommand(activateValueObject);
    return this;
  }

  public void thenAssertGoodsReceiptStrategyWasCalledWithUserCode(String userCode) {
    Mockito.verify(byGoodsReceiptStrategy, Mockito.times(1))
        .updateStockAvailability(userCode);
  }

  public void thenAssertGoodsIssueStrategyWasCalledWithUserCode(String userCode) {
    Mockito.verify(byGoodsIssueStrategy, Mockito.times(1))
        .updateStockAvailability(userCode);
  }

  public void thenAssertInitialStockUploadStrategyWasCalledWithUserCode(String userCode) {
    Mockito.verify(byInitialStockUploadStrategy, Mockito.times(1))
        .updateStockAvailability(userCode);
  }

  public CObStockAvailabilityUpdateCommandTestUtils withInventoryDocumentActivateValueObject(
      String inventoryType, String userCode) {
    activateValueObject = new DObInventoryDocumentActivateValueObject();
    activateValueObject.setInventoryType(inventoryType);
    activateValueObject.setUserCode(userCode);
    return this;
  }

}
