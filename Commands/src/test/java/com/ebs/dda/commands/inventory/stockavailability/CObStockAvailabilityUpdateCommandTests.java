package com.ebs.dda.commands.inventory.stockavailability;

import com.ebs.dda.commands.inventory.stockavailability.utils.CObStockAvailabilityUpdateCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CObStockAvailabilityUpdateCommandTests {
  private CObStockAvailabilityUpdateCommandTestUtils utils;

  @Before
  public void init() {
    utils = new CObStockAvailabilityUpdateCommandTestUtils();
  }

  @Test
  public void Should_UseGoodsReceiptStrategy() {
    utils
        .withInventoryDocumentActivateValueObject("GR_PO", "2021000001")
        .whenActivate()
        .thenAssertGoodsReceiptStrategyWasCalledWithUserCode("2021000001");
  }

  @Test
  public void Should_UseGoodsIssueStrategy() {
    utils
        .withInventoryDocumentActivateValueObject("GI_SI", "2021000001")
        .whenActivate()
        .thenAssertGoodsIssueStrategyWasCalledWithUserCode("2021000001");
  }

  @Test
  public void Should_UseInitialUploadStrategy() {
    utils
        .withInventoryDocumentActivateValueObject("ISU", "2021000001")
        .whenActivate()
        .thenAssertInitialStockUploadStrategyWasCalledWithUserCode("2021000001");
  }
}
