package com.ebs.dda.commands.inventory.stockavailability.strategies.utils;

import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.commands.inventory.stockavailability.mocks.entities.IObInitialStockUploadItemQuantityGeneralModelMock;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByInitialStockUploadStrategy;
import com.ebs.dda.jpa.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModelRep;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils {

  private String userCode;
  private IObInitialStockUploadItemQuantityGeneralModelRep
      initialStockUploadItemQuantityGeneralModelRep;
  private UpdateStockAvailabilityService updateService;
  private UpdateStockAvailabilityByInitialStockUploadStrategy initialStockUploadStrategy;
  private List<IObInitialStockUploadItemQuantityGeneralModel> initialStockUploadItems;

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils() {
    mockDependencies();
    initStrategy();
  }

  private static Predicate<IObInitialStockUploadItemQuantityGeneralModel>
      initialStockUploadItemItemQuantitiesEqual(
          IObInitialStockUploadItemQuantityGeneralModel item) {
    return p ->
        p.getUserCode().equals(item.getUserCode())
            && p.getStockType().equals(item.getStockType())
            && p.getCompanyCode().equals(item.getCompanyCode())
            && p.getPlantCode().equals(item.getPlantCode())
            && p.getItemCode().equals(item.getItemCode())
            && p.getPurchaseUnitCode().equals(item.getPurchaseUnitCode())
            && p.getUnitOfMeasureCode().equals(item.getUnitOfMeasureCode())
            && p.getQuantity().equals(item.getQuantity())
            && p.getStoreHouseCode().equals(item.getStoreHouseCode());
  }

  public static IObInitialStockUploadItemQuantityGeneralModelMock initialStockUploadItem(
      String stockType,
      String purchaseUnitCode,
      String companyCode,
      String plantCode,
      String storehouseCode,
      String itemCode,
      String unitOfMeasureCode,
      String userCode,
      BigDecimal quantity) {
    return new IObInitialStockUploadItemQuantityGeneralModelMock(
        userCode,
        itemCode,
        plantCode,
        storehouseCode,
        companyCode,
        purchaseUnitCode,
        unitOfMeasureCode,
        quantity,
        stockType);
  }

  private void initStrategy() {
    initialStockUploadStrategy = new UpdateStockAvailabilityByInitialStockUploadStrategy();
    initialStockUploadStrategy.setInitialStockUploadItemQuantityGeneralModelRep(
        initialStockUploadItemQuantityGeneralModelRep);
    initialStockUploadStrategy.setStockAvailabilityUpdateService(updateService);
  }

  private void mockDependencies() {
    initialStockUploadItemQuantityGeneralModelRep =
        Mockito.mock(IObInitialStockUploadItemQuantityGeneralModelRep.class);
    updateService = Mockito.mock(UpdateStockAvailabilityService.class);
  }

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils withUserCode(
      String userCode) {
    this.userCode = userCode;
    return this;
  }

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils whenUpdate() {
    mockData();
    initialStockUploadStrategy.updateStockAvailability(userCode);
    return this;
  }

  private void mockData() {
    Mockito.when(initialStockUploadItemQuantityGeneralModelRep.findAllByUserCode(userCode))
        .thenReturn(initialStockUploadItems);
  }

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils
      thenAssertRepositoryWasCalledWithCode(String userCode) {
    Mockito.verify(initialStockUploadItemQuantityGeneralModelRep, Mockito.times(1))
        .findAllByUserCode(userCode);
    return this;
  }

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils
      thenAssertUpdateServiceWasCalledWithItemsList() {
    ArgumentCaptor<IObInitialStockUploadItemQuantityGeneralModel> updateResult =
        ArgumentCaptor.forClass(IObInitialStockUploadItemQuantityGeneralModel.class);
    Mockito.verify(updateService, Mockito.times(initialStockUploadItems.size()))
        .updateOrCreateStockAvailabilityForItem(
            updateResult.capture(), Mockito.any());
    assertInitialStockUploadItemsAreEqual(initialStockUploadItems, updateResult.getAllValues());
    return this;
  }

  private void assertInitialStockUploadItemsAreEqual(
      List<IObInitialStockUploadItemQuantityGeneralModel> actual,
      List<IObInitialStockUploadItemQuantityGeneralModel> expected) {
    Assert.assertEquals("Number of updated items doesn't match", expected.size(), actual.size());
    expected.stream()
        .forEach(
            item -> {
              Assert.assertNotNull(
                  "Item doesn't exist",
                  actual.stream()
                      .filter(initialStockUploadItemItemQuantitiesEqual(item))
                      .findAny());
            });
  }

  public UpdateStockAvailabilityByInitialStockUploadStrategyTestUtils withInitialStockUploadItems(
      IObInitialStockUploadItemQuantityGeneralModelMock... items) {
    initialStockUploadItems = new ArrayList<>(Arrays.asList(items));
    return this;
  }
}
