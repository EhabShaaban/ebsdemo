package com.ebs.dda.commands.accounting.costing.utils;

import com.ebs.dda.commands.accounting.costing.mocks.entities.DObPaymentRequestCurrencyPriceGeneralModelMock;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.entities.CompanyMockUtils;
import com.ebs.entities.ExchangeRateMockUtils;
import com.ebs.entities.GoodsReceiptMockUtils;
import com.ebs.entities.VendorInvoiceSummaryUtils;
import com.ebs.entities.VendorInvoiceUtils;
import com.ebs.repositories.CompanyRepMockUtils;
import com.ebs.repositories.ExchangeRateRepMockUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.mockito.Mockito;

public class ExchangeRateStrategyTestUtils {

  public void prepareEmptyListForVendorInvoicePayments(
      DObPaymentRequestCurrencyPriceGeneralModelRep paymentRequestRep,
      DObVendorInvoiceGeneralModel vendorInvoice) {
    Mockito.when(
            paymentRequestRep.findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
                vendorInvoice.getUserCode(),
                "INVOICE",
                PaymentType.OTHER_PARTY_FOR_PURCHASE.name()))
        .thenReturn(new ArrayList<>());
  }

  public void preparePayment(
      DObPaymentRequestCurrencyPriceGeneralModelRep paymentRequestRep,
      String userCode,
      String dueDocumentCode,
      String dueDocumentType,
      BigDecimal netAmount,
      BigDecimal currencyPrice) {

    List<DObPaymentRequestCurrencyPriceGeneralModel> payments =
        Collections.singletonList(
            new DObPaymentRequestCurrencyPriceGeneralModelMock(
                userCode, dueDocumentCode, dueDocumentType, netAmount, currencyPrice));

    Mockito.when(
            paymentRequestRep.findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
                dueDocumentCode, dueDocumentType, PaymentType.OTHER_PARTY_FOR_PURCHASE.name()))
        .thenReturn(payments);
  }

  public DObVendorInvoiceGeneralModel prepareVendorInvoice(
      DObVendorInvoiceGeneralModelRep vendorInvoiceRep, String purchaseOrderCode) {
    DObVendorInvoiceGeneralModel vendorInvoice =
        VendorInvoiceUtils.newGeneralModel()
            .userCode("2021000001")
            .purchaseOrderCode(purchaseOrderCode)
            .currencyIso("USD")
            .build();
    Mockito.when(vendorInvoiceRep.findOneByPurchaseOrderCode(purchaseOrderCode))
        .thenReturn(vendorInvoice);
    return vendorInvoice;
  }

  public CObCompanyGeneralModel prepareCompanyGeneralModel(String companyCode) {
    CObCompanyGeneralModel cObCompanyGeneralModel =
        CompanyMockUtils.mockGeneralModel().currencyIso("EGP").userCode(companyCode).build();
    CompanyRepMockUtils.findOneByUserCode(cObCompanyGeneralModel);
    return cObCompanyGeneralModel;
  }

  public void prepareExchangeRateGeneralModel(
      String vendorInvoiceCurrencyISO, String companyCurrencyISO, BigDecimal value) {
    CObExchangeRateGeneralModel exchangeRateGeneralModel =
        ExchangeRateMockUtils.mockGeneralModel().secondValue(value).build();
    ExchangeRateRepMockUtils.findLatestDefaultExchangeRate(
        exchangeRateGeneralModel, vendorInvoiceCurrencyISO, companyCurrencyISO);
  }

  public IObVendorInvoiceSummary prepareVendorInvoiceSummary(String total, String remaining) {
    return VendorInvoiceSummaryUtils.newEntity()
        .total(new BigDecimal(total))
        .remaining(new BigDecimal(remaining))
        .build();
  }

  public DObGoodsReceiptGeneralModel prepareGoodsReceipt() {
    return GoodsReceiptMockUtils.mockGeneralModel()
        .userCode("2021000001")
        .refDocumentCode("2021000001")
        .build();
  }

  public void thenAssertPaymentCurrencyPriceGeneralModelRepIsCalled(
      DObPaymentRequestCurrencyPriceGeneralModelRep paymentRequestCurrencyPriceRep,
      String referenceCode,
      String dueDocumentType,
      String paymentType) {
    Mockito.verify(paymentRequestCurrencyPriceRep, Mockito.times(1))
        .findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
            referenceCode, dueDocumentType, paymentType);
  }
}
