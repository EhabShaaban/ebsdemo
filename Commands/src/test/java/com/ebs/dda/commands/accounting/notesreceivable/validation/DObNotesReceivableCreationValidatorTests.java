package com.ebs.dda.commands.accounting.notesreceivable.validation;

import com.ebs.dda.commands.accounting.notesreceivable.validation.utils.DObNotesReceivableCreationValidatorTestUtils;
import com.ebs.dda.jpa.accounting.monetarynotes.MonetaryNoteFormEnum;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class DObNotesReceivableCreationValidatorTests {

  private DObNotesReceivableCreationValidatorTestUtils utils;

  public DObNotesReceivableCreationValidatorTests() {
    utils = new DObNotesReceivableCreationValidatorTestUtils();
  }

  @Test
  public void Should_Pass_WhenValidData()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("0002")
      .companyCode("0002")
      .businessPartnerCode("000007")
      .documentOwnerId(1L)
      .amount(BigDecimal.valueOf(1000))
      .currency("EGP")
      .validateSuccessfully();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenNonExistBusinessUnit()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("9999")
      .companyCode("0002")
      .businessPartnerCode("000007")
      .documentOwnerId(1L)
      .amount(BigDecimal.valueOf(1000))
      .currency("EGP")
      .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenNonExistCompany()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("0002")
      .companyCode("9999")
      .businessPartnerCode("000007")
      .documentOwnerId(1L)
      .amount(BigDecimal.valueOf(1000))
      .currency("EGP")
      .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenNonExistBusinessPartner()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("0002")
      .companyCode("0002")
      .businessPartnerCode("784512")
      .documentOwnerId(1L)
      .amount(BigDecimal.valueOf(1000))
      .currency("EGP")
      .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenNonExistDocumentOwner()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("0002")
      .companyCode("0002")
      .businessPartnerCode("000007")
      .documentOwnerId(5L)
      .amount(BigDecimal.valueOf(1000))
      .currency("EGP")
      .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenNonExistCurrency()
    throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .businessUnitCode("0002")
      .companyCode("0002")
      .businessPartnerCode("000007")
      .documentOwnerId(1L)
      .amount(BigDecimal.valueOf(1000))
      .currency("WWW")
      .throwsArgumentViolationSecurityException();
  }
}
