package com.ebs.dda.commands.inventory.stockavailability.factory;

import com.ebs.dda.commands.inventory.stockavailability.factory.utils.UpdateStockAvailabilityStrategyFactoryTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class UpdateStockAvailabilityStrategyFactoryTests {
  private UpdateStockAvailabilityStrategyFactoryTestUtils utils;

  @Before
  public void init() {
    utils = new UpdateStockAvailabilityStrategyFactoryTestUtils();
  }

  @Test
  public void Should_ReturnGoodsIssueStrategyForGoodsIssueBasedOnSalesInvoice() {
    utils.whenStrategyFetchedWithType("GI_SI").thenAssertGoodsIssueStrategyReturned();
  }

  @Test
  public void Should_ReturnGoodsIssueStrategyForGoodsIssueBasedOnSalesOrder() {
    utils.whenStrategyFetchedWithType("GI_SO").thenAssertGoodsIssueStrategyReturned();
  }

  @Test
  public void Should_ReturnGoodsReceiptStrategyForGoodsReceiptBasedOnSalesReturn() {
    utils.whenStrategyFetchedWithType("GR_SR").thenAssertGoodsReceiptStrategyReturned();
  }

  @Test
  public void Should_ReturnGoodsReceiptStrategyForGoodsReceiptBasedOnPurchaseOrder() {
    utils.whenStrategyFetchedWithType("GR_PO").thenAssertGoodsReceiptStrategyReturned();
  }

  @Test
  public void Should_ReturnGoodsReceiptStrategyForInitialStockUpload() {
    utils.whenStrategyFetchedWithType("ISU").thenAssertInitialStockUploadStrategyReturned();
  }

  @Test
  public void Should_ThrowExceptionForIllegalArgument() {
    utils.whenStrategyFetchedWithType("Test").thenAssertExceptionThrown();
  }


}
