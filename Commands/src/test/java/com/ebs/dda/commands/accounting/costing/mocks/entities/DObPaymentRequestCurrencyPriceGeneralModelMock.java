package com.ebs.dda.commands.accounting.costing.mocks.entities;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModel;

import java.math.BigDecimal;

public class DObPaymentRequestCurrencyPriceGeneralModelMock
    extends DObPaymentRequestCurrencyPriceGeneralModel {
  private String userCode;
  private String referenceDocumentCode;
  private String dueDocumentType;
  private BigDecimal netAmount;
  private BigDecimal currencyPrice;

  public DObPaymentRequestCurrencyPriceGeneralModelMock(
      String userCode,
      String referenceDocumentCode,
      String dueDocumentType,
      BigDecimal netAmount,
      BigDecimal currencyPrice) {
    this.userCode = userCode;
    this.referenceDocumentCode = referenceDocumentCode;
    this.dueDocumentType = dueDocumentType;
    this.netAmount = netAmount;
    this.currencyPrice = currencyPrice;
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  @Override
  public String getDueDocumentType() {
    return dueDocumentType;
  }

  @Override
  public BigDecimal getNetAmount() {
    return netAmount;
  }

  @Override
  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }
}
