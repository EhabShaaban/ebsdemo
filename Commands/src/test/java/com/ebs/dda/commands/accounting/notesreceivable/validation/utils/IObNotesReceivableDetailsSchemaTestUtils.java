package com.ebs.dda.commands.accounting.notesreceivable.validation.utils;

import org.junit.jupiter.api.Assertions;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidator;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidatorFactory;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesDetailsValueObject;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

public class IObNotesReceivableDetailsSchemaTestUtils {

	public static final Object MISSING_KEY_MARKER = "N/A";
	private JsonObject requestBody;

	public IObNotesReceivableDetailsSchemaTestUtils() {
		requestBody = new JsonObject();
	}

	public IObNotesReceivableDetailsSchemaTestUtils withNoteBank(Object value) {
		return setFieldValue(IIObMonetaryNotesDetailsValueObject.NOTE_BANK, value);
	}

	public IObNotesReceivableDetailsSchemaTestUtils withNoteNumber(Object value) {
		return setFieldValue(IIObMonetaryNotesDetailsValueObject.NOTE_NUMBER, value);
	}

	public IObNotesReceivableDetailsSchemaTestUtils withDepotCode(Object value) {
		return setFieldValue(IIObMonetaryNotesDetailsValueObject.DEPOT_CODE, value);
	}

	public IObNotesReceivableDetailsSchemaTestUtils withDueDate(Object value) {
		return setFieldValue(IIObMonetaryNotesDetailsValueObject.DUE_DATE, value);
	}

	private IObNotesReceivableDetailsSchemaTestUtils setFieldValue(String field, Object value) {
		if (value == null) {
			requestBody.add(field, JsonNull.INSTANCE);
		}
		if (value instanceof String) {
			if (value.equals(MISSING_KEY_MARKER)) {
				return this;
			}
			requestBody.addProperty(field, (String) value);
		}
		if (value instanceof Number) {
			requestBody.addProperty(field, (Number) value);
		}
		return this;
	}

	public void assertThrowsArgumentSecurityException() {
		Assertions.assertThrows(ArgumentViolationSecurityException.class, this::validateJsonSchema);
	}

	public void assertValidateSuccessfully() throws Exception {
		validateJsonSchema();
	}

	private void validateJsonSchema() throws Exception {
		JsonSchemaValidator schemaValidator = new JsonSchemaValidator()
						.jsonSchemaFactory(new JsonSchemaValidatorFactory(
										IDObNotesReceivableJsonSchema.NOTE_DETAILS_SCHEMA));
		schemaValidator.validateSchemaForString(requestBody.toString());
	}

}
