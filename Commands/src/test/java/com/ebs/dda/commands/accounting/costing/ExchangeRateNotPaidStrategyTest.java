package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.commands.accounting.costing.utils.ExchangeRateStrategyTestUtils;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.repositories.CompanyRepMockUtils;
import com.ebs.repositories.ExchangeRateRepMockUtils;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ExchangeRateNotPaidStrategyTest {

  @MockBean private DObVendorInvoiceGeneralModelRep vendorInvoiceRep;
  @MockBean private DObPaymentRequestCurrencyPriceGeneralModelRep paymentRequestCurrencyPriceRep;

  private ExchangeRateStrategyTestUtils utils;
  private CostingExchangeRateUtils costingExchangeRateUtils;
  private ICalculateCostingExchangeRateStrategy strategy;

  @Before
  public void setUp() throws Exception {
    costingExchangeRateUtils =
        new CostingExchangeRateUtils(
            vendorInvoiceRep,
            paymentRequestCurrencyPriceRep,
            CompanyRepMockUtils.getRepository(),
            ExchangeRateRepMockUtils.getRepository());
    strategy = new CostingExchangeRateNotPaidStrategy(costingExchangeRateUtils);
    utils = new ExchangeRateStrategyTestUtils();
  }

  @Test
  public void testNotPaid() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceipt = utils.prepareGoodsReceipt();
    DObVendorInvoiceGeneralModel vendorInvoice =
        utils.prepareVendorInvoice(vendorInvoiceRep, goodsReceipt.getRefDocumentCode());
    CObCompanyGeneralModel companyGeneralModel =
        utils.prepareCompanyGeneralModel(goodsReceipt.getCompanyCode());

    utils.prepareExchangeRateGeneralModel(
        vendorInvoice.getCurrencyISO(),
        companyGeneralModel.getCurrencyIso(),
            BigDecimal.valueOf(16.32));
    // act
    BigDecimal currencyPrice = strategy.calculate(goodsReceipt);

    // assert
    Assert.assertEquals(currencyPrice, BigDecimal.valueOf(16.32));
  }
}
