package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObGeneralPaymentCreateCommandTestUtils;
import org.junit.Before;
import org.junit.Test;

public class DObGeneralPaymentCreateCommandTests {

	private DObGeneralPaymentCreateCommandTestUtils utils;

	@Before
	public void setup() {
		utils = new DObGeneralPaymentCreateCommandTestUtils();
	}

	@Test
	public void testCreateGeneralBankPayment() throws Exception {
		utils.withPaymentForm("BANK").withPaymentType("OTHER_PARTY_FOR_PURCHASE")
						.withBusinessUnit("0002")
						.withReferenceDocument("PURCHASEORDER", "2021000003", 1L, 2L, 2L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(1L).assertThatPaymentCurrencyId(null);
		utils.assertThatReferenceDocumentId(2L);
	}
	@Test
	public void testCreateGeneralCashPayment() throws Exception {
		utils.withPaymentForm("CASH").withPaymentType("OTHER_PARTY_FOR_PURCHASE")
						.withBusinessUnit("0002")
						.withReferenceDocument("PURCHASEORDER", "2021000003", 1L, 1L, 4L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(1L).assertThatPaymentCurrencyId(null);
		utils.assertThatReferenceDocumentId(4L);
	}
}
