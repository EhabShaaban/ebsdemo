package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dda.commands.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.masterdata.vendor.IObVendorAccountingInfoGeneralModelMockUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.entities.order.purchaseorder.DObLocalPurchaseOrderMockUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentRequestRepUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObPaymentRequestAccountDeterminationCommandTestUtils {

    private final DObPaymentRequestAccountDeterminationValueObject valueObject;
    private Long orderId;
    private String accountCode;

    public DObPaymentRequestAccountDeterminationCommandTestUtils() {
        valueObject = new DObPaymentRequestAccountDeterminationValueObject();
        DObPaymentRequestRepUtils.resetRepository();
        PaymentUtils.resetValue();
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withPaymentRequest(Long paymentRequestId, String paymentRequestCode, String paymentRequestType) {
        valueObject.setCode(paymentRequestCode);
        PaymentUtils.withPaymentRequestId(paymentRequestId);
        PaymentUtils.withRefInstanceId(paymentRequestId);
        PaymentUtils.withPaymentCode(paymentRequestCode);
        PaymentUtils.withPaymentType(paymentRequestType);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withDueDocumentType(String dueDocumentType) {
        PaymentUtils.withDueDocumentType(dueDocumentType);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withPaymentForm(String paymentForm) {
        PaymentUtils.withPaymentForm(paymentForm);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withDueDocumentCode(String dueDocumentCode) {
        PaymentUtils.withDueDocumentCode(dueDocumentCode);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withBusinessPartnerCode(String businessPartnerCode) {
        PaymentUtils.withBusinessPartnerCode(businessPartnerCode);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withBankAccountId (Long bankAccountId) {
        PaymentUtils.withBankAccountId(bankAccountId);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withTreasuryAccountId (Long treasuryAccountId) {
        PaymentUtils.withTreasuryAccountId(treasuryAccountId);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withAmount(BigDecimal amount) {
        PaymentUtils.withAmount(amount);
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withOrder(Long poId, String orderCode) {
        DObLocalPurchaseOrderMockUtils.buildEntity(poId, orderCode);
        this.orderId = poId;
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils withVendor(String vendorCode, String vendorAccountCode) {
        IObVendorAccountingInfoGeneralModelMockUtils.buildGeneralModel(vendorCode, vendorAccountCode);
        VendorMockUtils.buildMObVendor(vendorCode, 1L);
        this.accountCode = vendorAccountCode;
        return this;
    }

    public DObPaymentRequestAccountDeterminationCommandTestUtils whenExecute() throws Exception {
        PaymentUtils.buildPaymentDetailsGeneralModel();
        DObPaymentRequestAccountDeterminationCommand command = DObPaymentRequestAccountDeterminationCommandTestFactory.createCommandInstance(PaymentUtils.getPaymentDetails(), this.orderId, this.accountCode);
        command.executeCommand(valueObject);
        return this;
    }


    public DObPaymentRequestAccountDeterminationCommandTestUtils saveMethodIsCalledSuccessfully() throws Exception {
        Mockito.verify(DObPaymentRequestRepUtils.getRepository(), Mockito.times(1)).update(PaymentUtils.getPaymentRequest());
        return this;
    }

    public void numberOfAccountsDeterminedIs(int accountsNum) {
        ArgumentCaptor<IObAccountingDocumentAccountingDetails> argumentCaptor = ArgumentCaptor.forClass(IObAccountingDocumentAccountingDetails.class);
        Mockito.verify(PaymentUtils.getPaymentRequest(), Mockito.times(accountsNum)).addLineDetail(ArgumentMatchers.any(), argumentCaptor.capture());
    }
}
