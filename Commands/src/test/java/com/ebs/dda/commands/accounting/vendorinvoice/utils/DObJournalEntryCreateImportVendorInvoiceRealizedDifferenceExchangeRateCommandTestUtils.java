package com.ebs.dda.commands.accounting.vendorinvoice.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep;
import com.ebs.entities.accounting.journalentry.DObVendorInvoiceJournalEntryMockUtils;
import com.ebs.entities.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils;
import com.ebs.repositories.LObGlobalAccountConfigGeneralModelRepUtils;
import com.ebs.repositories.accounting.journalentry.DobVendorInvoiceJournalEntryRepMockUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public
class DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils {

  private DObJournalEntryCreateValueObject valueObject;
  private DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand command;
  private DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep
      invoiceRealizedExchangeRateDifferenceGeneralModelRep;

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils() {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.resetValue();
    DObVendorInvoiceJournalEntryMockUtils.resetVendorInvoiceJournalEntry();
    DobVendorInvoiceJournalEntryRepMockUtils.resetRepository();
    valueObject = new DObJournalEntryCreateValueObject();
    command = new DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand();
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      WithVendorInvoiceCode(String vendorInvoiceCode) {
    valueObject.setUserCode(vendorInvoiceCode);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      WithJournalEntryCode(String journalEntryCode) {
    valueObject.setJournalEntryCode(journalEntryCode);
    DObVendorInvoiceJournalEntryMockUtils.withUserCode(journalEntryCode);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withPaymentAmount(BigDecimal paymentAmount) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.withPaymentRequestNetAmount(
        paymentAmount);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withVendorInvoiceCurrencyPrice(BigDecimal vendorInvoiceCurrencyPrice) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils
        .withVendorInvoiceCurrencyPrice(vendorInvoiceCurrencyPrice);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withPaymentCurrencyPrice(BigDecimal currentCurrencyPrice) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.withPaymentCurrencyPrice(
        currentCurrencyPrice);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withCompanyCurrencyId(Long localCurrencyId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.withCompanyCurrencyId(
        localCurrencyId);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withVendorSubLedger(String vendorSubLedger) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils
        .withVendorInvoiceAccountLedger(vendorSubLedger);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withGlVendorAccountId(Long glDebitAccountId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils
        .withVendorInvoiceGlAccountId(glDebitAccountId);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      withVendorId(Long vendorId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils
        .withVendorInvoiceGlSubAccountId(vendorId);
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      whenExecute() throws Exception {
    prepareRealizedExchangeRateDifferenceGeneralModelRep();
    prepareUserAccountSecurityService();
    prepareLObGlobalAccountConfigGeneralModelRep();
    prepareVendorInvoiceJournalEntryRepUtils();
    command.executeCommand(valueObject);
    return this;
  }

  private void prepareRealizedExchangeRateDifferenceGeneralModelRep() {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel
        realizedExchangeRateDifferenceGeneralModel =
            DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.buildEntity();
    List<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel>
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModels = new ArrayList<>();
    vendorInvoiceRealizedExchangeRateDifferenceGeneralModels.add(
        realizedExchangeRateDifferenceGeneralModel);

    invoiceRealizedExchangeRateDifferenceGeneralModelRep =
        Mockito.mock(DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep.class);

    Mockito.when(
            invoiceRealizedExchangeRateDifferenceGeneralModelRep.findByVendorInvoiceCodeAndPaymentType(
                valueObject.getUserCode(), PaymentTypeEnum.PaymentType.VENDOR.name()))
        .thenReturn(vendorInvoiceRealizedExchangeRateDifferenceGeneralModels);
    command.setVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep(
        invoiceRealizedExchangeRateDifferenceGeneralModelRep);
  }

  private void prepareUserAccountSecurityService() {
    CommandTestUtils commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("admin");
    IUserAccountSecurityService userAccountSecurityService =
        commandTestUtils.getUserAccountSecurityService();
    command.setUserAccountSecurityService(userAccountSecurityService);
  }

  private void prepareLObGlobalAccountConfigGeneralModelRep() {
    LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
        ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);
    command.setGlobalAccountConfigGeneralModelRep(
        LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
  }

  private void prepareVendorInvoiceJournalEntryRepUtils() {
    DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry =
        DObVendorInvoiceJournalEntryMockUtils.mockEntity().build();
    DobVendorInvoiceJournalEntryRepMockUtils.findOneByUserCode(vendorInvoiceJournalEntry);

    command.setVendorInvoiceJournalEntryRep(DobVendorInvoiceJournalEntryRepMockUtils.getRepository());
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      assertThatJournalEntriesIsUpdated() throws Exception {
    Mockito.verify(DobVendorInvoiceJournalEntryRepMockUtils.getRepository(), Mockito.times(1))
        .update(DObVendorInvoiceJournalEntryMockUtils.getVendorInvoiceJournalEntry());
    return this;
  }

  public DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      assertThatJournalEntryIsRecorded(Long glAccountId, BigDecimal value, String accountingEntry) {
    ArgumentCaptor<IObJournalEntryItem> journalEntryArgumentCaptor =
        ArgumentCaptor.forClass(IObJournalEntryItem.class);
    Mockito.verify(
            DObVendorInvoiceJournalEntryMockUtils.getVendorInvoiceJournalEntry(), Mockito.times(2))
        .addLineDetail(
            Mockito.eq(IDObJournalEntry.journalItemAttr), journalEntryArgumentCaptor.capture());
    List<IObJournalEntryItem> journalEntries = journalEntryArgumentCaptor.getAllValues();
    IObJournalEntryItem actualJournalItem =
        journalEntries.stream()
            .filter(
                item ->
                    glAccountId.equals(item.getAccountId())
                        && ((accountingEntry.equals(AccountingEntry.CREDIT.name())
                                && value.compareTo(item.getCredit()) == 0)
                            || (accountingEntry.equals(AccountingEntry.DEBIT.name())
                                && value.compareTo(item.getDebit()) == 0)))
            .findAny()
            .orElse(null);
    Assert.assertNotNull(actualJournalItem);
    return this;
  }

  public void assertThatNoJournalEntryIsAdded() {
    ArgumentCaptor<IObJournalEntryItem> journalEntryArgumentCaptor =
        ArgumentCaptor.forClass(IObJournalEntryItem.class);
    Mockito.verify(
            DObVendorInvoiceJournalEntryMockUtils.getVendorInvoiceJournalEntry(), Mockito.never())
        .addLineDetail(
            Mockito.eq(IDObJournalEntry.journalItemAttr), journalEntryArgumentCaptor.capture());
  }
}
