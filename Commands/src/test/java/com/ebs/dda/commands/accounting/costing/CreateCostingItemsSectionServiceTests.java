package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemsCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.entities.LandedCostSummaryGeneralModelMockUtils;
import com.ebs.entities.VendorInvoiceItemsMockUtils;
import com.ebs.entities.VendorInvoiceSummaryUtils;
import com.ebs.entities.accounting.costing.CostingItemMockUtils;
import com.ebs.repositories.*;
import com.ebs.repositories.masterdata.item.ItemGeneralModelRepMockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class CreateCostingItemsSectionServiceTests {
  private CreateCostingItemsSectionService service;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    service =
        new CreateCostingItemsSectionService(
            IObInvoiceSummaryGeneralModelRepUtils.getRepository(),
            IObVendorInvoiceItemsGeneralModelRepUtils.getRepository(),
            IObGoodsReceiptReceivedItemsGeneralModelRepUtils.getRepository(),
            IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRepUtils.getRepository(),
            ItemGeneralModelRepMockUtils.getRepository(),
            LandedCostFactorItemsSummaryGeneralModelRepUtils.getRepository(),
            commandTestUtils.getUserAccountSecurityService());
  }

  @Test
  public void shouldSetCostingItemActualUnitPriceAndActualUnitLandedCost() {

    // Arrange
    String goodsReceiptCode = "20210000001";
    String landedCostCode = "20210000001";
    String vendorInvoiceCode = "20210000001";
    BigDecimal currencyPrice = new BigDecimal("12.123");
    Long costingItemId = 1L;
    Long costingItemItemId = 1L;
    Long costingItemUomId = 1L;
    String costingItemStockType = StockTypeEnum.StockType.UNRESTRICTED_USE.name();
    BigDecimal costingItemQuantity = new BigDecimal("10");
    BigDecimal costingItemWeight = new BigDecimal("100");
    BigDecimal orderUnitPrice = new BigDecimal("12");
    BigDecimal quantityInOrderUnit = new BigDecimal("12");
    String totalAmountBeforeTaxes = "125";
    BigDecimal actualTotalAmount = BigDecimal.valueOf(1570);

    IObVendorInvoiceItemsGeneralModel vendorInvoice =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode(vendorInvoiceCode)
            .itemId(costingItemItemId)
            .orderUnitId(costingItemUomId)
            .quantityInOrderUnit(quantityInOrderUnit)
            .orderUnitPrice(orderUnitPrice)
            .build();
    IObVendorInvoiceItemsGeneralModelRepUtils.findOneByInvoiceCodeAndItemIdAndOrderUnitId(
        vendorInvoice);

    IObInvoiceSummaryGeneralModel invoiceSummary =
        VendorInvoiceSummaryUtils.mockGeneralModel()
            .invoiceCode(vendorInvoiceCode)
            .invoiceType(IDocumentTypes.VENDOR_INVOICE)
            .totalAmountBeforeTaxes(totalAmountBeforeTaxes)
            .build();
    IObInvoiceSummaryGeneralModelRepUtils.findOneByInvoiceCodeAndInvoiceType(invoiceSummary);

    IObLandedCostFactorItemsSummary landedCost =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostCode)
            .actualValue(actualTotalAmount)
            .build();
    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCost);

    IObCostingItem costingItem =
        CostingItemMockUtils.mockEntity()
            .id(costingItemId)
            .itemId(costingItemItemId)
            .uomId(costingItemUomId)
            .stockType(costingItemStockType)
            .quantity(costingItemQuantity)
            .itemWeight(costingItemWeight)
            .build();

    IObCostingItemsCreationValueObject costingItemsCreationValueObject =
        new IObCostingItemsCreationValueObject(
            vendorInvoiceCode, goodsReceiptCode, landedCostCode, currencyPrice,true);

    BigDecimal expectedActualUnitPrice = new BigDecimal("174.5712");
    BigDecimal expectedActualUnitLandedCost = new BigDecimal("15700");

    // Act
    service.setCostingItemCostFields(costingItem, costingItemsCreationValueObject);

    // assert
    Mockito.verify(costingItem, Mockito.times(1)).setActualUnitPrice(expectedActualUnitPrice);
    Mockito.verify(costingItem, Mockito.times(0)).setEstimateUnitPrice(expectedActualUnitPrice);

    Mockito.verify(costingItem, Mockito.times(1))
        .setActualUnitLandedCost(expectedActualUnitLandedCost);
    Mockito.verify(costingItem, Mockito.times(0))
        .setEstimateUnitLandedCost(expectedActualUnitLandedCost);
  }

  @Test
  public void shouldSetCostingItemEstimateUnitPriceAndEstimateUnitLandedCost() {

    // Arrange
    String goodsReceiptCode = "20210000001";
    String landedCostCode = "20210000001";
    String vendorInvoiceCode = "20210000001";
    BigDecimal currencyPrice = new BigDecimal("12.123");
    Long costingItemId = 1L;
    Long costingItemItemId = 1L;
    Long costingItemUomId = 1L;
    String costingItemStockType = StockTypeEnum.StockType.UNRESTRICTED_USE.name();
    BigDecimal costingItemQuantity = new BigDecimal("10");
    BigDecimal expectedCostingItemWeight = new BigDecimal("1.152");
    BigDecimal orderUnitPrice = new BigDecimal("12");
    BigDecimal quantityInOrderUnit = new BigDecimal("12");
    String totalAmountBeforeTaxes = "125";
    BigDecimal estimateTotalAmount = BigDecimal.valueOf(1570);

    IObVendorInvoiceItemsGeneralModel vendorInvoice =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode(vendorInvoiceCode)
            .itemId(costingItemItemId)
            .orderUnitId(costingItemUomId)
            .quantityInOrderUnit(quantityInOrderUnit)
            .orderUnitPrice(orderUnitPrice)
            .build();
    IObVendorInvoiceItemsGeneralModelRepUtils.findOneByInvoiceCodeAndItemIdAndOrderUnitId(
        vendorInvoice);

    IObInvoiceSummaryGeneralModel invoiceSummary =
        VendorInvoiceSummaryUtils.mockGeneralModel()
            .invoiceCode(vendorInvoiceCode)
            .invoiceType(IDocumentTypes.VENDOR_INVOICE)
            .totalAmountBeforeTaxes(totalAmountBeforeTaxes)
            .build();
    IObInvoiceSummaryGeneralModelRepUtils.findOneByInvoiceCodeAndInvoiceType(invoiceSummary);

    IObLandedCostFactorItemsSummary landedCost =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostCode)
            .estimateValue(estimateTotalAmount)
            .build();
    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCost);

    IObCostingItem costingItem =
        CostingItemMockUtils.mockEntity()
            .itemId(costingItemItemId)
            .uomId(costingItemUomId)
            .stockType(costingItemStockType)
            .quantity(costingItemQuantity)
            .itemWeight(expectedCostingItemWeight)
            .build();

    IObCostingItemsCreationValueObject costingItemsCreationValueObject =
        new IObCostingItemsCreationValueObject(
            vendorInvoiceCode, goodsReceiptCode, landedCostCode, currencyPrice,false);

    BigDecimal expectedEstimateUnitPrice = new BigDecimal("174.5712");
    BigDecimal expectedEstimateUnitLandedCost = new BigDecimal("180.864");

    // Act
    service.setCostingItemCostFields(costingItem, costingItemsCreationValueObject);

    // assert
    Mockito.verify(costingItem, Mockito.times(1)).setToBeConsideredInCostingPer(expectedCostingItemWeight);
    Mockito.verify(costingItem, Mockito.times(1)).setEstimateUnitPrice(expectedEstimateUnitPrice);
    Mockito.verify(costingItem, Mockito.times(0)).setActualUnitPrice(expectedEstimateUnitPrice);

    Mockito.verify(costingItem, Mockito.times(1))
            .setEstimateUnitLandedCost(expectedEstimateUnitLandedCost);
    Mockito.verify(costingItem, Mockito.times(0))
        .setActualUnitLandedCost(expectedEstimateUnitLandedCost);
  }
}
