package com.ebs.dda.commands.accounting.vendorinvoice.utils;


import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceAccountDeterminationCommand;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentOrderAccountingDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.entities.accounting.VendorInvoiceUtils;
import com.ebs.entities.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelMockUtils;
import com.ebs.entities.order.purchaseorder.IObPurchaseOrderFulFillerVendorMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceRepMockUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DObVendorInvoiceAccountDeterminationCommandTestUtils {

    private final DObVendorInvoiceActivateValueObject valueObject;
    private final List<DObVendorInvoiceItemAccountsPreparationGeneralModel> items;
    private final List<IObVendorInvoiceTaxesGeneralModel> taxes;

    public DObVendorInvoiceAccountDeterminationCommandTestUtils() {
        valueObject = new DObVendorInvoiceActivateValueObject();
        items = new ArrayList<>();
        taxes = new ArrayList<>();
        DObVendorInvoiceRepMockUtils.resetRepository();
        VendorInvoiceUtils.resetValue();
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withVendorInvoice(String vendorInvoiceCode, String vendorInvoiceType) {
        valueObject.setVendorInvoiceCode(vendorInvoiceCode);
        VendorInvoiceUtils.withVendorInvoiceCode(vendorInvoiceCode);
        VendorInvoiceUtils.withVendorInvoiceType(vendorInvoiceType);
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.withVendorInvoiceCode(vendorInvoiceCode);
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.withVendorInvoiceCode(vendorInvoiceCode);
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withVendorId(Long vendorId) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.withGlSubAccountId(vendorId);
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withAmount(BigDecimal amount) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.withAmount(amount);
        return this;

    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withVendorAccountId(Long vendorAccountId) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.withGlAccountId(vendorAccountId);
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils whenExecute() throws Exception {
        DObVendorInvoiceAccountDeterminationCommand command = DObVendorInvoiceAccountDeterminationCommandTestFactory.createCommandInstance(VendorInvoiceUtils.getVendorInvoiceType(), valueObject.getVendorInvoiceCode(), items, taxes);
        command.executeCommand(valueObject);
        return this;
    }


    public DObVendorInvoiceAccountDeterminationCommandTestUtils saveMethodIsCalledSuccessfully() throws Exception {
        Mockito.verify(DObVendorInvoiceRepMockUtils.getRepository(), Mockito.times(1)).update(VendorInvoiceUtils.getVendorInvoice());
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withImportPurchaseOrderId(Long importPurchaseOrderId) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.withOrderId(importPurchaseOrderId);
        IObPurchaseOrderFulFillerVendorMockUtils.withReferencePurchaseOrderId(importPurchaseOrderId);
        return this;
    }
    public DObVendorInvoiceAccountDeterminationCommandTestUtils withReferencePurchaseOrderId(Long importPurchaseOrderId) {
        IObPurchaseOrderFulFillerVendorMockUtils.withReferencePurchaseOrderId(importPurchaseOrderId);
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withItem(Long itemAccountId, BigDecimal itemAmount) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.withGlAccountId(itemAccountId);
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.withAmount(itemAmount);
        items.add(DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.buildEntity());
        return this;
    }

    public void numberOfAccountsDeterminedIs(int accountsNum) {
        ArgumentCaptor<IObAccountingDocumentOrderAccountingDetails> argumentCaptor = ArgumentCaptor.forClass(IObAccountingDocumentOrderAccountingDetails.class);
        Mockito.verify(VendorInvoiceUtils.getVendorInvoice(), Mockito.times(accountsNum)).addLineDetail(ArgumentMatchers.any(), argumentCaptor.capture());
    }


    public DObVendorInvoiceAccountDeterminationCommandTestUtils withTaxPercentage(BigDecimal taxPercentage) {
        taxes.add(IObVendorInvoiceTaxesGeneralModelMockUtils.buildGeneralModel(taxPercentage));
        return this;
    }

    public DObVendorInvoiceAccountDeterminationCommandTestUtils withServiceOrderId(Long serviceOrderId) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.withOrderId(serviceOrderId);
        IObPurchaseOrderFulFillerVendorMockUtils.withOrderId(serviceOrderId);
        return this;
    }
}
