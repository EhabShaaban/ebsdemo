package com.ebs.dda.commands.order.purchaseorder.mocks.entities;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import lombok.Builder;
import org.mockito.Mockito;

public class EntityLockCommandMockUtils {
  @Builder(builderMethodName = "newEntity")
  public static EntityLockCommand<DObOrderDocument> buildDObOrderDocumentEntityLockCommand() {
    EntityLockCommand entityLockCommand = Mockito.mock(EntityLockCommand.class);
    return entityLockCommand;
  }
}
