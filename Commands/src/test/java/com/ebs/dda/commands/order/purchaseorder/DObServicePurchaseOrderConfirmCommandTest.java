package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderNewStateMachineFactory;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;
import com.ebs.entities.order.purchaseorder.DObServicePurchaseOrderMockUtils;
import com.ebs.entities.order.purchaseorder.IObOrderCycleDatesMockUtils;
import com.ebs.entities.order.purchaseorder.IObOrderItemSummaryGeneralModelMockUtils;
import com.ebs.repositories.order.purchaseorder.DObPurchaseOrderRepMockUtils;
import com.ebs.repositories.order.purchaseorder.IObOrderCycleDatesRepMockUtils;
import com.ebs.repositories.order.purchaseorder.IObOrderItemSummaryGeneralModelRepMockUtils;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DObServicePurchaseOrderConfirmCommandTest {

  @Mock private DObPurchaseOrderNewStateMachineFactory mockedFactory;
  @Mock private DObServicePurchaseOrderStateMachine servicePurchaseOrderStateMachine;

  private DObPurchaseOrderConfirmCommand confirmCommand;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {

    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");

    confirmCommand = new DObPurchaseOrderConfirmCommand();
    confirmCommand.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    confirmCommand.setPurchaseOrderRep(DObPurchaseOrderRepMockUtils.getRepository());
    confirmCommand.setOrderCycleDatesRep(IObOrderCycleDatesRepMockUtils.getRepository());
    confirmCommand.setOrderSummaryGeneralModelRep(
        IObOrderItemSummaryGeneralModelRepMockUtils.getRepository());
    confirmCommand.setPurchaseOrderFactory(mockedFactory);

    Mockito.doReturn(servicePurchaseOrderStateMachine)
        .when(mockedFactory)
        .createPurchaseOrderStateMachine(Mockito.any(DObPurchaseOrder.class));
  }

  @Test
  public void shouldSetRemainingForServicePurchaseOrderAfterConfirmedState() throws Exception {
    //    Arrange
    String purchaseOrderCode = "2018000015";
    String totalAmountAfterTaxes = "14025.123456";

    DObPurchaseOrderConfirmValueObject valueObject = new DObPurchaseOrderConfirmValueObject();

    DObPurchaseOrder purchaseOrder =
        DObServicePurchaseOrderMockUtils.mockEntity()
            .id(1L)
            .userCode(purchaseOrderCode)
            .objectTypeCode(OrderTypeEnum.SERVICE_PO.name())
            .build();
    DObPurchaseOrderRepMockUtils.findOneByUserCode(purchaseOrder);

    IObOrderCycleDates orderCycleDates = IObOrderCycleDatesMockUtils.mockEntity().id(1L).build();
    IObOrderCycleDatesRepMockUtils.findOneByRefInstanceId(orderCycleDates);

    IObOrderItemSummaryGeneralModel summary =
        IObOrderItemSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(purchaseOrderCode)
            .totalAmountAfterDiscount(new BigDecimal(totalAmountAfterTaxes))
            .build();
    IObOrderItemSummaryGeneralModelRepMockUtils.findOneByUserCode(summary);

    //    Act
    confirmCommand.executeCommand(purchaseOrderCode, valueObject);

    //    Assert
    Mockito.verify(purchaseOrder, Mockito.times(1))
        .setRemaining(new BigDecimal(totalAmountAfterTaxes));
  }
}
