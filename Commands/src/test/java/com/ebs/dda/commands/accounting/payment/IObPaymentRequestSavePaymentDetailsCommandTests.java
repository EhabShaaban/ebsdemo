package com.ebs.dda.commands.accounting.payment;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.commands.accounting.payment.utils.IObPaymentRequestSavePaymentDetailsCommandTestUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class IObPaymentRequestSavePaymentDetailsCommandTests {

    private IObPaymentRequestSavePaymentDetailsCommandTestUtils utils;

    @Before
    public void setup() { utils = new IObPaymentRequestSavePaymentDetailsCommandTestUtils(); }

    @Test
    public void Should_ThrowArgumentViolationSecurityException_When_NetAmountValueNotValidAsNumber() {
        utils.withNetAmountAsString(" ");

        Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
                .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
    }

    @Test
    public void Should_ThrowArgumentViolationSecurityException_When_NetAmountValueNotValidAsMinimum() {
        utils.withNetAmount(BigDecimal.valueOf(-0.000005));

        Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
                .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
    }

    @Test
    public void Should_ThrowArgumentViolationSecurityException_When_NetAmountValueNotValidAsMaximum() {
        utils.withNetAmount(BigDecimal.valueOf(1000000001));

        Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
                .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
    }

    @Test
    public void Should_ThrowArgumentViolationSecurityException_When_NetAmountValueNotValidAsMultipleOf() {
        utils.withNetAmount(BigDecimal.valueOf(152.0142621));

        Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
                .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
    }
}
