package com.ebs.dda.commands.inventory.stockavailability.utils;

import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.commands.inventory.stockavailability.mocks.entities.CObStockAvailabilityMock;
import com.ebs.dda.commands.inventory.stockavailability.mocks.entities.IObItemQuantityGeneralModelMock;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.stockavailability.CObStockAvailabilityRep;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class UpdateStockAvailabilityServiceTestUtils {

  private CObStockAvailabilityRep repository;
  private CommandTestUtils commandTestUtils;
  private StockAvailabilityCodeGenerator codeGenerator;
  private UpdateStockAvailabilityService service;
  private IObItemQuantityGeneralModel item;
  private BigDecimal stockAvailabilityQuantity;
  private String stockAvailabilityCode;

  public UpdateStockAvailabilityServiceTestUtils() {
    mockDependencies();
    initService();
  }

  public static IObItemQuantityGeneralModelMock Item(
      String stockType,
      String purchaseUnitCode,
      String companyCode,
      String plantCode,
      String storehouseCode,
      String itemCode,
      String unitOfMeasureCode,
      String userCode,
      BigDecimal quantity) {
    return new IObItemQuantityGeneralModelMock(
        userCode,
        itemCode,
        plantCode,
        storehouseCode,
        companyCode,
        purchaseUnitCode,
        unitOfMeasureCode,
        quantity,
        stockType);
  }

  private void initService() {
    service = new UpdateStockAvailabilityService();
    service.setStockAvailabilityRep(repository);
    service.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    service.setCodeGenerator(codeGenerator);
  }

  private void mockDependencies() {
    repository = Mockito.mock(CObStockAvailabilityRep.class);
    commandTestUtils = new CommandTestUtils();
    codeGenerator = Mockito.mock(StockAvailabilityCodeGenerator.class);
  }

  public UpdateStockAvailabilityServiceTestUtils whenUpdate() {
    mockData();
    service.updateOrCreateStockAvailabilityForItem(item, item.getQuantity());
    return this;
  }

  private void mockData() {
    mockGenerator();
    mockUserAccount();
  }

  private void mockGenerator() {
    Mockito.when(codeGenerator.generateUserCode(Mockito.any()))
        .thenReturn(this.stockAvailabilityCode);
  }

  public UpdateStockAvailabilityServiceTestUtils withItem(IObItemQuantityGeneralModel item) {
    this.item = item;
    return this;
  }

  public UpdateStockAvailabilityServiceTestUtils withStockAvailabilityCode(String code) {
    this.stockAvailabilityCode = code;
    return this;
  }

  public UpdateStockAvailabilityServiceTestUtils
      thenAssertStockAvailabilityFetchedOnceFromRepositoryWithCode(String stockAvailabilityCode) {
    Mockito.verify(repository, Mockito.times(1)).findOneByUserCode(stockAvailabilityCode);
    return this;
  }

  public void thenAssertStockAvailabilityRepositoryUpdateCalledOnceWithProperValue(
      String userCode, BigDecimal quantity) {
    ArgumentCaptor<CObStockAvailability> updateResult =
        ArgumentCaptor.forClass(CObStockAvailability.class);
    try {
      Mockito.verify(repository, Mockito.times(1)).update(updateResult.capture());
    } catch (Exception e) {
      Assert.fail("Exception while asserting " + e.getMessage());
    }
    assertUpdateValue(updateResult.getValue(), userCode, quantity);
  }

  private void assertUpdateValue(CObStockAvailability value, String userCode, BigDecimal quantity) {
    Assert.isEqual(value.getUserCode(), userCode, "Different stock availability was updated");
    Assert.isTrue(
        value.getAvailableQuantity().compareTo(quantity) == 0,
        "Quantity was not calculated correctly");
  }

  public void thenAssertStockAvailabilityRepositoryCreateCalledOnceWithProperValue(
      String userCode, BigDecimal quantity) {
    ArgumentCaptor<CObStockAvailability> createResult =
        ArgumentCaptor.forClass(CObStockAvailability.class);
    try {
      Mockito.verify(repository, Mockito.times(1)).create(createResult.capture());
    } catch (Exception e) {
      Assert.fail("Exception while asserting " + e.getMessage());
    }
    assertCreateValue(createResult.getValue(), userCode, quantity);
  }

  private void assertCreateValue(CObStockAvailability value, String userCode, BigDecimal quantity) {
    Assert.isEqual(value.getUserCode(), userCode, "Different stock availability was created");
    Assert.isTrue(
        value.getAvailableQuantity().compareTo(quantity) == 0, "Created quantity doesn't match");
  }

  private void mockUserAccount() {
    commandTestUtils.mockUserAccount("ADMIN");
  }

  public UpdateStockAvailabilityServiceTestUtils withStockAvailabilityQuantity(BigDecimal value) {
    this.stockAvailabilityQuantity = value;
    CObStockAvailabilityMock stockAvailabilityMock =
        new CObStockAvailabilityMock(this.stockAvailabilityCode, this.stockAvailabilityQuantity);
    Mockito.when(repository.findOneByUserCode(stockAvailabilityCode))
        .thenReturn(stockAvailabilityMock);
    return this;
  }
}
