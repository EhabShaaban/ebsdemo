package com.ebs.dda.commands.accounting.electronicinvoice;

import com.ebs.dda.commands.accounting.electronicinvoice.utils.SalesInvoiceEInvoiceSubmitCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SalesInvoiceEInvoiceSubmitCommandTest {

    private SalesInvoiceEInvoiceSubmitCommandTestUtils testUtils;

    @Before
    public void setup() {
        testUtils = new SalesInvoiceEInvoiceSubmitCommandTestUtils();
    }

    @Test
    public void Should_SerializeEInvoiceData_Successfully() throws Exception {
        testUtils
            .salesInvoiceId(1L)
            .salesInvoiceUserCode("2021000001")
            .withValidEInvoiceCoreData()
            .withValidEInvoiceItemsData()
            .withValidEInvoiceTaxesData()
            .verifyThatEInvoiceDataSerializedSuccessfully();
    }
}
