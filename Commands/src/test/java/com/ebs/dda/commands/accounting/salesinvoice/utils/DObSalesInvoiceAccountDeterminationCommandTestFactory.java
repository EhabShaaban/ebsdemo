package com.ebs.dda.commands.accounting.salesinvoice.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceAccountDeterminationCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import com.ebs.repositories.accounting.salesInvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils;
import com.ebs.repositories.accounting.salesInvoice.DObSalesInvoiceRepMockUtils;
import com.ebs.repositories.accounting.salesInvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils;

import java.util.List;

public class DObSalesInvoiceAccountDeterminationCommandTestFactory {
    private static DObSalesInvoiceAccountDeterminationCommand command;

    public static DObSalesInvoiceAccountDeterminationCommand createCommandInstance(String salesInvoiceCode, List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> items) {
        command = new DObSalesInvoiceAccountDeterminationCommand();
        prepareGeneralCommandDependencies(salesInvoiceCode, items);
        return command;
    }

    private static void prepareGeneralCommandDependencies(String salesInvoiceCode, List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> items) {
        prepareSalesInvoice();
        prepareCreditDebitAccounts();
        prepareSalesInvoiceTaxes(salesInvoiceCode, items);
        prepareUserAccountSecurityService();
    }

    private static void prepareSalesInvoice() {
        DObSalesInvoice salesInvoice = DObSalesInvoiceMockUtils.buildEntity();
        DObSalesInvoiceRepMockUtils.findOneByUserCode(salesInvoice);
        command.setSalesInvoiceRep(DObSalesInvoiceRepMockUtils.getDObSalesInvoiceRep());
    }

    private static void prepareCreditDebitAccounts() {
        DObSalesInvoiceJournalEntryPreparationGeneralModel creditPreparationGeneralModel = DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.buildEntity();
        DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.findOneByCode(creditPreparationGeneralModel);
        command.setSalesInvoicePreparationRep(DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.getRepository());
    }

    private static void prepareSalesInvoiceTaxes(String salesInvoiceCode, List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> items) {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils.findByCode(items, salesInvoiceCode);
        command.setSalesInvoiceItemsPreparationRep(IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils.getRepository());
    }

    private static void prepareUserAccountSecurityService() {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }
}
