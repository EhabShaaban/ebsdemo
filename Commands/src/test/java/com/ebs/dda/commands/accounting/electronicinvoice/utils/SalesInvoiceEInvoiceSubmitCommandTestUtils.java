package com.ebs.dda.commands.accounting.electronicinvoice.utils;

import com.ebs.dda.accounting.electronicinvoice.DObSalesInvoiceEInvoiceData;
import com.ebs.dda.accounting.electronicinvoice.DObSalesInvoiceEInvoiceDataFactory;
import com.ebs.dda.accounting.electronicinvoice.EInvoiceTaxAuthorityClient;
import com.ebs.dda.commands.accounting.electronicinvoice.EInvoiceSubmitCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.entities.accounting.electronicinvoice.DObSalesInvoiceEInvoiceDataGeneralModelMockUtils;
import com.ebs.entities.accounting.electronicinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModelMockUtils;
import com.ebs.entities.accounting.electronicinvoice.IObSalesInvoiceTaxesGeneralModelMockUtils;
import com.ebs.repositories.accounting.electronicinvoice.DObSalesInvoiceEInvoiceDataGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.electronicinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.electronicinvoice.IObSalesInvoiceTaxesGeneralModelRepMockUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class SalesInvoiceEInvoiceSubmitCommandTestUtils {

    private EInvoiceSubmitCommand<DObSalesInvoiceEInvoiceData> command;
    private Long salesInvoiceId;
    private String userCode;

    private DObSalesInvoiceEInvoiceDataFactory eInvoiceDataFactory;

    private DObSalesInvoiceEInvoiceDataGeneralModel eInvoiceDataGM;
    private List<DObSalesInvoiceItemsEInvoiceDataGeneralModel> eInvoiceItemsDataGM;
    private List<IObSalesInvoiceTaxesGeneralModel> eInvoiceTaxesDataGM;

    private EInvoiceTaxAuthorityClient taxAuthorityClient;

    public SalesInvoiceEInvoiceSubmitCommandTestUtils salesInvoiceId(Long id) {
        this.salesInvoiceId = id;
        return this;
    }

    public SalesInvoiceEInvoiceSubmitCommandTestUtils salesInvoiceUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public SalesInvoiceEInvoiceSubmitCommandTestUtils withValidEInvoiceCoreData() {
        this.eInvoiceDataGM =  DObSalesInvoiceEInvoiceDataGeneralModelMockUtils
            .newGeneralModel()
            .id(salesInvoiceId)
            .companyTaxRegistrationNumber("N/A")
            .companyName("Al Madina")
            .companyCountry("Egypt")
            .companyGovernate("Cairo")
            .companyRegion("N/A")
            .companyStreet("N/A")
            .companyBuildingNumber("N/A")
            .companyPostalCode("14523")

            .customerTaxRegistrationNumber("N/A")
            .customerName("Al Ahram")
            .customerType("N/A")
            .customerCountry("Egypt")
            .customerGovernate("Cairo")
            .customerRegion("El Sheikh Zayed")
            .customerStreet("N/A")
            .customerBuildingNumber("N/A")
            .customerPostalCode("12596")

            .journalDate(DateTime.parse("2021-09-21T16:27:33.000+02:00"))
            .taxpayerActivityCode("N/A")
            .invoiceSerialNumber("N/A")
            .extraDiscountAmount(BigDecimal.ZERO)
            .build();

        return this;
    }

    public SalesInvoiceEInvoiceSubmitCommandTestUtils withValidEInvoiceItemsData() {

        this.eInvoiceItemsDataGM = Arrays.asList(
            DObSalesInvoiceItemsEInvoiceDataGeneralModelMockUtils
                .buildGeneralModel()
                .itemUserCode("AnyItemCode")
                .itemName("Item1")
                .itemType("GS1")
                .itemBrickCode("N/A")
                .itemBaseUnit("N/A")
                .quantity(BigDecimal.valueOf(5))
                .currencySoldIso("EGP")
                .amountSold(BigDecimal.valueOf(20))
                .currencyExchangeRate(BigDecimal.ONE)
                .salesTotal(BigDecimal.valueOf(100))
                .valueDifference(BigDecimal.ZERO)
                .totalTaxableFees(BigDecimal.ZERO)
                .itemsDiscount(BigDecimal.ZERO)
                .build(),
            DObSalesInvoiceItemsEInvoiceDataGeneralModelMockUtils
                .buildGeneralModel()
                .itemUserCode("AnyItemCode2")
                .itemName("Item2")
                .itemType("GS1")
                .itemBrickCode("N/A")
                .itemBaseUnit("N/A")
                .quantity(BigDecimal.valueOf(4.25))
                .currencySoldIso("USD")
                .amountSold(BigDecimal.valueOf(3))
                .currencyExchangeRate(BigDecimal.valueOf(15.67))
                .salesTotal(BigDecimal.valueOf(199.7925))
                .valueDifference(BigDecimal.ZERO)
                .totalTaxableFees(BigDecimal.ZERO)
                .itemsDiscount(BigDecimal.ZERO)
                .build()

        );
        return this;
    }

    public SalesInvoiceEInvoiceSubmitCommandTestUtils withValidEInvoiceTaxesData() {
        this.eInvoiceTaxesDataGM = Arrays.asList(
            IObSalesInvoiceTaxesGeneralModelMockUtils
                .buildGeneralModel()
                .taxCode("VAT")
                .taxAmount(BigDecimal.valueOf(0.14))
                .build(),
            IObSalesInvoiceTaxesGeneralModelMockUtils
                .buildGeneralModel()
                .taxCode("VAT2")
                .taxAmount(BigDecimal.valueOf(0.025))
                .build()

        );
        return this;
    }

    public void verifyThatEInvoiceDataSerializedSuccessfully() throws Exception {
        init();
        command.executeCommand(userCode);

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(taxAuthorityClient).submit(argumentCaptor.capture(), Mockito.eq(DObSalesInvoiceEInvoiceData.class));
        String eInvoiceJSON = argumentCaptor.getValue();

        JsonParser parser = new JsonParser();
        JsonElement actualJSON = parser.parse(eInvoiceJSON);

        JsonElement expectedJSON = parser.parse(
            "{\"issuer\":{\"id\":\"N/A\",\"name\":\"Al Madina\",\"type\":\"B\",\"address\":{\"branchId\":\"\",\"country\":\"Egypt\",\"governate\":\"Cairo\",\"regionCity\":\"N/A\",\"street\":\"N/A\",\"buildingNumber\":\"N/A\",\"postalCode\":\"14523\"}},\"receiver\":{\"id\":\"N/A\",\"name\":\"Al Ahram\",\"type\":\"N/A\",\"address\":{\"country\":\"Egypt\",\"governate\":\"Cairo\",\"regionCity\":\"El Sheikh Zayed\",\"street\":\"N/A\",\"buildingNumber\":\"N/A\",\"postalCode\":\"12596\"}},\"documentType\":\"I\",\"documentTypeVersion\":\"1.0\",\"dateTimeIssued\":\"2021-09-21T16:27:33.000+02:00\",\"taxpayerActivityCode\":\"N/A\",\"internalId\":\"N/A\",\"invoiceLines\":[{\"description\":\"Item1\",\"itemType\":\"GS1\",\"itemCode\":\"N/A\",\"unitType\":\"N/A\",\"quantity\":5,\"unitValue\":{\"currencySold\":\"EGP\",\"amountEGP\":20,\"amountSold\":20,\"currencyExchangeRate\":1},\"salesTotal\":100,\"total\":100,\"valueDifference\":0,\"totalTaxableFees\":0,\"netTotal\":100,\"itemsDiscount\":0,\"internalCode\":\"AnyItemCode\"},{\"description\":\"Item2\",\"itemType\":\"GS1\",\"itemCode\":\"N/A\",\"unitType\":\"N/A\",\"quantity\":4.25,\"unitValue\":{\"currencySold\":\"USD\",\"amountEGP\":47.01,\"amountSold\":3,\"currencyExchangeRate\":15.67},\"salesTotal\":199.7925,\"total\":199.7925,\"valueDifference\":0,\"totalTaxableFees\":0,\"netTotal\":199.7925,\"itemsDiscount\":0,\"internalCode\":\"AnyItemCode2\"}],\"totalSalesAmount\":299.7925,\"totalDiscountAmount\":0,\"netAmount\":299.7925,\"extraDiscountAmount\":0,\"totalItemsDiscountAmount\":0,\"totalAmount\":349.25827,\"taxTotals\":[{\"taxType\":\"VAT\",\"amount\":0.14},{\"taxType\":\"VAT2\",\"amount\":0.025}],\"signatures\":[]}");

        Assert.assertEquals(expectedJSON,actualJSON);
    }

    private void init() {

        eInvoiceDataFactory = new DObSalesInvoiceEInvoiceDataFactory();

        setCoreEInvoiceData();
        setEInvoiceItemsData();
        setEInvoiceTaxesData();

        command = new EInvoiceSubmitCommand<>(DObSalesInvoiceEInvoiceData.class);
        command.setEInvoiceDataFactory(eInvoiceDataFactory);

        setTaxAuthorityClient();
    }

    private void setTaxAuthorityClient() {
        taxAuthorityClient = Mockito.mock(EInvoiceTaxAuthorityClient.class);

        Mockito.doNothing().when(taxAuthorityClient)
            .submit(Mockito.any(), Mockito.eq(DObSalesInvoiceEInvoiceData.class));
        command.setTaxAuthorityClient(taxAuthorityClient);
    }

    private void setCoreEInvoiceData() {
        DObSalesInvoiceEInvoiceDataGeneralModelRepMockUtils
            .mockFindOneByCode(userCode, eInvoiceDataGM);

        eInvoiceDataFactory.setEInvoiceDataGMRep(
            DObSalesInvoiceEInvoiceDataGeneralModelRepMockUtils.getRepository()
        );
    }

    private void setEInvoiceItemsData() {

        DObSalesInvoiceItemsEInvoiceDataGeneralModelRepMockUtils
            .mockFindByRefInstanceId(
                salesInvoiceId,
                eInvoiceItemsDataGM
            );

        eInvoiceDataFactory.setInvoiceItemsEInvoiceDataGMRep(
            DObSalesInvoiceItemsEInvoiceDataGeneralModelRepMockUtils.getRepository()
        );
    }

    private void setEInvoiceTaxesData() {

        IObSalesInvoiceTaxesGeneralModelRepMockUtils
            .mockFindByRefInstanceId(salesInvoiceId, eInvoiceTaxesDataGM);

        eInvoiceDataFactory.setInvoiceTaxesGMRep(
            IObSalesInvoiceTaxesGeneralModelRepMockUtils.getRepository()
        );
    }
}
