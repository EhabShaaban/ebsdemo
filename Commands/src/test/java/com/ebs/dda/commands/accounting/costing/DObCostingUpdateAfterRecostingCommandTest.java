package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.CostingExchangeRateService;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.jpa.accounting.costing.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.entities.VendorInvoiceUtils;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingItemMockUtils;
import com.ebs.entities.accounting.costing.CostingMockUtils;
import com.ebs.repositories.VendorInvoiceGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsRepUtils;
import com.ebs.repositories.accounting.costing.CostingItemsRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingRepUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DObCostingUpdateAfterRecostingCommandTest {
  private DObCostingUpdateAfterRecostingCommand command;
  @Mock private CostingExchangeRateService costingExchangeRateService;
  private CommandTestUtils commandTestUtils;
  @Mock private CreateCostingItemsSectionService createCostingItemsSectionService;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("ADMIN");
    command = new DObCostingUpdateAfterRecostingCommand();
    command.setExchangeRateService(costingExchangeRateService);
    command.setCostingDetailsGeneralModelRep(
        CostingDetailsGeneralModelRepMockUtils.getRepository());
    command.setCostingDetailsRep(CostingDetailsRepUtils.getRepository());
    command.setCostingRep(CostingRepUtils.getRepository());
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setCostingItemRep(CostingItemsRepMockUtils.getRepository());
    command.setVendorInvoiceGeneralModelRep(VendorInvoiceGeneralModelRepMockUtils.getRepository());
    command.setCreateCostingItemsSectionService(createCostingItemsSectionService);
  }

  @Test
  public void should_setActualCostingExchangeRateInCostingDetails() throws Exception {
    // Arrange
    String goodsReceiptCode = "20210000001";
    String purchaseOrderCode = "20210000001";
    String landedCostCode = "20210000001";
    String vendorInvoiceCode = "20210000001";
    String actualExchangeRate = "12.123";
    String costingUserCode = "20210000005";
    Long costingId = 1L;

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .userCode(vendorInvoiceCode)
            .purchaseOrderCode(purchaseOrderCode)
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    Mockito.when(costingExchangeRateService.calculate(goodsReceiptCode))
        .thenReturn(new BigDecimal(actualExchangeRate));

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .goodsReceiptUserCode(goodsReceiptCode)
            .userCode(costingUserCode)
            .id(costingId)
            .build();

    CostingDetailsGeneralModelRepMockUtils.findOneByGoodsReceiptCode(costingDetailsGeneralModel);

    IObCostingDetails costingDetails = CostingDetailsMockUtils.mockEntity().id(costingId).build();
    CostingDetailsRepUtils.findOneById(costingDetails);

    DObCosting costing = CostingMockUtils.mockEntity().userCode(costingUserCode).build();
    CostingRepUtils.findOneByUserCode(costing);

    DObCostingDocumentRecostingValueObject costingDocumentRecostingValueObject =
        new DObCostingDocumentRecostingValueObject(
            purchaseOrderCode, goodsReceiptCode, landedCostCode);

    // Act
    command.executeCommand(costingDocumentRecostingValueObject);
    // Assert
    Mockito.verify(costingExchangeRateService, Mockito.times(1)).calculate(goodsReceiptCode);
    Mockito.verify(CostingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByGoodsReceiptCode(goodsReceiptCode);
    Mockito.verify(CostingDetailsRepUtils.getRepository(), Mockito.times(1)).findOneById(costingId);
    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(costingUserCode);
    Mockito.verify(costingDetails, Mockito.times(1))
        .setCurrencyPriceActualTime(new BigDecimal(actualExchangeRate));
    Mockito.verify(costing, Mockito.times(1))
        .setHeaderDetail(IDObCosting.costingDetailsAttr, costingDetails);
    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1)).update(costing);
  }

  @Test
  public void should_setActualUnitPriceAndUnitLandedCostInCostingItems() throws Exception {
    // Arrange
    String goodsReceiptCode = "20210000001";
    String purchaseOrderCode = "20210000001";
    String landedCostCode = "20210000001";
    String vendorInvoiceCode = "20210000001";
    String currencyPrice = "12.123";
    String costingUserCode = "20210000005";
    Long costingId = 1L;

    List<IObCostingItem> costingItems = new ArrayList<>();
    costingItems.add(CostingItemMockUtils.mockEntity().refInstanceId(costingId).build());
    costingItems.add(CostingItemMockUtils.mockEntity().refInstanceId(costingId).build());
    CostingItemsRepMockUtils.findByRefInstanceId(costingItems);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .userCode(vendorInvoiceCode)
            .purchaseOrderCode(purchaseOrderCode)
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    Mockito.when(costingExchangeRateService.calculate(goodsReceiptCode))
        .thenReturn(new BigDecimal(currencyPrice));

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .goodsReceiptUserCode(goodsReceiptCode)
            .userCode(costingUserCode)
            .id(costingId)
            .build();

    CostingDetailsGeneralModelRepMockUtils.findOneByGoodsReceiptCode(costingDetailsGeneralModel);

    IObCostingDetails costingDetails =
        CostingDetailsMockUtils.mockEntity()
            .id(costingId)
            .currencyPriceActualTime(new BigDecimal(currencyPrice))
            .build();
    CostingDetailsRepUtils.findOneById(costingDetails);

    DObCosting costing =
        CostingMockUtils.mockEntity().id(costingId).userCode(costingUserCode).build();
    CostingRepUtils.findOneByUserCode(costing);

    IObCostingItemsCreationValueObject costingItemsCreationValueObject =
        new IObCostingItemsCreationValueObject(
            vendorInvoiceCode, goodsReceiptCode, landedCostCode, new BigDecimal(currencyPrice),true);

    DObCostingDocumentRecostingValueObject costingDocumentRecostingValueObject =
        new DObCostingDocumentRecostingValueObject(
            purchaseOrderCode, goodsReceiptCode, landedCostCode);

    // Act
    command.executeCommand(costingDocumentRecostingValueObject);
    // Assert

    Mockito.verify(createCostingItemsSectionService, Mockito.times(costingItems.size()))
        .setCostingItemCostFields(
            Mockito.any(IObCostingItem.class),
            Mockito.any(IObCostingItemsCreationValueObject.class));
    Mockito.verify(costing, Mockito.times(1))
        .setLinesDetails(IDObCosting.accountingDocumentItemsAttr, costingItems);
    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1)).update(costing);
  }

  @After
  public void tearDown() throws Exception {
    Mockito.reset(costingExchangeRateService);
    CostingDetailsGeneralModelRepMockUtils.resetRepository();
    CostingDetailsRepUtils.resetRepository();
    CostingRepUtils.resetRepository();
  }
}
