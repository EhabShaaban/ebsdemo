package com.ebs.dda.commands.accounting.notesreceivable.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.commands.accounting.notesreceivables.DObJournalEntryCreateNotesReceivableCommand;
import com.ebs.dda.jpa.accounting.journalentry.*;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.utils.DocumentObjectCodeGeneratorMockUtils;
import com.ebs.dda.utils.UserAccountSecurityServiceTestUtils;
import com.ebs.entities.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModelMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModelMockUtils;
import com.ebs.repositories.accounting.monetarynotes.DObMonetaryNotesJournalEntryRepMockUtils;
import com.ebs.repositories.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModelRepMockUtils;
import org.joda.time.format.DateTimeFormat;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class DObJournalEntryCreateNotesReceivableCommandTestUtils {

  private static final Long NOTES_RECEIVABLE_ID = 1L;
  private static final String NOTES_RECEIVABLE_CODE = "2021000001";
  private static final Long NOTES_RECEIVABLE_COMPANY_ID = 1L;
  private static final Long NOTES_RECEIVABLE_BUSINESS_UNIT_ID = 1L;
  private static final Long NOTES_RECEIVABLE_FISCAL_PERIOD_ID = 1L;
  private static final BigDecimal TOTAL_NOTES_RECEIVABLE_AMOUNT = BigDecimal.valueOf(25000);
  private static final Long NOTES_RECEIVABLE_COMPANY_CURRENCY_ID = 1L;
  private static final Long NOTES_RECEIVABLE_CURRENCY_ID = 1L;
  private static final Long NOTES_RECEIVABLE_EXCHANGE_RATE_ID = 36L;
  private static final BigDecimal NOTES_RECEIVABLE_CURRENCY_PRICE = BigDecimal.valueOf(1);
  private static final String JOURNAL_ENTRY_USER_CODE = "2021000001";

  private static final Long LOCAL_SO_CUSTOMER_ID = 45L;
  private static final BigDecimal LOCAL_SO_AMOUNT_TO_COLLECT = BigDecimal.valueOf(5000.102487);

  private static final Long LOCAL_SI_CUSTOMER_ID = 47L;
  private static final BigDecimal LOCAL_SI_AMOUNT_TO_COLLECT = BigDecimal.valueOf(6999.897513);

  private static final BigDecimal CREDIT_ACC_DETAILS_NR_AMOUNT_TO_COLLECT = BigDecimal.valueOf(13000);
  private static final Long CREDIT_ACC_DETAILS_NR_ID = 21L;

  private static final Long LOCAL_SO_CUSTOMERS_GL_ACCOUNT_ID = 11L;
  private static final Long LOCAL_SI_CUSTOMERS_GL_ACCOUNT_ID = 31L;
  private static final Long NOTES_RECEIVABLES_GL_ACCOUNT_ID = 26L;

  private DObNotesReceivableActivateValueObject valueObject;
  private DObJournalEntryCreateNotesReceivableCommand command;
  private Map<String, IObNotesReceivableAccountingDetailsGeneralModel> accountingDetailsMap;

  public DObJournalEntryCreateNotesReceivableCommandTestUtils() {
    valueObject = new DObNotesReceivableActivateValueObject();
    command = new DObJournalEntryCreateNotesReceivableCommand();
    accountingDetailsMap = new HashMap<>();
    DObMonetaryNotesJournalEntryRepMockUtils.reset();
  }

  public DObJournalEntryCreateNotesReceivableCommandTestUtils notesReceivableCode(String s) {
    valueObject.setNotesReceivableCode("2021000001");
    return this;
  }

  public DObJournalEntryCreateNotesReceivableCommandTestUtils journalEntryDate(String s) {
    valueObject.setJournalEntryDate("08-Jul-2021 04:30 PM");
    return this;
  }

  public DObJournalEntryCreateNotesReceivableCommandTestUtils withOneSIRefDoc() {
    accountingDetailsMap.put("CREDIT_SI", mockCreditSICustomerAccDetailsItem());
    return this;
  }

  public DObJournalEntryCreateNotesReceivableCommandTestUtils withOneSORefDoc() {
    accountingDetailsMap.put("CREDIT_SO", mockCreditSOCustomerAccDetailsItem());
    return this;
  }

  public DObJournalEntryCreateNotesReceivableCommandTestUtils withOneNRRefDoc() {
    accountingDetailsMap.put("CREDIT_NR", mockCreditNRAccDetailsItem());
    return this;
  }

  public void assertThatJournalEntryCreatedSuccessfully() throws Exception {
    setupMocking();

    command.executeCommand(valueObject);

    DObNotesReceivableJournalEntry createdJE = verifyThatCreateIsCalled();
    assertJournalEntryValues(createdJE);
    assertJournalEntryItems(createdJE);
  }

  private void setupMocking() {
    setuoCodeGenerator();
    setupUserAccountSecurityService();
    setupJEPreprationGMRep(valueObject);

    command
      .setNotesReceivableJournalEntryRep(DObMonetaryNotesJournalEntryRepMockUtils.getRepository());

    setupAccountingDetailsGMRep(valueObject);
  }

  private void setuoCodeGenerator() {
    DocumentObjectCodeGeneratorMockUtils.mockGenerateCode(
      DObJournalEntry.class.getSimpleName(),
      JOURNAL_ENTRY_USER_CODE);
    command.setCodeGenerator(DocumentObjectCodeGeneratorMockUtils.getCodeGenerator());
  }

  private void setupUserAccountSecurityService() {
    IUserAccountSecurityService userAccountSecurityService = UserAccountSecurityServiceTestUtils.setup();
    command.setUserAccountSecurityService(userAccountSecurityService);
  }

  private void setupJEPreprationGMRep(DObNotesReceivableActivateValueObject valueObject) {

    DObNotesReceivableJournalEntryPreparationGeneralModel journalEntryPreparationGM =
      DObNotesReceivableJournalEntryPreparationGeneralModelMockUtils
        .mockGeneralModel()
        .id(NOTES_RECEIVABLE_ID)
        .code(NOTES_RECEIVABLE_CODE)
        .companyId(NOTES_RECEIVABLE_COMPANY_ID)
        .businessUnitId(NOTES_RECEIVABLE_BUSINESS_UNIT_ID)
        .fiscalPeriodId(NOTES_RECEIVABLE_FISCAL_PERIOD_ID)
        .companyCurrencyId(NOTES_RECEIVABLE_COMPANY_CURRENCY_ID)
        .documentCurrencyId(NOTES_RECEIVABLE_CURRENCY_ID)
        .exchangeRateId(NOTES_RECEIVABLE_EXCHANGE_RATE_ID)
        .currencyPrice(NOTES_RECEIVABLE_CURRENCY_PRICE)
        .build();

    DObNotesReceivableJournalEntryPreparationGeneralModelRepMockUtils
      .mockFindOneByCode(journalEntryPreparationGM, NOTES_RECEIVABLE_CODE);

    command.setJournalEntryPreparationGMRep(
      DObNotesReceivableJournalEntryPreparationGeneralModelRepMockUtils.getRepository());
  }

  private void setupAccountingDetailsGMRep(DObNotesReceivableActivateValueObject valueObject) {

    accountingDetailsMap.put("DEBIT_NR", mockDebitNRAccDetailsItem());

    IObNotesReceivableAccountingDetailsGeneralModelRepMockUtils
      .mockFindByDocumentCodeOrderByAccountingEntryDesc(
        accountingDetailsMap.values().stream().collect(Collectors.toList()),
        NOTES_RECEIVABLE_CODE
      );

    command.setAccountingDetailsGMRep(
      IObNotesReceivableAccountingDetailsGeneralModelRepMockUtils.getRepository());
  }

  private IObNotesReceivableAccountingDetailsGeneralModel mockDebitNRAccDetailsItem() {
    IObNotesReceivableAccountingDetailsGeneralModel accDetails =
      IObNotesReceivableAccountingDetailsGeneralModelMockUtils
        .mockGeneralModel()
        .subLedger(SubLedgers.Notes_Receivables.name())
        .accountId(NOTES_RECEIVABLES_GL_ACCOUNT_ID)
        .subAccountId(NOTES_RECEIVABLE_ID)
        .amount(TOTAL_NOTES_RECEIVABLE_AMOUNT)
        .accountingEntry(AccountingEntry.DEBIT.name())
        .build();
    return accDetails;
  }

  private IObNotesReceivableAccountingDetailsGeneralModel mockCreditSOCustomerAccDetailsItem() {
    IObNotesReceivableAccountingDetailsGeneralModel accDetails =
      IObNotesReceivableAccountingDetailsGeneralModelMockUtils
        .mockGeneralModel()
        .subLedger(SubLedgers.Local_Customer.name())
        .accountId(LOCAL_SO_CUSTOMERS_GL_ACCOUNT_ID)
        .subAccountId(LOCAL_SO_CUSTOMER_ID)
        .amount(LOCAL_SO_AMOUNT_TO_COLLECT)
        .accountingEntry(AccountingEntry.CREDIT.name())
        .build();
    return accDetails;
  }

  private IObNotesReceivableAccountingDetailsGeneralModel mockCreditSICustomerAccDetailsItem() {
    IObNotesReceivableAccountingDetailsGeneralModel accDetails =
      IObNotesReceivableAccountingDetailsGeneralModelMockUtils
        .mockGeneralModel()
        .subLedger(SubLedgers.Local_Customer.name())
        .accountId(LOCAL_SI_CUSTOMERS_GL_ACCOUNT_ID)
        .subAccountId(LOCAL_SI_CUSTOMER_ID)
        .amount(LOCAL_SI_AMOUNT_TO_COLLECT)
        .accountingEntry(AccountingEntry.CREDIT.name())
        .build();
    return accDetails;
  }

  private IObNotesReceivableAccountingDetailsGeneralModel mockCreditNRAccDetailsItem() {
    IObNotesReceivableAccountingDetailsGeneralModel accDetails =
      IObNotesReceivableAccountingDetailsGeneralModelMockUtils
        .mockGeneralModel()
        .subLedger(SubLedgers.Notes_Receivables.name())
        .accountId(NOTES_RECEIVABLES_GL_ACCOUNT_ID)
        .subAccountId(CREDIT_ACC_DETAILS_NR_ID)
        .amount(CREDIT_ACC_DETAILS_NR_AMOUNT_TO_COLLECT)
        .accountingEntry(AccountingEntry.CREDIT.name())
        .build();
    return accDetails;
  }

  private DObNotesReceivableJournalEntry verifyThatCreateIsCalled() throws Exception {
    ArgumentCaptor<DObNotesReceivableJournalEntry> argumentCaptor =
      ArgumentCaptor.forClass(DObNotesReceivableJournalEntry.class);

    Mockito.verify(DObMonetaryNotesJournalEntryRepMockUtils.getRepository())
      .create(argumentCaptor.capture());

    return argumentCaptor.getValue();
  }

  private void assertJournalEntryValues(DObNotesReceivableJournalEntry createdJE) {

    Assert.assertEquals(Long.valueOf(1L), createdJE.getDocumentReferenceId());
    Assert.assertEquals(JOURNAL_ENTRY_USER_CODE, createdJE.getUserCode());
    Assert.assertEquals(NOTES_RECEIVABLE_COMPANY_ID, createdJE.getCompanyId());
    Assert.assertEquals(NOTES_RECEIVABLE_BUSINESS_UNIT_ID, createdJE.getPurchaseUnitId());
    Assert.assertEquals(NOTES_RECEIVABLE_FISCAL_PERIOD_ID, createdJE.getFiscalPeriodId());
    Assert.assertTrue(createdJE.getCurrentStates().contains(BasicStateMachine.ACTIVE_STATE));

    Assert.assertEquals(DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a")
      .parseDateTime(valueObject.getJournalEntryDate()), createdJE.getDueDate());

    Assert.assertEquals(
      accountingDetailsMap.size(),
      createdJE.getAllLines(IDObJournalEntry.journalItemAttr).size()
    );

    Assert.assertNotNull(createdJE.getCreationDate());
    Assert.assertNotNull(createdJE.getCreationInfo());

    Assert.assertNotNull(createdJE.getModifiedDate());
    Assert.assertNotNull(createdJE.getModificationInfo());
  }

  private void assertJournalEntryItems(DObNotesReceivableJournalEntry createdJE) {

    List<IObJournalEntryItem> journalEntryItems = createdJE
      .getAllLines(IDObJournalEntry.journalItemAttr)
      .stream()
      .collect(Collectors.toList());
    journalEntryItems.forEach(item -> {
      if (isItemDebitNR(item)) {
        assertDebitJEItem((IObJournalEntryItemNotesReceivableSubAccount) item);
      }
      if (isItemCreditNR(item)) {
        assertNRCreditJEItem((IObJournalEntryItemNotesReceivableSubAccount) item);
      }
      if (isCreditSOJEItem(item)){
        assertSOCustomerCreditJEItem((IObJournalEntryItemCustomerSubAccount) item);
      }
      if (isCreditSIJEItem(item)){
        assertSICustomerCreditJEItem((IObJournalEntryItemCustomerSubAccount) item);
      }
    });
  }

  private boolean isCreditSIJEItem(IObJournalEntryItem item) {
    return item instanceof IObJournalEntryItemCustomerSubAccount
      && item.getAccountId().equals(LOCAL_SI_CUSTOMERS_GL_ACCOUNT_ID);
  }

  private boolean isItemCreditNR(IObJournalEntryItem item) {
    return item instanceof IObJournalEntryItemNotesReceivableSubAccount
      && isCreditJEItem(item);
  }

  private boolean isItemDebitNR(IObJournalEntryItem item) {
    return item instanceof IObJournalEntryItemNotesReceivableSubAccount
      && !isCreditJEItem(item);
  }

  private boolean isCreditSOJEItem(IObJournalEntryItem item) {
    return item instanceof IObJournalEntryItemCustomerSubAccount
      && item.getAccountId().equals(LOCAL_SO_CUSTOMERS_GL_ACCOUNT_ID);
  }

  private boolean isCreditJEItem(IObJournalEntryItem item){
    return item.getDebit().compareTo(BigDecimal.ZERO) == 0;
  }

  private void assertDebitJEItem(IObJournalEntryItemNotesReceivableSubAccount debitItem) {
    Assert.assertEquals(NOTES_RECEIVABLES_GL_ACCOUNT_ID, debitItem.getAccountId());
    Assert.assertEquals(NOTES_RECEIVABLE_ID, debitItem.getSubAccountId());

    Assert.assertEquals(TOTAL_NOTES_RECEIVABLE_AMOUNT.compareTo(debitItem.getDebit()),0);
    Assert.assertEquals(BigDecimal.ZERO.compareTo(debitItem.getCredit()), 0);

    Assert.assertEquals(NOTES_RECEIVABLE_COMPANY_CURRENCY_ID, debitItem.getCompanyCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_ID, debitItem.getDocumentCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_EXCHANGE_RATE_ID, debitItem.getCompanyExchangeRateId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_PRICE.compareTo(debitItem.getCompanyCurrencyPrice()),0);

    Assert.assertNotNull(debitItem.getCreationDate());
    Assert.assertNotNull(debitItem.getCreationInfo());

    Assert.assertNotNull(debitItem.getModifiedDate());
    Assert.assertNotNull(debitItem.getModificationInfo());
  }

  private void assertSOCustomerCreditJEItem(IObJournalEntryItemCustomerSubAccount creditItem) {
    Assert.assertEquals(SubLedgers.Local_Customer.name(), creditItem.getType());

    Assert.assertEquals(LOCAL_SO_CUSTOMERS_GL_ACCOUNT_ID, creditItem.getAccountId());
    Assert.assertEquals(LOCAL_SO_CUSTOMER_ID, creditItem.getSubAccountId());

    Assert.assertEquals(LOCAL_SO_AMOUNT_TO_COLLECT.compareTo(creditItem.getCredit()),0);
    Assert.assertEquals(BigDecimal.ZERO.compareTo(creditItem.getDebit()), 0);

    Assert.assertEquals(NOTES_RECEIVABLE_COMPANY_CURRENCY_ID, creditItem.getCompanyCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_ID, creditItem.getDocumentCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_EXCHANGE_RATE_ID, creditItem.getCompanyExchangeRateId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_PRICE.compareTo(creditItem.getCompanyCurrencyPrice()),0);

    Assert.assertNotNull(creditItem.getCreationDate());
    Assert.assertNotNull(creditItem.getCreationInfo());

    Assert.assertNotNull(creditItem.getModifiedDate());
    Assert.assertNotNull(creditItem.getModificationInfo());
  }

  private void assertSICustomerCreditJEItem(IObJournalEntryItemCustomerSubAccount creditItem) {
    Assert.assertEquals(SubLedgers.Local_Customer.name(), creditItem.getType());

    Assert.assertEquals(LOCAL_SI_CUSTOMERS_GL_ACCOUNT_ID, creditItem.getAccountId());
    Assert.assertEquals(LOCAL_SI_CUSTOMER_ID, creditItem.getSubAccountId());

    Assert.assertEquals(LOCAL_SI_AMOUNT_TO_COLLECT.compareTo(creditItem.getCredit()),0);
    Assert.assertEquals(BigDecimal.ZERO.compareTo(creditItem.getDebit()), 0);

    Assert.assertEquals(NOTES_RECEIVABLE_COMPANY_CURRENCY_ID, creditItem.getCompanyCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_ID, creditItem.getDocumentCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_EXCHANGE_RATE_ID, creditItem.getCompanyExchangeRateId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_PRICE.compareTo(creditItem.getCompanyCurrencyPrice()),0);

    Assert.assertNotNull(creditItem.getCreationDate());
    Assert.assertNotNull(creditItem.getCreationInfo());

    Assert.assertNotNull(creditItem.getModifiedDate());
    Assert.assertNotNull(creditItem.getModificationInfo());
  }

  private void assertNRCreditJEItem(IObJournalEntryItemNotesReceivableSubAccount creditItem) {
    Assert.assertEquals(NOTES_RECEIVABLES_GL_ACCOUNT_ID, creditItem.getAccountId());
    Assert.assertEquals(CREDIT_ACC_DETAILS_NR_ID, creditItem.getSubAccountId());

    Assert.assertEquals(CREDIT_ACC_DETAILS_NR_AMOUNT_TO_COLLECT.compareTo(creditItem.getCredit()),0);
    Assert.assertEquals(BigDecimal.ZERO.compareTo(creditItem.getDebit()), 0);

    Assert.assertEquals(NOTES_RECEIVABLE_COMPANY_CURRENCY_ID, creditItem.getCompanyCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_ID, creditItem.getDocumentCurrencyId());
    Assert.assertEquals(NOTES_RECEIVABLE_EXCHANGE_RATE_ID, creditItem.getCompanyExchangeRateId());
    Assert.assertEquals(NOTES_RECEIVABLE_CURRENCY_PRICE.compareTo(creditItem.getCompanyCurrencyPrice()),0);

    Assert.assertNotNull(creditItem.getCreationDate());
    Assert.assertNotNull(creditItem.getCreationInfo());

    Assert.assertNotNull(creditItem.getModifiedDate());
    Assert.assertNotNull(creditItem.getModificationInfo());
  }

}
