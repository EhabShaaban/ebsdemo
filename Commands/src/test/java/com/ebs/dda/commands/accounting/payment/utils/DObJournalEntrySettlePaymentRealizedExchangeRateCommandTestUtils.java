package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.DObJournalEntrySettlePaymentRealizedExchangeRateCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.entities.accounting.journalentry.DObPaymentRequestJournalEntryUtils;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.repositories.LObGlobalAccountConfigGeneralModelRepUtils;
import com.ebs.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRepUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRepUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

public class DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils {

    private final DObJournalEntryCreateValueObject valueObject;
    private final DObJournalEntrySettlePaymentRealizedExchangeRateCommand command;

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils() {
        PaymentUtils.resetValue();
        DObPaymentRequestJournalEntryUtils.resetPaymentRequestJournalEntry();
        DObPaymentRequestJournalEntryRepUtils.resetRepository();
        valueObject = new DObJournalEntryCreateValueObject();
        command = new DObJournalEntrySettlePaymentRealizedExchangeRateCommand();
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withPaymentCode(String paymentCode) {
        valueObject.setUserCode(paymentCode);
        PaymentUtils.withPaymentCode(paymentCode);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withJournalEntryCode(String journalEntryCode) {
        valueObject.setJournalEntryCode(journalEntryCode);
        DObPaymentRequestJournalEntryUtils.withUserCode(journalEntryCode);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withPaymentAmount(BigDecimal paymentAmount) {
        PaymentUtils.withAmount(paymentAmount);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withVendorInvoiceCurrencyPrice(BigDecimal vendorInvoiceCurrencyPrice) {
        PaymentUtils.withDueDocumentCurrencyPrice(vendorInvoiceCurrencyPrice);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withCurrentCurrencyPrice(BigDecimal currentCurrencyPrice) {
        PaymentUtils.withCurrencyPrice(currentCurrencyPrice);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withLocalCurrencyId(Long localCurrencyId) {
        PaymentUtils.withLocalCurrencyId(localCurrencyId);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withVendorSubLedger(String debitSubLedger) {
        PaymentUtils.withDebitSubLedger(debitSubLedger);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withGlVendorAccountId(Long glDebitAccountId) {
        PaymentUtils.withGlDebitAccountId(glDebitAccountId);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils withVendorId(Long businessPartnerId) {
        PaymentUtils.withBusinessPartnerId(businessPartnerId);
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils whenExecute() throws Exception {
        prepareRealizedExchangeRateDifferenceGeneralModelRep();
        prepareUserAccountSecurityService();
        prepareLObGlobalAccountConfigGeneralModelRep();
        preparePaymentRequestJournalEntryRepUtils();
        command.executeCommand(valueObject);
        return this;
    }

    private void prepareRealizedExchangeRateDifferenceGeneralModelRep(){
        DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel realizedExchangeRateDifferenceGeneralModel = PaymentUtils.buildPaymentCalculateRealizedExchangeRateDifferenceGeneralModel();
        DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRepUtils.findOneByPaymentCode(realizedExchangeRateDifferenceGeneralModel);
        command.setExchangeRateDifferenceGeneralModelRep(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRepUtils.getRepository());
    }
    private void prepareUserAccountSecurityService() {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }
    private void prepareLObGlobalAccountConfigGeneralModelRep() {
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);
        command.setAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
    }
    private void preparePaymentRequestJournalEntryRepUtils() {
        DObPaymentRequestJournalEntryUtils.buildPaymentRequestJournalEntry();
        DObPaymentRequestJournalEntry paymentRequestJournalEntry = DObPaymentRequestJournalEntryUtils.getPaymentRequestJournalEntry();
        DObPaymentRequestJournalEntryRepUtils.findOneByUserCode(paymentRequestJournalEntry);
        command.setJournalEntryRep(DObPaymentRequestJournalEntryRepUtils.getRepository());
    }
    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils assertThatJournalEntriesIsUpdated() throws Exception {
        Mockito.verify(DObPaymentRequestJournalEntryRepUtils.getRepository(), Mockito.times(1)).update(DObPaymentRequestJournalEntryUtils.getPaymentRequestJournalEntry());
        return this;
    }

    public DObJournalEntrySettlePaymentRealizedExchangeRateCommandTestUtils assertThatJournalEntryIsRecorded(Long glAccountId, BigDecimal value, String accountingEntry) {
        ArgumentCaptor<IObJournalEntryItem> journalEntryArgumentCaptor = ArgumentCaptor.forClass(IObJournalEntryItem.class);
        Mockito.verify(DObPaymentRequestJournalEntryUtils.getPaymentRequestJournalEntry(), Mockito.times(2)).addLineDetail(Mockito.eq(IDObJournalEntry.journalItemAttr), journalEntryArgumentCaptor.capture());
        List<IObJournalEntryItem> journalEntries = journalEntryArgumentCaptor.getAllValues();
        IObJournalEntryItem actualJournalItem = journalEntries.stream()
                .filter(item -> glAccountId.equals(item.getAccountId())
                        && (
                        (accountingEntry.equals(AccountingEntry.CREDIT.name())
                                && value.compareTo(item.getCredit()) == 0)
                                || (accountingEntry.equals(AccountingEntry.DEBIT.name())
                                && value.compareTo(item.getDebit()) == 0))
                )
                .findAny()
                .orElse(null);
        Assert.assertNotNull(actualJournalItem);
        return this;
    }

    public void assertThatNoJournalEntryIsAdded() {
        ArgumentCaptor<IObJournalEntryItem> journalEntryArgumentCaptor = ArgumentCaptor.forClass(IObJournalEntryItem.class);
        Mockito.verify(DObPaymentRequestJournalEntryUtils.getPaymentRequestJournalEntry(), Mockito.never()).addLineDetail(Mockito.eq(IDObJournalEntry.journalItemAttr), journalEntryArgumentCaptor.capture());
    }
}
