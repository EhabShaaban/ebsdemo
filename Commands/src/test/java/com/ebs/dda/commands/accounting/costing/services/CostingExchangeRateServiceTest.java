package com.ebs.dda.commands.accounting.costing.services;

import com.ebs.dda.commands.accounting.costing.CostingExchangeRateStrategyFactory;
import com.ebs.dda.commands.accounting.costing.ICalculateCostingExchangeRateStrategy;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.entities.GoodsReceiptMockUtils;
import com.ebs.entities.VendorInvoiceSummaryUtils;
import com.ebs.entities.VendorInvoiceUtils;
import com.ebs.repositories.GoodsReceiptGeneralModelRepUtils;
import com.ebs.repositories.VendorInvoiceGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class CostingExchangeRateServiceTest {
  private CostingExchangeRateService service;
  @Mock private CostingExchangeRateStrategyFactory factory;
  @Mock private ICalculateCostingExchangeRateStrategy strategy;

  @Before
  public void setUp() throws Exception {
    service =
        new CostingExchangeRateService(
            GoodsReceiptGeneralModelRepUtils.getRepository(),
            VendorInvoiceGeneralModelRepMockUtils.getRepository(),
            VendorInvoiceSummaryRepUtils.getRepository(),
            factory);
  }

  @Test
  public void should_CalculateCostingExchangeRateForGoodsReceiptCode() {
    // Arrange
    String goodsReceiptCode = "20210000001";
    String purchaseOrderCode = "20210000002";
    long vendorInvoiceId = 2L;
    String actualCost = "12.123";
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .userCode(goodsReceiptCode)
            .refDocumentCode(purchaseOrderCode)
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .purchaseOrderCode(purchaseOrderCode)
            .id(vendorInvoiceId)
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity().refInstanceId(vendorInvoiceId).build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    Mockito.when(factory.getStrategy(vendorInvoiceSummary)).thenReturn(strategy);
    Mockito.when(strategy.calculate(goodsReceiptGeneralModel))
        .thenReturn(new BigDecimal(actualCost));

    // Act
    BigDecimal actualValue = service.calculate(goodsReceiptCode);

    // Assert
    Mockito.verify(GoodsReceiptGeneralModelRepUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(goodsReceiptCode);
    Mockito.verify(VendorInvoiceGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByPurchaseOrderCode(purchaseOrderCode);
    Mockito.verify(VendorInvoiceSummaryRepUtils.getRepository(), Mockito.times(1))
        .findOneByRefInstanceId(vendorInvoiceId);
    Assert.assertEquals(0, actualValue.compareTo(new BigDecimal(actualCost)));
  }

  @After
  public void tearDown() {
    GoodsReceiptGeneralModelRepUtils.resetRepository();
    VendorInvoiceGeneralModelRepMockUtils.resetRepository();
    VendorInvoiceSummaryRepUtils.resetRepository();
    Mockito.reset(factory);
    Mockito.reset(strategy);
  }
}
