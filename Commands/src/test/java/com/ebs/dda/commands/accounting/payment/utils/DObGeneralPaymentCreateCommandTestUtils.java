package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dda.commands.accounting.paymentrequest.DObGeneralPaymentCreateCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRep;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRepUtils;
import org.junit.jupiter.api.Assertions;

public class DObGeneralPaymentCreateCommandTestUtils
				extends DObPaymentRequestCreateCommandTestUtils<DObGeneralPaymentCreateCommand> {

	public DObGeneralPaymentCreateCommandTestUtils() {
		super(DObGeneralPaymentCreateCommand.class);
	}

	@Override
	protected void preparePaymentTypeCommandDependencies(DObPaymentRequestCreateValueObject valueObject, DObGeneralPaymentCreateCommand command) {
		preparePaymentReferenceOrderRep(command);
	}
	private void preparePaymentReferenceOrderRep(DObGeneralPaymentCreateCommand command) {
		DObPaymentReferenceOrderGeneralModel paymentReferenceOrderGeneralModel = PaymentUtils
						.buildPaymentReferenceOrderGeneralModel();
		DObPaymentReferenceOrderGeneralModelRepUtils
						.findOneByUserCode(paymentReferenceOrderGeneralModel);
		DObPaymentReferenceOrderGeneralModelRep repository = DObPaymentReferenceOrderGeneralModelRepUtils
						.getRepository();
		command.setRefDocumentRep(repository);
	}
	public void assertThatReferenceDocumentId(Long referenceDocumentId) {
		IObPaymentRequestPurchaseOrderPaymentDetails paymentDetails = (IObPaymentRequestPurchaseOrderPaymentDetails) getCreatedObject().getHeaderDetail(IDObPaymentRequest.paymentdetailsAttr);
		Assertions.assertEquals(paymentDetails.getDueDocumentId(), referenceDocumentId);
	}
}
