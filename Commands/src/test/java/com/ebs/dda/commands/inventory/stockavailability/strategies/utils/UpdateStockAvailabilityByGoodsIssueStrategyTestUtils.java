package com.ebs.dda.commands.inventory.stockavailability.strategies.utils;

import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.commands.inventory.stockavailability.mocks.entities.IObGoodsIssueItemQuantityGeneralModelMock;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsIssueStrategy;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModelRep;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class UpdateStockAvailabilityByGoodsIssueStrategyTestUtils {

  private String userCode;
  private IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep;
  private UpdateStockAvailabilityService updateService;
  private UpdateStockAvailabilityByGoodsIssueStrategy strategy;
  private List<IObGoodsIssueItemQuantityGeneralModel> goodsIssueItems;

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils() {
    mockDependencies();
    initStrategy();
  }

  private void initStrategy() {
    strategy = new UpdateStockAvailabilityByGoodsIssueStrategy();
    strategy.setGoodsIssueItemQuantityGeneralModelRep(goodsIssueItemQuantityGeneralModelRep);
    strategy.setStockAvailabilityUpdateService(updateService);
  }

  private void mockDependencies() {
    goodsIssueItemQuantityGeneralModelRep =
        Mockito.mock(IObGoodsIssueItemQuantityGeneralModelRep.class);
    updateService = Mockito.mock(UpdateStockAvailabilityService.class);
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils withUserCode(String userCode) {
    this.userCode = userCode;
    return this;
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils whenUpdate() {
    mockData();
    strategy.updateStockAvailability(userCode);
    return this;
  }

  private void mockData() {
    Mockito.when(goodsIssueItemQuantityGeneralModelRep.findAllByUserCode(userCode))
        .thenReturn(goodsIssueItems);
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils withGoodsIssueItems(
      IObGoodsIssueItemQuantityGeneralModelMock... items) {
    goodsIssueItems = new ArrayList<>(Arrays.asList(items));
    return this;
  }

  public static IObGoodsIssueItemQuantityGeneralModelMock goodsIssueItem(
      String purchaseUnitCode,
      String companyCode,
      String plantCode,
      String storehouseCode,
      String itemCode,
      String unitOfMeasureCode,
      String userCode,
      BigDecimal quantity) {
    return new IObGoodsIssueItemQuantityGeneralModelMock(
        userCode,
        itemCode,
        plantCode,
        storehouseCode,
        companyCode,
        purchaseUnitCode,
        unitOfMeasureCode,
        quantity);
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils
      thenAssertRepositoryWasCalledWithUserCode(String userCode) {
    Mockito.verify(goodsIssueItemQuantityGeneralModelRep, Mockito.times(1))
        .findAllByUserCode(userCode);
    return this;
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils
      thenAssertUpdateServiceWasCalledWithItemsList() {
    ArgumentCaptor<IObGoodsIssueItemQuantityGeneralModel> updateResult =
        ArgumentCaptor.forClass(IObGoodsIssueItemQuantityGeneralModel.class);
    Mockito.verify(updateService, Mockito.times(goodsIssueItems.size()))
        .updateOrCreateStockAvailabilityForItem(updateResult.capture(), Mockito.any());
    assertGoodsIssueItemsAreEqual(goodsIssueItems, updateResult.getAllValues());
    return this;
  }

  public UpdateStockAvailabilityByGoodsIssueStrategyTestUtils
      thenAssertItemQuantitiesAreNegative() {
    ArgumentCaptor<BigDecimal> updateQuantityResult = ArgumentCaptor.forClass(BigDecimal.class);
    Mockito.verify(updateService, Mockito.times(goodsIssueItems.size()))
        .updateOrCreateStockAvailabilityForItem(Mockito.any(), updateQuantityResult.capture());
    Assert.assertFalse(
        updateQuantityResult.getAllValues().stream()
            .anyMatch(item -> item.compareTo(BigDecimal.ZERO) > 0));
    return this;
  }

  private void assertGoodsIssueItemsAreEqual(
      List<IObGoodsIssueItemQuantityGeneralModel> actual,
      List<IObGoodsIssueItemQuantityGeneralModel> expected) {
    Assert.assertEquals("Number of updated items doesn't match", expected.size(), actual.size());
    expected.stream()
        .forEach(
            item -> {
              Assert.assertNotNull(
                  "Item doesn't exist",
                  actual.stream().filter(goodsIssueItemQuantitiesEqual(item)).findAny());
            });
  }

  private static Predicate<IObGoodsIssueItemQuantityGeneralModel> goodsIssueItemQuantitiesEqual(
      IObGoodsIssueItemQuantityGeneralModel item) {
    return p ->
        p.getUserCode().equals(item.getUserCode())
            && p.getStockType().equals(item.getStockType())
            && p.getCompanyCode().equals(item.getCompanyCode())
            && p.getPlantCode().equals(item.getPlantCode())
            && p.getItemCode().equals(item.getItemCode())
            && p.getPurchaseUnitCode().equals(item.getPurchaseUnitCode())
            && p.getUnitOfMeasureCode().equals(item.getUnitOfMeasureCode())
            && p.getQuantity().equals(item.getQuantity())
            && p.getStoreHouseCode().equals(item.getStoreHouseCode());
  }
}
