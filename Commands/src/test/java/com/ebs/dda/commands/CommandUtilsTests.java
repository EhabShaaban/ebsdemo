package com.ebs.dda.commands;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class CommandUtilsTests {

  @Test()
  public void shouldGetDivisionResultWithRoundingModeIfNonTerminatingDecimalExpansionRepeated() {

    //    Arrange
    BigDecimal firstNumber = BigDecimal.ONE;
    BigDecimal secondNumber = new BigDecimal("3");

    //    Act
    BigDecimal divideResult = CommandUtils.getDivideResult(firstNumber, secondNumber);

    //    Assert
    Assert.assertEquals(new BigDecimal("0.3333333333"), divideResult);
  }

  @Test()
  public void shouldGetDivisionResultWithRoundingModeIfNonTerminatingDecimalExpansionNonRepeated() {

    //    Arrange
    BigDecimal firstNumber = new BigDecimal("47.123456789");
    BigDecimal secondNumber = new BigDecimal("6.123456789");

    //    Act
    BigDecimal divideResult = CommandUtils.getDivideResult(firstNumber, secondNumber);

    //    Assert
    Assert.assertEquals(new BigDecimal("7.6955645173"), divideResult);
  }

  @Test()
  public void shouldGetDivisionResultWithoutRoundingModeIfResultIsTerminating() {

    //    Arrange
    BigDecimal firstNumber = new BigDecimal("1");
    BigDecimal secondNumber = new BigDecimal("64");

    //    Act
    BigDecimal divideResult = CommandUtils.getDivideResult(firstNumber, secondNumber);

    //    Assert
    Assert.assertEquals(new BigDecimal("0.015625"), divideResult);
  }
}
