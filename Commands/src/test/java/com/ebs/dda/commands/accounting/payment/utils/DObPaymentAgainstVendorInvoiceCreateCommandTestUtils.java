package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentAgainstVendorInvoiceCreateCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetails;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModelRepUtils;
import com.ebs.repositories.masterdata.vendor.MObVendorRepUtils;
import org.junit.jupiter.api.Assertions;

public class DObPaymentAgainstVendorInvoiceCreateCommandTestUtils extends
				DObPaymentRequestCreateCommandTestUtils<DObPaymentAgainstVendorInvoiceCreateCommand> {

	public DObPaymentAgainstVendorInvoiceCreateCommandTestUtils() {
		super(DObPaymentAgainstVendorInvoiceCreateCommand.class);
	}

	@Override
	protected void preparePaymentTypeCommandDependencies(
					DObPaymentRequestCreateValueObject valueObject,
					DObPaymentAgainstVendorInvoiceCreateCommand command) {
		preparePaymentReferenceOrderRep(command);
		prepareVendorRep(valueObject, command);
	}

	private void prepareVendorRep(DObPaymentRequestCreateValueObject valueObject,
					DObPaymentAgainstVendorInvoiceCreateCommand command) {
		MObVendor vendor = VendorMockUtils.buildMObVendor(valueObject.getBusinessPartnerCode(), 1L);
		MObVendorRepUtils.findOneByUserCode(vendor);
		MObVendorRep repository = MObVendorRepUtils.getRepository();
		command.setVendorRep(repository);
	}

	private void preparePaymentReferenceOrderRep(
					DObPaymentAgainstVendorInvoiceCreateCommand command) {
		DObPaymentReferenceVendorInvoiceGeneralModel paymentReferenceVendorInvoiceGeneralModel = PaymentUtils
						.buildPaymentReferenceVendorInvoiceGeneralModel();
		DObPaymentReferenceVendorInvoiceGeneralModelRepUtils
						.findOneByUserCode(paymentReferenceVendorInvoiceGeneralModel);
		DObPaymentReferenceVendorInvoiceGeneralModelRep repository = DObPaymentReferenceVendorInvoiceGeneralModelRepUtils
						.getRepository();
		command.setRefDocumentRep(repository);
	}

	public void assertThatReferenceDocumentId(Long referenceDocumentId) {
		IObPaymentRequestInvoicePaymentDetails invoicePaymentDetails = (IObPaymentRequestInvoicePaymentDetails) getCreatedObject().getHeaderDetail(IDObPaymentRequest.paymentdetailsAttr);
		Assertions.assertEquals(invoicePaymentDetails.getDueDocumentId(), referenceDocumentId);
	}
}
