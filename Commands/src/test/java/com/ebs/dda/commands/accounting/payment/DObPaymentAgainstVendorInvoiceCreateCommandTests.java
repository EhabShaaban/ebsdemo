package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObPaymentAgainstVendorInvoiceCreateCommandTestUtils;
import org.junit.Before;
import org.junit.Test;

public class DObPaymentAgainstVendorInvoiceCreateCommandTests {

	private DObPaymentAgainstVendorInvoiceCreateCommandTestUtils utils;

	@Before
	public void setup() {
		utils = new DObPaymentAgainstVendorInvoiceCreateCommandTestUtils();
	}

	@Test
	public void testCreateBankPaymentAgainstVendorInvoice() throws Exception {
		utils.withPaymentForm("BANK").withPaymentType("VENDOR").withBusinessUnit("0002")
						.withBusinessPartner("000002")
						.withReferenceDocument("INVOICE", "2021000003", 5L, 1L, 7L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(5L).assertThatPaymentCurrencyId(1L);
		utils.assertThatReferenceDocumentId(7L);
	}

	@Test
	public void testCreateCashPaymentAgainstVendorInvoice() throws Exception {
		utils.withPaymentForm("CASH").withPaymentType("VENDOR").withBusinessUnit("0002")
						.withBusinessPartner("000002")
						.withReferenceDocument("INVOICE", "2021000003", 3L, 2L, 5L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(3L).assertThatPaymentCurrencyId(2L);
		utils.assertThatReferenceDocumentId(5L);
	}
}
