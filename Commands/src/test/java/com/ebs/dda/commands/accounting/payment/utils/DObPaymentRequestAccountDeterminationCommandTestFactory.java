package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.paymentrequest.*;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObLocalPurchaseOrder;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.masterdata.chartofaccount.HObGLAccountUtils;
import com.ebs.entities.masterdata.vendor.IObVendorAccountingInfoGeneralModelMockUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.entities.order.purchaseorder.DObLocalPurchaseOrderMockUtils;
import com.ebs.repositories.LObGlobalAccountConfigGeneralModelRepUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentRequestRepUtils;
import com.ebs.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRepUtils;
import com.ebs.repositories.masterdata.chartofaccount.HObChartOfAccountRepUtils;
import com.ebs.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRepUtils;
import com.ebs.repositories.masterdata.vendor.MObVendorRepUtils;
import com.ebs.repositories.order.purchaseorder.DObPurchaseOrderRepMockUtils;

public class DObPaymentRequestAccountDeterminationCommandTestFactory {
    private static DObPaymentRequestAccountDeterminationCommand command;

    public static DObPaymentRequestAccountDeterminationCommand createCommandInstance(IObPaymentRequestPaymentDetailsGeneralModel paymentDetails, Long orderId, String accountCode) {
        String dueDocument = paymentDetails.getDueDocumentType();
        String paymentMethod = paymentDetails.getPaymentForm();
        String orderCode = paymentDetails.getDueDocumentCode();
        String vendorCode = paymentDetails.getBusinessPartnerCode();

        if (dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name()) && paymentMethod.equals(PaymentFormEnum.PaymentForm.BANK.name())) {
            command = new DObBankPaymentForPurchaseOrderAccountDeterminationCommand();
            prepareGeneralCommandDependencies();
            prepareBankPaymentForPurchaseOrderDependencies((DObBankPaymentForPurchaseOrderAccountDeterminationCommand) command, orderId, orderCode);
            return command;
        } else if (dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name()) && paymentMethod.equals(PaymentFormEnum.PaymentForm.CASH.name())) {
            command = new DObCashPaymentForPurchaseOrderAccountDeterminationCommand();
            prepareGeneralCommandDependencies();
            prepareCashPaymentForPurchaseOrderDependencies((DObCashPaymentForPurchaseOrderAccountDeterminationCommand) command, orderId, orderCode);
            return command;
        } else if (dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name()) && paymentMethod.equals(PaymentFormEnum.PaymentForm.BANK.name())) {
            command = new DObBankPaymentForVendorAccountDeterminationCommand();
            prepareGeneralCommandDependencies();
            prepareBankPaymentForVendorDependencies((DObBankPaymentForVendorAccountDeterminationCommand) command, vendorCode, accountCode);
            return command;
        } else if (dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name()) && paymentMethod.equals(PaymentFormEnum.PaymentForm.CASH.name())) {
            command = new DObCashPaymentForVendorAccountDeterminationCommand();
            prepareGeneralCommandDependencies();
            prepareCashPaymentForVendorDependencies((DObCashPaymentForVendorAccountDeterminationCommand) command, vendorCode, accountCode);
            return command;
        } else {
            throw new IllegalArgumentException("Invalid PaymentRequest Type");
        }
    }

    private static void prepareGeneralCommandDependencies() {
        preparePaymentRequest();
        preparePaymentDetails();
        prepareUserAccountSecurityService();
    }

    private static void preparePaymentRequest() {
        DObPaymentRequest paymentRequest = PaymentUtils.buildPaymentRequest();
        DObPaymentRequestRepUtils.findOneByUserCode(paymentRequest);
        command.setPaymentRequestRep(DObPaymentRequestRepUtils.getRepository());
    }

    private static void preparePaymentDetails() {
        IObPaymentRequestPaymentDetailsGeneralModel paymentDetails = PaymentUtils.buildPaymentDetailsGeneralModel();
        IObPaymentRequestPaymentDetailsGeneralModelRepUtils.findOneByRefInstanceId(paymentDetails);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                IObPaymentRequestPaymentDetailsGeneralModelRepUtils.getRepository());
    }

    private static void prepareUserAccountSecurityService() {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }


    private static void prepareBankPaymentForPurchaseOrderDependencies(DObBankPaymentForPurchaseOrderAccountDeterminationCommand command, Long orderId, String orderCode) {
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT);
        DObLocalPurchaseOrder purchaseOrder = DObLocalPurchaseOrderMockUtils.buildEntity(orderId, orderCode);
        DObPurchaseOrderRepMockUtils.findOneByUserCode(purchaseOrder);
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);
        command.setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
        command.setPurchaseOrderRep(DObPurchaseOrderRepMockUtils.getRepository());
    }

    private static void prepareCashPaymentForPurchaseOrderDependencies(DObCashPaymentForPurchaseOrderAccountDeterminationCommand command, Long orderId, String orderCode) {
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT);
        DObLocalPurchaseOrder purchaseOrder = DObLocalPurchaseOrderMockUtils.buildEntity(orderId, orderCode);
        DObPurchaseOrderRepMockUtils.findOneByUserCode(purchaseOrder);
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);
        command.setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
        command.setPurchaseOrderRep(DObPurchaseOrderRepMockUtils.getRepository());
    }

    private static void prepareBankPaymentForVendorDependencies(DObBankPaymentForVendorAccountDeterminationCommand command, String vendorUserCode, String accountCode) {
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT);
        MObVendor vendor = VendorMockUtils.buildMObVendor(vendorUserCode, 1L);
        MObVendorRepUtils.findOneByUserCode(vendor);
        IObVendorAccountingInfoGeneralModel vendorAccountingInfo = IObVendorAccountingInfoGeneralModelMockUtils.buildGeneralModel(vendorUserCode, accountCode);
        IObVendorAccountingInfoGeneralModelRepUtils.findByVendorCode(vendorAccountingInfo);
        HObGLAccount glAccount = HObGLAccountUtils.buildHObGLAccount(accountCode);
        HObChartOfAccountRepUtils.findOneByUserCode(glAccount);
        command.setChartOfAccountRep(HObChartOfAccountRepUtils.getRepository());
        command.setVendorAccountingInfoRep(IObVendorAccountingInfoGeneralModelRepUtils.getRepository());
        command.setVendorRep(MObVendorRepUtils.getRepository());
        command.setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
    }

    private static void prepareCashPaymentForVendorDependencies(DObCashPaymentForVendorAccountDeterminationCommand command, String vendorUserCode, String accountCode) {
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT);
        MObVendor vendor = VendorMockUtils.buildMObVendor(vendorUserCode, 1L);
        MObVendorRepUtils.findOneByUserCode(vendor);
        IObVendorAccountingInfoGeneralModel vendorAccountingInfo = IObVendorAccountingInfoGeneralModelMockUtils.buildGeneralModel(vendorUserCode, accountCode);
        IObVendorAccountingInfoGeneralModelRepUtils.findByVendorCode(vendorAccountingInfo);
        HObGLAccount glAccount = HObGLAccountUtils.buildHObGLAccount(accountCode);
        HObChartOfAccountRepUtils.findOneByUserCode(glAccount);
        command.setChartOfAccountRep(HObChartOfAccountRepUtils.getRepository());
        command.setVendorAccountingInfoRep(IObVendorAccountingInfoGeneralModelRepUtils.getRepository());
        command.setVendorRep(MObVendorRepUtils.getRepository());
        command.setGlobalAccountConfigGeneralModelRep(LObGlobalAccountConfigGeneralModelRepUtils.getRepository());
    }
}