package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidator;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidatorFactory;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestJsonSchema;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsValueObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.gson.JsonObject;

import java.math.BigDecimal;

public class IObPaymentRequestSavePaymentDetailsCommandTestUtils {

    private JsonObject valueObjectAsJson;

    public IObPaymentRequestSavePaymentDetailsCommandTestUtils() { valueObjectAsJson = new JsonObject(); }

    public void applySchemaValidationOnServiceItemValueObject() throws Exception {
        String paymentDetailsSchema = IDObPaymentRequestJsonSchema.PAYMENT_DETAILS_SCHEMA;
        try {
            JsonSchemaValidator schemaValidator = new JsonSchemaValidator()
                    .jsonSchemaFactory(new JsonSchemaValidatorFactory(paymentDetailsSchema));
            schemaValidator.validateSchemaForString(valueObjectAsJson.toString());
        } catch (JsonParseException ex) {
            throw new ArgumentViolationSecurityException();
        }
    }

    public IObPaymentRequestSavePaymentDetailsCommandTestUtils withNetAmount(BigDecimal netAmount) {
        valueObjectAsJson.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_NET_AMOUNT, netAmount);
        return this;
    }

    public IObPaymentRequestSavePaymentDetailsCommandTestUtils withNetAmountAsString(String netAmount) {
        valueObjectAsJson.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_NET_AMOUNT, netAmount);
        return this;
    }
}
