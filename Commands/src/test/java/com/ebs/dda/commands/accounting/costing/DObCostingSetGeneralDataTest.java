package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.entities.*;
import com.ebs.entities.accounting.costing.CostingMockUtils;
import com.ebs.repositories.*;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
public class DObCostingSetGeneralDataTest {
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObCostingStateMachine costingStateMachine;

  private DObCostingCreateCommand command;
  private CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory;
  private CostingExchangeRateFullyPaidStrategy costingExchangeRateFullyPaidStrategy;
  private CommandTestUtils commandTestUtils;

  @MockBean private DObCostingRep costingRep;
  @MockBean private CreateCostingItemsSectionService createCostingItemsSectionService;

  @Before
  public void init() {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    documentObjectCodeGenerator = Mockito.mock(DocumentObjectCodeGenerator.class);
    costingStateMachine = Mockito.mock(DObCostingStateMachine.class);

    costingExchangeRateStrategyFactory = Mockito.mock(CostingExchangeRateStrategyFactory.class);
    costingExchangeRateFullyPaidStrategy = Mockito.mock(CostingExchangeRateFullyPaidStrategy.class);

    command = new DObCostingCreateCommand(documentObjectCodeGenerator, costingStateMachine);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setGoodsReceiptGeneralModelRep(GoodsReceiptGeneralModelRepUtils.getRepository());
    command.setVendorInvoiceGeneralModelRep(VendorInvoiceGeneralModelRepMockUtils.getRepository());
    command.setVendorInvoiceSummaryRep(VendorInvoiceSummaryRepUtils.getRepository());
    command.setCostingRep(costingRep);
    command.setCostingExchangeRateStrategyFactory(costingExchangeRateStrategyFactory);
    command.setLandedCostGeneralModelRep(LandedCostGeneralModelRepMockUtils.getRepository());
    command.setCreateCostingItemsSectionService(createCostingItemsSectionService);

    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @After
  public void after() {
    LandedCostGeneralModelRepMockUtils.resetRepository();
  }

  @Test
  public void should_SetGeneralDataForCostingDocument() throws Exception {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .id(1L)
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);


    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED)
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    Mockito.when(costingExchangeRateStrategyFactory.getStrategy(Mockito.any()))
        .thenReturn(costingExchangeRateFullyPaidStrategy);
    Mockito.when(costingExchangeRateFullyPaidStrategy.calculate(Mockito.any()))
        .thenReturn(new BigDecimal("16.38"));

    DObCosting expectedCosting = CostingMockUtils.mockEntity().userCode("")
            .creationDate(DateTime.now())
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .modifiedDate(DateTime.now()).build();

    Mockito.when(createCostingItemsSectionService.getCostingItems(Mockito.any())).thenReturn(new ArrayList<>());
    // act
    command.executeCommand(valueObject);

    // assert
    ArgumentCaptor<DObCosting> createdObjectCaptor = ArgumentCaptor.forClass(DObCosting.class);
    Mockito.verify(costingRep, Mockito.times(1)).create(createdObjectCaptor.capture());
    DObCosting createdObject = createdObjectCaptor.getValue();
    Assert.assertEquals(expectedCosting.getCreationInfo(),  createdObject.getCreationInfo());
    Assert.assertEquals(expectedCosting.getCreationDate(),  createdObject.getCreationDate());
    Assert.assertEquals(expectedCosting.getModificationInfo(),  createdObject.getModificationInfo());
    Assert.assertEquals(expectedCosting.getModifiedDate(),  createdObject.getModifiedDate());
    Mockito.verify(costingStateMachine, Mockito.times(1))
        .initObjectState(Mockito.any(DObCosting.class));

  }

}
