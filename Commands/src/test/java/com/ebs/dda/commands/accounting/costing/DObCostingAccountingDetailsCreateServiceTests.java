package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.DObCostingAccountingDetailsCreateService;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.entities.*;
import com.ebs.repositories.*;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DObCostingAccountingDetailsCreateServiceTests {

  private DObCostingAccountingDetailsCreateService costingAccountingDetailsCreateService;

  private CommandTestUtils commandTestUtils;

  @Before
  public void init() {
    commandTestUtils = new CommandTestUtils();

    costingAccountingDetailsCreateService =
        new DObCostingAccountingDetailsCreateService(
            LandedCostFactorItemsSummaryGeneralModelRepUtils.getRepository(),
            LObGlobalAccountConfigGeneralModelRepUtils.getRepository(),
            VendorInvoiceItemsGeneralModelRepMockUtils.getRepository(),
            ItemAccountingInfoGeneralModelRepMockUtils.getRepository(),
            AccountingDocumentActivationDetailsGeneralModelRepMockUtils.getRepository(),
            VendorInvoiceSummaryRepUtils.getRepository(),
            LandedCostGeneralModelRepMockUtils.getRepository(),
            commandTestUtils.getUserAccountSecurityService());

    commandTestUtils.mockUserAccount("Admin");
    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @Test
  public void
      shouldGetPurchasingAccountingDetailWithCreditRealizedItemAccountingDetailsAndDebitRealizedExchangeRateDetail() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceItemsGeneralModel firstVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000001")
            .totalAmount(BigDecimal.valueOf(1000))
            .build();
    IObVendorInvoiceItemsGeneralModel secondVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000002")
            .totalAmount(BigDecimal.valueOf(1500))
            .build();
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems =
        Arrays.asList(firstVendorInvoiceItem, secondVendorInvoiceItem);
    VendorInvoiceItemsGeneralModelRepMockUtils.findByGoodsReceiptCode(vendorInvoiceItems);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    IObAccountingDocumentActivationDetailsGeneralModel invoiceActivationDetailsGeneralModel =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .currencyPrice(new BigDecimal("17.44"))
            .objectTypeCode("VendorInvoice")
            .build();
    AccountingDocumentActivationDetailsGeneralModelRepMockUtils.findOneByCodeAndObjectTypeCode(
        invoiceActivationDetailsGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    BigDecimal costingCurrencyPrice = new BigDecimal("16.38");

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .purchaseOrderCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED)
            .userCode("2021000001")
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostGeneralModel.getUserCode())
            .actualValue(BigDecimal.valueOf(1570.0))
            .build();

    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCostFactorItemsSummary);

    IObItemAccountingInfoGeneralModel firstItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(firstVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(25L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        firstItemAccountingInfoGeneralModel);

    IObItemAccountingInfoGeneralModel secondItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(secondVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(26L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        secondItemAccountingInfoGeneralModel);

    LObGlobalGLAccountConfigGeneralModel purchasingGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel purchaseOrderGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel realizedGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);

    IObAccountingDocumentAccountingDetails purchasingAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchasingGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("42520"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    IObAccountingDocumentAccountingDetails purchaseOrderAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchaseOrderGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails firstItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(firstItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("16380"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails secondItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(secondItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("24570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails firstItemRealizedAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(firstItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1060"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails secondItemRealizedAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(secondItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1590"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails realizedExchangeRateAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(realizedGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("2650"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    List<IObAccountingDocumentAccountingDetails> costingAccountingDetails =
        costingAccountingDetailsCreateService.getCostingAccountingDetails(
            vendorInvoiceGeneralModel,
            costingCurrencyPrice,
            goodsReceiptGeneralModel.getUserCode());

    // assert

    IObAccountingDocumentAccountingDetails actualPurchasingAccountingDetail =
        getAccountingDocumentAccountingDetail(purchasingAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualPurchaseOrderAccountingDetail =
        getAccountingDocumentAccountingDetail(
            purchaseOrderAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualFirstItemAccountingDetail =
        getAccountingDocumentAccountingDetail(firstItemAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualSecondItemAccountingDetail =
        getAccountingDocumentAccountingDetail(secondItemAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualFirstItemRealizedAccountingDetail =
        getAccountingDocumentAccountingDetail(
            firstItemRealizedAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualSecondItemRealizedAccountingDetail =
        getAccountingDocumentAccountingDetail(
            secondItemRealizedAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualRealizedExchangeRateAccountingDetail =
        getAccountingDocumentAccountingDetail(
            realizedExchangeRateAccountingDetail, costingAccountingDetails);

    Assert.assertNotNull(actualPurchasingAccountingDetail);
    Assert.assertNotNull(actualPurchaseOrderAccountingDetail);
    Assert.assertNotNull(actualFirstItemAccountingDetail);
    Assert.assertNotNull(actualSecondItemAccountingDetail);
    Assert.assertNotNull(actualFirstItemRealizedAccountingDetail);
    Assert.assertNotNull(actualSecondItemRealizedAccountingDetail);
    Assert.assertNotNull(actualRealizedExchangeRateAccountingDetail);
    Assert.assertEquals(7, costingAccountingDetails.size());
  }

  @Test
  public void
      shouldGetPurchasingAccountingDetailWithDebitRealizedItemAccountingDetailsAndCreditRealizedExchangeRateDetail() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceItemsGeneralModel firstVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000001")
            .totalAmount(BigDecimal.valueOf(1000))
            .build();
    IObVendorInvoiceItemsGeneralModel secondVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000002")
            .totalAmount(BigDecimal.valueOf(1500))
            .build();
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems =
        Arrays.asList(firstVendorInvoiceItem, secondVendorInvoiceItem);
    VendorInvoiceItemsGeneralModelRepMockUtils.findByGoodsReceiptCode(vendorInvoiceItems);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    IObAccountingDocumentActivationDetailsGeneralModel invoiceActivationDetailsGeneralModel =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .currencyPrice(new BigDecimal("15.38"))
            .objectTypeCode("VendorInvoice")
            .build();
    AccountingDocumentActivationDetailsGeneralModelRepMockUtils.findOneByCodeAndObjectTypeCode(
        invoiceActivationDetailsGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .purchaseOrderCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED)
            .userCode("2021000001")
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostGeneralModel.getUserCode())
            .actualValue(BigDecimal.valueOf(1570))
            .build();

    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCostFactorItemsSummary);

    IObItemAccountingInfoGeneralModel firstItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(firstVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(25L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        firstItemAccountingInfoGeneralModel);

    IObItemAccountingInfoGeneralModel secondItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(secondVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(26L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        secondItemAccountingInfoGeneralModel);

    LObGlobalGLAccountConfigGeneralModel purchasingGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel purchaseOrderGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel realizedGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    BigDecimal costingCurrencyPrice = new BigDecimal("16.38");

    IObAccountingDocumentAccountingDetails purchasingAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchasingGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("42520"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    IObAccountingDocumentAccountingDetails purchaseOrderAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchaseOrderGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails firstItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(firstItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("16380"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails secondItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(secondItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("24570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails firstItemRealizedAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(firstItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("1000"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails secondItemRealizedAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(secondItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("1500"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails realizedExchangeRateAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(realizedGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("2500"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    // act

    List<IObAccountingDocumentAccountingDetails> costingAccountingDetails =
        costingAccountingDetailsCreateService.getCostingAccountingDetails(
            vendorInvoiceGeneralModel,
            costingCurrencyPrice,
            goodsReceiptGeneralModel.getUserCode());

    // assert

    IObAccountingDocumentAccountingDetails actualPurchasingAccountingDetail =
        getAccountingDocumentAccountingDetail(purchasingAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualPurchaseOrderAccountingDetail =
        getAccountingDocumentAccountingDetail(
            purchaseOrderAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualFirstItemAccountingDetail =
        getAccountingDocumentAccountingDetail(firstItemAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualSecondItemAccountingDetail =
        getAccountingDocumentAccountingDetail(secondItemAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualFirstItemRealizedAccountingDetail =
        getAccountingDocumentAccountingDetail(
            firstItemRealizedAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualSecondItemRealizedAccountingDetail =
        getAccountingDocumentAccountingDetail(
            secondItemRealizedAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualRealizedExchangeRateAccountingDetail =
        getAccountingDocumentAccountingDetail(
            realizedExchangeRateAccountingDetail, costingAccountingDetails);

    Assert.assertNotNull(actualPurchasingAccountingDetail);
    Assert.assertNotNull(actualPurchaseOrderAccountingDetail);
    Assert.assertNotNull(actualFirstItemAccountingDetail);
    Assert.assertNotNull(actualSecondItemAccountingDetail);
    Assert.assertNotNull(actualFirstItemRealizedAccountingDetail);
    Assert.assertNotNull(actualSecondItemRealizedAccountingDetail);
    Assert.assertNotNull(actualRealizedExchangeRateAccountingDetail);
    Assert.assertEquals(7, costingAccountingDetails.size());
  }

  @Test
  public void
      shouldGetPurchasingAccountingDetailWithoutRealizedItemAccountingDetailsOrRealizedExchangeRateDetail() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceItemsGeneralModel firstVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000001")
            .totalAmount(BigDecimal.valueOf(1000))
            .build();
    IObVendorInvoiceItemsGeneralModel secondVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000002")
            .totalAmount(BigDecimal.valueOf(1500))
            .build();
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems =
        Arrays.asList(firstVendorInvoiceItem, secondVendorInvoiceItem);
    VendorInvoiceItemsGeneralModelRepMockUtils.findByGoodsReceiptCode(vendorInvoiceItems);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    BigDecimal costingCurrencyPrice = new BigDecimal("16.38");

    IObAccountingDocumentActivationDetailsGeneralModel invoiceActivationDetailsGeneralModel =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .currencyPrice(costingCurrencyPrice)
            .objectTypeCode("VendorInvoice")
            .build();
    AccountingDocumentActivationDetailsGeneralModelRepMockUtils.findOneByCodeAndObjectTypeCode(
        invoiceActivationDetailsGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .purchaseOrderCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED)
            .userCode("2021000001")
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostGeneralModel.getUserCode())
            .actualValue(BigDecimal.valueOf(1570))
            .build();

    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCostFactorItemsSummary);

    IObItemAccountingInfoGeneralModel firstItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(firstVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(25L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        firstItemAccountingInfoGeneralModel);

    IObItemAccountingInfoGeneralModel secondItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(secondVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(26L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        secondItemAccountingInfoGeneralModel);

    LObGlobalGLAccountConfigGeneralModel purchasingGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel purchaseOrderGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT);

    LObGlobalGLAccountConfigGeneralModel realizedGLAccountConfigGeneralModel =
        LObGlobalAccountConfigGeneralModelRepUtils.findOneByUserCode(
            ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    IObAccountingDocumentAccountingDetails purchasingAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchasingGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("42520"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    IObAccountingDocumentAccountingDetails purchaseOrderAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(purchaseOrderGLAccountConfigGeneralModel.getGlAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails firstItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(firstItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("16380"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();

    IObAccountingDocumentAccountingDetails secondItemAccountingDetail =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(secondItemAccountingInfoGeneralModel.getAccountId())
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("24570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(landedCostGeneralModel.getPurchaseOrderId())
            .build();
    // act

    List<IObAccountingDocumentAccountingDetails> costingAccountingDetails =
        costingAccountingDetailsCreateService.getCostingAccountingDetails(
            vendorInvoiceGeneralModel,
            costingCurrencyPrice,
            goodsReceiptGeneralModel.getUserCode());

    // assert

    IObAccountingDocumentAccountingDetails actualPurchasingAccountingDetail =
        getAccountingDocumentAccountingDetail(purchasingAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualPurchaseOrderAccountingDetail =
        getAccountingDocumentAccountingDetail(
            purchaseOrderAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualFirstItemAccountingDetail =
        getAccountingDocumentAccountingDetail(firstItemAccountingDetail, costingAccountingDetails);
    IObAccountingDocumentAccountingDetails actualSecondItemAccountingDetail =
        getAccountingDocumentAccountingDetail(secondItemAccountingDetail, costingAccountingDetails);

    Assert.assertNotNull(actualPurchasingAccountingDetail);
    Assert.assertNotNull(actualPurchaseOrderAccountingDetail);
    Assert.assertNotNull(actualFirstItemAccountingDetail);
    Assert.assertNotNull(actualSecondItemAccountingDetail);
    Assert.assertEquals(4, costingAccountingDetails.size());
  }

  private IObAccountingDocumentAccountingDetails getAccountingDocumentAccountingDetail(
      IObAccountingDocumentAccountingDetails accountingDetail,
      Collection<IObAccountingDocumentAccountingDetails> costingAccountingDetails) {
    IObAccountingDocumentAccountingDetails actual =
        costingAccountingDetails.stream()
            .filter(detail -> accountingDetail.getGlAccountId().equals(detail.getGlAccountId()))
            .filter(detail -> accountingDetail.getAmount().compareTo(detail.getAmount()) == 0)
            .filter(detail -> accountingDetail.getCreationInfo().equals(detail.getCreationInfo()))
            .filter(
                detail ->
                    accountingDetail.getModificationInfo().equals(detail.getModificationInfo()))
            .filter(
                detail -> accountingDetail.getAccountingEntry().equals(detail.getAccountingEntry()))
            .filter(detail -> accountingDetail.getCreationDate().equals(detail.getCreationDate()))
            .filter(detail -> accountingDetail.getModifiedDate().equals(detail.getModifiedDate()))
            .filter(
                detail -> accountingDetail.getObjectTypeCode().equals(detail.getObjectTypeCode()))
            .filter(detail -> accountingDetail.getGlSubAccountId() == detail.getGlSubAccountId())
            .findAny()
            .orElse(null);
    return actual;
  }
}
