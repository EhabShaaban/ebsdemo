package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.salesinvoice.utils.DObSalesInvoiceAccountDeterminationCommandTestUtils;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObSalesInvoiceAccountDeterminationCommandTests {
  private DObSalesInvoiceAccountDeterminationCommandTestUtils utils;

  @Before
  public void setUp() {
    utils = new DObSalesInvoiceAccountDeterminationCommandTestUtils();
  }

  @Test
  public void testDetermineAccountsForSalesInvoiceBasedOnSalesOrder_whenAllTaxesArePositive()
      throws Exception {
    utils
        .withSalesInvoice(1L, "2021000001")
        .withSalesAccountId(42L)
        .withCustomerAccountId(11L)
        .withCustomerId(73L)
        .withAmountBeforeTaxes(BigDecimal.valueOf(330))
        .withAmountAfterTaxes(BigDecimal.valueOf(448.80))
        .withTax(43L, 7L, BigDecimal.valueOf(3.3))
        .withTax(44L, 8L, BigDecimal.valueOf(1.65))
        .withTax(45L, 9L, BigDecimal.valueOf(33))
        .withTax(46L, 10L, BigDecimal.valueOf(39.6))
        .withTax(47L, 11L, BigDecimal.valueOf(1.65))
        .withTax(48L, 12L, BigDecimal.valueOf(39.6))
        .whenExecute()
        .saveMethodIsCalledSuccessfully()
        .numberOfAccountsDeterminedIs(8)
        .assertThatCustomerAccountingDetailsIsRecorded(
            11L, BigDecimal.valueOf(448.80), AccountingEntry.DEBIT.name(), 1)
        .assertThatAccountingDetailsIsRecorded(
            43L, BigDecimal.valueOf(3.3), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            42L, BigDecimal.valueOf(330), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            44L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            45L, BigDecimal.valueOf(33), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            46L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            47L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            48L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name(), 7);
  }

  @Test
  public void testDetermineAccountsForSalesInvoiceBasedOnSalesOrder_whenTaxesHaveNegativeValue()
      throws Exception {
    utils
        .withSalesInvoice(1L, "2021000001")
        .withSalesAccountId(42L)
        .withCustomerAccountId(11L)
        .withCustomerId(73L)
        .withAmountBeforeTaxes(BigDecimal.valueOf(330))
        .withAmountAfterTaxes(BigDecimal.valueOf(448.80))
        .withTax(43L, 7L, BigDecimal.valueOf(-3.3))
        .withTax(44L, 8L, BigDecimal.valueOf(1.65))
        .withTax(45L, 9L, BigDecimal.valueOf(33))
        .withTax(46L, 10L, BigDecimal.valueOf(39.6))
        .withTax(47L, 11L, BigDecimal.valueOf(1.65))
        .withTax(48L, 12L, BigDecimal.valueOf(39.6))
        .whenExecute()
        .saveMethodIsCalledSuccessfully()
        .numberOfAccountsDeterminedIs(8)
        .assertThatCustomerAccountingDetailsIsRecorded(
            11L, BigDecimal.valueOf(448.80), AccountingEntry.DEBIT.name(), 1)
        .assertThatAccountingDetailsIsRecorded(
            43L, BigDecimal.valueOf(3.3), AccountingEntry.DEBIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            42L, BigDecimal.valueOf(330), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            44L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            45L, BigDecimal.valueOf(33), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            46L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            47L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name(), 7)
        .assertThatAccountingDetailsIsRecorded(
            48L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name(), 7);
  }
}
