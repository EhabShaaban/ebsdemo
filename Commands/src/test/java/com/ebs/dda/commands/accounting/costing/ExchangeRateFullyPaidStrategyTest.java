package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.commands.accounting.costing.utils.ExchangeRateStrategyTestUtils;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.repositories.ExchangeRateRepMockUtils;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ExchangeRateFullyPaidStrategyTest {

  @MockBean private DObVendorInvoiceGeneralModelRep vendorInvoiceRep;
  @MockBean private DObPaymentRequestCurrencyPriceGeneralModelRep paymentRequestCurrencyPriceRep;
  @MockBean private CObCompanyGeneralModelRep cObCompanyGeneralModelRep;

  private CostingExchangeRateUtils costingExchangeRateUtils;
  private ICalculateCostingExchangeRateStrategy strategy;
  private ExchangeRateStrategyTestUtils utils;

  @Before
  public void setUp() throws Exception {
    costingExchangeRateUtils =
        new CostingExchangeRateUtils(
            vendorInvoiceRep,
            paymentRequestCurrencyPriceRep,
            cObCompanyGeneralModelRep,
            ExchangeRateRepMockUtils.getRepository());
    strategy = new CostingExchangeRateFullyPaidStrategy(costingExchangeRateUtils);
    utils = new ExchangeRateStrategyTestUtils();
  }

  @Test
  public void testFullyPaidOnOnePayment() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceipt = utils.prepareGoodsReceipt();
    DObVendorInvoiceGeneralModel vendorInvoice =
        utils.prepareVendorInvoice(vendorInvoiceRep, goodsReceipt.getRefDocumentCode());
    utils.prepareEmptyListForVendorInvoicePayments(paymentRequestCurrencyPriceRep, vendorInvoice);
    utils.preparePayment(
        paymentRequestCurrencyPriceRep,
        "2021000001",
        goodsReceipt.getRefDocumentCode(),
        "PURCHASEORDER",
        BigDecimal.valueOf(2000),
        BigDecimal.valueOf(17));
    strategy.setVendorInvoiceSummary(utils.prepareVendorInvoiceSummary("2000", "0"));
    // act
    BigDecimal currencyPrice = strategy.calculate(goodsReceipt);

    // assert
    Assert.assertEquals(
        "expected currency price not equals " + BigDecimal.valueOf(17) + " " + currencyPrice,
        0,
        BigDecimal.valueOf(17).compareTo(currencyPrice));
  }

  @Test
  public void testFullyPaidOnMultiplePayments() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceipt = utils.prepareGoodsReceipt();
    DObVendorInvoiceGeneralModel vendorInvoice =
        utils.prepareVendorInvoice(vendorInvoiceRep, goodsReceipt.getRefDocumentCode());
    utils.preparePayment(
        paymentRequestCurrencyPriceRep,
        "2021000001",
        goodsReceipt.getRefDocumentCode(),
        "PURCHASEORDER",
        BigDecimal.valueOf(2000),
        BigDecimal.valueOf(17));
    utils.preparePayment(
        paymentRequestCurrencyPriceRep,
        "2021000002",
        vendorInvoice.getUserCode(),
        "INVOICE",
        BigDecimal.valueOf(500),
        BigDecimal.valueOf(16));
    strategy.setVendorInvoiceSummary(utils.prepareVendorInvoiceSummary("2500", "0"));

    // act
    BigDecimal currencyPrice = strategy.calculate(goodsReceipt);

    // assert
    Assert.assertEquals(
        "expected currency price not equals " + BigDecimal.valueOf(16.8) + " " + currencyPrice,
        0,
        BigDecimal.valueOf(16.8).compareTo(currencyPrice));
  }

  @Test
  public void Should_UsePaymentCurrencyPriceGeneralModelRep() {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceipt = utils.prepareGoodsReceipt();
    DObVendorInvoiceGeneralModel vendorInvoice =
        utils.prepareVendorInvoice(vendorInvoiceRep, goodsReceipt.getRefDocumentCode());
    utils.preparePayment(
        paymentRequestCurrencyPriceRep,
        "2021000001",
        goodsReceipt.getRefDocumentCode(),
        "PURCHASEORDER",
        BigDecimal.valueOf(2000),
        BigDecimal.valueOf(17));

    strategy.setVendorInvoiceSummary(utils.prepareVendorInvoiceSummary("2500", "0"));

    // act
    BigDecimal currencyPrice = strategy.calculate(goodsReceipt);

    // assert
    utils.thenAssertPaymentCurrencyPriceGeneralModelRepIsCalled(
        paymentRequestCurrencyPriceRep,
        goodsReceipt.getRefDocumentCode(),
        "PURCHASEORDER",
        PaymentType.OTHER_PARTY_FOR_PURCHASE.name());
  }
}
