package com.ebs.dda.commands.inventory.stockavailability;

import com.ebs.dda.commands.inventory.stockavailability.utils.UpdateStockAvailabilityServiceTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.ebs.dda.commands.inventory.stockavailability.utils.UpdateStockAvailabilityServiceTestUtils.Item;

@RunWith(MockitoJUnitRunner.class)
public class UpdateStockAvailabilityServiceTests {
  private UpdateStockAvailabilityServiceTestUtils utils;

  @Before
  public void init() {
    utils = new UpdateStockAvailabilityServiceTestUtils();
  }

  @Test
  public void Should_UpdateExistingStockAvailabilityByAdding() {
    utils
        .withItem(
            Item(
                "UNRESTRICTED_USE",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("10.255252534")))
        .withStockAvailabilityCode("UNRESTRICTED_USE000100010001000100530001")
        .withStockAvailabilityQuantity(new BigDecimal("10.63266363"))
        .whenUpdate()
        .thenAssertStockAvailabilityFetchedOnceFromRepositoryWithCode(
            "UNRESTRICTED_USE000100010001000100530001")
        .thenAssertStockAvailabilityRepositoryUpdateCalledOnceWithProperValue(
            "UNRESTRICTED_USE000100010001000100530001", new BigDecimal("20.887916164"));
  }

  @Test
  public void Should_UpdateExistingStockAvailabilityBySubtracting() {
    utils
        .withItem(
            Item(
                "UNRESTRICTED_USE",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("-5.865375387")))
        .withStockAvailabilityCode("UNRESTRICTED_USE000100010001000100530001")
        .withStockAvailabilityQuantity(new BigDecimal("10.5398539"))
        .whenUpdate()
        .thenAssertStockAvailabilityFetchedOnceFromRepositoryWithCode(
            "UNRESTRICTED_USE000100010001000100530001")
        .thenAssertStockAvailabilityRepositoryUpdateCalledOnceWithProperValue(
            "UNRESTRICTED_USE000100010001000100530001", new BigDecimal("4.674478513"));
  }

  @Test
  public void Should_CreateNewStockAvailability() {
    utils
        .withItem(
            Item(
                "UNRESTRICTED_USE",
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("10.255252534")))
        .withStockAvailabilityCode("UNRESTRICTED_USE000100010001000100530001")
        .whenUpdate()
        .thenAssertStockAvailabilityFetchedOnceFromRepositoryWithCode(
            "UNRESTRICTED_USE000100010001000100530001")
        .thenAssertStockAvailabilityRepositoryCreateCalledOnceWithProperValue(
            "UNRESTRICTED_USE000100010001000100530001", new BigDecimal("10.255252534"));
  }
}
