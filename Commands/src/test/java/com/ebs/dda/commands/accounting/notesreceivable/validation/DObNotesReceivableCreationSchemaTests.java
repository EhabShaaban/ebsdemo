package com.ebs.dda.commands.accounting.notesreceivable.validation;

import com.ebs.dda.commands.accounting.notesreceivable.validation.utils.DObNotesReceivableCreationSchemaTestUtils;
import com.ebs.dda.jpa.accounting.monetarynotes.MonetaryNoteFormEnum;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableTypeEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(JUnitPlatform.class)
public class DObNotesReceivableCreationSchemaTests {

  private DObNotesReceivableCreationSchemaTestUtils utils;

  @BeforeEach
  public void init() {
    utils = new DObNotesReceivableCreationSchemaTestUtils();
  }

  @Test
  public void Should_Pass_WithValidData() throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .withBusinessUnitCode("0002")
      .withCompanyCode("0002")
      .withBusinessPartnerCode("000007")
      .withDocumentOwnerId(1L)
      .withAmount(new BigDecimal(33000))
      .withCurrencyIso("EGP")
      .assertValidateSuccessfully();
  }

  @Test
  public void Should_Pass_WithMinimumAmount() throws Exception {
    utils
      .noteForm(MonetaryNoteFormEnum.CHEQUE)
      .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
      .withBusinessUnitCode("0002")
      .withCompanyCode("0002")
      .withBusinessPartnerCode("000007")
      .withDocumentOwnerId(1L)
      .withAmount(new BigDecimal(0.01))
      .withCurrencyIso("EGP")
      .assertValidateSuccessfully();
  }

  @TestFactory
  public Stream<DynamicTest>  Should_ThrowsArgumentSecurityException_When_InvalidNoteForm() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("Numeric value", 123);
          put("Not in possible values", "ANOTHER_FORM");
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidValue = invalidValues.get(key);
        utils
          .noteForm(invalidValue)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidNRType() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("Numeric value", 123);
          put("Not in possible values", "ANOTHER_TYPE");
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidValue = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(invalidValue)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidBusinessUnitCode() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("Numeric value", 1000);
          put("Code exceeds max number of digits", "00002");
          put("Code has alphabets", "test");
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidCode = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode(invalidCode)
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidCompanyCode() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("Numeric value", 1000);
          put("Code exceeds max number of digits", "00002");
          put("Code has alphabets", "test");
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidCode = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode(invalidCode)
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidBusinessPartnerCode() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("Numeric value", 1000);
          put("Code exceeds max number of digits", "0000002");
          put("Code has alphabets", "test");
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidCode = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode(invalidCode)
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidDocumentOwnerId() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("String value", "1");
          put("Negative value", -1);
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidCode = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(invalidCode)
          .withAmount(new BigDecimal(33000))
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidAmount() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("String value", "1");
          put("Negative amount", BigDecimal.valueOf(-1));
          put("Zero amount", BigDecimal.ZERO);
          put("Less than minimum", BigDecimal.valueOf(0.001));
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidAmount = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(invalidAmount)
          .withCurrencyIso("EGP")
          .assertThrowsArgumentSecurityException();
      }));
  }


  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidIso() {
    Map<String, Object> invalidValues =
      new HashMap<String, Object>() {
        {
          put("Null value", null);
          put("Missing key", DObNotesReceivableCreationSchemaTestUtils.MISSING_KEY_MARKER);
          put("Empty value", "");
          put("Code has non ASCII letters", "<@#_!/\\>");
          put("One char value", "1");
          put("Four char value", "EEEE");
          put("Numeric", 100);
        }
      };
    return invalidValues
      .keySet()
      .stream()
      .map(key -> DynamicTest.dynamicTest(key, () -> {
        Object invalidAmount = invalidValues.get(key);
        utils
          .noteForm(MonetaryNoteFormEnum.CHEQUE)
          .type(NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION)
          .withBusinessUnitCode("0002")
          .withCompanyCode("0002")
          .withBusinessPartnerCode("000007")
          .withDocumentOwnerId(1L)
          .withAmount(new BigDecimal(1000))
          .withCurrencyIso(invalidAmount)
          .assertThrowsArgumentSecurityException();
      }));
  }

}
