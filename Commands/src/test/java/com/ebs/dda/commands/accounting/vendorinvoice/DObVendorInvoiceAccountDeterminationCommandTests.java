package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.commands.accounting.vendorinvoice.utils.DObVendorInvoiceAccountDeterminationCommandTestUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceRepMockUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObVendorInvoiceAccountDeterminationCommandTests {
  private DObVendorInvoiceAccountDeterminationCommandTestUtils utils;

  @Before
  public void setUp() {
    utils = new DObVendorInvoiceAccountDeterminationCommandTestUtils();
  }

  @Test
  public void testDetermineImportGoodsInvoiceAccounts() throws Exception {
    utils
        .withVendorInvoice("2021000001", "IMPORT_GOODS_INVOICE")
        .withVendorId(2L)
        .withVendorAccountId(5L)
        .withAmount(BigDecimal.valueOf(2000))
        .withImportPurchaseOrderId(3L)
        .withItem(10L, BigDecimal.valueOf(1000))
        .withItem(10L, BigDecimal.valueOf(500))
        .withItem(20L, BigDecimal.valueOf(500))
        .whenExecute()
        .saveMethodIsCalledSuccessfully()
        .numberOfAccountsDeterminedIs(3);
  }

  @Test
  public void testDetermineLocalGoodsInvoiceAccounts() throws Exception {
    utils
        .withVendorInvoice("2021000001", "LOCAL_GOODS_INVOICE")
        .withVendorId(2L)
        .withVendorAccountId(5L)
        .withAmount(BigDecimal.valueOf(2000))
        .withImportPurchaseOrderId(3L)
        .withItem(10L, BigDecimal.valueOf(1000))
        .withItem(10L, BigDecimal.valueOf(500))
        .withItem(20L, BigDecimal.valueOf(500))
        .withTaxPercentage(BigDecimal.valueOf(5))
        .withTaxPercentage(BigDecimal.valueOf(2))
        .whenExecute()
        .saveMethodIsCalledSuccessfully()
        .numberOfAccountsDeterminedIs(3);
  }

  @Test
  public void testDetermineShipmentInvoiceAccounts() throws Exception {
    utils
        .withVendorInvoice("2021000001", "SHIPMENT_INVOICE")
        .withVendorId(2L)
        .withVendorAccountId(5L)
        .withAmount(BigDecimal.valueOf(500))
        .withServiceOrderId(7L)
        .withReferencePurchaseOrderId(3L)
        .withItem(33L, BigDecimal.valueOf(300))
        .withItem(33L, BigDecimal.valueOf(150))
        .withItem(35L, BigDecimal.valueOf(50))
        .withTaxPercentage(BigDecimal.valueOf(14))
        .withTaxPercentage(BigDecimal.valueOf(2))
        .whenExecute()
        .saveMethodIsCalledSuccessfully()
        .numberOfAccountsDeterminedIs(3);
  }

  @After
  public void tearDown() throws Exception {
    DObVendorInvoiceRepMockUtils.resetRepository();
  }
}
