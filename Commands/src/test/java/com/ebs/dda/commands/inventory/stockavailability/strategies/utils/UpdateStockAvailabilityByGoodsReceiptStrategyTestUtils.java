package com.ebs.dda.commands.inventory.stockavailability.strategies.utils;

import com.ebs.dda.commands.inventory.stockavailability.UpdateStockAvailabilityService;
import com.ebs.dda.commands.inventory.stockavailability.mocks.entities.IObGoodsReceiptItemQuantityGeneralModelMock;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsReceiptStrategy;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModelRep;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils {

  private String userCode;
  private IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep;
  private UpdateStockAvailabilityService updateService;
  private UpdateStockAvailabilityByGoodsReceiptStrategy goodsReceiptStrategy;
  private List<IObGoodsReceiptItemQuantityGeneralModel> goodsReceiptItems;

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils() {
    mockDependencies();
    initStrategy();
  }

  private void initStrategy() {
    goodsReceiptStrategy = new UpdateStockAvailabilityByGoodsReceiptStrategy();
    goodsReceiptStrategy.setGoodsReceiptItemQuantityGeneralModelRep(
        goodsReceiptItemQuantityGeneralModelRep);
    goodsReceiptStrategy.setStockAvailabilityUpdateService(updateService);
  }

  private void mockDependencies() {
    goodsReceiptItemQuantityGeneralModelRep =
        Mockito.mock(IObGoodsReceiptItemQuantityGeneralModelRep.class);
    updateService = Mockito.mock(UpdateStockAvailabilityService.class);
  }

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils withUserCode(String userCode) {
    this.userCode = userCode;
    return this;
  }

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils whenUpdate() {
    mockData();
    goodsReceiptStrategy.updateStockAvailability(userCode);
    return this;
  }

  private void mockData() {
    Mockito.when(goodsReceiptItemQuantityGeneralModelRep.findAllByUserCode(userCode))
        .thenReturn(goodsReceiptItems);
  }

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils withGoodsReceiptItems(
      IObGoodsReceiptItemQuantityGeneralModelMock... items) {
    goodsReceiptItems = new ArrayList<>(Arrays.asList(items));
    return this;
  }

  public static IObGoodsReceiptItemQuantityGeneralModelMock goodsReceiptItem(
      String stockType,
      String purchaseUnitCode,
      String companyCode,
      String plantCode,
      String storehouseCode,
      String itemCode,
      String unitOfMeasureCode,
      String userCode,
      BigDecimal quantity) {
    return new IObGoodsReceiptItemQuantityGeneralModelMock(
        userCode,
        itemCode,
        plantCode,
        storehouseCode,
        companyCode,
        purchaseUnitCode,
        unitOfMeasureCode,
        quantity,
        stockType);
  }

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils
      thenAssertRepositoryWasCalledWithCode(String userCode) {
    Mockito.verify(goodsReceiptItemQuantityGeneralModelRep, Mockito.times(1))
        .findAllByUserCode(userCode);
    return this;
  }

  public UpdateStockAvailabilityByGoodsReceiptStrategyTestUtils
      thenAssertUpdateServiceWasCalledWithItemsList() {
    ArgumentCaptor<IObGoodsReceiptItemQuantityGeneralModel> updateResult =
        ArgumentCaptor.forClass(IObGoodsReceiptItemQuantityGeneralModel.class);
    Mockito.verify(updateService, Mockito.times(goodsReceiptItems.size()))
        .updateOrCreateStockAvailabilityForItem(updateResult.capture(), Mockito.any());
    assertGoodsReceiptItemsAreEqual(updateResult.getAllValues(), goodsReceiptItems);
    return this;
  }

  private void assertGoodsReceiptItemsAreEqual(
      List<IObGoodsReceiptItemQuantityGeneralModel> actual,
      List<IObGoodsReceiptItemQuantityGeneralModel> expected) {
    Assert.assertEquals("Number of updated items doesn't match", expected.size(), actual.size());
    expected.stream()
        .forEach(
            item -> {
              Assert.assertNotNull(
                  "Item doesn't exist",
                  actual.stream().filter(goodsReceiptItemQuantitiesEqual(item)).findAny());
            });
  }

  private static Predicate<IObGoodsReceiptItemQuantityGeneralModel> goodsReceiptItemQuantitiesEqual(
      IObGoodsReceiptItemQuantityGeneralModel item) {
    return p ->
        p.getUserCode().equals(item.getUserCode())
            && p.getStockType().equals(item.getStockType())
            && p.getCompanyCode().equals(item.getCompanyCode())
            && p.getPlantCode().equals(item.getPlantCode())
            && p.getItemCode().equals(item.getItemCode())
            && p.getPurchaseUnitCode().equals(item.getPurchaseUnitCode())
            && p.getUnitOfMeasureCode().equals(item.getUnitOfMeasureCode());
  }
}
