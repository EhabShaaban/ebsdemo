package com.ebs.dda.commands.inventory.stockavailability.mocks.entities;

import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;

import java.math.BigDecimal;

public class CObStockAvailabilityMock extends CObStockAvailability {
    public CObStockAvailabilityMock(String userCode, BigDecimal availableQuantity) {
        setUserCode(userCode);
        setAvailableQuantity(availableQuantity);
    }

}
