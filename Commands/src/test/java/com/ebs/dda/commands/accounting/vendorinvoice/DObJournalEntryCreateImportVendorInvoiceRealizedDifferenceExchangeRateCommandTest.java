package com.ebs.dda.commands.accounting.vendorinvoice;

import com.ebs.dda.commands.accounting.vendorinvoice.utils.DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTest {

  private DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils
      utils;

  @Before
  public void setup() {
    utils =
        new DObJournalEntryCreateImportVendorInvoiceRealizedDifferenceExchangeRateCommandTestUtils();
  }

  @Test
  public void Should_DoNoThing_WhenThereIsNoRealizedDifferenceOnExchangeRate() throws Exception {
    utils.WithVendorInvoiceCode("2021000001")
        .WithJournalEntryCode("2021000001")
        .withPaymentAmount(BigDecimal.valueOf(100))
        .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(15))
        .withPaymentCurrencyPrice(BigDecimal.valueOf(15))
        .withCompanyCurrencyId(1L)
        .withGlVendorAccountId(1L)
        .withVendorId(1L)
        .withVendorSubLedger("Import_Vendors")
        .whenExecute()
        .assertThatNoJournalEntryIsAdded();
  }

  @Test
  public void
      Should_CreditTheRealizedExRateAndDebitTheVendor_WhenRealizedDifferenceOnExchangeRateIsPositive()
          throws Exception {
    utils.WithVendorInvoiceCode("2021000001")
        .WithJournalEntryCode("2021000001")
        .withPaymentAmount(BigDecimal.valueOf(100))
        .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(15))
        .withPaymentCurrencyPrice(BigDecimal.valueOf(12.5))
        .withCompanyCurrencyId(1L)
        .withGlVendorAccountId(1L)
        .withVendorId(1L)
        .withVendorSubLedger("Import_Vendors")
        .whenExecute()
        .assertThatJournalEntriesIsUpdated()
        .assertThatJournalEntryIsRecorded(1L, BigDecimal.valueOf(250), AccountingEntry.DEBIT.name())
        .assertThatJournalEntryIsRecorded(
            14L, BigDecimal.valueOf(250), AccountingEntry.CREDIT.name());
  }

  @Test
  public void
      Should_DebitTheRealizedExRateAndCreditTheVendor_WhenRealizedDifferenceOnExchangeRateIsNegative()
          throws Exception {

    utils.WithVendorInvoiceCode("2021000001")
        .WithJournalEntryCode("2021000001")
        .withPaymentAmount(BigDecimal.valueOf(100))
        .withVendorInvoiceCurrencyPrice(BigDecimal.valueOf(12.5))
        .withPaymentCurrencyPrice(BigDecimal.valueOf(15))
        .withCompanyCurrencyId(1L)
        .withGlVendorAccountId(1L)
        .withVendorId(1L)
        .withVendorSubLedger("Import_Vendors")
        .whenExecute()
        .assertThatJournalEntriesIsUpdated()
        .assertThatJournalEntryIsRecorded(
            1L, BigDecimal.valueOf(250), AccountingEntry.CREDIT.name())
        .assertThatJournalEntryIsRecorded(
            14L, BigDecimal.valueOf(250), AccountingEntry.DEBIT.name());
  }
}
