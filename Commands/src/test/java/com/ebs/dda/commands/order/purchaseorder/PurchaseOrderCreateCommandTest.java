package com.ebs.dda.commands.order.purchaseorder;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.order.purchaseorder.factory.DObPurchaseOrderFactory;
import com.ebs.dda.commands.order.purchaseorder.utils.PurchaseOrderCreateCommandTestUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.entities.purchases.OrderCompanyMockUtils;
import com.ebs.entities.purchases.PurchaseOrderMockUtils;
import com.ebs.repositories.masterdata.vendor.VendorGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.CompanyTaxesGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.OrderCompanyGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.PurchaseOrderRepTestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PurchaseOrderCreateCommandTest {
  PurchaseOrderCreateCommandTestUtils utils;
  @MockBean private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  @MockBean private DObPurchaseOrderFactory purchaseOrderFactory;
  @MockBean private DObPurchaseOrderStateMachineFactory stateMachineFactory;
  @MockBean private DObPurchaseOrderRep purchaseOrderRep;
  PurchaseOrderCreateCommand command;
  private CommandTestUtils commandTestUtils;

  @Before
  public void setUp() throws Exception {
    commandTestUtils = new CommandTestUtils();
    command = new PurchaseOrderCreateCommand(documentObjectCodeGenerator, stateMachineFactory);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setVendorGeneralModelRep(VendorGeneralModelRepTestUtils.getRepository());
    command.setOrderCompanyGeneralModelRep(OrderCompanyGeneralModelRepTestUtils.getRepository());
    command.setCompanyTaxesGeneralModelRep(CompanyTaxesGeneralModelRepTestUtils.getRepository());
    command.setPurchaseOrderRep(PurchaseOrderRepTestUtils.getRepository());
    command.setPurchaseOrderFactory(purchaseOrderFactory);
    utils = new PurchaseOrderCreateCommandTestUtils();
  }

  @Test
  public void shouldSetCompanyTaxesIfServicePurchaseOrder() {
    //    Arrange
    utils.prepareUserCodeToBeGenerated(documentObjectCodeGenerator);
    commandTestUtils.mockUserAccount("ADMIN");
    VendorGeneralModelRepTestUtils.findOneByUserCode(
        VendorMockUtils.mockGeneralModel().id(5L).build(), Mockito.anyString());
    PurchaseOrderRepTestUtils.findOneByUserCode(
        PurchaseOrderMockUtils.mockEntity().id(10L).build(), Mockito.anyString());
    OrderCompanyGeneralModelRepTestUtils.findOneByUserCode(OrderCompanyMockUtils.mockGeneralModel()
            .purchaseUnitId(15L)
            .companyId(20L)
            .companyCode("0001")
            .build(), Mockito.anyString());
    utils.prepareCompanyTaxes(5);
    utils.prepareStateMachine(stateMachineFactory);
    DObServicePurchaseOrder purchaseOrder = utils.preparePurchaseOrderFactory(purchaseOrderFactory);
    DObPurchaseOrderCreateValueObject valueObject =
        utils.prepareValueObject(OrderTypeEnum.SERVICE_PO);

    try {
      //    Act
      command.executeCommand(valueObject);
      //    Assert
      utils.assertStateMachineCalled(stateMachineFactory);
      utils.assertPurchaseOrderValuesSet(purchaseOrder);
      utils.assertPurchaseOrderCreated();
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }
}
