package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.costing.apis.IDObCostingActionNames;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.DObCostingAccountingDetailsCreateService;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.entities.AccountingDocumentAccountingDetailsUtils;
import com.ebs.entities.VendorInvoiceUtils;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingMockUtils;
import com.ebs.repositories.VendorInvoiceGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingRepUtils;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DObCostingActivateCommandTest {
  DObCostingActivateCommand costingActivateCommand;
  CommandTestUtils commandTestUtils;
  @Mock DObCostingStateMachine costingStateMachine;
  @Mock DObCostingAccountingDetailsCreateService costingAccountingDetailsCreateService;

  @Before
  public void setUp() throws Exception {
    costingActivateCommand = new DObCostingActivateCommand(costingStateMachine);
    costingActivateCommand.setCostingRep(CostingRepUtils.getRepository());
    commandTestUtils = new CommandTestUtils();
    costingActivateCommand.setUserAccountSecurityService(
        commandTestUtils.getUserAccountSecurityService());
    costingActivateCommand.setCostingDetailsGeneralModelRep(
        CostingDetailsGeneralModelRepMockUtils.getRepository());
    costingActivateCommand.setVendorInvoiceGeneralModelRep(
        VendorInvoiceGeneralModelRepMockUtils.getRepository());
    costingActivateCommand.setCostingAccountingDetailsCreateService(
        costingAccountingDetailsCreateService);
  }

  @Test
  public void should_UpdateModificationInfoAndDateAndState() throws Exception {
    // Prepare

    String costingUserCode = "20210000001";
    String currencyPriceActualTime = "17.42";
    String purchaseOrderCode = "2021000005";
    String vendorInvoiceUserCode = "2021000006";

    DObCostingActivateValueObject valueObject = new DObCostingActivateValueObject();
    valueObject.setUserCode(costingUserCode);
    DObCosting costingDocument = CostingMockUtils.mockEntity().userCode(costingUserCode).build();
    CostingRepUtils.findOneByUserCode(costingDocument);
    commandTestUtils.mockUserAccount("ADMIN");
    Mockito.doReturn(true).when(costingStateMachine).canFireEvent(IDObCostingActionNames.ACTIVATE);

    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        CostingDetailsMockUtils.mockGeneralModel()
            .userCode(costingUserCode)
            .purchaseOrderCode(purchaseOrderCode)
            .currencyPriceActualTime(new BigDecimal(currencyPriceActualTime))
            .build();
    CostingDetailsGeneralModelRepMockUtils.findOneByUserCode(costingDetailsGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .userCode(vendorInvoiceUserCode)
            .purchaseOrderCode(purchaseOrderCode)
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);
    List<IObAccountingDocumentAccountingDetails> accountingDetailsList =
        mockAccountingDetailsReturn();

    Mockito.when(
            costingAccountingDetailsCreateService.getCostingAccountingDetails(
                vendorInvoiceGeneralModel,
                costingDetailsGeneralModel.getCurrencyPriceActualTime(),
                purchaseOrderCode))
        .thenReturn(accountingDetailsList);
    // Act
    costingActivateCommand.executeCommand(valueObject);
    // Assert
    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(costingUserCode);
    Mockito.verify(CostingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByUserCode(costingUserCode);
    Mockito.verify(VendorInvoiceGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
        .findOneByPurchaseOrderCode(purchaseOrderCode);
    Mockito.verify(costingAccountingDetailsCreateService, Mockito.times(1))
        .getCostingAccountingDetails(
            vendorInvoiceGeneralModel,
            costingDetailsGeneralModel.getCurrencyPriceActualTime(),
            purchaseOrderCode);
    Mockito.verify(costingDocument, Mockito.times(1))
        .setLinesDetails(IDObCosting.accountingDetailsAttr, accountingDetailsList);
    Mockito.verify(costingStateMachine, Mockito.times(1))
        .canFireEvent(IDObCostingActionNames.ACTIVATE);
    Mockito.verify(costingStateMachine, Mockito.times(1))
        .fireEvent(IDObCostingActionNames.ACTIVATE);
    Mockito.verify(costingStateMachine, Mockito.times(1)).save();
    Mockito.verify(CostingRepUtils.getRepository(), Mockito.times(1)).update(costingDocument);
  }

  private List<IObAccountingDocumentAccountingDetails> mockAccountingDetailsReturn() {
    List<IObAccountingDocumentAccountingDetails> accountingDetailsList = new ArrayList<>();

    IObAccountingDocumentAccountingDetails accountingDetailsFirst =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(1L)
            .accountingEntry(AccountingEntry.DEBIT.name())
            .amount(new BigDecimal("42520"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .build();

    IObAccountingDocumentAccountingDetails accountingDetailsSecond =
        AccountingDocumentAccountingDetailsUtils.newEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .glAccountId(1L)
            .accountingEntry(AccountingEntry.CREDIT.name())
            .amount(new BigDecimal("1570"))
            .objectTypeCode(IDocumentTypes.COSTING)
            .subAccountId(2L)
            .build();
    accountingDetailsList.add(accountingDetailsFirst);
    accountingDetailsList.add(accountingDetailsSecond);
    return accountingDetailsList;
  }

  @After
  public void tearDown() throws Exception {
    CostingRepUtils.resetRepository();
    CostingDetailsGeneralModelRepMockUtils.resetRepository();
    VendorInvoiceGeneralModelRepMockUtils.resetRepository();
    Mockito.reset(costingStateMachine);
    Mockito.reset(commandTestUtils.getUserAccountSecurityService());
  }
}
