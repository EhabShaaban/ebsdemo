package com.ebs.dda.commands.accounting.notesreceivable;

import com.ebs.dda.commands.accounting.notesreceivable.utils.IObNotesReceivableRefDocumentDeleteCommandTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IObNotesReceivableRefDocumentDeleteCommandTest {
  IObNotesReceivableRefDocumentDeleteCommandTestUtils utils;

  @Before
  public void init() throws Exception {
    utils = new IObNotesReceivableRefDocumentDeleteCommandTestUtils();
  }

  @Test
  public void Should_DeleteNRRefDocument() throws Exception {
    utils
        .whenExecuteReferenceDocumentDeleteCommand("2021000001", 1L, "NOTES_RECEIVABLE")
        .thenAssertNotesReceivableRefDocumentDeleted();
  }

  @Test
  public void Should_DeleteSalesOrderRefDocument() {
    utils
        .whenExecuteReferenceDocumentDeleteCommand("2021000001", 1L, "SALES_ORDER")
        .thenAssertSalesOrderRefDocumentDeleted();
  }

  @Test
  public void Should_DeleteSalesInvoiceRefDocument() {
    utils
        .whenExecuteReferenceDocumentDeleteCommand("2021000001", 1L, "SALES_INVOICE")
        .thenAssertSalesInvoiceRefDocumentDeleted();
  }
}
