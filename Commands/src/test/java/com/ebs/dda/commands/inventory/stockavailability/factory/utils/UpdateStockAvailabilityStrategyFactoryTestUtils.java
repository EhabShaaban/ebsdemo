package com.ebs.dda.commands.inventory.stockavailability.factory.utils;

import com.ebs.dda.commands.inventory.stockavailability.factory.UpdateStockAvailabilityStrategyFactory;
import com.ebs.dda.commands.inventory.stockavailability.strategies.IUpdateStockAvailabilityStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsIssueStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsReceiptStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByInitialStockUploadStrategy;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.mockito.Mockito;


public class UpdateStockAvailabilityStrategyFactoryTestUtils {

  private UpdateStockAvailabilityByGoodsIssueStrategy goodsIssueStrategy;
  private UpdateStockAvailabilityByGoodsReceiptStrategy goodsReceiptStrategy;
  private UpdateStockAvailabilityByInitialStockUploadStrategy initialStockUploadStrategy;
  private UpdateStockAvailabilityStrategyFactory factory;
  private IUpdateStockAvailabilityStrategy strategy;
  private IllegalArgumentException exception;

  public UpdateStockAvailabilityStrategyFactoryTestUtils() {
    mockDependencies();
    initFactory();
  }

  private void initFactory() {
    factory = new UpdateStockAvailabilityStrategyFactory();
    factory.setByInitialStockUploadStrategy(initialStockUploadStrategy);
    factory.setByGoodsReceiptStrategy(goodsReceiptStrategy);
    factory.setByGoodsIssueStrategy(goodsIssueStrategy);
  }

  private void mockDependencies() {
    initialStockUploadStrategy =
        Mockito.mock(UpdateStockAvailabilityByInitialStockUploadStrategy.class);
    goodsIssueStrategy = Mockito.mock(UpdateStockAvailabilityByGoodsIssueStrategy.class);
    goodsReceiptStrategy = Mockito.mock(UpdateStockAvailabilityByGoodsReceiptStrategy.class);
  }

  public UpdateStockAvailabilityStrategyFactoryTestUtils whenStrategyFetchedWithType(String type) {
    try {
    strategy = factory.getStockAvailabilityUpdateStrategyByType(type);
    } catch (IllegalArgumentException ex) {
      exception = ex;
    }
    return this;
  }

  public void thenAssertGoodsIssueStrategyReturned() {
    Assert.isTrue(
        strategy instanceof UpdateStockAvailabilityByGoodsIssueStrategy,
        "Wrong strategy fetched");
  }

  public void thenAssertGoodsReceiptStrategyReturned() {
    Assert.isTrue(
            strategy instanceof UpdateStockAvailabilityByGoodsReceiptStrategy,
            "Wrong strategy fetched");
  }

  public void thenAssertInitialStockUploadStrategyReturned() {
    Assert.isTrue(
            strategy instanceof UpdateStockAvailabilityByInitialStockUploadStrategy,
            "Wrong strategy fetched");
  }

  public void thenAssertExceptionThrown() {
    Assert.isNotNull(
            exception,
            "Exception wasn't thrown");
  }
}
