package com.ebs.dda.commands.accounting.landedcost;

import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import com.ebs.dda.commands.accounting.landedcost.utils.DObLandedCostUpdateActualCostCommandTestUtils;

@RunWith(JUnit4.class)
public class DObLandedCostUpdateActualCostCommandTest {

  private DObLandedCostUpdateActualCostCommandTestUtils utils;
  
  @Before
  public void setup() {
    utils = new DObLandedCostUpdateActualCostCommandTestUtils();
  }
  
  @Test
  public void Should_SetActualOfExistEstimatedCostFactorItems() throws Exception {

    utils
      .purhcaseOrderCode("2021000002")
      .landedCost(8L, "2021000008")

      .itemMasterData(6L, "000006")
      .savedCostFactorItem(BigDecimal.valueOf(4000), null)
      .inputCostFactorItem(BigDecimal.valueOf(4250.352644))

      .itemMasterData(7L, "000007")
      .savedCostFactorItem(BigDecimal.valueOf(500), null)
      .inputCostFactorItem(BigDecimal.valueOf(500))
      
      .referenceDocumentId(15L)
      .verifySetActualOfExistEstimatedCostFactorItem();
  }

  @Test
  public void Should_SetActualOfNonExistCostFactorItem() throws Exception {

    utils
      .purhcaseOrderCode("2021000002")
      .landedCost(8L, "2021000008")
      .itemMasterData(6L, "000006")
      .inputCostFactorItem(BigDecimal.valueOf(4250.352644))
      .referenceDocumentId(15L)
      .verifySetActualOfNonExistCostFactorItem();
  }

  @Test
  public void Should_UpdateActualOfAlreadyActualCostFactorItem() throws Exception {
    
    utils
    .purhcaseOrderCode("2021000002")
    .landedCost(8L, "2021000008")
    .itemMasterData(6L, "000006")
    .savedCostFactorItem(BigDecimal.valueOf(4000), BigDecimal.valueOf(2350.658946))
    .inputCostFactorItem(BigDecimal.valueOf(4250.352644))
    .referenceDocumentId(15L)
    .verifyUpdateActualOfAlreadyActualCostFactorItem();
  }


}
