package com.ebs.dda.commands.inventory.stockavailability.mocks.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModel;

import java.math.BigDecimal;

public class IObGoodsReceiptItemQuantityGeneralModelMock
    extends IObGoodsReceiptItemQuantityGeneralModel {
  private String userCode;
  private String itemCode;
  private String stockType;
  private String unitOfMeasureCode;
  private String purchaseUnitCode;
  private String companyCode;
  private String storeHouseCode;
  private String plantCode;

  public IObGoodsReceiptItemQuantityGeneralModelMock(
      String userCode,
      String itemCode,
      String plantCode,
      String storehouseCode,
      String companyCode,
      String purchaseUnitCode,
      String unitOfMeasureCode,
      BigDecimal quantity,
      String stockType) {
    this.userCode = userCode;
    this.itemCode = itemCode;
    this.plantCode = plantCode;
    this.storeHouseCode = storehouseCode;
    this.companyCode = companyCode;
    this.purchaseUnitCode = purchaseUnitCode;
    this.unitOfMeasureCode = unitOfMeasureCode;
    this.stockType = stockType;
    setQuantity(quantity);
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getItemCode() {
    return itemCode;
  }

  @Override
  public String getStockType() {
    return stockType;
  }

  @Override
  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  @Override
  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  @Override
  public String getCompanyCode() {
    return companyCode;
  }

  @Override
  public String getStoreHouseCode() {
    return storeHouseCode;
  }

  @Override
  public String getPlantCode() {
    return plantCode;
  }
}
