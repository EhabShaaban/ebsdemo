package com.ebs.dda.commands.inventory.stockavailability.strategies;

import com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByGoodsIssueStrategyTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.ebs.dda.commands.inventory.stockavailability.strategies.utils.UpdateStockAvailabilityByGoodsIssueStrategyTestUtils.goodsIssueItem;

@RunWith(MockitoJUnitRunner.class)
public class UpdateStockAvailabilityByGoodsIssueStrategyTest {

  private UpdateStockAvailabilityByGoodsIssueStrategyTestUtils utils;

  @Before
  public void init() {
    utils = new UpdateStockAvailabilityByGoodsIssueStrategyTestUtils();
  }

  @Test
  public void Should_FetchItemsFromRepositoryAndCallUpdateServiceWithItemsList() {
    utils
        .withUserCode("2021000001")
        .withGoodsIssueItems(
            goodsIssueItem(
                "0001", "0001", "0001", "0001", "0053", "0001", "2021000001", BigDecimal.TEN),
            goodsIssueItem(
                "0001",
                "0001",
                "0001",
                "0001",
                "0053",
                "0001",
                "2021000001",
                new BigDecimal("5.0")))
        .whenUpdate()
        .thenAssertRepositoryWasCalledWithUserCode("2021000001")
        .thenAssertUpdateServiceWasCalledWithItemsList()
        .thenAssertItemQuantitiesAreNegative();
  }
}
