package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dda.commands.accounting.paymentrequest.DObDownPaymentCreateCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPurchaseOrderPaymentDetails;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRepUtils;
import com.ebs.repositories.masterdata.vendor.MObVendorRepUtils;
import org.junit.jupiter.api.Assertions;

public class DObDownPaymentCreateCommandTestUtils
				extends DObPaymentRequestCreateCommandTestUtils<DObDownPaymentCreateCommand> {

	public DObDownPaymentCreateCommandTestUtils() {
		super(DObDownPaymentCreateCommand.class);
	}

	@Override
	protected void preparePaymentTypeCommandDependencies(
					DObPaymentRequestCreateValueObject valueObject,
					DObDownPaymentCreateCommand command) {
		preparePaymentReferenceOrderRep(command);
		prepareVendorRep(valueObject, command);
	}

	private void prepareVendorRep(DObPaymentRequestCreateValueObject valueObject,
					DObDownPaymentCreateCommand command) {
		MObVendor vendor = VendorMockUtils.buildMObVendor(valueObject.getBusinessPartnerCode(), 1L);
		MObVendorRepUtils.findOneByUserCode(vendor);
		MObVendorRep repository = MObVendorRepUtils.getRepository();
		command.setVendorRep(repository);
	}

	private void preparePaymentReferenceOrderRep(DObDownPaymentCreateCommand command) {
		DObPaymentReferenceOrderGeneralModel paymentReferenceOrderGeneralModel = PaymentUtils
						.buildPaymentReferenceOrderGeneralModel();
		DObPaymentReferenceOrderGeneralModelRepUtils
						.findOneByUserCode(paymentReferenceOrderGeneralModel);
		DObPaymentReferenceOrderGeneralModelRep repository = DObPaymentReferenceOrderGeneralModelRepUtils
						.getRepository();
		command.setRefDocumentRep(repository);
	}
	public void assertThatReferenceDocumentId(Long referenceDocumentId) {
		IObPaymentRequestPurchaseOrderPaymentDetails paymentDetails = (IObPaymentRequestPurchaseOrderPaymentDetails) getCreatedObject().getHeaderDetail(IDObPaymentRequest.paymentdetailsAttr);
		Assertions.assertEquals(paymentDetails.getDueDocumentId(), referenceDocumentId);
	}
}
