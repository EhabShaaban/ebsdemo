package com.ebs.dda.commands.accounting.notesreceivable.utils;

import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.notesreceivables.IObNotesReceivableRefDocumentUpdateRemainingCommand;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateNRRemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateSIRemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.NotesReceivableRefDocumentUpdateSORemainingHandler;
import com.ebs.dda.commands.accounting.notesreceivables.factory.NotesReceivablesReferenceDocumentUpdateRemainingFactory;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObMonetaryNotesRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.entities.ExchangeRateMockUtils;
import com.ebs.entities.accounting.monetarynotes.DObNotesReceivableMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableReferenceDocumentMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceSummaryMockUtils;
import com.ebs.entities.accounting.salesinvoice.SalesInvoiceBusinessPartnerMockUtils;
import com.ebs.entities.order.salesorder.DObSalesOrderMockUtils;
import com.ebs.entities.order.salesorder.IObSalesOrderDataMockUtils;
import com.ebs.repositories.ExchangeRateRepMockUtils;
import com.ebs.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.salesInvoice.DObSalesInvoiceRepMockUtils;
import com.ebs.repositories.accounting.salesInvoice.SalesInvoiceBusinessPartnerGeneralModelRepMockUtils;
import com.ebs.repositories.order.salesorder.DObSalesOrderRepMockUtils;
import com.ebs.repositories.order.salesorder.IObSalesOrderDataGeneralModelRepMockUtils;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils {
  private IObNotesReceivableRefDocumentUpdateRemainingCommand command;
  private CommandTestUtils commandTestUtils;
  private String notesReceivableCode;
  private String notesReceivableCurrencyIso;

  private IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;
  private DObMonetaryNotesRep notesReceivablesRep;
  private IObMonetaryNotesDetailsRep notesDetailsRep;
  private NotesReceivablesReferenceDocumentUpdateRemainingFactory
      notesReceivablesReferenceDocumentUpdateRemainingFactory;
  private NotesReceivableRefDocumentUpdateNRRemainingHandler
      notesReceivableRefDocumentUpdateNRRemainingHandler;
  private NotesReceivableRefDocumentUpdateSORemainingHandler
      notesReceivableRefDocumentUpdateSORemainingHandler;
  private NotesReceivableRefDocumentUpdateSIRemainingHandler
      notesReceivableRefDocumentUpdateSIRemainingHandler;
  private List<IObNotesReceivablesReferenceDocumentGeneralModel>
      notesReceivablesReferenceDocumentGeneralModelList;

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils() throws Exception {
    notesReceivablesReferenceDocumentGeneralModelList = new ArrayList<>();
    resetRepository();
    mockDependencies();
    prepareData();
    initNotesReceivableRefDocumentUpdateNRRemainingHandler();
    initNotesReceivableRefDocumentUpdateSORemainingHandler();
    initNotesReceivableRefDocumentUpdateSIRemainingHandler();
    initNotesReceivablesReferenceDocumentUpdateRemainingFactory();
    initCommand();
  }

  private void resetRepository() {
    ExchangeRateRepMockUtils.resetRepository();
    DObNotesReceivablesGeneralModelRepMockUtils.reset();
    IObSalesOrderDataGeneralModelRepMockUtils.resetRepository();
    SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.resetRepository();
  }

  private void prepareData() {
    prepareNotesReceivableRefDocument(notesReceivablesRep);
    prepareIObNotesReceivableRefDocumentDetails(notesDetailsRep);
    prepareSalesOrderRefDocument();
    prepareSalesInvoiceRefDocument();
  }

  private void prepareNotesReceivableRefDocument(DObMonetaryNotesRep notesReceivablesRep) {
    DObMonetaryNotes monetaryNotes = DObNotesReceivableMockUtils.mockEntity().id(1L).build();
    DObNotesReceivablesGeneralModel notesReceivables = DObNotesReceivableMockUtils.mockGeneralModel()
            .id(1L)
            .currencyIso("EGP")
            .build();

    Mockito.when(notesReceivablesRep.findOneByUserCode("2021000003")).thenReturn(monetaryNotes);
    DObNotesReceivablesGeneralModelRepMockUtils.findById(notesReceivables);
  }

  private void prepareSalesOrderRefDocument() {
    DObSalesOrder salesOrder =
        DObSalesOrderMockUtils.newEntity().id(1L).userCode("2021000001").remaining(new BigDecimal("1500.0")).build();
    IObSalesOrderDataGeneralModel salesOrderDataGeneralModel = IObSalesOrderDataMockUtils.mockGeneralModel()
            .salesOrderId(1L)
            .currencyIso("USD")
            .build();

    DObSalesOrderRepMockUtils.findOneByUserCode(salesOrder);
    IObSalesOrderDataGeneralModelRepMockUtils.findOneByRefInstanceId(salesOrderDataGeneralModel);
  }

  private void prepareSalesInvoiceRefDocument() {
    DObSalesInvoiceMockUtils.withSalesInvoiceId(1L);
    DObSalesInvoiceMockUtils.withSalesInvoiceCode("2021000001");
    DObSalesInvoice salesInvoice = DObSalesInvoiceMockUtils.buildEntity();
    IObSalesInvoiceSummary iObSalesInvoiceSummary =
        IObSalesInvoiceSummaryMockUtils.newEntity()
            .refInstanceId(1L)
            .remaining(BigDecimal.valueOf(5100))
            .build();
    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartner =
            SalesInvoiceBusinessPartnerMockUtils.mockGeneralModel().salesInvoiceId(1L).currencyIso("EGP").build();

    DObSalesInvoiceRepMockUtils.findOneByUserCode(salesInvoice);
    DObSalesInvoiceRepMockUtils.findIObSalesInvoiceSummaryBySiId(
        iObSalesInvoiceSummary, salesInvoice);
    SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.findOneByRefInstanceId(salesInvoiceBusinessPartner);
  }

  private void prepareIObNotesReceivableRefDocumentDetails(
      IObMonetaryNotesDetailsRep notesDetailsRep) {
    IObMonetaryNotesDetails notesDetails = new IObMonetaryNotesDetails();
    notesDetails.setRemaining(new BigDecimal(14000));
    notesDetails.setRefInstanceId(1L);
    Mockito.when(notesDetailsRep.findOneByRefInstanceId(1L)).thenReturn(notesDetails);
  }

  private void mockDependencies() {
    commandTestUtils = new CommandTestUtils();
    notesReceivablesReferenceDocumentGeneralModelRep =
        Mockito.mock(IObNotesReceivablesReferenceDocumentGeneralModelRep.class);
    notesReceivablesRep = Mockito.mock(DObMonetaryNotesRep.class);
    notesDetailsRep = Mockito.mock(IObMonetaryNotesDetailsRep.class);
  }

  private void initCommand() {
    command = new IObNotesReceivableRefDocumentUpdateRemainingCommand();
    command.setNotesReceivablesReferenceDocumentUpdateRemainingFactory(
        notesReceivablesReferenceDocumentUpdateRemainingFactory);
    command.setNotesReceivablesReferenceDocumentGeneralModelRep(
        notesReceivablesReferenceDocumentGeneralModelRep);
  }

  private void initNotesReceivablesReferenceDocumentUpdateRemainingFactory() {
    notesReceivablesReferenceDocumentUpdateRemainingFactory =
        new NotesReceivablesReferenceDocumentUpdateRemainingFactory();
    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateNRRemainingHandler(
            notesReceivableRefDocumentUpdateNRRemainingHandler);
    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateSORemainingHandler(
            notesReceivableRefDocumentUpdateSORemainingHandler);
    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateSIRemainingHandler(
            notesReceivableRefDocumentUpdateSIRemainingHandler);
  }

  private void initNotesReceivableRefDocumentUpdateNRRemainingHandler() {
    notesReceivableRefDocumentUpdateNRRemainingHandler =
        new NotesReceivableRefDocumentUpdateNRRemainingHandler();
    notesReceivableRefDocumentUpdateNRRemainingHandler.setUserAccountSecurityService(
        commandTestUtils.getUserAccountSecurityService());
    notesReceivableRefDocumentUpdateNRRemainingHandler.setDObMonetaryNotesRep(notesReceivablesRep);
    notesReceivableRefDocumentUpdateNRRemainingHandler.setIObMonetaryNotesDetailsRep(
        notesDetailsRep);
    notesReceivableRefDocumentUpdateNRRemainingHandler.setExchangeRateRep(ExchangeRateRepMockUtils.getRepository());
    notesReceivableRefDocumentUpdateNRRemainingHandler.setNotesReceivablesGeneralModelRep(
            DObNotesReceivablesGeneralModelRepMockUtils.getRepository());
  }

  private void initNotesReceivableRefDocumentUpdateSORemainingHandler() {
    notesReceivableRefDocumentUpdateSORemainingHandler =
        new NotesReceivableRefDocumentUpdateSORemainingHandler();
    notesReceivableRefDocumentUpdateSORemainingHandler.setUserAccountSecurityService(
        commandTestUtils.getUserAccountSecurityService());
    notesReceivableRefDocumentUpdateSORemainingHandler.setDObSalesOrderRep(
        DObSalesOrderRepMockUtils.getRepository());
    notesReceivableRefDocumentUpdateSORemainingHandler.setExchangeRateRep(ExchangeRateRepMockUtils.getRepository());
    notesReceivableRefDocumentUpdateSORemainingHandler.setSalesOrderDataGeneralModelRep(
            IObSalesOrderDataGeneralModelRepMockUtils.getRepository());
  }

  private void initNotesReceivableRefDocumentUpdateSIRemainingHandler() {
    notesReceivableRefDocumentUpdateSIRemainingHandler =
        new NotesReceivableRefDocumentUpdateSIRemainingHandler();
    notesReceivableRefDocumentUpdateSIRemainingHandler.setUserAccountSecurityService(
        commandTestUtils.getUserAccountSecurityService());
    notesReceivableRefDocumentUpdateSIRemainingHandler.setDObSalesInvoiceRep(
        DObSalesInvoiceRepMockUtils.getDObSalesInvoiceRep());
    notesReceivableRefDocumentUpdateSIRemainingHandler.setIObSalesInvoiceSummaryRep(
        DObSalesInvoiceRepMockUtils.getIObSalesInvoiceSummaryRep());
    notesReceivableRefDocumentUpdateSIRemainingHandler.setExchangeRateRep(ExchangeRateRepMockUtils.getRepository());
    notesReceivableRefDocumentUpdateSIRemainingHandler.setSalesInvoiceBusinessPartnerRep(
            SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.getRepository());
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils withNotesReceivableCode(
      String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils withNotesReceivableCurrencyIso(
          String notesReceivableCurrencyIso) {
    this.notesReceivableCurrencyIso = notesReceivableCurrencyIso;
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils whenExecute() {
    DObNotesReceivableUpdateRefDocumentRemainingValueObject valueObject =
        new DObNotesReceivableUpdateRefDocumentRemainingValueObject();
    valueObject.setNotesReceivableCode(notesReceivableCode);
    try {
      command.executeCommand(valueObject);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils withReferenceDocumentData(
      String refDocumentCode,
      String notesReceivableCode,
      BigDecimal amountToCollect,
      String documentType) {

    IObNotesReceivablesReferenceDocumentGeneralModel notesReceivablesReferenceDocumentGeneralModel =
        IObNotesReceivableReferenceDocumentMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refDocumentCode(refDocumentCode)
            .amountToCollect(amountToCollect)
            .documentType(documentType)
            .currencyIso(notesReceivableCurrencyIso)
            .build();

    notesReceivablesReferenceDocumentGeneralModelList.add(
        notesReceivablesReferenceDocumentGeneralModel);
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils
      withReturnedReferenceDocumentData() {
    Mockito.doReturn(notesReceivablesReferenceDocumentGeneralModelList)
        .when(notesReceivablesReferenceDocumentGeneralModelRep)
        .findByNotesReceivableCodeOrderByIdAsc(notesReceivableCode);
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils withUser(String userName) {
    commandTestUtils.mockUserAccount(userName);
    return this;
  }

  public IObNotesReceivableRefDocumentUpdateRemainingCommandTestUtils withExchangeRateData(String nrCurrencyIso,
                                                                                           String refDocCurrencyIso,
                                                                                           BigDecimal amount) {
    CObExchangeRateGeneralModel exchangeRate =
            ExchangeRateMockUtils.mockGeneralModel()
                    .firstCurrencyIso(nrCurrencyIso)
                    .secondCurrencyIso(refDocCurrencyIso)
                    .secondValue(amount)
                    .build();
    ExchangeRateRepMockUtils.findLatestDefaultExchangeRate(exchangeRate, nrCurrencyIso, refDocCurrencyIso);
    return this;
  }

  public void thenAssertNotesReceivableRemainingIsUpdatedAs(BigDecimal expectedRemaining) {
    IObMonetaryNotesDetails refDocumentDetails = notesDetailsRep.findOneByRefInstanceId(1L);
    Assert.isEqual(
        0,
        refDocumentDetails.getRemaining().compareTo(expectedRemaining),
        "NotesReceivable Remaining is not updated correctly");
  }

  public void thenAssertSalesOrderRemainingIsUpdatedAs(BigDecimal expectedRemaining) {
    DObSalesOrder salesOrder =
        DObSalesOrderRepMockUtils.getRepository().findOneByUserCode("2021000001");
    BigDecimal remaining = salesOrder.getRemaining();
    Assert.isEqual(
        0, remaining.compareTo(expectedRemaining), "SalesOrder Remaining is not updated correctly");
  }

  public void thenAssertSalesInvoiceRemainingIsUpdatedAs(BigDecimal expectedRemaining) {
    DObSalesInvoice salesInvoice =
        DObSalesInvoiceRepMockUtils.getDObSalesInvoiceRep().findOneByUserCode("2021000001");
    Long salesInvoiceId = salesInvoice.getId();
    IObSalesInvoiceSummary salesInvoiceSummary =
        DObSalesInvoiceRepMockUtils.getIObSalesInvoiceSummaryRep()
            .findOneByRefInstanceId(salesInvoiceId);

    BigDecimal remaining = salesInvoiceSummary.getRemaining();
    Assert.isEqual(
        0,
        remaining.compareTo(expectedRemaining),
        "SalesInvoice Remaining is not updated correctly");
  }
}
