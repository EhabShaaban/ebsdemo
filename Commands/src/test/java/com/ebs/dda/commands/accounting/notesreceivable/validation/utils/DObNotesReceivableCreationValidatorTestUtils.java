package com.ebs.dda.commands.accounting.notesreceivable.validation.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivableCreationValidator;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesCreateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.MonetaryNoteFormEnum;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableTypeEnum;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.entities.CompanyMockUtils;
import com.ebs.entities.DocumentOwnerTestUtils;
import com.ebs.entities.masterdata.businessunit.BusinessUnitTestUtils;
import com.ebs.entities.masterdata.currency.CurrencyTestUtils;
import com.ebs.entities.masterdata.customer.IObCustomerBusinessUnitTestUtils;
import com.ebs.repositories.CompanyRepMockUtils;
import com.ebs.repositories.DocumentOwnerRepTestUtils;
import com.ebs.repositories.masterdata.businessunit.BusinessUnitRepTestUtils;
import com.ebs.repositories.masterdata.currency.CurrencyRepTestUtils;
import com.ebs.repositories.masterdata.customer.CustomerBusinessUnitRepTestUtils;
import org.junit.Assert;

import java.math.BigDecimal;

public class DObNotesReceivableCreationValidatorTestUtils {

  private static final String EXIST_BUSINES_UNIT_CODE = "0002";
  private static final String EXIST_COMPANY_CODE = "0002";
  private static final String EXIST_BUSINESS_PARTNER_CODE = "000007";
  private static final Long EXIST_DOCUMENT_OWNER_ID = 1L;
  private static final String EXIST_ISO = "EGP";

  private DObNotesReceivablesCreateValueObject valueObject;
  private DObNotesReceivableCreationValidator validator;

  public DObNotesReceivableCreationValidatorTestUtils() {
    valueObject = new DObNotesReceivablesCreateValueObject();
    validator = new DObNotesReceivableCreationValidator();
  }

  public DObNotesReceivableCreationValidatorTestUtils noteForm(MonetaryNoteFormEnum noteForm) {
    valueObject.setNoteForm(noteForm);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils type(NotesReceivableTypeEnum type) {
    valueObject.setType(type);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils businessUnitCode(String businessUnitCode) {
    valueObject.setBusinessUnitCode(businessUnitCode);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils companyCode(String companyCode) {
    valueObject.setCompanyCode(companyCode);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils businessPartnerCode(
    String businessPartnerCode) {
    valueObject.setBusinessPartnerCode(businessPartnerCode);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils documentOwnerId(Long documentOwnerId) {
    valueObject.setDocumentOwnerId(documentOwnerId);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils amount(BigDecimal amount) {
    valueObject.setAmount(amount);
    return this;
  }

  public DObNotesReceivableCreationValidatorTestUtils currency(String currency) {
    valueObject.setCurrencyIso(currency);
    return this;
  }

  public void throwsArgumentViolationSecurityException() throws Exception {
    try {
      ValidationResult validationResult = executeValidate();
      Assert.assertFalse(validationResult.isValid());
      Assert.fail();
    } catch (ArgumentViolationSecurityException e) {
    }
  }

  public void validateSuccessfully() throws Exception {
    ValidationResult validationResult = executeValidate();
    Assert.assertTrue(validationResult.isValid());
  }

  private ValidationResult executeValidate() throws Exception {
    setupMocks();
    return validator.validate(valueObject);
  }

  private void setupMocks() {
    mockBusinessUnitRep();
    mockCompanyRep();
    mockBusinessPartnerRep();
    mockDocumentOwnerRep();
    mockCurrencyRep();
  }

  private void mockBusinessUnitRep() {
    CObPurchasingUnitGeneralModel businessUnit = BusinessUnitTestUtils
      .newGeneralModel()
      .userCode(EXIST_BUSINES_UNIT_CODE)
      .build();

    BusinessUnitRepTestUtils.mockFindBusinessUnitByCode(businessUnit, EXIST_BUSINES_UNIT_CODE);
    validator.setBusinessUnitRep(BusinessUnitRepTestUtils.getRepository());
  }

  private void mockCompanyRep() {
    CObCompanyGeneralModel company = CompanyMockUtils
      .mockGeneralModel()
      .userCode(EXIST_COMPANY_CODE)
      .build();

    CompanyRepMockUtils.findOneByUserCode(company);

    validator.setCompanyRep(CompanyRepMockUtils.getRepository());
  }

  private void mockBusinessPartnerRep() {
    IObCustomerBusinessUnitGeneralModel customer = IObCustomerBusinessUnitTestUtils
      .newGeneralModel()
      .businessUnitCode(EXIST_BUSINES_UNIT_CODE)
      .build();

    CustomerBusinessUnitRepTestUtils
      .mockFindCustomerByCodeAndBusinessUnitCode(customer, EXIST_BUSINESS_PARTNER_CODE,
        EXIST_BUSINES_UNIT_CODE);

    validator.setBusinessPartnerRep(CustomerBusinessUnitRepTestUtils.getRepository());
  }

  private void mockDocumentOwnerRep() {

    DocumentOwnerGeneralModel docmentOwner = DocumentOwnerTestUtils
      .mockGeneralModel()
      .userId(EXIST_DOCUMENT_OWNER_ID)
      .build();

    DocumentOwnerRepTestUtils.mockFindByUserIdAndObjectName(docmentOwner, EXIST_DOCUMENT_OWNER_ID,
      IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME);

    validator.setDocumentOwnerRep(DocumentOwnerRepTestUtils.getRepository());
  }

  private void mockCurrencyRep() {

    CObCurrency currency = CurrencyTestUtils
      .mockEntity()
      .iso(EXIST_ISO)
      .build();

    CurrencyRepTestUtils.mockFindByIso(currency, EXIST_ISO);

    validator.setCurrencyRep(CurrencyRepTestUtils.getRepository());
  }

}
