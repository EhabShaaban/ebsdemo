package com.ebs.dda.commands.accounting.payment.utils;

import java.math.BigDecimal;

import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentUpdateVendorInvoiceRemainingCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.entities.accounting.VendorInvoiceUtils;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.repositories.accounting.vendorinvoice.DObVendorInvoiceRepMockUtils;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import com.ebs.repositories.payment.PaymentRequestRepUtils;

public class DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils {

    private DObPaymentUpdateVendorInvoiceRemainingCommand command;
    private DObPaymentRequestActivateValueObject valueObject;

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils() {
        valueObject = new DObPaymentRequestActivateValueObject();
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils withPaymentCode(String paymentCode) {
        PaymentUtils.withPaymentCode(paymentCode);
        valueObject.setPaymentRequestCode(paymentCode);
        return this;
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils withAmount(BigDecimal paymentAmount) {
        PaymentUtils.withAmount(paymentAmount);
        return this;
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils withVendorInvoiceCode(String vendorInvoiceCode) {
        PaymentUtils.withDueDocumentCode(vendorInvoiceCode);
        VendorInvoiceUtils.withVendorInvoiceCode(vendorInvoiceCode);
        return this;
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils whenExecute() throws Exception {
        prepareCommandDependencies();
        command.executeCommand(valueObject);
        return this;
    }

    private void prepareCommandDependencies() {
        command = new DObPaymentUpdateVendorInvoiceRemainingCommand();
        preparePaymentDetails();
        prepareUserAccountSecurityService();
        prepareVendorInvoice();
        prepareVendorInvoiceSummary();
    }

    private void preparePaymentDetails() {
        IObPaymentRequestPaymentDetailsGeneralModel paymentDetailsGeneralModel = PaymentUtils.buildPaymentDetailsGeneralModel();
        PaymentRequestRepUtils.findIObPaymentRequestPaymentDetailsByPaymentCode(paymentDetailsGeneralModel);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(PaymentRequestRepUtils.getPaymentRequestPaymentDetailsRep());
    }

    private void prepareUserAccountSecurityService() {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }

    private void prepareVendorInvoice() {
        DObVendorInvoice vendorInvoice = VendorInvoiceUtils.buildVendorInvoice();
        DObVendorInvoiceRepMockUtils.findOneByVendorInvoiceCode(vendorInvoice);
        command.setVendorInvoiceRep(DObVendorInvoiceRepMockUtils.getRepository());
    }

    private void prepareVendorInvoiceSummary() {
        IObVendorInvoiceSummary vendorInvoiceSummary = VendorInvoiceUtils.buildVendorInvoiceSummary();
        VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);
        command.setVendorInvoiceSummaryRep(VendorInvoiceSummaryRepUtils.getRepository());
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils withVendorInvoiceId(Long vendorInvoiceId) {
        VendorInvoiceUtils.withVendorInvoiceId(vendorInvoiceId);
        return this;
    }

    public DObPaymentUpdateVendorInvoiceRemainingCommandTestUtils withVendorInvoiceRemaining(BigDecimal remaining) {
        VendorInvoiceUtils.withVendorInvoiceRemaining(remaining);
        return this;
    }

	public void assertThatRemainingIsUpdatedTo(BigDecimal updatedRemaining) throws Exception {
		prepareUpdatedVendorInvoice(updatedRemaining);
		ArgumentCaptor<DObVendorInvoice> vendorInvoice = ArgumentCaptor
						.forClass(DObVendorInvoice.class);
		Mockito.verify(DObVendorInvoiceRepMockUtils.getRepository(), Mockito.times(1))
						.update(vendorInvoice.capture());
		ArgumentCaptor<IObVendorInvoiceSummary> updatedVendorInvoiceSummary = ArgumentCaptor
						.forClass(IObVendorInvoiceSummary.class);
		Mockito.verify(vendorInvoice.getValue(), Mockito.times(1)).setHeaderDetail(
						ArgumentMatchers.any(), updatedVendorInvoiceSummary.capture());
		Assert.assertEquals(updatedRemaining
						.compareTo(updatedVendorInvoiceSummary.getValue().getRemaining()), 0);

	}

    private void prepareUpdatedVendorInvoice(BigDecimal updatedRemaining) {
        VendorInvoiceUtils.withVendorInvoiceRemaining(updatedRemaining);
        VendorInvoiceUtils.buildVendorInvoiceSummary();
    }
}
