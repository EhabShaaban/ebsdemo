package com.ebs.dda.commands.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.salesinvoice.utils.DObJournalEntryCreateSalesInvoiceCommandTestUtils;
import com.ebs.dda.jpa.accounting.journalentry.DObSalesInvoiceJournalEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class DObJournalEntryCreateSalesInvoiceCommandTest {

  private DObJournalEntryCreateSalesInvoiceCommandTestUtils utils;

  @Before
  public void setup() {
    utils = new DObJournalEntryCreateSalesInvoiceCommandTestUtils();
  }

  @Test
  public void Should_CreditAllTaxes_WhenAllTaxesArePositive()
      throws Exception {
    utils
        .withSalesInvoice("2021000001")
        .WithJournalEntryCode("2021000001")
        .withSalesAccount(42L, BigDecimal.valueOf(330))
        .withCustomer(11L, 73L, BigDecimal.valueOf(448.80))
        .withTax(43L, 7L, BigDecimal.valueOf(3.3))
        .withTax(44L, 8L, BigDecimal.valueOf(1.65))
        .withTax(45L, 9L, BigDecimal.valueOf(33))
        .withTax(46L, 10L, BigDecimal.valueOf(39.6))
        .withTax(47L, 11L, BigDecimal.valueOf(1.65))
        .withTax(48L, 12L, BigDecimal.valueOf(39.6))
        .whenExecute()
        .assertThatJournalEntriesIsUpdated();
    DObSalesInvoiceJournalEntry createdJE = utils.verifyThatCreateIsCalled();
    utils.assertJournalEntryItems(createdJE,11L, BigDecimal.valueOf(448.80), AccountingEntry.DEBIT.name());
    utils.assertJournalEntryItems(createdJE,43L, BigDecimal.valueOf(3.3), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,42L, BigDecimal.valueOf(330), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,44L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,45L, BigDecimal.valueOf(33), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,46L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,47L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,48L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name());

  }

  @Test
  public void Should_DebitNagativeTaxWithItsAbsoluteValue_WhenThereIsNegativeTax()
      throws Exception {

    utils
        .withSalesInvoice("2021000001")
        .WithJournalEntryCode("2021000001")
        .withSalesAccount(42L, BigDecimal.valueOf(330))
        .withCustomer(11L, 73L, BigDecimal.valueOf(442.20))
        .withTax(43L, 7L, BigDecimal.valueOf(-3.3))
        .withTax(44L, 8L, BigDecimal.valueOf(1.65))
        .withTax(45L, 9L, BigDecimal.valueOf(33))
        .withTax(46L, 10L, BigDecimal.valueOf(39.6))
        .withTax(47L, 11L, BigDecimal.valueOf(1.65))
        .withTax(48L, 12L, BigDecimal.valueOf(39.6))
        .whenExecute()
        .assertThatJournalEntriesIsUpdated();
    DObSalesInvoiceJournalEntry createdJE = utils.verifyThatCreateIsCalled();
    utils.assertJournalEntryItems(createdJE,11L, BigDecimal.valueOf(442.20), AccountingEntry.DEBIT.name());
    utils.assertJournalEntryItems(createdJE,43L, BigDecimal.valueOf(3.3), AccountingEntry.DEBIT.name());
    utils.assertJournalEntryItems(createdJE,42L, BigDecimal.valueOf(330), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,44L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,45L, BigDecimal.valueOf(33), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,46L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,47L, BigDecimal.valueOf(1.65), AccountingEntry.CREDIT.name());
    utils.assertJournalEntryItems(createdJE,48L, BigDecimal.valueOf(39.6), AccountingEntry.CREDIT.name());
  }
}
