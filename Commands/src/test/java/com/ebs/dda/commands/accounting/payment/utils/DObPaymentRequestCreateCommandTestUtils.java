package com.ebs.dda.commands.accounting.payment.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachineTestUtils;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.codegenerator.DocumentObjectCodeGeneratorMockTestUtils;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestCreateCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.masterdata.businessunit.BusinessUnitTestUtils;
import com.ebs.repositories.masterdata.businessunit.CObPurchasingUnitRepTestUtils;
import com.ebs.repositories.payment.PaymentRequestRepUtils;
import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.lang.reflect.Constructor;

public abstract class DObPaymentRequestCreateCommandTestUtils<T extends DObPaymentRequestCreateCommand> {

	private final Class<T> clazz;
    private DObPaymentRequest createdObject;

	public DObPaymentRequestCreateCommandTestUtils(Class<T> clazz) {
		this.clazz = clazz;
		PaymentUtils.resetValue();
	}
	public DObPaymentRequestCreateCommandTestUtils withPaymentForm(String paymentForm) {
		PaymentUtils.withPaymentForm(paymentForm);
		PaymentRequestRepUtils.resetRepository();
		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils withPaymentType(String paymentType) {
		PaymentUtils.withPaymentType(paymentType);
		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils withBusinessUnit(String businessUnit) {
		PaymentUtils.withBusinessUnitCode(businessUnit);

		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils withBusinessPartner(String businessPartner) {
		PaymentUtils.withBusinessPartnerCode(businessPartner);
		return this;
	}
	public DObPaymentRequestCreateCommandTestUtils withReferenceDocument(String documentType, String code,
                                                                         Long companyId, Long currencyId, Long id) {
		PaymentUtils.withDueDocumentType(documentType);
		PaymentUtils.withDueDocumentCode(code);
		PaymentUtils.withDueDocumentCompanyId(companyId);
		PaymentUtils.withDueDocumentCurrencyId(currencyId);
		PaymentUtils.withDueDocumentId(id);
		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils withDocumentOwner(Long documentOwner) {
		PaymentUtils.withDocumentOwnerId(documentOwner);
		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils assertThatPaymentIsCreatedSuccessfully() throws Exception {
		ArgumentCaptor<DObPaymentRequest> createdObjectCaptor = ArgumentCaptor.forClass(DObPaymentRequest.class);
		Mockito.verify(PaymentRequestRepUtils.getPaymentRequestRep(), Mockito.times(1)).create(createdObjectCaptor.capture());
		this.createdObject = createdObjectCaptor.getValue();
		return this;
	}
	protected DObPaymentRequestStateMachine getStateMachine() throws Exception {
		DObPaymentRequestStateMachineTestUtils.initObjectState();
		return DObPaymentRequestStateMachineTestUtils.getStateMachine();
	}
	protected void prepareUserAccountSecurityService(T command) {
		CommandTestUtils commandTestUtils = new CommandTestUtils();
		commandTestUtils.mockUserAccount("admin");
		IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
		command.setUserAccountSecurityService(userAccountSecurityService);
	}
	protected void preparePaymentRequestRep(T command) {
		DObPaymentRequestRep paymentRequestRep = PaymentRequestRepUtils.getPaymentRequestRep();
		command.setPaymentRequestRep(paymentRequestRep);
	}

	protected void prepareDocumentObjectCodeGenerator(T command) {
		DocumentObjectCodeGeneratorMockTestUtils.mockGenerateUserCode("2021000001");
		DocumentObjectCodeGenerator generator = DocumentObjectCodeGeneratorMockTestUtils.getGenerator();
		command.setDocumentObjectCodeGenerator(generator);
	}

	protected void prepareBusinessUnitRep(DObPaymentRequestCreateValueObject valueObject, T command) {
		CObPurchasingUnit businessUnit = BusinessUnitTestUtils.buildEntity(valueObject.getBusinessUnitCode());
		CObPurchasingUnitRepTestUtils.mockFindBusinessUnitByCode(businessUnit, valueObject.getBusinessUnitCode());
		CObPurchasingUnitRep repository = CObPurchasingUnitRepTestUtils.getRepository();
		command.setPurchasingUnitRep(repository);
	}

	protected void prepareCommonCommandDependencies(DObPaymentRequestCreateValueObject valueObject,
											T command) {
		prepareBusinessUnitRep(valueObject, command);
		prepareDocumentObjectCodeGenerator(command);
		preparePaymentRequestRep(command);
		prepareUserAccountSecurityService(command);
	}
	public DObPaymentRequestCreateCommandTestUtils executeCommand() throws Exception {
		DObPaymentRequestCreateValueObject valueObject = PaymentUtils
				.buildPaymentCreateValueObject();
		DObPaymentRequestStateMachine stateMachine = getStateMachine();
		T command = createCommandInstance(stateMachine);
		prepareCommonCommandDependencies(valueObject, command);
		preparePaymentTypeCommandDependencies(valueObject, command);
		command.executeCommand(valueObject);
		return this;
	}

	protected abstract void preparePaymentTypeCommandDependencies(DObPaymentRequestCreateValueObject valueObject, T command);

	private T createCommandInstance(DObPaymentRequestStateMachine stateMachine) throws Exception {
		Constructor<?> constructor = clazz.getConstructor(DObPaymentRequestStateMachine.class);
		return (T) constructor.newInstance(new Object[] {stateMachine});
	}

	public DObPaymentRequestCreateCommandTestUtils assertThatPaymentCompanyId(Long companyId) {
	IObPaymentRequestCompanyData companyData =	this.createdObject.getHeaderDetail(IDObPaymentRequest.companyDataAttr);
	Assertions.assertEquals(companyData.getCompanyId(), companyId);
		return this;
	}

	public DObPaymentRequestCreateCommandTestUtils assertThatPaymentCurrencyId(Long currencyId) {
		IObPaymentRequestPaymentDetails paymentDetails = this.createdObject.getHeaderDetail(IDObPaymentRequest.paymentdetailsAttr);
		Assertions.assertEquals(paymentDetails.getCurrencyId(), currencyId);
		return this;
	}

	protected DObPaymentRequest getCreatedObject() {
		return createdObject;
	}
}
