package com.ebs.dda.commands.accounting.landedcost.utils;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.entities.accounting.landedcost.LandedCostTestUtils;
import com.ebs.repositories.accounting.landedcost.LandedCostRepTestUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.accounting.landedcost.DObLandedCostUpdateActualCostCommand;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.utils.UserAccountSecurityServiceTestUtils;
import com.ebs.entities.accounting.landedcost.CostFactorItemTestUtils;
import com.ebs.entities.accounting.landedcost.LandedCostGeneralModelTestUtils;
import com.ebs.entities.masterdata.item.ItemTestUtils;
import com.ebs.repositories.accounting.landedcost.CostFactorItemRepTestUtils;
import com.ebs.repositories.accounting.landedcost.LandedCostGeneralModelRepTestUtils;
import com.ebs.repositories.masterdata.item.ItemRepTestUtils;

public class DObLandedCostUpdateActualCostCommandTestUtils {

  private DObLandedCostUpdateActualCostValueObject valueObject;
  private DObLandedCostUpdateActualCostCommand command;

  private DObLandedCostGeneralModel landedCostGeneralModel;
  private DObLandedCost landedCost;
  private String poCode;
  private LinkedList<MObItem> itemsMasterData;
  private List<IObLandedCostFactorItems> savedCostFactorItems;
  private List<IObLandedCostFactorItems> inputCostFactorItems;
  private Long refDocumentId;

  public DObLandedCostUpdateActualCostCommandTestUtils() {

    itemsMasterData = new LinkedList<>();
    savedCostFactorItems = new ArrayList<>();
    inputCostFactorItems = new ArrayList<>();
    CostFactorItemRepTestUtils.reset();
  }

  public DObLandedCostUpdateActualCostCommandTestUtils purhcaseOrderCode(String poCode) {
    this.poCode = poCode;
    return this;
  }

  public DObLandedCostUpdateActualCostCommandTestUtils landedCost(Long landedCostId,
      String landedCostCode) {
    landedCostGeneralModel = LandedCostGeneralModelTestUtils
      .newGeneralModel()
      .id(landedCostId)
      .userCode(landedCostCode)
      .purhcaseOrderCode(poCode)
      .build();
    landedCost = LandedCostTestUtils.newEntity().id(landedCostId).userCode(landedCostCode).build();
    return this;
  }

  public DObLandedCostUpdateActualCostCommandTestUtils itemMasterData(Long itemId,
      String itemCode) {
    itemsMasterData.add(ItemTestUtils
      .newEntity()
      .id(itemId)
      .userCode(itemCode)
      .build()
    );
    return this;
  }

  public DObLandedCostUpdateActualCostCommandTestUtils savedCostFactorItem(
      BigDecimal estimatedValue, BigDecimal actualValue) {
    savedCostFactorItems.add(CostFactorItemTestUtils
      .newEntity()
      .landedcostId(landedCostGeneralModel.getId())
      .mobItemId(itemsMasterData.peekLast().getId())
      .estimatedValue(estimatedValue)
      .actualValue(actualValue)
      .build()
    );
    return this;
  }

  public DObLandedCostUpdateActualCostCommandTestUtils inputCostFactorItem(BigDecimal actualValue) {
    inputCostFactorItems.add(CostFactorItemTestUtils
      .newEntity()
      .landedcostId(landedCostGeneralModel.getId())
      .mobItemId(itemsMasterData.peekLast().getId())
      .actualValue(actualValue)
      .build()
    );
    return this;
  }

  public DObLandedCostUpdateActualCostCommandTestUtils referenceDocumentId(Long refDocumentId) {
    this.refDocumentId = refDocumentId;
    return this;
  }

  public void verifySetActualOfExistEstimatedCostFactorItem() throws Exception {
    setupValueObject();
    setupCommand();
    setupMocking();

    command.executeCommand(valueObject);

    for (int i = 0; i < savedCostFactorItems.size(); i++) {

      IObLandedCostFactorItems savedItem = savedCostFactorItems.get(i);
      IObLandedCostFactorItems inputItem = inputCostFactorItems.get(i);

      compareBigDecimals(savedItem.getActualValue(), inputItem.getActualValue());

      assertCostFactorItemDetails(savedItem, inputItem);
      Mockito.verify(CostFactorItemRepTestUtils.getRepository()).update(savedItem);
    }
  }

  public void verifySetActualOfNonExistCostFactorItem() throws Exception {
    setupValueObject();
    setupCommand();
    setupMocking();

    command.executeCommand(valueObject);

    ArgumentCaptor<IObLandedCostFactorItems> createCostFactorItemCaptor =
        ArgumentCaptor.forClass(IObLandedCostFactorItems.class);

    for (int i = 0; i < inputCostFactorItems.size(); i++) {
      IObLandedCostFactorItems inputItem = inputCostFactorItems.get(i);

      Mockito.verify(CostFactorItemRepTestUtils.getRepository())
          .update(createCostFactorItemCaptor.capture());

      IObLandedCostFactorItems savedItem = createCostFactorItemCaptor.getValue();
      MObItem mObItem = itemsMasterData.get(i);
      assertEquals(mObItem.getId(), savedItem.getCostItemId());

      compareBigDecimals(savedItem.getEstimateValue(), BigDecimal.ZERO);
      compareBigDecimals(savedItem.getActualValue(), inputItem.getActualValue());

      assertCostFactorItemDetails(savedItem, inputItem);
    }
  }

  public void verifyUpdateActualOfAlreadyActualCostFactorItem() throws Exception {
    setupValueObject();
    setupCommand();
    setupMocking();

    List<BigDecimal> notUpdatedItemsActualCosts = new ArrayList<>();
    savedCostFactorItems.forEach(savedItem -> notUpdatedItemsActualCosts
        .add(savedItem.getActualValue()));

    command.executeCommand(valueObject);

    for (int i = 0; i < savedCostFactorItems.size(); i++) {

      IObLandedCostFactorItems savedItem = savedCostFactorItems.get(i);
      IObLandedCostFactorItems inputItem = inputCostFactorItems.get(i);
      BigDecimal oldActualCost = notUpdatedItemsActualCosts.get(i);

      BigDecimal accumelatedActualCost =
          inputItem.getActualValue().add(oldActualCost);

      assertEquals(accumelatedActualCost.compareTo(savedItem.getActualValue()),
          0);

      assertCostFactorItemDetails(savedItem, inputItem);
      Mockito.verify(CostFactorItemRepTestUtils.getRepository()).update(savedItem);
    }
  }

  private void compareBigDecimals(BigDecimal savedItem, BigDecimal inputItem) {
    assertEquals(inputItem.compareTo(savedItem), 0);
  }


  private void setupMocking() {
    CostFactorItemRepTestUtils.findCostFactorItems(savedCostFactorItems, landedCostGeneralModel.getId());

    itemsMasterData.forEach(itme -> ItemRepTestUtils.findItemByCode(itme, itme.getUserCode()));

    LandedCostGeneralModelRepTestUtils.findLandedCostByPOAndState(landedCostGeneralModel, poCode,
        DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED);

    LandedCostRepTestUtils.findLandedCostByUserCode(landedCost, landedCost.getUserCode());
  }

  private void assertCostFactorItemDetails(IObLandedCostFactorItems savedItem,
      IObLandedCostFactorItems inputItem) throws Exception {

    Collection<IObLandedCostFactorItemsDetails> costFactorItemDetails =
        savedItem.getAllLines(IIObLandedCostFactorItems.costFactorItemsDetails);
    assertEquals(costFactorItemDetails.size(), 1);

    IObLandedCostFactorItemsDetails details =
        (IObLandedCostFactorItemsDetails) costFactorItemDetails.toArray()[0];

    assertEquals(refDocumentId, details.getDocumentInfoId());
    assertEquals(inputItem.getActualValue()
        .compareTo(details.getActualValue()), 0);
  }

  private void setupValueObject() {
    valueObject = new DObLandedCostUpdateActualCostValueObject();
    valueObject.setPurchaseOrderCode(poCode);
    valueObject.setRefDocumentId(refDocumentId);
    for (int i = 0; i < inputCostFactorItems.size(); i++) {
      String itemCode = itemsMasterData.get(i).getUserCode();
      BigDecimal actualValue = inputCostFactorItems.get(i).getActualValue();
      valueObject.addItem(itemCode, actualValue);
    }
  }

  private void setupCommand() {
    command = new DObLandedCostUpdateActualCostCommand();
    command.setLandedCostGeneralModelRep(LandedCostGeneralModelRepTestUtils.getRepository());
    command.setLandedCostRep(LandedCostRepTestUtils.getRepository());
    command.setItemRep(ItemRepTestUtils.getRepository());
    command.setLandedCostFactorItemsRep(CostFactorItemRepTestUtils.getRepository());
    command.setUserAccountSecurityService(UserAccountSecurityServiceTestUtils.setup());
  }

}
