package com.ebs.dda.commands.inventory.storetransaction;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModel;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionRep;
import com.ebs.entities.accounting.salesinvoice.SalesInvoiceBusinessPartnerMockUtils;
import com.ebs.entities.inventory.goodsissue.GoodsIssueRefDocumentDataMockUtils;
import com.ebs.entities.inventory.goodsreceipt.GoodsReceiptDataMockUtils;
import com.ebs.entities.inventory.goodsreceipt.GoodsReceiptSalesReturnDataMockUtils;
import com.ebs.entities.inventory.goodsreceipt.GoodsReceiptSalesReturnOrdinaryReceivedItemMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsIssueStoreTransactionMockUtils;
import com.ebs.entities.inventory.storetransaction.GoodsReceiptStoreTransactionMockUtils;
import com.ebs.entities.inventory.storetransaction.StoreTransactionMockUtils;
import com.ebs.entities.masterdata.item.ItemMockUtils;
import com.ebs.entities.masterdata.measuer.MeasureMockUtils;
import com.ebs.entities.order.salesreturn.SalesReturnOrderDetailsMockUtils;
import com.ebs.repositories.accounting.salesInvoice.SalesInvoiceBusinessPartnerGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.goodsissue.GoodsIssueRefDocumentDataGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.goodsreceipt.GoodsReceiptDataGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.goodsreceipt.GoodsReceiptSalesReturnDataGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.goodsreceipt.GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.storetransaction.GoodsIssueStoreTransactionGeneralModelRepMockUtils;
import com.ebs.repositories.inventory.storetransaction.StoreTransactionGeneralModelRepMockUtils;
import com.ebs.repositories.masterdata.item.ItemGeneralModelRepMockUtils;
import com.ebs.repositories.masterdata.measure.MeasureRepMockUtils;
import com.ebs.repositories.order.salesreturn.SalesReturnOrderDetailsGeneralModelRepMockUtils;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
public class GoodsReceiptSalesReturnStoreTransactionCostingTest {
  TObStoreTransactionCreateGoodsReceiptCommand command;
  private CommandTestUtils commandTestUtils;
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;

  @MockBean private TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep;

  @Before
  public void before() {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    documentObjectCodeGenerator = Mockito.mock(DocumentObjectCodeGenerator.class);
    command = new TObStoreTransactionCreateGoodsReceiptCommand(documentObjectCodeGenerator);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setGoodsReceiptStoreTransactionRep(goodsReceiptStoreTransactionRep);
    command.setGoodsReceiptDataGeneralModelRep(
        GoodsReceiptDataGeneralModelRepMockUtils.getRepository());
    command.setGoodsReceiptSalesReturnDataGeneralModelRep(
        GoodsReceiptSalesReturnDataGeneralModelRepMockUtils.getRepository());
    command.setSalesReturnOrderDetailsGeneralModelRep(
        SalesReturnOrderDetailsGeneralModelRepMockUtils.getRepository());
    command.setSalesInvoiceBusinessPartnerGeneralModelRep(
        SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.getRepository());
    command.setGoodsIssueRefDocumentDataGeneralModelRep(
        GoodsIssueRefDocumentDataGeneralModelRepMockUtils.getRepository());
    command.setGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep(
        GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils.getRepository());
    command.setItemGeneralModelRep(ItemGeneralModelRepMockUtils.getRepository());
    command.setMeasureRep(MeasureRepMockUtils.repository);
    command.setGoodsIssueStoreTransactionGeneralModelRep(
        GoodsIssueStoreTransactionGeneralModelRepMockUtils.getRepository());
    command.setStoreTransactionGeneralModelRep(
        StoreTransactionGeneralModelRepMockUtils.getRepository());
  }

  @Test
  public void shouldSetOneStoreTransactionCostValuesFromOneOldGoodsReceiptStoreTransaction()
      throws Exception {
    // arrange
    DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel =
        GoodsReceiptDataMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .id(1L)
            .companyCode("0001")
            .companyId(1L)
            .plantCode("0001")
            .plantId(1L)
            .storehouseCode("0001")
            .storehouseId(1L)
            .purchaseUnitId(1L)
            .build();
    GoodsReceiptDataGeneralModelRepMockUtils.findOneByUserCode(goodsReceiptDataGeneralModel);

    IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel =
        GoodsReceiptSalesReturnDataMockUtils.mockGeneralModel()
            .goodsReceiptCode(goodsReceiptDataGeneralModel.getUserCode())
            .salesReturnCode("2021000001")
            .build();
    GoodsReceiptSalesReturnDataGeneralModelRepMockUtils.findOneByGoodsReceiptCode(
        goodsReceiptSalesReturnDataGeneralModel);

    IObSalesReturnOrderDetailsGeneralModel salesReturnOrderDetailsGeneralModel =
        SalesReturnOrderDetailsMockUtils.mockGeneralModel()
            .salesReturnCode(goodsReceiptSalesReturnDataGeneralModel.getSalesReturnOrderCode())
            .salesInvoiceCode("2021000001")
            .build();
    SalesReturnOrderDetailsGeneralModelRepMockUtils.findOneBySalesReturnOrderCode(
        salesReturnOrderDetailsGeneralModel);

    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
        SalesInvoiceBusinessPartnerMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesReturnOrderDetailsGeneralModel.getSalesInvoiceCode())
            .salesOrderCode("2021000001")
            .build();
    SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.findOneByCode(
        salesInvoiceBusinessPartnerGeneralModel);

    IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
        GoodsIssueRefDocumentDataMockUtils.mockGeneralModel()
            .salesOrderCode(salesInvoiceBusinessPartnerGeneralModel.getSalesOrder())
            .goodsIssueCode("2021000001")
            .build();
    GoodsIssueRefDocumentDataGeneralModelRepMockUtils
        .findOneByRefDocumentAndGoodsIssueStateContains(goodsIssueRefDocumentDataGeneralModel);

    IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel =
            GoodsReceiptSalesReturnOrdinaryReceivedItemMockUtils.mockGeneralModel()
                .goodsReceiptCode(goodsReceiptDataGeneralModel.getUserCode())
                .itemCode("000001")
                .receivedQtyUoE(new BigDecimal("100.0"))
                .unitOfEntryCode("0001")
                .stockType(StockTypeEnum.StockType.UNRESTRICTED_USE.toString())
                .build();
    GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils.findOneByGoodsReceiptCode(
        Collections.singletonList(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel));

    MObItemGeneralModel itemGeneralModel =
        ItemMockUtils.mockGeneralModel()
            .itemCode(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode())
            .itemid(1L)
            .build();
    ItemGeneralModelRepMockUtils.findByUserCode(itemGeneralModel);

    CObMeasure measure =
        MeasureMockUtils.mockEntity()
            .measureCode(
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode())
            .measureid(1L)
            .build();
    MeasureRepMockUtils.findOneByUserCode(measure);

    TObGoodsIssueStoreTransactionGeneralModel goodsIssueStoreTransactionGeneralModel =
        GoodsIssueStoreTransactionMockUtils.mockGeneralModel()
            .userCode("2021000002")
            .id(1L)
            .refTransactionCode("2021000001")
            .goodsIssueCode("2021000001")
            .quantity(new BigDecimal("100"))
            .itemCode(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode())
            .unitOfMeasureCode(
                goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode())
            .storeHouseCode(goodsReceiptDataGeneralModel.getStorehouseCode())
            .companyCode(goodsReceiptDataGeneralModel.getCompanyCode())
            .plantCode(goodsReceiptDataGeneralModel.getPlantCode())
            .build();
    GoodsIssueStoreTransactionGeneralModelRepMockUtils
        .findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
            Collections.singletonList(goodsIssueStoreTransactionGeneralModel));

    TObStoreTransactionGeneralModel storeTransactionGeneralModel =
        StoreTransactionMockUtils.mockGeneralModel()
            .userCode(goodsIssueStoreTransactionGeneralModel.getRefTransactionCode())
            .originalAddingDate(new DateTime())
            .estimateCost(new BigDecimal("90"))
            .actualCost(new BigDecimal("100"))
            .build();
    StoreTransactionGeneralModelRepMockUtils.findOneByUserCode(storeTransactionGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptDataGeneralModel.getUserCode());
    valueObject.setInventoryType("GR_SRO");

    TObGoodsReceiptStoreTransaction expectedGoodsReceiptStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000003")
            .estimateCost(new BigDecimal("90"))
            .actualCost(new BigDecimal("100"))
            .build();
    // act
    command.executeCommand(valueObject);
    // assert
    ArgumentCaptor<List<TObGoodsReceiptStoreTransaction>> createdObjectCaptor =
        ArgumentCaptor.forClass(List.class);
    Mockito.verify(goodsReceiptStoreTransactionRep, Mockito.times(1))
        .saveAll(createdObjectCaptor.capture());
    List<TObGoodsReceiptStoreTransaction> createdStoreTransactionsList =
        createdObjectCaptor.getValue();
    Assert.assertEquals(1, createdStoreTransactionsList.size());

    TObGoodsReceiptStoreTransaction actualStoreTransaction =
        getGoodsReceiptStoreTransaction(
            expectedGoodsReceiptStoreTransaction, createdStoreTransactionsList);
    Assert.assertNotNull(actualStoreTransaction);
  }

  @Test
  public void
      shouldSetMultipleStoreTransactionCostValuesFromMultipleOldGoodsReceiptStoreTransaction()
          throws Exception {
    // arrange
    DObGoodsReceiptDataGeneralModel goodsReceiptDataGeneralModel =
        GoodsReceiptDataMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .id(1L)
            .companyCode("0001")
            .companyId(1L)
            .plantCode("0001")
            .plantId(1L)
            .storehouseCode("0001")
            .storehouseId(1L)
            .purchaseUnitId(1L)
            .build();
    GoodsReceiptDataGeneralModelRepMockUtils.findOneByUserCode(goodsReceiptDataGeneralModel);

    IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel =
        GoodsReceiptSalesReturnDataMockUtils.mockGeneralModel()
            .goodsReceiptCode(goodsReceiptDataGeneralModel.getUserCode())
            .salesReturnCode("2021000001")
            .build();
    GoodsReceiptSalesReturnDataGeneralModelRepMockUtils.findOneByGoodsReceiptCode(
        goodsReceiptSalesReturnDataGeneralModel);

    IObSalesReturnOrderDetailsGeneralModel salesReturnOrderDetailsGeneralModel =
        SalesReturnOrderDetailsMockUtils.mockGeneralModel()
            .salesReturnCode(goodsReceiptSalesReturnDataGeneralModel.getSalesReturnOrderCode())
            .salesInvoiceCode("2021000001")
            .build();
    SalesReturnOrderDetailsGeneralModelRepMockUtils.findOneBySalesReturnOrderCode(
        salesReturnOrderDetailsGeneralModel);

    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
        SalesInvoiceBusinessPartnerMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesReturnOrderDetailsGeneralModel.getSalesInvoiceCode())
            .salesOrderCode("2021000001")
            .build();
    SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.findOneByCode(
        salesInvoiceBusinessPartnerGeneralModel);

    IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
        GoodsIssueRefDocumentDataMockUtils.mockGeneralModel()
            .salesOrderCode(salesInvoiceBusinessPartnerGeneralModel.getSalesOrder())
            .goodsIssueCode("2021000001")
            .build();
    GoodsIssueRefDocumentDataGeneralModelRepMockUtils
        .findOneByRefDocumentAndGoodsIssueStateContains(goodsIssueRefDocumentDataGeneralModel);

    IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
        firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel =
            GoodsReceiptSalesReturnOrdinaryReceivedItemMockUtils.mockGeneralModel()
                .goodsReceiptCode(goodsReceiptDataGeneralModel.getUserCode())
                .itemCode("000001")
                .receivedQtyUoE(new BigDecimal(100.0))
                .unitOfEntryCode("0001")
                .stockType(StockTypeEnum.StockType.UNRESTRICTED_USE.toString())
                .build();
    GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils.findOneByGoodsReceiptCode(
        ImmutableList.of(
            firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel));

    MObItemGeneralModel firstItemGeneralModel =
        ItemMockUtils.mockGeneralModel()
            .itemCode(firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode())
            .itemid(1L)
            .build();
    ItemGeneralModelRepMockUtils.findByUserCode(firstItemGeneralModel);

    CObMeasure firstMeasure =
        MeasureMockUtils.mockEntity()
            .measureCode(
                firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode())
            .measureid(1L)
            .build();
    MeasureRepMockUtils.findOneByUserCode(firstMeasure);

    TObGoodsIssueStoreTransactionGeneralModel firstGoodsIssueStoreTransactionGeneralModel =
        GoodsIssueStoreTransactionMockUtils.mockGeneralModel()
            .userCode("2021000003")
            .id(1L)
            .refTransactionCode("2021000001")
            .goodsIssueCode("2021000001")
            .quantity(new BigDecimal("60"))
            .itemCode(firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode())
            .unitOfMeasureCode(
                firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode())
            .storeHouseCode(goodsReceiptDataGeneralModel.getStorehouseCode())
            .companyCode(goodsReceiptDataGeneralModel.getCompanyCode())
            .plantCode(goodsReceiptDataGeneralModel.getPlantCode())
            .build();

    TObGoodsIssueStoreTransactionGeneralModel secondGoodsIssueStoreTransactionGeneralModel =
        GoodsIssueStoreTransactionMockUtils.mockGeneralModel()
            .userCode("2021000004")
            .id(1L)
            .refTransactionCode("2021000002")
            .goodsIssueCode("2021000002")
            .quantity(new BigDecimal("40"))
            .itemCode(firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getItemCode())
            .unitOfMeasureCode(
                firstGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getUnitOfEntryCode())
            .storeHouseCode(goodsReceiptDataGeneralModel.getStorehouseCode())
            .companyCode(goodsReceiptDataGeneralModel.getCompanyCode())
            .plantCode(goodsReceiptDataGeneralModel.getPlantCode())
            .build();
    GoodsIssueStoreTransactionGeneralModelRepMockUtils
        .findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
                ImmutableList.of(firstGoodsIssueStoreTransactionGeneralModel, secondGoodsIssueStoreTransactionGeneralModel));

    TObStoreTransactionGeneralModel firstStoreTransactionGeneralModel =
        StoreTransactionMockUtils.mockGeneralModel()
            .userCode(firstGoodsIssueStoreTransactionGeneralModel.getRefTransactionCode())
            .originalAddingDate(new DateTime())
            .estimateCost(new BigDecimal("66.789"))
            .actualCost(new BigDecimal("165.396"))
            .build();
    StoreTransactionGeneralModelRepMockUtils.findOneByUserCode(firstStoreTransactionGeneralModel);

    TObStoreTransactionGeneralModel secondStoreTransactionGeneralModel =
        StoreTransactionMockUtils.mockGeneralModel()
            .userCode(secondGoodsIssueStoreTransactionGeneralModel.getRefTransactionCode())
            .originalAddingDate(new DateTime())
            .estimateCost(new BigDecimal("12.456"))
            .actualCost(new BigDecimal("11.396"))
            .build();
    StoreTransactionGeneralModelRepMockUtils.findOneByUserCode(secondStoreTransactionGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptDataGeneralModel.getUserCode());
    valueObject.setInventoryType("GR_SRO");

    TObGoodsReceiptStoreTransaction firstExpectedGoodsReceiptStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000005")
            .estimateCost(new BigDecimal("66.789"))
            .actualCost(new BigDecimal("165.396"))
            .build();

    TObGoodsReceiptStoreTransaction secondExpectedGoodsReceiptStoreTransaction =
        GoodsReceiptStoreTransactionMockUtils.mockEntity()
            .userCode("2021000006")
            .estimateCost(new BigDecimal("12.456"))
            .actualCost(new BigDecimal("11.396"))
            .build();
    // act
    command.executeCommand(valueObject);
    // assert
    ArgumentCaptor<List<TObGoodsReceiptStoreTransaction>> createdObjectCaptor =
        ArgumentCaptor.forClass(List.class);
    Mockito.verify(goodsReceiptStoreTransactionRep, Mockito.times(1))
        .saveAll(createdObjectCaptor.capture());
    List<TObGoodsReceiptStoreTransaction> createdStoreTransactionsList =
        createdObjectCaptor.getValue();
    Assert.assertEquals(2, createdStoreTransactionsList.size());

    TObGoodsReceiptStoreTransaction firstActualStoreTransaction =
        getGoodsReceiptStoreTransaction(
            firstExpectedGoodsReceiptStoreTransaction, createdStoreTransactionsList);
    Assert.assertNotNull(firstActualStoreTransaction);

    TObGoodsReceiptStoreTransaction secondActualStoreTransaction =
        getGoodsReceiptStoreTransaction(
            secondExpectedGoodsReceiptStoreTransaction, createdStoreTransactionsList);
    Assert.assertNotNull(secondActualStoreTransaction);
  }

  private TObGoodsReceiptStoreTransaction getGoodsReceiptStoreTransaction(
      TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction,
      Collection<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactions) {
    TObGoodsReceiptStoreTransaction actual =
        goodsReceiptStoreTransactions.stream()
            .filter(
                transaction ->
                    goodsReceiptStoreTransaction
                            .getEstimateCost()
                            .compareTo(transaction.getEstimateCost())
                        == 0)
            .filter(
                transaction ->
                    goodsReceiptStoreTransaction
                            .getActualCost()
                            .compareTo(transaction.getActualCost())
                        == 0)
            .findAny()
            .orElse(null);
    return actual;
  }
}
