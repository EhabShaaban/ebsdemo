package com.ebs.dda.commands;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommandTestUtils {
  private IUserAccountSecurityService userAccountSecurityService;

  public void mockUserAccount(String accountName) {
    Mockito.when(userAccountSecurityService.getLoggedInUser()).thenReturn(createUser(accountName));
  }

  public CommandTestUtils() {
    userAccountSecurityService = Mockito.mock(IUserAccountSecurityService.class);
  }

  public IUserAccountSecurityService getUserAccountSecurityService() {
    return userAccountSecurityService;
  }

  public DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }

  private IBDKUser createUser(final String username) {
    return new IBDKUser() {
      @Override
      public String getUsername() {
        return username;
      }

      @Override
      public String getCompany() {
        return "CO";
      }

      @Override
      public boolean equals(IBDKUser anotherUser) {
        return false;
      }

      @Override
      public Long getUserId() {
        return 0L;
      }

      @Override
      public Boolean getSysPasswordFlag() {
        return false;
      }

      @Override
      public String toString() {
        return username;
      }
    };
  }
}
