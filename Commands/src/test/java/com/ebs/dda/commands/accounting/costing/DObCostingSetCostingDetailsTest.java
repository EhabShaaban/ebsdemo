package com.ebs.dda.commands.accounting.costing;

import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetails;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.entities.*;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.repositories.*;
import com.ebs.repositories.accounting.vendorinvoice.VendorInvoiceSummaryRepUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
public class DObCostingSetCostingDetailsTest {
  private DocumentObjectCodeGenerator documentObjectCodeGenerator;
  private DObCostingStateMachine costingStateMachine;

  private DObCostingCreateCommand command;
  private CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory;
  private CostingExchangeRateFullyPaidStrategy costingExchangeRateFullyPaidStrategy;
  private CommandTestUtils commandTestUtils;

  @MockBean private DObCostingRep costingRep;
  @Mock private CreateCostingItemsSectionService createCostingItemsSectionService;

  @Before
  public void init() {
    commandTestUtils = new CommandTestUtils();
    commandTestUtils.mockUserAccount("Admin");
    documentObjectCodeGenerator = Mockito.mock(DocumentObjectCodeGenerator.class);
    costingStateMachine = Mockito.mock(DObCostingStateMachine.class);

    costingExchangeRateStrategyFactory = Mockito.mock(CostingExchangeRateStrategyFactory.class);
    costingExchangeRateFullyPaidStrategy = Mockito.mock(CostingExchangeRateFullyPaidStrategy.class);

    command = new DObCostingCreateCommand(documentObjectCodeGenerator, costingStateMachine);
    command.setUserAccountSecurityService(commandTestUtils.getUserAccountSecurityService());
    command.setGoodsReceiptGeneralModelRep(GoodsReceiptGeneralModelRepUtils.getRepository());
    command.setVendorInvoiceGeneralModelRep(VendorInvoiceGeneralModelRepMockUtils.getRepository());
    command.setVendorInvoiceSummaryRep(VendorInvoiceSummaryRepUtils.getRepository());
    command.setCostingRep(costingRep);
    command.setCostingExchangeRateStrategyFactory(costingExchangeRateStrategyFactory);
    command.setLandedCostGeneralModelRep(LandedCostGeneralModelRepMockUtils.getRepository());
    command.setCreateCostingItemsSectionService(createCostingItemsSectionService);
    DateTime dateTime = new DateTime();
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  @After
  public void after() {
    LandedCostGeneralModelRepMockUtils.resetRepository();
  }

  @Test
  public void shouldSetCurrencyPriceValueInCurrencyPriceEstimateTime() throws Exception {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .id(1L)
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceItemsGeneralModel firstVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000001")
            .totalAmount(BigDecimal.valueOf(1000))
            .build();
    IObVendorInvoiceItemsGeneralModel secondVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000002")
            .totalAmount(BigDecimal.valueOf(1500))
            .build();
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems =
        Arrays.asList(firstVendorInvoiceItem, secondVendorInvoiceItem);
    VendorInvoiceItemsGeneralModelRepMockUtils.findByGoodsReceiptCode(vendorInvoiceItems);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    IObAccountingDocumentActivationDetailsGeneralModel invoiceActivationDetailsGeneralModel =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .currencyPrice(new BigDecimal("17.44"))
            .objectTypeCode("VendorInvoice")
            .build();
    AccountingDocumentActivationDetailsGeneralModelRepMockUtils.findOneByCodeAndObjectTypeCode(
        invoiceActivationDetailsGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED)
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostGeneralModel.getUserCode())
            .estimateValue(BigDecimal.valueOf(1570))
            .build();
    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCostFactorItemsSummary);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    Mockito.when(costingExchangeRateStrategyFactory.getStrategy(Mockito.any()))
        .thenReturn(costingExchangeRateFullyPaidStrategy);
    Mockito.when(costingExchangeRateFullyPaidStrategy.calculate(Mockito.any()))
        .thenReturn(new BigDecimal("16.38"));

    IObCostingDetails expectedCostingDetails =
        CostingDetailsMockUtils.mockEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .goodsReceiptId(goodsReceiptGeneralModel.getId())
            .currencyPriceEstimateTime(new BigDecimal("16.38"))
            .build();

    // act
    command.executeCommand(valueObject);

    // assert
    ArgumentCaptor<DObCosting> createdObjectCaptor = ArgumentCaptor.forClass(DObCosting.class);
    Mockito.verify(costingRep, Mockito.times(1)).create(createdObjectCaptor.capture());
    DObCosting createdObject = createdObjectCaptor.getValue();
    IObCostingDetails actualCostingDetail =
        createdObject.getHeaderDetail(IDObCosting.costingDetailsAttr);

    Assert.assertEquals(
        expectedCostingDetails.getCreationInfo(), actualCostingDetail.getCreationInfo());
    Assert.assertEquals(
        expectedCostingDetails.getModificationInfo(), actualCostingDetail.getModificationInfo());
    Assert.assertEquals(
        expectedCostingDetails.getCreationDate(), actualCostingDetail.getCreationDate());
    Assert.assertEquals(
        expectedCostingDetails.getModifiedDate(), actualCostingDetail.getModifiedDate());
    Assert.assertEquals(
        expectedCostingDetails.getGoodsReceiptId(), actualCostingDetail.getGoodsReceiptId());
    Assert.assertEquals(
        compareBigDecimals(
            expectedCostingDetails.getCurrencyPriceEstimateTime(),
            actualCostingDetail.getCurrencyPriceEstimateTime()),
        0);
    Assert.assertNull(actualCostingDetail.getCurrencyPriceActualTime());
  }

  private int compareBigDecimals(BigDecimal firstBigDecimal, BigDecimal secondBigDecimal) {
    return firstBigDecimal.compareTo(secondBigDecimal);
  }

  @Test
  public void shouldSetCurrencyPriceValueInCurrencyPriceActualTime() throws Exception {
    // arrange
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        GoodsReceiptMockUtils.mockGeneralModel()
            .id(1L)
            .userCode("2021000001")
            .refDocumentCode("2021000001")
            .build();
    GoodsReceiptGeneralModelRepUtils.findOneByUserCode(goodsReceiptGeneralModel);

    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .id(65L)
            .userCode("2021000001")
            .purchaseOrderCode(goodsReceiptGeneralModel.getRefDocumentCode())
            .currencyIso("USD")
            .build();
    VendorInvoiceGeneralModelRepMockUtils.findOneByPurchaseOrderCode(vendorInvoiceGeneralModel);

    IObVendorInvoiceItemsGeneralModel firstVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000001")
            .totalAmount(BigDecimal.valueOf(1000))
            .build();
    IObVendorInvoiceItemsGeneralModel secondVendorInvoiceItem =
        VendorInvoiceItemsMockUtils.mockGeneralModel()
            .vendorInvoiceCode("2021000001")
            .itemCode("000002")
            .totalAmount(BigDecimal.valueOf(1500))
            .build();
    List<IObVendorInvoiceItemsGeneralModel> vendorInvoiceItems =
        Arrays.asList(firstVendorInvoiceItem, secondVendorInvoiceItem);
    VendorInvoiceItemsGeneralModelRepMockUtils.findByGoodsReceiptCode(vendorInvoiceItems);

    IObVendorInvoiceSummary vendorInvoiceSummary =
        VendorInvoiceSummaryUtils.newEntity()
            .refInstanceId(vendorInvoiceGeneralModel.getId())
            .total(new BigDecimal("2500"))
            .build();
    VendorInvoiceSummaryRepUtils.findOneByRefInstanceId(vendorInvoiceSummary);

    IObAccountingDocumentActivationDetailsGeneralModel invoiceActivationDetailsGeneralModel =
        AccountingDocumentActivationDetailsMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .currencyPrice(new BigDecimal("17.44"))
            .objectTypeCode("VendorInvoice")
            .build();
    AccountingDocumentActivationDetailsGeneralModelRepMockUtils.findOneByCodeAndObjectTypeCode(
        invoiceActivationDetailsGeneralModel);

    DObLandedCostGeneralModel landedCostGeneralModel =
        LandedCostGeneralModelMockUtils.mockGeneralModel()
            .userCode("2021000001")
            .purchaseOrderId(22L)
            .state(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED)
            .build();
    LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesNotLike(
        landedCostGeneralModel);

    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        LandedCostSummaryGeneralModelMockUtils.mockGeneralModel()
            .userCode(landedCostGeneralModel.getUserCode())
            .actualValue(BigDecimal.valueOf(1570))
            .build();
    LandedCostFactorItemsSummaryGeneralModelRepUtils.findOneByCode(landedCostFactorItemsSummary);

    IObItemAccountingInfoGeneralModel firstItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(firstVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(25L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        firstItemAccountingInfoGeneralModel);

    IObItemAccountingInfoGeneralModel secondItemAccountingInfoGeneralModel =
        IObItemAccountingInfoMockUtils.mockGeneralModel()
            .itemCode(secondVendorInvoiceItem.getItemCode())
            .accountCode("10207")
            .accountId(26L)
            .build();
    ItemAccountingInfoGeneralModelRepMockUtils.findOneByUserCode(
        secondItemAccountingInfoGeneralModel);

    DObInventoryDocumentActivateValueObject valueObject =
        new DObInventoryDocumentActivateValueObject();
    valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());

    Mockito.when(costingExchangeRateStrategyFactory.getStrategy(Mockito.any()))
        .thenReturn(costingExchangeRateFullyPaidStrategy);
    Mockito.when(costingExchangeRateFullyPaidStrategy.calculate(Mockito.any()))
        .thenReturn(new BigDecimal("16.38"));

    IObCostingDetails expectedCostingDetails =
        CostingDetailsMockUtils.mockEntity()
            .creationInfo("Admin")
            .modificationInfo("Admin")
            .creationDate(DateTime.now())
            .modifiedDate(DateTime.now())
            .goodsReceiptId(goodsReceiptGeneralModel.getId())
            .currencyPriceActualTime(new BigDecimal("16.38"))
            .build();

    // act
    command.executeCommand(valueObject);

    // assert
    ArgumentCaptor<DObCosting> createdObjectCaptor = ArgumentCaptor.forClass(DObCosting.class);
    Mockito.verify(costingRep, Mockito.times(1)).create(createdObjectCaptor.capture());
    DObCosting createdObject = createdObjectCaptor.getValue();
    IObCostingDetails actualCostingDetail =
        createdObject.getHeaderDetail(IDObCosting.costingDetailsAttr);

    Assert.assertEquals(
        expectedCostingDetails.getCreationInfo(), actualCostingDetail.getCreationInfo());
    Assert.assertEquals(
        expectedCostingDetails.getModificationInfo(), actualCostingDetail.getModificationInfo());
    Assert.assertEquals(
        expectedCostingDetails.getCreationDate(), actualCostingDetail.getCreationDate());
    Assert.assertEquals(
        expectedCostingDetails.getModifiedDate(), actualCostingDetail.getModifiedDate());
    Assert.assertEquals(
        expectedCostingDetails.getGoodsReceiptId(), actualCostingDetail.getGoodsReceiptId());
    Assert.assertEquals(
        compareBigDecimals(
            expectedCostingDetails.getCurrencyPriceActualTime(),
            actualCostingDetail.getCurrencyPriceActualTime()),
        0);
    Assert.assertNull(actualCostingDetail.getCurrencyPriceEstimateTime());
  }
}
