package com.ebs.dda.commands.accounting.notesreceivable.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.ebs.dda.commands.accounting.notesreceivable.validation.utils.IObNotesReceivableDetailsSchemaTestUtils;

@RunWith(JUnitPlatform.class)
public class IObNotesReceivableDetailsSchemaTests {

	private IObNotesReceivableDetailsSchemaTestUtils utils;

	@BeforeEach
	public void init() {
		utils = new IObNotesReceivableDetailsSchemaTestUtils();
	}

	@TestFactory
	public Stream<DynamicTest> should_ThrowsArgumentSecurityException_When_InvalidNoteBank() {
		Map<String, Object> invalidValues = new HashMap<String, Object>() {
			{
				put("Has non ASCII letters", "<@#_!/\\>");
				put("Numeric value", 123);
				put("String with numbers", "123");
				put("String exceeds max length limits",
								"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			}
		};
		return invalidValues.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			Object invalidValue = invalidValues.get(key);
			utils.withNoteBank(invalidValue).withNoteNumber("537899").withDepotCode("0001")
							.withDueDate("05-Sep-2021 10:30 AM")
							.assertThrowsArgumentSecurityException();
		}));
	}

	@TestFactory
	public Stream<DynamicTest> should_ThrowsArgumentSecurityException_When_InvalidNoteNumber() {
		Map<String, Object> invalidValues = new HashMap<String, Object>() {
			{
				put("Has non ASCII letters", "<@#_!/\\>");
				put("String value", "Number");
				put("Number exceeds max length limits",
								"12121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121");
			}
		};
		return invalidValues.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			Object invalidValue = invalidValues.get(key);
			utils.withNoteBank("Bank Misr").withNoteNumber(invalidValue).withDepotCode("0001")
							.withDueDate("05-Sep-2021 10:30 AM")
							.assertThrowsArgumentSecurityException();
		}));
	}

	@TestFactory
	public Stream<DynamicTest> should_ThrowsArgumentSecurityException_When_InvalidDepotCode() {
		Map<String, Object> invalidValues = new HashMap<String, Object>() {
			{
				put("Has non ASCII letters", "<@#_!/\\>");
				put("String value", "code");
				put("Numeric value", 1234);
				put("Number exceeds max length limits", "00001");
				put("Number less than min length limits", "001");
			}
		};
		return invalidValues.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			Object invalidValue = invalidValues.get(key);
			utils.withNoteBank("Bank Misr").withNoteNumber("253256").withDepotCode(invalidValue)
							.withDueDate("05-Sep-2021 10:30 AM")
							.assertThrowsArgumentSecurityException();
		}));
	}

	@TestFactory
	public Stream<DynamicTest> should_ThrowsArgumentSecurityException_When_InvalidDueDate() {
		Map<String, Object> invalidValues = new HashMap<String, Object>() {
			{
				put("Different month format", "05-09-2021 10:30 AM");
				put("Different format with slash", "05/09/2021 10:30 AM");
				put("Invalid day", "32-09-2021 10:30 AM");
				put("Invalid month", "05-13-2021 10:30 AM");
				put("Invalid day time", "05-Sep-2021 10:30 MM");
				put("Invalid time", "05-Sep-2021 70:30 MM");
				put("String value", "date");
				put("Numeric value", 1234);
			}
		};
		return invalidValues.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			Object invalidValue = invalidValues.get(key);
			utils.withNoteBank("Bank Misr").withNoteNumber("253256").withDepotCode("0001")
							.withDueDate(invalidValue).assertThrowsArgumentSecurityException();
		}));
	}
}
