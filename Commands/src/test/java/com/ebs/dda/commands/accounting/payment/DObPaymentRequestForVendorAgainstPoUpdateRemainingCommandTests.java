package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTests {
  private DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils utils;

  @Before
  public void setUp() {
    utils = new DObPaymentRequestForVendorAgainstPoUpdateRemainingCommandTestUtils();
  }

  @Test
  public void testUpdatePoRemainingWithPaymentAmount() throws Exception {
    utils
        .withPaymentCode("2019000030")
        .withAmount(BigDecimal.valueOf(1000))
        .withPurchaseOrderCode("2019000108")
        .withPoRemaining(BigDecimal.valueOf(488000))
        .whenExecute()
        .assertThatRemainingIsUpdatedTo(BigDecimal.valueOf(487000));
  }
}
