package com.ebs.dda.commands.accounting.journalbalance.utils;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.CommandTestUtils;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceDecreaseBalanceCommand;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceUpdateValueObject;
import com.ebs.repositories.accounting.journalbalance.CObJournalBalanceRepUtils;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CObJournalBalanceDecreaseBalanceCommandTestUtils {
    private JournalBalanceUpdateValueObject valueObject;
    private CObJournalBalance journalBalance;

    public CObJournalBalanceDecreaseBalanceCommandTestUtils() {
        valueObject = new JournalBalanceUpdateValueObject();
        journalBalance = new CObJournalBalance();
    }

    public CObJournalBalanceDecreaseBalanceCommandTestUtils withBalanceCode(String balanceCode) {
        valueObject.setJournalBalanceCode(balanceCode);
        journalBalance.setUserCode(balanceCode);
        return this;
    }

    public CObJournalBalanceDecreaseBalanceCommandTestUtils withBalanceAmount(BigDecimal balanceAmount) {
        journalBalance.setBalance(balanceAmount);
        return this;
    }

    public CObJournalBalanceDecreaseBalanceCommandTestUtils withPaymentAmount(BigDecimal paymentAmount) {
        valueObject.setAmount(paymentAmount);
        return this;
    }

    public CObJournalBalanceDecreaseBalanceCommandTestUtils assertThatBalanceDecreasedTo(BigDecimal updatedBalanceAmount) {
        ArgumentCaptor<CObJournalBalance> journalBalanceArgumentCaptor = ArgumentCaptor.forClass(CObJournalBalance.class);
        Mockito.verify(CObJournalBalanceRepUtils.getRepository(), Mockito.times(1)).save(journalBalanceArgumentCaptor.capture());
        CObJournalBalance journalBalance = journalBalanceArgumentCaptor.getValue();
        Assert.assertEquals(updatedBalanceAmount.compareTo(journalBalance.getBalance()), 0);
        return this;
    }

    public CObJournalBalanceDecreaseBalanceCommandTestUtils whenExecute() throws Exception {
        CObJournalBalanceDecreaseBalanceCommand command = new CObJournalBalanceDecreaseBalanceCommand();
        CObJournalBalanceRepUtils.findOneByUserCode(journalBalance);
        command.setJournalBalanceRep(CObJournalBalanceRepUtils.getRepository());
        setUserAccountSecurityService(command);
        command.executeCommand(valueObject);
        return this;
    }
    private void setUserAccountSecurityService(CObJournalBalanceDecreaseBalanceCommand command) {
        CommandTestUtils commandTestUtils = new CommandTestUtils();
        commandTestUtils.mockUserAccount("admin");
        IUserAccountSecurityService userAccountSecurityService = commandTestUtils.getUserAccountSecurityService();
        command.setUserAccountSecurityService(userAccountSecurityService);
    }
}
