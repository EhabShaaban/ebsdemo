package com.ebs.dda.commands.accounting.payment;

import com.ebs.dda.commands.accounting.payment.utils.DObDownPaymentCreateCommandTestUtils;
import org.junit.Before;
import org.junit.Test;

public class DObDownPaymentCreateCommandTests {

	private DObDownPaymentCreateCommandTestUtils utils;

	@Before
	public void setup() {
		utils = new DObDownPaymentCreateCommandTestUtils();
	}

	@Test
	public void testCreateDownPaymentBankPayment() throws Exception {
		utils.withPaymentForm("BANK").withPaymentType("VENDOR").withBusinessUnit("0002")
						.withBusinessPartner("000002")
						.withReferenceDocument("PURCHASEORDER", "2021000003", 1L, 2L, 5L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(1L).assertThatPaymentCurrencyId(2L);
		utils.assertThatReferenceDocumentId(5L);
	}

	@Test
	public void testCreateDownPaymentCashPayment() throws Exception {
		utils.withPaymentForm("CASH").withPaymentType("VENDOR").withBusinessUnit("0002")
						.withBusinessPartner("000002")
						.withReferenceDocument("PURCHASEORDER", "2021000003", 2L, 3L, 7L)
						.withDocumentOwner(34L).executeCommand()
						.assertThatPaymentIsCreatedSuccessfully().assertThatPaymentCompanyId(2L).assertThatPaymentCurrencyId(3L);
		utils.assertThatReferenceDocumentId(7L);
	}
}
