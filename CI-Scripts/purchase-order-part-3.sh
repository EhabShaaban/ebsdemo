#!/bin/bash
apt-get update && apt-get --assume-yes install postgresql-client
git pull origin develop
export CWD=$(pwd)
cd database
./reinitdbtest.sh
cd $CWD
mkdir -p ~/.m2/repository/org/apache/commons/commons-scxml2/2.0
cp ./DomainBusinessApplications/dependencies/commons-scxml2-2.0.jar ~/.m2/repository/org/apache/commons/commons-scxml2/2.0/
mvn clean install -DskipTests
cd ApplicationLauncher
mvn clean -Dtest="DObPurchaseOrderPaymentAndDeliveryCucumberRunner" test
