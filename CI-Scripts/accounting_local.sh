#!/bin/bash

export CWD=$(pwd)
cd database
./reinitdbtest.sh
cd $CWD
mkdir -p ~/.m2/repository/org/apache/commons/commons-scxml2/2.0
cp ./DomainBusinessApplications/dependencies/commons-scxml2-2.0.jar ~/.m2/repository/org/apache/commons/commons-scxml2/2.0/
mvn clean install
cd ApplicationLauncher
mvn clean -Dtest="DObPaymentRequestCucumberRunner,
                  DObVendorInvoiceCucumberRunner,
                  DObJournalEntryCucumberRunner,
                  DObInvoiceSaveEditItemCucumberRunner,
                  DObNotesReceivableCucumberRunner,
                  DObCollectionCucumberRunner,
                  DObLandedCostCucumberRunner,
                  DObSettlementCucumberRunner
                 " test
