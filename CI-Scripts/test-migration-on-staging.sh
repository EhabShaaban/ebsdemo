#!/bin/bash
BUCKET=staging-madina-storage
KEY=`aws s3 ls $BUCKET --recursive | sort | tail -n 1 | awk '{print $4}'`

aws s3 cp s3://$BUCKET/$KEY latest-staging-backup.gz

apt-get update && apt-get --assume-yes install postgresql-client

psql -h localhost -U postgres -p 5432 -a -q -f database/postgres/schema/reinitDB.sql
pg_restore -h localhost -U postgres -p 5432 -d ebdk_core_v2 < latest-staging-backup.gz > /dev/null
psql -v ON_ERROR_STOP=1 -h localhost -d ebdk_core_v2 -U postgres -p 5432 -a -q -f database/postgres/releases/latest-migration.sql
