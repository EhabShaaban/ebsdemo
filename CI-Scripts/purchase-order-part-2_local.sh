#!/bin/bash
export CWD=$(pwd)
cd database
./reinitdbtest.sh
cd $CWD
mvn clean install
cd ApplicationLauncher
mvn clean -Dtest="DObPurchaseOrderCucumberRunner,
                  DObPurchaseOrderCompanyCucumberRunner,
                  DObPurchaseOrderConsigneeCucumberRunner,
                  DObPurchaseOrderPaymentAndDeliveryAuthorizationCucumberRunner,
                  DObPurchaseOrderEditPaymentAndDeliveryCucumberRunner,
                  DObPurchaseOrderSavePaymentAndDeliveryCucumberRunner,
                  DObPurchaseOrderValidatePaymentAndDeliveryCucumberRunner,
                  DObPurchaseOrderViewPaymentAndDeliveryCucumberRunner,
                  DObPurchaseOrderApproveCucumberRunner,
                  DObSalesOrderCreateCucumberRunner,
                  DObSalesOrderReadAllTypesCucumberRunner,
                  DObSalesOrderViewAllCucumberRunner,
                  DObSalesOrderAllowedActionsHomeScreenCucumberRunner,
                  DObSalesOrderAllowedActionsObjectScreenCucumberRunner,
                  IObSalesOrderViewGeneralDataCucumberRunner,
                  IObSalesOrderViewCompanyStoreDataCucumberRunner,
                  IObSalesOrderViewSalesOrderDataCucumberRunner,
                  IObSalesOrderEditCompanyAndStoreCucumberRunner,
                  IObSalesOrderEditSalesOrderDataCucumberRunner,
                  IObSalesOrderItemsCucumberRunner,
                  IObSalesOrderSaveSalesOrderDataHappyPathRunner,
                  DObSalesOrderDeleteCucumberRunner
                  " test