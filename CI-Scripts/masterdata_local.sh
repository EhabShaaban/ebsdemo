#!/bin/bash

export CWD=$(pwd)
cd database
./reinitdbtest.sh
cd $CWD
mvn clean install
cd ApplicationLauncher
mvn clean -Dtest="MObItemCucumberRunner,
                  MObVendorCucumberRunner,
                  ItemVendorRecordCucumberRunner,
                  GeneralLogoutLockReleaseCucumberRunner,
                  MObCustomerCucumberRunner,
                  DObSalesResponsibleReadAllCucumberRunner,
                  HObItem*CucumberRunner,
                  CObItem*CucumberRunner
                 " test

