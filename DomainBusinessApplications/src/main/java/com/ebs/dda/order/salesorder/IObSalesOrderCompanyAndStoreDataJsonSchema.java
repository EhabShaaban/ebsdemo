package com.ebs.dda.order.salesorder;

public interface IObSalesOrderCompanyAndStoreDataJsonSchema {

  String SAVE =
      "{\n"
          + "  \"properties\": {\n"
          + "    \"storeCode\": {\n"
          + "      \"type\": [\n"
          + "        \"string\",\n"
          + "        \"null\"\n"
          + "      ],\n"
          + "      \"pattern\": \"^([0-9])*$\",\n"
          + "      \"minLength\": 4,\n"
          + "      \"maxLength\": 4\n"
          + "    }\n"
          + "  }\n"
          + "}";
}
