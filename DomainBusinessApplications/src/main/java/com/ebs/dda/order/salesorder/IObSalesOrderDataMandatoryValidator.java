package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;
import java.util.Map;

public class IObSalesOrderDataMandatoryValidator implements ISectionMandatoryValidator {

  private IObSalesOrderDataGeneralModelRep salesOrderDataRep;
  private DObSalesOrderGeneralModelRep salesOrderRep;
  private MandatoryValidatorUtils validatorUtils;

  public IObSalesOrderDataMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setSalesOrderDataRep(IObSalesOrderDataGeneralModelRep salesOrderDataRep) {
    this.salesOrderDataRep = salesOrderDataRep;
  }
  public void setSalesOrderRep(DObSalesOrderGeneralModelRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) throws Exception {
    validatorUtils = new MandatoryValidatorUtils();

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderRep.findOneByUserCode(
            ((IObSalesOrderDataValueObject) valueObject).getSalesOrderCode());

    validatorUtils.validateMandatoriesForValueObjectAsObjectState(
        ISalesOrderDataMandatoryAttributes.MANDATORIES,
        salesOrderGeneralModel.getCurrentState(),
        valueObject);
  }

  @Override
  public List<String> validateGeneralModelMandatories(String salesOrderCode, String state)
      throws Exception {
    IObSalesOrderDataGeneralModel generalModel =
        salesOrderDataRep.findOneBySalesOrderCode(salesOrderCode);

    Map<String, String[]> mandatories = ISalesOrderDataMandatoryAttributes.MANDATORIES;
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);
    return validatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes, generalModel);
  }
}
