package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderDataGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface ISalesOrderDataMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {};
  String[] REJECTED_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  String[] APPROVED_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  String[] DELIVERY_COMPLETE_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  String[] CANCELED_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  String[] EXPIRED_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  String[] WAITING_APPROVAL_STATE_MANDATORIES = {
    IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
    IIObSalesOrderDataGeneralModel.CURRENCY_CODE,
    IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
    IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
    IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE
  };
  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObSalesOrderStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.REJECTED, REJECTED_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.APPROVED, APPROVED_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.CANCELED, CANCELED_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.DELIVERY_COMPLETE, DELIVERY_COMPLETE_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.EXPIRED, EXPIRED_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.WAITING_APPROVAL, WAITING_APPROVAL_STATE_MANDATORIES)
          .build();
}
