package com.ebs.dda.order.salesreturnorder;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IObSalesReturnOrderItemsEditabilityManger extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObSalesReturnOrderItemGeneralModel.RETURN_REASON_CODE,
                IIObSalesReturnOrderItemGeneralModel.RETURN_QUANTITY));

    enabledAttributesPerState.put(
        DObSalesReturnOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(DObSalesReturnOrderStateMachine.SHIPPED_STATE, new HashSet<>());
    enabledAttributesPerState.put(
        DObSalesReturnOrderStateMachine.CREDIT_NOTE_ACTIVATED_STATE, new HashSet<>());
    enabledAttributesPerState.put(
        DObSalesReturnOrderStateMachine.GOODS_RECEIPT_ACTIVATED_STATE, new HashSet<>());
    enabledAttributesPerState.put(DObSalesReturnOrderStateMachine.COMPLETED_STATE, new HashSet<>());
    return enabledAttributesPerState;
  }
}
