package com.ebs.dda.order.salesorder;

public interface IDObSalesOrderSectionNames {

  String COMPANY_AND_STORE_SECTION = "CompanyAndStoreData";
  String SALES_ORDER_DATA_SECTION = "SalesOrderData";
  String ITEMS_SECTION = "Items";
}
