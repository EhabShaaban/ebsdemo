package com.ebs.dda.order.salesorder;

public interface IDObSalesOrderActionNames {

  String READ_COMPANY_AND_STORE_DATA = "ReadCompanyAndStoreData";
  String READ_SALES_ORDER_DATA = "ReadSalesOrderData";
  String READ_ITEMS = "ReadItems";
  String DELETE_ITEM = "DeleteItem";
  String UPDATE_SALES_ORDER_DATA = "UpdateSalesOrderData";
  String UPDATE_COMPANY_AND_STORE_DATA = "UpdateCompanyAndStoreData";
  String UPDATE_ITEMS = "UpdateItems";
  String SUBMIT_FOR_APPROVAL = "SubmitForApproval";
  String DELETE = "Delete";
  String APPROVE = "Approve";
  String REJECT = "Reject";
  String POST_INVOICE = "PostInvoice";
  String POST_GOODS_ISSUE = "PostGoodsIssue";
}
