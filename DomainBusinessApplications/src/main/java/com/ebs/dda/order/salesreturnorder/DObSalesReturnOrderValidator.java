package com.ebs.dda.order.salesreturnorder;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;

import java.util.List;

public class DObSalesReturnOrderValidator {

  private ValidationResult validationResult;
  private IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep;

  public DObSalesReturnOrderValidator() {}

  public ValidationResult validate(String salesReturnOrderCode) throws Exception {
    validationResult = new ValidationResult();

    List<IObSalesReturnOrderItemGeneralModel> salesReturnItems =
        salesReturnOrderItemGeneralModelRep.findBySalesReturnOrderCodeOrderByIdDesc(
            salesReturnOrderCode);

    for (IObSalesReturnOrderItemGeneralModel item : salesReturnItems) {
      if (isReturnQuantityGreaterThanMaxReturnQuantity(item)) {
        validationResult.bindError(
            IIObSalesReturnOrderItemGeneralModel.RETURN_QUANTITY,
            ISalesReturnOrderMessages.FAILURE_MARK_AS_SHIPPED);
        break;
      }
    }

    return validationResult;
  }

  private boolean isReturnQuantityGreaterThanMaxReturnQuantity(
      IObSalesReturnOrderItemGeneralModel item) {
    return item.getReturnQuantity().compareTo(item.getMaxReturnQuantity()) > 0;
  }

  public void setSalesReturnOrderItemGeneralModelRep(
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep) {
    this.salesReturnOrderItemGeneralModelRep = salesReturnOrderItemGeneralModelRep;
  }
}
