package com.ebs.dda.order.salesorder;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderDataGeneralModel;

import java.util.*;

public class IObSalesOrderDataEditabilityManager extends AbstractEditabilityManager {
  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
                IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
                IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE,
                IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
                IIObSalesOrderDataGeneralModel.NOTES));

    Set<String> rejectedEnabledAttributes =
            new HashSet<>(
                    Arrays.asList(
                            IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS_ID,
                            IIObSalesOrderDataGeneralModel.CONTACT_PERSON_ID,
                            IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE,
                            IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE,
                            IIObSalesOrderDataGeneralModel.NOTES));

    enabledAttributesPerState.put(DObSalesOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(DObSalesOrderStateMachine.REJECTED, rejectedEnabledAttributes);
    enabledAttributesPerState.put(DObSalesOrderStateMachine.APPROVED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.DELIVERY_COMPLETE, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.CANCELED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.EXPIRED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.WAITING_APPROVAL, new HashSet<>());

    return enabledAttributesPerState;
  }
}
