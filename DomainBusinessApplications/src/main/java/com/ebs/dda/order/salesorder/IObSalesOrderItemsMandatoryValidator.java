package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class IObSalesOrderItemsMandatoryValidator implements ISectionMandatoryValidator {

  private IObSalesOrderItemsGeneralModelRep itemsGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;

  public IObSalesOrderItemsMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setItemsGeneralModelRep(IObSalesOrderItemsGeneralModelRep itemsGeneralModelRep) {
    this.itemsGeneralModelRep = itemsGeneralModelRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) {}

  @Override
  public List<String> validateGeneralModelMandatories(String salesOrderCode, String state) {
    List<String> itemsMissingFields = new ArrayList<>();

    List<IObSalesOrderItemGeneralModel> itemsGeneralModels =
        itemsGeneralModelRep.findBySalesOrderCodeOrderByIdDesc(salesOrderCode);

    if (itemsGeneralModels.isEmpty()) {
      itemsMissingFields.add(ISalesOrderItemsMandatoryAttributes.EMPTY_ITEMS_LIST);
    }

    return itemsMissingFields;
  }
}
