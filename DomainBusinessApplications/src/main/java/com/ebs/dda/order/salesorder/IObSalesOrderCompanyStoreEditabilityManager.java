package com.ebs.dda.order.salesorder;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderCompanyAndStoreDataGeneralModel;

import java.util.*;

public class IObSalesOrderCompanyStoreEditabilityManager extends AbstractEditabilityManager {
  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObSalesOrderCompanyAndStoreDataGeneralModel.STORE_CODE));

    Set<String> rejectedEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObSalesOrderCompanyAndStoreDataGeneralModel.STORE_CODE));

    enabledAttributesPerState.put(DObSalesOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(DObSalesOrderStateMachine.REJECTED, rejectedEnabledAttributes);
    enabledAttributesPerState.put(DObSalesOrderStateMachine.APPROVED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.CANCELED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.DELIVERY_COMPLETE, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.EXPIRED, new HashSet<>());
    enabledAttributesPerState.put(DObSalesOrderStateMachine.WAITING_APPROVAL, new HashSet<>());

    return enabledAttributesPerState;
  }
}
