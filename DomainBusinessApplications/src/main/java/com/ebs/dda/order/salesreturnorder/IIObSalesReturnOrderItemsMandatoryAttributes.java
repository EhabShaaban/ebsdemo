package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IIObSalesReturnOrderItemsMandatoryAttributes {
  String EMPTY_ITEMS_LIST = "ItemsList";
  String[] DRAFT_STATE_MANDATORIES = {
    IIObSalesReturnOrderItemGeneralModel.RETURN_QUANTITY,
  };
  String[] SHIPPED_STATE_MANDATORIES = {
    IIObSalesReturnOrderItemGeneralModel.RETURN_REASON_CODE,
    IIObSalesReturnOrderItemGeneralModel.RETURN_QUANTITY,
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObSalesReturnOrderStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObSalesReturnOrderStateMachine.SHIPPED_STATE, SHIPPED_STATE_MANDATORIES)
          .build();
}
