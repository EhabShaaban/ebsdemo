package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyAndStoreDataGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderCompanyAndStoreDataGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;
import java.util.Map;

public class IObSalesOrderCompanyStoreDataMandatoryValidator implements ISectionMandatoryValidator {

  private IObSalesOrderCompanyAndStoreDataGeneralModelRep companyStoreDataRep;
  private MandatoryValidatorUtils validatorUtils;

  public IObSalesOrderCompanyStoreDataMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setCompanyStoreDataRep(
      IObSalesOrderCompanyAndStoreDataGeneralModelRep companyStoreDataRep) {
    this.companyStoreDataRep = companyStoreDataRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) {}

  @Override
  public List<String> validateGeneralModelMandatories(String salesOrderCode, String state)
      throws Exception {
    IObSalesOrderCompanyAndStoreDataGeneralModel generalModel =
        companyStoreDataRep.findOneBySalesOrderCode(salesOrderCode);

    if (generalModel == null) {
      generalModel = new IObSalesOrderCompanyAndStoreDataGeneralModel();
    }

    Map<String, String[]> mandatories =
        ISalesOrderCompanyAndStoreDataMandatoryAttributes.MANDATORIES;
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);
    return validatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes, generalModel);
  }
}
