package com.ebs.dda.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import java.util.HashSet;
import java.util.Set;

public class DObSalesOrderStateMachine extends AbstractStateMachine {
  public static final String DRAFT_STATE = "Draft";
  public static final String WAITING_APPROVAL = "WaitingApproval";
  public static final String APPROVED = "Approved";
  public static final String GOODS_ISSUE_ACTIVATED = "GoodsIssueActivated";
  public static final String SALES_INVOICE_ACTIVATED = "SalesInvoiceActivated";
  public static final String REJECTED = "Rejected";
  public static final String CANCELED = "Canceled";
  public static final String EXPIRED = "Expired";
  public static final String READY_FOR_DELIVERY = "ReadyForDelivery";
  public static final String DELIVERY_COMPLETE = "DeliveryComplete";

  private Set<String> objectStates;
  private boolean initialized;

  private DObSalesOrder object;
  private DObSalesOrderGeneralModel objectGeneralModel;

  public DObSalesOrderStateMachine() throws Exception {
    super(
        DObSalesOrderStateMachine.class
            .getClassLoader()
            .getResource("META-INF/DObSalesOrder.scxml"));
  }

  public boolean goToDraft() {
    return true;
  }

  public boolean goToWaitingApproval() {
    return true;
  }

  public boolean goToApproved() {
    return true;
  }

  public boolean goToSalesInvoiceActivated() {
    return true;
  }

  public boolean goToGoodsIssueActivated() {
    return true;
  }

  public boolean goToRejected() {
    return true;
  }

  public boolean goToCanceled() {
    return true;
  }

  public boolean goToExpired() {
    return true;
  }

  public boolean goToReadyForDelivery() {
    return true;
  }

  public boolean goToDeliveryComplete() {
    return true;
  }

  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObSalesOrder) {
      this.object = (DObSalesOrder) obj;
    } else {
      this.objectGeneralModel = (DObSalesOrderGeneralModel) obj;
    }
  }
}
