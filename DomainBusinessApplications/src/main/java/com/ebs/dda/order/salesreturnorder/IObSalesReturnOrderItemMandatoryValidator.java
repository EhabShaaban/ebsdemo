package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.*;

public class IObSalesReturnOrderItemMandatoryValidator implements ISectionMandatoryValidator {

  private IObSalesReturnOrderItemGeneralModelRep itemRep;
  private MandatoryValidatorUtils validatorUtils;

  public IObSalesReturnOrderItemMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setItemRep(IObSalesReturnOrderItemGeneralModelRep itemRep) {
    this.itemRep = itemRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) {}

  @Override
  public List<String> validateGeneralModelMandatories(String salesReturnOrderCode, String nextState)
      throws Exception {
    HashSet<String> result = new HashSet<>();

    List<IObSalesReturnOrderItemGeneralModel> items =
        itemRep.findBySalesReturnOrderCodeOrderByIdDesc(salesReturnOrderCode);

    if (items.isEmpty()) {
      result.add(IIObSalesReturnOrderItemsMandatoryAttributes.EMPTY_ITEMS_LIST);
      return new ArrayList<>(result);
    }

    result = getItemsMissingFeilds(nextState, items);
    return new ArrayList<>(result);
  }

  private HashSet<String> getItemsMissingFeilds(
      String nextState, List<IObSalesReturnOrderItemGeneralModel> items) throws Exception {

    HashSet<String> result = new HashSet<>();
    Map<String, String[]> mandatories = IIObSalesReturnOrderItemsMandatoryAttributes.MANDATORIES;

    String currState = null;

    if (nextState.equals(DObSalesReturnOrderStateMachine.SHIPPED_STATE)) {
      currState = DObSalesReturnOrderStateMachine.DRAFT_STATE;
    }

    String[] currMandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(mandatories, currState);

    String[] nextMandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(mandatories, nextState);

    HashSet<String> mandatoryAttributesSet =
        new HashSet<>(Arrays.asList(nextMandatoryAttributes));
    mandatoryAttributesSet.removeAll(Arrays.asList(currMandatoryAttributes));

    String[] mandatoryAttributes = mandatoryAttributesSet.toArray(new String[0]);

    for (IObSalesReturnOrderItemGeneralModel item : items) {
      List<String> missingFields =
          validatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes, item);

      result.addAll(missingFields);

      if (result.size() == mandatoryAttributes.length) break;
    }

    return result;
  }
}
