package com.ebs.dda.order.salesreturnorder;

public interface IDObSalesReturnOrderActionNames {

  String READ_COMPANY_DATA = "ReadCompanyData";
  String READ_RETURN_DETAILS = "ReadReturnDetails";
  String READ_ITEMS = "ReadItems";
  String UPDATE_ITEMS = "UpdateItems";
  String MARK_AS_SHIPPED = "MarkAsShipped";
  String ACTIVATE_GOODS_RECEIPT = "ActivateGoodsReceipt";
}
