package com.ebs.dda.order.salesorder;

import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.validation.mandatoryvalidator.*;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesOrderMandatoryValidator {

  private IObSalesOrderCompanyStoreDataMandatoryValidator companyStoreDataMandatoryValidator;
  private IObSalesOrderDataMandatoryValidator salesOrderDataMandatoryValidator;
  private IObSalesOrderItemsMandatoryValidator salesOrderItemsMandatoryValidator;

  private MandatoryValidatorUtils validatorUtils;

  public DObSalesOrderMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public Map<String, Object> validateAllSectionsForState(String salesOrderCode, String state)
      throws Exception {
    Map<String, Object> salesOrderMissingFields = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(
        salesOrderMissingFields,
        IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION,
        validateCompanyStoreDataSection(salesOrderCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        salesOrderMissingFields,
        IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION,
        validateSalesOrderDataSection(salesOrderCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        salesOrderMissingFields,
        IDObSalesOrderSectionNames.ITEMS_SECTION,
        validateSalesOrderItemsSection(salesOrderCode, state));

    return salesOrderMissingFields;
  }

  private List<String> validateCompanyStoreDataSection(String salesOrderCode, String state)
      throws Exception {
    return companyStoreDataMandatoryValidator.validateGeneralModelMandatories(
        salesOrderCode, state);
  }

  private List<String> validateSalesOrderDataSection(String salesOrderCode, String state)
      throws Exception {
    return salesOrderDataMandatoryValidator.validateGeneralModelMandatories(salesOrderCode, state);
  }

  private List<String> validateSalesOrderItemsSection(String salesOrderCode, String state) {
    return salesOrderItemsMandatoryValidator.validateGeneralModelMandatories(salesOrderCode, state);
  }

  public void setCompanyStoreDataMandatoryValidator(
      IObSalesOrderCompanyStoreDataMandatoryValidator companyStoreDataMandatoryValidator) {
    this.companyStoreDataMandatoryValidator = companyStoreDataMandatoryValidator;
  }

  public void setSalesOrderDataMandatoryValidator(
      IObSalesOrderDataMandatoryValidator salesOrderDataMandatoryValidator) {
    this.salesOrderDataMandatoryValidator = salesOrderDataMandatoryValidator;
  }

  public void setSalesOrderItemsMandatoryValidator(
      IObSalesOrderItemsMandatoryValidator salesOrderItemsMandatoryValidator) {
    this.salesOrderItemsMandatoryValidator = salesOrderItemsMandatoryValidator;
  }
}
