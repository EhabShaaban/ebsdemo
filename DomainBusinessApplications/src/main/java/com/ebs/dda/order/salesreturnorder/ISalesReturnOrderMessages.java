package com.ebs.dda.order.salesreturnorder;

public interface ISalesReturnOrderMessages {
  String SUCCESS_MARK_AS_SHIPPED = "SRO-msg-01";
  String FAILURE_MARK_AS_SHIPPED = "SRO-msg-02";
}
