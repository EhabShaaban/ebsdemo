package com.ebs.dda.order.salesreturnorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.CObSalesReturnOrderType;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderCreateValueObject;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.repositories.order.salesreturnorder.CObSalesReturnOrderTypeRep;

public class DObSalesReturnOrderCreateValidator {

  private final String deliveredToCustomerState = "%Active%";
  private ValidationResult validationResult;
  private CObSalesReturnOrderTypeRep salesReturnOrderTypeRep;
  private CObProductManagerRep productManagerRep;
  private DocumentOwnerGeneralModelRep documentOwnerRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public ValidationResult validate(DObSalesReturnOrderCreateValueObject creationValueObject)
      throws Exception {
    validationResult = new ValidationResult();

    checkThatTypeExists(creationValueObject.getTypeCode());
    checkThatDocumentOwnerExists(creationValueObject.getDocumentOwnerId());
    checkThatSalesInvoiceIsDeliveredToCustomer(creationValueObject.getSalesInvoiceCode());
    return validationResult;
  }

  private void checkThatSalesInvoiceIsDeliveredToCustomer(String salesInvoiceCode)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCodeAndCurrentStatesLike(
            salesInvoiceCode, deliveredToCustomerState);
    if (salesInvoice == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatTypeExists(String typeCode) throws Exception {
    CObSalesReturnOrderType salesReturnOrderType =
        salesReturnOrderTypeRep.findOneByUserCode(typeCode);
    if (salesReturnOrderType == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private boolean checkThatDocumentOwnerExists(Long documentOwnerId) throws Exception {
    DocumentOwnerGeneralModel documentOwner =
        documentOwnerRep.findOneByUserIdAndObjectName(
            documentOwnerId, IDObSalesReturnOrder.SYS_NAME);
    if (documentOwner == null) {
      validationResult.bindError(
          IDObSalesReturnOrderCreateValueObject.DOCUMENT_OWNER_ID,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
      return false;
    }
    return true;
  }

  public void setSalesReturnOrderTypeRep(CObSalesReturnOrderTypeRep salesReturnOrderTypeRep) {
    this.salesReturnOrderTypeRep = salesReturnOrderTypeRep;
  }

  public void setProductManagerRep(CObProductManagerRep productManagerRep) {
    this.productManagerRep = productManagerRep;
  }

  public void setDocumentOwnerRep(DocumentOwnerGeneralModelRep documentOwnerRep) {
    this.documentOwnerRep = documentOwnerRep;
  }

  public void setDObSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }
}
