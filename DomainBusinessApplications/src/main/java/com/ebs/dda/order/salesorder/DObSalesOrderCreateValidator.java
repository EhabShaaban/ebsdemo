package com.ebs.dda.order.salesorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.salesresponsible.CObSalesResponsible;
import com.ebs.dda.jpa.order.salesorder.CObSalesOrderType;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderCreateValueObject;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.salesresponsible.CObSalesResponsibleRep;
import com.ebs.dda.repositories.order.salesorder.CObSalesOrderTypeRep;

public class DObSalesOrderCreateValidator {

  private ValidationResult validationResult;
  private CObSalesOrderTypeRep salesOrderTypeRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private MObCustomerRep customerRep;
  private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  private CObSalesResponsibleRep salesResponsibleRep;
  private CObCompanyGeneralModelRep companyGeneralModelRep;

  public void validateMandatoryFields(DObSalesOrderCreateValueObject creationValueObject)
      throws Exception {
    checkCodeIsNotNull(creationValueObject.getTypeCode());
    checkCodeIsNotNull(creationValueObject.getBusinessUnitCode());
    checkCodeIsNotNull(creationValueObject.getCompanyCode());
    checkCodeIsNotNull(creationValueObject.getCustomerCode());
    checkCodeIsNotNull(creationValueObject.getSalesResponsibleCode());
    checkThatTypeExists(creationValueObject.getTypeCode());
  }

  public ValidationResult validate(DObSalesOrderCreateValueObject creationValueObject)
      throws Exception {
    validationResult = new ValidationResult();

    checkThatCompanyExists(creationValueObject.getCompanyCode());
    checkThatCustomerIsBelongsToBusinessUnit(
        creationValueObject.getCustomerCode(), creationValueObject.getBusinessUnitCode());
    checkThatSalesResponsibleExists(creationValueObject.getSalesResponsibleCode());
    return validationResult;
  }

  private void checkThatCompanyExists(String companyCode) {
    CObCompanyGeneralModel company = companyGeneralModelRep.findOneByUserCode(companyCode);
    if (company == null) {
      validationResult.bindError(
          IDObSalesOrderCreateValueObject.COMPANY_CODE,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkThatCustomerIsBelongsToBusinessUnit(
      String customerCode, String businessUnitCode) throws Exception {
    if (checkThatBusinessUnitExists(businessUnitCode) && checkThatCustomerExists(customerCode)) {
      IObCustomerBusinessUnitGeneralModel customerBusinessUnitGeneralModel =
          customerBusinessUnitGeneralModelRep.findOneByUserCodeAndBusinessUnitcode(
              customerCode, businessUnitCode);
      if (customerBusinessUnitGeneralModel == null) {
        validationResult.bindError(
            IDObSalesOrderCreateValueObject.CUSTOMER_CODE,
            IExceptionsCodes.General_OBJECT_CHANGE_BUSINESS_UNIT);
      }
    }
  }

  private void checkThatSalesResponsibleExists(String salesResponsibleCode) {
    CObSalesResponsible salesResponsible =
        salesResponsibleRep.findOneByUserCode(salesResponsibleCode);
    if (salesResponsible == null) {
      validationResult.bindError(
          IDObSalesOrderCreateValueObject.SALES_RESPONSIBLE_CODE,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private boolean checkThatCustomerExists(String customerCode) {
    MObCustomer customer = customerRep.findOneByUserCode(customerCode);
    if (customer == null) {
      validationResult.bindError(
          IDObSalesOrderCreateValueObject.CUSTOMER_CODE,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
      return false;
    }
    return true;
  }

  private boolean checkThatBusinessUnitExists(String businessUnitCode) {
    CObPurchasingUnit businessUnit = purchasingUnitRep.findOneByUserCode(businessUnitCode);
    if (businessUnit == null) {
      validationResult.bindError(
          IDObSalesOrderCreateValueObject.BUSINESS_UNIT_CODE,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
      return false;
    }
    return true;
  }

  private void checkThatTypeExists(String typeCode) throws Exception {
    CObSalesOrderType salesOrderType = salesOrderTypeRep.findOneByUserCode(typeCode);
    if (salesOrderType == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkCodeIsNotNull(String code) throws Exception {
    if (code == null || code.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setSalesOrderTypeRep(CObSalesOrderTypeRep salesOrderTypeRep) {
    this.salesOrderTypeRep = salesOrderTypeRep;
  }

  public void setCompanyGeneralModelRep(
      CObCompanyGeneralModelRep companyGeneralModelRep) {
    this.companyGeneralModelRep = companyGeneralModelRep;
  }

  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }

  public void setCustomerBusinessUnitGeneralModelRep(
      IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep) {
    this.customerBusinessUnitGeneralModelRep = customerBusinessUnitGeneralModelRep;
  }

  public void setSalesResponsibleRep(CObSalesResponsibleRep salesResponsibleRep) {
    this.salesResponsibleRep = salesResponsibleRep;
  }
}
