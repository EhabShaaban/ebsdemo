package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderCompanyAndStoreDataGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface ISalesOrderCompanyAndStoreDataMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {};
  String[] REJECTED_STATE_MANDATORIES = {
    IIObSalesOrderCompanyAndStoreDataGeneralModel.COMPANY_CODE,
    IIObSalesOrderCompanyAndStoreDataGeneralModel.STORE_CODE
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObSalesOrderStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.APPROVED, REJECTED_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.REJECTED, REJECTED_STATE_MANDATORIES)
          .build();
}
