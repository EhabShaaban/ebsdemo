package com.ebs.dda.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import java.util.HashSet;
import java.util.Set;

public class DObSalesReturnOrderStateMachine extends AbstractStateMachine {
  public static final String DRAFT_STATE = "Draft";
  public static final String SHIPPED_STATE = "Shipped";
  public static final String GOODS_RECEIPT_ACTIVATED_STATE = "GoodsReceiptActivated";
  public static final String CREDIT_NOTE_ACTIVATED_STATE = "CreditNoteActivated";
  public static final String COMPLETED_STATE = "Completed";

  private Set<String> objectStates;
  private boolean initialized;

  private DObSalesReturnOrder object;
  private DObSalesReturnOrderGeneralModel objectGeneralModel;

  public DObSalesReturnOrderStateMachine() throws Exception {
    super(
        DObSalesReturnOrderStateMachine.class
            .getClassLoader()
            .getResource("META-INF/DObSalesReturnOrder.scxml"));
  }

  public boolean goToDraft() {
    return true;
  }

  public boolean goToShipped() {
    return true;
  }

  public boolean goToCreditNoteActivated() {
    return true;
  }

  public boolean goToGoodsReceiptActivated() {
    return true;
  }

  public boolean goToCompleted() {
    return true;
  }

  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObSalesReturnOrder) {
      this.object = (DObSalesReturnOrder) obj;
    } else {
      this.objectGeneralModel = (DObSalesReturnOrderGeneralModel) obj;
    }
  }
}
