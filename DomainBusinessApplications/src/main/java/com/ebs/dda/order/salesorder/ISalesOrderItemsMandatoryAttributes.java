package com.ebs.dda.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderItemGeneralModel;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface ISalesOrderItemsMandatoryAttributes {
  String EMPTY_ITEMS_LIST = "ItemsList";
  String[] DRAFT_STATE_MANDATORIES = {
    IIObSalesOrderItemGeneralModel.ITEM_CODE,
    IIObSalesOrderItemGeneralModel.UNIT_OF_MEASURE_CODE,
    IIObSalesOrderItemGeneralModel.QUANTITY,
    IIObSalesOrderItemGeneralModel.SALES_PRICE
  };
  String[] REJECTED_STATE_MANDATORIES = {
    IIObSalesOrderItemGeneralModel.ITEM_CODE,
    IIObSalesOrderItemGeneralModel.UNIT_OF_MEASURE_CODE,
    IIObSalesOrderItemGeneralModel.QUANTITY,
    IIObSalesOrderItemGeneralModel.SALES_PRICE
  };
  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObSalesOrderStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObSalesOrderStateMachine.REJECTED, REJECTED_STATE_MANDATORIES)
          .build();
}
