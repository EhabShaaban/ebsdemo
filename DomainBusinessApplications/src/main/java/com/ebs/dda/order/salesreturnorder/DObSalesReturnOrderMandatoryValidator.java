package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesReturnOrderMandatoryValidator {

  private IObSalesReturnOrderItemMandatoryValidator itemMandatoryValidator;

  private MandatoryValidatorUtils validatorUtils;

  public DObSalesReturnOrderMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public Map<String, Object> validateAllSectionsForState(
      String salesReturnOrderCode, String state) throws Exception {
    Map<String, Object> missingFields = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(
        missingFields,
        IDObSalesReturnOrderSectionNames.ITEMS_SECTION,
        validateSalesReturnOrderItemsSection(salesReturnOrderCode, state));

    return missingFields;
  }

  private List<String> validateSalesReturnOrderItemsSection(
      String salesReturnOrderCode, String state) throws Exception {
    return itemMandatoryValidator.validateGeneralModelMandatories(salesReturnOrderCode, state);
  }

  public void setItemMandatoryValidator(
      IObSalesReturnOrderItemMandatoryValidator itemMandatoryValidator) {
    this.itemMandatoryValidator = itemMandatoryValidator;
  }
}
