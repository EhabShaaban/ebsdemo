package com.ebs.dda.validation.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MandatoryValidatorUtils {

  private ValidationUtils validationUtils;

  public MandatoryValidatorUtils() {
    validationUtils = new ValidationUtils();
  }

  public Set<String> getMandatoryAttributesByState(
      Map<String, String[]> mandatories, Set<String> currentStates) {
    Set<String> mandatorySet = new HashSet<String>();
    for (String state : currentStates) {
      String[] stateMandatories = mandatories.get(state);
      if (stateMandatories != null && stateMandatories.length > 0) {
        mandatorySet.addAll(Arrays.asList(stateMandatories));
      }
    }
    return mandatorySet;
  }

  public String[] getMandatoryAttributesByState(
      Map<String, String[]> mandatories, String currentState) {
    return mandatories.get(currentState);
  }

  public String[] getMandatoryAttributesByKey(Map<String, String[]> mandatories, String keyName) {
    return mandatories.get(keyName);
  }

  public void validateMandatoriesForValueObject(Set<String> mandatoryAttributes, Object valueObject)
      throws Exception {
    for (String mandatoryAttribute : mandatoryAttributes) {
      Object mandatoryAttributeValue =
          validationUtils.getAttributeValue(mandatoryAttribute, valueObject);
      if (checkIfNullOrEmpty(mandatoryAttributeValue)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  public void validateMandatoriesForValueObjectAsObjectState(
      Map<String, String[]> mandatoryAttributes, String currentState, Object valueObject)
      throws Exception {
    String[] currentStateMandatories = mandatoryAttributes.get(currentState);
    for (String mandatoryAttribute : currentStateMandatories) {
      Object mandatoryAttributeValue =
          validationUtils.getAttributeValue(mandatoryAttribute, valueObject);
      if (checkIfNullOrEmpty(mandatoryAttributeValue)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  public List<String> validateMandatoriesForGeneralModel(
      String[] mandatoryAttributes, Object valueObject) throws RuntimeException {
    List<String> validationResult = new ArrayList<>();
    if (mandatoryAttributes == null) return validationResult;
    for (String mandatoryAttribute : mandatoryAttributes) {
      Object mandatoryAttributeValue =
          validationUtils.getAttributeValue(mandatoryAttribute, valueObject);
      if (checkIfNullOrEmpty(mandatoryAttributeValue)) {
        validationResult.add(mandatoryAttribute);
      }
    }
    return validationResult;
  }

  public void populateSectionMissingFieldstoResult(
      Map<String, List<String>> allSectionsMissingFields,
      String sectionKey,
      List<String> sectionMissingFields) {
    if (sectionMissingFields.isEmpty()) return;
    allSectionsMissingFields.put(sectionKey, sectionMissingFields);
  }

  public void populateSectionMissingFieldstoResult(
      Map<String, Object> allSectionsMissingFields,
      String sectionKey,
      Object sectionMissingFields) {
    if (sectionMissingFields instanceof List && ((List<?>) sectionMissingFields).isEmpty()) return;
    if (sectionMissingFields instanceof Map && ((Map<?, ?>) sectionMissingFields).isEmpty()) return;
    allSectionsMissingFields.put(sectionKey, sectionMissingFields);
  }

  public String[] getInterSectionBetweenTwoMandatoryArrays(
      String[] firstArray, String[] secondArray) {
    Set<String> interSectionSet = new HashSet<>(Arrays.asList(firstArray));
    interSectionSet.retainAll(Arrays.asList(secondArray));
    return Arrays.copyOf(interSectionSet.toArray(), interSectionSet.size(), String[].class);
  }

  public String[] getAllMandatoriesBetweenTwoArrays(String[] firstArray, String[] secondArray) {
    Set<String> interSectionSet = new HashSet<>(Arrays.asList(firstArray));
    for (String str : secondArray) {
      interSectionSet.add(str);
    }
    return Arrays.copyOf(interSectionSet.toArray(), interSectionSet.size(), String[].class);
  }

  public boolean checkIfNullOrEmpty(Object value) {
    return (value == null || ("").equals(value));
  }
}
