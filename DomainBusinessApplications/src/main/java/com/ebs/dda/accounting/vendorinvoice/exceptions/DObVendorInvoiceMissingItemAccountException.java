package com.ebs.dda.accounting.vendorinvoice.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class DObVendorInvoiceMissingItemAccountException extends BusinessRuleViolationException {
    public DObVendorInvoiceMissingItemAccountException(ValidationResult validationResult) {
        super(validationResult);
    }

    @Override
    public String getResponseCode() {
        return IExceptionsCodes.INVOICE_ITEM_HAS_NO_ACCOUNT;
    }
}
