package com.ebs.dda.accounting.vendorinvoice.apis;

import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IDObInvoiceItemsMandatoryAttributes {

	String[] DRAFT_STATE_MANDATORIES = {IIObInvoiceItemsGeneralModel.ITEM_CODE,
			IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT,
			IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE,
			IIObInvoiceItemsGeneralModel.ITEM_PRICE
			};
	Map<String, String[]> INVOICE_ITEMS_MANDATORIES = ImmutableMap
			.<String, String[]>builder()
			.put(DObVendorInvoiceStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
			.build();
}
