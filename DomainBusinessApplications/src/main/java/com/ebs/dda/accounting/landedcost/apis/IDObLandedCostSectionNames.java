package com.ebs.dda.accounting.landedcost.apis;

public interface IDObLandedCostSectionNames {

  String LANDED_COST_SECTION = "LandedCostDetails";
  String COST_FACTOR_ITEM_SECTION = "CostFactorItem";
}
