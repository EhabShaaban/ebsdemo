package com.ebs.dda.accounting.landedcost.apis;

public interface IDObLandedCostJsonSchema {
  String CREATION_SCHEMA =
          "{\n" +
                  "  \"properties\": {\n" +
                  "    \"landedCostType\": {\n" +
                  "      \"type\": [\n" +
                  "        \"string\"\n" +
                  "      ],\n" +
                  "      \"enum\": [\n" +
                  "        \"IMPORT\",\n" +
                  "        \"LOCAL\"\n" +
                  "      ]\n" +
                  "    },\n" +
                  "    \"purchaseOrderCode\": {\n" +
                  "      \"type\": [\n" +
                  "        \"string\"\n" +
                  "      ]},\n" +
                  "      \"documentOwnerId\": {\n" +
                  "        \"type\": [\n" +
                  "          \"integer\"\n" +
                  "        ]\n" +
                  "      },\n" +
                  "      \"minLength\": 10,\n" +
                  "      \"maxLength\": 10,\n" +
                  "      \"pattern\": \"^([0-9])*$\"\n" +
                  "\n" +
                  "  }\n" +
                  "}";

  String COST_FACTOR_ITEMS_SCHEMA = "{\n" +
          "  \"properties\": {\n" +
          "    \"itemCode\": {\n" +
          "      \"type\": [\n" +
          "        \"string\"\n" +
          "      ],\n" +
          "      \"minLength\": 6,\n" +
          "      \"maxLength\": 6,\n" +
          "      \"pattern\": \"^([0-9])*$\"\n" +
          "    },\n" +
          "    \"value\": {\n" +
          "      \"type\": [\n" +
          "        \"number\"\n" +
          "      ],\n" +
          "      \"minimum\": 0.001,\n" +
          "      \"maximum\": 1000000000,\n" +
          "      \"multipleOf\": 0.000001\n" +
          "\n" +
          "    }\n" +
          "  }\n" +
          "}\n" +
          "\n" +
          "\n" +
          "\n" +
          "\n" +
          "\n";
}