package com.ebs.dda.accounting.vendorinvoice.editiablity;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DObVendorInvoiceAddItemEditabilityManager extends AbstractEditabilityManager {

  @Override
	public Map<String, Set<String>> getEnabledAttributesConfig() {
		Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
		Set<String> draftEnabledAttributes = new HashSet<>();
		draftEnabledAttributes.addAll(
				Arrays.asList(
						IIObInvoiceItemsGeneralModel.ITEM_CODE,
						IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT,
						IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE,
						IIObInvoiceItemsGeneralModel.ITEM_PRICE));

		enabledAttributesPerState.put(DObVendorInvoiceStateMachine.DRAFT_STATE, draftEnabledAttributes);

		return enabledAttributesPerState;
	}

	public Map<String, Set<String>> getEnabledAttributesConfigForRequestEdit() {
		Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
		Set<String> draftEnabledAttributes = new HashSet<>();
		draftEnabledAttributes.addAll(
				Arrays.asList(
						IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT,
						IIObInvoiceItemsGeneralModel.ITEM_PRICE));

		enabledAttributesPerState.put(DObVendorInvoiceStateMachine.DRAFT_STATE, draftEnabledAttributes);

		return enabledAttributesPerState;
	}
}
