package com.ebs.dda.accounting.vendorinvoice.validation.apis;

import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoiceDetails;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IIObInvoiceDetailsMandatoryAttributes {

  String[] POSTED_STATE_MANDATORIES = {
    IIObVendorInvoiceDetails.INVOICE_DATE,
    IIObVendorInvoiceDetails.CURRENCY_ISO,
    IIObVendorInvoiceDetails.INVOICE_NUMBER
  };
  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(DObVendorInvoiceStateMachine.POSTED_STATE, POSTED_STATE_MANDATORIES)
          .build();
}
