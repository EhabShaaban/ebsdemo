package com.ebs.dda.accounting.settlement.apis;

public interface IDObSettlementSectionNames {

  String ACCOUNTING_DETAILS_SECTION = "AccountingDetails";
  String SETTLEMENT_DETAILS_SECTION = "SettlementDetails";
}
