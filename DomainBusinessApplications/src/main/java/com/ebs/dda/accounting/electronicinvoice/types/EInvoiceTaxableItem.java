package com.ebs.dda.accounting.electronicinvoice.types;

import java.math.BigDecimal;

public class EInvoiceTaxableItem {
    private String taxType;
    private BigDecimal rate;
    private String subType;
    private BigDecimal amount;
}
