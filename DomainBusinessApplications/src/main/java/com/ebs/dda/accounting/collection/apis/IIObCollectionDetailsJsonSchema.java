package com.ebs.dda.accounting.collection.apis;

public interface IIObCollectionDetailsJsonSchema {
  String COLLECTION_DETAILS_SCHEMA =
      "{\n" +
              "  \"properties\": {\n" +
              "    \"bankAccountNumber\": {\n" +
              "      \"type\": [\n" +
              "        \"string\",\n" +
              "        \"null\"\n" +
              "      ],\n" +
              "      \"pattern\": \"^([ A-Za-z0-9/-])*$\"\n" +
              "    },\n" +
              "    \"treasuryCode\": {\n" +
              "      \"type\": [\n" +
              "        \"string\",\n" +
              "        \"null\"\n" +
              "      ],\n" +
              "      \"minLength\": 4,\n" +
              "      \"maxLength\": 4,\n" +
              "      \"pattern\": \"^([0-9])*$\"\n" +
              "    },\n" +
              "    \"amount\": {\n" +
              "      \"type\": [\n" +
              "        \"number\",\n" +
              "        \"null\"\n" +
              "      ],\n" +
              "      \"maximum\": 1000000000,\n" +
              "      \"minimum\": 0.000001,\n" +
              "      \"multipleOf\": 0.000001\n" +
              "    }\n" +
              "  }\n" +
              "}";
}
