package com.ebs.dda.accounting.salesinvoice.validation.apis;

import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface ISalesInvoiceItemsMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {
    IIObInvoiceItemsGeneralModel.ITEM_CODE,
    IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT,
    IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE,
    IIObInvoiceItemsGeneralModel.ITEM_PRICE
  };

  String[] POSTED_STATE_MANDATORIES = {};

  Map<String, String[]> MANDATORIES =
          ImmutableMap.<String, String[]>builder()
                  .put(DObSalesInvoiceWithoutReferenceStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
                  .put(DObSalesInvoiceWithoutReferenceStateMachine.READY_FOR_DELIVERING_STATE, POSTED_STATE_MANDATORIES)
                  .put(DObSalesInvoiceWithoutReferenceStateMachine.DELIVERED_TO_CUSTOMER_STATE, POSTED_STATE_MANDATORIES)
          .build();
}
