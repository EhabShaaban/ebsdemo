package com.ebs.dda.accounting.collection.apis;

public interface IDObCollectionActionNames {

  String ACTIVATE = "Activate";
  String READ_COMPANY = "ReadCompany";
  String READ_ACCOUNTING_DETAILS = "ReadAccountingDetails";
  String READ_COLLECTION_DETAILS = "ReadCollectionDetails";
  String READ_Activation_DETAILS = "ReadActivationDetails";
  String UPDATE_COLLECTION_DETAILS = "UpdateCollectionDetails";

}
