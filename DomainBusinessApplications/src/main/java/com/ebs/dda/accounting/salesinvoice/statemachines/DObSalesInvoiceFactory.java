package com.ebs.dda.accounting.salesinvoice.statemachines;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.InvalidStateMachineException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceDeliverToCustomerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;

public class DObSalesInvoiceFactory {

  public static final String LOCAL_TYPE = "SALES_INVOICE_WITHOUT_REFERENCE";
  public static final String ORDER_BASED_TYPE = "SALES_INVOICE_FOR_SALES_ORDER";

  private StatefullBusinessObject salesInvoice;

  public AbstractStateMachine createSalesInvoiceStateMachine(StatefullBusinessObject invoice)
      throws Exception {
    this.salesInvoice = invoice;
    String invoiceTypeCode = getInvoiceTypeCode(invoice);
    if (invoiceTypeCode.equals(LOCAL_TYPE)) {
      return createLocalSalesInvoiceStateMachine();
    }
    if (invoiceTypeCode.equals(ORDER_BASED_TYPE)) {
      return createOrderBasedSalesInvoiceStateMachine();
    }
    throw new InvalidStateMachineException(
        "DObSalesInvoiceFactory : No suitable state machine for this sales invoice type");
  }

  private String getInvoiceTypeCode(StatefullBusinessObject salesInvoice) {
    if(salesInvoice instanceof DObSalesInvoice) {
      if(((DObSalesInvoice) salesInvoice).getTypeId() == 1) {
        return LOCAL_TYPE;
      } else if(((DObSalesInvoice) salesInvoice).getTypeId() == 2) {
        return ORDER_BASED_TYPE;
      }
    } else if (salesInvoice instanceof DObSalesInvoiceGeneralModel) {
      return ((DObSalesInvoiceGeneralModel) salesInvoice).getInvoiceTypeCode();
    }
    else if (salesInvoice instanceof DObSalesInvoiceDeliverToCustomerGeneralModel) {
      return ((DObSalesInvoiceDeliverToCustomerGeneralModel) salesInvoice).getInvoiceTypeCode();
    }
    return null;
  }

  private DObSalesInvoiceWithoutReferenceStateMachine createLocalSalesInvoiceStateMachine()
      throws Exception {
    DObSalesInvoiceWithoutReferenceStateMachine localSalesInvoiceStateMachine =
        new DObSalesInvoiceWithoutReferenceStateMachine();
    localSalesInvoiceStateMachine.initObjectState(this.salesInvoice);
    return localSalesInvoiceStateMachine;
  }

  private DObSalesInvoiceSalesOrderBasedStateMachine createOrderBasedSalesInvoiceStateMachine()
      throws Exception {
    DObSalesInvoiceSalesOrderBasedStateMachine orderBasedSalesInvoiceStateMachine =
        new DObSalesInvoiceSalesOrderBasedStateMachine();
    orderBasedSalesInvoiceStateMachine.initObjectState(this.salesInvoice);
    return orderBasedSalesInvoiceStateMachine;
  }

  public void isAllowedAction(StatefullBusinessObject invoice, String actionName) throws Exception {
    this.salesInvoice = invoice;
    String invoiceTypeCode = getInvoiceTypeCode(invoice);
    if (invoiceTypeCode.equals(LOCAL_TYPE)) {
      DObSalesInvoiceWithoutReferenceStateMachine salesInvoiceStateMachine =
          new DObSalesInvoiceWithoutReferenceStateMachine();
        salesInvoiceStateMachine.initObjectState(invoice);
      salesInvoiceStateMachine.isAllowedAction(actionName);
    }
    if (invoiceTypeCode.equals(ORDER_BASED_TYPE)) {
      DObSalesInvoiceSalesOrderBasedStateMachine salesInvoiceStateMachine =
          new DObSalesInvoiceSalesOrderBasedStateMachine();
        salesInvoiceStateMachine.initObjectState(invoice);
      salesInvoiceStateMachine.isAllowedAction(actionName);
    }
  }
}
