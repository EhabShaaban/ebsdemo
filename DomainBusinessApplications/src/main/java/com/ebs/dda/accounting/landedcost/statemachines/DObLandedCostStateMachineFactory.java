package com.ebs.dda.accounting.landedcost.statemachines;

import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

public class DObLandedCostStateMachineFactory {

  public static final String DRAFT_STATE = "Draft";

  public AbstractStateMachine createStateMachine(DObAccountingDocument landedCost) throws Exception {
    DObLandedCostStateMachine stateMachine = new DObLandedCostStateMachine();
    stateMachine.initObjectState(landedCost);
    return stateMachine;
  }
  public void isAllowedAction(DObAccountingDocument landedCost, String actionName) throws Exception {
      DObLandedCostStateMachine landedCostStateMachine =
              new DObLandedCostStateMachine();
      landedCostStateMachine.initObjectState(landedCost);
      landedCostStateMachine.isAllowedAction(actionName);

  }


}
