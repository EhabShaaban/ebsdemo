package com.ebs.dda.accounting.landedcost.validation.api;

import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostDetailsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IIObLandedCostConvertToActualMandatoryAttributes {

  String[] CONVERT_TO_ACTUAL_STATE_MANDATORIES = {
    IIObLandedCostDetailsGeneralModel.VENDOR_INVOICE_CODE
  };
  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED, CONVERT_TO_ACTUAL_STATE_MANDATORIES)
          .build();
}
