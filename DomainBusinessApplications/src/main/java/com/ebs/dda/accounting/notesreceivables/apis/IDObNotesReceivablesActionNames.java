package com.ebs.dda.accounting.notesreceivables.apis;

public interface IDObNotesReceivablesActionNames {
  String ACTIVATE = "Activate";
  String CASH = "Cash";
  String READ_COMPANY_DATA = "ReadCompanyData";
  String READ_NOTES_RECEIVABLE_DETAILS = "ReadNotesReceivableDetails";
  String READ_ACCOUNTING_DETAILS = "ReadAccountingDetails";
  String READ_REFERENCE_DOCUMENTS = "ReadReferenceDocuments";
  String UPDATE_NOTES_DETAILS_SECTION = "UpdateNotesReceivableDetails";
  String ADD_REFERENCE_DOCUMENT = "AddReferenceDocument";
  String DELETE_REFERENCE_DOCUMENT = "DeleteReferenceDocument";
  String READ_ACTIVATION_DETAILS = "ReadActivationDetails";


}
