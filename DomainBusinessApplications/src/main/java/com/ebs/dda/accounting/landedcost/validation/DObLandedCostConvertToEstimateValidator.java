package com.ebs.dda.accounting.landedcost.validation;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_UC_Failure_MISSING_MANDATORIES;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.PURCHASE_ORDER_HAS_POSTED_LANDED_COST;
import static com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostCreationValueObject.PURCHASE_ORDER_CODE;
import static com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine.CANCELLED_STATE;
import static com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine.DRAFT_STATE;
import java.util.List;
import java.util.Set;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsGeneralModelRep;

public class DObLandedCostConvertToEstimateValidator {

  private ValidationResult validationResult;

  private IObOrderCompanyGeneralModelRep purhcaseOrderGMRep;
  private DObLandedCostGeneralModelRep landedCostGMRep;
  private IObLandedCostFactorItemsGeneralModelRep landedCostFactorItemsGeneralModelRep;

  public ValidationResult validateBusinessRules(String landedCostCode) throws Exception {
    validationResult = new ValidationResult();

    DObLandedCostGeneralModel landedCost = landedCostGMRep.findOneByUserCode(landedCostCode);
    validateLandedCost(landedCost);
    validatePurchaseOrder(landedCost.getPurchaseOrderCode());
    return validationResult;
  }

  private void validateLandedCost(DObLandedCostGeneralModel landedCost) throws Exception {
    checkThatLandedCostIsExist(landedCost);
    checkThatLandedCostInDraftState(landedCost);
    checkThatAtLeastOneCostFactorItemExist(landedCost.getUserCode());
  }

  private void checkThatLandedCostIsExist(DObLandedCostGeneralModel landedCost) throws Exception {
    if (landedCost == null) {
      throw new InstanceNotExistException();
    }
  }

  private void checkThatLandedCostInDraftState(DObLandedCostGeneralModel landedCost)
      throws Exception {

    Set<String> states = landedCost.getCurrentStates();
    if (states.contains(DRAFT_STATE)) {
      return;
    }
    throw new ActionNotAllowedPerStateException(DObLandedCost.class, landedCost.getId(),
        landedCost.getCurrentState(), IDObLandedCostActionNames.CONVERT_TO_ESTIMATE);
  }

  private void checkThatAtLeastOneCostFactorItemExist(String code) throws Exception {
    List<IObLandedCostFactorItemsGeneralModel> landedCostFactorItems =
        landedCostFactorItemsGeneralModelRep.findAllByCodeOrderByIdDesc(code);
    if (landedCostFactorItems.isEmpty()) {
      validationResult.bindError(IIObLandedCostFactorItemsGeneralModel.ITEM_CODE,
          General_UC_Failure_MISSING_MANDATORIES);
    }
  }

  private void validatePurchaseOrder(String code) throws Exception {
    IObOrderCompanyGeneralModel po = purhcaseOrderGMRep.findByUserCode(code);
    checkThatPurchaseOrderHasNotPostedLandedCost(code);
    checkThatPurchaseOrderInCorrectState(po);
  }

  private void checkThatPurchaseOrderInCorrectState(IObOrderCompanyGeneralModel po)
      throws Exception {
    Set<String> states = po.getCurrentStates();
    if (states.contains(CANCELLED_STATE)) {
      validationResult.bindError(PURCHASE_ORDER_CODE, General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
    }
  }

  private void checkThatPurchaseOrderHasNotPostedLandedCost(String purchaseOrderCode) {
    DObLandedCostGeneralModel landedCost =
        landedCostGMRep.findByPurchaseOrderCodeAndCurrentStatesNotLike(purchaseOrderCode,
            "%" + BasicStateMachine.DRAFT_STATE + "%");

    if (landedCost != null) {
      validationResult.bindError(PURCHASE_ORDER_CODE, PURCHASE_ORDER_HAS_POSTED_LANDED_COST);
    }
  }

  public void setPurhcaseOrderGMRep(IObOrderCompanyGeneralModelRep purhcaseOrderGMRep) {
    this.purhcaseOrderGMRep = purhcaseOrderGMRep;
  }

  public void setLandedCostGMRep(DObLandedCostGeneralModelRep landedCostGMRep) {
    this.landedCostGMRep = landedCostGMRep;
  }

  public void setLandedCostFactorItemsGeneralModelRep(
      IObLandedCostFactorItemsGeneralModelRep landedCostFactorItemsGeneralModelRep) {
    this.landedCostFactorItemsGeneralModelRep = landedCostFactorItemsGeneralModelRep;
  }
}
