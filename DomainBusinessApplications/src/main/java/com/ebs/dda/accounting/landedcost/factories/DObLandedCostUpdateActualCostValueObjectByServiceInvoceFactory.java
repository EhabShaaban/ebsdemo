package com.ebs.dda.accounting.landedcost.factories;

import java.math.BigDecimal;
import java.util.List;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostUpdateActualCostValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;

public class DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory {

  private DObVendorInvoiceGeneralModelRep serviceInvoiceRep;
  private IObVendorInvoiceItemsGeneralModelRep serviceInvoiceItemsRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderRep;

  public DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory(
      DObVendorInvoiceGeneralModelRep serviceInvoiceRep,
      IObVendorInvoiceItemsGeneralModelRep serviceInvoiceItemsRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderRep) {

    this.serviceInvoiceRep = serviceInvoiceRep;
    this.serviceInvoiceItemsRep = serviceInvoiceItemsRep;
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public DObLandedCostUpdateActualCostValueObject create(String serviceInvoiceCode) {

    DObVendorInvoiceGeneralModel serviceInvoice =
        serviceInvoiceRep.findOneByUserCode(serviceInvoiceCode);

    List<IObVendorInvoiceItemsGeneralModel> serviceInvoiceItems =
        serviceInvoiceItemsRep.findByRefInstanceId(serviceInvoice.getId());

    DObLandedCostUpdateActualCostValueObject valueObject =
        new DObLandedCostUpdateActualCostValueObject();

    valueObject.setPurchaseOrderCode(getServicePOCode(serviceInvoice.getPurchaseOrderCode()));
    valueObject.setRefDocumentId(serviceInvoice.getId());

    serviceInvoiceItems.forEach(
        item -> valueObject.addItem(item.getItemCode(), item.getTotalAmount()));

    return valueObject;
  }

  private String getServicePOCode(String purhcaseOrderCode) {

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderRep.findOneByUserCode(purhcaseOrderCode);
    return purchaseOrder.getRefDocumentCode();
  }

}
