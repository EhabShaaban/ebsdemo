package com.ebs.dda.accounting.landedcost.validation;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.landedcost.validation.api.IIObLandedCostConvertToActualMandatoryAttributes;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.THERE_IS_NO_ACTIVATED_GOODS_INVOICE_IN_ACTIVATE_STATE;
import static com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostGeneralModel.GOODS_INVOICE_CODE;

public class DObLandedCostConvertToActualValidator {

    private ValidationResult validationResult;
    private MandatoryValidatorUtils validatorUtils;

    private DObLandedCostGeneralModelRep landedCostGeneralModelMRep;
    private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
    private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;

    public DObLandedCostConvertToActualValidator() {
        this.validatorUtils = new MandatoryValidatorUtils();
    }

    public ValidationResult validate(String landedCostCode) throws Exception {
        validationResult = new ValidationResult();

        DObLandedCostGeneralModel landedCost = landedCostGeneralModelMRep.findOneByUserCode(landedCostCode);
        validateLandedCost(landedCost);
        return validationResult;
    }

    private void validateLandedCost(DObLandedCostGeneralModel landedCost) throws Exception {
        checkThatLandedCostIsExist(landedCost);
        checkThatLandedCostInEstimatedValueCompletedState(landedCost);
        checkThatLandedCostGoodsInvoiceIsActivated(landedCost.getUserCode());
    }

    private void checkThatLandedCostGoodsInvoiceIsActivated(String userCode) throws Exception {
        IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel = landedCostDetailsGeneralModelRep.findOneByLandedCostCode(userCode);
        if (landedCostDetailsGeneralModel.getVendorInvoiceCode() == null) {
            validationResult.bindError(GOODS_INVOICE_CODE, THERE_IS_NO_ACTIVATED_GOODS_INVOICE_IN_ACTIVATE_STATE);
        } else {
            DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel = vendorInvoiceGeneralModelRep.findOneByUserCode(landedCostDetailsGeneralModel.getVendorInvoiceCode());
            Set<String> states = vendorInvoiceGeneralModel.getCurrentStates();
            if (!states.contains(DObVendorInvoiceStateMachine.POSTED_STATE) || landedCostDetailsGeneralModel.getVendorInvoiceCode() == null) {
                validationResult.bindError(GOODS_INVOICE_CODE, THERE_IS_NO_ACTIVATED_GOODS_INVOICE_IN_ACTIVATE_STATE);
            }
        }
    }

    private void checkThatLandedCostIsExist(DObLandedCostGeneralModel landedCost) throws Exception {
        if (landedCost == null) {
            throw new InstanceNotExistException();
        }
    }


    private void checkThatLandedCostInEstimatedValueCompletedState(DObLandedCostGeneralModel landedCost)
            throws Exception {

        Set<String> states = landedCost.getCurrentStates();

        if (!states.contains(DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED)) {
            throw new ActionNotAllowedPerStateException(DObLandedCost.class, landedCost.getId(),
                    landedCost.getCurrentState(), IDObLandedCostActionNames.CONVERT_TO_ACTUAL);
        }

    }

    public List<String> validateGeneralModelMandatories(String landedCostCode, String state)
            throws Exception {
        IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
                landedCostDetailsGeneralModelRep.findOneByLandedCostCode(landedCostCode);
        String[] mandatoryAttributes =
                validatorUtils.getMandatoryAttributesByState(
                        IIObLandedCostConvertToActualMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL, state);
        return validatorUtils.validateMandatoriesForGeneralModel(
                mandatoryAttributes, landedCostDetailsGeneralModel);
    }

    public Map<String, Object> validateAllSectionsForState(String landedCostCode, String state)
            throws Exception {
        Map<String, Object> sectionsMissingFields = new HashMap<>();
        List<String> detailsMissingFields = validateGeneralModelMandatories(landedCostCode, state);
        if (!detailsMissingFields.isEmpty()) {
            sectionsMissingFields.put(IDObLandedCostSectionNames.LANDED_COST_SECTION, detailsMissingFields);
        }
        return sectionsMissingFields;
    }

    public void setLandedCostGeneralModelMRep(DObLandedCostGeneralModelRep landedCostGeneralModelMRep) {
        this.landedCostGeneralModelMRep = landedCostGeneralModelMRep;
    }

    public void setLandedCostDetailsGeneralModelRep(IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep) {
        this.landedCostDetailsGeneralModelRep = landedCostDetailsGeneralModelRep;
    }

    public void setVendorInvoiceGeneralModelRep(DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
        this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
    }


}
