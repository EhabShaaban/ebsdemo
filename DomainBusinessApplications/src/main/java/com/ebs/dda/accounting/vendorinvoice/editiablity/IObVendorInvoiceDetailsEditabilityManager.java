package com.ebs.dda.accounting.vendorinvoice.editiablity;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoiceDetailsGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IObVendorInvoiceDetailsEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(
            IIObVendorInvoiceDetailsGeneralModel.PAYMENT_TERM,
            IIObVendorInvoiceDetailsGeneralModel.INVOICE_NUMBER,
            IIObVendorInvoiceDetailsGeneralModel.INVOICE_DATE,
            IIObVendorInvoiceDetailsGeneralModel.CURRENCY,
            IIObVendorInvoiceDetailsGeneralModel.DOWN_PAYMENT
          ));

    enabledAttributesPerState.put(DObVendorInvoiceStateMachine.DRAFT_STATE, draftEnabledAttributes);

    return enabledAttributesPerState;
  }
}
