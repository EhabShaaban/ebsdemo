package com.ebs.dda.accounting.salesinvoice.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.IIObSalesInvoiceBusinessPartnerGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SalesInvoiceBusinessPartnerEditabilityManger
    extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState =
        new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(IIObSalesInvoiceBusinessPartnerGeneralModel.PAYMENT_TERM_CODE));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObSalesInvoiceBusinessPartnerGeneralModel.CURRENCY));
    enabledAttributesPerState.put(
        DObSalesInvoiceWithoutReferenceStateMachine.DRAFT_STATE, draftEnabledAttributes);

    return enabledAttributesPerState;
  }

}
