package com.ebs.dda.accounting.collection.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;

import java.util.*;

public class IObCollectionDetailsEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerStateAndType = new HashMap<String, Set<String>>();

    Set<String> cashEnabledAttributes = new HashSet<>();
    cashEnabledAttributes.addAll(Arrays.asList(IIObCollectionDetailsGeneralModel.TREASURY_CODE));
    cashEnabledAttributes.addAll(Arrays.asList(IIObCollectionDetailsGeneralModel.AMOUNT));

    Set<String> bankEnabledAttributes = new HashSet<>();
    bankEnabledAttributes.addAll(
        Arrays.asList(IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_NUMBER));
    bankEnabledAttributes.addAll(Arrays.asList(IIObCollectionDetailsGeneralModel.AMOUNT));

    enabledAttributesPerStateAndType.put(
        CollectionMethodEnum.CollectionMethod.CASH.toString(), cashEnabledAttributes);
    enabledAttributesPerStateAndType.put(
        CollectionMethodEnum.CollectionMethod.BANK.toString(), bankEnabledAttributes);
    return enabledAttributesPerStateAndType;
  }
}
