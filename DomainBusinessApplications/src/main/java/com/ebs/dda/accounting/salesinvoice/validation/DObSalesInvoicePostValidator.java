package com.ebs.dda.accounting.salesinvoice.validation;

import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IIObSalesInvoiceDetailsMandatoryAttributes;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames;
import com.ebs.dda.accounting.settlement.validation.api.IIObSettlementDetailsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesInvoicePostValidator {

    private ValidationResult validationResult;
    private IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep;
    private MandatoryValidatorUtils validatorUtils;


    public DObSalesInvoicePostValidator() {
        validatorUtils = new MandatoryValidatorUtils();

    }

    public ValidationResult validate(
            DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel) throws Exception {
        validationResult = new ValidationResult();
        if (salesInvoiceGeneralModel
                .getInvoiceTypeCode()
                .equals(DObSalesInvoiceFactory.ORDER_BASED_TYPE)) {
            checkSalesOrderState(salesInvoiceGeneralModel);
        }
        return validationResult;
    }

    public Map<String, List<String>> validateAllSectionsForState(String salesInvoiceCode)
            throws Exception {
        Map<String, List<String>> sectionsMissingFields = new HashMap<>();
        List<String> salesInvoiceDetailsMissingFields =
                validateSalesInvoiceDetailsMandatories(salesInvoiceCode);
        if (!salesInvoiceDetailsMissingFields.isEmpty()) {
            sectionsMissingFields.put(
                    IDObSalesInvoiceSectionNames.SALES_INVOICE_DETAILS, salesInvoiceDetailsMissingFields);
        }
        return sectionsMissingFields;
    }

    public List<String> validateSalesInvoiceDetailsMandatories(String salesInvoiceCode) throws Exception {
        IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceDetails =
                salesInvoiceBusinessPartnerGeneralModelRep.findOneByCode(salesInvoiceCode);
        String[] mandatoryAttributes =
                validatorUtils.getMandatoryAttributesByState(
                        IIObSalesInvoiceDetailsMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL,
                        DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE);
        return validatorUtils.validateMandatoriesForGeneralModel(
                mandatoryAttributes, salesInvoiceDetails);
    }

    private void checkSalesOrderState(
            DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel) throws Exception {
        DObSalesOrderGeneralModel salesOrder = new DObSalesOrderGeneralModel();
        salesOrder.setCurrentStates(salesInvoiceGeneralModel.getSalesOrderCurrentStates());
        if (salesOrder != null) {
            checkIfTransitionIsAllowedBasedOnSalesOrderStateInStateMachine(salesOrder);
        }
    }

    private void checkIfTransitionIsAllowedBasedOnSalesOrderStateInStateMachine(
            DObSalesOrderGeneralModel salesOrder) throws Exception {
        DObSalesOrderStateMachine salesOrderStateMachine = new DObSalesOrderStateMachine();
        salesOrderStateMachine.initObjectState(salesOrder);
        if (!salesOrderStateMachine.canFireEvent(IDObSalesOrderActionNames.POST_INVOICE)) {
            throw new ObjectStateNotValidException();
        }
    }

    public void setSalesInvoiceBusinessPartnerGeneralModelRep(IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep) {
        this.salesInvoiceBusinessPartnerGeneralModelRep = salesInvoiceBusinessPartnerGeneralModelRep;
    }
}
