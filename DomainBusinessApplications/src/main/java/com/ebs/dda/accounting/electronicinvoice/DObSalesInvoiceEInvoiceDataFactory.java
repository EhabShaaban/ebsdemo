package com.ebs.dda.accounting.electronicinvoice;

import com.ebs.dda.accounting.electronicinvoice.types.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModelRep;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DObSalesInvoiceEInvoiceDataFactory implements EInvoiceDataFactory {

    @Setter
    private DObSalesInvoiceEInvoiceDataGeneralModelRep eInvoiceDataGMRep;
    private DObSalesInvoiceEInvoiceDataGeneralModel eInvoiceDataGM;

    @Setter
    private DObSalesInvoiceItemsEInvoiceDataGeneralModelRep invoiceItemsEInvoiceDataGMRep;
    private List<DObSalesInvoiceItemsEInvoiceDataGeneralModel> invoiceItemsEInvoiceDataGM;

    @Setter
    private IObSalesInvoiceTaxesGeneralModelRep invoiceTaxesGMRep;
    private List<IObSalesInvoiceTaxesGeneralModel> invoiceTaxesGM;

    private void init(String userCode) {
        eInvoiceDataGM = eInvoiceDataGMRep.findOneByUserCode(userCode);
        invoiceItemsEInvoiceDataGM = invoiceItemsEInvoiceDataGMRep.findByRefInstanceId(eInvoiceDataGM.getId());
        invoiceTaxesGM = invoiceTaxesGMRep.findByRefInstanceId(eInvoiceDataGM.getId());
    }
    public EInvoiceData createInvoiceData(String userCode) {
        init(userCode);

        BigDecimal totalItemsSalesAmount = calculateTotalSalesAmount();
        BigDecimal totalItemsDiscountAmount = calculateTotalDiscountAmount();

        BigDecimal netAmount = totalItemsSalesAmount.subtract(totalItemsDiscountAmount);
        BigDecimal totalTaxAmount = calculateTotalTaxAmount();

        return DObSalesInvoiceEInvoiceData
            .builder()
            .issuer(createIssuer())
            .receiver(createReceiver())
            .documentType(EInvoiceAPIProperities.DOC_TYPE_ISSUER)
            .documentTypeVersion(EInvoiceAPIProperities.API_CURRUNT_VERSION)
            .dateTimeIssued(eInvoiceDataGM.getJournalDate().toString())
            .taxpayerActivityCode(eInvoiceDataGM.getTaxpayerActivityCode())
            .internalId(eInvoiceDataGM.getInvoiceSerialNumber())
            .invoiceLines(createInvoiceLines())
            .totalSalesAmount(totalItemsSalesAmount)
            .totalDiscountAmount(totalItemsDiscountAmount)
            .netAmount(netAmount)
            .taxTotals(createTaxTotals())
            .extraDiscountAmount(eInvoiceDataGM.getExtraDiscountAmount())
            .totalItemsDiscountAmount(totalItemsDiscountAmount.add(eInvoiceDataGM.getExtraDiscountAmount()))
            .totalAmount(netAmount.add(netAmount.multiply(totalTaxAmount)).setScale(5, RoundingMode.CEILING))
            .signatures(createSignatures())
            .build();
    }

    private EInvoicePartner createIssuer() {
        return EInvoicePartner
            .builder()
            .id(eInvoiceDataGM.getCompanyTaxRegistrationNumber())
            .name(eInvoiceDataGM.getCompanyName())
            .type(EInvoiceAPIProperities.BUSINESS_IN_EGYPT)
            .address(createIssuerAddress())
            .build();
    }

    private EInvoicePartnerAddress createIssuerAddress() {
        return EInvoicePartnerAddress
            .builder()
            .branchId("")// TODO: must be set
            .country(eInvoiceDataGM.getCompanyCountry())
            .governate(eInvoiceDataGM.getCompanyGovernate())
            .regionCity(eInvoiceDataGM.getCompanyRegion())
            .street(eInvoiceDataGM.getCompanyStreet())
            .buildingNumber(eInvoiceDataGM.getCompanyBuildingNumber())
            .postalCode(eInvoiceDataGM.getCompanyPostalCode())
            .build();
    }

    private EInvoicePartner createReceiver() {
        return EInvoicePartner
            .builder()
            .id(eInvoiceDataGM.getCustomerTaxRegistrationNumber())
            .name(eInvoiceDataGM.getCustomerName())
            .type(eInvoiceDataGM.getCustomerType())
            .address(createReceiverAddress())
            .build();
    }

    private EInvoicePartnerAddress createReceiverAddress() {
        return EInvoicePartnerAddress
            .builder()
            .country(eInvoiceDataGM .getCustomerCountry())
            .governate(eInvoiceDataGM.getCustomerGovernate())
            .regionCity(eInvoiceDataGM.getCustomerRegion())
            .street(eInvoiceDataGM.getCustomerStreet())
            .buildingNumber(eInvoiceDataGM.getCustomerBuildingNumber())
            .postalCode(eInvoiceDataGM.getCustomerPostalCode())
            .build();
    }

    private List<EInvoiceLine> createInvoiceLines() {
        return invoiceItemsEInvoiceDataGM
            .stream()
            .map(this::createInvoiceLine)
            .collect(Collectors.toList());
    }

    private EInvoiceLine createInvoiceLine(DObSalesInvoiceItemsEInvoiceDataGeneralModel item) {

        BigDecimal nonTaxableDiscount = item.getItemsDiscount();
        BigDecimal taxableDiscount = BigDecimal.ZERO;
        BigDecimal totalTaxes = BigDecimal.ZERO;

        BigDecimal multipleOfPriceAndQuantity = item.getSalesTotal();
        BigDecimal priceAfterApplyingTaxableDiscount = multipleOfPriceAndQuantity.subtract(taxableDiscount);
        BigDecimal totalAfterRemovingTaxableDiscountAddingTaxes = priceAfterApplyingTaxableDiscount.add(totalTaxes);
        BigDecimal totalAfterRemovingNonTaxableDiscount = totalAfterRemovingTaxableDiscountAddingTaxes.subtract(nonTaxableDiscount);

        return EInvoiceLine
            .builder()
            .internalCode(item.getItemUserCode())
            .description(item.getItemName())
            .itemType(EInvoiceAPIProperities.LINE_TYPE)
            .itemCode(item.getItemBrickCode())
            .unitType(item.getItemBaseUnit())

            .quantity(item.getQuantity())
            .unitValue(createItemUnitValue(item))
            .salesTotal(multipleOfPriceAndQuantity)

            .itemsDiscount(item.getItemsDiscount())
            .totalTaxableFees(totalTaxes)

            .netTotal(priceAfterApplyingTaxableDiscount)
            .total(totalAfterRemovingNonTaxableDiscount)

            .valueDifference(BigDecimal.ZERO)
            .build();
    }

    private EInvoiceValue createItemUnitValue(DObSalesInvoiceItemsEInvoiceDataGeneralModel item) {
        return EInvoiceValue
            .builder()
            .currencySold(item.getCurrencySoldIso())
            .amountEGP(item.getAmountSold().multiply(item.getCurrencyExchangeRate()))
            .amountSold(item.getAmountSold())
            .currencyExchangeRate(item.getCurrencyExchangeRate())
            .build();
    }

    private List<EInvoiceTax> createTaxTotals() {
        return invoiceTaxesGM
            .stream()
            .map(this::createEInvoiceTax)
            .collect(Collectors.toList());
    }

    private EInvoiceTax createEInvoiceTax(IObSalesInvoiceTaxesGeneralModel tax) {
        return EInvoiceTax
            .builder()
            .taxType(tax.getTaxCode())
            .amount(tax.getTaxAmount())
            .build();
    }

    private BigDecimal calculateTotalSalesAmount() {
        return invoiceItemsEInvoiceDataGM
            .stream()
            .map(DObSalesInvoiceItemsEInvoiceDataGeneralModel::getSalesTotal)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateTotalDiscountAmount() {
        return invoiceItemsEInvoiceDataGM
            .stream()
            .map(DObSalesInvoiceItemsEInvoiceDataGeneralModel::getItemsDiscount)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateTotalTaxAmount() {
        return invoiceTaxesGM
            .stream()
            .map(IObSalesInvoiceTaxesGeneralModel::getTaxAmount)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private List<EInvoiceSignature> createSignatures() {
        return new ArrayList<>();//TODO: to be implemented
    }

}
