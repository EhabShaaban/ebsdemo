package com.ebs.dda.accounting.landedcost.apis;

import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface ILandedCostFactorItemsMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {
    IIObLandedCostFactorItemsGeneralModel.ITEM_CODE,
    IIObLandedCostFactorItemsGeneralModel.ESTIMATED_VALUE

  };

  String[] POSTED_STATE_MANDATORIES = {};

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObLandedCostStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .build();
}
