package com.ebs.dda.accounting.landedcost.apis;

public interface IDObLandedCostActionNames {

  String READ_LANDED_COST_DETAILS = "ReadLandedCostDetails";
  String READ_COMPANY_DATA = "ReadCompanyData";
  String UPDATE_COST_FACTOR_ITEMS = "UpdateCostFactorItem";
  String CONVERT_TO_ESTIMATE = "ConvertToEstimate";
  String CONVERT_TO_ACTUAL = "ConvertToActual";
}
