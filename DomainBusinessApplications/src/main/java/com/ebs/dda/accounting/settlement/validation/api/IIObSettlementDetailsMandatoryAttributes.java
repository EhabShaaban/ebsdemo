package com.ebs.dda.accounting.settlement.validation.api;

import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementDetailsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IIObSettlementDetailsMandatoryAttributes {

  String[] POSTED_STATE_MANDATORIES = {IIObSettlementDetailsGeneralModel.CURRENCY_ISO};
  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(DObSettlementStateMachine.POSTED_STATE, POSTED_STATE_MANDATORIES)
          .build();
}
