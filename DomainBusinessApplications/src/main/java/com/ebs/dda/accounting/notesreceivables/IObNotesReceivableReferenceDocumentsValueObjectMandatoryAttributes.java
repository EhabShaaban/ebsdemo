package com.ebs.dda.accounting.notesreceivables;

import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesReferenceDocumentsValueObject;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IObNotesReceivableReferenceDocumentsValueObjectMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {
    IIObMonetaryNotesReferenceDocumentsValueObject.DOCUMENT_TYPE,
    IIObMonetaryNotesReferenceDocumentsValueObject.REF_DOCUMENT_CODE,
    IIObMonetaryNotesReferenceDocumentsValueObject.AMOUNT_TO_COLLECT
  };
  String[] ACTIVE_STATE_MANDATORIES = {};

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObNotesReceivablesStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObNotesReceivablesStateMachine.ACTIVE_STATE, ACTIVE_STATE_MANDATORIES)
          .build();
}
