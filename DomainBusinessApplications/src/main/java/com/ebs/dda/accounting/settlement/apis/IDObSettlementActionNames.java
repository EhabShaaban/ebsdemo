package com.ebs.dda.accounting.settlement.apis;

public interface IDObSettlementActionNames {

  String READ_SETTLEMENT_DETAILS = "ReadSettlementDetails";
  String READ_SETTLEMENT_ACCOUNTING_DETAILS = "ReadAccountingDetails";
  String READ_SETTLEMENT_COMPANY_DATA = "ReadCompanyData";
  String READ_SETTLEMENT_ACTIVATION_DETAILS = "ReadActivationDetails";
  String READ_SETTLEMENT_SUMMARY = "ReadSummary";
  String UPDATE_SETTLEMENT_ACCOUNTING_DETAILS = "UpdateAccountingDetails";
  String POST = "Post";
}
