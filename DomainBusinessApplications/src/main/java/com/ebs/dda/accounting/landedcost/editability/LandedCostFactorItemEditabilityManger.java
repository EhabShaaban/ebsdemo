package com.ebs.dda.accounting.landedcost.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel;

import java.util.*;

public class LandedCostFactorItemEditabilityManger
    extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState =
        new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(IIObLandedCostFactorItemsGeneralModel.ITEM_CODE));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObLandedCostFactorItemsGeneralModel.ESTIMATED_VALUE));
    enabledAttributesPerState.put(
        DObLandedCostStateMachine.DRAFT_STATE, draftEnabledAttributes);

    return enabledAttributesPerState;
  }

}
