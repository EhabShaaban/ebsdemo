package com.ebs.dda.accounting.electronicinvoice;

import com.ebs.dda.accounting.electronicinvoice.types.*;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.List;

@Builder
public class DObSalesInvoiceEInvoiceData implements EInvoiceData {

    private EInvoicePartner issuer;
    private EInvoicePartner receiver;
    private String documentType;
    private String documentTypeVersion;
    private String dateTimeIssued;
    private String taxpayerActivityCode;
    private String internalId;
    private String purchaseOrderReference;
    private String purchaseOrderDescription;
    private String salesOrderReference;
    private String salesOrderDescription;
    private String proformaInvoiceNumber;
    private EInvoicePayment payment;
    private EInvoiceDelivery delivery;
    private List<EInvoiceLine> invoiceLines;
    private BigDecimal totalSalesAmount;
    private BigDecimal totalDiscountAmount;
    private BigDecimal netAmount;
    private BigDecimal extraDiscountAmount;
    private BigDecimal totalItemsDiscountAmount;
    private BigDecimal totalAmount;
    private List<EInvoiceTax> taxTotals;
    private List<EInvoiceSignature> signatures;

}
