package com.ebs.dda.accounting.paymentrequest.apis;

public interface IDObPaymentRequestSectionNames {

  String COMPANY_DATA_SECTION = "CompanyData";
  String PAYMENT_DETAILS_SECTION = "PaymentDetails";
  String ACCOUNTING_DETAILS_SECTION = "AccountingDetails";
}
