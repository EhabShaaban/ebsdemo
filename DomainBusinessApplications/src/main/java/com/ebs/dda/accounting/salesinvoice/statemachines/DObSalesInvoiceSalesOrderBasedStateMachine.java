package com.ebs.dda.accounting.salesinvoice.statemachines;

public class DObSalesInvoiceSalesOrderBasedStateMachine extends DObSalesInvoiceStateMachine {

	public DObSalesInvoiceSalesOrderBasedStateMachine() throws Exception {
		super("META-INF/DObOrderBasedSalesInvoice.scxml");
	}

	@Override
	public boolean goToDraft() {
		return true;
	}

	@Override
	public boolean goToActive() {
		return true;
	}

	@Override
	public boolean goToClosed() {
		return true;
	}

	@Override
	public boolean goToOpened() {
		return true;
	}

	@Override
	public boolean goToOperativeStates() {
		return true;
	}

	@Override
	public boolean goToParallelOperativeStates() {
		return true;
	}

	@Override
	public boolean goToDeliveringStates() {
		return true;
	}

	@Override
	public boolean goToParallelClosed() {
		return true;
	}
}
