package com.ebs.dda.accounting.paymentrequest.validation.apis;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IPaymentRequestPaymentDetailsMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {};

  String[] PAYMENT_DONE_STATE_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE,
    IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };
  String[] GENERAL_TYPE_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE,
    IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };

  String[] PARTY_TYPE_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };

  String[] UNDER_SETTLEMENT_TYPE_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };

  String[] BASED_ON_CREDIT_NOTE_TYPE_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };

  String[] BANK_FORM_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE,
    IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_CODE
  };

  String[] CASH_FORM_MANDATORIES = {
    IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT,
    IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE,
    IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE,
    IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObPaymentRequestStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObPaymentRequestStateMachine.PAYMENT_DONE_STATE, PAYMENT_DONE_STATE_MANDATORIES)
          .put(
              PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.toString(),
              GENERAL_TYPE_MANDATORIES)
          .put(PaymentTypeEnum.PaymentType.VENDOR.toString(), PARTY_TYPE_MANDATORIES)
          .put(
              PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT.toString(),
              UNDER_SETTLEMENT_TYPE_MANDATORIES)
          .put(
              PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.toString(),
              BASED_ON_CREDIT_NOTE_TYPE_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.BANK.toString(), BANK_FORM_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.CASH.toString(), CASH_FORM_MANDATORIES)
          .build();
}
