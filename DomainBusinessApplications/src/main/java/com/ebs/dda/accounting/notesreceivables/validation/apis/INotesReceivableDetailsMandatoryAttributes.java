package com.ebs.dda.accounting.notesreceivables.validation.apis;

import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface INotesReceivableDetailsMandatoryAttributes {

    String[] DRAFT_STATE_MANDATORIES = {
            IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_CODE,
            IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_NAME,
            IDObNotesReceivablesGeneralModel.REF_DOCUMENT_TYPE,
            IDObNotesReceivablesGeneralModel.REF_DOCUMENT_CODE,
            IDObNotesReceivablesGeneralModel.AMOUNT,
            IDObNotesReceivablesGeneralModel.CURRENCY_ISO,
            IDObNotesReceivablesGeneralModel.DUE_DATE,
            IDObNotesReceivablesGeneralModel.NOTES_INFORMATION,
            IDObNotesReceivablesGeneralModel.ISSUING_BANK
    };

    String[] ACTIVE_STATE_MANDATORIES = {
            IDObNotesReceivablesGeneralModel.NOTE_FORM,
            IDObNotesReceivablesGeneralModel.CURRENCY_ISO,
            IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_CODE,
            IDObNotesReceivablesGeneralModel.AMOUNT,
            IDObNotesReceivablesGeneralModel.NOTE_NUMBER,
            IDObNotesReceivablesGeneralModel.REMAINING,
            IDObNotesReceivablesGeneralModel.NOTE_BANK,
            IDObNotesReceivablesGeneralModel.DUE_DATE,
            IDObNotesReceivablesGeneralModel.DEPOT_CODE
    };

    Map<String, String[]> MANDATORIES =
            ImmutableMap.<String, String[]>builder()
                    .put(DObNotesReceivablesStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
                    .put(DObNotesReceivablesStateMachine.ACTIVE_STATE, ACTIVE_STATE_MANDATORIES)
                    .build();

}
