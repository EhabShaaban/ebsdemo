package com.ebs.dda.accounting.salesinvoice.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SalesInvoiceItemEditabilityManger
    extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState =
        new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(IIObInvoiceItemsGeneralModel.ITEM_CODE));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObInvoiceItemsGeneralModel.ITEM_PRICE));
    enabledAttributesPerState.put(
        DObSalesInvoiceWithoutReferenceStateMachine.DRAFT_STATE, draftEnabledAttributes);

    return enabledAttributesPerState;
  }

}
