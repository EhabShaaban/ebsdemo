package com.ebs.dda.accounting.notesreceivables;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;

import java.util.*;

public class IObNotesReceivableDetailsEditabilityManager extends AbstractEditabilityManager {
  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IDObNotesReceivablesGeneralModel.NOTE_NUMBER,
                IDObNotesReceivablesGeneralModel.NOTE_BANK,
                IDObNotesReceivablesGeneralModel.DEPOT_CODE,
                IDObNotesReceivablesGeneralModel.DUE_DATE));

    Set<String> activeEnabledAttributes =
        new HashSet<>(Arrays.asList(IDObNotesReceivablesGeneralModel.DEPOT_CODE));

    enabledAttributesPerState.put(
        DObNotesReceivablesStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(
        DObNotesReceivablesStateMachine.ACTIVE_STATE, activeEnabledAttributes);

    return enabledAttributesPerState;
  }
}
