package com.ebs.dda.accounting.salesinvoice.editiablity;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;

import java.util.*;

public class DObSalesInvoiceEditCompanyDataEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(Arrays.asList(IDObSalesInvoiceGeneralModel.COMPANY_TAXES));

    enabledAttributesPerState.put(DObSalesInvoiceWithoutReferenceStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState
            .put(DObSalesInvoiceWithoutReferenceStateMachine.READY_FOR_DELIVERING_STATE, new HashSet<>());
    enabledAttributesPerState
            .put(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE, new HashSet<>());

    return enabledAttributesPerState;
  }
}
