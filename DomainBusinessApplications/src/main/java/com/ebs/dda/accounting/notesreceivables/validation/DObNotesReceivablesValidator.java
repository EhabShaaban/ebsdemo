package com.ebs.dda.accounting.notesreceivables.validation;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesSectionNames;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObNotesReceivablesValidator {

  private static final String AMOUNT = "Amount";
  private ValidationResult validationResult;
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private DObNotesReceivableDetailsMandatoryValidator notesReceivableDetailsMandatoryValidator;
  private DObNotesReceivableGeneralDataMandatoryValidator
      notesReceivableGeneralDataMandatoryValidator;
  private DObSalesInvoiceGeneralModelRep dObSalesInvoiceGeneralModelRep;

  public DObNotesReceivablesValidator() {

    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public ValidationResult validate(DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {

//    validationResult = new ValidationResult();
//    String refDocumentCode = notesReceivablesGeneralModel.getRefDocumentCode();
//    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
//        dObSalesInvoiceGeneralModelRep.findOneByUserCode(refDocumentCode);
//    double salesInvoiceRemaining = dObSalesInvoiceGeneralModel.getRemaining();
//    double notesReceivableAmount = Double.parseDouble(notesReceivablesGeneralModel.getAmount());

//    if (salesInvoiceRemaining < notesReceivableAmount) {
//      validationResult.bindError(AMOUNT, IExceptionsCodes.NR_AMOUNT_GREATER_THAN_SI_REMAINING);
//    }
    return validationResult;
  }

  public Map<String, List<String>> validateAllSectionsForState(String nrCode, String state)
      throws Exception {
    Map<String, List<String>> sectionsMissingFields = new HashMap<>();

    mandatoryValidatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IDObNotesReceivablesSectionNames.NOTES_RECEIVABELS_DETAILS_SECTION,
        validateNotesReceivableDetailsSection(nrCode, state));

    mandatoryValidatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IDObNotesReceivablesSectionNames.GENERAL_DATA_SECTION,
        validateNotesReceivableGenaralDataSection(nrCode, state));

    return sectionsMissingFields;
  }

  private List<String> validateNotesReceivableDetailsSection(
      String paymentRequestCode, String state) throws Exception {
    return notesReceivableDetailsMandatoryValidator.validateGeneralModelMandatories(
        paymentRequestCode, state);
  }

  private List<String> validateNotesReceivableGenaralDataSection(
      String paymentRequestCode, String state) throws Exception {
    return notesReceivableGeneralDataMandatoryValidator.validateGeneralModelMandatories(
        paymentRequestCode, state);
  }

  public void setNotesReceivableDetailsMandatoryValidator(
      DObNotesReceivableDetailsMandatoryValidator notesReceivableDetailsMandatoryValidator) {
    this.notesReceivableDetailsMandatoryValidator = notesReceivableDetailsMandatoryValidator;
  }

  public void setNotesReceivableGeneralDataMandatoryValidator(
      DObNotesReceivableGeneralDataMandatoryValidator
          notesReceivableGeneralDataMandatoryValidator) {
    this.notesReceivableGeneralDataMandatoryValidator =
        notesReceivableGeneralDataMandatoryValidator;
  }

  public void setdObSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep dObSalesInvoiceGeneralModelRep) {
    this.dObSalesInvoiceGeneralModelRep = dObSalesInvoiceGeneralModelRep;
  }
}
