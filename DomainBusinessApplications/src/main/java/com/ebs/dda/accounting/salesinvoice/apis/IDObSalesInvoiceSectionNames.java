package com.ebs.dda.accounting.salesinvoice.apis;

public interface IDObSalesInvoiceSectionNames {
    String COMPANY_DATA_SECTION = "CompanyData";
    String BUSINESS_PARTNER_SECTION = "BusinessPartner";
    String ITEM_SECTION = "InvoiceItems";
    String TAXES_SECTION = "InvoiceTaxes";
    String SUMMARY_SECTION = "InvoiceSummary";
    String SALES_INVOICE_DETAILS = "SalesInvoiceDetails";
}