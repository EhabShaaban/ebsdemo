package com.ebs.dda.accounting.settlement.apis;

import static com.ebs.dac.foundation.realization.statemachine.BasicStateMachine.DRAFT_STATE;
import static com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY;
import static com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE;
import static com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel.AMOUNT;
import static com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_CODE;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.google.common.collect.ImmutableMap;

public interface IIObSettlementAccountingDetailsAttributesConfig {

  final String[] DRAFT_MANDATORIES = {ACCOUNTING_ENTRY, ACCOUNT_CODE, SUB_ACCOUNT_CODE, AMOUNT};
  final String[] POSTED_MANDATORIES = {};

  final Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder().put(DRAFT_STATE, DRAFT_MANDATORIES).build();


  final Set<String> DRAFT_ENABLED_ATTRIBUTES =
      new HashSet<>(Arrays.asList(ACCOUNTING_ENTRY, ACCOUNT_CODE, SUB_ACCOUNT_CODE, AMOUNT));

  final Map<String, Set<String>> EDITABLE_ATTRIBUTES = ImmutableMap.<String, Set<String>>builder()
      .put(DRAFT_STATE, DRAFT_ENABLED_ATTRIBUTES).build();

}
