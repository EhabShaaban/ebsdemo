package com.ebs.dda.accounting.collection.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class CollectionAmountGreaterThanSalesOrderRemainingException extends BusinessRuleViolationException {

  private static final long serialVersionUID = 1L;

  public CollectionAmountGreaterThanSalesOrderRemainingException(ValidationResult validationResult) {
    super(validationResult);
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.C_AMOUNT_GREATER_THAN_SALES_ORDER_REMAINING;
  }

}
