package com.ebs.dda.accounting.notesreceivables;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;

import java.util.*;

public class IObNotesReceivableReferenceDocumentsEditabilityManager
    extends AbstractEditabilityManager {
  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObNotesReceivablesReferenceDocumentGeneralModel.DOCUMENT_TYPE,
                IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE,
                IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT));

    enabledAttributesPerState.put(
        DObNotesReceivablesStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(
        DObNotesReceivablesStateMachine.ACTIVE_STATE, new HashSet<>());

    return enabledAttributesPerState;
  }
}
