package com.ebs.dda.accounting.electronicinvoice.types;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class EInvoiceValue {
    private String currencySold;
    private BigDecimal amountEGP;
    private BigDecimal amountSold;
    private BigDecimal currencyExchangeRate;
}
