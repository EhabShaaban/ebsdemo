package com.ebs.dda.accounting.salesinvoice.apis;

public interface IDObSalesInvoiceActionNames {

  String READ_GENERAL_DATA = "ReadGeneralData";
  String READ_COMPANY = "ReadCompany";
  String READ_TAXES = "ReadTaxes";
  String READ_SUMMARY = "ReadSummary";
  String READ_POSTING_DETAILS = "ReadPostingDetails";
  String UPDATE_COMPANY = "UpdateCompany";
  String READ_BUSINESS_PARTNER = "ReadBusinessPartner";
  String EDIT_BUSINESS_PARTNER = "UpdateBusinessPartner";
  String READ_ITEMS_SECTION = "ReadItems";
  String UPDATE_item = "UpdateItems";
  String UPDATE_TAXES = "UpdateTaxes";
  String DELIVER_TO_CUSTOMER = "Activate";
  String MARK_READY_FOR_DELIVERING = "MarkAsReadyForDelivering";
  String CLOSE = "Close";
  String SUBMIT_EINVOICE = "SubmitEInvoice";

}
