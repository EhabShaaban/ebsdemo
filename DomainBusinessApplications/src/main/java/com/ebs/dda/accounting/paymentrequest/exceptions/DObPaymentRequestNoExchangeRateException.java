package com.ebs.dda.accounting.paymentrequest.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class DObPaymentRequestNoExchangeRateException extends BusinessRuleViolationException {
    public DObPaymentRequestNoExchangeRateException(ValidationResult validationResult) {
        super(validationResult);
    }

    @Override
    public String getResponseCode() {

        return IExceptionsCodes.NO_LATEST_EXCHANGE_RATE;
    }
}
