package com.ebs.dda.accounting.salesinvoice.apis;

import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.IIObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementDetailsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IIObSalesInvoiceDetailsMandatoryAttributes {

  String[] POSTED_STATE_MANDATORIES = {IIObSalesInvoiceBusinessPartnerGeneralModel.PAYMENT_TERM_CODE
          ,IIObSalesInvoiceBusinessPartnerGeneralModel.CURRENCY
  };
  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE, POSTED_STATE_MANDATORIES)
          .build();
}
