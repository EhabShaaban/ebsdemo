package com.ebs.dda.accounting.electronicinvoice.types;

public class EInvoicePayment {
    private String bankName;
    private String bankAddress;
    private String bankAccountNo;
    private String bankAccountIBAN;
    private String swiftCode;
    private String terms;
}
