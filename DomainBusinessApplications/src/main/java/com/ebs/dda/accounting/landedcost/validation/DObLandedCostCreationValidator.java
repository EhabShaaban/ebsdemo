package com.ebs.dda.accounting.landedcost.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCostType;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCost;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;

import java.util.Set;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.PURCHASE_ORDER_HAS_POSTED_LANDED_COST;
import static com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostCreationValueObject.PURCHASE_ORDER_CODE;
import static com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine.CANCELLED_STATE;
import static com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine.DRAFT_STATE;

public class DObLandedCostCreationValidator {

    private ValidationResult validationResult;

    private IObOrderCompanyGeneralModelRep purhcaseOrderGMRep;
    private DObLandedCostGeneralModelRep landedCostGMRep;
    private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

    public void checkMandatories(DObLandedCostCreationValueObject valueObject) throws Exception {
        checkStringFieldIsNotNull(valueObject.getLandedCostType());
        checkStringFieldIsNotNull(valueObject.getPurchaseOrderCode());
        checkStringFieldIsNotNull(valueObject.getDocumentOwnerId());
    }

    public ValidationResult validateBusinessRules(DObLandedCostCreationValueObject valueObject)
            throws Exception {
        validationResult = new ValidationResult();
        validateDocumentOwner(valueObject);
        validatePurchaseOrder(valueObject);
        return validationResult;
    }

    private void validateDocumentOwner(DObLandedCostCreationValueObject valueObject) throws ArgumentViolationSecurityException {
        DocumentOwnerGeneralModel documentOwner = documentOwnerGeneralModelRep.findOneByUserIdAndObjectName(valueObject.getDocumentOwnerId(), IDObLandedCost.SYS_NAME);
        if (documentOwner == null) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void validatePurchaseOrder(DObLandedCostCreationValueObject valueObject)
            throws Exception {
        IObOrderCompanyGeneralModel po =
                checkThatPurchaseOrderIsExist(valueObject.getPurchaseOrderCode());
        checkThatPurchaseOrderTypeAsLandedCostType(po, valueObject);
        checkThatPurchaseOrderInCorrectState(po);
        checkThatPurchaseOrderHasCompany(po);
        checkThatPurchaseOrderHasNotPostedLandedCost(valueObject.getPurchaseOrderCode());
    }

    private IObOrderCompanyGeneralModel checkThatPurchaseOrderIsExist(String purchaseOrderCode)
            throws Exception {
        IObOrderCompanyGeneralModel po = purhcaseOrderGMRep.findByUserCode(purchaseOrderCode);
        if (po == null) {
            throw new ArgumentViolationSecurityException();
        }
        return po;
    }

    private void checkThatPurchaseOrderTypeAsLandedCostType(
            IObOrderCompanyGeneralModel po, DObLandedCostCreationValueObject valueObject)
            throws Exception {
        if (valueObject.getLandedCostType().equals(ICObLandedCostType.IMPORT)
                && !po.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
            throw new ArgumentViolationSecurityException();
        }
        if (valueObject.getLandedCostType().equals(ICObLandedCostType.LOCAL)
                && !po.getObjectTypeCode().equals(OrderTypeEnum.LOCAL_PO.name())) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkThatPurchaseOrderInCorrectState(IObOrderCompanyGeneralModel po)
            throws Exception {
        Set<String> states = po.getCurrentStates();
        if (states.contains(DRAFT_STATE)) {
            throw new ArgumentViolationSecurityException();
        }
        if (states.contains(CANCELLED_STATE)) {
            validationResult.bindError(PURCHASE_ORDER_CODE, General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
        }
    }

    private void checkThatPurchaseOrderHasCompany(IObOrderCompanyGeneralModel po) throws Exception {
        if (po.getCompanyCode() == null) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkThatPurchaseOrderHasNotPostedLandedCost(String purchaseOrderCode) {
        DObLandedCostGeneralModel landedCost =
                landedCostGMRep.findByPurchaseOrderCodeAndCurrentStatesNotLike(
                        purchaseOrderCode, "%Draft%");

        if (landedCost != null) {
            validationResult.bindError(PURCHASE_ORDER_CODE, PURCHASE_ORDER_HAS_POSTED_LANDED_COST);
        }
    }

    private void checkStringFieldIsNotNull(Object field) throws Exception {
        if (field == null || field.toString().trim().isEmpty()) {
            throw new ArgumentViolationSecurityException();
        }
    }

    public void setPurhcaseOrderGMRep(IObOrderCompanyGeneralModelRep purhcaseOrderGMRep) {
        this.purhcaseOrderGMRep = purhcaseOrderGMRep;
    }

    public void setLandedCostGMRep(DObLandedCostGeneralModelRep landedCostGMRep) {
        this.landedCostGMRep = landedCostGMRep;
    }

    public void setDocumentOwnerGeneralModelRep(DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
        this.documentOwnerGeneralModelRep = documentOwnerGeneralModelRep;
    }
}
