package com.ebs.dda.accounting.paymentrequest.validation.apis;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IPaymentRequestCompanyDataMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {};

  String[] PAYMENT_DONE_STATE_MANDATORIES = {IIObPaymentRequestPaymentDetailsGeneralModel.COMPANY_CODE};

  String[] BANK_FORM_MANDATORIES = {
          IIObPaymentRequestPaymentDetailsGeneralModel.COMPANY_CODE
  };

  String[] CASH_FORM_MANDATORIES = {IIObPaymentRequestPaymentDetailsGeneralModel.COMPANY_CODE};

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObPaymentRequestStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObPaymentRequestStateMachine.PAYMENT_DONE_STATE, PAYMENT_DONE_STATE_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.BANK.toString(), BANK_FORM_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.CASH.toString(), CASH_FORM_MANDATORIES)
          .build();
}
