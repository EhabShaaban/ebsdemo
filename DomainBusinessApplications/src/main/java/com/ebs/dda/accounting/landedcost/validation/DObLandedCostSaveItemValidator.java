package com.ebs.dda.accounting.landedcost.validation;

import java.math.BigDecimal;
import java.util.List;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObAddLandedCostFactorItemValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;

public class DObLandedCostSaveItemValidator {

	private ValidationResult validationResult;

	private MObItemGeneralModelRep itemGeneralModelRep;
	private IObLandedCostFactorItemsGeneralModelRep costFactorItemsGeneralModelRep;

	public void checkMandatories(IObAddLandedCostFactorItemValueObject valueObject)
					throws Exception {
		checkStringFieldIsNotNull(valueObject.getItemCode());
		checkNumbericFieldIsNotNull(valueObject.getValue());
	}

	public ValidationResult validate(IObAddLandedCostFactorItemValueObject valueObject)
					throws Exception {
		validationResult = new ValidationResult();
		checkMandatories(valueObject);
		checkBusinessRules(valueObject);
		return validationResult;
	}

	private void checkBusinessRules(IObAddLandedCostFactorItemValueObject valueObject)
					throws Exception {
		MObItemGeneralModel itemGeneralModel = itemGeneralModelRep
						.findByUserCode(valueObject.getItemCode());
		List<IObLandedCostFactorItemsGeneralModel> landedCostFactorItemsGeneralModel = costFactorItemsGeneralModelRep
						.findAllByCode(valueObject.getLandedCostCode());
		checkThatItemExists(itemGeneralModel);
		checkThatItemIsDuplicated(landedCostFactorItemsGeneralModel, valueObject.getItemCode());
		checkThatItemIsNotCostFactor(itemGeneralModel);
	}

	private void checkThatItemIsNotCostFactor(MObItemGeneralModel itemGeneralModel) throws Exception {
		if (itemGeneralModel != null) {
			if (itemGeneralModel.getCostFactor() == null || !itemGeneralModel.getCostFactor()) {
				throw new ArgumentViolationSecurityException();
			}
		}
	}

	private void checkThatItemIsDuplicated(
					List<IObLandedCostFactorItemsGeneralModel> landedCostFactorItems,
					String newItemCode) throws Exception {
		boolean isItemExistsBefore = landedCostFactorItems.stream()
						.anyMatch(item -> item.getItemCode().equals(newItemCode));
		if (isItemExistsBefore) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkThatItemExists(MObItemGeneralModel itemGeneralModel) {
		if (itemGeneralModel == null) {
			validationResult.bindError(IIObLandedCostFactorItemValueObject.ITEM_CODE,
							IExceptionsCodes.General_MISSING_FIELD_INPUT);
		}
	}

	private void checkStringFieldIsNotNull(String field) throws Exception {
		if (field == null || field.trim().isEmpty()) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkNumbericFieldIsNotNull(BigDecimal field) throws Exception {
		if (field == null) {
			throw new ArgumentViolationSecurityException();
		}
	}

	public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
		this.itemGeneralModelRep = itemGeneralModelRep;
	}

	public void setCostFactorItemsGeneralModelRep(
					IObLandedCostFactorItemsGeneralModelRep costFactorItemsGeneralModelRep) {
		this.costFactorItemsGeneralModelRep = costFactorItemsGeneralModelRep;
	}
}
