package com.ebs.dda.accounting.notesreceivables.apis;

public interface IDObNotesReceivablesSectionNames {

  String NOTES_RECEIVABELS_DETAILS_SECTION = "PaymentDetails";
  String GENERAL_DATA_SECTION = "AccountingDetails";
  String NOTES_DETAILS_SECTION = "NotesDetails";
  String REFERENCE_DOCUMENTS_SECTION = "ReferenceDocuments";

}
