package com.ebs.dda.accounting.paymentrequest.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class DObPaymentMissingDueDocumentDataException extends BusinessRuleViolationException {
    public DObPaymentMissingDueDocumentDataException(ValidationResult validationResult) {
        super(validationResult);
    }

    @Override
    public String getResponseCode() {

        return IExceptionsCodes.PAYMENT_MISSING_DUE_DOCUMENT_DATA;
    }
}
