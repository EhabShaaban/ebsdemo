package com.ebs.dda.accounting.paymentrequest.validation.apis;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IPaymentRequestAccountingDetailsMandatoryAttributes {
  String[] DRAFT_STATE_MANDATORIES = {};

  String[] ALL_STATE_MANDATORIES = {
    IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE,
    IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_NAME,
    IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY,
    IIObAccountingDocumentAccountDetailsGeneralModel.SUB_LEDGER,
    IIObAccountingDocumentAccountDetailsGeneralModel.AMOUNT
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObPaymentRequestStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObPaymentRequestStateMachine.PAYMENT_DONE_STATE, ALL_STATE_MANDATORIES)
          .put(
              PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.toString(),
              ALL_STATE_MANDATORIES)
          .put(PaymentTypeEnum.PaymentType.VENDOR.toString(), ALL_STATE_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.BANK.toString(), ALL_STATE_MANDATORIES)
          .put(PaymentFormEnum.PaymentForm.CASH.toString(), ALL_STATE_MANDATORIES)
          .build();
}
