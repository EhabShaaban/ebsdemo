package com.ebs.dda.accounting.vendorinvoice.apis;

public interface IDOVendorInvoiceSchema {
  String CREATION_SCHEMA = "{\n" +
          "  \"properties\": {\n" +
          "    \"invoiceType\": {\n" +
          "      \"type\": [\n" +
          "        \"string\"\n" +
          "      ],\n" +
          "      \"enum\": [\n" +
          "        \"LOCAL_GOODS_INVOICE\",\n" +
          "        \"IMPORT_GOODS_INVOICE\",\n" +
          "        \"SHIPMENT_INVOICE\",\n" +
          "        \"CUSTOM_TRUSTEE_INVOICE\",\n" +
          "        \"INSURANCE_INVOICE\"\n" +
          "      ]\n" +
          "    },\n" +
          "    \"purchaseOrderCode\": {\n" +
          "      \"type\": [\n" +
          "        \"string\"\n" +
          "      ],\n" +
          "      \"minLength\": 10,\n" +
          "      \"maxLength\": 10,\n" +
          "      \"pattern\": \"^([0-9])*$\"\n" +
          "    },\n" +
          "    \"documentOwnerId\": {\n" +
          "      \"type\": [\n" +
          "        \"integer\"\n" +
          "      ]\n" +
          "    },\n" +
          "    \"vendorCode\": {\n" +
          "      \"type\": [\n" +
          "        \"string\"\n" +
          "      ],\n" +
          "      \"minLength\": 6,\n" +
          "      \"maxLength\": 6,\n" +
          "      \"pattern\": \"^([0-9])*$\"\n" +
          "    }\n" +
          "  }\n" +
          "}";
  String ACTIVATION_SCHEMA = "{\n" +
          "  \"properties\": {\n" +
          "    \"journalDate\": {\n" +
          "      \"type\": [\n" +
          "        \"string\"\n" +
          "      ],\n" +
          "      \"pattern\": \"^([0][1-9]|[1][0-9]|[2][0-9]|[3][0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\\\\d{4})( ([0-1][0-9]|[2][0-3]):[0-5][0-9])( (AM|PM))$\"\n" +
          "    }\n" +
          "  }\n" +
          "}\n";
}









