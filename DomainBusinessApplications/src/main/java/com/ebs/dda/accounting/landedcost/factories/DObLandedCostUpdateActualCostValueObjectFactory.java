package com.ebs.dda.accounting.landedcost.factories;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostUpdateActualCostValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;

public class DObLandedCostUpdateActualCostValueObjectFactory {

  private DObLandedCostUpdateActualCostValueObjectByPaymentFactory paymentFactory;
  private DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory serviceInvoceFactory;

  public DObLandedCostUpdateActualCostValueObjectFactory(
      DObLandedCostUpdateActualCostValueObjectByPaymentFactory paymentFactory,
      DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory serviceInvoceFactory) {

    this.paymentFactory = paymentFactory;
    this.serviceInvoceFactory = serviceInvoceFactory;
  }

  public DObLandedCostUpdateActualCostValueObject create(String objectSysName, String objectCode) {

    if (IDObPaymentRequest.SYS_NAME.equals(objectSysName)) {
      return paymentFactory.create(objectCode);
    }

    if (IDObVendorInvoice.SYS_NAME.equals(objectSysName)) {
      return serviceInvoceFactory.create(objectCode);
    }

    throw new UnsupportedOperationException(objectSysName + " factory is not supported");
  }

}
