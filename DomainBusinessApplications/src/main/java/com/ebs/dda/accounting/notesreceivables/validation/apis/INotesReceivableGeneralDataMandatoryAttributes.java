package com.ebs.dda.accounting.notesreceivables.validation.apis;

import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface INotesReceivableGeneralDataMandatoryAttributes {

    String[] DRAFT_STATE_MANDATORIES = {
            IDObNotesReceivablesGeneralModel.SOURCE,
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE,
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR,
            IDObNotesReceivablesGeneralModel.COMPANY_CODE_NAME
    };

    String[] ACTIVE_STATE_MANDATORIES = {
            IDObNotesReceivablesGeneralModel.SOURCE,
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE,
            IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR,
            IDObNotesReceivablesGeneralModel.COMPANY_CODE_NAME
    };

    Map<String, String[]> MANDATORIES =
            ImmutableMap.<String, String[]>builder()
                    .put(DObNotesReceivablesStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
                    .put(DObNotesReceivablesStateMachine.ACTIVE_STATE, ACTIVE_STATE_MANDATORIES)
                    .build();

}
