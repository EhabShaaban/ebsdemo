package com.ebs.dda.accounting.electronicinvoice.types;

import java.math.BigDecimal;

public class EInvoiceDelivery {
    private String approach;
    private String packaging;
    private String dateValidity;
    private String exportPort;
    private String countryOfOrigin;
    private BigDecimal grossWeight;
    private BigDecimal netWeight;
    private String terms;
}
