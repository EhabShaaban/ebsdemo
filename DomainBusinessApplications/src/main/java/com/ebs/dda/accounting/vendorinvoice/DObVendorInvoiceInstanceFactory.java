package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.*;

public class DObVendorInvoiceInstanceFactory {


    public DObVendorInvoice createVendorInvoiceInstance(String vendorInvoiceType) {
        switch (vendorInvoiceType) {
            case VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE:
                return new DObLocalGoodsInvoice();
            case VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE:
                return new DObImportGoodsInvoice();
            case VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE:
                return new DObCustomTrusteeInvoice();
            case VendorInvoiceTypeEnum.Values.SHIPMENT_INVOICE:
                return new DObShipmentInvoice();
            case VendorInvoiceTypeEnum.Values.INSURANCE_INVOICE:
                return new DObInsuranceInvoice();
        }
        throw new IllegalArgumentException("Unsupported Vendor Invoice Type");
    }
}
