package com.ebs.dda.accounting.electronicinvoice;

public interface EInvoiceAPIProperities {
    String API_CURRUNT_VERSION = "1.0";
    String DOC_TYPE_ISSUER = "I";
    String BUSINESS_IN_EGYPT = "B";
    String LINE_TYPE = "GS1";
}
