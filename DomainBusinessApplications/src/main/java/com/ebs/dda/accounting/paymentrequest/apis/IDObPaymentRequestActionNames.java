package com.ebs.dda.accounting.paymentrequest.apis;

public interface IDObPaymentRequestActionNames {

    String READ_COMPANY_DATA = "ReadCompanyData";
    String READ_PAYMENT_DETAILS = "ReadPaymentDetails";
    String READ_ACCOUNTING_DETAILS = "ReadAccountingDetails";
    String READ_POSTING_DETAILS = "ReadPostingDetails";
    String Edit_PAYMENT_DETAILS = "EditPaymentDetails";
    String Edit_ACCOUNTING_DETAILS = "EditAccountingDetails";
    String ACTIVATE = "Activate";
}
