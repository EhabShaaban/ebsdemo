package com.ebs.dda.accounting.notesreceivables;

import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesDetailsValueObject;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IObNotesReceivableDetailsValueObjectMandatoryAttributes {

  String[] DRAFT_STATE_MANDATORIES = {};
  String[] ACTIVE_STATE_MANDATORIES = {
    IIObMonetaryNotesDetailsValueObject.NOTE_NUMBER,
    IIObMonetaryNotesDetailsValueObject.NOTE_BANK,
    IIObMonetaryNotesDetailsValueObject.DEPOT_CODE,
    IIObMonetaryNotesDetailsValueObject.DUE_DATE
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObNotesReceivablesStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
          .put(DObNotesReceivablesStateMachine.ACTIVE_STATE, ACTIVE_STATE_MANDATORIES)
          .build();
}
