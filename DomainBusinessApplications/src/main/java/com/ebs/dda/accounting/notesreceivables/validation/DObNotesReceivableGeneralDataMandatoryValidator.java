package com.ebs.dda.accounting.notesreceivables.validation;

import com.ebs.dda.accounting.notesreceivables.validation.apis.INotesReceivableGeneralDataMandatoryAttributes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.List;
import java.util.Map;

public class DObNotesReceivableGeneralDataMandatoryValidator
        implements ISectionMandatoryValidator {
    private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
    private MandatoryValidatorUtils validatorUtils;

    public DObNotesReceivableGeneralDataMandatoryValidator() {
        validatorUtils = new MandatoryValidatorUtils();
    }

    @Override
    public void validateValueObjectMandatories(Object o) {}

    @Override
    public List<String> validateGeneralModelMandatories(String notesReceivableCode, String state)
            throws Exception {
        DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
                notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);
        Map<String, String[]> mandatories =
                INotesReceivableGeneralDataMandatoryAttributes.MANDATORIES;
        String[] stateMandatoryAttributes =
                validatorUtils.getMandatoryAttributesByState(mandatories, state);
        return validatorUtils.validateMandatoriesForGeneralModel(
                stateMandatoryAttributes, notesReceivablesGeneralModel);
    }

    public void setNotesReceivablesGeneralModelRep(
            DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
        this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
    }
}
