package com.ebs.dda.accounting.notesreceivables.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesCreateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;

public class DObNotesReceivableCreationValidator {

  private ValidationResult validationResult;

  private CObPurchasingUnitGeneralModelRep businessUnitRep;
  private CObCompanyGeneralModelRep companyRep;
  private IObCustomerBusinessUnitGeneralModelRep businessPartnerRep;
  private DocumentOwnerGeneralModelRep documentOwnerRep;
  private CObCurrencyRep currencyRep;

  public ValidationResult validate(DObNotesReceivablesCreateValueObject valueObject)
    throws Exception {
    validationResult = new ValidationResult();
    checkMissingMandatoriesFields(valueObject);
    checkInvalidData(valueObject);
    return validationResult;
  }

  private void checkInvalidData(DObNotesReceivablesCreateValueObject valueObject) throws Exception {
    checkInvalidBusinessUnit(valueObject.getBusinessUnitCode());
    checkInvalidCompany(valueObject.getCompanyCode());
    checkInvalidCustomer(valueObject);
    checkInvalidDocumentOwner(valueObject.getDocumentOwnerId());
    checkInvalidCurrency(valueObject.getCurrencyIso());
  }

  private void checkInvalidBusinessUnit(String businessUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel businessUnit =
      businessUnitRep.findOneByUserCode(businessUnitCode);
    checkIfEntityIsNull(businessUnit);
  }

  private void checkInvalidCompany(String companyCode) throws Exception {
    CObCompanyGeneralModel company = companyRep.findOneByUserCode(companyCode);
    checkIfEntityIsNull(company);
  }

  private void checkInvalidCustomer(DObNotesReceivablesCreateValueObject valueObject)
    throws Exception {
    IObCustomerBusinessUnitGeneralModel customer = businessPartnerRep
      .findOneByUserCodeAndBusinessUnitcode(valueObject.getBusinessPartnerCode(),
        valueObject.getBusinessUnitCode());
    checkIfEntityIsNull(customer);
  }

  private void checkInvalidDocumentOwner(Long documentOwnerId) throws Exception {
    DocumentOwnerGeneralModel documentOwner = documentOwnerRep
      .findOneByUserIdAndObjectName(documentOwnerId, IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME);

    checkIfEntityIsNull(documentOwner);
  }

  private void checkInvalidCurrency(String currencyIso) throws Exception {
    CObCurrency currency = currencyRep.findOneByIso(currencyIso);
    checkIfEntityIsNull(currency);
  }

  private void checkMissingMandatoriesFields(DObNotesReceivablesCreateValueObject valueObject)
    throws Exception {
    checkIfStringIsEmpty(valueObject.getBusinessUnitCode());
    checkIfStringIsEmpty(valueObject.getCompanyCode());
    checkIfStringIsEmpty(valueObject.getBusinessPartnerCode());
    checkIfEntityIsNull(valueObject.getDocumentOwnerId());
    checkIfEntityIsNull(valueObject.getAmount());
    checkIfStringIsEmpty(valueObject.getCurrencyIso());
  }

  private void checkIfEntityIsNull(Object field) throws Exception {
    if (field == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfStringIsEmpty(Object field) throws Exception {
    if (field == null || field.toString().trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setBusinessUnitRep(CObPurchasingUnitGeneralModelRep businessUnitRep) {
    this.businessUnitRep = businessUnitRep;
  }

  public void setCompanyRep(CObCompanyGeneralModelRep companyRep) {
    this.companyRep = companyRep;
  }

  public void setBusinessPartnerRep(IObCustomerBusinessUnitGeneralModelRep businessPartnerRep) {
    this.businessPartnerRep = businessPartnerRep;
  }

  public void setDocumentOwnerRep(DocumentOwnerGeneralModelRep documentOwnerRep) {
    this.documentOwnerRep = documentOwnerRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }
}
