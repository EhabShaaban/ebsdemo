package com.ebs.dda.accounting.journalentry.apis;

public interface IIObJournalEntryActionNames {

  String READ_ITEM_SECTION = "ReadJournalItem";

}
