package com.ebs.dda.accounting.vendorinvoice.apis;

import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoiceDetails;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IDObInvoiceDetailsMandatoryAttributes {
	String[] DRAFT_STATE_MANDATORIES = {};

	
	String[] POST_STATE_MANDATORIES = {IIObVendorInvoiceDetails.INVOICE_NUMBER,
			IIObVendorInvoiceDetails.INVOICE_DATE, IIObVendorInvoiceDetails.PAYMENT_TERM,
			IIObVendorInvoiceDetails.CURRENCY_ISO, IIObVendorInvoiceDetails.CURRENCY_NAME};
	
	Map<String, String[]> INVOICE_DETAILS_MANDATORIES = ImmutableMap
			.<String, String[]>builder()
			.put(DObVendorInvoiceStateMachine.DRAFT_STATE, DRAFT_STATE_MANDATORIES)
			.build();
}

