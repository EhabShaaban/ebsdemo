package com.ebs.dda.accounting.salesinvoice.validation;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MISSING_FIELD_INPUT;
import static com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceCreationValueObject.DOCUMENT_OWNER_ID;
import static com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceCreationValueObject.SALES_ORDER_CODE;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.CANCELED;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.DELIVERY_COMPLETE;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.DRAFT_STATE;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.EXPIRED;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.READY_FOR_DELIVERY;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.REJECTED;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.SALES_INVOICE_ACTIVATED;
import static com.ebs.dda.order.salesorder.DObSalesOrderStateMachine.WAITING_APPROVAL;
import java.util.Set;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceCreationValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.CObSalesInvoiceRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;

public class DObSalesInvoiceCreationValidator {

  private ValidationResult validationResult;

  private CObSalesInvoiceRep salesInvoiceTypeRep;
  private DObSalesOrderRep salesOrderRep;
  private CObPurchasingUnitRep businessUnitRep;
  private DocumentOwnerGeneralModelRep documentOwnerRep;

  public void checkMandatories(DObSalesInvoiceCreationValueObject valueObject) throws Exception {
    checkStringFieldIsNotNull(valueObject.getInvoiceTypeCode());
    checkStringFieldIsNotNull(valueObject.getBusinessUnitCode());
    checkNumericFieldIsNotNull(valueObject.getDocumentOwnerId());
    if (isSalesInvoiceForSalesOrder(valueObject.getInvoiceTypeCode())) {
      checkStringFieldIsNotNull(valueObject.getSalesOrderCode());
      checkStringFieldIsNull(valueObject.getCompanyCode());
      checkStringFieldIsNull(valueObject.getCustomerCode());
    }
  }

  public ValidationResult validateBusinessRules(DObSalesInvoiceCreationValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();

    validateSalesInvoiceType(valueObject.getInvoiceTypeCode());
    if (isSalesInvoiceForSalesOrder(valueObject.getInvoiceTypeCode())) {
      validateSalesOrder(valueObject.getSalesOrderCode());
    }
    validateBusinessUnit(valueObject.getBusinessUnitCode());
    validateDocumentOwner(valueObject.getDocumentOwnerId());

    return validationResult;
  }


  /*************************** SalesInvoiceType ********************/
  private void validateSalesInvoiceType(String invoiceTypeCode) throws Exception {
    checkThatSalesInvoiceTypeIsExist(invoiceTypeCode);
  }

  private void checkThatSalesInvoiceTypeIsExist(String invoiceTypeCode) throws Exception {
    if (!salesInvoiceTypeRep.existsByUserCode(invoiceTypeCode)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  /*************************** SalesOrder **************************/
  private void validateSalesOrder(String salesOrderCode) throws Exception {
    DObSalesOrder salesOrder = checkThatSalesOrderIsExist(salesOrderCode);
    checkThatSalesOrderInCorrectState(salesOrder);
  }

  private DObSalesOrder checkThatSalesOrderIsExist(String salesOrderCode) throws Exception {
    DObSalesOrder salesOrder = salesOrderRep.findOneByUserCode(salesOrderCode);
    if (salesOrder == null) {
      throw new ArgumentViolationSecurityException();
    }
    return salesOrder;
  }

  private void checkThatSalesOrderInCorrectState(DObSalesOrder salesOrder) throws Exception {
    Set<String> states = salesOrder.getCurrentStates();
    checkSalesOrderErrorStates(states);
    checkSalesOrderFailureStates(states);
  }

  private void checkSalesOrderErrorStates(Set<String> states)
      throws ArgumentViolationSecurityException {
    String[] errorStates =
        new String[] {DRAFT_STATE, REJECTED, WAITING_APPROVAL, EXPIRED, DELIVERY_COMPLETE};

    for (int i = 0; i < errorStates.length; i++) {
      if (states.contains(errorStates[i])) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkSalesOrderFailureStates(Set<String> states) {
    String[] failureStates = new String[] {CANCELED, SALES_INVOICE_ACTIVATED, READY_FOR_DELIVERY};

    for (int i = 0; i < failureStates.length; i++) {
      if (states.contains(failureStates[i])) {
        validationResult.bindError(SALES_ORDER_CODE, General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
        return;
      }
    }
  }

  /*************************** BusinessUnit ************************/
  private void validateBusinessUnit(String businessUnitCode) throws Exception {
    checkThatBusinessUnitIsExist(businessUnitCode);
  }

  private void checkThatBusinessUnitIsExist(String businessUnitCode) throws Exception {
    if (!businessUnitRep.existsByUserCode(businessUnitCode)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  /*************************** DocumentOwner ********************/
  private void validateDocumentOwner(Long documentOwnerId) throws Exception {
    checkThatDocumnetOwnerIsExist(documentOwnerId);
  }

  private void checkThatDocumnetOwnerIsExist(Long documentOwnerId) {
    DocumentOwnerGeneralModel docOwner =
        documentOwnerRep.findOneByUserIdAndObjectName(documentOwnerId, IDObSalesInvoice.SYS_NAME);
    if (docOwner == null) {
      validationResult.bindError(DOCUMENT_OWNER_ID, General_MISSING_FIELD_INPUT);
    }
  }

  /*************************** General **************************/

  private boolean isSalesInvoiceForSalesOrder(String invoiceTypeCode) {
    return invoiceTypeCode.equals("SALES_INVOICE_FOR_SALES_ORDER");
  }

  private void checkStringFieldIsNotNull(String field) throws Exception {
    if (field == null || field.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkNumericFieldIsNotNull(Long field) throws Exception {
    if (field == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkStringFieldIsNull(String field) throws Exception {
    if (field != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setSalesInvoiceTypeRep(CObSalesInvoiceRep salesInvoiceTypeRep) {
    this.salesInvoiceTypeRep = salesInvoiceTypeRep;
  }


  public void setSalesOrderRep(DObSalesOrderRep salesOrderRep) {
    this.salesOrderRep = salesOrderRep;
  }


  public void setBusinessUnitRep(CObPurchasingUnitRep businessUnitRep) {
    this.businessUnitRep = businessUnitRep;
  }


  public void setDocumentOwnerRep(DocumentOwnerGeneralModelRep documentOwnerRep) {
    this.documentOwnerRep = documentOwnerRep;
  }



}
