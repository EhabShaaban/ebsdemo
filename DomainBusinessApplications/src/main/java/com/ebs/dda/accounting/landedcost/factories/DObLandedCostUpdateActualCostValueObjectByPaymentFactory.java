package com.ebs.dda.accounting.landedcost.factories;

import java.math.BigDecimal;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostUpdateActualCostValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;

public class DObLandedCostUpdateActualCostValueObjectByPaymentFactory {

  private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderRep;

  public DObLandedCostUpdateActualCostValueObjectByPaymentFactory(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderRep) {

    this.paymentDetailsRep = paymentDetailsRep;
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public DObLandedCostUpdateActualCostValueObject create(String paymentCode) {

    DObLandedCostUpdateActualCostValueObject valueObject =
        new DObLandedCostUpdateActualCostValueObject();

    IObPaymentRequestPaymentDetailsGeneralModel paymentDetails =
        paymentDetailsRep.findOneByUserCode(paymentCode);

    valueObject.setPurchaseOrderCode(getPurchaseOrderCode(paymentDetails));
    valueObject.setRefDocumentId(paymentDetails.getRefInstanceId());

    valueObject.addItem(paymentDetails.getCostFactorItemCode(),
        paymentDetails.getNetAmount());

    return valueObject;
  }

  private String getPurchaseOrderCode(IObPaymentRequestPaymentDetailsGeneralModel paymentDetails) {
    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderRep.findOneByUserCode(paymentDetails.getDueDocumentCode());

    String purchaseOrderCode = getPoCode(purchaseOrder);
    return purchaseOrderCode;
  }

  private String getPoCode(DObPurchaseOrderGeneralModel purchaseOrder) {
    if (purchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name())) {
      return purchaseOrder.getRefDocumentCode();
    }
    return purchaseOrder.getUserCode();
  }

}
