package com.ebs.dda.accounting.settlement.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementCreationValueObject;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;

public class DObSettlementCreationValidator {
	private ValidationResult validationResult;

	private CObPurchasingUnitRep businessUnitRep;
	private CObCompanyRep companyRep;
	private CObCurrencyRep currencyRep;
	private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

	public ValidationResult validate(DObSettlementCreationValueObject valueObject)
					throws Exception {
		validationResult = new ValidationResult();
		checkMissingMandatoriesFields(valueObject);
		checkInvalidData(valueObject);
		return validationResult;
	}

	private void checkInvalidData(DObSettlementCreationValueObject valueObject) throws Exception {
		checkInvalidBusinessUnit(valueObject);
		checkInvalidCompany(valueObject);
		checkInvalidCurrency(valueObject);
		checkInvalidDocumentOwner(valueObject);
	}

	private void checkInvalidBusinessUnit(DObSettlementCreationValueObject valueObject)
					throws Exception {
		CObPurchasingUnit businessUnit = businessUnitRep
						.findOneByUserCode(valueObject.getBusinessUnitCode());
		checkIfEntityIsNull(businessUnit);
	}

	private void checkInvalidCompany(DObSettlementCreationValueObject valueObject)
					throws Exception {
		CObCompany company = companyRep.findOneByUserCode(valueObject.getCompanyCode());
		checkIfEntityIsNull(company);
	}

	private void checkInvalidCurrency(DObSettlementCreationValueObject valueObject)
					throws Exception {
		CObCurrency currency = currencyRep.findOneByUserCode(valueObject.getCurrencyCode());
		checkIfEntityIsNull(currency);
	}

	private void checkInvalidDocumentOwner(DObSettlementCreationValueObject valueObject)
					throws Exception {
		DocumentOwnerGeneralModel documentOwner = documentOwnerGeneralModelRep
						.findOneByUserIdAndObjectName(valueObject.getDocumentOwnerId(),
										IDObSettlement.SYS_NAME);
		checkIfEntityIsNull(documentOwner);
	}

	private void checkMissingMandatoriesFields(DObSettlementCreationValueObject valueObject)
					throws Exception {
		checkIfEntityIsNull(valueObject.getType());
		checkIfEntityIsNull(valueObject.getBusinessUnitCode());
		checkIfEntityIsNull(valueObject.getCompanyCode());
		checkIfEntityIsNull(valueObject.getCurrencyCode());
		checkIfEntityIsNull(valueObject.getDocumentOwnerId());

	}

	public static void checkIfEntityIsNull(Object object) throws Exception {
		if (object == null || object.toString().trim().isEmpty()) {
			throw new ArgumentViolationSecurityException();
		}
	}

	public void setBusinessUnitRep(CObPurchasingUnitRep businessUnitRep) {
		this.businessUnitRep = businessUnitRep;
	}

	public void setCompanyRep(CObCompanyRep companyRep) {
		this.companyRep = companyRep;
	}

	public void setCurrencyRep(CObCurrencyRep currencyRep) {
		this.currencyRep = currencyRep;
	}

	public void setDocumentOwnerGeneralModelRep(
					DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
		this.documentOwnerGeneralModelRep = documentOwnerGeneralModelRep;
	}
}
