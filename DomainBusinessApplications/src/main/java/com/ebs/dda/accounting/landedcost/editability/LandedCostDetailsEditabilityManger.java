package com.ebs.dda.accounting.landedcost.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostDetailsGeneralModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LandedCostDetailsEditabilityManger extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(IIObLandedCostDetailsGeneralModel.INVOICE_AMOUNT));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObLandedCostDetailsGeneralModel.VENDOR_INVOICE_CODE));
    draftEnabledAttributes.addAll(
            Arrays.asList(IIObLandedCostDetailsGeneralModel.DAMAGE_STOCK));
    enabledAttributesPerState.put(
        DObLandedCostStateMachine.DRAFT_STATE, draftEnabledAttributes);

    return enabledAttributesPerState;
  }

}
