package com.ebs.dda.accounting.settlement.apis;

public interface IDObSettlementJsonSchema {
	String CREATION_SCHEMA = "{\n" + "  \"properties\": {\n" + "    \"type\": {\n"
					+ "      \"type\": [\n" + "        \"string\"\n" + "      ],\n"
					+ "      \"enum\": [\n" + "        \"SETTLEMENTS\"\n" + "      ]\n" + "    },\n"
					+ "    \"businessUnitCode\": {\n" + "      \"type\": [\n"
					+ "        \"string\"\n" + "      ],\n" + "      \"minLength\": 4,\n"
					+ "      \"maxLength\": 4,\n" + "      \"pattern\": \"^([0-9])*$\"\n"
					+ "    },\n" + "    \"companyCode\": {\n" + "      \"type\": [\n"
					+ "        \"string\"\n" + "      ],\n" + "      \"minLength\": 4,\n"
					+ "      \"maxLength\": 4,\n" + "      \"pattern\": \"^([0-9])*$\"\n"
					+ "    },\n" + "    \"currencyCode\": {\n" + "      \"type\": [\n"
					+ "        \"string\"\n" + "      ],\n" + "      \"minLength\": 4,\n"
					+ "      \"maxLength\": 4,\n" + "      \"pattern\": \"^([0-9])*$\"\n"
					+ "    },\n" + "    \"documentOwnerId\": {\n" + "      \"type\": [\n"
					+ "        \"number\"\n" + "      ]\n" + "    }\n" + "  }\n" + "}";
}
