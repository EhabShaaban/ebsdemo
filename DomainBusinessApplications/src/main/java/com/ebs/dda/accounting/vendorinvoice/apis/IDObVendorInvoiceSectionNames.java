package com.ebs.dda.accounting.vendorinvoice.apis;

public interface IDObVendorInvoiceSectionNames {
	  String INVOICE_DETAILS = "InvoiceDetails";
  String ITEMS_SECTION = "InvoiceItems";
  String TAXES_SECTION = "InvoiceTaxes";
    String SUMMARY_SECTION = "InvoiceSummary";

}
