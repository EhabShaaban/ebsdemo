package com.ebs.dda.accounting.settlement.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.SubAccountGeneralModel;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;

public class IObSettlementAddAccountingDetailsValidator {
  private ValidationResult validationResult;
  private MandatoryValidatorUtils validatorUtils;

  private DObSettlementGeneralModelRep settlementGeneralModelRep;
  private HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep;
  private SubAccountGeneralModelRep subAccountGeneralModelRep;

  public IObSettlementAddAccountingDetailsValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public ValidationResult validate(
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject) throws Exception {
    validationResult = new ValidationResult();
    checkMissingMandatoriesFields(accountingDetailsValueObject);
    checkInvalidData(accountingDetailsValueObject);

    return validationResult;
  }

  private void checkInvalidData(
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    checkIfValidSettlement(accountingDetailsValueObject);
    checkIfValidGLAccountAndHasSubAccount(accountingDetailsValueObject);
  }

  private void checkIfValidGLSubAccount(
      HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel,
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    List<SubAccountGeneralModel> subAccountGeneralModelList =
        subAccountGeneralModelRep.findAllByLedger(chartOfAccountsGeneralModel.getLeadger());
    Boolean retrievedSubAccount = true;
    if (chartOfAccountsGeneralModel.getLeadger() != null) {
      retrievedSubAccount =
          subAccountGeneralModelList.stream()
              .map(SubAccountGeneralModel::getCode)
              .anyMatch(accountingDetailsValueObject.getGlSubAccountCode()::contains);
    }
    if (!retrievedSubAccount) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValidGLAccountAndHasSubAccount(
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel =
        glAccountsGeneralModelRep.findOneByUserCode(
            accountingDetailsValueObject.getGlAccountCode());
    checkIfEntityIsNull(chartOfAccountsGeneralModel);
    checkIfHasSubAccount(chartOfAccountsGeneralModel, accountingDetailsValueObject);
    checkIfValidGLSubAccount(chartOfAccountsGeneralModel, accountingDetailsValueObject);
  }



  private void checkIfHasSubAccount(
      HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel,
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    if ((chartOfAccountsGeneralModel.getLeadger() == null
            && accountingDetailsValueObject.getGlSubAccountCode() != null)
        || (chartOfAccountsGeneralModel.getLeadger() != null
            && accountingDetailsValueObject.getGlSubAccountCode() == null)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValidSettlement(
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    DObSettlementGeneralModel settlementGeneralModel =
        settlementGeneralModelRep.findOneByUserCode(
            accountingDetailsValueObject.getSettlementCode());
    checkIfEntityIsNull(settlementGeneralModel);
  }

  private void checkMissingMandatoriesFields(
      IObSettlementAddAccountingDetailsValueObject accountingDetailsValueObject)
      throws ArgumentViolationSecurityException {
    checkIfEntityIsNull(accountingDetailsValueObject.getAccountType());
    checkIfEntityIsNull(accountingDetailsValueObject.getGlAccountCode());
    checkIfEntityIsNull(accountingDetailsValueObject.getSettlementCode());
    checkIfEntityIsNull(accountingDetailsValueObject.getAmount());
  }

  private void checkIfEntityIsNull(Object object) throws ArgumentViolationSecurityException {
    if (object == null || object.toString().trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setValidationResult(ValidationResult validationResult) {
    this.validationResult = validationResult;
  }

  public void setSubAccountGeneralModelRep(SubAccountGeneralModelRep subAccountGeneralModelRep) {
    this.subAccountGeneralModelRep = subAccountGeneralModelRep;
  }

  public void setSettlementGeneralModelRep(DObSettlementGeneralModelRep settlementGeneralModelRep) {
    this.settlementGeneralModelRep = settlementGeneralModelRep;
  }

  public void setGlAccountsGeneralModelRep(
      HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep) {
    this.glAccountsGeneralModelRep = glAccountsGeneralModelRep;
  }
}
