package com.ebs.dda.accounting.paymentrequest.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class DObPaymentAmountGreaterThanVendorInvoiceException extends BusinessRuleViolationException {
    public DObPaymentAmountGreaterThanVendorInvoiceException(ValidationResult validationResult) {
        super(validationResult);
    }

    @Override
    public String getResponseCode() {
        return IExceptionsCodes.PAYMENT_AMOUNT_GREATER_THAN_VENDOR_INVOICE_REMAINING;
    }
}
