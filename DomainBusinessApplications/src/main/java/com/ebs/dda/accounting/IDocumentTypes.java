package com.ebs.dda.accounting;

public interface IDocumentTypes {
  String PAYMENT_REQUEST = "PaymentRequest";
  String COLLECTION = "Collection";
  String VENDOR_INVOICE = "VendorInvoice";
  String SALES_INVOICE = "SalesInvoice";
  String LANDED_COST = "LandedCost";
  String SETTLEMENT = "Settlement";
  String COSTING = "Costing";
  String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
  String NOTES_RECEIVABLE_AS_COLLECTION = "NOTES_RECEIVABLE_AS_COLLECTION";
}
