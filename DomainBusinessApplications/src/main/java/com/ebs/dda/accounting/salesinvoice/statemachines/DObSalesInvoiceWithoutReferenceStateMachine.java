package com.ebs.dda.accounting.salesinvoice.statemachines;

public class DObSalesInvoiceWithoutReferenceStateMachine extends DObSalesInvoiceStateMachine {

	public static final String READY_FOR_DELIVERING_STATE = "ReadyForDelivering";

	public DObSalesInvoiceWithoutReferenceStateMachine() throws Exception {
		super("META-INF/DObSalesInvoice.scxml");
	}

	public boolean goToReadyForDelivering() {
		return true;
	}

	@Override
	public boolean goToDraft() {
		return true;
	}

	@Override
	public boolean goToActive() {
		return true;
	}

	@Override
	public boolean goToClosed() {
		return true;
	}

	@Override
	public boolean goToOpened() {
		return true;
	}

	@Override
	public boolean goToOperativeStates() {
		return true;
	}

	@Override
	public boolean goToParallelOperativeStates() {
		return true;
	}

	@Override
	public boolean goToDeliveringStates() {
		return true;
	}

	@Override
	public boolean goToParallelClosed() {
		return true;
	}
}
