package com.ebs.dda.accounting.electronicinvoice.types;

import lombok.Builder;

import java.math.BigDecimal;
import java.util.List;

@Builder
public class EInvoiceLine {
    private String description;
    private String itemType;
    private String itemCode;
    private String unitType;
    private BigDecimal quantity;
    private EInvoiceValue unitValue;
    private BigDecimal salesTotal;
    private BigDecimal total;
    private BigDecimal valueDifference;
    private BigDecimal totalTaxableFees;
    private BigDecimal netTotal;
    private BigDecimal itemsDiscount;
    private EInvoiceDiscount discount;
    private List<EInvoiceTaxableItem> taxableItems;
    private String internalCode;
}
