package com.ebs.dda.accounting.paymentrequest.apis;

public interface IDObPaymentRequestJsonSchema {

    String PAYMENT_CREATE_SCHEMA = "{\n" +
            "  \"properties\": {\n" +
            "    \"paymentType\": {\n" +
            "      \"type\": \"string\",\n" +
            "      \"enum\": [\n" +
            "        \"OTHER_PARTY_FOR_PURCHASE\",\n" +
            "        \"VENDOR\",\n" +
            "        \"UNDER_SETTLEMENT\",\n" +
            "        \"BASED_ON_CREDIT_NOTE\"\n" +

            "      ]\n" +
            "    },\n" +
            "    \"paymentForm\": {\n" +
            "      \"type\": \"string\",\n" +
            "      \"enum\": [\n" +
            "        \"CASH\",\n" +
            "        \"BANK\"\n" +
            "      ]\n" +
            "    },\n" +
            "    \"businessUnitCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\"\n" +
            "      ],\n" +
            "      \"minLength\": 4,\n" +
            "      \"maxLength\": 4,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"dueDocumentCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"maxLength\": 10,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"currencyCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"minLength\": 4,\n" +
            "      \"maxLength\": 4,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"businessPartnerCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"minLength\": 6,\n" +
            "      \"maxLength\": 6,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"documentOwnerId\": {\n" +
            "      \"type\": [\n" +
            "        \"integer\"\n" +
            "      ]\n" +
            "    },\n" +

            "    \"dueDocumentType\": {\n" +
            "      \"type\": [\n" +
            "        \"string\"\n" +
            "      ],\n" +
            "      \"optional\": false,\n" +
            "      \"enum\": [\n" +
            "        \"INVOICE\",\n" +
            "        \"PURCHASEORDER\",\n" +
            "        \"CREDITNOTE\"\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n";

    String PAYMENT_DETAILS_SCHEMA = "{\n" +
            "  \"properties\": {\n" +
            "    \"netAmount\": {\n" +
            "      \"type\": [\n" +
            "        \"number\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"maximum\": 1000000000,\n" +
            "      \"minimum\": 0.000001,\n" +
            "      \"multipleOf\": 0.000001" +
            "    },\n" +
            "    \"currencyCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"maxLength\": 4,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"description\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"pattern\": \"^([\\u0621-\\u064A\\u0660-\\u0669\\uFE70-\\uFEFF]|[ A-Za-z0-9\\u0621-\\u064A\\u0660-\\u0669\\uFE70-\\uFEFF_@.%&,-/()*\\n])*$\",\n" +
            "      \"maxLength\": 250\n" +
            "    },\n" +
            "    \"companyBankId\": {\n" +
            "      \"type\": [\n" +
            "        \"number\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"costFactorItemCode\":{\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"maxLength\": 6,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "  \t},\n" +
            "    \"treasuryCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"maxLength\": 4,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    String PAYMENT_ACTIVATE_SCHEMA = "{\n" +
            "  \"properties\": {\n" +
            "    \"dueDate\": {\n" +
            "      \"type\": \"string\",\n" +
            "      \"pattern\": \"^([0][1-9]|[1][0-9]|[2][0-9]|[3][0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\\\\d{4})( ([0-1][0-9]|[2][0-3]):[0-5][0-9])( (AM|PM))$\"\n" +
            "    }\n" +
            "  }\n" +
            "}";
}
