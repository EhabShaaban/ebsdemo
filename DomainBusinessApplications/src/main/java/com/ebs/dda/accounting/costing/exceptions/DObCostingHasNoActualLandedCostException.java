package com.ebs.dda.accounting.costing.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class DObCostingHasNoActualLandedCostException extends BusinessRuleViolationException {
  public DObCostingHasNoActualLandedCostException(ValidationResult validationResult) {
    super(validationResult);
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.COSTING_HAS_NO_ACTUAL_LANDED_COST;
  }
}
