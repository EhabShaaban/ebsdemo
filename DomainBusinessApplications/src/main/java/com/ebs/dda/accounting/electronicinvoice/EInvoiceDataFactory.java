package com.ebs.dda.accounting.electronicinvoice;

public interface EInvoiceDataFactory {

    EInvoiceData createInvoiceData(String userCode);

}
