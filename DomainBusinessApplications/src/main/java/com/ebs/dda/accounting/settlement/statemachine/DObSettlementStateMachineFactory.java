package com.ebs.dda.accounting.settlement.statemachine;

import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

public class DObSettlementStateMachineFactory {

  public static final String DRAFT_STATE = "Draft";

  public AbstractStateMachine createStateMachine(DObAccountingDocument settlement) throws Exception {
    DObSettlementStateMachine stateMachine = new DObSettlementStateMachine();
    stateMachine.initObjectState(settlement);
    return stateMachine;
  }
  public void isAllowedAction(DObAccountingDocument settlement, String actionName) throws Exception {
      DObSettlementStateMachine landedCostStateMachine =
              new DObSettlementStateMachine();
      landedCostStateMachine.initObjectState(settlement);
      landedCostStateMachine.isAllowedAction(actionName);

  }


}
