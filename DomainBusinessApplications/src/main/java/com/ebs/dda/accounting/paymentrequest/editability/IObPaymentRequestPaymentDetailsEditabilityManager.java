package com.ebs.dda.accounting.paymentrequest.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;

import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import java.util.*;

public class IObPaymentRequestPaymentDetailsEditabilityManager extends AbstractEditabilityManager {

  @Override
    public Map<String, Set<String>> getEnabledAttributesConfig() {
        Map<String, Set<String>> enabledAttributesPerStateAndType = new HashMap<String, Set<String>>();
        Set<String> draftEnabledAttributes = new HashSet<>();
        draftEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT));
        draftEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE));
        draftEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION));
        draftEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE));

        enabledAttributesPerStateAndType.put(DObPaymentRequestStateMachine.DRAFT_STATE, draftEnabledAttributes);
        enabledAttributesPerStateAndType.put(DObPaymentRequestStateMachine.PAYMENT_DONE_STATE, new HashSet<>());

        Set<String> partyEnabledAttributes = new HashSet<>();
        partyEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT));
        partyEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION));

        Set<String> generalEnabledAttributes = new HashSet<>();
        generalEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT));
        generalEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE));
        generalEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION));
        generalEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.COST_FACTOR_ITEM_CODE));

        Set<String> underSettlementEnabledAttributes = new HashSet<>();
      underSettlementEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT));
      underSettlementEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE));
      underSettlementEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION));

      Set<String> basedOnAccountingNoteEnabledAttributes = new HashSet<>();
    basedOnAccountingNoteEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT));
    basedOnAccountingNoteEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_CODE));
    basedOnAccountingNoteEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION));

        enabledAttributesPerStateAndType.put(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.toString(),generalEnabledAttributes);
        enabledAttributesPerStateAndType.put(PaymentTypeEnum.PaymentType.VENDOR.toString(),partyEnabledAttributes);
        enabledAttributesPerStateAndType.put(PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT.toString(),underSettlementEnabledAttributes);
        enabledAttributesPerStateAndType.put(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.toString(),basedOnAccountingNoteEnabledAttributes);

        Set<String> cashEnabledAttributes = new HashSet<>();
        cashEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE));

        Set<String> bankEnabledAttributes = new HashSet<>();
        bankEnabledAttributes.addAll(Arrays.asList(IIObPaymentRequestPaymentDetailsGeneralModel.COMPANY_BANK_ID));

        enabledAttributesPerStateAndType.put(PaymentFormEnum.PaymentForm.CASH.toString(), cashEnabledAttributes);
        enabledAttributesPerStateAndType.put(PaymentFormEnum.PaymentForm.BANK.toString(), bankEnabledAttributes);
        return enabledAttributesPerStateAndType;
    }
}
