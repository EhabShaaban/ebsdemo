package com.ebs.dda.accounting.costing;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;

import java.util.HashSet;
import java.util.Set;

public class DObCostingStateMachine extends AbstractStateMachine {

  public static final String ACTIVE_STATE = "Active";
  public static final String DRAFT_STATE = "Draft";


  private Set<String> objectStates;
  private boolean initialized;

  private DObCosting object;
  private DObCostingGeneralModel objectGeneralModel;

  public DObCostingStateMachine() throws Exception {
    super(DObCostingStateMachine.class.getClassLoader().getResource("META-INF/DObCosting.scxml"));
  }

  public boolean goToDraft() {
    return true;
  }

  public boolean goToActive() {
    return true;
  }

  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObCosting) {
      this.object = (DObCosting) obj;
    } else {
      this.objectGeneralModel = (DObCostingGeneralModel) obj;
    }
  }

  protected Long getEntityId() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return this.object == null ? this.objectGeneralModel.getId() : this.object.getId();
  }

  public void isAllowedAction(String actionName) throws Exception {
    Set<String> currentStates = getEntity().getCurrentStates();
    if (currentStates == null) {
      throw new IllegalArgumentException("Object current states must not be null");
    }
    if (actionName == null || actionName.trim().isEmpty()) {
      throw new IllegalArgumentException("Action name must not be null or empty");
    }
    boolean canFireAction = false;
    for (String state : currentStates) {
      canFireAction = canFireEvent(actionName);
      if (canFireAction) {
        break;
      }
    }
    if (!canFireAction) {
      throw new ActionNotAllowedPerStateException(
          getEntity().getClass(), getEntityId(), getEntity().getCurrentState(), actionName);
    }
  }
}
