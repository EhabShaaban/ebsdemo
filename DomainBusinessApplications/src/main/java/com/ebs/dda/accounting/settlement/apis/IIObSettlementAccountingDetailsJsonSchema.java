package com.ebs.dda.accounting.settlement.apis;

public interface IIObSettlementAccountingDetailsJsonSchema {
  String SAVE_ACCOUNTING_DETAILS_SCHEMA =
          "{\n  \"properties\": {\n    \"accountType\": {\n      \"type\": [\n        \"string\"\n      ],\n      \"enum\": [\n        \"CREDIT\",\n        \"DEBIT\"\n      ]\n    },\n    \"glAccountCode\": {\n      \"type\": [\n        \"string\"\n      ],\n      \"minLength\": 5,\n      \"maxLength\": 5,\n      \"pattern\": \"^([0-9])*$\"\n    },\n    \"glSubAccountCode\": {\n      \"type\": [\n        \"string\",\n        \"null\"\n      ]\n    },\n    \"amount\": {\n      \"type\": [\n        \"number\"\n      ],\n      \"maximum\": 1000000000,\n      \"minimum\": 0,\n      \"multipleOf\": 0.000001\n    }\n  }\n}";
}
