package com.ebs.dda.accounting.electronicinvoice.types;

import lombok.Builder;

@Builder
public class EInvoicePartner {

    private String id;
    private String name;
    private String type;
    private EInvoicePartnerAddress address;

}
