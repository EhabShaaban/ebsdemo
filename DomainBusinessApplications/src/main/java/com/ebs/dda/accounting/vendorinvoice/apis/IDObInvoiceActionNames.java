package com.ebs.dda.accounting.vendorinvoice.apis;

public interface IDObInvoiceActionNames {

  String READ_GENERAL_DATA_SECTION = "ReadGeneralData";
  String READ_COMPANY_DATA_SECTION = "ReadCompany";
  String READ_ITEMS_SECTION = "ReadItems";
  String READ_Summary_SECTION = "ReadSummary";
  String UPDATE_ITEMS = "UpdateItems";
  String UPDATE_TAXES = "UpdateTaxes";
  String READ_INVOICE_DETAILS = "ReadInvoiceDetail";
  String EDIT_INVOICE_DETAILS = "UpdateInvoiceDetails";
  String POST_INVOICE = "Post";
  String CREATE_JOURNAL_ENTRY_INVOICE = "CreateJournalEntry";

  String READ_TAXES_SECTION = "ReadTaxes";
  String READ_POSTING_DETAILS = "ReadPostingDetail";
}


