package com.ebs.dda.accounting.landedcost.validation;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceipt;
import com.ebs.dda.jpa.inventory.inventorydocument.IDObInventoryDocument;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class DObLandedCostJournalEntryCreationValidator {

  private ValidationResult validationResult;
  private MandatoryValidatorUtils validatorUtils;

  @Autowired private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
  @Autowired private DObGoodsReceiptGeneralModelRep goodsReceiptPurchaseOrderRep;


  private final String PO_GR="GR_PO";

  public DObLandedCostJournalEntryCreationValidator() {
    this.validatorUtils = new MandatoryValidatorUtils();
  }

  public ValidationResult validate(String landedCostCode) throws Exception {
    validationResult = new ValidationResult();
    checkThatLandedCostPOHasActiveGoodsReceipt(landedCostCode);
    return validationResult;
  }

  private void checkThatLandedCostPOHasActiveGoodsReceipt(String landedCostCode) throws Exception {
    IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
        landedCostDetailsGeneralModelRep.findOneByLandedCostCode(landedCostCode);
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptPurchaseOrderRep.findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
            landedCostDetailsGeneralModel.getPurchaseOrderCode(), PO_GR, "%"+ DObGoodsReceiptStateMachine.ACTIVE_STATE +"%");
    if (goodsReceiptGeneralModel == null) {
      return;
    }
  }
}
