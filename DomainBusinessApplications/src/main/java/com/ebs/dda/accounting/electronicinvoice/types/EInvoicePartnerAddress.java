package com.ebs.dda.accounting.electronicinvoice.types;

import lombok.Builder;

@Builder
public class EInvoicePartnerAddress {
    private String branchId;
    private String country;
    private String governate;
    private String regionCity;
    private String street;
    private String buildingNumber;
    private String postalCode;
    private String floor;
    private String room;
    private String landmark;
    private String additionalInformation;
}
