package com.ebs.dda.accounting.electronicinvoice.types;

import lombok.Builder;

import java.math.BigDecimal;

@Builder
public class EInvoiceTax {

    private String taxType;
    private BigDecimal amount;
}
