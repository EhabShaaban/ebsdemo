package com.ebs.dda.accounting.paymentrequest.validation;

import com.ebs.dda.accounting.paymentrequest.validation.apis.IPaymentRequestPaymentDetailsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestPaymentDetailsMandatoryValidator
    implements ISectionMandatoryValidator {
  private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;

  public DObPaymentRequestPaymentDetailsMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  @Override
  public void validateValueObjectMandatories(Object o) {}

  @Override
  public List<String> validateGeneralModelMandatories(String paymentRequestCode, String state) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);

    IObPaymentRequestPaymentDetailsGeneralModel paymentDetailsGeneralModel =
        paymentDetailsGeneralModelRep.findOneByUserCode(paymentRequestCode);

    Map<String, String[]> mandatories =
        IPaymentRequestPaymentDetailsMandatoryAttributes.MANDATORIES;

    String[] stateMandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(mandatories, state);

    String[] paymentTypeMandatoryAttributes = validatorUtils
        .getMandatoryAttributesByKey(mandatories, paymentRequestGeneralModel.getPaymentType());

    String[] paymentFormMandatoryAttributes = validatorUtils
        .getMandatoryAttributesByKey(mandatories, paymentRequestGeneralModel.getPaymentForm());

    String[] mandatoryAttributes = validatorUtils.getInterSectionBetweenTwoMandatoryArrays(
        stateMandatoryAttributes, paymentTypeMandatoryAttributes);

    mandatoryAttributes = validatorUtils.getInterSectionBetweenTwoMandatoryArrays(
        mandatoryAttributes, paymentFormMandatoryAttributes);

    return validatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes,
        paymentDetailsGeneralModel);
  }

  public void setPaymentRequestGeneralModelRep(
      DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep) {
    this.paymentRequestGeneralModelRep = paymentRequestGeneralModelRep;
  }

  public void setPaymentDetailsGeneralModelRep(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep) {
    this.paymentDetailsGeneralModelRep = paymentDetailsGeneralModelRep;
  }
}
