package com.ebs.dda.masterdata.item;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItemCreateValueObject;
import com.ebs.dda.jpa.masterdata.item.IObItemPurchaseUnitValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItemCreationValueObject;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;

import java.util.List;

public class MObItemCreationValidator {
  private ValidationResult validationResult;
  private CObMaterialGroupGeneralModelRep materialGroupGeneralModelRep;
  private CObMeasureRep measureRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";
  private String CONSUMABLE_ITEM_TYPE = "CONSUMABLE";
  private String SERVICE_ITEM_TYPE = "SERVICE";

  public ValidationResult validate(MObItemCreationValueObject creationValueObject)
      throws ArgumentViolationSecurityException {
    validationResult = new ValidationResult();

    if (creationValueObject.getItemType().equals(COMMERCIAL_ITEM_TYPE)) {
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getBaseUnitCode());
      checkIfValueNotExistForMandatoryBooleanAttr(creationValueObject.getBatchManaged());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemGroupCode());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemName());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getMarketName());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemType());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemOldNumber());
      checkIfValueNotExistForMandatoryListAttr(creationValueObject.getPurchaseUnits());

      validateItemGroup(creationValueObject);
      validateBaseUnit(creationValueObject);
      validatePurchaseUnits(creationValueObject);
      validateItemType(creationValueObject);
    } else {
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getBaseUnitCode());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemName());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getMarketName());
      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemType());
      checkIfValueExistsForBooleanAttr(creationValueObject.getBatchManaged());
      checkIfValueExistsForStringAttr(creationValueObject.getItemGroupCode());

      checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getItemOldNumber());
      checkIfValueNotExistForMandatoryListAttr(creationValueObject.getPurchaseUnits());

      validateBaseUnit(creationValueObject);
      validatePurchaseUnits(creationValueObject);
      validateItemType(creationValueObject);
    }
    return validationResult;
  }

  private void validateItemType(MObItemCreationValueObject creationValueObject) {

    if (!(creationValueObject.getItemType().equals(COMMERCIAL_ITEM_TYPE)
        || creationValueObject.getItemType().equals(SERVICE_ITEM_TYPE)
        || creationValueObject.getItemType().equals(CONSUMABLE_ITEM_TYPE))) {
      validationResult.bindError(
          IMObItemCreateValueObject.ITEM_TYPE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfValueExistsForStringAttr(String itemGroupCode)
      throws ArgumentViolationSecurityException {
    if (!(itemGroupCode == null)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValueExistsForBooleanAttr(Boolean batchManaged)
      throws ArgumentViolationSecurityException {
    if (!(batchManaged == null)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validatePurchaseUnits(MObItemCreationValueObject creationValueObject)
      throws ArgumentViolationSecurityException {
    List<IObItemPurchaseUnitValueObject> purchaseUnits = creationValueObject.getPurchaseUnits();
    for (IObItemPurchaseUnitValueObject purchaseUnitValueObject : purchaseUnits) {

      Boolean isExistingPurchaseUnit =
          checkIfPurchaseUnitExist(purchaseUnitValueObject.getPurchaseUnitCode());
      if (!isExistingPurchaseUnit) {
        validationResult.bindError(
            IMObItemCreateValueObject.PURCHASE_UNITS,
            IExceptionsCodes.ITEM_PURCHASE_UNITS_NOT_EXIST);
        break;
      }
    }
  }

  private Boolean checkIfPurchaseUnitExist(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    return purchasingUnitGeneralModel != null;
  }

  private void validateBaseUnit(MObItemCreationValueObject creationValueObject)
      throws ArgumentViolationSecurityException {
    String baseUnitCode = creationValueObject.getBaseUnitCode();
    checkIfBaseUnitExist(baseUnitCode);
  }

  private void checkIfBaseUnitExist(String baseUnitCode) {
    CObMeasure measure = measureRep.findOneByUserCode(baseUnitCode);
    if (measure == null) {
      validationResult.bindError(
          IMObItemCreateValueObject.BASE_UNIT_CODE, IExceptionsCodes.ITEM_BASE_UNIT_NOT_EXIST);
    }
  }

  private void checkIfValueNotExistForMandatoryStringAttr(String value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValueNotExistForMandatoryBooleanAttr(Boolean value)
      throws ArgumentViolationSecurityException {
    if (value == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValueNotExistForMandatoryListAttr(List<IObItemPurchaseUnitValueObject> value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateItemGroup(MObItemCreationValueObject creationValueObject)
      throws ArgumentViolationSecurityException {
    String itemGroupCode = creationValueObject.getItemGroupCode();

    CObMaterialGroupGeneralModel materialGroupGeneralModel = checkIfItemGroupExist(itemGroupCode);
    if (materialGroupGeneralModel != null) {
      checkIfItemGroupIsLeaf(materialGroupGeneralModel);
    }
  }

  private void checkIfItemGroupIsLeaf(CObMaterialGroupGeneralModel materialGroupGeneralModel)
      throws ArgumentViolationSecurityException {
    if (!materialGroupGeneralModel.getLevel().getId().toString().equals("LEAF")) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private CObMaterialGroupGeneralModel checkIfItemGroupExist(String itemGroupCode) {
    CObMaterialGroupGeneralModel materialGroupGeneralModel;
    materialGroupGeneralModel = materialGroupGeneralModelRep.findOneByUserCode(itemGroupCode);
    if (null == materialGroupGeneralModel) {
      validationResult.bindError(
          IMObItemCreateValueObject.ITEM_GROUP_CODE, IExceptionsCodes.ITEM_ITEM_GROUP_NOT_EXIST);
    }
    return materialGroupGeneralModel;
  }

  public void setMaterialGroupGeneralModelRep(
      CObMaterialGroupGeneralModelRep materialGroupGeneralModelRep) {
    this.materialGroupGeneralModelRep = materialGroupGeneralModelRep;
  }

  public void setMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }
}
