package com.ebs.dda.masterdata.unitofmeasure;

public interface CObMeasureJsonSchema {
  String CREATE =
      "{\n"
          + "  \"properties\": {\n"
          + "    \"unitOfMeasureName\": {\n"
          + "      \"type\": \"string\",\n"
          + "      \"minLength\": 1,\n"
          + "      \"maxLength\": 50\n"
          + "    },\n"
          + "    \"isStandard\": {\n"
          + "      \"type\": \"boolean\"\n"
          + "    }\n"
          + "  }\n"
          + "}";
}
