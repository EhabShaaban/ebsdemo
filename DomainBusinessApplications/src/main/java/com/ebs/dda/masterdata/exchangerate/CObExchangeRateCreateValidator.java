package com.ebs.dda.masterdata.exchangerate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTime;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateValueObject;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateType;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;

public class CObExchangeRateCreateValidator {
  private ValidationResult validationResult;
  private CObCurrencyRep cObCurrencyRep;
  private CObExchangeRateGeneralModelRep cObExchangeRateRep;

  public ValidationResult validate(CObExchangeRateValueObject cObExchangeRateValueObject)
      throws Exception {
    validationResult = new ValidationResult();
    validateCurrency(cObExchangeRateValueObject);
    validatePrice(cObExchangeRateValueObject);
    validateERDate(cObExchangeRateValueObject);
    return validationResult;
  }

  private void validateERDate(CObExchangeRateValueObject cObExchangeRateValueObject)
      throws Exception {
    CObCurrency cObCurrency =
        cObCurrencyRep.findOneByUserCode(cObExchangeRateValueObject.getFirstCurrencyCode());
    if (cObCurrency != null) {
      CObExchangeRateGeneralModel cObExchangeRate =
          cObExchangeRateRep.findTopByFirstCurrencyIdAndExchangeRateTypeOrderByCreationDateDesc(
              cObCurrency.getId(), ICObExchangeRateType.USER_DAILY_RATE);
      if (cObExchangeRate != null && isDateEqualsToday(cObExchangeRate.getValidFrom())) {
        throw new CObExchangeRateExistsWithTheSameDateAndCurrency();
      }
    }
  }

  private boolean isDateEqualsToday(DateTime dateTime) {
    DateTime today = new DateTime();
    return today.toLocalDate().compareTo(dateTime.toLocalDate()) == 0;
  }

  private String formateDate(Date date) {
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    return formatter.format(date);
  }

  private void validatePrice(CObExchangeRateValueObject cObExchangeRateValueObject)
      throws Exception {
    checkIfValueDoesNotExistForMandatoryAttr(cObExchangeRateValueObject.getSecondValue());
  }

  private void validateCurrency(CObExchangeRateValueObject cObExchangeRateValueObject)
      throws ArgumentViolationSecurityException {
    checkIfValueDoesNotExistForMandatoryAttr(cObExchangeRateValueObject.getFirstCurrencyCode());
    checkIfCurrencyIsEgp(cObExchangeRateValueObject.getFirstCurrencyCode());
    checkIfCurrencyDoesNotExist(cObExchangeRateValueObject.getFirstCurrencyCode());
  }

  private void checkIfCurrencyDoesNotExist(String currencyCode) {
    CObCurrency cObCurrency = cObCurrencyRep.findOneByUserCode(currencyCode);
    if (cObCurrency == null) {
      validationResult.bindError(ICObExchangeRateValueObject.FIRST_CURRENCY,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfCurrencyIsEgp(String currencyCode) throws ArgumentViolationSecurityException {
    CObCurrency egpCurrency = cObCurrencyRep.findOneByIso("EGP");
    if (currencyCode.equals(egpCurrency.getUserCode())) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfValueDoesNotExistForMandatoryAttr(Object value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.toString().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setcObCurrencyRep(CObCurrencyRep cObCurrencyRep) {
    this.cObCurrencyRep = cObCurrencyRep;
  }

  public void setcObExchangeRateRep(CObExchangeRateGeneralModelRep cObExchangeRateRep) {
    this.cObExchangeRateRep = cObExchangeRateRep;
  }
}
