package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class CObExchangeRateExistsWithTheSameDateAndCurrency extends Exception
        implements IExceptionResponse {
  @Override
  public String getResponseCode() {
    return IExceptionsCodes.EXCHANGE_RATE_EXISTS_WITH_THE_SAME_DATE_AND_CURRENCY;
  }
}
