package com.ebs.dda.masterdata.item;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItemAccountingInfoValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItemAccountingInfoValueObject;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;

public class MObItemAccountingDetailsValidator {
    public static final String LEAF_LEVEL = "3";
    public static final String IN_ACTIVE_STATE = "InActive";
    private ValidationResult validationResult;
    private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;

    public void setChartOfAccountsGeneralModelRep(HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
        this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
    }

    public ValidationResult validate(MObItemAccountingInfoValueObject valueObject) throws Exception {

        validationResult = new ValidationResult();

        checkIfValueNotExistForMandatoryAttr(valueObject.getAccountCode());

        validateAccount(valueObject);
        return validationResult;
    }

    private void checkIfValueNotExistForMandatoryAttr(String textValue)
            throws ArgumentViolationSecurityException {
        String value = textValue.trim();
        if (value == null || value.isEmpty()) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void validateAccount(MObItemAccountingInfoValueObject valueObject)
            throws ArgumentViolationSecurityException {
        String accountCode = valueObject.getAccountCode();
        try {
            Long.parseLong(accountCode);
        } catch (Exception ex) {
            throw new ArgumentViolationSecurityException();
        }
        checkAccountIsValid(valueObject);
    }

    private void checkAccountIsValid(MObItemAccountingInfoValueObject valueObject) throws ArgumentViolationSecurityException {

        HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel = chartOfAccountsGeneralModelRep.findOneByUserCode(valueObject.getAccountCode());

        if (chartOfAccountsGeneralModel == null) {
            validationResult.bindError(
                    IMObItemAccountingInfoValueObject.ACCOUNT_CODE,
                    IExceptionsCodes.General_MISSING_FIELD_INPUT);
        } else
            validateAccountIsLeafAndActive(valueObject);
    }


    private void validateAccountIsLeafAndActive(MObItemAccountingInfoValueObject valueObject)
            throws ArgumentViolationSecurityException {
        String accountCode = valueObject.getAccountCode();

        HObChartOfAccountsGeneralModel hObLeafGLAccountsGeneralModel =
                chartOfAccountsGeneralModelRep.findByCurrentStateNotContainsAndLevelAndCode(IN_ACTIVE_STATE, LEAF_LEVEL, accountCode);

        if (hObLeafGLAccountsGeneralModel == null)
            throw new ArgumentViolationSecurityException();
    }

}
