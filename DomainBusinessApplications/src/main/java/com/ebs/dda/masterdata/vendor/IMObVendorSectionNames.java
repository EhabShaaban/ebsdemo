package com.ebs.dda.masterdata.vendor;

public interface IMObVendorSectionNames {
    String GENERAL_DATA_SECTION = "GeneralData";
    String ACCOUNTING_DETAILS_SECTION = "AccountingDetails";
}
