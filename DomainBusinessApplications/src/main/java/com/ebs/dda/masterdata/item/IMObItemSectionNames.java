package com.ebs.dda.masterdata.item;

public interface IMObItemSectionNames {
  String GENERAL_DATA_SECTION = "GeneralData";
  String UOM_SECTION = "UnitOfMeasures";
  String ACCOUNTING_DETAILS_SECTION = "AccountingDetails";
}
