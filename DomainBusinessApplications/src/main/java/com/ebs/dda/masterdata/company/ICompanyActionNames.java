package com.ebs.dda.masterdata.company;

public interface ICompanyActionNames {
  String READ_TAXES = "ReadTaxes";
}
