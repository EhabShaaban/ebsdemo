package com.ebs.dda.masterdata.vendor;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorCreationValueObject;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorPurchaseUnitValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorCreationValueObject;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;

import java.util.List;

/**
 * @author Niveen Magdy
 * @since Oct 29, 2018
 */
public class MObVendorCreationValidator {

  private ValidationResult validationResult;
  private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private MObVendorRep vendorRep;
  private String COMMERCIAL_VENDOR_TYPE = "COMMERCIAL";
  private String CONSUMABLE_VENDOR_TYPE = "CONSUMABLE";
  private String SERVICE_VENDOR_TYPE = "SERVICE";
  private String FIXED_ASSETS_VENDOR_TYPE = "FIXED_ASSETS";

  public void validate(MObVendorCreationValueObject creationValueObject)
      throws ArgumentViolationSecurityException {

    checkIfValueNotExistForMandatoryAttr(creationValueObject.getVendorType());
    checkIfValueNotExistForMandatoryAttr(creationValueObject.getVendorName());
    checkIfValueNotExistForMandatoryAttr(creationValueObject.getOldVendorReference());
    checkIfPurchaseUnitValuesNotExistForMandatoryAttr(creationValueObject.getPurchaseUnits());

    if (creationValueObject.getVendorType().equals(COMMERCIAL_VENDOR_TYPE)) {
      checkIfValueNotExistForMandatoryAttr(creationValueObject.getPurchaseResponsibleCode());
    } else {
      checkIfValueExistsForStringAttr(creationValueObject.getPurchaseResponsibleCode());
    }
  }

  public ValidationResult validateOnBusiness(MObVendorCreationValueObject valueObject) {
    validationResult = new ValidationResult();

    validatePurchasingUnits(valueObject);

    validateVendorType(valueObject);

    if (valueObject.getVendorType().equals(COMMERCIAL_VENDOR_TYPE)) {
      validatePurchasingResponsible(valueObject);
    }

    checkThatOldVendorNumberIsUnique(valueObject.getOldVendorReference());
    return validationResult;
  }

  private void checkThatOldVendorNumberIsUnique(String oldVendorNumber) {
    MObVendor vendor = vendorRep.findOneByOldVendorNumber(oldVendorNumber);
    if (vendor != null) {
      validationResult.bindError(
          IMObVendorCreationValueObject.OLD_VENDOR_REFERENCE,
          IExceptionsCodes.VENDOR_OLD_NUMBER_EXIST);
    }
  }

  private void validateVendorType(MObVendorCreationValueObject creationValueObject) {
    if (!(creationValueObject.getVendorType().equals(COMMERCIAL_VENDOR_TYPE)
        || creationValueObject.getVendorType().equals(SERVICE_VENDOR_TYPE)
        || creationValueObject.getVendorType().equals(CONSUMABLE_VENDOR_TYPE)
        || creationValueObject.getVendorType().equals(FIXED_ASSETS_VENDOR_TYPE))) {
      validationResult.bindError(
          IMObVendorCreationValueObject.VENDOR_TYPE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfValueNotExistForMandatoryAttr(String value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfPurchaseUnitValuesNotExistForMandatoryAttr(
      List<IObVendorPurchaseUnitValueObject> purchaseUnits)
      throws ArgumentViolationSecurityException {
    if (purchaseUnits == null || purchaseUnits.size() == 0) {
      throw new ArgumentViolationSecurityException();
    }
  }

  // =====================================PurchasingResponsible======================================================
  private void validatePurchasingResponsible(MObVendorCreationValueObject creationValueObject) {
    checkIfPurchaseResponsibleExists(creationValueObject);
  }

  private void checkIfPurchaseResponsibleExists(MObVendorCreationValueObject creationValueObject) {
    CObPurchasingResponsibleGeneralModel purchasingResponsibleGeneralModel =
        purchasingResponsibleGeneralModelRep.findOneByUserCode(
            creationValueObject.getPurchaseResponsibleCode());

    if (purchasingResponsibleGeneralModel == null) {
      validationResult.bindError(
          IMObVendorCreationValueObject.PURCHASE_RESPONSIBLE_CODE,
          IExceptionsCodes.VENDOR_PURCHASING_RESPONSIBLE_NOT_EXIST);
    }
  }

  private void checkIfValueExistsForStringAttr(String purchaseResponsibleCode)
      throws ArgumentViolationSecurityException {
    if (!(purchaseResponsibleCode == null)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validatePurchasingUnits(MObVendorCreationValueObject creationValueObject) {
    List<IObVendorPurchaseUnitValueObject> purchaseUnits = creationValueObject.getPurchaseUnits();

    for (IObVendorPurchaseUnitValueObject purchaseUnit : purchaseUnits) {
      checkIfPurchasingUnitExists(purchaseUnit.getPurchaseUnitCode());
    }
  }

  private void checkIfPurchasingUnitExists(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);

    if (null == purchasingUnitGeneralModel) {
      validationResult.bindError(
          IMObVendorCreationValueObject.PURCHASE_UNIT_CODE,
          IExceptionsCodes.VENDOR_PURCHASING_UNIT_NOT_EXIST);
    }
  }

  public void setPurchasingResponsibleGeneralModelRep(
      CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep) {
    this.purchasingResponsibleGeneralModelRep = purchasingResponsibleGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }
}
