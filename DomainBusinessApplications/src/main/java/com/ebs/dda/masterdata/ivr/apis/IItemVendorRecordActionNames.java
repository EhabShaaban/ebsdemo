package com.ebs.dda.masterdata.ivr.apis;

public interface IItemVendorRecordActionNames {

  String VIEW_GENERAL_DATA = "ReadGeneralData";
  String EDIT_GENERAL_DATA = "UpdateGeneralData";
}
