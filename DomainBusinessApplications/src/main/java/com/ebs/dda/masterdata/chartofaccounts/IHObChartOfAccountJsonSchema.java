package com.ebs.dda.masterdata.chartofaccounts;

public interface IHObChartOfAccountJsonSchema {

    String CREATION_SCHEMA = "{\n" +
            "  \"properties\": {\n" +
            "    \"accountName\": {\n" +
            "      \"type\": [\n" +
            "        \"string\"\n" +
            "      ],\n" +
            "      \"pattern\": \"^([\\u0621-\\u064A\\u0660-\\u0669\\uFE70-\\uFEFF]|[ A-Za-z0-9_\\/().%,*-])*$\",\n" +
            "      \"maxLength\": 100\n" +
            "    },\n" +
            "    \"level\": {\n" +
            "      \"type\": \"string\",\n" +
            "      \"enum\": [\n" +
            "        \"1\",\n" +
            "        \"2\",\n" +
            "        \"3\"\n" +
            "      ]\n" +
            "    },\n" +
            "    \"oldAccountCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\"\n" +
            "      ],\n" +
            "      \"minLength\": 1,\n" +
            "      \"maxLength\": 50,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"parentCode\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"minLength\": 1,\n" +
            "      \"maxLength\": 6,\n" +
            "      \"pattern\": \"^([0-9])*$\"\n" +
            "    },\n" +
            "    \"accountType\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"enum\": [\n" +
            "        \"TYPE_01\",\n" +
            "        \"TYPE_11\",\n" +
            "        \"TYPE_02\",\n" +
            "        \"TYPE_03\",\n" +
            "        \"TYPE_04\",\n" +
            "        null\n" +
            "      ]\n" +
            "    },\n" +
            "    \"creditDebit\": {\n" +
            "      \"type\": \"string\",\n" +
            "      \"enum\": [\n" +
            "        \"CREDIT\",\n" +
            "        \"DEBIT\",\n" +
            "        \"null\"\n" +
            "      ]\n" +
            "    },\n" +
            "    \"mappedAccount\": {\n" +
            "      \"type\": [\n" +
            "        \"string\",\n" +
            "        \"null\"\n" +
            "      ],\n" +
            "      \"enum\": [\n" +
            "        \"CashGLAccount\",\n" +
            "        \"BankGLAccount\",\n" +
            "        \"SalesGLAccount\",\n" +
            "        \"RealizedGLAccount\",\n" +
            "        \"UnrealizedCurrencyGainLossGLAccount\",\n" +
            "        \"CustomersGLAccount\",\n" +
            "        \"TaxesGLAccount\",\n" +
            "        \"ImportPurchasingGLAccount\",\n" +
            "        \"LocalPurchasingGLAccount\",\n" +
            "        \"PurchasingGLAccount\",\n" +
            "        \"PurchaseOrderGLAccount\",\n" +
            "        \"NotesReceivablesGLAccount\",\n" +
            "        \"TreasuryGLAccount\",\n" +
            "        null\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            ", \"required\": [\"accountName\", \"level\", \"oldAccountCode\"]\n"
            +
            "}\n";
}
