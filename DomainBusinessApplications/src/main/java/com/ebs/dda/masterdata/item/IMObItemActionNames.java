package com.ebs.dda.masterdata.item;

public interface IMObItemActionNames {
  String READ_ITEM_GENERAL_DATA = "ReadGeneralData";
  String VIEW_UNIT_OF_MEASURE_SECTION = "ReadUoMData";
  String EDIT_UOM_SECTION = "UpdateUoMData";
  String EDIT_ACCOUNTING_DETAILS_SECTION = "UpdateAccountingDetails";
  String READ_TYPES = "ReadTypes";
}
