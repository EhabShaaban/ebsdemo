package com.ebs.dda.masterdata.customer;

public interface ICustomerActionNames {
  String READ_TAX_ADMINISTRATION = "ReadTaxAdministration";
  String READ_ISSUE_FROM = "ReadIssueFrom";
}
