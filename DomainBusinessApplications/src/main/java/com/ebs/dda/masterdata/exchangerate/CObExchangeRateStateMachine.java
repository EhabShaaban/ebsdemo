package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;

import java.util.HashSet;
import java.util.Set;

public class CObExchangeRateStateMachine extends AbstractStateMachine {

    private Set<String> objectStates;
    private boolean initialized;

    private CObExchangeRate object;
    private CObExchangeRateGeneralModel objectGeneralModel;

    public CObExchangeRateStateMachine() throws Exception {
        super(
                CObExchangeRateStateMachine.class
                        .getClassLoader()
                        .getResource("META-INF/CObExchangeRate.scxml"));

    }

    public boolean goToActive() {
        return true;
    }

    @Override
    public boolean resetMachine() {
        return false;
    }

    @Override
    protected <T extends IStatefullBusinessObject> T getEntity() {
        if (this.object == null && this.objectGeneralModel == null) {
            throw new IllegalArgumentException("Object is NULL");
        }
        return (T) (this.object == null ? this.objectGeneralModel : this.object);
    }

    protected void setEntity(IStatefullBusinessObject obj) {
        if (obj instanceof CObExchangeRate) {
            this.object = (CObExchangeRate) obj;
        } else {
            this.objectGeneralModel = (CObExchangeRateGeneralModel) obj;
        }
    }

    @Override
    public void initObjectState(IStatefullBusinessObject obj) throws Exception {
        if (obj == null) {
            throw new IllegalArgumentException("The object must have current state(s)");
        }
        if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
            Set<String> currentStates = new HashSet<String>();
            String initialState = getEngine().getStateMachine().getInitial();
            currentStates.add(initialState);
            obj.setCurrentStates(currentStates);
        }
        objectStates = new HashSet<String>(obj.getCurrentStates());
        this.getEngine().setConfiguration(obj.getCurrentStates());
        setEntity(obj);
        initialized = true;

    }
}
