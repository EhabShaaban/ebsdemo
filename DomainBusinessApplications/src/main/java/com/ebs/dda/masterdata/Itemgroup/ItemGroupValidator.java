package com.ebs.dda.masterdata.Itemgroup;

import com.ebs.dac.foundation.exceptions.data.RootOrHierarchyHasChildException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;
import com.ebs.dda.jpa.masterdata.itemgroup.HObItemGroupValueObject;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;

import java.util.List;

public class ItemGroupValidator {

    private ValidationResult validationResult;
    private CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep;

    public ValidationResult validateCreation(HObItemGroupValueObject creationValueObject)
            throws ArgumentViolationSecurityException {
        validationResult = new ValidationResult();
        checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getName());
        checkIfValueNotExistForMandatoryStringAttr(creationValueObject.getType());
        checkIfLevelIsRootAndHasParent(
                creationValueObject.getType(), creationValueObject.getParentCode());
        if (!creationValueObject
                .getType()
                .equals(HierarchyLevelEnumType.HierarchyLevelEnum.ROOT.toString())) {
            checkIfParentValueExistForMandatoryListAttr(creationValueObject.getParentCode());
        }
        return validationResult;
    }

    public void validateDeletion(String itemGroupCode)
            throws RootOrHierarchyHasChildException {
        checkIfItemGroupHasChilderen(itemGroupCode);
    }

    private void checkIfValueNotExistForMandatoryStringAttr(String value)
            throws ArgumentViolationSecurityException {
        if (value == null || value.isEmpty()) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkIfParentValueExistForMandatoryListAttr(String parentCode)
            throws ArgumentViolationSecurityException {

        if (parentCode == null || parentCode.isEmpty()) {
            throw new ArgumentViolationSecurityException();
        }
        CObMaterialGroupGeneralModel materialGroupGeneralModel =
                itemGroupGeneralModelRep.findOneByUserCode(parentCode);
        if (materialGroupGeneralModel != null) {
            HierarchyLevelEnumType level =
                    itemGroupGeneralModelRep.findItemGroupLevel(parentCode).getLevel();
            if (materialGroupGeneralModel
                    .getLevel()
                    .getId()
                    .equals(HierarchyLevelEnumType.HierarchyLevelEnum.LEAF)) {
                throw new ArgumentViolationSecurityException();
            }
        } else {
            throw new ArgumentViolationSecurityException();
        }
    }

    public void setItemGroupGeneralModelRep(
            CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep) {
        this.itemGroupGeneralModelRep = itemGroupGeneralModelRep;
    }

    public void checkIfLevelIsRootAndHasParent(String level, String parentCode)
            throws ArgumentViolationSecurityException {
        if (parentCode != null && level.equals(HierarchyLevelEnumType.HierarchyLevelEnum.ROOT)) {
            throw new ArgumentViolationSecurityException();
        }
    }

    public void checkIfItemGroupHasChilderen(String itemGroupCode)
            throws RootOrHierarchyHasChildException {
        validationResult = new ValidationResult();
        List<CObMaterialGroupGeneralModel> parentGroups =
                itemGroupGeneralModelRep.findByParentCode(itemGroupCode);
        if (!parentGroups.isEmpty()) {
            throw new RootOrHierarchyHasChildException();
        }
    }
}
