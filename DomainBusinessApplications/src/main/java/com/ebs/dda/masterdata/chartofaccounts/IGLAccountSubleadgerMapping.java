package com.ebs.dda.masterdata.chartofaccounts;

import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;

import java.util.HashMap;
import java.util.Map;

public interface IGLAccountSubleadgerMapping {

	Map<String, String> subLeadgerMap = new HashMap<String, String>() {
		{
			put(ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT, SubLedgers.Treasuries.name());
			put(ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT, SubLedgers.Banks.name());
			put(ILObGlobalGLAccountConfig.CUSTOMERS_GL_ACCOUNT, SubLedgers.Local_Customer.name());
			put(ILObGlobalGLAccountConfig.TAXES_GL_ACCOUNT, SubLedgers.Taxes.name());
			put(ILObGlobalGLAccountConfig.IMPORT_PURCHASING_GL_ACCOUNT, SubLedgers.PO.name());
			put(ILObGlobalGLAccountConfig.LOCAL_PURCHASING_GL_ACCOUNT, SubLedgers.PO.name());
			put(ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT, SubLedgers.NONE.name());
			put(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT, SubLedgers.PO.name());
			put(ILObGlobalGLAccountConfig.NOTES_RECEIVABLES_GL_ACCOUNT,
							SubLedgers.Notes_Receivables.name());
			put(ILObGlobalGLAccountConfig.TREASURY_GL_ACCOUNT, SubLedgers.Treasuries.name());
			put(ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT, SubLedgers.NONE.name());
			put(ILObGlobalGLAccountConfig.UNREALIZED_ACCOUNT, SubLedgers.NONE.name());
			put(ILObGlobalGLAccountConfig.SALES_GL_ACCOUNT, SubLedgers.NONE.name());
		}
	};
}
