package com.ebs.dda.masterdata.vendor;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorCreationValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MObVendorGeneralDataValidator {
  private static final String VENDOR_TYPE_COMMERCIAL = "COMMERCIAL";
  private ValidationResult validationResult;
  private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private MObVendorGeneralModelRep vendorGeneralModelRep;

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setPurchasingResponsibleGeneralModelRep(
      CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep) {
    this.purchasingResponsibleGeneralModelRep = purchasingResponsibleGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setVendorGeneralModelRep(MObVendorGeneralModelRep vendorGeneralModelRep) {
    this.vendorGeneralModelRep = vendorGeneralModelRep;
  }

  public ValidationResult validate(MObVendorGeneralDataValueObject valueObject) throws Exception {

    validationResult = new ValidationResult();
    MObVendorGeneralModel vendorGeneralModel = vendorGeneralModelRep.findOneByUserCode(valueObject.getVendorCode());

    checkIfValueNotExistForMandatoryAttr(valueObject.getVendorName());
    checkIfPurchaseUnitValuesNotExistForMandatoryAttr(valueObject.getPurchaseUnits());
    validatePurchasingUnits(valueObject);

    if (vendorGeneralModel.getTypeCode().equals(VENDOR_TYPE_COMMERCIAL)) {
      checkIfValueNotExistForMandatoryAttr(valueObject.getPurchaseResponsibleCode());
      validatePurchasingResponsible(valueObject);
    }

    return validationResult;
  }

  private void checkIfValueNotExistForMandatoryAttr(String textValue)
      throws ArgumentViolationSecurityException {
    String value = textValue.trim();
    if (value == null || value.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfPurchaseUnitValuesNotExistForMandatoryAttr(List<String> purchaseUnits)
      throws ArgumentViolationSecurityException {
    if (purchaseUnits == null || purchaseUnits.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }
  // =====================================PurchasingResponsible======================================================
  private void validatePurchasingResponsible(MObVendorGeneralDataValueObject valueObject)
      throws ArgumentViolationSecurityException {
    String purchaseResponsibleCode = valueObject.getPurchaseResponsibleCode();

    try {
      Long.parseLong(purchaseResponsibleCode);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
    checkLengthOfCode(purchaseResponsibleCode);
    checkIfPurchaseResponsibleExists(valueObject);
  }

  private void checkIfPurchaseResponsibleExists(MObVendorGeneralDataValueObject valueObject) {
    CObPurchasingResponsibleGeneralModel purchasingResponsibleGeneralModel =
        purchasingResponsibleGeneralModelRep.findOneByUserCode(
            valueObject.getPurchaseResponsibleCode());

    if (purchasingResponsibleGeneralModel == null) {
      validationResult.bindError(
          IMObVendorCreationValueObject.PURCHASE_RESPONSIBLE_CODE,
          IExceptionsCodes.VENDOR_PURCHASING_RESPONSIBLE_NOT_EXIST);
    }
  }

  // =====================================PurchasingUnit======================================================

  private void validatePurchasingUnits(MObVendorGeneralDataValueObject creationValueObject)
      throws ArgumentViolationSecurityException {
    List<String> purchaseUnits = creationValueObject.getPurchaseUnits();

    for (String purchaseUnitCode : purchaseUnits) {
      validatePurchasingUnit(purchaseUnitCode);
    }
    validatePurchaseUnitReferencedByIVRObject(creationValueObject);
  }

  private void validatePurchaseUnitReferencedByIVRObject(
      MObVendorGeneralDataValueObject vendorGeneralDataValueObject) {
    String vendorCode = vendorGeneralDataValueObject.getVendorCode();
    List<String> purchaseUnitCodes = vendorGeneralDataValueObject.getPurchaseUnits();
    List<ItemVendorRecordGeneralModel> itemVendorRecords =
        itemVendorRecordGeneralModelRep.findAllByVendorCode(vendorCode);
    Set<String> requiredPurchaseUnits = new HashSet<>();
    itemVendorRecords.stream()
        .forEach(
            itemVendorRecord -> {
              String iVRPurchaseUnitCode = itemVendorRecord.getPurchasingUnitCode();
              if (!purchaseUnitCodes.contains(iVRPurchaseUnitCode)) {
                requiredPurchaseUnits.add(iVRPurchaseUnitCode);
              }
            });
    if (!requiredPurchaseUnits.isEmpty()) {
      List<String> purchaseUnits = getRequiredPurchaseUnitNames(requiredPurchaseUnits);
      validationErrorForPurchaseUnitReferencedByIVRObject(purchaseUnits);
    }
  }

  private List<String> getRequiredPurchaseUnitNames(Set<String> purchaseUnits) {
    List<String> purchaseUnitNames = new ArrayList<>();
    for (String purchaseUnitCode : purchaseUnits) {
      String purchaseUnitName = getPurchaseUnitNameWithCode(purchaseUnitCode);
      purchaseUnitNames.add(purchaseUnitName);
    }
    return purchaseUnitNames;
  }

  private String getPurchaseUnitNameWithCode(String code) {
    CObPurchasingUnitGeneralModel purchaseUnit =
        purchasingUnitGeneralModelRep.findOneByUserCode(code);
    LocalizedString localizedPurchaseUnit = purchaseUnit.getName();
    return localizedPurchaseUnit.getValue("en");
  }

  private void validationErrorForPurchaseUnitReferencedByIVRObject(List<String> purchaseUnits) {
    validationResult.bindErrorWithValues(
        IMObVendorCreationValueObject.PURCHASE_UNIT_CODE,
        IExceptionsCodes.VENDOR_PURCHASE_UNITS_DELETION_BUT_REFERENCED_BY_IVR,
        purchaseUnits);
  }

  private void validatePurchasingUnit(String purchaseUnitCode)
      throws ArgumentViolationSecurityException {

    try {
      Long.parseLong(purchaseUnitCode);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
    checkLengthOfCode(purchaseUnitCode);
    checkifPurchasingUnitExists(purchaseUnitCode);
  }

  private void checkifPurchasingUnitExists(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);

    if (null == purchasingUnitGeneralModel) {
      validationResult.bindError(
          IMObVendorCreationValueObject.PURCHASE_UNIT_CODE,
          IExceptionsCodes.VENDOR_PURCHASING_UNIT_NOT_EXIST);
    }
  }

  private void checkLengthOfCode(String code) throws ArgumentViolationSecurityException {
    if (code.length() != 4) {
      throw new ArgumentViolationSecurityException();
    }
  }
}
