package com.ebs.dda.masterdata.ivr.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecordValueObject;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorCommonPurchaseUnitsGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorCommonPurchaseUnitsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;

import java.util.List;

public class ItemVendorRecordCreationValidator {

  private ValidationResult validationResult;
  private MObVendorGeneralModelRep vendorGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private CObCurrencyRep currencyRep;
  private IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private ItemVendorCommonPurchaseUnitsGeneralModelRep itemVendorCommonPurchaseUnitsGeneralModelRep;

  public ValidationResult validate(ItemVendorRecordValueObject itemVendorRecordValueObject)
      throws ArgumentViolationSecurityException {
    validationResult = new ValidationResult();

    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getItemCode());
    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getVendorCode());
    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getPurchaseUnitCode());
    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getUnitOfMeasureCode());
    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getOldItemNumber());
    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getItemVendorCode());

    checkIfCurrencyNotExistInCaseOfPriceExists(
        itemVendorRecordValueObject.getCurrencyCode(), itemVendorRecordValueObject.getPrice());

    checkIfVendorInstanceExist(itemVendorRecordValueObject);
    checkIfItemInstanceExist(itemVendorRecordValueObject);
    checkIfPurchasingUnitIsCommonBetweenItemAndVendor(itemVendorRecordValueObject);
    checkIfCurrencyInstanceExist(itemVendorRecordValueObject);
    checkIfUnitOfMeasureIsOneOfTheItemMeasures(itemVendorRecordValueObject);
    checkIfOldItemNumberAlreadyExistsInIVRWithTheSamePurchaseUnit(itemVendorRecordValueObject);

    if (validationResult.isValid()) {
      checkItemVendorRecordUniqueness(itemVendorRecordValueObject);
    }

    return validationResult;
  }

  public void setVendorGeneralModelRep(MObVendorGeneralModelRep vendorGeneralModelRep) {
    this.vendorGeneralModelRep = vendorGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setAlternativeUoMGeneralModelRep(
      IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep) {
    this.alternativeUoMGeneralModelRep = alternativeUoMGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setItemVendorCommonPurchaseUnitsGeneralModelRep(
      ItemVendorCommonPurchaseUnitsGeneralModelRep itemVendorCommonPurchaseUnitsGeneralModelRep) {
    this.itemVendorCommonPurchaseUnitsGeneralModelRep =
        itemVendorCommonPurchaseUnitsGeneralModelRep;
  }

  private void checkIfValueNotExistForMandatoryAttr(Object value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.toString().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfCurrencyNotExistInCaseOfPriceExists(String currencyCode, Double price)
      throws ArgumentViolationSecurityException {
    if (price != null && (currencyCode == null || currencyCode.isEmpty())) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfOldItemNumberAlreadyExistsInIVRWithTheSamePurchaseUnit(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {
    String oldItemNumber = itemVendorRecordValueObject.getOldItemNumber();
    String purchaseUnitCode = itemVendorRecordValueObject.getPurchaseUnitCode();
    validateOldItemNumber(getIVRByOldItemNumberAndPurchaseUnit(oldItemNumber, purchaseUnitCode));
  }

  private List<ItemVendorRecordGeneralModel> getIVRByOldItemNumberAndPurchaseUnit(
      String oldItemNumber, String purchaseUnitCode) {
    return itemVendorRecordGeneralModelRep.findByOldItemNumberAndPurchasingUnitCode(
        oldItemNumber, purchaseUnitCode);
  }

  private void validateOldItemNumber(
      List<ItemVendorRecordGeneralModel> itemVendorRecordGeneralModel) {
    if (itemVendorRecordGeneralModel.size() > 0) {
      validationResult.bindError(
          IItemVendorRecordValueObject.OLD_ITEM_NUMBER, IExceptionsCodes.IVR_OLD_ITEM_NUMBER_EXIST);
    }
  }

  private void checkIfVendorInstanceExist(ItemVendorRecordValueObject itemVendorRecordValueObject) {
    MObVendorGeneralModel vendorGeneralModel =
        vendorGeneralModelRep.findOneByUserCode(itemVendorRecordValueObject.getVendorCode());
    if (vendorGeneralModel == null) {
      validationResult.bindError(
          IItemVendorRecordValueObject.VENDOR_CODE, IExceptionsCodes.IVR_VENDOR_NOT_EXIST);
    }
  }

  private void checkIfItemInstanceExist(ItemVendorRecordValueObject itemVendorRecordValueObject) {
    MObItemGeneralModel itemGeneralModel =
        itemGeneralModelRep.findByUserCode(itemVendorRecordValueObject.getItemCode());
    if (itemGeneralModel == null) {
      validationResult.bindError(
          IItemVendorRecordValueObject.ITEM_CODE, IExceptionsCodes.IVR_ITEM_NOT_EXIST);
    }
  }

  private void checkIfPurchasingUnitIsCommonBetweenItemAndVendor(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {

    ItemVendorCommonPurchaseUnitsGeneralModel itemVendorCommonPurchaseUnitsGeneralModel =
        itemVendorCommonPurchaseUnitsGeneralModelRep
            .findOneByVendorCodeAndItemCodeAndPurchasingUnitCode(
                itemVendorRecordValueObject.getVendorCode(),
                itemVendorRecordValueObject.getItemCode(),
                itemVendorRecordValueObject.getPurchaseUnitCode());

    if (itemVendorCommonPurchaseUnitsGeneralModel == null) {
      validationResult.bindError(
          IItemVendorRecordValueObject.PURCHASE_UNIT, IExceptionsCodes.IVR_PURCHASE_UNIT_NOT_EXIST);
    }
  }

  private void checkIfCurrencyInstanceExist(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {
    if (itemVendorRecordValueObject.getCurrencyCode() != null) {
      CObCurrency currency =
          currencyRep.findOneByUserCode(itemVendorRecordValueObject.getCurrencyCode());
      if (currency == null) {
        validationResult.bindError(
            IItemVendorRecordValueObject.CURRENCY_CODE, IExceptionsCodes.IVR_CURRENCY_NOT_EXIST);
      }
    }
  }

  private void checkIfUnitOfMeasureIsOneOfTheItemMeasures(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {

    String itemCode = itemVendorRecordValueObject.getItemCode();
    String unitOfMeasureCode = itemVendorRecordValueObject.getUnitOfMeasureCode();

    IObAlternativeUoMGeneralModel alternativeUoMGeneralModel =
        alternativeUoMGeneralModelRep.findByItemCodeAndAlternativeUnitOfMeasureCode(
            itemCode, unitOfMeasureCode);

    MObItemGeneralModel item =
        itemGeneralModelRep.findByUserCodeAndBasicUnitOfMeasureCode(itemCode, unitOfMeasureCode);

    if (alternativeUoMGeneralModel == null && item == null) {
      validationResult.bindError(
          IItemVendorRecordValueObject.UOM_CODE, IExceptionsCodes.IVR_UOM_NOT_EXIST);
    }
  }

  private void checkItemVendorRecordUniqueness(
      ItemVendorRecordValueObject itemVendorRecordValueObject) {
    ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
        itemVendorRecordGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                itemVendorRecordValueObject.getItemCode(),
                itemVendorRecordValueObject.getVendorCode(),
                itemVendorRecordValueObject.getUnitOfMeasureCode(),
                itemVendorRecordValueObject.getPurchaseUnitCode());

    if (itemVendorRecordGeneralModel != null) {
      validationResult.bindError(
          IItemVendorRecordValueObject.IVR, IExceptionsCodes.IVR_ALREADY_EXIST);
    }
  }
}
