package com.ebs.dda.masterdata.item;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.item.IMObItemUOMValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemUOMValueObject;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.measure.UnitOfMeasureValueObject;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MObItemUOMValidator {

  private ValidationResult validationResult;
  private CObMeasureRep measureRep;
  private List<String> alternativeUnitCodesList;
  private List<String> oldItemNumbersList;
  private MObItemGeneralModelRep itemGeneralModelRep;

  public void setCObMeasureRep(CObMeasureRep measureRep) {
    this.measureRep = measureRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public ValidationResult validate(MObItemUOMValueObject valueObjects) throws Exception {
    validationResult = new ValidationResult();
    alternativeUnitCodesList = new ArrayList<>();
    oldItemNumbersList = new ArrayList<>();
    for (UnitOfMeasureValueObject valueObject : valueObjects.getItemUnitOfMeasures()) {
      validateEachValueObject(valueObject, valueObjects.getItemCode());
    }
    checkForDuplicateEntries(valueObjects.getItemUnitOfMeasures());
    return validationResult;
  }

  private void validateEachValueObject(UnitOfMeasureValueObject valueObject, String itemCode)
      throws Exception {

    Float conversionFactor = valueObject.getConversionFactor();
    checkIfValueNotExistForMandatoryAttr(valueObject.getOldItemNumber());
    checkIfValueNotExistForMandatoryAttr(conversionFactor.toString());
    checkIfValueNotExistForMandatoryAttr(valueObject.getAlternativeUnitCode());

    validateOldItemNumberValue(valueObject.getOldItemNumber());
    validateAlternativeUnit(valueObject, itemCode);
    validateConversionFactor(conversionFactor);
  }

  private void prepareLists(List<UnitOfMeasureValueObject> valueObjects) {
    for (UnitOfMeasureValueObject valueObject : valueObjects) {
      alternativeUnitCodesList.add(valueObject.getAlternativeUnitCode());
      oldItemNumbersList.add(valueObject.getOldItemNumber());
    }
  }

  private void checkForDuplicateEntries(List<UnitOfMeasureValueObject> valueObjects)
      throws Exception {
    prepareLists(valueObjects);
    checkForDuplicateAlternativeUnitsInListOfValueObjects();
  }

  private void checkForDuplicateAlternativeUnitsInListOfValueObjects() throws Exception {
    Set<String> alternativeUnitSet = new HashSet<>(alternativeUnitCodesList);
    if (alternativeUnitSet.size() < alternativeUnitCodesList.size()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateOldItemNumberValue(String oldItemNumber) throws Exception {
    try {
      Long.parseLong(oldItemNumber);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
    checkValueOfOldItemNumber(oldItemNumber);
  }

  private void checkIfValueNotExistForMandatoryAttr(String value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateAlternativeUnit(UnitOfMeasureValueObject valueObject, String itemCode)
      throws ArgumentViolationSecurityException {
    String alternativeUnitCode = valueObject.getAlternativeUnitCode();
    try {
      Long.parseLong(alternativeUnitCode);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
    checkLengthOfCode(alternativeUnitCode);
    checkIfAlternativeUnitExists(alternativeUnitCode);
    checkIfAlternativeUnitIsBaseUnit(alternativeUnitCode, itemCode);
  }

  private void checkIfAlternativeUnitExists(String alternativeUnitCode) {
    CObMeasure measure = measureRep.findOneByUserCode(alternativeUnitCode);
    if (measure == null) {
      validationResult.bindError(
          IMObItemUOMValueObject.ALTERNATIVE_UNIT + '-' + alternativeUnitCode,
          IExceptionsCodes.ITEM_BASE_UNIT_NOT_EXIST);
    }
  }

  private void checkIfAlternativeUnitIsBaseUnit(String alternativeUnitCode, String itemCode)
      throws ArgumentViolationSecurityException {
    MObItemGeneralModel itemGeneralModel = itemGeneralModelRep.findByUserCode(itemCode);
    if (itemGeneralModel.getBasicUnitOfMeasureCode().equals(alternativeUnitCode)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateConversionFactor(Float conversionFactor) throws Exception {
    String integerFactor = ((Long) conversionFactor.longValue()).toString();
    if (conversionFactor <= 0 || integerFactor.length() > 9) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkValueOfOldItemNumber(String entry) throws ArgumentViolationSecurityException {
    Long oldItemNUmber = Long.parseLong(entry);
    if (entry.length() > 20 || oldItemNUmber <= 0) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkLengthOfCode(String code) throws ArgumentViolationSecurityException {
    if (code.length() != 4) {
      throw new ArgumentViolationSecurityException();
    }
  }
}
