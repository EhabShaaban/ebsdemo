package com.ebs.dda.masterdata.assetmasterdata;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.masterdata.assetmasterdata.MObAsset;
import com.ebs.dda.jpa.masterdata.assetmasterdata.MObAssetGeneralModel;

import java.util.HashSet;
import java.util.Set;

public class MObAssetStateMachine extends AbstractStateMachine {

  private Set<String> objectStates;
  private boolean initialized;

  private MObAsset object;
  private MObAssetGeneralModel objectGeneralModel;

  public MObAssetStateMachine() throws Exception {
    super(MObAssetStateMachine.class.getClassLoader().getResource("META-INF/MObAsset.scxml"));
  }

  public boolean goToActive() {
    return true;
  }


  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof MObAsset) {
      this.object = (MObAsset) obj;
    } else {
      this.objectGeneralModel = (MObAssetGeneralModel) obj;
    }
  }
}
