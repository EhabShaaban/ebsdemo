package com.ebs.dda.masterdata.vendor;

public interface IMObVendorActionNames {
    String VIEW_GENERAL_DATA_SECTION = "ReadGeneralData";
    String EDIT_GENERAL_DATA_SECTION = "UpdateGeneralData";
    String READ_ALL = "ReadAll";

    String EDIT_ACCOUNTING_DETAILS_SECTION = "UpdateAccountingDetails";
}
