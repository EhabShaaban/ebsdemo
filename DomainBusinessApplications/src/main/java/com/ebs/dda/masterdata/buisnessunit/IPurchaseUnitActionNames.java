package com.ebs.dda.masterdata.buisnessunit;

public interface IPurchaseUnitActionNames {
  String READ_ALL_FOR_PURCHASE_ORDER = "ReadAll_ForPO";
}
