package com.ebs.dda.masterdata.item;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItemCreateValueObject;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.productmanager.CObProductManager;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MObItemGeneralDataMandatoryValidator {

  private ValidationResult validationResult;
  private MObItemRep itemRep;
  private CObProductManagerRep productManagerRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;

  public ValidationResult validate(MObItemGeneralDataValueObject itemGeneralDataValueObject)
      throws ArgumentViolationSecurityException, InstanceNotExistException {

    validationResult = new ValidationResult();

    checkIfItemExist(itemGeneralDataValueObject);

    checkMandatoryListNotEmpty(itemGeneralDataValueObject.getPurchasingUnitCodes());
    checkMandatoryStringNotNullOrEmpty(itemGeneralDataValueObject.getItemName());
    checkMandatoryStringNotNullOrEmpty(itemGeneralDataValueObject.getMarketName());
    checkMandatoryStringNotNullOrEmpty(itemGeneralDataValueObject.getProductManagerCode());

    validatePurchaseUnits(itemGeneralDataValueObject);
    checkIfProductManagerExist(itemGeneralDataValueObject);

    return validationResult;
  }

  private void checkIfProductManagerExist(MObItemGeneralDataValueObject itemGeneralDataValueObject) {
    String productManagerCode = itemGeneralDataValueObject.getProductManagerCode();
    CObProductManager productManager = productManagerRep.findOneByUserCode(productManagerCode);
    if(productManager == null && productManagerCode != null){
      validationResult.bindError(
              IMObItemGeneralDataValueObject.PRODUCT_MANAGER_CODE,
              IExceptionsCodes.ITEM_PRODUCT_MANAGER_NOT_EXIST);
      return ;
    }

  }

  private void checkIfItemExist(MObItemGeneralDataValueObject itemGeneralDataValueObject)
      throws InstanceNotExistException {
    MObItem item = itemRep.findOneByUserCode(itemGeneralDataValueObject.getItemCode());

    if (item == null) {
      throw new InstanceNotExistException();
    }
  }

  private Boolean checkIfPurchaseUnitExist(String purchaseUnitCode) {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    return purchasingUnitGeneralModel != null;
  }

  private void checkMandatoryListNotEmpty(List<String> PurchasingUnitCodes)
      throws ArgumentViolationSecurityException {
    if (PurchasingUnitCodes == null || PurchasingUnitCodes.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkMandatoryStringNotNullOrEmpty(String value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validatePurchaseUnits(MObItemGeneralDataValueObject itemGeneralDataValueObject)
      throws ArgumentViolationSecurityException {
    List<String> purchasingUnitCodes = itemGeneralDataValueObject.getPurchasingUnitCodes();
    for (String purchaseUnitCode : purchasingUnitCodes) {
      validatePurchaseUnitCodeLength(purchaseUnitCode);
      validateThatPurchaseUnitLongValue(purchaseUnitCode);

      if (validatePurchaseUnitExistence(purchaseUnitCode)) {
        return;
      }
    }
    validatePurchaseUnitReferencedByIVRObject(itemGeneralDataValueObject);
  }

  private void validatePurchaseUnitReferencedByIVRObject(
      MObItemGeneralDataValueObject itemGeneralDataValueObject) {
    String itemCode = itemGeneralDataValueObject.getItemCode();
    List<String> purchasingUnitCodes = itemGeneralDataValueObject.getPurchasingUnitCodes();
    List<ItemVendorRecordGeneralModel> itemVendorRecords =
            itemVendorRecordGeneralModelRep.findAllByItemCode(itemCode);
    Set<String> requiredPurchaseUnits = new HashSet<>();
    itemVendorRecords.stream()
            .forEach(
                    itemVendorRecord -> {
                      String iVRPurchaseUnitCode = itemVendorRecord.getPurchasingUnitCode();
                      if (!purchasingUnitCodes.contains(iVRPurchaseUnitCode)) {
                        requiredPurchaseUnits.add(iVRPurchaseUnitCode);
                      }
                    });
    if (!requiredPurchaseUnits.isEmpty()) {
      List<String> purchaseUnits = getRequiredPurchaseUnitNames(requiredPurchaseUnits);
      validationErrorForPurchaseUnitReferencedByIVRObject(purchaseUnits);
    }
  }

  private List<String> getRequiredPurchaseUnitNames(Set<String> purchaseUnits) {
    List<String> purchaseUnitNames = new ArrayList<>();
    for (String purchaseUnitCode : purchaseUnits) {
      String purchaseUnitName = getPurchaseUnitNameWithCode(purchaseUnitCode);
      purchaseUnitNames.add(purchaseUnitName);
    }
    return purchaseUnitNames;
  }

  private String getPurchaseUnitNameWithCode(String code) {
    CObPurchasingUnitGeneralModel purchaseUnit =
            purchasingUnitGeneralModelRep.findOneByUserCode(code);
    LocalizedString localizedPurchaseUnit = purchaseUnit.getName();
    return localizedPurchaseUnit.getValue("en");
  }

  private void validationErrorForPurchaseUnitReferencedByIVRObject(List<String> purchaseUnits) {
    validationResult.bindErrorWithValues(
            IMObItemCreateValueObject.PURCHASE_UNITS,
            IExceptionsCodes.ITEM_PURCHASE_UNITS_DELETION_BUT_REFERENCED_BY_IVR,
            purchaseUnits);
  }

  private boolean validatePurchaseUnitExistence(String purchaseUnitCode) {
    Boolean isExistingPurchaseUnit = checkIfPurchaseUnitExist(purchaseUnitCode);
    if (!isExistingPurchaseUnit) {
      validationResult.bindError(
          IMObItemCreateValueObject.PURCHASE_UNITS,
          IExceptionsCodes.ITEM_PURCHASE_UNITS_NOT_EXIST);
      return true;
    }
    return false;
  }

  private void validateThatPurchaseUnitLongValue(String purchaseUnitCode)
      throws ArgumentViolationSecurityException {
    try {
      Long.parseLong(purchaseUnitCode);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validatePurchaseUnitCodeLength(String purchaseUnitCode)
      throws ArgumentViolationSecurityException {
    if (purchaseUnitCode.length() != 4) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setItemRep(MObItemRep itemRep) {
    this.itemRep = itemRep;
  }

  public void setProductManagerRep(CObProductManagerRep productManagerRep) {
    this.productManagerRep = productManagerRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }
}
