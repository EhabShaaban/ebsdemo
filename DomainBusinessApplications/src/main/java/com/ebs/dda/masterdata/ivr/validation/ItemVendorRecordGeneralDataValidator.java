package com.ebs.dda.masterdata.ivr.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecordGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralDataValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;

public class ItemVendorRecordGeneralDataValidator {

  private ValidationResult validationResult;
  private CObCurrencyRep currencyRep;

  public ValidationResult validate(
      ItemVendorRecordGeneralDataValueObject itemVendorRecordValueObject)
      throws ArgumentViolationSecurityException {
    validationResult = new ValidationResult();

    checkIfValueNotExistForMandatoryAttr(itemVendorRecordValueObject.getItemCodeAtVendor());

    checkIfCurrencyNotExistInCaseOfPriceExists(
        itemVendorRecordValueObject.getCurrencyCode(), itemVendorRecordValueObject.getPrice());
    checkIfCurrencyInstanceExist(itemVendorRecordValueObject);

    return validationResult;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  private void checkIfValueNotExistForMandatoryAttr(Object value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.toString().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfCurrencyNotExistInCaseOfPriceExists(String currencyCode, Double price)
      throws ArgumentViolationSecurityException {
    if (price != null && (currencyCode == null || currencyCode.isEmpty())) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfCurrencyInstanceExist(
      ItemVendorRecordGeneralDataValueObject itemVendorRecordValueObject) {
    if (itemVendorRecordValueObject.getCurrencyCode() != null) {
      CObCurrency currency =
          currencyRep.findOneByUserCode(itemVendorRecordValueObject.getCurrencyCode());
      if (currency == null) {
        validationResult.bindError(
            IItemVendorRecordGeneralDataValueObject.CURRENCY,
            IExceptionsCodes.IVR_CURRENCY_NOT_EXIST);
      }
    }
  }
}
