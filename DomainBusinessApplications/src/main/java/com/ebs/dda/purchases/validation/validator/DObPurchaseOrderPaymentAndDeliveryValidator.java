package com.ebs.dda.purchases.validation.validator;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.masterdata.lookups.LObContainerRequirement;
import com.ebs.dda.jpa.masterdata.lookups.LObIncoterms;
import com.ebs.dda.jpa.masterdata.lookups.LObPort;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObContainerRequirementRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIncotermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObPortRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.editability.DObPurchasePaymentAndDeliverySectionEditabilityManager;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.LObModeOfTransport;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.CObShipping;
import com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups.LObModeOfTransportRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.CObShippingRep;

public class DObPurchaseOrderPaymentAndDeliveryValidator {

  private DObPurchaseOrderPaymentAndDeliveryValueObject newPaymentAndDeliveryValueObject;
  private ValidationResult validationResult;
  private LObIncotermsRep lObIncotermsRep;
  private CObCurrencyRep cObCurrencyRep;
  private CObShippingRep cObShippingRep;
  private LObModeOfTransportRep lObModeOfTransportRep;
  private LObPortRep lObPortRep;
  private CObPaymentTermsRep cobPaymentTermsRep;
  private LObContainerRequirementRep lObContainerRequirementRep;
  private DObPurchasePaymentAndDeliverySectionEditabilityManager
      paymentAndDeliverySectionEditabilityManager;

  public ValidationResult validate(DObPurchaseOrderPaymentAndDeliveryValueObject valueObject)
      throws Exception {

    validationResult = new ValidationResult();

    String incotermCode = valueObject.getIncoterm();
    String currencyCode = valueObject.getCurrency();
    String paymentTermsCode = valueObject.getPaymentTerms();
    String shippingInstructionsCode = valueObject.getShippingInstructions();
    String containersTypeCode = valueObject.getContainersType();
    String transportModeCode = valueObject.getModeOfTransport();
    String loadingPortCode = valueObject.getLoadingPort();
    String dischargePortCode = valueObject.getDischargePort();

    checkIfIncotermExists(incotermCode);
    checkIfCurrencyExists(currencyCode);
    checkIfPaymentTermsExists(paymentTermsCode);
    checkIfShippingInstructionsExists(shippingInstructionsCode);
    checkIfContainerTypeExists(containersTypeCode);
    checkIfTransportModeExists(transportModeCode);
    checkIfLoadingPortExists(loadingPortCode);
    checkIfDischargePortExists(dischargePortCode);
    checkOneCollectionDateExist(valueObject.getCollectionDate(), valueObject.getNumberOfDays());
    newPaymentAndDeliveryValueObject = checkIfFieldsAreEditable(valueObject);

    return validationResult;
  }


  public DObPurchaseOrderPaymentAndDeliveryValueObject getNewPaymentAndDeliveryValueObject() {
    return newPaymentAndDeliveryValueObject;
  }

  private DObPurchaseOrderPaymentAndDeliveryValueObject checkIfFieldsAreEditable(
      DObPurchaseOrderPaymentAndDeliveryValueObject paymentAndDeliveryValueObject)
      throws Exception {
    return paymentAndDeliverySectionEditabilityManager.applyEditabilityConfiguration(
        paymentAndDeliveryValueObject);
  }

  private void checkOneCollectionDateExist(String collectionDate, String numberOfDays)
      throws ArgumentViolationSecurityException {
    if ((collectionDate != null && numberOfDays != null))
      throw new ArgumentViolationSecurityException();
  }

  private void checkIfIncotermExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObIncoterms lObIncoterms = lObIncotermsRep.findOneByUserCode(attributeCode);

    if (lObIncoterms != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_INCOTERM_MSG);
  }

  private void checkIfCurrencyExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    CObCurrency cObCurrency = cObCurrencyRep.findOneByUserCode(attributeCode);

    if (cObCurrency != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_CURRENCY_MSG);
  }

  private void checkIfPaymentTermsExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    CObPaymentTerms cObPaymentTerms = cobPaymentTermsRep.findOneByUserCode(attributeCode);

    if (cObPaymentTerms != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_PAYMENT_TERM_MSG);
  }

  private void checkIfShippingInstructionsExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    CObShipping cObShipping = cObShippingRep.findOneByUserCode(attributeCode);

    if (cObShipping != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_SHIPPING_INSTRUCTIONS_MSG);
  }

  private void checkIfContainerTypeExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObContainerRequirement lObContainerRequirement =
        lObContainerRequirementRep.findOneByUserCode(attributeCode);

    if (lObContainerRequirement != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_TYPE,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_CONTAINER_TYPE_MSG);
  }

  private void checkIfTransportModeExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObModeOfTransport lObModeOfTransport = lObModeOfTransportRep.findOneByUserCode(attributeCode);

    if (lObModeOfTransport != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_MODE_OF_TRANSPORT_MSG);
  }

  private void checkIfLoadingPortExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObPort lObPort = lObPortRep.findOneByUserCode(attributeCode);

    if (lObPort != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.LOADING_PORT,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_LOADING_PORT_MSG);
  }

  private void checkIfDischargePortExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObPort lObPort = lObPortRep.findOneByUserCode(attributeCode);

    if (lObPort != null) return;

    validationResult.bindError(
        IDObPurchaseOrderPaymentAndDeliveryValueObject.DISCHARGE_PORT,
        IExceptionsCodes.PURCHASE_ORDER_INACTIVE_DISCHARGE_PORT_MSG);
  }

  public void setLObIncotermsRep(LObIncotermsRep lObIncotermsRep) {
    this.lObIncotermsRep = lObIncotermsRep;
  }

  public void setCObCurrencyRep(CObCurrencyRep cObCurrencyRep) {
    this.cObCurrencyRep = cObCurrencyRep;
  }

  public void setLObModeOfTransportRep(LObModeOfTransportRep lObModeOfTransportRep) {
    this.lObModeOfTransportRep = lObModeOfTransportRep;
  }

  public void setCobPaymentTermsRep(CObPaymentTermsRep cobPaymentTermsRep) {
    this.cobPaymentTermsRep = cobPaymentTermsRep;
  }

  public void setCObShippingRep(CObShippingRep cObShippingRep) {
    this.cObShippingRep = cObShippingRep;
  }

  public void setLObPortRep(LObPortRep lObPortRep) {
    this.lObPortRep = lObPortRep;
  }

  public void setLObContainerRequirementRep(LObContainerRequirementRep lObContainerRequirementRep) {
    this.lObContainerRequirementRep = lObContainerRequirementRep;
  }

  public void setPaymentAndDeliverySectionEditabilityManager(
      DObPurchasePaymentAndDeliverySectionEditabilityManager
          paymentAndDeliverySectionEditabilityManager) {
    this.paymentAndDeliverySectionEditabilityManager = paymentAndDeliverySectionEditabilityManager;
  }
}
