package com.ebs.dda.purchases.apis;

public interface IPurchaseOrderActionNames {

  String EDIT_HEADER_SECTION = "EditHeader";
  String VIEW_VENDOR_SECTION = "ReadVendor";
  String EDIT_CONSIGNEE_SECTION = "UpdateConsignee";
  String EDIT_COMPANY_SECTION = "UpdateCompany";
  String VIEW_PAYMENT_AND_DELIVERY_SECTION = "ReadPaymentTerms";
  String VIEW_PAYMENT_TERM_SECTION = "ReadPaymentTerms";
  String READ_COMPANY_DATA = "ReadCompany";
  String VIEW_CONSIGNEE_SECTION = "ReadConsigneeData";
  String VIEW_REQUIRED_DOCUMENTS = "ReadRequiredDocuments";
  String READ_ITEMS_SECTION_WITH_PRICES = "ReadItemsWithPrices";
  String VIEW_ITEMS_SECTION_WITHOUT_PRICES = "ReadItemsWithoutPrices";
  String UPDATE_PAYMENT_TERMS = "UpdatePaymentTerms";
  String EDIT_REQUIRED_DOCUMENTS_SECTION = "UpdateRequiredDocuments";
  String VIEW_ATTACHMENT_SECTION = "ReadAttachments";
  String VIEW_APPROVAL_CYCLES = "ReadApprovalCycles";
  String UPDATE_ITEM = "UpdateItem";
  String ADD_QTY_IN_DN_PL = "AddPL/DNQuantity";
  String REPLACE_APPROVER_PO = "ReplaceApprover";
  String APPROVE_PO = "Approve";
  String MARK_AS_PI_REQUESTED = "MarkAsPIRequested";
  String MARK_AS_SHIPPED = "MarkAsShipped";
  String REJECT_PO = "Reject";
  String MARK_AS_CONFIRMED = "Confirm";
  String MARK_AS_PRODUCTION_FINISHED = "MarkAsProductionFinished";
  String MARK_AS_ARRIVED = "MarkAsArrived";
  String MARK_AS_CLEARED = "MarkAsCleared";
  String EXPORT_PDF = "ExportPDF";
  String MARK_AS_DELIVERY_COMPLETE = "MarkAsDeliveryComplete";
  String CANCEL = "Cancel";
  String SUBMIT_FOR_APPROVAL = "SubmitForApproval";
  String OPEN_FOR_UPDATES = "OpenUpdates";
  String POST_GOODS_RECEIPT = "PostGoodsReceipt";
  String GOODS_RECEIVED = "Received";
}
