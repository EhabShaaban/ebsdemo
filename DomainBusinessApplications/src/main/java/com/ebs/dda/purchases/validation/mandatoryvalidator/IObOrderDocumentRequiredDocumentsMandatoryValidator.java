package com.ebs.dda.purchases.validation.mandatoryvalidator;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderDocumentRequiredDocumentsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderDocumentRequiredDocumentsValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObPurchaseOrderDocumentValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IRequiredDocumentsMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IObOrderDocumentRequiredDocumentsMandatoryValidator
    implements ISectionMandatoryValidator {

  private DObOrderDocumentRep orderDocumentRep;
  private IObOrderDocumentRequiredDocumentsGeneralModelRep
      orderDocumentRequiredDocumentsGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;

  public IObOrderDocumentRequiredDocumentsMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) throws Exception {
    IObOrderDocumentRequiredDocumentsValueObject documentsValueObject =
        (IObOrderDocumentRequiredDocumentsValueObject) valueObject;
    DObOrderDocument order =
        orderDocumentRep.findOneByUserCode(documentsValueObject.getPurchaseOrderCode());
    validateThatRequiredDocumentsHasList(documentsValueObject, order);
    validatePurchaseOrderRequiredDocumentsMandatoryFields(documentsValueObject, order);
  }

  @Override
  public List<String> validateGeneralModelMandatories(String purchaseOrderCode, String state)
      throws Exception {
    HashSet<String> result = new HashSet<>();
    List<IObOrderDocumentRequiredDocumentsGeneralModel> requiredDocumentsGeneralModelList =
        orderDocumentRequiredDocumentsGeneralModelRep.findByUserCodeOrderById(purchaseOrderCode);
    Map<String, String[]> mandatories =
        IRequiredDocumentsMandatoryAttributes.GENERAL_MODEL_MANDATORIES;

    if (!IRequiredDocumentsMandatoryAttributes.isDraftOrCanceled(state)
        && requiredDocumentsGeneralModelList.isEmpty()) {
      result.add(IRequiredDocumentsMandatoryAttributes.EMPTY_SECTION_DATA);
      return new ArrayList<>(result);
    }

    result =
        getRequiredDocumentsMissingFields(state, requiredDocumentsGeneralModelList, mandatories);
    return new ArrayList<>(result);
  }

  private HashSet<String> getRequiredDocumentsMissingFields(
      String state,
      List<IObOrderDocumentRequiredDocumentsGeneralModel> requiredDocumentsGeneralModelList,
      Map<String, String[]> mandatories)
      throws Exception {
    HashSet<String> result = new HashSet<>();
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);

    for (IObOrderDocumentRequiredDocumentsGeneralModel requiredDocumentsGeneralModel :
        requiredDocumentsGeneralModelList) {
      List<String> requiredDocRecordMissingFields =
          validatorUtils.validateMandatoriesForGeneralModel(
              mandatoryAttributes, requiredDocumentsGeneralModel);
      result.addAll(requiredDocRecordMissingFields);
    }
    return result;
  }

  private void validateThatRequiredDocumentsHasList(
      IObOrderDocumentRequiredDocumentsValueObject valueObject, DObOrderDocument dObOrder)
      throws ArgumentViolationSecurityException {
    boolean hasListInState =
        !IRequiredDocumentsMandatoryAttributes.isDraftOrCanceled(dObOrder.getCurrentState());
    if (valueObject.hasEmptyList() && hasListInState) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validatePurchaseOrderRequiredDocumentsMandatoryFields(
      IObOrderDocumentRequiredDocumentsValueObject valueObject, DObOrderDocument order)
      throws Exception {
    Map<String, String[]> documentsMandatories =
        IRequiredDocumentsMandatoryAttributes.VALUE_OBJECT_MANDATORIES;
    Set<String> documentsMandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            documentsMandatories, order.getCurrentStates());
    List<IObPurchaseOrderDocumentValueObject> purchaseOrderDocuments =
        valueObject.getPurchaseOrderDocuments();
    for (IObPurchaseOrderDocumentValueObject document : purchaseOrderDocuments) {
      validatorUtils.validateMandatoriesForValueObject(documentsMandatoryAttributes, document);
    }
  }

  public void setOrderDocumentRequiredDocumentsGeneralModelRep(
      IObOrderDocumentRequiredDocumentsGeneralModelRep
          orderDocumentRequiredDocumentsGeneralModelRep) {
    this.orderDocumentRequiredDocumentsGeneralModelRep =
        orderDocumentRequiredDocumentsGeneralModelRep;
  }

  public void setOrderDocumentRep(DObOrderDocumentRep orderDocumentRep) {
    this.orderDocumentRep = orderDocumentRep;
  }
}
