package com.ebs.dda.purchases.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DObPurchaseOrderItemsQtyInDnPlEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {

    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    getEnabledAttributes(enabledAttributesPerState);
    return enabledAttributesPerState;
  }

  private void getEnabledAttributes(
      Map<String, Set<String>> enabledAttributesPerState) {

    Set<String> enabledAttributes = new HashSet<>();

    enabledAttributes.addAll(
        Arrays.asList(
            IPurchaseOrderItemValueObject.QUANTITY_IN_DN));

    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.SHIPPED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.ARRIVED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.CLEARED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, enabledAttributes);
  }
}
