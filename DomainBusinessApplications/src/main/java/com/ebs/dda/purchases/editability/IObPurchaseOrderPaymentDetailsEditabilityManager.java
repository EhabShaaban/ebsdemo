package com.ebs.dda.purchases.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;

import java.util.*;

public class IObPurchaseOrderPaymentDetailsEditabilityManager extends AbstractEditabilityManager {
  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();

    Set<String> draftEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IIObOrderPaymentDetailsGeneralModel.PAYMENT_TERM_CODE,
                IIObOrderPaymentDetailsGeneralModel.CURRENCY_ISO));

    enabledAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.CONFIRMED_STATE, new HashSet<>());
    return enabledAttributesPerState;
  }
}
