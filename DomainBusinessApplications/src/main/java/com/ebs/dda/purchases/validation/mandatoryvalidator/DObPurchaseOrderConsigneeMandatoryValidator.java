package com.ebs.dda.purchases.validation.mandatoryvalidator;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderConsigneeGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderConsigneeGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConsigneeValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IConsigneeMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DObPurchaseOrderConsigneeMandatoryValidator implements ISectionMandatoryValidator {

  private DObPurchaseOrderRep dObPurchaseOrderRep;
  private DObPurchaseOrderConsigneeGeneralModelRep consigneeGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;

  public DObPurchaseOrderConsigneeMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setdObOrderRep(DObPurchaseOrderRep dObPurchaseOrderRep) {
    this.dObPurchaseOrderRep = dObPurchaseOrderRep;
  }

  public void setConsigneeGeneralModelRep(
      DObPurchaseOrderConsigneeGeneralModelRep consigneeGeneralModelRep) {
    this.consigneeGeneralModelRep = consigneeGeneralModelRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) throws Exception {
    DObPurchaseOrderConsigneeValueObject orderConsigneeValueObject =
        (DObPurchaseOrderConsigneeValueObject) valueObject;
    DObPurchaseOrder dObPurchaseOrder =
        dObPurchaseOrderRep.findOneByUserCode(orderConsigneeValueObject.getPurchaseOrderCode());
    Map<String, String[]> mandatories = IConsigneeMandatoryAttributes.MANDATORIES;
    Set<String> mandatoryAttributesByState =
        validatorUtils.getMandatoryAttributesByState(mandatories, dObPurchaseOrder.getCurrentStates());

    validatorUtils.validateMandatoriesForValueObject(mandatoryAttributesByState, valueObject);
  }

  @Override
  public List<String> validateGeneralModelMandatories(String purchaseOrderCode, String state)
      throws Exception {
    DObPurchaseOrderConsigneeGeneralModel headerGeneralModel =
        consigneeGeneralModelRep.findByUserCode(purchaseOrderCode);
    Map<String, String[]> mandatories = IConsigneeMandatoryAttributes.MANDATORIES;
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);
    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, headerGeneralModel);
  }
}
