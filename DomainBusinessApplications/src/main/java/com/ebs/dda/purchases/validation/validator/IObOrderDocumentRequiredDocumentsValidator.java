package com.ebs.dda.purchases.validation.validator;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.LObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObPurchaseOrderDocumentValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderDocumentRequiredDocumentsValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObPurchaseOrderDocumentValueObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IObOrderDocumentRequiredDocumentsValidator {

  private ValidationResult validationResult;
  private LObAttachmentTypeRep lObAttachmentTypeRep;

  public void setlObAttachmentTypeRep(LObAttachmentTypeRep lObAttachmentTypeRep) {
    this.lObAttachmentTypeRep = lObAttachmentTypeRep;
  }

  public ValidationResult validate(IObOrderDocumentRequiredDocumentsValueObject valueObject)
      throws Exception {

    validationResult = new ValidationResult();

    List<IObPurchaseOrderDocumentValueObject> purchaseOrderDocuments =
        valueObject.getPurchaseOrderDocuments();

    for (IObPurchaseOrderDocumentValueObject document : purchaseOrderDocuments) {
      checkIfDocumentExists(document.getDocumentTypeCode());
    }
    checkDocumentsDuplication(purchaseOrderDocuments);
    return validationResult;
  }

  private void checkDocumentsDuplication(
      List<IObPurchaseOrderDocumentValueObject> purchaseOrderDocuments) throws Exception {
    Set<String> appeared = new HashSet<>();
    for (IObPurchaseOrderDocumentValueObject item : purchaseOrderDocuments) {
      if (!appeared.add(item.getDocumentTypeCode())) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkIfDocumentExists(String attributeCode) {

    if (attributeCode == null || attributeCode.equals("")) return;

    LObAttachmentType lObAttachmentType = lObAttachmentTypeRep.findOneByUserCode(attributeCode);

    if (lObAttachmentType != null) return;

    validationResult.bindError(
        IIObPurchaseOrderDocumentValueObject.DOCUMENT_TYPE_CODE,
        IExceptionsCodes.PURCHASE_ORDER_DOCUMENT_TYPE_EXIST_MSG);
  }
}
