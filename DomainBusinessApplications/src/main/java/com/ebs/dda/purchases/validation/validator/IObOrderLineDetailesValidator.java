package com.ebs.dda.purchases.validation.validator;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import java.util.List;

public class IObOrderLineDetailesValidator {
  private ValidationResult validationResult;
  private DObPurchaseOrderGeneralModelRep purchaseOrderHeaderGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;

  public ValidationResult validate(IObOrderLineDetailsValueObject valueObject) throws Exception {

    validationResult = new ValidationResult();
    String orderCode = valueObject.getPurchaseOrderCode();
    String itemCode = valueObject.getItemCode();

    checkIfOrderItemIsUnique(orderCode, itemCode);

    DObPurchaseOrderGeneralModel orderHeaderGeneralModel =
        purchaseOrderHeaderGeneralModelRep.findOneByUserCode(orderCode);
    String vendorCode = orderHeaderGeneralModel.getVendorCode();
    String purchaseUnitCode = orderHeaderGeneralModel.getPurchaseUnitCode();

    checkIfOrderItemHasAtLeastOneItemVendorRecord(itemCode, vendorCode, purchaseUnitCode);
    return validationResult;
  }

  private void checkIfOrderItemIsUnique(String orderCode, String itemCode)
      throws Exception {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel = orderLineDetailsGeneralModelRep
        .findByOrderCodeAndItemCode(orderCode, itemCode);
    if (orderLineDetailsGeneralModel != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfOrderItemHasAtLeastOneItemVendorRecord(
      String itemCode, String vendorCode, String purchaseUnitCode) {
    List<ItemVendorRecordGeneralModel> itemVendorRecordGeneralModels =
        itemVendorRecordGeneralModelRep.findByItemCodeAndVendorCodeAndPurchasingUnitCode(
            itemCode, vendorCode, purchaseUnitCode);
    if (itemVendorRecordGeneralModels.isEmpty()) {
      validationResult.bindError(
          IPurchaseOrderItemValueObject.ITEM_CODE,
          IExceptionsCodes.PURCHASE_ORDER_ITEM_HAS_NO_IVR_MSG);
    }
  }
  //  setters

  public void setPurchaseOrderHeaderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderHeaderGeneralModelRep) {
    this.purchaseOrderHeaderGeneralModelRep = purchaseOrderHeaderGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setOrderLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
  }
}
