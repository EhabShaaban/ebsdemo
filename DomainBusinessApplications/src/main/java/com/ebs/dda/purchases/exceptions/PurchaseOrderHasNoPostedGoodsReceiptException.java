package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class PurchaseOrderHasNoPostedGoodsReceiptException extends Exception
    implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.PURCHASE_ORDER_HAS_NO_POSTED_GR;
  }
}
