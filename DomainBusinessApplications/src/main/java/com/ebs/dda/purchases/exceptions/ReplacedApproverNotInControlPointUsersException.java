package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ReplacedApproverNotInControlPointUsersException extends Exception
    implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.REPLACED_APPROVER_NOT_IN_CONTROL_POINT_USERS;
  }
}
