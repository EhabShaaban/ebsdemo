package com.ebs.dda.purchases.editability;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderItemGeneralModel;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IIObServicePurchaseOrderItemsMandatoryAttributes {
  String EMPTY_ITEMS_LIST = "ItemsList";
  String[] ITEM_SECTION_MANDATORIES = {
    IIObOrderItemGeneralModel.ITEM_CODE,
    IIObOrderItemGeneralModel.UNIT_OF_MEASURE_CODE,
    IIObOrderItemGeneralModel.QUANTITY,
    IIObOrderItemGeneralModel.PRICE
  };
  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObServicePurchaseOrderStateMachine.DRAFT_STATE, ITEM_SECTION_MANDATORIES)
          .put(DObServicePurchaseOrderStateMachine.CONFIRMED_STATE, ITEM_SECTION_MANDATORIES)
          .build();
}
