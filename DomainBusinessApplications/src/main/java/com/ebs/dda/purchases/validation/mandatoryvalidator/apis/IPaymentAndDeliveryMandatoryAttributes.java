package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.apis.IDObPaymentAndDeliveryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IPaymentAndDeliveryMandatoryAttributes {

  String[] IMPORT_FOR_VALUE_OBJECT_CANCEL = {
    IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT
  };

  String[] IMPORT_FOR_VALUE_OBJECT_ACTIVE = {
    IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT
  };

  String[] IMPORT_FOR_VALUE_OBJECT_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE = {
      IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
      IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
      IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
      IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_NUMBER,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_TYPE,
      IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE,
      IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.LOADING_PORT,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.DISCHARGE_PORT
  };

  String[] LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE = {
    IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE,
    IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT
  };

  Map<String, String[]> LOCAL_MANDATORIES_FOR_VALUE_OBJECT =
      ImmutableMap.<String, String[]>builder()
          .put(
              DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.WAITING_APPROVAL,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.APPROVED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CANCELLED_STATE,
              LOCAL_FOR_VALUE_OBJECT_ALL_STATES_EXCEPT_DRAFT_STATE)
          .build();

  Map<String, String[]> IMPORT_MANDATORIES_FOR_VALUE_OBJECT =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, IMPORT_FOR_VALUE_OBJECT_ACTIVE)
          .put(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL, IMPORT_FOR_VALUE_OBJECT_ACTIVE)
          .put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, IMPORT_FOR_VALUE_OBJECT_ACTIVE)
          .put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, IMPORT_FOR_VALUE_OBJECT_ACTIVE)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
              IMPORT_FOR_VALUE_OBJECT_ACTIVE)
          .put(
              DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
              IMPORT_FOR_VALUE_OBJECT_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
              IMPORT_FOR_VALUE_OBJECT_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              IMPORT_FOR_VALUE_OBJECT_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              IMPORT_FOR_VALUE_OBJECT_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(DObImportPurchaseOrderStateMachine.CANCELLED_STATE, IMPORT_FOR_VALUE_OBJECT_CANCEL)
          .build();

  String[] IMPORT_FOR_GENERAL_MODEL_OPENFORUPDATES_WAITINGAPPROVAL_CANCEL = {
    IDObPaymentAndDeliveryGeneralModel.INCOTERM,
    IDObPaymentAndDeliveryGeneralModel.CURRENCY,
    IDObPaymentAndDeliveryGeneralModel.PAYMENT_TERMS,
    IDObPaymentAndDeliveryGeneralModel.SHIPPING_INSTRUCTIONS,
    IDObPaymentAndDeliveryGeneralModel.COLLECTION_DATE,
    IDObPaymentAndDeliveryGeneralModel.MODE_OF_TRANSPORT
  };

  String[] IMPORT_FOR_GENERAL_MODEL_APPROVED_CONFIRMED_FINISHED = {
    IDObPaymentAndDeliveryGeneralModel.INCOTERM,
    IDObPaymentAndDeliveryGeneralModel.CURRENCY,
    IDObPaymentAndDeliveryGeneralModel.PAYMENT_TERMS,
    IDObPaymentAndDeliveryGeneralModel.SHIPPING_INSTRUCTIONS,
    IDObPaymentAndDeliveryGeneralModel.COLLECTION_DATE,
    IDObPaymentAndDeliveryGeneralModel.MODE_OF_TRANSPORT
  };

  String[] IMPORT_FOR_GENERAL_MODEL_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE = {
    IDObPaymentAndDeliveryGeneralModel.INCOTERM,
    IDObPaymentAndDeliveryGeneralModel.CURRENCY,
    IDObPaymentAndDeliveryGeneralModel.PAYMENT_TERMS,
    IDObPaymentAndDeliveryGeneralModel.SHIPPING_INSTRUCTIONS,
    IDObPaymentAndDeliveryGeneralModel.COLLECTION_DATE,
    IDObPaymentAndDeliveryGeneralModel.MODE_OF_TRANSPORT,
    IDObPaymentAndDeliveryGeneralModel.CONTAINERS_NUMBER,
    IDObPaymentAndDeliveryGeneralModel.CONTAINERS_TYPE,
    IDObPaymentAndDeliveryGeneralModel.LOADING_PORT,
    IDObPaymentAndDeliveryGeneralModel.DISCHARGE_PORT
  };

  String[] LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE = {
    IDObPaymentAndDeliveryGeneralModel.INCOTERM,
    IDObPaymentAndDeliveryGeneralModel.CURRENCY,
    IDObPaymentAndDeliveryGeneralModel.PAYMENT_TERMS,
    IDObPaymentAndDeliveryGeneralModel.COLLECTION_DATE,
    IDObPaymentAndDeliveryGeneralModel.MODE_OF_TRANSPORT
  };

  Map<String, String[]> LOCAL_MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(
              DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.WAITING_APPROVAL,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CANCELLED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.APPROVED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
              LOCAL_FOR_GENERAL_MODEL_ALL_STATES_EXCEPT_DRAFT_STATE)
          .build();

  Map<String, String[]> IMPORT_MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(
              DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
              IMPORT_FOR_GENERAL_MODEL_OPENFORUPDATES_WAITINGAPPROVAL_CANCEL)
          .put(
              DObImportPurchaseOrderStateMachine.WAITING_APPROVAL,
              IMPORT_FOR_GENERAL_MODEL_OPENFORUPDATES_WAITINGAPPROVAL_CANCEL)
          .put(
              DObImportPurchaseOrderStateMachine.CANCELLED_STATE,
              IMPORT_FOR_GENERAL_MODEL_OPENFORUPDATES_WAITINGAPPROVAL_CANCEL)
          .put(
              DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
              IMPORT_FOR_GENERAL_MODEL_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
              IMPORT_FOR_GENERAL_MODEL_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              IMPORT_FOR_GENERAL_MODEL_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              IMPORT_FOR_GENERAL_MODEL_SHIPPED_ARRIVED_CLEARED_DELIVERYCOMPLETE)
          .put(
              DObImportPurchaseOrderStateMachine.APPROVED_STATE,
              IMPORT_FOR_GENERAL_MODEL_APPROVED_CONFIRMED_FINISHED)
          .put(
              DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
              IMPORT_FOR_GENERAL_MODEL_APPROVED_CONFIRMED_FINISHED)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
              IMPORT_FOR_GENERAL_MODEL_APPROVED_CONFIRMED_FINISHED)
          .build();
}
