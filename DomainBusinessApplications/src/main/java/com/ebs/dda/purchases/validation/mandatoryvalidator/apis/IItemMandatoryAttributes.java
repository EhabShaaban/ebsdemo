package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IItemMandatoryAttributes {
  String EMPTY_SECTION_DATA = "OrderItemsList";
  String QUANTITIES_EMPTY_SECTION_DATA = "QuantitiesPerOrderItem";

  String[] ITEM_FOR_OTHER_STATES = {IPurchaseOrderItemValueObject.ITEM_CODE};
  String[] ITEM_FOR_CLEARED_AND_DELIVERY_COMPLETE_STATES = {
    IPurchaseOrderItemValueObject.ITEM_CODE, IPurchaseOrderItemValueObject.QUANTITY_IN_DN
  };

  Map<String, String[]> ITEM_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.ACTIVE_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.SHIPPED_STATE, ITEM_FOR_OTHER_STATES)
          .put(DObImportPurchaseOrderStateMachine.ARRIVED_STATE, ITEM_FOR_OTHER_STATES)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              ITEM_FOR_CLEARED_AND_DELIVERY_COMPLETE_STATES)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              ITEM_FOR_CLEARED_AND_DELIVERY_COMPLETE_STATES)
          .build();

  String[] ITEM_QUANTITES_FOR_ALL_STATES = {
    IIObOrderLineDetailsQuantitiesValueObject.QUANTITY,
    IIObOrderLineDetailsQuantitiesValueObject.ORDER_UNIT_CODE,
    IIObOrderLineDetailsQuantitiesValueObject.PRICE,
    IIObOrderLineDetailsQuantitiesValueObject.DISCOUNT_PERCENTAGE
  };

  Map<String, String[]> ITEM_QUANTITES_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.SHIPPED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.ARRIVED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CLEARED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CANCELLED_STATE, ITEM_QUANTITES_FOR_ALL_STATES)
          .build();

  static boolean isDraft(String state) {
    return state.equals(DObImportPurchaseOrderStateMachine.DRAFT_STATE);
  }
}
