package com.ebs.dda.purchases.statemachines;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import java.util.HashSet;
import java.util.Set;

public class DObImportPurchaseOrderStateMachine extends AbstractStateMachine {

  public static final String DRAFT_STATE = "Draft";
  public static final String OPEN_FOR_UPDATES = "OpenForUpdates";
  public static final String WAITING_APPROVAL = "WaitingApproval";
  public static final String APPROVED_STATE = "Approved";
  public static final String CONFIRMED_STATE = "Confirmed";
  public static final String FINISHED_PRODUCTION_STATE = "FinishedProduction";
  public static final String SHIPPED_STATE = "Shipped";
  public static final String ARRIVED_STATE = "Arrived";
  public static final String CLEARED_STATE = "Cleared";
  public static final String DELIVERY_COMPLETE_STATE = "DeliveryComplete";
  public static final String ACTIVE_STATE = "Active";
  public static final String CANCELLED_STATE = "Cancelled";

  private Set<String> objectStates;

  private boolean initialized;

  private DObPurchaseOrder object;
  private DObPurchaseOrderGeneralModel objectGeneralModel;

  public DObImportPurchaseOrderStateMachine() throws Exception {

    super(
        DObImportPurchaseOrderStateMachine.class
            .getClassLoader()
            .getResource("META-INF/DObImportPurchaseOrder.scxml"));
  }

  public boolean goToDraft() {
    return true;
  }

  public boolean goToOpenForUpdates() {
    return true;
  }

  public boolean goToInOperation() {
    return true;
  }

  public boolean goToActiveStates() {
    return true;
  }

  public boolean goToParallelActive() {
    return true;
  }

  public boolean goToActive() {
    return true;
  }

  public boolean goToCanBeCancelledStates() {
    return true;
  }

  public boolean goToCannotBeCancelledStates() {
    return true;
  }

  public boolean goToWaitingApproval() {
    return true;
  }

  public boolean goToApproved() {
    return true;
  }

  public boolean goToRejected() {
    return true;
  }

  public boolean goToOnHold() {
    return true;
  }

  public boolean goToWaitingVendorConfirmation() {
    return true;
  }

  public boolean goToConfirmed() {
    return true;
  }

  public boolean goToFinishedProduction() {
    return true;
  }

  public boolean goToShipped() {
    return true;
  }

  public boolean goToArrived() {
    return true;
  }

  public boolean goToCleared() {
    return true;
  }

  public boolean goToClosed() {
    return true;
  }

  public boolean goToCancelled() {
    return true;
  }

  public boolean goToHoldingRegion() {
    return true;
  }

  public boolean goToCancellingRegion() {
    return true;
  }

  public boolean goToOperation() {
    return true;
  }

  public boolean goToCancellingHoldingParallel() {
    return true;
  }

  public boolean goToInactiveParallelBlock() {
    return true;
  }

  public boolean goToInactiveParallel() {
    return true;
  }

  public boolean goToInactive() {
    return true;
  }

  public boolean goToInactiveStates() {
    return true;
  }

  public boolean goToDeliveryComplete() {
    return true;
  }

  public boolean goToPayment() {
    return true;
  }

  public boolean goToGoodsState() {
    return true;
  }

  public boolean goToNotReceived() {
    return true;
  }

  public boolean goToReceived() {
    return true;
  }

  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObPurchaseOrder) {
      this.object = (DObPurchaseOrder) obj;
    } else {
      this.objectGeneralModel = (DObPurchaseOrderGeneralModel) obj;
    }
  }
}
