package com.ebs.dda.purchases.validation.mandatoryvalidator;

import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObPurchaseOrderGeneralModelsMandatoryValidator {

  private DObPurchaseOrderConsigneeMandatoryValidator consigneeMandatoryValidator;

  private IObOrderDocumentRequiredDocumentsMandatoryValidator requiredDocumentsMandatoryValidator;

  private DObPurchaseOrderPaymentAndDeliveryMandatoryValidator paymentAndDeliveryMandatoryValidator;

  private DObPurchaseOrderItemsMandatoryValidator itemsMandatoryValidator;

  private MandatoryValidatorUtils validatorUtils;

  public DObPurchaseOrderGeneralModelsMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public Map<String, List<String>> validateAllSectionsForState(
      String purchaseOrderCode, String state) throws Exception {
    Map<String, List<String>> sectionsMissingFields = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IPurchaseOrderSectionNames.CONSIGNEE_SECTION,
        validateConsigneeSection(purchaseOrderCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION,
        validateRequiredDocumentsSection(purchaseOrderCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION,
        validatePaymentAndDeliverySection(purchaseOrderCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        sectionsMissingFields,
        IPurchaseOrderSectionNames.ITEMS_SECTION,
        validateItemSection(purchaseOrderCode, state));

    return sectionsMissingFields;
  }

  private List<String> validateConsigneeSection(String purchaseOrderCode, String state)
      throws Exception {
    return consigneeMandatoryValidator.validateGeneralModelMandatories(purchaseOrderCode, state);
  }

  private List<String> validateRequiredDocumentsSection(String purchaseOrderCode, String state)
      throws Exception {
    return requiredDocumentsMandatoryValidator.validateGeneralModelMandatories(
        purchaseOrderCode, state);
  }

  private List<String> validatePaymentAndDeliverySection(String purchaseOrderCode, String state)
      throws Exception {
    return paymentAndDeliveryMandatoryValidator.validateGeneralModelMandatories(
        purchaseOrderCode, state);
  }

  private List<String> validateItemSection(String purchaseOrderCode, String state)
      throws Exception {
    return itemsMandatoryValidator.validateGeneralModelMandatories(purchaseOrderCode, state);
  }

  public void setConsigneeMandatoryValidator(
      DObPurchaseOrderConsigneeMandatoryValidator consigneeMandatoryValidator) {
    this.consigneeMandatoryValidator = consigneeMandatoryValidator;
  }

  public void setRequiredDocumentsMandatoryValidator(
      IObOrderDocumentRequiredDocumentsMandatoryValidator requiredDocumentsMandatoryValidator) {
    this.requiredDocumentsMandatoryValidator = requiredDocumentsMandatoryValidator;
  }

  public void setPaymentAndDeliveryMandatoryValidator(
      DObPurchaseOrderPaymentAndDeliveryMandatoryValidator paymentAndDeliveryMandatoryValidator) {
    this.paymentAndDeliveryMandatoryValidator = paymentAndDeliveryMandatoryValidator;
  }

  public void setItemsMandatoryValidator(
      DObPurchaseOrderItemsMandatoryValidator itemsMandatoryValidator) {
    this.itemsMandatoryValidator = itemsMandatoryValidator;
  }
}
