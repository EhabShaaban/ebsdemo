package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import java.util.List;

public interface ISectionMandatoryValidator<IValueObject> {

  void validateValueObjectMandatories(IValueObject valueObject) throws Exception;

  Object validateGeneralModelMandatories(String code, String state) throws Exception;
}
