package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IGeneralDataMandatoryAttributes {

  String[] FOR_ALL_STATES_FOR_VALUE_OBJECTS_VALIDATION = {
    IPurchaseOrderValueObject.PURCHASE_RESPONSIBLE_CODE
  };

  Map<String, String[]> MANDATORIES_FOR_VALUE_OBJECT =
      ImmutableMap.<String, String[]>builder()
          .put(
              DObImportPurchaseOrderStateMachine.DRAFT_STATE, FOR_ALL_STATES_FOR_VALUE_OBJECTS_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.ACTIVE_STATE,
              FOR_ALL_STATES_FOR_VALUE_OBJECTS_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.CANCELLED_STATE,
              FOR_ALL_STATES_FOR_VALUE_OBJECTS_VALIDATION)
          .build();

  String[] FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION = {
      IPurchaseOrderValueObject.VENDOR_CODE,
      IPurchaseOrderValueObject.PURCHASE_UNIT_CODE,
      IPurchaseOrderValueObject.PURCHASE_RESPONSIBLE_CODE
  };

  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(
              DObImportPurchaseOrderStateMachine.DRAFT_STATE, FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.WAITING_APPROVAL,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.APPROVED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.CLEARED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .put(
              DObImportPurchaseOrderStateMachine.CANCELLED_STATE,
              FOR_ALL_STATES_FOR_GENERAL_MODEL_VALIDATION)
          .build();
}
