package com.ebs.dda.purchases.statemachines;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.InvalidStateMachineException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObImportPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObLocalPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;

public class DObPurchaseOrderStateMachineFactory {

  public AbstractStateMachine createPurchaseOrderStateMachine(DObPurchaseOrder orderDocument)
      throws Exception {

    if (orderDocument instanceof DObImportPurchaseOrder) {
      return createImportPurchaseOrderStateMachine(orderDocument);
    }
    if (orderDocument instanceof DObLocalPurchaseOrder) {
      return createLocalPurchaseOrderStateMachine(orderDocument);
    }
    if (orderDocument instanceof DObServicePurchaseOrder) {
      return createServicePurchaseOrderStateMachine(orderDocument);
    }
    throw new InvalidStateMachineException(
        "DObPurchaseOrderFactory : No suitable state machine for this purchase order type");
  }

  public AbstractStateMachine createPurchaseOrderStateMachine(
      DObPurchaseOrderGeneralModel orderDocument) throws Exception {

    if (orderDocument.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
      return createImportPurchaseOrderStateMachine(orderDocument);
    }
    if (orderDocument.getObjectTypeCode().equals(OrderTypeEnum.LOCAL_PO.name())) {
      return createLocalPurchaseOrderStateMachine(orderDocument);
    }
    if (orderDocument.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name())) {
      return createServicePurchaseOrderStateMachine(orderDocument);
    }
    throw new InvalidStateMachineException(
        "DObPurchaseOrderFactory : No suitable state machine for this purchase order type");
  }

  private DObLocalPurchaseOrderStateMachine createLocalPurchaseOrderStateMachine(
      IStatefullBusinessObject orderDocument) throws Exception {
    DObLocalPurchaseOrderStateMachine localPurchaseOrderStateMachine =
        new DObLocalPurchaseOrderStateMachine();
    localPurchaseOrderStateMachine.initObjectState(orderDocument);
    return localPurchaseOrderStateMachine;
  }

  private DObImportPurchaseOrderStateMachine createImportPurchaseOrderStateMachine(
      IStatefullBusinessObject orderDocument) throws Exception {
    DObImportPurchaseOrderStateMachine importPurchaseOrderStateMachine =
        new DObImportPurchaseOrderStateMachine();
    importPurchaseOrderStateMachine.initObjectState(orderDocument);
    return importPurchaseOrderStateMachine;
  }

  private AbstractStateMachine createServicePurchaseOrderStateMachine(
      IStatefullBusinessObject orderDocument) throws Exception {
    DObServicePurchaseOrderStateMachine servicePurchaseOrderStateMachine =
        new DObServicePurchaseOrderStateMachine();
    servicePurchaseOrderStateMachine.initObjectState(orderDocument);
    return servicePurchaseOrderStateMachine;
  }
}
