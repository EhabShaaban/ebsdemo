package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderConsigneeValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IConsigneeMandatoryAttributes {
  String[] FOR_DELIVERY_COMPLETE_VALIDATION = {
    IDObPurchaseOrderConsigneeValueObject.CONSIGNEE_CODE,
    IDObPurchaseOrderConsigneeValueObject.PLANT_CODE,
    IDObPurchaseOrderConsigneeValueObject.STOREHOUSE_CODE
  };

  String[] FOR_OTHER_STATES_VALIDATION = {};

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, FOR_OTHER_STATES_VALIDATION)
          .put(DObImportPurchaseOrderStateMachine.ACTIVE_STATE, FOR_OTHER_STATES_VALIDATION)
          .put(DObImportPurchaseOrderStateMachine.CANCELLED_STATE, FOR_OTHER_STATES_VALIDATION)
          .put(DObImportPurchaseOrderStateMachine.CLEARED_STATE, FOR_DELIVERY_COMPLETE_VALIDATION)
          .put(DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE, FOR_DELIVERY_COMPLETE_VALIDATION)
          .build();
}
