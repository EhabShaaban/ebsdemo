package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IPurchaseOrderPaymentDetailsMandatoryAttributes {

  String[] CONFIRMED_STATE_MANDATORIES = {
    IIObOrderPaymentDetailsGeneralModel.PAYMENT_TERM_CODE,
    IIObOrderPaymentDetailsGeneralModel.CURRENCY_ISO
  };

  Map<String, String[]> MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObServicePurchaseOrderStateMachine.DRAFT_STATE,new String[]{} )
          .put(DObServicePurchaseOrderStateMachine.CONFIRMED_STATE, CONFIRMED_STATE_MANDATORIES)
          .build();
}
