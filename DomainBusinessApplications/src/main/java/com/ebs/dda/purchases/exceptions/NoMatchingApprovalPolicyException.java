package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class NoMatchingApprovalPolicyException extends Exception implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.PO_NO_MATCHING_APPROVAL_POLICY;
  }
}
