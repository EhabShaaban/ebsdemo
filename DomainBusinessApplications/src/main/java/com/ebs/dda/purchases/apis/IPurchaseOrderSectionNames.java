package com.ebs.dda.purchases.apis;

public interface IPurchaseOrderSectionNames {
  String GENERAL_DATA_SECTION = "GeneralData";
  String HEADER_SECTION = "HeaderData";
  String ITEMS_SECTION = "ItemsData";
  String CONSIGNEE_SECTION = "ConsigneeData";
  String PAYMENT_DELIVERY_SECTION = "PaymentTerms";
  String PAYMENT_DETAILS_SECTION = "PaymentDetails";
  String REQUIRED_DOCUMENTS_SECTION = "RequiredDocs";
  String COMPANY_SECTION = "CompanyData";
}
