package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ControlPointApproverHasAlreadyAssignedException extends Exception
    implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.CONTROL_POINT_APPROVER_HAS_ALREADY_ASSIGNED;
  }
}
