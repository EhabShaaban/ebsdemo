package com.ebs.dda.purchases.validation.mandatoryvalidator.apis;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObPurchaseOrderDocumentValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IRequiredDocumentsMandatoryAttributes {
  String EMPTY_SECTION_DATA = "RequiredDocs";

  String[] FOR_VALUE_OBJECT_ALL_STATES = {
    IIObPurchaseOrderDocumentValueObject.DOCUMENT_TYPE_CODE,
    IIObPurchaseOrderDocumentValueObject.DOCUMENT_NUMBER_OF_COPIES
  };

  Map<String, String[]> VALUE_OBJECT_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, FOR_VALUE_OBJECT_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.ACTIVE_STATE, FOR_VALUE_OBJECT_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CANCELLED_STATE, FOR_VALUE_OBJECT_ALL_STATES)
          .build();

  String[] FOR_GENERAL_MODEL_ALL_STATES = {
    "attachmentCode", IIObPurchaseOrderDocumentValueObject.DOCUMENT_NUMBER_OF_COPIES
  };

  Map<String, String[]> GENERAL_MODEL_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.WAITING_APPROVAL, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.SHIPPED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.ARRIVED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CLEARED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .put(DObImportPurchaseOrderStateMachine.CANCELLED_STATE, FOR_GENERAL_MODEL_ALL_STATES)
          .build();

  static boolean isDraftOrCanceled(String state) {
    return state.equals(DObImportPurchaseOrderStateMachine.DRAFT_STATE)
        || state.equals(DObImportPurchaseOrderStateMachine.CANCELLED_STATE);
  }
}
