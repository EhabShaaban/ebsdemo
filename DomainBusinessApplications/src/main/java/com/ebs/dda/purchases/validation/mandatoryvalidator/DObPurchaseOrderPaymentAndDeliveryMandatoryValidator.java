package com.ebs.dda.purchases.validation.mandatoryvalidator;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.ValidationUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPaymentAndDeliveryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPaymentAndDeliveryGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IPaymentAndDeliveryMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.*;

public class DObPurchaseOrderPaymentAndDeliveryMandatoryValidator
    implements ISectionMandatoryValidator {

  private final String COLLECTION_DATE =
      IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE;
  private final String NUMBER_OF_DAYS = "numberOfDays";
  private DObPurchaseOrderRep dObPurchaseOrderRep;
  private DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;
  private ValidationUtils validationUtils;

  public DObPurchaseOrderPaymentAndDeliveryMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
    validationUtils = new ValidationUtils();
  }

  public void setdObOrderRep(DObPurchaseOrderRep dObPurchaseOrderRep) {
    this.dObPurchaseOrderRep = dObPurchaseOrderRep;
  }

  public void setPaymentAndDeliveryGeneralModelRep(
      DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep) {
    this.paymentAndDeliveryGeneralModelRep = paymentAndDeliveryGeneralModelRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) throws Exception {
    DObPurchaseOrderPaymentAndDeliveryValueObject deliveryValueObject =
        (DObPurchaseOrderPaymentAndDeliveryValueObject) valueObject;
    DObPurchaseOrder dObPurchaseOrder =
        dObPurchaseOrderRep.findOneByUserCode(deliveryValueObject.getPurchaseOrderCode());
    Map<String, String[]> mandatories = new HashMap<>();
    mandatories = getMandatoriesByOrderType(dObPurchaseOrder);
    Set<String> mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            mandatories, dObPurchaseOrder.getCurrentStates());
    for (String mandatoryAttribute : mandatoryAttributes) {
      Object mandatoryAttributeValue =
          validationUtils.getAttributeValue(mandatoryAttribute, valueObject);
      if (isNullCollectionDate(mandatoryAttribute, mandatoryAttributeValue))
        mandatoryAttributeValue = validationUtils.getAttributeValue(NUMBER_OF_DAYS, valueObject);
      if (validatorUtils.checkIfNullOrEmpty(mandatoryAttributeValue)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private Map<String, String[]> getMandatoriesByOrderType(DObPurchaseOrder dObPurchaseOrder) {
    Map<String, String[]> mandatories;
    if (dObPurchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
      mandatories = IPaymentAndDeliveryMandatoryAttributes.IMPORT_MANDATORIES_FOR_VALUE_OBJECT;
    } else {
      mandatories = IPaymentAndDeliveryMandatoryAttributes.LOCAL_MANDATORIES_FOR_VALUE_OBJECT;
    }
    return mandatories;
  }

  @Override
  public List<String> validateGeneralModelMandatories(String purchaseOrderCode, String state)
      throws Exception {
    DObPurchaseOrder dObPurchaseOrder = dObPurchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    Map<String, String[]> mandatories = getGeneralModelMandatoriesByOrderType(dObPurchaseOrder);

    DObPaymentAndDeliveryGeneralModel paymentAndDeliveryGeneralModel =
        paymentAndDeliveryGeneralModelRep.findByUserCode(purchaseOrderCode);
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);

    return validateMandatoriesForGeneralModel(mandatoryAttributes, paymentAndDeliveryGeneralModel);
  }

  private Map<String, String[]> getGeneralModelMandatoriesByOrderType(
      DObPurchaseOrder dObPurchaseOrder) {
    Map<String, String[]> mandatories = new HashMap<>();
    if (dObPurchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
      mandatories = IPaymentAndDeliveryMandatoryAttributes.IMPORT_MANDATORIES_FOR_GENERAL_MODEL;
    } else {
      mandatories = IPaymentAndDeliveryMandatoryAttributes.LOCAL_MANDATORIES_FOR_GENERAL_MODEL;
    }
    return mandatories;
  }

  private List<String> validateMandatoriesForGeneralModel(
      String[] mandatoryAttributes,
      DObPaymentAndDeliveryGeneralModel paymentAndDeliveryGeneralModel)
      throws Exception {
    List<String> validationResult = new ArrayList<>();
    if (mandatoryAttributes == null) return validationResult;
    for (String mandatoryAttribute : mandatoryAttributes) {
      Object mandatoryAttributeValue =
          validationUtils.getAttributeValue(mandatoryAttribute, paymentAndDeliveryGeneralModel);
      if (isNullCollectionDate(mandatoryAttribute, mandatoryAttributeValue)) {
        mandatoryAttributeValue =
            validationUtils.getAttributeValue(NUMBER_OF_DAYS, paymentAndDeliveryGeneralModel);
      }
      if (validatorUtils.checkIfNullOrEmpty(mandatoryAttributeValue)) {
        validationResult.add(mandatoryAttribute);
      }
    }
    return validationResult;
  }

  private boolean isNullCollectionDate(String mandatoryAttribute, Object mandatoryAttributeValue) {
    return mandatoryAttribute.equals(COLLECTION_DATE) && mandatoryAttributeValue == null;
  }
}
