package com.ebs.dda.purchases.validation.mandatoryvalidator;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsQuantitiesGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IItemMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DObPurchaseOrderItemsMandatoryValidator implements ISectionMandatoryValidator {
  private IObOrderLineDetailsGeneralModelRep lineDetailsGeneralModelRep;
  private IObOrderLineDetailsQuantitiesGeneralModelRep quantitiesGeneralModelRep;
  private DObPurchaseOrderRep orderRep;

  private MandatoryValidatorUtils validatorUtils;

  public DObPurchaseOrderItemsMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep lineDetailsGeneralModelRep) {
    this.lineDetailsGeneralModelRep = lineDetailsGeneralModelRep;
  }

  public void setQuantitiesGeneralModelRep(
      IObOrderLineDetailsQuantitiesGeneralModelRep quantitiesGeneralModelRep) {
    this.quantitiesGeneralModelRep = quantitiesGeneralModelRep;
  }
  public void setOrderRep(DObPurchaseOrderRep dObPurchaseOrderRep) {
    this.orderRep = dObPurchaseOrderRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) throws Exception {
    IObOrderLineDetailsValueObject itemValueObject =
        (IObOrderLineDetailsValueObject) valueObject;
    DObPurchaseOrder order = orderRep.findOneByUserCode(itemValueObject.getPurchaseOrderCode());
    Map<String, String[]> mandatories = IItemMandatoryAttributes.ITEM_MANDATORIES;
    Set<String> mandatoryAttributesByState =
        validatorUtils.getMandatoryAttributesByState(mandatories, order.getCurrentStates());

    validatorUtils.validateMandatoriesForValueObject(mandatoryAttributesByState, valueObject);
  }

  public void validateQuantityValueObjectMandatories(Object valueObject) throws Exception {
    IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject =
        (IObOrderLineDetailsQuantitiesValueObject) valueObject;
    DObPurchaseOrder order = orderRep.findOneByUserCode(quantitiesValueObject.getOrderCode());
    Map<String, String[]> mandatories = IItemMandatoryAttributes.ITEM_QUANTITES_MANDATORIES;
    Set<String> mandatoryAttributesByState =
        validatorUtils.getMandatoryAttributesByState(mandatories, order.getCurrentStates());

    validatorUtils.validateMandatoriesForValueObject(mandatoryAttributesByState, valueObject);
  }

  @Override
  public List<String> validateGeneralModelMandatories(String purchaseOrderCode, String state)
      throws Exception {
    HashSet<String> result = new HashSet<>();
    List<IObOrderLineDetailsGeneralModel> itemsGeneralModelList =
        lineDetailsGeneralModelRep.findByOrderCode(purchaseOrderCode);
    Map<String, String[]> mandatories = IItemMandatoryAttributes.ITEM_MANDATORIES;

    if (!IItemMandatoryAttributes.isDraft(state) && itemsGeneralModelList.isEmpty()) {
      result.add(IItemMandatoryAttributes.EMPTY_SECTION_DATA);
      return new ArrayList<>(result);
    }

    result = getItemsMissingFeilds(state, itemsGeneralModelList, mandatories);
    return new ArrayList<>(result);
  }

  private HashSet<String> getItemsMissingFeilds(
      String state,
      List<IObOrderLineDetailsGeneralModel> itemsGeneralModel,
      Map<String, String[]> mandatories)
      throws Exception {
    HashSet<String> result = new HashSet<>();
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);

    for (IObOrderLineDetailsGeneralModel itemGeneralModel : itemsGeneralModel) {
      List<IObOrderLineDetailsQuantitiesGeneralModel> itemsQuantitiesGeneralModelList =
          this.quantitiesGeneralModelRep.findByRefInstanceIdOrderByIdDesc(itemGeneralModel.getId());
      if (!IItemMandatoryAttributes.isDraft(state) && itemsQuantitiesGeneralModelList.isEmpty()) {
        result.add(IItemMandatoryAttributes.QUANTITIES_EMPTY_SECTION_DATA);
        continue;
      }
      List<String> requiredDocRecordMissingFields =
          validatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes, itemGeneralModel);
      result.addAll(requiredDocRecordMissingFields);
    }
    return result;
  }
}
