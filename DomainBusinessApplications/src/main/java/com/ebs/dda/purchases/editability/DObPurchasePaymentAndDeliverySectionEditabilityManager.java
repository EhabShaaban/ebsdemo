package com.ebs.dda.purchases.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPaymentAndDeliveryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPaymentAndDeliveryGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DObPurchasePaymentAndDeliverySectionEditabilityManager
    extends AbstractEditabilityManager {

  private DObPaymentAndDeliveryGeneralModelRep dObPaymentAndDeliveryGeneralModelRep;

  public DObPurchasePaymentAndDeliverySectionEditabilityManager(
      DObPaymentAndDeliveryGeneralModelRep dObPaymentAndDeliveryGeneralModelRep) {
    this.dObPaymentAndDeliveryGeneralModelRep = dObPaymentAndDeliveryGeneralModelRep;
  }

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {

    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    getShippedArrivedClearedEnabledAttributes(enabledAttributesPerState);
    getDraftOpenForUpdateEnabledAttributes(enabledAttributesPerState);
    return enabledAttributesPerState;
  }

  private void getShippedArrivedClearedEnabledAttributes(
      Map<String, Set<String>> enabledAttributesPerState) {

    Set<String> enabledAttributes = new HashSet<>();

    enabledAttributes.addAll(
        Arrays.asList(
            IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_NUMBER,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_TYPE,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.LOADING_PORT,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.DISCHARGE_PORT));

    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.APPROVED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, enabledAttributes);
    enabledAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.SHIPPED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.ARRIVED_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.CLEARED_STATE, enabledAttributes);
  }

  private void getDraftOpenForUpdateEnabledAttributes(
      Map<String, Set<String>> enabledAttributesPerState) {

    Set<String> enabledAttributes = new HashSet<>();

    enabledAttributes.addAll(
        Arrays.asList(
            IDObPurchaseOrderPaymentAndDeliveryValueObject.INCOTERM,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.CURRENCY,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.PAYMENT_TERMS,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.SHIPPING_INSTRUCTIONS,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_NUMBER,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_TYPE,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.NUMBER_OF_DAYS,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.MODE_OF_TRANSPORT,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.LOADING_PORT,
            IDObPurchaseOrderPaymentAndDeliveryValueObject.DISCHARGE_PORT));

    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, enabledAttributes);
  }

  public DObPurchaseOrderPaymentAndDeliveryValueObject applyEditabilityConfiguration(
      DObPurchaseOrderPaymentAndDeliveryValueObject inputVersionObject) throws Exception {

    String purchaseOrdCode = inputVersionObject.getPurchaseOrderCode();
    DObPaymentAndDeliveryGeneralModel dObPaymentAndDeliveryGeneralModel =
        dObPaymentAndDeliveryGeneralModelRep.findByUserCode(purchaseOrdCode);

    Set<String> enabledAttributes =
        getEnabledAttributesInGivenStates(dObPaymentAndDeliveryGeneralModel.getCurrentStates());

    DObPurchaseOrderPaymentAndDeliveryValueObject savedValueObject =
        getSavedValueObject(dObPaymentAndDeliveryGeneralModel);

    for (String enabledAttribute : enabledAttributes) {
      Object inputVersionValue = getAttributeValue(enabledAttribute, inputVersionObject);
      setAttributeValue(enabledAttribute, inputVersionValue, savedValueObject);
    }

    return savedValueObject;
  }

  private DObPurchaseOrderPaymentAndDeliveryValueObject getSavedValueObject(
      DObPaymentAndDeliveryGeneralModel dObPaymentAndDeliveryGeneralModel) {

    DObPurchaseOrderPaymentAndDeliveryValueObject savedValueObject =
        new DObPurchaseOrderPaymentAndDeliveryValueObject();

    savedValueObject.setPurchaseOrderCode(dObPaymentAndDeliveryGeneralModel.getUserCode());
    savedValueObject.setIncoterm(dObPaymentAndDeliveryGeneralModel.getIncotermCode());
    savedValueObject.setCurrency(dObPaymentAndDeliveryGeneralModel.getCurrencyCode());
    savedValueObject.setPaymentTerms(dObPaymentAndDeliveryGeneralModel.getPaymentTermCode());
    savedValueObject.setShippingInstructions(
        dObPaymentAndDeliveryGeneralModel.getShippingInstructionsCode());
    savedValueObject.setContainersType(dObPaymentAndDeliveryGeneralModel.getContainerCode());
    savedValueObject.setModeOfTransport(dObPaymentAndDeliveryGeneralModel.getTransportModeCode());
    savedValueObject.setLoadingPort(dObPaymentAndDeliveryGeneralModel.getLoadingPortCode());
    savedValueObject.setDischargePort(dObPaymentAndDeliveryGeneralModel.getDischargePortCode());

    if (dObPaymentAndDeliveryGeneralModel.getCollectionDate() != null) {
      DateTime collectionDate =
          new DateTime(
              (Timestamp.valueOf(dObPaymentAndDeliveryGeneralModel.getCollectionDate())).getTime());
      DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
      savedValueObject.setCollectionDate(formatter.print(collectionDate));
    } else if (dObPaymentAndDeliveryGeneralModel.getNumberOfDays() != null) {
      savedValueObject.setNumberOfDays(
          String.valueOf(dObPaymentAndDeliveryGeneralModel.getNumberOfDays()));
    }

    if (dObPaymentAndDeliveryGeneralModel.getContainersNo() != null) {
      savedValueObject.setContainersNo(
          String.valueOf(dObPaymentAndDeliveryGeneralModel.getContainersNo()));
    }

    return savedValueObject;
  }
}
