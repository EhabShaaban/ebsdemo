package com.ebs.dda.purchases.validation.validator;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCompanyValueObject;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsRep;

/**
 * @author Eslam
 * @since October 11, 2018
 */
public class DObPurchaseOrderCompanyValidator {
  private ValidationResult validationResult;

  private CObCompanyGeneralModelRep companyGeneralModelRep;
  private IObCompanyBankDetailsRep companyBankDetailsRep;
  private IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep;

  public ValidationResult validate(DObPurchaseOrderCompanyValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();
    checkIfCompanyIsNotChanged(valueObject.getCompanyCode(), valueObject.getPurchaseOrderCode());
    checkIfBankAccountIsRelatedToCompany(valueObject, getCompanyByUserCode(valueObject));
    return validationResult;
  }

  private void checkIfCompanyIsNotChanged(String companyCode, String purchaseOrderCode)
      throws Exception {
    IObOrderCompanyGeneralModel orderCompany =
        orderCompanyGeneralModelRep.findByUserCode(purchaseOrderCode);
    if (isServicePO(orderCompany) && POHasDifferentCompany(companyCode, orderCompany)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private boolean POHasDifferentCompany(
      String companyCode, IObOrderCompanyGeneralModel orderCompany) {
    if (orderCompany.getCompanyCode() == null) return companyExistsInValueObject(companyCode);
    return !orderCompany.getCompanyCode().equals(companyCode);
  }

  private boolean companyExistsInValueObject(String companyCode) {
    return companyCode != null;
  }

  private boolean isServicePO(IObOrderCompanyGeneralModel companyGeneralModel) {
    return companyGeneralModel.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name());
  }

  private void checkIfBankAccountIsRelatedToCompany(
      DObPurchaseOrderCompanyValueObject creationValueObject, CObCompanyGeneralModel company)
      throws Exception {

    if (creationValueObject.getBankAccountId() != null && company == null) {
      throw new ArgumentViolationSecurityException();
    }

    if (creationValueObject.getBankAccountId() != null) {
      IObCompanyBankDetails companyBankDetails =
          companyBankDetailsRep.findOneByIdAndRefInstanceId(
              creationValueObject.getBankAccountId(), company.getId());
      if (companyBankDetails == null) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private CObCompanyGeneralModel getCompanyByUserCode(
      DObPurchaseOrderCompanyValueObject creationValueObject) {
    return this.companyGeneralModelRep.findOneByUserCode(creationValueObject.getCompanyCode());
  }

  public void setCompanyGeneralModelRep(CObCompanyGeneralModelRep companyGeneralModelRep) {
    this.companyGeneralModelRep = companyGeneralModelRep;
  }

  public void setCompanyBankDetailsRep(IObCompanyBankDetailsRep companyBankDetailsRep) {
    this.companyBankDetailsRep = companyBankDetailsRep;
  }

  public void setOrderCompanyGeneralModelRep(
      IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep) {
    this.orderCompanyGeneralModelRep = orderCompanyGeneralModelRep;
  }
}
