package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class MoreThanOneMatchedApprovalPolicyException extends Exception implements IExceptionResponse{

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.MORE_THAN_ONE_MATCHING_APPROVAL_POLICY;
  }
	
}
