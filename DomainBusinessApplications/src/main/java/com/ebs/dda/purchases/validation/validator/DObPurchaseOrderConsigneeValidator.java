package com.ebs.dda.purchases.validation.validator;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.plant.CObPlant;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouse;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.repositories.masterdata.plant.CObPlantRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderConsigneeEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConsigneeValueObject;
import java.util.Optional;

public class DObPurchaseOrderConsigneeValidator {

  private ValidationResult validationResult;
  private CObPlantRep plantRep;
  private CObStorehouseRep storehouseRep;
  private CObCompanyGeneralModelRep cObCompanyGeneralModelRep;
  private CObPlantGeneralModelRep cObPlantGeneralModelRep;
  private CObStorehouseGeneralModelRep cObStorehouseGeneralModelRep;

  public ValidationResult validate(DObPurchaseOrderConsigneeValueObject consigneeValueObject)
      throws ArgumentViolationSecurityException {
    validationResult = new ValidationResult();
    CObCompanyGeneralModel companyGeneralModel =
        checkIfCompanyExists(
            consigneeValueObject.getConsigneeCode(),
            IIObOrderConsigneeEnterpriseData.CONSIGNEE_CODE);
    CObPlantGeneralModel plantGeneralModel =
        checkIfPlantExists(
            consigneeValueObject.getPlantCode(), IIObOrderConsigneeEnterpriseData.PLANT_CODE);
    CObStorehouseGeneralModel storehouseGeneralModel =
        checkIfStorehouseExists(
            consigneeValueObject.getStorehouseCode(),
            consigneeValueObject.getPlantCode(),
            IIObOrderConsigneeEnterpriseData.STOREHOUSE_CODE);

    checkPlantRelatedToSelectedConsignee(companyGeneralModel, plantGeneralModel);
    checkStorehouseRelatedToSelectedPlant(plantGeneralModel, storehouseGeneralModel);

    return validationResult;
  }

  private void checkPlantRelatedToSelectedConsignee(
      CObCompanyGeneralModel companyGeneralModel, CObPlantGeneralModel plantGeneralModel) {
    if (companyGeneralModel != null && plantGeneralModel != null) {
      Optional<CObPlant> plant = plantRep.findById(plantGeneralModel.getId());
      if (companyGeneralModel.getId() != plant.get().getCompanyId())
        validationResult.bindError(
            IIObOrderConsigneeEnterpriseData.PLANT_CODE,
            IExceptionsCodes.PURCHASE_ORDER_PLANT_NOT_EXIST_MSG);
    }
  }

  private void checkStorehouseRelatedToSelectedPlant(
      CObPlantGeneralModel plantGeneralModel, CObStorehouseGeneralModel storehouseGeneralModel) {
    if (plantGeneralModel != null && storehouseGeneralModel != null) {
      Optional<CObStorehouse> storehouse = storehouseRep.findById(storehouseGeneralModel.getId());
      if (plantGeneralModel.getId() != storehouse.get().getPlantId())
        validationResult.bindError(
            IIObOrderConsigneeEnterpriseData.STOREHOUSE_CODE,
            IExceptionsCodes.PURCHASE_ORDER_STOREHOUSE_NOT_EXIST_OR_RELATED_TO_PLANT_MSG);
    }
  }

  protected CObCompanyGeneralModel checkIfCompanyExists(String entityCode, String attributeName) {

    CObCompanyGeneralModel entity = null;
    if (isNotNullOrEmpty(entityCode)) {
      entity = cObCompanyGeneralModelRep.findOneByUserCode(entityCode);
      if (entity == null) {
        validationResult.bindError(
            attributeName, IExceptionsCodes.PURCHASE_ORDER_COMPANY_NOT_EXIST);
      }
    }
    return entity;
  }

  private boolean isNotNullOrEmpty(String entityCode) {
    return entityCode != null && !entityCode.isEmpty();
  }

  protected CObPlantGeneralModel checkIfPlantExists(String entityCode, String attributeName) {

    CObPlantGeneralModel entity = null;
    if (isNotNullOrEmpty(entityCode)) {
      entity = cObPlantGeneralModelRep.findOneByUserCode(entityCode);
      if (entity == null) {
        validationResult.bindError(
            attributeName, IExceptionsCodes.PURCHASE_ORDER_PLANT_NOT_EXIST_MSG);
      }
    }
    return entity;
  }

  protected CObStorehouseGeneralModel checkIfStorehouseExists(
      String storehouseCode, String plantCode, String attributeName) {

    CObStorehouseGeneralModel entity = null;
    if (isNotNullOrEmpty(storehouseCode)) {

      entity = cObStorehouseGeneralModelRep.findByUserCodeAndPlantCode(storehouseCode, plantCode);
      if (entity == null) {
        validationResult.bindError(
            attributeName,
            IExceptionsCodes.PURCHASE_ORDER_STOREHOUSE_NOT_EXIST_OR_RELATED_TO_PLANT_MSG);
      }
    }
    return entity;
  }

  public void setcObPlantRep(CObPlantRep plantRep) {
    this.plantRep = plantRep;
  }

  public void setcObStorehouseRep(CObStorehouseRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setcObCompanyGeneralModelRep(CObCompanyGeneralModelRep cObCompanyGeneralModelRep) {
    this.cObCompanyGeneralModelRep = cObCompanyGeneralModelRep;
  }

  public void setcObPlantGeneralModelRep(CObPlantGeneralModelRep cObPlantGeneralModelRep) {
    this.cObPlantGeneralModelRep = cObPlantGeneralModelRep;
  }

  public void setcObStorehouseGeneralModelRep(
      CObStorehouseGeneralModelRep cObStorehouseGeneralModelRep) {
    this.cObStorehouseGeneralModelRep = cObStorehouseGeneralModelRep;
  }
}
