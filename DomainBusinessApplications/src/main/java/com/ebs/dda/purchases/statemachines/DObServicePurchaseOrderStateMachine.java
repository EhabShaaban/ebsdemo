package com.ebs.dda.purchases.statemachines;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;

import java.util.HashSet;
import java.util.Set;

public class DObServicePurchaseOrderStateMachine extends AbstractStateMachine {

  public static final String DRAFT_STATE = "Draft";
  public static final String CONFIRMED_STATE = "Confirmed";

  private Set<String> objectStates;

  private boolean initialized;

  private DObPurchaseOrder object;
  private DObPurchaseOrderGeneralModel objectGeneralModel;

  public DObServicePurchaseOrderStateMachine() throws Exception {

    super(
        DObServicePurchaseOrderStateMachine.class
            .getClassLoader()
            .getResource("META-INF/DObServicePurchaseOrder.scxml"));
  }

  public boolean goToDraft() {
    return true;
  }
  public boolean goToConfirmed() {
    return true;
  }


  @Override
  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  @Override
  public boolean resetMachine() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.objectGeneralModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.objectGeneralModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObPurchaseOrder) {
      this.object = (DObPurchaseOrder) obj;
    } else {
      this.objectGeneralModel = (DObPurchaseOrderGeneralModel) obj;
    }
  }
}
