package com.ebs.dda.purchases.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DObPurchaseOrderItemsSectionEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {

    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    getDraftOpenForUpdateEnabledAttributes(enabledAttributesPerState);
    return enabledAttributesPerState;
  }

  private void getDraftOpenForUpdateEnabledAttributes(
      Map<String, Set<String>> enabledAttributesPerState) {

    Set<String> enabledAttributes = new HashSet<>();

    enabledAttributes.addAll(
        Arrays.asList(
            IPurchaseOrderItemValueObject.ITEM_CODE,
            IPurchaseOrderItemValueObject.QUANTITY_IN_DN));

    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.DRAFT_STATE, enabledAttributes);
    enabledAttributesPerState.put(DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, enabledAttributes);
  }
}
