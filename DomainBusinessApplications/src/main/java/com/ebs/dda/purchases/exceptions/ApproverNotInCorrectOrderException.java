package com.ebs.dda.purchases.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ApproverNotInCorrectOrderException extends Exception implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.APPROVER_NOT_IN_CORRECT_ORDER;
  }
}
