package com.ebs.dda.purchases.apis;

public interface IPurchaseOrderPaymentAndDeliveryActions {
  String READ_CURRENCY = "ReadCurrency";
  String READ_SHIPPING_INSTRUCTIONS = "ReadShippingInstructions";
  String READ_PAYMENT_TERMS = "ReadPaymentTerms";
  String READ_INCOTERMS = "ReadIncoTerms";
  String READ_CONTAINER_TYPES = "ReadContainerTypes";
  String READ_PORTS = "ReadPorts";
  String READ_TRANSPORT_MODES = "ReadTransportModes";
}
