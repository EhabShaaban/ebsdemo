package com.ebs.dda.purchases.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderItemGeneralModel;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IObServicePurchaseOrderItemsSectionEditabilityManager
    extends AbstractEditabilityManager {

  public IObServicePurchaseOrderItemsSectionEditabilityManager() {}

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.addAll(
        Arrays.asList(
            IIObOrderItemGeneralModel.ITEM_CODE,
            IIObOrderItemGeneralModel.UNIT_OF_MEASURE_CODE,
            IIObOrderItemGeneralModel.QUANTITY,
            IIObOrderItemGeneralModel.PRICE));

    enabledAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.CONFIRMED_STATE, new HashSet<>());

    return enabledAttributesPerState;
  }
}
