package com.ebs.dda.purchases.editability;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObLocalPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;

import java.util.*;

public class DObPurchaseOrderCompanySectionEditabilityManager {
  private Map<String, Set<String>> enabledServicePOAttributesPerState =
      new HashMap<String, Set<String>>();
  private Map<String, Set<String>> enabledImportPOAttributesPerState =
      new HashMap<String, Set<String>>();
  private Map<String, Set<String>> enabledLocalPOAttributesPerState =
      new HashMap<String, Set<String>>();

  public DObPurchaseOrderCompanySectionEditabilityManager() {
    setServicePurchaseOrderCompanySectionEditabilityManager();
    setLocalPurchaseOrderCompanySectionEditabilityManager();
    setImportPurchaseOrderCompanySectionEditabilityManager();
  }

  private void setServicePurchaseOrderCompanySectionEditabilityManager() {
    Set<String> draftEnabledAttributes =
        new HashSet<>(Arrays.asList(IDObPurchaseOrderGeneralModel.BANK_ACCOUNT_NUMBER));

    Set<String> confirmedEnabledAttributes =
        new HashSet<>(Arrays.asList(IDObPurchaseOrderGeneralModel.BANK_ACCOUNT_NUMBER));

    enabledServicePOAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledServicePOAttributesPerState.put(
        DObServicePurchaseOrderStateMachine.CONFIRMED_STATE, confirmedEnabledAttributes);
  }

  private void setLocalPurchaseOrderCompanySectionEditabilityManager() {
    Set<String> localPurchaseOrderEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IDObPurchaseOrderGeneralModel.BANK_ACCOUNT_NUMBER,
                IDObPurchaseOrderGeneralModel.COMPANY_CODE));

    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.DRAFT_STATE, localPurchaseOrderEnabledAttributes);
    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.OPEN_FOR_UPDATES, localPurchaseOrderEnabledAttributes);
    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE, localPurchaseOrderEnabledAttributes);
    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.APPROVED_STATE, localPurchaseOrderEnabledAttributes);
    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.CONFIRMED_STATE, localPurchaseOrderEnabledAttributes);
    enabledLocalPOAttributesPerState.put(
        DObLocalPurchaseOrderStateMachine.SHIPPED_STATE, localPurchaseOrderEnabledAttributes);
  }

  private void setImportPurchaseOrderCompanySectionEditabilityManager() {
    Set<String> ImportPurchaseOrderEnabledAttributes =
        new HashSet<>(
            Arrays.asList(
                IDObPurchaseOrderGeneralModel.BANK_ACCOUNT_NUMBER,
                IDObPurchaseOrderGeneralModel.COMPANY_CODE));

    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.DRAFT_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.APPROVED_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.CONFIRMED_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
        ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.SHIPPED_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.ARRIVED_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.CLEARED_STATE, ImportPurchaseOrderEnabledAttributes);
    enabledImportPOAttributesPerState.put(
        DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE,
        ImportPurchaseOrderEnabledAttributes);
  }

  public Set<String> getEnabledAttributesConfigByTypeAndState(
      String purchaseOrderType, Set<String> states) {
    if (purchaseOrderType.equals(OrderTypeEnum.SERVICE_PO.name())) {
      return getEnabledAttributesInGivenStates(enabledServicePOAttributesPerState, states);
    } else if (purchaseOrderType.equals(OrderTypeEnum.IMPORT_PO.name())) {
      return getEnabledAttributesInGivenStates(enabledImportPOAttributesPerState, states);
    }
    return getEnabledAttributesInGivenStates(enabledLocalPOAttributesPerState, states);
  }

  public Set<String> getEnabledAttributesInGivenStates(
      Map<String, Set<String>> enabledAttributesPerState, Set<String> states) {

    Set<String> allEnabledAttributes = new HashSet<>();
    for (String state : states) {
      Set<String> enabledAttributes = enabledAttributesPerState.get(state);
      if (enabledAttributes != null) {
        allEnabledAttributes.addAll(enabledAttributes);
      }
    }

    return allEnabledAttributes;
  }
}
