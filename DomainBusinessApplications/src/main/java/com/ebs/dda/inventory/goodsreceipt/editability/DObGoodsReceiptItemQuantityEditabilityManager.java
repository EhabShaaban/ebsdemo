package com.ebs.dda.inventory.goodsreceipt.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemQuantities;

import java.util.*;

public class DObGoodsReceiptItemQuantityEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> quantityEnabledAttributes = new HashSet<>();
      quantityEnabledAttributes.addAll(
              Arrays.asList(
                      IIObGoodsReceiptItemQuantities.RECEIVED_QTY_UOE,
                      IIObGoodsReceiptItemQuantities.UNIT_OF_ENTRY_CODE,
                      IIObGoodsReceiptItemQuantities.STOCK_TYPE,
                      IIObGoodsReceiptItemQuantities.NOTES));

    enabledAttributesPerState.put(
        DObGoodsReceiptStateMachine.DRAFT_STATE, quantityEnabledAttributes);

    return enabledAttributesPerState;
  }
}
