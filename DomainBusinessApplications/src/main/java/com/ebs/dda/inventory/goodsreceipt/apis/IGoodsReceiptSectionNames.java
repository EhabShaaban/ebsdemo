package com.ebs.dda.inventory.goodsreceipt.apis;

public interface IGoodsReceiptSectionNames {
  String HEADER_SECTION = "HeaderData";
  String ITEMS_SECTION = "ItemsData";
}
