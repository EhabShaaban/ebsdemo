package com.ebs.dda.inventory.goodsreceipt.validation.apis;

import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemBatches;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemQuantities;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptReceivedItemsData;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IGoodsReceiptReceivedItemMandatoryAttributes {

    String[] DRAFT_STATE_ITEM_MANDATORIES = {IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE};
    String[] POSTED_STATE_ITEM_MANDATORIES = {IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE};

    String MISSING_ITEMS_LIST = "ItemsList";
    String MISSING_DATA_ITEM_QUANTITIES = "ItemQuantities";

    Map<String, String[]> ITEM_GENERAL_MODEL_MANDATORIES =
            ImmutableMap.<String, String[]>builder()
                    .put(DObGoodsReceiptStateMachine.DRAFT_STATE, DRAFT_STATE_ITEM_MANDATORIES)
                    .put(DObGoodsReceiptStateMachine.ACTIVE_STATE, POSTED_STATE_ITEM_MANDATORIES)
                    .build();

    String[] ITEM_BATCH_GENERAL_MODEL_MANDATORIES = {
            IIObGoodsReceiptItemBatches.BATCH_CODE,
            IIObGoodsReceiptItemBatches.PRODUCTION_DATE,
            IIObGoodsReceiptItemBatches.EXPIRATION_DATE,
            IIObGoodsReceiptItemBatches.RECEIVED_QTY_UOE,
            IIObGoodsReceiptItemBatches.UNIT_OF_ENTRY_CODE,
            IIObGoodsReceiptItemBatches.STOCK_TYPE
    };

  String[] ITEM_QUANTITY_GENERAL_MODEL_MANDATORIES = {
    IIObGoodsReceiptItemQuantities.RECEIVED_QTY_UOE,
    IIObGoodsReceiptItemQuantities.UNIT_OF_ENTRY_CODE,
          IIObGoodsReceiptItemQuantities.STOCK_TYPE
  };

  Map<String, String[]> ITEM_BACTHES_GENERAL_MODEL_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObGoodsReceiptStateMachine.DRAFT_STATE, ITEM_BATCH_GENERAL_MODEL_MANDATORIES)
          .put(DObGoodsReceiptStateMachine.ACTIVE_STATE, ITEM_BATCH_GENERAL_MODEL_MANDATORIES)
          .build();

  Map<String, String[]> ITEM_QUANTITIES_GENERAL_MODEL_MANDATORIES =
      ImmutableMap.<String, String[]>builder()
          .put(DObGoodsReceiptStateMachine.DRAFT_STATE, ITEM_QUANTITY_GENERAL_MODEL_MANDATORIES)
          .put(DObGoodsReceiptStateMachine.ACTIVE_STATE, ITEM_QUANTITY_GENERAL_MODEL_MANDATORIES)
          .build();
}
