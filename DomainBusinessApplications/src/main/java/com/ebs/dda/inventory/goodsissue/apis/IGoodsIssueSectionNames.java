package com.ebs.dda.inventory.goodsissue.apis;

public interface IGoodsIssueSectionNames {
    String ITEMS_SECTION = "Item";
}
