package com.ebs.dda.inventory.goodsreceipt.validation.apis;

import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IGoodsReceiptGeneralDataMandatoryAttributes {

  String[] POSTED_STATE_MANDATORIES = {
          IDObGoodsReceiptGeneralModel.TYPE,
          IDObGoodsReceiptGeneralModel.STOREHOUSE_CODE,
          IDObGoodsReceiptGeneralModel.STOREKEEPER_CODE
  };
  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder()
          .put(DObGoodsReceiptStateMachine.ACTIVE_STATE, POSTED_STATE_MANDATORIES)
          .build();
}
