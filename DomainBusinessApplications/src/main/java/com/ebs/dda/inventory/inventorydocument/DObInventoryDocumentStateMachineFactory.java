package com.ebs.dda.inventory.inventorydocument;

import com.ebs.dac.foundation.exceptions.operational.other.InvalidStateMachineException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.inventory.initialstockupload.statemachines.DObInitialStockUploadStateMachine;
import com.ebs.dda.inventory.stocktransformation.statemachines.DObStockTransformationStateMachine;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUpload;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformation;

public class DObInventoryDocumentStateMachineFactory {

  public AbstractStateMachine createStateMachine(DObInventoryDocument inventoryDocument)
          throws Exception {
    if (inventoryDocument instanceof DObGoodsIssue) {
      DObGoodsIssueStateMachine goodsIssueStateMachine = new DObGoodsIssueStateMachine();
      goodsIssueStateMachine.initObjectState(inventoryDocument);
      return goodsIssueStateMachine;
    }
    if (inventoryDocument instanceof DObGoodsReceipt) {
      DObGoodsReceiptStateMachine dObGoodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
      dObGoodsReceiptStateMachine.initObjectState(inventoryDocument);
      return dObGoodsReceiptStateMachine;
    }
    if (inventoryDocument instanceof DObStockTransformation) {
      DObStockTransformationStateMachine stockTransformationStateMachine =
              new DObStockTransformationStateMachine();
      stockTransformationStateMachine.initObjectState(inventoryDocument);
      return stockTransformationStateMachine;
    }
    if (inventoryDocument instanceof DObInitialStockUpload) {
      DObInitialStockUploadStateMachine initialStockUploadStateMachine =
              new DObInitialStockUploadStateMachine();
      initialStockUploadStateMachine.initObjectState(inventoryDocument);
      return initialStockUploadStateMachine;
    }
    throw new InvalidStateMachineException(
            "DObInventoryDocumentStateMachineFactory : No suitable state machine for this DObInventoryDocument");
  }
}
