package com.ebs.dda.inventory.goodsreceipt.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptReceivedItemMandatoryAttributes;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.*;

public class DObGoodsReceiptItemsSectionValidator implements ISectionMandatoryValidator {
  private String GR_PO_INVENTORY_DOCUMENT_TYPE = "GR_PO";

  private ValidationResult validationResult;
  private MandatoryValidatorUtils validatorUtils;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep;
  private IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep itemBatchesGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
      grPurchaseOrderItemQuantitiesGeneralModelRep;
  private IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
      grSalesReturnQuantitiesGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      goodsReceiptPurchaseOrderDataGeneralModelRep;

  public DObGoodsReceiptItemsSectionValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public ValidationResult validate(IObGoodsReceiptReceivedItemsDataValueObject itemsDataValueObject)
      throws Exception {
    validationResult = new ValidationResult();
    String grCode = itemsDataValueObject.getGoodsReceiptCode();
    String itemCode = itemsDataValueObject.getItemCode();

    try {
      Long.parseLong(itemCode);
    } catch (Exception ex) {
      throw new ArgumentViolationSecurityException();
    }
    checkIfItemExistsInGoodsReceipt(itemCode, grCode);
    checkIfOrderItemHasAtLeastOneItemVendorRecord(itemCode, grCode);
    return validationResult;
  }

  private void checkIfItemExistsInGoodsReceipt(String itemCode, String grCode)
      throws ArgumentViolationSecurityException {
    IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptRecievedItemsGeneralModel =
        receivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(grCode, itemCode);
    if (goodsReceiptRecievedItemsGeneralModel != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfOrderItemHasAtLeastOneItemVendorRecord(
      String itemCode, String goodsReceiptCode) {
    IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPurchaseOrderDataGeneralModel =
        goodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(goodsReceiptCode);
    String vendorCode = goodsReceiptPurchaseOrderDataGeneralModel.getVendorCode();
    String purchaseUnitCode = goodsReceiptPurchaseOrderDataGeneralModel.getPurchaseUnitCode();
    List<ItemVendorRecordGeneralModel> itemVendorRecordGeneralModels =
        itemVendorRecordGeneralModelRep.findByItemCodeAndVendorCodeAndPurchasingUnitCode(
            itemCode, vendorCode, purchaseUnitCode);
    if (itemVendorRecordGeneralModels.isEmpty()) {
      validationResult.bindError(
          IGoodsReceiptItemValueObject.ITEM_CODE, IExceptionsCodes.GOODS_RECEIPT_ITEM_NOT_IN_IVR);
    }
  }


  @Override
  public void validateValueObjectMandatories(Object o) throws Exception {}

  @Override
  public Object validateGeneralModelMandatories(String code, String state) {
    Map<String, List<String>> missingFields = new HashMap<>();

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
            goodsReceiptGeneralModelRep.findOneByUserCode(code);

    List<IObGoodsReceiptReceivedItemsGeneralModel> receivedItems =
            receivedItemsGeneralModelRep.findByGoodsReceiptCode(goodsReceiptGeneralModel.getUserCode());

    if (receivedItems.isEmpty()) {
      return Collections.singletonList(IGoodsReceiptReceivedItemMandatoryAttributes.MISSING_ITEMS_LIST);
    }

    for (IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel : receivedItems) {
      String[] itemMandatoryAttributes =
              validatorUtils.getMandatoryAttributesByState(
                      IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_GENERAL_MODEL_MANDATORIES, state);

      List<String> missingItemFields =
              validatorUtils.validateMandatoriesForGeneralModel(
                      itemMandatoryAttributes, itemGeneralModel);

      if (isBatchManagedItem(itemGeneralModel)) {
        validateBatchedItem(
                goodsReceiptGeneralModel.getUserCode(), itemGeneralModel, missingItemFields, state);
      } else {
        validateOrdinaryItem(goodsReceiptGeneralModel, itemGeneralModel, missingItemFields);
      }

      if (!missingItemFields.isEmpty()) {
        missingFields.put(itemGeneralModel.getItemCode(), missingItemFields);
      }
    }
    return missingFields;
  }

  private void validateOrdinaryItem(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel,
      IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel,
      List<String> missingItemFields) {
    boolean isEmptyItemQty = false;
    if (goodsReceiptGeneralModel.getInventoryDocumentType().equals(GR_PO_INVENTORY_DOCUMENT_TYPE)) {
      List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel> itemQuantities =
          getGRPurchaseOrderItemQuantities(
              goodsReceiptGeneralModel.getUserCode(), itemGeneralModel);
      isEmptyItemQty = itemQuantities.isEmpty();
    } else {
      List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel> itemQuantities =
          getGRSalesReturnItemQuantities(goodsReceiptGeneralModel.getUserCode(), itemGeneralModel);
      isEmptyItemQty = itemQuantities.isEmpty();
    }
    if (isEmptyItemQty) {
      setMissingItemQuantitiesData(missingItemFields);
    }
  }

  private void validateBatchedItem(
      String goodsReceiptCode,
      IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel,
      List<String> missingItemFields,
      String state) {
    List<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel> itemBatches =
        getItemBatches(goodsReceiptCode, itemGeneralModel);
    if (itemBatches.isEmpty()) {
      setMissingItemQuantitiesData(missingItemFields);
    }
    for (IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel batch : itemBatches) {
      String[] batchMandatoryAttributes =
          validatorUtils.getMandatoryAttributesByState(
              IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_BACTHES_GENERAL_MODEL_MANDATORIES,
              state);
      List<String> missingBatchFields =
          validatorUtils.validateMandatoriesForGeneralModel(batchMandatoryAttributes, batch);
      if (!missingBatchFields.isEmpty()) {
        setMissingItemQuantitiesData(missingItemFields);
        break;
      }
    }
  }

  private void setMissingItemQuantitiesData(List<String> missingItemFields) {
    missingItemFields.add(
        IGoodsReceiptReceivedItemMandatoryAttributes.MISSING_DATA_ITEM_QUANTITIES);
  }

  private List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
      getGRPurchaseOrderItemQuantities(
          String goodsReceiptCode, IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel) {
    return grPurchaseOrderItemQuantitiesGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
        goodsReceiptCode, itemGeneralModel.getItemCode());
  }

  private List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
      getGRSalesReturnItemQuantities(
          String goodsReceiptCode, IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel) {
    return grSalesReturnQuantitiesGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
        goodsReceiptCode, itemGeneralModel.getItemCode());
  }

  private boolean isBatchManagedItem(IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel) {
    return itemGeneralModel
        .getObjectTypeCode()
        .equals(IIObGoodsReceiptReceivedItemsData.BATCHED_ITEMS_OBJECT_TYPE_CODE);
  }

  private List<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel> getItemBatches(
      String goodsReceiptCode, IObGoodsReceiptReceivedItemsGeneralModel itemGeneralModel) {
    return itemBatchesGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
        goodsReceiptCode, itemGeneralModel.getItemCode());
  }

  public void setReceivedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep) {
    this.receivedItemsGeneralModelRep = receivedItemsGeneralModelRep;
  }

  public void setItemBatchesGeneralModelRep(
      IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep itemBatchesGeneralModelRep) {
    this.itemBatchesGeneralModelRep = itemBatchesGeneralModelRep;
  }

  public void setGrPurchaseOrderItemQuantitiesGeneralModelRep(
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
          grPurchaseOrderItemQuantitiesGeneralModelRep) {
    this.grPurchaseOrderItemQuantitiesGeneralModelRep =
        grPurchaseOrderItemQuantitiesGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          goodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.goodsReceiptPurchaseOrderDataGeneralModelRep =
        goodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setGrSalesReturnQuantitiesGeneralModelRep(
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
          grSalesReturnQuantitiesGeneralModelRep) {
    this.grSalesReturnQuantitiesGeneralModelRep = grSalesReturnQuantitiesGeneralModelRep;
  }

  public void setGoodReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }
}
