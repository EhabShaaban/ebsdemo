package com.ebs.dda.inventory.goodsreceipt.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class PurchaseOrderHasInvalidLandedCostException extends BusinessRuleViolationException {
  public PurchaseOrderHasInvalidLandedCostException(ValidationResult validationResult) {
    super(validationResult);
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.GOODS_RECEIPT_REF_DOCUMENT_HAS_INVALID_LANDED_COST;
  }
}
