package com.ebs.dda.inventory.inventorydocument;

public interface IDObInventoryActionNames {
    String READ_GENERAL_DATA = "ReadGeneralData";
    String READ_COMPANY = "ReadCompany";
    String ACTIVATE = "Activate";
}
