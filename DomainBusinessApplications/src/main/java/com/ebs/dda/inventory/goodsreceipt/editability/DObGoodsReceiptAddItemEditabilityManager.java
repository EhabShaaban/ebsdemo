package com.ebs.dda.inventory.goodsreceipt.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptReceivedItemsData;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DObGoodsReceiptAddItemEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> draftEnabledAttributes = new HashSet<>();
      draftEnabledAttributes.addAll(Arrays.asList(IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE));

    enabledAttributesPerState.put(DObGoodsReceiptStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(DObGoodsReceiptStateMachine.ACTIVE_STATE, new HashSet<>());

    return enabledAttributesPerState;
  }
}
