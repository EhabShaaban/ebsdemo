package com.ebs.dda.inventory.goodsreceipt.validation;

import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptGeneralDataMandatoryAttributes;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;

public class DObGoodsReceiptGeneralDataSectionValidator implements ISectionMandatoryValidator {

  private MandatoryValidatorUtils validatorUtils;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;

  public DObGoodsReceiptGeneralDataSectionValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public List<String> validateGeneralModelMandatories(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel, String state) throws Exception {

    String[] mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            IGoodsReceiptGeneralDataMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL, state);
    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, goodsReceiptGeneralModel);
  }

  @Override
  public void validateValueObjectMandatories(Object o) throws Exception {}

  @Override
  public List<String> validateGeneralModelMandatories(String code, String state) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(code);

    String[] mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            IGoodsReceiptGeneralDataMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL, state);

    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, goodsReceiptGeneralModel);
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }
}
