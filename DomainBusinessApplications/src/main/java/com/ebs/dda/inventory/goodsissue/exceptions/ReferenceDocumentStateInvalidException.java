package com.ebs.dda.inventory.goodsissue.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class ReferenceDocumentStateInvalidException extends BusinessRuleViolationException {
    public ReferenceDocumentStateInvalidException(ValidationResult validationResult) {
        super(validationResult);
    }
    @Override
    public String getResponseCode() {
        return IExceptionsCodes.REF_DOCUMENT_STATE_IS_INVALID;
    }
}
