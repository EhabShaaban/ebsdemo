package com.ebs.dda.inventory.goodsissue.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsissue.apis.IGoodsIssueSectionNames;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IIObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.stockavailability.DObStockAvailabilityGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DObGoodsIssueValidator {

  private static final String UNRESTRICTED_STOCK_TYPE = "UNRESTRICTED_USE";
  private static final String GOODS_ISSUE_SALES_ORDER_TYPE = "BASED_ON_SALES_ORDER";
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep;
  private IObGoodsIssueItemGeneralModelRep goodsIssueItemsGeneralModelRep;
  private DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep;

  private ValidationResult validationResult;

  public DObGoodsIssueValidator() {}

  public ValidationResult validate(DObGoodsIssueGeneralModel goodsIssueGeneralModel) {
    validationResult = new ValidationResult();

    String goodsIssueGeneralModelTypeNameEn = goodsIssueGeneralModel.getTypeCode();
    if (goodsIssueGeneralModelTypeNameEn.equals(GOODS_ISSUE_SALES_ORDER_TYPE)) {
      IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
          goodsIssueRefDocumentDataGeneralModelRep.findOneByGoodsIssueCode(
              goodsIssueGeneralModel.getUserCode());
      validateSalesOrderStateIsValid(goodsIssueRefDocumentDataGeneralModel.getRefDocument());
    }

    List<String> items = new ArrayList<String>();

    List<IObGoodsIssueItemGeneralModel> goodsIssueItems =
        goodsIssueItemsGeneralModelRep.findByGoodsIssueCodeOrderByIdDesc(
            goodsIssueGeneralModel.getUserCode());

    for (IObGoodsIssueItemGeneralModel goodsIssueItem : goodsIssueItems) {

      CObStockAvailabilitiesGeneralModel itemStockAvailability =  getItemRemainingAmount(goodsIssueGeneralModel, goodsIssueItem);

      BigDecimal itemRemainingAmount = itemStockAvailability == null ? BigDecimal.ZERO :
              itemStockAvailability.getAvailableQuantity();

      if (isRemainingLessThanQuantity(goodsIssueItem, itemRemainingAmount)) {
        items.add(
            "Item: "
                + goodsIssueItem.getItemCode()
                + " - "
                + goodsIssueItem.getItemName().getValue("en")
                + " - "
                + goodsIssueItem.getUnitOfMeasureSymbol().getValue("en"));
      }
    }

    if (!items.isEmpty()) {
      bindItemHasQtyMoreThnaRemainingError(items);
    }
    return validationResult;
  }

  private boolean isRemainingLessThanQuantity(IObGoodsIssueItemGeneralModel goodsIssueItem, BigDecimal itemRemainingAmount) {
    return itemRemainingAmount.compareTo(goodsIssueItem.getQuantity()) < 0;
  }

  private void bindItemHasQtyMoreThnaRemainingError(List<String> items) {
    validationResult.bindErrorWithValues(
        IGoodsIssueSectionNames.ITEMS_SECTION,
        IExceptionsCodes.ITEM_HAS_QUANTITY_MORE_THAN_REMAINING_IN_STORE,
        items);
  }

  private void validateSalesOrderStateIsValid(String refDocumentCode) {
    DObSalesOrderGeneralModel approvedSalesOrderGeneralModel =
            salesOrderGeneralModelRep.findOneByUserCodeAndCurrentStatesLike(
                    refDocumentCode, "%" + DObSalesOrderStateMachine.APPROVED + "%");
    DObSalesOrderGeneralModel salesInvoiceActivatedSalesOrderGeneralModel =
            salesOrderGeneralModelRep.findOneByUserCodeAndCurrentStatesLike(
                    refDocumentCode, "%" + DObSalesOrderStateMachine.SALES_INVOICE_ACTIVATED + "%");
    if (approvedSalesOrderGeneralModel == null
            && salesInvoiceActivatedSalesOrderGeneralModel == null) {
      validationResult.bindError(
              IIObGoodsIssueRefDocumentDataGeneralModel.REF_DOCUMENT,
              IExceptionsCodes.REF_DOCUMENT_STATE_IS_INVALID);
    }
  }

  private CObStockAvailabilitiesGeneralModel getItemRemainingAmount(
      DObGoodsIssueGeneralModel goodsIssueGeneralModel,
      IObGoodsIssueItemGeneralModel goodsIssueItem) {
    String companyCode = goodsIssueGeneralModel.getCompanyCode();
    String plantCode = goodsIssueGeneralModel.getPlantCode();
    String storehouseCode = goodsIssueGeneralModel.getStorehouseCode();
    String businessUnitCode = goodsIssueGeneralModel.getPurchaseUnitCode();
    String UnitOfMeasureCode = goodsIssueItem.getUnitOfMeasureCode();

    CObStockAvailabilitiesGeneralModel itemStockAvailability =
            stockAvailabilityGeneralModelRep
                    .findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCode(
                            goodsIssueItem.getItemCode(),
                            companyCode,
                            plantCode,
                            storehouseCode,
                            businessUnitCode,
                            UNRESTRICTED_STOCK_TYPE,
                            UnitOfMeasureCode);

    return itemStockAvailability;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setGoodsIssueRefDocumentDataGeneralModelRep(
          IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep) {
    this.goodsIssueRefDocumentDataGeneralModelRep = goodsIssueRefDocumentDataGeneralModelRep;
  }

  public void setGoodsIssueItemsGeneralModelRep(
          IObGoodsIssueItemGeneralModelRep goodsIssueItemsGeneralModelRep) {
    this.goodsIssueItemsGeneralModelRep = goodsIssueItemsGeneralModelRep;
  }

  public void setStockAvailabilityGeneralModelRep(DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep) {
    this.stockAvailabilityGeneralModelRep = stockAvailabilityGeneralModelRep;
  }
}
