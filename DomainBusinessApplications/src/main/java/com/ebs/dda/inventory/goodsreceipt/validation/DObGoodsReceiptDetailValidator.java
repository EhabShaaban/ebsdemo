package com.ebs.dda.inventory.goodsreceipt.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.ValidationUtils;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptReceivedItemMandatoryAttributes;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.List;

public class DObGoodsReceiptDetailValidator extends AbstractValidator {

  private final String GR_PO = "PURCHASE_ORDER";
  private final String GR_SR = "SALES_RETURN";
  private ItemVendorRecordUnitOfMeasuresGeneralModelRep
      itemVendorRecordUnitOfMeasuresGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      iObGoodsReceiptPurchaseOrderDataGeneralModelRep;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
      goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep;
  private IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
      goodsReceiptSalesReturnItemQuantitiesGeneralModelRep;
  private IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
      iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep;
  private IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private ValidationResult validationResult;
  private ValidationUtils validationUtils;

  @Override
  public ValidationResult validate(Object valueObject) throws Exception {
    IObGoodsReceiptItemDetailValueObject detailValueObject =
        (IObGoodsReceiptItemDetailValueObject) valueObject;
    validationResult = new ValidationResult();
    validationUtils = new ValidationUtils();

    if (((IObGoodsReceiptItemDetailValueObject) valueObject).getGoodsReceiptType().equals(GR_PO)) {
      validateUnitOfEntryForGoodsReceiptPurchaseOrder(detailValueObject);
    }
    validateMissingMandatories(detailValueObject);
    if (detailValueObject.getDetailType().equals("Batch")) {
      checkIfProductionDateIsBeforeExpirationDate(
          (IObGoodsReceiptItemBatchValueObject) detailValueObject);
      validateUoeAndStockTypeAndBatchCodeNotExistInTheItem(
          (IObGoodsReceiptItemBatchValueObject) valueObject);
    } else if (!validationResult.hasError(IIObGoodsReceiptItemDetailValueObject.UOE_CODE)) {
      validateUomAndStockTypeNotExistInTheItem((IObGoodsReceiptItemDetailValueObject) valueObject);
    }

    if (!((IObGoodsReceiptItemDetailValueObject) valueObject).getGoodsReceiptType().equals(GR_PO)
        && !validationResult.hasError(IIObGoodsReceiptItemDetailValueObject.ITEM_DETAIL)) {
      validateUnitOfEntryForGoodsReceiptSalesReturn(detailValueObject);
      validateGoodsReceiptTotalItemQuantitiesDoesNotExceedSalesReturnQty(detailValueObject);
    }
    return validationResult;
  }

  private void validateGoodsReceiptTotalItemQuantitiesDoesNotExceedSalesReturnQty(
      IObGoodsReceiptItemDetailValueObject detailValueObject) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(detailValueObject.getGoodsReceiptCode());
    List<IObSalesReturnOrderItemGeneralModel> salesReturnOrderItemGeneralModels =
        getReturnedItemGeneralModels(detailValueObject, goodsReceiptGeneralModel);
    BigDecimal salesReturnItemQtySummation =
        getSalesReturnItemQtySummation(salesReturnOrderItemGeneralModels);
    List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
        salesReturnItemQuantitiesGeneralModels =
            getGoodsReceiptReturnedItemGeneralModels(detailValueObject);
    BigDecimal goodsReceiptSalesReturnItemQtySummation =
        getGoodsReceiptSalesReturnItemQtySummation(salesReturnItemQuantitiesGeneralModels);
    if ((goodsReceiptSalesReturnItemQtySummation.add(detailValueObject.getReceivedQtyUoE()))
            .compareTo(salesReturnItemQtySummation)
        > 0) {
      validationResult.bindError(
          IIObGoodsReceiptItemDetailValueObject.ITEM_DETAIL,
          IExceptionsCodes.GOODS_RECEIPT_ITEM_QUANTITIES_EXCEED_ITEM_RETURNED_QUANTITY);
    }
  }

  private List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
      getGoodsReceiptReturnedItemGeneralModels(
          IObGoodsReceiptItemDetailValueObject detailValueObject) {
    if (detailValueObject.getId() != null) {
      return goodsReceiptSalesReturnItemQuantitiesGeneralModelRep
          .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndIdNot(
              detailValueObject.getGoodsReceiptCode(),
              detailValueObject.getItemCode(),
              detailValueObject.getUnitOfEntryCode(),
              detailValueObject.getId());
    }
    return goodsReceiptSalesReturnItemQuantitiesGeneralModelRep
        .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCode(
            detailValueObject.getGoodsReceiptCode(),
            detailValueObject.getItemCode(),
            detailValueObject.getUnitOfEntryCode());
  }

  private List<IObSalesReturnOrderItemGeneralModel> getReturnedItemGeneralModels(
      IObGoodsReceiptItemDetailValueObject detailValueObject,
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) {
    return salesReturnOrderItemGeneralModelRep
        .findAllBySalesReturnOrderCodeAndItemCodeAndOrderUnitCode(
            goodsReceiptGeneralModel.getRefDocumentCode(),
            detailValueObject.getItemCode(),
            detailValueObject.getUnitOfEntryCode());
  }

  private BigDecimal getGoodsReceiptSalesReturnItemQtySummation(
      List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
          salesReturnItemQuantitiesGeneralModels) {
    BigDecimal qty = BigDecimal.ZERO;
    for (IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
        iObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel :
            salesReturnItemQuantitiesGeneralModels) {
      qty = qty.add(iObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.getReceivedQtyUoE());
    }
    return qty;
  }

  private BigDecimal getSalesReturnItemQtySummation(
      List<IObSalesReturnOrderItemGeneralModel> salesReturnOrderItemGeneralModels) {
    BigDecimal qty = BigDecimal.ZERO;
    for (IObSalesReturnOrderItemGeneralModel iObSalesReturnOrderItemGeneralModel :
        salesReturnOrderItemGeneralModels) {
      qty = qty.add(iObSalesReturnOrderItemGeneralModel.getReturnQuantity());
    }
    return qty;
  }

  private void validateUnitOfEntryForGoodsReceiptSalesReturn(
      IObGoodsReceiptItemDetailValueObject detailValueObject) {
    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(detailValueObject.getGoodsReceiptCode());
    IObSalesReturnOrderItemGeneralModel salesReturnOrderItem =
        salesReturnOrderItemGeneralModelRep
            .findOneBySalesReturnOrderCodeAndItemCodeAndOrderUnitCode(
                goodsReceipt.getRefDocumentCode(),
                detailValueObject.getItemCode(),
                detailValueObject.getUnitOfEntryCode());
    if (salesReturnOrderItem == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateUomAndStockTypeNotExistInTheItem(
      IObGoodsReceiptItemDetailValueObject valueObject) {

    Object ordinaryReceivedItemGeneralModel = null;

    switch (valueObject.getGoodsReceiptType()) {
      case GR_PO:
        if (inEditMode(valueObject)) {
          ordinaryReceivedItemGeneralModel =
              goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep
                  .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndIdNot(
                      valueObject.getGoodsReceiptCode(),
                      valueObject.getItemCode(),
                      valueObject.getUnitOfEntryCode(),
                      valueObject.getStockType().name(),
                      valueObject.getId());
        } else {
          ordinaryReceivedItemGeneralModel =
              goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep
                  .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockType(
                      valueObject.getGoodsReceiptCode(),
                      valueObject.getItemCode(),
                      valueObject.getUnitOfEntryCode(),
                      valueObject.getStockType().name());
        }
        break;
      case GR_SR:
        if (inEditMode(valueObject)) {
          ordinaryReceivedItemGeneralModel =
              goodsReceiptSalesReturnItemQuantitiesGeneralModelRep
                  .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndIdNot(
                      valueObject.getGoodsReceiptCode(),
                      valueObject.getItemCode(),
                      valueObject.getUnitOfEntryCode(),
                      valueObject.getStockType().name(),
                      valueObject.getId());
        } else {
          ordinaryReceivedItemGeneralModel =
              goodsReceiptSalesReturnItemQuantitiesGeneralModelRep
                  .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockType(
                      valueObject.getGoodsReceiptCode(),
                      valueObject.getItemCode(),
                      valueObject.getUnitOfEntryCode(),
                      valueObject.getStockType().name());
        }
        break;
    }

    if (ordinaryReceivedItemGeneralModel != null) {
      validationResult.bindError(
          IIObGoodsReceiptItemDetailValueObject.ITEM_DETAIL,
          IExceptionsCodes.GOODS_RECEIPT_ITEM_EXIST_WITH_SAME_UOM_STOCKTYPE);
    }
  }

  private boolean inEditMode(IObGoodsReceiptItemDetailValueObject valueObject) {
    return valueObject.getId() != null;
  }

  private void validateUoeAndStockTypeAndBatchCodeNotExistInTheItem(
      IObGoodsReceiptItemBatchValueObject valueObject) {
    IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel
        iObGoodsReceiptBatchedReceivedItemBatchesGeneralModel =
            iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
                .findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndBatchCode(
                    valueObject.getGoodsReceiptCode(),
                    valueObject.getItemCode(),
                    valueObject.getUnitOfEntryCode(),
                    valueObject.getStockType().name(),
                    valueObject.getBatchCode());
    if (iObGoodsReceiptBatchedReceivedItemBatchesGeneralModel != null) {
      validationResult.bindError(
          IIObGoodsReceiptItemDetailValueObject.ITEM_DETAIL,
          IExceptionsCodes.GOODS_RECEIPT_ITEM_EXIST_WITH_SAME_UOM_STOCKTYPE_BATCHCODE);
    }
  }

  private void validateMissingMandatories(IObGoodsReceiptItemDetailValueObject valueObject)
      throws Exception {
    if (valueObject.getDetailType().equals("Batch")) {
      for (String attr :
          IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_BATCH_GENERAL_MODEL_MANDATORIES) {
        Object attrValue = validationUtils.getAttributeValue(attr, valueObject);
        checkIfValueDoesNotExistForMandatoryAttr(attrValue);
      }
    } else {
      for (String attr :
          IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_QUANTITY_GENERAL_MODEL_MANDATORIES) {
        Object attrValue = validationUtils.getAttributeValue(attr, valueObject);
        checkIfValueDoesNotExistForMandatoryAttr(attrValue);
      }
    }
  }

  private void validateUnitOfEntryForGoodsReceiptPurchaseOrder(
      IObGoodsReceiptItemDetailValueObject valueObject) {
    IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(
            valueObject.getGoodsReceiptCode());

    ItemVendorRecordUnitOfMeasuresGeneralModel itemVendorRecordUoM =
        itemVendorRecordUnitOfMeasuresGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndPurchasingUnitCodeAndAlternativeUnitOfMeasureCode(
                valueObject.getItemCode(),
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getVendorCode(),
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getPurchaseUnitCode(),
                valueObject.getUnitOfEntryCode());

    String basicUnitOfMeasureCode = getItemBasicUnitOfMeasureCode(valueObject.getItemCode());

    ItemVendorRecordGeneralModel ivrUoMRecord =
        itemVendorRecordGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                valueObject.getItemCode(),
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getVendorCode(),
                basicUnitOfMeasureCode,
                iObGoodsReceiptPurchaseOrderDataGeneralModel.getPurchaseUnitCode());

    if (itemVendorRecordUoM == null && ivrUoMRecord == null) {
      validationResult.bindError(
          IIObGoodsReceiptItemDetailValueObject.UOE_CODE,
          IExceptionsCodes.GOODS_RECEIPT_MEASURE_IN_IVR);
    }
  }

  private String getItemBasicUnitOfMeasureCode(String itemCode) {
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    return item.getBasicUnitOfMeasureCode();
  }

  private void checkIfValueDoesNotExistForMandatoryAttr(Object value)
      throws ArgumentViolationSecurityException {
    if (value == null || value.toString().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfProductionDateIsBeforeExpirationDate(
      IObGoodsReceiptItemBatchValueObject valueObject) throws ArgumentViolationSecurityException {
    if (!getDateTime(valueObject.getProductionDate())
        .isBefore(getDateTime(valueObject.getExpirationDate()))) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
    return formatter.parseDateTime(dateTime);
  }

  public void setItemVendorRecordUnitOfMeasuresGeneralModelRep(
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep) {
    this.itemVendorRecordUnitOfMeasuresGeneralModelRep =
        itemVendorRecordUnitOfMeasuresGeneralModelRep;
  }

  public void setiObGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.iObGoodsReceiptPurchaseOrderDataGeneralModelRep =
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep(
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
          goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep) {
    this.goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep =
        goodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep;
  }

  public void setiObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep(
      IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
          iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep) {
    this.iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep =
        iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep;
  }

  public void setGoodsReceiptSalesReturnItemQuantitiesGeneralModelRep(
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
          goodsReceiptSalesReturnItemQuantitiesGeneralModelRep) {
    this.goodsReceiptSalesReturnItemQuantitiesGeneralModelRep =
        goodsReceiptSalesReturnItemQuantitiesGeneralModelRep;
  }

  public void setSalesReturnOrderItemGeneralModelRep(
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep) {
    this.salesReturnOrderItemGeneralModelRep = salesReturnOrderItemGeneralModelRep;
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }
}
