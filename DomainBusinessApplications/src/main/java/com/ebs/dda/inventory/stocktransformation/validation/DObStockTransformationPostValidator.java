package com.ebs.dda.inventory.stocktransformation.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;

import java.math.BigDecimal;

public class DObStockTransformationPostValidator {

  private TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep;

  private ValidationResult validationResult;

  public DObStockTransformationPostValidator() {}

  public ValidationResult validate(
      DObStockTransformationGeneralModel stockTransformationGeneralModel) {
    validationResult = new ValidationResult();

    String refStoreTransactionCode = stockTransformationGeneralModel.getRefStoreTransactionCode();

    TObStoreTransactionGeneralModel tObStoreTransactionGeneralModel =
        tObStoreTransactionGeneralModelRep.findOneByUserCode(refStoreTransactionCode);

    BigDecimal stockTransformationGeneralModelTransformedQty =
        BigDecimal.valueOf(stockTransformationGeneralModel.getTransformedQty());
    BigDecimal storeTransactionGeneralModelRemainingQuantity =
        tObStoreTransactionGeneralModel.getRemainingQuantity();

    if (stockTransformationGeneralModelTransformedQty.compareTo(
            storeTransactionGeneralModelRemainingQuantity)
        > 0) {
      validationResult.bindError(
          IDObStockTransformationGeneralModel.TRANSFORMED_QUANTITY,
          IExceptionsCodes.STKTR_TRANSFORMED_QTY_GREATER_THAN_ST_REMAINING);
    }

    return validationResult;
  }

  public void settObStoreTransactionGeneralModelRep(
      TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep) {
    this.tObStoreTransactionGeneralModelRep = tObStoreTransactionGeneralModelRep;
  }
}
