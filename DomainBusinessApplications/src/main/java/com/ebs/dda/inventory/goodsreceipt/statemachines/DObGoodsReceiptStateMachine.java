package com.ebs.dda.inventory.goodsreceipt.statemachines;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import org.apache.commons.scxml2.model.ModelException;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 19, 2018 3:10:23 PM
 */
public class DObGoodsReceiptStateMachine extends AbstractStateMachine {

  /* States of State Machine */
  public static final String DRAFT_STATE = "Draft";
  public static final String ACTIVE_STATE = "Active";

  private Set<String> objectStates;

  private boolean initialized;

  private DObGoodsReceipt object;
  private DObGoodsReceiptGeneralModel generalModel;

  public DObGoodsReceiptStateMachine() throws ModelException {
    super(
        DObGoodsReceiptStateMachine.class
            .getClassLoader()
            .getResource("META-INF/DObGoodsReceipt.scxml"));
    initialized = false;
  }

  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    setEntity(obj);
    initialized = true;
  }

  public boolean goToDraft() {
    return true;
  }

  public boolean goToActive() {
    return true;
  }

  @Override
  public boolean resetMachine() {
    // TODO Auto-generated method stub
    return false;
  }

  public void isAllowedAction(String actionName) throws Exception {
    Set<String> currentStates = getEntity().getCurrentStates();
    if (currentStates == null) {
      throw new IllegalArgumentException("Object current states must not be null");
    }
    if (actionName == null || actionName.trim().isEmpty()) {
      throw new IllegalArgumentException("Action name must not be null or empty");
    }
    boolean canFireAction = false;
    for (String state : currentStates) {
      canFireAction = canFireEvent(actionName);
      if (canFireAction) {
        break;
      }
    }
    if (!canFireAction) {
      throw new ActionNotAllowedPerStateException(
          getEntity().getClass(), getEntityId(), getEntity().getCurrentState(), actionName);
    }
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    if (this.object == null && this.generalModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return (T) (this.object == null ? this.generalModel : this.object);
  }

  protected void setEntity(IStatefullBusinessObject obj) {
    if (obj instanceof DObGoodsReceipt) {
      this.object = (DObGoodsReceipt) obj;
    } else {
      this.generalModel = (DObGoodsReceiptGeneralModel) obj;
    }
  }

  protected Long getEntityId() {
    if (this.object == null && this.generalModel == null) {
      throw new IllegalArgumentException("Object is NULL");
    }
    return this.object == null ? this.generalModel.getId() : this.object.getId();
  }
}
