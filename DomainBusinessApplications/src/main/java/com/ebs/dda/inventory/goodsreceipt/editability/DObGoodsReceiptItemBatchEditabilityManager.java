package com.ebs.dda.inventory.goodsreceipt.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemBatches;

import java.util.*;

public class DObGoodsReceiptItemBatchEditabilityManager extends AbstractEditabilityManager {

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<String, Set<String>>();
    Set<String> batchEnabledAttributes = new HashSet<>();
      batchEnabledAttributes.addAll(
              Arrays.asList(
                      IIObGoodsReceiptItemBatches.BATCH_CODE,
                      IIObGoodsReceiptItemBatches.PRODUCTION_DATE,
                      IIObGoodsReceiptItemBatches.EXPIRATION_DATE,
                      IIObGoodsReceiptItemBatches.RECEIVED_QTY_UOE,
                      IIObGoodsReceiptItemBatches.UNIT_OF_ENTRY_CODE,
                      IIObGoodsReceiptItemBatches.STOCK_TYPE,
                      IIObGoodsReceiptItemBatches.NOTES));

    enabledAttributesPerState.put(DObGoodsReceiptStateMachine.DRAFT_STATE, batchEnabledAttributes);

    return enabledAttributesPerState;
  }
}
