package com.ebs.dda.inventory.goodsreceipt.validation;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptItemsDifferenceReasonMandatoryAttributes;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsDifferenceReasonValueObject;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DObGoodsReceiptDifferenceReasonValidator
    implements ISectionMandatoryValidator<IObGoodsReceiptReceivedItemsDifferenceReasonValueObject> {

    private MandatoryValidatorUtils validatorUtils;
    private DObGoodsReceiptPurchaseOrderRep goodsReceiptRep;

    public DObGoodsReceiptDifferenceReasonValidator(DObGoodsReceiptPurchaseOrderRep goodsReceiptRep) {
        this.validatorUtils = new MandatoryValidatorUtils();
        this.goodsReceiptRep = goodsReceiptRep;
    }

  public ValidationResult validate(
      IObGoodsReceiptReceivedItemsDifferenceReasonValueObject valueObject)
      throws Exception {
    validateValueObjectMandatories(valueObject);
    return new ValidationResult();
  }

  @Override
  public void validateValueObjectMandatories(
      IObGoodsReceiptReceivedItemsDifferenceReasonValueObject valueObject) throws Exception {
    DObGoodsReceipt goodsReceiptGM =
        goodsReceiptRep.findOneByUserCode(valueObject.getGoodsReceiptCode());

    Map<String, String[]> mandatories =
        IGoodsReceiptItemsDifferenceReasonMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL;
    Set<String> mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            mandatories, goodsReceiptGM.getCurrentStates());

    validatorUtils.validateMandatoriesForValueObject(mandatoryAttributes, valueObject);
  }

  @Override
  public List<String> validateGeneralModelMandatories(String code, String state) throws Exception {
    return new ArrayList<>();
  }
}
