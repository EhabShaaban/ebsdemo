package com.ebs.dda.inventory.stocktransformation;

public interface IDObStockTransformationActionNames {
    String READ_REASONS = "ReadReasons";
    String READ_TYPES = "ReadTypes";
    String ACTIVATE = "Activate";
}
