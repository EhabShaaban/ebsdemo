package com.ebs.dda.inventory.goodsreceipt.apis;

public interface IGoodsReceiptActionNames {

  String VIEW_RECEIVED_ITEMS_SECTION = "ReadItems";
  String VIEW_PURCHASE_ORDER_SECTION = "ReadPOData";
  String VIEW_SALES_RETURN_DATA = "ReadSalesReturnData";
  String VIEW_HEADER_SECTION = "ReadHeader";
  String UPDATE_ITEM = "UpdateItem";
  String Edit_DIFFERENCE_REASON = "EditDifferentReason";
  String CALCULATE_ACTUAL_COST = "CalculateActualCost";
  String READ_COMPANY = "ReadCompany";
  String READ_ACCOUNTING_DETAILS = "ReadAccountingDetails";
  String READ_ACCOUNTING_DOCUMENT_ACTIVATION_DETAILS = "ReadAccountingDocumentActivationDetails";

}
