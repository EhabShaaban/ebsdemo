package com.ebs.dda.inventory.goodsreceipt.validation.apis;

import com.google.common.collect.ImmutableMap;
import java.util.Map;

public interface IGoodsReceiptItemsDifferenceReasonMandatoryAttributes {

  Map<String, String[]> MANDATORIES_FOR_GENERAL_MODEL =
      ImmutableMap.<String, String[]>builder().build();
}
