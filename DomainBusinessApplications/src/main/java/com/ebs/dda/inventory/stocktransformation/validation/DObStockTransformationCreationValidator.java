package com.ebs.dda.inventory.stocktransformation.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.stocktransformation.CObStockTransformationReason;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationCreateValueObject;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformationCreateValueObject;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationReasonRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class DObStockTransformationCreationValidator {

    private ValidationResult validationResult;
    private CObPurchasingUnitGeneralModelRep purchasingUnitRep;
    private TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep;
    private CObStockTransformationReasonRep stockTransformationReasonRep;

    public ValidationResult validate(DObStockTransformationCreateValueObject stockTransformationCreateValueObject) throws Exception {
        validationResult = new ValidationResult();

        checkBusinessUnitExist(stockTransformationCreateValueObject.getPurchaseUnitCode());
        checkStockTypeExist(stockTransformationCreateValueObject.getNewStockType());
        validateTransformationReason(stockTransformationCreateValueObject.getTransformationReasonCode(), stockTransformationCreateValueObject.getNewStockType());
        validateStoreTransaction(stockTransformationCreateValueObject.getStoreTransactionCode(), stockTransformationCreateValueObject.getNewStockType() , stockTransformationCreateValueObject.getTransformedQty());

        return validationResult;
    }

    private void validateTransformationReason(String transformationReasonCode, String newStockType) throws Exception {
        CObStockTransformationReason stockTransformationReason =
                stockTransformationReasonRep.findOneByUserCode(transformationReasonCode);
        checkTransformationReasonExist(stockTransformationReason);
        checkStockTypeMatchesStocktransformationReasonToStockTypeAttribute(newStockType, stockTransformationReason);
    }

    private void checkStockTypeMatchesStocktransformationReasonToStockTypeAttribute(String newStockType, CObStockTransformationReason stockTransformationReason) throws Exception {
        if (stockTransformationReason != null && !newStockType.equals(stockTransformationReason.getToStockType()) && !validationResult.hasError(IDObStockTransformationCreateValueObject.NEW_STOCK_TYPE)){
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkStockTypeExist(String newStockType) throws Exception {
        if(newStockType.isEmpty()){
            throw new ArgumentViolationSecurityException();
        }

        if (!isExsitingtransformationReason(newStockType)){
            validationResult.bindError(
                    IDObStockTransformationCreateValueObject.NEW_STOCK_TYPE,
                    IExceptionsCodes.General_MISSING_FIELD_INPUT);
        }
    }

    private Boolean isExsitingtransformationReason(String newStockType) {
        List<StockTypeEnum.StockType> stockTypes = Arrays.asList(StockTypeEnum.StockType.values());
        Boolean found = false;
        for (StockTypeEnum.StockType stockType : stockTypes){
            if (stockType.toString().equals(newStockType)){
                found = true;
                break;
            }
        }
        return found;
    }

    private void checkBusinessUnitExist(String businessUnitCode) {
        CObPurchasingUnitGeneralModel businessUnit =
                purchasingUnitRep.findOneByUserCode(businessUnitCode);
        if (businessUnit == null) {
            validationResult.bindError(
            IDObStockTransformationCreateValueObject.PURCHASE_UNIT_CODE,
                    IExceptionsCodes.General_MISSING_FIELD_INPUT);
        }
    }

    private void validateStoreTransaction(String storeTransactionCode, String newStockType, Double transformedQty) throws Exception {
        TObStoreTransactionGeneralModel storeTransactionGeneralModel =
                storeTransactionGeneralModelRep.findOneByUserCode(storeTransactionCode);
        checkStoreTransactionsExist(storeTransactionGeneralModel);
        if (storeTransactionGeneralModel!= null && newStockType != null){
            checkStockTypeChanged(newStockType, storeTransactionGeneralModel.getStockType());
            checkStoreTransactionsHasEnoughRemainingQty(storeTransactionGeneralModel, transformedQty);
        }
    }

    private void checkStoreTransactionsHasEnoughRemainingQty(TObStoreTransactionGeneralModel storeTransactionGeneralModel, Double transformedQty) {
        if (storeTransactionGeneralModel != null && BigDecimal.valueOf(transformedQty).compareTo(storeTransactionGeneralModel.getRemainingQuantity()) > 0 ) {
            validationResult.bindError(
                    IDObStockTransformationCreateValueObject.TRANSFORMED_QTY,
                    IExceptionsCodes.STKTR_TRANSFORMED_QTY_GREATER_THAN_ST_REMAINING);
        }
    }

    private void checkStockTypeChanged(String newStockType, String storeTransactionStockType) throws Exception {
        if (newStockType.equals(storeTransactionStockType)){
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkStoreTransactionsExist(TObStoreTransactionGeneralModel storeTransactionGeneralModel) throws Exception {
        if (storeTransactionGeneralModel == null) {
            throw new ArgumentViolationSecurityException();
        }
    }

    private void checkTransformationReasonExist(CObStockTransformationReason stockTransformationReason) {
        if (stockTransformationReason == null) {
            validationResult.bindError(
                    IDObStockTransformationCreateValueObject.TRANSFORMATION_REASON_CODE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
        }
    }

    public void setPurchasingUnitRep(CObPurchasingUnitGeneralModelRep purchasingUnitRep) {
        this.purchasingUnitRep = purchasingUnitRep;
    }

    public void setStoreTransactionGeneralModelRep(TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep) {
        this.storeTransactionGeneralModelRep = storeTransactionGeneralModelRep;
    }

    public void setStockTransformationReasonRep(CObStockTransformationReasonRep stockTransformationReasonRep) {
        this.stockTransformationReasonRep = stockTransformationReasonRep;
    }
}
