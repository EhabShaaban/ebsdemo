package com.ebs.dda.inventory.goodsreceipt.editability;

import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptReceivedItemsDifferenceReasonValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsDifferenceReasonValueObject;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IObGoodsReceiptItemDifferenceReasonEditabilityManager
    extends AbstractEditabilityManager {

  private final DObGoodsReceiptGeneralModelRep
      goodsReceiptHeaderGeneralModelRep;

  public IObGoodsReceiptItemDifferenceReasonEditabilityManager(
      DObGoodsReceiptGeneralModelRep goodsReceiptHeaderGeneralModelRep) {
    this.goodsReceiptHeaderGeneralModelRep = goodsReceiptHeaderGeneralModelRep;
  }

  @Override
  public Map<String, Set<String>> getEnabledAttributesConfig() {
    Map<String, Set<String>> enabledAttributesPerState = new HashMap<>();
    Set<String> draftEnabledAttributes = new HashSet<>();
    draftEnabledAttributes.add(
        IIObGoodsReceiptReceivedItemsDifferenceReasonValueObject.DIFFERENCE_REASON);
    enabledAttributesPerState.put(DObGoodsReceiptStateMachine.DRAFT_STATE, draftEnabledAttributes);
    enabledAttributesPerState.put(DObGoodsReceiptStateMachine.ACTIVE_STATE, draftEnabledAttributes);
    return enabledAttributesPerState;
  }

  public IObGoodsReceiptReceivedItemsDifferenceReasonValueObject removeUnEditableFields(
      IObGoodsReceiptReceivedItemsDifferenceReasonValueObject inputVersionObject) throws Exception {

    String goodsReceiptCode = inputVersionObject.getGoodsReceiptCode();
    DObGoodsReceiptGeneralModel goodsReceiptItemDataGM = goodsReceiptHeaderGeneralModelRep
        .findOneByUserCode(goodsReceiptCode);
    Set<String> enabledAttributes = getEnabledAttributesInGivenStates(
        goodsReceiptItemDataGM.getCurrentStates());

    IObGoodsReceiptReceivedItemsDifferenceReasonValueObject savedValueObject = new IObGoodsReceiptReceivedItemsDifferenceReasonValueObject();
    savedValueObject.setGoodsReceiptCode(inputVersionObject.getGoodsReceiptCode());
    savedValueObject.setItemCode(inputVersionObject.getItemCode());

    for (String enabledAttribute : enabledAttributes) {
      Object inputVersionValue = getAttributeValue(enabledAttribute, inputVersionObject);
      setAttributeValue(enabledAttribute, inputVersionValue, savedValueObject);
    }
    return savedValueObject;
  }

}
