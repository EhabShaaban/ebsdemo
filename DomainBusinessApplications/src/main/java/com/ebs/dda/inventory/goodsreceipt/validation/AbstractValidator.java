package com.ebs.dda.inventory.goodsreceipt.validation;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public abstract class AbstractValidator<T> {

  protected ValidationResult validationResult;

  public AbstractValidator() {
    validationResult = new ValidationResult();
  }

  public abstract ValidationResult validate(T creationValueObject) throws Exception;
}
