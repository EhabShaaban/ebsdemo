package com.ebs.dda.inventory.goodsissue.exceptions;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class ItemQuantityMoreThanInStoreException extends BusinessRuleViolationException {
    public ItemQuantityMoreThanInStoreException(ValidationResult validationResult) {
        super(validationResult);
    }
    @Override
    public String getResponseCode() {
        return IExceptionsCodes.ITEM_HAS_QUANTITY_MORE_THAN_REMAINING_IN_STORE;
    }
}
