package com.ebs.dda.inventory.goodsissue.validation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.CObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueCreateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueCreateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.CObGoodsIssueRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

public class DObGoodsIssueCreateValidator {

  private ValidationResult validationResult;
  private CObGoodsIssueRep goodsIssueRep;
  private CObPurchasingUnitRep purchasingUnitRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private CObPlantGeneralModelRep plantGeneralModelRep;
  private CObStorehouseGeneralModelRep storehouseGeneralModelRep;
  private StoreKeeperRep storeKeeperRep;
  private String GI_SI = "DELIVERY_TO_CUSTOMER";

  public ValidationResult validate(DObGoodsIssueCreateValueObject creationValueObject)
          throws Exception {
    validationResult = new ValidationResult();

    checkThatTypeExists(creationValueObject.getTypeCode());
    checkThatPurchaseUnitExists(creationValueObject.getPurchaseUnitCode());
    checkThatStorekeeperExists(creationValueObject.getStoreKeeperCode());
    if (creationValueObject.getTypeCode().equals(GI_SI)) {
      checkThatStorehouseExists(
              creationValueObject.getStoreHouseCode(), creationValueObject.getCompanyCode());
      checkThatCompanyIsBelongsToSalesInvoice(
              creationValueObject.getCompanyCode(), creationValueObject.getRefDocumentCode());
      checkThatSalesInvoiceExists(
              creationValueObject.getRefDocumentCode(), creationValueObject.getPurchaseUnitCode());
      checkThatSalesInvoiceInAllowedState(creationValueObject.getRefDocumentCode());
    }
    if (!creationValueObject.getTypeCode().equals(GI_SI)) {
      DObSalesOrderGeneralModel dObSalesOrderGeneralModel =
              checkThatSalesOrderExistsWithBusinessUnitAndCompanyAndStorehouse(
                      creationValueObject.getRefDocumentCode(),
                      creationValueObject.getPurchaseUnitCode(),
                      creationValueObject.getCompanyCode(),
                      creationValueObject.getStoreHouseCode());

      checkIfSalesOrderIsAllowedState(dObSalesOrderGeneralModel);
    }
    return validationResult;
  }

  private void checkIfSalesOrderIsAllowedState(DObSalesOrderGeneralModel dObSalesOrderGeneralModel)
          throws ArgumentViolationSecurityException {
    if (dObSalesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.DELIVERY_COMPLETE)
            || dObSalesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.GOODS_ISSUE_ACTIVATED)
            || dObSalesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.READY_FOR_DELIVERY)) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.REF_DOCUMENT_CODE,
              IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
    } else if (!dObSalesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.APPROVED)
            && (!dObSalesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.SALES_INVOICE_ACTIVATED))) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private DObSalesOrderGeneralModel
  checkThatSalesOrderExistsWithBusinessUnitAndCompanyAndStorehouse(
          String refDocumentCode,
          String purchaseUnitCode,
          String companyCode,
          String storeHouseCode)
          throws ArgumentViolationSecurityException {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
            salesOrderGeneralModelRep.findOneByUserCodeAndPurchaseUnitCodeAndCompanyCodeAndStoreCode(
                    refDocumentCode, purchaseUnitCode, companyCode, storeHouseCode);
    checkIfObjectIsNull(salesOrderGeneralModel);
    return salesOrderGeneralModel;
  }

  private void checkIfObjectIsNull(Object object) throws ArgumentViolationSecurityException {
    if (object == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatSalesInvoiceInAllowedState(String salesInvoiceCode) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
            salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    if (salesInvoiceGeneralModel != null
            && salesInvoiceGeneralModel
            .getCurrentStates()
            .contains(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE)) {
        validationResult.bindError(
                IDObGoodsIssueCreateValueObject.REF_DOCUMENT_CODE,
                IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
    }
  }

  private void checkThatCompanyIsBelongsToSalesInvoice(String companyCode, String salesInvoiceCode)
          throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
            salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    if (salesInvoiceGeneralModel != null
            && !salesInvoiceGeneralModel.getCompanyCode().equals(companyCode)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatStorekeeperExists(String storeKeeperCode) {
    StoreKeeper storeKeeper = storeKeeperRep.findOneByCode(storeKeeperCode);
    if (storeKeeper == null) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.STORE_KEEPER,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkThatStorehouseExists(String storehouseCode, String companyCode) {
    CObPlantGeneralModel plant = plantGeneralModelRep.findOneByCompanyCode(companyCode);

    CObStorehouseGeneralModel storehouse =
            storehouseGeneralModelRep.findByUserCodeAndPlantCode(storehouseCode, plant.getUserCode());
    if (storehouse == null) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.STORE_HOUSE,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkThatSalesInvoiceExists(String salesInvoiceCode, String purchaseUnitCode) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
            salesInvoiceGeneralModelRep.findOneByUserCodeAndPurchaseUnitCode(
                    salesInvoiceCode, purchaseUnitCode);
    if (salesInvoiceGeneralModel == null) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.REF_DOCUMENT_CODE,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkThatPurchaseUnitExists(String purchaseUnitCode) {
    CObPurchasingUnit purchaseUnit = purchasingUnitRep.findOneByUserCode(purchaseUnitCode);
    if (purchaseUnit == null) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.PURCHASE_UNIT,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkThatTypeExists(String typeCode) {
    CObGoodsIssue goodsIssueType = goodsIssueRep.findOneByUserCode(typeCode);
    if (goodsIssueType == null) {
      validationResult.bindError(
              IDObGoodsIssueCreateValueObject.TYPE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  public void setGoodsIssueRep(CObGoodsIssueRep goodsIssueRep) {
    this.goodsIssueRep = goodsIssueRep;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setPlantGeneralModelRep(CObPlantGeneralModelRep plantGeneralModelRep) {
    this.plantGeneralModelRep = plantGeneralModelRep;
  }

  public void setStorehouseGeneralModelRep(CObStorehouseGeneralModelRep storehouseGeneralModelRep) {
    this.storehouseGeneralModelRep = storehouseGeneralModelRep;
  }

  public void setStoreKeeperRep(StoreKeeperRep storeKeeperRep) {
    this.storeKeeperRep = storeKeeperRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }
}
