package com.ebs.dda.inventory.goodsreceipt.validation;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MISSING_FIELD_INPUT;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.INVALID_PURCHASE_ORDER_CODE_NO_PLANT;
import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.INVALID_STOREKEEPER_CODE;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptCreationValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptCreationValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.plant.CObPlant;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPlantEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderConsigneeGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPlantEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderConsigneeGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObLocalPurchaseOrderStateMachine;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Ahmed Ali Elfeky & Eslam Mostafa ,
 * @since Jun 24, 2018 4:06:06 PM
 */
public class DObGoodsReceiptCreationValidator {

  private static final String LOCAL_TYPE = "Local";
  private static final String IMPORT_TYPE = "Import";

  private ValidationResult validationResult;
  private DObGoodsReceiptCreationValueObject valueObject;
  private IObOrderPlantEnterpriseDataRep plantEnterpriseDataRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private DObPurchaseOrderConsigneeGeneralModelRep purchaseOrderConsigneeGeneralModelRep;
  private StoreKeeperRep skeeperRep;
  private CObCompanyGeneralModelRep cObCompanyRep;
  private CObStorehouseGeneralModelRep storehouseRep;
  private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  private CObPlantRep plantRep;

  public ValidationResult validate(DObGoodsReceiptCreationValueObject creationValueObject)
      throws Exception {

    init(creationValueObject);
    if (creationValueObject.getType().equals(IDObGoodsReceiptCreationValueObject.PURCHASE_ORDER)) {
      checkIfValidPurchaseOrderStateBasedOnType();
      checkIfPurchaseOrderHasPlant();
      checkIfStorehouseRelatesToPurchaseOrderPlant();
    } else {
      validateRefDocumentState();
      checkIfStorehouseExists();
      checkIfStorehouseRelatesToSalesRetrunOrderCompany();
    }
    checkIfStoreKeeperExists();
    checkIfCompanyExists();
    return validationResult;
  }

  public void validateRefDocumentState() throws Exception {
    if (!isValidSalesReturnState()) {
      validationResult.bindError(
          IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE,
          General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH);
    }
  }

  private void init(DObGoodsReceiptCreationValueObject creationValueObject) {
    validationResult = new ValidationResult();
    this.valueObject = creationValueObject;
  }

  private void checkIfStorehouseRelatesToPurchaseOrderPlant() {
    if (valueObject.getStorehouseCode() != null) {
      DObPurchaseOrderConsigneeGeneralModel consigneeGeneralModel =
          purchaseOrderConsigneeGeneralModelRep.findByUserCode(valueObject.getRefDocumentCode());
      CObStorehouseGeneralModel storehouse =
          storehouseRep.findByUserCodeAndPlantCode(
              valueObject.getStorehouseCode(), consigneeGeneralModel.getPlantCode());
      if (storehouse == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.STOREHOUSE,
            IExceptionsCodes.GOODS_RECEIPT_INSTANCE_NOT_EXIST_STOREHOUSE_MSG);
      }
    }
  }

  private void checkIfStorehouseRelatesToSalesRetrunOrderCompany() {
    if (valueObject.getStorehouseCode() != null) {
      CObStorehouseGeneralModel CObStorehouseGeneralModel =
          getStorehouseBasedOnUserCodeAndCompanyCode();
      if (CObStorehouseGeneralModel == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.STOREHOUSE,
            IExceptionsCodes.GOODS_RECEIPT_INSTANCE_NOT_EXIST_STOREHOUSE_MSG);
      }
    }
  }

  private void checkIfStorehouseExists() {
    if (valueObject.getStorehouseCode() != null) {
      CObStorehouseGeneralModel storehouse =
          storehouseRep.findOneByUserCode(valueObject.getStorehouseCode());
      if (storehouse == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.STOREHOUSE,
            IExceptionsCodes.GOODS_RECEIPT_INSTANCE_NOT_EXIST_STOREHOUSE_MSG);
      }
    }
  }

  private void checkIfPurchaseOrderHasPlant() {
    if (valueObject.getRefDocumentCode() != null) {

      DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(valueObject.getRefDocumentCode());

      IObOrderPlantEnterpriseData plant =
          plantEnterpriseDataRep.findOneByRefInstanceId(purchaseOrderGeneralModel.getId());

      if (plant == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE,
            INVALID_PURCHASE_ORDER_CODE_NO_PLANT);
      }
    }
  }

  private void checkIfStoreKeeperExists() {
    if (valueObject.getStoreKeeperCode() != null) {
      StoreKeeper storeKeeper = skeeperRep.findOneByCode(valueObject.getStoreKeeperCode());
      if (storeKeeper == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.STOREKEEPER, INVALID_STOREKEEPER_CODE);
      }
    }
  }

  private void checkIfCompanyExists() {
    if (valueObject.getCompanyCode() != null) {
      CObCompanyGeneralModel cObCompany =
          cObCompanyRep.findOneByUserCode(valueObject.getCompanyCode());
      if (cObCompany == null) {
        validationResult.bindError(
            IDObGoodsReceiptCreationValueObject.COMPANY, General_MISSING_FIELD_INPUT);
      }
    }
  }

  private boolean checkIfValidPurchaseOrderStateBasedOnType() throws Exception {

    boolean isCleared = false;
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(valueObject.getRefDocumentCode());
    if (purchaseOrderGeneralModel == null) {
      throw new ArgumentViolationSecurityException();
    }
    String orderTypeCode = purchaseOrderGeneralModel.getObjectTypeCode();
    if (orderTypeCode.equals(OrderTypeEnum.IMPORT_PO.name())) {
      checkIfValidImportPurchaseOrderState(purchaseOrderGeneralModel.getCurrentStates());
    }
    if (orderTypeCode.equals(OrderTypeEnum.LOCAL_PO.name())) {
      checkIfValidLocalPurchaseOrderState(purchaseOrderGeneralModel.getCurrentStates());
    }
    return isCleared;
  }

  private boolean isValidSalesReturnState() throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(valueObject.getRefDocumentCode());
    if (salesReturnOrderGeneralModel == null) {
      throw new ArgumentViolationSecurityException();
    }
    Set<String> currentStates = salesReturnOrderGeneralModel.getCurrentStates();
    return currentStates.contains(DObSalesReturnOrderStateMachine.SHIPPED_STATE)
        || currentStates.contains(DObSalesReturnOrderStateMachine.CREDIT_NOTE_ACTIVATED_STATE);
  }

  private void checkIfValidImportPurchaseOrderState(Set<String> currentStates) throws Exception {
    checkIfImportPOStateNotBeforeClearedState(currentStates);
    if (!currentStates.contains(DObImportPurchaseOrderStateMachine.CLEARED_STATE)) {
      bindErrorInvalidPurchaseOrderState();
    }
  }

  private void checkIfValidLocalPurchaseOrderState(Set<String> currentStates) throws Exception {
    checkIfLocalPOStateNotBeforeShippedState(currentStates);
    if (!currentStates.contains(DObLocalPurchaseOrderStateMachine.SHIPPED_STATE)) {
      bindErrorInvalidPurchaseOrderState();
    }
  }

  private void bindErrorInvalidPurchaseOrderState() {
    validationResult.bindError(
        IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE,
        General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH);
  }

  private CObStorehouseGeneralModel getStorehouseBasedOnUserCodeAndCompanyCode() {
    CObCompanyGeneralModel company = cObCompanyRep.findOneByUserCode(valueObject.getCompanyCode());
    CObPlant plant = plantRep.findOneByCompanyId(company.getId());

    CObStorehouseGeneralModel storehouseGeneralModel =
        storehouseRep.findByUserCodeAndPlantCode(
            valueObject.getStorehouseCode(), plant.getUserCode());

    return storehouseGeneralModel;
  }

  private void checkIfImportPOStateNotBeforeClearedState(Set<String> currentStates)
      throws ArgumentViolationSecurityException {
    Set<String> statesBeforeCleared = new HashSet<>();
    statesBeforeCleared.addAll(
        Arrays.asList(
            DObImportPurchaseOrderStateMachine.DRAFT_STATE,
            DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
            DObImportPurchaseOrderStateMachine.WAITING_APPROVAL,
            DObImportPurchaseOrderStateMachine.APPROVED_STATE,
            DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
            DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
            DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
            DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
            DObImportPurchaseOrderStateMachine.CANCELLED_STATE));
    for (String invalidState : statesBeforeCleared) {
      if (currentStates.contains(invalidState)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkIfLocalPOStateNotBeforeShippedState(Set<String> currentStates)
      throws ArgumentViolationSecurityException {
    Set<String> statesBeforeShipped = new HashSet<>();
    statesBeforeShipped.addAll(
        Arrays.asList(
            DObLocalPurchaseOrderStateMachine.DRAFT_STATE,
            DObLocalPurchaseOrderStateMachine.OPEN_FOR_UPDATES,
            DObLocalPurchaseOrderStateMachine.WAITING_APPROVAL,
            DObLocalPurchaseOrderStateMachine.APPROVED_STATE,
            DObLocalPurchaseOrderStateMachine.CONFIRMED_STATE));
    for (String invalidState : statesBeforeShipped) {
      if (currentStates.contains(invalidState)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setcObCompanyRep(CObCompanyGeneralModelRep cObCompanyRep) {
    this.cObCompanyRep = cObCompanyRep;
  }

  public void setPurchaseOrderConsigneeGeneralModelRep(
      DObPurchaseOrderConsigneeGeneralModelRep purchaseOrderConsigneeGeneralModelRep) {
    this.purchaseOrderConsigneeGeneralModelRep = purchaseOrderConsigneeGeneralModelRep;
  }

  public void setSkeeperRep(StoreKeeperRep skeeperRep) {
    this.skeeperRep = skeeperRep;
  }

  public void setStorehouseRep(CObStorehouseGeneralModelRep storehouseRep) {
    this.storehouseRep = storehouseRep;
  }

  public void setPlantEnterpriseDataRep(IObOrderPlantEnterpriseDataRep plantEnterpriseDataRep) {
    this.plantEnterpriseDataRep = plantEnterpriseDataRep;
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setPlantRep(CObPlantRep plantRep) {
    this.plantRep = plantRep;
  }
}
