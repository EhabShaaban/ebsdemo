package com.ebs.dda.inventory.goodsissue.apis;

public interface IDObGoodsIssueActionNames {
    String READ_GENERAL_DATA = "ReadGeneralData";
    String READ_COMPANY = "ReadCompany";
    String READ_SALES_INVOICE_DATA = "ReadSalesInvoiceData";
    String UPDATE_SALES_INVOICE_DATA = "UpdateSalesInvoiceData";
    String READ_SALES_ORDER_DATA = "ReadSalesOrderData";
    String READ_ITEMS = "ReadItem";
    String Export_PDF = "ExportPDF";
}
