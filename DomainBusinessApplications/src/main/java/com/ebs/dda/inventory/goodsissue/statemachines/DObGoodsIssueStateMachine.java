package com.ebs.dda.inventory.goodsissue.statemachines;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueSalesInvoice;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueSalesOrder;
import org.apache.commons.scxml2.model.ModelException;

import java.util.HashSet;
import java.util.Set;

public class DObGoodsIssueStateMachine extends AbstractStateMachine {
    public static final String DRAFT_STATE = "Draft";
    public static final String ACTIVE_STATE = "Active";

    private Set<String> objectStates;
    private boolean initialized;

    private DObGoodsIssue object;
    private DObGoodsIssueGeneralModel objectGeneralModel;

    public DObGoodsIssueStateMachine() throws ModelException {
        super(
                DObGoodsIssueStateMachine.class
                        .getClassLoader()
                        .getResource("META-INF/DObGoodsIssue.scxml"));
    }

    public boolean goToDraft() {
        return true;
    }

    public boolean goToActive() {
        return true;
    }

    @Override
    public void initObjectState(IStatefullBusinessObject obj) throws Exception {
        if (obj == null) {
            throw new IllegalArgumentException("The object must have current state(s)");
        }
        if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
            Set<String> currentStates = new HashSet<String>();
            String initialState = getEngine().getStateMachine().getInitial();
            currentStates.add(initialState);
            obj.setCurrentStates(currentStates);
        }
        objectStates = new HashSet<String>(obj.getCurrentStates());
        this.getEngine().setConfiguration(obj.getCurrentStates());
        setEntity(obj);
        initialized = true;
    }

    @Override
    public boolean resetMachine() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected <T extends IStatefullBusinessObject> T getEntity() {
        if (this.object == null && this.objectGeneralModel == null) {
            throw new IllegalArgumentException("Object is NULL");
        }
        return (T) (this.object == null ? this.objectGeneralModel : this.object);
    }

    protected void setEntity(IStatefullBusinessObject obj) {
        if (obj instanceof DObGoodsIssueSalesInvoice) {
            this.object = (DObGoodsIssueSalesInvoice) obj;
        }
        else if (obj instanceof DObGoodsIssueSalesOrder) {
            this.object = (DObGoodsIssueSalesOrder) obj;
        } else {
            this.objectGeneralModel = (DObGoodsIssueGeneralModel) obj;
        }
    }
}
