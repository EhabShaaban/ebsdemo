package com.ebs.dac.foundation.apis.exchangerates;

import com.ebs.dda.jpa.masterdata.exchangerate.CBEExchangeRate;
import java.time.LocalDate;
import org.springframework.web.client.RestTemplate;

/**
 * @author Mohamed Aboelnour
 * @since Nov 05, 2020
 */
public class CBEExchangeRateController {

  public Double getCurrencyPriceFromCBE(String currencyIso, String domainName) {
    Double currencyPrice = 0.0;
    String formatedDomainName = formatURL(domainName);
    String uri = "http://" + formatedDomainName + ":8080/command/exchangerates/cbe"
        + "/"
        + LocalDate.now().toString()
        + "/"
        + currencyIso;
    RestTemplate restTemplate = new RestTemplate();
    CBEExchangeRate exchangeRate = null;
    try {
      exchangeRate = restTemplate.getForObject(uri, CBEExchangeRate.class);
    } catch (Exception e) {
      // TODO handle error in calling exchange rates API.
    }
    if (exchangeRate != null) {
      currencyPrice = exchangeRate.getSell();
    }
    return currencyPrice;
  }

  private String formatURL(String domainName) {
    domainName = domainName.replace("http://","");
    int portIndex = domainName.indexOf(':');
    int end = portIndex > 0 ? portIndex : domainName.length();
    return domainName.substring(0,end);
  }
}
