package com.ebs.dac.foundation.realization.validation.config;

public enum RuleBrokenCase {
  WARNING,
  ERROR,
  FAILURE
}
