package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.apis.processing.IDataObjectAttributeProcessor;
import java.lang.reflect.Method;
import javax.management.modelmbean.InvalidTargetObjectTypeException;

public class SimpleAttributeProcessor implements IDataObjectAttributeProcessor {

  @Override
  public Object getAttributeValue(IEntityAttribute attribute, Object sourceObj) throws Exception {
    Object fieldValue = null;

    if (attribute == null) {
      throw new IllegalArgumentException("Illegal Attribute : " + attribute);
    }

    String attributeName = attribute.getName();
    if (attributeName == null || attributeName.trim().isEmpty()) {
      throw new IllegalArgumentException("Illegal Attribute Name : " + attributeName);
    }

    if (sourceObj == null) {
      throw new IllegalArgumentException("Illegal Target : " + sourceObj);
    }

    Class<?> sourceClz = sourceObj.getClass();

    /** *** Constructing Field's Default Getter Method Name **** */
    /** ***** Default Getter Name Is get + (F)ieldName ********* */
    String getterName = "get";

    // Capitalize first character of (F)ieldName
    getterName += attributeName.substring(0, 1).toUpperCase();
    getterName += attributeName.substring(1);
    /** ******************************************************* */
    boolean getterMethodInvoked = false;

    for (Method method : sourceClz.getMethods()) {
      method.setAccessible(true);

      if (method.getName().equals(getterName)) {
        /*
         * Default field getter method has no arguments and return the value of field.
         */
        fieldValue = method.invoke(sourceObj);
        getterMethodInvoked = true;
      }
    }

    if (getterMethodInvoked == false) {
      String msg =
          "Source Object Type ["
              + sourceClz.getSimpleName()
              + "] Doesn't Have Default Getter Method";
      msg += " For Field (" + attributeName + ")";

      throw new InvalidTargetObjectTypeException(msg);
    }

    return fieldValue;
  }

  @Override
  public Object setAttributeValue(IEntityAttribute attribute, Object attrValue, Object targetObj)
      throws Exception {
    if (attribute == null) {
      throw new IllegalArgumentException("Illegal Attribute : " + attribute);
    }

    String attributeName = attribute.getName();
    if (attributeName == null || attributeName.trim().isEmpty()) {
      throw new IllegalArgumentException("Illegal Attribute Name : " + attributeName);
    }

    if (targetObj == null) {
      throw new IllegalArgumentException("Illegal Target : " + targetObj);
    }

    /** *** Constructing Field's Default Setter Method Name **** */
    /** ***** Default Setter Name Is set + (F) + ieldName ****** */
    String setterName = "set";

    // Capitalize first character of (F)ieldName
    setterName += attributeName.substring(0, 1).toUpperCase();
    setterName += attributeName.substring(1);
    /** ******************************************************* */
    Class<?> targetClz = targetObj.getClass();

    boolean setterMethodInvoked = false;

    for (Method method : targetClz.getMethods()) {
      method.setAccessible(true);

      if (method.getName().equals(setterName)) {
        /*
         * Default setter method takes only new value of field as a parameter and has void as a return type.
         */
        method.invoke(targetObj, attrValue);
        setterMethodInvoked = true;
      }
    }

    if (setterMethodInvoked == false) {
      String msg = "Target Object Type Doesn't Have Default Setter Method";
      msg += " For Field (" + attributeName + ")";

      throw new InvalidTargetObjectTypeException(msg);
    }

    return targetObj;
  }
}
