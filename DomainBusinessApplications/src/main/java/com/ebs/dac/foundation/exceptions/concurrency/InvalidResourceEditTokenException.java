package com.ebs.dac.foundation.exceptions.concurrency;

public class InvalidResourceEditTokenException extends ConcurrentDataAccessException {

  private static final long serialVersionUID = 1L;

  public InvalidResourceEditTokenException(String message) {
    super(message);
  }

  public InvalidResourceEditTokenException() {
    this("Invalid resource edit token.");
  }

  public InvalidResourceEditTokenException(Exception cause) {
    super("Invalid resource edit token.", cause);
  }
}
