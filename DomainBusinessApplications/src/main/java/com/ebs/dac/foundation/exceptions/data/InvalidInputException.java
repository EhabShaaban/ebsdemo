package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.localization.LocalizedException;

public class InvalidInputException extends LocalizedException {

  private static final long serialVersionUID = 1L;

  private static final String message = "Invalid Input";

  private IEntityAttribute attribute;

  public InvalidInputException(IEntityAttribute attribue) {
    super(IExceptionsCodes.MALICIOUS_INPUT, new Object[] {attribue.getName()}, message);
    this.attribute = attribue;
  }

  public InvalidInputException() {
    super(IExceptionsCodes.MALICIOUS_INPUT, null, message);
  }

  @Override
  public String getMessage() {
    return String.format(message, attribute.getName());
  }
}
