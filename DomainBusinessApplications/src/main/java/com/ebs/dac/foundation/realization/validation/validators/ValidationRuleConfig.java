package com.ebs.dac.foundation.realization.validation.validators;

import com.ebs.dac.foundation.realization.validation.config.RuleBrokenCase;
import com.ebs.dac.foundation.realization.validation.validationrules.IValidationRule;

public class ValidationRuleConfig {
  private String attributeName;
  private IValidationRule validationRule;
  private RuleBrokenCase ruleBrokenCase;

  public String getAttributeName() {
    return attributeName;
  }

  public void setAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }

  public IValidationRule getValidationRule() {
    return validationRule;
  }

  public void setValidationRule(IValidationRule validationRule) {
    this.validationRule = validationRule;
  }

  public RuleBrokenCase getRuleBrokenCase() {
    return ruleBrokenCase;
  }

  public void setRuleBrokenCase(RuleBrokenCase ruleBrokenCase) {
    this.ruleBrokenCase = ruleBrokenCase;
  }
}
