/* Generated By:JavaCC: Do not edit this line. ConditionSyntaxCheckerConstants.java */
package com.ebs.dac.foundation.realization.condition.parser;

/** Token literal values and constants. Generated by org.javacc.parser.OtherFilesGen#start() */
public interface ConditionSyntaxCheckerConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int OPERATOR = 5;
  /** RegularExpression Id. */
  int PREDICATE_OPERATOR = 6;
  /** RegularExpression Id. */
  int VARIABLE_NAME = 7;
  /** RegularExpression Id. */
  int ALLOWED_STRING_OPD = 8;
  /** RegularExpression Id. */
  int NUMBER = 9;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\n\"",
    "\"\\r\"",
    "<OPERATOR>",
    "<PREDICATE_OPERATOR>",
    "<VARIABLE_NAME>",
    "<ALLOWED_STRING_OPD>",
    "<NUMBER>",
    "\"[\"",
    "\"]\"",
    "\"(\"",
    "\")\"",
  };
}
