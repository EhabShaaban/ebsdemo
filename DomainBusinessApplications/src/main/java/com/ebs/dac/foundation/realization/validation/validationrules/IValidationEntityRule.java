package com.ebs.dac.foundation.realization.validation.validationrules;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 24, 2018 3:17:00 PM
 */
public interface IValidationEntityRule extends IValidationRule {

  boolean validate(String attrName, Object entity, ValidationResult validationResult)
      throws Exception;
}
