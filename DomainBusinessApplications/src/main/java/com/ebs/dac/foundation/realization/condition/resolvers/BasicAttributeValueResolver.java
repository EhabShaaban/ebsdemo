package com.ebs.dac.foundation.realization.condition.resolvers;

import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.dbo.types.BusinessEnum;
import com.ebs.dac.foundation.realization.condition.utils.AttributeTypeDetectorUtil;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 25, 2018 4:24:54 PM
 */
class BasicAttributeValueResolver implements IEntityAttributeResolver {

  @Override
  public Object resolveAttributeValue(Object attrValue) throws Exception {

    if (AttributeTypeDetectorUtil.isEnumValue(attrValue)) {
      return getEnumValue(attrValue);
    }

    if (AttributeTypeDetectorUtil.isPrimitiveValue(attrValue)) {
      return attrValue;
    }

    throw new DevRulesViolation("This value " + attrValue + " is not basic attribute");
  }

  private Object getEnumValue(Object attrValue) {
    if (((BusinessEnum) attrValue).getId() != null) {
      return ((BusinessEnum) attrValue).getId().toString();
    }
    return null;
  }
}
