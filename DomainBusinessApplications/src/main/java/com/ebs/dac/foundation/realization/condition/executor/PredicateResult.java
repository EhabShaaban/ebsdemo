package com.ebs.dac.foundation.realization.condition.executor;

/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/
public class PredicateResult {

  private boolean result;

  public PredicateResult() {}

  public PredicateResult(Boolean result) {
    this.result = result;
  }

  public boolean isResult() {
    return result;
  }

  public void setResult(boolean result) {
    this.result = result;
  }
}
