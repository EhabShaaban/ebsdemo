package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.api.processing.IEmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.foundation.apis.processing.IDataObjectAttributeProcessorFactory;
import com.ebs.dac.foundation.apis.processing.IDataObjectProcessor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataObjectProcessor implements IDataObjectProcessor {

  private Class<?> objType;
  private IDataObjectAttributeProcessorFactory attributeProcessorFactory;

  public DataObjectProcessor(Class<?> clzz) {
    this(clzz, new DataObjectAttributeProcessorFactory());
  }

  public DataObjectProcessor(Object dataObj) {
    this(dataObj, new DataObjectAttributeProcessorFactory());
  }

  public DataObjectProcessor(
      Class<?> clzz, IDataObjectAttributeProcessorFactory attributeProcessorFactory) {
    if (clzz == null) {
      throw new NullPointerException("Class type must not be null");
    }
    this.objType = clzz;
    setAttributeProcessorFactory(attributeProcessorFactory);
  }

  public DataObjectProcessor(
      Object dataObj, IDataObjectAttributeProcessorFactory attributeProcessorFactory) {
    if (dataObj == null) {
      throw new NullPointerException("Data object must not be null");
    }
    this.objType = dataObj.getClass();
    setAttributeProcessorFactory(attributeProcessorFactory);
  }

  protected IDataObjectAttributeProcessorFactory getAttributeProcessorFatory() {
    if (this.attributeProcessorFactory == null) {
      throw new NullPointerException("The AttributeDataProcessor must not be null");
    }
    return this.attributeProcessorFactory;
  }

  public void setAttributeProcessorFactory(
      IDataObjectAttributeProcessorFactory attributeProcessorFactory) {
    this.attributeProcessorFactory = attributeProcessorFactory;
  }


  public Collection<Field> getAllFields() throws Exception {
    return getAllFields(this.objType);
  }

  private Collection<Field> getAllFields(Class<?> objType) throws Exception {
    Set<Field> fields = new HashSet<Field>();

    Field[] declaredFields = objType.getDeclaredFields();
    for (Field field : declaredFields) {
      fields.add(field);
    }

    Iterator<Class<?>> interfaceHierarchy = new DataObjectTypeHierarchy(objType).getHierarchy();
    Field[] superFields;

    while (interfaceHierarchy.hasNext()) {
      Class<?> superClass = interfaceHierarchy.next();

      if (superClass.equals(objType)) {
        continue;
      }

      superFields = superClass.getDeclaredFields();
      for (Field superField : superFields) {
        fields.add(superField);
      }
    }

    List<Field> result = new ArrayList<Field>();
    result.addAll(fields);
    return result;
  }


  @Override
  public Object getAttributeValue(IEntityAttribute attribute, Object sourceObj) throws Exception {
    return getAttributeProcessorFatory()
        .createAttributeProcessor(attribute)
        .getAttributeValue(attribute, sourceObj);
  }

  @Override
  public Object setAttributeValue(IEntityAttribute attribute, Object attrValue, Object targetObj)
      throws Exception {
    return getAttributeProcessorFatory()
        .createAttributeProcessor(attribute)
        .setAttributeValue(attribute, attrValue, targetObj);
  }

  @Override
  public boolean hasAttribute(String attrName) throws Exception {
    boolean hasAttr = false;

    if (attrName == null || attrName.isEmpty()) { // added by Ahmed Ali
      throw new IllegalArgumentException();
    }

    for (Field field : getAllFields()) {
      if (field.getName().equals(attrName)) {
        hasAttr = true;
        break;
      }
    }

    return hasAttr;
  }

  @Override
  public AttributeIdentity getAttributeIdentity(String attributeName, Class<?> entityInterface)
      throws Exception {

    AttributeIdentity attributeIdentity = null;

    if (attributeName == null || attributeName.trim().isEmpty()) {
      throw new IllegalArgumentException("attributeName must have a value");
    }

    if (entityInterface == null) {
      throw new IllegalArgumentException("entityInterface must not be Null");
    }

    String name = attributeName + ATTRBUITE_NAME_POSTFIX;

    Collection<Field> declaredFields = getAllFields(entityInterface);

    for (Field field : declaredFields) {
      if (field.getName().equals(name)) {
        Object fieldValue = field.get(null);
        if (fieldValue instanceof IEntityAttribute == false) {
          throw new DevRulesViolation(
              "attribute '" + attributeName + "' must be instance of EntityAttribute");
        }
        IEntityAttribute attributeType = (IEntityAttribute) fieldValue;
        attributeIdentity = new AttributeIdentity(attributeName, attributeType);

        if (attributeType instanceof ICompositeAttribute) {
          attributeIdentity.setMultiple(((CompositeAttribute<?>) attributeType).isMultiple());
        }

        if (attributeType instanceof IEmbeddedAttribute) {
          com.ebs.dac.dbo.processing.EmbeddedAttribute<?> embeddedAttribute =
              (com.ebs.dac.dbo.processing.EmbeddedAttribute<?>) attributeType;
          if (!(attributeType instanceof LocalizedTextAttribute)) {
            Class<?> embeddedAttrInterface = getEntityInterface(embeddedAttribute.getType());
            attributeIdentity.setAttributeInterface(embeddedAttrInterface);
          }
        }
      }
    }

    if (attributeIdentity == null) {
      throw new IllegalArgumentException(
          name + " does not exist at " + entityInterface.getSimpleName());
    }

    return attributeIdentity;
  }

  public Map<String, Object> getAttributesValuesMap(
      BusinessObject entity, Set<String> targetAttributes) throws Exception {
    Map<String, Object> result = new HashMap<>();

    result.putAll(getTargetAttributesValues(entity, targetAttributes));

    getPayloadTargetAttributesValues(entity, result, targetAttributes);

    return result;
  }

  private void getPayloadTargetAttributesValues(
      BusinessObject entity, Map<String, Object> result, Set<String> targetAttributes)
      throws Exception {
    if (entity instanceof IAggregateAwareEntity) {

      IAggregateAwareEntity aggregateEntity = (IAggregateAwareEntity) entity;
      IAggregateBusinessObject<BusinessObject> aggregateBusinessObject =
          aggregateEntity.aggregateBusinessObject();

      for (ICompositeAttribute<?> compositeAttr :
          aggregateBusinessObject.getAllInstanceAttributes()) {

        if (targetAttributes.contains(compositeAttr.getName())) {
          result.put(
              compositeAttr.getName(),
              aggregateBusinessObject.getInstanceAttribute((ICompositeAttribute) compositeAttr));
        }

        BusinessObject info = null;
        if (compositeAttr.isMultiple()) {
          List<?> multipleInstancesAttribute =
              aggregateBusinessObject.getMultipleInstancesAttribute(
                  (ICompositeAttribute) compositeAttr);
          info =
              (multipleInstancesAttribute.isEmpty())
                  ? null
                  : (BusinessObject) multipleInstancesAttribute.get(0);
        } else {
          info =
              (BusinessObject)
                  aggregateBusinessObject.getInstanceAttribute((ICompositeAttribute) compositeAttr);
        }

        if (info != null) {
          result.putAll(getTargetAttributesValues(info, targetAttributes));
          getPayloadTargetAttributesValues(info, result, targetAttributes);
        }
      }
    }
  }

  private Map<String, Object> getTargetAttributesValues(Object entity, Set<String> targetAttributes)
      throws Exception {
    Map<String, Object> result = new HashMap<>();
    Class<?> entityInterface = getEntityInterface(entity.getClass());

    for (Field field : getAllFields(entity.getClass())) {

      if (targetAttributes.contains(field.getName())) {

        AttributeIdentity attributeIdentity =
            getAttributeIdentity(field.getName(), entityInterface);
        Object attributeValue = getAttributeValue(attributeIdentity.getAttributeType(), entity);

        if (attributeValue != null) {
          result.put(field.getName(), attributeValue);
        }
      }
    }

    return result;
  }

  private Class<?> getEntityInterface(Class<?> cls) throws DevRulesViolation {
    EntityInterface entityInterface = cls.getAnnotation(EntityInterface.class);
    if (entityInterface == null) {
      throw new DevRulesViolation(
          "Entity " + cls.getSimpleName() + " doesn't annotated by @EntityInterface.");
    }
    return entityInterface.value();
  }
}
