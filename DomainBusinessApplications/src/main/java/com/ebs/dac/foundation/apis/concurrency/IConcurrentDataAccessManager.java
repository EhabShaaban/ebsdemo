package com.ebs.dac.foundation.apis.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.exceptions.concurrency.ConcurrentDataAccessException;
import com.ebs.dac.foundation.exceptions.concurrency.InvalidResourceEditTokenException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyLockedException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyUnlockedException;
import com.ebs.dac.foundation.exceptions.operational.locking.NotLockedByPrimaryActorException;
import java.util.List;

public interface IConcurrentDataAccessManager {

  /**
   * @param resourceName
   * @param resourceId
   * @return Created lock details
   * @throws ResourceAlreadyLockedException
   * @throws ConcurrentDataAccessException
   */
  LockDetails lock(String resourceName, String resourceCode, IBDKUser identity)
      throws Exception;

  LockDetails lock(
      String resourceName, String resourceCode, IBDKUser identity, ICAMListener listener)
      throws Exception;

  /**
   * @param tokenId
   * @return
   * @throws ResourceAlreadyUnlockedException
   * @throws InvalidResourceEditTokenException
   */
  LockDetails unlock(String tokenId)
      throws ResourceAlreadyUnlockedException, InvalidResourceEditTokenException;

  LockDetails unlock(String resourceName, String resourceCode, IBDKUser loggedInUser)
      throws ResourceAlreadyUnlockedException, NotLockedByPrimaryActorException;

  /**
   * Used to verify a Token ID matches the claimed resource ID<br>
   * <br>
   * It is worth noting that the source of the resourceId parameter should not be {@link
   * LockDetails}, because it would be correct, but rather should be retrieved from e.g. URL of an
   * Edit request.<br>
   * <br>
   * This is because this method serves as two purposes:<br>
   * - It asserts that supplied Token ID is still available (not expired)<br>
   * - Asserts that resource ID proposed to be edited does have a Token ID that actually matches it
   *
   * @param resourceId
   * @param tokenId
   * @throws InvalidResourceEditTokenException
   */
  void verifyToken(String tokenId, String resourceName, String resourceCode)
      throws InvalidResourceEditTokenException;

  void checkIfResourceIsLockedByUser(String resourceName, String resourceCode, IBDKUser user)
      throws Exception;

  /**
   * Modifies the original LockDetails reference's expiryDate attribute. This does not return a new
   * LockDetails object
   *
   * @param tokenId
   * @return
   * @throws InvalidResourceEditTokenException
   */
  LockDetails extendLock(String tokenId, String resourceName, String resourceCode)
      throws InvalidResourceEditTokenException;

  /**
   * Caution: Wise calling ahead. Heavy execution, it traverses all the locks while suspending all
   * other threads
   *
   * @param all clean all lock if set to true, or just the expired locks when set to false
   */
  void cleanUp(boolean all);

  List<LockDetails> listAllLocks();

  /**
   * @param identity The user identity to filter with. Note that the {@link IBDKUser#equals(IBDKUser)) method is the one used to check identity equality
   */
  List<LockDetails> listLocksAcquiredBy(IBDKUser identity);

  List<LockDetails> listLocksAcquiredForResource(String resourceCode);

  List<String> listResourceNameLocksAcquiredForResource(String resourceCode);

  boolean isListLocksAcquiredForResourceEmpty(String resourceCode);

  IEditTokenValidityPolicy getValidityStrategy();

  void setValidityStrategy(IEditTokenValidityPolicy validityStrategy);

  String getResourceRoot();

  LockDetails getByTokenId(String tokenId) throws NotLockedByPrimaryActorException;

  void registerListener(String tokenId, ICAMListener listener)
      throws InvalidResourceEditTokenException;

  List<LockDetails> listLocksAcquiredForResourceByUser(String resourceCode, IBDKUser user);
}
