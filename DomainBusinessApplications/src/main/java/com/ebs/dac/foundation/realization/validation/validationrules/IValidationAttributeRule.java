package com.ebs.dac.foundation.realization.validation.validationrules;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public interface IValidationAttributeRule extends IValidationRule {

  boolean validate(String attrName, Object attrValue, ValidationResult validationResult)
      throws Exception;
}
