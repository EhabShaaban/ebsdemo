package com.ebs.dac.foundation.exceptions.localization;

public interface IExceptionsCodes {

  String MISSING_MANDATORY_SINGLE_ATTRIBUTE_ERROR = "General_MVR_MissingMandatotyFieldError";
  String MISSING_MANDATORY_SINGLE_ATTRIBUTE_WARNING = "General_MVR_MissingMandatotyFieldWarning";
  String MISSING_MANDATORY_MULTIPLE_ATTRIBUTE_ERROR = "General_MVR_MissingMandatotyGridError";
  String MISSING_MANDATORY_MULTIPLE_ATTRIBUTE_WARNING = "General_MVR_MissingMandatotyGridWarning";

  String MISSING_MANDATORY_ATTRIBUTE = "msg_text_notNull";
  String ATTRIBUTE_IS_NOT_EMPTY = "msg_text_notEmpty";
  String DUPLICATE_UNIQUE_VALUE_ATTRIBUTE = "msg_text_unique";
  String DATE_COMPARISON_GREATER = "msg_text_dateComparisonGreater";
  String ALREADY_DEACTIVATED_RECORD = "msg_text_alreadyDeactivated";
  String DATE_COMPARISON_GREATER_OR_EQUAL = "msg_text_dateComparisonGreaterOrEqual";
  String INVALID_OWNER = "msg_text_invalidOwner";
  String INVALID_REFERENCE_ATTRIBUTE = "msg_text_invalidReference";
  String UNEDITABLE_ENTITY = "msg_text_uneditableEntity";
  String UNSUCCESSFUL_OPERATION = "msg_text_unsuccessfulOperation";
  String INVALID_EMAIL_ADDRESS = "msg_text_InvalidEmail";
  String INVALID_PHONE_NO = "msg_text_InvalidPhoneNumber";
  String INVALID_CREDIT_CARD_NO = "msg_text_InvalidCreditCardNumber";
  String NUM_COMPARISON_GREATER_THAN = "msg_text_numberComparisonGreater";
  String NON_NEGATIVE_VALUE = "msg_text_NonNegativeValue";
  String DEFAULT_VALUE_ALREADY_ESIXT = "msg_text_DefaultValueAlreadyExist";
  String INVALID_CARDINALITY_LOWER_LIMIT = "msg_text_InvalidCardinalityLowerLimit";
  String INVALID_CARDINALITY_LIMITS = "msg_text_InvalidCardinalityLimits";
  String DOCUMENT_ALREADY_ASSIGNETD = "msg_text_DocumentAlreadyAssigned";
  String MISSING_DEPENDABLE_VALUE = "msg_text_MissingDependableValue";
  String DUPLICATE_ATTACHEMENT_ASSIGNMENT = "msg_text_DuplicateAttachementAssignment";
  String ENTITY_CANNOT_BE_DELIMITED = "msg_text_EntityCannotBeDelimited";
  String MISSING_REQUIRED_ATTACHMENT_DOCUMENT = "msg_text_MissingRequiredAttachmentDocument";
  String INVALID_DETAIL = "msg_text_InvalidDetail";
  String ENTITY_CANNOT_BE_CREATED = "msg_text_EntityCannotBeCreated";
  String ENTITY_CANNOT_BE_DELETED = "msg_text_EntityCannotBeDeleted";
  String ENTITY_CANNOT_BE_UPDATED = "msg_text_EntityCannotBeUpdated";
  String CANT_EDIT_LOCKED = "msg_text_cantEditLocked";
  String UN_EDITABLE_ATTRIBUTE = "msg_text_unEditableAttribute";
  String MASTER_OBJECT_CANNOT_BE_ACTIVATED = "Msg_Text_SOb_CantBeActivated";
  String MASTER_OBJECT_CANNOT_BE_DEACTIVATED = "Msg_Text_SOb_CantBeDeactivated";
  String INCORRECT_LANG = "Msg_Validation_CorrectLanguage";
  String STRUCTUE_OBJECT_CANNOT_BE_DELETED = "Msg_Text_SOb_CantBeDeleted";
  String STRUCTURE_OBJECT_CANNOT_BE_ACTIVATED = "Msg_Text_SOb_CantBeActivated";
  String STRUCTURE_OBJECT_CANNOT_BE_DEACTIVATED = "Msg_Text_SOb_CantBeDeactivated";
  String HAVE_Exactly_ONE_DEFAULT_CONTACT = "Msg_Validation_DefaultContact";
  String DUPLICATE_SELECTED_PO = "Msg_Duplicate_Po";
  String MISSING_GENERAL_ATTRIBUTE = "msg_general_missing";
  String CANNOT_BE_UPDATED = "msg_cannot_be_updated";
  String HIERARCHY_LEAF_LEVEL_WITH_CHILDREN = "msg_text_inavlidHierarchyLeafLevel";
  String INVALID_HIERARCHY_PARENT_OBJECT = "msg_text_invalidHierarchyParentObject";
  String DEFAULT_OUT_OF_RANGE_MESSAGE = "msg_text_defaultOutOfRangeExceptionMessage";
  String PURCHASING_RULES_REMINDERS_OUT_OF_RANGE_MESSAGE = "msg_text_purchasingRulesRemindersOutOfRangeExceptionMessage";
  String CLIENT_BYPASSING_SECURITY_EXCEPTION = "msg_txt_clientBypassingSecurityException";
  String VALUE_IN_ALL_SUPPORTED_LANGUAGES = "msg_text_valueInAllSupportedLanguages";
  String NO_DUPLICATES = "General_NoDuplicates";

  /** ************************** Exception codes for usecases ****************** */

  String General_UC_Failure_InstanceNotExist = "Gen-msg-01";
  String General_UC_Failure_AlreadyLockedSection = "Gen-msg-02";
  String General_EditNotAllowedPerState = "Gen-msg-03";
  String MISSING_CORRECT_DATA_ERROR_CODE = "Gen-msg-05";
  String General_INVALID_INPUT = "Gen-msg-06";
  String MALICIOUS_INPUT = "Gen-msg-06";
  String APP_NOT_LOCKED_BY_PRIMARY_ACTOR_VIOLATION = "Gen-msg-07";
  String General_DeleteNotAllowedPerState = "Gen-msg-13";
  String General_UC_Failure_AlreadyLocked = "Gen-msg-14";
  String General_CONDITIONAL_AUTHORIZATION_CODE = "Gen-msg-15";
  String General_UC_Failure_RecordOfInstanceNotExist = "Gen-msg-20";
  String General_RecordDeleteNotAllowedPerState = "Gen-msg-21";
  String General_RecordDependence = "Gen-msg-27";
  String AUTHORIZATION_ERR = "Gen-msg-29";
  String AUTHORIZATION_EXCEPTION = "Gen-msg-29";
  String General_DataNavigationError = "Gen-msg-31";
  String General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE = "Gen-msg-32";
  String General_UC_Failure_AlreadyLocked_BySameUser = "Gen-msg-39";
  String General_UC_Failure_MISSING_MANDATORIES = "Gen-msg-40";
  String General_Update_RecordDependence = "Gen-msg-42";
  String General_UC_Failure_AlreadyLocked_BySameUser_InSameSession = "Gen-msg-41";
  String INCORRECT_FIELD_INPUT = "Gen-msg-46";
  String SUCCESSFUL_ACTIVATION_CODE = "Gen-msg-47";
  String General_MISSING_FIELD_INPUT = "Gen-msg-48";
  String General_OBJECT_CHANGE_BUSINESS_UNIT = "Gen-msg-49";
  String General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH = "Gen-msg-53";
  String INTERNAL_SERVER_ERROR = "Gen-msg-55";
  String General_UC_Failure_ReferentialIntegrityViolation = "General_UC_Failure_ReferentialIntegrityViolation";
  String General_UC_Failure_DataIntegrityViolation = "General_UC_Failure_DataIntegrityViolation";
  String APP_OPR_BATCH_ERR = "APP_OPR_BATCH_ERR";
  /** ************************** Exception codes for usecases ****************** */

  /** ************************** Exception codes for usecases ****************** */
  String PURCHASE_ORDER_INACTIVE_PURCHASING_UNIT_MSG = "PO-msg-01";

  String PURCHASE_ORDER_INACTIVE_PURCHASING_RESPONSIBLE_MSG = "PO-msg-02";
  String PURCHASE_ORDER_INACTIVE_VENDOR_MSG = "PO-msg-03";
  String PURCHASE_ORDER_COMPANY_NOT_EXIST = "PO-msg-05";
  String PURCHASE_ORDER_BANK_NOT_EXIST = "PO-msg-06";
  String PURCHASE_ORDER_ACCOUNT_NUMBER_EXIST = "PO-msg-07";
  String PURCHASE_ORDER_DELETED_PURCHASE_RESPONSIBLE_MSG = "PO-msg-09";
  String PURCHASE_ORDER_PLANT_NOT_EXIST_MSG = "PO-msg-10";
  String PURCHASE_ORDER_STOREHOUSE_NOT_EXIST_OR_RELATED_TO_PLANT_MSG = "PO-msg-11";

  /** ************************** Exception codes for Payment and Delivery ****************** */
  String PURCHASE_ORDER_INACTIVE_INCOTERM_MSG = "PO-msg-12";

  String PURCHASE_ORDER_INACTIVE_CURRENCY_MSG = "PO-msg-13";
  String PURCHASE_ORDER_INACTIVE_PAYMENT_TERM_MSG = "PO-msg-14";
  String PURCHASE_ORDER_INACTIVE_SHIPPING_INSTRUCTIONS_MSG = "PO-msg-15";
  String PURCHASE_ORDER_INACTIVE_CONTAINER_TYPE_MSG = "PO-msg-16";
  String PURCHASE_ORDER_INACTIVE_MODE_OF_TRANSPORT_MSG = "PO-msg-17";
  String PURCHASE_ORDER_INACTIVE_LOADING_PORT_MSG = "PO-msg-19";
  String PURCHASE_ORDER_INACTIVE_DISCHARGE_PORT_MSG = "PO-msg-20";

  /** ************************* Exception codes for required Documents ***************** */
  String PURCHASE_ORDER_DOCUMENT_TYPE_EXIST_MSG = "PO-msg-21";
  /** ************************* Exception codes for Order Items ***************** */
  String PURCHASE_ORDER_ITEM_HAS_NO_IVR_MSG = "PO-msg-42";

  String INVALID_STATE_DATE = "PO-msg-59";

  /** ************************* Exception codes for usecases ****************** */
  String ITEM_ITEM_GROUP_NOT_EXIST = "Item-msg-01";

  String ITEM_BASE_UNIT_NOT_EXIST = "Item-msg-02";
  String ITEM_PURCHASE_UNITS_NOT_EXIST = "Item-msg-03";
  String ITEM_PURCHASE_UNITS_DELETION_BUT_REFERENCED_BY_IVR = "Item-msg-04";
  String ITEM_PRODUCT_MANAGER_NOT_EXIST = "Item-msg-05";
  String VENDOR_PURCHASING_UNIT_NOT_EXIST = "Vendor-msg-01";
  String VENDOR_PURCHASING_RESPONSIBLE_NOT_EXIST = "Vendor-msg-02";
  String VENDOR_PURCHASE_UNITS_DELETION_BUT_REFERENCED_BY_IVR = "Vendor-msg-03";
  String VENDOR_OLD_NUMBER_EXIST = "Vendor-msg-04";

  String APPROVER_NOT_IN_CORRECT_ORDER = "PO-msg-32";
  String APPROVER_NOT_IN_APPROVAL_CYCLE = "PO-msg-33";

  String PURCHASE_ORDER_HAS_NO_POSTED_GR = "PO-msg-41";
  String PO_MEASURE_DOES_NOT_HAVE_IVR = "PO-msg-52";
  String PO_NO_MATCHING_APPROVAL_POLICY = "PO-msg-54";
  String MORE_THAN_ONE_MATCHING_APPROVAL_POLICY = "PO-msg-56";

  String CONTROL_POINT_APPROVER_HAS_ALREADY_APPROVED = "PO-msg-60";
  String REPLACED_APPROVER_NOT_IN_CONTROL_POINT_USERS = "PO-msg-61";
  String CONTROL_POINT_APPROVER_HAS_ALREADY_ASSIGNED = "PO-msg-62";

  String IVR_ITEM_NOT_EXIST = "IVR-msg-01";
  String IVR_VENDOR_NOT_EXIST = "IVR-msg-02";
  String IVR_CURRENCY_NOT_EXIST = "IVR-msg-03";
  String IVR_PURCHASE_UNIT_NOT_EXIST = "IVR-msg-04";
  String IVR_ALREADY_EXIST = "IVR-msg-05";
  String IVR_UOM_NOT_EXIST = "IVR-msg-06";
  String IVR_OLD_ITEM_NUMBER_EXIST = "IVR-msg-08";
  String ITEM_GROUP_HAS_CHILD = "IGR-msg-01";

  /** ************************* Customer Exception codes ****************** */

  String CUSTOMER_OLD_NUMBER_EXIST = "CUSTOMER-msg-01";


  /** ************************* Goods Receipt Exception codes for usecases ****************** */
  String GOODS_RECEIPT_INSTANCE_NOT_EXIST_STOREHOUSE_MSG = "GR-msg-06";

  String GOODS_RECEIPT_ITEM_NOT_IN_IVR = "GR-msg-10";
  String GOODS_RECEIPT_MEASURE_IN_IVR = "GR-msg-11";
  String GOODS_RECEIPT_ITEM_EXIST_WITH_SAME_UOM_STOCKTYPE = "GR-msg-14";
  String GOODS_RECEIPT_ITEM_EXIST_WITH_SAME_UOM_STOCKTYPE_BATCHCODE = "GR-msg-15";
  String INVALID_STOREKEEPER_CODE = "GR-msg-07";
  String INVALID_PURCHASE_ORDER_CODE_NO_PLANT = "GR-msg-13";
  String GOODS_RECEIPT_ITEM_QUANTITIES_EXCEED_ITEM_RETURNED_QUANTITY = "GR-msg-17";
  String GOODS_RECEIPT_REF_DOCUMENT_STATE_IS_INVALID = "GR-msg-19";
  String GOODS_RECEIPT_REF_DOCUMENT_HAS_INVALID_LANDED_COST = "GR-msg-18";

  /** ************************ Exception codes for Exchange Rate ****************** */
  String EXCHANGE_RATE_EXISTS_WITH_THE_SAME_DATE_AND_CURRENCY = "ER-msg-01";

  /** ************************ Invoice Exception Codes ****************** */
  String INVOICE_PO_HAS_NO_COMPANY = "Inv-msg-02";

  String INVOICE_VENDOR_HAS_NO_ACCOUNT = "Inv-msg-13";
  String INVOICE_ITEM_HAS_NO_ACCOUNT = "Inv-msg-14";
  String INVOICE_HAS_NO_ESTIMATED_LANDEDCOST = "Inv-msg-15";
  /** ************************ Payment Request Exception Codes ****************** */
  String VENDOR_HAS_NO_ACCOUNT_DETAILS = "Pr-msg-04";

  String NO_LATEST_EXCHANGE_RATE = "Pr-msg-03";
  String PAYMENT_MISSING_DUE_DOCUMENT_DATA = "Pr-msg-05";
  String PAYMENT_AMOUNT_GREATER_THAN_ORDER_TOTAL_AMOUNT = "Pr-msg-06";
  String PAYMENT_AMOUNT_GREATER_THAN_VENDOR_INVOICE_REMAINING = "Pr-msg-07";
  String PAYMENT_AMOUNT_GREATER_THAN_BALANCE = "Pr-msg-08";
  String PAYMENT_HAS_NO_ESTIMATED_LANDEDCOST = "Pr-msg-09";

  /** *********************** Goods Issue Codes ****************** */
  String ITEM_HAS_QUANTITY_MORE_THAN_REMAINING_IN_STORE = "GI-msg-01";

  String REF_DOCUMENT_STATE_IS_INVALID = "GI-msg-05";

  /** *********************** User Exception Codes ****************** */
  String CURRENT_PASSWORD_IS_DIFFERENT_TO_DEFAULT_PASSWORD = "Login-msg-01";

  String CURRENT_PASSWORD_IS_SAME_TO_NEW_PASSWORD = "Login-msg-05";

  /** *********************** Notes Receivables Codes ****************** */
  String NR_AMOUNT_GREATER_THAN_SI_REMAINING = "NR-msg-01";
  String AMOUNT_IS_GREATER_THAN_REMEAINING = "REC-MSG-01";
  String NR_AMOUNT_TO_COLLECT_NOT_EQUALS_NR_AMOUNT = "NR-msg-05";
  String NR_AMOUNT_TO_COLLECT_REF_DOCUMENT_GREATER_THAN_ITS_REMAINING= "NR-msg-06";
  String NR_SALES_ORDER_REF_DOCUMENT_INVALID_STATE = "NR-msg-07";

  /** ***************************** Collection Request ************** */
  String C_AMOUNT_GREATER_THAN_SALES_INVOICE_REMAINING = "C-msg-01";

  String C_AMOUNT_GREATER_THAN_SALES_ORDER_REMAINING = "C-msg-03";

  String REF_DOCUMENT_INVALID_STATE = "C-msg-04";
  /** ***************************** Stock Transformation ************** */
  String STKTR_TRANSFORMED_QTY_GREATER_THAN_ST_REMAINING = "TRAN-MSG-02";
  /** ***************************** actual cost ************** */
  String GOODS_INVOICE_NOT_EXIST = "ActualCost-msg-01";

  String CUSTOM_TRUSTEE_NOT_EXIST = "ActualCost-msg-02";
  /** ***************************** Landed Cost ************** */
  String PURCHASE_ORDER_HAS_POSTED_LANDED_COST = "Lc-msg-01";
  final String THERE_IS_NO_ACTIVATED_GOODS_INVOICE_IN_ACTIVATE_STATE = "Lc-msg-03";
  /** ***************************** Settlement ************** */
  String SETTLEMENTS_ACCOUNTING_DETAILS_NOT_EQUALS = "S-msg-10";
  String SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE = "S-msg-11";
  /** ***************************** Costing ************** */
  String COSTING_HAS_NO_ACTUAL_LANDED_COST = "Costing-msg-01";

}
