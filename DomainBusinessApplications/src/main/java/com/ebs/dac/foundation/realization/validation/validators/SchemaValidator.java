package com.ebs.dac.foundation.realization.validation.validators;

import java.util.Set;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Yara Ameen
 * @since Sep 17, 2020
 */
public class SchemaValidator {

	private ObjectMapper mapper = new ObjectMapper();
	private static final Logger logger =
			LoggerFactory.getLogger(SchemaValidator.class);
	public void validateRequestBodySchema(String requestBody, String objectSchema)
					throws Exception {

		JsonSchema objectSchemaNode = getJsonSchemaFromStringContent(objectSchema);
		JsonNode requestBodyNode = getJsonNodeFromStringContent(requestBody);

		Set<ValidationMessage> validationResult = objectSchemaNode.validate(requestBodyNode);

		if (!validationResult.isEmpty()) {
			validationResult.forEach(vm -> logger.error(vm.getMessage()));
			throw new ArgumentViolationSecurityException("Invalid Request Body");

		}
	}

	protected JsonNode getJsonNodeFromStringContent(String content) throws Exception {
		return mapper.readTree(content);
	}

	protected JsonSchema getJsonSchemaFromStringContent(String schemaContent) {
		JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);
		return factory.getSchema(schemaContent);
	}
}
