package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import java.util.Map;

public interface IPredicate {

  IOperand getRightOperand();

  void setRightOperand(IOperand operand);

  IOperand getLeftOperand();

  void setLeftOperand(IOperand operand);

  String getOperator();

  void setOperator(String operator);

  PredicateResult execute(Map<String, Object> map) throws Exception;
}
