package com.ebs.dac.foundation.realization.statemachine;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;

public interface IStateMachine {

  void initObjectState(IStatefullBusinessObject entity) throws Exception;

}
