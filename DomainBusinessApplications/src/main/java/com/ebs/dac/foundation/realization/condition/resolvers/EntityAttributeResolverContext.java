package com.ebs.dac.foundation.realization.condition.resolvers;

import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.foundation.realization.condition.utils.AttributeTypeDetectorUtil;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 25, 2018 3:53:11 PM
 */
public class EntityAttributeResolverContext {

  private IEntityAttributeResolver attributeResolver;

  public Map<String, Object> resolveAttributes(Map<String, Object> map) throws Exception {
    Map<String, Object> resolvedMap = new HashMap<>();

    for (String attrName : map.keySet()) {
      Object attrValue = map.get(attrName);
      if (attrValue == null) {
        continue;
      }
      Object resolvedValue = resolveAttributeValue(attrValue);
      if (resolvedValue != null) {
        resolvedMap.put(attrName, resolvedValue);
      }
    }

    return resolvedMap;
  }

  private Object resolveAttributeValue(Object attrValue) throws Exception {
    initAttributeResolver(attrValue);
    return attributeResolver.resolveAttributeValue(attrValue);
  }

  private void initAttributeResolver(Object attrValue) throws Exception {
    if (AttributeTypeDetectorUtil.isBasicAttributeValue(attrValue)) {
      attributeResolver = new BasicAttributeValueResolver();
    }
    if (AttributeTypeDetectorUtil.isEmbeddedAttribut(attrValue)) {
      attributeResolver = new EmbeddedAttributeValueResolver();
    }
    if (attributeResolver == null) {
      throw new DevRulesViolation(
          "No resolver provided for this type " + attrValue.getClass().getSimpleName());
    }
  }
}
