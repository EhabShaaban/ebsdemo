package com.ebs.dac.foundation.apis.security;

public interface IUserAccountSecurityService {

  /**
   * Gets the currently logged in user
   *
   * @return The currently logged in user, or null if no user is currently logged in
   */
  IBDKUser getLoggedInUser();
}
