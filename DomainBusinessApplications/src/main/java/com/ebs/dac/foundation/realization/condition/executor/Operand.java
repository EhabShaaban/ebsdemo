package com.ebs.dac.foundation.realization.condition.executor;

import com.ebs.dac.foundation.exceptions.conditionmanager.TypeMisMatchException;
import com.ebs.dac.foundation.realization.condition.parser.ConditionSyntaxCheckerConstants;
import com.ebs.dac.foundation.realization.condition.parser.Token;
import org.apache.commons.lang3.StringUtils;

/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/
public class Operand implements IOperand, Comparable<Object> {
  private String type;

  private Object value;

  public Object getValue() {
    return value;
  }

  public void setValue(Token stackToken) throws TypeMisMatchException {
    String tokenValue = stackToken.image;
    if (stackToken.kind == ConditionSyntaxCheckerConstants.ALLOWED_STRING_OPD) {
      this.type = ConditionTypes.STRING;
      this.value = tokenValue.substring(1, tokenValue.length() - 1);
      return;
    }
    if (stackToken.kind == ConditionSyntaxCheckerConstants.NUMBER) {
      if (tokenValue.contains(".")) {
        this.type = ConditionTypes.DOUBLE;
        this.value = Double.valueOf(tokenValue);
      } else {
        this.type = ConditionTypes.LONG;
        this.value = Long.valueOf(tokenValue);
      }
      return;
    }
    throw new TypeMisMatchException("Instance Type Not Found");
  }

  public void setValue(Object object) {
    this.value = object;
    this.type = StringUtils.lowerCase(object.getClass().getSimpleName());
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getLongValue() {
    return (Long) getValue();
  }

  public void setLongValue(Long value) {
    setValue(value);
  }

  public Double getDoubleValue() {
    return (Double) getValue();
  }

  public void setDoubleValue(Double value) {
    setValue(value);
  }

  public String getStringValue() {
    return (String) getValue();
  }

  public void setStringValue(String value) {
    setValue(value);
  }

  @Override
  public int compareTo(Object o) {
    return 0;
  }
}
