package com.ebs.dac.foundation.realization.condition.parser;

import com.ebs.dda.approval.dbo.jpa.entities.enums.OperatorBetweenCondition;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 19, 2018 11:44:21 AM
 */
class CompositeExpression implements IExpression {

  private IExpression leftExpression;
  private IExpression rightExpression;
  private OperatorBetweenCondition middleExpressionOperator;
  private OperatorBetweenCondition rightExpressionOperator;

  public CompositeExpression(IExpression leftExpression, IExpression rightExpression) {
    this.leftExpression = leftExpression;
    this.rightExpression = rightExpression;
    middleExpressionOperator = getRightExpressionOperator(leftExpression);
    rightExpressionOperator = getRightExpressionOperator(rightExpression);
  }

  private OperatorBetweenCondition getRightExpressionOperator(IExpression expression) {

    if (expression instanceof SimpleExpression) {
      SimpleExpression simpleExpression = (SimpleExpression) expression;
      return simpleExpression.getRightExpressionOperator();
    }
    if (expression instanceof CompositeExpression) {
      return getRightExpressionOperator(
          (IExpression) expression.getRightExpression());
    }

    return null;
  }

  @Override
  public Object getLeftExpression() {
    return leftExpression;
  }

  @Override
  public Object getRightExpression() {
    return rightExpression;
  }

  @Override
  public OperatorBetweenCondition  getMiddleExpressionOperator() {
    return middleExpressionOperator;
  }

  @Override
  public OperatorBetweenCondition getRightExpressionOperator() {
    return rightExpressionOperator;
  }

  @Override
  public String toString() {
    return leftExpression + " " + rightExpression;
  }
}
