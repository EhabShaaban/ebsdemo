package com.ebs.dac.foundation.realization.editability;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.management.modelmbean.InvalidTargetObjectTypeException;

public abstract class AbstractEditabilityManager {

  public abstract Map<String, Set<String>> getEnabledAttributesConfig();

  public Set<String> getEnabledAttributesInGivenStates(Set<String> states) {

    Map<String, Set<String>> enabledAttributesPerState = getEnabledAttributesConfig();
    Set<String> allEnabledAttributes = new HashSet<>();
    for (String state : states) {
      Set<String> enabledAttributes = enabledAttributesPerState.get(state);
      if (enabledAttributes != null) {
        allEnabledAttributes.addAll(enabledAttributes);
      }
    }

    return allEnabledAttributes;
  }

  public Object getAttributeValue(String attributeName, Object valueObject) throws Exception {

    String getterName = "get";
    Object attributeValue = null;
    // Capitalize first character of (F)ieldName
    getterName += attributeName.substring(0, 1).toUpperCase();
    getterName += attributeName.substring(1);
    Method getterMethod = valueObject.getClass().getMethod(getterName);
    getterMethod.setAccessible(true);
    attributeValue = getterMethod.invoke(valueObject);
    return attributeValue;
  }

  public Object setAttributeValue(String attributeName, Object attrValue, Object targetObj)
      throws Exception {
    /** *** Constructing Field's Default Setter Method Name **** */
    /** ***** Default Setter Name Is set + (F) + ieldName ****** */
    String setterName = "set";

    // Capitalize first character of (F)ieldName
    setterName += attributeName.substring(0, 1).toUpperCase();
    setterName += attributeName.substring(1);
    /** ******************************************************* */
    Class<?> targetClz = targetObj.getClass();

    boolean setterMethodInvoked = false;

    for (Method method : targetClz.getMethods()) {
      method.setAccessible(true);
      if (method.getName().equals(setterName)) {
        method.invoke(targetObj, attrValue);
        setterMethodInvoked = true;
      }
    }

    if (setterMethodInvoked == false) {
      String msg = "Target Object Type Doesn't Have Default Setter Method";
      msg += " For Field (" + attributeName + ")";

      throw new InvalidTargetObjectTypeException(msg);
    }

    return targetObj;
  }
}
