package com.ebs.dac.foundation.exceptions.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import java.util.List;

/** @author Hesham Saleh */
public class RootResourceAlreadyHaveSeparateLocksException extends ResourceAlreadyLockedException {

  private static final long serialVersionUID = 1L;

  List<LockDetails> currentLocks;

  public RootResourceAlreadyHaveSeparateLocksException(
      String resourceRoot, String resourceCode, List<LockDetails> currentLocks) {
    super(
        "The resource root "
            + resourceRoot
            + " with code "
            + resourceCode
            + " does have one or more separate locks that prevents inclusive locking.");
    this.currentLocks = currentLocks;
  }
}
