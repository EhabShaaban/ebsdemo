package com.ebs.dac.foundation.realization.validation.validators;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class JsonSchemaValidatorFactory {

	private Class objectClass;
	private String objectSchema;


	public JsonSchemaValidatorFactory(Class objectClass) {
		this.objectClass = objectClass;
	}

	public JsonSchemaValidatorFactory(String objectSchema) {
		this.objectSchema = objectSchema;
	}

	public String getObjectSchemaNodeFromResources() throws Exception {
		InputStream jsonSchema = JsonSchemaValidatorFactory.class.getResourceAsStream(
						"/META-INF/Schemas/" + objectClass.getSimpleName() + ".json");
		String jsonString = IOUtils.toString(jsonSchema);
		return jsonString;
	}
	public String getObjectSchemaNodeAsString()  {
		return objectSchema;
	}
}
