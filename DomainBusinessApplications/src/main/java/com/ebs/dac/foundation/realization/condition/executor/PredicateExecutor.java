package com.ebs.dac.foundation.realization.condition.executor;

import com.ebs.dac.foundation.realization.condition.utils.AttributeTypeDetectorUtil;

/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/
public class PredicateExecutor {

  public PredicateResult execute(
      PredicateResult leftOperand, PredicateResult rightOperand, String operator) {
    PredicateResult predicateResult = new PredicateResult();
    if (operator.equals("&&")) {
      predicateResult.setResult(rightOperand.isResult() && leftOperand.isResult());
    } else if (operator.equals("||")) {
      predicateResult.setResult(rightOperand.isResult() || leftOperand.isResult());
    }
    return predicateResult;
  }

  public PredicateResult execute(Operand leftOperand, Operand rightOperand, String operator)
      throws Exception {
    PredicateResult predicateResult = new PredicateResult();
    if (AttributeTypeDetectorUtil.isFloatingNumber(rightOperand.getType())) {
      predicateResult.setResult(excuteFloatingNumbers(leftOperand, rightOperand, operator));
    }
    if (AttributeTypeDetectorUtil.isDecimalNumber(rightOperand.getType())) {
      predicateResult.setResult(excuteDecimalNumbers(leftOperand, rightOperand, operator));
    }
    if (AttributeTypeDetectorUtil.isStringType(rightOperand.getType())) {
      predicateResult.setResult(excuteStrings(leftOperand, rightOperand, operator));
    }
    return predicateResult;
  }

  private boolean excuteStrings(Operand leftOperand, Operand rightOperand, String operator) {
    if (operator.equals("=")) {
      return leftOperand.getStringValue().equals(rightOperand.getStringValue());
    }
    throw new IllegalArgumentException("Not Supported Operator: " + operator);
  }

  private boolean excuteDecimalNumbers(Operand leftOperand, Operand rightOperand, String operator)
      throws Exception {
    Long rightValue = Long.valueOf(String.valueOf(rightOperand.getValue()));
    Long leftValue = Long.valueOf(String.valueOf(leftOperand.getValue()));
    switch (operator) {
      case ">":
        {
          return leftValue > rightValue;
        }
      case ">=":
        {
          return leftValue >= rightValue;
        }
      case "<":
        {
          return leftValue < rightValue;
        }
      case "<=":
        {
          return leftValue <= rightValue;
        }
      case "=":
        {
          return leftValue.equals(rightValue);
        }
      default:
        throw new IllegalArgumentException("Not Supported Operator: " + operator);
    }
  }

  private boolean excuteFloatingNumbers(
      Operand leftOperand, Operand rightOperand, String operator) {
    Double rightValue = Double.valueOf(String.valueOf(rightOperand.getValue()));
    Double leftValue = Double.valueOf(String.valueOf(leftOperand.getValue()));

    switch (operator) {
      case ">":
        {
          return leftValue > rightValue;
        }
      case ">=":
        {
          return leftValue >= rightValue;
        }
      case "<":
        {
          return leftValue < rightValue;
        }
      case "<=":
        {
          return leftValue <= rightValue;
        }
      case "=":
        {
          return leftValue.equals(rightValue);
        }
      default:
        throw new IllegalArgumentException("Not Supported Operator: " + operator);
    }
  }
}
