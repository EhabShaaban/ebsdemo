package com.ebs.dac.foundation.realization;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.apis.security.IAuthorizationService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.statemachine.IStateMachine;
import com.ebs.dac.foundation.realization.statemachine.StateMachineActions;

public class AllowedActionService {

	public static final String WILD_CARD_PERMISSION = "*";
	private IAuthorizationService authorizationService;
	private IStateMachine stateMachine;

	public AllowedActionService(IAuthorizationService authorizationService,
					AbstractStateMachine stateMachine) {
		this.authorizationService = authorizationService;
		this.stateMachine = stateMachine;
	}

	public AllowedActionService(IAuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}

	public Set<String> getObjectScreenAllowedActions(BusinessObject businessObject)
					throws Exception {

		Set<String> authorizedActions = new HashSet<>();
		authorizedActions = this.authorizationService
						.getCurrentUserAuthorizedActions(businessObject);
		if (StatefullBusinessObject.class.isAssignableFrom(businessObject.getClass())) {
			Set<String> stateMachineAllowedActions = getStateMachineAllowedActions(businessObject);
			this.authorizationService.setStateMachineActions(stateMachineAllowedActions);
			if (isReadAllPermitted(authorizedActions) || isWildCardPermission(authorizedActions)) { // for
																									// header
																									// navigation
																									// permission
				stateMachineAllowedActions.add(IActionsNames.READ_ALL);
			}
			if (isWildCardPermission(authorizedActions)) {
				return stateMachineAllowedActions;
			}
			authorizedActions.retainAll(stateMachineAllowedActions);
		}

		return authorizedActions;
	}

	private boolean isReadAllPermitted(Set<String> authorizedActions) {
		return authorizedActions.contains(IActionsNames.READ_ALL);
	}

	private boolean isWildCardPermission(Set<String> authorizedActions) {
		return authorizedActions.contains(AllowedActionService.WILD_CARD_PERMISSION);
	}

	public Set<String> getHomeScreenAllowedActions(Class<? extends BusinessObject> entityCls)
					throws Exception {
		BusinessObject entity = entityCls.newInstance();
		Set<String> authorizedActions = new HashSet<>();
		authorizedActions = this.authorizationService
						.getClassAuthorizedActions(entity.getSysName());
		if (StatefullBusinessObject.class.isAssignableFrom(entity.getClass())) {
			Set<String> homeScreenAllowedActions = new HashSet<>(Arrays.asList(IActionsNames.CREATE,
							IActionsNames.READ_ALL));
			if (authorizedActions.contains(AllowedActionService.WILD_CARD_PERMISSION)) {
				return homeScreenAllowedActions;
			}
			authorizedActions.retainAll(homeScreenAllowedActions);
		}
		return authorizedActions;
	}

	public Set<String> getHomeScreenAllowedActions(String sysName) throws Exception {
		Set<String> authorizedActions = new HashSet<>();
		authorizedActions = this.authorizationService.getClassAuthorizedActions(sysName);
		Set<String> homeScreenAllowedActions = new HashSet<>(Arrays.asList(IActionsNames.CREATE,
				IActionsNames.READ_ALL));
		if (authorizedActions.contains(AllowedActionService.WILD_CARD_PERMISSION)) {
			return homeScreenAllowedActions;
		}
		authorizedActions.retainAll(homeScreenAllowedActions);
		return authorizedActions;
	}

	private <T extends IStatefullBusinessObject> Set<String> getStateMachineAllowedActions(
					BusinessObject entity) throws Exception {

		T statefulEntity = (T) entity;
		stateMachine.initObjectState(statefulEntity);
		StateMachineActions<T> stateMachineActions = new StateMachineActions<>(statefulEntity,
						(AbstractStateMachine) stateMachine);

		return stateMachineActions.getAllowedActions();
	}

	public void setStateMachine(AbstractStateMachine stateMachine) {
		this.stateMachine = stateMachine;
	}
}
