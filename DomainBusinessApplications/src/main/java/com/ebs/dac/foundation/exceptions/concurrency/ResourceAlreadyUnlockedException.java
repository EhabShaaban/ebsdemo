package com.ebs.dac.foundation.exceptions.concurrency;

public class ResourceAlreadyUnlockedException extends ConcurrentDataAccessException {

  private static final long serialVersionUID = 1L;

  public ResourceAlreadyUnlockedException() {
    super("The resource is already unlocked.");
  }
}
