package com.ebs.dac.foundation.realization.validation.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.localization.LocalizedException;

public class MissingMandatoryAttributeException extends LocalizedException {

  private static final long serialVersionUID = 1L;

  private static final String message = "This field is required";

  public MissingMandatoryAttributeException() {
    super(IExceptionsCodes.MISSING_MANDATORY_SINGLE_ATTRIBUTE_ERROR, message);
  }

  @Override
  public String getMessage() {
    return message;
  }
}
