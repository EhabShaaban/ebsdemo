package com.ebs.dac.foundation.realization.statemachine;

/**
 * @author Menna Sayed
 * @date May 30, 2017
 */
public interface IActionsNames {

  String READ = "Read";
  String READ_ALL = "ReadAll";
  String READ_ALL_LIMITED = "ReadAllLimited";
  String READ_BANK_DETAILS = "ReadBankDetails";
  String READ_ADDRESS = "ReadAddress";
  String READ_CONTACT_PERSON = "ReadContactPerson";
  String LOCK = "Lock";
  String UNLOCK = "Unlock";
  String SAVE = "Save";
  String CREATE = "Create";
  String DELETE = "Delete";
  String DELETE_ALL = "DeleteAll";
  String ACTIVATE = "Activate";
  String ACTIVATE_ALL = "ActivateAll";
  String DEACTIVATE = "Deactivate";
  String DEACTIVATE_ALL = "DeactivateAll";
  String UPLOAD_RESOURCE = "UploadResource";
  String DOWNLOAD_RESOURCE = "DownloadResource";
  String DELETE_RESOURCE = "DeleteResource";
  String UPDATE_ITEM = "UpdateItem";
  String UPDATE_GENERAL_DATA = "UpdateGeneralData";
  String MARK_AS_PI_REQUESTED = "MarkAsPIRequested";
  String MARK_AS_SHIPPED = "MarkAsShipped";
  String READ_ACCOUNTING_DETAILS = "ReadAccountingDetails";
  String EDIT_ACCOUNTING_DETAILS = "EditAccountingDetails";
  String EDIT_GENERAL_DATA = "EditGeneralData";
  String READ_GENERAL_DATA = "ReadGeneralData";
  String READ_COST_FACTOR_ITEM = "ReadCostFactorItem";
  String EDIT_PAYMENT_DETAILS = "EditPaymentDetails";
  String POST = "Post";
  String READ_DETAILS = "ReadDetails";
  String READ_ITEMS = "ReadItems";
}
