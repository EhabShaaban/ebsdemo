package com.ebs.dac.foundation.realization.condition.resolvers;

import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.realization.condition.utils.AttributeTypeDetectorUtil;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 25, 2018 4:31:29 PM
 */
class EmbeddedAttributeValueResolver implements IEntityAttributeResolver {

  @Override
  public Object resolveAttributeValue(Object attrValue) throws Exception {

    if (AttributeTypeDetectorUtil.isLocalizedStringValue(attrValue)) {
      return resolveLocalizedStringValue(attrValue);
    }
    throw new DevRulesViolation("This value " + attrValue + " is not Embedded attribute");
  }

  private Object resolveLocalizedStringValue(Object attrValue) {
    LocalizedString localizedString = ((LocalizedString) attrValue);
    String enValue = localizedString.getValue(ISysDefaultLocales.ENGLISH_LANG_KEY);
    return enValue;
  }
}
