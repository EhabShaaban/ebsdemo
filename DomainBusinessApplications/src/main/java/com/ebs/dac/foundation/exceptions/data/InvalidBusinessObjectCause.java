package com.ebs.dac.foundation.exceptions.data;

public abstract class InvalidBusinessObjectCause extends BusinessException implements HasData {}
