package com.ebs.dac.foundation.exceptions.operational.other;

public class InvalidStateMachineException extends Exception {

  /** */
  private static final long serialVersionUID = 1L;

  public InvalidStateMachineException(String exceptionMessage) {
    super(exceptionMessage);
  }
}
