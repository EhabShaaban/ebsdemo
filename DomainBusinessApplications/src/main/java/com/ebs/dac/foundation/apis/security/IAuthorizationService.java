package com.ebs.dac.foundation.apis.security;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import java.util.Set;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 29, 2018 11:22:39 AM
 */
public interface IAuthorizationService {

  <T extends BusinessObject> Set<String> getCurrentUserAuthorizedActions(T entity) throws Exception;

  Set<String> getClassAuthorizedActions(String className) throws Exception;

  void setStateMachineActions(Set<String> stateMachineAllowedActions);
}
