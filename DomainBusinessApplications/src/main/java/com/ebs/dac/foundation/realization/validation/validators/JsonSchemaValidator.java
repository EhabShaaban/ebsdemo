package com.ebs.dac.foundation.realization.validation.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class JsonSchemaValidator implements Validator {

  private JsonSchemaValidatorFactory jsonSchemaFactory;
  private SchemaValidator schemaValidator;
  private String objectSchema;

  public JsonSchemaValidator() {
    schemaValidator = new SchemaValidator();
  }

  public JsonSchemaValidator jsonSchemaFactory(JsonSchemaValidatorFactory jsonSchemaFactory) {
    this.jsonSchemaFactory = jsonSchemaFactory;
    return this;
  }

  private String getObjectSchema() throws Exception {
    String objectSchema = jsonSchemaFactory.getObjectSchemaNodeFromResources();
    return objectSchema;
  }

  private String getObjectSchemaAsString() {
    String objectSchema = jsonSchemaFactory.getObjectSchemaNodeAsString();
    return objectSchema;
  }

  public void validateSchema(String context) throws Exception {
    objectSchema = getObjectSchema();
    schemaValidator.validateRequestBodySchema(context, objectSchema);
  }

  public void validateSchemaForString(String context) throws Exception {
    objectSchema = getObjectSchemaAsString();
    schemaValidator.validateRequestBodySchema(context, objectSchema);
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return String.class.isAssignableFrom(aClass);
  }

  @Override
  public void validate(Object context, Errors errors) {
    try {
      validateSchema((String) context);
    } catch (Exception e) {
      errors.reject("invalid input");
    }
  }
}
