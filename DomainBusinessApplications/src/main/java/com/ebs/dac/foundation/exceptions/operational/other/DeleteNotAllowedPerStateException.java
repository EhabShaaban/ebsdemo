package com.ebs.dac.foundation.exceptions.operational.other;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class DeleteNotAllowedPerStateException extends Exception implements IExceptionResponse {

  public DeleteNotAllowedPerStateException() {}

  public DeleteNotAllowedPerStateException(Exception exception) {
    this();
    initCause(exception);
  }

  @Override
  public String getResponseCode() {
    // TODO Auto-generated method stub
    return IExceptionsCodes.General_DeleteNotAllowedPerState;
  }
}
