package com.ebs.dac.foundation.realization.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;

import java.lang.reflect.Method;

public class ValidationUtils {

  public static void checkIfFieldValueIsMissing(Object value) throws Exception {
    if (value == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public Object getAttributeValue(String attributeName, Object valueObject)
      throws RuntimeException {

    String getterName = "get";
    Object attributeValue = null;
    // Capitalize first character of (F)ieldName
    getterName += attributeName.substring(0, 1).toUpperCase();
    getterName += attributeName.substring(1);
    Method getterMethod;
    try {
      getterMethod = valueObject.getClass().getMethod(getterName);
      getterMethod.setAccessible(true);
      attributeValue = getterMethod.invoke(valueObject);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return attributeValue;
  }
}
