package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class BusinessRuleViolationException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;
  private ValidationResult validationResult;

  public BusinessRuleViolationException() {}

  public BusinessRuleViolationException(ValidationResult validationResult) {
    this.validationResult = validationResult;
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.MISSING_CORRECT_DATA_ERROR_CODE;
  }

  public ValidationResult getValidationResult() {
    return validationResult;
  }
}
