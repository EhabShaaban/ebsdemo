package com.ebs.dac.foundation.exceptions.operational.accessability;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class RecordOfInstanceNotExistException extends Exception implements IExceptionResponse {

  private Class<?> entityType;
  private Long instanceID;
  private String entityClassName;

  public RecordOfInstanceNotExistException(Class<?> entityType, Long instanceID) {
    this.entityType = entityType;
    this.instanceID = instanceID;
  }

  public RecordOfInstanceNotExistException(String entityClassName) {
    this.entityClassName = entityClassName;
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_RecordOfInstanceNotExist;
  }
}
