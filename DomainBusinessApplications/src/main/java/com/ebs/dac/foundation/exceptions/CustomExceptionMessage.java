package com.ebs.dac.foundation.exceptions;

public class CustomExceptionMessage {

  private CustomMessageTextBundle message;

  public CustomMessageTextBundle getMessage() {
    return message;
  }

  public void setMessage(CustomMessageTextBundle message) {
    this.message = message;
  }

  public void setMessage(String message, Object[] messageArgs) {
    this.message = new CustomMessageTextBundle(message, messageArgs);
  }
}
