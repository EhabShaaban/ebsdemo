package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.foundation.exceptions.objects.CyclicInheritanceHierarchy;
import com.ebs.dac.foundation.exceptions.objects.InvalidDataObject;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Util class to resolve the inheritance hierarchy of a class and detect direct/indirect cyclic
 * inheritance
 *
 * @author shaf3y
 */
public class DataObjectTypeHierarchy {

  // List of super types of given realization object type
  private LinkedList<Class<?>> hierarchy;

  public DataObjectTypeHierarchy(Class<?> objType) throws Exception {
    hierarchy = new LinkedList<Class<?>>();
    if (objType.isInterface()) {
      resolveInterfaceHierarchy(objType);
    } else {
      resolveTypeHierarchy(objType);
    }
  }

  private void resolveTypeHierarchy(Class<?> objType) throws Exception {

    if (hierarchy.contains(
        objType)) { // this condition never occur as 'cyclic inheritance' compilation error will be
      // appeared

      Class<?> objectType = hierarchy.getFirst();
      throw new InvalidDataObject(objectType, new CyclicInheritanceHierarchy(objectType));

    } else {

      hierarchy.addLast(objType);
      Class<?> superClass = objType.getSuperclass();

      if (superClass != null) {
        resolveTypeHierarchy(superClass); // Resolve hierarchy for super class
      } else {
        return; // "STOP" no more recursion
      }
    }
  }

  private void resolveInterfaceHierarchy(Class<?> interfaceType) {

    Class<?>[] interfaces = interfaceType.getInterfaces();

    for (Class<?> interfaceCls : interfaces) {
      if (!hierarchy.contains(interfaceCls)) {
        hierarchy.add(interfaceCls);
        resolveInterfaceHierarchy(interfaceCls);
      }
    }
  }

  public Iterator<Class<?>> getHierarchy() {
    return hierarchy.iterator();
  }
}
