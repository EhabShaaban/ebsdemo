package com.ebs.dac.foundation.realization.concurrency.policies;

public interface TimeoutEditTokenValidityPolicyMBean {

  int retrieveEditSessionTime();
}
