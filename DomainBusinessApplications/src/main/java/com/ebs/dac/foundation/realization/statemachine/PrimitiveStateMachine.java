package com.ebs.dac.foundation.realization.statemachine;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.scxml2.model.EnterableState;
import org.apache.commons.scxml2.model.ModelException;

public class PrimitiveStateMachine<T extends IStatefullBusinessObject>
    extends AbstractStateMachine {

  public static final String ACTIVE_STATE = "Active";

  private Set<String> objectStates;

  private boolean initialized;

  private T object;

  public PrimitiveStateMachine() throws Exception {
    super();
    initialized = false;
  }

  public PrimitiveStateMachine(URL url) throws Exception {
    super(url);
    initialized = false;
  }

  public PrimitiveStateMachine(String scxmlFile) throws Exception {
    super(scxmlFile);
    initialized = false;
  }

  public PrimitiveStateMachine(T obj, URL url) throws Exception {
    super(url);
    initObjectState(obj);
  }

  public PrimitiveStateMachine(T obj) throws Exception {
    super();
    initObjectState(obj);
  }

  public T getObject() {
    return object;
  }

  @Override
  protected <T extends IStatefullBusinessObject> T getEntity() {
    return (T) getObject();
  }

  protected Properties init(Locale l) {
    Properties props = new Properties();
    String resource = null;
    try {
      resource = "META-INF/FSM" + (l == null ? "" : ("_" + l)) + ".properties";
      props.load(getClass().getClassLoader().getResourceAsStream(resource));
    } catch (Exception e) {
      // TODO Auto-generated catch block

    }
    return props;
  }

  public boolean isInitialized() {
    return initialized;
  }

  public void initObjectState(IStatefullBusinessObject obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must have current state(s)");
    }
    if (obj.getCurrentStates() == null || obj.getCurrentStates().isEmpty()) {
      Set<String> currentStates = new HashSet<String>();
      String initialState = getEngine().getStateMachine().getInitial();
      currentStates.add(initialState);
      obj.setCurrentStates(currentStates);
    }
    objectStates = new HashSet<String>(obj.getCurrentStates());
    this.getEngine().setConfiguration(obj.getCurrentStates());
    this.object = (T) obj;
    initialized = true;
  }

  public List<String> getStatesNames(Locale l) throws Exception {
    Properties properties = init(l);
    List<String> states = new ArrayList<String>();
    for (Object key : properties.keySet()) {
      String skey = (String) key;
      if (skey.startsWith("state.")) states.add(properties.getProperty(skey));
    }
    return states;
  }

  public String getStateName(Locale l, String stateKey) throws Exception {
    Properties properties = init(l);
    return properties.getProperty("state." + stateKey);
  }

  public List<String> getTransitionsNames(Locale l) throws Exception {
    Properties properties = init(l);
    List<String> states = new ArrayList<String>();
    for (Object key : properties.keySet()) {
      String skey = (String) key;
      if (skey.startsWith("event.")) states.add(properties.getProperty(skey));
    }
    return states;
  }

  public boolean Active() {
    return true;
  }

  public void save() {
    Set<EnterableState> estates = new HashSet<EnterableState>();
    estates.addAll(getEngine().getStatus().getStates());
    Iterator<EnterableState> it = estates.iterator();
    Set<String> states = new HashSet<String>();
    while (it.hasNext()) states.add(it.next().getId());
    object.setCurrentStates(states);
  }

  @Override
  public boolean resetMachine() {
    try {
      if (objectStates != null) // (object.getCurrentStates()!=null &&
        // !object.getCurrentStates().isEmpty())
        getEngine().setConfiguration(objectStates); // (object.getCurrentStates());
      else getEngine().reset();
    } catch (ModelException me) {
      logError(me);
      return false;
    }
    return true;
  }

  public String getInitialState() {
    return getEngine().getStateMachine().getInitial();
  }

  public boolean isInInitialState() {
    return object.getCurrentStates().contains(getInitialState());
  }

  public boolean isInInitialState(T object) {
    if (object == null || object.getCurrentStates() == null) {
      throw new IllegalArgumentException("object and its current states must not be null");
    }
    return object.getCurrentStates().contains(getInitialState());
  }
}
