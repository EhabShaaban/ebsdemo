package com.ebs.dac.foundation.realization.validation.validationrules;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dac.foundation.exceptions.data.InvalidInputException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import java.util.Locale;

public class CheckIfExistAndActiveReferenceRule<
        StatefullObject extends StatefullBusinessObject<DefaultUserCode>>
    implements IValidationAttributeRule {

  private StatefullBusinessObjectRep<StatefullObject, DefaultUserCode> rep;

  public CheckIfExistAndActiveReferenceRule(
      StatefullBusinessObjectRep<StatefullObject, DefaultUserCode> rep) {
    this.rep = rep;
  }

  // what if owner object has Inactive state , it can refer to inactive objects
  public boolean validate(
      String attributeName, Object refranceId, ValidationResult validationResult) {

    StatefullObject persistedEntity =
        rep.findByIdAndCurrentStatesLike((Long) refranceId, "%Active%");
    if (persistedEntity != null) {
      return true;
    }
    validationResult.bindError(
        attributeName, new InvalidInputException().getLocalizedMessage(Locale.ENGLISH));
    return false;
  }
}
