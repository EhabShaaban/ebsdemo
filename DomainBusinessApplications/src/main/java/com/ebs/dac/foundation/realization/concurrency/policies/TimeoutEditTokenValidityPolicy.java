package com.ebs.dac.foundation.realization.concurrency.policies;

import com.ebs.dac.foundation.apis.concurrency.IEditTokenValidityPolicy;
import org.joda.time.DateTime;

public class TimeoutEditTokenValidityPolicy
    implements TimeoutEditTokenValidityPolicyMBean, IEditTokenValidityPolicy {

  private static final int DEFAULT_TIMEOUT_IN_SECONDS = 30;
  private static final int DEFAULT_EXTENTION_ALLOWED_IN_SECONDS = 30;

  private int timeoutInSeconds = DEFAULT_TIMEOUT_IN_SECONDS;
  private int extentionAllowedInSeconds = DEFAULT_EXTENTION_ALLOWED_IN_SECONDS;

  public TimeoutEditTokenValidityPolicy() {}

  /**
   * @param initialTimeout In seconds
   * @param extention In seconds
   */
  public TimeoutEditTokenValidityPolicy(int initialTimeout, int extention) {
    this.timeoutInSeconds = initialTimeout;
    this.extentionAllowedInSeconds = extention;
  }

  @Override
  public DateTime calculateTokenExpiryDate(DateTime creationDate) {
    return creationDate.plusSeconds(timeoutInSeconds);
  }

  @Override
  public DateTime extendTokenExpiryDate(DateTime creationDate, DateTime expiryDate) {
    return expiryDate.plusSeconds(extentionAllowedInSeconds);
  }

  public int getTimeoutInSeconds() {
    return timeoutInSeconds;
  }

  public void setTimeoutInSeconds(int timeoutInSeconds) {
    this.timeoutInSeconds = timeoutInSeconds;
  }

  @Override // DON'T CHANGE THIS NAME , USED IN JMX BEAN
  public int retrieveEditSessionTime() {
    return timeoutInSeconds;
  }

  public int getExtentionAllowedInSeconds() {
    return extentionAllowedInSeconds;
  }

  public void setExtentionAllowedInSeconds(int extentionAllowedInSeconds) {
    this.extentionAllowedInSeconds = extentionAllowedInSeconds;
  }
}
