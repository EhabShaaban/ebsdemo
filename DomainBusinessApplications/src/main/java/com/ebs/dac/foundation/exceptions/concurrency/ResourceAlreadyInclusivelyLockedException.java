package com.ebs.dac.foundation.exceptions.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;

/**
 * TODO Pick better name than Inclusive Lock, for that it was not chosen under careful examination
 * of the proper terminology, the context here just means that there is a lock that precede all
 * other ones (i.e. "*" lock)
 *
 * @author Hesham Saleh
 */
public class ResourceAlreadyInclusivelyLockedException extends ResourceAlreadyLockedException {

  private static final long serialVersionUID = 1L;

  LockDetails currentLock;

  public ResourceAlreadyInclusivelyLockedException(LockDetails currentLock) {
    super(
        currentLock,
        "The resource root "
            + currentLock.getResourceRoot()
            + " with ID "
            + currentLock.getResourceCode()
            + " of the resource name "
            + currentLock.getResourceName()
            + " is inclusively locked.");
    this.currentLock = currentLock;
  }
}
