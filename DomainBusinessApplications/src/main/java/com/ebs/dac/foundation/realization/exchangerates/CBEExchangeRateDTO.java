package com.ebs.dac.foundation.realization.exchangerates;

public class CBEExchangeRateDTO {

  private String currencyISO;
  private Double sell;

  public String getCurrencyISO() {
    return currencyISO;
  }

  public void setCurrencyISO(String currencyISO) {
    this.currencyISO = currencyISO;
  }

  public Double getSell() {
    return sell;
  }

  public void setSell(Double sell) {
    this.sell = sell;
  }

}
