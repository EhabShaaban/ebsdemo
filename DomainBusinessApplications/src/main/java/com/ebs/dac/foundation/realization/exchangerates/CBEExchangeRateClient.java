package com.ebs.dac.foundation.realization.exchangerates;

import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author Mohamed Aboelnour, Ahmed ALi
 * @since Nov 05, 2020
 */
public class CBEExchangeRateClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(CBEExchangeRateClient.class);
  private static final String BASE_URL = "/command/exchangerates/cbe";

  private String domin;

  public CBEExchangeRateClient(String domin) {
    this.domin = domin;
  }

  public CBEExchangeRateDTO[] getLatestExchangeRates() {

    String url = domin + BASE_URL + "/" + LocalDate.now().toString();

    CBEExchangeRateDTO[] rates = new CBEExchangeRateDTO[] {};

    try {
      RestTemplate restTemplate = new RestTemplate();
      ResponseEntity<CBEExchangeRateDTO[]> response =
          restTemplate.getForEntity(url, CBEExchangeRateDTO[].class);

      if (response.getStatusCode().is2xxSuccessful()) {
        rates = response.getBody();
      } else {
        LOGGER.error("Failed to fetch exchange rates from CBE server",
            " response code: " + response.getStatusCode().getReasonPhrase());
      }
    } catch (Exception e) {
      LOGGER.error("Exception while fetching exchange rates from CBE server", e);
    }

    return rates;
  }

}
