package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import com.ebs.dac.foundation.exceptions.conditionmanager.TypeMisMatchException;
import com.ebs.dac.foundation.realization.condition.parser.ConditionSyntaxChecker;
import com.ebs.dac.foundation.realization.condition.parser.ConditionSyntaxCheckerConstants;
import com.ebs.dac.foundation.realization.condition.parser.Token;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class LexicalAnalyser {
  private Set<String> conditionVariables = new HashSet<>();

  public LexicalAnalyser() {}

  private ArrayList<Token> tokenize(ConditionSyntaxChecker parser) {
    ArrayList<Token> tokens = new ArrayList<>();

    Token token = parser.getNextToken();
    while (token.kind != ConditionSyntaxCheckerConstants.EOF) {
      tokens.add(token);
      token = parser.getNextToken();
    }
    return tokens;
  }

  public Set<String> getConditionVariables() {
    return conditionVariables;
  }

  public IPredicate createConditionObject(String condition) throws Exception {
    ConditionSyntaxChecker parser = new ConditionSyntaxChecker(new StringReader(condition));
    Stack<Object> objectsStack = new Stack<>();
    ArrayList<Token> tokens = tokenize(parser);

    for (Token t : tokens) {
      if (t.kind == ConditionSyntaxCheckerConstants.NUMBER
          || t.kind == ConditionSyntaxCheckerConstants.ALLOWED_STRING_OPD
          || t.kind == ConditionSyntaxCheckerConstants.VARIABLE_NAME
          || t.kind == ConditionSyntaxCheckerConstants.OPERATOR
          || t.kind == ConditionSyntaxCheckerConstants.PREDICATE_OPERATOR) {
        objectsStack.push(t);
        if (t.kind == ConditionSyntaxCheckerConstants.VARIABLE_NAME) {
          conditionVariables.add(t.image);
        }
      } else if (t.image.equals("]")) {
        Predicate simplePredicate = new Predicate();
        Operand rightOperand = new Operand();
        Operand leftOperand = new Operand();

        Token stackToken = (Token) objectsStack.pop();
        rightOperand.setValue(stackToken);

        stackToken = (Token) objectsStack.pop();
        if (stackToken.kind == ConditionSyntaxCheckerConstants.OPERATOR) {
          simplePredicate.setOperator(stackToken.image);
          simplePredicate.setRightOperand(rightOperand);
        } else {
          throw new TypeMisMatchException("Instance Type Not Found");
        }

        stackToken = (Token) objectsStack.pop();
        if (stackToken.kind == ConditionSyntaxCheckerConstants.VARIABLE_NAME) {
          leftOperand.setValue(stackToken.image);
          simplePredicate.setLeftOperand(leftOperand);
        } else {
          throw new TypeMisMatchException("Instance Type Not Found");
        }
        objectsStack.push(simplePredicate);
      } else if (t.image.equals(")")) {
        CompositePredicate compositePredicate = new CompositePredicate();
        IPredicate rightCompositeOperand = (IPredicate) objectsStack.pop();
        compositePredicate.setRightOperand((IOperand) rightCompositeOperand);

        Token stackToken = (Token) objectsStack.pop();
        if (stackToken.kind == ConditionSyntaxCheckerConstants.PREDICATE_OPERATOR) {
          compositePredicate.setOperator(stackToken.image);
        } else {
          throw new TypeMisMatchException("Instance Type Not Found");
        }

        IPredicate leftCompositeOperand = (IPredicate) objectsStack.pop();
        compositePredicate.setLeftOperand((IOperand) leftCompositeOperand);

        objectsStack.push(compositePredicate);
      }
    }

    return (IPredicate) objectsStack.pop();
  }
}
