package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.foundation.apis.processing.IDataObjectProcessor;
import com.ebs.dac.foundation.apis.processing.IDataObjectProcessorFactory;

public class DataObjectProcessorFactory implements IDataObjectProcessorFactory {

  public DataObjectProcessorFactory() {}

  public IDataObjectProcessor createProcessor(Class<?> clss) {
    return new DataObjectProcessor(clss, new DataObjectAttributeProcessorFactory());
  }
}
