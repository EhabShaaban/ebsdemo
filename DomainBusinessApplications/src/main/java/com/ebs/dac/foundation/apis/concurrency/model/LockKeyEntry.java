package com.ebs.dac.foundation.apis.concurrency.model;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public class LockKeyEntry {

  String resourceCode;
  String resourceName;

  public LockKeyEntry(String resourceName, String resourceCode) {
    this.resourceName = resourceName;
    this.resourceCode = resourceCode;
  }

  public String getResourceCode() {
    return resourceCode;
  }

  public void setResourceCode(String resourceCode) {
    this.resourceCode = resourceCode;
  }

  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  @Override
  public int hashCode() {
    HashCodeBuilder builder = new HashCodeBuilder();
    builder.append(resourceCode);
    builder.append(resourceName);
    return builder.toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return this.hashCode() == obj.hashCode();
  }
}
