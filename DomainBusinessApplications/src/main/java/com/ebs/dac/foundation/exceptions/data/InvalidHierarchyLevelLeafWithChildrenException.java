package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.localization.LocalizedException;

public class InvalidHierarchyLevelLeafWithChildrenException extends LocalizedException {

  private static final long serialVersionUID = 1L;

  private static final String message = "";

  private IEntityAttribute attribute;

  public InvalidHierarchyLevelLeafWithChildrenException(IEntityAttribute attribue) {
    super(
            IExceptionsCodes.ITEM_GROUP_HAS_CHILD,
        new Object[] {attribue.getName()},
        message);
    this.attribute = attribue;
  }

  public InvalidHierarchyLevelLeafWithChildrenException() {
    super(IExceptionsCodes.ITEM_GROUP_HAS_CHILD, null, message);
  }

  @Override
  public String getMessage() {
    return String.format(message, attribute.getName());
  }
}
