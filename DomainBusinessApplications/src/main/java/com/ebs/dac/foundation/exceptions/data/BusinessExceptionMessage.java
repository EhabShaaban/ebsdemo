package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.CustomExceptionMessage;
import com.ebs.dac.foundation.exceptions.CustomMessageTextBundle;

public class BusinessExceptionMessage extends CustomExceptionMessage {

  private String logId;

  private CustomMessageTextBundle reason;
  private CustomMessageTextBundle suggestion;

  public String getLogId() {
    return logId;
  }

  public void setLogId(String logId) {
    this.logId = logId;
  }

  public CustomMessageTextBundle getReason() {
    return reason;
  }

  public void setReason(CustomMessageTextBundle reason) {
    this.reason = reason;
  }

  public void setReason(String reason, Object[] reasonArgs) {
    this.reason = new CustomMessageTextBundle(reason, reasonArgs);
  }

  public CustomMessageTextBundle getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(CustomMessageTextBundle suggestion) {
    this.suggestion = suggestion;
  }

  public void setSuggestion(String suggestion, Object[] suggestionArgs) {
    this.suggestion = new CustomMessageTextBundle(suggestion, suggestionArgs);
  }
}
