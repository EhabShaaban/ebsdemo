package com.ebs.dac.foundation.exceptions.operational.accessability;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class DataNavigationException extends Exception implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_DataNavigationError;
  }
}
