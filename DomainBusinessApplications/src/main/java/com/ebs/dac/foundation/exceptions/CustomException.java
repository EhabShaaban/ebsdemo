package com.ebs.dac.foundation.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

public abstract class CustomException extends Exception {

  private static final long serialVersionUID = 1L;
  private CustomExceptionMessage message;

  /** ************************** Constructors ***************************** */
  public CustomException(CustomExceptionMessage message) {
    this.message = message;
  }

  public CustomException(Throwable cause) {
    super(cause);
  }
  /** **************** Abstract Protected Methods ************************* */

  public CustomException(CustomExceptionMessage message, Throwable cause) {
    super(cause);
    this.message = message;
  }

  /** **************** Abstract Protected Methods ************************* */
  protected abstract String getMessageResourceBundleName();
  /** **************** Implemented Protected Methods ********************** */

  // Must Be Called In Child Constructors For Message Construction
  protected abstract void constructMessage();

  protected ResourceBundle getMessageResource(Locale locale) {
    return ResourceBundle.getBundle(getMessageResourceBundleName(), locale);
  }

  /** ****************** Abstract Public Methods ************************** */
  @Override
  public abstract String getMessage();
  /** ************************** Constructors ***************************** */

  public abstract String getLocalizedMessage(Locale locale);

  /** **************** Implemented Public Methods ********************** */
  public CustomExceptionMessage getCustomMessage() {
    return this.message;
  }
  /** ****************** Abstract Public Methods ************************** */

  /** **************** Implemented Protected Methods ********************** */
  protected void setCustomMessage(CustomExceptionMessage message) {
    this.message = message;
  }

  /** **************** Implemented Public Methods ********************** */
}
