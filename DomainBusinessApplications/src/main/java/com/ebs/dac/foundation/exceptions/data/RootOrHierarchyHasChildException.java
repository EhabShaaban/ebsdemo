package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RootOrHierarchyHasChildException extends Exception implements IExceptionResponse {

    private static final long serialVersionUID = 1L;
    private static final Logger logger =
            LoggerFactory.getLogger(ClientBypassingSecurityException.class);
    private final String EX_CODE = IExceptionsCodes.ITEM_GROUP_HAS_CHILD;

    public RootOrHierarchyHasChildException() {
    }

    public RootOrHierarchyHasChildException(String msg) {
        logger.info(msg);
    }

    @Override
    public String getResponseCode() {
        return EX_CODE;
    }

    @Override
    public int getHttpStatusCode() {
        return IExceptionResponse.DEFAULT_EXCEPTION_HTTP_STATUS_CODE;
    }
}
