package com.ebs.dac.foundation.apis.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.realization.processing.AttributeIdentity;
import java.lang.reflect.Field;
import java.util.Collection;

public interface IDataObjectProcessor {

  String ATTRBUITE_NAME_POSTFIX = "Attr";

  /**
   * Retrieve all fields of a class declared & inherited ones
   *
   * @return
   * @throws Exception
   */
  Collection<Field> getAllFields() throws Exception;

  // TODO getReferentialAttributes

  // TODO getPayloadAttributes

  /**
   * Call getter method of targetObject for attribute to retrieve attrValue
   *
   * @param attribute
   * @param sourceObj
   * @return attribute's value from targetObj
   * @throws Exception
   */
  Object getAttributeValue(IEntityAttribute attribute, Object sourceObj) throws Exception;

  /**
   * Call setter method of targetObject for attribute to set attrValue
   *
   * @param attribute
   * @param attrValue
   * @param targetObj
   * @return
   * @throws Exception
   */
  Object setAttributeValue(IEntityAttribute attribute, Object attrValue, Object targetObj)
      throws Exception;

  /**
   * Checks if object has an attribute with "attrName"
   *
   * @param attrName
   * @return
   * @throws Exception
   */
  boolean hasAttribute(String attrName) throws Exception;

  /**
   * This method parses <b>attributePath</b> to get its value and its owner (master object or
   * information object) from <b>targetEntity</b><br>
   * For Example: attributePath is informationObject.attr1 this will return value of attr1 and the
   * owner of attr1 which is information object
   *
   * @param attributePath
   * @param targetEntity
   * @throws Exception
   */
  AttributeIdentity getAttributeIdentity(String attributeName, Class<?> entityInterface)
      throws Exception;
}
