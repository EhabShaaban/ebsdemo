package com.ebs.dac.foundation.exceptions.data;

public abstract class MissingRequiredData extends InvalidBusinessObjectCause {}
