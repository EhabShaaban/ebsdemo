package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.localization.LocalizedException;

public class ClientBypassingSecurityException extends LocalizedException {

  private static final long serialVersionUID = 1L;

  private static final String message = "";

  private IEntityAttribute attribute;

  public ClientBypassingSecurityException(IEntityAttribute attribue) {
    super(
        IExceptionsCodes.CLIENT_BYPASSING_SECURITY_EXCEPTION,
        new Object[] {attribue.getName()},
        message);
    this.attribute = attribue;
  }

  public ClientBypassingSecurityException() {
    super(IExceptionsCodes.CLIENT_BYPASSING_SECURITY_EXCEPTION, null, message);
  }

  @Override
  public String getMessage() {
    return String.format(message, attribute.getName());
  }
}
