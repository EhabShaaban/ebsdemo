package com.ebs.dac.foundation.exceptions;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class CustomMessageTextBundle {

  private String bundleCode;
  private Object[] parameters;

  public CustomMessageTextBundle(String bundleCode, Object[] parameters) {
    this.bundleCode = bundleCode;
    this.parameters = parameters;
  }

  public String getBundleCode() {
    return this.bundleCode;
  }

  public Object[] getParameters() {
    return this.parameters;
  }

  public String getText(ResourceBundle resourceBundle) {
    String text = resourceBundle.getString(bundleCode);

    if (parameters != null) {
      text = MessageFormat.format(text, parameters);
    }

    return text;
  }
}
