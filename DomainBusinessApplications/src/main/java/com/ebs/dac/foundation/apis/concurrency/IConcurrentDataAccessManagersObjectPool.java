package com.ebs.dac.foundation.apis.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IConcurrentDataAccessManagersObjectPool {

  IConcurrentDataAccessManager getConcurrentDataAccessManager(String resourceName);

  Collection<IConcurrentDataAccessManager> getAvailableConcurrentDataAccessManager();

  int getCleanupInterval();

  void setCleanupInterval(int cleanupInterval);

  /**
   * Clean up locks
   *
   * @param all indicates the deletion of all locks if set to true, only the expired ones if set to
   *     false
   */
  void cleanUp(boolean all);

  void evictAll();

  void evictIfPossible(List<String> tokenIds);

  Map<String, List<LockDetails>> listAll();

  void releaseAllLocksByCurrentUser() throws Exception;
}
