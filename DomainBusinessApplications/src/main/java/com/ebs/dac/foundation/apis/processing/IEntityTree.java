package com.ebs.dac.foundation.apis.processing;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;

/** Created by marisoft on 10/5/16. */
public interface IEntityTree<T extends BusinessObject> {

  Class<T> getType();

  <A extends BusinessObject> IEntityTree<A> getAggregate(String attributeName);

  <A extends BusinessObject> ICompositeAttribute<A> getCompositeAttribute(
      String attributeName);

  <A extends BusinessObject> void putAggregate(
      ICompositeAttribute<A> attribute, IEntityTree<A> attributeTree);

  <A extends BusinessObject> IEntityTree<A> getAggregate(ICompositeAttribute<A> attribute);

  <A extends BusinessObject> IEntityTree<A> removeAggregate(
      ICompositeAttribute<A> attribute);

  void setFormats(IEntityAttribute attribute, Class<?> format);

  Class<?> getFormat(IEntityAttribute attribute);

  boolean hasFormats();
}
