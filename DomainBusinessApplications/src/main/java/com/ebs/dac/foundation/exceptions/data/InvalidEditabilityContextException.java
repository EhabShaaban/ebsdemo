package com.ebs.dac.foundation.exceptions.data;

import java.util.Locale;

public class InvalidEditabilityContextException extends GeneralTechnicalException {

  /** */
  private static final long serialVersionUID = 1712826983105254729L;
  private final String message = "Invalid Editabilty Context";

  public InvalidEditabilityContextException() {}

  public InvalidEditabilityContextException(Exception cause) {
    super(cause);
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public String getLocalizedMessage() {
    return message;
  }

  @Override
  public String getLocalizedMessage(Locale locale) {
    return message;
  }
}
