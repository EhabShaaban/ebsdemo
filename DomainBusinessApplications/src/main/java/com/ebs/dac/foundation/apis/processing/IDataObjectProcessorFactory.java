package com.ebs.dac.foundation.apis.processing;

public interface IDataObjectProcessorFactory {
  IDataObjectProcessor createProcessor(Class<?> clss);
}
