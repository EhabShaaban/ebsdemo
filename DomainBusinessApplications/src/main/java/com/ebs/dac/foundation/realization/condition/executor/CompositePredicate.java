package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import java.util.Map;

public class CompositePredicate implements IPredicate, IOperand {

  private IPredicate leftOperand;
  private IPredicate rightOperand;
  private String operator;

  @Override
  public IOperand getRightOperand() {
    return (IOperand) this.rightOperand;
  }

  @Override
  public void setRightOperand(IOperand p) {
    this.rightOperand = (IPredicate) p;
  }

  @Override
  public IOperand getLeftOperand() {
    return (IOperand) this.leftOperand;
  }

  @Override
  public void setLeftOperand(IOperand p) {
    this.leftOperand = (IPredicate) p;
  }

  @Override
  public String getOperator() {
    return this.operator;
  }

  @Override
  public void setOperator(String s) {
    this.operator = s;
  }

  @Override
  public PredicateResult execute(Map<String, Object> map) throws Exception {
    PredicateResult leftResult = leftOperand.execute(map);
    PredicateResult rightResult = rightOperand.execute(map);
    PredicateExecutor predicateExecutor = new PredicateExecutor();
    return predicateExecutor.execute(leftResult, rightResult, getOperator());
  }
}
