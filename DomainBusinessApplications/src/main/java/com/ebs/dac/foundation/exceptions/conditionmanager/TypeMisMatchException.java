package com.ebs.dac.foundation.exceptions.conditionmanager;
// Created By xerix - 2/11/18 - 10:26 AM

public class TypeMisMatchException extends Exception {
  private static final long serialVersionUID = 1L;

  public TypeMisMatchException(String message) {
    super(message);
  }

  public TypeMisMatchException(String message, Exception cause) {
    super(message, cause);
  }
}
