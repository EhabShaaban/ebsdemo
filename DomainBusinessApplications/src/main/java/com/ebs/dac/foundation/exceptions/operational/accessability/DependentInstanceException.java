package com.ebs.dac.foundation.exceptions.operational.accessability;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class DependentInstanceException extends Exception implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_RecordDependence;
  }
}
