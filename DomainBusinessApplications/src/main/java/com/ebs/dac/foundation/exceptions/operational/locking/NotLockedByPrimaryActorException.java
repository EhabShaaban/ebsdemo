package com.ebs.dac.foundation.exceptions.operational.locking;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class NotLockedByPrimaryActorException extends Exception implements IExceptionResponse {
  private static final String EX_CODE = IExceptionsCodes.APP_NOT_LOCKED_BY_PRIMARY_ACTOR_VIOLATION;

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.APP_NOT_LOCKED_BY_PRIMARY_ACTOR_VIOLATION;
  }
}
