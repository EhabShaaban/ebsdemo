package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.foundation.apis.processing.IDataObjectAttributeProcessor;
import com.ebs.dac.foundation.apis.processing.IDataObjectAttributeProcessorFactory;

public class DataObjectAttributeProcessorFactory implements IDataObjectAttributeProcessorFactory {

  @Override
  public IDataObjectAttributeProcessor createAttributeProcessor(IEntityAttribute attribute)
      throws Exception {
    IDataObjectAttributeProcessor processor = null;

    if (attribute == null) {
      throw new IllegalArgumentException("The attribute param must not be null");
    }
    Class attributeClass = attribute.getClass();
    if (ICompositeAttribute.class.isAssignableFrom(attributeClass)) {
      processor = new CompositeAttributeProcessor();
    } else {
      processor = new SimpleAttributeProcessor();
    }
    return processor;
  }
}
