package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.CustomException;
import java.util.Locale;
import java.util.ResourceBundle;

public abstract class BusinessException extends CustomException {

  private static final long serialVersionUID = 1L;

  /** **************** Constructors ************************* */
  public BusinessException() {
    super(new BusinessExceptionMessage());
  }

  public BusinessException(Throwable cause) {
    super(new BusinessExceptionMessage(), cause);
  }
  /** ******** End Of Abstract Protected Methods *********** */

  /**
   * ******************************************************* ********** Abstract Protected Methods
   * ****************** ********** The Implementor Must Call Them ************** ********** In The
   * Constructor After Calling ************ ********** The Super Constructor ***********************
   * *******************************************************
   */
  protected abstract void constructReason();

  protected abstract void constructSuggestion();
  /** ******************* Constructors ************************* */

  /** **************** Abstract Public Methods ***************** */

  /** **************** Abstract Public Methods ***************** */
  public BusinessExceptionMessage getBusinessMessage() {
    return ((BusinessExceptionMessage) super.getCustomMessage());
  }

  @Override
  public String getMessage() {

    if (getBusinessMessage().getMessage() == null) {
      return null;
    }

    Locale defaultLocale = Locale.ENGLISH;
    ResourceBundle messageResource = getMessageResource(defaultLocale);
    return getBusinessMessage().getMessage().getText(messageResource);
  }

  @Override
  public String getLocalizedMessage(Locale locale) {

    if (getBusinessMessage().getMessage() == null) {
      return null;
    }

    ResourceBundle messageResource = getMessageResource(locale);
    return getBusinessMessage().getMessage().getText(messageResource);
  }

  public String getReason() {

    if (getBusinessMessage().getReason() == null) {
      return null;
    }

    Locale defaultLocale = Locale.ENGLISH;
    ResourceBundle messageResource = getMessageResource(defaultLocale);
    return getBusinessMessage().getReason().getText(messageResource);
  }

  public String getLocalizedReason(Locale locale) {

    if (getBusinessMessage().getReason() == null) {
      return null;
    }

    ResourceBundle messageResource = getMessageResource(locale);
    return getBusinessMessage().getReason().getText(messageResource);
  }

  public String getSuggestion() {

    if (getBusinessMessage().getSuggestion() == null) {
      return null;
    }

    Locale defaultLocale = Locale.ENGLISH;
    ResourceBundle messageResource = getMessageResource(defaultLocale);
    return getBusinessMessage().getSuggestion().getText(messageResource);
  }

  public String getLocalizedSuggestion(Locale locale) {

    if (getBusinessMessage().getSuggestion() == null) {
      return null;
    }

    ResourceBundle messageResource = getMessageResource(locale);
    return getBusinessMessage().getSuggestion().getText(messageResource);
  }
}
