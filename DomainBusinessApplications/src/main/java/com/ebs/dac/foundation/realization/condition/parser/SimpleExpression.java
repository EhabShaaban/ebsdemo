package com.ebs.dac.foundation.realization.condition.parser;

import com.ebs.dda.approval.dbo.jpa.entities.enums.OperatorBetweenCondition;
import com.ebs.dda.approval.dbo.jpa.entities.fields.ConditionFields;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 19, 2018 10:42:51 AM
 */
class SimpleExpression implements IExpression {

  private ConditionFields leftCondition;
  private ConditionFields rightCondition;

  public SimpleExpression(ConditionFields leftCondition, ConditionFields rightCondition) {
    this.leftCondition = leftCondition;
    this.rightCondition = rightCondition;
  }

  public SimpleExpression(ConditionFields leftCondition) {
    this.leftCondition = leftCondition;
  }

  public ConditionFields getLeftCondition() {
    return leftCondition;
  }

  public void setLeftCondition(ConditionFields leftCondition) {
    this.leftCondition = leftCondition;
  }

  public ConditionFields getRightCondition() {
    return rightCondition;
  }

  public void setRightCondition(ConditionFields rightCondition) {
    this.rightCondition = rightCondition;
  }

  @Override
  public Object getLeftExpression() {
    return leftCondition;
  }

  @Override
  public Object getRightExpression() {
    return rightCondition;
  }

  @Override
  public OperatorBetweenCondition getMiddleExpressionOperator() {
    return leftCondition.getConditionOperator();
  }

  @Override
  public OperatorBetweenCondition getRightExpressionOperator() {
    if (rightCondition != null) {
      return rightCondition.getConditionOperator();
    }
    return leftCondition.getConditionOperator();
  }

  @Override
  public String toString() {
    String str = leftCondition.toString();
    if (rightCondition != null) {
      str += " " + rightCondition.toString();
    }
    return str;
  }
}
