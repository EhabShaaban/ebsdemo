package com.ebs.dac.foundation.exceptions.localization;

public class ExceptionsBundlesNames {

  private static final String EXCEPTIONS_BUNDLES_BASE_PATH = "META-INF/ExceptionMessages/";

  public static final String BUSINESS_EXCEPTIONS_BUNDLE_NAME =
      EXCEPTIONS_BUNDLES_BASE_PATH + "BusinessExceptions";

  public static final String EXCEPTIONS_MESSAGES_BUNDLE_NAME =
      EXCEPTIONS_BUNDLES_BASE_PATH + "ExceptionsMessages";
}
