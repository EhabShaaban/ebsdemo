package com.ebs.dac.foundation.exceptions.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ebs.dac.foundation.exceptions.data.ClientBypassingSecurityException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ArgumentViolationSecurityException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  private final String EX_CODE = IExceptionsCodes.General_INVALID_INPUT;
  private static final Logger logger = LoggerFactory.getLogger(ClientBypassingSecurityException.class);

  public ArgumentViolationSecurityException() {}

  public ArgumentViolationSecurityException(String msg) {
    logger.info(msg);
  }

  @Override
  public String getResponseCode() {
    return EX_CODE;
  }

  @Override
  public int getHttpStatusCode() {
    return IExceptionResponse.SECURITY_VIOLATION_HTTP_STATUS_CODE;
  }
}
