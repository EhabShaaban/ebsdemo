package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class MissingSystemDataException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  public MissingSystemDataException(String message) {
    super(message);
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.INTERNAL_SERVER_ERROR;
  }

}
