package com.ebs.dac.foundation.apis.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;

public interface IDataObjectAttributeProcessor {
  Object getAttributeValue(IEntityAttribute attribute, Object sourceObj) throws Exception;

  Object setAttributeValue(IEntityAttribute attribute, Object attrValue, Object targetObj)
      throws Exception;
}
