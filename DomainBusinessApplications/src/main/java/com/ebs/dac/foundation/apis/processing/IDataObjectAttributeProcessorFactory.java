package com.ebs.dac.foundation.apis.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;

/** Created by marisoft on 10/12/16. */
public interface IDataObjectAttributeProcessorFactory {

  IDataObjectAttributeProcessor createAttributeProcessor(IEntityAttribute attribute)
      throws Exception;
}
