package com.ebs.dac.foundation.realization.condition.adapter;

import com.ebs.dac.foundation.realization.condition.executor.IPredicate;
import com.ebs.dac.foundation.realization.condition.executor.LexicalAnalyser;
import com.ebs.dac.foundation.realization.condition.parser.ConditionSyntaxChecker;

import java.util.*;

/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

public class ConditionManager {

  private Map<String,Object> resolvedreservedWords;
  private ArrayList<String> reservedWords;

  public ConditionManager(){
    resolvedreservedWords = new HashMap<>();
    reservedWords = new ArrayList<>();
      reservedWords.add("LoggedInUser");
  }

  public ConditionManager(Map<String,Object> resolvedreservedWords){
    this();
    setResolvedreservedWords(resolvedreservedWords);
  }

  public void setResolvedreservedWords(Map<String,Object> resolvedreservedWords){
      this.resolvedreservedWords.putAll(resolvedreservedWords);
  }

  public boolean validateCondition(String condition) {
    ConditionSyntaxChecker conditionSyntaxChecker = new ConditionSyntaxChecker();
    return conditionSyntaxChecker.isVaild(condition);
  }

  public boolean executeCondition(Map<String, Object> map, String condition) throws Exception {
    if (condition.trim().length() == 0) {
      return true;
    }
      String resolvedCondition = resolveReservedWords(condition);
      if (validateCondition(resolvedCondition)) {
      LexicalAnalyser lexicalAnalyser = new LexicalAnalyser();
          IPredicate predicate = lexicalAnalyser.createConditionObject(resolvedCondition);
      return predicate.execute(map).isResult();
    }
    return false;
  }

  private String resolveReservedWords(String condition) {
    String resolvedCondition = condition;
    for (String reservedWord : reservedWords) {
      resolvedCondition = resolveWord(reservedWord,resolvedCondition);
    }
    return resolvedCondition;
  }

  private String resolveWord(String reservedWord, String condition) {
    return condition.replace(reservedWord, String.valueOf(resolvedreservedWords.get(reservedWord)));
  }

  public Set<String> getConditionVariables(String condition) throws Exception {
    if (validateCondition(condition)) {
      LexicalAnalyser lexicalAnalyser = new LexicalAnalyser();
      lexicalAnalyser.createConditionObject(condition);
      return lexicalAnalyser.getConditionVariables();
    }
    return new HashSet<>();
  }
}
