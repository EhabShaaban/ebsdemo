package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 22, 2018 1:25:11 PMembeddedAttrInterface
 */
public class AttributeIdentity {

  private String attributeName;
  private IEntityAttribute attributeType;
  // for non-simple attribute as EmbeddedAttribute
  private Class<?> attributeInterface;
  private boolean isMultiple;

  public AttributeIdentity(String attributeName, IEntityAttribute attributeType) {
    this.attributeName = attributeName;
    this.attributeType = attributeType;
  }

  public String getAttributeName() {
    return attributeName;
  }

  public void setAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }

  public IEntityAttribute getAttributeType() {
    return attributeType;
  }

  public void setAttributeType(IEntityAttribute attributeType) {
    this.attributeType = attributeType;
  }

  public Class<?> getAttributeInterface() {
    return attributeInterface;
  }

  public void setAttributeInterface(Class<?> attributeInterface) {
    this.attributeInterface = attributeInterface;
  }

  public boolean isMultiple() {
    return isMultiple;
  }

  public void setMultiple(boolean isMultiple) {
    this.isMultiple = isMultiple;
  }
}
