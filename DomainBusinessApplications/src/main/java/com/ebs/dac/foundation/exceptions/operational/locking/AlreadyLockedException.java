package com.ebs.dac.foundation.exceptions.operational.locking;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class AlreadyLockedException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  private LockDetails currentLock;

  public AlreadyLockedException(LockDetails currentLock) {
    this.currentLock = currentLock;
  }

  public AlreadyLockedException(Exception e) {
    super(e);
  }

  public AlreadyLockedException() {}

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_AlreadyLocked;
  }
}
