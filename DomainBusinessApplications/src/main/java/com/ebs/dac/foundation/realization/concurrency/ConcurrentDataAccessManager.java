package com.ebs.dac.foundation.realization.concurrency;

import com.ebs.dac.foundation.apis.cache.ICache;
import com.ebs.dac.foundation.apis.cache.ICacheFactory;
import com.ebs.dac.foundation.apis.concurrency.ICAMListener;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.IEditTokenValidityPolicy;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.concurrency.model.LockKeyEntry;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.exceptions.concurrency.InvalidResourceEditTokenException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyLockedException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyUnlockedException;
import com.ebs.dac.foundation.exceptions.operational.locking.NotLockedByPrimaryActorException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.realization.concurrency.policies.TimeoutEditTokenValidityPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This concurrent realization manager acts as a lock manager to a specific resource. Every resource
 * type should have its own {@link ConcurrentDataAccessManager} instance, this is mainly due to
 * having all operations as synchronized hence separating them would have performance benefit of
 * avoid wait/resume bottleneck in a multi-threaded environments <br>
 * <br>
 * Implementation Decisions: Defensive copying is used to protect encapsulation of {@link
 * LockDetails}
 *
 * @author Hesham Saleh
 */
public class ConcurrentDataAccessManager
    implements IConcurrentDataAccessManager, ConcurrentDataAccessManagerMBean {

  private static final Logger logger = LoggerFactory.getLogger(ConcurrentDataAccessManager.class);
  private final InternalMap currentLocks;
  private ICacheFactory cacheFactory;
  private String resourceRoot;
  private IEditTokenValidityPolicy validityStrategy;

  public ConcurrentDataAccessManager(
      String resourceRoot,
      IEditTokenValidityPolicy editTokenValidityPolicy,
      ICacheFactory cacheFactory) {
    this.resourceRoot = resourceRoot;
    this.cacheFactory = cacheFactory;
    this.currentLocks = new InternalMap();
    if (editTokenValidityPolicy == null) {
      this.validityStrategy = new TimeoutEditTokenValidityPolicy();
    } else {
      this.validityStrategy = editTokenValidityPolicy;
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#lock(java.lang.String, IBDKUser)
   */

  public LockDetails lock(String resourceName, String resourceCode, IBDKUser identity)
      throws Exception {
    return lock(resourceName, resourceCode, identity, null);
  }

  public LockDetails lock(
      String resourceName, String resourceCode, IBDKUser identity, ICAMListener listener)
      throws Exception {
    synchronized (currentLocks) {
      checkIfThereArePreviousLockNotExpired(resourceName, resourceCode, identity);
      if (resourceName.equals("*")) {
        checkIfTheBusinessObjectCanBeLockedAsWhole(resourceCode, identity);
      }
      return doActualLock(resourceName, resourceCode, identity, listener);
    }
  }

  private LockDetails doActualLock(
      String resourceName, String resourceCode, IBDKUser identity, ICAMListener listener) {
    String tokenId = UUID.randomUUID().toString(); // TODO Maybe enhance this ? e.g. Include
    DateTime creationDate = new DateTime(); // TODO Consider the TimeZone here
    DateTime expiryDate = validityStrategy.calculateTokenExpiryDate(creationDate);
    Long age = expiryDate.getMillis() - creationDate.getMillis();
    LockDetails lock =
        new LockDetails(
            resourceRoot,
            resourceName,
            resourceCode,
            tokenId,
            creationDate,
            expiryDate,
            identity,
            age);
    currentLocks.put(lock, listener);
    return new LockDetails(lock);
  }

  private void checkIfTheBusinessObjectCanBeLockedAsWhole(String resourceCode, IBDKUser identity)
      throws Exception {
    List<LockDetails> currentLocksOnResource =
        currentLocks.getLocksOnResourceUserCode(resourceCode);
    if (currentLocksOnResource.size() > 0) {
      for (LockDetails lock : currentLocksOnResource) {
        checkIfAlreadyLockedAndNotExpired(lock, identity);
        removeExpiredLockDetails(lock);
      }
    }
  }

  private void checkIfThereArePreviousLockNotExpired(String resourceName, String resourceCode,
      IBDKUser identity)
      throws Exception {
    LockDetails previousLock =
        currentLocks.getByResourceNameAndUserCode(resourceName, resourceCode);
    checkIfAlreadyLockedAndNotExpired(previousLock, identity);
    removeExpiredLockDetails(previousLock);
  }

  private void checkIfAlreadyLockedAndNotExpired(LockDetails lock, IBDKUser identity)
      throws Exception {
    if (lock != null && lock.getExpiryDate().isAfterNow()) {
      if (lock.getIdentity().equals(identity)) {
        throw new ResourceAlreadyLockedBySameUserException();
      }
      throw new ResourceAlreadyLockedException(new LockDetails(lock));
    }
  }

  private void removeExpiredLockDetails(LockDetails lock) {
    if (lock != null && lock.getExpiryDate().isBeforeNow()) {
      currentLocks.remove(lock.getTokenId());
    }
  }
  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#unlock(java.lang.String)
   */

  public LockDetails unlock(String tokenId)
      throws ResourceAlreadyUnlockedException, InvalidResourceEditTokenException {
    synchronized (currentLocks) {
      LockDetails previousLock = currentLocks.getByToken(tokenId);
      if (previousLock == null) {
        throw new ResourceAlreadyUnlockedException();
      }
      currentLocks.remove(previousLock.getTokenId());
      return new LockDetails(previousLock);
    }
  }

  public LockDetails unlock(String resourceName, String resourceCode, IBDKUser loggedInUser)
      throws ResourceAlreadyUnlockedException, NotLockedByPrimaryActorException {
    synchronized (currentLocks) {
      LockDetails previousLock =
          currentLocks.getByResourceNameAndUserCode(resourceName, resourceCode);
      if (previousLock == null) {
        throw new ResourceAlreadyUnlockedException();
      }

      if (!previousLock.getIdentity().equals(loggedInUser)) {
        throw new NotLockedByPrimaryActorException();
      }

      currentLocks.remove(previousLock.getTokenId());
      return new LockDetails(previousLock);
    }
  }
  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#verifyToken(java.lang.String,
   * java.lang.String)
   */

  public void verifyToken(String tokenId, String resourceName, String resourceCode)
      throws InvalidResourceEditTokenException {
    if (!currentLocks.contains(tokenId, resourceName, resourceCode)) {
      throw new InvalidResourceEditTokenException();
    }
  }

  public void checkIfResourceIsLockedByUser(
      String resourceName, String resourceCode, IBDKUser loggedInUser) throws Exception {
    LockDetails lock = currentLocks.getByResourceNameAndUserCode(resourceName, resourceCode);
    if (lock != null && lock.getIdentity().equals(loggedInUser)) {
      if (lock.getExpiryDate().isBeforeNow()) { // remove lock if expired
        currentLocks.remove(lock.getTokenId());
        throw new NotLockedByPrimaryActorException();
      }
      return;
    }
    throw new NotLockedByPrimaryActorException();
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#extendLock(java.lang.String)
   */

  public LockDetails extendLock(String tokenId, String resourceName, String resourceCode)
      throws InvalidResourceEditTokenException {
    synchronized (currentLocks) {
      LockDetails lock = currentLocks.getByResourceNameAndUserCode(resourceName, resourceCode);
      if (lock == null || !lock.getTokenId().equals(tokenId)) {
        throw new InvalidResourceEditTokenException();
      }
      DateTime oldExpiryDate = lock.getExpiryDate();
      DateTime newExpiryDate =
          validityStrategy.extendTokenExpiryDate(lock.getCreationDate(), lock.getExpiryDate());
      lock.setExpiryDate(newExpiryDate);
      lock.setAge(newExpiryDate.getMillis() - oldExpiryDate.getMillis());
      return new LockDetails(lock);
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#cleanUp()
   */

  public void cleanUp(boolean all) {
    synchronized (currentLocks) {
      Collection<LockDetails> locks = currentLocks.getLocks();
      Collection<String> toRemove = new ArrayList<String>();
      for (LockDetails lock : locks) {
        if (all || lock.getExpiryDate().isBeforeNow()) {
          toRemove.add(lock.getTokenId());
        }
      }
      currentLocks.removeAll(toRemove);
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#listAllLocks()
   */

  public List<LockDetails> listAllLocks() {
    List<LockDetails> toRet = new ArrayList<LockDetails>();
    Collection<LockDetails> locks = currentLocks.getLocks();
    for (LockDetails lock : locks) {
      toRet.add(new LockDetails(lock));
    }
    return toRet;
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#listLocksAcquiredBy(IBDKUser)
   */

  public List<LockDetails> listLocksAcquiredBy(IBDKUser identity) {
    List<LockDetails> toRet = new ArrayList<LockDetails>();
    Collection<LockDetails> locks = currentLocks.getLocks();
    for (LockDetails lock : locks) {
      if (lock.getIdentity().equals(identity)) {
        toRet.add(new LockDetails(lock));
      }
    }
    return toRet;
  }

  public boolean isListLocksAcquiredForResourceEmpty(String resourceCode) {
    return listLocksAcquiredForResource(resourceCode).isEmpty();
  }

  public List<LockDetails> listLocksAcquiredForResource(String resourceCode) {
    List<LockDetails> toRet = new ArrayList<LockDetails>();
    Collection<LockDetails> locks = currentLocks.getLocks();
    for (LockDetails lock : locks) {
      if (lock.getResourceCode().equals(resourceCode)) {
        toRet.add(new LockDetails(lock));
      }
    }
    return toRet;
  }

  public List<LockDetails> listLocksAcquiredForResourceByUser(String resourceCode, IBDKUser user) {
    List<LockDetails> locksByResourceCodeAndUser = new ArrayList<LockDetails>();
    List<LockDetails> locks = listLocksAcquiredForResource(resourceCode);
    for (LockDetails lock : locks) {
      if (lock.getIdentity().equals(user)) {
        locksByResourceCodeAndUser.add(new LockDetails(lock));
      }
    }
    return locksByResourceCodeAndUser;
  }

  public List<String> listResourceNameLocksAcquiredForResource(String resourceCode) {
    List<String> resourceNames = new ArrayList<String>();
    Collection<LockDetails> locks = currentLocks.getLocks();
    for (LockDetails lock : locks) {
      if (lock.getResourceCode().equals(resourceCode)) {
        resourceNames.add(lock.getResourceName());
      }
    }
    return resourceNames;
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#getValidityStrategy()
   */

  public IEditTokenValidityPolicy getValidityStrategy() {
    return validityStrategy;
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * IConcurrentDataAccessManager#setValidityStrategy(IEditTokenValidityPolicy)
   */

  public void setValidityStrategy(IEditTokenValidityPolicy validityStrategy) {
    this.validityStrategy = validityStrategy;
  }

  /*
   * (non-Javadoc)
   *
   * @see IConcurrentDataAccessManager#getResourceRoot()
   */

  public String getResourceRoot() {
    return resourceRoot;
  }

  public LockDetails getByTokenId(String tokenId) throws NotLockedByPrimaryActorException {
    try {
      LockDetails lockDetails = currentLocks.getByToken(tokenId);
      return lockDetails;
    } catch (Exception e) {
      throw new NotLockedByPrimaryActorException();
    }
  }

  public void registerListener(String tokenId, ICAMListener listener)
      throws InvalidResourceEditTokenException {
    try {
      LockDetails lockDetails = currentLocks.getByToken(tokenId);
      if (listener != null) currentLocks.put(lockDetails, listener);
    } catch (Exception e) {
      throw new InvalidResourceEditTokenException();
    }
  }

  // =======================

  public void unlockAllLockedResourcesByUser(IBDKUser loggedInUser) throws Exception {
    List<LockDetails> userLocks = listLocksAcquiredBy(loggedInUser);
    for (LockDetails lockDetails : userLocks) {
      unlock(lockDetails.getTokenId());
    }
  }

  /** Use some regex map, and combine token with resource ID ? */
  private class InternalMap {

    // Logically, a HashMap would suffice if the client is not accessing these
    // methods except from synchronized blocks, but it is defined as
    // ConcurrentHashmap to
    // protect client misuse

    boolean isCacheEnabled = false;
    private Map<String, LockKeyEntry> tokenIdMap = new ConcurrentHashMap<String, LockKeyEntry>();
    private Map<LockKeyEntry, LockDetails> lockEntryMap = new ConcurrentHashMap<>();

    // Saving in cache server
    private Map<String, ICAMListener> listeners = new ConcurrentHashMap<String, ICAMListener>();
    private ICache<String, LockKeyEntry> tokenIdCacheMap;
    private ICache<LockKeyEntry, LockDetails> lockEntryCacheMap;

    public InternalMap() {
      isCacheEnabled = (cacheFactory != null);
      if (isCacheEnabled) {
        tokenIdCacheMap =
            cacheFactory.createConcurrentAccessManagerCache("tokenIdCacheMap_" + getResourceRoot());
        lockEntryCacheMap =
            cacheFactory.createConcurrentAccessManagerCache(
                "lockEntryCacheMap_" + getResourceRoot());
      }
    }

    public void put(LockDetails lockDetails, ICAMListener listener) {
      LockKeyEntry lockKeyEntry =
          new LockKeyEntry(lockDetails.getResourceName(), lockDetails.getResourceCode());
      tokenIdMap.put(lockDetails.getTokenId(), lockKeyEntry);
      lockEntryMap.put(lockKeyEntry, lockDetails);
      addListener(lockDetails.getTokenId(), listener);
      if (isCacheEnabled) {
        tokenIdCacheMap.put(lockDetails.getTokenId(), lockKeyEntry, true);
        lockEntryCacheMap.put(lockKeyEntry, lockDetails, true);
      }
    }

    public boolean contains(String tokenId, String resourceName, String resourceCode) {
      try {
        LockKeyEntry lockKeyEntry = tokenIdMap.get(tokenId);
        return lockKeyEntry != null
            && lockKeyEntry.getResourceCode().equals(resourceCode)
            && lockKeyEntry.getResourceName().equals(resourceName)
            && lockEntryMap.containsKey(lockKeyEntry);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);

        if (isCacheEnabled) {
          // Search in the cache
          try {
            LockKeyEntry lockKeyEntry = tokenIdCacheMap.get(tokenId);
            // If found in the remote cache, then add it to the local cache
            LockDetails lockDetails = lockEntryCacheMap.get(lockKeyEntry);
            if (lockKeyEntry.getResourceCode().equals(resourceCode) && (lockDetails != null)) {
              tokenIdMap.put(lockDetails.getTokenId(), lockKeyEntry);
              lockEntryMap.put(lockKeyEntry, lockDetails);
              return true;
            } else {
              return false;
            }
          } catch (Exception e2) {
            return false;
          }
        }

        return false;
      }
    }

    public LockDetails remove(String tokenId) {
      LockDetails lockDetails = null;

      LockKeyEntry lockKeyEntry = tokenIdMap.get(tokenId);
      if (lockKeyEntry != null) {
        lockDetails = lockEntryMap.remove(lockKeyEntry);
        tokenIdMap.remove(tokenId);
      } else if (isCacheEnabled) { // Search in the cache
        lockKeyEntry = tokenIdCacheMap.get(tokenId);

        if (lockKeyEntry != null) {
          tokenIdCacheMap.remove(tokenId, false);
          lockEntryCacheMap.remove(lockKeyEntry, false);
        }
      }

      fireListener(lockDetails);
      removeListener(tokenId);

      return lockDetails;
    }

    public void removeAll(Collection<String> tokenIdsList) {
      for (String tokenId : tokenIdsList) {
        remove(tokenId);
      }
    }

    public Collection<LockDetails> getLocks() {
      Collection<LockDetails> values = lockEntryMap.values();
      if (isCacheEnabled && values.isEmpty()) {
        values = lockEntryCacheMap.values();
      }
      return values;
    }

    public LockDetails getByToken(String tokenId) {
      LockKeyEntry lockKeyEntry = tokenIdMap.get(tokenId);
      if (lockKeyEntry == null) {
        return null;
      }
      return lockEntryMap.get(tokenIdMap.get(tokenId));
    }

    public LockDetails getByResourceNameAndUserCode(String resourceName, String resourceCode) {
      LockKeyEntry lockKeyEntry = new LockKeyEntry(resourceName, resourceCode);
      LockDetails lockDetails = lockEntryMap.get(lockKeyEntry);
      if (isCacheEnabled) {
        if (lockDetails == null) {
          lockDetails = lockEntryCacheMap.get(lockKeyEntry);
        }
      }
      return lockDetails;
    }

    public List<LockDetails> getLocksOnResourceUserCode(String resourceCode) {
      List<LockDetails> foundLockDetailsOnResource = new ArrayList<LockDetails>();
      for (LockKeyEntry lockKeyEntry : lockEntryMap.keySet()) {
        if (lockKeyEntry.getResourceCode().equals(resourceCode)) {
          LockDetails lock = lockEntryMap.get(lockKeyEntry);
          if (lock != null) foundLockDetailsOnResource.add(lock);
        }
      }
      return foundLockDetailsOnResource;
    }

    private void addListener(String tokenId, ICAMListener listener) {
      if (tokenId != null && listener != null) listeners.put(tokenId, listener);
    }

    private void removeListener(String tokenId) {
      if (tokenId != null) listeners.remove(tokenId);
    }

    private void fireListener(LockDetails lockDetails) {
      if (lockDetails == null || lockDetails.getTokenId() == null) return;
      ICAMListener listener = listeners.get(lockDetails.getTokenId());
      if (listener != null) listener.doWhenUnlock(lockDetails);
    }
  }
}
