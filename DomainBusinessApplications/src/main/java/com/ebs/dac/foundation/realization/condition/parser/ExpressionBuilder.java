package com.ebs.dac.foundation.realization.condition.parser;


import com.ebs.dda.approval.dbo.jpa.entities.enums.OperatorBetweenCondition;
import com.ebs.dda.approval.dbo.jpa.entities.fields.ConditionFields;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 19, 2018 10:41:55 AM
 */
class ExpressionBuilder {

  public ExpressionBuilder() {}

  public IExpression build(List<ConditionFields> conditionsList) {

    LinkedList<IExpression> expressionsList = buildSimpleExpressionsList(conditionsList);

    return buildExpressionTree(expressionsList);
  }

  private LinkedList<IExpression> buildSimpleExpressionsList(List<ConditionFields> conditionsList) {
    LinkedList<IExpression> expressions = new LinkedList<>();

    for (int i = 0; i < conditionsList.size(); i++) {

      ConditionFields curCondition = conditionsList.get(i);
      ConditionFields nxtCondition =
          (i < conditionsList.size() - 1) ? conditionsList.get(i + 1) : null;

      if (isExpressionEnded(curCondition)) {
        expressions.add(new SimpleExpression(curCondition));
      }

      if (nxtCondition != null) {
        if (isAndedExpression(curCondition)) {
          expressions.add(new SimpleExpression(curCondition, nxtCondition));
          i++;
        }
        if (isOrExpression(curCondition)) {
          if (isAndedExpression(nxtCondition)) {
            expressions.add(new SimpleExpression(curCondition));
          } else {
            expressions.add(new SimpleExpression(curCondition, nxtCondition));
            i++;
          }
        }
      }

      if (i == conditionsList.size() - 1 && !isExpressionEnded(conditionsList.get(i))) {
        throw new IllegalArgumentException("Invalid Expression");
      }
    }
    return expressions;
  }

  private IExpression buildExpressionTree(LinkedList<IExpression> expressionsList) {

    while (expressionsList.size() > 1) {

      for (int i = 0; i < expressionsList.size(); i++) {
        IExpression ex = expressionsList.get(i);
        IExpression nxtEx = (i < expressionsList.size() - 1) ? expressionsList.get(i + 1) : null;

        if (nxtEx != null) {
          if (isAndOperator(ex.getRightExpressionOperator())
              || (isOrOperator(ex.getRightExpressionOperator())
                  && !isAndOperator(nxtEx.getRightExpressionOperator()))) {
            expressionsList.remove(i);
            expressionsList.remove(i);
            expressionsList.add(i, new CompositeExpression(ex, nxtEx));
            break;
          }
        }
      }
    }
    return expressionsList.peek();
  }

  private boolean isAndedExpression(ConditionFields condition) {
    return condition.getConditionOperator() != null
        && isAndOperator(condition.getConditionOperator());
  }

  private boolean isAndOperator(OperatorBetweenCondition operator) {
    return operator.toString().equals(OperatorBetweenCondition.AND.toString());
  }

  private boolean isOrExpression(ConditionFields condition) {
    return condition.getConditionOperator() != null
        && isOrOperator(condition.getConditionOperator());
  }

  private boolean isOrOperator(OperatorBetweenCondition operator) {
    return operator.toString().equals(OperatorBetweenCondition.OR.toString());
  }

  private boolean isExpressionEnded(ConditionFields condition) {
    return condition.getConditionOperator() == null
        || condition
            .getConditionOperator()
            .toString()
            .equals(OperatorBetweenCondition.END.toString());
  }
}
