package com.ebs.dac.foundation.exceptions.operational.accessability;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;

public class ObjectStateNotValidException extends BusinessRuleViolationException {

  private static final long serialVersionUID = 1L;

  public ObjectStateNotValidException() {
    super();
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH;
  }
}
