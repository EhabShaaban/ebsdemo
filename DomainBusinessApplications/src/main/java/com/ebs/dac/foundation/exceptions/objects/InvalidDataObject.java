package com.ebs.dac.foundation.exceptions.objects;

public class InvalidDataObject extends Exception {

  private static final String message = "[%1$s] is not a valid realization object";
  private Class<?> objType;

  public InvalidDataObject(Class<?> objType, Exception cause) {
    super(message, cause);
    this.objType = objType;
  }

  @Override
  public String getMessage() {
    // TODO Auto-generated method stub
    return super.getMessage();
  }
}
