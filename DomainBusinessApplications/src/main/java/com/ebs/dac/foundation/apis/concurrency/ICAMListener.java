package com.ebs.dac.foundation.apis.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;

public interface ICAMListener {

  void doWhenUnlock(LockDetails lockDetails);
}
