package com.ebs.dac.foundation.realization.condition.executor;

/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/
public class ConditionTypes {
  public static final String LONG = "long";
  public static final String STRING = "string";
  public static final String DOUBLE = "double";
  public static final String FLOAT = "float";
  public static final String INTEGER = "integer";
  public static final String BYTE = "byte";
  public static final String SHORT = "short";
}
