package com.ebs.dac.foundation.realization.condition.parser;

import com.ebs.dda.approval.dbo.jpa.entities.enums.OperatorBetweenCondition;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 19, 2018 11:38:45 AM
 */
public interface IExpression {

  Object getLeftExpression();

  Object getRightExpression();

  OperatorBetweenCondition getMiddleExpressionOperator();

  OperatorBetweenCondition getRightExpressionOperator();
}
