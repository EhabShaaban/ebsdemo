package com.ebs.dac.foundation.realization.validation.validationrules;

import com.ebs.dac.foundation.realization.validation.exceptions.MissingMandatoryAttributeException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;

public class MandatoryAttributeRule implements IValidationAttributeRule {

  @Override
  public boolean validate(String attrName, Object attrValue, ValidationResult validationResult)
      throws Exception {
    if (attrValue != null) {
      return true;
    }
    validationResult.bindError(
        attrName, new MissingMandatoryAttributeException().getLocalizedMessage());
    return false;
  }
}
