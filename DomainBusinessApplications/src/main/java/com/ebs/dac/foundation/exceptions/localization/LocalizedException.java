package com.ebs.dac.foundation.exceptions.localization;

import com.ebs.dac.i18n.localizedbundles.AttributesResourceBundle;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
@Deprecated
public class LocalizedException extends Exception {

  private static final long serialVersionUID = 1L;
  transient Object[] parameters;
  private String code;

  public LocalizedException(String code, Object[] parameters, String message) {
    this(code, parameters, message, null);
  }

  public LocalizedException(String code, String message, Object... parameters) {
    this(code, parameters, message, null);
  }

  public LocalizedException(String code, String message, Exception cause, Object... parameters) {
    this(code, parameters, message, cause);
  }

  public LocalizedException(String code, Object[] parameters, String message, Exception cause) {
    super(message, cause);
    this.code = code;
    this.parameters = parameters;
  }

  public String getLocalizedMessage(Locale locale) {
    ResourceBundle coreResourceBundle =
        ResourceBundle.getBundle(ExceptionsBundlesNames.EXCEPTIONS_MESSAGES_BUNDLE_NAME, locale);

    ResourceBundle businessResourceBundle =
        ResourceBundle.getBundle(ExceptionsBundlesNames.BUSINESS_EXCEPTIONS_BUNDLE_NAME, locale);

    String localizedMessage = null;
    if (coreResourceBundle != null) {
      try {
        localizedMessage = coreResourceBundle.getString(code);
      } catch (MissingResourceException e) {
        if (businessResourceBundle != null) {
          localizedMessage = businessResourceBundle.getString(code);
        }
      }
      if (localizedMessage == null) {
        localizedMessage = getMessage();
      } else {
        if (parameters != null && parameters.length != 0) {
          parameters = AttributesResourceBundle.getLocalizedAttributes(parameters, locale);
          localizedMessage = MessageFormat.format(localizedMessage, parameters);
        }
      }
    }
    return localizedMessage;
  }

  public String getCode() {
    return this.code;
  }
}
