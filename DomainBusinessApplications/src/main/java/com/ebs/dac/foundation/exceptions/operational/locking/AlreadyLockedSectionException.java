package com.ebs.dac.foundation.exceptions.operational.locking;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

// Created By Hossam Hassan @ 7/29/18
public class AlreadyLockedSectionException extends Exception implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  private LockDetails currentLock;

  public AlreadyLockedSectionException(LockDetails currentLock) {
    this.currentLock = currentLock;
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_AlreadyLockedSection;
  }
}
