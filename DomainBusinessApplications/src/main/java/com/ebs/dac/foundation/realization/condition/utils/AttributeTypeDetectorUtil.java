package com.ebs.dac.foundation.realization.condition.utils;

import com.ebs.dac.dbo.types.BusinessEnum;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.realization.condition.executor.ConditionTypes;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 25, 2018 4:41:50 PM
 */
public class AttributeTypeDetectorUtil {

  public static boolean isPrimitiveValue(Object attrValue) {
    return String.class.isAssignableFrom(attrValue.getClass())
        || Number.class.isAssignableFrom(attrValue.getClass())
        || Boolean.class.isAssignableFrom(attrValue.getClass());
  }

  public static boolean isEnumValue(Object attrValue) {
    return BusinessEnum.class.isAssignableFrom(attrValue.getClass());
  }

  public static boolean isBasicAttributeValue(Object attrValue) {
    return isPrimitiveValue(attrValue) || isEnumValue(attrValue);
  }

  public static boolean isEmbeddedAttribut(Object attrValue) {
    return isLocalizedStringValue(attrValue);
  }

  public static boolean isLocalizedStringValue(Object attrValue) {
    return LocalizedString.class.isAssignableFrom(attrValue.getClass());
  }

  public static boolean isStringType(String type) {
    type = StringUtils.lowerCase(type);
    return type.equals(ConditionTypes.STRING);
  }

  public static boolean isDecimalNumber(String type) {
    type = StringUtils.lowerCase(type);
    return type.equals(ConditionTypes.LONG)
        || type.equals(ConditionTypes.INTEGER)
        || type.equals(ConditionTypes.BYTE)
        || type.equals(ConditionTypes.SHORT);
  }

  public static boolean isFloatingNumber(String type) {
    type = StringUtils.lowerCase(type);
    return type.equals(ConditionTypes.DOUBLE) || type.equals(ConditionTypes.FLOAT);
  }
}
