package com.ebs.dac.foundation.exceptions.operational.locking;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ResourceAlreadyLockedBySameUserInSameSessionException extends Exception implements IExceptionResponse {

  private static final long serialVersionUID = 1L;


  public ResourceAlreadyLockedBySameUserInSameSessionException() {
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_AlreadyLocked_BySameUser_InSameSession;
  }
}
