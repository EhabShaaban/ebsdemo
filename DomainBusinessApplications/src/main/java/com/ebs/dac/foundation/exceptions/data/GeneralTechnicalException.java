package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.CustomMessageTextBundle;
import com.ebs.dac.foundation.exceptions.IEnterpriseExceptionsMessagesResource;
import com.ebs.dac.foundation.exceptions.TechnicalException;
import java.util.Locale;
import java.util.ResourceBundle;

public class GeneralTechnicalException extends TechnicalException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String GENERAL_TECHNICAL_EXCEPTION_MESSAGE_CODE =
      "GENERAL_TECHNICAL_EXCEPTION_MESSAGE";

  public GeneralTechnicalException() {
    constructMessage();
  }

  public GeneralTechnicalException(Exception cause) {
    super(cause);
    constructMessage();
  }

  @Override
  protected String getMessageResourceBundleName() {
    return IEnterpriseExceptionsMessagesResource.BASE_NAME;
  }

  @Override
  protected void constructMessage() {
    CustomMessageTextBundle message =
        new CustomMessageTextBundle(GENERAL_TECHNICAL_EXCEPTION_MESSAGE_CODE, null);
    getTechnicalMessage().setMessage(message);
  }

  @Override
  public String getLocalizedMessage(Locale locale) {
    if (getTechnicalMessage().getMessage() == null) {
      return null;
    }

    ResourceBundle messageResource = getMessageResource(locale);
    return getTechnicalMessage().getMessage().getText(messageResource);
  }

  @Override
  public String getMessage() {

    if (getTechnicalMessage().getMessage() == null) {
      return null;
    }

    Locale defaultLocale = Locale.ENGLISH;

    ResourceBundle messageResource = getMessageResource(defaultLocale);
    return getTechnicalMessage().getMessage().getText(messageResource);
  }
}
