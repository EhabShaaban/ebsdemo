package com.ebs.dac.foundation.exceptions.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;

public class ResourceAlreadyLockedException extends ConcurrentDataAccessException {

  private static final long serialVersionUID = 1L;

  LockDetails currentLock;

  public ResourceAlreadyLockedException(LockDetails currentLock) {
    this(
        currentLock,
        "The resource "
            + currentLock.getResourceName()
            + " with ID "
            + currentLock.getResourceCode()
            + " rooted from resource "
            + currentLock.getResourceRoot()
            + " is already locked.");
  }

  public ResourceAlreadyLockedException(LockDetails currentLock, String message) {
    super(message);
    if (currentLock == null) {
      throw new IllegalArgumentException("The current lock has to be supplied");
    }
    this.currentLock = currentLock;
  }

  public ResourceAlreadyLockedException(String message) {
    super(message);
  }

  public LockDetails getCurrentLock() {
    return currentLock;
  }
}
