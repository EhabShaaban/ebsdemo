package com.ebs.dac.foundation.exceptions.concurrency;

public class ConcurrentDataAccessException extends Exception {

  private static final long serialVersionUID = 1L;

  public ConcurrentDataAccessException(String message) {
    super(message);
  }

  public ConcurrentDataAccessException(String message, Exception cause) {
    super(message, cause);
  }
}
