package com.ebs.dac.foundation.realization.processing;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.foundation.apis.processing.IDataObjectAttributeProcessor;
import java.util.List;

public class CompositeAttributeProcessor implements IDataObjectAttributeProcessor {

  @Override
  public Object getAttributeValue(IEntityAttribute attribute, Object sourceObj) throws Exception {

    if ((sourceObj instanceof IAggregateAwareEntity) == false)
      throw new DevRulesViolation(
          new IllegalArgumentException("Invalid Source Object: It should has a payload"));

    if ((attribute instanceof ICompositeAttribute) == false)
      throw new DevRulesViolation(new IllegalArgumentException("Invalid Composite Attribute"));

    IAggregateBusinessObject compositeObj =
        ((IAggregateAwareEntity) sourceObj).aggregateBusinessObject();
    ICompositeAttribute<InformationObject> compositeAttr =
        (ICompositeAttribute<InformationObject>) attribute;

    Object attrValue = null;

    if (compositeAttr.isMultiple()) {
      attrValue = compositeObj.<InformationObject>getMultipleInstancesAttribute(compositeAttr);
    } else {
      attrValue = compositeObj.<InformationObject>getSingleInstanceAttribute(compositeAttr);
    }

    return attrValue;
  }

  @Override
  public Object setAttributeValue(IEntityAttribute attribute, Object attrValue, Object targetObj)
      throws Exception {

    if ((targetObj instanceof IAggregateAwareEntity) == false)
      throw new DevRulesViolation("Invalid Target Object: It should has a payload");

    if ((attribute instanceof ICompositeAttribute) == false)
      throw new DevRulesViolation(new IllegalArgumentException("Invalid Composite Attribute"));

    IAggregateBusinessObject compositeObj =
        ((IAggregateAwareEntity) targetObj).aggregateBusinessObject();
    ICompositeAttribute<InformationObject> compositeAttr =
        (ICompositeAttribute<InformationObject>) attribute;

    if (attrValue != null) {
      if (compositeAttr.isMultiple())
        compositeObj.<InformationObject>putMultipleInstancesAttribute(
            compositeAttr, (List<InformationObject>) attrValue);
      else
        compositeObj.<InformationObject>putSingleInstanceAttribute(
            compositeAttr, (InformationObject) attrValue);
    }

    return targetObj;
  }
}
