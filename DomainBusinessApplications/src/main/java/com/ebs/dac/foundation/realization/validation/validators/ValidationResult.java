package com.ebs.dac.foundation.realization.validation.validators;

import com.ebs.dac.dbo.types.LocalizedString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResult {

  private Map<String, String> errorsValidationResult;
  private Map<String, String> warningsValidationResult;
  private Map<String, List> errorValues;

  public ValidationResult() {
    this.errorsValidationResult = new HashMap<>();
    this.warningsValidationResult = new HashMap<>();
    this.errorValues = new HashMap<>();
  }

  public void bindError(String attrName, String errorMsg) {
    this.errorsValidationResult.put(attrName, errorMsg);
  }

  public void bindErrorWithValues(String attrName, String errorMsg, List<String> errorValues) {
    this.errorsValidationResult.put(attrName, errorMsg);
    this.errorValues.put(attrName, errorValues);
  }

  public void appendBindErrorWithValues(String attrName, String errorMsg, List<String> errorValues) {
    this.errorsValidationResult.put(attrName, errorMsg);
    this.errorValues.computeIfAbsent(attrName, k -> new ArrayList<>()).addAll(errorValues);
  }

  public void bindWarning(String attrName, String warningMsg) {
    this.warningsValidationResult.put(attrName, warningMsg);
  }

  public boolean hasError(String attrName) {
    return this.errorsValidationResult.get(attrName) != null;
  }

  public boolean hasWarning(String attrName) {
    return this.warningsValidationResult.get(attrName) != null;
  }

  public boolean isValid() {
    return errorsValidationResult.isEmpty();
  }

  public String getErrorCode(String attrName) {
    return this.errorsValidationResult.getOrDefault(attrName, null);
  }

  public List<String> getErrorValues(String attrName) {
    return this.errorValues.getOrDefault(attrName, null);
  }
  public String getFistErrorCode (){
    Map.Entry<String,String> entry = errorsValidationResult.entrySet().iterator().next();
    return entry.getValue();
  }

  public void bindErrorWithLocalizedValues(String attrName, String errorMsg, List<LocalizedString> errorValues) {
    this.errorsValidationResult.put(attrName, errorMsg);
    this.errorValues.put(attrName, errorValues);
  }
}
