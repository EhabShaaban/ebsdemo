package com.ebs.dac.foundation.exceptions.data;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

import java.util.Map;

public class MissingFieldsException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;
  private Map<String, Object> missingFields;

  public MissingFieldsException(Map<String, Object> missingFields) {
    this.missingFields = missingFields;
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_MISSING_MANDATORIES;
  }

  @Override
  public int getHttpStatusCode() {
    return 200;
  }

  public Map<String, Object> getMissingFields() {
    return missingFields;
  }
}
