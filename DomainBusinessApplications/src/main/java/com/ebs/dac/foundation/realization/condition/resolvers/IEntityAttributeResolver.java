package com.ebs.dac.foundation.realization.condition.resolvers;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 25, 2018 4:22:45 PM
 */
interface IEntityAttributeResolver {
  Object resolveAttributeValue(Object attrValue) throws Exception;
}
