package com.ebs.dac.foundation.realization.concurrency;

public interface ConcurrentDataAccessManagerMBean {
  void cleanUp(boolean all);
}
