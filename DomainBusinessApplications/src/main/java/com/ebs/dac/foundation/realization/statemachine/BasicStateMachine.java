package com.ebs.dac.foundation.realization.statemachine;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import java.net.URL;

public class BasicStateMachine<T extends IStatefullBusinessObject>
    extends PrimitiveStateMachine<T> {

  /* States of State Machine */
  public static final String DRAFT_STATE = "Draft";

  /* Public External Actions */
  public static final String ACTIVATE_ACTION = "Activate";
  public static final String DEACTIVATE_ACTION = "Deactivate";

  /* Continue States of State Machine */
  public static final String INACTIVE_STATE = "Inactive";

  public BasicStateMachine() throws Exception {
    super();
  }

  public BasicStateMachine(URL url) throws Exception {
    super(url);
  }

  public BasicStateMachine(String scxmlFile) throws Exception {
    super(scxmlFile);
  }

  public BasicStateMachine(T obj, URL url) throws Exception {
    super(obj, url);
  }

  public BasicStateMachine(T obj) throws Exception {
    super(obj);
  }

  public boolean Draft() {
    return true;
  }

  public boolean isInactive() {
    return getObject().getCurrentStates().contains(INACTIVE_STATE);
  }

  public boolean isActive() {
    return getObject().getCurrentStates().contains(ACTIVE_STATE);
  }

  public boolean Inactive() {
    return true;
  }

  public boolean isDraft() {
    boolean draft = false;
    if (getObject().getCurrentStates().contains(DRAFT_STATE)) {
      draft = true;
    }
    return draft;
  }
}
