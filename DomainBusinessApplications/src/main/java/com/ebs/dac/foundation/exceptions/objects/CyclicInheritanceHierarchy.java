package com.ebs.dac.foundation.exceptions.objects;

public class CyclicInheritanceHierarchy extends Exception {

  private static final String message = "[%1$s] has cyclic inheritance hierarchy";
  private Class<?> clzz;

  public CyclicInheritanceHierarchy(Class<?> clzz) {
    this.clzz = clzz;
  }

  @Override
  public String getMessage() {
    return String.format(message, clzz.getName());
  }
}
