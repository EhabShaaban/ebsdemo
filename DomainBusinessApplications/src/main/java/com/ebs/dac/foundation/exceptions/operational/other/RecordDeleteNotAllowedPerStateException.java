package com.ebs.dac.foundation.exceptions.operational.other;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class RecordDeleteNotAllowedPerStateException extends Exception
    implements IExceptionResponse {

  @Override
  public String getResponseCode() {
    // TODO Auto-generated method stub
    return IExceptionsCodes.General_RecordDeleteNotAllowedPerState;
  }
}
