package com.ebs.dac.foundation.exceptions.operational.other;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ActionNotAllowedPerStateException extends RuntimeException implements IExceptionResponse {

  private Class<?> entityType;
  private Long instanceID;
  private String currentState;
  private String action;

  public ActionNotAllowedPerStateException(Class<?> entityType, Long instanceID, String currentState, String action) {
    this.entityType = entityType;
    this.instanceID = instanceID;
    this.currentState = currentState;
    this.action = action;
  }

  public ActionNotAllowedPerStateException() {}

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE;
  }
}
