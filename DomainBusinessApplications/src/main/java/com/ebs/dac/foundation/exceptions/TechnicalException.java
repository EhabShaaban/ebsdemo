package com.ebs.dac.foundation.exceptions;

public abstract class TechnicalException extends CustomException {
  /** */
  private static final long serialVersionUID = 1L;

  public TechnicalException() {
    super(new TechnicalExceptionMessage());
  }

  public TechnicalException(Exception cause) {
    super(cause);
    super.setCustomMessage(new TechnicalExceptionMessage());
  }

  protected abstract void constructMessage();

  public TechnicalExceptionMessage getTechnicalMessage() {
    return ((TechnicalExceptionMessage) super.getCustomMessage());
  }
}
