package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import com.ebs.dac.foundation.exceptions.conditionmanager.TypeMisMatchException;
import com.ebs.dac.foundation.realization.condition.utils.AttributeTypeDetectorUtil;
import java.util.Map;

public class Predicate implements IPredicate, IOperand {
  private Operand leftOperand;
  private Operand rightOperand;
  private String operator;

  @Override
  public Operand getRightOperand() {
    return this.rightOperand;
  }

  @Override
  public void setRightOperand(IOperand operand) {
    this.rightOperand = (Operand) operand;
  }

  @Override
  public Operand getLeftOperand() {
    return this.leftOperand;
  }

  @Override
  public void setLeftOperand(IOperand operand) {
    this.leftOperand = (Operand) operand;
  }

  @Override
  public String getOperator() {
    return this.operator;
  }

  @Override
  public void setOperator(String s) {
    this.operator = s;
  }

  @Override
  public PredicateResult execute(Map<String, Object> map) throws Exception {
    PredicateExecutor predicateExecutor = new PredicateExecutor();
    resolveLeftOperand(map);
    return predicateExecutor.execute(getLeftOperand(), getRightOperand(), getOperator());
  }

  private void resolveLeftOperand(Map<String, Object> map) throws Exception {

    if (!map.containsKey(leftOperand.getValue())) {
      throw new IllegalArgumentException("cannot find the corresponding key");
    }

    if (!isOperandsTypesCompatable(map)) {
      throw new TypeMisMatchException("incompatible types!");
    }
    leftOperand.setValue(map.get(leftOperand.getValue()));
  }

  private boolean isOperandsTypesCompatable(Map map) {
    String leftOperandValueType = map.get(leftOperand.getValue()).getClass().getSimpleName();
    String rightOperandValueType = rightOperand.getType();

    if (AttributeTypeDetectorUtil.isFloatingNumber(leftOperandValueType)
        && AttributeTypeDetectorUtil.isFloatingNumber(rightOperandValueType)) {
      return true;
    }
    if (AttributeTypeDetectorUtil.isDecimalNumber(leftOperandValueType)
        && AttributeTypeDetectorUtil.isDecimalNumber(rightOperandValueType)) {
      return true;
    }
    return AttributeTypeDetectorUtil.isStringType(leftOperandValueType)
        && AttributeTypeDetectorUtil.isStringType(rightOperandValueType);
  }
}
