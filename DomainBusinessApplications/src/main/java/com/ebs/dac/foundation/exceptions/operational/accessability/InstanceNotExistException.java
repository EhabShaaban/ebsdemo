package com.ebs.dac.foundation.exceptions.operational.accessability;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class InstanceNotExistException extends RuntimeException implements IExceptionResponse {

  private Class<?> entityType;
  private Long instanceID;
  private String entityClassName;

  public InstanceNotExistException(Class<?> entityType, Long instanceID) {
    this.entityType = entityType;
    this.instanceID = instanceID;
  }

  public InstanceNotExistException() {
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_UC_Failure_InstanceNotExist;
  }
}
