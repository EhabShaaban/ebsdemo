package com.ebs.dac.foundation.apis.concurrency;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import org.joda.time.DateTime;

/**
 * TODO Maybe enhance this to handle all {@link LockDetails} issues, from lock creation to
 * determining if it got expired or not.<br>
 * <br>
 * This is intended so that the logic of determining if a token is valid or not (through date
 * checking, for now) is moved from {@link IConcurrentDataAccessManager#cleanUp(false)} to this
 * strategy object
 *
 * @author Hesham Saleh
 */
public interface IEditTokenValidityPolicy {

  DateTime calculateTokenExpiryDate(DateTime creationDate);

  DateTime extendTokenExpiryDate(DateTime creationDate, DateTime expiryDate);
}
