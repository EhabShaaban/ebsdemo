package com.ebs.dac.foundation.exceptions.data;

public interface HasData {

  ExceptionMainData getData();
}
