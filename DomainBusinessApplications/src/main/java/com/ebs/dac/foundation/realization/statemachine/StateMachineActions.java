package com.ebs.dac.foundation.realization.statemachine;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import java.util.Set;

/**
 * @author Menna Sayed
 * @date May 30, 2017
 */
public class StateMachineActions<T extends IStatefullBusinessObject> {

  private AbstractStateMachine stateMachine;
  private T statefullObject;

  public StateMachineActions(T obj) throws Exception {
    if (obj == null) {
      throw new IllegalArgumentException("The object must not be null");
    }
    stateMachine = new BasicStateMachine<>(obj);
    statefullObject = obj;
  }

  public StateMachineActions(BasicStateMachine<T> stateMachine) {
    if (stateMachine == null) {
      throw new IllegalArgumentException("The basicStateMachine must not be null");
    }
    this.stateMachine = stateMachine;
    this.statefullObject = stateMachine.getObject();
  }

  public StateMachineActions(T entity, AbstractStateMachine stateMachine) {
    if (stateMachine == null) {
      throw new IllegalArgumentException("The StateMachine must not be null");
    }
    if (entity == null) {
      throw new IllegalArgumentException("The object must not be null");
    }
    this.stateMachine = stateMachine;
    statefullObject = entity;
  }

  public void isAllowedAction(String actionName) throws Exception {
    Set<String> currentStates = statefullObject.getCurrentStates();
    if (currentStates == null) {
      throw new IllegalArgumentException("Object current states must not be null");
    }
    if (actionName == null || actionName.trim().isEmpty()) {
      throw new IllegalArgumentException("Action name must not be null or empty");
    }
    boolean canFireAction = false;
    for (String state : currentStates) {
      canFireAction = stateMachine.canFireEvent(actionName);
      if (canFireAction) {
        break;
      }
    }
    if (!canFireAction) {
      throw new ActionNotAllowedPerStateException(
          statefullObject.getClass(),
          ((StatefullBusinessObject) statefullObject).getId(),
          statefullObject.getCurrentState(),
          actionName);
    }
  }

  public Set<String> getAllowedActions() {

    return stateMachine.getAllowedActions();
  }

  public AbstractStateMachine getStateMachine() {
    return stateMachine;
  }

  public <StateMachine extends BasicStateMachine<T>> void overrideBasicStateMachine(
      StateMachine machine) {
    if (machine == null) {
      throw new IllegalArgumentException("the StateMachine must not be null");
    }
    this.stateMachine = machine;
  }
}
