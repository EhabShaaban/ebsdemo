package com.ebs.dac.foundation.realization.validation.validators;

import com.ebs.dac.foundation.realization.validation.validationrules.IValidationAttributeRule;
import com.ebs.dac.foundation.realization.validation.validationrules.IValidationEntityRule;
import com.ebs.dac.foundation.realization.validation.validationrules.IValidationRule;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ValidationManager {

  protected ValidationResult validationResult;
  private Class<?> entityType;
  private List<ValidationRuleConfig> rulesList;

  public ValidationManager(Class<?> entityType) {
    validationResult = new ValidationResult();
    rulesList = new ArrayList<>();
    this.entityType = entityType;
  }

  public void addValidationRule(String attrName, IValidationRule rule) {
    ValidationRuleConfig validationRuleConfig = new ValidationRuleConfig();
    validationRuleConfig.setAttributeName(attrName);
    validationRuleConfig.setValidationRule(rule);
    rulesList.add(validationRuleConfig);
  }

  public ValidationResult executeValidationRules(Object creationValueObject) throws Exception {
    for (ValidationRuleConfig config : rulesList) {
      String attributeName = config.getAttributeName();
      boolean attributeHasError = validationResult.hasError(attributeName);
      boolean attributeHasWarning = validationResult.hasWarning(attributeName);

      if (!attributeHasError && !attributeHasWarning) {
        Object attributeValue = getAttributeValue(attributeName, creationValueObject);
        IValidationRule rule = config.getValidationRule();
        if (rule instanceof IValidationAttributeRule) {
          ((IValidationAttributeRule) rule)
              .validate(attributeName, attributeValue, validationResult);
        } else if (rule instanceof IValidationEntityRule) {
          ((IValidationEntityRule) rule)
              .validate(attributeName, creationValueObject, validationResult);
        }
      }
    }

    return validationResult;
  }

  public Object getAttributeValue(String attributeName, Object valueObject) throws Exception {

    String getterName = "get";
    Object attributeValue = null;
    // Capitalize first character of (F)ieldName
    getterName += attributeName.substring(0, 1).toUpperCase();
    getterName += attributeName.substring(1);

    for (Method method : entityType.getMethods()) {
      method.setAccessible(true);
      if (method.getName().equals(getterName)) {
        // Default field getter method has no arguments and return the value of field.
        attributeValue = method.invoke(valueObject);
      }
    }
    return attributeValue;
  }
}
