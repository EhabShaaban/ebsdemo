package com.ebs.dac.foundation.apis.security;

/**
 * The user to be injected in controllers in case it is used.<br>
 * <br>
 * This interface does not strictly represent the currently executing server, it can be used as a
 * Data Transfer Object for user related information
 */
public interface IBDKUser {

  String getUsername();

  String getCompany();

  Long getUserId();

  Boolean getSysPasswordFlag();

  boolean equals(IBDKUser anotherUser);
}
