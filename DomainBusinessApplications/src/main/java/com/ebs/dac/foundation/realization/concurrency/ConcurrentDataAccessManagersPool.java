package com.ebs.dac.foundation.realization.concurrency;

import com.ebs.dac.foundation.apis.cache.ICacheFactory;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.concurrency.IEditTokenValidityPolicy;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.concurrency.InvalidResourceEditTokenException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyUnlockedException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is intended to be used singleton-ed, the client is responsible for keeping a single
 * instance used
 *
 * @author Hesham Saleh
 */
public class ConcurrentDataAccessManagersPool implements IConcurrentDataAccessManagersObjectPool {

  private static final Logger logger =
      LoggerFactory.getLogger(ConcurrentDataAccessManagersPool.class);

  private static final int CLEAN_UP_INTERVAL_IN_SECONDS_DEFAULT = 60;
  private int cleanupInterval = CLEAN_UP_INTERVAL_IN_SECONDS_DEFAULT;

  private Map<String, ConcurrentDataAccessManager> managers;

  private IEditTokenValidityPolicy editTokenValidityPolicy;

  private String cacheServerUrl;
  private ICacheFactory cacheFactory;
  private IUserAccountSecurityService userAccountSecurityService;

  public ConcurrentDataAccessManagersPool(
      IEditTokenValidityPolicy editTokenValidityPolicy, ICacheFactory cacheFactory) {
    this(editTokenValidityPolicy, cacheFactory, true);
  }

  public ConcurrentDataAccessManagersPool(
      IEditTokenValidityPolicy editTokenValidityPolicy,
      ICacheFactory cacheFactory,
      boolean fireCleanupThread) {
    managers = new HashMap<String, ConcurrentDataAccessManager>();
    this.editTokenValidityPolicy = editTokenValidityPolicy;
    this.cacheFactory = cacheFactory;
    if (fireCleanupThread) {
      fireCleanupThread();
    }
  }

  public ConcurrentDataAccessManagersPool userAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
    return this;
  }

  private void fireCleanupThread() {
    Thread cleanupThread =
        new Thread(
            new Runnable() {

              public void run() {
                while (true) {
                  try {
                    Thread.sleep(cleanupInterval * 1000);
                    cleanUp(false); // clean the expired locks
                  } catch (InterruptedException e) {
                    logger.error(e.getMessage(), e);
                  }
                }
              }
            });
    cleanupThread.start();
  }

  public void evictAll() {
    synchronized (managers) {
      for (IConcurrentDataAccessManager manager : managers.values()) {
        List<LockDetails> locks = manager.listAllLocks();
        for (LockDetails lock : locks) {
          try {
            manager.unlock(lock.getTokenId());
          } catch (ResourceAlreadyUnlockedException e) {
            // Great, though unexpected
          } catch (InvalidResourceEditTokenException e) {
            logger.error(
                "The supplied lockTokenId is invalid eventhough its original source was from the manager "
                    + manager
                    + " it self",
                e);
          }
        }
      }
    }
  }

  /**
   * The need here is to evict the supplied token IDs if they are available
   *
   * <p>TODO This can greatly be enhanced by being able to extract the resourceName and the
   * resourceId from the tokenId representation, instead of traversing the whole list of locks
   *
   * @param tokenIds
   */
  public void evictIfPossible(List<String> tokenIds) {
    synchronized (managers) {
      for (IConcurrentDataAccessManager manager : managers.values()) {
        List<LockDetails> locks = manager.listAllLocks();
        for (LockDetails lock : locks) {
          if (tokenIds.contains(lock.getTokenId())) {
            try {
              manager.unlock(lock.getTokenId());
            } catch (ResourceAlreadyUnlockedException e) {
              // Great, though unexpected
            } catch (InvalidResourceEditTokenException e) {
              logger.error(
                  "The supplied lockTokenId is invalid eventhough its original source was from the manager "
                      + manager
                      + " it self",
                  e);
            }
          }
        }
      }
    }
  }

  public Map<String, List<LockDetails>> listAll() {
    Map<String, List<LockDetails>> all = new HashMap<String, List<LockDetails>>();
    for (Entry<String, ConcurrentDataAccessManager> entry : managers.entrySet()) {
      List<LockDetails> locks = entry.getValue().listAllLocks();
      if (!locks.isEmpty()) {
        all.put(entry.getValue().getResourceRoot(), locks);
      }
    }

    return all;
  }

  /* (non-Javadoc)
   * @see IConcurrentDataAccessManagersObjectPool#cleanUp()
   */
  public void cleanUp(boolean all) {
    synchronized (managers) {
      for (IConcurrentDataAccessManager manager : managers.values()) {
        manager.cleanUp(all);
      }
    }
  }

  /* (non-Javadoc)
   * @see IConcurrentDataAccessManagersObjectPool#getConcurrentDataAccessManager(java.lang.String)
   */
  public IConcurrentDataAccessManager getConcurrentDataAccessManager(String resourceName) {
    logger.debug("Getting ConcurrentDataAccessManager for resource " + resourceName);
    ConcurrentDataAccessManager manager;
    // synchronized (resourceName) { // Block only if two threads invoke this method with the exact
    // same resourceName object.
    synchronized (managers) {
      manager = managers.get(resourceName);
      if (manager == null) {
        logger.debug("First time requested, initializing new one.");
        // TODO Verify that this resource name is indeed one of the allowed resource names (i.e. in
        // non generic terms, the business catalog name should actually exist and be allowed)
        manager =
            new ConcurrentDataAccessManager(resourceName, editTokenValidityPolicy, cacheFactory);
        managers.put(resourceName, manager);
      }
    }
    logger.debug(
        "Returning ConcurrentDataAccessManager "
            + manager.toString()
            + " for resource "
            + resourceName);
    return manager;
  }

  /* (non-Javadoc)
   * @see IConcurrentDataAccessManagersObjectPool#getAvailableConcurrentDataAccessManager()
   */
  public Collection<IConcurrentDataAccessManager> getAvailableConcurrentDataAccessManager() {
    return Collections.unmodifiableCollection(managers.values());
  }

  /* (non-Javadoc)
   * @see IConcurrentDataAccessManagersObjectPool#getCleanupInterval()
   */
  public int getCleanupInterval() {
    return cleanupInterval;
  }

  /* (non-Javadoc)
   * @see IConcurrentDataAccessManagersObjectPool#setCleanupInterval(int)
   */
  public void setCleanupInterval(int cleanupInterval) {
    this.cleanupInterval = cleanupInterval;
  }

  public void releaseAllLocksByCurrentUser() throws Exception {
    IBDKUser loggedInUser = userAccountSecurityService.getLoggedInUser();
    Collection<ConcurrentDataAccessManager> accessManagers = managers.values();
    for (ConcurrentDataAccessManager concurrentDataAccessManager : accessManagers) {
      concurrentDataAccessManager.unlockAllLockedResourcesByUser(loggedInUser);
    }
  }
}
