package com.ebs.dac.foundation.apis.exchangerates;

public interface ICurrencyISO {
  static final String EGP = "EGP";
  static final String USD = "USD";
}
