package com.ebs.dac.foundation.exceptions.operational.other;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class EditNotAllowedPerStateException extends Exception implements IExceptionResponse {

  public EditNotAllowedPerStateException() {
  }

  public EditNotAllowedPerStateException(Exception exception) {
    this();
    initCause(exception);
  }

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.General_EditNotAllowedPerState;
  }
}
