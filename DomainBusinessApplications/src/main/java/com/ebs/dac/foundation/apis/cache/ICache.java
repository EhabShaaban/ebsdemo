package com.ebs.dac.foundation.apis.cache;

import java.util.Collection;

/** Created by hbayomy on 10/6/16. */
public interface ICache<K, V> {

  V get(K value);

  void put(K key, V value);

  void put(K key, V value, boolean b);

  int remove(K key);

  int remove(K key, boolean b);

  Collection<V> values();
}
