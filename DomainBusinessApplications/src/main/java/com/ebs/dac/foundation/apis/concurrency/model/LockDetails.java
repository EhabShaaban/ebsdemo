package com.ebs.dac.foundation.apis.concurrency.model;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import java.io.Serializable;
import org.joda.time.DateTime;

public class LockDetails implements Serializable {

  private static final long serialVersionUID = 1L;

  private String resourceRoot;
  private String resourceName;
  private String resourceCode;

  private String tokenId;
  private DateTime creationDate;
  private DateTime expiryDate;
  private Long age;

  private transient IBDKUser identity;

  public LockDetails(
      String resourceRoot,
      String resourceName,
      String resourceCode,
      String tokenId,
      DateTime creationDate,
      DateTime expiryDate,
      IBDKUser identity,
      Long age) {
    super();
    this.resourceRoot = resourceRoot;
    this.resourceName = resourceName;
    this.resourceCode = resourceCode;
    this.tokenId = tokenId;
    this.creationDate = creationDate;
    this.expiryDate = expiryDate;
    this.identity = identity;
    this.age = age;
  }

  public LockDetails(LockDetails lockDetails) {
    /*All fields used currently are already immutable,
    so no problems doing this as they don't need copy constructors*/
    this(
        lockDetails.getResourceRoot(),
        lockDetails.getResourceName(),
        lockDetails.getResourceCode(),
        lockDetails.getTokenId(),
        lockDetails.getCreationDate(),
        lockDetails.getExpiryDate(),
        lockDetails.getIdentity(),
        lockDetails.getAge());
  }

  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  public String getResourceCode() {
    return resourceCode;
  }

  public void setResourceCode(String resourceCode) {
    this.resourceCode = resourceCode;
  }

  public String getTokenId() {
    return tokenId;
  }

  public void setTokenId(String tokenId) {
    this.tokenId = tokenId;
  }

  public DateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(DateTime creationDate) {
    this.creationDate = creationDate;
  }

  public DateTime getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(DateTime expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Long getAge() {
    return age;
  }

  public void setAge(Long age) {
    this.age = age;
  }

  public IBDKUser getIdentity() {
    return identity;
  }

  public void setIdentity(IBDKUser identity) {
    this.identity = identity;
  }

  public String getResourceRoot() {
    return resourceRoot;
  }

  public void setResourceRoot(String resourceRoot) {
    this.resourceRoot = resourceRoot;
  }

  @Override
  public String toString() {
    return "LockDetails [resourceRoot="
        + resourceRoot
        + ", resourceName="
        + resourceName
        + ", resourceCode="
        + resourceCode
        + ", tokenId="
        + tokenId
        + ", creationDate="
        + creationDate
        + ", expiryDate="
        + expiryDate
        + ", age="
        + age
        + ", identity="
        + identity
        + "]";
  }
}
