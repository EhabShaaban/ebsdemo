package com.ebs.dac.foundation.apis.cache;

/** Created by hbayomy on 10/6/16. */
public interface ICacheFactory {

  <K, V> ICache<K, V> createConcurrentAccessManagerCache(String resourceRoot);
}
