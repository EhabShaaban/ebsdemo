package com.ebs.dac.aop.logging;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import lombok.SneakyThrows;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class LoggerAspect {

    @Autowired
    private IUserAccountSecurityService userAccountSecurityService;
    private static final String POINTCUT = "within(com.ebs.dda.rest..*Controller)";
    private static final Logger log = LoggerFactory.getLogger(LoggerAspect.class);
    private String loggedInUser;

    @Around(POINTCUT)
    @SneakyThrows
    public Object logArroundExec(ProceedingJoinPoint pjp) {
        this.loggedInUser = userAccountSecurityService.getLoggedInUser().toString();
        log.info("before {}", constructLogMsg(pjp));
        return pjp.proceed();
    }

    @AfterReturning(pointcut = POINTCUT, returning = "returnValue")
    public void logAfterExec(JoinPoint jp, Object returnValue) {
        log.info("after {} {}", constructLogMsg(jp), returnValue);
    }

    private String constructLogMsg(JoinPoint jp) {
        String args = Arrays.asList(jp.getArgs()).stream().map(String::valueOf).collect(Collectors.joining(",", "[", "]"));
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        StringBuilder sb = new StringBuilder("@");
        sb.append(method.getName());
        sb.append(":");
        sb.append(args);
        sb.append(" by: ");
        sb.append(loggedInUser);
        return sb.toString();
    }
}