package com.ebs.dac.common.utils.json.adapters;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

public class JodaTimeJsonAdapter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

  static final org.joda.time.format.DateTimeFormatter CLIENT_DATE_TIME_FORMATTER =
      DateTimeFormat.forPattern(DateFormats.CLIENT_DATE_TIME_WITH_TIMEZONE);

  static final org.joda.time.format.DateTimeFormatter DATE_TIME_FORMATTER =
          DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);

  @Override
  public DateTime deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
      throws JsonParseException {
    String jsonElementAsString = arg0.getAsString() + " Africa/Cairo";
    return jsonElementAsString.length() == 0
        ? null
        : CLIENT_DATE_TIME_FORMATTER.parseDateTime(jsonElementAsString).withZone(DateTimeZone.UTC);
  }

  @Override
  public JsonElement serialize(DateTime arg0, Type arg1, JsonSerializationContext arg2) {
    String jsonElementAsString = StringUtils.EMPTY;
    if (arg0 != null) {
      jsonElementAsString = DATE_TIME_FORMATTER.print(arg0);
    }
    return new JsonPrimitive(jsonElementAsString);
  }
}
