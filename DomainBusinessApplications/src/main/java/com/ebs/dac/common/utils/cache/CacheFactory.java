package com.ebs.dac.common.utils.cache;

import com.ebs.dac.foundation.apis.cache.ICache;
import com.ebs.dac.foundation.apis.cache.ICacheFactory;

/** Created by marisoft on 10/11/16. */
public class CacheFactory implements ICacheFactory {

  private static final CacheFactory instance = new CacheFactory();

  protected CacheFactory() {}

  public static synchronized CacheFactory getInstance() {
    return instance;
  }

  @Override
  public synchronized <K, V> ICache<K, V> createConcurrentAccessManagerCache(String resourceRoot) {
    return new CacheMap<K, V>(resourceRoot);
  }
}
