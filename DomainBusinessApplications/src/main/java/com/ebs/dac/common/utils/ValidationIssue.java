package com.ebs.dac.common.utils;

public class ValidationIssue {

  String message;
  String attributeName;

  public ValidationIssue(String message) {
    this(message, null);
  }

  public ValidationIssue(String message, String attributeName) {
    this.message = message;
    this.attributeName = attributeName;
  }

  public String getAffectedAttributeName() {
    return attributeName;
  }

  public void setAffectedAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }
}
