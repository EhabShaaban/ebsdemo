package com.ebs.dac.common.utils.json.adapters;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public class BigDecimalAdapter implements JsonSerializer<BigDecimal> {
  @Override
  public JsonElement serialize(
      BigDecimal bigDecimal, Type type, JsonSerializationContext jsonSerializationContext) {
    if (bigDecimal != null) {
      BigDecimal newValue = isInteger(bigDecimal) ? bigDecimal.setScale(1) : bigDecimal;
      return new Gson().toJsonTree(newValue.toPlainString());
    }
    return new JsonPrimitive("");
  }

  private boolean isInteger(BigDecimal bigDecimal) {
    return bigDecimal.stripTrailingZeros().scale() <= 0;
  }
}
