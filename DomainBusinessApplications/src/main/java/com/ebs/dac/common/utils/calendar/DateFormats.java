package com.ebs.dac.common.utils.calendar;

public class DateFormats {
  public static final String DATE_TIME_WITH_TIMEZONE = "yyyy-MM-dd HH:mm:ss Z";
  public static final String CLIENT_DATE_TIME_WITH_TIMEZONE = "dd-MMM-yyyy hh:mm a ZZZ";
}
