package com.ebs.dac.common.utils.exceptions;

import com.ebs.dac.common.utils.ValidationResults;

public class BlockerValidationResultsException extends Exception {

  private static final long serialVersionUID = 1L;

  private ValidationResults validationResults;

  public BlockerValidationResultsException(ValidationResults validationResults) {
    super();
    this.validationResults = validationResults;
  }

  public ValidationResults getValidationResults() {
    return validationResults;
  }
}
