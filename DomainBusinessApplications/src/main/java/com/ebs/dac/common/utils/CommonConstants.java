package com.ebs.dac.common.utils;

public class CommonConstants {

  /** The HTTP Request header that should be supplied by clients */
  public static final String COMPANY_CODE_HTTP_REQUEST_HEADER_ATTRIBUTE = "CCode";

  public static final String COMPANY_CODE_HTTP_REQUEST_HEADER_UNDETERMINED = "UNDETERMINED";

  public static final String COMPANY_CODE_HTTP_REQUEST_HEADER_BDK_COMPANY_CODE = "BDKCompanyCode";

  public static final String ORIGIN_LOCAL = "local"; // better to compare with case insensitive

  public static final String ORIGIN_REMOTE = "remote"; // better to compare with case insensitive
}
