package com.ebs.dac.common.utils.threads;

/**
 * Utility class to manage ThreadLocal's. It is recommended to ONLY use this utility instead of
 * manually managing Thread Local variables.
 *
 * @author Hesham Saleh
 */
public class ThreadLocalUtil {

  private static final ThreadLocal<ThreadVariables> THREAD_VARIABLES =
      new ThreadLocal<ThreadVariables>() {
        @Override
        protected ThreadVariables initialValue() {
          return new ThreadVariables();
        }
      };

  public static Object getThreadVariable(String name) {
    return THREAD_VARIABLES.get().get(name);
  }

  public static Object getThreadVariable(String name, InitialValueCreator initialValue) {
    Object o = THREAD_VARIABLES.get().get(name);
    if (o == null) {
      THREAD_VARIABLES.get().put(name, initialValue.create());
      return getThreadVariable(name);
    } else {
      return o;
    }
  }

  public static void putThreadVariable(String name, Object value) {
    THREAD_VARIABLES.get().put(name, value);
  }

  public static void removeThreadVariable(String name) {
    THREAD_VARIABLES.get().remove(name);
  }

  public static boolean containsThreadVariable(String name) {
    return THREAD_VARIABLES.get().containsKey(name);
  }

  public static void resetCurrentThreadVariables() {
    THREAD_VARIABLES.get().clear();
  }

  public static void destroy() {
    THREAD_VARIABLES.remove();
  }

  /**
   * For debugging purposes only. Should not be used directly by clients, instead use {@link
   * #putThreadVariable(String, Object)} and others
   *
   * @return
   */
  public static ThreadVariables getThreadVariablesInternalMap() {
    return THREAD_VARIABLES.get();
  }
}
