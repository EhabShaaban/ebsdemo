package com.ebs.dac.common.utils;

import java.util.ArrayList;
import java.util.List;

public class ValidationResults {

  List<ValidationIssue> warnings = new ArrayList<ValidationIssue>();
  List<ValidationIssue> errors = new ArrayList<ValidationIssue>();

  public void addWarning(ValidationIssue issue) {
    warnings.add(issue);
  }

  public void addError(ValidationIssue issue) {
    errors.add(issue);
  }

  public List<ValidationIssue> getWarnings() {
    return warnings;
  }

  public List<ValidationIssue> getErrors() {
    return errors;
  }

  public void addWarnings(List<ValidationIssue> issues) {
    for (ValidationIssue issue : issues) {
      warnings.add(issue);
    }
  }

  public void addErrors(List<ValidationIssue> issues) {
    for (ValidationIssue issue : issues) {
      errors.add(issue);
    }
  }

  /**
   * Determines whether these validation results are considered as blocker or not.<br>
   * Children wishing to change the criteria of determination are free to override this method.
   *
   * @return boolean
   */
  public boolean isBlocker() {
    return !errors.isEmpty();
  }

  public boolean isEmpty() {
    return warnings.isEmpty() && errors.isEmpty();
  }

  /**
   * Merges the supplied validation results with the current (so that the supplied one can be
   * ditched)
   */
  public void merge(ValidationResults otherValidationResults) {
    addWarnings(otherValidationResults.getWarnings());
    addErrors(otherValidationResults.getErrors());
  }
}
