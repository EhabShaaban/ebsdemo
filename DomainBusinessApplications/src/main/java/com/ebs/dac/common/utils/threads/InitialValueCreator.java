package com.ebs.dac.common.utils.threads;

public interface InitialValueCreator {

  Object create();
}
