package com.ebs.dac.common.utils.cache;

import com.ebs.dac.foundation.apis.cache.ICache;
import java.util.Collection;

/** Created by marisoft on 10/11/16. */
public class CacheMap<K, V> implements ICache<K, V> {

  private String resourceRoot;

  public CacheMap(String resourceRoot) {
    if (resourceRoot == null) {
      throw new IllegalArgumentException("The resource root must not be null");
    }
    this.resourceRoot = resourceRoot;
  }

  public String getResourceRoot() {
    return this.resourceRoot;
  }

  @Override
  public V get(K value) {
    return null;
  }

  @Override
  public void put(K key, V value) {}

  @Override
  public void put(K key, V value, boolean b) {}

  @Override
  public int remove(K key) {
    return 0;
  }

  @Override
  public int remove(K key, boolean b) {
    return 0;
  }

  @Override
  public Collection<V> values() {
    return null;
  }
}
