package com.ebs.dac.i18n.localizedbundles;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class AttributesResourceBundle {

  private static final String NAMES_BUNDLE_BASE_PATH = "META-INF/AttributesNames/";
  public static final String NAMES_BUNDLE_NAME = NAMES_BUNDLE_BASE_PATH + "names";
  private static final String PREFIX = "label_";

  public static String getLocalizedAttribute(String attributeName, Locale locale) {
    ResourceBundle resourceBundle = ResourceBundle.getBundle(NAMES_BUNDLE_NAME, locale);
    String localizedName = null;
    if (resourceBundle != null) {
      try {
        localizedName = resourceBundle.getString(PREFIX + attributeName);
      } catch (MissingResourceException e3) {
        localizedName = attributeName;
      }
    }
    return localizedName;
  }

  public static Object[] getLocalizedAttributes(Object[] attributesNames, Locale locale) {
    for (int i = 0; i < attributesNames.length; i++) {
      if (attributesNames[i] != null) {
        if (attributesNames[i].getClass().isEnum()) {
          attributesNames[i] =
              EnumsResourceBundle.getLocalizedValue((Enum<?>) attributesNames[i], locale);
        } else {
          attributesNames[i] = getLocalizedAttribute(attributesNames[i].toString(), locale);
        }
      }
    }
    return attributesNames;
  }
}
