package com.ebs.dac.i18n.languageutile;

/**
 * @author Hisham Ramadan
 * @since Oct 8, 2015
 */
public interface ILanguageRanges {
  /*
  * range of arabic chars/symbols is from 0x0600 to 0x06ff
       the arabic letter 'لا' is special case having the range from 0xFE70 to 0xFEFF
    */
  String AR_RANGE_START = "0600";
  String AR_RANGE_END = "06FF";

  String AR_RANGE_START_SPECTAIL_CASE = "FE70";
  String AR_RANGE_END_SPECTAIL_CASE = "FEFF";

  String EN_RANGE_START = "0020";
  String EN_RANGE_END = "007F";
}
