package com.ebs.dac.i18n.locales;

import java.util.Locale;

public interface ISysDefaultLocales {

  /*
   * Supported Languages Keys + Locales
   */

  String ENGLISH_LANG_KEY = "en";
  String ARABIC_LANG_KEY = "ar";
  String ARABIC_COUNTRY_KEY = "EG";

  Locale ENGLISH_LOCALE = Locale.US; // en-US
  Locale ARABIC_LOCALE = new Locale(ARABIC_LANG_KEY, ARABIC_COUNTRY_KEY); // ar-EG
}
