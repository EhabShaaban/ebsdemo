package com.ebs.dac.i18n.languageutile;

/**
 * @author Hisham Ramadan
 * @since Oct 8, 2015
 */
public class LanguageUnicodeRange {

  private Integer rangeStart;

  private Integer rangeEnd;

  public LanguageUnicodeRange(String start, String end) {
    this.rangeStart = unicodeToInt(start);
    this.rangeEnd = unicodeToInt(end);
  }

  private Integer unicodeToInt(String uc) {
    return Integer.parseInt(uc, 16);
  }

  public Integer getRangeStart() {
    return rangeStart;
  }

  public void setRangeStart(Integer rangeStart) {
    this.rangeStart = rangeStart;
  }

  public Integer getRangeEnd() {
    return rangeEnd;
  }

  public void setRangeEnd(Integer rangeEnd) {
    this.rangeEnd = rangeEnd;
  }
}
