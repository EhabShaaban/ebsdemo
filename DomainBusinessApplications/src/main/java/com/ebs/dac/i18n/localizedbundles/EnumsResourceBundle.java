package com.ebs.dac.i18n.localizedbundles;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import java.beans.Introspector;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.springframework.cglib.core.Local;

public class EnumsResourceBundle {

  private static final String ENUM_BUNDLE_BASENAME = "META-INF/Enums/enums";

  private static final Locale englishLocale = ISysDefaultLocales.ENGLISH_LOCALE;
  private static final Locale arabicLocale = ISysDefaultLocales.ARABIC_LOCALE;

  private static List<Locale> locales;

  static {
    locales = new ArrayList<Locale>();
    locales.add(arabicLocale);
    locales.add(englishLocale);
  }

  /**
   * This method is responsible for taking an ENUM class and return the corresponding localized
   * values based on the passed {@link Locale}
   *
   * @param cls the ENUM class
   * @param locale the locale of the returned values
   * @return a map of keys as the ENUM constant and their corresponding localized values based on
   *     the {@link Local}
   */
  public static Map<String, String> getValues(Class<?> cls, Locale locale) {
    if (locale == null) {
      locale = englishLocale;
    }

    Map<String, String> values = new HashMap<String, String>();
    List<?> keys = Arrays.asList(cls.getEnumConstants());
    if (keys != null) {
      for (Object key : keys) {
        String value = getLocalizedValue((Enum<?>) key, locale);
        values.put(key.toString(), value);
      }
    }

    return values;
  }

  /**
   * This method is responsible for taking an ENUM class and return all possible localized values
   *
   * @param cls the ENUM class
   * @param locale the locale of the returned values
   * @return a map of keys as the ENUM constant and their corresponding localized values based on
   *     the {@link Local}
   */
  public static Map<String, LocalizedString> getValues(Class<?> cls) {
    Map<String, LocalizedString> values = new HashMap<String, LocalizedString>();

    Object[] keys = cls.getEnumConstants();
    for (Object key : keys) {
      Map<String, String> possibleValues = new HashMap<String, String>();
      String value;

      for (Locale locale : locales) {
        value = getLocalizedValue((Enum<?>) key, locale);
        possibleValues.put(locale.getLanguage(), value);
      }

      values.put(key.toString(), new LocalizedString(possibleValues));
    }

    return values;
  }

  /**
   * Get the available localized values which corresponds to this ENUM key
   *
   * @param key one of the ENUM values
   * @return {@link LocalizedString} which contains all the localized values
   */
  public static LocalizedString getLocalizedValues(Enum<?> key) {
    LocalizedString localizedString = new LocalizedString();
    String value;
    for (Locale locale : locales) {
      value = getLocalizedValue(key, locale);
      if (value != null) {
        localizedString.setValue(locale, value);
      }
    }
    return localizedString;
  }

  /**
   * Get the localized value of that corresponds to the ENUM key and {@link Locale}
   *
   * @param key one of the ENUM values
   * @param locale the corresponding {@link Local} value
   * @return the value of the localized ENUM key if exists, the passed key otherwise
   */
  public static String getLocalizedValue(Enum<?> key, Locale locale) {
    ResourceBundle resourceBundle = ResourceBundle.getBundle(ENUM_BUNDLE_BASENAME, locale);
    String value;
    try {
      value =
          resourceBundle.getString(
              "enum_"
                  + Introspector.decapitalize(key.getClass().getSimpleName())
                  + "_"
                  + key.toString());
    } catch (MissingResourceException e) {
      value = key.toString();
    }
    return value;
  }
}
