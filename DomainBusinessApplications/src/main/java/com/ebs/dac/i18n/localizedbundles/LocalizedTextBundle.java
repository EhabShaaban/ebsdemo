package com.ebs.dac.i18n.localizedbundles;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizedTextBundle {

  private String code;
  private Object[] parameters;
  private String resourceBaseName;
  private ClassLoader resourceLoader;

  public LocalizedTextBundle(String code, String resourceBaseName, ClassLoader resourceLoader) {
    this.code = code;
    this.resourceBaseName = resourceBaseName;
    this.resourceLoader = resourceLoader;
  }

  public LocalizedTextBundle(
      String code, Object[] parameters, String resourceBaseName, ClassLoader resourceLoader) {
    this.code = code;
    this.parameters = parameters;
    this.resourceBaseName = resourceBaseName;
    this.resourceLoader = resourceLoader;
  }

  private ResourceBundle loadResourceBundle(Locale locale) {
    return ResourceBundle.getBundle(resourceBaseName, locale, resourceLoader);
  }

  public String getCode() {
    return code;
  }

  public Object[] getParameters() {
    return parameters;
  }

  public ClassLoader getResourceLoader() {
    return resourceLoader;
  }

  private String getText() {
    String text = loadResourceBundle(Locale.ENGLISH).getString(code);

    if (parameters != null) {
      Object[] localizedParameters = new Object[parameters.length];

      for (int i = 0; i < parameters.length; ++i) {
        Object parameter = parameters[i];

        if (parameter != null) {
          if (parameter instanceof LocalizedTextBundle) {
            parameter = ((LocalizedTextBundle) parameter).getText();
          }
        }

        localizedParameters[i] = parameter;
      }

      text = MessageFormat.format(text, localizedParameters);
    }

    return text;
  }

  private String getLocalizedText(Locale locale) {
    String text = loadResourceBundle(locale).getString(code);

    if (parameters != null) {
      Object[] localizedParameters = new Object[parameters.length];

      for (int i = 0; i < parameters.length; ++i) {
        Object parameter = parameters[i];

        if (parameter != null) {
          if (parameter instanceof LocalizedTextBundle) {
            parameter = ((LocalizedTextBundle) parameter).getLocalizedText(locale);
          }
        }

        localizedParameters[i] = parameter;
      }

      text = MessageFormat.format(text, localizedParameters);
    }

    return text;
  }
}
