package com.ebs.dac.i18n.languageutile;

import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Hisham Ramadan
 * @author Ahmed Ali Elfeky
 * @since Oct 8, 2015
 */
public class LangugeDetector {

  static LanguageUnicodeRange lanAR =
      new LanguageUnicodeRange(
          ILanguageRanges.AR_RANGE_START_SPECTAIL_CASE, ILanguageRanges.AR_RANGE_END_SPECTAIL_CASE);
  private static Map<String, LanguageUnicodeRange> languagesUnicodeRanges =
      new HashMap<String, LanguageUnicodeRange>();

  private static void fillLanguagesUnicodeRanges() {
    languagesUnicodeRanges.put(
        ISysDefaultLocales.ARABIC_LANG_KEY,
        new LanguageUnicodeRange(ILanguageRanges.AR_RANGE_START, ILanguageRanges.AR_RANGE_END));
    languagesUnicodeRanges.put(
        ISysDefaultLocales.ENGLISH_LANG_KEY,
        new LanguageUnicodeRange(ILanguageRanges.EN_RANGE_START, ILanguageRanges.EN_RANGE_END));
  }

  private static Integer charToIntUnicode(char ch) {
    return Integer.parseInt(String.format("\\u%04x", (int) ch).substring(2), 16);
  }

  public static String detect(String str) {
    String result = "";
    boolean validStr = true;
    fillLanguagesUnicodeRanges();

    // Get language of the first character
    String firstCharLang = "";
    int i = 0;
    Character firstChar = str.trim().charAt(i);
    while (!Character.isAlphabetic(firstChar) && i + 1 < str.length()) {
      firstChar = str.charAt(++i);
    }
    for (Map.Entry<String, LanguageUnicodeRange> entry : languagesUnicodeRanges.entrySet()) {

      String lang = entry.getKey();
      Integer rangeEnd = entry.getValue().getRangeEnd();
      Integer rangeStart = entry.getValue().getRangeStart();
      if (lang.equals("ar")) {
        if ((firstChar >= rangeStart && firstChar <= rangeEnd)
            || (firstChar >= lanAR.getRangeStart() && firstChar <= lanAR.getRangeEnd())) {
          firstCharLang = lang;
          break;
        }
      } else if (firstChar >= rangeStart && firstChar <= rangeEnd) {
        firstCharLang = lang;
        break;
      }
    }

    if (firstCharLang.equals("")) {
      // Neither arabic nor english
      validStr = false;
    } else {
      // Check whether all other characters are in the same lang as the first char of not
      Integer rangeEnd = languagesUnicodeRanges.get(firstCharLang).getRangeEnd();
      Integer rangeStart = languagesUnicodeRanges.get(firstCharLang).getRangeStart();
      for (Character c : str.toCharArray()) {
        if (!Character.isAlphabetic(c)) {
          continue;
        }
        Integer uc = charToIntUnicode(c);
        if (firstCharLang.equals("ar")) {
          if (!((uc >= rangeStart && uc <= rangeEnd)
              || (uc >= lanAR.getRangeStart() && uc <= lanAR.getRangeEnd()))) {
            validStr = false;
            break;
          }
        } else if (!(uc >= rangeStart && uc <= rangeEnd)) {
          validStr = false;
          break;
        }
      }
    }

    if (validStr) {
      result = firstCharLang;
    } else {
      result = "Invalid Language";
    }

    return result;
  }
}
