package com.ebs.dac.edit.config;

import java.util.ArrayList;
import java.util.List;

public class AuthorizedPermissionsConfig {

  List<AuthorizedPermissionConfig> permissionsConfig;

  public AuthorizedPermissionsConfig() {
    permissionsConfig = new ArrayList<>();
  }

  public List<AuthorizedPermissionConfig> getPermissionsConfig() {
    return permissionsConfig;
  }

  public void addPermissionConfig(
      String entitySysName, String permissionToCheck, String permissionToReturn) {
    AuthorizedPermissionConfig authorizedPermissionConfig =
        new AuthorizedPermissionConfig(entitySysName, permissionToCheck, permissionToReturn);
    this.permissionsConfig.add(authorizedPermissionConfig);
  }

  public class AuthorizedPermissionConfig {
    private String entitySysName;
    private String permissionToCheck;
    private String permissionToReturn;

    public AuthorizedPermissionConfig(
        String entitySysName, String permissionToCheck, String permissionToReturn) {
      this.entitySysName = entitySysName;
      this.permissionToCheck = permissionToCheck;
      this.permissionToReturn = permissionToReturn;
    }

    public String getEntitySysName() {
      return entitySysName;
    }

    public String getPermissionToCheck() {
      return permissionToCheck;
    }

    public String getPermissionToReturn() {
      return permissionToReturn;
    }
  }
}
