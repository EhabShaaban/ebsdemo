package com.ebs.dac.edit.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SectionEditConfig {

  private List<String> authorizedReads;
  private String[] mandatories;
  private Set<String> enabledAttributes;
  private Map<String, Object> defaultValues;

  public SectionEditConfig() {
	  authorizedReads = new ArrayList<>();
	  mandatories = new String[0];
	  enabledAttributes = new HashSet<>();
	  defaultValues = new HashMap<>();
  }

  public List<String> getAuthorizedReads() {
    return authorizedReads;
  }

  public void setAuthorizedReads(List<String> authorizedReads) {
    this.authorizedReads = authorizedReads;
  }

  public String[] getMandatories() {
    return mandatories;
  }

  public void setMandatories(String[] mandatories) {
    this.mandatories = mandatories;
  }

  public Set<String> getEnabledAttributes() {
    return enabledAttributes;
  }

  public void setEnabledAttributes(Set<String> enabledAttributes) {
    this.enabledAttributes = enabledAttributes;
  }

  public Map<String, Object> getDefaultValues() {
    return defaultValues;
  }

  public void setDefaultValues(Map<String, Object> defaultValues) {
    this.defaultValues = defaultValues;
  }
  
  public void addDefaultValue(String key , Object defaultValue) {
	    this.defaultValues.put(key, defaultValue);
  }
  
  
}
