package com.ebs.dac.foundation.realization.condition.resolvers;

import static org.junit.Assert.assertEquals;

import com.ebs.dac.dbo.exceptions.objects.DevRulesViolation;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.realization.condition.resolvers.utils.EntityAttributeResolverContextTestUtils;
import com.ebs.dac.foundation.realization.condition.resolvers.utils.EntityAttributeResolverContextTestUtils.FakeEnum;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Maged Zakzouk
 * @since Feb 28, 2018
 */
public class EntityAttributeResolverContextTest {

  private EntityAttributeResolverContext attributesResolver;
  private EntityAttributeResolverContextTestUtils testUtils;

  @Before
  public void setUp() {
    attributesResolver = new EntityAttributeResolverContext();
    testUtils = new EntityAttributeResolverContextTestUtils();
  }

  @Test
  public void resolveAttributes_testAnEmptyMap_ReturnedAnEmptyMap() throws Exception {
    Map<String, Object> result = attributesResolver.resolveAttributes(new HashMap<>());
    Assert.assertTrue(result.isEmpty());
  }

  @Test
  public void resolveAttributes_testMapWithNullValue_ReturnedAnEmptyMap() throws Exception {
    Map<String, Object> map = new HashMap<>();
    map.put("test null value", null);
    Map<String, Object> result = attributesResolver.resolveAttributes(map);
    Assert.assertTrue(result.isEmpty());
  }

  @Test
  public void
      resolveAttributes_testBasicAttributesValues_ReturnedMapWithSameOriginalMapAttributesValues()
          throws Exception {
    Map<String, Object> map = testUtils.createMapWithBasicAttributes();
    Map<String, Object> result = attributesResolver.resolveAttributes(map);
    assertEquals(map.get("test number"), result.get("test number"));
    assertEquals(map.get("test string"), result.get("test string"));
    assertEquals(map.get("test boolean"), result.get("test boolean"));
    assertEquals(((FakeEnum) (map.get("test enum"))).getId().toString(), result.get("test enum"));
  }

  @Test
  public void
      resolveAttributes_testEmbeddedLocalizedStringAttributeValue_ReturnedMapWithEnglishValueOfLocalizedString()
          throws Exception {
    Map<String, Object> map = testUtils.createMapWithEmbeddedAttributes();
    Map<String, Object> result = attributesResolver.resolveAttributes(map);
    assertEquals(
        ((LocalizedString) (map.get("test localized string")))
            .getValue(ISysDefaultLocales.ENGLISH_LOCALE),
        result.get("test localized string"));
  }

  @Test
  public void
      resolveAttributes_testEmbeddedLocalizedStringAttributeValueWithNullValues_ReturnedMapWithEnglishValueOfLocalizedString()
          throws Exception {
    Map<String, Object> map = testUtils.createMapWithEmbeddedAttributesWithNullValues();
    Map<String, Object> result = attributesResolver.resolveAttributes(map);
    assertEquals(
        ((LocalizedString) (map.get("test localized string")))
            .getValue(ISysDefaultLocales.ENGLISH_LOCALE),
        result.get("test localized string"));
  }

  @Test
  public void resolveAttributes_testUnsupportedAttribure_ReturnedAnEmptyMap() throws Exception {
    try {
      Map<String, Object> map = testUtils.createMapWithUnsupportedAttribure();
      Map<String, Object> result = attributesResolver.resolveAttributes(map);
      Assert.assertFalse("This test case should throw DevRulesViolation Exception", true);
    } catch (DevRulesViolation e) {
      Assert.assertTrue(true);
    }
  }
}
