package com.ebs.dac.foundation.realization.condition.adapter;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConditionManagerTest {
  private ConditionManager conditionManager;

  @Before
  public void setUp() {
    conditionManager = new ConditionManager();
  }

  @Test
  public void ValidCondition_PassNullCondition_ReturnFalse() {
    String condition = "";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithEqualOperator_ReturnTrue() {
    String condition = "[price = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithGreaterThanOperator_ReturnTrue() {
    String condition = "[price > 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithGreaterThanOrEqualOperator_ReturnTrue() {
    String condition = "[price >= 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithLessThanOperator_ReturnTrue() {
    String condition = "[price < 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithLessThanOrEqualOperator_ReturnTrue() {
    String condition = "[price <= 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithNotEqualOperator_ReturnTrue() {
    String condition = "[price = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringStartWithNumber_ReturnFalse() {
    String condition = "[ 1price = 100]";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringStartWithUnderScore_ReturnTrue() {
    String condition = "[ _price = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringStartWithDollarSign_ReturnTrue() {
    String condition = "[ $price = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringWithSpacesAtStart_ReturnTrue() {
    String condition = "[     price = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringWithSpacesAtMiddle_ReturnFalse() {
    String condition = "[ pr ice = 100]";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringWithLowerAndUpperCases_ReturnTrue() {
    String condition = "[ PrIce = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithFirstOperandStringWithAllValidCharacters_ReturnTrue() {
    String condition =
        "[ _$0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$0123456789 = 100]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandStringStartWithoutSingleQuote_ReturnFalse() {
    String condition = "[ price = jahl]";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandStringWithAllValidCharacters_ReturnTrue() {
    String condition =
        "[ price = '_0123456789abcdef% ghijklmnopqrst  @uvwx.yzAB.CDEFGHIJKLMNOPQRS, \n TUVWXYZ_0123456789']";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandIntegerNumber_ReturnTrue() {
    String condition = "[ price = 200]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandFullFloatNumber_ReturnTrue() {
    String condition = "[ price = 1.0]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandFloatNumberOnlyRightSide_ReturnTrue() {
    String condition = "[ price = .9]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandFloatNumberOnlyLeftSide_ReturnTrue() {
    String condition = "[ price = 1.]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandFloatWithDoubleDots_ReturnFalse() {
    String condition = "[ price = 1.2.0]";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandBigInteger_ReturnTrue() {
    String condition =
        "[ price = 12345678998765432112345678998765432112345678998765432112345678991223"
            + "876543211234567899876543211234567899876543211234567899876543211234567899876543211234562123"
            + "789987654321123456789987654321123456789987654321123456789987654321123456789987654321121233"
            + "456789987654321123456789987654321123456789987654321123456789987654321123456789987654321221"
            + "234567899876543211234567899876543211234567899876543211234567899876543211234567899876542232"
            + "112345678998765432112345678998765432112345678998765432112345678998765432112345678998762543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876254"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "212321312312312123321123456789987654321123456789987654321123456789987654321123456789987321]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandBigFloat_ReturnTrue() {
    String condition =
        "[ price = 1.2345678998765432112345678998765432112345678998765432112345678991223"
            + "876543211234567899876543211234567899876543211234567899876543211234567899876543211234562123"
            + "789987654321123456789987654321123456789987654321123456789987654321123456789987654321121233"
            + "456789987654321123456789987654321123456789987654321123456789987654321123456789987654321221"
            + "234567899876543211234567899876543211234567899876543211234567899876543211234567899876542232"
            + "112345678998765432112345678998765432112345678998765432112345678998765432112345678998762543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876254"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "212321312312312123321123456789987654321123456789987654321123456789987654321123456789987321]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  // minus second operand test cases
  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandMinusIntegerNumber_ReturnTrue() {
    String condition = "[ price = -200]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandMinusFullFloatNumber_ReturnTrue() {
    String condition = "[ price = -1.0]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandMinusFloatNumberOnlyRightSide_ReturnTrue() {
    String condition = "[ price = -.9]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandMinusFloatNumberOnlyLeftSide_ReturnTrue() {
    String condition = "[ price = -1.]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidSimpleConditionWithSecondOperandMinusFloatWithDoubleDots_ReturnFalse() {
    String condition = "[ price = -1.2.0]";
    Assert.assertFalse(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandMinusBigInteger_ReturnTrue() {
    String condition =
        "[ price = -12345678998765432112345678998765432112345678998765432112345678991223"
            + "876543211234567899876543211234567899876543211234567899876543211234567899876543211234562123"
            + "789987654321123456789987654321123456789987654321123456789987654321123456789987654321121233"
            + "456789987654321123456789987654321123456789987654321123456789987654321123456789987654321221"
            + "234567899876543211234567899876543211234567899876543211234567899876543211234567899876542232"
            + "112345678998765432112345678998765432112345678998765432112345678998765432112345678998762543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876254"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "212321312312312123321123456789987654321123456789987654321123456789987654321123456789987321]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidSimpleConditionWithSecondOperandMinusBigFloat_ReturnTrue() {
    String condition =
        "[ price = -1.2345678998765432112345678998765432112345678998765432112345678991223"
            + "876543211234567899876543211234567899876543211234567899876543211234567899876543211234562123"
            + "789987654321123456789987654321123456789987654321123456789987654321123456789987654321121233"
            + "456789987654321123456789987654321123456789987654321123456789987654321123456789987654321221"
            + "234567899876543211234567899876543211234567899876543211234567899876543211234567899876542232"
            + "112345678998765432112345678998765432112345678998765432112345678998765432112345678998762543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876543"
            + "211234567899876543211234567899876543211234567899876543211234567899876543211234567899876254"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "321123456789987654321123456789987654321123456789987654321123456789987654321123456789987654"
            + "212321312312312123321123456789987654321123456789987654321123456789987654321123456789987321]";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  // _______________________________________________________________________________________________________________
  @Test
  public void ValidCondition_PassValidCompositeCondationWithSimpleORSimpleCondition_ReturnTrue() {
    String condition = "([ price = 1] || [price = 1])";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void ValidCondition_PassValidCompositeCondationWithSimpleAndSimpleCondition_ReturnTrue() {
    String condition = "([ price = 1] && [price = 1])";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithSimpleAndCompositeCondition_ReturnTrue() {
    String condition = "([ price = 1] && ([price = 1] && [price = 1]))";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithSimpleORCompositeCondition_ReturnTrue() {
    String condition = "([ price = 1] || ([price = 1] && [price = 1]))";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithCompositeAndSimpleCondition_ReturnTrue() {
    String condition = "(([price = 1] && [price = 1] ) && [price = 1])";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithCompositeORSimpleCondition_ReturnTrue() {
    String condition = "(([price = 1] && [price = 1]) || [price = 1])";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithCompositeAndCompositeCondition_ReturnTrue() {
    String condition = "(([price = 1] && [price = 1]) && ([price = 1] && [price = 1]))";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }

  @Test
  public void
      ValidCondition_PassValidCompositeCondationWithCompositeORCompositeCondition_ReturnTrue() {
    String condition = "(([price = 1] && [price = 1]) || ([price = 1] && [price = 1]))";
    Assert.assertTrue(conditionManager.validateCondition(condition));
  }
}
