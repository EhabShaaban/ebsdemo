package com.ebs.dac.foundation.realization.concurrency;

import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeUtils.MillisProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This class is built on the assumption that tests will be running sequentially<br>
 * <br>
 * <code>
 * 		new DateTime(); // 2014-04-01T<b>13</b>:43:49.282+02:00<br>
 * 	<b>setCustomDateTime(new DateTime().plusHours(1));</b><br>
 * 	new DateTime(); // 2014-04-01T<b>14</b>:43:49.388+02:00<br>
 * 	<b>resetCustomDateTime();</b><br>
 * 	new DateTime(); // 2014-04-01T<b>13</b>:43:49.390+02:00<br>
 * </code>
 *
 * @author Hesham Saleh
 */
public class JodaTimeChangableTest {

  private static final Logger logger = Logger.getLogger(JodaTimeChangableTest.class.getName());
  private DateTime customDateTime;

  @BeforeClass
  public static void setUpCustomDateTimeBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownCustomDateTimeAfterClass() throws Exception {}

  @Before
  public void setUpCustomDateTimeBeforeTest() throws Exception {
    DateTimeUtils.setCurrentMillisProvider(
        new MillisProvider() {

          public long getMillis() {
            if (customDateTime != null) {
              return customDateTime.getMillis();
            } else {
              return System.currentTimeMillis();
            }
          }
        });
  }

  @After
  public void tearDownCustomDateTimeAfterTest() throws Exception {
    setCustomDateTime(null);
  }

  public void setCustomDateTime(DateTime customDateTime) {
    this.customDateTime = customDateTime;
  }

  public void resetCustomDateTime() {
    setCustomDateTime(null);
  }

  @Test
  public void testJodaTimeChangableTest() {
    logger.info("" + new DateTime()); // 2014-04-01T13:43:49.282+02:00
    setCustomDateTime(new DateTime().plusHours(1));
    logger.info("" + new DateTime()); // 2014-04-01T14:43:49.388+02:00
    resetCustomDateTime();
    logger.info("" + new DateTime()); // 2014-04-01T13:43:49.390+02:00
  }
}
