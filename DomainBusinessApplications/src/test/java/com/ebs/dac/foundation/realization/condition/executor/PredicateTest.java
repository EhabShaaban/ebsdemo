package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import com.ebs.dac.foundation.exceptions.conditionmanager.TypeMisMatchException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PredicateTest {
  private final Logger LOGGER = Logger.getLogger(Predicate.class.getName());

  private Predicate predicate;
  private Operand leftOperand;
  private Operand rightOperand;
  private Map<String, Object> map;

  @Before
  public void setUp() {
    predicate = new Predicate();
    leftOperand = new Operand();
    rightOperand = new Operand();
    map = new HashMap<>();
  }

  @Test
  public void getLeftValueFromMap_passMapDoesNotContainLeftHandSideStringValue_throwsException()
      throws Exception {
    map.put("price", 10.0);
    leftOperand.setStringValue("height");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setDoubleValue(10.0);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    try {
      predicate.execute(map);
      Assert.fail("Fail: IllegalArgumentException was not thrown");
    } catch (IllegalArgumentException e) {
      LOGGER.info("Success: IllegalArgumentException was thrown");
    }
  }

  @Test
  public void
      executePredicate_passMapContainLeftHandSideStringValueWithDifferentType_throwsException()
          throws Exception {
    map.put("price", 100);
    leftOperand.setStringValue("price");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setDoubleValue(100.0);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    try {
      predicate.execute(map);
      Assert.fail("Fail: TypeMisMatchException was not thrown");
    } catch (TypeMisMatchException e) {
      LOGGER.info("Success: TypeMisMatchException was thrown");
    }
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeFloatAndRighHandSideFloatWithTheSameValues_returnsTrue()
          throws Exception {
    map.put("price", 10.0);
    leftOperand.setStringValue("price");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setDoubleValue(10.0);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertTrue(predicate.execute(map).isResult());
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeFloatAndRighHandSideFloatWithTheDifferentValues_returnsFalse()
          throws Exception {
    map.put("price", 10.0);
    leftOperand.setStringValue("price");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setDoubleValue(11.0);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertFalse(predicate.execute(map).isResult());
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeLongAndRightHandSideLongWithTheSameValues_returnsTrue()
          throws Exception {
    map.put("price", 10L);
    leftOperand.setStringValue("price");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setLongValue(10L);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertTrue(predicate.execute(map).isResult());
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeLongAndRightHandSideLongWithTheDifferentValues_returnsFalse()
          throws Exception {
    map.put("price", 10L);
    leftOperand.setStringValue("price");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setLongValue(11L);
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertFalse(predicate.execute(map).isResult());
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeStringAndRighHandSideStringWithTheSameValues_returnsTrue()
          throws Exception {
    map.put("name", "test");
    leftOperand.setStringValue("name");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setStringValue("test");
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertTrue(predicate.execute(map).isResult());
  }

  @Test
  public void
      executePredicate_passPredicateWithLeftHandSideOfTypeStringAndRighHandSideStringWithTheDifferentValues_returnsFalse()
          throws Exception {
    map.put("name", "test");
    leftOperand.setStringValue("name");
    predicate.setLeftOperand(leftOperand);
    rightOperand.setStringValue("wrongTest");
    predicate.setRightOperand(rightOperand);
    predicate.setOperator("=");
    Assert.assertFalse(predicate.execute(map).isResult());
  }
}
