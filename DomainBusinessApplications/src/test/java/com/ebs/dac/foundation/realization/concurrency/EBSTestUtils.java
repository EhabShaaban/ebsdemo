package com.ebs.dac.foundation.realization.concurrency;

import com.ebs.dac.foundation.apis.security.IBDKUser;

public class EBSTestUtils {

  public static IBDKUser mockUser() {
    return mockUser("mohamed", "ZAHRA");
  }

  public static IBDKUser mockUser(final String username, final String companyCode) {
    IBDKUser user =
        new IBDKUser() {
          public String getUsername() {
            return username;
          }

          public String getCompany() {
            return companyCode;
          }

          public boolean equals(IBDKUser anotherUser) {
            return (username.equals(anotherUser.getUsername()));
          }

          @Override
          public Long getUserId() {
            return 0L;
          }

          @Override
          public Boolean getSysPasswordFlag() {
            return false;
          }
        };

    // TODO In case of the tested code actually uses the usual SecurityUtils.getSubject() shiro's
    // specific way, the following may be useful
    // So, better to create a dedicated Test class to be extended by tests that requires
    // authentication
    // See http://agileice.blogspot.com/2009/12/coding-around-shiro-security-in-grails.html
    // And https://shiro.apache.org/testing.html
    /*
    ThreadContext.put(ThreadContext.SECURITY_MANAGER_KEY,
    	  [getSubject: {subject} as SecurityManager])
    	SecurityUtils.metaClass.static.getSubject = {subject}
    */

    return user;
  }
}
