package com.ebs.dac.foundation.realization.processing.fakeobjects;

import com.ebs.dac.dbo.processing.BasicAttribute;

/** Created by yara on 11/12/17. */
public class EmbeddedAttributeSample {

  public static final String NESTED1_ATTR = "nested1";
  public static final BasicAttribute nested1Attr = new BasicAttribute( NESTED1_ATTR);

  public static final String NESTED2_ATTR = "nested2";
  public static final BasicAttribute nested2Attr = new BasicAttribute(NESTED2_ATTR);
}
