package com.ebs.dac.foundation.realization.condition.resolvers.utils;

import com.ebs.dac.dbo.types.BusinessEnum;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Maged Zakzouk
 * @since Feb 28, 2018
 */
public class EntityAttributeResolverContextTestUtils {
  private Map<String, Object> map;
  private LocalizedString localizedString;

  public EntityAttributeResolverContextTestUtils() {
    map = new HashMap<>();
  }

  public Map<String, Object> createMapWithBasicAttributes() {
    map.put("test number", 10.0);
    map.put("test string", "test string attribute");
    map.put("test boolean", true);
    addEnumAttributeToMap();
    return map;
  }

  private void addEnumAttributeToMap() {
    FakeEnum fakeEnum = new FakeEnum();
    fakeEnum.setId(FakeEnumObject.V1);
    map.put("test enum", fakeEnum);
  }

  public Map<String, Object> createMapWithEmbeddedAttributes() {
    localizedString = new LocalizedString();
    localizedString.setValue(ISysDefaultLocales.ENGLISH_LOCALE, "english value");
    localizedString.setValue(ISysDefaultLocales.ARABIC_LOCALE, "arabic value");
    map.put("test localized string", localizedString);
    return map;
  }

  public Map<String, Object> createMapWithEmbeddedAttributesWithNullValues() {
    localizedString = new LocalizedString();
    localizedString.setValue(ISysDefaultLocales.ENGLISH_LOCALE, null);
    localizedString.setValue(ISysDefaultLocales.ARABIC_LOCALE, null);
    map.put("test localized string", localizedString);
    return map;
  }

  public Map<String, Object> createMapWithUnsupportedAttribure() {
    map.put("test Fake Coded Object", new FakeCodedObject());
    return map;
  }

  public enum FakeEnumObject {
    V1,
    V2
  }

  public class FakeEnum extends BusinessEnum {

    private static final long serialVersionUID = 1L;

    @Enumerated(EnumType.STRING)
    private FakeEnumObject value;

    public FakeEnum() {}

    @Override
    public FakeEnumObject getId() {
      return this.value;
    }

    public void setId(FakeEnumObject id) {
      this.value = id;
    }
  }
}
