package com.ebs.dac.foundation.realization.concurrency;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.exceptions.concurrency.InvalidResourceEditTokenException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyLockedException;
import com.ebs.dac.foundation.exceptions.concurrency.ResourceAlreadyUnlockedException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.realization.concurrency.policies.TimeoutEditTokenValidityPolicy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Menna Sayed
 * @date May 9, 2017
 */
public class ConcurrencyDataAccessManagerTest extends JodaTimeChangableTest {

  private static final String RESOURCE_ROOT_EQUALS_A = "A";
  private static final String RESOURCE_ROOT_EQUALS_B = "B";
  private static final String RESOURCE_NAME_EQUALS_AA = "AA";
  private static final String RESOURCE_NAME_EQUALS_BB = "BB";
  private static final String RESOURCE_NAME_EQUALS_Asterisk = "*";
  private static final String NOT_EXIST_TOKEN_ID = "NotExistTokenId";
  private static final String VIOLATE_TOKEN_ID = "diffTokenId";
  private static final String RESOURCE_ID_EQUALS_1L = "1";
  private static final String RESOURCE_ID_EQUALS_2L = "2";
  private static final String RESOURCE_ID_EQUALS_3L = "3";
  private static final String RESOURCE_ID_EQUALS_4L = "4";
  private static final Integer INITIAL_TOKEN_TIMEOUT_EQUEL_10 = 10;
  private static final Integer INITIAL_TOKEN_TIMEOUT_EQUEL_60 = 60;
  private static final Integer EXTENTION_EQUEL_60 = 60;
  private static final Integer EXTENTION_EQUEL_30 = 30;
  private static final Logger logger =
      LoggerFactory.getLogger(ConcurrencyDataAccessManagerTest.class.getName());
  protected volatile List<Event> events;
  private ConcurrentDataAccessManagersPool pool;
  private IBDKUser user;

  @Before
  public void beforeEachTest() throws Exception {
    events = new CopyOnWriteArrayList<Event>();
    user = EBSTestUtils.mockUser();
    pool =
        new ConcurrentDataAccessManagersPool(
            new TimeoutEditTokenValidityPolicy(INITIAL_TOKEN_TIMEOUT_EQUEL_60, EXTENTION_EQUEL_60),
            null);
    pool.userAccountSecurityService(
        new IUserAccountSecurityService() {
          @Override
          public IBDKUser getLoggedInUser() {
            return user;
          }
        });
  }

  @After
  public void AfterEachTest() throws Exception {
    if (pool != null) {
      pool.cleanUp(true);
    }
  }

  private IConcurrentDataAccessManager successLockWithResourceNameAndResourceIdEquel1L(
      String resourceName)
      throws Exception {
    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);

    LockDetails lockDetails = managerA.lock(resourceName, RESOURCE_ID_EQUALS_1L, user);
    managerA.verifyToken(lockDetails.getTokenId(), resourceName, RESOURCE_ID_EQUALS_1L);

    return managerA;
  }

  @Test
  public void lock_ReLockBySameThread_AlreadyLockedBySameUserException()
      throws Exception {
    IConcurrentDataAccessManager successLockWithResourceName =
        successLockWithResourceNameAndResourceIdEquel1L(RESOURCE_NAME_EQUALS_AA);
    try {
      successLockWithResourceName.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
      Assert.fail(
          "ERROR: No ResourceAlreadyLockedException was thrown in lock Function (ReLockBySameThread)");
    } catch (ResourceAlreadyLockedBySameUserException e) {
      logger.info(
          "Success: ResourceAlreadyLockedException was thrown in lock Function (ReLockBySameThread)");
    }
  }

  @Test
  public void
      lock_ReLockBySameResourceCodeAndResourceNameNotAsteriskBySameUser_ReturnedLockDetails()
      throws Exception {
    IConcurrentDataAccessManager successLockWithResourceName =
        successLockWithResourceNameAndResourceIdEquel1L(RESOURCE_NAME_EQUALS_Asterisk);
    LockDetails lockDetailsForResourceId_Equal_1L =
        successLockWithResourceName.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    assertThat(
        lockDetailsForResourceId_Equal_1L.getResourceName(),
        containsString(RESOURCE_NAME_EQUALS_AA));
    assertThat(lockDetailsForResourceId_Equal_1L.getResourceCode(), equalTo(RESOURCE_ID_EQUALS_1L));
  }

  @Test
  public void
      lock_ReLockBySameResourceCodeAndResourceNameIsAsterisk_ThrowAlreadyLockedBySameUserException()
      throws Exception {
    IConcurrentDataAccessManager successLockWithResourceName =
        successLockWithResourceNameAndResourceIdEquel1L(RESOURCE_NAME_EQUALS_AA);
    try {
      successLockWithResourceName.lock(RESOURCE_NAME_EQUALS_Asterisk, RESOURCE_ID_EQUALS_1L, user);
      Assert.fail(
          "ERROR: No RootResourceAlreadyHaveSeparateLocksException was thrown in lock Function (ReLockBySameResourceIdAndResourceNameIsAsterisk)");
    } catch (ResourceAlreadyLockedBySameUserException e) {
      logger.info(
          "Success: RootResourceAlreadyHaveSeparateLocksException was thrown in lock Function (ReLockBySameResourceIdAndResourceNameIsAsterisk)");
    }
  }

  @Test
  public void lock_LockWithAnotherResourceIdAfterSuccessLock_ReturnedLockDetails()
      throws Exception {
    IConcurrentDataAccessManager successLockWithResourceName =
        successLockWithResourceNameAndResourceIdEquel1L(RESOURCE_NAME_EQUALS_AA);

    LockDetails lockDetailsForResourceId_Equal_2L =
        successLockWithResourceName.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_2L, user);

    assertThat(
        lockDetailsForResourceId_Equal_2L.getResourceName(),
        containsString(RESOURCE_NAME_EQUALS_AA));
    assertThat(lockDetailsForResourceId_Equal_2L.getResourceCode(), equalTo(RESOURCE_ID_EQUALS_2L));
  }

  @Test
  public void
      lock_TryingLockTheSameInstanceInTwoThread_AddedResourceLockedEventThenAlreadyLockedBySameUserExceptionvent()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);

    Thread thread1 =
        new Thread(
            new Runnable() {

              public void run() {
                try {
                  managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
                  events.add(
                      new ResourceLockedEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "T1"));
                } catch (Exception e) {
                  events.add(
                      new AlreadyLockedBySameUserExceptionEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "T1"));
                }
              }
            });

    Thread thread2 =
        new Thread(
            new Runnable() {

              public void run() {
                try {
                  managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
                  events.add(
                      new ResourceLockedEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "T2"));
                } catch (Exception e) {
                  events.add(
                      new AlreadyLockedBySameUserExceptionEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "T2"));
                }
              }
            });

    thread1.start();
    thread2.start();
    // use join for waiting until it finishes, the events list should be updated
    thread1.join();
    thread2.join();

    boolean isResourceLockedEvent =
        events.get(0) instanceof ResourceLockedEvent
            || events.get(1) instanceof ResourceLockedEvent;
    boolean isAlreadyLockedBySameUserExceptionEvent =
        events.get(0) instanceof AlreadyLockedBySameUserExceptionEvent
            || events.get(1) instanceof AlreadyLockedBySameUserExceptionEvent;

    assertThat(isResourceLockedEvent, is(equalTo(true)));
    assertThat(isAlreadyLockedBySameUserExceptionEvent, is(equalTo(true)));
  }

  @Test
  public void
      lock_LockTwoDifferentResourceRootsWithTheSameResourceIdInTwoThreads_AddedTwoResourcesLockedEvent()
      throws Exception {

    Thread t1 =
        new Thread(
            new Runnable() {

              public void run() {
                IConcurrentDataAccessManager managerA =
                    pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
                try {
                  managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
                  events.add(
                      new ResourceLockedEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "FIRST"));
                } catch (Exception e) {
                  events.add(
                      new ResourceAlreadyLockedExceptionEvent(
                          RESOURCE_ROOT_EQUALS_A,
                          RESOURCE_NAME_EQUALS_AA,
                          RESOURCE_ID_EQUALS_1L,
                          "FIRST"));
                }
              }
            });

    Thread t2 =
        new Thread(
            new Runnable() {

              public void run() {
                IConcurrentDataAccessManager managerB =
                    pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_B);
                try {
                  managerB.lock(RESOURCE_NAME_EQUALS_BB, RESOURCE_ID_EQUALS_1L, user);
                  events.add(
                      new ResourceLockedEvent(
                          RESOURCE_ROOT_EQUALS_B,
                          RESOURCE_NAME_EQUALS_BB,
                          RESOURCE_ID_EQUALS_1L,
                          "SECOND"));
                } catch (Exception e) {
                  events.add(
                      new ResourceAlreadyLockedExceptionEvent(
                          RESOURCE_ROOT_EQUALS_B,
                          RESOURCE_NAME_EQUALS_BB,
                          RESOURCE_ID_EQUALS_1L,
                          "SECOND"));
                }
              }
            });
    t1.start();
    t2.start();
    // use join for waiting until it finishes, the events list should be updated
    t1.join();
    t2.join();

    assertThat(events.get(0), instanceOf(ResourceLockedEvent.class));
    assertThat(events.get(1), instanceOf(ResourceLockedEvent.class));
  }

  @Test
  public void verifyToken_WithNotExistLock_ThrowInvalidResourceEditTokenException()
      throws ResourceAlreadyLockedException, InvalidResourceEditTokenException,
          InterruptedException, ResourceAlreadyUnlockedException {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    try {
      managerA.verifyToken(NOT_EXIST_TOKEN_ID, RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L);
      Assert.fail(
          "ERROR: No InvalidResourceEditTokenException was thrown in verifyToken Function (WithNotExistLock)");
    } catch (InvalidResourceEditTokenException e) {
      logger.info(
          "Success: InvalidResourceEditTokenException was thrown in verifyToken Function (WithNotExistLock)");
    }
  }

  @Test
  public void
      verifyToken_LockAndUnlockTokenIdThenVerfiyTokenId_ThrowInvalidResourceEditTokenException()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);

    LockDetails lockDetails = managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    managerA.unlock(lockDetails.getTokenId());

    try {
      managerA.verifyToken(
          lockDetails.getTokenId(), lockDetails.getResourceName(), lockDetails.getResourceCode());
      Assert.fail(
          "ERROR: No InvalidResourceEditTokenException was thrown in verifyToken Function (LockAndUnlockTokenIdThenVerfiyTokenId)");
    } catch (InvalidResourceEditTokenException e) {
      logger.info(
          "Success: InvalidResourceEditTokenException was thrown in verifyToken Function (LockAndUnlockTokenIdThenVerfiyTokenId)");
    }
  }

  @Test
  public void unlock_UnlockTokenIdAlreadyLocked_LockDetailsNotNull()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    LockDetails lockDetails = managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    LockDetails unLockDetails = managerA.unlock(lockDetails.getTokenId());

    assertThat(unLockDetails.getResourceName(), containsString(RESOURCE_NAME_EQUALS_AA));
    assertThat(unLockDetails.getResourceCode(), equalTo(RESOURCE_ID_EQUALS_1L));
    assertThat(unLockDetails.getTokenId(), containsString(lockDetails.getTokenId()));
  }

  @Test
  public void unlock_UnlockAnotherResourceThatNotLocked_ThrowResourceAlreadyUnlockedException()
      throws InvalidResourceEditTokenException {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    try {
      managerA.unlock(VIOLATE_TOKEN_ID);
      Assert.fail(
          "ERROR: No ResourceAlreadyUnlockedException was thrown in unLock Function (UnlockAnotherResourceThatNotLocked)");
    } catch (ResourceAlreadyUnlockedException e) {
      logger.info(
          "Success: ResourceAlreadyUnlockedException was thrown in unLock Function (UnlockAnotherResourceThatNotLocked)");
    }
  }

  @Test
  public void extendLock_ExtendLockWithTokenId_NewLockHasExpireDateGreaterThanOlderLock()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    LockDetails lock = managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);

    LockDetails newLock =
        managerA.extendLock(lock.getTokenId(), RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L);
    assertThat(lock.getExpiryDate().isBefore(newLock.getExpiryDate()), is(true));
  }

  @Test
  public void
      extendLock_ExtendLockForAnotherResourceThatNotLocked_ThrowInvalidResourceEditTokenException()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    try {
      managerA.extendLock(VIOLATE_TOKEN_ID, RESOURCE_NAME_EQUALS_BB, RESOURCE_ID_EQUALS_2L);
      Assert.fail(
          "ERROR: No InvalidResourceEditTokenException was thrown in extendLock Function (ExtendLockForAnotherResourceThatNotLocked)");
    } catch (InvalidResourceEditTokenException e) {
      logger.info(
          "Success: InvalidResourceEditTokenException was thrown in extendLock Function (ExtendLockForAnotherResourceThatNotLocked)");
    }
  }

  @Test
  public void extendLock_ExtendLockWithDifferentTokenId_ThrowInvalidResourceEditTokenException()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    try {
      managerA.extendLock(VIOLATE_TOKEN_ID, RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L);
      Assert.fail(
          "ERROR: No InvalidResourceEditTokenException was thrown in extendLock Function (ExtendLockWithDifferentTokenId)");
    } catch (InvalidResourceEditTokenException e) {
      logger.info(
          "Success: InvalidResourceEditTokenException was thrown in extendLock Function (ExtendLockWithDifferentTokenId)");
    }
  }

  @Test
  public void cleanUp_BooleanAllIsTrue_ThrowInvalidResourceEditTokenException()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    LockDetails lockDetailFor1L =
        managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    LockDetails lockDetailFor2L =
        managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_2L, user);
    managerA.cleanUp(true);
    try {
      managerA.verifyToken(
          lockDetailFor1L.getTokenId(),
          lockDetailFor1L.getResourceName(),
          lockDetailFor1L.getResourceCode());
      Assert.fail("ERROR: The lock should have been cleaned");
    } catch (InvalidResourceEditTokenException e) {
      logger.info("Success: The lock has been cleaned");
    }
  }

  @Test
  public void cleanUp_BooleanAllIsFalseAndTimeOutForLock_ThrowInvalidResourceEditTokenException()
      throws Exception {
    pool =
        new ConcurrentDataAccessManagersPool(
            new TimeoutEditTokenValidityPolicy(INITIAL_TOKEN_TIMEOUT_EQUEL_10, EXTENTION_EQUEL_30),
            null);

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    LockDetails lock = managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);

    DateTime laterTime = lock.getExpiryDate().plusHours(1);
    setCustomDateTime(laterTime);
    managerA.cleanUp(false);

    try {
      managerA.verifyToken(lock.getTokenId(), lock.getResourceName(), lock.getResourceCode());
      Assert.fail("ERROR: The lock should have been cleaned");
    } catch (InvalidResourceEditTokenException e) {
      logger.info("Success: The lock has been cleaned");
    }
  }

  @Test
  public void
      cleanUp_BooleanAllIsFalse_ValidForLock2AndThrowInvalidResourceEditTokenExceptionForLock1()
      throws Exception {

    pool =
        new ConcurrentDataAccessManagersPool(
            new TimeoutEditTokenValidityPolicy(INITIAL_TOKEN_TIMEOUT_EQUEL_10, EXTENTION_EQUEL_30),
            null);
    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    LockDetails lockDetailFor1L =
        managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    LockDetails lockDetailFor2L =
        managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_2L, user);

    DateTime laterTime = lockDetailFor1L.getExpiryDate().plusHours(1);
    setCustomDateTime(laterTime);

    managerA
        .getByTokenId(lockDetailFor2L.getTokenId())
        .setExpiryDate(lockDetailFor2L.getExpiryDate().plusHours(2));

    managerA.cleanUp(false);

    managerA.verifyToken(
        lockDetailFor2L.getTokenId(),
        lockDetailFor2L.getResourceName(),
        lockDetailFor2L.getResourceCode());

    try {
      managerA.verifyToken(
          lockDetailFor1L.getTokenId(),
          lockDetailFor1L.getResourceName(),
          lockDetailFor1L.getResourceCode());
      Assert.fail("ERROR: The lock should have been cleaned");
    } catch (InvalidResourceEditTokenException e) {
      logger.info("Success: The lock has been cleaned");
    }
  }

  @Test
  public void listLocksAcquiredBy_LockResourcesThenRetrieveListOfThem_SizeOfListEquel3()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_2L, user);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_3L, user);

    IBDKUser anotherUser = EBSTestUtils.mockUser("AnotherUser", "Madina");
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_4L, anotherUser);

    List<LockDetails> locks = managerA.listLocksAcquiredBy(user);

    assertThat(locks.size(), is(equalTo(3)));
  }

  @Test
  public void listAllLocks_LockDifferentResourcesTypesThenRetrieveListOfThem_SizeOfListEquel4()
      throws Exception {

    IConcurrentDataAccessManager managerA =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_A);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_1L, user);
    managerA.lock(RESOURCE_NAME_EQUALS_AA, RESOURCE_ID_EQUALS_2L, user);

    IConcurrentDataAccessManager managerB =
        pool.getConcurrentDataAccessManager(RESOURCE_ROOT_EQUALS_B);
    managerB.lock(RESOURCE_NAME_EQUALS_BB, RESOURCE_ID_EQUALS_1L, user);
    managerB.lock(RESOURCE_NAME_EQUALS_BB, RESOURCE_ID_EQUALS_2L, user);

    List<LockDetails> locks = new ArrayList<LockDetails>();
    Collection<IConcurrentDataAccessManager> managers =
        pool.getAvailableConcurrentDataAccessManager();

    for (IConcurrentDataAccessManager someManager : managers) {
      locks.addAll(someManager.listAllLocks());
    }

    assertThat(locks.size(), is(equalTo(4)));
  }

  /**
   * Simple idea to handle sharing a flag between different threads, useful mainly for this test's
   * usage
   *
   * @author Hesham Saleh
   */
  public class Event extends Object {
    public String resourceRoot, resourceName, threadName, resourceCod;

    public Event(String resourceRoot, String resourceName, String resourceCod, String threadName) {
      this.resourceRoot = resourceRoot;
      this.resourceName = resourceName;
      this.resourceCod = resourceCod;
      this.threadName = threadName;
    }
  }

  public class ResourceLockedEvent extends Event {
    public ResourceLockedEvent(
        String resourceRoot, String resourceName, String resourceCode, String threadName) {
      super(resourceRoot, resourceName, resourceCode, threadName);
    }
  }

  public class ResourceAlreadyLockedExceptionEvent extends Event {

    public ResourceAlreadyLockedExceptionEvent(
        String resourceRoot, String resourceName, String resourceCode, String threadName) {
      super(resourceRoot, resourceName, resourceCode, threadName);
    }
  }

  public class AlreadyLockedBySameUserExceptionEvent extends Event {

    public AlreadyLockedBySameUserExceptionEvent(
        String resourceRoot, String resourceName, String resourceCode, String threadName) {
      super(resourceRoot, resourceName, resourceCode, threadName);
    }
  }
}
