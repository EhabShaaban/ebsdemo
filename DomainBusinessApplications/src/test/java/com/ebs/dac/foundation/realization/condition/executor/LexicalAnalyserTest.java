package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LexicalAnalyserTest {

  private LexicalAnalyser lexicalAnalyser;

  @Before
  public void setUp() {
    lexicalAnalyser = new LexicalAnalyser();
  }

  @Test
  public void createCondition_passSimpleConditionString_returnsPredicateObject() throws Exception {
    Predicate predicate = (Predicate) lexicalAnalyser.createConditionObject("[price = 10]");

    Assert.assertEquals(predicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) predicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(predicate.getOperator(), "=");
  }

  @Test
  public void
      createCondition_passCompositeSimpleConditionStringOfSimpleAndSimple_returnsCompositeObject()
          throws Exception {
    CompositePredicate compositePredicate =
        (CompositePredicate)
            lexicalAnalyser.createConditionObject("([price = 10] && [height > 110])");
    Predicate leftPredicate = (Predicate) compositePredicate.getLeftOperand();
    Predicate rightPredicate = (Predicate) compositePredicate.getRightOperand();

    Assert.assertEquals(leftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) leftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(leftPredicate.getOperator(), "=");

    Assert.assertEquals(compositePredicate.getOperator(), "&&");

    Assert.assertEquals(rightPredicate.getLeftOperand().getStringValue(), "height");
    Assert.assertEquals((long) rightPredicate.getRightOperand().getLongValue(), 110L);
    Assert.assertEquals(rightPredicate.getOperator(), ">");
  }

  @Test
  public void
      createCondition_passCompositeConditionStringOfCompositeAndSimple_returnsCompositeObject()
          throws Exception {
    CompositePredicate compositePredicate =
        (CompositePredicate)
            lexicalAnalyser.createConditionObject(
                "(([price = 10] && [height > 110]) || [price = 10])");

    CompositePredicate leftPredicate = (CompositePredicate) compositePredicate.getLeftOperand();
    Predicate compositeLeftPredicate = (Predicate) leftPredicate.getLeftOperand();
    Predicate compositeRightPredicate = (Predicate) leftPredicate.getRightOperand();

    Predicate rightPredicate = (Predicate) compositePredicate.getRightOperand();

    Assert.assertEquals(compositeLeftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) compositeLeftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(compositeLeftPredicate.getOperator(), "=");

    Assert.assertEquals(leftPredicate.getOperator(), "&&");

    Assert.assertEquals(compositeRightPredicate.getLeftOperand().getStringValue(), "height");
    Assert.assertEquals((long) compositeRightPredicate.getRightOperand().getLongValue(), 110L);
    Assert.assertEquals(compositeRightPredicate.getOperator(), ">");

    Assert.assertEquals(compositePredicate.getOperator(), "||");

    Assert.assertEquals(rightPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) rightPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(rightPredicate.getOperator(), "=");
  }

  @Test
  public void
      createCondition_passCompositeConditionStringOfSimpleAndComposite_returnsCompositeObject()
          throws Exception {
    CompositePredicate compositePredicate =
        (CompositePredicate)
            lexicalAnalyser.createConditionObject(
                "([price = 10] || ([price = 10] && [height > 110]) )");

    CompositePredicate rightPredicate = (CompositePredicate) compositePredicate.getRightOperand();
    Predicate compositeLeftPredicate = (Predicate) rightPredicate.getLeftOperand();
    Predicate compositeRightPredicate = (Predicate) rightPredicate.getRightOperand();

    Predicate leftPredicate = (Predicate) compositePredicate.getLeftOperand();

    Assert.assertEquals(compositeLeftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) compositeLeftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(compositeLeftPredicate.getOperator(), "=");

    Assert.assertEquals(rightPredicate.getOperator(), "&&");

    Assert.assertEquals(compositeRightPredicate.getLeftOperand().getStringValue(), "height");
    Assert.assertEquals((long) compositeRightPredicate.getRightOperand().getLongValue(), 110L);
    Assert.assertEquals(compositeRightPredicate.getOperator(), ">");

    Assert.assertEquals(compositePredicate.getOperator(), "||");

    Assert.assertEquals(leftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) leftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(leftPredicate.getOperator(), "=");
  }

  @Test
  public void
      createCondition_passCompositeConditionStringOfCompositeAndComposite_returnsCompositeObject()
          throws Exception {
    CompositePredicate compositePredicate =
        (CompositePredicate)
            lexicalAnalyser.createConditionObject(
                "(([price = 10] && [height > 110]) || ([price = 10] && [height > 110]) )");

    CompositePredicate leftPredicate = (CompositePredicate) compositePredicate.getLeftOperand();
    Predicate leftCompositeLeftPredicate = (Predicate) leftPredicate.getLeftOperand();
    Predicate leftCompositeRightPredicate = (Predicate) leftPredicate.getRightOperand();

    CompositePredicate rightPredicate = (CompositePredicate) compositePredicate.getRightOperand();
    Predicate rightCompositeLeftPredicate = (Predicate) rightPredicate.getLeftOperand();
    Predicate rightCompositeRightPredicate = (Predicate) rightPredicate.getRightOperand();

    Assert.assertEquals(leftCompositeLeftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) leftCompositeLeftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(leftCompositeLeftPredicate.getOperator(), "=");

    Assert.assertEquals(leftPredicate.getOperator(), "&&");

    Assert.assertEquals(leftCompositeRightPredicate.getLeftOperand().getStringValue(), "height");
    Assert.assertEquals((long) leftCompositeRightPredicate.getRightOperand().getLongValue(), 110L);
    Assert.assertEquals(leftCompositeRightPredicate.getOperator(), ">");

    Assert.assertEquals(rightCompositeLeftPredicate.getLeftOperand().getStringValue(), "price");
    Assert.assertEquals((long) rightCompositeLeftPredicate.getRightOperand().getLongValue(), 10L);
    Assert.assertEquals(rightCompositeLeftPredicate.getOperator(), "=");

    Assert.assertEquals(leftPredicate.getOperator(), "&&");

    Assert.assertEquals(rightCompositeRightPredicate.getLeftOperand().getStringValue(), "height");
    Assert.assertEquals((long) rightCompositeRightPredicate.getRightOperand().getLongValue(), 110L);
    Assert.assertEquals(rightCompositeRightPredicate.getOperator(), ">");

    Assert.assertEquals(compositePredicate.getOperator(), "||");
  }
}
