package com.ebs.dac.foundation.realization.condition.executor;
/*
 Created by Mohamed Adel && Hossam Hassan  on 8/2/2018.
*/

import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CompositePredicateTest {

  private Map<String, Object> map;

  @Before
  public void setUp() {
    map = new HashMap<>();
  }

  private Predicate createPredicate(String variableName, Double value) {
    Operand leftOperand = new Operand();
    Operand rightOperand = new Operand();
    Predicate predicate = new Predicate();

    leftOperand.setStringValue(variableName);
    predicate.setLeftOperand(leftOperand);

    rightOperand.setValue(value);
    predicate.setRightOperand(rightOperand);

    predicate.setOperator("=");
    return predicate;
  }

  private <Predicate extends IPredicate> CompositePredicate createCompositePredicate(
      Predicate leftGeneralPredicate, Predicate rightGeneralPredicate, String operator) {
    CompositePredicate compositePredicate = new CompositePredicate();
    compositePredicate.setLeftOperand((IOperand) leftGeneralPredicate);
    compositePredicate.setRightOperand((IOperand) rightGeneralPredicate);
    compositePredicate.setOperator(operator);
    return compositePredicate;
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateAndValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");
    Assert.assertTrue(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateAndInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 11.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");
    Assert.assertFalse(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateAndValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");
    Assert.assertFalse(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateAndInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 11.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");
    Assert.assertFalse(compositePredicate.execute(map).isResult());
  }

  @Test
  public void executeCompositePredicate_PassValidSimplePredicateOrValidSimplePredicate_ReturnsTrue()
      throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "||");
    Assert.assertTrue(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateOrInValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 11.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "||");
    Assert.assertTrue(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateOrValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "||");
    Assert.assertTrue(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateOrInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);
    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 11.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "||");
    Assert.assertFalse(compositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateAndValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "&&");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateAndInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateAndValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateAndInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateOrValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidSimplePredicateOrInValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateOrValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidSimplePredicateOrInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(validPredicate, compositePredicate, "||");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateAndValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "&&");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateAndValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateAndInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateAndInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateOrValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateOrValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 10.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateOrInValidSimplePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateOrInValidSimplePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validPredicate = createPredicate("price", 11.0);

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, validPredicate, "||");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateAndValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 10.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "&&");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateAndInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 11.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateAndValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 10.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateAndInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 11.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "&&");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateOrValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 10.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassValidCompositePredicateOrInValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 10.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 11.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateOrValidCompositePredicate_ReturnsTrue()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 10.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "||");

    Assert.assertTrue(containerCompositePredicate.execute(map).isResult());
  }

  @Test
  public void
      executeCompositePredicate_PassInValidCompositePredicateOrInValidCompositePredicate_ReturnsFalse()
          throws Exception {
    map.put("price", 10.0);

    Predicate validLeftPredicate = createPredicate("price", 11.0);
    Predicate validRightPredicate = createPredicate("price", 10.0);
    CompositePredicate compositePredicate =
        createCompositePredicate(validLeftPredicate, validRightPredicate, "&&");

    Predicate validLeftPredicate2 = createPredicate("price", 11.0);
    Predicate validRightPredicate2 = createPredicate("price", 10.0);
    CompositePredicate compositePredicate2 =
        createCompositePredicate(validLeftPredicate2, validRightPredicate2, "&&");

    CompositePredicate containerCompositePredicate =
        createCompositePredicate(compositePredicate, compositePredicate2, "||");

    Assert.assertFalse(containerCompositePredicate.execute(map).isResult());
  }
}
