package com.ebs.dac.foundation.realization.processing.fakeobjects;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import com.ebs.dac.dbo.processing.BasicAttribute;
import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dac.dbo.processing.EmbeddedAttribute;
import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 5, 2018 9:59:13 AM
 */
public class DataObjectProcessorFakeObjects {

  public interface IFakeEmbeddedAttribute {
    String SIMPLE_NESTED_ATTR_NAME = "simpleNestedAttr";
    BasicAttribute simpleNestedAttrAttr =
        new BasicAttribute( SIMPLE_NESTED_ATTR_NAME);

    String LOCALIZED_TEXT_NESTED_ATTR_NAME = "localizedTextNestedAttr";
    LocalizedTextAttribute<LocalizedString> localizedTextNestedAttrAttr =
        new LocalizedTextAttribute<LocalizedString>(
            LOCALIZED_TEXT_NESTED_ATTR_NAME);
  }

  public interface IFakeNonAggregateObject {

    String SYS_NAME = "FakeSimpleObject";

    String SIMPLE_ATTR_NAME = "simpleAttr";
    BasicAttribute simpleAttrAttr = new BasicAttribute( SIMPLE_ATTR_NAME);

    String SIMPLE_ATTR2_NAME = "simpleAttr2";
    BasicAttribute simpleAttr2Attr = new BasicAttribute( SIMPLE_ATTR2_NAME);

    String LOCALIZED_TEXT_ATTR_NAME = "localizedTextAttr";
    LocalizedTextAttribute<LocalizedString> localizedTextAttrAttr =
        new LocalizedTextAttribute<LocalizedString>(
            LOCALIZED_TEXT_ATTR_NAME);

    String FAKE_EMBEDDED_ATTR_NAME = "fakeEmbeddedAttr";
    EmbeddedAttribute<FakeEmbeddedAttribute> fakeEmbeddedAttrAttr =
        new EmbeddedAttribute<FakeEmbeddedAttribute>(
            FAKE_EMBEDDED_ATTR_NAME, FakeEmbeddedAttribute.class);
  }

  public interface IFakeAggregateObject {

    String FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME = "fakeNonAggregateSinglePayloadAttr";
    CompositeAttribute<FakeNonAggregateSinglePayload> fakeNonAggregateSinglePayloadAttrAttr =
        new CompositeAttribute<FakeNonAggregateSinglePayload>(
            FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME,
            FakeNonAggregateSinglePayload.class,
            false);

    String FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME = "fakeNonAggregateMultiplePayloadAttr";
    CompositeAttribute<FakeNonAggregateMultiplePayload> fakeNonAggregateMultiplePayloadAttrAttr =
        new CompositeAttribute<FakeNonAggregateMultiplePayload>(
            FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME,
            FakeNonAggregateMultiplePayload.class,
            true);
  }

  public interface IFakeNonAggregateSinglePayload extends IInformationObject {

    String SYS_NAME = "FakeNonAggregateSinglePayload";

    String SIMPLE_SINGLE_PAYLOAD_ATTR_NAME = "simpleSinglePayloadAttr";
    BasicAttribute simpleSinglePayloadAttrAttr =
        new BasicAttribute( SIMPLE_SINGLE_PAYLOAD_ATTR_NAME);

    String SIMPLE_SINGLE_PAYLOAD_ATTR2_NAME = "simpleSinglePayloadAttr2";
    BasicAttribute simpleSinglePayloadAttr2Attr =
        new BasicAttribute(SIMPLE_SINGLE_PAYLOAD_ATTR2_NAME);

    String LOCALIZED_TEXT_SINGLE_PAYLOAD_ATTR_NAME = "localizedTextSinglePayloadAttr";
    LocalizedTextAttribute<LocalizedString> localizedTextSinglePayloadAttrAttr =
        new LocalizedTextAttribute<LocalizedString>(
            LOCALIZED_TEXT_SINGLE_PAYLOAD_ATTR_NAME);

    String FAKE_EMBEDDED_SINGLE_PAYLOAD_ATTR_NAME = "fakeEmbeddedSinglePayloadAttr";
    EmbeddedAttribute<FakeEmbeddedAttribute> fakeEmbeddedSinglePayloadAttrAttr =
        new EmbeddedAttribute<FakeEmbeddedAttribute>(
            FAKE_EMBEDDED_SINGLE_PAYLOAD_ATTR_NAME,
            FakeEmbeddedAttribute.class);
  }

  public interface IFakeNonAggregateMultiplePayload extends IInformationObject {

    String SYS_NAME = "FakeNonAggregateMultiplePayload";

    String SIMPLE_MULTIPLE_PAYLOAD_ATTR_NAME = "simpleMultiplePayloadAttr";
    BasicAttribute simpleMultiplePayloadAttrAttr =
        new BasicAttribute( SIMPLE_MULTIPLE_PAYLOAD_ATTR_NAME);

    String SIMPLE_MULTIPLE_PAYLOAD_ATTR2_NAME = "simpleMultiplePayloadAttr2";
    BasicAttribute simpleMultiplePayloadAttr2Attr =
        new BasicAttribute( SIMPLE_MULTIPLE_PAYLOAD_ATTR2_NAME);

    String LOCALIZED_TEXT_MULTIPLE_PAYLOAD_ATTR_NAME = "localizedTextMultiplePayloadAttr";
    LocalizedTextAttribute<LocalizedString> localizedTextMultiplePayloadAttrAttr =
        new LocalizedTextAttribute<LocalizedString>(
            LOCALIZED_TEXT_MULTIPLE_PAYLOAD_ATTR_NAME);

    String FAKE_EMBEDDED_MULTIPLE_PAYLOAD_ATTR_NAME = "fakeEmbeddedMultiplePayloadAttr";
    EmbeddedAttribute<FakeEmbeddedAttribute> fakeEmbeddedMultiplePayloadAttrAttr =
        new EmbeddedAttribute<FakeEmbeddedAttribute>(
            FAKE_EMBEDDED_MULTIPLE_PAYLOAD_ATTR_NAME,
            FakeEmbeddedAttribute.class);
  }

  public interface IFakeThreeLevelAggregateObject {

    String FAKE_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME = "fakeAggregateSinglePayloadAttr";
    CompositeAttribute<FakeAggregateSinglePayload> fakeAggregateSinglePayloadAttrAttr =
        new CompositeAttribute<FakeAggregateSinglePayload>(
            FAKE_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME,
            FakeAggregateSinglePayload.class,
            false);

    String FAKE_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME = "fakeAggregateMultiplePayloadAttr";
    CompositeAttribute<FakeAggregateMultiplePayload> fakeAggregateMultiplePayloadAttrAttr =
        new CompositeAttribute<FakeAggregateMultiplePayload>(
            FAKE_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME,
            FakeAggregateMultiplePayload.class,
            true);
  }

  public interface IFakeAggregateSinglePayload extends IInformationObject {

    String SIMPLE_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME = "simpleAggregateSinglePayloadAttr";
    BasicAttribute simpleAggregateSinglePayloadAttrAttr =
        new BasicAttribute(SIMPLE_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME);

    String FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME = "fakeNonAggregateSinglePayloadAttr";
    CompositeAttribute<FakeNonAggregateSinglePayload> fakeNonAggregateSinglePayloadAttrAttr =
        new CompositeAttribute<FakeNonAggregateSinglePayload>(
            FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME,
            FakeNonAggregateSinglePayload.class,
            false);

    String FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME = "fakeNonAggregateMultiplePayloadAttr";
    CompositeAttribute<FakeNonAggregateMultiplePayload> fakeNonAggregateMultiplePayloadAttrAttr =
        new CompositeAttribute<FakeNonAggregateMultiplePayload>(
            FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME,
            FakeNonAggregateMultiplePayload.class,
            true);
  }

  public interface IFakeAggregateMultiplePayload extends IInformationObject {}

  @EntityInterface(IFakeEmbeddedAttribute.class)
  public static class FakeEmbeddedAttribute {

    private Integer simpleNestedAttr;

    @com.ebs.dac.dbo.api.annotations.EmbeddedAttribute
    private LocalizedString localizedTextNestedAttr;
  }

  @EntityInterface(IFakeNonAggregateObject.class)
  public static class FakeNonAggregateObject extends BusinessObject {

    private static final long serialVersionUID = 1L;

    private Integer simpleAttr;

    private String simpleAttr2;

    @com.ebs.dac.dbo.api.annotations.EmbeddedAttribute private LocalizedString localizedTextAttr;

    @com.ebs.dac.dbo.api.annotations.EmbeddedAttribute
    private FakeEmbeddedAttribute fakeEmbeddedAttr;

    @Override
    public String getSysName() {
      return IFakeNonAggregateObject.SYS_NAME;
    }

    public Integer getSimpleAttr() {
      return simpleAttr;
    }

    public void setSimpleAttr(Integer simpleAttr) {
      this.simpleAttr = simpleAttr;
    }

    public String getSimpleAttr2() {
      return simpleAttr2;
    }

    public void setSimpleAttr2(String simpleAttr2) {
      this.simpleAttr2 = simpleAttr2;
    }

    public LocalizedString getLocalizedTextAttr() {
      return localizedTextAttr;
    }

    public void setLocalizedTextAttr(LocalizedString localizedTextAttr) {
      this.localizedTextAttr = localizedTextAttr;
    }

    public FakeEmbeddedAttribute getFakeEmbeddedAttr() {
      return fakeEmbeddedAttr;
    }

    public void setFakeEmbeddedAttr(FakeEmbeddedAttribute fakeEmbeddedAttr) {
      this.fakeEmbeddedAttr = fakeEmbeddedAttr;
    }
  }

  @EntityInterface(IFakeAggregateObject.class)
  public static class FakeAggregateObject extends BusinessObject implements IAggregateAwareEntity {

    private static final long serialVersionUID = 1L;
    private AggregateBusinessObject aggregateBusinessObject;

    @Override
    public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
      if (aggregateBusinessObject == null) {
        aggregateBusinessObject = new AggregateBusinessObject();
      }
      return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
    }

    @Override
    public String getSysName() {
      return null;
    }
  }

  @EntityInterface(IFakeNonAggregateSinglePayload.class)
  public static class FakeNonAggregateSinglePayload extends InformationObject {

    private static final long serialVersionUID = 1L;

    private Integer simpleSinglePayloadAttr;

    private String simpleSinglePayloadAttr2;

    @com.ebs.dac.dbo.api.annotations.EmbeddedAttribute
    private LocalizedString localizedTextSinglePayloadAttr;

    @com.ebs.dac.dbo.api.annotations.EmbeddedAttribute
    private FakeEmbeddedAttribute fakeEmbeddedSinglePayloadAttr;

    @Override
    public String getSysName() {
      return IFakeNonAggregateSinglePayload.SYS_NAME;
    }
  }

  @EntityInterface(IFakeNonAggregateMultiplePayload.class)
  public static class FakeNonAggregateMultiplePayload extends InformationObject {

    private static final long serialVersionUID = 1L;

    @Override
    public String getSysName() {
      return IFakeNonAggregateSinglePayload.SYS_NAME;
    }
  }

  @EntityInterface(IFakeThreeLevelAggregateObject.class)
  public static class FakeThreeLevelAggregateObject extends BusinessObject
      implements IAggregateAwareEntity {

    private static final long serialVersionUID = 1L;
    private AggregateBusinessObject aggregateBusinessObject;

    @Override
    public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
      if (aggregateBusinessObject == null) {
        aggregateBusinessObject = new AggregateBusinessObject();
      }
      return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
    }

    @Override
    public String getSysName() {
      return null;
    }
  }

  @EntityInterface(IFakeAggregateSinglePayload.class)
  public static class FakeAggregateSinglePayload extends InformationObject
      implements IAggregateAwareEntity {

    private static final long serialVersionUID = 1L;

    private AggregateBusinessObject aggregateBusinessObject;

    @Override
    public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
      if (aggregateBusinessObject == null) {
        aggregateBusinessObject = new AggregateBusinessObject();
      }
      return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
    }

    @Override
    public String getSysName() {
      return null;
    }
  }

  public static class FakeAggregateMultiplePayload extends InformationObject
      implements IAggregateAwareEntity {

    private static final long serialVersionUID = 1L;

    private AggregateBusinessObject aggregateBusinessObject;

    @Override
    public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
      if (aggregateBusinessObject == null) {
        aggregateBusinessObject = new AggregateBusinessObject();
      }
      return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
    }

    @Override
    public String getSysName() {
      return null;
    }
  }
}
