package com.ebs.dac.foundation.realization.processing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.FakeAggregateObject;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.FakeNonAggregateMultiplePayload;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.FakeNonAggregateObject;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.FakeNonAggregateSinglePayload;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.IFakeAggregateObject;
import com.ebs.dac.foundation.realization.processing.fakeobjects.DataObjectProcessorFakeObjects.IFakeNonAggregateObject;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.junit.Test;

/* getAttributeValue ,setAttributeValue aren't
 * tested as they are covered in other data processors test classes.*/

/////////////////////////////////

public class DataObjectProcessorTest {

  private static final Logger LOGGER = Logger.getLogger(DataObjectProcessorTest.class.getName());

  private BusinessObject targetEntity;

  private FakeNonAggregateSinglePayload nonAggregateSinglePayload;
  private FakeNonAggregateMultiplePayload nonAggregateMultiplePayload;

  private Set<String> targetAttributesSet;
  private DataObjectProcessor processor;
  private Map<String, Object> result;

  @Test
  public void getAttributesValuesMap_NonAggregateEntityAndEmptyAttributeSet_ReturnEmptyResult()
      throws Exception {
    whenNonAggregateEntity().whenTargetAttributesSetIsEmpty().assertThatResultIsEmpty();
  }

  @Test
  public void getAttributesValuesMap_NonAggregateEntityAndNonExistAttributeSet_ReturnEmptyResult()
      throws Exception {
    whenNonAggregateEntity().whenNonExistAttributeAsTarget().assertThatResultIsEmpty();
  }

  @Test
  public void
      getAttributesValuesMap_NonAggregateEntityAndLocalizedTextAttributeSet_ReturnTargetLocalizedTextAttribute()
          throws Exception {
    whenNonAggregateEntity()
        .withLocalizedTextAttributesAsTarget()
        .assertThatTargetLocalizedTextAttributesAreReturned();
  }

  @Test
  public void
      getAttributesValuesMap_AggregateEntityAndSingleCompositeAttributeSet_ReturnTargetSingleCompositeAttribute()
          throws Exception {
    whenAggregateEntity()
        .withNonAggregateSinglePayload()
        .withSingleCompositeAttributeAsTarget()
        .assertThatTargetSingleCompositeAttributesIsReturned();
  }

  @Test
  public void
      getAttributesValuesMap_AggregateEntityAndMultipleCompositeAttributeSet_ReturnTargetMultipleCompositeAttribute()
          throws Exception {
    whenAggregateEntity()
        .withNonAggregateMultiplePayload()
        .withMultipleCompositeAttributeAsTarget()
        .assertThatTargetMultipleCompositeAttributesAreReturned();
  }

  private DataObjectProcessorTest whenNonAggregateEntity() {
    targetEntity = new FakeNonAggregateObject();
    return this;
  }

  private DataObjectProcessorTest whenAggregateEntity() {
    targetEntity = new FakeAggregateObject();
    return this;
  }


  private DataObjectProcessorTest withNonAggregateSinglePayload() throws Exception {
    nonAggregateSinglePayload = new FakeNonAggregateSinglePayload();

    ((IAggregateAwareEntity) targetEntity)
        .aggregateBusinessObject()
        .putSingleInstanceAttribute(
            IFakeAggregateObject.fakeNonAggregateSinglePayloadAttrAttr, nonAggregateSinglePayload);

    return this;
  }

  private DataObjectProcessorTest withNonAggregateMultiplePayload() throws Exception {
    nonAggregateMultiplePayload = new FakeNonAggregateMultiplePayload();

    List<FakeNonAggregateMultiplePayload> multiplePayload = new ArrayList<>();
    multiplePayload.add(nonAggregateMultiplePayload);

    ((IAggregateAwareEntity) targetEntity)
        .aggregateBusinessObject()
        .putMultipleInstancesAttribute(
            IFakeAggregateObject.fakeNonAggregateMultiplePayloadAttrAttr, multiplePayload);

    return this;
  }

  private DataObjectProcessorTest whenTargetAttributesSetIsEmpty() {
    targetAttributesSet = new HashSet<>();
    return this;
  }

  private DataObjectProcessorTest whenNonExistAttributeAsTarget() {
    targetAttributesSet = new HashSet<>(Arrays.asList("NonExistAttribute"));
    return this;
  }

  private DataObjectProcessorTest withLocalizedTextAttributesAsTarget() {
    LocalizedString localizedTextAttr = new LocalizedString();
    localizedTextAttr.setValue(ISysDefaultLocales.ENGLISH_LOCALE, "english");
    localizedTextAttr.setValue(ISysDefaultLocales.ARABIC_LOCALE, "عربي");

    ((FakeNonAggregateObject) targetEntity).setLocalizedTextAttr(localizedTextAttr);

    targetAttributesSet =
        new HashSet<>(Arrays.asList(IFakeNonAggregateObject.LOCALIZED_TEXT_ATTR_NAME));
    return this;
  }

  private DataObjectProcessorTest withSingleCompositeAttributeAsTarget() {
    targetAttributesSet =
        new HashSet<>(
            Arrays.asList(IFakeAggregateObject.FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME));
    return this;
  }

  private DataObjectProcessorTest withMultipleCompositeAttributeAsTarget() {
    targetAttributesSet =
        new HashSet<>(
            Arrays.asList(IFakeAggregateObject.FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME));
    return this;
  }

  private DataObjectProcessorTest assertThatResultIsEmpty() throws Exception {
    processor = new DataObjectProcessor(targetEntity);
    result = processor.getAttributesValuesMap(targetEntity, targetAttributesSet);
    assertTrue(result.isEmpty());
    return this;
  }

  private DataObjectProcessorTest assertThatTargetLocalizedTextAttributesAreReturned()
      throws Exception {
    processor = new DataObjectProcessor(targetEntity);
    result = processor.getAttributesValuesMap(targetEntity, targetAttributesSet);

    LocalizedString targetLocalizedString =
        (LocalizedString) result.get(IFakeNonAggregateObject.LOCALIZED_TEXT_ATTR_NAME);

    assertEquals(targetLocalizedString.getValue(ISysDefaultLocales.ENGLISH_LOCALE), "english");
    assertEquals(targetLocalizedString.getValue(ISysDefaultLocales.ARABIC_LOCALE), "عربي");
    return this;
  }

  private DataObjectProcessorTest assertThatTargetSingleCompositeAttributesIsReturned()
      throws Exception {
    processor = new DataObjectProcessor(targetEntity);
    result = processor.getAttributesValuesMap(targetEntity, targetAttributesSet);

    assertNotNull(result.get(IFakeAggregateObject.FAKE_NON_AGGREGATE_SINGLE_PAYLOAD_ATTR_NAME));

    return this;
  }

  private DataObjectProcessorTest assertThatTargetMultipleCompositeAttributesAreReturned()
      throws Exception {
    processor = new DataObjectProcessor(targetEntity);
    result = processor.getAttributesValuesMap(targetEntity, targetAttributesSet);

    assertNotNull(result.get(IFakeAggregateObject.FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME));
    assertTrue(
        Collection.class.isAssignableFrom(
            result
                .get(IFakeAggregateObject.FAKE_NON_AGGREGATE_MULTIPLE_PAYLOAD_ATTR_NAME)
                .getClass()));

    return this;
  }
}
