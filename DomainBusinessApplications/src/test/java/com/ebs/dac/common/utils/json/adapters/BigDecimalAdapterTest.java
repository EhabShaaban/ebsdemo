package com.ebs.dac.common.utils.json.adapters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class BigDecimalAdapterTest {
  private TypeAdapter<BigDecimal> bigDecimalTypeAdapter;
  @Before
  public void initAdapter() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapter(BigDecimal.class, new BigDecimalAdapter());
    Gson gson = gsonBuilder.create();
    bigDecimalTypeAdapter = gson.getAdapter(BigDecimal.class);
  }

  @Test
  public void Should_returnIntegerBigDecimalWithZeroAddedAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("1"));
    assertJsonString(jsonValue, "1.0");
  }

  @Test
  public void Should_returnIntegerBigDecimalThatHasAlotOfPaddedZeroesWithSingleZeroAddedAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("1.0000000"));
    assertJsonString(jsonValue, "1.0");
  }

  @Test
  public void Should_returnPositiveBigDecimalValueAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("1.1234567899876543211112482784728472"));
    assertJsonString(jsonValue, "1.1234567899876543211112482784728472");
  }

  @Test
  public void Should_returnPositiveBigDecimalValueForNumberWithZeroesAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("100"));
    assertJsonString(jsonValue, "100.0");
  }

  @Test
  public void Should_returnNegativeBigDecimalValueAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("-1.12345678998765432100"));
    assertJsonString(jsonValue, "-1.12345678998765432100");
  }

  @Test
  public void Should_returnZeroBigDecimalValueAsAString() {
    String jsonValue = bigDecimalTypeAdapter.toJson(new BigDecimal("0"));
    assertJsonString(jsonValue, "0.0");
  }

  @Test
  public void Should_returnEmptyStringForNullBigDecimalAsAString() {
    BigDecimal value = null;
    String jsonValue = bigDecimalTypeAdapter.toJson(value);
    Assert.isEqual(
            jsonValue.compareTo("null"), 0, "Converted value doesn't match");
  }

  private void assertJsonString(String actual, String expected) {
    Assert.isEqual(
            actual.compareTo("\"" + expected + "\""), 0, "Converted value doesn't match");
  }
}
