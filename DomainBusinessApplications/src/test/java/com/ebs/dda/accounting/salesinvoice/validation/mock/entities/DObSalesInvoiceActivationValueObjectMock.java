package com.ebs.dda.accounting.salesinvoice.validation.mock.entities;

public class DObSalesInvoiceActivationValueObjectMock {

  private String documentCode;

  public String getDocumentCode() {
    return documentCode;
  }

  public void setDocumentCode(String documentCode) {
    this.documentCode = documentCode;
  }
}
