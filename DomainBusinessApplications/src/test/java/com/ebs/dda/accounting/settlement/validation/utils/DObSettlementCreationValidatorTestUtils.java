package com.ebs.dda.accounting.settlement.validation.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.settlement.validation.DObSettlementCreationValidator;
import com.ebs.dda.accounting.settlement.validation.mocks.entities.DObSettlementCreationValueObjectMock;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObSettlementCreationValidatorTestUtils {

	private DObSettlementCreationValueObjectMock valueObject;
	private DObSettlementCreationValidator validator;


	private CObPurchasingUnitRep businessUnitRep;
	private CObCompanyRep companyRep;
	private CObCurrencyRep currencyRep;
	private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

	public DObSettlementCreationValidatorTestUtils() {
		valueObject = new DObSettlementCreationValueObjectMock();
		validator = new DObSettlementCreationValidator();

		businessUnitRep = Mockito.mock(CObPurchasingUnitRep.class);
		companyRep = Mockito.mock(CObCompanyRep.class);
		currencyRep = Mockito.mock(CObCurrencyRep.class);
		documentOwnerGeneralModelRep = Mockito.mock(DocumentOwnerGeneralModelRep.class);
	}

	public DObSettlementCreationValidatorTestUtils withType(String type) {
		valueObject.setType(type);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils withBusinessUnitCode(String businessUnitCode) {
		valueObject.setBusinessUnitCode(businessUnitCode);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils withCompanyCode(String companyCode) {
		valueObject.setCompanyCode(companyCode);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils withCurrencyCode(String currencyCode) {
		valueObject.setCurrencyCode(currencyCode);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils withDocumentOwnerId(Long documentOwnerId) {
		valueObject.setDocumentOwnerId(documentOwnerId);
		return this;
	}

	public void assertThrowsArgumentSecurityException() {
		assertThrows(ArgumentViolationSecurityException.class, () -> {
			validator.validate(valueObject);
		});
	}

	public DObSettlementCreationValidatorTestUtils whenBusinessUnitDoesNotExist() {
		mockCompany();
		mockCurrency();
		mockDocumentOwner();
		Mockito.when(businessUnitRep.findOneByUserCode(valueObject.getBusinessUnitCode()))
				.thenReturn(null);
		validator.setBusinessUnitRep(businessUnitRep);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils whenCompanyDoesNotExist() {
		mockBusinessUnit();
		mockCurrency();
		mockDocumentOwner();
		Mockito.when(companyRep.findOneByUserCode(valueObject.getCompanyCode()))
				.thenReturn(null);
		validator.setCompanyRep(companyRep);

		return this;
	}

	public DObSettlementCreationValidatorTestUtils whenCurrencyDoesNotExist() {
		mockBusinessUnit();
		mockCompany();
		mockDocumentOwner();
		Mockito.when(currencyRep.findOneByUserCode(valueObject.getCurrencyCode()))
				.thenReturn(null);
		validator.setCurrencyRep(currencyRep);
		return this;
	}

	public DObSettlementCreationValidatorTestUtils whenDocumentOwnerDoesNotExist() {
		mockBusinessUnit();
		mockCompany();
		mockCurrency();
		Mockito.when(documentOwnerGeneralModelRep.findOneByUserIdAndObjectName(valueObject.getDocumentOwnerId(), IDObSettlement.SYS_NAME))
				.thenReturn(null);
		validator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
		return this;
	}

	private void mockBusinessUnit(){
		Mockito.when(businessUnitRep.findOneByUserCode(valueObject.getBusinessUnitCode()))
				.thenReturn(new CObPurchasingUnit());
		validator.setBusinessUnitRep(businessUnitRep);
	}
	private void mockCompany(){
		Mockito.when(companyRep.findOneByUserCode(valueObject.getCompanyCode()))
				.thenReturn(new CObCompany());
		validator.setCompanyRep(companyRep);

	}
	private void mockCurrency(){
		Mockito.when(currencyRep.findOneByUserCode(valueObject.getCurrencyCode()))
				.thenReturn(new CObCurrency());
		validator.setCurrencyRep(currencyRep);
	}
	private void mockDocumentOwner(){
		Mockito.when(documentOwnerGeneralModelRep.findOneByUserIdAndObjectName(valueObject.getDocumentOwnerId(), IDObSettlement.SYS_NAME))
				.thenReturn(new DocumentOwnerGeneralModel());
		validator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
	}
}
