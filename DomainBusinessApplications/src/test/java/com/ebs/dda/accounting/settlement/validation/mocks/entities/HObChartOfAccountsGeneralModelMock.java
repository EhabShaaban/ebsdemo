package com.ebs.dda.accounting.settlement.validation.mocks.entities;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;

public class HObChartOfAccountsGeneralModelMock extends HObChartOfAccountsGeneralModel {

  private String leadger;
  private String userCode;
  private String creditDebit;

  @Override
  public String getLeadger() {
    return leadger;
  }

  public void setLeadger(String leadger) {
    this.leadger = leadger;
  }

  @Override
  public void setUserCode(String userCode) {
    this.userCode = userCode;
  }

  @Override
  public String getCreditDebit() {
    return creditDebit;
  }

  public void setCreditDebit(String creditDebit) {
    this.creditDebit = creditDebit;
  }
}
