package com.ebs.dda.accounting.settlement.validation.mocks.entities;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;

import java.math.BigDecimal;

public class IObSettlementAccountDetailsGeneralModelMock extends IObSettlementAccountDetailsGeneralModel {
  private String documentCode;
  private String accountingEntry;
  private BigDecimal amount;

  @Override
  public String getDocumentCode() {
    return documentCode;
  }

  public void setDocumentCode(String documentCode) {
    this.documentCode = documentCode;
  }

  @Override
  public String getAccountingEntry() {
    return accountingEntry;
  }

  public void setAccountingEntry(String accountingEntry) {
    this.accountingEntry = accountingEntry;
  }

  @Override
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
