package com.ebs.dda.accounting.settlement.validation;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import com.ebs.dda.accounting.settlement.validation.utils.IObSettlementAddAccountingDetailsValidatorTestUtils;

@RunWith(JUnitPlatform.class)
public class IObSettlementAddAccountingDetailsValidatorTests {

  private IObSettlementAddAccountingDetailsValidatorTestUtils utils;

  @BeforeEach
  public void init() {
    utils = new IObSettlementAddAccountingDetailsValidatorTestUtils();
  }

  private Map<String, String> settlementCodeValuesMap =
      new HashMap<String, String>() {
        {
          put("Null Value", null);
          put("Empty String", "");
          put("malicious", "2222222222");
        }
      };
  private Map<String, String> glAccountCodeValuesMap =
      new HashMap<String, String>() {
        {
          put("Null Value", null);
          put("Empty String", "");
          put("malicious", "22222");
        }
      };

  @TestFactory
  public Stream<DynamicTest>
      Should_ThrowsArgumentSecurityException_When_SettlementIsMissingOrNotExist() {
    return settlementCodeValuesMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String codeValue = settlementCodeValuesMap.get(key);
                      utils
                          .withCode(codeValue)
                          .withGLAccountCode("10221")
                          .withGLAccountCreditDebit("DEBIT")
                          .withGLSubAccountCode("000002")
                          .withAccountType("DEBIT")
                          .withAmount(BigDecimal.valueOf(2000))
                          .whenSettlementNotExist()
                          .assertThrowsArgumentSecurityException();
                    }));
  }

  @TestFactory
  public Stream<DynamicTest>
      Should_ThrowsArgumentSecurityException_When_GLAccountIsMissingOrNotExist() {
    return glAccountCodeValuesMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String codeValue = glAccountCodeValuesMap.get(key);
                      utils
                          .withCode("2021000001")
                          .withGLAccountCode(codeValue)
                          .withGLAccountCreditDebit("DEBIT")
                          .withGLSubAccountCode("000002")
                          .withAccountType("DEBIT")
                          .withAmount(BigDecimal.valueOf(2000))
                          .whenGlAccountNotExist()
                          .assertThrowsArgumentSecurityException();
                    }));
  }

  @Test
  public void
      Should_ThrowsArgumentSecurityException_When_GLAccountHasLedgerAndValueObjectDoesntHasGLSubAccount() {
    utils
        .withCode("2021000001")
        .withGLAccountCode("10221")
        .withGLAccountCreditDebit("DEBIT")
        .withGLSubAccountCode(null)
        .withAccountType("DEBIT")
        .withAmount(BigDecimal.valueOf(2000))
        .withLedger("Local_Vendors")
        .whenGlAccountHasLedgerAndValueObjectDoesntHasGLSubAccount()
        .assertThrowsArgumentSecurityException();
  }

  @Test
  public void
      Should_ThrowsArgumentSecurityException_When_GLAccountDoesntHasLedgerAndValueObjectHasGLSubAccount() {
    utils
        .withCode("2021000001")
        .withGLAccountCode("10221")
        .withGLAccountCreditDebit("DEBIT")
        .withGLSubAccountCode("000002")
        .withAccountType("DEBIT")
        .withAmount(BigDecimal.valueOf(2000))
        .withLedger(null)
        .whenGlAccountDoesntHasLedgerAndValueObjectHasGLSubAccount()
        .assertThrowsArgumentSecurityException();
  }

  @Test
  public void Should_ThrowsArgumentSecurityException_When_IsNotValidSubAccount() {
    utils
        .withCode("2021000001")
        .withGLAccountCode("10221")
        .withGLAccountCreditDebit("DEBIT")
        .withGLSubAccountCode("000002")
        .withAccountType("DEBIT")
        .withAmount(BigDecimal.valueOf(2000))
        .withLedger("Local_Vendors")
        .whenIsNotValidSubAccount()
        .assertThrowsArgumentSecurityException();
  }
}
