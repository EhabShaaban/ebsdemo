package com.ebs.dda.accounting.settlement.validation.mocks.entities;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;

public class IObSettlementDetailsGeneralModelMock extends IObSettlementDetailsGeneralModel {
    private String settlementCode;
    private String currencyCode;
    private String currencyISO;

    @Override
    public String getSettlementCode() {
        return settlementCode;
    }

    public void setSettlementCode(String settlementCode) {
        this.settlementCode = settlementCode;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String getCurrencyISO() {
        return currencyISO;
    }

    public void setCurrencyISO(String currencyISO) {
        this.currencyISO = currencyISO;
    }
}
