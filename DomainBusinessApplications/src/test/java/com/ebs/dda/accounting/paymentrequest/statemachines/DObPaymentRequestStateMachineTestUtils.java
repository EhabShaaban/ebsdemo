package com.ebs.dda.accounting.paymentrequest.statemachines;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

public class DObPaymentRequestStateMachineTestUtils {
	public static final DObPaymentRequestStateMachine stateMachine = Mockito
					.mock(DObPaymentRequestStateMachine.class);

	public static void initObjectState() throws Exception {
		Mockito.doNothing().when(stateMachine).initObjectState(ArgumentMatchers.any());
	}

	public static DObPaymentRequestStateMachine getStateMachine() {
		return stateMachine;
	}
}
