package com.ebs.dda.accounting.settlement.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.ebs.dda.accounting.settlement.validation.utils.DObSettlementCreationValidatorTestUtils;

@RunWith(JUnitPlatform.class)
public class DObSettlementCreationValidatorTests {

	private DObSettlementCreationValidatorTestUtils utils;

	@BeforeEach
	public void init() {
		utils = new DObSettlementCreationValidatorTestUtils();
	}

	private Map<String, String> missingValuesMap = new HashMap<String, String>() {
		{
			put("Null Value", null);
			put("Empty String", " ");
		}
	};

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_TypeIsMissing() {
		return missingValuesMap.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String missingValue = missingValuesMap.get(key);
			utils.withType(missingValue).withBusinessUnitCode("0002").withCompanyCode("0002")
							.withCurrencyCode("0001").withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_BusinessUnitIsMissing() {
		return missingValuesMap.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String missingValue = missingValuesMap.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode(missingValue).withCompanyCode("0002")
							.withCurrencyCode("0001").withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_CompanyIsMissing() {
		return missingValuesMap.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String missingValue = missingValuesMap.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode(missingValue)
							.withCurrencyCode("0001").withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_CurrencyIsMissing() {
		return missingValuesMap.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String missingValue = missingValuesMap.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
							.withCurrencyCode(missingValue).withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_DocumentOwnerIsMissing() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
						.withCurrencyCode("0001").withDocumentOwnerId(null)
						.assertThrowsArgumentSecurityException();
	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_BusinessUnitDoesNotExist() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("9999").withCompanyCode("0002")
						.withCurrencyCode("0001").withDocumentOwnerId(34L)
						.whenBusinessUnitDoesNotExist().assertThrowsArgumentSecurityException();
	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_CompanyDoesNotExist() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("9999")
						.withCurrencyCode("0001").withDocumentOwnerId(34L).whenCompanyDoesNotExist()
						.assertThrowsArgumentSecurityException();
	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_CurrencyDoesNotExist() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
						.withCurrencyCode("9999").withDocumentOwnerId(34L)
						.whenCurrencyDoesNotExist().assertThrowsArgumentSecurityException();
	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_DocumentOwnerDoesNotExist() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
						.withCurrencyCode("0001").withDocumentOwnerId(999L)
						.whenDocumentOwnerDoesNotExist().assertThrowsArgumentSecurityException();
	}
}
