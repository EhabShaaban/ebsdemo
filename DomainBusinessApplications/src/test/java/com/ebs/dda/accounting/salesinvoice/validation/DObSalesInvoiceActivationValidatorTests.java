package com.ebs.dda.accounting.salesinvoice.validation;

import com.ebs.dda.accounting.salesinvoice.validation.utils.DObSalesInvoiceActivationValidatorTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(JUnitPlatform.class)
public class DObSalesInvoiceActivationValidatorTests {

  private DObSalesInvoiceActivationValidatorTestUtils utils;

  @BeforeEach
  public void init() {
    utils = new DObSalesInvoiceActivationValidatorTestUtils();
  }

  private Map<String, String> missingValuesMap =
      new HashMap<String, String>() {
        {
          put("Null Value", null);
          put("Empty String", "");
        }
      };

  @TestFactory
  public Stream<DynamicTest> Should_ReturnErrorBindToDetailsSection_When_CurrencyIsMissing() {
    return missingValuesMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String missingValue = missingValuesMap.get(key);
                      utils
                          .withDocumentCode("202100009")
                          .withCurrency(missingValue)
                          .withPaymentTerm("0001")
                          .whenSectionFieldIsMissing()
                          .assertThatSectionFieldAreMissing("SalesInvoiceDetails", "currencyIso");
                    }));
  }
  @TestFactory
  public Stream<DynamicTest> Should_ReturnErrorBindToDetailsSection_When_PaymentTermIsMissing() {
    return missingValuesMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String missingValue = missingValuesMap.get(key);
                      utils
                          .withDocumentCode("202100009")
                          .withCurrency("USD")
                          .withPaymentTerm(missingValue)
                          .whenSectionFieldIsMissing()
                          .assertThatSectionFieldAreMissing("SalesInvoiceDetails", "paymentTermCode");
                    }));
  }
}
