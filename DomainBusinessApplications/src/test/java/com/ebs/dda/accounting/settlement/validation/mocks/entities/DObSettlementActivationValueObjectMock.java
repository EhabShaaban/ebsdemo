package com.ebs.dda.accounting.settlement.validation.mocks.entities;

public class DObSettlementActivationValueObjectMock {

  private String documentCode;

  public String getDocumentCode() {
    return documentCode;
  }

  public void setDocumentCode(String documentCode) {
    this.documentCode = documentCode;
  }
}
