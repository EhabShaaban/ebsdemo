package com.ebs.dda.accounting.settlement.validation.utils;

import static org.junit.jupiter.api.Assertions.assertThrows;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.mockito.Mockito;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.accounting.settlement.validation.IObSettlementAddAccountingDetailsValidator;
import com.ebs.dda.accounting.settlement.validation.mocks.entities.HObChartOfAccountsGeneralModelMock;
import com.ebs.dda.accounting.settlement.validation.mocks.entities.SubAccountGeneralModelMock;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.SubAccountGeneralModel;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;

public class IObSettlementAddAccountingDetailsValidatorTestUtils {

  private IObSettlementAddAccountingDetailsValueObject valueObject;
  private IObSettlementAddAccountingDetailsValidator validator;

  DObSettlementGeneralModelRep settlementGeneralModelRep;
  HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep;
  SubAccountGeneralModelRep subAccountGeneralModelRep;
  HObChartOfAccountsGeneralModelMock chartOfAccountsGeneralModelMock;
  List<SubAccountGeneralModel> subAccountGeneralModelMocks;

  public IObSettlementAddAccountingDetailsValidatorTestUtils() {
    valueObject = new IObSettlementAddAccountingDetailsValueObject();
    validator = new IObSettlementAddAccountingDetailsValidator();
    chartOfAccountsGeneralModelMock = new HObChartOfAccountsGeneralModelMock();
    subAccountGeneralModelMocks = new ArrayList<>();
    settlementGeneralModelRep = Mockito.mock(DObSettlementGeneralModelRep.class);
    glAccountsGeneralModelRep = Mockito.mock(HObChartOfAccountsGeneralModelRep.class);
    subAccountGeneralModelRep = Mockito.mock(SubAccountGeneralModelRep.class);
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withCode(String documentCode) {
    valueObject.setSettlementCode(documentCode);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withAccountType(String accountType) {
    valueObject.setAccountType(accountType);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withGLAccountCode(
      String glAccountCode) {
    valueObject.setGlAccountCode(glAccountCode);
    chartOfAccountsGeneralModelMock.setUserCode(glAccountCode);
    return this;
  }
  public IObSettlementAddAccountingDetailsValidatorTestUtils withGLAccountCreditDebit(
      String creditDebit) {

    chartOfAccountsGeneralModelMock.setCreditDebit(creditDebit);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withLedger(String ledger) {
    chartOfAccountsGeneralModelMock.setLeadger(ledger);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withGLSubAccountCode(
      String glSubAccountCode) {
    valueObject.setGlSubAccountCode(glSubAccountCode);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils withAmount(BigDecimal amount) {
    valueObject.setAmount(amount);
    return this;
  }

  public void assertThrowsArgumentSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> {
          validator.validate(valueObject);
        });
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils whenSettlementNotExist() {
    Mockito.when(settlementGeneralModelRep.findOneByUserCode(valueObject.getSettlementCode()))
        .thenReturn(null);
    validator.setSettlementGeneralModelRep(settlementGeneralModelRep);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils whenGlAccountNotExist() {
    mockSettlement();
    Mockito.when(glAccountsGeneralModelRep.findOneByUserCode(valueObject.getGlAccountCode()))
        .thenReturn(null);
    validator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils
  whenGlAccountHasLedgerAndValueObjectDoesntHasGLSubAccount() {
    mockSettlement();
    Mockito.when(glAccountsGeneralModelRep.findOneByUserCode(valueObject.getGlAccountCode()))
        .thenReturn(chartOfAccountsGeneralModelMock);
    validator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
    return this;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils
  whenGlAccountDoesntHasLedgerAndValueObjectHasGLSubAccount() {
    mockSettlement();
    Mockito.when(glAccountsGeneralModelRep.findOneByUserCode(valueObject.getGlAccountCode()))
        .thenReturn(chartOfAccountsGeneralModelMock);
    validator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
    return this;
  }

  private void mockSettlement() {
    Mockito.when(settlementGeneralModelRep.findOneByUserCode(valueObject.getSettlementCode()))
        .thenReturn(getSettlementObject());
    validator.setSettlementGeneralModelRep(settlementGeneralModelRep);
  }

  private DObSettlementGeneralModel getSettlementObject() {
    DObSettlementGeneralModel settlementGeneralModel = new DObSettlementGeneralModel();
    settlementGeneralModel.setUserCode(valueObject.getSettlementCode());
    return settlementGeneralModel;
  }

  public IObSettlementAddAccountingDetailsValidatorTestUtils whenIsNotValidSubAccount() {
    mockSettlement();
    getSubAccountGeneralModelMocks();
    Mockito.when(glAccountsGeneralModelRep.findOneByUserCode(valueObject.getGlAccountCode()))
        .thenReturn(chartOfAccountsGeneralModelMock);
    Mockito.when(
            subAccountGeneralModelRep.findAllByLedger(chartOfAccountsGeneralModelMock.getLeadger()))
        .thenReturn(subAccountGeneralModelMocks);
    validator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
    validator.setSubAccountGeneralModelRep(subAccountGeneralModelRep);
    return this;
  }

  private void getSubAccountGeneralModelMocks() {
    SubAccountGeneralModelMock subAccountGeneralModel = new SubAccountGeneralModelMock();
    subAccountGeneralModel.setCode("000003");
    subAccountGeneralModel.setLedger(chartOfAccountsGeneralModelMock.getLeadger());
    subAccountGeneralModelMocks.add(subAccountGeneralModel);
  }
}
