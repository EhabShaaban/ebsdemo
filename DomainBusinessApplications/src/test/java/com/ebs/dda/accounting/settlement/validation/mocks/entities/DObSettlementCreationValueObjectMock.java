package com.ebs.dda.accounting.settlement.validation.mocks.entities;

import com.ebs.dda.jpa.accounting.settlements.DObSettlementCreationValueObject;

public class DObSettlementCreationValueObjectMock extends DObSettlementCreationValueObject {

	private String type;
	private String businessUnitCode;
	private String companyCode;
	private String currencyCode;
	private Long documentOwnerId;

	public void setType(String type) {
		this.type = type;
	}

	public void setBusinessUnitCode(String businessUnitCode) {
		this.businessUnitCode = businessUnitCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setDocumentOwnerId(Long documentOwnerId) {
		this.documentOwnerId = documentOwnerId;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public String getBusinessUnitCode() {
		return businessUnitCode;
	}

	@Override
	public String getCompanyCode() {
		return companyCode;
	}

	@Override
	public String getCurrencyCode() {
		return currencyCode;
	}

	@Override
	public Long getDocumentOwnerId() {
		return documentOwnerId;
	}
}
