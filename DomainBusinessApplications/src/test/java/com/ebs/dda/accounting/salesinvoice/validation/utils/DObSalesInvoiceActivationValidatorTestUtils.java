package com.ebs.dda.accounting.salesinvoice.validation.utils;

import com.ebs.dda.accounting.salesinvoice.validation.DObSalesInvoicePostValidator;
import com.ebs.dda.accounting.salesinvoice.validation.mock.entities.DObSalesInvoiceActivationValueObjectMock;
import com.ebs.dda.accounting.salesinvoice.validation.mock.entities.IObSalesInvoiceDetailsMock;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import org.junit.Assert;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DObSalesInvoiceActivationValidatorTestUtils {
  private DObSalesInvoiceActivationValueObjectMock valueObject;
  private IObSalesInvoiceDetailsMock IObSalesInvoiceBusinessPartnerGeneralModelMock;
  private IObSalesInvoiceBusinessPartnerGeneralModelRep IObSalesInvoiceBusinessPartnerGeneralModelRep;
  private DObSalesInvoicePostValidator validator;




  public DObSalesInvoiceActivationValidatorTestUtils() {
    valueObject = new DObSalesInvoiceActivationValueObjectMock();
    IObSalesInvoiceBusinessPartnerGeneralModelMock = new IObSalesInvoiceDetailsMock();
    validator = new DObSalesInvoicePostValidator();
    IObSalesInvoiceBusinessPartnerGeneralModelRep = Mockito.mock(IObSalesInvoiceBusinessPartnerGeneralModelRep.class);
  }


  public DObSalesInvoiceActivationValidatorTestUtils whenSectionFieldIsMissing() {
    validator.setSalesInvoiceBusinessPartnerGeneralModelRep(IObSalesInvoiceBusinessPartnerGeneralModelRep);
    Mockito.when(
            IObSalesInvoiceBusinessPartnerGeneralModelRep.findOneByCode(valueObject.getDocumentCode()))
        .thenReturn(IObSalesInvoiceBusinessPartnerGeneralModelMock);

    return this;
  }


  public DObSalesInvoiceActivationValidatorTestUtils withPaymentTerm(String paymentTermCode) {
    IObSalesInvoiceBusinessPartnerGeneralModelMock.setPaymentTermCode(paymentTermCode);
    return this;
  }
  public DObSalesInvoiceActivationValidatorTestUtils withDocumentCode(String documentCode) {
    IObSalesInvoiceBusinessPartnerGeneralModelMock.setCode(documentCode);
    valueObject.setDocumentCode(documentCode);
    return this;
  }

  public DObSalesInvoiceActivationValidatorTestUtils withCurrency(String currencyIso) {
    IObSalesInvoiceBusinessPartnerGeneralModelMock.setCurrencyIso(currencyIso);
    return this;
  }
  public void assertThatSectionFieldAreMissing(String sectionName, String missingField ) throws Exception {
    Map<String, List<String>> sectionMissingFields = validator.validateAllSectionsForState(valueObject.getDocumentCode());
    List<String> missingFields = sectionMissingFields.get(sectionName);
    Assert.assertEquals(missingFields, Arrays.asList(missingField));
  }

}
