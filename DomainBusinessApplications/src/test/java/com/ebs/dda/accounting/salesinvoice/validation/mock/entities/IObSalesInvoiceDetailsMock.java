package com.ebs.dda.accounting.salesinvoice.validation.mock.entities;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;

public class IObSalesInvoiceDetailsMock extends IObSalesInvoiceBusinessPartnerGeneralModel {
    private String code;
    public void setCode(String invoiceCode){
        this.code = invoiceCode;
    }

    @Override
    public String getCode() {
        return code;
    }
}
