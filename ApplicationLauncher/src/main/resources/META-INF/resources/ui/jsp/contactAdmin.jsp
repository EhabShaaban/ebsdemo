<%@ include file="include.jsp" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Al Madina for Advanced Technology</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/login/libs/bootstrap/dist/css/bootstrap.min.css"/>"
          rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
        body {
            /*
            background: url('./eagle.svg') no-repeat center center fixed;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            */
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQIW2P8+vXrfwYgYAQxuLm5GQFVhwfhkI+XXwAAAABJRU5ErkJggg==) repeat;
        }

        .login {
            opacity: 0.8;

            /* IE 5-7 */
            filter: alpha(opacity=80);

            /* Netscape */
            -moz-opacity: 0.8;

            /* Safari 1.x */
            -khtml-opacity: 0.8;
        }

    </style>

</head>
<body>
<div class="container login">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="thumbnail">
                <img src="<c:url value="/resources/login/images/madina-tech.png"/>">
                <div class="caption">
                    <span class="help-block" style="text-align: center; /* color:red; */">Please contact your administrator.</span>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<c:url value="/resources/login/libs/jquery/dist/jquery.min.js"/>"></script>
<script src="<c:url value="/resources/login/libs/bootstrap/dist/js/bootstrap.min.js"/>"></script>

<script type="text/javascript">
  $(function () {
    var $loginForm = $('.login'),
        positionLoginForm = function () {
          var height = $(window).height(),
              formHeight = $loginForm.height()
          ;

          $('.login').css('margin', (height / 2 - formHeight / 2) + 'px auto');

        };

    $(window).resize(positionLoginForm);
    positionLoginForm();
  });
</script>

</body>
</html>