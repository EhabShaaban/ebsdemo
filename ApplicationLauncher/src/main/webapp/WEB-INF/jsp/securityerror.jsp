<%@ include file="include.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Al Madina for Advanced Technology</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
        href="<c:url value="./assets/bootstrap/dist/css/bootstrap.min.css"/>"
        rel="stylesheet" id="bootstrap-css">
<style type="text/css">
body {
	background: white;
}

.container {
	margin-left: auto;
	margin-right: auto;
	display: block;
    margin-top: 200px;
    width: 520px;
}

.container-body {
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    height: 250px;
}

button {
    width: 481px;
    color: white;
    background: #00838f;
    border: none;
    height: 60px;
    border-radius: 4px;
    font-size: 20px;
}

h1, h4, .button-container {
    text-align: center;
}

</style>

</head>
<body>
<form action="/login" method="get" id="login-form">

    <div class="container">

        <div class="container-body">
            <div class="">
                <h1>Oops! Something went wrong</h1>
            </div>
            <div>
                <h4>An unknown error occurred. Please try again after a while. If the problem
                    persists, please contact technical support.</h4>
				</div>
            <div class="button-container">
                <button>Login</button>
            </div>
        </div>
    </div>
</form>

	<script
			src="<c:url value="./assets/jquery/dist/jquery.min.js"/>"></script>
	<script
			src="<c:url value="./assets/bootstrap/dist/js/bootstrap.min.js"/>"></script>


</body>
</html>
