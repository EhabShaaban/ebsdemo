<%@ include file="include.jsp" %>
<%@ page pageEncoding="UTF-8" %>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=" utf-8
    ">
    <title>Al Madina for Advanced Technology</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="./assets/bootstrap/dist/css/bootstrap.css"/>" rel="stylesheet"
          id="bootstrap-css">
    <script src="<c:url value="./assets/jquery/dist/jquery.min.js"/>"></script>
    <style type="text/css">
        p {
            margin: 0px;
        }

        .page-container {
            display: flex;
            justify-content: center;
            width: 100%;
            height: 100%;
            align-items: center;
            background: #f7f8fc;
        }

        .login-container {
            width: 480px;
            height: 590px;
            border-radius: 4px;
            background-color: #ffffff;
            box-shadow: 0 1px 2px 0 #d8dde6;
            justify-content: space-between;
        }

        .login-head {
            display: flex;
            justify-content: center;
            margin: 15px 0px;
        }

        .logo {
            width: 104px;
            height: 106px;
            object-fit: contain;
        }

        .login-form-container {
            margin: 0px 40px 40px 40px;
            height: 60%;
        }

        .login-forget {
            color: #5c6bc0;
            padding: 6px 0px;
        }

        .help-block {
            background: #ffc5c7;
            height: 72px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        #login-form {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
        }

        .help-block-header {
            margin-bottom: 0px;
        }
    </style>
</head>
<body style="overflow: hidden">

<shiro:notAuthenticated>
<c:choose>
<c:when test="${cookie['lang'].value=='ar'}">
<div class="page-container" dir="rtl">
    <div class="login-container" id="english-login">
        <div class="login-head">
            <img class="logo" src="<c:url value="./assets/images/logo.png"/>">
        </div>
        <div class="login-form-container">
            <%
                if (request.getAttribute("isAuthenticated") != null && !(Boolean) request
                        .getAttribute("isAuthenticated")) {
            %>
            <div class="help-block">
                <p class="help-block-header">
                    كلمة المرور غير صحيحه
                </p>
                <a type="button" login-forget">هل نسيت كلمة المرور ؟</a>
            </div>
            <%
                } else {
                }%>
            <form action="" method="post" id="login-form">


                <div class="form-group">
                    <label for="login-username">اسم المستخدم</label>
                    <input type="text" class="form-control" id="login-username"
                           name="username" placeholder="اسم المستخدم">
                    <small style="color:red;"
                           class="form-text text-muted username-help-block" hidden>ادخل اسم المستخدم
                    </small>
                </div>
                <div class="form-group">
                    <label for="login-username">Password</label>
                    <input type="password" class="form-control" id="login-password"
                           name="password" placeholder="كلمة المرور">
                    <small style="color:red;"
                           class="form-text text-muted password-help-block" hidden>ادخل كلمة المرور
                    </small>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-link login-forget">هل نسيت كلمة المرور
                    </button>
                </div>
                <input type="hidden" name="rememberMe" value="0">
                <input class="btn btn-lg btn-primary btn-block" type="submit" value="تسجيل الدخول"
                       style="color: white; background: #5c6bc0;border: none; height: 60px">
            </form>
        </div>
    </div>
    <div/>
    </c:when>
    <c:otherwise>
    <div class="page-container">
        <div class="login-container" id="english-login">
            <div class="login-head">
                <img class="logo" src="<c:url value="./assets/images/logo.png"/>">
            </div>
            <div class="login-form-container">
                <%
                    if (request.getAttribute("isAuthenticated") != null && !(Boolean) request
                            .getAttribute("isAuthenticated")) {
                %>
                <div class="help-block">
                    <p class="help-block-header">
                        The username or password is not correct.
                    </p>
                    <a type="button" login-forget">Did you forgot the password?</a>
                </div>
                <%
                    } else {
                    }%>
                <form action="" method="post" id="login-form">


                    <div class="form-group">
                        <label for="login-username">Email or Username</label>
                        <input type="text" class="form-control" id="login-username"
                               name="username" placeholder="Enter email">
                        <small style="color:red;"
                               class="form-text text-muted username-help-block" hidden>Missing Username
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="login-username">Password</label>
                        <input type="password" class="form-control" id="login-password"
                               name="password" placeholder="Enter Password">
                        <small style="color:red;"
                               class="form-text text-muted password-help-block" hidden>Missing Password
                        </small>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-link login-forget">Forget your
                            password?
                        </button>
                    </div>
                    <input type="hidden" name="rememberMe" value="0">
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login"
                           style="color: white; background: #5c6bc0;border: none; height: 60px">
                </form>
            </div>
        </div>
        <div/>
        </c:otherwise>
        </c:choose>

        <script src="<c:url value="./assets/bootstrap/dist/js/bootstrap.min.js"/>"></script>

        <script type="text/javascript">
          $(function () {
            $('#login-form').submit(function (e) {
              e.preventDefault();
              var username = $('#login-username').val();
              var password = $('#login-password').val();
              var isMissingData = false;

              if (username == null || username.trim() === '') {
                isMissingData = true;
                $('.username-help-block').show();
                $('.help-block').hide();
              } else {
                $('.username-help-block').hide();
              }
              if (password == null || password.trim() === '') {
                isMissingData = true;
                $('.password-help-block').show();
                $('.help-block').hide();

              } else {
                $('.password-help-block').hide();

              }
              if (!isMissingData) {
                $('.username-help-block').hide();
                $('.password-help-block').hide();
                $('.help-block').show();
                $('#login-username').val($.trim($('#login-username').val()));
                this.submit();
              }
            });
          });
        </script>
        </shiro:notAuthenticated>

        <shiro:authenticated/>

</body>
</html>
