package rest;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.security.exceptions.FirstFactorAuthenticationException;
import com.ebs.dac.security.services.UserInfoService;
import com.ebs.dac.security.shiro.filters.CustomFormAuthenticationFilter;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.EBSSubject;
import com.google.gson.Gson;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

@RestController
@RequestMapping(value = "/")
public class AuthenticationController {

  public static final String CHANGE_DEFAULT_PASSWORD_PAGE = "/#/user-settings/change-default-password";
  private final String UNICODE_FORMAT = "UTF-8";
  private final String REQUESTED_URL = "RequestedURL";
  private final String SUCCESS_URL = "shiro.successUrl";
  private final String LOCATION = "Location";

  @Autowired
  private IConcurrentDataAccessManagersObjectPool accessManagersPool;
  @Autowired
  private Environment environment;

  @RequestMapping("/loggedin")
  public String getLoggedInUser() {
    Subject subject = SecurityUtils.getSubject();
    UserInfoService userInfoService = new UserInfoService();
    return (subject != null && subject.getPrincipal() != null)
        ? new Gson().toJson(userInfoService.getUserInfo((EBSPrincipal) subject.getPrincipal()))
        : null;
  }
  @GetMapping("/login")
  public ModelAndView loginGet(HttpServletResponse response, HttpServletRequest request) throws Exception {
    Subject subject = SecurityUtils.getSubject();
    if ( subject.isAuthenticated()) {
      response.setStatus(HttpStatus.FOUND.value());
      response.setHeader(LOCATION, getUserRequestedURL(request));
      return null;
    }
    return new ModelAndView("login");
  }

  @GetMapping("/securityerror")
  public ModelAndView viewSecurityErrorJSP() {

    return new ModelAndView("securityerror");
  }

  @PostMapping("/login")
  public ModelAndView loginPost(@RequestParam String username, @RequestParam String password,
      HttpServletResponse response, HttpServletRequest request) {

    UsernamePasswordToken token = new UsernamePasswordToken(username, password);
    Subject subject = SecurityUtils.getSubject();
    try {
      subject.login(token);
      response.setStatus(HttpStatus.FOUND.value());
      request.setAttribute("isAuthenticated", true);
      try {
        if (((EBSPrincipal) subject.getPrincipal()).getSysPasswordFlag()) {
          redirectToChangeDefaultPasswordPage(response);
        } else {
          response.setHeader(LOCATION, getUserRequestedURL(request));
        }
      } catch (UnsupportedEncodingException e) {
      }
      return null;
    } catch (FirstFactorAuthenticationException exception) {
      request.setAttribute("isAuthenticated", false);
      return new ModelAndView("login");
    }
  }

  private void redirectToChangeDefaultPasswordPage(HttpServletResponse response)
      throws UnsupportedEncodingException {
    String successUrl = environment.getProperty(SUCCESS_URL);
    String url = URLDecoder.decode(successUrl + CHANGE_DEFAULT_PASSWORD_PAGE, UNICODE_FORMAT);
    response.setHeader(LOCATION, url);

  }

  private String getUserRequestedURL(HttpServletRequest httpRequest)
      throws UnsupportedEncodingException {
    String url = environment.getProperty(SUCCESS_URL);
    Cookie[] cookies = httpRequest.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().trim().equals(REQUESTED_URL)) {
          url = URLDecoder.decode(cookie.getValue(), UNICODE_FORMAT);
          break;
        }
      }
    }
    return url;
  }

  @GetMapping("/change-default-password")
  public ModelAndView viewChangePasswordJSP() {

    return new ModelAndView("change-default-password");
  }

  @GetMapping("/logout")
  public ModelAndView logout(HttpServletResponse response, HttpServletRequest request)
      throws Exception {
    Subject subject = SecurityUtils.getSubject();
    accessManagersPool.releaseAllLocksByCurrentUser();
    deleteRequestedURLCookie(response, request);
    setDefaultSuccessURL();
    subject.logout();
    return new ModelAndView("login");
  }

  private void setDefaultSuccessURL() {
    CustomFormAuthenticationFilter authenticationFilter = new CustomFormAuthenticationFilter();
    authenticationFilter.setSuccessUrl(environment.getProperty(SUCCESS_URL));
  }

  private void deleteRequestedURLCookie(HttpServletResponse response, HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
      if (cookie.getName().trim().equals(REQUESTED_URL)) {
        cookie.setValue("");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        break;
      }
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/isAdmin")
  public @ResponseBody boolean isAdmin() {

    ArrayList<String> adminRoles = adminRolesList();
    EBSSubject subject = (EBSSubject) SecurityUtils.getSubject();

    for (String adminRole : adminRoles) {
      if (subject.hasRole(adminRole)) {
        return true;
      }
    }
    return false;
  }
  public ArrayList<String> getAdminRoles() {
    return adminRolesList();
  }

  private ArrayList<String> adminRolesList() {
    ArrayList<String> roles = new ArrayList();

    roles.add("SuperUserSubRole");
    roles.add("GROwner_Signmedia");
    roles.add("ItemOwner");
    roles.add("ItemOwner_Flexo");
    roles.add("ItemOwner_Offset");
    roles.add("ItemOwner_Digital");
    roles.add("ItemOwner_Signmedia");
    roles.add("ItemOwner_Textile");
    roles.add("ItemOwner_Corrugated");
    roles.add("ItemViewer_Flexo");
    roles.add("ItemViewer_Offset");
    roles.add("ItemViewer_Digital");
    roles.add("ItemViewer_Signmedia");
    roles.add("ItemViewer_Textile");
    roles.add("ItemViewer_Corrugated");
    roles.add("ItemViewer");
    roles.add("VendorOwner_Flexo");
    roles.add("VendorOwner_Offset");
    roles.add("VendorOwner_Digital");
    roles.add("VendorOwner_Signmedia");
    roles.add("VendorOwner_Textile");
    roles.add("VendorOwner_Corrugated");
    roles.add("VendorViewer_Flexo");
    roles.add("VendorViewer_Offset");
    roles.add("VendorViewer_Digital");
    roles.add("VendorViewer_Signmedia");
    roles.add("VendorViewer_Textile");
    roles.add("VendorViewer_Corrugated");
    roles.add("VendorViewer");
    roles.add("IVROwner_Flexo");
    roles.add("IVROwner_Offset");
    roles.add("IVROwner_Digital");
    roles.add("IVROwner_Signmedia");
    roles.add("IVROwner_Textile");
    roles.add("IVROwner_Corrugated");

    roles.add("VendorViewer");

    return roles;
  }
}
