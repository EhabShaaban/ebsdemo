package com.ebs.dda.config;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRatesUpdateCommandRemotly;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 21, 2020 9:14:47 AM
 */
@Configuration
public class ScheduleTasks {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleTasks.class);

  @Autowired
  private CObExchangeRatesUpdateCommandRemotly exRatesUpdateCmd;

  @Scheduled(cron = "0 1 7 * * ?")
  @PostConstruct
  public void getLatestExchangeRates() {
    LOGGER.info("Trying to get latest exchange rates...");
    try {
      exRatesUpdateCmd.executeCommand(null);
    } catch (Exception e) {
      LOGGER.error("Failed to get latest exchange rates. caused by: " + e.getMessage());
    }
  }


}
