package com.ebs.dda.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ebs.dac.spring.configs.AuthorizationConfig;
import com.ebs.dac.spring.configs.CorsFilterBean;
import com.ebs.dac.spring.configs.JPAConfiguration;
import com.ebs.dac.spring.configs.ShiroConfig;
import com.ebs.dac.spring.configs.ShiroHazelCastCacheConfig;
import com.ebs.dac.spring.configs.WebConfig;

import config.ReportingApplicationLauncher;

@SpringBootApplication
@Import({
  JPAConfiguration.class,
  CommandApplicationLauncher.class,
  QueryApplicationLauncher.class,
  ShiroHazelCastCacheConfig.class,
  ShiroConfig.class,
  WebConfig.class,
  CorsFilterBean.class,
  AuthorizationConfig.class,
  VendorBeans.class,
  ItemBeans.class,
  ReportingApplicationLauncher.class,
  ScheduleTasks.class
})
@EntityScan({
  "com.ebs.dac.security.jpa.entities",
  "com.ebs.dac.dbo.jpa.entities",
  "com.ebs.dda.dbo.jpa.entities",
  "com.ebs.dda.masterdata.dbo.jpa.entities",
  "com.ebs.dda.shipping.dbo.jpa.entities",
  "com.ebs.dda.purchases.dbo.jpa.entities",
  "com.ebs.dac.dbo.jpa.converters",
  "com.ebs.dda.accounting.dbo.jpa.entities",
  "com.ebs.dda.approval.dbo.jpa.entities",
  "com.ebs.dda.accounting.paymentrequest.dbo.jpa.entites",
  "com.ebs.dda.accounting.dbo.jpa.entities",
  "com.ebs.dda.accounting.journalentry.dbo.jpa.entities",
  "com.ebs.dda.dbo.jpa.entities.lookups",
  "com.ebs.dda.sales.dbo.jpa.entities",
  "com.ebs.dda.notesreceivables.dbo.jpa.entities",
  "com.ebs.dda.jpa.accounting.collection",
  "com.ebs.dda.jpa",
})
@ComponentScan("rest")
@EnableScheduling
public class ApplicationLauncher {

  private static final Logger log = LoggerFactory.getLogger(ApplicationLauncher.class);

  public static void main(String[] args) {
    log.info(
        ""
            + SpringApplication.run(ApplicationLauncher.class, args)
                .getBean(ShiroConfig.FilterChainDefinitionInterceptor.class));
  }

}
