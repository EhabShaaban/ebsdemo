#Author & Reviewer: Yara Ameen on Date: 24-Nov-2019

Feature: Request Change Default Password

  Background:
    Given the following users and passwords exist:
      | UserName                       | Password | IsDefault |
      | TestUser_With_Default_Password | madina   | True      |

  #EBS-5699
  Scenario Outline: (01) Save new password with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "TestUser_With_Default_Password" with limited session
    And "TestUser_With_Default_Password" is forwarded to the change password page
    When "TestUser_With_Default_Password" request to change the default password with the following values:
      | CurrentPassword   | NewPassword   | ConfirmedNewPassword   |
      | <CurrentPassword> | <NewPassword> | <ConfirmedNewPassword> |
    Then "TestUser_With_Default_Password" is logged out
    And "TestUser_With_Default_Password" is forwarded to the error page
    Examples:
      | CurrentPassword | NewPassword | ConfirmedNewPassword |
      |                 | 123456      | 123456               |
      | madina          |             | 123456               |
      | madina          | 123456      |                      |

  #EBS-5699
  Scenario Outline: (02) Save new password with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "TestUser_With_Default_Password" with limited session
    And "TestUser_With_Default_Password" is forwarded to the change password page
    When "TestUser_With_Default_Password" request to change the default password with the following values:
      | CurrentPassword   | NewPassword   | ConfirmedNewPassword   |
      | <CurrentPassword> | <NewPassword> | <ConfirmedNewPassword> |
    Then "TestUser_With_Default_Password" is logged out
    And "TestUser_With_Default_Password" is forwarded to the error page
    Examples:
      | CurrentPassword | NewPassword | ConfirmedNewPassword |
      | madina          | 12          | 123456               |
      | madina          | 123456      | 12                   |
      | madina          | 123456      | 342156               |
      | madina          | 12          | 12                   |

  #EBS-5699
  Scenario Outline: (03) Save new password with with incorrect data entry (Validation Failure)
    Given user is logged in as "TestUser_With_Default_Password" with limited session
    And "TestUser_With_Default_Password" is forwarded to the change password page
    When "TestUser_With_Default_Password" request to change the default password with the following values:
      | CurrentPassword   | NewPassword   | ConfirmedNewPassword   |
      | <CurrentPassword> | <NewPassword> | <ConfirmedNewPassword> |
    Then a failure notification is sent to "TestUser_With_Default_Password" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "TestUser_With_Default_Password"
    Examples:
      | CurrentPassword | NewPassword | ConfirmedNewPassword | Field           | ErrMsg       |
      | abcdef          | 123456      | 123456               | currentPassword | Login-msg-01 |
      | madina          | madina      | madina               | newPassword     | Login-msg-05 |
