#Author & Reviewer: Yara Ameen & Aya Sadek & Shirin Mahmoud on Date: 21-Nov-2019

Feature: Request Change Default Password

  Background:
    Given the following users and passwords exist:
      | UserName                       | Password | IsDefault |
      | TestUser_With_Default_Password | madina   | True      |

  #EBS-5699
  Scenario: (01) Save new password by an authenticated user (Happy Path)
    Given user is logged in as "TestUser_With_Default_Password" with limited session
    And "TestUser_With_Default_Password" is forwarded to the change password page
    When "TestUser_With_Default_Password" request to change the default password with the following values:
      | CurrentPassword | NewPassword | ConfirmedNewPassword |
      | madina          | 123456      | 123456               |
    Then a success notification is sent to "TestUser_With_Default_Password" with the following message "Gen-msg-04"
    And "TestUser_With_Default_Password" is logged out
    And "TestUser_With_Default_Password" is forwarded to the login page
    And "TestUser_With_Default_Password" password is changed with the following values:
      | NewPassword | IsDefault |
      | 123456      | False     |

