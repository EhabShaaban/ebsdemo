Feature: Automated Invoice Users Access Rights Insertion


  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                           |
      | VendorInvoiceOwner_Flexo       |
      | VendorInvoiceViewer_Flexo      |
      | VendorInvoiceOwner_Offset            |
      | VendorInvoiceViewer_Offset     |
      | VendorInvoiceOwner_Digital           |
      | VendorInvoiceViewer_Digital    |
      | VendorInvoiceOwner_Signmedia         |
      | VendorInvoiceViewer_Signmedia  |
      | VendorInvoiceOwner_Textile           |
      | VendorInvoiceViewer_Textile    |
      | VendorInvoiceOwner_Corrugated        |
      | VendorInvoiceViewer_Corrugated |
      | JournalCreator_Flexo           |
      | JournalCreator_Offset          |
      | JournalCreator_Digital         |
      | JournalCreator_Signmedia       |
      | JournalCreator_Textile         |
      | JournalCreator_Corrugated      |
      | VendorInvoiceViewer            |

    Then the following roles created successfully:
      | Role                           |
      | VendorInvoiceOwner_Flexo             |
      | VendorInvoiceViewer_Flexo      |
      | VendorInvoiceOwner_Offset            |
      | VendorInvoiceViewer_Offset     |
      | VendorInvoiceOwner_Digital           |
      | VendorInvoiceViewer_Digital    |
      | VendorInvoiceOwner_Signmedia         |
      | VendorInvoiceViewer_Signmedia  |
      | VendorInvoiceOwner_Textile           |
      | VendorInvoiceViewer_Textile    |
      | VendorInvoiceOwner_Corrugated        |
      | VendorInvoiceViewer_Corrugated |
      | JournalCreator_Flexo           |
      | JournalCreator_Offset          |
      | JournalCreator_Digital         |
      | JournalCreator_Signmedia       |
      | JournalCreator_Textile         |
      | JournalCreator_Corrugated      |
      | VendorInvoiceViewer            |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Flexo      | VendorInvoiceOwner_Flexo        |
      | PurchasingResponsible_Flexo      | VendorInvoiceViewer_Flexo       |
      | PurchasingResponsible_Offset     | VendorInvoiceOwner_Offset       |
      | PurchasingResponsible_Offset     | VendorInvoiceViewer_Offset      |

      | PurchasingResponsible_Signmedia  | VendorInvoiceOwner_Signmedia    |
      | PurchasingResponsible_Signmedia  | VendorInvoiceViewer_Signmedia   |

      | PurchasingResponsible_Textile    | VendorInvoiceOwner_Textile      |
      | PurchasingResponsible_Textile    | VendorInvoiceViewer_Textile     |

      | PurchasingResponsible_Corrugated | VendorInvoiceOwner_Corrugated   |
      | PurchasingResponsible_Corrugated | VendorInvoiceViewer_Corrugated  |

      | LogisticsResponsible_Flexo       | VendorInvoiceOwner_Flexo        |
      | LogisticsResponsible_Flexo       | VendorInvoiceViewer_Flexo       |

      | LogisticsResponsible_Offset      | VendorInvoiceOwner_Offset       |
      | LogisticsResponsible_Offset      | VendorInvoiceViewer_Offset      |

      | LogisticsResponsible_Digital     | VendorInvoiceOwner_Digital      |
      | LogisticsResponsible_Digital     | VendorInvoiceViewer_Digital     |

      | LogisticsResponsible_Signmedia   | VendorInvoiceOwner_Signmedia    |
      | LogisticsResponsible_Signmedia   | VendorInvoiceViewer_Signmedia   |

      | LogisticsResponsible_Textile     | VendorInvoiceOwner_Textile      |
      | LogisticsResponsible_Textile     | VendorInvoiceViewer_Textile     |

      | LogisticsResponsible_Corrugated  | VendorInvoiceOwner_Corrugated   |
      | LogisticsResponsible_Corrugated  | VendorInvoiceViewer_Corrugated  |

      | Accountant_Flexo                 | JournalCreator_Flexo      |
      | Accountant_Flexo                 | VendorInvoiceViewer_Flexo       |

      | Accountant_Offset                | JournalCreator_Offset     |
      | Accountant_Offset                | VendorInvoiceViewer_Offset      |

      | Accountant_Digital               | JournalCreator_Digital    |
      | Accountant_Digital               | VendorInvoiceViewer_Digital     |

      | Accountant_Signmedia             | JournalCreator_Signmedia  |
      | Accountant_Signmedia             | VendorInvoiceViewer_Signmedia   |

      | Accountant_Textile               | JournalCreator_Textile    |
      | Accountant_Textile               | VendorInvoiceViewer_Textile     |

      | Accountant_Corrugated            | JournalCreator_Corrugated |
      | Accountant_Corrugated            | VendorInvoiceViewer_Corrugated  |

      | AccountantHead_Flexo             | JournalCreator_Flexo      |
      | AccountantHead_Flexo             | VendorInvoiceViewer_Flexo       |

      | AccountantHead_Offset            | JournalCreator_Offset     |
      | AccountantHead_Offset            | VendorInvoiceViewer_Offset      |


      | AccountantHead_Digital           | JournalCreator_Digital    |
      | AccountantHead_Digital           | VendorInvoiceViewer_Digital     |

      | AccountantHead_Signmedia         | JournalCreator_Signmedia  |
      | AccountantHead_Signmedia         | VendorInvoiceViewer_Signmedia   |

      | AccountantHead_Textile           | JournalCreator_Textile    |
      | AccountantHead_Textile           | VendorInvoiceViewer_Textile     |


      | AccountantHead_Corrugated        | JournalCreator_Corrugated |
      | AccountantHead_Corrugated        | VendorInvoiceViewer_Corrugated  |

      | B.U.Director_Flexo               | VendorInvoiceViewer_Flexo       |
      | B.U.Director_Offset              | VendorInvoiceViewer_Offset      |
      | B.U.Director_Digital             | VendorInvoiceViewer_Digital     |
      | B.U.Director_Signmedia           | VendorInvoiceViewer_Signmedia   |
      | B.U.Director_Textile             | VendorInvoiceViewer_Textile     |
      | B.U.Director_Corrugated          | VendorInvoiceViewer_Corrugated  |

      | M.D                              | InvoiceViewer             |
      | Deputy_M.D                       | InvoiceViewer             |
      | Quality_Specialist               | InvoiceViewer             |
      | Auditor                          | InvoiceViewer             |
      | Corporate_Taxes_Accountant       | InvoiceViewer             |
      | Corporate_Accounting_Head        | InvoiceViewer             |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Invoice" entity permissions:
      | Permission           |
      | Create               |
      | UpdateInvoiceDetails |
      | UpdateItems          |
      | UpdateInstallments   |
      | Post                 |
      | Delete               |
      | ReadAll              |
      | ReadGeneralData      |
      | ReadInvoiceDetail    |
      | ReadPostingDetail    |
      | ReadItems            |
      | ReadInstallments     |


    Then the following "Invoice" entity permissions created successfully:
      | Permission           |
      | Create               |
      | UpdateInvoiceDetails |
      | UpdateItems          |
      | UpdateInstallments   |
      | Post                 |
      | Delete               |
      | ReadAll              |
      | ReadGeneralData      |
      | ReadInvoiceDetail    |
      | ReadPostingDetail    |
      | ReadItems            |
      | ReadInstallments     |


  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "Invoice" entity conditions:
      | Condition                       |
      | [purchaseUnitName='Flexo']      |
      | [purchaseUnitName='Offset']     |
      | [purchaseUnitName='Digital']    |
      | [purchaseUnitName='Signmedia']  |
      | [purchaseUnitName='Textile']    |
      | [purchaseUnitName='Corrugated'] |
    Then the following "Invoice" entity conditions created successfully:
      | Condition                       |
      | [purchaseUnitName='Flexo']      |
      | [purchaseUnitName='Offset']     |
      | [purchaseUnitName='Digital']    |
      | [purchaseUnitName='Signmedia']  |
      | [purchaseUnitName='Textile']    |
      | [purchaseUnitName='Corrugated'] |


  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Invoice" entity:
      | Subrole                   | Permission           | Condition                       |
      | VendorInvoiceOwner_Flexo        | Create               |                                 |
      | VendorInvoiceOwner_Offset       | Create               |                                 |
      | VendorInvoiceOwner_Digital      | Create               |                                 |
      | VendorInvoiceOwner_Signmedia    | Create               |                                 |
      | VendorInvoiceOwner_Textile      | Create               |                                 |
      | VendorInvoiceOwner_Corrugated   | Create               |                                 |

      | VendorInvoiceOwner_Flexo        | UpdateInvoiceDetails | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | UpdateItems          | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | UpdateInstallments   | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | Delete               | [purchaseUnitName='Flexo']      |

      | VendorInvoiceOwner_Offset       | UpdateInvoiceDetails | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | UpdateItems          | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | UpdateInstallments   | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | Delete               | [purchaseUnitName='Offset']     |

      | VendorInvoiceOwner_Digital      | UpdateInvoiceDetails | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | UpdateItems          | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | UpdateInstallments   | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | Delete               | [purchaseUnitName='Digital']    |

      | VendorInvoiceOwner_Signmedia    | UpdateInvoiceDetails | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | UpdateItems          | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | UpdateInstallments   | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | Delete               | [purchaseUnitName='Signmedia']  |

      | VendorInvoiceOwner_Textile      | UpdateInvoiceDetails | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | UpdateItems          | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | UpdateInstallments   | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | Delete               | [purchaseUnitName='Textile']    |

      | VendorInvoiceOwner_Corrugated   | UpdateInvoiceDetails | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | UpdateItems          | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | UpdateInstallments   | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | Delete               | [purchaseUnitName='Corrugated'] |


      | InvoiceViewer             | ReadAll              |                                 |
      | InvoiceViewer             | ReadGeneralData      |                                 |
      | InvoiceViewer             | ReadInvoiceDetail    |                                 |
      | InvoiceViewer             | ReadPostingDetail    |                                 |
      | InvoiceViewer             | ReadItems            |                                 |
      | InvoiceViewer             | ReadInstallments     |                                 |


      | VendorInvoiceViewer_Flexo       | ReadAll              | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadGeneralData      | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadInvoiceDetail    | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadPostingDetail    | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadItems            | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadInstallments     | [purchaseUnitName='Flexo']      |

      | VendorInvoiceViewer_Offset      | ReadAll              | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadGeneralData      | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadInvoiceDetail    | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadPostingDetail    | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadItems            | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadInstallments     | [purchaseUnitName='Offset']     |

      | VendorInvoiceViewer_Digital     | ReadAll              | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadGeneralData      | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadInvoiceDetail    | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadPostingDetail    | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadItems            | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadInstallments     | [purchaseUnitName='Digital']    |

      | VendorInvoiceViewer_Signmedia   | ReadAll              | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadGeneralData      | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadInvoiceDetail    | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadPostingDetail    | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadItems            | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadInstallments     | [purchaseUnitName='Signmedia']  |

      | VendorInvoiceViewer_Textile     | ReadAll              | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadGeneralData      | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadInvoiceDetail    | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadPostingDetail    | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadItems            | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadInstallments     | [purchaseUnitName='Textile']    |

      | VendorInvoiceViewer_Corrugated  | ReadAll              | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadGeneralData      | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadInvoiceDetail    | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadPostingDetail    | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadItems            | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadInstallments     | [purchaseUnitName='Corrugated'] |

      | JournalCreator_Flexo      | CreateJournalEntry   | [purchaseUnitName='Flexo']      |
      | JournalCreator_Offset     | CreateJournalEntry   | [purchaseUnitName='Offset']     |
      | JournalCreator_Digital    | CreateJournalEntry   | [purchaseUnitName='Digital']    |
      | JournalCreator_Signmedia  | CreateJournalEntry   | [purchaseUnitName='Signmedia']  |
      | JournalCreator_Textile    | CreateJournalEntry   | [purchaseUnitName='Textile']    |
      | JournalCreator_Corrugated | CreateJournalEntry   | [purchaseUnitName='Corrugated'] |

    Then the following roles , permissions and conditions assignment created successfully for "Invoice" entity:
      | Subrole                   | Permission           | Condition                       |
      | VendorInvoiceOwner_Flexo        | Create               |                                 |
      | VendorInvoiceOwner_Offset       | Create               |                                 |
      | VendorInvoiceOwner_Digital      | Create               |                                 |
      | VendorInvoiceOwner_Signmedia    | Create               |                                 |
      | VendorInvoiceOwner_Textile      | Create               |                                 |
      | VendorInvoiceOwner_Corrugated   | Create               |                                 |

      | VendorInvoiceOwner_Flexo        | UpdateInvoiceDetails | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | UpdateItems          | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | UpdateInstallments   | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Flexo        | Delete               | [purchaseUnitName='Flexo']      |

      | VendorInvoiceOwner_Offset       | UpdateInvoiceDetails | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | UpdateItems          | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | UpdateInstallments   | [purchaseUnitName='Offset']     |
      | VendorInvoiceOwner_Offset       | Delete               | [purchaseUnitName='Offset']     |

      | VendorInvoiceOwner_Digital      | UpdateInvoiceDetails | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | UpdateItems          | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | UpdateInstallments   | [purchaseUnitName='Digital']    |
      | VendorInvoiceOwner_Digital      | Delete               | [purchaseUnitName='Digital']    |

      | VendorInvoiceOwner_Signmedia    | UpdateInvoiceDetails | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | UpdateItems          | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | UpdateInstallments   | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Signmedia    | Delete               | [purchaseUnitName='Signmedia']  |

      | VendorInvoiceOwner_Textile      | UpdateInvoiceDetails | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | UpdateItems          | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | UpdateInstallments   | [purchaseUnitName='Textile']    |
      | VendorInvoiceOwner_Textile      | Delete               | [purchaseUnitName='Textile']    |

      | VendorInvoiceOwner_Corrugated   | UpdateInvoiceDetails | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | UpdateItems          | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | UpdateInstallments   | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceOwner_Corrugated   | Delete               | [purchaseUnitName='Corrugated'] |


      | InvoiceViewer             | ReadAll              |                                 |
      | InvoiceViewer             | ReadGeneralData      |                                 |
      | InvoiceViewer             | ReadInvoiceDetail    |                                 |
      | InvoiceViewer             | ReadPostingDetail    |                                 |
      | InvoiceViewer             | ReadItems            |                                 |
      | InvoiceViewer             | ReadInstallments     |                                 |


      | VendorInvoiceViewer_Flexo       | ReadAll              | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadGeneralData      | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadInvoiceDetail    | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadPostingDetail    | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadItems            | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Flexo       | ReadInstallments     | [purchaseUnitName='Flexo']      |

      | VendorInvoiceViewer_Offset      | ReadAll              | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadGeneralData      | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadInvoiceDetail    | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadPostingDetail    | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadItems            | [purchaseUnitName='Offset']     |
      | VendorInvoiceViewer_Offset      | ReadInstallments     | [purchaseUnitName='Offset']     |

      | VendorInvoiceViewer_Digital     | ReadAll              | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadGeneralData      | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadInvoiceDetail    | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadPostingDetail    | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadItems            | [purchaseUnitName='Digital']    |
      | VendorInvoiceViewer_Digital     | ReadInstallments     | [purchaseUnitName='Digital']    |

      | VendorInvoiceViewer_Signmedia   | ReadAll              | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadGeneralData      | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadInvoiceDetail    | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadPostingDetail    | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadItems            | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Signmedia   | ReadInstallments     | [purchaseUnitName='Signmedia']  |

      | VendorInvoiceViewer_Textile     | ReadAll              | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadGeneralData      | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadInvoiceDetail    | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadPostingDetail    | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadItems            | [purchaseUnitName='Textile']    |
      | VendorInvoiceViewer_Textile     | ReadInstallments     | [purchaseUnitName='Textile']    |

      | VendorInvoiceViewer_Corrugated  | ReadAll              | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadGeneralData      | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadInvoiceDetail    | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadPostingDetail    | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadItems            | [purchaseUnitName='Corrugated'] |
      | VendorInvoiceViewer_Corrugated  | ReadInstallments     | [purchaseUnitName='Corrugated'] |
#
      | JournalCreator_Flexo      | CreateJournalEntry   | [purchaseUnitName='Flexo']      |
      | JournalCreator_Offset     | CreateJournalEntry   | [purchaseUnitName='Offset']     |
      | JournalCreator_Digital    | CreateJournalEntry   | [purchaseUnitName='Digital']    |
      | JournalCreator_Signmedia  | CreateJournalEntry   | [purchaseUnitName='Signmedia']  |
      | JournalCreator_Textile    | CreateJournalEntry   | [purchaseUnitName='Textile']    |
      | JournalCreator_Corrugated | CreateJournalEntry   | [purchaseUnitName='Corrugated'] |

