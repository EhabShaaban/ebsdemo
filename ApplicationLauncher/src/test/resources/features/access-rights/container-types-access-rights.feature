Feature: Automated ContainerType Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                |
      | ContainerTypeViewer |
    Then the following roles created successfully:
      | Role                |
      | ContainerTypeViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole             |
      #Flexo
      | PurchasingResponsible_Flexo      | ContainerTypeViewer |
      #Offset
      | PurchasingResponsible_Offset     | ContainerTypeViewer |
      #Digital
      | PurchasingResponsible_Digital    | ContainerTypeViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | ContainerTypeViewer |
      #Textile
      | PurchasingResponsible_Textile    | ContainerTypeViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | ContainerTypeViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole             |
      #Flexo
      | PurchasingResponsible_Flexo      | ContainerTypeViewer |
      #Offset
      | PurchasingResponsible_Offset     | ContainerTypeViewer |
      #Digital
      | PurchasingResponsible_Digital    | ContainerTypeViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | ContainerTypeViewer |
      #Textile
      | PurchasingResponsible_Textile    | ContainerTypeViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | ContainerTypeViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ContainerType" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "ContainerType" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ContainerType" entity:
      | Subrole             | Permission | Condition |
      | ContainerTypeViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "ContainerType" entity:
      | Subrole             | Permission | Condition |
      | ContainerTypeViewer | ReadAll    |           |

