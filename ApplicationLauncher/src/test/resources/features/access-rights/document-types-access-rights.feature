Feature: Automated DocumentType Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                |
      | DocumentTypesViewer |
    Then the following roles created successfully:
      | Role                |
      | DocumentTypesViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole             |
      #Flexo
      | PurchasingResponsible_Flexo      | DocumentTypesViewer |
      #Offset
      | PurchasingResponsible_Offset     | DocumentTypesViewer |
      #Digital
      | PurchasingResponsible_Digital    | DocumentTypesViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | DocumentTypesViewer |
      #Textile
      | PurchasingResponsible_Textile    | DocumentTypesViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | DocumentTypesViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole             |
      #Flexo
      | PurchasingResponsible_Flexo      | DocumentTypesViewer |
      #Offset
      | PurchasingResponsible_Offset     | DocumentTypesViewer |
      #Digital
      | PurchasingResponsible_Digital    | DocumentTypesViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | DocumentTypesViewer |
      #Textile
      | PurchasingResponsible_Textile    | DocumentTypesViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | DocumentTypesViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "DocumentType" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "DocumentType" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "DocumentType" entity:
      | Subrole             | Permission | Condition |
      | DocumentTypesViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "DocumentType" entity:
      | Subrole             | Permission | Condition |
      | DocumentTypesViewer | ReadAll    |           |

