Feature: Automated Currency Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role           |
      | CurrencyViewer |
    Then the following roles created successfully:
      | Role           |
      | CurrencyViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole        |
      | PurchasingResponsible_Flexo      | CurrencyViewer |
      | PurchasingResponsible_Offset     | CurrencyViewer |
      | PurchasingResponsible_Digital    | CurrencyViewer |
      | PurchasingResponsible_Signmedia  | CurrencyViewer |
      | PurchasingResponsible_Textile    | CurrencyViewer |
      | PurchasingResponsible_Corrugated | CurrencyViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole        |
      | PurchasingResponsible_Flexo      | CurrencyViewer |
      | PurchasingResponsible_Offset     | CurrencyViewer |
      | PurchasingResponsible_Digital    | CurrencyViewer |
      | PurchasingResponsible_Signmedia  | CurrencyViewer |
      | PurchasingResponsible_Textile    | CurrencyViewer |
      | PurchasingResponsible_Corrugated | CurrencyViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Currency" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Currency" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Currency" entity:
      | Subrole        | Permission | Condition |
      | CurrencyViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Currency" entity:
      | Subrole        | Permission | Condition |
      | CurrencyViewer | ReadAll    |           |