Feature: Automated Country Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role          |
      | CountryViewer |
    Then the following roles created successfully:
      | Role          |
      | CountryViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole       |
      #Flexo
      | PurchasingResponsible_Flexo      | CountryViewer |
      #Offset
      | PurchasingResponsible_Offset     | CountryViewer |
      #Digital
      | PurchasingResponsible_Digital    | CountryViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | CountryViewer |
      #Textile
      | PurchasingResponsible_Textile    | CountryViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | CountryViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole       |
      #Flexo
      | PurchasingResponsible_Flexo      | CountryViewer |
      #Offset
      | PurchasingResponsible_Offset     | CountryViewer |
      #Digital
      | PurchasingResponsible_Digital    | CountryViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | CountryViewer |
      #Textile
      | PurchasingResponsible_Textile    | CountryViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | CountryViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Country" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Country" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Country" entity:
      | Subrole       | Permission | Condition |
      | CountryViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Country" entity:
      | Subrole       | Permission | Condition |
      | CountryViewer | ReadAll    |           |

