Feature: Automated Incoterm Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role           |
      | IncotermViewer |
    Then the following roles created successfully:
      | Role           |
      | IncotermViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole        |
      #Flexo
      | PurchasingResponsible_Flexo      | IncotermViewer |
      #Offset
      | PurchasingResponsible_Offset     | IncotermViewer |
      #Digital
      | PurchasingResponsible_Digital    | IncotermViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | IncotermViewer |
      #Textile
      | PurchasingResponsible_Textile    | IncotermViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | IncotermViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole        |
      #Flexo
      | PurchasingResponsible_Flexo      | IncotermViewer |
      #Offset
      | PurchasingResponsible_Offset     | IncotermViewer |
      #Digital
      | PurchasingResponsible_Digital    | IncotermViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | IncotermViewer |
      #Textile
      | PurchasingResponsible_Textile    | IncotermViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | IncotermViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Incoterm" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Incoterm" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Incoterm" entity:
      | Subrole        | Permission | Condition |
      | IncotermViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Incoterm" entity:
      | Subrole        | Permission | Condition |
      | IncotermViewer | ReadAll    |           |

