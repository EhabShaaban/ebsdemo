Feature: Automated Storehouse Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role             |
      | StorehouseViewer |
    Then the following roles created successfully:
      | Role             |
      | StorehouseViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole          |
      | PurchasingResponsible_Flexo      | StorehouseViewer |
      | PurchasingResponsible_Offset     | StorehouseViewer |
      | PurchasingResponsible_Digital    | StorehouseViewer |
      | PurchasingResponsible_Signmedia  | StorehouseViewer |
      | PurchasingResponsible_Textile    | StorehouseViewer |
      | PurchasingResponsible_Corrugated | StorehouseViewer |
      | Storekeeper_Flexo                | StorehouseViewer |
      | Storekeeper_Offset               | StorehouseViewer |
      | Storekeeper_Digital              | StorehouseViewer |
      | Storekeeper_Signmedia            | StorehouseViewer |
      | Storekeeper_Textile              | StorehouseViewer |
      | Storekeeper_Corrugated           | StorehouseViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole          |
      | PurchasingResponsible_Flexo      | StorehouseViewer |
      | PurchasingResponsible_Offset     | StorehouseViewer |
      | PurchasingResponsible_Digital    | StorehouseViewer |
      | PurchasingResponsible_Signmedia  | StorehouseViewer |
      | PurchasingResponsible_Textile    | StorehouseViewer |
      | PurchasingResponsible_Corrugated | StorehouseViewer |
      | Storekeeper_Flexo                | StorehouseViewer |
      | Storekeeper_Offset               | StorehouseViewer |
      | Storekeeper_Digital              | StorehouseViewer |
      | Storekeeper_Signmedia            | StorehouseViewer |
      | Storekeeper_Textile              | StorehouseViewer |
      | Storekeeper_Corrugated           | StorehouseViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Storehouse" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Storehouse" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Storehouse" entity:
      | Subrole          | Permission | Condition |
      | StorehouseViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Storehouse" entity:
      | Subrole          | Permission | Condition |
      | StorehouseViewer | ReadAll    |           |