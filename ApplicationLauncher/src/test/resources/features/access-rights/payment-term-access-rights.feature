Feature: Automated Payment Terms Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role              |
      | PaymentTermViewer |
    Then the following roles created successfully:
      | Role              |
      | PaymentTermViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole           |
      | PurchasingResponsible_Flexo      | PaymentTermViewer |
      | PurchasingResponsible_Offset     | PaymentTermViewer |
      | PurchasingResponsible_Digital    | PaymentTermViewer |
      | PurchasingResponsible_Signmedia  | PaymentTermViewer |
      | PurchasingResponsible_Textile    | PaymentTermViewer |
      | PurchasingResponsible_Corrugated | PaymentTermViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole           |
      | PurchasingResponsible_Flexo      | PaymentTermViewer |
      | PurchasingResponsible_Offset     | PaymentTermViewer |
      | PurchasingResponsible_Digital    | PaymentTermViewer |
      | PurchasingResponsible_Signmedia  | PaymentTermViewer |
      | PurchasingResponsible_Textile    | PaymentTermViewer |
      | PurchasingResponsible_Corrugated | PaymentTermViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "PaymentTerms" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "PaymentTerms" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "PaymentTerms" entity:
      | Subrole           | Permission | Condition |
      | PaymentTermViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "PaymentTerms" entity:
      | Subrole           | Permission | Condition |
      | PaymentTermViewer | ReadAll    |           |