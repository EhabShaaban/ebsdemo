Feature: Automated Users Insertion

  Scenario: Insert users
    Given System admin is logged in
    When System admin wants to create the following users:
      | User                | ProfileName         | Password |
      | Gehan.Ahmed         | Gehan Ahmed         | Test1    |
      | Amany.El-sayed      | Amany El-sayed      | Test2    |
      | Mahmoud.Abdelaziz   | Mahmoud Abdelaziz   | Test3    |
      | Ahmed.Gamal         | Ahmed Gamal         | Test4    |
      | Shady.Abdelatif     | Shady Abdelatif     | Test5    |
      | Mohamed.Abdelmoniem | Mohamed Abdelmoniem | Test6    |
      | Ashraf.Salah        | Ashraf Salah        | Test7    |
      | Essam.Mahmoud       | Essam Mahmoud       | Test8    |
      | Ashraf.Fathi        | Ashraf Fathi        | Test9    |
      | Gamal.Abdelalim     | Gamal Abdelalim     | Test10   |
      | Ahmed.Seif          | Ahmed Seif          | Test11   |
      | Abeer.Ahmed         | Abeer Ahmed         | Test12   |
      | Mahmoud.Fathi       | Mahmoud Fathi       | Test13   |
      | Mostafa.Mehannah    | Mostafa Mehannah    | Test14   |
      | Hagag.Ahmed         | Hagag Ahmed         | Test15   |
    Then the following users created successfully:
      | User                | ProfileName         | Password |
      | Gehan.Ahmed         | Gehan Ahmed         | Test1    |
      | Amany.El-sayed      | Amany El-sayed      | Test2    |
      | Mahmoud.Abdelaziz   | Mahmoud Abdelaziz   | Test3    |
      | Ahmed.Gamal         | Ahmed Gamal         | Test4    |
      | Shady.Abdelatif     | Shady Abdelatif     | Test5    |
      | Mohamed.Abdelmoniem | Mohamed Abdelmoniem | Test6    |
      | Ashraf.Salah        | Ashraf Salah        | Test7    |
      | Essam.Mahmoud       | Essam Mahmoud       | Test8    |
      | Ashraf.Fathi        | Ashraf Fathi        | Test9    |
      | Gamal.Abdelalim     | Gamal Abdelalim     | Test10   |
      | Ahmed.Seif          | Ahmed Seif          | Test11   |
      | Abeer.Ahmed         | Abeer Ahmed         | Test12   |
      | Mahmoud.Fathi       | Mahmoud Fathi       | Test13   |
      | Mostafa.Mehannah    | Mostafa Mehannah    | Test14   |
      | Hagag.Ahmed         | Hagag Ahmed         | Test15   |
