Feature: Automated Item Users Access Rights Insertion


  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                  |
      | ItemOwner_Flexo       |
      | ItemOwner_Offset      |
      | ItemOwner_Digital     |
      | ItemOwner_Signmedia   |
      | ItemOwner_Textile     |
      | ItemOwner_Corrugated  |

      | ItemViewer_Flexo      |
      | ItemViewer_Offset     |
      | ItemViewer_Digital    |
      | ItemViewer_Signmedia  |
      | ItemViewer_Textile    |
      | ItemViewer_Corrugated |

      | ItemViewer            |

    Then the following roles created successfully:
      | Role                  |
      | ItemOwner_Flexo       |
      | ItemOwner_Offset      |
      | ItemOwner_Digital     |
      | ItemOwner_Signmedia   |
      | ItemOwner_Textile     |
      | ItemOwner_Corrugated  |

      | ItemViewer_Flexo      |
      | ItemViewer_Offset     |
      | ItemViewer_Digital    |
      | ItemViewer_Signmedia  |
      | ItemViewer_Textile    |
      | ItemViewer_Corrugated |

      | ItemViewer            |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole               |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo       |
      | PurchasingResponsible_Offset     | ItemOwner_Offset      |
      | PurchasingResponsible_Digital    | ItemOwner_Digital     |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia   |
      | PurchasingResponsible_Textile    | ItemOwner_Textile     |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated  |

      | LogisticsResponsible_Flexo       | ItemViewer_Flexo      |
      | LogisticsResponsible_Offset      | ItemViewer_Offset     |
      | LogisticsResponsible_Digital     | ItemViewer_Digital    |
      | LogisticsResponsible_Signmedia   | ItemViewer_Signmedia  |
      | LogisticsResponsible_Textile     | ItemViewer_Textile    |
      | LogisticsResponsible_Corrugated  | ItemViewer_Corrugated |

      | SalesManager_Flexo               | ItemViewer_Flexo      |
      | SalesManager_Offset              | ItemViewer_Offset     |
      | SalesManager_Digital             | ItemViewer_Digital    |
      | SalesManager_Signmedia           | ItemViewer_Signmedia  |
      | SalesManager_Textile             | ItemViewer_Textile    |
      | SalesManager_Corrugated          | ItemViewer_Corrugated |

      | SalesRepresentative_Flexo        | ItemViewer_Flexo      |
      | SalesRepresentative_Offset       | ItemViewer_Offset     |
      | SalesRepresentative_Digital      | ItemViewer_Digital    |
      | SalesRepresentative_Signmedia    | ItemViewer_Signmedia  |
      | SalesRepresentative_Textile      | ItemViewer_Textile    |
      | SalesRepresentative_Corrugated   | ItemViewer_Corrugated |

      | Marketeer_Flexo                  | ItemViewer_Flexo      |
      | Marketeer_Offset                 | ItemViewer_Offset     |
      | Marketeer_Digital                | ItemViewer_Digital    |
      | Marketeer_Signmedia              | ItemViewer_Signmedia  |
      | Marketeer_Textile                | ItemViewer_Textile    |
      | Marketeer_Corrugated             | ItemViewer_Corrugated |

      | Storekeeper_Flexo                | ItemViewer_Flexo      |
      | Storekeeper_Offset               | ItemViewer_Offset     |
      | Storekeeper_Digital              | ItemViewer_Digital    |
      | Storekeeper_Signmedia            | ItemViewer_Signmedia  |
      | Storekeeper_Textile              | ItemViewer_Textile    |
      | Storekeeper_Corrugated           | ItemViewer_Corrugated |

      | Accountant_Flexo                 | ItemViewer_Flexo      |
      | Accountant_Offset                | ItemViewer_Offset     |
      | Accountant_Digital               | ItemViewer_Digital    |
      | Accountant_Signmedia             | ItemViewer_Signmedia  |
      | Accountant_Textile               | ItemViewer_Textile    |
      | Accountant_Corrugated            | ItemViewer_Corrugated |

      | AccountantHead_Flexo             | ItemViewer_Flexo      |
      | AccountantHead_Offset            | ItemViewer_Offset     |
      | AccountantHead_Digital           | ItemViewer_Digital    |
      | AccountantHead_Signmedia         | ItemViewer_Signmedia  |
      | AccountantHead_Textile           | ItemViewer_Textile    |
      | AccountantHead_Corrugated        | ItemViewer_Corrugated |

      | B.U.Director_Flexo               | ItemViewer_Flexo      |
      | B.U.Director_Offset              | ItemViewer_Offset     |
      | B.U.Director_Digital             | ItemViewer_Digital    |
      | B.U.Director_Signmedia           | ItemViewer_Signmedia  |
      | B.U.Director_Textile             | ItemViewer_Textile    |
      | B.U.Director_Corrugated          | ItemViewer_Corrugated |

      | M.D                              | ItemViewer            |
      | Deputy_M.D                       | ItemViewer            |
      | Quality_Specialist               | ItemViewer            |
      | Auditor                          | ItemViewer            |
      | Corporate_Taxes_Accountant       | ItemViewer            |
      | Corporate_Accounting_Head        | ItemViewer            |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole               |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo       |
      | PurchasingResponsible_Offset     | ItemOwner_Offset      |
      | PurchasingResponsible_Digital    | ItemOwner_Digital     |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia   |
      | PurchasingResponsible_Textile    | ItemOwner_Textile     |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated  |

      | LogisticsResponsible_Flexo       | ItemViewer_Flexo      |
      | LogisticsResponsible_Offset      | ItemViewer_Offset     |
      | LogisticsResponsible_Digital     | ItemViewer_Digital    |
      | LogisticsResponsible_Signmedia   | ItemViewer_Signmedia  |
      | LogisticsResponsible_Textile     | ItemViewer_Textile    |
      | LogisticsResponsible_Corrugated  | ItemViewer_Corrugated |

      | SalesManager_Flexo               | ItemViewer_Flexo      |
      | SalesManager_Offset              | ItemViewer_Offset     |
      | SalesManager_Digital             | ItemViewer_Digital    |
      | SalesManager_Signmedia           | ItemViewer_Signmedia  |
      | SalesManager_Textile             | ItemViewer_Textile    |
      | SalesManager_Corrugated          | ItemViewer_Corrugated |

      | SalesRepresentative_Flexo        | ItemViewer_Flexo      |
      | SalesRepresentative_Offset       | ItemViewer_Offset     |
      | SalesRepresentative_Digital      | ItemViewer_Digital    |
      | SalesRepresentative_Signmedia    | ItemViewer_Signmedia  |
      | SalesRepresentative_Textile      | ItemViewer_Textile    |
      | SalesRepresentative_Corrugated   | ItemViewer_Corrugated |

      | Marketeer_Flexo                  | ItemViewer_Flexo      |
      | Marketeer_Offset                 | ItemViewer_Offset     |
      | Marketeer_Digital                | ItemViewer_Digital    |
      | Marketeer_Signmedia              | ItemViewer_Signmedia  |
      | Marketeer_Textile                | ItemViewer_Textile    |
      | Marketeer_Corrugated             | ItemViewer_Corrugated |

      | Storekeeper_Flexo                | ItemViewer_Flexo      |
      | Storekeeper_Offset               | ItemViewer_Offset     |
      | Storekeeper_Digital              | ItemViewer_Digital    |
      | Storekeeper_Signmedia            | ItemViewer_Signmedia  |
      | Storekeeper_Textile              | ItemViewer_Textile    |
      | Storekeeper_Corrugated           | ItemViewer_Corrugated |

      | Accountant_Flexo                 | ItemViewer_Flexo      |
      | Accountant_Offset                | ItemViewer_Offset     |
      | Accountant_Digital               | ItemViewer_Digital    |
      | Accountant_Signmedia             | ItemViewer_Signmedia  |
      | Accountant_Textile               | ItemViewer_Textile    |
      | Accountant_Corrugated            | ItemViewer_Corrugated |

      | AccountantHead_Flexo             | ItemViewer_Flexo      |
      | AccountantHead_Offset            | ItemViewer_Offset     |
      | AccountantHead_Digital           | ItemViewer_Digital    |
      | AccountantHead_Signmedia         | ItemViewer_Signmedia  |
      | AccountantHead_Textile           | ItemViewer_Textile    |
      | AccountantHead_Corrugated        | ItemViewer_Corrugated |

      | B.U.Director_Flexo               | ItemViewer_Flexo      |
      | B.U.Director_Offset              | ItemViewer_Offset     |
      | B.U.Director_Digital             | ItemViewer_Digital    |
      | B.U.Director_Signmedia           | ItemViewer_Signmedia  |
      | B.U.Director_Textile             | ItemViewer_Textile    |
      | B.U.Director_Corrugated          | ItemViewer_Corrugated |

      | M.D                              | ItemViewer            |
      | Deputy_M.D                       | ItemViewer            |
      | Quality_Specialist               | ItemViewer            |
      | Auditor                          | ItemViewer            |
      | Corporate_Taxes_Accountant       | ItemViewer            |
      | Corporate_Accounting_Head        | ItemViewer            |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Item" entity permissions:
      | Permission        |
      | Create            |
      | Delete            |
      | ReadAll           |
      | ReadGeneralData   |
      | UpdateGeneralData |
      | ReadUoMData       |
      | UpdateUoMData     |
    Then the following "Item" entity permissions created successfully:
      | Permission        |
      | Create            |
      | Delete            |
      | ReadAll           |
      | ReadGeneralData   |
      | UpdateGeneralData |
      | ReadUoMData       |
      | UpdateUoMData     |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "Item" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "Item" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Item" entity:
      | Subrole               | Permission        | Condition                               |
      | ItemViewer            | ReadAll           |                                         |
      | ItemViewer            | ReadGeneralData   |                                         |
      | ItemViewer            | ReadUoMData       |                                         |

      | ItemOwner_Flexo       | Create            |                                         |
      | ItemOwner_Offset      | Create            |                                         |
      | ItemOwner_Digital     | Create            |                                         |
      | ItemOwner_Signmedia   | Create            |                                         |
      | ItemOwner_Textile     | Create            |                                         |
      | ItemOwner_Corrugated  | Create            |                                         |

      | ItemOwner_Flexo       | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | ReadUoMData       | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | UpdateGeneralData | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | UpdateUoMData     | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | Delete            | [purchaseUnitName='Flexible Packaging'] |

      | ItemOwner_Offset      | ReadAll           | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | ReadUoMData       | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | UpdateGeneralData | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | UpdateUoMData     | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | Delete            | [purchaseUnitName='Offset']             |

      | ItemOwner_Digital     | ReadAll           | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | ReadUoMData       | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | UpdateGeneralData | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | UpdateUoMData     | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | Delete            | [purchaseUnitName='Digital']            |

      | ItemOwner_Signmedia   | ReadAll           | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | ReadUoMData       | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | UpdateGeneralData | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | UpdateUoMData     | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | Delete            | [purchaseUnitName='Signmedia']          |

      | ItemOwner_Textile     | ReadAll           | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | ReadUoMData       | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | UpdateGeneralData | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | UpdateUoMData     | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | Delete            | [purchaseUnitName='Textile']            |

      | ItemOwner_Corrugated  | ReadAll           | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | ReadUoMData       | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | UpdateGeneralData | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | UpdateUoMData     | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | Delete            | [purchaseUnitName='Corrugated']         |


      | ItemViewer_Flexo      | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | ItemViewer_Flexo      | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | ItemViewer_Flexo      | ReadUoMData       | [purchaseUnitName='Flexible Packaging'] |

      | ItemViewer_Offset     | ReadAll           | [purchaseUnitName='Offset']             |
      | ItemViewer_Offset     | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | ItemViewer_Offset     | ReadUoMData       | [purchaseUnitName='Offset']             |

      | ItemViewer_Digital    | ReadAll           | [purchaseUnitName='Digital']            |
      | ItemViewer_Digital    | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | ItemViewer_Digital    | ReadUoMData       | [purchaseUnitName='Digital']            |

      | ItemViewer_Signmedia  | ReadAll           | [purchaseUnitName='Signmedia']          |
      | ItemViewer_Signmedia  | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | ItemViewer_Signmedia  | ReadUoMData       | [purchaseUnitName='Signmedia']          |

      | ItemViewer_Textile    | ReadAll           | [purchaseUnitName='Textile']            |
      | ItemViewer_Textile    | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | ItemViewer_Textile    | ReadUoMData       | [purchaseUnitName='Textile']            |

      | ItemViewer_Corrugated | ReadAll           | [purchaseUnitName='Corrugated']         |
      | ItemViewer_Corrugated | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | ItemViewer_Corrugated | ReadUoMData       | [purchaseUnitName='Corrugated']         |


    Then the following roles , permissions and conditions assignment created successfully for "Item" entity:
      | Subrole               | Permission        | Condition                               |
      | ItemViewer            | ReadAll           |                                         |
      | ItemViewer            | ReadGeneralData   |                                         |
      | ItemViewer            | ReadUoMData       |                                         |

      | ItemOwner_Flexo       | Create            |                                         |
      | ItemOwner_Offset      | Create            |                                         |
      | ItemOwner_Digital     | Create            |                                         |
      | ItemOwner_Signmedia   | Create            |                                         |
      | ItemOwner_Textile     | Create            |                                         |
      | ItemOwner_Corrugated  | Create            |                                         |

      | ItemOwner_Flexo       | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | ReadUoMData       | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | UpdateGeneralData | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | UpdateUoMData     | [purchaseUnitName='Flexible Packaging'] |
      | ItemOwner_Flexo       | Delete            | [purchaseUnitName='Flexible Packaging'] |

      | ItemOwner_Offset      | ReadAll           | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | ReadUoMData       | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | UpdateGeneralData | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | UpdateUoMData     | [purchaseUnitName='Offset']             |
      | ItemOwner_Offset      | Delete            | [purchaseUnitName='Offset']             |

      | ItemOwner_Digital     | ReadAll           | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | ReadUoMData       | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | UpdateGeneralData | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | UpdateUoMData     | [purchaseUnitName='Digital']            |
      | ItemOwner_Digital     | Delete            | [purchaseUnitName='Digital']            |

      | ItemOwner_Signmedia   | ReadAll           | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | ReadUoMData       | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | UpdateGeneralData | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | UpdateUoMData     | [purchaseUnitName='Signmedia']          |
      | ItemOwner_Signmedia   | Delete            | [purchaseUnitName='Signmedia']          |

      | ItemOwner_Textile     | ReadAll           | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | ReadUoMData       | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | UpdateGeneralData | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | UpdateUoMData     | [purchaseUnitName='Textile']            |
      | ItemOwner_Textile     | Delete            | [purchaseUnitName='Textile']            |

      | ItemOwner_Corrugated  | ReadAll           | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | ReadUoMData       | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | UpdateGeneralData | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | UpdateUoMData     | [purchaseUnitName='Corrugated']         |
      | ItemOwner_Corrugated  | Delete            | [purchaseUnitName='Corrugated']         |


      | ItemViewer_Flexo      | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | ItemViewer_Flexo      | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | ItemViewer_Flexo      | ReadUoMData       | [purchaseUnitName='Flexible Packaging'] |

      | ItemViewer_Offset     | ReadAll           | [purchaseUnitName='Offset']             |
      | ItemViewer_Offset     | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | ItemViewer_Offset     | ReadUoMData       | [purchaseUnitName='Offset']             |

      | ItemViewer_Digital    | ReadAll           | [purchaseUnitName='Digital']            |
      | ItemViewer_Digital    | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | ItemViewer_Digital    | ReadUoMData       | [purchaseUnitName='Digital']            |

      | ItemViewer_Signmedia  | ReadAll           | [purchaseUnitName='Signmedia']          |
      | ItemViewer_Signmedia  | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | ItemViewer_Signmedia  | ReadUoMData       | [purchaseUnitName='Signmedia']          |

      | ItemViewer_Textile    | ReadAll           | [purchaseUnitName='Textile']            |
      | ItemViewer_Textile    | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | ItemViewer_Textile    | ReadUoMData       | [purchaseUnitName='Textile']            |

      | ItemViewer_Corrugated | ReadAll           | [purchaseUnitName='Corrugated']         |
      | ItemViewer_Corrugated | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | ItemViewer_Corrugated | ReadUoMData       | [purchaseUnitName='Corrugated']         |

