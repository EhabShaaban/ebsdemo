Feature: Automated PurchasingUnit Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                     |
      | PurUnitReader_Flexo      |
      | PurUnitReader_Offset     |
      | PurUnitReader_Digital    |
      | PurUnitReader_Signmedia  |
      | PurUnitReader_Textile    |
      | PurUnitReader_Corrugated |
      | PurUnitReader            |
    Then the following roles created successfully:
      | Role                     |
      | PurUnitReader_Flexo      |
      | PurUnitReader_Offset     |
      | PurUnitReader_Digital    |
      | PurUnitReader_Signmedia  |
      | PurUnitReader_Textile    |
      | PurUnitReader_Corrugated |
      | PurUnitReader            |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                  |
            #Flexo
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | LogisticsResponsible_Flexo       | PurUnitReader_Flexo      |
      | Storekeeper_Flexo                | PurUnitReader_Flexo      |
      | Accountant_Flexo                 | PurUnitReader_Flexo      |
      | SalesManager_Flexo               | PurUnitReader_Flexo      |
      | B.U.Director_Flexo               | PurUnitReader_Flexo      |
      | AccountantHead_Flexo             | PurUnitReader_Flexo      |
      | BudgetController_Flexo           | PurUnitReader_Flexo      |
            #Offset
      | PurchasingResponsible_Offset     | PurUnitReader_Offset     |
      | LogisticsResponsible_Offset      | PurUnitReader_Offset     |
      | Storekeeper_Offset               | PurUnitReader_Offset     |
      | Accountant_Offset                | PurUnitReader_Offset     |
      | SalesManager_Offset              | PurUnitReader_Offset     |
      | B.U.Director_Offset              | PurUnitReader_Offset     |
      | AccountantHead_Offset            | PurUnitReader_Offset     |
      | BudgetController_Offset          | PurUnitReader_Offset     |
            #Digital
      | PurchasingResponsible_Digital    | PurUnitReader_Digital    |
      | LogisticsResponsible_Digital     | PurUnitReader_Digital    |
      | Storekeeper_Digital              | PurUnitReader_Digital    |
      | Accountant_Digital               | PurUnitReader_Digital    |
      | SalesManager_Digital             | PurUnitReader_Digital    |
      | B.U.Director_Digital             | PurUnitReader_Digital    |
      | AccountantHead_Digital           | PurUnitReader_Digital    |
      | BudgetController_Digital         | PurUnitReader_Digital    |
            #Signmedia
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | LogisticsResponsible_Signmedia   | PurUnitReader_Signmedia  |
      | Storekeeper_Signmedia            | PurUnitReader_Signmedia  |
      | Accountant_Signmedia             | PurUnitReader_Signmedia  |
      | SalesManager_Signmedia           | PurUnitReader_Signmedia  |
      | B.U.Director_Signmedia           | PurUnitReader_Signmedia  |
      | AccountantHead_Signmedia         | PurUnitReader_Signmedia  |
      | BudgetController_Signmedia       | PurUnitReader_Signmedia  |
            #Textile
      | PurchasingResponsible_Textile    | PurUnitReader_Textile    |
      | LogisticsResponsible_Textile     | PurUnitReader_Textile    |
      | Storekeeper_Textile              | PurUnitReader_Textile    |
      | Accountant_Textile               | PurUnitReader_Textile    |
      | SalesManager_Textile             | PurUnitReader_Textile    |
      | B.U.Director_Textile             | PurUnitReader_Textile    |
      | AccountantHead_Textile           | PurUnitReader_Textile    |
      | BudgetController_Textile         | PurUnitReader_Textile    |
            #Corrugated
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | LogisticsResponsible_Corrugated  | PurUnitReader_Corrugated |
      | Storekeeper_Corrugated           | PurUnitReader_Corrugated |
      | Accountant_Corrugated            | PurUnitReader_Corrugated |
      | SalesManager_Corrugated          | PurUnitReader_Corrugated |
      | B.U.Director_Corrugated          | PurUnitReader_Corrugated |
      | AccountantHead_Corrugated        | PurUnitReader_Corrugated |
      | BudgetController_Corrugated      | PurUnitReader_Corrugated |
            #General
      | M.D                              | PurUnitReader            |
      | Deputy_M.D                       | PurUnitReader            |
      | Quality_Specialist               | PurUnitReader            |
      | Auditor                          | PurUnitReader            |
      | Corporate_Taxes_Accountant       | PurUnitReader            |
      | Corporate_Accounting_Head        | PurUnitReader            |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | LogisticsResponsible_Flexo       | PurUnitReader_Flexo      |
      | Storekeeper_Flexo                | PurUnitReader_Flexo      |
      | Accountant_Flexo                 | PurUnitReader_Flexo      |
      | SalesManager_Flexo               | PurUnitReader_Flexo      |
      | B.U.Director_Flexo               | PurUnitReader_Flexo      |
      | AccountantHead_Flexo             | PurUnitReader_Flexo      |
      | BudgetController_Flexo           | PurUnitReader_Flexo      |
            #Offset
      | PurchasingResponsible_Offset     | PurUnitReader_Offset     |
      | LogisticsResponsible_Offset      | PurUnitReader_Offset     |
      | Storekeeper_Offset               | PurUnitReader_Offset     |
      | Accountant_Offset                | PurUnitReader_Offset     |
      | SalesManager_Offset              | PurUnitReader_Offset     |
      | B.U.Director_Offset              | PurUnitReader_Offset     |
      | AccountantHead_Offset            | PurUnitReader_Offset     |
      | BudgetController_Offset          | PurUnitReader_Offset     |
            #Digital
      | PurchasingResponsible_Digital    | PurUnitReader_Digital    |
      | LogisticsResponsible_Digital     | PurUnitReader_Digital    |
      | Storekeeper_Digital              | PurUnitReader_Digital    |
      | Accountant_Digital               | PurUnitReader_Digital    |
      | SalesManager_Digital             | PurUnitReader_Digital    |
      | B.U.Director_Digital             | PurUnitReader_Digital    |
      | AccountantHead_Digital           | PurUnitReader_Digital    |
      | BudgetController_Digital         | PurUnitReader_Digital    |
            #Signmedia
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | LogisticsResponsible_Signmedia   | PurUnitReader_Signmedia  |
      | Storekeeper_Signmedia            | PurUnitReader_Signmedia  |
      | Accountant_Signmedia             | PurUnitReader_Signmedia  |
      | SalesManager_Signmedia           | PurUnitReader_Signmedia  |
      | B.U.Director_Signmedia           | PurUnitReader_Signmedia  |
      | AccountantHead_Signmedia         | PurUnitReader_Signmedia  |
      | BudgetController_Signmedia       | PurUnitReader_Signmedia  |
            #Textile
      | PurchasingResponsible_Textile    | PurUnitReader_Textile    |
      | LogisticsResponsible_Textile     | PurUnitReader_Textile    |
      | Storekeeper_Textile              | PurUnitReader_Textile    |
      | Accountant_Textile               | PurUnitReader_Textile    |
      | SalesManager_Textile             | PurUnitReader_Textile    |
      | B.U.Director_Textile             | PurUnitReader_Textile    |
      | AccountantHead_Textile           | PurUnitReader_Textile    |
      | BudgetController_Textile         | PurUnitReader_Textile    |
            #Corrugated
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | LogisticsResponsible_Corrugated  | PurUnitReader_Corrugated |
      | Storekeeper_Corrugated           | PurUnitReader_Corrugated |
      | Accountant_Corrugated            | PurUnitReader_Corrugated |
      | SalesManager_Corrugated          | PurUnitReader_Corrugated |
      | B.U.Director_Corrugated          | PurUnitReader_Corrugated |
      | AccountantHead_Corrugated        | PurUnitReader_Corrugated |
      | BudgetController_Corrugated      | PurUnitReader_Corrugated |
            #General
      | M.D                              | PurUnitReader            |
      | Deputy_M.D                       | PurUnitReader            |
      | Quality_Specialist               | PurUnitReader            |
      | Auditor                          | PurUnitReader            |
      | Corporate_Taxes_Accountant       | PurUnitReader            |
      | Corporate_Accounting_Head        | PurUnitReader            |


  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "PurchasingUnit" entity permissions:
      | Permission    |
      | ReadAll       |
      | ReadAll_ForPO |
    Then the following "PurchasingUnit" entity permissions created successfully:
      | Permission    |
      | ReadAll       |
      | ReadAll_ForPO |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "PurchasingUnit" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "PurchasingUnit" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "PurchasingUnit" entity:
      | Subrole                  | Permission    | Condition                               |
      | PurUnitReader_Flexo      | ReadAll       |                                         |
      | PurUnitReader_Flexo      | ReadAll_ForPO | [purchaseUnitName='Flexible Packaging'] |

      | PurUnitReader_Offset     | ReadAll       |                                         |
      | PurUnitReader_Offset     | ReadAll_ForPO | [purchaseUnitName='Offset']             |

      | PurUnitReader_Digital    | ReadAll       |                                         |
      | PurUnitReader_Digital    | ReadAll_ForPO | [purchaseUnitName='Digital']            |

      | PurUnitReader_Signmedia  | ReadAll       |                                         |
      | PurUnitReader_Signmedia  | ReadAll_ForPO | [purchaseUnitName='Signmedia']          |

      | PurUnitReader_Textile    | ReadAll       |                                         |
      | PurUnitReader_Textile    | ReadAll_ForPO | [purchaseUnitName='Textile']            |

      | PurUnitReader_Corrugated | ReadAll       |                                         |
      | PurUnitReader_Corrugated | ReadAll_ForPO | [purchaseUnitName='Corrugated']         |

      | PurUnitReader            | ReadAll       |                                         |

    Then the following roles , permissions and conditions assignment created successfully for "PurchasingUnit" entity:
      | Subrole                  | Permission    | Condition                               |
      | PurUnitReader_Flexo      | ReadAll       |                                         |
      | PurUnitReader_Flexo      | ReadAll_ForPO | [purchaseUnitName='Flexible Packaging'] |

      | PurUnitReader_Offset     | ReadAll       |                                         |
      | PurUnitReader_Offset     | ReadAll_ForPO | [purchaseUnitName='Offset']             |

      | PurUnitReader_Digital    | ReadAll       |                                         |
      | PurUnitReader_Digital    | ReadAll_ForPO | [purchaseUnitName='Digital']            |

      | PurUnitReader_Signmedia  | ReadAll       |                                         |
      | PurUnitReader_Signmedia  | ReadAll_ForPO | [purchaseUnitName='Signmedia']          |

      | PurUnitReader_Textile    | ReadAll       |                                         |
      | PurUnitReader_Textile    | ReadAll_ForPO | [purchaseUnitName='Textile']            |

      | PurUnitReader_Corrugated | ReadAll       |                                         |
      | PurUnitReader_Corrugated | ReadAll_ForPO | [purchaseUnitName='Corrugated']         |

      | PurUnitReader            | ReadAll       |                                         |
