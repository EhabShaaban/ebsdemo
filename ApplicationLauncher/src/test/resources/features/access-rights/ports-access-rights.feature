Feature: Automated Port Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role       |
      | PortViewer |
    Then the following roles created successfully:
      | Role       |
      | PortViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole    |
      #Flexo
      | PurchasingResponsible_Flexo      | PortViewer |
      #Offset
      | PurchasingResponsible_Offset     | PortViewer |
      #Digital
      | PurchasingResponsible_Digital    | PortViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | PortViewer |
      #Textile
      | PurchasingResponsible_Textile    | PortViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | PortViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole    |
      #Flexo
      | PurchasingResponsible_Flexo      | PortViewer |
      #Offset
      | PurchasingResponsible_Offset     | PortViewer |
      #Digital
      | PurchasingResponsible_Digital    | PortViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | PortViewer |
      #Textile
      | PurchasingResponsible_Textile    | PortViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | PortViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Port" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Port" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Port" entity:
      | Subrole    | Permission | Condition |
      | PortViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Port" entity:
      | Subrole    | Permission | Condition |
      | PortViewer | ReadAll    |           |

