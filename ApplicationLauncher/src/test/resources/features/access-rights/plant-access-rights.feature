Feature: Automated Plant Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role        |
      | PlantViewer |
    Then the following roles created successfully:
      | Role        |
      | PlantViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole     |
      | PurchasingResponsible_Flexo      | PlantViewer |
      | PurchasingResponsible_Offset     | PlantViewer |
      | PurchasingResponsible_Digital    | PlantViewer |
      | PurchasingResponsible_Signmedia  | PlantViewer |
      | PurchasingResponsible_Textile    | PlantViewer |
      | PurchasingResponsible_Corrugated | PlantViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole     |
      | PurchasingResponsible_Flexo      | PlantViewer |
      | PurchasingResponsible_Offset     | PlantViewer |
      | PurchasingResponsible_Digital    | PlantViewer |
      | PurchasingResponsible_Signmedia  | PlantViewer |
      | PurchasingResponsible_Textile    | PlantViewer |
      | PurchasingResponsible_Corrugated | PlantViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Plant" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Plant" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Plant" entity:
      | Subrole     | Permission | Condition |
      | PlantViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Plant" entity:
      | Subrole     | Permission | Condition |
      | PlantViewer | ReadAll    |           |