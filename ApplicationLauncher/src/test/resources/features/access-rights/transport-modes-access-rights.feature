Feature: Automated ModeofTransport Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                  |
      | ModeofTransportViewer |
    Then the following roles created successfully:
      | Role                  |
      | ModeofTransportViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole               |
      #Flexo
      | PurchasingResponsible_Flexo      | ModeofTransportViewer |
      #Offset
      | PurchasingResponsible_Offset     | ModeofTransportViewer |
      #Digital
      | PurchasingResponsible_Digital    | ModeofTransportViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | ModeofTransportViewer |
      #Textile
      | PurchasingResponsible_Textile    | ModeofTransportViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | ModeofTransportViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole               |
      #Flexo
      | PurchasingResponsible_Flexo      | ModeofTransportViewer |
      #Offset
      | PurchasingResponsible_Offset     | ModeofTransportViewer |
      #Digital
      | PurchasingResponsible_Digital    | ModeofTransportViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | ModeofTransportViewer |
      #Textile
      | PurchasingResponsible_Textile    | ModeofTransportViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | ModeofTransportViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ModeOfTransport" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "ModeOfTransport" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ModeOfTransport" entity:
      | Subrole               | Permission | Condition |
      | ModeofTransportViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "ModeOfTransport" entity:
      | Subrole               | Permission | Condition |
      | ModeofTransportViewer | ReadAll    |           |

