Feature: Automated PARENT ROLES Insertion
  #This is only for PARNET ROLES
  Scenario: Insert Parent Roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                             |
      | PurchasingResponsible_Signmedia  |
      | LogisticsResponsible_Signmedia   |
      | Marketeer_Signmedia              |
      | SalesManager_Signmedia           |
      | Storekeeper_Signmedia            |
      | SalesRepresentative_Signmedia    |
      | Accountant_Signmedia             |
      | BudgetController_Signmedia       |
      | AccountantHead_Signmedia         |
      | B.U.Director_Signmedia           |

      | PurchasingResponsible_Flexo      |
      | LogisticsResponsible_Flexo       |
      | Marketeer_Flexo                  |
      | SalesManager_Flexo               |
      | Storekeeper_Flexo                |
      | SalesRepresentative_Flexo        |
      | Accountant_Flexo                 |
      | BudgetController_Flexo           |
      | AccountantHead_Flexo             |
      | B.U.Director_Flexo               |

      | PurchasingResponsible_Offset     |
      | LogisticsResponsible_Offset      |
      | Marketeer_Offset                 |
      | SalesManager_Offset              |
      | Storekeeper_Offset               |
      | SalesRepresentative_Offset       |
      | Accountant_Offset                |
      | BudgetController_Offset          |
      | AccountantHead_Offset            |
      | B.U.Director_Offset              |

      | PurchasingResponsible_Digital    |
      | LogisticsResponsible_Digital     |
      | Marketeer_Digital                |
      | SalesManager_Digital             |
      | Storekeeper_Digital              |
      | SalesRepresentative_Digital      |
      | Accountant_Digital               |
      | BudgetController_Digital         |
      | AccountantHead_Digital           |
      | B.U.Director_Digital             |

      | PurchasingResponsible_Textile    |
      | LogisticsResponsible_Textile     |
      | Marketeer_Textile                |
      | SalesManager_Textile             |
      | Storekeeper_Textile              |
      | SalesRepresentative_Textile      |
      | Accountant_Textile               |
      | BudgetController_Textile         |
      | AccountantHead_Textile           |
      | B.U.Director_Textile             |

      | PurchasingResponsible_Corrugated |
      | LogisticsResponsible_Corrugated  |
      | Marketeer_Corrugated             |
      | SalesManager_Corrugated          |
      | Storekeeper_Corrugated           |
      | SalesRepresentative_Corrugated   |
      | Accountant_Corrugated            |
      | BudgetController_Corrugated      |
      | AccountantHead_Corrugated        |
      | B.U.Director_Corrugated          |

      | M.D                              |
      | Deputy_M.D                       |
      | Quality_Specialist               |
      | Auditor                          |
      | Corporate_Taxes_Accountant       |
      | Corporate_Accounting_Head        |

    Then the following roles created successfully:
      | Role                             |
      | PurchasingResponsible_Signmedia  |
      | LogisticsResponsible_Signmedia   |
      | Marketeer_Signmedia              |
      | SalesManager_Signmedia           |
      | Storekeeper_Signmedia            |
      | SalesRepresentative_Signmedia    |
      | Accountant_Signmedia             |
      | BudgetController_Signmedia       |
      | AccountantHead_Signmedia         |
      | B.U.Director_Signmedia           |

      | PurchasingResponsible_Flexo      |
      | LogisticsResponsible_Flexo       |
      | Marketeer_Flexo                  |
      | SalesManager_Flexo               |
      | Storekeeper_Flexo                |
      | SalesRepresentative_Flexo        |
      | Accountant_Flexo                 |
      | BudgetController_Flexo           |
      | AccountantHead_Flexo             |
      | B.U.Director_Flexo               |

      | PurchasingResponsible_Offset     |
      | LogisticsResponsible_Offset      |
      | Marketeer_Offset                 |
      | SalesManager_Offset              |
      | Storekeeper_Offset               |
      | SalesRepresentative_Offset       |
      | Accountant_Offset                |
      | BudgetController_Offset          |
      | AccountantHead_Offset            |
      | B.U.Director_Offset              |

      | PurchasingResponsible_Digital    |
      | LogisticsResponsible_Digital     |
      | Marketeer_Digital                |
      | SalesManager_Digital             |
      | Storekeeper_Digital              |
      | SalesRepresentative_Digital      |
      | Accountant_Digital               |
      | BudgetController_Digital         |
      | AccountantHead_Digital           |
      | B.U.Director_Digital             |

      | PurchasingResponsible_Textile    |
      | LogisticsResponsible_Textile     |
      | Marketeer_Textile                |
      | SalesManager_Textile             |
      | Storekeeper_Textile              |
      | SalesRepresentative_Textile      |
      | Accountant_Textile               |
      | BudgetController_Textile         |
      | AccountantHead_Textile           |
      | B.U.Director_Textile             |

      | PurchasingResponsible_Corrugated |
      | LogisticsResponsible_Corrugated  |
      | Marketeer_Corrugated             |
      | SalesManager_Corrugated          |
      | Storekeeper_Corrugated           |
      | SalesRepresentative_Corrugated   |
      | Accountant_Corrugated            |
      | BudgetController_Corrugated      |
      | AccountantHead_Corrugated        |
      | B.U.Director_Corrugated          |

      | M.D                              |
      | Deputy_M.D                       |
      | Quality_Specialist               |
      | Auditor                          |
      | Corporate_Taxes_Accountant       |
      | Corporate_Accounting_Head        |