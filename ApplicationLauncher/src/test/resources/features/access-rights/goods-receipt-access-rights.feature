Feature: Automated GR Users Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                                |
      | GRViewer_Flexo                      |
      | GRViewer_Offset                     |
      | GRViewer_Digital                    |
      | GRViewer_Signmedia                  |
      | GRViewer_Textile                    |
      | GRViewer_Corrugated                 |

      | GRDifferenceReasonEditor_Flexo      |
      | GRDifferenceReasonEditor_Offset     |
      | GRDifferenceReasonEditor_Digital    |
      | GRDifferenceReasonEditor_Signmedia  |
      | GRDifferenceReasonEditor_Textile    |
      | GRDifferenceReasonEditor_Corrugated |

      | GROwner_Flexo                       |
      | GROwner_Offset                      |
      | GROwner_Digital                     |
      | GROwner_Signmedia                   |
      | GROwner_Textile                     |
      | GROwner_Corrugated                  |

      | GRViewer                            |

    Then the following roles created successfully:
      | Role                                |
      | GRViewer_Flexo                      |
      | GRViewer_Offset                     |
      | GRViewer_Digital                    |
      | GRViewer_Signmedia                  |
      | GRViewer_Textile                    |
      | GRViewer_Corrugated                 |

      | GRDifferenceReasonEditor_Flexo      |
      | GRDifferenceReasonEditor_Offset     |
      | GRDifferenceReasonEditor_Digital    |
      | GRDifferenceReasonEditor_Signmedia  |
      | GRDifferenceReasonEditor_Textile    |
      | GRDifferenceReasonEditor_Corrugated |

      | GROwner_Flexo                       |
      | GROwner_Offset                      |
      | GROwner_Digital                     |
      | GROwner_Signmedia                   |
      | GROwner_Textile                     |
      | GROwner_Corrugated                  |

      | GRViewer                            |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                             |
      | PurchasingResponsible_Flexo      | GRViewer_Flexo                      |
      | PurchasingResponsible_Offset     | GRViewer_Offset                     |
      | PurchasingResponsible_Digital    | GRViewer_Digital                    |
      | PurchasingResponsible_Signmedia  | GRViewer_Signmedia                  |
      | PurchasingResponsible_Textile    | GRViewer_Textile                    |
      | PurchasingResponsible_Corrugated | GRViewer_Corrugated                 |

      | PurchasingResponsible_Flexo      | GRDifferenceReasonEditor_Flexo      |
      | PurchasingResponsible_Offset     | GRDifferenceReasonEditor_Offset     |
      | PurchasingResponsible_Digital    | GRDifferenceReasonEditor_Digital    |
      | PurchasingResponsible_Signmedia  | GRDifferenceReasonEditor_Signmedia  |
      | PurchasingResponsible_Textile    | GRDifferenceReasonEditor_Textile    |
      | PurchasingResponsible_Corrugated | GRDifferenceReasonEditor_Corrugated |

      | LogisticsResponsible_Flexo       | GRViewer_Flexo                      |
      | LogisticsResponsible_Offset      | GRViewer_Offset                     |
      | LogisticsResponsible_Digital     | GRViewer_Digital                    |
      | LogisticsResponsible_Signmedia   | GRViewer_Signmedia                  |
      | LogisticsResponsible_Textile     | GRViewer_Textile                    |
      | LogisticsResponsible_Corrugated  | GRViewer_Corrugated                 |

      | LogisticsResponsible_Flexo       | GRDifferenceReasonEditor_Flexo      |
      | LogisticsResponsible_Offset      | GRDifferenceReasonEditor_Offset     |
      | LogisticsResponsible_Digital     | GRDifferenceReasonEditor_Digital    |
      | LogisticsResponsible_Signmedia   | GRDifferenceReasonEditor_Signmedia  |
      | LogisticsResponsible_Textile     | GRDifferenceReasonEditor_Textile    |
      | LogisticsResponsible_Corrugated  | GRDifferenceReasonEditor_Corrugated |

      | Storekeeper_Flexo                | GROwner_Flexo                       |
      | Storekeeper_Offset               | GROwner_Offset                      |
      | Storekeeper_Digital              | GROwner_Digital                     |
      | Storekeeper_Signmedia            | GROwner_Signmedia                   |
      | Storekeeper_Textile              | GROwner_Textile                     |
      | Storekeeper_Corrugated           | GROwner_Corrugated                  |

      | Accountant_Flexo                 | GRViewer_Flexo                      |
      | Accountant_Offset                | GRViewer_Offset                     |
      | Accountant_Digital               | GRViewer_Digital                    |
      | Accountant_Signmedia             | GRViewer_Signmedia                  |
      | Accountant_Textile               | GRViewer_Textile                    |
      | Accountant_Corrugated            | GRViewer_Corrugated                 |

      | Marketeer_Flexo                  | GRViewer_Flexo                      |
      | Marketeer_Offset                 | GRViewer_Offset                     |
      | Marketeer_Digital                | GRViewer_Digital                    |
      | Marketeer_Signmedia              | GRViewer_Signmedia                  |
      | Marketeer_Textile                | GRViewer_Textile                    |
      | Marketeer_Corrugated             | GRViewer_Corrugated                 |

      | B.U.Director_Flexo               | GRViewer_Flexo                      |
      | B.U.Director_Offset              | GRViewer_Offset                     |
      | B.U.Director_Digital             | GRViewer_Digital                    |
      | B.U.Director_Signmedia           | GRViewer_Signmedia                  |
      | B.U.Director_Textile             | GRViewer_Textile                    |
      | B.U.Director_Corrugated          | GRViewer_Corrugated                 |

      | M.D                              | GRViewer                            |
      | Deputy_M.D                       | GRViewer                            |
      | Quality_Specialist               | GRViewer                            |
      | Auditor                          | GRViewer                            |
      | Corporate_Taxes_Accountant       | GRViewer                            |
      | Corporate_Accounting_Head        | GRViewer                            |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                             |
      | PurchasingResponsible_Flexo      | GRViewer_Flexo                      |
      | PurchasingResponsible_Offset     | GRViewer_Offset                     |
      | PurchasingResponsible_Digital    | GRViewer_Digital                    |
      | PurchasingResponsible_Signmedia  | GRViewer_Signmedia                  |
      | PurchasingResponsible_Textile    | GRViewer_Textile                    |
      | PurchasingResponsible_Corrugated | GRViewer_Corrugated                 |

      | PurchasingResponsible_Flexo      | GRDifferenceReasonEditor_Flexo      |
      | PurchasingResponsible_Offset     | GRDifferenceReasonEditor_Offset     |
      | PurchasingResponsible_Digital    | GRDifferenceReasonEditor_Digital    |
      | PurchasingResponsible_Signmedia  | GRDifferenceReasonEditor_Signmedia  |
      | PurchasingResponsible_Textile    | GRDifferenceReasonEditor_Textile    |
      | PurchasingResponsible_Corrugated | GRDifferenceReasonEditor_Corrugated |

      | LogisticsResponsible_Flexo       | GRViewer_Flexo                      |
      | LogisticsResponsible_Offset      | GRViewer_Offset                     |
      | LogisticsResponsible_Digital     | GRViewer_Digital                    |
      | LogisticsResponsible_Signmedia   | GRViewer_Signmedia                  |
      | LogisticsResponsible_Textile     | GRViewer_Textile                    |
      | LogisticsResponsible_Corrugated  | GRViewer_Corrugated                 |

      | LogisticsResponsible_Flexo       | GRDifferenceReasonEditor_Flexo      |
      | LogisticsResponsible_Offset      | GRDifferenceReasonEditor_Offset     |
      | LogisticsResponsible_Digital     | GRDifferenceReasonEditor_Digital    |
      | LogisticsResponsible_Signmedia   | GRDifferenceReasonEditor_Signmedia  |
      | LogisticsResponsible_Textile     | GRDifferenceReasonEditor_Textile    |
      | LogisticsResponsible_Corrugated  | GRDifferenceReasonEditor_Corrugated |

      | Storekeeper_Flexo                | GROwner_Flexo                       |
      | Storekeeper_Offset               | GROwner_Offset                      |
      | Storekeeper_Digital              | GROwner_Digital                     |
      | Storekeeper_Signmedia            | GROwner_Signmedia                   |
      | Storekeeper_Textile              | GROwner_Textile                     |
      | Storekeeper_Corrugated           | GROwner_Corrugated                  |

      | Accountant_Flexo                 | GRViewer_Flexo                      |
      | Accountant_Offset                | GRViewer_Offset                     |
      | Accountant_Digital               | GRViewer_Digital                    |
      | Accountant_Signmedia             | GRViewer_Signmedia                  |
      | Accountant_Textile               | GRViewer_Textile                    |
      | Accountant_Corrugated            | GRViewer_Corrugated                 |

      | Marketeer_Flexo                  | GRViewer_Flexo                      |
      | Marketeer_Offset                 | GRViewer_Offset                     |
      | Marketeer_Digital                | GRViewer_Digital                    |
      | Marketeer_Signmedia              | GRViewer_Signmedia                  |
      | Marketeer_Textile                | GRViewer_Textile                    |
      | Marketeer_Corrugated             | GRViewer_Corrugated                 |

      | B.U.Director_Flexo               | GRViewer_Flexo                      |
      | B.U.Director_Offset              | GRViewer_Offset                     |
      | B.U.Director_Digital             | GRViewer_Digital                    |
      | B.U.Director_Signmedia           | GRViewer_Signmedia                  |
      | B.U.Director_Textile             | GRViewer_Textile                    |
      | B.U.Director_Corrugated          | GRViewer_Corrugated                 |

      | M.D                              | GRViewer                            |
      | Deputy_M.D                       | GRViewer                            |
      | Quality_Specialist               | GRViewer                            |
      | Auditor                          | GRViewer                            |
      | Corporate_Taxes_Accountant       | GRViewer                            |
      | Corporate_Accounting_Head        | GRViewer                            |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "GoodsReceipt" entity permissions:
      | Permission          |
      | Create              |
      | ReadAll             |
      | ReadHeader          |
      | UpdateHeader        |
      | ReadPOData          |
      | ReadItems           |
      | UpdateItem          |
      | Delete              |
      | Post                |
      | ExportPDF           |
      | EditDifferentReason |

    Then the following "GoodsReceipt" entity permissions created successfully:
      | Permission          |
      | Create              |
      | ReadAll             |
      | ReadHeader          |
      | UpdateHeader        |
      | ReadPOData          |
      | ReadItems           |
      | UpdateItem          |
      | Delete              |
      | Post                |
      | ExportPDF           |
      | EditDifferentReason |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "GoodsReceipt" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "GoodsReceipt" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "GoodsReceipt" entity:
      | Subrole                             | Permission          | Condition                               |
      | GROwner_Flexo                       | Create              |                                         |
      | GROwner_Offset                      | Create              |                                         |
      | GROwner_Digital                     | Create              |                                         |
      | GROwner_Signmedia                   | Create              |                                         |
      | GROwner_Textile                     | Create              |                                         |
      | GROwner_Corrugated                  | Create              |                                         |

      | GROwner_Flexo                       | ReadAll             | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadAll             | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadAll             | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadAll             | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadAll             | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadAll             | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadHeader          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadHeader          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadHeader          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadHeader          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadHeader          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadHeader          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadPOData          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadPOData          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadPOData          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadPOData          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadPOData          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadPOData          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadItems           | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadItems           | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadItems           | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadItems           | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadItems           | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadItems           | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | UpdateHeader        | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | UpdateHeader        | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | UpdateHeader        | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | UpdateHeader        | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | UpdateHeader        | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | UpdateHeader        | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | UpdateItem          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | UpdateItem          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | UpdateItem          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | UpdateItem          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | UpdateItem          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | UpdateItem          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | Delete              | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | Delete              | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | Delete              | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | Delete              | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | Delete              | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | Delete              | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | Post                | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | Post                | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | Post                | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | Post                | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | Post                | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | Post                | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ExportPDF           | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ExportPDF           | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ExportPDF           | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ExportPDF           | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ExportPDF           | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ExportPDF           | [purchaseUnitName='Corrugated']         |

      | GRDifferenceReasonEditor_Flexo      | EditDifferentReason | [purchaseUnitName='Flexible Packaging'] |
      | GRDifferenceReasonEditor_Offset     | EditDifferentReason | [purchaseUnitName='Offset']             |
      | GRDifferenceReasonEditor_Digital    | EditDifferentReason | [purchaseUnitName='Digital']            |
      | GRDifferenceReasonEditor_Signmedia  | EditDifferentReason | [purchaseUnitName='Signmedia']          |
      | GRDifferenceReasonEditor_Textile    | EditDifferentReason | [purchaseUnitName='Textile']            |
      | GRDifferenceReasonEditor_Corrugated | EditDifferentReason | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadAll             |                                         |
      | GRViewer_Flexo                      | ReadAll             | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadAll             | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadAll             | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadAll             | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadAll             | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadAll             | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadHeader          |                                         |
      | GRViewer_Flexo                      | ReadHeader          | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadHeader          | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadHeader          | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadHeader          | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadHeader          | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadHeader          | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadPOData          |                                         |
      | GRViewer_Flexo                      | ReadPOData          | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadPOData          | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadPOData          | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadPOData          | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadPOData          | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadPOData          | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadItems           |                                         |
      | GRViewer_Flexo                      | ReadItems           | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadItems           | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadItems           | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadItems           | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadItems           | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadItems           | [purchaseUnitName='Corrugated']         |

    Then the following roles , permissions and conditions assignment created successfully for "GoodsReceipt" entity:
      | Subrole                             | Permission          | Condition                               |
      | GROwner_Flexo                       | Create              |                                         |
      | GROwner_Offset                      | Create              |                                         |
      | GROwner_Digital                     | Create              |                                         |
      | GROwner_Signmedia                   | Create              |                                         |
      | GROwner_Textile                     | Create              |                                         |
      | GROwner_Corrugated                  | Create              |                                         |

      | GROwner_Flexo                       | ReadAll             | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadAll             | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadAll             | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadAll             | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadAll             | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadAll             | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadHeader          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadHeader          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadHeader          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadHeader          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadHeader          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadHeader          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadPOData          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadPOData          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadPOData          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadPOData          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadPOData          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadPOData          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ReadItems           | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ReadItems           | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ReadItems           | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ReadItems           | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ReadItems           | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ReadItems           | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | UpdateHeader        | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | UpdateHeader        | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | UpdateHeader        | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | UpdateHeader        | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | UpdateHeader        | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | UpdateHeader        | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | UpdateItem          | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | UpdateItem          | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | UpdateItem          | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | UpdateItem          | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | UpdateItem          | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | UpdateItem          | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | Delete              | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | Delete              | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | Delete              | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | Delete              | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | Delete              | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | Delete              | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | Post                | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | Post                | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | Post                | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | Post                | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | Post                | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | Post                | [purchaseUnitName='Corrugated']         |

      | GROwner_Flexo                       | ExportPDF           | [purchaseUnitName='Flexible Packaging'] |
      | GROwner_Offset                      | ExportPDF           | [purchaseUnitName='Offset']             |
      | GROwner_Digital                     | ExportPDF           | [purchaseUnitName='Digital']            |
      | GROwner_Signmedia                   | ExportPDF           | [purchaseUnitName='Signmedia']          |
      | GROwner_Textile                     | ExportPDF           | [purchaseUnitName='Textile']            |
      | GROwner_Corrugated                  | ExportPDF           | [purchaseUnitName='Corrugated']         |

      | GRDifferenceReasonEditor_Flexo      | EditDifferentReason | [purchaseUnitName='Flexible Packaging'] |
      | GRDifferenceReasonEditor_Offset     | EditDifferentReason | [purchaseUnitName='Offset']             |
      | GRDifferenceReasonEditor_Digital    | EditDifferentReason | [purchaseUnitName='Digital']            |
      | GRDifferenceReasonEditor_Signmedia  | EditDifferentReason | [purchaseUnitName='Signmedia']          |
      | GRDifferenceReasonEditor_Textile    | EditDifferentReason | [purchaseUnitName='Textile']            |
      | GRDifferenceReasonEditor_Corrugated | EditDifferentReason | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadAll             |                                         |
      | GRViewer_Flexo                      | ReadAll             | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadAll             | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadAll             | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadAll             | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadAll             | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadAll             | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadHeader          |                                         |
      | GRViewer_Flexo                      | ReadHeader          | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadHeader          | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadHeader          | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadHeader          | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadHeader          | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadHeader          | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadPOData          |                                         |
      | GRViewer_Flexo                      | ReadPOData          | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadPOData          | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadPOData          | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadPOData          | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadPOData          | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadPOData          | [purchaseUnitName='Corrugated']         |

      | GRViewer                            | ReadItems           |                                         |
      | GRViewer_Flexo                      | ReadItems           | [purchaseUnitName='Flexible Packaging'] |
      | GRViewer_Offset                     | ReadItems           | [purchaseUnitName='Offset']             |
      | GRViewer_Digital                    | ReadItems           | [purchaseUnitName='Digital']            |
      | GRViewer_Signmedia                  | ReadItems           | [purchaseUnitName='Signmedia']          |
      | GRViewer_Textile                    | ReadItems           | [purchaseUnitName='Textile']            |
      | GRViewer_Corrugated                 | ReadItems           | [purchaseUnitName='Corrugated']         |