Feature: Automated Storekeeper Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role              |
      | StorekeeperViewer |
    Then the following roles created successfully:
      | Role              |
      | StorekeeperViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                   | Subrole           |
      | Storekeeper_Flexo      | StorekeeperViewer |
      | Storekeeper_Offset     | StorekeeperViewer |
      | Storekeeper_Digital    | StorekeeperViewer |
      | Storekeeper_Signmedia  | StorekeeperViewer |
      | Storekeeper_Textile    | StorekeeperViewer |
      | Storekeeper_Corrugated | StorekeeperViewer |
    Then the following parent roles are assigned to subroles:
      | Role                   | Subrole           |
      | Storekeeper_Flexo      | StorekeeperViewer |
      | Storekeeper_Offset     | StorekeeperViewer |
      | Storekeeper_Digital    | StorekeeperViewer |
      | Storekeeper_Signmedia  | StorekeeperViewer |
      | Storekeeper_Textile    | StorekeeperViewer |
      | Storekeeper_Corrugated | StorekeeperViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Storekeeper" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Storekeeper" entity permissions created successfully:
      | Permission |
      | ReadAll    |


  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Storekeeper" entity:
      | Subrole           | Permission | Condition |
      | StorekeeperViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Storekeeper" entity:
      | Subrole           | Permission | Condition |
      | StorekeeperViewer | ReadAll    |           |