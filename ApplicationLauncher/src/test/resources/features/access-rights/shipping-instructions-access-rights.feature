Feature: Automated Payment Terms Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                      |
      | ShippingInstructionViewer |
    Then the following roles created successfully:
      | Role                      |
      | ShippingInstructionViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Flexo      | ShippingInstructionViewer |
      | PurchasingResponsible_Offset     | ShippingInstructionViewer |
      | PurchasingResponsible_Digital    | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia  | ShippingInstructionViewer |
      | PurchasingResponsible_Textile    | ShippingInstructionViewer |
      | PurchasingResponsible_Corrugated | ShippingInstructionViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Flexo      | ShippingInstructionViewer |
      | PurchasingResponsible_Offset     | ShippingInstructionViewer |
      | PurchasingResponsible_Digital    | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia  | ShippingInstructionViewer |
      | PurchasingResponsible_Textile    | ShippingInstructionViewer |
      | PurchasingResponsible_Corrugated | ShippingInstructionViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ShippingInstructions" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "ShippingInstructions" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ShippingInstructions" entity:
      | Subrole                   | Permission | Condition |
      | ShippingInstructionViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "ShippingInstructions" entity:
      | Subrole                   | Permission | Condition |
      | ShippingInstructionViewer | ReadAll    |           |