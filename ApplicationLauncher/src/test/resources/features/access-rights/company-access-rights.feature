Feature: Automated Company Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                              |
      | CompanyViewer_WithoutBankAccounts |
      | CompanyViewer                     |
    Then the following roles created successfully:
      | Role                              |
      | CompanyViewer_WithoutBankAccounts |
      | CompanyViewer                     |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                           |
      | PurchasingResponsible_Flexo      | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Offset     | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Digital    | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Signmedia  | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Textile    | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Corrugated | CompanyViewer_WithoutBankAccounts |

      | Accountant_Flexo                 | CompanyViewer                     |
      | AccountantHead_Flexo             | CompanyViewer                     |

      | Accountant_Offset                | CompanyViewer                     |
      | AccountantHead_Offset            | CompanyViewer                     |

      | Accountant_Digital               | CompanyViewer                     |
      | AccountantHead_Digital           | CompanyViewer                     |

      | Accountant_Signmedia             | CompanyViewer                     |
      | AccountantHead_Signmedia         | CompanyViewer                     |

      | Accountant_Textile               | CompanyViewer                     |
      | AccountantHead_Textile           | CompanyViewer                     |

      | Accountant_Corrugated            | CompanyViewer                     |
      | AccountantHead_Corrugated        | CompanyViewer                     |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                           |
      | PurchasingResponsible_Flexo      | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Offset     | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Digital    | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Signmedia  | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Textile    | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Corrugated | CompanyViewer_WithoutBankAccounts |

      | Accountant_Flexo                 | CompanyViewer                     |
      | AccountantHead_Flexo             | CompanyViewer                     |

      | Accountant_Offset                | CompanyViewer                     |
      | AccountantHead_Offset            | CompanyViewer                     |

      | Accountant_Digital               | CompanyViewer                     |
      | AccountantHead_Digital           | CompanyViewer                     |

      | Accountant_Signmedia             | CompanyViewer                     |
      | AccountantHead_Signmedia         | CompanyViewer                     |

      | Accountant_Textile               | CompanyViewer                     |
      | AccountantHead_Textile           | CompanyViewer                     |

      | Accountant_Corrugated            | CompanyViewer                     |
      | AccountantHead_Corrugated        | CompanyViewer                     |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Company" entity permissions:
      | Permission      |
      | ReadAll         |
      | ReadBankDetails |
    Then the following "Company" entity permissions created successfully:
      | Permission      |
      | ReadAll         |
      | ReadBankDetails |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Company" entity:
      | Subrole                           | Permission      | Condition |
      | CompanyViewer                     | ReadAll         |           |
      | CompanyViewer                     | ReadBankDetails |           |
      | CompanyViewer_WithoutBankAccounts | ReadAll         |           |
    Then the following roles , permissions and conditions assignment created successfully for "Company" entity:
      | Subrole                           | Permission      | Condition |
      | CompanyViewer                     | ReadAll         |           |
      | CompanyViewer                     | ReadBankDetails |           |
      | CompanyViewer_WithoutBankAccounts | ReadAll         |           |

