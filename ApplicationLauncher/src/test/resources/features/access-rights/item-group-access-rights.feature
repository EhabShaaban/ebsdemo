Feature: Automated Storekeeper Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role            |
      | ItemGroupViewer |
    Then the following roles created successfully:
      | Role            |
      | ItemGroupViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole         |
      | PurchasingResponsible_Flexo      | ItemGroupViewer |
      | PurchasingResponsible_Offset     | ItemGroupViewer |
      | PurchasingResponsible_Digital    | ItemGroupViewer |
      | PurchasingResponsible_Signmedia  | ItemGroupViewer |
      | PurchasingResponsible_Textile    | ItemGroupViewer |
      | PurchasingResponsible_Corrugated | ItemGroupViewer |
    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole         |
      | PurchasingResponsible_Flexo      | ItemGroupViewer |
      | PurchasingResponsible_Offset     | ItemGroupViewer |
      | PurchasingResponsible_Digital    | ItemGroupViewer |
      | PurchasingResponsible_Signmedia  | ItemGroupViewer |
      | PurchasingResponsible_Textile    | ItemGroupViewer |
      | PurchasingResponsible_Corrugated | ItemGroupViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ItemGroup" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "ItemGroup" entity permissions created successfully:
      | Permission |
      | ReadAll    |


  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ItemGroup" entity:
      | Subrole         | Permission | Condition |
      | ItemGroupViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "ItemGroup" entity:
      | Subrole         | Permission | Condition |
      | ItemGroupViewer | ReadAll    |           |