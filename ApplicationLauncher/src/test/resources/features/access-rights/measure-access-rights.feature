Feature: Automated Measures Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role           |
      | MeasuresViewer |
    Then the following roles created successfully:
      | Role           |
      | MeasuresViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole        |
      #Flexo
      | PurchasingResponsible_Flexo      | MeasuresViewer |
      | LogisticsResponsible_Flexo       | MeasuresViewer |
      | Storekeeper_Flexo                | MeasuresViewer |
      | Accountant_Flexo                 | MeasuresViewer |
      | AccountantHead_Flexo             | MeasuresViewer |
      | SalesManager_Flexo               | MeasuresViewer |
      | B.U.Director_Flexo               | MeasuresViewer |
      #Offset
      | PurchasingResponsible_Offset     | MeasuresViewer |
      | LogisticsResponsible_Offset      | MeasuresViewer |
      | Storekeeper_Offset               | MeasuresViewer |
      | Accountant_Offset                | MeasuresViewer |
      | AccountantHead_Offset            | MeasuresViewer |
      | SalesManager_Offset              | MeasuresViewer |
      | B.U.Director_Offset              | MeasuresViewer |
      #Digital
      | PurchasingResponsible_Digital    | MeasuresViewer |
      | LogisticsResponsible_Digital     | MeasuresViewer |
      | Storekeeper_Digital              | MeasuresViewer |
      | Accountant_Digital               | MeasuresViewer |
      | AccountantHead_Digital           | MeasuresViewer |
      | SalesManager_Digital             | MeasuresViewer |
      | B.U.Director_Digital             | MeasuresViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | MeasuresViewer |
      | LogisticsResponsible_Signmedia   | MeasuresViewer |
      | Storekeeper_Signmedia            | MeasuresViewer |
      | Accountant_Signmedia             | MeasuresViewer |
      | AccountantHead_Signmedia         | MeasuresViewer |
      | SalesManager_Signmedia           | MeasuresViewer |
      | B.U.Director_Signmedia           | MeasuresViewer |
      #Textile
      | PurchasingResponsible_Textile    | MeasuresViewer |
      | LogisticsResponsible_Textile     | MeasuresViewer |
      | Storekeeper_Textile              | MeasuresViewer |
      | Accountant_Textile               | MeasuresViewer |
      | AccountantHead_Textile           | MeasuresViewer |
      | SalesManager_Textile             | MeasuresViewer |
      | B.U.Director_Textile             | MeasuresViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | MeasuresViewer |
      | LogisticsResponsible_Corrugated  | MeasuresViewer |
      | Storekeeper_Corrugated           | MeasuresViewer |
      | Accountant_Corrugated            | MeasuresViewer |
      | AccountantHead_Corrugated        | MeasuresViewer |
      | SalesManager_Corrugated          | MeasuresViewer |
      | B.U.Director_Corrugated          | MeasuresViewer |
      #General
      | M.D                              | MeasuresViewer |
      | Deputy_M.D                       | MeasuresViewer |
      | Quality_Specialist               | MeasuresViewer |
      | Auditor                          | MeasuresViewer |
      | Corporate_Taxes_Accountant       | MeasuresViewer |
      | Corporate_Accounting_Head        | MeasuresViewer |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole        |
      #Flexo
      | PurchasingResponsible_Flexo      | MeasuresViewer |
      | LogisticsResponsible_Flexo       | MeasuresViewer |
      | Storekeeper_Flexo                | MeasuresViewer |
      | Accountant_Flexo                 | MeasuresViewer |
      | AccountantHead_Flexo             | MeasuresViewer |
      | SalesManager_Flexo               | MeasuresViewer |
      | B.U.Director_Flexo               | MeasuresViewer |
      #Offset
      | PurchasingResponsible_Offset     | MeasuresViewer |
      | LogisticsResponsible_Offset      | MeasuresViewer |
      | Storekeeper_Offset               | MeasuresViewer |
      | Accountant_Offset                | MeasuresViewer |
      | AccountantHead_Offset            | MeasuresViewer |
      | SalesManager_Offset              | MeasuresViewer |
      | B.U.Director_Offset              | MeasuresViewer |
      #Digital
      | PurchasingResponsible_Digital    | MeasuresViewer |
      | LogisticsResponsible_Digital     | MeasuresViewer |
      | Storekeeper_Digital              | MeasuresViewer |
      | Accountant_Digital               | MeasuresViewer |
      | AccountantHead_Digital           | MeasuresViewer |
      | SalesManager_Digital             | MeasuresViewer |
      | B.U.Director_Digital             | MeasuresViewer |
      #Signmedia
      | PurchasingResponsible_Signmedia  | MeasuresViewer |
      | LogisticsResponsible_Signmedia   | MeasuresViewer |
      | Storekeeper_Signmedia            | MeasuresViewer |
      | Accountant_Signmedia             | MeasuresViewer |
      | AccountantHead_Signmedia         | MeasuresViewer |
      | SalesManager_Signmedia           | MeasuresViewer |
      | B.U.Director_Signmedia           | MeasuresViewer |
      #Textile
      | PurchasingResponsible_Textile    | MeasuresViewer |
      | LogisticsResponsible_Textile     | MeasuresViewer |
      | Storekeeper_Textile              | MeasuresViewer |
      | Accountant_Textile               | MeasuresViewer |
      | AccountantHead_Textile           | MeasuresViewer |
      | SalesManager_Textile             | MeasuresViewer |
      | B.U.Director_Textile             | MeasuresViewer |
      #Corrugated
      | PurchasingResponsible_Corrugated | MeasuresViewer |
      | LogisticsResponsible_Corrugated  | MeasuresViewer |
      | Storekeeper_Corrugated           | MeasuresViewer |
      | Accountant_Corrugated            | MeasuresViewer |
      | AccountantHead_Corrugated        | MeasuresViewer |
      | SalesManager_Corrugated          | MeasuresViewer |
      | B.U.Director_Corrugated          | MeasuresViewer |
      #General
      | M.D                              | MeasuresViewer |
      | Deputy_M.D                       | MeasuresViewer |
      | Quality_Specialist               | MeasuresViewer |
      | Auditor                          | MeasuresViewer |
      | Corporate_Taxes_Accountant       | MeasuresViewer |
      | Corporate_Accounting_Head        | MeasuresViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Measures" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "Measures" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Measures" entity:
      | Subrole        | Permission | Condition |
      | MeasuresViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "Measures" entity:
      | Subrole        | Permission | Condition |
      | MeasuresViewer | ReadAll    |           |

