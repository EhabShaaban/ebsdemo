Feature: Automated Users & Parent roles Assignment

  Scenario: Assign Users to Parent roles
    Given System admin is logged in
    When System admin wants to assign the following users to parent roles:
      | User                | Role                            |
      | Gehan.Ahmed         | PurchasingResponsible_Signmedia |
      | Gehan.Ahmed         | LogisticsResponsible_Signmedia  |
      | Gehan.Ahmed         | Marketeer_Signmedia             |
      | Gehan.Ahmed         | SalesManager_Signmedia          |

      | Amany.El-sayed      | PurchasingResponsible_Signmedia |
      | Amany.El-sayed      | LogisticsResponsible_Signmedia  |
      | Amany.El-sayed      | Marketeer_Signmedia             |
      | Amany.El-sayed      | SalesRepresentative_Signmedia   |

      | Mahmoud.Abdelaziz   | Storekeeper_Signmedia           |
      | Mahmoud.Abdelaziz   | SalesRepresentative_Signmedia   |

      | Ahmed.Gamal         | Storekeeper_Signmedia           |

      | Shady.Abdelatif     | Accountant_Signmedia            |

      | Mohamed.Abdelmoniem | BudgetController_Signmedia      |

      | Ashraf.Salah        | AccountantHead_Signmedia        |
      | Ashraf.Salah        | Corporate_Taxes_Accountant      |
      | Ashraf.Salah        | Corporate_Accounting_Head       |

      | Essam.Mahmoud       | B.U.Director_Signmedia          |

      | Ashraf.Fathi        | M.D                             |

      | Gamal.Abdelalim     | Deputy_M.D                      |

      | Ahmed.Seif          | Quality_Specialist              |
      | Ahmed.Seif          | Auditor                         |

      | Abeer.Ahmed         | Quality_Specialist              |
      | Abeer.Ahmed         | Auditor                         |

      | Mahmoud.Fathi       | Quality_Specialist              |

      | Mostafa.Mehannah    | Auditor                         |

      | Hagag.Ahmed         | Corporate_Taxes_Accountant      |


    Then the following users are assigned to parent roles:
      | User                | Role                            |
      | Gehan.Ahmed         | PurchasingResponsible_Signmedia |
      | Gehan.Ahmed         | LogisticsResponsible_Signmedia  |
      | Gehan.Ahmed         | Marketeer_Signmedia             |
      | Gehan.Ahmed         | SalesManager_Signmedia          |

      | Amany.El-sayed      | PurchasingResponsible_Signmedia |
      | Amany.El-sayed      | LogisticsResponsible_Signmedia  |
      | Amany.El-sayed      | Marketeer_Signmedia             |
      | Amany.El-sayed      | SalesRepresentative_Signmedia   |

      | Mahmoud.Abdelaziz   | Storekeeper_Signmedia           |
      | Mahmoud.Abdelaziz   | SalesRepresentative_Signmedia   |

      | Ahmed.Gamal         | Storekeeper_Signmedia           |

      | Shady.Abdelatif     | Accountant_Signmedia            |

      | Mohamed.Abdelmoniem | BudgetController_Signmedia      |

      | Ashraf.Salah        | AccountantHead_Signmedia        |
      | Ashraf.Salah        | Corporate_Taxes_Accountant      |
      | Ashraf.Salah        | Corporate_Accounting_Head       |

      | Essam.Mahmoud       | B.U.Director_Signmedia          |

      | Ashraf.Fathi        | M.D                             |

      | Gamal.Abdelalim     | Deputy_M.D                      |

      | Ahmed.Seif          | Quality_Specialist              |
      | Ahmed.Seif          | Auditor                         |

      | Abeer.Ahmed         | Quality_Specialist              |
      | Abeer.Ahmed         | Auditor                         |

      | Mahmoud.Fathi       | Quality_Specialist              |

      | Mostafa.Mehannah    | Auditor                         |

      | Hagag.Ahmed         | Corporate_Taxes_Accountant      |