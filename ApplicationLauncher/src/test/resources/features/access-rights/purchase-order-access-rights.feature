Feature: Automated PO Users Access Rights Insertion


  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                             |
      | POOwner_Flexo                    |
      | POOwner_Offset                   |
      | POOwner_Digital                  |
      | POOwner_Signmedia                |
      | POOwner_Textile                  |
      | POOwner_Corrugated               |

      | POViewer_Flexo                   |
      | POViewer_Offset                  |
      | POViewer_Digital                 |
      | POViewer_Signmedia               |
      | POViewer_Textile                 |
      | POViewer_Corrugated              |

      | POViewer                         |

      | POLogisticsOwner_Flexo           |
      | POLogisticsOwner_Offset          |
      | POLogisticsOwner_Digital         |
      | POLogisticsOwner_Signmedia       |
      | POLogisticsOwner_Textile         |
      | POLogisticsOwner_Corrugated      |

      | POViewerLimited_Flexo            |
      | POViewerLimited_Offset           |
      | POViewerLimited_Digital          |
      | POViewerLimited_Signmedia        |
      | POViewerLimited_Textile          |
      | POViewerLimited_Corrugated       |

      | POApprover_Flexo                 |
      | POApprover_Offset                |
      | POApprover_Digital               |
      | POApprover_Signmedia             |
      | POApprover_Textile               |
      | POApprover_Corrugated            |

      | POPaymentDecsionMaker_Flexo      |
      | POPaymentDecsionMaker_Offset     |
      | POPaymentDecsionMaker_Digital    |
      | POPaymentDecsionMaker_Signmedia  |
      | POPaymentDecsionMaker_Textile    |
      | POPaymentDecsionMaker_Corrugated |


    Then the following roles created successfully:
      | Role                             |
      | POOwner_Flexo                    |
      | POOwner_Offset                   |
      | POOwner_Digital                  |
      | POOwner_Signmedia                |
      | POOwner_Textile                  |
      | POOwner_Corrugated               |

      | POViewer_Flexo                   |
      | POViewer_Offset                  |
      | POViewer_Digital                 |
      | POViewer_Signmedia               |
      | POViewer_Textile                 |
      | POViewer_Corrugated              |

      | POViewer                         |

      | POLogisticsOwner_Flexo           |
      | POLogisticsOwner_Offset          |
      | POLogisticsOwner_Digital         |
      | POLogisticsOwner_Signmedia       |
      | POLogisticsOwner_Textile         |
      | POLogisticsOwner_Corrugated      |

      | POViewerLimited_Flexo            |
      | POViewerLimited_Offset           |
      | POViewerLimited_Digital          |
      | POViewerLimited_Signmedia        |
      | POViewerLimited_Textile          |
      | POViewerLimited_Corrugated       |

      | POApprover_Flexo                 |
      | POApprover_Offset                |
      | POApprover_Digital               |
      | POApprover_Signmedia             |
      | POApprover_Textile               |
      | POApprover_Corrugated            |

      | POPaymentDecsionMaker_Flexo      |
      | POPaymentDecsionMaker_Offset     |
      | POPaymentDecsionMaker_Digital    |
      | POPaymentDecsionMaker_Signmedia  |
      | POPaymentDecsionMaker_Textile    |
      | POPaymentDecsionMaker_Corrugated |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                          |
      | PurchasingResponsible_Flexo      | POOwner_Flexo                    |
      | PurchasingResponsible_Offset     | POOwner_Offset                   |
      | PurchasingResponsible_Digital    | POOwner_Digital                  |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia                |
      | PurchasingResponsible_Textile    | POOwner_Textile                  |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated               |

      | LogisticsResponsible_Flexo       | POViewer_Flexo                   |
      | LogisticsResponsible_Offset      | POViewer_Offset                  |
      | LogisticsResponsible_Digital     | POViewer_Digital                 |
      | LogisticsResponsible_Signmedia   | POViewer_Signmedia               |
      | LogisticsResponsible_Textile     | POViewer_Textile                 |
      | LogisticsResponsible_Corrugated  | POViewer_Corrugated              |

      | LogisticsResponsible_Flexo       | POLogisticsOwner_Flexo           |
      | LogisticsResponsible_Offset      | POLogisticsOwner_Offset          |
      | LogisticsResponsible_Digital     | POLogisticsOwner_Digital         |
      | LogisticsResponsible_Signmedia   | POLogisticsOwner_Signmedia       |
      | LogisticsResponsible_Textile     | POLogisticsOwner_Textile         |
      | LogisticsResponsible_Corrugated  | POLogisticsOwner_Corrugated      |

      | Storekeeper_Flexo                | POViewerLimited_Flexo            |
      | Storekeeper_Offset               | POViewerLimited_Offset           |
      | Storekeeper_Digital              | POViewerLimited_Digital          |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia        |
      | Storekeeper_Textile              | POViewerLimited_Textile          |
      | Storekeeper_Corrugated           | POViewerLimited_Corrugated       |

      | Accountant_Flexo                 | POViewer_Flexo                   |
      | Accountant_Offset                | POViewer_Offset                  |
      | Accountant_Digital               | POViewer_Digital                 |
      | Accountant_Signmedia             | POViewer_Signmedia               |
      | Accountant_Textile               | POViewer_Textile                 |
      | Accountant_Corrugated            | POViewer_Corrugated              |

      | Accountant_Flexo                 | POApprover_Flexo                 |
      | Accountant_Offset                | POApprover_Offset                |
      | Accountant_Digital               | POApprover_Digital               |
      | Accountant_Signmedia             | POApprover_Signmedia             |
      | Accountant_Textile               | POApprover_Textile               |
      | Accountant_Corrugated            | POApprover_Corrugated            |

      | PurchasingResponsible_Flexo      | POApprover_Flexo                 |
      | PurchasingResponsible_Offset     | POApprover_Offset                |
      | PurchasingResponsible_Digital    | POApprover_Digital               |
      | PurchasingResponsible_Signmedia  | POApprover_Signmedia             |
      | PurchasingResponsible_Textile    | POApprover_Textile               |
      | PurchasingResponsible_Corrugated | POApprover_Corrugated            |

      | Accountant_Flexo                 | POPaymentDecsionMaker_Flexo      |
      | Accountant_Offset                | POPaymentDecsionMaker_Offset     |
      | Accountant_Digital               | POPaymentDecsionMaker_Digital    |
      | Accountant_Signmedia             | POPaymentDecsionMaker_Signmedia  |
      | Accountant_Textile               | POPaymentDecsionMaker_Textile    |
      | Accountant_Corrugated            | POPaymentDecsionMaker_Corrugated |

      | BudgetController_Flexo           | POViewer_Flexo                   |
      | BudgetController_Offset          | POViewer_Offset                  |
      | BudgetController_Digital         | POViewer_Digital                 |
      | BudgetController_Signmedia       | POViewer_Signmedia               |
      | BudgetController_Textile         | POViewer_Textile                 |
      | BudgetController_Corrugated      | POViewer_Corrugated              |

      | BudgetController_Flexo           | POApprover_Flexo                 |
      | BudgetController_Offset          | POApprover_Offset                |
      | BudgetController_Digital         | POApprover_Digital               |
      | BudgetController_Signmedia       | POApprover_Signmedia             |
      | BudgetController_Textile         | POApprover_Textile               |
      | BudgetController_Corrugated      | POApprover_Corrugated            |

      | AccountantHead_Flexo             | POViewer_Flexo                   |
      | AccountantHead_Offset            | POViewer_Offset                  |
      | AccountantHead_Digital           | POViewer_Digital                 |
      | AccountantHead_Signmedia         | POViewer_Signmedia               |
      | AccountantHead_Textile           | POViewer_Textile                 |
      | AccountantHead_Corrugated        | POViewer_Corrugated              |

      | AccountantHead_Flexo             | POApprover_Flexo                 |
      | AccountantHead_Offset            | POApprover_Offset                |
      | AccountantHead_Digital           | POApprover_Digital               |
      | AccountantHead_Signmedia         | POApprover_Signmedia             |
      | AccountantHead_Textile           | POApprover_Textile               |
      | AccountantHead_Corrugated        | POApprover_Corrugated            |

      | AccountantHead_Flexo             | POPaymentDecsionMaker_Flexo      |
      | AccountantHead_Offset            | POPaymentDecsionMaker_Offset     |
      | AccountantHead_Digital           | POPaymentDecsionMaker_Digital    |
      | AccountantHead_Signmedia         | POPaymentDecsionMaker_Signmedia  |
      | AccountantHead_Textile           | POPaymentDecsionMaker_Textile    |
      | AccountantHead_Corrugated        | POPaymentDecsionMaker_Corrugated |

      | SalesManager_Flexo               | POViewer_Flexo                   |
      | SalesManager_Offset              | POViewer_Offset                  |
      | SalesManager_Digital             | POViewer_Digital                 |
      | SalesManager_Signmedia           | POViewer_Signmedia               |
      | SalesManager_Textile             | POViewer_Textile                 |
      | SalesManager_Corrugated          | POViewer_Corrugated              |

      | SalesManager_Flexo               | POApprover_Flexo                 |
      | SalesManager_Offset              | POApprover_Offset                |
      | SalesManager_Digital             | POApprover_Digital               |
      | SalesManager_Signmedia           | POApprover_Signmedia             |
      | SalesManager_Textile             | POApprover_Textile               |
      | SalesManager_Corrugated          | POApprover_Corrugated            |

      | B.U.Director_Flexo               | POViewer_Flexo                   |
      | B.U.Director_Offset              | POViewer_Offset                  |
      | B.U.Director_Digital             | POViewer_Digital                 |
      | B.U.Director_Signmedia           | POViewer_Signmedia               |
      | B.U.Director_Textile             | POViewer_Textile                 |
      | B.U.Director_Corrugated          | POViewer_Corrugated              |

      | B.U.Director_Flexo               | POApprover_Flexo                 |
      | B.U.Director_Offset              | POApprover_Offset                |
      | B.U.Director_Digital             | POApprover_Digital               |
      | B.U.Director_Signmedia           | POApprover_Signmedia             |
      | B.U.Director_Textile             | POApprover_Textile               |
      | B.U.Director_Corrugated          | POApprover_Corrugated            |

      | M.D                              | POViewer                         |
      | Deputy_M.D                       | POViewer                         |
      | Quality_Specialist               | POViewer                         |
      | Auditor                          | POViewer                         |
      | Corporate_Taxes_Accountant       | POViewer                         |
      | Corporate_Accounting_Head        | POViewer                         |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                          |
      | PurchasingResponsible_Flexo      | POOwner_Flexo                    |
      | PurchasingResponsible_Offset     | POOwner_Offset                   |
      | PurchasingResponsible_Digital    | POOwner_Digital                  |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia                |
      | PurchasingResponsible_Textile    | POOwner_Textile                  |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated               |

      | LogisticsResponsible_Flexo       | POViewer_Flexo                   |
      | LogisticsResponsible_Offset      | POViewer_Offset                  |
      | LogisticsResponsible_Digital     | POViewer_Digital                 |
      | LogisticsResponsible_Signmedia   | POViewer_Signmedia               |
      | LogisticsResponsible_Textile     | POViewer_Textile                 |
      | LogisticsResponsible_Corrugated  | POViewer_Corrugated              |

      | LogisticsResponsible_Flexo       | POLogisticsOwner_Flexo           |
      | LogisticsResponsible_Offset      | POLogisticsOwner_Offset          |
      | LogisticsResponsible_Digital     | POLogisticsOwner_Digital         |
      | LogisticsResponsible_Signmedia   | POLogisticsOwner_Signmedia       |
      | LogisticsResponsible_Textile     | POLogisticsOwner_Textile         |
      | LogisticsResponsible_Corrugated  | POLogisticsOwner_Corrugated      |

      | Storekeeper_Flexo                | POViewerLimited_Flexo            |
      | Storekeeper_Offset               | POViewerLimited_Offset           |
      | Storekeeper_Digital              | POViewerLimited_Digital          |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia        |
      | Storekeeper_Textile              | POViewerLimited_Textile          |
      | Storekeeper_Corrugated           | POViewerLimited_Corrugated       |

      | Accountant_Flexo                 | POViewer_Flexo                   |
      | Accountant_Offset                | POViewer_Offset                  |
      | Accountant_Digital               | POViewer_Digital                 |
      | Accountant_Signmedia             | POViewer_Signmedia               |
      | Accountant_Textile               | POViewer_Textile                 |
      | Accountant_Corrugated            | POViewer_Corrugated              |

      | Accountant_Flexo                 | POApprover_Flexo                 |
      | Accountant_Offset                | POApprover_Offset                |
      | Accountant_Digital               | POApprover_Digital               |
      | Accountant_Signmedia             | POApprover_Signmedia             |
      | Accountant_Textile               | POApprover_Textile               |
      | Accountant_Corrugated            | POApprover_Corrugated            |

      | PurchasingResponsible_Flexo      | POApprover_Flexo                 |
      | PurchasingResponsible_Offset     | POApprover_Offset                |
      | PurchasingResponsible_Digital    | POApprover_Digital               |
      | PurchasingResponsible_Signmedia  | POApprover_Signmedia             |
      | PurchasingResponsible_Textile    | POApprover_Textile               |
      | PurchasingResponsible_Corrugated | POApprover_Corrugated            |

      | Accountant_Flexo                 | POPaymentDecsionMaker_Flexo      |
      | Accountant_Offset                | POPaymentDecsionMaker_Offset     |
      | Accountant_Digital               | POPaymentDecsionMaker_Digital    |
      | Accountant_Signmedia             | POPaymentDecsionMaker_Signmedia  |
      | Accountant_Textile               | POPaymentDecsionMaker_Textile    |
      | Accountant_Corrugated            | POPaymentDecsionMaker_Corrugated |

      | BudgetController_Flexo           | POViewer_Flexo                   |
      | BudgetController_Offset          | POViewer_Offset                  |
      | BudgetController_Digital         | POViewer_Digital                 |
      | BudgetController_Signmedia       | POViewer_Signmedia               |
      | BudgetController_Textile         | POViewer_Textile                 |
      | BudgetController_Corrugated      | POViewer_Corrugated              |

      | BudgetController_Flexo           | POApprover_Flexo                 |
      | BudgetController_Offset          | POApprover_Offset                |
      | BudgetController_Digital         | POApprover_Digital               |
      | BudgetController_Signmedia       | POApprover_Signmedia             |
      | BudgetController_Textile         | POApprover_Textile               |
      | BudgetController_Corrugated      | POApprover_Corrugated            |

      | AccountantHead_Flexo             | POViewer_Flexo                   |
      | AccountantHead_Offset            | POViewer_Offset                  |
      | AccountantHead_Digital           | POViewer_Digital                 |
      | AccountantHead_Signmedia         | POViewer_Signmedia               |
      | AccountantHead_Textile           | POViewer_Textile                 |
      | AccountantHead_Corrugated        | POViewer_Corrugated              |

      | AccountantHead_Flexo             | POApprover_Flexo                 |
      | AccountantHead_Offset            | POApprover_Offset                |
      | AccountantHead_Digital           | POApprover_Digital               |
      | AccountantHead_Signmedia         | POApprover_Signmedia             |
      | AccountantHead_Textile           | POApprover_Textile               |
      | AccountantHead_Corrugated        | POApprover_Corrugated            |

      | AccountantHead_Flexo             | POPaymentDecsionMaker_Flexo      |
      | AccountantHead_Offset            | POPaymentDecsionMaker_Offset     |
      | AccountantHead_Digital           | POPaymentDecsionMaker_Digital    |
      | AccountantHead_Signmedia         | POPaymentDecsionMaker_Signmedia  |
      | AccountantHead_Textile           | POPaymentDecsionMaker_Textile    |
      | AccountantHead_Corrugated        | POPaymentDecsionMaker_Corrugated |

      | SalesManager_Flexo               | POViewer_Flexo                   |
      | SalesManager_Offset              | POViewer_Offset                  |
      | SalesManager_Digital             | POViewer_Digital                 |
      | SalesManager_Signmedia           | POViewer_Signmedia               |
      | SalesManager_Textile             | POViewer_Textile                 |
      | SalesManager_Corrugated          | POViewer_Corrugated              |

      | SalesManager_Flexo               | POApprover_Flexo                 |
      | SalesManager_Offset              | POApprover_Offset                |
      | SalesManager_Digital             | POApprover_Digital               |
      | SalesManager_Signmedia           | POApprover_Signmedia             |
      | SalesManager_Textile             | POApprover_Textile               |
      | SalesManager_Corrugated          | POApprover_Corrugated            |

      | B.U.Director_Flexo               | POViewer_Flexo                   |
      | B.U.Director_Offset              | POViewer_Offset                  |
      | B.U.Director_Digital             | POViewer_Digital                 |
      | B.U.Director_Signmedia           | POViewer_Signmedia               |
      | B.U.Director_Textile             | POViewer_Textile                 |
      | B.U.Director_Corrugated          | POViewer_Corrugated              |

      | B.U.Director_Flexo               | POApprover_Flexo                 |
      | B.U.Director_Offset              | POApprover_Offset                |
      | B.U.Director_Digital             | POApprover_Digital               |
      | B.U.Director_Signmedia           | POApprover_Signmedia             |
      | B.U.Director_Textile             | POApprover_Textile               |
      | B.U.Director_Corrugated          | POApprover_Corrugated            |

      | M.D                              | POViewer                         |
      | Deputy_M.D                       | POViewer                         |
      | Quality_Specialist               | POViewer                         |
      | Auditor                          | POViewer                         |
      | Corporate_Taxes_Accountant       | POViewer                         |
      | Corporate_Accounting_Head        | POViewer                         |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ImportPurchaseOrder" entity permissions:
      | Permission               |
      | Create                   |
      | ReadHeader               |
      | ReadAll                  |
      | ReadCompanyData          |
      | ReadConsigneeData        |
      | ReadItemsWithPrices      |
      | ReadPaymentTerms         |
      | ReadRequiredDocs         |
      | ReadAttachments          |
      | UploadAttachments        |
      | DownloadAttachments      |
      | DeleteAttachments        |
      | ReadApprovalCycles       |
      | EditHeader               |
      | EditConsigneeData        |
      | EditPaymentTerms         |
      | EditRequiredDocs         |
      | UpdateItem               |
      | Delete                   |
      | MarkAsPIRequested        |
      | SubmitForApproval        |
      | OpenUpdates              |
      | Confirm                  |
      | MarkAsProductionFinished |
      | MarkAsShipped            |
      | MarkAsDeliveryComplete   |
      | Cancel                   |
      | ExportPDF                |
      | MarkAsCleared            |
      | MarkAsArrived            |
      | AddPL/DNQuantity         |
      | EditCompanyData          |
      | Approve                  |
      | Reject                   |
      | ExportPDFWithoutPrices   |
      | ReadItemsWithoutPrices   |
      | MarkAsReadyForPayment    |

    Then the following "ImportPurchaseOrder" entity permissions created successfully:
      | Permission               |
      | Create                   |
      | ReadHeader               |
      | ReadAll                  |
      | ReadCompanyData          |
      | ReadConsigneeData        |
      | ReadItemsWithPrices      |
      | ReadPaymentTerms         |
      | ReadRequiredDocs         |
      | ReadAttachments          |
      | UploadAttachments        |
      | DownloadAttachments      |
      | DeleteAttachments        |
      | ReadApprovalCycles       |
      | EditHeader               |
      | EditConsigneeData        |
      | EditPaymentTerms         |
      | EditRequiredDocs         |
      | UpdateItem               |
      | Delete                   |
      | MarkAsPIRequested        |
      | SubmitForApproval        |
      | OpenUpdates              |
      | Confirm                  |
      | MarkAsProductionFinished |
      | MarkAsShipped            |
      | MarkAsDeliveryComplete   |
      | Cancel                   |
      | ExportPDF                |
      | MarkAsCleared            |
      | MarkAsArrived            |
      | AddPL/DNQuantity         |
      | EditCompanyData          |
      | Approve                  |
      | Reject                   |
      | ExportPDFWithoutPrices   |
      | ReadItemsWithoutPrices   |
      | MarkAsReadyForPayment    |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "LocalPurchaseOrder" entity permissions:
      | Permission               |
      | Create                   |
      | ReadHeader               |
      | ReadAll                  |
      | ReadCompanyData          |
      | ReadConsigneeData        |
      | ReadItemsWithPrices      |
      | ReadPaymentTerms         |
      | ReadRequiredDocs         |
      | ReadAttachments          |
      | UploadAttachments        |
      | DownloadAttachments      |
      | DeleteAttachments        |
      | ReadApprovalCycles       |
      | EditHeader               |
      | EditConsigneeData        |
      | EditPaymentTerms         |
      | EditRequiredDocs         |
      | UpdateItem               |
      | Delete                   |
      | MarkAsPIRequested        |
      | SubmitForApproval        |
      | OpenUpdates              |
      | Confirm                  |
      | MarkAsProductionFinished |
      | MarkAsShipped            |
      | MarkAsDeliveryComplete   |
      | Cancel                   |
      | ExportPDF                |
      | MarkAsCleared            |
      | MarkAsArrived            |
      | AddPL/DNQuantity         |
      | EditCompanyData          |
      | Approve                  |
      | Reject                   |
      | ExportPDFWithoutPrices   |
      | ReadItemsWithoutPrices   |

    Then the following "LocalPurchaseOrder" entity permissions created successfully:
      | Permission               |
      | Create                   |
      | ReadHeader               |
      | ReadAll                  |
      | ReadCompanyData          |
      | ReadConsigneeData        |
      | ReadItemsWithPrices      |
      | ReadPaymentTerms         |
      | ReadRequiredDocs         |
      | ReadAttachments          |
      | UploadAttachments        |
      | DownloadAttachments      |
      | DeleteAttachments        |
      | ReadApprovalCycles       |
      | EditHeader               |
      | EditConsigneeData        |
      | EditPaymentTerms         |
      | EditRequiredDocs         |
      | UpdateItem               |
      | Delete                   |
      | MarkAsPIRequested        |
      | SubmitForApproval        |
      | OpenUpdates              |
      | Confirm                  |
      | MarkAsProductionFinished |
      | MarkAsShipped            |
      | MarkAsDeliveryComplete   |
      | Cancel                   |
      | ExportPDF                |
      | MarkAsCleared            |
      | MarkAsArrived            |
      | AddPL/DNQuantity         |
      | EditCompanyData          |
      | Approve                  |
      | Reject                   |
      | ExportPDFWithoutPrices   |
      | ReadItemsWithoutPrices   |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "ImportPurchaseOrder" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "ImportPurchaseOrder" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "LocalPurchaseOrder" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "LocalPurchaseOrder" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ImportPurchaseOrder" entity:
      | Subrole                          | Permission               | Condition                               |
      | POOwner_Flexo                    | Create                   |                                         |
      | POOwner_Offset                   | Create                   |                                         |
      | POOwner_Digital                  | Create                   |                                         |
      | POOwner_Signmedia                | Create                   |                                         |
      | POOwner_Textile                  | Create                   |                                         |
      | POOwner_Corrugated               | Create                   |                                         |

      | POViewer                         | ReadAll                  |                                         |
      | POViewer                         | ReadHeader               |                                         |
      | POViewer                         | ReadCompanyData          |                                         |
      | POViewer                         | ReadConsigneeData        |                                         |
      | POViewer                         | ReadItemsWithPrices      |                                         |
      | POViewer                         | ReadPaymentTerms         |                                         |
      | POViewer                         | ReadRequiredDocs         |                                         |
      | POViewer                         | ReadAttachments          |                                         |
      | POViewer                         | ReadApprovalCycles       |                                         |
      | POViewer                         | ExportPDF                |                                         |

      | POOwner_Flexo                    | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadItemsWithPrices      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadAttachments          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UploadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DownloadAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DeleteAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadApprovalCycles       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UpdateItem               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Delete                   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsPIRequested        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | SubmitForApproval        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | OpenUpdates              | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Confirm                  | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsProductionFinished | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsShipped            | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsDeliveryComplete   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Cancel                   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ExportPDF                | [purchaseUnitName='Flexible Packaging'] |

      | POOwner_Offset                   | ReadAll                  | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadHeader               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadCompanyData          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadItemsWithPrices      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadPaymentTerms         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadRequiredDocs         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadAttachments          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UploadAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DownloadAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DeleteAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadApprovalCycles       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditHeader               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditConsigneeData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditPaymentTerms         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditRequiredDocs         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UpdateItem               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Delete                   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsPIRequested        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | SubmitForApproval        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | OpenUpdates              | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Confirm                  | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsProductionFinished | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsShipped            | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsDeliveryComplete   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Cancel                   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ExportPDF                | [purchaseUnitName='Offset']             |

      | POOwner_Digital                  | ReadAll                  | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadHeader               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadCompanyData          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadItemsWithPrices      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadPaymentTerms         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadRequiredDocs         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadAttachments          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UploadAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DownloadAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DeleteAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadApprovalCycles       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditHeader               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditConsigneeData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditPaymentTerms         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditRequiredDocs         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UpdateItem               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Delete                   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsPIRequested        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | SubmitForApproval        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | OpenUpdates              | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Confirm                  | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsProductionFinished | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsShipped            | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsDeliveryComplete   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Cancel                   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ExportPDF                | [purchaseUnitName='Digital']            |

      | POOwner_Signmedia                | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadCompanyData          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadItemsWithPrices      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadAttachments          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UploadAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DownloadAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DeleteAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadApprovalCycles       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditHeader               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UpdateItem               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Delete                   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsPIRequested        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | SubmitForApproval        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | OpenUpdates              | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Confirm                  | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsProductionFinished | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsShipped            | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsDeliveryComplete   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Cancel                   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ExportPDF                | [purchaseUnitName='Signmedia']          |

      | POOwner_Textile                  | ReadAll                  | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadHeader               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadCompanyData          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadItemsWithPrices      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadPaymentTerms         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadRequiredDocs         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadAttachments          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UploadAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DownloadAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DeleteAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadApprovalCycles       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditHeader               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditConsigneeData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditPaymentTerms         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditRequiredDocs         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UpdateItem               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Delete                   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsPIRequested        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | SubmitForApproval        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | OpenUpdates              | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Confirm                  | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsProductionFinished | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsShipped            | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsDeliveryComplete   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Cancel                   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ExportPDF                | [purchaseUnitName='Textile']            |

      | POOwner_Corrugated               | ReadAll                  | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadHeader               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadCompanyData          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadItemsWithPrices      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadPaymentTerms         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadRequiredDocs         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadAttachments          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UploadAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DownloadAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DeleteAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadApprovalCycles       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditHeader               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditConsigneeData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditPaymentTerms         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditRequiredDocs         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UpdateItem               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Delete                   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsPIRequested        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | SubmitForApproval        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | OpenUpdates              | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Confirm                  | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsProductionFinished | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsShipped            | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsDeliveryComplete   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Cancel                   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ExportPDF                | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | MarkAsArrived            | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | MarkAsArrived            | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | MarkAsArrived            | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | MarkAsArrived            | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | MarkAsArrived            | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | MarkAsArrived            | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | MarkAsCleared            | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | MarkAsCleared            | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | MarkAsCleared            | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | MarkAsCleared            | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | MarkAsCleared            | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | MarkAsCleared            | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | AddPL/DNQuantity         | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | AddPL/DNQuantity         | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | AddPL/DNQuantity         | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | AddPL/DNQuantity         | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | AddPL/DNQuantity         | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | AddPL/DNQuantity         | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | EditCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | EditCompanyData          | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | EditCompanyData          | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | EditCompanyData          | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | EditCompanyData          | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | EditCompanyData          | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | MarkAsReadyForPayment    | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | MarkAsReadyForPayment    | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | MarkAsReadyForPayment    | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | MarkAsReadyForPayment    | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | MarkAsReadyForPayment    | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | MarkAsReadyForPayment    | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Approve                  | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Approve                  | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Approve                  | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Approve                  | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Approve                  | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Approve                  | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Reject                   | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Reject                   | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Reject                   | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Reject                   | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Reject                   | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Reject                   | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAll                  | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAll                  | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAll                  | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAll                  | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadHeader               | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadHeader               | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadHeader               | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadHeader               | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadCompanyData          | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadCompanyData          | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadCompanyData          | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadCompanyData          | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadCompanyData          | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadItemsWithPrices      | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadItemsWithPrices      | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadItemsWithPrices      | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadItemsWithPrices      | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadItemsWithPrices      | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadItemsWithPrices      | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadPaymentTerms         | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadPaymentTerms         | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadPaymentTerms         | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadPaymentTerms         | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadRequiredDocs         | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadRequiredDocs         | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadRequiredDocs         | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadRequiredDocs         | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAttachments          | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAttachments          | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAttachments          | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAttachments          | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAttachments          | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAttachments          | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadApprovalCycles       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadApprovalCycles       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadApprovalCycles       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadApprovalCycles       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadApprovalCycles       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadApprovalCycles       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ExportPDF                | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ExportPDF                | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ExportPDF                | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ExportPDF                | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ExportPDF                | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ExportPDF                | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadAll                  | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadAll                  | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadAll                  | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadAll                  | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadHeader               | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadHeader               | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadHeader               | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadHeader               | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadItemsWithoutPrices   | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadItemsWithoutPrices   | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadItemsWithoutPrices   | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadItemsWithoutPrices   | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadItemsWithoutPrices   | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadItemsWithoutPrices   | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ExportPDFWithoutPrices   | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ExportPDFWithoutPrices   | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ExportPDFWithoutPrices   | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ExportPDFWithoutPrices   | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ExportPDFWithoutPrices   | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ExportPDFWithoutPrices   | [purchaseUnitName='Corrugated']         |

    Then the following roles , permissions and conditions assignment created successfully for "ImportPurchaseOrder" entity:
      | Subrole                          | Permission               | Condition                               |
      | POOwner_Flexo                    | Create                   |                                         |
      | POOwner_Offset                   | Create                   |                                         |
      | POOwner_Digital                  | Create                   |                                         |
      | POOwner_Signmedia                | Create                   |                                         |
      | POOwner_Textile                  | Create                   |                                         |
      | POOwner_Corrugated               | Create                   |                                         |

      | POViewer                         | ReadAll                  |                                         |
      | POViewer                         | ReadHeader               |                                         |
      | POViewer                         | ReadCompanyData          |                                         |
      | POViewer                         | ReadConsigneeData        |                                         |
      | POViewer                         | ReadItemsWithPrices      |                                         |
      | POViewer                         | ReadPaymentTerms         |                                         |
      | POViewer                         | ReadRequiredDocs         |                                         |
      | POViewer                         | ReadAttachments          |                                         |
      | POViewer                         | ReadApprovalCycles       |                                         |
      | POViewer                         | ExportPDF                |                                         |

      | POOwner_Flexo                    | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadItemsWithPrices      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadAttachments          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UploadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DownloadAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DeleteAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadApprovalCycles       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UpdateItem               | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Delete                   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsPIRequested        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | SubmitForApproval        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | OpenUpdates              | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Confirm                  | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsProductionFinished | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsShipped            | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsDeliveryComplete   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Cancel                   | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ExportPDF                | [purchaseUnitName='Flexible Packaging'] |

      | POOwner_Offset                   | ReadAll                  | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadHeader               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadCompanyData          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadItemsWithPrices      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadPaymentTerms         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadRequiredDocs         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadAttachments          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UploadAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DownloadAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DeleteAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadApprovalCycles       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditHeader               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditConsigneeData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditPaymentTerms         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditRequiredDocs         | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UpdateItem               | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Delete                   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsPIRequested        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | SubmitForApproval        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | OpenUpdates              | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Confirm                  | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsProductionFinished | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsShipped            | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsDeliveryComplete   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Cancel                   | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ExportPDF                | [purchaseUnitName='Offset']             |

      | POOwner_Digital                  | ReadAll                  | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadHeader               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadCompanyData          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadItemsWithPrices      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadPaymentTerms         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadRequiredDocs         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadAttachments          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UploadAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DownloadAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DeleteAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadApprovalCycles       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditHeader               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditConsigneeData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditPaymentTerms         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditRequiredDocs         | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UpdateItem               | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Delete                   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsPIRequested        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | SubmitForApproval        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | OpenUpdates              | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Confirm                  | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsProductionFinished | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsShipped            | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsDeliveryComplete   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Cancel                   | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ExportPDF                | [purchaseUnitName='Digital']            |

      | POOwner_Signmedia                | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadCompanyData          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadItemsWithPrices      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadAttachments          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UploadAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DownloadAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DeleteAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadApprovalCycles       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditHeader               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UpdateItem               | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Delete                   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsPIRequested        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | SubmitForApproval        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | OpenUpdates              | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Confirm                  | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsProductionFinished | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsShipped            | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsDeliveryComplete   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Cancel                   | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ExportPDF                | [purchaseUnitName='Signmedia']          |

      | POOwner_Textile                  | ReadAll                  | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadHeader               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadCompanyData          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadItemsWithPrices      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadPaymentTerms         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadRequiredDocs         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadAttachments          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UploadAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DownloadAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DeleteAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadApprovalCycles       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditHeader               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditConsigneeData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditPaymentTerms         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditRequiredDocs         | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UpdateItem               | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Delete                   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsPIRequested        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | SubmitForApproval        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | OpenUpdates              | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Confirm                  | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsProductionFinished | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsShipped            | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsDeliveryComplete   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Cancel                   | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ExportPDF                | [purchaseUnitName='Textile']            |

      | POOwner_Corrugated               | ReadAll                  | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadHeader               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadCompanyData          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadItemsWithPrices      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadPaymentTerms         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadRequiredDocs         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadAttachments          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UploadAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DownloadAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DeleteAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadApprovalCycles       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditHeader               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditConsigneeData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditPaymentTerms         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditRequiredDocs         | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UpdateItem               | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Delete                   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsPIRequested        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | SubmitForApproval        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | OpenUpdates              | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Confirm                  | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsProductionFinished | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsShipped            | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsDeliveryComplete   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Cancel                   | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ExportPDF                | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | MarkAsArrived            | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | MarkAsArrived            | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | MarkAsArrived            | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | MarkAsArrived            | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | MarkAsArrived            | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | MarkAsArrived            | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | MarkAsCleared            | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | MarkAsCleared            | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | MarkAsCleared            | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | MarkAsCleared            | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | MarkAsCleared            | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | MarkAsCleared            | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | AddPL/DNQuantity         | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | AddPL/DNQuantity         | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | AddPL/DNQuantity         | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | AddPL/DNQuantity         | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | AddPL/DNQuantity         | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | AddPL/DNQuantity         | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | EditCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | EditCompanyData          | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | EditCompanyData          | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | EditCompanyData          | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | EditCompanyData          | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | EditCompanyData          | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | MarkAsReadyForPayment    | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | MarkAsReadyForPayment    | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | MarkAsReadyForPayment    | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | MarkAsReadyForPayment    | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | MarkAsReadyForPayment    | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | MarkAsReadyForPayment    | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Approve                  | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Approve                  | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Approve                  | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Approve                  | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Approve                  | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Approve                  | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Reject                   | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Reject                   | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Reject                   | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Reject                   | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Reject                   | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Reject                   | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAll                  | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAll                  | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAll                  | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAll                  | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadHeader               | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadHeader               | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadHeader               | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadHeader               | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadCompanyData          | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadCompanyData          | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadCompanyData          | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadCompanyData          | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadCompanyData          | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadCompanyData          | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadItemsWithPrices      | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadItemsWithPrices      | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadItemsWithPrices      | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadItemsWithPrices      | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadItemsWithPrices      | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadItemsWithPrices      | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadPaymentTerms         | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadPaymentTerms         | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadPaymentTerms         | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadPaymentTerms         | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadPaymentTerms         | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadPaymentTerms         | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadRequiredDocs         | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadRequiredDocs         | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadRequiredDocs         | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadRequiredDocs         | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadRequiredDocs         | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadRequiredDocs         | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAttachments          | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAttachments          | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAttachments          | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAttachments          | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAttachments          | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAttachments          | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadApprovalCycles       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadApprovalCycles       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadApprovalCycles       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadApprovalCycles       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadApprovalCycles       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadApprovalCycles       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ExportPDF                | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ExportPDF                | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ExportPDF                | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ExportPDF                | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ExportPDF                | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ExportPDF                | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadAll                  | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadAll                  | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadAll                  | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadAll                  | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadAll                  | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadAll                  | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadHeader               | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadHeader               | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadHeader               | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadHeader               | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadHeader               | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadHeader               | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadConsigneeData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadConsigneeData        | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadConsigneeData        | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadConsigneeData        | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadConsigneeData        | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadConsigneeData        | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadItemsWithoutPrices   | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadItemsWithoutPrices   | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadItemsWithoutPrices   | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadItemsWithoutPrices   | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadItemsWithoutPrices   | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadItemsWithoutPrices   | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ExportPDFWithoutPrices   | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ExportPDFWithoutPrices   | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ExportPDFWithoutPrices   | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ExportPDFWithoutPrices   | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ExportPDFWithoutPrices   | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ExportPDFWithoutPrices   | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "LocalPurchaseOrder" entity:
      | Subrole                          | Permission             | Condition                               |
      | POOwner_Flexo                    | Create                 |                                         |
      | POOwner_Offset                   | Create                 |                                         |
      | POOwner_Digital                  | Create                 |                                         |
      | POOwner_Signmedia                | Create                 |                                         |
      | POOwner_Textile                  | Create                 |                                         |
      | POOwner_Corrugated               | Create                 |                                         |

      | POViewer                         | ReadAll                |                                         |
      | POViewer                         | ReadHeader             |                                         |
      | POViewer                         | ReadCompanyData        |                                         |
      | POViewer                         | ReadConsigneeData      |                                         |
      | POViewer                         | ReadItemsWithPrices    |                                         |
      | POViewer                         | ReadPaymentTerms       |                                         |
      | POViewer                         | ReadRequiredDocs       |                                         |
      | POViewer                         | ReadAttachments        |                                         |
      | POViewer                         | ReadApprovalCycles     |                                         |
      | POViewer                         | ExportPDF              |                                         |

      | POOwner_Flexo                    | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadItemsWithPrices    | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UploadAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DownloadAttachments    | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DeleteAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadApprovalCycles     | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UpdateItem             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Delete                 | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsPIRequested      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | SubmitForApproval      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | OpenUpdates            | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Confirm                | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsShipped          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsDeliveryComplete | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Cancel                 | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ExportPDF              | [purchaseUnitName='Flexible Packaging'] |

      | POOwner_Offset                   | ReadAll                | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadHeader             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadCompanyData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadItemsWithPrices    | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadPaymentTerms       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadRequiredDocs       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UploadAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DownloadAttachments    | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DeleteAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadApprovalCycles     | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditHeader             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditConsigneeData      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditPaymentTerms       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditRequiredDocs       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UpdateItem             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Delete                 | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsPIRequested      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | SubmitForApproval      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | OpenUpdates            | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Confirm                | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsShipped          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsDeliveryComplete | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Cancel                 | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ExportPDF              | [purchaseUnitName='Offset']             |

      | POOwner_Digital                  | ReadAll                | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadHeader             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadCompanyData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadItemsWithPrices    | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadPaymentTerms       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadRequiredDocs       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UploadAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DownloadAttachments    | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DeleteAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadApprovalCycles     | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditHeader             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditConsigneeData      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditPaymentTerms       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditRequiredDocs       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UpdateItem             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Delete                 | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsPIRequested      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | SubmitForApproval      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | OpenUpdates            | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Confirm                | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsShipped          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsDeliveryComplete | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Cancel                 | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ExportPDF              | [purchaseUnitName='Digital']            |

      | POOwner_Signmedia                | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadCompanyData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadItemsWithPrices    | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UploadAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DownloadAttachments    | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DeleteAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadApprovalCycles     | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditHeader             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UpdateItem             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Delete                 | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsPIRequested      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | SubmitForApproval      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | OpenUpdates            | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Confirm                | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsShipped          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsDeliveryComplete | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Cancel                 | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ExportPDF              | [purchaseUnitName='Signmedia']          |

      | POOwner_Textile                  | ReadAll                | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadHeader             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadCompanyData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadItemsWithPrices    | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadPaymentTerms       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadRequiredDocs       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UploadAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DownloadAttachments    | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DeleteAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadApprovalCycles     | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditHeader             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditConsigneeData      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditPaymentTerms       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditRequiredDocs       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UpdateItem             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Delete                 | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsPIRequested      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | SubmitForApproval      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | OpenUpdates            | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Confirm                | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsShipped          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsDeliveryComplete | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Cancel                 | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ExportPDF              | [purchaseUnitName='Textile']            |

      | POOwner_Corrugated               | ReadAll                | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadHeader             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadCompanyData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadItemsWithPrices    | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadPaymentTerms       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadRequiredDocs       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UploadAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DownloadAttachments    | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DeleteAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadApprovalCycles     | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditHeader             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditConsigneeData      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditPaymentTerms       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditRequiredDocs       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UpdateItem             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Delete                 | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsPIRequested      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | SubmitForApproval      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | OpenUpdates            | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Confirm                | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsShipped          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsDeliveryComplete | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Cancel                 | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ExportPDF              | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | AddPL/DNQuantity       | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | AddPL/DNQuantity       | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | AddPL/DNQuantity       | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | AddPL/DNQuantity       | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | AddPL/DNQuantity       | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | AddPL/DNQuantity       | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | EditCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | EditCompanyData        | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | EditCompanyData        | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | EditCompanyData        | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | EditCompanyData        | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | EditCompanyData        | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Approve                | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Approve                | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Approve                | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Approve                | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Approve                | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Approve                | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Reject                 | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Reject                 | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Reject                 | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Reject                 | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Reject                 | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Reject                 | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAll                | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAll                | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAll                | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAll                | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadHeader             | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadHeader             | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadHeader             | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadHeader             | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadCompanyData        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadCompanyData        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadCompanyData        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadCompanyData        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadCompanyData        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadItemsWithPrices    | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadItemsWithPrices    | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadItemsWithPrices    | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadItemsWithPrices    | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadItemsWithPrices    | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadItemsWithPrices    | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadPaymentTerms       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadPaymentTerms       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadPaymentTerms       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadPaymentTerms       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadRequiredDocs       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadRequiredDocs       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadRequiredDocs       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadRequiredDocs       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAttachments        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAttachments        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAttachments        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAttachments        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAttachments        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadApprovalCycles     | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadApprovalCycles     | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadApprovalCycles     | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadApprovalCycles     | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadApprovalCycles     | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadApprovalCycles     | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ExportPDF              | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ExportPDF              | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ExportPDF              | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ExportPDF              | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ExportPDF              | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ExportPDF              | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadAll                | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadAll                | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadAll                | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadAll                | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadHeader             | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadHeader             | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadHeader             | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadHeader             | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadItemsWithoutPrices | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadItemsWithoutPrices | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadItemsWithoutPrices | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadItemsWithoutPrices | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadItemsWithoutPrices | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadItemsWithoutPrices | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ExportPDFWithoutPrices | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ExportPDFWithoutPrices | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ExportPDFWithoutPrices | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ExportPDFWithoutPrices | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ExportPDFWithoutPrices | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ExportPDFWithoutPrices | [purchaseUnitName='Corrugated']         |

    Then the following roles , permissions and conditions assignment created successfully for "LocalPurchaseOrder" entity:
      | Subrole                          | Permission             | Condition                               |
      | POOwner_Flexo                    | Create                 |                                         |
      | POOwner_Offset                   | Create                 |                                         |
      | POOwner_Digital                  | Create                 |                                         |
      | POOwner_Signmedia                | Create                 |                                         |
      | POOwner_Textile                  | Create                 |                                         |
      | POOwner_Corrugated               | Create                 |                                         |

      | POViewer                         | ReadAll                |                                         |
      | POViewer                         | ReadHeader             |                                         |
      | POViewer                         | ReadCompanyData        |                                         |
      | POViewer                         | ReadConsigneeData      |                                         |
      | POViewer                         | ReadItemsWithPrices    |                                         |
      | POViewer                         | ReadPaymentTerms       |                                         |
      | POViewer                         | ReadRequiredDocs       |                                         |
      | POViewer                         | ReadAttachments        |                                         |
      | POViewer                         | ReadApprovalCycles     |                                         |
      | POViewer                         | ExportPDF              |                                         |

      | POOwner_Flexo                    | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadItemsWithPrices    | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UploadAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DownloadAttachments    | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | DeleteAttachments      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ReadApprovalCycles     | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | EditRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | UpdateItem             | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Delete                 | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsPIRequested      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | SubmitForApproval      | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | OpenUpdates            | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Confirm                | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsShipped          | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | MarkAsDeliveryComplete | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | Cancel                 | [purchaseUnitName='Flexible Packaging'] |
      | POOwner_Flexo                    | ExportPDF              | [purchaseUnitName='Flexible Packaging'] |

      | POOwner_Offset                   | ReadAll                | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadHeader             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadCompanyData        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadItemsWithPrices    | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadPaymentTerms       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadRequiredDocs       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadAttachments        | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UploadAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DownloadAttachments    | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | DeleteAttachments      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ReadApprovalCycles     | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditHeader             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditConsigneeData      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditPaymentTerms       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | EditRequiredDocs       | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | UpdateItem             | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Delete                 | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsPIRequested      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | SubmitForApproval      | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | OpenUpdates            | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Confirm                | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsShipped          | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | MarkAsDeliveryComplete | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | Cancel                 | [purchaseUnitName='Offset']             |
      | POOwner_Offset                   | ExportPDF              | [purchaseUnitName='Offset']             |

      | POOwner_Digital                  | ReadAll                | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadHeader             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadCompanyData        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadItemsWithPrices    | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadPaymentTerms       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadRequiredDocs       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadAttachments        | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UploadAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DownloadAttachments    | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | DeleteAttachments      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ReadApprovalCycles     | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditHeader             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditConsigneeData      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditPaymentTerms       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | EditRequiredDocs       | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | UpdateItem             | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Delete                 | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsPIRequested      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | SubmitForApproval      | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | OpenUpdates            | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Confirm                | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsShipped          | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | MarkAsDeliveryComplete | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | Cancel                 | [purchaseUnitName='Digital']            |
      | POOwner_Digital                  | ExportPDF              | [purchaseUnitName='Digital']            |

      | POOwner_Signmedia                | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadCompanyData        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadItemsWithPrices    | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadAttachments        | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UploadAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DownloadAttachments    | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | DeleteAttachments      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ReadApprovalCycles     | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditHeader             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | EditRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | UpdateItem             | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Delete                 | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsPIRequested      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | SubmitForApproval      | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | OpenUpdates            | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Confirm                | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsShipped          | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | MarkAsDeliveryComplete | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | Cancel                 | [purchaseUnitName='Signmedia']          |
      | POOwner_Signmedia                | ExportPDF              | [purchaseUnitName='Signmedia']          |

      | POOwner_Textile                  | ReadAll                | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadHeader             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadCompanyData        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadItemsWithPrices    | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadPaymentTerms       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadRequiredDocs       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadAttachments        | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UploadAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DownloadAttachments    | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | DeleteAttachments      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ReadApprovalCycles     | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditHeader             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditConsigneeData      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditPaymentTerms       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | EditRequiredDocs       | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | UpdateItem             | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Delete                 | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsPIRequested      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | SubmitForApproval      | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | OpenUpdates            | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Confirm                | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsShipped          | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | MarkAsDeliveryComplete | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | Cancel                 | [purchaseUnitName='Textile']            |
      | POOwner_Textile                  | ExportPDF              | [purchaseUnitName='Textile']            |

      | POOwner_Corrugated               | ReadAll                | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadHeader             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadCompanyData        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadItemsWithPrices    | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadPaymentTerms       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadRequiredDocs       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadAttachments        | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UploadAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DownloadAttachments    | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | DeleteAttachments      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ReadApprovalCycles     | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditHeader             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditConsigneeData      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditPaymentTerms       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | EditRequiredDocs       | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | UpdateItem             | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Delete                 | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsPIRequested      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | SubmitForApproval      | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | OpenUpdates            | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Confirm                | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsShipped          | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | MarkAsDeliveryComplete | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | Cancel                 | [purchaseUnitName='Corrugated']         |
      | POOwner_Corrugated               | ExportPDF              | [purchaseUnitName='Corrugated']         |

      | POLogisticsOwner_Flexo           | AddPL/DNQuantity       | [purchaseUnitName='Flexible Packaging'] |
      | POLogisticsOwner_Offset          | AddPL/DNQuantity       | [purchaseUnitName='Offset']             |
      | POLogisticsOwner_Digital         | AddPL/DNQuantity       | [purchaseUnitName='Digital']            |
      | POLogisticsOwner_Signmedia       | AddPL/DNQuantity       | [purchaseUnitName='Signmedia']          |
      | POLogisticsOwner_Textile         | AddPL/DNQuantity       | [purchaseUnitName='Textile']            |
      | POLogisticsOwner_Corrugated      | AddPL/DNQuantity       | [purchaseUnitName='Corrugated']         |

      | POPaymentDecsionMaker_Flexo      | EditCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POPaymentDecsionMaker_Offset     | EditCompanyData        | [purchaseUnitName='Offset']             |
      | POPaymentDecsionMaker_Digital    | EditCompanyData        | [purchaseUnitName='Digital']            |
      | POPaymentDecsionMaker_Signmedia  | EditCompanyData        | [purchaseUnitName='Signmedia']          |
      | POPaymentDecsionMaker_Textile    | EditCompanyData        | [purchaseUnitName='Textile']            |
      | POPaymentDecsionMaker_Corrugated | EditCompanyData        | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Approve                | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Approve                | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Approve                | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Approve                | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Approve                | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Approve                | [purchaseUnitName='Corrugated']         |

      | POApprover_Flexo                 | Reject                 | [purchaseUnitName='Flexible Packaging'] |
      | POApprover_Offset                | Reject                 | [purchaseUnitName='Offset']             |
      | POApprover_Digital               | Reject                 | [purchaseUnitName='Digital']            |
      | POApprover_Signmedia             | Reject                 | [purchaseUnitName='Signmedia']          |
      | POApprover_Textile               | Reject                 | [purchaseUnitName='Textile']            |
      | POApprover_Corrugated            | Reject                 | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAll                | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAll                | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAll                | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAll                | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadHeader             | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadHeader             | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadHeader             | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadHeader             | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadCompanyData        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadCompanyData        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadCompanyData        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadCompanyData        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadCompanyData        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadCompanyData        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadItemsWithPrices    | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadItemsWithPrices    | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadItemsWithPrices    | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadItemsWithPrices    | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadItemsWithPrices    | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadItemsWithPrices    | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadPaymentTerms       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadPaymentTerms       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadPaymentTerms       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadPaymentTerms       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadPaymentTerms       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadPaymentTerms       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadRequiredDocs       | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadRequiredDocs       | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadRequiredDocs       | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadRequiredDocs       | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadRequiredDocs       | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadRequiredDocs       | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadAttachments        | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadAttachments        | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadAttachments        | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadAttachments        | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadAttachments        | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadAttachments        | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ReadApprovalCycles     | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ReadApprovalCycles     | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ReadApprovalCycles     | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ReadApprovalCycles     | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ReadApprovalCycles     | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ReadApprovalCycles     | [purchaseUnitName='Corrugated']         |

      | POViewer_Flexo                   | ExportPDF              | [purchaseUnitName='Flexible Packaging'] |
      | POViewer_Offset                  | ExportPDF              | [purchaseUnitName='Offset']             |
      | POViewer_Digital                 | ExportPDF              | [purchaseUnitName='Digital']            |
      | POViewer_Signmedia               | ExportPDF              | [purchaseUnitName='Signmedia']          |
      | POViewer_Textile                 | ExportPDF              | [purchaseUnitName='Textile']            |
      | POViewer_Corrugated              | ExportPDF              | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadAll                | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadAll                | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadAll                | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadAll                | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadAll                | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadAll                | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadHeader             | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadHeader             | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadHeader             | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadHeader             | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadHeader             | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadHeader             | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadConsigneeData      | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadConsigneeData      | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadConsigneeData      | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadConsigneeData      | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadConsigneeData      | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadConsigneeData      | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ReadItemsWithoutPrices | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ReadItemsWithoutPrices | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ReadItemsWithoutPrices | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ReadItemsWithoutPrices | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ReadItemsWithoutPrices | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ReadItemsWithoutPrices | [purchaseUnitName='Corrugated']         |

      | POViewerLimited_Flexo            | ExportPDFWithoutPrices | [purchaseUnitName='Flexible Packaging'] |
      | POViewerLimited_Offset           | ExportPDFWithoutPrices | [purchaseUnitName='Offset']             |
      | POViewerLimited_Digital          | ExportPDFWithoutPrices | [purchaseUnitName='Digital']            |
      | POViewerLimited_Signmedia        | ExportPDFWithoutPrices | [purchaseUnitName='Signmedia']          |
      | POViewerLimited_Textile          | ExportPDFWithoutPrices | [purchaseUnitName='Textile']            |
      | POViewerLimited_Corrugated       | ExportPDFWithoutPrices | [purchaseUnitName='Corrugated']         |