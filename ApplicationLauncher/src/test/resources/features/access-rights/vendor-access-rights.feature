Feature: Automated Vendor Users Access Rights Insertion


  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                    |
      | VendorOwner_Flexo       |
      | VendorOwner_Offset      |
      | VendorOwner_Digital     |
      | VendorOwner_Signmedia   |
      | VendorOwner_Textile     |
      | VendorOwner_Corrugated  |

      | VendorViewer_Flexo      |
      | VendorViewer_Offset     |
      | VendorViewer_Digital    |
      | VendorViewer_Signmedia  |
      | VendorViewer_Textile    |
      | VendorViewer_Corrugated |

      | VendorViewer            |
    Then the following roles created successfully:
      | Role                    |
      | VendorOwner_Flexo       |
      | VendorOwner_Offset      |
      | VendorOwner_Digital     |
      | VendorOwner_Signmedia   |
      | VendorOwner_Textile     |
      | VendorOwner_Corrugated  |

      | VendorViewer_Flexo      |
      | VendorViewer_Offset     |
      | VendorViewer_Digital    |
      | VendorViewer_Signmedia  |
      | VendorViewer_Textile    |
      | VendorViewer_Corrugated |

      | VendorViewer            |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                 |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo       |
      | PurchasingResponsible_Offset     | VendorOwner_Offset      |
      | PurchasingResponsible_Digital    | VendorOwner_Digital     |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia   |
      | PurchasingResponsible_Textile    | VendorOwner_Textile     |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated  |

      | Accountant_Flexo                 | VendorViewer_Flexo      |
      | Accountant_Offset                | VendorViewer_Offset     |
      | Accountant_Digital               | VendorViewer_Digital    |
      | Accountant_Signmedia             | VendorViewer_Signmedia  |
      | Accountant_Textile               | VendorViewer_Textile    |
      | Accountant_Corrugated            | VendorViewer_Corrugated |

      | AccountantHead_Flexo             | VendorViewer_Flexo      |
      | AccountantHead_Offset            | VendorViewer_Offset     |
      | AccountantHead_Digital           | VendorViewer_Digital    |
      | AccountantHead_Signmedia         | VendorViewer_Signmedia  |
      | AccountantHead_Textile           | VendorViewer_Textile    |
      | AccountantHead_Corrugated        | VendorViewer_Corrugated |

      | B.U.Director_Flexo               | VendorViewer_Flexo      |
      | B.U.Director_Offset              | VendorViewer_Offset     |
      | B.U.Director_Digital             | VendorViewer_Digital    |
      | B.U.Director_Signmedia           | VendorViewer_Signmedia  |
      | B.U.Director_Textile             | VendorViewer_Textile    |
      | B.U.Director_Corrugated          | VendorViewer_Corrugated |

      | M.D                              | VendorViewer            |
      | Deputy_M.D                       | VendorViewer            |
      | Quality_Specialist               | VendorViewer            |
      | Auditor                          | VendorViewer            |
      | Corporate_Taxes_Accountant       | VendorViewer            |
      | Corporate_Accounting_Head        | VendorViewer            |


    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                 |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo       |
      | PurchasingResponsible_Offset     | VendorOwner_Offset      |
      | PurchasingResponsible_Digital    | VendorOwner_Digital     |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia   |
      | PurchasingResponsible_Textile    | VendorOwner_Textile     |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated  |

      | Accountant_Flexo                 | VendorViewer_Flexo      |
      | Accountant_Offset                | VendorViewer_Offset     |
      | Accountant_Digital               | VendorViewer_Digital    |
      | Accountant_Signmedia             | VendorViewer_Signmedia  |
      | Accountant_Textile               | VendorViewer_Textile    |
      | Accountant_Corrugated            | VendorViewer_Corrugated |

      | AccountantHead_Flexo             | VendorViewer_Flexo      |
      | AccountantHead_Offset            | VendorViewer_Offset     |
      | AccountantHead_Digital           | VendorViewer_Digital    |
      | AccountantHead_Signmedia         | VendorViewer_Signmedia  |
      | AccountantHead_Textile           | VendorViewer_Textile    |
      | AccountantHead_Corrugated        | VendorViewer_Corrugated |

      | B.U.Director_Flexo               | VendorViewer_Flexo      |
      | B.U.Director_Offset              | VendorViewer_Offset     |
      | B.U.Director_Digital             | VendorViewer_Digital    |
      | B.U.Director_Signmedia           | VendorViewer_Signmedia  |
      | B.U.Director_Textile             | VendorViewer_Textile    |
      | B.U.Director_Corrugated          | VendorViewer_Corrugated |

      | M.D                              | VendorViewer            |
      | Deputy_M.D                       | VendorViewer            |
      | Quality_Specialist               | VendorViewer            |
      | Auditor                          | VendorViewer            |
      | Corporate_Taxes_Accountant       | VendorViewer            |
      | Corporate_Accounting_Head        | VendorViewer            |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "Vendor" entity permissions:
      | Permission        |
      | Create            |
      | Delete            |
      | ReadAll           |
      | ReadGeneralData   |
      | UpdateGeneralData |
    Then the following "Vendor" entity permissions created successfully:
      | Permission        |
      | Create            |
      | Delete            |
      | ReadAll           |
      | ReadGeneralData   |
      | UpdateGeneralData |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "Vendor" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "Vendor" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "Vendor" entity:
      | Subrole                 | Permission        | Condition                               |
      | VendorOwner_Flexo       | Create            |                                         |
      | VendorOwner_Offset      | Create            |                                         |
      | VendorOwner_Digital     | Create            |                                         |
      | VendorOwner_Signmedia   | Create            |                                         |
      | VendorOwner_Textile     | Create            |                                         |
      | VendorOwner_Corrugated  | Create            |                                         |

      | VendorOwner_Flexo       | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | UpdateGeneralData | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | Delete            | [purchaseUnitName='Flexible Packaging'] |

      | VendorOwner_Offset      | ReadAll           | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | UpdateGeneralData | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | Delete            | [purchaseUnitName='Offset']             |

      | VendorOwner_Digital     | ReadAll           | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | UpdateGeneralData | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | Delete            | [purchaseUnitName='Digital']            |

      | VendorOwner_Signmedia   | ReadAll           | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | UpdateGeneralData | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | Delete            | [purchaseUnitName='Signmedia']          |

      | VendorOwner_Textile     | ReadAll           | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | UpdateGeneralData | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | Delete            | [purchaseUnitName='Textile']            |

      | VendorOwner_Corrugated  | ReadAll           | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | UpdateGeneralData | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | Delete            | [purchaseUnitName='Corrugated']         |

      | VendorViewer_Flexo      | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | VendorViewer_Flexo      | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |

      | VendorViewer_Offset     | ReadAll           | [purchaseUnitName='Offset']             |
      | VendorViewer_Offset     | ReadGeneralData   | [purchaseUnitName='Offset']             |

      | VendorViewer_Digital    | ReadAll           | [purchaseUnitName='Digital']            |
      | VendorViewer_Digital    | ReadGeneralData   | [purchaseUnitName='Digital']            |

      | VendorViewer_Signmedia  | ReadAll           | [purchaseUnitName='Signmedia']          |
      | VendorViewer_Signmedia  | ReadGeneralData   | [purchaseUnitName='Signmedia']          |

      | VendorViewer_Textile    | ReadAll           | [purchaseUnitName='Textile']            |
      | VendorViewer_Textile    | ReadGeneralData   | [purchaseUnitName='Textile']            |

      | VendorViewer_Corrugated | ReadAll           | [purchaseUnitName='Corrugated']         |
      | VendorViewer_Corrugated | ReadGeneralData   | [purchaseUnitName='Corrugated']         |

      | VendorViewer            | ReadAll           |                                         |
      | VendorViewer            | ReadGeneralData   |                                         |

    Then the following roles , permissions and conditions assignment created successfully for "Vendor" entity:
      | Subrole                 | Permission        | Condition                               |
      | VendorOwner_Flexo       | Create            |                                         |
      | VendorOwner_Offset      | Create            |                                         |
      | VendorOwner_Digital     | Create            |                                         |
      | VendorOwner_Signmedia   | Create            |                                         |
      | VendorOwner_Textile     | Create            |                                         |
      | VendorOwner_Corrugated  | Create            |                                         |

      | VendorOwner_Flexo       | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | UpdateGeneralData | [purchaseUnitName='Flexible Packaging'] |
      | VendorOwner_Flexo       | Delete            | [purchaseUnitName='Flexible Packaging'] |

      | VendorOwner_Offset      | ReadAll           | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | ReadGeneralData   | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | UpdateGeneralData | [purchaseUnitName='Offset']             |
      | VendorOwner_Offset      | Delete            | [purchaseUnitName='Offset']             |

      | VendorOwner_Digital     | ReadAll           | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | ReadGeneralData   | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | UpdateGeneralData | [purchaseUnitName='Digital']            |
      | VendorOwner_Digital     | Delete            | [purchaseUnitName='Digital']            |

      | VendorOwner_Signmedia   | ReadAll           | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | ReadGeneralData   | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | UpdateGeneralData | [purchaseUnitName='Signmedia']          |
      | VendorOwner_Signmedia   | Delete            | [purchaseUnitName='Signmedia']          |

      | VendorOwner_Textile     | ReadAll           | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | ReadGeneralData   | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | UpdateGeneralData | [purchaseUnitName='Textile']            |
      | VendorOwner_Textile     | Delete            | [purchaseUnitName='Textile']            |

      | VendorOwner_Corrugated  | ReadAll           | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | ReadGeneralData   | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | UpdateGeneralData | [purchaseUnitName='Corrugated']         |
      | VendorOwner_Corrugated  | Delete            | [purchaseUnitName='Corrugated']         |

      | VendorViewer_Flexo      | ReadAll           | [purchaseUnitName='Flexible Packaging'] |
      | VendorViewer_Flexo      | ReadGeneralData   | [purchaseUnitName='Flexible Packaging'] |

      | VendorViewer_Offset     | ReadAll           | [purchaseUnitName='Offset']             |
      | VendorViewer_Offset     | ReadGeneralData   | [purchaseUnitName='Offset']             |

      | VendorViewer_Digital    | ReadAll           | [purchaseUnitName='Digital']            |
      | VendorViewer_Digital    | ReadGeneralData   | [purchaseUnitName='Digital']            |

      | VendorViewer_Signmedia  | ReadAll           | [purchaseUnitName='Signmedia']          |
      | VendorViewer_Signmedia  | ReadGeneralData   | [purchaseUnitName='Signmedia']          |

      | VendorViewer_Textile    | ReadAll           | [purchaseUnitName='Textile']            |
      | VendorViewer_Textile    | ReadGeneralData   | [purchaseUnitName='Textile']            |

      | VendorViewer_Corrugated | ReadAll           | [purchaseUnitName='Corrugated']         |
      | VendorViewer_Corrugated | ReadGeneralData   | [purchaseUnitName='Corrugated']         |

      | VendorViewer            | ReadAll           |                                         |
      | VendorViewer            | ReadGeneralData   |                                         |


