Feature: Automated PurchasingResponsible Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role          |
      | PurRespReader |
    Then the following roles created successfully:
      | Role          |
      | PurRespReader |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole       |
            #Flexo
      | PurchasingResponsible_Flexo      | PurRespReader |
      | Accountant_Flexo                 | PurRespReader |
      | B.U.Director_Flexo               | PurRespReader |

            #Offset
      | PurchasingResponsible_Offset     | PurRespReader |
      | Accountant_Offset                | PurRespReader |
      | B.U.Director_Offset              | PurRespReader |

            #Digital
      | PurchasingResponsible_Digital    | PurRespReader |
      | Accountant_Digital               | PurRespReader |
      | B.U.Director_Digital             | PurRespReader |

            #Signmedia
      | PurchasingResponsible_Signmedia  | PurRespReader |
      | Accountant_Signmedia             | PurRespReader |
      | B.U.Director_Signmedia           | PurRespReader |

            #Textile
      | PurchasingResponsible_Textile    | PurRespReader |
      | Accountant_Textile               | PurRespReader |
      | B.U.Director_Textile             | PurRespReader |

            #Corrugated
      | PurchasingResponsible_Corrugated | PurRespReader |
      | Accountant_Corrugated            | PurRespReader |
      | B.U.Director_Corrugated          | PurRespReader |

            #General
      | M.D                              | PurRespReader |
      | Deputy_M.D                       | PurRespReader |
      | Quality_Specialist               | PurRespReader |
      | Auditor                          | PurRespReader |
      | Corporate_Taxes_Accountant       | PurRespReader |
      | Corporate_Accounting_Head        | PurRespReader |

    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole       |
            #Flexo
      | PurchasingResponsible_Flexo      | PurRespReader |
      | Accountant_Flexo                 | PurRespReader |
      | B.U.Director_Flexo               | PurRespReader |

            #Offset
      | PurchasingResponsible_Offset     | PurRespReader |
      | Accountant_Offset                | PurRespReader |
      | B.U.Director_Offset              | PurRespReader |

            #Digital
      | PurchasingResponsible_Digital    | PurRespReader |
      | Accountant_Digital               | PurRespReader |
      | B.U.Director_Digital             | PurRespReader |

            #Signmedia
      | PurchasingResponsible_Signmedia  | PurRespReader |
      | Accountant_Signmedia             | PurRespReader |
      | B.U.Director_Signmedia           | PurRespReader |

            #Textile
      | PurchasingResponsible_Textile    | PurRespReader |
      | Accountant_Textile               | PurRespReader |
      | B.U.Director_Textile             | PurRespReader |

            #Corrugated
      | PurchasingResponsible_Corrugated | PurRespReader |
      | Accountant_Corrugated            | PurRespReader |
      | B.U.Director_Corrugated          | PurRespReader |

            #General
      | M.D                              | PurRespReader |
      | Deputy_M.D                       | PurRespReader |
      | Quality_Specialist               | PurRespReader |
      | Auditor                          | PurRespReader |
      | Corporate_Taxes_Accountant       | PurRespReader |
      | Corporate_Accounting_Head        | PurRespReader |


  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "PurchasingResponsible" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "PurchasingResponsible" entity permissions created successfully:
      | Permission |
      | ReadAll    |


  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "PurchasingResponsible" entity:
      | Subrole       | Permission | Condition |
      | PurRespReader | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "PurchasingResponsible" entity:
      | Subrole       | Permission | Condition |
      | PurRespReader | ReadAll    |           |