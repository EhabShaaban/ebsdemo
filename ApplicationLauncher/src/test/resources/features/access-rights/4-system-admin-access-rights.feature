Feature: Automated System Admins Access Rights Insertion

  Scenario: Insert users
    Given System admin is logged in
    When System admin wants to create the following users:
      | User  | ProfileName | Password |
      | admin | Admin       | madina   |
    Then the following users created successfully:
      | User  | ProfileName | Password |
      | admin | Admin       | madina   |

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role             |
      | SuperUser        |
      | SuperUserSubRole |
    Then the following roles created successfully:
      | Role             |
      | SuperUser        |
      | SuperUserSubRole |

  Scenario: Assign Users to Parent roles
    Given System admin is logged in
    When System admin wants to assign the following users to parent roles:
      | User  | Role      |
      | admin | SuperUser |
    Then the following users are assigned to parent roles:
      | User  | Role      |
      | admin | SuperUser |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role      | Subrole          |
      | SuperUser | SuperUserSubRole |
    Then the following parent roles are assigned to subroles:
      | Role      | Subrole          |
      | SuperUser | SuperUserSubRole |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "*" entity permissions:
      | Permission |
      | *          |
    Then the following "*" entity permissions created successfully:
      | Permission |
      | *          |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "*" entity:
      | Subrole          | Permission | Condition |
      | SuperUserSubRole | *          |           |
    Then the following roles , permissions and conditions assignment created successfully for "*" entity:
      | Subrole          | Permission | Condition |
      | SuperUserSubRole | *          |           |