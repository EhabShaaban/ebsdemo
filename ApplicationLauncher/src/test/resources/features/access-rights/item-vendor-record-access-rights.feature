Feature: Automated IVR Users Access Rights Insertion


  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role                        |
      | IVROwner_Flexo              |
      | IVROwner_Offset             |
      | IVROwner_Digital            |
      | IVROwner_Signmedia          |
      | IVROwner_Textile            |
      | IVROwner_Corrugated         |

      | IVRViewer_Flexo             |
      | IVRViewer_Offset            |
      | IVRViewer_Digital           |
      | IVRViewer_Signmedia         |
      | IVRViewer_Textile           |
      | IVRViewer_Corrugated        |

      | IVRViewerLimited_Flexo      |
      | IVRViewerLimited_Offset     |
      | IVRViewerLimited_Digital    |
      | IVRViewerLimited_Signmedia  |
      | IVRViewerLimited_Textile    |
      | IVRViewerLimited_Corrugated |

      | IVRViewer                   |
    Then the following roles created successfully:
      | Role                        |
      | IVROwner_Flexo              |
      | IVROwner_Offset             |
      | IVROwner_Digital            |
      | IVROwner_Signmedia          |
      | IVROwner_Textile            |
      | IVROwner_Corrugated         |

      | IVRViewer_Flexo             |
      | IVRViewer_Offset            |
      | IVRViewer_Digital           |
      | IVRViewer_Signmedia         |
      | IVRViewer_Textile           |
      | IVRViewer_Corrugated        |

      | IVRViewerLimited_Flexo      |
      | IVRViewerLimited_Offset     |
      | IVRViewerLimited_Digital    |
      | IVRViewerLimited_Signmedia  |
      | IVRViewerLimited_Textile    |
      | IVRViewerLimited_Corrugated |

      | IVRViewer                   |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                             | Subrole                     |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo              |
      | PurchasingResponsible_Offset     | IVROwner_Offset             |
      | PurchasingResponsible_Digital    | IVROwner_Digital            |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia          |
      | PurchasingResponsible_Textile    | IVROwner_Textile            |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated         |

      | LogisticsResponsible_Flexo       | IVRViewer_Flexo             |
      | LogisticsResponsible_Offset      | IVRViewer_Offset            |
      | LogisticsResponsible_Digital     | IVRViewer_Digital           |
      | LogisticsResponsible_Signmedia   | IVRViewer_Signmedia         |
      | LogisticsResponsible_Textile     | IVRViewer_Textile           |
      | LogisticsResponsible_Corrugated  | IVRViewer_Corrugated        |

      | Storekeeper_Flexo                | IVRViewerLimited_Flexo      |
      | Storekeeper_Offset               | IVRViewerLimited_Offset     |
      | Storekeeper_Digital              | IVRViewerLimited_Digital    |
      | Storekeeper_Signmedia            | IVRViewerLimited_Signmedia  |
      | Storekeeper_Textile              | IVRViewerLimited_Textile    |
      | Storekeeper_Corrugated           | IVRViewerLimited_Corrugated |

      | Accountant_Flexo                 | IVRViewer_Flexo             |
      | Accountant_Offset                | IVRViewer_Offset            |
      | Accountant_Digital               | IVRViewer_Digital           |
      | Accountant_Signmedia             | IVRViewer_Signmedia         |
      | Accountant_Textile               | IVRViewer_Textile           |
      | Accountant_Corrugated            | IVRViewer_Corrugated        |

      | AccountantHead_Flexo             | IVRViewer_Flexo             |
      | AccountantHead_Offset            | IVRViewer_Offset            |
      | AccountantHead_Digital           | IVRViewer_Digital           |
      | AccountantHead_Signmedia         | IVRViewer_Signmedia         |
      | AccountantHead_Textile           | IVRViewer_Textile           |
      | AccountantHead_Corrugated        | IVRViewer_Corrugated        |

      | B.U.Director_Flexo               | IVRViewer_Flexo             |
      | B.U.Director_Offset              | IVRViewer_Offset            |
      | B.U.Director_Digital             | IVRViewer_Digital           |
      | B.U.Director_Signmedia           | IVRViewer_Signmedia         |
      | B.U.Director_Textile             | IVRViewer_Textile           |
      | B.U.Director_Corrugated          | IVRViewer_Corrugated        |

      | M.D                              | IVRViewer                   |
      | Deputy_M.D                       | IVRViewer                   |
      | Quality_Specialist               | IVRViewer                   |
      | Auditor                          | IVRViewer                   |
      | Corporate_Taxes_Accountant       | IVRViewer                   |
      | Corporate_Accounting_Head        | IVRViewer                   |


    Then the following parent roles are assigned to subroles:
      | Role                             | Subrole                     |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo              |
      | PurchasingResponsible_Offset     | IVROwner_Offset             |
      | PurchasingResponsible_Digital    | IVROwner_Digital            |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia          |
      | PurchasingResponsible_Textile    | IVROwner_Textile            |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated         |

      | LogisticsResponsible_Flexo       | IVRViewer_Flexo             |
      | LogisticsResponsible_Offset      | IVRViewer_Offset            |
      | LogisticsResponsible_Digital     | IVRViewer_Digital           |
      | LogisticsResponsible_Signmedia   | IVRViewer_Signmedia         |
      | LogisticsResponsible_Textile     | IVRViewer_Textile           |
      | LogisticsResponsible_Corrugated  | IVRViewer_Corrugated        |

      | Storekeeper_Flexo                | IVRViewerLimited_Flexo      |
      | Storekeeper_Offset               | IVRViewerLimited_Offset     |
      | Storekeeper_Digital              | IVRViewerLimited_Digital    |
      | Storekeeper_Signmedia            | IVRViewerLimited_Signmedia  |
      | Storekeeper_Textile              | IVRViewerLimited_Textile    |
      | Storekeeper_Corrugated           | IVRViewerLimited_Corrugated |

      | Accountant_Flexo                 | IVRViewer_Flexo             |
      | Accountant_Offset                | IVRViewer_Offset            |
      | Accountant_Digital               | IVRViewer_Digital           |
      | Accountant_Signmedia             | IVRViewer_Signmedia         |
      | Accountant_Textile               | IVRViewer_Textile           |
      | Accountant_Corrugated            | IVRViewer_Corrugated        |

      | AccountantHead_Flexo             | IVRViewer_Flexo             |
      | AccountantHead_Offset            | IVRViewer_Offset            |
      | AccountantHead_Digital           | IVRViewer_Digital           |
      | AccountantHead_Signmedia         | IVRViewer_Signmedia         |
      | AccountantHead_Textile           | IVRViewer_Textile           |
      | AccountantHead_Corrugated        | IVRViewer_Corrugated        |

      | B.U.Director_Flexo               | IVRViewer_Flexo             |
      | B.U.Director_Offset              | IVRViewer_Offset            |
      | B.U.Director_Digital             | IVRViewer_Digital           |
      | B.U.Director_Signmedia           | IVRViewer_Signmedia         |
      | B.U.Director_Textile             | IVRViewer_Textile           |
      | B.U.Director_Corrugated          | IVRViewer_Corrugated        |

      | M.D                              | IVRViewer                   |
      | Deputy_M.D                       | IVRViewer                   |
      | Quality_Specialist               | IVRViewer                   |
      | Auditor                          | IVRViewer                   |
      | Corporate_Taxes_Accountant       | IVRViewer                   |
      | Corporate_Accounting_Head        | IVRViewer                   |


  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "ItemVendorRecord" entity permissions:
      | Permission                   |
      | Create                       |
      | Delete                       |
      | ReadAll                      |
      | ReadGeneralData              |
      | UpdateGeneralData            |
      | ReadAll_WithoutPrice         |
      | ReadGeneralData_WithoutPrice |
    Then the following "ItemVendorRecord" entity permissions created successfully:
      | Permission                   |
      | Create                       |
      | Delete                       |
      | ReadAll                      |
      | ReadGeneralData              |
      | UpdateGeneralData            |
      | ReadAll_WithoutPrice         |
      | ReadGeneralData_WithoutPrice |

  Scenario: Insert Entity Conditions
    Given System admin is logged in
    When System admin wants to create the following "ItemVendorRecord" entity conditions:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |
    Then the following "ItemVendorRecord" entity conditions created successfully:
      | Condition                               |
      | [purchaseUnitName='Flexible Packaging'] |
      | [purchaseUnitName='Offset']             |
      | [purchaseUnitName='Digital']            |
      | [purchaseUnitName='Signmedia']          |
      | [purchaseUnitName='Textile']            |
      | [purchaseUnitName='Corrugated']         |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "ItemVendorRecord" entity:
      | Subrole                     | Permission                   | Condition                               |
      | IVROwner_Flexo              | Create                       |                                         |
      | IVROwner_Offset             | Create                       |                                         |
      | IVROwner_Digital            | Create                       |                                         |
      | IVROwner_Signmedia          | Create                       |                                         |
      | IVROwner_Textile            | Create                       |                                         |
      | IVROwner_Corrugated         | Create                       |                                         |

      | IVROwner_Flexo              | ReadAll                      | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | ReadGeneralData              | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | UpdateGeneralData            | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | Delete                       | [purchaseUnitName='Flexible Packaging'] |

      | IVROwner_Offset             | ReadAll                      | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | ReadGeneralData              | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | UpdateGeneralData            | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | Delete                       | [purchaseUnitName='Offset']             |

      | IVROwner_Digital            | ReadAll                      | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | ReadGeneralData              | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | UpdateGeneralData            | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | Delete                       | [purchaseUnitName='Digital']            |

      | IVROwner_Signmedia          | ReadAll                      | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | ReadGeneralData              | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | UpdateGeneralData            | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | Delete                       | [purchaseUnitName='Signmedia']          |

      | IVROwner_Textile            | ReadAll                      | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | ReadGeneralData              | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | UpdateGeneralData            | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | Delete                       | [purchaseUnitName='Textile']            |

      | IVROwner_Corrugated         | ReadAll                      | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | ReadGeneralData              | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | UpdateGeneralData            | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | Delete                       | [purchaseUnitName='Corrugated']         |

      | IVRViewer_Flexo             | ReadAll                      | [purchaseUnitName='Flexible Packaging'] |
      | IVRViewer_Flexo             | ReadGeneralData              | [purchaseUnitName='Flexible Packaging'] |

      | IVRViewer_Offset            | ReadAll                      | [purchaseUnitName='Offset']             |
      | IVRViewer_Offset            | ReadGeneralData              | [purchaseUnitName='Offset']             |

      | IVRViewer_Digital           | ReadAll                      | [purchaseUnitName='Digital']            |
      | IVRViewer_Digital           | ReadGeneralData              | [purchaseUnitName='Digital']            |

      | IVRViewer_Signmedia         | ReadAll                      | [purchaseUnitName='Signmedia']          |
      | IVRViewer_Signmedia         | ReadGeneralData              | [purchaseUnitName='Signmedia']          |

      | IVRViewer_Textile           | ReadAll                      | [purchaseUnitName='Textile']            |
      | IVRViewer_Textile           | ReadGeneralData              | [purchaseUnitName='Textile']            |

      | IVRViewer_Corrugated        | ReadAll                      | [purchaseUnitName='Corrugated']         |
      | IVRViewer_Corrugated        | ReadGeneralData              | [purchaseUnitName='Corrugated']         |

      | IVRViewer                   | ReadAll                      |                                         |
      | IVRViewer                   | ReadGeneralData              |                                         |

      | IVRViewerLimited_Flexo      | ReadAll_WithoutPrice         | [purchaseUnitName='Flexible Packaging'] |
      | IVRViewerLimited_Flexo      | ReadGeneralData_WithoutPrice | [purchaseUnitName='Flexible Packaging'] |

      | IVRViewerLimited_Offset     | ReadAll_WithoutPrice         | [purchaseUnitName='Offset']             |
      | IVRViewerLimited_Offset     | ReadGeneralData_WithoutPrice | [purchaseUnitName='Offset']             |

      | IVRViewerLimited_Digital    | ReadAll_WithoutPrice         | [purchaseUnitName='Digital']            |
      | IVRViewerLimited_Digital    | ReadGeneralData_WithoutPrice | [purchaseUnitName='Digital']            |

      | IVRViewerLimited_Signmedia  | ReadAll_WithoutPrice         | [purchaseUnitName='Signmedia']          |
      | IVRViewerLimited_Signmedia  | ReadGeneralData_WithoutPrice | [purchaseUnitName='Signmedia']          |

      | IVRViewerLimited_Textile    | ReadAll_WithoutPrice         | [purchaseUnitName='Textile']            |
      | IVRViewerLimited_Textile    | ReadGeneralData_WithoutPrice | [purchaseUnitName='Textile']            |

      | IVRViewerLimited_Corrugated | ReadAll_WithoutPrice         | [purchaseUnitName='Corrugated']         |
      | IVRViewerLimited_Corrugated | ReadGeneralData_WithoutPrice | [purchaseUnitName='Corrugated']         |

    Then the following roles , permissions and conditions assignment created successfully for "ItemVendorRecord" entity:
      | Subrole                     | Permission                   | Condition                               |
      | IVROwner_Flexo              | Create                       |                                         |
      | IVROwner_Offset             | Create                       |                                         |
      | IVROwner_Digital            | Create                       |                                         |
      | IVROwner_Signmedia          | Create                       |                                         |
      | IVROwner_Textile            | Create                       |                                         |
      | IVROwner_Corrugated         | Create                       |                                         |

      | IVROwner_Flexo              | ReadAll                      | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | ReadGeneralData              | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | UpdateGeneralData            | [purchaseUnitName='Flexible Packaging'] |
      | IVROwner_Flexo              | Delete                       | [purchaseUnitName='Flexible Packaging'] |

      | IVROwner_Offset             | ReadAll                      | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | ReadGeneralData              | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | UpdateGeneralData            | [purchaseUnitName='Offset']             |
      | IVROwner_Offset             | Delete                       | [purchaseUnitName='Offset']             |

      | IVROwner_Digital            | ReadAll                      | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | ReadGeneralData              | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | UpdateGeneralData            | [purchaseUnitName='Digital']            |
      | IVROwner_Digital            | Delete                       | [purchaseUnitName='Digital']            |

      | IVROwner_Signmedia          | ReadAll                      | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | ReadGeneralData              | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | UpdateGeneralData            | [purchaseUnitName='Signmedia']          |
      | IVROwner_Signmedia          | Delete                       | [purchaseUnitName='Signmedia']          |

      | IVROwner_Textile            | ReadAll                      | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | ReadGeneralData              | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | UpdateGeneralData            | [purchaseUnitName='Textile']            |
      | IVROwner_Textile            | Delete                       | [purchaseUnitName='Textile']            |

      | IVROwner_Corrugated         | ReadAll                      | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | ReadGeneralData              | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | UpdateGeneralData            | [purchaseUnitName='Corrugated']         |
      | IVROwner_Corrugated         | Delete                       | [purchaseUnitName='Corrugated']         |

      | IVRViewer_Flexo             | ReadAll                      | [purchaseUnitName='Flexible Packaging'] |
      | IVRViewer_Flexo             | ReadGeneralData              | [purchaseUnitName='Flexible Packaging'] |

      | IVRViewer_Offset            | ReadAll                      | [purchaseUnitName='Offset']             |
      | IVRViewer_Offset            | ReadGeneralData              | [purchaseUnitName='Offset']             |

      | IVRViewer_Digital           | ReadAll                      | [purchaseUnitName='Digital']            |
      | IVRViewer_Digital           | ReadGeneralData              | [purchaseUnitName='Digital']            |

      | IVRViewer_Signmedia         | ReadAll                      | [purchaseUnitName='Signmedia']          |
      | IVRViewer_Signmedia         | ReadGeneralData              | [purchaseUnitName='Signmedia']          |

      | IVRViewer_Textile           | ReadAll                      | [purchaseUnitName='Textile']            |
      | IVRViewer_Textile           | ReadGeneralData              | [purchaseUnitName='Textile']            |

      | IVRViewer_Corrugated        | ReadAll                      | [purchaseUnitName='Corrugated']         |
      | IVRViewer_Corrugated        | ReadGeneralData              | [purchaseUnitName='Corrugated']         |

      | IVRViewer                   | ReadAll                      |                                         |
      | IVRViewer                   | ReadGeneralData              |                                         |

      | IVRViewerLimited_Flexo      | ReadAll_WithoutPrice         | [purchaseUnitName='Flexible Packaging'] |
      | IVRViewerLimited_Flexo      | ReadGeneralData_WithoutPrice | [purchaseUnitName='Flexible Packaging'] |

      | IVRViewerLimited_Offset     | ReadAll_WithoutPrice         | [purchaseUnitName='Offset']             |
      | IVRViewerLimited_Offset     | ReadGeneralData_WithoutPrice | [purchaseUnitName='Offset']             |

      | IVRViewerLimited_Digital    | ReadAll_WithoutPrice         | [purchaseUnitName='Digital']            |
      | IVRViewerLimited_Digital    | ReadGeneralData_WithoutPrice | [purchaseUnitName='Digital']            |

      | IVRViewerLimited_Signmedia  | ReadAll_WithoutPrice         | [purchaseUnitName='Signmedia']          |
      | IVRViewerLimited_Signmedia  | ReadGeneralData_WithoutPrice | [purchaseUnitName='Signmedia']          |

      | IVRViewerLimited_Textile    | ReadAll_WithoutPrice         | [purchaseUnitName='Textile']            |
      | IVRViewerLimited_Textile    | ReadGeneralData_WithoutPrice | [purchaseUnitName='Textile']            |

      | IVRViewerLimited_Corrugated | ReadAll_WithoutPrice         | [purchaseUnitName='Corrugated']         |
      | IVRViewerLimited_Corrugated | ReadGeneralData_WithoutPrice | [purchaseUnitName='Corrugated']         |


