Feature: Automated StockTypes Access Rights Insertion

  Scenario: Insert roles
    Given System admin is logged in
    When System admin wants to create the following roles :
      | Role             |
      | StockTypesViewer |
    Then the following roles created successfully:
      | Role             |
      | StockTypesViewer |

  Scenario: Assign roles to subroles
    Given System admin is logged in
    When System admin wants to assign the following parent roles to subroles:
      | Role                   | Subrole          |
      | Storekeeper_Flexo      | StockTypesViewer |
      | Storekeeper_Offset     | StockTypesViewer |
      | Storekeeper_Digital    | StockTypesViewer |
      | Storekeeper_Signmedia  | StockTypesViewer |
      | Storekeeper_Textile    | StockTypesViewer |
      | Storekeeper_Corrugated | StockTypesViewer |
    Then the following parent roles are assigned to subroles:
      | Role                   | Subrole          |
      | Storekeeper_Flexo      | StockTypesViewer |
      | Storekeeper_Offset     | StockTypesViewer |
      | Storekeeper_Digital    | StockTypesViewer |
      | Storekeeper_Signmedia  | StockTypesViewer |
      | Storekeeper_Textile    | StockTypesViewer |
      | Storekeeper_Corrugated | StockTypesViewer |

  Scenario: Insert Entity Permissions
    Given System admin is logged in
    When System admin wants to create the following "StockType" entity permissions:
      | Permission |
      | ReadAll    |
    Then the following "StockType" entity permissions created successfully:
      | Permission |
      | ReadAll    |

  Scenario: Assign Roles permissions and conditions
    Given System admin is logged in
    When System admin wants to assign the following roles , permissions and conditions for "StockType" entity:
      | Subrole          | Permission | Condition |
      | StockTypesViewer | ReadAll    |           |
    Then the following roles , permissions and conditions assignment created successfully for "StockType" entity:
      | Subrole          | Permission | Condition |
      | StockTypesViewer | ReadAll    |           |