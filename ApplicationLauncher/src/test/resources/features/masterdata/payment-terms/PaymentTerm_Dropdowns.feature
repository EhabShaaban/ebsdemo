Feature: View All payment term

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole           |
      | SalesSpecialist_Signmedia | PaymentTermViewer |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission           | Condition |
      | PaymentTermViewer | PaymentTerms:ReadAll |           |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
      | 0003 | 100% Advanced Payment               |

  Scenario: (01) Read list of PaymentTerms dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all PaymentTerms
    Then the following PaymentTerms values will be presented to "Ahmed.Al-Ashry":
      | PaymentTerms                               |
      | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 0002 - 20% advance, 80% Copy of B/L        |
      | 0003 - 100% Advanced Payment               |
    And total number of PaymentTerms returned to "Ahmed.Al-Ashry" is equal to 3

  @Future
  Scenario: (02) Read list of PaymentTerms dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all PaymentTerms
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page