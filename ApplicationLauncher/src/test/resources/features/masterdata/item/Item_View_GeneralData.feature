# Author: Somaya Aboulwafa, 21-Nov-2018
# Reviewer Analysis: hend ahmed
# Reviewer Quality : Marina

Feature: View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission           | Condition                       |
      | ItemOwner_Signmedia  | Item:ReadGeneralData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:ReadGeneralData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:ReadGeneralData | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole     | *:*                  |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission           | Condition                       |
      | Amr.Khalil  | Item:ReadGeneralData | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:ReadGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadGeneralData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | Item:ReadGeneralData |
    And the following ItemGroups exist:
      | Code   | Name                 | GroupLevel | ParentCode | ParentName     |
      | 000003 | White Signmedia Inks | LEAF       | 000002     | Signmedia Inks |
    And the following Measures exist:
      | Code | Type     | Symbol | Name         |
      | 0019 | Standard | M2     | Square Meter |
      | 0014 | Standard | Liter  | Liter        |
      | 0030 | Business | PC     | Piece        |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |
    And the following GeneralData for Items exist:
      | ItemCode | ItemName                                              | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | PurchasingUnit                     | CreationDate         | MarketName                                            | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll                    | 000003    | 0019     | FALSE        | 12345678910111213 | 0002                               | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll                    | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 000003    | 0019     | FALSE        | 12345678910111213 | 0001                               | 05-Aug-2018 09:02 AM | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | COMMERCIAL | 0002 - Mahmoud Amr   |
      | 000012   | Ink2                                                  | 000003    | 0014     | FALSE        | 12345678910111213 | 0006                               | 05-Aug-2018 09:02 AM | Ink2                                                  | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000008   | PRO-V-ST-2                                            | 000003    | 0019     | FALSE        | 12345678910111213 | 0001, 0002                         | 05-Aug-2018 09:02 AM | PRO-V-ST-2                                            | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000053   | Hot Laminated Frontlit Fabric roll 2                  | 000003    | 0019     | FALSE        | 12345678910111213 | 0002                               | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll 2                  | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000014   | Insurance service                                     |           | 0019     |              | 12345678910111231 | 0001, 0002, 0003, 0004, 0005, 0006 | 04-Feb-2020 09:27 AM | Insurance service                                     | SERVICE    | 0001 - Mohamed Nabil |
      | 000015   | Laptop HP Pro-book core i5                            |           | 0019     |              | 12345678910111231 | 0001, 0002, 0003, 0004, 0005, 0006 | 04-Feb-2020 09:27 AM | Laptop HP Pro-book core i5                            | CONSUMABLE | 0001 - Mohamed Nabil |


#    # EBS-1383
  Scenario Outline: (01) View GeneralData section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view GeneralData section of Item with code "<ItemCode>"
    Then the following values of GeneralData section for Item with code "<ItemCode>" are displayed to "Gehan.Ahmed":
      | ItemName   | ItemGroup   | BaseUnit   | BatchManaged   | OldItemReference   | PurchasingUnit   | CreationDate   | MarketName   | Type   | ProductManager   |
      | <ItemName> | <ItemGroup> | <BaseUnit> | <BatchManaged> | <OldItemReference> | <PurchasingUnit> | <CreationDate> | <MarketName> | <Type> | <ProductManager> |
    Examples:
      | ItemCode | ItemName                           | ItemGroup            | BaseUnit     | BatchManaged | OldItemReference  | PurchasingUnit                                         | CreationDate         | MarketName                         | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll | White Signmedia Inks | Square Meter | FALSE        | 12345678910111213 | Signmedia                                              | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000014   | Insurance service                  |                      | Square Meter |              | 12345678910111231 | Flexo, Signmedia, Offset, Digital, Textile, Corrugated | 04-Feb-2020 09:27 AM | Insurance service                  | SERVICE    | 0001 - Mohamed Nabil |
      | 000015   | Laptop HP Pro-book core i5         |                      | Square Meter |              | 12345678910111231 | Flexo, Signmedia, Offset, Digital, Textile, Corrugated | 04-Feb-2020 09:27 AM | Laptop HP Pro-book core i5         | CONSUMABLE | 0001 - Mohamed Nabil |

    # EBS-1383
  Scenario Outline: (02) View GeneralData section by an authorized user who has two roles - AmrKhalil+Flexo,Corrugated  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view GeneralData section of Item with code "<ItemCode>"
    Then the following values of GeneralData section for Item with code "<ItemCode>" are displayed to "Amr.Khalil":
      | ItemName   | ItemGroup   | BaseUnit   | BatchManaged   | OldItemReference   | PurchasingUnit   | CreationDate   | MarketName   | Type   | ProductManager   |
      | <ItemName> | <ItemGroup> | <BaseUnit> | <BatchManaged> | <OldItemReference> | <PurchasingUnit> | <CreationDate> | <MarketName> | <Type> | <ProductManager> |
    Examples:
      | ItemCode | ItemName                                              | ItemGroup            | BaseUnit     | BatchManaged | OldItemReference  | PurchasingUnit   | CreationDate         | MarketName                                            | Type       | ProductManager       |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | White Signmedia Inks | Square Meter | FALSE        | 12345678910111213 | Flexo            | 05-Aug-2018 09:02 AM | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | COMMERCIAL | 0002 - Mahmoud Amr   |
      | 000012   | Ink2                                                  | White Signmedia Inks | Liter        | FALSE        | 12345678910111213 | Corrugated       | 05-Aug-2018 09:02 AM | Ink2                                                  | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000008   | PRO-V-ST-2                                            | White Signmedia Inks | Square Meter | FALSE        | 12345678910111213 | Flexo, Signmedia | 05-Aug-2018 09:02 AM | PRO-V-ST-2                                            | COMMERCIAL | 0001 - Mohamed Nabil |

    # EBS-1383
  Scenario Outline: (03) View GeneralData section where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "<ItemCode>" successfully
    When "Gehan.Ahmed" requests to view GeneralData section of Item with code "<ItemCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | ItemCode |
      | 000053   |
      | 000014   |
      | 000015   |

 # EBS-1383
  Scenario Outline: (04) View GeneralData section by unauthorized user (Authorization error)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view GeneralData section of Item with code "<ItemCode>"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
    Examples:
      | ItemCode |
      | 000001   |
      | 000014   |
      | 000015   |
    # EBS-1383
  Scenario: (05) View GeneralData section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view GeneralData section of Item with code "000001"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
