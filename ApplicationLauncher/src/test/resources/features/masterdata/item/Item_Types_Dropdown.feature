#Author: Ahmad Hamed

Feature: View Item Types

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole        |
      | SalesSpecialist_Signmedia | ItemTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole        | Permission       | Condition |
      | ItemTypeViewer | ItemType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | ItemType:ReadAll |
    And the following ItemTypes exist:
      | Code       |
      | COMMERCIAL |
      | SERVICE    |
      | CONSUMABLE |

    #EBS-6237
  Scenario: (01) Read list of Item Types dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read Item Types
    Then the following ItemTypes values will be presented to "Ahmed.Al-Ashry":
      | Code       |
      | COMMERCIAL |
      | SERVICE    |
      | CONSUMABLE |
    And total number of records returned to "Ahmed.Al-Ashry" is "3"

    #EBS-6237
  Scenario: (02) Read list of Item Types dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read Item Types
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page