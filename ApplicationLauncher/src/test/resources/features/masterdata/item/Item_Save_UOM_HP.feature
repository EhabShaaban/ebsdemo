#Author : Marina,Sarah  (18/12/2018)


Feature: Request Save UnitOfMeasures section section (Happy Path)

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Signmedia  | MeasuresViewer       |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Flexo      | MeasuresViewer       |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | PurchasingResponsible_Corrugated | MeasuresViewer       |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission         | Condition                       |
      | ItemOwner_Signmedia  | Item:UpdateUoMData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:UpdateUoMData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:UpdateUoMData | [purchaseUnitName='Corrugated'] |
      | MeasuresViewer       | Measures:ReadAll   |                                 |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000007 | PRO-V-ST-1                                              | 0002           |
      | 000012 | Ink2                                                    | 0006           |
    And the following UnitOfMeasures section exist in Item with code "000007":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0029          | 110              | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000006":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0035          | 63.5             | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000012":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0042          | 2                | 12345678910111238 |
      | 0043          | 2.5              | 12345678910111239 |

  Scenario: (01) Save UnitOfMeasures section within edit session - with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 160              | 12345678910111300 |
    Then Item with Code "000007" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And UnitOfMeasures section of Item with code "000007" is updated as follows:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 160              | 12345678910111300 |
    And the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  Scenario: (02) Save UnitOfMeasures section within edit session - with one role (Save after deleting one UOM) (Happy Path)
    Given user is logged in as "Amr.Khalil"
    And UnitOfMeasures section of Item with code "000012" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    When "Amr.Khalil" saves UnitOfMeasures section of Item with code "000012" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0042              | Bottle-2Liter | 10               | 12345678910111302 |
    Then Item with Code "000012" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Amr.Khalil    | 07-Jan-2019 09:30 AM |
    And UnitOfMeasures section of Item with code "000012" is updated as follows:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0042              | Bottle-2Liter | 10               | 12345678910111302 |
    And the lock by "Amr.Khalil" on UnitOfMeasures section of Item with code "000012" is released
    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-04"

  Scenario: (03) Save UnitOfMeasures section within edit session - with one role (Save after deleting all UOMs) (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with empty list
    Then Item with Code "000007" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And UnitOfMeasures section of Item with code "000007" is updated with empty list
    And the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  Scenario: (04) Save UnitOfMeasures section within edit session - with Two role-Flexo  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    And UnitOfMeasures section of Item with code "000006" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    When "Amr.Khalil" saves UnitOfMeasures section of Item with code "000006" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0035              | Roll 1.27x50  | 63.5             | 12345678910111231 |
      | 0024              | Container FCL | 100              | 12345678910111302 |
    Then Item with Code "000006" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Amr.Khalil    | 07-Jan-2019 09:30 AM |
    And UnitOfMeasures section of Item with code "000006" is updated as follows:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0035              | Roll 1.27x50  | 63.5             | 12345678910111231 |
      | 0024              | Container FCL | 100              | 12345678910111302 |
    And the lock by "Amr.Khalil" on UnitOfMeasures section of Item with code "000006" is released
    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-04"

  Scenario: (05) Save UnitOfMeasures section within edit session - with Two role-Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    And UnitOfMeasures section of Item with code "000012" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    When "Amr.Khalil" saves UnitOfMeasures section of Item with code "000012" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit   | ConversionFactor | OldItemNumber     |
      | 0042              | Bottle-2Liter   | 10               | 12345678910111302 |
      | 0043              | Bottle-2.5Liter | 2                | 12345678910111304 |
      | 0040              | Bottle-1Liter   | 2                | 12345678910111304 |
      | 0041              | Bottle-1.5Liter | 5                | 12345678910111303 |
      | 0039              | Drum-70Kg       | 12               | 12345678910111301 |
    Then Item with Code "000012" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Amr.Khalil    | 07-Jan-2019 09:30 AM |
    And UnitOfMeasures section of Item with code "000012" is updated as follows:
      | AlternateUnitCode | AlternateUnit   | ConversionFactor | OldItemNumber     |
      | 0042              | Bottle-2Liter   | 10               | 12345678910111302 |
      | 0043              | Bottle-2.5Liter | 2                | 12345678910111304 |
      | 0040              | Bottle-1Liter   | 2                | 12345678910111304 |
      | 0041              | Bottle-1.5Liter | 5                | 12345678910111303 |
      | 0039              | Drum-70Kg       | 12               | 12345678910111301 |
    And the lock by "Amr.Khalil" on UnitOfMeasures section of Item with code "000012" is released
    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-04"