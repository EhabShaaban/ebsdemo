# Author: Aya Sadek, 18-Dec-2018
# Reviewer: Hosam Bayomy, Somaya Ahmed, 19-Dec-2018

Feature: Save Item GeneralData HP

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition                       |
      | ItemOwner_Signmedia  | Item:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
    And the following GeneralData for Items exist:
      | ItemCode | ItemName                                              | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | PurchasingUnit | CreationDate         | MarketName                                            | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll                    | 000003    | 0019     | FALSE        | 12345678910111213 | 0002           | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll                    | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 000003    | 0019     | FALSE        | 12345678910111213 | 0001           | 05-Aug-2018 09:02 AM | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | COMMERCIAL | 0002 - Mahmoud Amr   |
      | 000012   | Ink2                                                  | 000003    | 0014     | FALSE        | 12345678910111213 | 0006           | 05-Aug-2018 09:02 AM | Ink2                                                  | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000008   | PRO-V-ST-2                                            | 000003    | 0019     | FALSE        | 12345678910111213 | 0001, 0002     | 05-Aug-2018 09:02 AM | PRO-V-ST-2                                            | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000051   | Ink5                                                  | 000003    | 0019     | FALSE        | 12345678910111213 | 0001, 0002     | 05-Aug-2018 09:02 AM | Ink5                                                  | COMMERCIAL | 0001 - Mohamed Nabil |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0006 | Corrugated |
    And the following IVR exist:
      | ItemCode | VendorCode | PurchasingUnit |
      | 000051   | 000001     | 0002           |
    And the following ProductManagers exist:
      | Code | Name          |
      | 0001 | Mohamed Nabil |
      | 0002 | Mahmoud Amr   |
      | 0003 | Moustafa      |
    And edit session is "30" minutes

  @ResetData
  Scenario Outline:(01) Save GeneralData section within edit session by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of Item with code "<ItemCodeValue>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" saves GeneralData section of Item with Code "<ItemCodeValue>" at "07-Jan-2019 09:30 AM" with the following values:
      | Name               | PurchasingUnit           | MarketName      | ProductManager   |
      | <NewItemNameValue> | <NewPurchasingUnitValue> | <NewMarketName> | <ProductManager> |
    Then GeneralData Section of Item with Code "<ItemCodeValue>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | ItemCode        | ItemName           | ItemGroup        | BaseUnit        | BatchManaged        | OldItemReference        | PurchasingUnit           | CreationDate        | MarketName      | Type   | ProductManager   |
      | <User>        | 07-Jan-2019 09:30 AM | <ItemCodeValue> | <NewItemNameValue> | <ItemGroupValue> | <BaseUnitValue> | <BatchManagedValue> | <OldItemReferenceValue> | <NewPurchasingUnitValue> | <CreationDateValue> | <NewMarketName> | <Type> | <ProductManager> |
    And the lock by "<User>" on GeneralData section of Item with code "<ItemCodeValue>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | User        | ItemCodeValue | NewItemNameValue    | ItemGroupValue | BaseUnitValue | BatchManagedValue | OldItemReferenceValue | NewPurchasingUnitValue | CreationDateValue    | NewMarketName                                              | Type       | ProductManager       |
      | Gehan.Ahmed | 000001        | Test_Gehan          | 000003         | 0019          | FALSE             | 12345678910111213     | 0002, 0001, 0003       | 05-Aug-2018 09:02 AM | Test Hot Laminated Frontlit Fabric roll                    | COMMERCIAL | 0003 - Moustafa      |
      | Gehan.Ahmed | 000008        | Test_PRO-V-ST-2     | 000003         | 0019          | FALSE             | 12345678910111213     | 0001, 0002, 0004       | 05-Aug-2018 09:02 AM | Test PRO-V-ST-2                                            | COMMERCIAL | 0002 - Mahmoud Amr   |
      | Amr.Khalil  | 000005        | Test_Amr_Flexo      | 000003         | 0019          | FALSE             | 12345678910111213     | 0001, 0006             | 05-Aug-2018 09:02 AM | Test self adhesive vinal 100 micron 140 grm glossy 1.07x50 | COMMERCIAL | 0001 - Mohamed Nabil |
      | Amr.Khalil  | 000012        | Test_Amr_Corrugated | 000003         | 0014          | FALSE             | 12345678910111213     | 0001, 0006             | 05-Aug-2018 09:02 AM | Test Ink2                                                  | COMMERCIAL | 0001 - Mohamed Nabil |
      | Gehan.Ahmed | 000008        | Test_PRO-V-ST-2     | 000003         | 0019          | FALSE             | 12345678910111213     | 0001, 0002, 0003       | 05-Aug-2018 09:02 AM | Test PRO-V-ST-2                                            | COMMERCIAL | 0003 - Moustafa      |

  #EBS-2899
  @ResetData
  Scenario: (02) Save GeneralData section within Delete purchasing unit of item that hasn't record in IVR  (Happy Path) User two roles
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000051" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000051" at "07-Jan-2019 09:30 AM" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Ink5 | 0002           | Test Ink5  | 0001 - Mohamed Nabil |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Item with code "000051" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"