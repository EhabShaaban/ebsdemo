Feature: View AccountingInfo section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission                 | Condition                       |
      | ItemOwner_Signmedia  | Item:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia  | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole     | *:*                        |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                 | Condition                       |
      | Amr.Khalil  | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission                 |
      | Afaf | Item:ReadAccountingDetails |
    And the following AccountingDetails for Items exist:
      | ItemCode | AccountCode | AccountName | BusinessUnit |
      | 000001   | 10207       | PO          | Signmedia    |
      | 000055   | 10207       | PO          | Signmedia    |

  # EBS-1383
  Scenario: (01) View AccountingInfo section by an authorizd user for (Commercial/Consumable) Item(Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view AccoutingInfo section of Item with code "000001"
    Then the following values of AccoutingInfo section for Item with code "000001" are displayed to "Gehan.Ahmed":
      | AccountCode | AccountName |
      | 10207       | PO          |

  Scenario: (02) View AccountingInfo section by an authorizd user for Service Item (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view AccoutingInfo section of Item with code "000055"
    Then the following values of AccoutingInfo section for Service Item with code "000055" are displayed to "Gehan.Ahmed":
      | AccountCode | AccountName | CostFactor |
      | 10207       | PO          | true       |

  Scenario: (03) View AccountingInfo section where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to view AccoutingInfo section of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (04) View AccountingInfo section an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view AccoutingInfo section of Item with code "000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (05) View AccountingInfo section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view AccoutingInfo section of Item with code "000001"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page