Feature: Delete Item

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission  | Condition                       |
      | ItemOwner_Signmedia  | Item:Delete | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:Delete | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:Delete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole     | *:*         |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission  | Condition                       |
      | Gehan.Ahmed | Item:Delete | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:Delete | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Item:Delete | [purchaseUnitName='Signmedia']  |
    And the following users doesn't have the following permissions:
      | User              | Permission  |
      | Mahmoud.Abdelaziz | Item:Delete |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000007 | PRO-V-ST-1                                              | 0002           |
      | 000008 | PRO-V-ST-2                                              | 0001, 0002     |
      | 000009 | Machine1                                                | 0001           |
      | 000052 | Ink7                                                    | 0006           |
    And the following IVR exits:
      | ItemCode | VendorCode |
      | 000006   | 000004     |
    And the following PurchaseOrders exist:
      | Code       |
      | 2018000001 |
    And PurchaseOrder with code "2018000001" has the following OrderItems:
      | Item Code |
      | 000001    |

  # EBS-1308
  Scenario Outline:(01) Delete Item by an authorized user who has one or two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with code "<ItemCode>"
    Then Item with code "<ItemCode>" is deleted from the system
    And a success notification is sent to "<User>" with the following message "Gen-msg-12"
    Examples:
      | User        | ItemCode |
      | Gehan.Ahmed | 000007   |
      | Gehan.Ahmed | 000008   |
      | Amr.Khalil  | 000008   |
      | Amr.Khalil  | 000009   |
      | Amr.Khalil  | 000052   |

  # EBS-1308
  Scenario: (02) Delete Item, where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000007" successfully
    When "Gehan.Ahmed" requests to delete Item with code "000007"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-1308
  Scenario Outline: (03) Delete Item, where Item is referenced by anbother object (e.g.IVR, PO) (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with code "<ItemCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-27"
    Examples:
      | User        | ItemCode |
      | Gehan.Ahmed | 000001   |
      | Amr.Khalil  | 000006   |

  # EBS-1321
  Scenario Outline: (04) Delete Item that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<ItemSection>" of Item with code "000007" in edit mode
    When "Gehan.Ahmed" requests to delete Item with code "000007"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | ItemSection    |
      | GeneralData    |
      | UnitOfMeasures |

 # EBS-4091
  Scenario Outline: (05) Delete Item that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "Gehan.Ahmed"
    And first "Gehan.Ahmed" opens "<ItemSection>" of Item with code "000007" in edit mode
    When "Gehan.Ahmed" requests to delete Item with code "000007"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-41"
    Examples:
      | ItemSection    |
      | GeneralData    |
      | UnitOfMeasures |

  # EBS-1308
  Scenario: (06) Delete Item by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000007"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  # EBS-1308
  Scenario: (07) Delete Item by an unauthorized user due to unmatched condition (Exception Case: because there may be a case where user can view a record while not authorized to delete it based on condition)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to delete Item with code "000007"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page