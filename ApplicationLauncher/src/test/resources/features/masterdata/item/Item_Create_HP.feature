# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmad Hamed due to update required by story EBS-6237

Feature: Create Item - Happy Path

  Background:

    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia  | ItemGroupViewer          |
      | PurchasingResponsible_Signmedia  | MeasuresViewer           |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | ItemTypeViewer           |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo          |
      | PurchasingResponsible_Flexo      | ItemGroupViewer          |
      | PurchasingResponsible_Flexo      | MeasuresViewer           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | ItemTypeViewer           |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated     |
      | PurchasingResponsible_Corrugated | ItemGroupViewer          |
      | PurchasingResponsible_Corrugated | MeasuresViewer           |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | ItemTypeViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition |
      | ItemOwner_Signmedia      | Item:Create            |           |
      | ItemOwner_Flexo          | Item:Create            |           |
      | ItemOwner_Corrugated     | Item:Create            |           |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |           |
      | ItemGroupViewer          | ItemGroup:ReadAll      |           |
      | MeasuresViewer           | Measures:ReadAll       |           |
      | ItemTypeViewer           | ItemType:ReadAll       |           |
    And the following ItemGroups exist:
      | Code   | Name                 | GroupLevel | ParentCode | ParentName     |
      | 000001 | Ink                  | ROOT       |            |                |
      | 000002 | Signmedia Inks       | HIERARCHY  | 000001     | Ink            |
      | 000003 | White Signmedia Inks | LEAF       | 000002     | Signmedia Inks |
    And the following Measures exist:
      | Code | Type     | Symbol | Name  |
      | 0001 | Standard | M      | Meter |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following ItemTypes exist:
      | Code       |
      | COMMERCIAL |
      | SERVICE    |
      | CONSUMABLE |

  ################ Create Happy Paths

  Scenario Outline: (01) Create Item Commercial with all mandatory fields by an authorized user who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created Item was with code "000050"
    When "<User>" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | <PurchasingUnitValue> | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then a new Item is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | ItemCode | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                                             | Type       |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051   | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | <PurchasingUnitValue> | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PurchasingUnitValue |
      | Gehan.Ahmed | 0002                |
      | Gehan.Ahmed | 0003, 0004, 0005    |
      | Gehan.Ahmed | 0001, 0006          |
      | Amr.Khalil  | 0002                |
      | Amr.Khalil  | 0003, 0004, 0005    |
      | Amr.Khalil  | 0001, 0006          |

  Scenario Outline: (02) Create Item Service with all mandatory fields by an authorized user who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created Item was with code "000050"
    When "<User>" creates Item with the following values:
      | Name              | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName        | Type    |
      | Insurance Service |           | 0001     |              | 123467891011     | <PurchasingUnitValue> | Insurance Service | SERVICE |
    Then a new Item is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | ItemCode | Name              | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName        | Type    |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051   | Insurance Service |           | 0001     |              | 123467891011     | <PurchasingUnitValue> | Insurance Service | SERVICE |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PurchasingUnitValue |
      | Gehan.Ahmed | 0002                |
      | Gehan.Ahmed | 0003, 0004, 0005    |
      | Gehan.Ahmed | 0001, 0006          |
      | Amr.Khalil  | 0002                |
      | Amr.Khalil  | 0003, 0004, 0005    |
      | Amr.Khalil  | 0001, 0006          |

  Scenario Outline: (03) Create Item Consumable with all mandatory fields by an authorized user who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created Item was with code "000050"
    When "<User>" creates Item with the following values:
      | Name                       | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                 | Type       |
      | Laptop HP Pro-book core i5 |           | 0001     |              | 123467891011     | <PurchasingUnitValue> | Laptop HP Pro-book core i5 | CONSUMABLE |
    Then a new Item is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | ItemCode | Name                       | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                 | Type       |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051   | Laptop HP Pro-book core i5 |           | 0001     |              | 123467891011     | <PurchasingUnitValue> | Laptop HP Pro-book core i5 | CONSUMABLE |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PurchasingUnitValue |
      | Gehan.Ahmed | 0002                |
      | Gehan.Ahmed | 0003, 0004, 0005    |
      | Gehan.Ahmed | 0001, 0006          |
      | Amr.Khalil  | 0002                |
      | Amr.Khalil  | 0003, 0004, 0005    |
      | Amr.Khalil  | 0001, 0006          |
