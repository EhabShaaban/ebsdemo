#Author: Aya Sadek on Date: 03-Jan-2019, 01:54 PM
#Reviewer: Hend Ahmed  Date: 08-Jan-2019, 01:58 PM

Feature: Save Item GeneralData Val

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia | ProductManagerViewer    |
      | SuperUser                       | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission             | Condition                      |
      | ItemOwner_Signmedia     | Item:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll |                                |
      | ProductManagerViewer    | ProductManager:ReadAll |                                |
      | SuperUserSubRole        | *:*                    |                                |
    And the following GeneralData for Items exist:
      | ItemCode | ItemName                           | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | PurchasingUnit | CreationDate         | MarketName                         | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll | 000003    | 0019     | FALSE        | 12345678910111213 | 0002           | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000007   | PRO-V-ST-1                         | 000003    | 0019     | FALSE        | 12345678910111213 | 0002           | 05-Aug-2018 09:02 AM | PRO-V-ST-1                         | COMMERCIAL | 0001 - Mohamed Nabil |
      | 000051   | Ink5                               | 000003    | 0019     | FALSE        | 12345678910111213 | 0002,0001      | 05-Aug-2018 09:02 AM | Ink5                               | COMMERCIAL | 0001 - Mohamed Nabil |
    And the following IVR exist:
      | ItemCode | VendorCode | PurchasingUnit |
      | 000051   | 000001     | 0002           |
    And the following ProductManagers exist:
      | Code | Name          |
      | 0001 | Mohamed Nabil |
      | 0002 | Mahmoud Amr   |
      | 0003 | Moustafa      |
    And edit session is "30" minutes

  ###### Save GeneralData section with Incorrect Data (Validation Failure)  ##############################################################
  #EBS-1425
  Scenario Outline: (01) Save GeneralData section with Incorrect data: Instance doesn't exist for Items (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | Name   | PurchasingUnit   | MarketName   | ProductManager   |
      | <Name> | <PurchasingUnit> | <MarketName> | <ProductManager> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<FieldName>" field "<Massage>" and sent to "Gehan.Ahmed"
    Examples:
      | Name | PurchasingUnit | MarketName | ProductManager       | FieldName          | Massage     |
      | Test | 9999           | Test       | 0001 - Mohamed Nabil | purchaseUnits      | Item-msg-03 |
      | Test | 0002           | Test       | 9999 - Mohamed       | productManagerCode | Item-msg-05 |

  #EBS-2899
  Scenario: (02) Save GeneralData section with Delete purchasing unit of item that has record in IVR with this purchsing unit (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000051" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000051" at "07-Jan-2019 09:30 AM" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Ink5 | 0001           | Test       | 0001 - Mohamed Nabil |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to field "Item-msg-04" and sent to "Gehan.Ahmed"

  ####### Save GeneralData section with Malicious Input  (Abuse Cases )####################################################################
  #EBS-1425
  Scenario Outline: (03) Save GeneralData section with Malicious Input: Field is string for Items (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | Name               | PurchasingUnit           | MarketName | ProductManager   |
      | <NewItemNameValue> | <NewPurchasingUnitValue> | Test       | <ProductManager> |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Item with code "000001" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | NewItemNameValue                                                                                      | NewPurchasingUnitValue | ProductManager       |
      | Test                                                                                                  | Ay 7aga                | 0001 - Mohamed Nabil |
      | Test                                                                                                  | 1234564                | 0001 - Mohamed Nabil |
      | Test                                                                                                  | 12345$#$               | 0001 - Mohamed Nabil |
      | Test@                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test#                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test$                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test^                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test&                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test~                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test+                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test<                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test>                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | "Test"                                                                                                | 0001, 0002             | 0001 - Mohamed Nabil |
      | 'TEst'                                                                                                | 0001, 0002             | 0001 - Mohamed Nabil |
      | [Test]                                                                                                | 0001, 0002             | 0001 - Mohamed Nabil |
      | {Test}                                                                                                | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test?                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test=                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | \|Test                                                                                                | 0001, 0002             | 0001 - Mohamed Nabil |
      | \|\|Test                                                                                              | 0001, 0002             | 0001 - Mohamed Nabil |
      | \Test                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test!                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test:                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test;                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test�                                                                                                 | 0001, 0002             | 0001 - Mohamed Nabil |
      | TTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest | 0001, 0002             | 0001 - Mohamed Nabil |
      | Test                                                                                                  | 0001, 0002             | Ay7aga               |

  ####### Save GeneralData section with Malicious Input  (Abuse Cases )####################################################################
  #EBS-1425
  Scenario Outline: (04) Save GeneralData section with Malicious Input: Field is string for Items (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | Name | PurchasingUnit | MarketName          | ProductManager       |
      | Test | 0001, 0002     | <NewItemMarketName> | 0001 - Mohamed Nabil |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Item with code "000001" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | NewItemMarketName                                                                                     |
      | Test@                                                                                                 |
      | Test#                                                                                                 |
      | Test$                                                                                                 |
      | Test^                                                                                                 |
      | Test&                                                                                                 |
      | Test~                                                                                                 |
      | Test+                                                                                                 |
      | Test<                                                                                                 |
      | Test>                                                                                                 |
      | "Test"                                                                                                |
      | 'TEst'                                                                                                |
      | [Test]                                                                                                |
      | {Test}                                                                                                |
      | Test?                                                                                                 |
      | Test=                                                                                                 |
      | \|Test                                                                                                |
      | \|\|Test                                                                                              |
      | \Test                                                                                                 |
      | Test!                                                                                                 |
      | Test:                                                                                                 |
      | Test;                                                                                                 |
      | Test�                                                                                                 |
      | TTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest |

  ############ Save GeneralData section with missing mandatory fields (Abuse Cases) #########################################################
  #EBS-1425
  Scenario Outline: (05) Save GeneralData section with Missing mandatory Fields for Items  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | Name               | PurchasingUnit           | MarketName          | ProductManager   |
      | <NewItemNameValue> | <NewPurchasingUnitValue> | <NewItemMarketName> | <ProductManager> |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Item with code "000001" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | NewItemNameValue | NewPurchasingUnitValue | NewItemMarketName | ProductManager       |
      | Test             | 0001, 0002             |                   | 0001 - Mohamed Nabil |
      | Test             |                        | Test              | 0001 - Mohamed Nabil |
      |                  | 0001, 0002             | Test              | 0001 - Mohamed Nabil |
      | Test             | 0001, 0002             | Test              |                      |

  ######### Save GeneralData section (After edit session expires) ################################################################################
  #EBS-1425
  Scenario: (06) Save GeneralData section after edit session expired for Items (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000001" at "07-Jan-2019 09:41 AM" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Test | 0001, 0002     | Test       | 0001 - Mohamed Nabil |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-1425
  Scenario: (07) Save GeneralData section after edit session expired & Item is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the Item with code "000007" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" saves GeneralData section of Item with Code "000007" at "07-Jan-2019 09:45 AM" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Test | 0001, 0002     | Test       | 0001 - Mohamed Nabil |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"