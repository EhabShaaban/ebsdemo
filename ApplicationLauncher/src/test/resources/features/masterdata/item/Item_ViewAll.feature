# Reviewer: Hend and Somaya (06-Feb-2019 9:30 am)
# Updated by Ahmad Hamed due to update required by story EBS-6237 and EBS-6239, Reviewer: Somaya Ahmed

Feature: View all Items

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | hr1          | SuperUser                        |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role                 |
      | SuperUser                        | SuperUserSubRole         |
      | M.D                              | ItemViewer               |
      | M.D                              | ItemGroupViewer          |
      | M.D                              | MeasuresViewer           |
      | M.D                              | PurUnitReader_Signmedia  |
      | M.D                              | ItemTypeViewer           |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia  | ItemGroupViewer          |
      | PurchasingResponsible_Signmedia  | MeasuresViewer           |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | ItemTypeViewer           |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo          |
      | PurchasingResponsible_Flexo      | ItemGroupViewer          |
      | PurchasingResponsible_Flexo      | MeasuresViewer           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | ItemTypeViewer           |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated     |
      | PurchasingResponsible_Corrugated | ItemGroupViewer          |
      | PurchasingResponsible_Corrugated | MeasuresViewer           |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | ItemTypeViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition                       |
      | SuperUserSubRole         | *:*                    |                                 |
      | ItemViewer               | Item:ReadAll           |                                 |
      | ItemOwner_Signmedia      | Item:ReadAll           | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo          | Item:ReadAll           | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated     | Item:ReadAll           | [purchaseUnitName='Corrugated'] |
      | ItemGroupViewer          | ItemGroup:ReadAll      |                                 |
      | MeasuresViewer           | Measures:ReadAll       |                                 |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |                                 |
      | ItemTypeViewer           | ItemType:ReadAll       |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission   | Condition                       |
      | Amr.Khalil  | Item:ReadAll | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:ReadAll | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission   |
      | Afaf | Item:ReadAll |
    And the following ViewAll data for Items exist:
      | ItemCode | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   | ItemType                |
      | 000001   | Hot Laminated Frontlit Fabric roll                      | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               | COMMERCIAL - Commercial |
      | 000002   | Hot Laminated Frontlit Backlit roll                     | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               | COMMERCIAL - Commercial |
      | 000003   | Flex Primer Varnish E33                                 | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               | COMMERCIAL - Commercial |
      | 000004   | Flex cold foil adhesive E01                             | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               | COMMERCIAL - Commercial |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   | COMMERCIAL - Commercial |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   | COMMERCIAL - Commercial |
      | 000007   | PRO-V-ST-1                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               | COMMERCIAL - Commercial |
      | 000008   | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia | COMMERCIAL - Commercial |
      | 000009   | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   | COMMERCIAL - Commercial |
      | 000010   | Machine2                                                | 000003 - White Signmedia Inks | 0003 - Kilogram     | False        | 12345678910111213 | 0004 - Digital                 | COMMERCIAL - Commercial |
      | 000011   | Ink1                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0005 - Textile                 | COMMERCIAL - Commercial |
      | 000012   | Ink2                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              | COMMERCIAL - Commercial |
      | 000013   | Ink3                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              | COMMERCIAL - Commercial |
      | 000014   | Insurance service                                       |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               | SERVICE - Service       |
      | 000015   | Laptop HP Pro-book core i5                              |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               | CONSUMABLE - Consumable |
    And the total number of existing Items are 15

 # EBS-6239
  Scenario: (01) View all Items by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view all Items in Home Screen
    Then the following values will be presented to "Ashraf.Fathi" in Home Screen:
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5                              |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000014   | SERVICE - Service       | Insurance service                                       |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000013   | COMMERCIAL - Commercial | Ink3                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000012   | COMMERCIAL - Commercial | Ink2                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000011   | COMMERCIAL - Commercial | Ink1                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0005 - Textile                 |
      | 000010   | COMMERCIAL - Commercial | Machine2                                                | 000003 - White Signmedia Inks | 0003 - Kilogram     | False        | 12345678910111213 | 0004 - Digital                 |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000005   | COMMERCIAL - Commercial | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000004   | COMMERCIAL - Commercial | Flex cold foil adhesive E01                             | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000003   | COMMERCIAL - Commercial | Flex Primer Varnish E33                                 | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000002   | COMMERCIAL - Commercial | Hot Laminated Frontlit Backlit roll                     | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000001   | COMMERCIAL - Commercial | Hot Laminated Frontlit Fabric roll                      | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
    And the total number of records presented to "Ashraf.Fathi" in Home screen are 15

# EBS-6239
  Scenario: (02) View all Items by an authorized user WITH condition: User has ONE role  (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view all Items in Home Screen
    Then the following values will be presented to "Gehan.Ahmed" in Home Screen:
      | ItemCode | Type                    | Name                                | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5          |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000014   | SERVICE - Service       | Insurance service                   |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                          | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1                          | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000004   | COMMERCIAL - Commercial | Flex cold foil adhesive E01         | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000003   | COMMERCIAL - Commercial | Flex Primer Varnish E33             | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000002   | COMMERCIAL - Commercial | Hot Laminated Frontlit Backlit roll | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000001   | COMMERCIAL - Commercial | Hot Laminated Frontlit Fabric roll  | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
    And the total number of records presented to "Gehan.Ahmed" in Home screen are 8
     
# EBS-6239
  Scenario: (03) View all Items by an authorized user WITH condition: User has TWO role  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view all Items in Home Screen
    Then the following values will be presented to "Amr.Khalil" in Home Screen:
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000013   | COMMERCIAL - Commercial | Ink3                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000012   | COMMERCIAL - Commercial | Ink2                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000005   | COMMERCIAL - Commercial | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
    And the total number of records presented to "Amr.Khalil" in Home screen are 6

  Scenario: (04) View all Items by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view all Items in Home Screen
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #### Filter by ---> Item Code - String - Contains
  # EBS-3511
  Scenario: (05) Filter Items by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on ItemCode which contains "00001" with 3 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                       | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5 |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia  |
      | 000014   | SERVICE - Service       | Insurance service          |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia  |
      | 000013   | COMMERCIAL - Commercial | Ink3                       | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated |
    And the total number of records in search results by "hr1" are 7

  # EBS-3511
  Scenario: (06) Filter Items by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of Items with filter applied on ItemCode which contains "00001" with 3 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit        | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000012   | COMMERCIAL - Commercial | Ink2     | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1     | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0005 - Textile    |
      | 000010   | COMMERCIAL - Commercial | Machine2 | 000003 - White Signmedia Inks | 0003 - Kilogram | False        | 12345678910111213 | 0004 - Digital    |
    And the total number of records in search results by "hr1" are 7

  # EBS-3511
  Scenario: (07) Filter Items by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on ItemCode which contains "2" with 10 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                                | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000012   | COMMERCIAL - Commercial | Ink2                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated |
      | 000002   | COMMERCIAL - Commercial | Hot Laminated Frontlit Backlit roll | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia  |
    And the total number of records in search results by "hr1" are 2

  # EBS-3587
  Scenario: (08) Filter Items by authorized user using "Contains" - User with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Items with filter applied on ItemCode which contains "00001" with 10 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | ItemCode | Type                    | Name                               | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit     |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5         |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia |
      | 000014   | SERVICE - Service       | Insurance service                  |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia |
      | 000001   | COMMERCIAL - Commercial | Hot Laminated Frontlit Fabric roll | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia |
    And the total number of records in search results by "Gehan.Ahmed" are 3

  # EBS-3587
  Scenario: (09) Filter Items by authorized user using "Contains" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Items with filter applied on ItemCode which contains "00001" with 10 records per page
    Then the following values will be presented to "Amr.Khalil":
      | ItemCode | Type                    | Name | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
    And the total number of records in search results by "Amr.Khalil" are 2

  #### Filter by ---> Item Name - Json - Contains
  # EBS-3567
  Scenario: (10) Filter Items by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on Name which contains "nk" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0005 - Textile    |
    And the total number of records in search results by "hr1" are 3

  #### Filter by ---> Item Group - Json - Contains
  # EBS-3567
  Scenario: (11) Filter Items by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on ItemGroup which contains "wHIte" with 13 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000013   | COMMERCIAL - Commercial | Ink3                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000012   | COMMERCIAL - Commercial | Ink2                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000011   | COMMERCIAL - Commercial | Ink1                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0005 - Textile                 |
      | 000010   | COMMERCIAL - Commercial | Machine2                                                | 000003 - White Signmedia Inks | 0003 - Kilogram     | False        | 12345678910111213 | 0004 - Digital                 |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000005   | COMMERCIAL - Commercial | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000004   | COMMERCIAL - Commercial | Flex cold foil adhesive E01                             | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000003   | COMMERCIAL - Commercial | Flex Primer Varnish E33                                 | 000003 - White Signmedia Inks | 0003 - Kilogram     | TRUE         | 12345678910111213 | 0002 - Signmedia               |
      | 000002   | COMMERCIAL - Commercial | Hot Laminated Frontlit Backlit roll                     | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000001   | COMMERCIAL - Commercial | Hot Laminated Frontlit Fabric roll                      | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
    And the total number of records in search results by "hr1" are 13

  #### Filter by ---> Base Unit - Json - Contains - En
  # EBS-3567
  Scenario: (12) Filter Items by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on  BaseUnit which contains "Liter" with 4 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0005 - Textile    |
      | 000009   | COMMERCIAL - Commercial | Machine1 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0001 - Flexo      |
    And the total number of records in search results by "hr1" are 4
    #### Filter by ---> Base Unit - Json - Contains - Ar
    # EBS-3567
  Scenario: (13) Filter Items by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on  BaseUnit which contains "لتر" with 4 records per page while current locale is "ar-EG"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0005 - Textile    |
      | 000009   | COMMERCIAL - Commercial | Machine1 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0001 - Flexo      |
    And the total number of records in search results by "hr1" are 4
  # EBS-3587
  Scenario: (14) Filter Items by authorized user using "Contains" - User with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Items with filter applied on  BaseUnit which contains "Met" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "Gehan.Ahmed":
      | ItemCode | Type                    | Name                                | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5          |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000014   | SERVICE - Service       | Insurance service                   |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia               |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                          | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1                          | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000002   | COMMERCIAL - Commercial | Hot Laminated Frontlit Backlit roll | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000001   | COMMERCIAL - Commercial | Hot Laminated Frontlit Fabric roll  | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
    And the total number of records in search results by "Gehan.Ahmed" are 6

  # EBS-3587
  Scenario: (15) Filter Items by authorized user using "Contains" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Items with filter applied on ItemGroup which contains "White" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Amr.Khalil":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2     | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000009   | COMMERCIAL - Commercial | Machine1 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0001 - Flexo      |
    And the total number of records in search results by "Amr.Khalil" are 6

  #### Filter by ---> Purchase Unit Name

  Scenario: (16) Filter Items by authorized user using "Contains" - Superuser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on PurchaseUnit which contains "Flexo" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000005   | COMMERCIAL - Commercial | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
    And the total number of records in search results by "hr1" are 4


  Scenario: (17) Filter Items by authorized user using "Contains" - Superuser (authorization Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Items with filter applied on PurchaseUnit which contains "Corrugated" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "Amr.Khalil":
      | ItemCode | Type                    | Name | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
    And the total number of records in search results by "Amr.Khalil" are 2

  #### Filter by ---> ItemType
  #EBS-6239
  Scenario: (18) Filter Items by authorized user using "Equals" - SuperUser
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on ItemType which equals "Commercial" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit        | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3     | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2     | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1     | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0005 - Textile    |
      | 000010   | COMMERCIAL - Commercial | Machine2 | 000003 - White Signmedia Inks | 0003 - Kilogram | False        | 12345678910111213 | 0004 - Digital    |
      | 000009   | COMMERCIAL - Commercial | Machine1 | 000003 - White Signmedia Inks | 0014 - Liter    | False        | 12345678910111213 | 0001 - Flexo      |
    And the total number of records in search results by "hr1" are 13

  #EBS-6239
  Scenario: (19) Filter Items by authorized user using "Equals" - SuperUser
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on ItemType which equals "Service" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | ItemCode | Type              | Name              | ItemGroup | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit     |
      | 000014   | SERVICE - Service | Insurance service |           | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia |
    And the total number of records in search results by "hr1" are 1

  #### Filter by ---> BatchManaged - Boolean
  # EBS-3568
  Scenario: (20) Filter Items by authorized user using "equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on BatchManaged which equals "False" with 5 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                       | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000015   | CONSUMABLE - Consumable | Laptop HP Pro-book core i5 |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia  |
      | 000014   | SERVICE - Service       | Insurance service          |                               | 0019 - Square Meter | False        | 12345678910111231 | 0002 - Signmedia  |
      | 000013   | COMMERCIAL - Commercial | Ink3                       | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2                       | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1                       | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0005 - Textile    |
    And the total number of records in search results by "hr1" are 13

  # EBS-3568
  Scenario: (21) Filter Items by authorized user using "equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of Items with filter applied on BatchManaged which equals "False" with 5 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000010   | COMMERCIAL - Commercial | Machine2                                                | 000003 - White Signmedia Inks | 0003 - Kilogram     | False        | 12345678910111213 | 0004 - Digital                 |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
    And the total number of records in search results by "hr1" are 13

  # EBS-3568
  Scenario: (22) Filter Items by authorized user using "equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with filter applied on BatchManaged which equals "True" with 3 records per page
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name                        | ItemGroup                     | BaseUnit        | BatchManaged | OldItemReference  | BusinessUnit     |
      | 000004   | COMMERCIAL - Commercial | Flex cold foil adhesive E01 | 000003 - White Signmedia Inks | 0003 - Kilogram | TRUE         | 12345678910111213 | 0002 - Signmedia |
      | 000003   | COMMERCIAL - Commercial | Flex Primer Varnish E33     | 000003 - White Signmedia Inks | 0003 - Kilogram | TRUE         | 12345678910111213 | 0002 - Signmedia |
    And the total number of records in search results by "hr1" are 2

  # EBS-3587
  Scenario: (23) Filter Items by authorized user using "equals" - User with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Items with filter applied on BatchManaged which equals "True" with 3 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | ItemCode | Type                    | Name                        | ItemGroup                     | BaseUnit        | BatchManaged | OldItemReference  | BusinessUnit     |
      | 000004   | COMMERCIAL - Commercial | Flex cold foil adhesive E01 | 000003 - White Signmedia Inks | 0003 - Kilogram | TRUE         | 12345678910111213 | 0002 - Signmedia |
      | 000003   | COMMERCIAL - Commercial | Flex Primer Varnish E33     | 000003 - White Signmedia Inks | 0003 - Kilogram | TRUE         | 12345678910111213 | 0002 - Signmedia |
    And the total number of records in search results by "Gehan.Ahmed" are 2

  # EBS-3587
  Scenario: (24) Filter Items by authorized user using "equals" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Items with filter applied on BatchManaged which equals "False" with 5 records per page
    Then the following values will be presented to "Amr.Khalil":
      | ItemCode | Type                    | Name                                                    | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000013   | COMMERCIAL - Commercial | Ink3                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000012   | COMMERCIAL - Commercial | Ink2                                                    | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0006 - Corrugated              |
      | 000009   | COMMERCIAL - Commercial | Machine1                                                | 000003 - White Signmedia Inks | 0014 - Liter        | False        | 12345678910111213 | 0001 - Flexo                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2                                              | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000006   | COMMERCIAL - Commercial | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo                   |
    And the total number of records in search results by "Amr.Khalil" are 6

  #### Item - Filter - Multiple

  Scenario: (25) Filter Items by authorized user using - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with 3 records per page and the following filters applied on Item while current locale is "en-US"
      | FieldName     | Operation | Value    |
      | userCode      | contains  | 00001    |
      | itemGroupName | contains  | White    |
      | itemName      | contains  | Machine  |
      | baseUnit      | contains  | Kilogram |
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name     | ItemGroup                     | BaseUnit        | BatchManaged | OldItemReference  | BusinessUnit   |
      | 000010   | COMMERCIAL - Commercial | Machine2 | 000003 - White Signmedia Inks | 0003 - Kilogram | False        | 12345678910111213 | 0004 - Digital |
    And the total number of records in search results by "hr1" are 1


  Scenario: (26) Filter Items by authorized user using - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Items with 3 records per page and the following filters applied on Item while current locale is "en-US"
      | FieldName     | Operation | Value |
      | userCode      | contains  | 00001 |
      | itemGroupName | contains  | White |
      | itemName      | contains  | Ink   |
    Then the following values will be presented to "hr1":
      | ItemCode | Type                    | Name | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000011   | COMMERCIAL - Commercial | Ink1 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0005 - Textile    |
    And the total number of records in search results by "hr1" are 3

  # EBS-3587
  Scenario: (27) Filter Items by authorized user using "Contains" - User with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Items with 3 records per page and the following filters applied on Item while current locale is "en-US"
      | FieldName     | Operation | Value |
      | userCode      | contains  | 00000 |
      | itemGroupName | contains  | White |
      | itemName      | contains  | PRO   |
    Then the following values will be presented to "Gehan.Ahmed":
      | ItemCode | Type                    | Name       | ItemGroup                     | BaseUnit            | BatchManaged | OldItemReference  | BusinessUnit                   |
      | 000008   | COMMERCIAL - Commercial | PRO-V-ST-2 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0001 - Flexo, 0002 - Signmedia |
      | 000007   | COMMERCIAL - Commercial | PRO-V-ST-1 | 000003 - White Signmedia Inks | 0019 - Square Meter | False        | 12345678910111213 | 0002 - Signmedia               |
    And the total number of records in search results by "Gehan.Ahmed" are 2

  # EBS-3587
  Scenario: (28) Filter Items by authorized user using "Contains" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Items with 3 records per page and the following filters applied on Item while current locale is "en-US"
      | FieldName     | Operation | Value |
      | userCode      | contains  | 00001 |
      | itemGroupName | contains  | White |
      | itemName      | contains  | Ink   |
    Then the following values will be presented to "Amr.Khalil":
      | ItemCode | Type                    | Name | ItemGroup                     | BaseUnit     | BatchManaged | OldItemReference  | BusinessUnit      |
      | 000013   | COMMERCIAL - Commercial | Ink3 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
      | 000012   | COMMERCIAL - Commercial | Ink2 | 000003 - White Signmedia Inks | 0014 - Liter | False        | 12345678910111213 | 0006 - Corrugated |
    And the total number of records in search results by "Amr.Khalil" are 2