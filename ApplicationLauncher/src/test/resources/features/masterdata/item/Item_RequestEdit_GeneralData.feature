Feature: Request to edit and cancel GeneralData section in Item

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Ahmed.Hamdi | Accountant_Flexo                 |
      | hr1         | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia      |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo          |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated     |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Signmedia  | ProductManagerViewer     |
      | PurchasingResponsible_Flexo      | ProductManagerViewer     |
      | PurchasingResponsible_Corrugated | ProductManagerViewer     |
      | Accountant_Flexo                 | ItemViewer_Flexo         |
      | SuperUser                        | SuperUserSubRole         |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition                       |
      | ItemOwner_Signmedia      | Item:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo          | Item:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated     | Item:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |                                 |
      | ProductManagerViewer     | ProductManager:ReadAll |                                 |
      | SuperUserSubRole         | *:*                    |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission             | Condition                       |
      | Amr.Khalil  | Item:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User        | Permission             |
      | Ahmed.Hamdi | Item:UpdateGeneralData |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000002 | Hot Laminated Frontlit Backlit roll                     | 0002           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000012 | Ink2                                                    | 0006           |
      | 000053 | Hot Laminated Frontlit Fabric roll 2                    | 0002           |
      | 000054 | Hot Laminated Frontlit Fabric roll 3                    | 0002           |
    And edit session is "30" minutes

    ### Request to edit GeneralData section ###

  Scenario Outline:(01) Request to edit General data section by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit GeneralData section of Item with code "<ItemCode>"
    Then GeneralData section of Item with code "<ItemCode>" becomes in edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads    |
      | ReadPurchasingUnit |
      | ReadProductManager |
    Examples:
      | User        | ItemCode |
      | Gehan.Ahmed | 000002   |
      | Amr.Khalil  | 000006   |
      | Amr.Khalil  | 000012   |

  Scenario:(02) Request to edit General data section that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the Item with code "000002" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of Item with code "000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario:(03) Request to edit General data section of deleted Item (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario:(04) Request to edit General data section with unauthorized user (Abuse Case \ Client Bypassing)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit GeneralData section of Item with code "000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario:(05) Request to edit General data section with unauthorized user with condition (Abuse Case \ Client Bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit GeneralData section of Item with code "000006"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    ### Request to Cancel saving GeneralData section ###

  Scenario Outline:(06) Request to Cancel saving General data section by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of Item with code "<ItemCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving GeneralData section of Item with code "<ItemCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on GeneralData section of Item with code "<ItemCode>" is released
    Examples:
      | User        | ItemCode |
      | Gehan.Ahmed | 000002   |
      | Amr.Khalil  | 000006   |
      | Amr.Khalil  | 000012   |

  Scenario:(07) Request to Cancel saving General data section after lock session is expire
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving GeneralData section of Item with code "000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (08) Request to Cancel saving General data section after lock session is expire and Item doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of Item with code "000054" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Item with code "000054" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving GeneralData section of Item with code "000054" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario:(09) Request to Cancel saving General data section with unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" cancels saving GeneralData section of Item with code "000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario:(10) Request to Cancel saving General data section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" cancels saving GeneralData section of Item with code "000006"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
