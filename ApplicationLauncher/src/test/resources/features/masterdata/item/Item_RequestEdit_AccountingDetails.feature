Feature: Request to edit and cancel AccountingDetails section

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Gehan.Ahmed.NoAccounts | PurchasingResponsible_Signmedia_CannotViewAccountsRole |
      | hr1                    | SuperUser                                              |
      | Afaf                   | FrontDesk                                              |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole                      |
      | PurchasingResponsible_Signmedia                        | ItemOwner_Signmedia          |
      | PurchasingResponsible_Signmedia                        | ChartOfAccountsViewerLimited |
      | PurchasingResponsible_Signmedia_CannotViewAccountsRole | ItemOwner_Signmedia          |
      | SuperUser                                              | SuperUserSubRole             |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                   | Condition                      |
      | ItemOwner_Signmedia          | Item:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewerLimited | GLAccount:ReadAllLimited     |                                |
      | SuperUserSubRole             | *:*                          |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                   | Condition                  |
      | Gehan.Ahmed | Item:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                   | Permission                   |
      | Afaf                   | Item:UpdateAccountingDetails |
      | Gehan.Ahmed.NoAccounts | GLAccount:ReadAllLimited     |
    And the following AccountingDetails for Items exist:
      | ItemCode | AccountCode | AccountName | BusinessUnit |
      | 000053   |             |             |              |
      | 000005   | 10107       | Lands 2     | Flexo        |
      | 000002   | 10207       | PO          | Signmedia    |
      | 000014   |             |             | Signmedia    |
    And the following Accounts exist:
      | AccountCode | AccountName     | Level | State  |
      | 10207       | PO              | 3     | Active |
      | 10201       | Service Vendors | 3     | Active |
      | 10107       | Lands 2         | 3     | Active |
    And edit session is "30" minutes

  Scenario:(01) Request to edit Accounting Details section for (Commercial/Consumable) by an authorized user with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Item with code "000002"
    Then AccountingDetails section of Item with code "000002" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadAccounts    |
    And the following mandatory fields are returned to "Gehan.Ahmed":
      | MandatoriesFields |
      | accountCode       |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | accountCode    |

  Scenario:(02) Request to edit Accounting Details section for Service Item by an authorized user with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Item with code "000014"
    Then AccountingDetails section of Item with code "000014" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadAccounts    |
    And the following mandatory fields are returned to "Gehan.Ahmed":
      | MandatoriesFields |
      | accountCode       |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | accountCode    |
      | costFactor     |


  Scenario:(03) Request to edit Accounting Details section (No authorized reads)
    Given user is logged in as "Gehan.Ahmed.NoAccounts"
    When "Gehan.Ahmed.NoAccounts" requests to edit AccountingDetails section of Item with code "000002"
    Then AccountingDetails section of Item with code "000002" becomes in edit mode and locked by "Gehan.Ahmed.NoAccounts"
    And there are no authorized reads returned to "Gehan.Ahmed.NoAccounts"


  Scenario:(04) Request to edit Accounting Details section that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the AccountingDetails section Item with code "000002" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Item with code "000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario:(05) Request to edit Accounting Details section of deleted Item (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(06) Request to edit Accounting Details section with unauthorized user with/without condition(Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit AccountingDetails section of Item with code "000005"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |


  Scenario:(06) Request to Cancel saving Accounting Details section within the edit session by an authorized user(Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Item with code "000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released


  Scenario:(07) Request to Cancel saving Accounting Details section after edit session is expire
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Item with code "000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"


  Scenario: (08) Request to Cancel saving Accounting Details section after edit session expire and section got locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And AccountingDetails section of Item with code "000002" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Item with code "000002" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"


  Scenario: (09) Request to Cancel saving Accounting Details section after edit session is expire and Item doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Item with code "000002" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Item with code "000002" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(10) Request to Cancel saving Accounting Details section with unauthorized user with/without condition(Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving AccountingDetails section of Item with code "000005"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |



