# Author: Hossam bayomy , Hend Ahmed 12-Dec-2018 10:00 AM
# updated by: Fatma Al Zahraa (EBS - 8121)
# Author Quality: Shirin Mahmoud


Feature: Read Allowed Actions for Item

  Background:
    Given the following users and roles exist:
      | Name               | Role                                                    |
            # One Role --> One Subrole without condition
      | ItemOwnerUser      | PurchasingResponsible                                   |
            # One Role --> One Subrole with condition
      | Gehan.Ahmed        | PurchasingResponsible_Signmedia                         |
            # Two Roles --> Each with one Subrole with condition - similar subroles
      | Amr.Khalil         | PurchasingResponsible_Flexo                             |
      | Amr.Khalil         | PurchasingResponsible_Corrugated                        |
            # Two Roles --> Each with one subrole, one with condition and the other without -similar subroles
      | TestUser1          | PurchasingResponsible_Signmedia                         |
      | TestUser1          | PurchasingResponsible                                   |
            # Two Roles --> Each with one subrole with condition - differet subroles
      | TestUser2          | PurchasingResponsible_Signmedia                         |
      | TestUser2          | LogisticsResponsible_Signmedia                          |
            # Two Roles --> Each with one subrole one with condition and the other without - differet subroles
      | TestUser3          | PurchasingResponsible_Signmedia                         |
      | TestUser3          | LogisticsResponsible                                    |
            # One Role --> Two Subroles with condition - Similar Subroles
      | TestUser4          | PurchasingResponsible_Signmedia_Flexo                   |
            # One Role --> Two Subroles one with condition and the other without - Similar Subroles
      | TestUser5          | PurchasingResponsible_ALL_Flexo                         |
            # One Role --> Two Subroles - Different Subroles with no conditions
      | TestUser6          | PurchasingResponsible_And_LogisticResponsible           |
            # One Role --> Two Subroles - Different Subroles with consitions
      | TestUser7          | PurchasingResponsible_Signmedia_And_LogisticResponsible |
            # One Role --> viewer
      | TestUser8          | LogisticsResponsible                                    |
            # User Authorized ro create only
      | CreateOnlyUser     | CreateOnlyRole                                          |
            # Not authorized
      | Afaf               | FrontDesk                                               |
            #SuperUser
      | hr1                | SuperUser                                               |
            #User who is authorized to read only some sections
      | CannotReadSections | CannotReadSectionsRole                                  |
    And the following roles and sub-roles exist:
      | Role                                                    | Subrole                   |
      | CreateOnlyRole                                          | CreateOnlySubRole         |
      | CreateOnlyRole                                          | MeasuresOwner             |
      | PurchasingResponsible                                   | ItemOwner                 |
      | PurchasingResponsible                                   | MeasuresOwner             |
      | PurchasingResponsible                                   | ItemTypeViewer            |
      | PurchasingResponsible_Signmedia                         | ItemOwner_Signmedia       |
      | PurchasingResponsible_Signmedia                         | MeasuresOwner             |
      | PurchasingResponsible_Signmedia                         | ItemTypeViewer            |
      | PurchasingResponsible_Flexo                             | ItemOwner_Flexo           |
      | PurchasingResponsible_Flexo                             | MeasuresOwner             |
      | PurchasingResponsible_Flexo                             | ItemTypeViewer            |
      | PurchasingResponsible_Corrugated                        | ItemOwner_Corrugated      |
      | PurchasingResponsible_Corrugated                        | MeasuresOwner             |
      | PurchasingResponsible_Corrugated                        | ItemTypeViewer            |
      | LogisticsResponsible                                    | ItemViewer                |
      | LogisticsResponsible                                    | ItemTypeViewer            |
      | LogisticsResponsible_Signmedia                          | ItemViewer_Signmedia      |
      | PurchasingResponsible_Signmedia_Flexo                   | ItemOwner_Signmedia       |
      | PurchasingResponsible_Signmedia_Flexo                   | MeasuresOwner             |
      | PurchasingResponsible_Signmedia_Flexo                   | ItemOwner_Flexo           |
      | PurchasingResponsible_Signmedia_Flexo                   | ItemTypeViewer            |
      | PurchasingResponsible_ALL_Flexo                         | ItemOwner                 |
      | PurchasingResponsible_ALL_Flexo                         | MeasuresOwner             |
      | PurchasingResponsible_ALL_Flexo                         | ItemOwner_Flexo           |
      | PurchasingResponsible_ALL_Flexo                         | ItemTypeViewer            |
      | PurchasingResponsible_And_LogisticResponsible           | ItemOwner                 |
      | PurchasingResponsible_And_LogisticResponsible           | MeasuresOwner             |
      | PurchasingResponsible_And_LogisticResponsible           | ItemViewer                |
      | PurchasingResponsible_And_LogisticResponsible           | ItemTypeViewer            |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | ItemOwner_Signmedia       |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | MeasuresOwner             |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | ItemTypeViewer            |
      | SuperUser                                               | SuperUserSubRole          |
      | CannotReadSectionsRole                                  | CannotReadSectionsSubRole |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                 | Condition                       |
      | CreateOnlySubRole         | Item:Create                |                                 |
      | ItemOwner                 | Item:ReadAll               |                                 |
      | ItemOwner                 | Item:ReadGeneralData       |                                 |
      | ItemOwner                 | Item:ReadUoMData           |                                 |
      | ItemOwner                 | Item:UpdateGeneralData     |                                 |
      | ItemOwner                 | Item:UpdateUoMData         |                                 |
      | ItemOwner                 | Item:Delete                |                                 |
      | ItemOwner                 | Item:Create                |                                 |
      | ItemOwner                 | Item:ReadAccountingDetails |                                 |
      | ItemOwner_Signmedia       | Item:ReadAll               | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:ReadUoMData           | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:UpdateGeneralData     | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:UpdateUoMData         | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:Delete                | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Signmedia       | Item:Create                |                                 |
      | ItemOwner_Flexo           | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:Delete                | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | ItemOwner_Flexo           | Item:Create                |                                 |
      | ItemOwner_Corrugated      | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Corrugated      | Item:Create                |                                 |
      | ItemViewer                | Item:ReadAll               |                                 |
      | ItemViewer                | Item:ReadGeneralData       |                                 |
      | ItemViewer                | Item:ReadUoMData           |                                 |
      | ItemViewer                | Item:ReadAccountingDetails |                                 |
      | ItemViewer_Signmedia      | Item:ReadAll               | [purchaseUnitName='Signmedia']  |
      | ItemViewer_Signmedia      | Item:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | ItemViewer_Signmedia      | Item:ReadUoMData           | [purchaseUnitName='Signmedia']  |
      | ItemViewer_Signmedia      | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | SuperUserSubRole          | *:*                        |                                 |
      | CannotReadSectionsSubRole | Item:Delete                |                                 |
      | MeasuresOwner             | Measures:Create            |                                 |
      | ItemTypeViewer            | ItemType:ReadAll           |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                 | Condition                       |
      | Gehan.Ahmed | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:Delete                | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Item:ReadAll               | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:ReadUoMData           | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:UpdateGeneralData     | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:UpdateUoMData         | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:Delete                | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | TestUser1   | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:Delete                | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | TestUser1   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser1   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:Delete                | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | TestUser2   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser2   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:Delete                | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | TestUser3   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser3   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser4   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser5   | Item:ReadAll               | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:ReadUoMData           | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:UpdateGeneralData     | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:UpdateUoMData         | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:Delete                | [purchaseUnitName='Signmedia']  |
      | TestUser5   | Item:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | TestUser7   | Item:ReadAll               | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:ReadUoMData           | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:UpdateUoMData         | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:Delete                | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | TestUser7   | Item:ReadAll               | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:ReadUoMData           | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:UpdateUoMData         | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:Delete                | [purchaseUnitName='Flexo']      |
      | TestUser7   | Item:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
    And the following users doesn't have the following permissions:
      | User           | Permission                 |
      | TestUser8      | Item:UpdateGeneralData     |
      | TestUser8      | Measures:Create            |
      | TestUser8      | Item:UpdateUoMData         |
      | TestUser8      | Item:Delete                |
      | TestUser8      | Item:Create                |
      | CreateOnlyUser | Item:ReadAll               |
      | CreateOnlyUser | Item:ReadGeneralData       |
      | CreateOnlyUser | Item:ReadUoMData           |
      | CreateOnlyUser | Item:UpdateGeneralData     |
      | CreateOnlyUser | Item:UpdateUoMData         |
      | CreateOnlyUser | Item:Delete                |
      | CreateOnlyUser | Item:ReadAccountingDetails |
      | Afaf           | Item:ReadAll               |
      | Afaf           | Item:ReadGeneralData       |
      | Afaf           | Item:ReadUoMData           |
      | Afaf           | Item:UpdateGeneralData     |
      | Afaf           | Item:UpdateUoMData         |
      | Afaf           | Item:Delete                |
      | Afaf           | Item:Create                |
      | Afaf           | Item:ReadAccountingDetails |
      | Afaf           | Measures:Create            |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000007 | PRO-V-ST-1                                              | 0002           |
      | 000009 | Machine1                                                | 0001           |
      | 000012 | Ink2                                                    | 0006           |

  #EBS-8121
  # One Role --> One Subrole without condition (Scenario 01)
  Scenario Outline: (01) Read Allowed Actions for Item One Role (PurchaseResponsible) --> One Subrole with no condition (ItemOwner)
    Given user is logged in as "ItemOwnerUser"
    When "ItemOwnerUser" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "ItemOwnerUser":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000006 | 0001           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # One Role --> One Subrole with condition (Scenario 02)
  Scenario Outline: (02) Read Allowed Actions for Item One Role (PurchaseResponsible_Signmedia) --> One Subrole with condition (ItemOwner_Signmedia)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "Gehan.Ahmed":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |

    #EBS-8121
  # Two Roles --> Each with one Subrole with condition - similar subroles (Scenario 03)
  Scenario Outline: (03) Read Allowed Actions for Item Two Roles (PurchaseResponsible_Signmedia \ ItemOwner_Signmedia) and (PurchaseResponsible_Flexo \ ItemOwner_Flexo)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "Amr.Khalil":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000006 | 0001           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # Two Roles --> Each with one subrole, one with condition and the other without -similar subroles (Scenario 04)
  Scenario Outline: (04) Read Allowed Actions for Item ItemOwner with two roles one with condition and another without condition
    Given user is logged in as "TestUser1"
    When "TestUser1" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser1":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000006 | 0001           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # Two Roles --> Each with one subrole with condition - differet subroles (Scenario 05)
  Scenario Outline: (05) Read Allowed Actions for Item ItemOwner with condition and ItemViewer with condition
    Given user is logged in as "TestUser2"
    When "TestUser2" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser2":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |

    #EBS-8121
  # Two Roles --> Each with one subrole one with condition and the other without - differet subroles (Scenario 06, 07)
  Scenario Outline: (06) Read Allowed Actions for Item ItemOwner with condition and ItemViewer without condition
    Given user is logged in as "TestUser3"
    When "TestUser3" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser3":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |

  Scenario Outline: (07) Read Allowed Actions for Item ItemOwner with condition and ItemViewer without condition
    Given user is logged in as "TestUser3"
    When "TestUser3" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser3":
      | AllowedActions        |
      | ReadGeneralData       |
      | ReadUoMData           |
      | ReadAll               |
      | ReadAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000006 | 0001           |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # One Role --> Two Subroles with condition - Similar Subroles (Scenario 08)
  Scenario Outline: (08) Read Allowed Actions for Item ItemOwner with condition and ItemViewer without condition
    Given user is logged in as "TestUser4"
    When "TestUser4" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser4":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000006 | 0001           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |

    #EBS-8121
  # One Role --> Two Subroles one with condition and the other without - Similar Subroles (Scenario 09)
  Scenario Outline: (09) Read Allowed Actions for Item ItemOwner with condition and ItemViewer without condition
    Given user is logged in as "TestUser5"
    When "TestUser5" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser5":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000006 | 0001           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # One Role --> Two Subroles - Different Subroles with no conditions (Scenario 10)
  Scenario Outline: (10) Read Allowed Actions for Item ItemOwner with condition and ItemViewer without condition
    Given user is logged in as "TestUser6"
    When "TestUser6" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser6":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000006 | 0001           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |
      | 000009 | 0001           |
      | 000012 | 0006           |

    #EBS-8121
  # One Role --> Two Subroles - Different Subroles with conditions (Scenario 11)
  Scenario Outline: (11) Read Allowed Actions for Item One Role --> Two Subroles - Different Subroles with conditions - Part 1
    Given user is logged in as "TestUser7"
    When "TestUser7" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser7":
      | AllowedActions          |
      | ReadGeneralData         |
      | ReadUoMData             |
      | UpdateGeneralData       |
      | UpdateUoMData           |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
      | CreateMeasure           |

    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0002           |
      | 000007 | 0002           |
      | 000008 | 0001, 0002     |

  Scenario Outline: (12) Read Allowed Actions for Item One Role --> Two Subroles - Different Subroles with conditions - Part 2
    Given user is logged in as "TestUser7"
    When "TestUser7" requests to read actions of Item with code "<Code>"
    Then the following actions are displayed to "TestUser7":
      | AllowedActions        |
      | ReadGeneralData       |
      | ReadUoMData           |
      | ReadAll               |
      | ReadAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000006 | 0001           |
      | 000009 | 0001           |
      | 000012 | 0006           |

  Scenario: (13) Read Allowed Actions for Item unauthorized user requests to read actions of item
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read actions of Item with code "000012"
    Then an authorization error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-29"

  Scenario: (14) Read Allowed Actions for Item user requests to read actions of item that not exist
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to read actions of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (15) Read Allowed Actions for Item User who is authorized to read only some sections
    Given user is logged in as "CannotReadSections"
    When "CannotReadSections" requests to read actions of Item with code "000001"
    Then the following actions are displayed to "CannotReadSections":
      | Allowed actions |
      | Delete          |

  Scenario Outline: (16) Read Allowed Actions for Item home screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Item home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User           | AllowedActions |
            # One Role --> One Subrole
      | ItemOwnerUser  | ReadAll,Create |
            # Two Roles --> similar subroles
      | Amr.Khalil     | ReadAll,Create |
            # Two Roles --> differet subroles
      | TestUser2      | ReadAll,Create |
            # One Role --> One Subrole- viewer
      | TestUser8      | ReadAll        |
      | hr1            | ReadAll,Create |
            # user authorized to create only
      | CreateOnlyUser | Create         |

  Scenario: (17) Read Allowed Actions for Item unauthorized user requests to read actions of item
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Item home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
