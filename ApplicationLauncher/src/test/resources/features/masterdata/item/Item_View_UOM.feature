#Author : Marina  (26/11/2018)
# Analysis Reviewer: Hend Ahmed, 04-Dec-2018 9:27 AM

Feature: view UnitOfMeasures section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Afaf        | FrontDesk                        |
      | hr1         | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition                       |
      | ItemOwner_Signmedia  | Item:ReadUoMData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:ReadUoMData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:ReadUoMData | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole     | *:*              |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission       | Condition                       |
      | Amr.Khalil  | Item:ReadUoMData | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:ReadUoMData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadUoMData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | Item:ReadUoMData |
    And the following sub-roles are admin sub-roles:
      | AdminSubRoles        |
      | ItemOwner_Signmedia  |
      | ItemOwner_Flexo      |
      | ItemOwner_Corrugated |
      | SuperUserSubRole     |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000007 | PRO-V-ST-1                                              | 0002           |
      | 000012 | Ink2                                                    | 0006           |
      | 000053 | Hot Laminated Frontlit Fabric roll 2                    | 0002           |
      | 000058 | Flex Primer Varnish E22                                 | 0002           |
    And the following Measures exist:
      | Code | Type     | Symbol          | Name            |
      | 0029 | Business | Roll 2.20x50    | Roll 2.20x50    |
      | 0032 | Business | Roll 3.20x50    | Roll 3.20x50    |
      | 0035 | Business | Roll 1.27x50    | Roll 1.27x50    |
      | 0042 | Business | Bottle-2Liter   | Bottle-2Liter   |
      | 0043 | Business | Bottle-2.5Liter | Bottle-2.5Liter |
    And the following UnitOfMeasures section exist in Item with code "000007":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0029          | 110              | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000001":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | 0029            | 110              | 12345678910111225 |
      | 0032            | 160              | 12345678910111226 |
      | 0033            | 1                | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000006":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | 0035            | 63.5             | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000012":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | 0042            | 2                | 12345678910111238 |
      | 0043            | 2.5              | 12345678910111239 |


  # EBS-1527
  Scenario: (01) View UnitOfMeasures section empty by authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view UnitOfMeasures section of Item with code "000058"
    Then an empty UnitOfMeasures section of Item with code "000058" is displayed to "Gehan.Ahmed"

  # EBS-1527
  Scenario: (02) View UnitOfMeasures section by authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view UnitOfMeasures section of Item with code "000001"
    Then the following values of UnitOfMeasures section of Item with code "000001" are displayed to "Gehan.Ahmed":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | Roll 2.70x50    | 1                | 12345678910111231 |
      | Roll 3.20x50    | 160              | 12345678910111226 |
      | Roll 2.20x50    | 110              | 12345678910111225 |

  # EBS-1527
  Scenario: (03) View UnitOfMeasures section by authorized user who has two roles - AmrKhalil+Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view UnitOfMeasures section of Item with code "000006"
    Then the following values of UnitOfMeasures section of Item with code "000006" are displayed to "Amr.Khalil":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | Kilogram        | 63.5             | 12345678910111231 |
      | Roll 1.27x50    | 63.5             | 12345678910111231 |

  # EBS-1527
  Scenario: (04) View UnitOfMeasures section by authorized user who has two roles - AmrKhalil+Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view UnitOfMeasures section of Item with code "000012"
    Then the following values of UnitOfMeasures section of Item with code "000012" are displayed to "Amr.Khalil":
      | AlternativeUnit | ConversionFactor | OldItemNumber     |
      | Bottle-2.5Liter | 2.5              | 12345678910111239 |
      | Bottle-2Liter   | 2                | 12345678910111238 |

  # EBS-1527
  Scenario: (05) View UnitOfMeasures section where Item doesn't exist- Note: Item has no references so that it can be deleted (Exception Case)

    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to view UnitOfMeasures section of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-1527
  Scenario: (06) View UnitOfMeasures section by an unauthorized user (Authorization error)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view UnitOfMeasures section of Item with code "000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-1527
  Scenario: (07) View UnitOfMeasures section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view UnitOfMeasures section of Item with code "000001"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page