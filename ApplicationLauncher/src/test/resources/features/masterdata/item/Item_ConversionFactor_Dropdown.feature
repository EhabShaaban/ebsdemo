#Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Ahmed

Feature: View Item's Conversion Factor & BaseUnit

  Background:

    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole              |
      | SalesSpecialist_Signmedia | ItemViewer_Signmedia |
      | SalesSpecialist_Signmedia | MeasuresViewer       |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition                      |
      | ItemViewer_Signmedia | Item:ReadAll     | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia | Item:ReadUoMData | [purchaseUnitName='Signmedia'] |
      | MeasuresViewer       | Measures:ReadAll |                                |

    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | Item:ReadAll     |
      | Afaf | Item:ReadUoMData |
      | Afaf | Measures:ReadAll |

    And the following Items exist with the below details:
      | Item          | Type       | UOM       | PurchaseUnit                   |
      | 000051 - Ink5 | COMMERCIAL | 0019 - M2 | 0001 - Flexo, 0002 - Signmedia |

    And the following UnitOfMeasures section exist in Item with code "000051 - Ink5":
      | AlternativeUnit     | ConversionFactor | OldItemNumber     |
      | 0029 - Roll 2.20x50 | 110.0            | 12345678910111231 |

   #EBS-6770
  Scenario: (01) Read Item's BaseUnit & ConversionFactor - dropdown by authorized user with condition (With Item AlternativeUOM) (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read BaseUnit and ConversionFactor for Item "000051 - Ink5" with AlternativeUOM "0029 - Roll 2.20x50"
    Then the following BaseUnit and ConversionFactor values will be presented to "Ahmed.Al-Ashry":
      | BaseUnit  | ConversionFactor |
      | 0019 - M2 | 110.0            |
    And total number of Item BaseUnit and ConversionFactor records returned to "Ahmed.Al-Ashry" is "1"

   #EBS-6770
  Scenario: (02) Read Item's BaseUnit & ConversionFactor - dropdown by authorized user with condition (With Item BaseUnit) (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read BaseUnit and ConversionFactor for Item "000051 - Ink5" with BaseUnit "0019 - M2"
    Then the following BaseUnit and ConversionFactor values will be presented to "Ahmed.Al-Ashry":
      | BaseUnit  | ConversionFactor |
      | 0019 - M2 | 1.0              |
    And total number of Item BaseUnit and ConversionFactor records returned to "Ahmed.Al-Ashry" is "1"

   #EBS-6770
  Scenario Outline: (03) Read Item's BaseUnit & ConversionFactor - dropdown by authorized user with malicious/non-existing input (Validation)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read BaseUnit and ConversionFactor for Item "<Item>" with AlternativeUOM "<UOM>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | Item          | UOM                 |
      | 000051 - Ink5 | potato              |
      | potato        | 0029 - Roll 2.20x50 |

   #EBS-6770
  Scenario: (04) Read Item's BaseUnit & ConversionFactor - dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read BaseUnit and ConversionFactor for Item "000051 - Ink5" with AlternativeUOM "0019 - Roll 2.20x50"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page