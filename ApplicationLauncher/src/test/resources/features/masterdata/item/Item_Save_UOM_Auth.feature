#Author: Shrouk Alaa on Date: 13-Jan-2019, 11:26 AM
#Reviewer: Hend Ahmed on Date: 14/Jan-2019, 11:56 AM
Feature: Save_Authorization UoM section in Item

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Amr.Khalil             | PurchasingResponsible_Flexo                            |
      | Amr.Khalil             | PurchasingResponsible_Corrugated                       |
      | Gehan.Ahmed.NoMeasures | PurchasingResponsible_Signmedia_CannotReadMeasuresRole |
      | Shady.Abdelatif        | Accountant_Signmedia                                   |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole              |
      | PurchasingResponsible_Flexo                            | ItemOwner_Flexo      |
      | PurchasingResponsible_Flexo                            | MeasuresViewer       |
      | PurchasingResponsible_Corrugated                       | ItemOwner_Corrugated |
      | PurchasingResponsible_Corrugated                       | MeasuresViewer       |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole | ItemOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission         | Condition                       |
      | ItemOwner_Signmedia  | Item:UpdateUoMData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:UpdateUoMData | [purchaseUnitName='Flexo']      |
      | MeasuresViewer       | Measures:ReadAll   |                                 |
      | ItemOwner_Corrugated | Item:UpdateUoMData | [purchaseUnitName='Corrugated'] |
      | MeasuresViewer       | Measures:ReadAll   |                                 |
    And the following users have the following permissions without the following conditions:
      | User       | Permission         | Condition                      |
      | Amr.Khalil | Item:UpdateUoMData | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                   | Permission         |
      | Shady.Abdelatif        | Item:UpdateUoMData |
      | Gehan.Ahmed.NoMeasures | Measures:ReadAll   |
    And the following GeneralData for Items exist:
      | ItemCode | ItemName                           | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | PurchasingUnit | CreationDate         | MarketName                         | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll | 000003    | 0019     | FALSE        | 12345678910111213 | 0002           | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0001 - Mohamed Nabil |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |

    # Save section by a user who is not authorized to save
  Scenario: (01) Save UnitOfMeasures section by an unauthorized users (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves UoM section of Item with Code "000001" with the following values:
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | Roll 2.20x50  | 110              | 12345678910111225 |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    # Save section by a user who is not authorized to save due to condition
  Scenario: (02) Save UnitOfMeasures section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves UoM section of Item with Code "000001" with the following values:
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | Roll 2.20x50  | 110              | 12345678910111225 |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

    ######## Save section using values which the user is not authorized to read
  Scenario: (03) Save UnitOfMeasures by section User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoMeasures"
    And UnitOfMeasures section of Item with code "000001" is locked by "Gehan.Ahmed.NoMeasures" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed.NoMeasures" saves UoM section of Item with Code "000001" with the following values:
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | Roll 2.20x50  | 110              | 12345678910111225 |
    Then "Gehan.Ahmed.NoMeasures" is logged out
    And "Gehan.Ahmed.NoMeasures" is forwarded to the error page