Feature: View Item's Alternative UOM

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole              |
      | SalesSpecialist_Signmedia | ItemViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition                      |
      | ItemViewer_Signmedia | Item:ReadUoMData | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | Item:ReadUoMData |
    And the following Items exist with the below details:
      | Item                                        | Type       | UOM       | PurchaseUnit     |
      | 000001 - Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000001 - Hot Laminated Frontlit Fabric roll":
      | UOM                 |
      | 0019 - M2           |
      | 0029 - Roll 2.20x50 |
      | 0032 - Roll 3.20x50 |
      | 0033 - Roll 2.70x50 |

  Scenario: (01) Read Item's UOMs dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read UOMs for Item "000001 - Hot Laminated Frontlit Fabric roll"
    Then the following UOM values will be presented to "Ahmed.Al-Ashry":
      | UOM                 |
      | 0019 - M2           |
      | 0033 - Roll 2.70x50 |
      | 0032 - Roll 3.20x50 |
      | 0029 - Roll 2.20x50 |
    And total number of records returned to "Ahmed.Al-Ashry" is "4"

  Scenario: (02) Read Item's UOMs dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read UOMs for Item "000001 - Hot Laminated Frontlit Fabric roll"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page