# Author: Marina , 13-Jan-2018 ,  11:25 AM
Feature: Save UoM section in Item (Validations and Exceptions)

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia |
      | PurchasingResponsible_Signmedia | MeasuresViewer      |
      | SuperUser                       | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission         | Condition                      |
      | ItemOwner_Signmedia | Item:UpdateUoMData | [purchaseUnitName='Signmedia'] |
      | MeasuresViewer      | Measures:ReadAll   |                                |
      | SuperUserSubRole    | *:*                |                                |
    And the following Items exist:
      | Code   | Name                                 | PurchasingUnit |
      | 000007 | PRO-V-ST-1                           | 0002           |
      | 000053 | Hot Laminated Frontlit Fabric roll 2 | 0002           |
    And the following UnitOfMeasures section exist in Item with code "000053":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0035          | 63.5             | 12345678910111231 |
    And the following UnitOfMeasures section exist in Item with code "000012":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0043          | 2.5              | 12345678910111239 |
      | 0042          | 2                | 12345678910111238 |
    And edit session is "30" minutes

  ###### Save UnitOfMeasures section with Incorrect Data (Validation Failure)  ##############################################################
  Scenario: (01) Save UnitOfMeasures section with Incorrect Data: Alternate Unit doesn't exist >> Gen-BR-01/Item-BR-04 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 160              | 123456789101114   |
      | 0050              | test1         | 160              | 12345678910111405 |
      | 0051              | test2         | 160              | 12345678910111440 |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "alternativeUnit" field "Item-msg-02" and sent to "Gehan.Ahmed" with codes:
      | Codes |
      | 0050  |
      | 0051  |

  ####### Save UnitOfMeasures section with Malicious Input  (Abuse Cases )####################################################################
  Scenario Outline: (02) Save UnitOfMeasures section with malicious data: Alternate Unit is incorrect >> tem-BR-02 - ConversionFactor is incorrect >> Gen-BR-09 - OldItemNumber is incorrect >> Gen-BR-04 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode         | AlternateUnit         | ConversionFactor         | OldItemNumber         |
      | <AlternateUnitCode_Value> | <AlternateUnit_Value> | <ConversionFactor_Value> | <OldItemNumber_Value> |
    Then the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | AlternateUnitCode_Value | AlternateUnit_Value | ConversionFactor_Value | OldItemNumber_Value   |
      | 0024                    | Container FCL       | 0                      | 12345678910111425     |
      | 0024                    | Container FCL       | -1                     | 12345678910111426     |
      | 0024                    | Container FCL       | 1000000000             | 12345678910111427     |
      | 0024                    | Container FCL       | test                   | 12345678910111428     |
      | 0024                    | Container FCL       | *&^$                   | 12345678910111429     |
      | 0035                    | Roll 1.27x50        | 150                    | 0                     |
      | 0035                    | Roll 1.27x50        | 150                    | 123456789123456789123 |
      | 0035                    | Roll 1.27x50        | 150                    | test                  |
      | 0035                    | Roll 1.27x50        | 150                    | *&^$                  |
      | 12456                   | testCode1           | 150                    | 12345678910111426     |
      | test                    | testCode2           | 150                    | 12345678910111427     |

  Scenario: (03) Save UnitOfMeasures section with malicious data: Alternate Unit is repeated >> Item-BR-04 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 150              | 12345678910111443 |
      | 0024              | Container FCL | 160              | 12345678910111444 |
    Then the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (04) Save UnitOfMeasures section with malicious data: Alternate Unit is base unit >> Item-BR-04 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0019              | Square Meter  | 150              | 12345678910111443 |

    Then the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  ############ Save UnitOfMeasures section with missing mandatory fields (Abuse Cases) #########################################################
  Scenario Outline: (05) Save UnitOfMeasures section with Missing mandatory Fields for UnitOfMeasures section (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:30 AM" with the following values:
      | AlternateUnit             | ConversionFactor         | OldItemNumber         |
      | <AlternateUnitCode_Value> | <ConversionFactor_Value> | <OldItemNumber_Value> |
    Then the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | AlternateUnitCode_Value | AlternateUnit_Value | ConversionFactor_Value | OldItemNumber_Value |
      |                         |                     | 150                    | 12345678910111430   |
      | 0024                    | Container FCL       |                        | 12345678910111431   |
      | 0024                    | Container FCL       | 150                    |                     |

  ######### Save UnitOfMeasuressection (After edit session expires) ################################################################################
  Scenario: (06) Save UnitOfMeasures section after edit session expired for Items (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000007" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000007" at "07-Jan-2019 09:41 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 150F             | 12345678910111432 |
    Then the lock by "Gehan.Ahmed" on UnitOfMeasures section of Item with code "000007" is released
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (07) Save UnitOfMeasures section after edit session expired & Item is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And UnitOfMeasures section of Item with code "000053" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the Item with code "000053" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" saves UnitOfMeasures section of Item with code "000053" at "07-Jan-2019 09:45 AM" with the following values:
      | AlternateUnitCode | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0024              | Container FCL | 150              | 12345678910111433 |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
