Feature: Read Next and Previous in Item

Feature Description

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
      | hr1          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role             |
      | M.D                              | ItemViewer           |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission   | Condition                       |
      | ItemViewer           | Item:ReadAll |                                 |
      | ItemOwner_Signmedia  | Item:ReadAll | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:ReadAll | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:ReadAll | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole     | *:*          |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission   | Condition                       |
      | Amr.Khalil  | Item:ReadAll | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:ReadAll | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission   |
      | Afaf | Item:ReadAll |
    And the following Items with the following order exist:
      | Code   | PurchasingUnit |
      | 100012 | 0006           |
      | 100011 | 0005           |
      | 100010 | 0004           |
      | 100009 | 0001           |
      | 100008 | 0001, 0002     |
      | 100007 | 0002           |
      | 100006 | 0001           |
      | 100005 | 0001           |
      | 100004 | 0002           |
      | 100003 | 0002           |

  Scenario Outline:: (01) Read Next in Item by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view next Item of current Item with code "<ItemCode>"
    Then the next Item code "<NextValue>" is displayed to "<User>"
    Examples:
      | User         | ItemCode | NextValue |
      | Gehan.Ahmed  | 100004   | 100007    |
      | Gehan.Ahmed  | 100007   | 100008    |
      | Amr.Khalil   | 100005   | 100006    |
      | Amr.Khalil   | 100006   | 100008    |
      | Amr.Khalil   | 100009   | 100012    |
      | Ashraf.Fathi | 100004   | 100005    |
      | Ashraf.Fathi | 100009   | 100010    |
      | Ashraf.Fathi | 100010   | 100011    |
      | Ashraf.Fathi | 100011   | 100012    |

  Scenario Outline: (02) Read Previous in Item by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view previous Item of current Item with code "<ItemCode>"
    Then the previous Item code "<PrevValue>" is displayed to "<User>"
    Examples:
      | User         | ItemCode | PrevValue |
      | Gehan.Ahmed  | 100004   | 100003    |
      | Gehan.Ahmed  | 100007   | 100004    |
      | Gehan.Ahmed  | 100008   | 100007    |
      | Amr.Khalil   | 100006   | 100005    |
      | Amr.Khalil   | 100009   | 100008    |
      | Amr.Khalil   | 100012   | 100009    |
      | Ashraf.Fathi | 100004   | 100003    |
      | Ashraf.Fathi | 100009   | 100008    |
      | Ashraf.Fathi | 100010   | 100009    |
      | Ashraf.Fathi | 100011   | 100010    |
      | hr1          | 100012   | 100011    |

  Scenario: (03) Read Next in Item Navigate unexisted next record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Amr.Khalil"
    And "Amr.Khalil" first deleted the Item with code "100012" successfully
    When "hr1" requests to view next Item of current Item with code "100011"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  Scenario: (04) Read Previous in Item Navigate unexisted Previous record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the Item with code "100003" successfully
    When "hr1" requests to view previous Item of current Item with code "100004"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  Scenario: (05)Read Next in Item Navigate unexisted next record
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the Item with code "100007" successfully
    When "hr1" requests to view next Item of current Item with code "100006"
    Then the next Item code "100008" is displayed to "hr1"

  Scenario: (06) Read Previous in Item Navigate unexisted Previous record
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the Item with code "100004" successfully
    When "hr1" requests to view previous Item of current Item with code "100005"
    Then the previous Item code "100003" is displayed to "hr1"

  Scenario: (07) Read Next in Item by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view next Item of current Item with code "100005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (08) Read Previous in Item by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view previous Item of current Item with code "100005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page