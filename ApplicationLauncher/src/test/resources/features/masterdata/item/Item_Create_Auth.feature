# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmad Hamed due to update required by story EBS-6237

Feature: Create Item - Authorization

  Background:

    Given the following users and roles exist:
      | Name                        | Role                                                    |
      | Mahmoud.Abdelaziz           | Storekeeper_Signmedia                                   |
      | Gehan.Ahmed.NoItemGroup     | PurchasingResponsible_Signmedia_CannotReadItemGroupRole |
      | Gehan.Ahmed.NoMeasures      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole  |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingResponsible_Signmedia_CannotReadPurUnitRole   |
      | Gehan.Ahmed.NoItemTypes     | PurchasingResponsible_Signmedia_CannotReadItemTypesRole |
    And the following roles and sub-roles exist:
      | Role                                                    | Subrole                 |
      | PurchasingResponsible_Signmedia_CannotReadItemGroupRole | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_CannotReadItemGroupRole | MeasuresViewer          |
      | PurchasingResponsible_Signmedia_CannotReadItemGroupRole | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemGroupRole | ItemTypeViewer          |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole  | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole  | ItemGroupViewer         |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole  | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole  | ItemTypeViewer          |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole   | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole   | ItemGroupViewer         |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole   | MeasuresViewer          |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole   | ItemTypeViewer          |
      | PurchasingResponsible_Signmedia_CannotReadItemTypesRole | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_CannotReadItemTypesRole | MeasuresViewer          |
      | PurchasingResponsible_Signmedia_CannotReadItemTypesRole | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemTypesRole | ItemGroupViewer         |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission             | Condition |
      | ItemOwner_Signmedia     | Item:Create            |           |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll |           |
      | ItemGroupViewer         | ItemGroup:ReadAll      |           |
      | MeasuresViewer          | Measures:ReadAll       |           |
      | ItemTypeViewer          | ItemType:ReadAll       |           |
    And the following users doesn't have the following permissions:
      | User                        | Permission             |
      | Gehan.Ahmed.NoItemGroup     | ItemGroup:ReadAll      |
      | Gehan.Ahmed.NoMeasures      | Measures:ReadAll       |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingUnit:ReadAll |
      | Gehan.Ahmed.NoItemTypes     | ItemType:ReadAll       |
      | Mahmoud.Abdelaziz           | Item:Create            |
    And the following ItemGroups exist:
      | Code   | Name                 | GroupLevel | ParentCode | ParentName     |
      | 000001 | Ink                  | ROOT       |            |                |
      | 000002 | Signmedia Inks       | HIERARCHY  | 000001     | Ink            |
      | 000003 | White Signmedia Inks | LEAF       | 000002     | Signmedia Inks |
    And the following Measures exist:
      | Code | Type     | Symbol | Name  |
      | 0001 | Standard | M      | Meter |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following ItemTypes exist:
      | Code       |
      | COMMERCIAL |
      | SERVICE    |
      | CONSUMABLE |
  ######## Create by an unauthorized user

  # EBS-2180
  Scenario: (01) Create Item by an unauthorized user  (Abuse case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  ######## Create using values which the user is not authorized to Read

  # EBS-2180
  Scenario Outline: (02) Create Item by User is not authorized to read: BaseUnit / ItemGroup / Purchase Units / ItemType (Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                        |
      | Gehan.Ahmed.NoMeasures      |
      | Gehan.Ahmed.NoItemGroup     |
      | Gehan.Ahmed.NoPurchaseUnits |
      | Gehan.Ahmed.NoItemTypes     |