Feature: RequestEdit and CancelEdit UnitOfMeasures section in Item

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | hr1          | SuperUser                        |
      | Ashraf.Fathi | M.D                              |
    And the following roles and sub-roles exist:
      | Role                             | Subrole              |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated |
      | PurchasingResponsible_Signmedia  | MeasuresViewer       |
      | PurchasingResponsible_Flexo      | MeasuresViewer       |
      | PurchasingResponsible_Corrugated | MeasuresViewer       |
      | SuperUser                        | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission         | Condition                       |
      | ItemOwner_Signmedia  | Item:UpdateUoMData | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo      | Item:UpdateUoMData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:UpdateUoMData | [purchaseUnitName='Corrugated'] |
      | MeasuresViewer       | Measures:ReadAll   |                                 |
      | SuperUserSubRole     | *:*                |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission         | Condition                       |
      | Amr.Khalil  | Item:UpdateUoMData | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Item:UpdateUoMData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Item:UpdateUoMData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User         | Permission         |
      | Ashraf.Fathi | Item:UpdateUoMData |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
      | 000012 | Ink2                                                    | 0006           |
      | 000053 | Hot Laminated Frontlit Fabric roll 2                    | 0002           |
      | 000054 | Hot Laminated Frontlit Fabric roll 3                    | 0002           |
    And edit session is "30" minutes

    ############# Request to open UnitOfMeasures in edit mode ######################################################

  Scenario Outline: (01) Request to edit UnitOfMeasures section: User with one or more role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit UnitOfMeasures section of Item with code "<ItemCode>"
    Then UnitOfMeasures section of Item with code "<ItemCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
    Examples:
      | ItemCode | User        |
      | 000001   | Gehan.Ahmed |
      | 000006   | Amr.Khalil  |
      | 000012   | Amr.Khalil  |


  Scenario: (02) Request to edit UnitOfMeasures section when section is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened UnitOfMeasures section of Item with code "000001" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit UnitOfMeasures section of Item with code "000001"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"


  Scenario: (03) Request to edit UnitOfMeasures section of an Item that doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000053" successfully
    When "Gehan.Ahmed" requests to edit UnitOfMeasures section of Item with code "000053"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (04) Request to edit UnitOfMeasures section by an unauthorized (Abuse Case)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to edit UnitOfMeasures section of Item with code "000006"
    Then "Ashraf.Fathi" is logged out
    And "Ashraf.Fathi" is forwarded to the error page

  Scenario: (05) Request to edit UnitOfMeasures section by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit UnitOfMeasures section of Item with code "000006"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    ############# Cancel saving  UnitOfMeasures  ######################################################

  Scenario Outline: (06) Cancel save UnitOfMeasures section within the edit session User with one or more role (Happy Path)
    Given user is logged in as "<User>"
    And UnitOfMeasures section of Item with code "<ItemCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving UnitOfMeasures section of Item with code "<ItemCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on UnitOfMeasures section of Item with code "<ItemCode>" is released
    Examples:
      | ItemCode | User        |
      | 000001   | Gehan.Ahmed |
      | 000006   | Amr.Khalil  |
      | 000012   | Amr.Khalil  |

  Scenario: (07) Cancel save UnitOfMeasures section after edit session expire (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And UnitOfMeasures section of Item with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving UnitOfMeasures section of Item with code "000001" at "07-Jan-2019 09:45 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"


  Scenario: (08) Cancel save UnitOfMeasures section while Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And UnitOfMeasures section of Item with code "000054" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the Item with code "000054" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" cancels saving UnitOfMeasures section of Item with code "000054" at "07-Jan-2019 09:46 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (09) Cancel save UnitOfMeasures section by an unauthorized users (Authorization\Abuse Case)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" cancels saving UnitOfMeasures section of Item with code "000001"
    Then "Ashraf.Fathi" is logged out
    And "Ashraf.Fathi" is forwarded to the error page

  Scenario: (10) Cancel save UnitOfMeasures section by an unauthorized user due unmatched condition (Authorization\Abuse Case becoz user has no way to enter edit mode)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" cancels saving UnitOfMeasures section of Item with code "000001"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
