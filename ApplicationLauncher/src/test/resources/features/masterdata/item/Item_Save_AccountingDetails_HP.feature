Feature: Save Happy Path AccountingDetails section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia          |
      | PurchasingResponsible_Signmedia | ChartOfAccountsViewerLimited |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                   | Condition                      |
      | ItemOwner_Signmedia          | Item:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewerLimited | GLAccount:ReadAllLimited     |                                |
    And the following AccountingDetails for Items exist:
      | ItemCode | AccountCode | AccountName | BusinessUnit |
      | 000014   |             |             | Signmedia    |
      | 000005   | 10107       | Lands 2     | Flexo        |
      | 000002   | 10207       | PO          | Signmedia    |
    And the following Accounts exist:
      | AccountCode | AccountName     | Level | State  |
      | 10207       | PO              | 3     | Active |
      | 10201       | Service Vendors | 3     | Active |
      | 10107       | Lands 2         | 3     | Active |
    And edit session is "30" minutes

  Scenario:(1) Save Accounting Details section within edit session for (Commercial/Consumable) item (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      | 10202       |
    Then AccountingDetails Section of Item with Code "000002" is updated as follows:
      | LastUpdatedBy | LastUpdateDate     | AccountCode |
      | Gehan.Ahmed   | 7-Jan-2019 9:30 AM | 10202       |
    And the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  Scenario:(2) Save Accounting Details section within edit session for service Item (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000014" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Service Item with Code "000014" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode | CostFactor |
      | 10202       | true       |
    Then AccountingDetails Section of Service Item with Code "000014" is updated as follows:
      | LastUpdatedBy | LastUpdateDate     | AccountCode | CostFactor |
      | Gehan.Ahmed   | 7-Jan-2019 9:30 AM | 10202       | true       |
    And the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000014" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"