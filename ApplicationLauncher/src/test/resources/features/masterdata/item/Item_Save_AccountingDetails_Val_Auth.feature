Feature: Save Validation AccountingDetails section

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Gehan.Ahmed.NoAccounts | PurchasingResponsible_Signmedia_CannotViewAccountsRole |
      | hr1                    | SuperUser                                              |
      | Afaf                   | FrontDesk                                              |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole                      |
      | PurchasingResponsible_Signmedia                        | ItemOwner_Signmedia          |
      | PurchasingResponsible_Signmedia                        | ChartOfAccountsViewerLimited |
      | PurchasingResponsible_Signmedia_CannotViewAccountsRole | ItemOwner_Signmedia          |
      | SuperUser                                              | SuperUserSubRole             |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                   | Condition                      |
      | ItemOwner_Signmedia          | Item:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewerLimited | GLAccount:ReadAllLimited     |                                |
      | SuperUserSubRole             | *:*                          |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                   | Condition                  |
      | Gehan.Ahmed | Item:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                   | Permission               |
      | Gehan.Ahmed.NoAccounts | GLAccount:ReadAllLimited |
    And the following AccountingDetails for Items exist:
      | ItemCode | AccountCode | AccountName | BusinessUnit |
      | 000005   | 10107       | Lands 2     | Flexo        |
      | 000002   | 10207       | PO          | Signmedia    |
      | 000014   |             |             | Signmedia    |
    And the following Accounts exist:
      | AccountCode | AccountName     | Level | State  |
      | 10207       | PO              | 3     | Active |
      | 10201       | Service Vendors | 3     | Active |
      | 10107       | Lands 2         | 3     | Active |
    And edit session is "30" minutes

  Scenario:(1) Save Accounting Details section with Incorrect Data: Account doesn't exist (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      | 99999       |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Account field "Gen-msg-48" and sent to "Gehan.Ahmed"


  Scenario: (2) Save Accounting Details section with Malicious Data: Account is string for (Commercial/Consumable) Item(Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      | Ay7aga      |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (3) Save Accounting Details section with Malicious Data: CostFactor is string for Service Item (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000014" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Service Item with Code "000014" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode | CostFactor |
      | 10203       | Ay7aga     |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000014" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page


  Scenario:(4) Save Accounting Details section with Malicious Data: Account is level 2 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      | 102         |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(5) Save Accounting Details section with Malicious Data: Account is inActive (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      | 10101       |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(6) Save Accounting Details section with Malicious Data: Account is empty (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | AccountCode |
      |             |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Item with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page


  Scenario:(7) Save Accounting Details section after edit session expired (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:41 am" with the following values:
      | AccountCode |
      | 10202       |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario:(8) Save Accounting Details section after edit session expired & Item is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    And "hr1" first deleted the Item with code "000002" successfully at "07-Jan-2019 09:41 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Item with Code "000002" at "07-Jan-2019 09:45 am" with the following values:
      | AccountCode |
      | 10201       |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(9) Save Accounting Details section by an unauthorized users due unmatched (with/without)condition (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves AccountingDetails section of Item with Code "000005" with the following values:
      | AccountCode |
      | 10102       |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |


  Scenario: (10) Save Accounting Details section by a user who is not authorized to read one of data entry (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed.NoAccounts"
    And AccountingDetails section of Item with code "000002" is locked by "Gehan.Ahmed.NoAccounts" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed.NoAccounts" saves AccountingDetails section of Item with Code "000002" with the following values:
      | AccountCode |
      | 10102       |
    Then the lock by "Gehan.Ahmed.NoAccounts" on AccountingDetails section of Item with code "000002" is released
    Then "Gehan.Ahmed.NoAccounts" is logged out
    And "Gehan.Ahmed.NoAccounts" is forwarded to the error page