# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmad Hamed due to update required by story EBS-6237

Feature: Create Item - Validations

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia | ItemGroupViewer         |
      | PurchasingResponsible_Signmedia | MeasuresViewer          |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia | ItemTypeViewer          |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission             | Condition |
      | ItemOwner_Signmedia     | Item:Create            |           |
      | ItemGroupViewer         | ItemGroup:ReadAll      |           |
      | MeasuresViewer          | Measures:ReadAll       |           |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll |           |
      | ItemTypeViewer          | ItemType:ReadAll       |           |
    And the following ItemGroups exist:
      | Code   | Name                 | GroupLevel | ParentCode | ParentName     |
      | 000001 | Ink                  | ROOT       |            |                |
      | 000002 | Signmedia Inks       | HIERARCHY  | 000001     | Ink            |
      | 000003 | White Signmedia Inks | LEAF       | 000002     | Signmedia Inks |
    And the following Measures exist:
      | Code | Type     | Symbol | Name  |
      | 0001 | Standard | M      | Meter |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following ItemTypes exist:
      | Code       |
      | COMMERCIAL |
      | SERVICE    |
      | CONSUMABLE |
  ######## Create with missing mandatory fields (Abuse cases since create button is not enabled until all mandatories are entered)

  Scenario: (01) Create Item Commercial with missing mandatory field: Name (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      |      | 000003    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (02) Create Item Commercial with missing mandatory field: Item Group (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 |           | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (03) Create Item Commercial with missing mandatory field: Base Unit (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    |          | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (04) Create Item Commercial with missing mandatory field: BatchManaged (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     |              | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (05) Create Item Commercial with missing mandatory field: OldItemReference (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        |                  | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (06) Create Item Commercial with missing mandatory field: PurchasingUnit (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     |                | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (07) Create Item Commercial with missing mandatory field: MarketName (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | 0002           |            | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (08) Create Item Commercial with missing mandatory field: Type (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 |      |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  ######## Create with incorrect input (Validation Failure)

  Scenario: (09) Create Item Incorrect input: Item Group doesn't exist >> Item-BR-01 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 999999    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Item Group field "Item-msg-01" and sent to "Gehan.Ahmed"

  Scenario: (10) Create Item Incorrect input: Base Unit doesn't exist >> Item-BR-02 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 9999     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to BaseUnit field "Item-msg-02" and sent to "Gehan.Ahmed"

  Scenario Outline: (11) Create Item Incorrect input: PurchasingUnit doesn't exist >> Item-BR-02 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | <PurchasingUnitValue> | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to PurchasingUnit field "Item-msg-03" and sent to "Gehan.Ahmed"
    Examples:
      | PurchasingUnitValue |
      | 0002, 9999          |
      | 9999, 0002          |
      | 9999                |

  Scenario: (12) Create Item Incorrect input: Item Type doesn't exist >> Item-BR-02 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type   |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 |           | 0001     |              | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | POTATO |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to ItemType field "Gen-msg-48" and sent to "Gehan.Ahmed"

  ######## Create with malicious input (Abuse Case\Client bypassing)

  Scenario Outline: (13) Create Item Malicious input: Name is invalid >> Gen-BR-01 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name            | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | <ItemNameValue> | 000003    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemNameValue                                                                                                 |
      | Self Adhesive Vinal <100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal >100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal #100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal $100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal &100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal +100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal ^100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal @100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal !100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal =100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal ~100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal \|100 micron- 140 gm- Glossy 1.07x50                                                      |
      | Self Adhesive Vinal \\100 micron- 140 gm- Glossy 1.07x50                                                      |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 |

  Scenario Outline: (14) Create Item Malicious input: MarketName is invalid >> Gen-BR-01 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName        | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | 0002           | <MarketNameValue> | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | MarketNameValue                                                                                               |
      | Self Adhesive Vinal <100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal >100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal #100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal $100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal &100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal +100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal ^100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal @100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal !100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal =100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal ~100 micron- 140 gm- Glossy 1.07x50                                                       |
      | Self Adhesive Vinal \|100 micron- 140 gm- Glossy 1.07x50                                                      |
      | Self Adhesive Vinal \\100 micron- 140 gm- Glossy 1.07x50                                                      |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 |

  Scenario: (15) Create Item Malicious input: Item Group is string >> Item-BR-01 (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | Ay7aga    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (16) Create Item Malicious input: Item Group not a leaf >> Item-BR-01 (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000001    | 0001     | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (17) Create Item Malicious input: Base Unit is a string >> Item-BR-02 (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | Ay7aga   | False        | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (18) Create Item Malicious input: BatchManaged is invalid (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | Ay7aga       | 123467891011     | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario Outline: (19) Create Item Malicious input: OldItemReference is invalid >> Gen-BR-04 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference        | PurchasingUnit | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | <OldItemReferenceValue> | 0002           | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | OldItemReferenceValue                                   |
      | Self Adhesive Vinal <100 micron- 140 gm- Glossy 1.07x50 |
      | 111111111111111111111                                   |
      | 1 2222222222                                            |
      | 1-23-34-5678                                            |
      | 1A2A3A4A5Aa                                             |

  Scenario Outline: (20) Create Item Malicious input: Purchasing Unit is invalid (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Item with the following values:
      | Name                                                   | ItemGroup | BaseUnit | BatchManaged | OldItemReference | PurchasingUnit        | MarketName                                             | Type       |
      | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | 000003    | 0001     | False        | 123467891011     | <PurchasingUnitValue> | Self Adhesive Vinal 100 micron- 140 gm- Glossy 1.07x50 | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | PurchasingUnitValue |
      | 0002, Ay7aga        |
      | Ay7aga, 0002        |
      | Ay7aga              |

