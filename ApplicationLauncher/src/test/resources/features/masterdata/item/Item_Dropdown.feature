Feature: View All Items

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Ahmed.Seif     | Quality_Specialist        |
      | Afaf           | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole              |
      | SalesSpecialist_Signmedia | ItemViewer_Signmedia |
      | Quality_Specialist        | ItemViewer           |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission   | Condition                      |
      | ItemViewer_Signmedia | Item:ReadAll | [purchaseUnitName='Signmedia'] |
      | ItemViewer           | Item:ReadAll |                                |
    And the following users doesn't have the following permissions:
      | User | Permission   |
      | Afaf | Item:ReadAll |
    And the following Items exist with the below details:
      | Item                                                             | Type       | UOM          | PurchaseUnit                                                                                     |
      | 000001 - Hot Laminated Frontlit Fabric roll                      | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000002 - Hot Laminated Frontlit Backlit roll                     | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000003 - Flex Primer Varnish E33                                 | COMMERCIAL | 0003 - Kg    | 0002 - Signmedia                                                                                 |
      | 000004 - Flex cold foil adhesive E01                             | COMMERCIAL | 0003 - Kg    | 0002 - Signmedia                                                                                 |
      | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50   | COMMERCIAL | 0019 - M2    | 0001 - Flexo                                                                                     |
      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | COMMERCIAL | 0019 - M2    | 0001 - Flexo                                                                                     |
      | 000007 - PRO-V-ST-1                                              | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000008 - PRO-V-ST-2                                              | COMMERCIAL | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000009 - Machine1                                                | COMMERCIAL | 0014 - Liter | 0001 - Flexo                                                                                     |
      | 000010 - Machine2                                                | COMMERCIAL | 0003 - Kg    | 0004 - Digital                                                                                   |
      | 000011 - Ink1                                                    | COMMERCIAL | 0014 - Liter | 0005 - Textile                                                                                   |
      | 000012 - Ink2                                                    | COMMERCIAL | 0014 - Liter | 0006 - Corrugated                                                                                |
      | 000013 - Ink3                                                    | COMMERCIAL | 0014 - Liter | 0006 - Corrugated                                                                                |
      | 000015 - Laptop HP Pro-book core i5                              | CONSUMABLE | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000051 - Ink5                                                    | COMMERCIAL | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000052 - Ink7                                                    | COMMERCIAL | 0014 - Liter | 0006 - Corrugated                                                                                |
      | 000053 - Hot Laminated Frontlit Fabric roll 2                    | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000054 - Hot Laminated Frontlit Fabric roll 3                    | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000058 - Flex Primer Varnish E22                                 | COMMERCIAL | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000059 - Ink3                                                    | COMMERCIAL | 0014 - Liter | 0006 - Corrugated                                                                                |
      | 000060 - Hot Laminated Frontlit Fabric roll 1                    | COMMERCIAL | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia, 0003 - Offset, 0004 - Digital, 0005 - Textile, 0006 - Corrugated |
      | 000014 - Insurance service                                       | SERVICE    | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000055 - Insurance service2                                      | SERVICE    | 0019 - M2    | 0002 - Signmedia                                                                                 |
      | 000056 - Shipment service                                        | SERVICE    | 0019 - M2    | 0001 - Flexo                                                                                     |
      | 000057 - Bank Fees                                               | SERVICE    | 0019 - M2    | 0002 - Signmedia                                                                                 |

  ##################################################### Authorized Items ####################################################
  Scenario: (01) Read list of Items dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Items
    Then the following Items values will be presented to "Ahmed.Al-Ashry":
      | Item                                          | UOM       | PurchaseUnit                                                                                     |
      | 000060 - Hot Laminated Frontlit Fabric roll 1 | 0019 - M2 | 0001 - Flexo, 0002 - Signmedia, 0003 - Offset, 0004 - Digital, 0005 - Textile, 0006 - Corrugated |
      | 000058 - Flex Primer Varnish E22              | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000057 - Bank Fees                            | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000055 - Insurance service2                   | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000051 - Ink5                                 | 0019 - M2 | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000015 - Laptop HP Pro-book core i5           | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000014 - Insurance service                    | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000008 - PRO-V-ST-2                           | 0019 - M2 | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000007 - PRO-V-ST-1                           | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000004 - Flex cold foil adhesive E01          | 0003 - Kg | 0002 - Signmedia                                                                                 |
      | 000003 - Flex Primer Varnish E33              | 0003 - Kg | 0002 - Signmedia                                                                                 |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0019 - M2 | 0002 - Signmedia                                                                                 |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0019 - M2 | 0002 - Signmedia                                                                                 |
    And total number of records returned to "Ahmed.Al-Ashry" is "15"

  Scenario: (02) Read list of Items dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Items
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  ######################################### Authorized Items Filtered By Business Unit #######################################
  #EBS-6506
  Scenario: (03) Read list of Items dropdown filtered by BusinessUnit by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read all Items with business unit "0001 - Flexo"
    Then the following Items values will be presented to "Ahmed.Seif":
      | Item                                                             | UOM          | PurchaseUnit                                                                                     |
      | 000060 - Hot Laminated Frontlit Fabric roll 1                    | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia, 0003 - Offset, 0004 - Digital, 0005 - Textile, 0006 - Corrugated |
      | 000056 - Shipment service                                        | 0019 - M2    | 0001 - Flexo                                                                                     |
      | 000051 - Ink5                                                    | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000009 - Machine1                                                | 0014 - Liter | 0001 - Flexo                                                                                     |
      | 000008 - PRO-V-ST-2                                              | 0019 - M2    | 0001 - Flexo, 0002 - Signmedia                                                                   |
      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0019 - M2    | 0001 - Flexo                                                                                     |
      | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 0019 - M2    | 0001 - Flexo                                                                                     |
    And total number of records returned to "Ahmed.Seif" is "7"

  #EBS-6506
  Scenario: (04) Read list of Items dropdown filtered by BusinessUnit by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Items with business unit "0002 - Signmedia"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  ######################################### Authorized Items Filtered By Type Code #######################################
  #EBS-7650
  Scenario: (05) Read list of Items dropdown filtered by Type code by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read all Items with type code "SERVICE"
    Then the following Items values will be presented to "Ahmed.Seif":
      | Item                        | UOM       | PurchaseUnit     |
      | 000057 - Bank Fees          | 0019 - M2 | 0002 - Signmedia |
      | 000056 - Shipment service   | 0019 - M2 | 0001 - Flexo     |
      | 000055 - Insurance service2 | 0019 - M2 | 0002 - Signmedia |
      | 000014 - Insurance service  | 0019 - M2 | 0002 - Signmedia |
    And total number of records returned to "Ahmed.Seif" is "4"

  #EBS-7650
  Scenario: (06) Read list of Items dropdown filtered by Type code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Items with type code "SERVICE"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page