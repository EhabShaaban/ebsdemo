#Author: Aya Sadek on Date: 08-Jan-2019, 01:54 PM
#Reviewer: Hend Ahmed, Somaya Ahmed (09-Jan-2019)

Feature: Save Item GeneralData Auth

  Background:
    Given the following users and roles exist:
      | Name                          | Role                                                         |
      | Shady.Abdelatif               | Accountant_Signmedia                                         |
      | Amr.Khalil                    | PurchasingResponsible_Flexo                                  |
      | Amr.Khalil                    | PurchasingResponsible_Corrugated                             |
      | Gehan.Ahmed.NoPurchaseUnits   | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        |
      | Gehan.Ahmed.NoProductManagers | PurchasingResponsible_Signmedia_CannotReadProductManagerRole |
    And the following roles and sub-roles exist:
      | Role                                                         | Subrole                  |
      | PurchasingResponsible_Flexo                                  | ItemOwner_Flexo          |
      | PurchasingResponsible_Corrugated                             | ItemOwner_Corrugated     |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | ProductManagerViewer     |
      | PurchasingResponsible_Signmedia_CannotReadProductManagerRole | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia_CannotReadProductManagerRole | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Flexo                                  | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo                                  | ProductManagerViewer     |
      | PurchasingResponsible_Corrugated                             | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated                             | ProductManagerViewer     |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition                       |
      | ItemOwner_Flexo          | Item:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated     | Item:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Signmedia      | Item:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |                                 |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |                                 |
      | ProductManagerViewer     | ProductManager:ReadAll |                                 |
    And the following users have the following permissions without the following conditions:
      | User       | Permission             | Condition                      |
      | Amr.Khalil | Item:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                          | Permission             |
      | Shady.Abdelatif               | Item:UpdateGeneralData |
      | Gehan.Ahmed.NoPurchaseUnits   | PurchasingUnit:ReadAll |
      | Gehan.Ahmed.NoProductManagers | ProductManager:ReadAll |
    And the following GeneralData for Items exist:
      | ItemCode | ItemName                           | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | PurchasingUnit | CreationDate         | MarketName                         | Type       | ProductManager       |
      | 000001   | Hot Laminated Frontlit Fabric roll | 000003    | 0019     | FALSE        | 12345678910111213 | 0002           | 05-Aug-2018 09:02 AM | Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0001 - Mohamed Nabil |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |
    And the following ProductManagers exist:
      | Code | Name          |
      | 0001 | Mohamed Nabil |
      | 0002 | Mahmoud Amr   |
      | 0003 | Moustafa      |

  # Save section by a user who is not authorized to save
  Scenario: (01) Save GeneralData section by an unauthorized users (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves GeneralData section of Item with Code "000001" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Test | 0001           | Test       | 0001 - Mohamed Nabil |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  # Save section by a user who is not authorized to save due to condition
  Scenario: (02) Save GeneralData section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves GeneralData section of Item with Code "000001" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Test | 0001           | Test       | 0001 - Mohamed Nabil |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

  ######## Save section using values which the user is not authorized to read
  Scenario: (03) Save GeneralData section by User is not authorized to read the PurchaseUnits values (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoPurchaseUnits"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed.NoPurchaseUnits" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed.NoPurchaseUnits" saves GeneralData section of Item with Code "000001" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager       |
      | Test | 0001           | Test       | 0001 - Mohamed Nabil |
    Then "Gehan.Ahmed.NoPurchaseUnits" is logged out
    And "Gehan.Ahmed.NoPurchaseUnits" is forwarded to the error page

  ######## Save section using values which the user is not authorized to read
  Scenario: (03) Save GeneralData section by User is not authorized to read the ProductManagers values (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoProductManagers"
    And GeneralData section of Item with code "000001" is locked by "Gehan.Ahmed.NoProductManagers" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed.NoProductManagers" saves GeneralData section of Item with Code "000001" with the following values:
      | Name | PurchasingUnit | MarketName | ProductManager     |
      | Test | 0001           | Test       | 0002 - Mahmoud Amr |
    Then "Gehan.Ahmed.NoProductManagers" is logged out
    And "Gehan.Ahmed.NoProductManagers" is forwarded to the error page