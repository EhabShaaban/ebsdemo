# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Customer Home Screen Allowed Actions:

  Background:
    Given the following users and roles exist:
      | Name            | Role                     |
      | Ashraf.Salah    | AccountantHead_Signmedia |
      | Shady.Abdelatif | Accountant_Signmedia     |
      | CreateOnlyUser  | CreateOnlyRole           |
      | Afaf            | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                  |
      | AccountantHead_Signmedia | CustomerOwner_Signmedia  |
      | AccountantHead_Signmedia | CustomerViewer_Signmedia |
      | Accountant_Signmedia     | CustomerViewer_Signmedia |
      | CreateOnlyRole           | CreateOnlySubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission       | Condition                      |
      | CustomerOwner_Signmedia  | Customer:Create  |                                |
      | CustomerViewer_Signmedia | Customer:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole        | Customer:Create  |                                |
    And the following users doesn't have the following permissions:
      | User            | Permission       |
      | Shady.Abdelatif | Customer:Create  |
      | Afaf            | Customer:Create  |
      | Afaf            | Customer:ReadAll |
      | CreateOnlyUser  | Customer:ReadAll |

  # EBS-5539
  Scenario Outline: (01) Read allowed actions in Customers Home Screen, by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Customers home screen
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User            | AllowedActions |
      | Ashraf.Salah    | ReadAll,Create |
      | Shady.Abdelatif | ReadAll        |
      | CreateOnlyUser  | Create         |

  # EBS-5539
  Scenario: (02) Read allowed actions in Customers Home Screen, by Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Customers home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"