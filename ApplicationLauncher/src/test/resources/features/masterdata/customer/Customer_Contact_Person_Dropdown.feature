# Author: Eman Mansour, Reviewer: Somaya Ahmed

Feature: View All Contact Persons For Specific Customer

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                  |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                 | Condition                      |
      | CustomerViewer_Signmedia | Customer:ReadContactPerson | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                 | Condition                  |
      | Ahmed.Al-Ashry | Customer:ReadContactPerson | [purchaseUnitName='Flexo'] |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000002 | Client2               | 0001 - Flexo     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
    And the following ContactPersons for Customers exist:
      | Customer                       | CustomerContactPersonId | ContactPerson           |
      | 000007 - مطبعة أكتوبر الهندسية | 5                       | Wael Fathay             |
      | 000007 - مطبعة أكتوبر الهندسية | 8                       | October Contact Person  |
      | 000002 - Client2               | 2                       | Client 2 Contact Person |

  Scenario: (01) Read list of ContactPersons dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all ContactPersons for selected Customer "000007" in a dropdown
    Then the following ContactPersons values will be presented to "Ahmed.Al-Ashry" in the dropdown:
      | ContactPerson          |
      | Wael Fathay            |
      | October Contact Person |
    And total number of ContactPersons returned to "Ahmed.Al-Ashry" in the dropdown is equal to 2

  Scenario: (02) Read list of ContactPersons dropdown by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all ContactPersons for selected Customer "000002" in a dropdown
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page