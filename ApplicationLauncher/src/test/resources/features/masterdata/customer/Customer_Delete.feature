# Author Dev: Order Team (01-March-2021)
# Author Quality: Shirin Mahmoud

Feature: Delete Customer

  Background:
    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Salah | AccountantHead_Signmedia |
      | hr1          | SuperUser                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                 |
      | AccountantHead_Signmedia | CustomerOwner_Signmedia |
      | SuperUser                | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission      | Condition                      |
      | CustomerOwner_Signmedia | Customer:Delete | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole        | *:*             |                                |
    And the following users have the following permissions without the following conditions:
      | User         | Permission      | Condition                  |
      | Ashraf.Salah | Customer:Delete | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for Customer exist:
      | Code   | MarketName            | LegalName             | Type  | CreditLimit | Currency   | State  | OldCustomerNumber | CreatedBy | CreationDate         |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | LOCAL | 5000.000    | 0001 - EGP | Active | 1                 | Admin     | 20-Oct-2019 11:00 AM |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | LOCAL | 5000.000    | 0001 - EGP | Active | 2                 | Admin     | 11-Oct-2019 11:00 AM |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | LOCAL | 5000.000    | 0001 - EGP | Active | 3                 | Admin     | 10-Aug-2019 11:00 AM |
    #@INSERT
    And the following BusinessUnits for Customer exist:
      | Code   | BusinessUnit                    |
      | 000008 | 0002 - Signmedia, 0003 - Offset |
      | 000009 | 0001 - Flexo                    |
      | 000011 | 0002 - Signmedia                |
    #@INSERT
    And the following LegalRegistrationData for Customer exist:
      | Code   | RegistrationNumber | IssuedFrom          | TaxCardNumber | TaxAdministration   | TaxFileNumber         |
      | 000008 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000009 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000011 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
    #@INSERT
    And the following Customer Addresses exist:
      | Code   | AddressName       | AddressDescription          | Type |
      | 000011 | Giza              | 7th st, El Sheikh Zayed     |      |
      | 000011 | Heliopolis Branch | 99, El Hegaz St, Heliopolis |      |
    #@INSERT
    And the following Customer ContactPersons exist:
      | Code   | ContactPersonName      |
      | 000011 | Wael Fathay            |
      | 000011 | October Contact Person |
 #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State                 | CreationDate         |DocumentOwner  |
      | 2019000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | SalesInvoiceActivated | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company        | Store                     |
      | 2019000010 | 0002 - DigiPro | 0003 - DigiPro Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2019000010 | 000011 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000011 - 99, El Hegaz St, Heliopolis | EGP         | 000011 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

  # EBS-7008
  Scenario: (01) Delete Customer, where Customer is Active - by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete Customer with code "000008"
    Then Customer with code "000008" is deleted
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-12"

  # EBS-7008
  Scenario: (02) Delete Customer, where Customer doesn't exist (Exception Case)
    Given user is logged in as "Ashraf.Salah"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Customer with code "000008" successfully
    When "Ashraf.Salah" requests to delete Customer with code "000008"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-01"

  # EBS-7008
  #Apply on SO, SI, SRO, GI-BasedOn-SO, GR-BasedOn-SRO, Collection[All Types Except BasedOn-Payment & BasedOn-DebitNote?], NR, CN-BasedOn-SRO, GL-Sub-Account, PR-BasedOn-CN?
  Scenario Outline: (03) Delete Customer, where Customer is referenced by anbother object (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Customer with code "<CustomerCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-27"
    Examples:
      | User         | CustomerCode |
      | Ashraf.Salah | 000011       |

  # EBS-7008
  @Future
  Scenario: (04) Delete Customer, where Customer is locked by another user (Exception Case)
    Given user is logged in as "Ashraf.Salah"create Customer-Delete BDD
    And another user is logged in as "hr1"
    And first "hr1" opens "GeneralData" section of Customer with code "000008" in edit mode
    When "Ashraf.Salah" requests to delete Customer with code "000008"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-14"


  # EBS-7008
  Scenario: (05) Delete Customer, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete Customer with code "000009"
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
