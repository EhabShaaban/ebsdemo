# Author Dev: Order Team (15-Feb-2021)
# updated by: Fatma Al Zahraa (EBS - 8219)
# Author Quality: Shirin Mahmoud

Feature: Create Customer - Validation

  Background:
    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Salah | AccountantHead_Signmedia |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                 |
      | AccountantHead_Signmedia | CustomerOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission      | Condition |
      | CustomerOwner_Signmedia | Customer:Create |           |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following IssuedFrom exist:
      | Code | Name         |
      | 0001 | جنوب القاهرة |
    And the following TaxAdministration exist:
      | Code | Name         |
      | 0001 | مأمورية شمال |

    #@INSERT
    And the following GeneralData for Customer exist:
      | Code   | MarketName           | LegalName            | Type  | CreditLimit | Currency   | State  | OldCustomerNumber | CreatedBy | CreationDate         |
      | 000008 | شركة الفؤاد التجارية | شركة الفؤاد التجارية | LOCAL | 5000.000    | 0001 - EGP | Active | 12345             | Admin     | 20-Oct-2019 11:00 AM |
   #@INSERT
    And the following BusinessUnits for Customer exist:
      | Code   | BusinessUnit                    |
      | 000008 | 0002 - Signmedia, 0003 - Offset |
    #@INSERT
    And the following LegalRegistrationData for Customer exist:
      | Code   | RegistrationNumber | IssuedFrom          | TaxCardNumber | TaxAdministration   | TaxFileNumber         |
      | 000008 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |


  # Apply first scenario for all fields in Unit Tests
  # Apply Example 1 , 2 , 3  of second scenario for Currency, TaxAdministration and BusinessUnit in Unit Tests
  # Apply other Examples of second scenario for LegalName  for MarketName, RegistrationNumber, TaxFileNumber and TaxCardNumber in Unit Tests

  # EBS-1052
  # EBS-8219
  Scenario Outline: (01) Create Customer, with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates Customer with the following values:
      | LegalName   | MarketName            | BusinessUnit | CreditLimit | Currency | RegistrationNumber | IssuedFrom | TaxAdministration | TaxCardNumber | TaxFileNumber         | OldCustomerNumber |
      | <LegalName> | مطبعة أكتوبر الهندسية | 0001, 0002   | 100000.000  | EGP      | 56734              | 0001       | 0001              | 345-567-874   | 567-4-34523-345-23-45 | 12345             |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | LegalName |
      |           |
      | ""        |
      | N/A       |

  # EBS-1052
  # EBS-8219
  Scenario Outline: (02) Create Customer, with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates Customer with the following values:
      | LegalName   | MarketName            | BusinessUnit | CreditLimit | Currency | RegistrationNumber | IssuedFrom   | TaxAdministration | TaxCardNumber | TaxFileNumber         | OldCustomerNumber   |
      | <LegalName> | مطبعة أكتوبر الهندسية | 0001, 0002   | 100000.000  | EGP      | 56734              | <IssuedFrom> | 0001              | 345-567-874   | 567-4-34523-345-23-45 | <OldCustomerNumber> |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | IssuedFrom | LegalName                                                                                              | OldCustomerNumber      |
      | 00001      | name                                                                                                   | 12345                  |
      | ay7aga     | name                                                                                                   | 54321                  |
      | 9999       | name                                                                                                   | 6789                   |
      | 0001       | asdfghjkmnasdfghjkmnasdfghjkmnasdfghjkmnasdfghjkmn2asdfghjkmnasdfghjkmnasdfghjkmnasdfghjkmnasdfghjkmn2 | 2468                   |
      | 0001       | $                                                                                                      | 35911                  |
      | 0001       | name                                                                                                   | 35$ 911                |
      | 0001       | name                                                                                                   | 1234567891011121314156 |

   # EBS-8219
  Scenario: (03) Create Customer, with incorrect input: There is already an existing Customer record with same oldCustomerNumber (Validation Failure)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates Customer with the following values:
      | LegalName             | MarketName            | BusinessUnit | CreditLimit | Currency | RegistrationNumber | IssuedFrom | TaxAdministration | TaxCardNumber | TaxFileNumber         | OldCustomerNumber |
      | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0001, 0002   | 100000.000  | EGP      | 56734              | 0001       | 0001              | 345-567-874   | 567-4-34523-345-23-45 | 12345             |
    Then a failure notification is sent to "Ashraf.Salah" with the following message "Gen-msg-05"
    And the following error message "CUSTOMER-msg-01" is returned and sent to "Ashraf.Salah"

