# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Customer Object Screen Allowed Actions:

  Background:
    Given the following users and roles exist:
      | Name               | Role                     |
      | Ashraf.Salah       | AccountantHead_Signmedia |
      | Shady.Abdelatif    | Accountant_Signmedia     |
      | CannotReadSections | CannotReadSectionsRole   |
      | hr1                | SuperUser                |
      | Afaf               | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                   |
      | AccountantHead_Signmedia | CustomerOwner_Signmedia   |
      | AccountantHead_Signmedia | CustomerViewer_Signmedia  |
      | Accountant_Signmedia     | CustomerViewer_Signmedia  |
      | CannotReadSectionsRole   | CannotReadSectionsSubRole |
      | SuperUser                | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | CustomerOwner_Signmedia   | Customer:Delete                    | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia  | Customer:ReadAll                   | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia  | Customer:ReadLegalRegistrationData | [purchaseUnitName='Signmedia'] |
      | CannotReadSectionsSubRole | Customer:Delete                    |                                |
      | SuperUserSubRole          | *:*                                |                                |
    And the following users doesn't have the following permissions:
      | User               | Permission                         |
      | Afaf               | Customer:ReadAll                   |
      | Afaf               | Customer:ReadLegalRegistrationData |
      | Afaf               | Customer:Delete                    |
      | Shady.Abdelatif    | Customer:Delete                    |
      | CannotReadSections | Customer:ReadAll                   |
      | CannotReadSections | Customer:ReadLegalRegistrationData |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Ashraf.Salah    | Customer:ReadAll                   | [purchaseUnitName='Flexo'] |
      | Ashraf.Salah    | Customer:ReadLegalRegistrationData | [purchaseUnitName='Flexo'] |
      | Ashraf.Salah    | Customer:Delete                    | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Customer:ReadAll                   | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Customer:ReadLegalRegistrationData | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for Customer exist:
      | Code   | MarketName           | LegalName            | Type  | CreditLimit | Currency   | State  | OldCustomerNumber | CreatedBy | CreationDate         |
      | 000008 | شركة الفؤاد التجارية | شركة الفؤاد التجارية | LOCAL | 5000.000    | 0001 - EGP | Active | 1                 | Admin     | 20-Oct-2019 11:00 AM |
      | 000009 | المطبعة الأمنية      | المطبعة الأمنية      | LOCAL | 5000.000    | 0001 - EGP | Active | 2                 | Admin     | 11-Oct-2019 11:00 AM |
    #@INSERT
    And the following BusinessUnits for Customer exist:
      | Code   | BusinessUnit                    |
      | 000008 | 0002 - Signmedia, 0003 - Offset |
      | 000009 | 0001 - Flexo                    |
    #@INSERT
    And the following LegalRegistrationData for Customer exist:
      | Code   | RegistrationNumber | IssuedFrom          | TaxCardNumber | TaxAdministration   | TaxFileNumber         |
      | 000008 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000009 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |

  # EBS-5765
  Scenario Outline: (01) Read allowed actions in Customer Object Screen, - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Customer with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code   | AllowedActions                             | NoOfActions |
      | Ashraf.Salah       | 000008 | ReadAll, ReadLegalRegistrationData, Delete | 3           |
      | Shady.Abdelatif    | 000008 | ReadAll, ReadLegalRegistrationData         | 2           |
      | CannotReadSections | 000008 | Delete                                     | 1           |

  # EBS-5765
  Scenario Outline: (02) Read allowed actions in Customer Object Screen, - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Customer with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | Code   |
      | Ashraf.Salah    | 000009 |
      | Shady.Abdelatif | 000009 |
      | Afaf            | 000009 |

  # EBS-5765
  @Future
  Scenario: (03) Read allowed actions in Customer Object Screen, - Object does not exist (Exception)
    Given user is logged in as "Ashraf.Salah"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Customer with code "000008" successfully
    When "Ashraf.Salah" requests to read actions of Customer with code "000008"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-01"
