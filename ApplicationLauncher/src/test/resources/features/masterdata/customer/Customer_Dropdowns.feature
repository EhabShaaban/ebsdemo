Feature: View All Customers

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                  |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission       | Condition                      |
      | CustomerViewer_Signmedia | Customer:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following Customers exist:
      | Code   | Name     | BusinessUnit     |
      | 000001 | Al Ahram | 0002 - Signmedia |
      | 000002 | Client2  | 0001 - Flexo     |
      | 000004 | Client3  | 0001 - Flexo     |

  Scenario: (01) Read list of Customers dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Customers for selected BU "0002"
    Then the following Customers values will be presented to "Ahmed.Al-Ashry":
      | Customer                       |
      | 000007 - مطبعة أكتوبر الهندسية |
      | 000001 - Al Ahram              |
    And total number of Customers returned to "Ahmed.Al-Ashry" is equal to 2

  @Future
  Scenario: (02) Read list of Customers dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Customers
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page