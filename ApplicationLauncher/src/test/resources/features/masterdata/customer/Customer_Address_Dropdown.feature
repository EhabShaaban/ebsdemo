# Author: Waseem Salama, Reviewer: Somaya Ahmed

Feature: View All Customer Addresses

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                  |
      | SalesCoordinator_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission           | Condition                      |
      | CustomerViewer_Signmedia | Customer:ReadAddress | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User           | Permission           | Condition                  |
      | Manar.Mohammed | Customer:ReadAddress | [purchaseUnitName='Flexo'] |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000002 | Client2               | 0001 - Flexo     |
    And the following Addresses for Customers exist:
      | Customer                       | CustomerAddressId | AddressName        | AddressDescription          |
      | 000007 - مطبعة أكتوبر الهندسية | 6                 | Heliopolis Branch  | 99, El Hegaz St, Heliopolis |
      | 000007 - مطبعة أكتوبر الهندسية | 5                 | Giza               | 7th st, El Sheikh Zayed     |
      | 000002 - Client2               | 2                 | 66, Salah Salem St | 66, Salah Salem St          |

  #EBS-6458
  Scenario: (01) Read list of Customer Addresses dropdown by authorized user due to condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read all CustomerAddresses for Customer with code "000007" in a dropdown
    Then the following CustomerAddresses values will be presented to "Manar.Mohammed" in the dropdown:
      | CustomerAddressId | AddressName       | AddressDescription          |
      | 6                 | Heliopolis Branch | 99, El Hegaz St, Heliopolis |
      | 5                 | Giza              | 7th st, El Sheikh Zayed     |
    And total number of CustomerAddresses returned to "Manar.Mohammed" in the dropdown is equal to 2

  #EBS-6458
  Scenario: (02) Read list of Customer Addresses dropdown by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read all CustomerAddresses for Customer with code "000002" in a dropdown
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page