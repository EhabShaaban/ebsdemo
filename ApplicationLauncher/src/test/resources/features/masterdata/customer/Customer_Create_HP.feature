# Author Dev: Order Team (31-Jan-2021)
# updated by: Fatma Al Zahraa (EBS - 6814)
# updated by: Fatma Al Zahraa (EBS - 8270)
# updated by: Fatma Al Zahraa (EBS - 8219)
# Author Quality: Shirin Mahmoud

Feature: Create Customer - Happy Path

  Background:

    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Salah | AccountantHead_Signmedia |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                 |
      | AccountantHead_Signmedia | CustomerOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission      | Condition |
      | CustomerOwner_Signmedia | Customer:Create |           |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following IssuedFrom exist:
      | Code | Name         |
      | 0001 | جنوب القاهرة |
    And the following TaxAdministration exist:
      | Code | Name         |
      | 0001 | مأمورية شمال |

  # EBS-6560
  # EBS-6814
  #EBS-8270
  #EBS - 8219
  Scenario: (01) Create Customer, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created Customer was with code "000007"
    When "Ashraf.Salah" creates Customer with the following values:
      | LegalName             | MarketName            | BusinessUnit | CreditLimit | Currency | RegistrationNumber | IssuedFrom | TaxAdministration | TaxCardNumber | TaxFileNumber         | OldCustomerNumber |
      | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0001, 0002   | 100000.000  | EGP      | 56734              | 0001       | 0001              | 345-567-874   | 567-4-34523-345-23-45 | 12345             |
    Then a new Customer is created with code "000008" as follows in the General Data:
      | CreationDate         | CreatedBy    | LegalName             | CreditLimit | Currency | MarketName            | BusinessUnit                   | State  | OldCustomerNumber |
      | 01-Jan-2021 11:00 AM | Ashraf.Salah | مطبعة أكتوبر الهندسية | 100000.000  | EGP      | مطبعة أكتوبر الهندسية | 0001 - Flexo, 0002 - Signmedia | Active | 12345             |
    And the LegalRegistrationData Section for the Customer with Code "000008" is updated as follows:
      | RegistrationNumber | IssuedFrom          | TaxCardNumber | TaxAdministration   | TaxFileNumber         |
      | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-11"