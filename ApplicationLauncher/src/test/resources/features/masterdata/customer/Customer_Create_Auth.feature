# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Create Customer - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                                                 | Role                                                                                   |
      | Ashraf.Salah                                                         | AccountantHead_Signmedia                                                               |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | AccountantHead_Signmedia_CannotReadBusinessUnitCurrencyIssuedFromTaxAdministrationRole |
      | Afaf                                                                 | FrontDesk                                                                              |
    And the following roles and sub-roles exist:
      | Role                                                                                   | Subrole                  |
      | AccountantHead_Signmedia                                                               | CustomerOwner_Signmedia  |
      | AccountantHead_Signmedia                                                               | CustomerViewer_Signmedia |
      | AccountantHead_Signmedia                                                               | PurUnitReader_Signmedia  |
      | AccountantHead_Signmedia                                                               | CurrencyViewer           |
      | AccountantHead_Signmedia_CannotReadBusinessUnitCurrencyIssuedFromTaxAdministrationRole | CustomerOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                     | Condition                      |
      | CustomerOwner_Signmedia  | Customer:Create                |                                |
      | CustomerViewer_Signmedia | Customer:ReadIssueFrom         |                                |
      | CustomerViewer_Signmedia | Customer:ReadTaxAdministration |                                |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll         | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer           | Currency:ReadAll               |                                |
    And the following users doesn't have the following permissions:
      | User                                                                 | Permission                     |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | Customer:ReadIssueFrom         |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | Customer:ReadTaxAdministration |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | PurchasingUnit:ReadAll         |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | Currency:ReadAll               |
      | Afaf                                                                 | Customer:Create                |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following IssuedFrom exist:
      | Code | Name         |
      | 0001 | جنوب القاهرة |
    And the following TaxAdministration exist:
      | Code | Name         |
      | 0001 | مأمورية شمال |

  # EBS-1039
  Scenario: (01) Request Create Customer, by an authorized user with authorized reads
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to create Customer
    Then the following authorized reads are returned to "Ashraf.Salah":
      | AuthorizedReads       |
      | ReadBusinessUnit      |
      | ReadCurrency          |
      | ReadIssuedFrom        |
      | ReadTaxAdministration |

  # EBS-1039
  Scenario: (02) Request Create Customer, by an authorized user with missing authorized reads
    Given user is logged in as "Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration"
    When "Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration" requests to create Customer
    Then there are no authorized reads returned to "Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration"

  # EBS-1039
  Scenario: (03) Request Create Customer, by an unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create Customer
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-1039
  Scenario Outline: (04) Create Customer, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" creates Customer with the following values:
      | LegalName             | MarketName            | Type  | BusinessUnit   | CreditLimit | Currency | RegistrationNumber | IssuedFrom | TaxAdministration | TaxCardNumber | TaxFileNumber         | OldCustomerNumber |
      | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | LOCAL | <BusinessUnit> | 100000.000  | EGP      | 56734              | 0001       | 0001              | 345-567-874   | 567-4-34523-345-23-45 | 12345             |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                                                                 | BusinessUnit |
      | Ashraf.Salah.NoBusinessUnitNoCurrencyNoIssuedFromNoTaxAdministration | 0002         |

#  Ashraf.Salah.NoBusinessUnit
#  Ashraf.Salah.NoCurrency
#  Ashraf.Salah.NoIssuedFrom
#  Ashraf.Salah.NoTaxAdministration

  #  @Future
#  TODO: remove permission -> PurUnitReader_Signmedia -> PurchasingUnit:ReadAll -> no condition from the system
#      | Ashraf.Salah                                                         | 0001         |
