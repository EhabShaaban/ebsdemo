# Author Dev: Order Team (31-Jan-2021)
  # updated by: Fatma Al Zahraa (EBS - 8270)
  # updated by: Fatma Al Zahraa (EBS - 8498)
  # Author Quality: Shirin Mahmoud

Feature: View All Customers

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                 |
      | Accountant_Signmedia | CustomerViewer_Signmedia |
      | Accountant_Signmedia | PurUnitReader_Signmedia  |
      | SuperUser            | SuperUserSubRole         |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition                      |
      | CustomerViewer_Signmedia | Customer:ReadAll       | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole         | *:*                    |                                |
    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | Customer:ReadAll |
  
  #@INSERT
    And the following GeneralData for Customer exist:
      | Code   | MarketName            | LegalName             | Type  | CreditLimit | Currency   | State  | OldCustomerNumber | CreatedBy | CreationDate         |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | LOCAL | 5000.000    | 0001 - EGP | Active | 1                 | Admin     | 20-Oct-2019 11:00 AM |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | LOCAL | 5000.000    | 0001 - EGP | Active | 2                 | Admin     | 11-Oct-2019 11:00 AM |
      | 000010 | Al Ahram              | Al Ahram              | LOCAL | 5000.000    | 0001 - EGP | Active | 3                 | Admin     | 10-Aug-2019 11:00 AM |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | LOCAL | 5000.000    | 0001 - EGP | Active | 4                 | Admin     | 10-Aug-2019 11:00 AM |
  #@INSERT
    And the following BusinessUnits for Customer exist:
      | Code   | BusinessUnit                    |
      | 000008 | 0002 - Signmedia, 0003 - Offset |
      | 000009 | 0001 - Flexo                    |
      | 000010 | 0001 - Flexo                    |
      | 000011 | 0002 - Signmedia                |
  #@INSERT
    And the following LegalRegistrationData for Customer exist:
      | Code   | RegistrationNumber | IssuedFrom          | TaxCardNumber | TaxAdministration   | TaxFileNumber         |
      | 000008 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000009 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000010 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
      | 000011 | 56734              | 0001 - جنوب القاهرة | 345-567-874   | 0001 - مأمورية شمال | 567-4-34523-345-23-45 |
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (01) View all Customers, by an authorized user WITHOUT condition - Super user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with no filter applied with 20 records per page
    Then the following values will be presented to "hr1":
      | Code   | MarketName            | LegalName             | BusinessUnit                    | OldCustomerNumber | State  |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0002 - Signmedia                | 4                 | Active |
      | 000010 | Al Ahram              | Al Ahram              | 0001 - Flexo                    | 3                 | Active |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | 0001 - Flexo                    | 2                 | Active |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "hr1" are 4
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (02) View all Customers, by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of Customers with no filter applied with 10 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code   | MarketName            | LegalName             | BusinessUnit                    | OldCustomerNumber | State  |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0002 - Signmedia                | 4                 | Active |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "Shady.Abdelatif" are 2
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (03) View all Customers, Filter by Code by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on Code which contains "008" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code   | MarketName           | LegalName            | BusinessUnit                    | OldCustomerNumber | State  |
      | 000008 | شركة الفؤاد التجارية | شركة الفؤاد التجارية | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "hr1" are 1
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (04) View all Customers, Filter by MarketName by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on MarketName which contains "مطبعة" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code   | MarketName            | LegalName             | BusinessUnit     | OldCustomerNumber | State  |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0002 - Signmedia | 4                 | Active |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | 0001 - Flexo     | 2                 | Active |
    And the total number of records in search results by "hr1" are 2
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (05) View all Customers, Filter by LegalName by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on LegalName which contains "Al Ahram" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code   | MarketName | LegalName | BusinessUnit | OldCustomerNumber | State  |
      | 000010 | Al Ahram   | Al Ahram  | 0001 - Flexo | 3                 | Active |
    And the total number of records in search results by "hr1" are 1
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  @Future
  Scenario: (06) View all Customers, Filter by Type by authorized user using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on Type which equals "LOCAL" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code   | MarketName            | LegalName             | BusinessUnit                    | OldCustomerNumber | State  |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0002 - Signmedia                | 4                 | Active |
      | 000010 | Al Ahram              | Al Ahram              | 0001 - Flexo                    | 3                 | Active |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | 0001 - Flexo                    | 2                 | Active |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "hr1" are 4
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (07) View all Customers, Filter by BusinessUnit by authorized user using "Contains" - SuperUser
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on BusinessUnit which contains "Flexo" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code   | MarketName      | LegalName       | BusinessUnit | OldCustomerNumber | State  |
      | 000010 | Al Ahram        | Al Ahram        | 0001 - Flexo | 3                 | Active |
      | 000009 | المطبعة الأمنية | المطبعة الأمنية | 0001 - Flexo | 2                 | Active |
    And the total number of records in search results by "hr1" are 2
  
  #EBS-4897
  #EBS-8270
  #EBS-8498
  Scenario: (08) View all Customers, Filter by State by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on State which contains "Active" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code   | MarketName            | LegalName             | BusinessUnit                    | OldCustomerNumber | State  |
      | 000011 | مطبعة أكتوبر الهندسية | مطبعة أكتوبر الهندسية | 0002 - Signmedia                | 4                 | Active |
      | 000010 | Al Ahram              | Al Ahram              | 0001 - Flexo                    | 3                 | Active |
      | 000009 | المطبعة الأمنية       | المطبعة الأمنية       | 0001 - Flexo                    | 2                 | Active |
      | 000008 | شركة الفؤاد التجارية  | شركة الفؤاد التجارية  | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "hr1" are 4
  
  #EBS-8498
  Scenario: (09) View all Customers, Filter by OldCustomerNumber by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Customers with filter applied on OldCustomerNumber which contains "1" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code   | MarketName           | LegalName            | BusinessUnit                    | OldCustomerNumber | State  |
      | 000008 | شركة الفؤاد التجارية | شركة الفؤاد التجارية | 0002 - Signmedia, 0003 - Offset | 1                 | Active |
    And the total number of records in search results by "hr1" are 1
  
  #EBS-4897
  Scenario: (10) View all Customers, by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Customers with no filter applied with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page