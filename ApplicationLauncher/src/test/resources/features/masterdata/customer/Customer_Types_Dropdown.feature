# Author Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud

Feature: View All Customer Types

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission       | Condition                      |
      | CustomerViewer_Signmedia | Customer:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | Customer:ReadAll |

    #EBS-1046
  Scenario: (01) Read list of Customers Types dropdown, by authorized user with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all CustomerTypes
    Then the following CustomerTypes values will be presented to "Shady.Abdelatif":
      | Type  |
      | LOCAL |
    And total number of CustomerTypes returned to "Shady.Abdelatif" is equal to 1

    #EBS-1046
  Scenario: (02) Read list of Customers Types dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all CustomerTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page