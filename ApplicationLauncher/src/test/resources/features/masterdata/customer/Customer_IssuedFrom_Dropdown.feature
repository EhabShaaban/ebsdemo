# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud

Feature: View All Customer IssuedFrom

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition |
      | CustomerViewer_Signmedia | Customer:ReadIssueFrom |           |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | Customer:ReadIssueFrom |
    And the following IssuedFrom exist:
      | Code | Name                    |
      | 0001 | جنوب القاهرة            |
      | 0002 | 6-Oct                   |
      | 0003 | القاهرة                 |
      | 0004 | شمال القاهرة            |
      | 0005 | الجيزة                  |
      | 0006 | 15-May                  |
      | 0007 | الأسكندرية              |
      | 0008 | المنصورة                |
      | 0009 | إستثمار القاهرة         |
      | 0010 | إستثمار أسيوط           |
      | 0011 | إستثمار الإسماعيلية     |
      | 0012 | إستثمار الأسكندرية      |
      | 0013 | إستثمار العاشر من رمضان |

 #EBS-8119
  Scenario: (01) Read list of IssuedFrom dropdown, by authorized user with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all IssuedFrom
    Then the following IssuedFrom values will be presented to "Shady.Abdelatif":
      | IssuedFrom                     |
      | 0013 - إستثمار العاشر من رمضان |
      | 0012 - إستثمار الأسكندرية      |
      | 0011 - إستثمار الإسماعيلية     |
      | 0010 - إستثمار أسيوط           |
      | 0009 - إستثمار القاهرة         |
      | 0008 - المنصورة                |
      | 0007 - الأسكندرية              |
      | 0006 - 15-May                  |
      | 0005 - الجيزة                  |
      | 0004 - شمال القاهرة            |
      | 0003 - القاهرة                 |
      | 0002 - 6-Oct                   |
      | 0001 - جنوب القاهرة            |
    And total number of IssuedFrom returned to "Shady.Abdelatif" is equal to 13

    #EBS-8119
  Scenario: (02) Read list of IssuedFrom dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all IssuedFrom
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page