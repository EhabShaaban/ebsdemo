# Author Dev: Eman Mansour
# Author Quality: Shirin Mahmoud

Feature: View All Customer TaxAdministration

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | CustomerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                     | Condition |
      | CustomerViewer_Signmedia | Customer:ReadTaxAdministration |           |
    And the following users doesn't have the following permissions:
      | User | Permission                     |
      | Afaf | Customer:ReadTaxAdministration |
    And the following TaxAdministration exist:
      | Code | Name         |
      | 0001 | مأمورية شمال |

    #EBS-8120
  Scenario: (01) Read list of TaxAdministration dropdown, by authorized user with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all TaxAdministration
    Then the following TaxAdministration values will be presented to "Shady.Abdelatif":
      | TaxAdministration   |
      | 0001 - مأمورية شمال |
    And total number of TaxAdministration returned to "Shady.Abdelatif" is equal to 1

    #EBS-8120
  Scenario: (02) Read list of TaxAdministration dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all TaxAdministration
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page