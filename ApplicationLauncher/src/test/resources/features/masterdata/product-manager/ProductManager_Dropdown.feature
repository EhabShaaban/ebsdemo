# Author Dev: Eman Mansour,Author Quality: Shirin Mahmoud

Feature: View All Product Managers

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole              |
      | PurchasingResponsible_Signmedia | ProductManagerViewer |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition |
      | ProductManagerViewer | ProductManager:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | ProductManager:ReadAll |
    And the following ProductManagers exist:
      | Code | Name          |
      | 0001 | Mohamed Nabil |
      | 0002 | Mahmoud Amr   |
      | 0003 | Moustafa      |
    And the total number of existing ProductManagers are 3

  Scenario: (01) Read list of ProductManagers dropdown by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all ProductManagers in a dropdown
    Then the following ProductManagers values will be presented to "Gehan.Ahmed" in a dropdown:
      | ProductManager       |
      | 0003 - Moustafa      |
      | 0002 - Mahmoud Amr   |
      | 0001 - Mohamed Nabil |
    And total number of ProductManagers returned to "Gehan.Ahmed" in a dropdown is equal to 3


  Scenario: (02) Read list of ProductManagers dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all ProductManagers in a dropdown
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page