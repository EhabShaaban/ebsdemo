# Author: Somaya Ahmed
# Reviewer: Hend Ahmed (20-Jan-2019)
# Updated by: Somaya Ahmed (11-Mar-2019)

Feature: Create IVR - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | IVROwner_Signmedia      |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia | CurrencyViewer          |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission              | Condition                      |
      | IVROwner_Signmedia      | ItemVendorRecord:Create |                                |
      | ItemOwner_Signmedia     | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia     | Item:ReadUoMData        | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia   | Vendor:ReadAll          | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer          | Currency:ReadAll        |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll  |                                |
    And the following Items exist:
      | Code   | Name       | PurchasingUnit |
      | 000008 | PRO-V-ST-2 | 0001, 0002     |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000008 | Vendor 2 | 0002, 0001     |
    And the Item with code "000008" has the following Base UOM:
      | Code | Symbol |
      | 0019 | M2     |
    And the Item with code "000008" has the following Alternate UoMs:
      | Code | Symbol       |
      | 0029 | Roll 2.20x50 |
      | 0032 | Roll 3.20x50 |
      | 0034 | Roll 1.07x50 |
    And the following Currencies exist:
      | Code | Name           |
      | 0001 | Egyptian Pound |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

  ######## Create Happy Paths
  Scenario Outline: (01) Create IVR with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And Last created ItemVendorRecord was with code "000050"
    When "Gehan.Ahmed" creates IVR with the following values at "01-Jan-2019 11:00 AM":
      | ItemCode | VendorCode | PurchasingUnit            | UoMCode    | ItemCodeAtVendor        | OldItemReference | Price        | CurrencyCode        |
      | 000008   | 000008     | <PurchasingUnitCodeValue> | <UOMValue> | <ItemCodeAtVendorValue> | 8910111213141    | <PriceValue> | <CurrencyCodeValue> |
    Then a new IVR is created with the following values:
      | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | IVRCode | ItemCode | VendorCode | PurchasingUnit            | UoMCode    | ItemCodeAtVendor        | OldItemReference | Price        | Currency            |
      | Gehan.Ahmed | Gehan.Ahmed   | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051  | 000008   | 000008     | <PurchasingUnitCodeValue> | <UOMValue> | <ItemCodeAtVendorValue> | 8910111213141    | <PriceValue> | <CurrencyCodeValue> |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"
    Examples:
      | PurchasingUnitCodeValue | ItemCodeAtVendorValue | PriceValue | CurrencyCodeValue | UOMValue |
      | 0002                    | ABC123-0001           | 11.0       | 0001              | 0019     |
      | 0001                    | ABC123-0001           | 11.0       | 0001              | 0019     |
      | 0001                    | ABC123-0001           |            |                   | 0019     |
      | 0001                    | ABC123-0001           |            | 0001              | 0019     |
      | 0001                    | ABC123-0001           |            | 0001              | 0019     |


