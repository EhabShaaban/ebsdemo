# Author: Hend Ahmed, Nanacy Shoukry, Shrouk Alaa
# Reviewer: Somaya Ahmed (26-Mar-2019)

# TODO: Allowed actions shall return authorized reads of drop-down filters

Feature: Read Allowed Actions for IVR Object Screen

  Background:
    Given the following users and roles exist:
      | Name               | Role                                                    |
      # One Role --> One Subrole without condition
      | IVROwnerUser       | PurchasingResponsible                                   |
      # One Role --> One Subrole with condition
      | Gehan.Ahmed        | PurchasingResponsible_Signmedia                         |
      | Gehan.Ahmed        | LogisticsResponsible_Signmedia                          |
      | TestUser1          | PurchasingResponsible_Signmedia                         |
      | TestUser1          | PurchasingResponsible                                   |
      # Two Roles --> Each with one subrole one with condition and the other without - differet subroles
      | TestUser3          | PurchasingResponsible_Signmedia                         |
      | TestUser3          | LogisticsResponsible                                    |
      # One Role --> Two Subroles with condition - Similar Subroles
      | TestUser4          | PurchasingResponsible_Signmedia_Flexo                   |
      # One Role --> Two Subroles with condition - Similar Subroles
      | TestUser4          | PurchasingResponsible_Signmedia_Flexo                   |
      # One Role --> Two Subroles one with condition and the other without - Similar Subroles
      | TestUser5          | PurchasingResponsible_ALL_Flexo                         |
      # One Role --> Two Subroles - Different Subroles with no conditions
      | TestUser6          | PurchasingResponsible_And_LogisticResponsible           |
      # One Role --> Two Subroles - Different Subroles with consitions
      | TestUser7          | PurchasingResponsible_Signmedia_And_LogisticResponsible |
      #User who is authorized to read only some sections
      | CannotReadSections | CannotReadSectionsRole                                  |
      # Two Roles --> similar subroles
      | Amr.Khalil         | PurchasingResponsible_Flexo                             |
      | Amr.Khalil         | PurchasingResponsible_Corrugated                        |
      # user authorized to create only
      | CreateOnlyUser     | CreateOnlyRole                                          |
      # Not authorized
      | Afaf               | FrontDesk                                               |
      #SuperUser
      | hr1                | SuperUser                                               |
      # one role- one subrole- without condition
      | Ashraf.Fathi       | M.D                                                     |
    And the following roles and sub-roles exist:
      | Role                                                    | Subrole                   |
      | PurchasingResponsible                                   | IVROwner                  |
      | PurchasingResponsible_Signmedia                         | IVROwner_Signmedia        |
      | PurchasingResponsible_Flexo                             | IVROwner_Flexo            |
      | PurchasingResponsible_Corrugated                        | IVROwner_Corrugated       |
      | CreateOnlyRole                                          | CreateOnlySubRole         |
      | SuperUser                                               | SuperUserSubRole          |
      | M.D                                                     | IVRViewer                 |
      | LogisticsResponsible                                    | IVRViewer                 |
      | LogisticsResponsible_Signmedia                          | IVRViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_Flexo                   | IVROwner_Signmedia        |
      | PurchasingResponsible_Signmedia_Flexo                   | IVROwner_Flexo            |
      | PurchasingResponsible_ALL_Flexo                         | IVROwner                  |
      | PurchasingResponsible_ALL_Flexo                         | IVROwner_Flexo            |
      | PurchasingResponsible_And_LogisticResponsible           | IVROwner                  |
      | PurchasingResponsible_And_LogisticResponsible           | IVRViewer                 |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | IVROwner_Signmedia        |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | IVRViewer                 |
      | CannotReadSectionsRole                                  | CannotReadSectionsSubRole |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                       |
      | IVROwner                  | ItemVendorRecord:ReadAll           |                                 |
      | IVROwner                  | ItemVendorRecord:Create            |                                 |
      | IVROwner                  | ItemVendorRecord:Delete            |                                 |
      | IVROwner                  | ItemVendorRecord:ReadGeneralData   |                                 |
      | IVROwner                  | ItemVendorRecord:UpdateGeneralData |                                 |
      | IVROwner_Signmedia        | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Signmedia']  |
      | IVROwner_Signmedia        | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | IVROwner_Signmedia        | ItemVendorRecord:Delete            | [purchaseUnitName='Signmedia']  |
      | IVROwner_Signmedia        | ItemVendorRecord:Create            |                                 |
      | IVROwner_Flexo            | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Flexo']      |
      | IVROwner_Flexo            | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | IVROwner_Flexo            | ItemVendorRecord:Delete            | [purchaseUnitName='Flexo']      |
      | IVROwner_Flexo            | ItemVendorRecord:Create            |                                 |
      | IVROwner_Flexo            | ItemVendorRecord:ReadAll           | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated       | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Corrugated'] |
      | IVROwner_Corrugated       | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | IVROwner_Corrugated       | ItemVendorRecord:Delete            | [purchaseUnitName='Corrugated'] |
      | IVROwner_Corrugated       | ItemVendorRecord:Create            |                                 |
      | IVROwner_Corrugated       | ItemVendorRecord:ReadAll           | [purchaseUnitName='Corrugated'] |
      | CreateOnlySubRole         | ItemVendorRecord:Create            |                                 |
      | SuperUserSubRole          | *:*                                |                                 |
      | IVRViewer                 | ItemVendorRecord:ReadAll           |                                 |
      | IVRViewer                 | ItemVendorRecord:ReadGeneralData   |                                 |
      | CannotReadSectionsSubRole | ItemVendorRecord:Delete            |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                         | Condition                       |
      | Amr.Khalil  | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | ItemVendorRecord:Delete            | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | ItemVendorRecord:Delete            | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | ItemVendorRecord:Delete            | [purchaseUnitName='Corrugated'] |
      | TestUser4   | ItemVendorRecord:ReadGeneralData   | [purchaseUnitName='Corrugated'] |
      | TestUser4   | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | TestUser4   | ItemVendorRecord:Delete            | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User               | Permission                         |
      | CannotReadSections | ItemVendorRecord:ReadGeneralData   |
      | CannotReadSections | ItemVendorRecord:UpdateGeneralData |
      | CannotReadSections | ItemVendorRecord:Create            |
      | CannotReadSections | ItemVendorRecord:ReadAll           |
      | CreateOnlyUser     | ItemVendorRecord:ReadAll           |
      | CreateOnlyUser     | ItemVendorRecord:ReadGeneralData   |
      | CreateOnlyUser     | ItemVendorRecord:UpdateGeneralData |
      | CreateOnlyUser     | ItemVendorRecord:Delete            |
      | Afaf               | ItemVendorRecord:ReadGeneralData   |
      | Afaf               | ItemVendorRecord:UpdateGeneralData |
      | Afaf               | ItemVendorRecord:Delete            |
      | Afaf               | ItemVendorRecord:Create            |
      | Afaf               | ItemVendorRecord:ReadAll           |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | Price   | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 50.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000008  | 000006   | 000001     | 0035    | R6000            | 200.50  | 0001         | 0001             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 100.00  | 0003         | 0006             | 02-Aug-2018 10:30 AM |
      | 000014  | 000002   | 000002     | 0033    | GDF730-ddd       | 11500.8 | 0003         | 0002             | 02-Aug-2018 10:30 AM |

  # EBS-3827
  Scenario Outline: (01) ViewAll IVRs - Users with different IVR authorizations for Object Screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of IVR with IVRCode "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User               | Code   | AllowedActions                                      |
      | IVROwnerUser       | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | IVROwnerUser       | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | IVROwnerUser       | 000011 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser1          | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser1          | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser1          | 000011 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser5          | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser5          | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser5          | 000011 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser6          | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser6          | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser6          | 000011 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | Gehan.Ahmed        | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | Amr.Khalil         | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | Amr.Khalil         | 000011 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser3          | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser3          | 000008 | ReadGeneralData, ReadAll                            |
      | TestUser3          | 000011 | ReadGeneralData, ReadAll                            |
      | TestUser7          | 000001 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | TestUser7          | 000008 | ReadGeneralData, ReadAll                            |
      | TestUser7          | 000011 | ReadGeneralData, ReadAll                            |
      | TestUser4          | 000001 | ReadGeneralData, UpdateGeneralData, Delete          |
      | TestUser4          | 000008 | ReadGeneralData, UpdateGeneralData, Delete, ReadAll |
      | CannotReadSections | 000001 | Delete                                              |

  # EBS-3827
  Scenario Outline: (02) ViewAll IVRs - Unauthorized user to any action in Object Screen - Due to condition or not
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of IVR with IVRCode "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User        | Code   |
      | Gehan.Ahmed | 000008 |
      | Gehan.Ahmed | 000011 |
      | Amr.Khalil  | 000001 |
      | TestUser4   | 000011 |
      | Afaf        | 000008 |

  # EBS-3827
  Scenario: (03) ViewAll IVRs - user requests to read actions of IVR that not exist
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the IVR with code "000014" successfully
    When "Gehan.Ahmed" requests to read actions of IVR with IVRCode "000014"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-3827
  Scenario Outline: (04) ViewAll IVRs - Users with different IVR authorizations for Home Screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of IVR home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User           | AllowedActions |
      | IVROwnerUser   | ReadAll,Create |
      | Amr.Khalil     | ReadAll,Create |
      | Gehan.Ahmed    | ReadAll,Create |
      | Ashraf.Fathi   | ReadAll        |
      | hr1            | ReadAll,Create |
      | CreateOnlyUser | Create         |

  # EBS-3827
  Scenario Outline: (05) ViewAll IVRs - Unauthorized user to any action in Home Screen - Due to condition or not
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of IVR home screen
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User |
      | Afaf |
