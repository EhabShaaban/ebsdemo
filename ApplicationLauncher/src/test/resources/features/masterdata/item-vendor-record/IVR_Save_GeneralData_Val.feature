# Author: Zyad Ghorab (08-April-2019)

Feature: Save IVR GeneralData - Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Sub-role           |
      | PurchasingResponsible_Signmedia | IVROwner_Signmedia |
      | PurchasingResponsible_Signmedia | CurrencyViewer     |
      | SuperUser                       | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                         | Condition                      |
      | IVROwner_Signmedia | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer     | Currency:ReadAll                   |                                |
      | SuperUserSubRole   | *:*                                |                                |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00 | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000054  | 000001   | 000002     | 0043    | GDF530-440       | 12345678910      | 50.00 | 0001         | 0002             | 02-Aug-2018 10:30 AM |
    And the following Currencies exist:
      | Code | Name                 |
      | 0002 | United States Dollar |
    And edit session is "30" minutes

  ###### Save GeneralData section with Incorrect Data (Validation Failure)  ##############################################################

  #EBS-2473
  Scenario: (01) IVR Save GeneralData Section with Incorrect data: Currency doesn't exist for IVRs (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of IVR with code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCodeAtVendor | Price | CurrencyCode |
      | GDF530-440-1     | 12.0  | 9999         |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "Currency" field "IVR-msg-03" and sent to "Gehan.Ahmed"

  ###### Save GeneralData section with After session expires (Exception)  ##############################################################

  #EBS-2473
  Scenario: (02) IVR Save GeneralData Section after edit session expired for IVRs (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of IVR with code "000001" at "07-Jan-2019 09:41 AM" with the following values:
      | ItemCodeAtVendor | Price | CurrencyCode |
      | GDF530-440-1     | 12.0  | 0002         |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-2473
  Scenario: (03) IVR Save GeneralData Section after edit session expired for IVRs & IVR is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of IVR with code "000054" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the IVR with code "000054" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" saves GeneralData section of IVR with code "000054" at "07-Jan-2019 09:45 AM" with the following values:
      | ItemCodeAtVendor | Price | CurrencyCode |
      | GDF530-440-1     | 12.0  | 0002         |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

   #EBS-2473
  Scenario Outline: (04) IVR Save GeneralData Section with Missing mandatory Fields for IVRs  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of IVR with code "000001" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCodeAtVendor        | Price        | CurrencyCode        |
      | <ItemCodeAtVendorValue> | <PriceValue> | <CurrencyCodeValue> |
    Then the lock by "Gehan.Ahmed" on GeneralData section of IVR with code "000001" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeAtVendorValue | PriceValue | CurrencyCodeValue |
      |                       | 10         | 0002              |
      | GDF530-440-3          | 10         |                   |

  ######## Save IVR  GeneralData section with Malicious input (Abuse Cases)

   #EBS-2473
  Scenario Outline: (05) IVR Save GeneralData Section with malicous input that violates business rules enforced by the client (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves GeneralData section of IVR with code "000001" with the following values:
      | ItemCodeAtVendor        | Price        | CurrencyCode        |
      | <ItemCodeAtVendorValue> | <PriceValue> | <CurrencyCodeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeAtVendorValue                                                                                 | PriceValue        | CurrencyCodeValue |
      | ABC123-0001                                                                                           | 11.0              | Ay7aga            |
      | ABC123-0001                                                                                           | 11.0              | ""                |
      | Test@                                                                                                 | 11.0              | 0001              |
      | Test#                                                                                                 | 11.0              | 0001              |
      | Test$                                                                                                 | 11.0              | 0001              |
      | Test^                                                                                                 | 11.0              | 0001              |
      | Test&                                                                                                 | 11.0              | 0001              |
      | Test~                                                                                                 | 11.0              | 0001              |
      | Test+                                                                                                 | 11.0              | 0001              |
      | Test<                                                                                                 | 11.0              | 0001              |
      | Test>                                                                                                 | 11.0              | 0001              |
      | "Test"                                                                                                | 11.0              | 0001              |
      | 'TEst'                                                                                                | 11.0              | 0001              |
      | [Test]                                                                                                | 11.0              | 0001              |
      | {Test}                                                                                                | 11.0              | 0001              |
      | Test?                                                                                                 | 11.0              | 0001              |
      | Test=                                                                                                 | 11.0              | 0001              |
      | \|Test                                                                                                | 11.0              | 0001              |
      | \|\|Test                                                                                              | 11.0              | 0001              |
      | \Test                                                                                                 | 11.0              | 0001              |
      | Test!                                                                                                 | 11.0              | 0001              |
      | Test:                                                                                                 | 11.0              | 0001              |
      | Test;                                                                                                 | 11.0              | 0001              |
      | Test¦                                                                                                 | 11.0              | 0001              |
      | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestt | 11.0              | 0001              |
      | ""                                                                                                    | 11.0              | 0001              |
      | ABC123-0001                                                                                           | -1.0              | 0001              |
      | ABC123-0001                                                                                           | 10000000000.00    | 0001              |
      | ABC123-0001                                                                                           | 10,000,000,000.00 | 0001              |
      | ABC123-0001                                                                                           | ay7aga            | 0001              |
      | ABC123-0001                                                                                           | ""                | 0001              |
