Feature: Read Next and Previous in Item Vendor Record

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
      | hr1          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | M.D                              | IVRViewer           |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia  |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo      |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                       |
      | IVRViewer           | ItemVendorRecord:ReadAll |                                 |
      | IVROwner_Flexo      | ItemVendorRecord:ReadAll | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated | ItemVendorRecord:ReadAll | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                      |                                 |
    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | ItemVendorRecord:ReadAll |
    And the following IVRs with the following order exist:
      | IVRCode | PurchaseUnitCode |
      | 000001  | 0002             |
      | 000002  | 0002             |
      | 000003  | 0002             |
      | 000004  | 0002             |
      | 000005  | 0002             |
      | 000006  | 0002             |
      | 000007  | 0002             |
      | 000008  | 0001             |
      | 000009  | 0002             |
      | 000010  | 0005             |
      | 000011  | 0006             |
      | 000012  | 0001             |
      | 000013  | 0001             |

  Scenario Outline: (01) IVR Read Next and Previous - Read Next by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view next IVR of current IVR with code "<IVRCode>"
    Then the next IVR code "<NextValue>" is displayed to "<User>"
    Examples:
      | User         | IVRCode | NextValue |
      | Gehan.Ahmed  | 000001  | 000002    |
      | Gehan.Ahmed  | 000007  | 000009    |
      | Amr.Khalil   | 000008  | 000011    |
      | Amr.Khalil   | 000011  | 000012    |
      | Ashraf.Fathi | 000008  | 000009    |
      | Ashraf.Fathi | 000009  | 000010    |
      | Ashraf.Fathi | 000010  | 000011    |
      | Ashraf.Fathi | 000011  | 000012    |

  Scenario Outline: (02) IVR Read Next and Previous - Read Previous by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view previous IVR of current IVR with code "<IVRCode>"
    Then the previous IVR code "<PrevValue>" is displayed to "<User>"
    Examples:
      | User         | IVRCode | PrevValue |
      | Gehan.Ahmed  | 000009  | 000007    |
      | Gehan.Ahmed  | 000007  | 000006    |
      | Gehan.Ahmed  | 000006  | 000005    |
      | Amr.Khalil   | 000013  | 000012    |
      | Amr.Khalil   | 000012  | 000011    |
      | Amr.Khalil   | 000011  | 000008    |
      | Ashraf.Fathi | 000012  | 000011    |
      | Ashraf.Fathi | 000011  | 000010    |
      | Ashraf.Fathi | 000010  | 000009    |
      | Ashraf.Fathi | 000009  | 000008    |
      | hr1          | 000011  | 000010    |

  Scenario: (03) IVR Read Next and Previous - Navigate unexisted next record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Amr.Khalil"
    And "Amr.Khalil" first deleted the IVR with code "000013" successfully
    When "hr1" requests to view next IVR of current IVR with code "000012"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  Scenario: (04) IVR Read Next and Previous - Navigate unexisted Previous record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the IVR with code "000001" successfully
    When "hr1" requests to view previous IVR of current IVR with code "000002"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  Scenario: (05) IVR Read Next and Previous - Navigate unexisted next record
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the IVR with code "000007" successfully
    When "hr1" requests to view next IVR of current IVR with code "000006"
    Then the next IVR code "000008" is displayed to "hr1"

  Scenario: (06) IVR Read Next and Previous - Navigate unexisted Previous record
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the IVR with code "000002" successfully
    When "hr1" requests to view previous IVR of current IVR with code "000003"
    Then the previous IVR code "000001" is displayed to "hr1"

  Scenario: (07) IVR Read Next and Previous - Read Next by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view next IVR of current IVR with code "000005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (08) IVR Read Next and Previous - Read Previous by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view previous IVR of current IVR with code "000005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page