# Author: Zyad Ghorab (25-Mar-2019)
# Reviewer: Hend Ahmed and Somaya Ahmed (26-Mar-2019)

Feature: View IVR GeneralData

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia  |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo      |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                       |
      | IVROwner_Flexo      | ItemVendorRecord:ReadAll | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated | ItemVendorRecord:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following users have the following permissions without the following conditions:
      | User       | Permission               | Condition                      |
      | Amr.Khalil | ItemVendorRecord:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | ItemVendorRecord:ReadAll |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 34567891011      | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000004  | 000004   | 000002     | 0039    | GDH670-888       | 4567891011       | 50.00    | 0002         | 0002             | 02-Aug-2018 10:30 AM |
      | 000008  | 000006   | 000001     | 0035    | R6000            | 89101112         | 200.50   | 0001         | 0001             | 02-Aug-2018 10:30 AM |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000006  | 000002   | 000002     | 0032    | GDF730-440       | 67891011         | 600.40   | 0004         | 0002             | 02-Aug-2018 10:30 AM |
      | 000007  | 000003   | 000002     | 0037    | GDF830-440       | 78910111         | 50.30    | 0005         | 0002             | 02-Aug-2018 10:30 AM |
      | 000009  | 000004   | 000002     | 0038    | GDF530-200       | 9101112          | 100.30   | 0006         | 0002             | 02-Aug-2018 10:30 AM |
      | 000010  | 000060   | 000051     | 0029    | GDF530-201       | 1011121314       | 50.00    | 0003         | 0005             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 11121314         | 100.00   | 0003         | 0006             | 02-Aug-2018 10:30 AM |
      | 000012  | 000060   | 000001     | 0029    | GDF530-203       | 12131415         | 70.00    | 0005         | 0001             | 02-Aug-2018 10:30 AM |
      | 000013  | 000005   | 000051     | 0034    | GDF530-205       | 13141516         | 11500.80 | 0003         | 0001             | 02-Aug-2018 10:30 AM |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit                     |
      | 000060 | Hot Laminated Frontlit Fabric roll 1                    | 0001, 0002, 0003, 0004, 0005, 0006 |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002                               |
      | 000002 | Hot Laminated Frontlit Backlit roll                     | 0002                               |
      | 000003 | Flex Primer Varnish E33                                 | 0002                               |
      | 000004 | Flex cold foil adhesive E01                             | 0002                               |
      | 000005 | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 0001                               |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001                               |
    And the following Vendors exist:
      | Code   | Name              | PurchasingUnit                     |
      | 000051 | Vendor 6          | 0001, 0002, 0003, 0004, 0005, 0006 |
      | 000001 | Siegwerk          | 0001                               |
      | 000002 | Zhejiang          | 0002                               |
      | 000003 | ABC               | 0003                               |
      | 000004 | Vendor Digital    | 0004                               |
      | 000005 | Vendor Textile    | 0005                               |
      | 000006 | Vendor Corrugated | 0006                               |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
      | 0003 | Euro                 |
      | 0004 | Chinese Yuan         |
      | 0005 | Japanese Yen         |
      | 0006 | Hong Kong Dollar     |
    And the following Measures exist:
      | Code | Type     | Symbol        | Name          |
      | 0029 | Business | Roll 2.20x50  | Roll 2.20x50  |
      | 0032 | Business | Roll 3.20x50  | Roll 3.20x50  |
      | 0034 | Business | Roll 1.07x50  | Roll 1.07x50  |
      | 0035 | Business | Roll 1.27x50  | Roll 1.27x50  |
      | 0036 | Business | Drum-10Kg     | Drum-10Kg     |
      | 0037 | Business | Drum-50Kg     | Drum-50Kg     |
      | 0038 | Business | Drum-100Kg    | Drum-100Kg    |
      | 0039 | Business | Drum-70Kg     | Drum-70Kg     |
      | 0042 | Business | Bottle-2Liter | Bottle-2Liter |

  #### Happy Path
  # Reviewed
  # EBS-3826
  Scenario Outline: (01) IVR View GeneralData Section by an authorized user who has one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view GeneralData section of IVR with code "<IVRCode>"
    Then the following values of GeneralData section of IVR with code "<IVRCode>" are displayed to "<User>":
      | CreationDate   | IVRCode   | ItemCode   | ItemName   | VendorCode   | VendorName   | UOMSymbol   | ItemCodeAtVendor   | Price   | Currency   | OldItemReference   | PurchaseUnitName   |
      | <CreationDate> | <IVRCode> | <ItemCode> | <ItemName> | <VendorCode> | <VendorName> | <UOMSymbol> | <ItemCodeAtVendor> | <Price> | <Currency> | <OldItemReference> | <PurchaseUnitName> |
    Examples:
      | User        | CreationDate         | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | Price  | Currency       | OldItemReference | PurchaseUnitName |
      | Gehan.Ahmed | 02-Aug-2018 10:30 AM | 000002  | 000002   | Hot Laminated Frontlit Backlit roll                     | 000002     | Zhejiang          | Roll 2.20x50 | GDB550-610       | 100.00 | Egyptian Pound | 234567891011     | Signmedia        |
      | Amr.Khalil  | 02-Aug-2018 10:30 AM | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk          | Roll 1.27x50 | R6000            | 200.50 | Egyptian Pound | 89101112         | Flexo            |
      | Amr.Khalil  | 02-Aug-2018 10:30 AM | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 100.00 | Euro           | 11121314         | Corrugated       |

  # EBS-1638
  Scenario: (02) IVR View GeneralData Section where IVR doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the IVR with code "000001" successfully
    When "Gehan.Ahmed" requests to view GeneralData section of IVR with code "000001"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline: (03) IVR View GeneralData Section by an unauthorized user due to condition or not (Unauthorized/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view GeneralData section of IVR with code "<IVRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User       | IVRCode |
      | Afaf       | 000001  |
      | Amr.Khalil | 000002  |
