# Author: Aya Sadek (20-Jan-2019)
# Reviewer: Somaya Ahmed (11-Mar-2019)

Feature: Create IVR -Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | IVROwner_Signmedia      |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia     |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | CurrencyViewer          |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission              | Condition                      |
      | IVROwner_Signmedia      | ItemVendorRecord:Create |                                |
      | ItemOwner_Signmedia     | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia     | Item:ReadUoMData        | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia   | Vendor:ReadAll          | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer          | Currency:ReadAll        |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll  |                                |
    And the following Items exist:
      | Code   | Name                               | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll | 0002           |
      | 000008 | PRO-V-ST-2                         | 0001, 0002     |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000008 | Vendor 2 | 0002, 0001     |
      | 000002 | Zhejiang | 0002           |
    And the Item with code "000008" has the following Alternate UoMs:
      | Code | Symbol       |
      | 0029 | Roll 2.20x50 |
      | 0032 | Roll 3.20x50 |
      | 0034 | Roll 1.07x50 |
    And the Item with code "000001" has the following Alternate UoMs:
      | Code | Symbol       |
      | 0029 | Roll 2.20x50 |
      | 0032 | Roll 3.20x50 |
    And the following Currencies exist:
      | Code | Name           |
      | 0001 | Egyptian Pound |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | Price | CurrencyCode | PurchaseUnitCode | OldItemReference | ID | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 50.00 | 0001         | 0002             | 1234567891011    | 1  | 02-Aug-2018 10:30 AM |


  ######## Create with missing mandatory fields (Abuse cases)
  Scenario Outline: (01) Create IVR with missing mandatory field: ItemCode/VendroCode (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates IVR with the following values:
      | ItemCode        | VendorCode        | PurchasingUnit            | UoMCode        | ItemCodeAtVendor        | OldItemReference        | Price        | CurrencyCode        |
      | <ItemCodeValue> | <VendorCodeValue> | <PurchasingUnitCodeValue> | <UOMCodeValue> | <ItemCodeAtVendorValue> | <OldItemReferenceValue> | <PriceValue> | <CurrencyCodeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeValue | VendorCodeValue | PurchasingUnitCodeValue | UOMCodeValue | ItemCodeAtVendorValue | OldItemReferenceValue | PriceValue | CurrencyCodeValue |
      |               | 000008          | 0001                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | 000008        |                 | 0001                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | 000008        | 000008          |                         | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | 000008        | 000008          | 0001                    |              | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       |                   |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001           |                       | 11.0       | 0001              |
      | 000008        | 000008          | 0001                    | 0019         |                       | 8910111213141         | 11.0       | 0001              |

  ######## Create with incorrect input (Validation Failure)
  Scenario Outline: (02) Create IVR with incorrect input: input field doesn't exist (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates IVR with the following values:
      | ItemCode        | VendorCode        | PurchasingUnit            | UoMCode        | ItemCodeAtVendor        | OldItemReference        | Price        | CurrencyCode        |
      | <ItemCodeValue> | <VendorCodeValue> | <PurchasingUnitCodeValue> | <UOMCodeValue> | <ItemCodeAtVendorValue> | <OldItemReferenceValue> | <PriceValue> | <CurrencyCodeValue> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<FieldwithIncorrectInput>" field "<ErrorMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | FieldwithIncorrectInput | ErrorMsg   | ItemCodeValue | VendorCodeValue | PurchasingUnitCodeValue | UOMCodeValue | ItemCodeAtVendorValue | OldItemReferenceValue | PriceValue | CurrencyCodeValue |
      | ItemCode                | IVR-msg-01 | 999999        | 000008          | 0001                    | 0019         | ABC123-0001           | 9101112131415         | 11.0       | 0001              |
      | VendorCode              | IVR-msg-02 | 000008        | 999999          | 0001                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | PurchasingUnit          | IVR-msg-04 | 000008        | 000008          | 9999                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | UOMCode                 | IVR-msg-06 | 000008        | 000008          | 0001                    | 9999         | ABC123-0001           | 8910111213141         | 11.0       | 0001              |
      | CurrencyCode            | IVR-msg-03 | 000008        | 000008          | 0001                    | 0019         | ABC123-0001           | 8910111213141         | 11.0       | 9999              |
      | OldItemNumber           | IVR-msg-08 | 000008        | 000008          | 0001                    | 0019         | ABC123-0001           | 12131415              | 11.0       | 0001              |

  Scenario: (03) Create IVR with incorrect input: There is already an existing IVR record with same Item, Vendor, PurchasingUnit and UOM (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates IVR with the following values:
      | ItemCode | VendorCode | PurchasingUnit | UoMCode | ItemCodeAtVendor | OldItemReference | Price | CurrencyCode |
      | 000001   | 000002     | 0002           | 0029    | IDV34            | 1234567891011    | 11.0  | 0001         |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message "IVR-msg-05" is returned and sent to "Gehan.Ahmed"

  Scenario Outline: (04) Create IVR with incorrect input: PurchasingUnit is not common in Item & Vendor (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates IVR with the following values:
      | ItemCode        | VendorCode        | PurchasingUnit            | UoMCode        | ItemCodeAtVendor | OldItemReference | Price | CurrencyCode |
      | <ItemCodeValue> | <VendorCodeValue> | <PurchasingUnitCodeValue> | <UOMCodeValue> | ABC123-0001      | 6235628288278    | 11.0  | 0001         |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "PurchasingUnit" field "IVR-msg-04" and sent to "Gehan.Ahmed"

    Examples:
      | ItemCodeValue | VendorCodeValue | PurchasingUnitCodeValue | UOMCodeValue |
      | 000008        | 000008          | 0003                    | 0019         |
      | 000008        | 000002          | 0001                    | 0019         |
      | 000001        | 000008          | 0001                    | 0029         |


  ######## Create with Malicious input (Abuse Cases)
  Scenario Outline: (05) Create IVR with malicous input that violates business rules enforced by the client (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates IVR with the following values:
      | ItemCode        | VendorCode        | PurchasingUnit            | UoMCode        | ItemCodeAtVendor        | OldItemReference        | Price        | CurrencyCode        |
      | <ItemCodeValue> | <VendorCodeValue> | <PurchasingUnitCodeValue> | <UOMCodeValue> | <ItemCodeAtVendorValue> | <OldItemReferenceValue> | <PriceValue> | <CurrencyCodeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeValue | VendorCodeValue | PurchasingUnitCodeValue | UOMCodeValue | ItemCodeAtVendorValue                                                                                 | OldItemReferenceValue | PriceValue        | CurrencyCodeValue |
      | ay7aga        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | ""            | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | Ay7aga          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | ""              | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | Ay7aga                  | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | ""                      | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | Ay7aga       | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | ""           | ABC123-0001                                                                                           | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | Ay7aga            |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 11.0              | ""                |
      | 000008        | 000008          | 0001                    | 0019         | Test@                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test#                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test$                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test^                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test&                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test~                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test+                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test<                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test>                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | "Test"                                                                                                | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | 'TEst'                                                                                                | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | [Test]                                                                                                | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | {Test}                                                                                                | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test?                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test=                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | \|Test                                                                                                | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | \|\|Test                                                                                              | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | \Test                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test!                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test:                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test;                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | Test¦                                                                                                 | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestt | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ""                                                                                                    | 8910111213141         | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | -1.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | 10,000,000,000.00 | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | ay7aga            | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 8910111213141         | ""                | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | ay7aga                | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123@                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123#                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123$                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123^                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123&                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123*                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123~                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123+                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123<                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123>                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | "123"                 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | '123'                 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | [123]                 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | {123}                 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123?                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123=                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | \|123                 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | \|\|123               | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | \123                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123!                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123:                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123;                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123¦                  | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | -1                    | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 123458839210923456781 | 11.0              | 0001              |
      | 000008        | 000008          | 0001                    | 0019         | ABC123-0001                                                                                           | 0                     | 11.0              | 0001              |


