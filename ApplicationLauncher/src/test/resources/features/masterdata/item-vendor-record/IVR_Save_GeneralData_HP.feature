# Author: Mohamed Adel

Feature: Save IVR GeneralData - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia  |
      | PurchasingResponsible_Signmedia  | CurrencyViewer      |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo      |
      | PurchasingResponsible_Flexo      | CurrencyViewer      |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated |
      | PurchasingResponsible_Corrugated | CurrencyViewer      |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                         | Condition                       |
      | IVROwner_Signmedia  | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | IVROwner_Flexo      | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | CurrencyViewer      | Currency:ReadAll                   |                                 |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price  | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00  | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 11121314         | 100.00 | 0003         | 0006             | 02-Aug-2018 10:30 AM |
      | 000012  | 000060   | 000001     | 0029    | GDF530-203       | 12131415         | 70.00  | 0005         | 0001             | 02-Aug-2018 10:30 AM |

  #### Happy Path EBS:2025
  Scenario Outline: (01) IVR Save GeneralData Section within edit session by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of IVR with code "<IVRCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" saves GeneralData section of IVR with code "<IVRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCodeAtVendor        | Price        | CurrencyCode    |
      | <ItemCodeAtVendorValue> | <PriceValue> | <CurrencyValue> |
    Then IVR with code "<IVRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | ItemCode        | VendorCode        | UOMCode        | ItemCodeAtVendor        | Price        | OldItemReference        | CurrencyCode    | PurchaseUnitCode        | CreationDate        |
      | <User>        | 07-Jan-2019 09:30 AM | <ItemCodeValue> | <VendorCodeValue> | <UOMCodeValue> | <ItemCodeAtVendorValue> | <PriceValue> | <OldItemReferenceValue> | <CurrencyValue> | <PurchaseUnitCodeValue> | <CreationDateValue> |
    And the lock by "<User>" on GeneralData section of IVR with code "<IVRCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | User        | IVRCode | ItemCodeValue | VendorCodeValue | UOMCodeValue | ItemCodeAtVendorValue | OldItemReferenceValue | PriceValue | CurrencyValue | PurchaseUnitCodeValue | CreationDateValue    |
      | Gehan.Ahmed | 000001  | 000001        | 000002          | 0029         | GDF530-440-1          | 12345678910           | 12.0       | 0002          | 0002                  | 02-Aug-2018 10:30 AM |
      | Gehan.Ahmed | 000001  | 000001        | 000002          | 0029         | GDF530-440-1          | 12345678910           |            | 0002          | 0002                  | 02-Aug-2018 10:30 AM |
      | Gehan.Ahmed | 000001  | 000001        | 000002          | 0029         | GDF530-440-1          | 12345678910           |            |               | 0002                  | 02-Aug-2018 10:30 AM |
      | Amr.Khalil  | 000011  | 000060        | 000006          | 0029         | GDF530-202-1          | 11121314              | 110.00     | 0002          | 0006                  | 02-Aug-2018 10:30 AM |
      | Amr.Khalil  | 000012  | 000060        | 000001          | 0029         | GDF530-203-2          | 12131415              | 80.00      | 0002          | 0001                  | 02-Aug-2018 10:30 AM |
