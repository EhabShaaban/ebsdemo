# Author: Aya Sadek (20-Jan-2019)
# Reviewer: Somaya Ahmed (11-Mar-2019)
Feature: Create IVR - Authorization

  Background:
    Given the following users and roles exist:
      | Name                        | Role                                                   |
      | Mahmoud.Abdelaziz           | Storekeeper_Signmedia                                  |
      | Gehan.Ahmed.NoItems         | PurchasingResponsible_Signmedia_CannotReadItemsRole    |
      | Gehan.Ahmed.NoVendor        | PurchasingResponsible_Signmedia_CannotReadVendor       |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  |
      | Gehan.Ahmed.NoCurrency      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole |
      # New Role
      | Gehan.Ahmed.NoItemUoM       | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole                        |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | VendorOwner_Signmedia          |
      # new subrole
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | ItemViewerWithoutUOM_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | PurUnitReader_Signmedia        |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission              | Condition                      |
      | IVROwner_Signmedia             | ItemVendorRecord:Create |                                |
      | ItemOwner_Signmedia            | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia            | Item:ReadUoMData        | [purchaseUnitName='Signmedia'] |
      | ItemViewerWithoutUOM_Signmedia | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia          | Vendor:ReadAll          | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer                 | Currency:ReadAll        |                                |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll  |                                |
    And the following users doesn't have the following permissions:
      | User                        | Permission              |
      | Mahmoud.Abdelaziz           | ItemVendorRecord:Create |
      | Gehan.Ahmed.NoItems         | Item:ReadAll            |
      | Gehan.Ahmed.NoVendor        | Vendor:ReadAll          |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingUnit:ReadAll  |
      | Gehan.Ahmed.NoCurrency      | Currency:ReadAll        |
      | Gehan.Ahmed.NoItemUoM       | Item:ReadUoMData        |
    And the following Items exist:
      | Code   | Name       | PurchasingUnit |
      | 000008 | PRO-V-ST-2 | 0001, 0002     |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000008 | Vendor 2 | 0002, 0001     |
    And the Item with code "000008" has the following Alternate UoMs:
      | Code | Symbol       |
      | 0029 | Roll 2.20x50 |
      | 0032 | Roll 3.20x50 |
      | 0034 | Roll 1.07x50 |
    And the following Currencies exist:
      | Code | Name           |
      | 0001 | Egyptian Pound |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

  ######## Create by an unauthorized user
  Scenario Outline: (01) Create IVR by an unauthorized user OR an authorized user with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates IVR with the following values:
      | ItemCode | VendorCode | PurchasingUnit | UoMCode | ItemCodeAtVendor | Price | CurrencyCode |
      | 000008   | 000008     | 0002           | 0019    | ABC123-0001      | 11.0  | 0001         |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                        |
      | Mahmoud.Abdelaziz           |
      | Gehan.Ahmed.NoItems         |
      | Gehan.Ahmed.NoVendor        |
      | Gehan.Ahmed.NoPurchaseUnits |
      | Gehan.Ahmed.NoCurrency      |
      | Gehan.Ahmed.NoMeasures      |
