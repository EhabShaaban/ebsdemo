# Authorr: Marina (28-Jan-2019)
# Reviewer: Hend (29-Jan-2019)
# Reviewer: Somaya (13-Mar-2019)
Feature: Request to edit and cancel GeneralData section in IVR

  Background:
    Given the following users and roles exist:
      | Name                        | Role                                                   |
      | Gehan.Ahmed                 | PurchasingResponsible_Signmedia                        |
      | Amr.Khalil                  | PurchasingResponsible_Flexo                            |
      | Amr.Khalil                  | PurchasingResponsible_Corrugated                       |
      | Mahmoud.Abdelaziz           | Storekeeper_Signmedia                                  |
      | hr1                         | SuperUser                                              |
      | Gehan.Ahmed.NoItems         | PurchasingResponsible_Signmedia_CannotReadItemsRole    |
      | Gehan.Ahmed.NoVendor        | PurchasingResponsible_Signmedia_CannotReadVendor       |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  |
      | Gehan.Ahmed.NoCurrency      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole |
      | Gehan.Ahmed.NoItemUoM       | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole                        |
      | PurchasingResponsible_Signmedia                        | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia                        | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia                        | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia                        | CurrencyReader_Signmedia       |
      | PurchasingResponsible_Signmedia                        | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Flexo                            | IVROwner_Flexo                 |
      | PurchasingResponsible_Flexo                            | VendorOwner_Flexo              |
      | PurchasingResponsible_Flexo                            | ItemOwner_Flexo                |
      | PurchasingResponsible_Flexo                            | CurrencyReader_Flexo           |
      | PurchasingResponsible_Flexo                            | PurUnitReader_Flexo            |
      | PurchasingResponsible_Corrugated                       | IVROwner_Corrugated            |
      | PurchasingResponsible_Corrugated                       | VendorOwner_Corrugated         |
      | PurchasingResponsible_Corrugated                       | ItemOwner_Corrugated           |
      | PurchasingResponsible_Corrugated                       | CurrencyReader_Corrugated      |
      | PurchasingResponsible_Corrugated                       | PurUnitReader_Corrugated       |
      | SuperUser                                              | SuperUserSubRole               |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole    | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadVendor       | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole  | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | IVROwner_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | ItemViewerWithoutUOM_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadItemUoMRole  | PurUnitReader_Signmedia        |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                         | Condition                       |
      | IVROwner_Signmedia             | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
      | IVROwner_Flexo                 | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated            | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Signmedia            | Item:ReadAll                       | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo                | Item:ReadAll                       | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated           | Item:ReadAll                       | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Signmedia          | Vendor:ReadAll                     | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo              | Vendor:ReadAll                     | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated         | Vendor:ReadAll                     | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Signmedia            | Item:ReadUoMData                   | [purchaseUnitName='Signmedia']  |
      | ItemOwner_Flexo                | Item:ReadUoMData                   | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated           | Item:ReadUoMData                   | [purchaseUnitName='Corrugated'] |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll             |                                 |
      | PurUnitReader_Flexo            | PurchasingUnit:ReadAll             |                                 |
      | PurUnitReader_Corrugated       | PurchasingUnit:ReadAll             |                                 |
      | CurrencyViewer                 | Currency:ReadAll                   |                                 |
      | SuperUserSubRole               | *:*                                |                                 |
      | ItemViewerWithoutUOM_Signmedia | Item:ReadAll                       | [purchaseUnitName='Signmedia']  |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                         | Condition                      |
      | Amr.Khalil | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                        | Permission                         |
      | Mahmoud.Abdelaziz           | ItemVendorRecord:UpdateGeneralData |
      | Gehan.Ahmed.NoItems         | Item:ReadAll                       |
      | Gehan.Ahmed.NoVendor        | Vendor:ReadAll                     |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingUnit:ReadAll             |
      | Gehan.Ahmed.NoCurrency      | Currency:ReadAll                   |
      | Gehan.Ahmed.NoItemUoM       | Item:ReadUoMData                   |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 34567891011      | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000004  | 000004   | 000002     | 0039    | GDH670-888       | 4567891011       | 50.00    | 0002         | 0002             | 02-Aug-2018 10:30 AM |
      | 000008  | 000006   | 000001     | 0035    | R6000            | 89101112         | 200.50   | 0001         | 0001             | 02-Aug-2018 10:30 AM |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000006  | 000002   | 000002     | 0032    | GDF730-440       | 67891011         | 600.40   | 0004         | 0002             | 02-Aug-2018 10:30 AM |
      | 000007  | 000003   | 000002     | 0037    | GDF830-440       | 78910111         | 50.30    | 0005         | 0002             | 02-Aug-2018 10:30 AM |
      | 000009  | 000004   | 000002     | 0038    | GDF530-200       | 9101112          | 100.30   | 0006         | 0002             | 02-Aug-2018 10:30 AM |
      | 000010  | 000060   | 000051     | 0029    | GDF530-201       | 1011121314       | 50.00    | 0003         | 0005             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 11121314         | 100.00   | 0003         | 0006             | 02-Aug-2018 10:30 AM |
      | 000012  | 000060   | 000001     | 0029    | GDF530-203       | 12131415         | 70.00    | 0005         | 0001             | 02-Aug-2018 10:30 AM |
      | 000013  | 000005   | 000051     | 0034    | GDF530-205       | 13141516         | 11500.80 | 0003         | 0001             | 02-Aug-2018 10:30 AM |
    And edit session is "30" minutes

  #### Request to Edit GeneralData section in IVR
  #### Happy Paths
  #EBS-1656
  Scenario Outline: (01) IVR Request to edit GeneralData Section by an authorized user with one or two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit GeneralData section of IVR with code "<IVRCode>"
    Then GeneralData section of IVR with "<IVRCode>" becomes in edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>": "<AuthorizedReads>"
    Examples:
      | User                        | IVRCode | AuthorizedReads                                                          |
      | Gehan.Ahmed                 | 000001  | ReadItems, ReadVendors, ReadItemUOMs, ReadPurchasingUnit, ReadCurrencies |
      | Amr.Khalil                  | 000008  | ReadItems, ReadVendors, ReadItemUOMs, ReadPurchasingUnit, ReadCurrencies |
      | Amr.Khalil                  | 000011  | ReadItems, ReadVendors, ReadItemUOMs, ReadPurchasingUnit, ReadCurrencies |
      | Gehan.Ahmed.NoItems         | 000001  | ReadVendors, ReadPurchasingUnit, ReadCurrencies                          |
      | Gehan.Ahmed.NoVendor        | 000001  | ReadItems, ReadItemUOMs, ReadPurchasingUnit, ReadCurrencies              |
      | Gehan.Ahmed.NoPurchaseUnits | 000001  | ReadItems, ReadVendors, ReadItemUOMs,  ReadCurrencies                    |
      | Gehan.Ahmed.NoCurrency      | 000001  | ReadItems, ReadVendors, ReadItemUOMs, ReadPurchasingUnit                 |
      | Gehan.Ahmed.NoItemUoM       | 000001  | ReadItems, ReadVendors, ReadPurchasingUnit, ReadCurrencies               |

  #### Exceptions Cases
  #EBS-1656
  Scenario:(02) IVR Request to edit GeneralData Section that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened GeneralData section of IVR with code "000001" in edit mode successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of IVR with code "000001"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  #EBS-1656
  Scenario:(03) IVR Request to edit GeneralData Section of deleted IVR (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the IVR with code "000001" successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of IVR with code "000001"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #### Abuse Cases
  #EBS-1656
  Scenario Outline:(04) IVR Request to edit GeneralData Section with unauthorized user due to condition OR not (Abuse Case )
    Given user is logged in as "<User>"
    When "<User>" requests to edit GeneralData section of IVR with code "<IVRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | IVRCode |
      | Amr.Khalil        | 000001  |
      | Mahmoud.Abdelaziz | 000001  |

  #### Request to Cancel saving GeneralData section in IVR
  #### Happy Path
  #EBS-1656
  Scenario Outline:(06) IVR Cancel Request to edit GeneralData Section by an authorized user with one role or two roles (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of IVR with code "<IVRCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving  GeneralData section of IVR with code "<IVRCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on GeneralData section of IVR with code "<IVRCode>" is released
    Examples:
      | User        | IVRCode |
      | Gehan.Ahmed | 000001  |
      | Amr.Khalil  | 000008  |
      | Amr.Khalil  | 000011  |

  #### Exceptions Cases
  #EBS-1656
  Scenario:(07) IVR Cancel Request to edit GeneralData Section after lock session is expire (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving GeneralData section of IVR with code "000001" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-1656
  Scenario: (08) IVR Cancel Request to edit GeneralData Section after lock session is expire and section got locked (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And GeneralData section of IVR with code "000001" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving  GeneralData section of IVR with code "000001" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-1656
  Scenario: (09) IVR Cancel Request to edit GeneralData Section after lock session is expire and Item doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the IVR with code "000001" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving  GeneralData section of IVR with code "000001" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #### Abuse Cases
  #EBS-1656
  Scenario Outline:(10) IVR Cancel Request to edit GeneralData Section with unauthorized user due to condition OR not (Abuse Case )
    Given user is logged in as "<User>"
    When "<User>" cancels saving GeneralData section of IVR with code "<IVRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | IVRCode |
      | Amr.Khalil        | 000001  |
      | Mahmoud.Abdelaziz | 000001  |
