#Authors: Yara Ameen & Waseem Salama

Feature: Save IVR GeneralData - Authorization

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed.NoCurrency | PurchasingResponsible_Signmedia_CannotViewCurrencyRole |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Afaf                   | FrontDesk                                              |
    And the following roles and sub-roles exist:
      | Role                                                   | Sub-role           |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole | IVROwner_Signmedia |
      | PurchasingResponsible_Signmedia                        | IVROwner_Signmedia |
      | PurchasingResponsible_Signmedia                        | CurrencyViewer     |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                         | Condition                      |
      | IVROwner_Signmedia | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
      | CurrencyViewer     | Currency:ReadAll                   |                                |
    And the following users have the following permissions without the following conditions:
      | User                   | Permission                         | Condition                       |
      | Gehan.Ahmed            | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed            | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed.NoCurrency | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed.NoCurrency | ItemVendorRecord:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User                   | Permission               |
      | Afaf                   | ItemVendorRecord:ReadAll |
      | Gehan.Ahmed.NoCurrency | Currency:ReadAll         |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price  | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00  | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 11121314         | 100.00 | 0003         | 0006             | 02-Aug-2018 10:30 AM |
    And the following Currencies exist:
      | Code | Name                 |
      | 0002 | United States Dollar |
    
  #### Authorization
  #EBS-2400
  Scenario: (01) IVR Save GeneralData Section by an unauthorized user (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoCurrency"
    And GeneralData section of IVR with code "000001" is locked by "Gehan.Ahmed.NoCurrency" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed.NoCurrency" saves GeneralData section of IVR with code "000001" at "07-Jan-2019 09:20 AM" with the following values:
      | ItemCodeAtVendor | Price | CurrencyCode |
      | GDF530-440-1     | 12.0  | 0002         |
    Then the lock by "Gehan.Ahmed.NoCurrency" on GeneralData section of IVR with code "000001" is released
    And "Gehan.Ahmed.NoCurrency" is logged out
    And "Gehan.Ahmed.NoCurrency" is forwarded to the error page

  #EBS-2400
  Scenario Outline: (02) IVR Save GeneralData Section by an unauthorized user (Abuse case)
    Given user is logged in as "<User>"
    When "<User>" saves GeneralData section of IVR with code "<IVRCode>" with the following values:
      | ItemCodeAtVendor        | Price        | CurrencyCode    |
      | <ItemCodeAtVendorValue> | <PriceValue> | <CurrencyValue> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | IVRCode | ItemCodeAtVendorValue | PriceValue | CurrencyValue |
      | Gehan.Ahmed | 000011  | GDF530-202-1          | 110.00     | 0002          |
      | Afaf        | 000001  | GDF530-440-1          | 12.0       | 0002          |
