# Author: Aya Sadek (20-Jan-2019 03:30 PM)
# Reviewer: Somaya Ahmed (20-Jan-2019 ) ,Hend Ahmed (22,Jan,2019) 9:26 AM
# Reviewer: Niveen Magdy (07-April-2019)
Feature: Delete IVR

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia  |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo      |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission              | Condition                       |
      | IVROwner_Signmedia  | ItemVendorRecord:Delete | [purchaseUnitName='Signmedia']  |
      | IVROwner_Flexo      | ItemVendorRecord:Delete | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated | ItemVendorRecord:Delete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                     |                                 |
    And the following users have the following permissions without the following conditions:
      | User       | Permission              | Condition                      |
      | Amr.Khalil | ItemVendorRecord:Delete | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User              | Permission              |
      | Mahmoud.Abdelaziz | ItemVendorRecord:Delete |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price  | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000054  | 000001   | 000002     | 0043    | GDF530-440       | 12345678910      | 50.00  | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000055  | 000002   | 000002     | 0043    | GDB550-610       | 234567891011     | 100.00 | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000056  | 000003   | 000002     | 0042    | GDH670-777       | 34567891011      | 100.00 | 0001         | 0001             | 02-Aug-2018 10:30 AM |
      | 000057  | 000004   | 000002     | 0033    | GDH670-888       | 4567891011       | 50.00  | 0002         | 0006             | 02-Aug-2018 10:30 AM |

  # EBS-2167
  Scenario Outline: (01) Delete IVR by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete IVR with Code "<IVRCode>" successfully
    Then IVR with Code "<IVRCode>" is deleted from the system
    And a success notification is sent to "<User>" with the following message "Gen-msg-12"
    Examples:
      | User        | IVRCode |
      | Gehan.Ahmed | 000054  |
      | Gehan.Ahmed | 000055  |
      | Amr.Khalil  | 000056  |
      | Amr.Khalil  | 000057  |

  # EBS-2167
  Scenario: (02) Delete IVR, where IVR doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the IVR with code "000054" successfully
    When "Gehan.Ahmed" requests to delete IVR with Code "000054"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-2167
  Scenario: (03) Delete IVR that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened GeneralData section of IVR with code "000054" in edit mode successfully
    When "Gehan.Ahmed" requests to delete IVR with Code "000054"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"

  # EBS-2167
  Scenario Outline: (04) Delete IVR by an unauthorized user OR by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete IVR with Code "000054"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              |
      | Amr.Khalil        |
      | Mahmoud.Abdelaziz |
