# Author: Somaya Ahmed (20-Jan-2019)
# updated by: Fatma Al Zahraa (EBS - 8499)
# Reviewed: Hend Ahmed and Somaya Ahmed (07-Mar-2019)

# TODO: Add scenarios for validation (i.e. URL, requested page, page size, etc.)
# TODO: In the Then part let the resut include both Vendor Code and Name (now it include Vendor Name only)

Feature: View All IVR

  # TODO: Data Scripts: We need to add UOM 0029 to the Alternative Units of  Item 000060
  # TODO: Remove the following IVR records (as they have conflicts):
  # | 000003 | 000001 | 0036 | 000030 | 70.00   | 0002 | 0002 | 5 |
  # | 000004 | 000001 | 0039 | 000050 | 1000.00 | 0001 | 0002 | 6 |
  # | 000005 | 000004 | 0034 | R5000  | 80.00   | 0003 | 0001 | 7 |

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role                 |
      | M.D                              | IVRViewer                |
      | PurchasingResponsible_Signmedia  | IVROwner_Signmedia       |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | MeasuresViewer           |
      | PurchasingResponsible_Flexo      | IVROwner_Flexo           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | MeasuresViewer           |
      | PurchasingResponsible_Corrugated | IVROwner_Corrugated      |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | MeasuresViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission               | Condition                       |
      | IVRViewer                | ItemVendorRecord:ReadAll |                                 |
      | IVROwner_Flexo           | ItemVendorRecord:ReadAll | [purchaseUnitName='Flexo']      |
      | IVROwner_Corrugated      | ItemVendorRecord:ReadAll | [purchaseUnitName='Corrugated'] |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll   |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll   |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll   |                                 |
      | MeasuresViewer           | Measures:ReadAll         |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                       |
      | Amr.Khalil  | ItemVendorRecord:ReadAll | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | ItemVendorRecord:ReadAll | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | ItemVendorRecord:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | ItemVendorRecord:ReadAll |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 34567891011      | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000004  | 000004   | 000002     | 0039    | GDH670-888       | 4567891011       | 50.00    | 0002         | 0002             | 02-Aug-2018 10:30 AM |
      # Vendor Code Changed from 4 to 1 and this change has a reflect on delete item
      | 000008  | 000006   | 000001     | 0035    | R6000            | 89101112         | 200.50   | 0001         | 0001             | 02-Aug-2018 10:30 AM |
      # The following are new scripts
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000006  | 000002   | 000002     | 0032    | GDF730-440       | 67891011         | 600.40   | 0004         | 0002             | 02-Aug-2018 10:30 AM |
      | 000007  | 000003   | 000002     | 0037    | GDF830-440       | 78910111         | 50.30    | 0005         | 0002             | 02-Aug-2018 10:30 AM |
      | 000009  | 000004   | 000002     | 0038    | GDF530-200       | 9101112          | 100.30   | 0006         | 0002             | 02-Aug-2018 10:30 AM |
      | 000010  | 000060   | 000051     | 0029    | GDF530-201       | 1011121314       | 50.00    | 0003         | 0005             | 02-Aug-2018 10:30 AM |
      | 000011  | 000060   | 000006     | 0029    | GDF530-202       | 11121314         | 100.00   | 0003         | 0006             | 02-Aug-2018 10:30 AM |
      | 000012  | 000060   | 000001     | 0029    | GDF530-203       | 12131415         | 70.00    | 0005         | 0001             | 02-Aug-2018 10:30 AM |
      | 000013  | 000005   | 000051     | 0034    | GDF530-205       | 13141516         | 11500.80 | 0003         | 0001             | 02-Aug-2018 10:30 AM |
      | 000014  | 000002   | 000002     | 0033    | GDF730-ddd       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit                     |
      | 000060 | Hot Laminated Frontlit Fabric roll 1                    | 0001, 0002, 0003, 0004, 0005, 0006 |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002                               |
      | 000002 | Hot Laminated Frontlit Backlit roll                     | 0002                               |
      | 000003 | Flex Primer Varnish E33                                 | 0002                               |
      | 000004 | Flex cold foil adhesive E01                             | 0002                               |
      | 000005 | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 0001                               |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001                               |
    And the following Vendors exist:
      | Code   | Name              | PurchasingUnit                     |
      | 000051 | Vendor 6          | 0001, 0002, 0003, 0004, 0005, 0006 |
      | 000001 | Siegwerk          | 0001                               |
      | 000002 | Zhejiang          | 0002                               |
      | 000003 | ABC               | 0003                               |
      | 000004 | Vendor Digital    | 0004                               |
      | 000005 | Vendor Textile    | 0005                               |
      | 000006 | Vendor Corrugated | 0006                               |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
      | 0003 | Euro                 |
      | 0004 | Chinese Yuan         |
      | 0005 | Japanese Yen         |
      | 0006 | Hong Kong Dollar     |
    And the following Measures exist:
      | Code | Type     | Symbol       | Name         |
      | 0029 | Business | Roll 2.20x50 | Roll 2.20x50 |
      | 0032 | Business | Roll 3.20x50 | Roll 3.20x50 |
      | 0033 | Business | Roll 2.70x50 | Roll 2.70x50 |
      | 0034 | Business | Roll 1.07x50 | Roll 1.07x50 |
      | 0035 | Business | Roll 1.27x50 | Roll 1.27x50 |
      | 0036 | Business | Drum-10Kg    | Drum-10Kg    |
      | 0037 | Business | Drum-50Kg    | Drum-50Kg    |
      | 0038 | Business | Drum-100Kg   | Drum-100Kg   |
      | 0039 | Business | Drum-70Kg    | Drum-70Kg    |

  #EBS - 8499
  Scenario: (01) View all IVRs by an authorized user WITHOUT condition - Paging + Super user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of IVRs with no filter applied with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency         | PurchaseUnitName |
      | 000014  | 000002   | Hot Laminated Frontlit Backlit roll                     | 000002     | Zhejiang          | Roll 2.70x50 | GDF730-ddd       | 567891011        | 11500.80 | Euro             | Signmedia        |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000051     | Vendor 6          | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro             | Flexo            |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000001     | Siegwerk          | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00    | Japanese Yen     | Flexo            |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00   | Euro             | Corrugated       |
      | 000010  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000051     | Vendor 6          | Roll 2.20x50 | GDF530-201       | 1011121314       | 50.00    | Euro             | Textile          |
      | 000009  | 000004   | Flex cold foil adhesive E01                             | 000002     | Zhejiang          | Drum-100Kg   | GDF530-200       | 9101112          | 100.30   | Hong Kong Dollar | Signmedia        |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk          | Roll 1.27x50 | R6000            | 89101112         | 200.50   | Egyptian Pound   | Flexo            |
      | 000007  | 000003   | Flex Primer Varnish E33                                 | 000002     | Zhejiang          | Drum-50Kg    | GDF830-440       | 78910111         | 50.30    | Japanese Yen     | Signmedia        |
      | 000006  | 000002   | Hot Laminated Frontlit Backlit roll                     | 000002     | Zhejiang          | Roll 3.20x50 | GDF730-440       | 67891011         | 600.40   | Chinese Yuan     | Signmedia        |
      | 000005  | 000001   | Hot Laminated Frontlit Fabric roll                      | 000002     | Zhejiang          | Roll 3.20x50 | GDF630-440       | 567891011        | 11500.80 | Euro             | Signmedia        |
    And the total number of records in search results by "Ashraf.Fathi" are 14

  #EBS - 8499
  Scenario: (02) View all IVRs by an authorized user WITHOUT condition - Paging + Super user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 2 of IVRs with no filter applied with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi":
      | IVRCode | ItemCode | ItemName                            | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency             | PurchaseUnitName |
      | 000004  | 000004   | Flex cold foil adhesive E01         | 000002     | Zhejiang   | Drum-70Kg    | GDH670-888       | 4567891011       | 50.00  | United States Dollar | Signmedia        |
      | 000003  | 000003   | Flex Primer Varnish E33             | 000002     | Zhejiang   | Drum-10Kg    | GDH670-777       | 34567891011      | 100.00 | Egyptian Pound       | Signmedia        |
      | 000002  | 000002   | Hot Laminated Frontlit Backlit roll | 000002     | Zhejiang   | Roll 2.20x50 | GDB550-610       | 234567891011     | 100.00 | Egyptian Pound       | Signmedia        |
      | 000001  | 000001   | Hot Laminated Frontlit Fabric roll  | 000002     | Zhejiang   | Roll 2.20x50 | GDF530-440       | 12345678910      | 50.00  | Egyptian Pound       | Signmedia        |
    And the total number of records in search results by "Ashraf.Fathi" are 14

  #EBS - 8499
  Scenario: (03) View all IVRs by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of IVRs with no filter applied with 10 records per page
    Then the following values will be presented to "Amr.Khalil":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency       | PurchaseUnitName |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000051     | Vendor 6          | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro           | Flexo            |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000001     | Siegwerk          | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00    | Japanese Yen   | Flexo            |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00   | Euro           | Corrugated       |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk          | Roll 1.27x50 | R6000            | 89101112         | 200.50   | Egyptian Pound | Flexo            |
    And the total number of records in search results by "Amr.Khalil" are 4

  # Filter by ---> IVRCode- String - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (04) View all IVRs filtered by Code by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on IVRCode which contains "00001" with 3 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                              | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency     | PurchaseUnitName |
      | 000014  | 000002   | Hot Laminated Frontlit Backlit roll                   | 000002     | Zhejiang   | Roll 2.70x50 | GDF730-ddd       | 567891011        | 11500.80 | Euro         | Signmedia        |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 000051     | Vendor 6   | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro         | Flexo            |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                  | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00    | Japanese Yen | Flexo            |
    And the total number of records in search results by "hr1" are 6

  # EBS-3607
  #EBS - 8499
  Scenario: (05) View all IVRs filtered by Code by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of IVR with filter applied on IVRCode which contains "00001" with 3 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency       | PurchaseUnitName |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00 | Euro           | Corrugated       |
      | 000010  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000051     | Vendor 6          | Roll 2.20x50 | GDF530-201       | 1011121314       | 50.00  | Euro           | Textile          |
      | 000001  | 000001   | Hot Laminated Frontlit Fabric roll   | 000002     | Zhejiang          | Roll 2.20x50 | GDF530-440       | 12345678910      | 50.00  | Egyptian Pound | Signmedia        |
    And the total number of records in search results by "hr1" are 6

  # EBS-3607
  #EBS - 8499
  Scenario: (06) View all IVRs filtered by Code by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on IVRCode which contains "2" with 10 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency       | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00  | Japanese Yen   | Flexo            |
      | 000002  | 000002   | Hot Laminated Frontlit Backlit roll  | 000002     | Zhejiang   | Roll 2.20x50 | GDB550-610       | 234567891011     | 100.00 | Egyptian Pound | Signmedia        |
    And the total number of records in search results by "hr1" are 2

  # EBS-3607
  #EBS - 8499
  Scenario: (07) View all IVRs filtered by Code by authorized user using "Contains" - User with one roles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of IVR with filter applied on IVRCode which contains "00001" with 10 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | IVRCode | ItemCode | ItemName                            | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency       | PurchaseUnitName |
      | 000014  | 000002   | Hot Laminated Frontlit Backlit roll | 000002     | Zhejiang   | Roll 2.70x50 | GDF730-ddd       | 567891011        | 11500.80 | Euro           | Signmedia        |
      | 000001  | 000001   | Hot Laminated Frontlit Fabric roll  | 000002     | Zhejiang   | Roll 2.20x50 | GDF530-440       | 12345678910      | 50.00    | Egyptian Pound | Signmedia        |
    And the total number of records in search results by "Gehan.Ahmed" are 2

  # EBS-3607
  #EBS - 8499
  Scenario: (08) View all IVRs filtered by code by authorized user using "Contains" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of IVR with filter applied on IVRCode which contains "00001" with 10 records per page
    Then the following values will be presented to "Amr.Khalil":
      | IVRCode | ItemCode | ItemName                                              | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency     | PurchaseUnitName |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 000051     | Vendor 6          | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro         | Flexo            |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                  | 000001     | Siegwerk          | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00    | Japanese Yen | Flexo            |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1                  | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00   | Euro         | Corrugated       |
    And the total number of records in search results by "Amr.Khalil" are 3

  # Filter by ---> Vendor- MixedValues - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (09) View all IVRs filtered by VendorName by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Vendor which contains "Sieg" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency       | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00  | Japanese Yen   | Flexo            |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk   | Roll 1.27x50 | R6000            | 89101112         | 200.50 | Egyptian Pound | Flexo            |
    And the total number of records in search results by "hr1" are 2

  # EBS-3607
  #EBS - 8499
  Scenario: (10) View all IVRs filtered by VendorCode by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Vendor which contains "00001" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency       | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00  | Japanese Yen   | Flexo            |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk   | Roll 1.27x50 | R6000            | 89101112         | 200.50 | Egyptian Pound | Flexo            |
    And the total number of records in search results by "hr1" are 2

  # EBS-3607
  #EBS - 8499
  Scenario: (11) View all IVRs filtered by VendorName by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Vendor which contains "sie" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency       | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                    | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00  | Japanese Yen   | Flexo            |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk   | Roll 1.27x50 | R6000            | 89101112         | 200.50 | Egyptian Pound | Flexo            |
    And the total number of records in search results by "hr1" are 2

  # Filter by ---> Item- MixedValues - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (12) View all IVRs filtered by ItemName by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Item which contains "self adhesive" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                                | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency       | PurchaseUnitName |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000051     | Vendor 6   | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro           | Flexo            |
      | 000008  | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001     | Siegwerk   | Roll 1.27x50 | R6000            | 89101112         | 200.50   | Egyptian Pound | Flexo            |
    And the total number of records in search results by "hr1" are 2

  # Filter by ---> UoM - Json - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (13) View all IVRs filtered by UOMSymbol by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on UOM which equals "0037" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                | VendorCode | VendorName | UOMSymbol | ItemCodeAtVendor | OldItemReference | Price | Currency     | PurchaseUnitName |
      | 000007  | 000003   | Flex Primer Varnish E33 | 000002     | Zhejiang   | Drum-50Kg | GDF830-440       | 78910111         | 50.30 | Japanese Yen | Signmedia        |
    And the total number of records in search results by "hr1" are 1

  # Filter by ---> ItemCodeAtVendor - String - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (14) View all IVRs filtered by ItemCodeAtVendor by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on ItemCodeAtVendor which contains "GDF530" with 3 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                                              | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price    | Currency     | PurchaseUnitName |
      | 000013  | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 000051     | Vendor 6          | Roll 1.07x50 | GDF530-205       | 13141516         | 11500.80 | Euro         | Flexo            |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1                  | 000001     | Siegwerk          | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00    | Japanese Yen | Flexo            |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1                  | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00   | Euro         | Corrugated       |
    And the total number of records in search results by "hr1" are 6

  # Filter by ---> PurchaseUnit - Json - Contains
  # EBS-3607
  #EBS - 8499
  Scenario: (15) View all IVRs filtered by PurchaseUnitName by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on PurchaseUnitName which contains "Text" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price | Currency | PurchaseUnitName |
      | 000010  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000051     | Vendor 6   | Roll 2.20x50 | GDF530-201       | 1011121314       | 50.00 | Euro     | Textile          |
    And the total number of records in search results by "hr1" are 1

  # Filter by ---> PurchaseUnit - Number - Equal
  # EBS-3607
  #EBS - 8499
  Scenario: (16) View all IVRs filtered by Price by authorized user using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Price which equals "70.00" with 10 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price | Currency     | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00 | Japanese Yen | Flexo            |
    And the total number of records in search results by "hr1" are 1

  Scenario: (17) View all IVRs filtered by Price by authorized user using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on Price which equals "EG" with 10 records per page
    Then the total number of records in search results by "hr1" are 0

  # Filter by ---> Muliple Columns
  # EBS-3607
  #EBS - 8499
  Scenario: (18) View all IVRs filtered by authorized user using multiple columns - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with 10 records per page and the following filters applied on IVR while current locale is "en-US"
      | FieldName      | Operation | Value   |
      | userCode       | contains  | 00001   |
      | item           | contains  | roll 1  |
      | vendor         | contains  | Vendor  |
      | itemVendorCode | contains  | GDF530- |
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName        | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price  | Currency | PurchaseUnitName |
      | 000011  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000006     | Vendor Corrugated | Roll 2.20x50 | GDF530-202       | 11121314         | 100.00 | Euro     | Corrugated       |
      | 000010  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000051     | Vendor 6          | Roll 2.20x50 | GDF530-201       | 1011121314       | 50.00  | Euro     | Textile          |
    And the total number of records in search results by "hr1" are 2

  #EBS - 8499
  Scenario: (19) View all IVRs filtered by OldItemReference by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of IVR with filter applied on OldItemReference which contains "12131415" with 3 records per page
    Then the following values will be presented to "hr1":
      | IVRCode | ItemCode | ItemName                             | VendorCode | VendorName | UOMSymbol    | ItemCodeAtVendor | OldItemReference | Price | Currency     | PurchaseUnitName |
      | 000012  | 000060   | Hot Laminated Frontlit Fabric roll 1 | 000001     | Siegwerk   | Roll 2.20x50 | GDF530-203       | 12131415         | 70.00 | Japanese Yen | Flexo            |
    And the total number of records in search results by "hr1" are 1

  Scenario: (20) View all IVRs by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of IVRs with no filter applied with 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page



