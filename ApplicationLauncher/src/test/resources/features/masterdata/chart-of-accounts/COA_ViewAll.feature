Feature: View All Chart Of Accounts

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role              |
      | Corporate_Accounting_Head | ChartOfAccountsViewer |
    And the following sub-roles and permissions exist:
      | Subrole               | Permission        | Condition |
      | ChartOfAccountsViewer | GLAccount:ReadAll |           |
    And the following GL Accounts exist:
      | AccountCode | AccountName         | AccountType | Level | ParentCodeName       | Credit/Debit | State    | Ledger        |
      | 1           | Assets              |             | 1     |                      |              | Active   |               |
      | 101         | Fixed Assets        |             | 2     | 1 - Assets           |              | Active   |               |
      | 10101       | Machines            | TYPE_01     | 3     | 101 - Fixed Assets   | DEBIT        | InActive | Banks         |
      | 102         | Current Assets      |             | 2     | 1 - Assets           |              | Active   |               |
      | 10201       | Service Vendors     | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active   | PO            |
      | 2           | Liabilities         |             | 1     |                      |              | Active   | Local_Vendors |
      | 201         | Current Liabilities |             | 2     | 2 - Liabilities      |              | Active   |               |
      | 3           | Uses                |             | 1     |                      |              | Active   |               |
     # Filter By String (Contains)
  Scenario: (01) Filter View All Chart Of Accounts by AccountCode by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with filter applied on AccountCode which contains "1" with 3 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | AccountCode | AccountName         | AccountType | Level | ParentCodeName       | Credit/Debit | State  |
      | 201         | Current Liabilities |             | 2     | 2 - Liabilities      |              | Active |
      | 10201       | Service Vendors     | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active |
      | 102         | Current Assets      |             | 2     | 1 - Assets           |              | Active |
    And the total number of records in search results by "Ashraf.Salah" are 6
    # Filter By Json (Contains)
  Scenario: (02) Filter View All Chart Of Accounts by AccountType by authorized user using "Equals" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with filter applied on AccountType which equals "TYPE_11" with 1 records per page while current locale is "en-US"
    Then the following values will be presented to "Ashraf.Salah":
      | AccountCode | AccountName     | AccountType | Level | ParentCodeName       | Credit/Debit | State  |
      | 10201       | Service Vendors | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active |
    And the total number of records in search results by "Ashraf.Salah" are 1
    # Filter By long Value
  Scenario: (03) FilterView All Chart Of Accounts by Level by authorized user using "Equals" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with filter applied on Level which equals "3" with 2 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | AccountCode | AccountName     | AccountType | Level | ParentCodeName       | Credit/Debit | State    |
      | 10201       | Service Vendors | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active   |
      | 10101       | Machines        | TYPE_01     | 3     | 101 - Fixed Assets   | DEBIT        | InActive |
    And the total number of records in search results by "Ashraf.Salah" are 2
#    # Filter By List Value
  Scenario: (04) Filter View All Chart Of Accounts by State by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with filter applied on State which contains "Active" with 4 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | AccountCode | AccountName         | AccountType | Level | ParentCodeName       | Credit/Debit | State  |
      | 3           | Uses                |             | 1     |                      |              | Active |
      | 201         | Current Liabilities |             | 2     | 2 - Liabilities      |              | Active |
      | 2           | Liabilities         |             | 1     |                      |              | Active |
      | 10201       | Service Vendors     | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active |
    And the total number of records in search results by "Ashraf.Salah" are 7
   # Filter By Multiple Columns
  Scenario: (05) Filter View All Chart Of Accounts by AccountName and Level by authorized user using "Equals" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with 10 records per page while current locale is "en-US" and the following filters applied on Accounts
      | FieldName   | Operation | Value  |
      | accountName | contains  | assets |
      | level       | equals    | 2      |
    Then the following values will be presented to "Ashraf.Salah":
      | AccountCode | AccountName    | AccountType | Level | ParentCodeName | Credit/Debit | State  |
      | 102         | Current Assets |             | 2     | 1 - Assets     |              | Active |
      | 101         | Fixed Assets   |             | 2     | 1 - Assets     |              | Active |

    And the total number of records in search results by "Ashraf.Salah" are 2
     # Filter By non-existing value
  Scenario: (06) Filter View All Chart Of Accounts by AccountCode by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of Accounts with filter applied on AccountCode which contains "4864" with 6 records per page
    Then the total number of records in search results by "Ashraf.Salah" are 0
#
  Scenario: (07) View All Chart Of Accounts by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Accounts with filter applied on AccountCode which contains "1" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
