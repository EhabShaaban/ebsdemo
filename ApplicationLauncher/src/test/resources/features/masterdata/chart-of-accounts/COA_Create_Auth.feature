# Author Dev: Yara Ameen
# Author Quality: Khadrah Ali
Feature: Create GLAccounts (Auth)

  Background:
    Given the following users and roles exist:
      | Name                  | Role                                           |
      | Afaf                  | FrontDesk                                      |
      | Ashraf.Salah.NoParent | Corporate_Accounting_Head_CanNotReadParentRole |
      | Gamal.Abdelalim       | Deputy_M.D                                     |

    And the following roles and sub-roles exist:
      | Role                                           | Sub-role              |
      | Corporate_Accounting_Head_CanNotReadParentRole | ChartOfAccountsOwner  |
      | Deputy_M.D                                     | ChartOfAccountsViewer |

    And the following sub-roles and permissions exist:
      | Subrole               | Permission        | Condition |
      | ChartOfAccountsOwner  | GLAccount:Create  |           |
      | ChartOfAccountsViewer | GLAccount:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User                  | Permission        |
      | Gamal.Abdelalim       | GLAccount:Create  |
      | Afaf                  | GLAccount:Create  |
      | Ashraf.Salah.NoParent | GLAccount:ReadAll |

    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName | AccountType | Level | AccountParent | Credit/Debit | State  | Subledger | OldAccountCode | MappedAccount |
      | 2           | Liabilities |             | 1     |               |              | Active |           | 5              |               |


  #EBS-4328
  Scenario Outline: (01) Create GLAccount by an unauthorized user (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates GLAccount with following values:
      | AccountName | Level | OldAccountCode |
      | Cash        | 1     | 130            |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User            |
      | Gamal.Abdelalim |
      | Afaf            |

  #EBS-4328
  Scenario: (02) Create GLAccount by an authorized user with missing authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Ashraf.Salah.NoParent"
    When "Ashraf.Salah.NoParent" creates GLAccount with following values:
      | AccountName | Level | OldAccountCode | AccountParent   | AccountType | Credit/Debit | MappedAccount |
      | Banks       | 2     | 130            | 2 - Liabilities |             |              |               |
    Then "Ashraf.Salah.NoParent" is logged out
    And "Ashraf.Salah.NoParent" is forwarded to the error page
