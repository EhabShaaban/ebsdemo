# Author Dev: Yara Ameen & Evram Hany
# Author Quality: Ehab Shaban
# Reviewer: Somaya Ahmed and Hosam Bayomy

Feature: Delete  GLAccounts (HP)

  Background:

    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
      | Afaf         | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role             |
      | Corporate_Accounting_Head | ChartOfAccountsOwner |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition |
      | ChartOfAccountsOwner | GLAccount:Delete |           |

    And the following users doesn't have the following permissions:
      | User | Permission       |
      | Afaf | GLAccount:Delete |

    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName         | AccountType | Level | AccountParent        | Credit/Debit | State  | Subledger | OldAccountCode | MappedAccount          |
      | 1           | Assets              |             | 1     |                      |              | Active |           | 4              |                        |
      | 102         | Current Assets      |             | 2     | 1 - Assets           |              | Active |           | 471            |                        |
      | 10201       | Goods               | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active | PO        | 47101          | PurchaseOrderGLAccount |
      | 10202       | Customers           | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active | PO        | 47102          | CustomersGLAccount     |
      | 2           | Liabilities         |             | 1     |                      |              | Active |           | 5              |                        |
      | 201         | Current Liabilities |             | 2     | 2 - Liabilities      |              | Active |           | 502            |                        |
      | 3           | Uses                |             | 1     |                      |              | Active |           | 6              |                        |

    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument          | Company        |
      | 2021000030 | 01-Sep-2020 10:47 AM | hr1       | 02-Sep-2019 00:00 AM | 0002 - Signmedia | 2019         | 2019000104 - VendorInvoice | 0002 - DigiPro |

    #@INSERT
    And the following JournalEntries Details exist:
      | JournalEntryCode | JournalDate          | Subledger | GLAccount     | GLSubAccount | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2021000030       | 02-Sep-2019 12:00 AM | PO        | 10201 - Goods | 2018000049   | 0      | 422   | USD                | EGP               | 10                        | EGP             | 10                      |

  #EBS-4882
  Scenario Outline: (01) Delete GLAccount of types Root or Intermediate by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete GLAccount with code "<AccountCode>"
    Then the following GLAccount with code "<AccountCode>" is deleted successfully
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-12"
    Examples:
      | AccountCode |
      | 201         |
      | 3           |

  #EBS-4882
  Scenario: (02) Delete GLAccount of type leaf that has mapped account by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete GLAccount with code "10202"
    Then the following GLAccount with code "10202" is deleted successfully
    Then the following GLAccounts configuration relation with code "CustomersGLAccount" is deleted successfully
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-12"


     # EBS-4882
  Scenario: (03) Delete GLAccount which has been deleted by another user (Exception Case)
    Given user is logged in as "Ashraf.Salah"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GLAccount with code "10202" successfully
    When "Ashraf.Salah" requests to delete GLAccount with code "10202"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-01"

     # EBS-4882
  Scenario: (04) Delete GLAccount which has Journal Entries  (Exception Case)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete GLAccount with code "10201"
    Then the following GLAccount with code "10201" is not deleted
    Then the following GLAccounts configuration relation with code "PurchaseOrderGLAccount" is not deleted
    And an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-27"

   # EBS-4882
  Scenario: (05) Delete GLAccount which has Child GLAccounts  (Exception Case)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to delete GLAccount with code "1"
    Then the following GLAccount with code "10201" is not deleted
    And an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-27"


  #EBS-4882
  Scenario: (06) Delete GLAccount by an unauthorized user (Happy Path)
    Given user is logged in as "Afaf"
    When "Afaf" requests to delete GLAccount with code "3"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page



