# Author Dev: Ahmed Ali
# Author Quality: Khadra Ali
# Author PO : Somaya Abolwafaa
Feature: Allowed Actions Chart Of Accounts

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
      | Ahmed.Seif   | Quality_Specialist        |
      | Afaf         | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole               |
      | Corporate_Accounting_Head | ChartOfAccountsOwner  |
      | Corporate_Accounting_Head | ChartOfAccountsViewer |
      | Quality_Specialist        | ChartOfAccountsViewer |
    And the following sub-roles and permissions exist:
      | Subrole               | Permission        | Condition |
      | ChartOfAccountsOwner  | GLAccount:Delete  |           |
      | ChartOfAccountsViewer | GLAccount:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User       | Permission       |
      | Ahmed.Seif | GLAccount:Delete |
    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName | AccountType | Level | AccountParent | Credit/Debit | State  | Subledger | OldAccountCode | MappedAccount |
      | 3           | Uses        |             | 1     |               |              | Active |           | 6              |               |

  #EBS-8504
  Scenario Outline: (01) Read allowed actions in ChartOfAccounts Object Screen - All States - (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of ChartOfAccounts with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>"

    Examples:
      | User         | Code | AllowedActions |
      | Ashraf.Salah | 3    | Delete,ReadAll |
      | Ahmed.Seif   | 3    | ReadAll        |

  #EBS-8504
  Scenario Outline: (02) Read allowed actions in ChartOfAccounts Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of ChartOfAccounts with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"

    Examples:
      | User | Code |
      | Afaf | 3    |

  #EBS-8504
  Scenario: (03) Read allowed actions in ChartOfAccounts Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ashraf.Salah"
    And another user is logged in as "hr1"
    And "hr1" requests to delete GLAccount with code "3"
    When "Ashraf.Salah" requests to read actions of ChartOfAccounts with code "3"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-01"
