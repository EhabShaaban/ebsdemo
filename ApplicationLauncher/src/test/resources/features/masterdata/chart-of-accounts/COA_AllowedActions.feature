Feature: Allowed Actions Chart Of Accounts

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
      | Ahmed.Seif   | Quality_Specialist        |
      | Afaf         | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole               |
      | Corporate_Accounting_Head | ChartOfAccountsOwner  |
      | Corporate_Accounting_Head | ChartOfAccountsViewer |
      | Quality_Specialist        | ChartOfAccountsViewer |

    And the following sub-roles and permissions exist:
      | Subrole               | Permission        | Condition |
      | ChartOfAccountsOwner  | GLAccount:Create  |           |
      | ChartOfAccountsViewer | GLAccount:ReadAll |           |

  Scenario Outline: (01) Allowed Actions Chart Of Accounts Authorized Users for Home Screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Chart Of Accounts home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User         | AllowedActions |
      | Ashraf.Salah | ReadAll,Create |
      | Ahmed.Seif   | ReadAll        |

  Scenario: (02) Allowed Actions Chart Of Accounts Read allowed actions in Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Chart Of Accounts home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
