#Author: Ahmad Hamed

Feature: Dropdowns ChartOfAccounts

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Sub-role                     |
      | PurchasingResponsible_Signmedia | ChartOfAccountsViewerLimited |

    And the following sub-roles and permissions exist:
      | Subrole                      | Permission               | Condition |
      | ChartOfAccountsViewerLimited | GLAccount:ReadAllLimited |           |

    And the following ChartOfAccounts exist:
      | Account                                  | Level | State    |
      | 1 - Assets                               | 1     | Active   |
      | 101 - Fixed Assets                       | 2     | Active   |
      | 10101 - Machines                         | 3     | InActive |
      | 102 - Current Assets                     | 2     | Active   |
      | 10201 - Service Vendors                  | 3     | Active   |
      | 2 - Liabilities                          | 1     | Active   |
      | 201 - Current Liabilities                | 2     | Active   |
      | 3 - Uses                                 | 1     | Active   |
      | 10102 - Lands                            | 3     | Active   |
      | 10202 - Treasury                         | 3     | Active   |
      | 10203 - Customers                        | 3     | Active   |
      | 10204 - Banks                            | 3     | Active   |
      | 10430 - Cash                             | 3     | Active   |
      | 41101 - Realized Differences on Exchange | 3     | Active   |
      | 10431 - Bank                             | 3     | Active   |
      | 10220 - GoodsInProgress                  | 3     | Active   |
      | 10221 - service vendors 2                | 3     | Active   |
      | 10205 - Beginning inventory              | 3     | Active   |
      | 10206 - Debitors                         | 3     | Active   |
      | 10107 - Lands 2                          | 3     | Active   |
      | 10207 - PO                               | 3     | Active   |
      | 10208 - Notes receivables                | 3     | Active   |
      | 40101 - Realized exchange rate           | 3     | Active   |
      | 4 - Resources                            | 1     | Active   |
      | 401 - Exchange Rate                      | 2     | Active   |
      | 202 - Ownership equity                   | 2     | Active   |
      | 301 - Expenses                           | 2     | Active   |
      | 302 - Purchased Items                    | 2     | Active   |
      | 402 - sales                              | 2     | Active   |
      | 20101 - Creditors                        | 3     | Active   |
      | 20103 - Local Vendors                    | 3     | Active   |
      | 20104 - Import Vendors                   | 3     | Active   |
      | 30101 - sales expenses                   | 3     | Active   |
      | 30102 - Adminstrative expenses           | 3     | Active   |
      | 30201 - Local                            | 3     | Active   |
      | 30202 - Vendors                          | 3     | Active   |
      | 40102 - un realized exchange rate        | 3     | Active   |
      | 40202 - sales                            | 3     | Active   |
      | 20102 - Taxes                            | 3     | Active   |
      | 20201 - Capital Price                    | 3     | Active   |
      | 20202 - last year profit                 | 3     | Active   |
      | 6 - NoChildAccount                       | 1     | Active   |
      | 7 - Usage                                | 1     | Active   |
      | 701 - Total Purchases                    | 2     | Active   |
      | 70101 - Import Purchasing                | 3     | Active   |
      | 70102 - Local Purchasing                 | 3     | Active   |
      | 70103 - Purchasing                       | 3     | Active   |
    And the total number of existing ChartOfAccounts are 47


    #EBS-5725
  Scenario: (01) Dropdowns ChartOfAccounts Read list of ChartOfAccounts dropdown by authorized user with no condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all ChartOfAccounts that has an "Active" State and Level"3"
    Then the following ChartOfAccounts values will be presented to "Gehan.Ahmed":
      | Account                                  | Level | State  |
      | 70103 - Purchasing                       | 3     | Active |
      | 70102 - Local Purchasing                 | 3     | Active |
      | 70101 - Import Purchasing                | 3     | Active |
      | 41101 - Realized Differences on Exchange | 3     | Active |
      | 40202 - sales                            | 3     | Active |
      | 40102 - un realized exchange rate        | 3     | Active |
      | 40101 - Realized exchange rate           | 3     | Active |
      | 30202 - Vendors                          | 3     | Active |
      | 30201 - Local                            | 3     | Active |
      | 30102 - Adminstrative expenses           | 3     | Active |
      | 30101 - sales expenses                   | 3     | Active |
      | 20202 - last year profit                 | 3     | Active |
      | 20201 - Capital Price                    | 3     | Active |
      | 20104 - Import Vendors                   | 3     | Active |
      | 20103 - Local Vendors                    | 3     | Active |
      | 20102 - Taxes                            | 3     | Active |
      | 20101 - Creditors                        | 3     | Active |
      | 10431 - Bank                             | 3     | Active |
      | 10430 - Cash                             | 3     | Active |
      | 10221 - service vendors 2                | 3     | Active |
      | 10220 - GoodsInProgress                  | 3     | Active |
      | 10208 - Notes receivables                | 3     | Active |
      | 10207 - PO                               | 3     | Active |
      | 10206 - Debitors                         | 3     | Active |
      | 10205 - Beginning inventory              | 3     | Active |
      | 10204 - Banks                            | 3     | Active |
      | 10203 - Customers                        | 3     | Active |
      | 10202 - Treasury                         | 3     | Active |
      | 10201 - Service Vendors                  | 3     | Active |
      | 10107 - Lands 2                          | 3     | Active |
      | 10102 - Lands                            | 3     | Active |
    And total number of ChartOfAccounts returned to "Gehan.Ahmed" is equal to 31

    #EBS-5725
  Scenario: (02) Dropdowns ChartOfAccounts Read list of ChartOfAccounts dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all ChartOfAccounts that has an "Active" State and Level"3"
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
