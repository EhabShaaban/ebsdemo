# Author Dev: Yara Ameen
# Author Quality: Khadrah Ali
Feature: Create GLAccounts (VAL)

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role             |
      | Corporate_Accounting_Head | ChartOfAccountsOwner |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition |
      | ChartOfAccountsOwner | GLAccount:Create |           |

   #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName    | AccountType | Level | AccountParent        | Credit/Debit | State  | Subledger | OldAccountCode | MappedAccount          |
      | 1           | Assets         |             | 1     |                      |              | Active |           | 4              |                        |
      | 102         | Current Assets |             | 2     | 1 - Assets           |              | Active |           | 471            |                        |
      | 10201       | Goods          | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active | PO        | 47101          | PurchaseOrderGLAccount |

    #EBS-4328
  Scenario Outline: (01) Create GLAccount with missing mandatory field or entering disabled field (Abuse Case\Clientbypassing)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName   | Level   | OldAccountCode   | AccountParent   | AccountType   | Credit/Debit  | MappedAccount   |
      | <AccountName> | <Level> | <OldAccountCode> | <AccountParent> | <AccountType> | <CreditDebit> | <MappedAccount> |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | AccountName | Level | OldAccountCode | AccountParent        | AccountType | CreditDebit | MappedAccount |
      #missing accountName
      |             | 1     | 78             |                      |             |             |               |
      |             | 2     | 78             | 1 - Assets           |             |             |               |
      |             | 3     | 78             | 102 - Current Assets | TYPE_11     | DEBIT       | CashGLAccount |
      #missing level
      | Cash        |       | 78             |                      |             |             |               |
      #missing oldAccountCode
      | Cash        | 1     |                |                      |             |             |               |
      | Cash        | 2     |                | 1 - Assets           |             |             |               |
      | Cash        | 3     |                | 102 - Current Assets | TYPE_11     | DEBIT       | CashGLAccount |
      #missing accountParent in case of Level 2 & 3
      | Cash        | 2     | 78             |                      |             |             |               |
      | Cash        | 3     | 78             |                      | TYPE_11     | DEBIT       | CashGLAccount |
      #missing accountType in case of Level 3
      | Cash        | 3     | 78             | 102 - Current Assets |             | DEBIT       | CashGLAccount |
      #missing CREDIT/DEBIT in case of Level 3
      | Cash        | 3     | 78             | 102 - Current Assets | TYPE_11     |             | CashGLAccount |
      #missing mappedAccount in case of Level 3
      | Cash        | 3     | 78             | 102 - Current Assets | TYPE_11     | DEBIT       |               |
     #add accountParent in case of Level 1
      | Cash        | 1     | 78             | 102 - Current Assets |             |             |               |
      #add accountType in case of Level 1
      | Cash        | 1     | 78             |                      | TYPE_11     |             |               |
      #add CREDIT/DEBIT in case of Level 1
      | Cash        | 1     | 78             |                      |             | DEBIT       |               |
      #add mappedAccount in case of Level 1
      | Cash        | 1     | 78             |                      |             |             | CashGLAccount |
      #add accountType in case of Level 2
      | Cash        | 2     | 78             | 1 - Assets           | TYPE_11     |             |               |
      #add CREDIT/DEBIT in case of Level 2
      | Cash        | 2     | 78             | 1 - Assets           |             | DEBIT       |               |
      #add mappedAccount in case of Level 2
      | Cash        | 2     | 78             | 1 - Assets           |             |             | CashGLAccount |

  #EBS-4328
  Scenario Outline: (02) Create GLAccount with malicious AccountName (Abuse Case\Clientbypassing)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName   | Level | OldAccountCode |
      | <AccountName> | 1     | 78             |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | AccountName                                                                                           |
      | ay<7aga                                                                                               |
      | ay>7aga                                                                                               |
      | ay#7aga                                                                                               |
      | ay$7aga                                                                                               |
      | ay&7aga                                                                                               |
      | ay+7aga                                                                                               |
      | ay^7aga                                                                                               |
      | ay@7aga                                                                                               |
      | ay!7aga                                                                                               |
      | ay=7aga                                                                                               |
      | ay~7aga                                                                                               |
      | ay\7aga                                                                                               |
      | ay \|7aga                                                                                             |
      | ay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7ag |

  #EBS-4328
  Scenario Outline: (03) Create GLAccount with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName | Level   | OldAccountCode   | AccountParent   | AccountType   | Credit/Debit  | MappedAccount   |
      | Cash        | <Level> | <OldAccountCode> | <AccountParent> | <AccountType> | <CreditDebit> | <MappedAccount> |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | Level | OldAccountCode | AccountParent           | AccountType | CreditDebit | MappedAccount          |
      #invalid level
      | 4     | 78             | 102 - Current Assets    | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid oldAccountCode
      | 3     | ay7aga         | 102 - Current Assets    | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid accountParentCode
      | 3     | 78             | ay7aga - Current Assets | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid accountType
      | 3     | 78             | 102 - Current Assets    | ay7aga      | DEBIT       | CashGLAccount          |
      #invalid CREDIT/DEBIT
      | 3     | 78             | 102 - Current Assets    | TYPE_11     | ay7aga      | CashGLAccount          |
      #invalid mappedAccount
      | 3     | 78             | 102 - Current Assets    | TYPE_11     | DEBIT       | ay7aga                 |
      #already exist mappedAccount
      | 3     | 78             | 102 - Current Assets    | TYPE_11     | DEBIT       | PurchaseOrderGLAccount |
      #invalid parent(intermediate) in case of Level 2
      | 2     | 78             | 102 - Current Assets    | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid parent(leaf) in case of Level 2
      | 2     | 78             | 10201 - Goods           | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid parent(root) in case of Level 3
      | 3     | 78             | 1 - Assets              | TYPE_11     | DEBIT       | CashGLAccount          |
      #invalid parent(leaf) in case of Level 3
      | 3     | 78             | 10201 - Goods           | TYPE_11     | DEBIT       | CashGLAccount          |

  #EBS-4328
  Scenario: (04) Create GLAccount with incorrect data entry (Validation Failure)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName | Level | OldAccountCode | AccountParent | AccountType | Credit/Debit | MappedAccount |
      | Cash        | 3     | 78             | 107 - Assets  | TYPE_11     | DEBIT        | CashGLAccount |
    Then a failure notification is sent to "Ashraf.Salah" with the following message "Gen-msg-05"
    And the following error message is attached to ParentCode field "Gen-msg-48" and sent to "Ashraf.Salah"



