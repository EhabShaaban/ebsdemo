# Author Dev: Yara Ameen
# Author Quality: Khadrah Ali
Feature: Create GLAccounts (HP)

  Background:

    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role             |
      | Corporate_Accounting_Head | ChartOfAccountsOwner |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission       | Condition |
      | ChartOfAccountsOwner | GLAccount:Create |           |

    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName         | AccountType | Level | AccountParent        | Credit/Debit | State  | Subledger | OldAccountCode | MappedAccount          |
      | 1           | Assets              |             | 1     |                      |              | Active |           | 4              |                        |
      | 102         | Current Assets      |             | 2     | 1 - Assets           |              | Active |           | 471            |                        |
      | 10201       | Goods               | TYPE_11     | 3     | 102 - Current Assets | DEBIT        | Active | PO        | 47101          | PurchaseOrderGLAccount |
      | 2           | Liabilities         |             | 1     |                      |              | Active |           | 5              |                        |
      | 201         | Current Liabilities |             | 2     | 2 - Liabilities      |              | Active |           | 502            |                        |

  #EBS-4328
  Scenario: (01) Create GLAccount of Level "1" with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created GLAccount of Level "1" was with AccountCode "2"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName      | Level | OldAccountCode |
      | Local Purchasing | 1     | 9              |
    Then a new GLAccount is created as follows:
      | AccountCode | AccountName      | CreationDate         | LastUpdateDate       | CreatedBy    | LastUpdatedBy | State  |
      | 3           | Local Purchasing | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | Ashraf.Salah | Ashraf.Salah  | Active |
    And the GLAccount Details with code "3" is updated as follows:
      | Level | OldAccountCode | AccountType | AccountParent | Credit/Debit | Subledger | MappedAccount |
      | 1     | 9              |             |               |              |           |               |
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-11"

  #EBS-4328
  Scenario Outline: (02) Create GLAccount of Level "2" and Level "3" with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And last created GLAccount below GLAccount "<AccountParent>" was with code "<LastAccountCode>"
    When "Ashraf.Salah" creates GLAccount with following values:
      | AccountName | Level   | OldAccountCode | AccountParent   | AccountType   | Credit/Debit  | MappedAccount   |
      | Banks       | <Level> | 130            | <AccountParent> | <AccountType> | <CreditDebit> | <MappedAccount> |
    Then a new GLAccount is created as follows:
      | AccountCode   | AccountName | CreationDate         | LastUpdateDate       | CreatedBy    | LastUpdatedBy | State  |
      | <AccountCode> | Banks       | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | Ashraf.Salah | Ashraf.Salah  | Active |
    And the GLAccount Details with code "<AccountCode>" is updated as follows:
      | Level   | OldAccountCode | AccountType   | AccountParent   | Credit/Debit  | Subledger   | MappedAccount   |
      | <Level> | 130            | <AccountType> | <AccountParent> | <CreditDebit> | <Subledger> | <MappedAccount> |
    And the GLAccounts configuration is updated as follows:
      | GLConfigurationCode   | GLConfigurationAccount   |
      | <GLConfigurationCode> | <GLConfigurationAccount> |
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-11"
    Examples:
      | Level | AccountParent        | LastAccountCode | AccountCode | AccountType | CreditDebit | Subledger | MappedAccount | GLConfigurationCode | GLConfigurationAccount |
      | 2     | 2 - Liabilities      | 201             | 202         |             |             |           |               |                     |                        |
      | 3     | 102 - Current Assets | 10201           | 10202       | TYPE_11     | DEBIT       | Banks     | BankGLAccount | BankGLAccount       | 10202 - Banks          |




