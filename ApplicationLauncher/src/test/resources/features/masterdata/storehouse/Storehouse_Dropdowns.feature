#Author: Waseem Salama
#Tester: Shirin Mahmoud

Feature: View All Storehouses

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole          |
      | SalesSpecialist_Signmedia | StorehouseViewer |
    And the following sub-roles and permissions exist:
      | Subrole          | Permission         | Condition |
      | StorehouseViewer | Storehouse:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | Storehouse:ReadAll |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following Plants exist for Company "0001 - AL Madina":
      | Plant                    |
      | 0001 - Madina Tech Plant |
    And the following Storehouses exist for Plant "0001 - Madina Tech Plant":
      | Storehouse                      |
      | 0001 - AlMadina Main Store      |
      | 0002 - AlMadina Secondary Store |

  ################################### Storehouses Filtered By Plant ###################################

  #EBS-6529
  Scenario: (01) Read list of Storehouses dropdown filtered by Plant by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Storehouses for Plant "0001 - Madina Tech Plant"
    Then the following Storehouses values will be presented to "Ahmed.Al-Ashry":
      | Storehouse                      |
      | 0001 - AlMadina Main Store      |
      | 0002 - AlMadina Secondary Store |
    And total number of Storehouses returned to "Ahmed.Al-Ashry" is equal to 2

  @Future
  #EBS-6529
  Scenario: (02) Read list of Storehouses dropdown filtered by Plant by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Storehouses for Plant "0001 - Madina Tech Plant"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  ################################## Storehouses Filtered By Company ##################################

  #EBS-6529
  Scenario: (03) Read list of Storehouses dropdown filtered by Company by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Storehouses for Company "0001 - AL Madina"
    Then the following Storehouses values will be presented to "Ahmed.Al-Ashry":
      | Storehouse                      |
      | 0001 - AlMadina Main Store      |
      | 0002 - AlMadina Secondary Store |
    And total number of Storehouses returned to "Ahmed.Al-Ashry" is equal to 2

  #EBS-6529
  Scenario: (04) Read list of Storehouses dropdown filtered by Company by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Storehouses for Company "0001 - AL Madina"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page