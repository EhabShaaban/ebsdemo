Feature: View All Currency

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole        |
      | SalesSpecialist_Signmedia | CurrencyViewer |
    And the following sub-roles and permissions exist:
      | Subrole        | Permission       | Condition |
      | CurrencyViewer | Currency:ReadAll |           |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
      | 0003 | Euro                 |
      | 0004 | Chinese Yuan         |
      | 0005 | Japanese Yen         |
      | 0006 | Hong Kong Dollar     |
      | 0007 | Indian Rupee         |
      | 0008 | North Korean Won     |
      | 0009 | South Korean Won     |
      | 0010 | Saudi Riyal          |
      | 0011 | Turkish Lira         |
      | 0012 | UAE Dirham           |
      | 0013 | Pound Sterling       |

  Scenario: (01) Read list of Currency dropdown by authorized (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Currency
    Then the following Currency values will be presented to "Ahmed.Al-Ashry":
      | Currency                    |
      | 0001 - Egyptian Pound       |
      | 0002 - United States Dollar |
      | 0003 - Euro                 |
      | 0004 - Chinese Yuan         |
      | 0005 - Japanese Yen         |
      | 0006 - Hong Kong Dollar     |
      | 0007 - Indian Rupee         |
      | 0008 - North Korean Won     |
      | 0009 - South Korean Won     |
      | 0010 - Saudi Riyal          |
      | 0011 - Turkish Lira         |
      | 0012 - UAE Dirham           |
      | 0013 - Pound Sterling       |

    And total number of Currency returned to "Ahmed.Al-Ashry" is equal to 13


  @Future
  Scenario: (02) Read list of Currency dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Currency
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page