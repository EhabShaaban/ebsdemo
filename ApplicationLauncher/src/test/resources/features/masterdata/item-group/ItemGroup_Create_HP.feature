Feature: Create ItemGroup- Happy Path

  Background:

    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia  | ItemGroupOwner           |
      | PurchasingResponsible_Signmedia  | MeasuresViewer           |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo          |
      | PurchasingResponsible_Flexo      | ItemGroupOwner           |
      | PurchasingResponsible_Flexo      | MeasuresViewer           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated     |
      | PurchasingResponsible_Corrugated | ItemGroupOwner           |
      | PurchasingResponsible_Corrugated | MeasuresViewer           |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition |
      | ItemOwner_Signmedia      | Item:Create            |           |
      | ItemOwner_Flexo          | Item:Create            |           |
      | ItemOwner_Corrugated     | Item:Create            |           |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |           |
      | ItemGroupOwner           | ItemGroup:Create       |           |
      | ItemGroupOwner           | ItemGroup:ReadAll      |           |
      | MeasuresViewer           | Measures:ReadAll       |           |

    And the following ItemGroups exist:
      | Code   | Name           | GroupLevel | ParentCode | ParentName     |
      | 000002 | Signmedia Inks | ROOT       |            |                |
      | 000021 | Laminated      | HIERARCHY  | 000002     | Signmedia Inks |
      | 000211 | Signmedia Inks | LEAF       | 000021     | Laminated      |


  Scenario Outline: (01) Create ItemGroup of type root with all mandatory fields and by authorized uer who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And  Last created ItemGroup as "ROOT" was with code "000002"
    When "<User>" creates ItemGroup with the following values:
      | Name         | GroupLevel | ParentCode |
      | Digital Inks | ROOT       |            |
    Then a new ItemGroup is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | Code   | Name         | GroupLevel | ParentCode | ParentName |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | <CODE> | Digital Inks | ROOT       |            |            |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | CODE   |
      | Gehan.Ahmed | 000003 |
      | Amr.Khalil  | 000004 |


  Scenario Outline: (02) Create ItemGroup of type intermediate  with all mandatory fields and by authorized uer who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    When "<User>" creates ItemGroup with the following values:
      | Name         | GroupLevel | ParentCode |
      | Digital Inks | HIERARCHY  | 000002     |
    Then a new ItemGroup is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | Code   | Name         | GroupLevel | ParentCode | ParentName     |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | <CODE> | Digital Inks | HIERARCHY  | 000002     | Signmedia Inks |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | CODE   |
      | Gehan.Ahmed | 000022 |
      | Amr.Khalil  | 000023 |


  Scenario Outline: (03) Create ItemGroup of type leaf  with all mandatory fields and by authorized uer who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    When "<User>" creates ItemGroup with the following values:
      | Name         | GroupLevel | ParentCode |
      | Digital Inks | LEAF       | 000021     |
    Then a new ItemGroup is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | Code   | Name         | GroupLevel | ParentCode | ParentName |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | <CODE> | Digital Inks | LEAF       | 000021     | Laminated  |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | CODE   |
      | Gehan.Ahmed | 000212 |
      | Amr.Khalil  | 000213 |


