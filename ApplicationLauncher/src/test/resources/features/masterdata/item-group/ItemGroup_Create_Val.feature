Feature: Create ItemGroup- validation

  Background:

    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | ItemOwner_Signmedia      |
      | PurchasingResponsible_Signmedia  | ItemGroupOwner           |
      | PurchasingResponsible_Signmedia  | MeasuresViewer           |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Flexo      | ItemOwner_Flexo          |
      | PurchasingResponsible_Flexo      | ItemGroupOwner           |
      | PurchasingResponsible_Flexo      | MeasuresViewer           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Corrugated | ItemOwner_Corrugated     |
      | PurchasingResponsible_Corrugated | ItemGroupOwner           |
      | PurchasingResponsible_Corrugated | MeasuresViewer           |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission             | Condition |
      | ItemOwner_Signmedia      | Item:Create            |           |
      | ItemOwner_Flexo          | Item:Create            |           |
      | ItemOwner_Corrugated     | Item:Create            |           |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll |           |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll |           |
      | ItemGroupOwner           | ItemGroup:Create       |           |
      | ItemGroupOwner           | ItemGroup:ReadAll      |           |
      | MeasuresViewer           | Measures:ReadAll       |           |
    And the following ItemGroups exist:
      | Code   | Name           | GroupLevel | ParentCode | ParentName     |
      | 000002 | Signmedia Inks | ROOT       |            |                |
      | 000021 | Laminated      | HIERARCHY  | 000002     | Signmedia Inks |
      | 000211 | Signmedia Inks | LEAF       | 000021     | Laminated      |





     ######## Create with missing mandatory fields
  Scenario: (01) Create ItemGroup with missing mandatory field: Name (abuseCase)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name | GroupLevel | ParentCode |
      |      | ROOT       |            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (02) Create ItemGroup with missing mandatory field: type (abuseCase)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name           | GroupLevel | ParentCode |
      | Signmedia Inks |            |            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (03) Create ItemGroup with missing mandatory field: parent (abuseCase)
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name           | GroupLevel | ParentCode |
      | Signmedia Inks |            |            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (04) Create ItemGroup with missing mandatory field: parent (abuseCase)
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name           | GroupLevel | ParentCode |
      | Signmedia Inks | LEAF       |            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (05) Create ItemGroup with Parent value that doesn’t exit
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name           | GroupLevel | ParentCode |
      | Signmedia Inks | HIERARCHY  | 000003     |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (06) Malicious input: parent Item Group is a leaf >>  ItemGroup-BR‌-01 (Abuse\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates ItemGroup with the following values:
      | Name           | GroupLevel | ParentCode |
      | Signmedia Inks | HIERARCHY  | 000211     |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page







