Feature: Delete ItemGroup

  Background:

    Given the following users and roles exist:
      | Name                             | Role                            |
      | Gehan.Ahmed                      | PurchasingResponsible_Signmedia |
      | hr1                              | SuperUser                       |
      | Mahmoud.Abdelaziz                | Storekeeper_Signmedia           |
      | Mahmoud.Abdelaziz.itemGroupsRead | thisRolrForReadItemGroup        |


    And the following roles and sub-roles exist:
      | Role                            | Subrole          |
      | PurchasingResponsible_Signmedia | ItemGroupOwner   |
      | SuperUser                       | SuperUserSubRole |
      | thisRolrForReadItemGroup        | ReadItemGroup    |

    And the following sub-roles and permissions exist:
      | Subrole          | Permission       | Condition |
      | ItemGroupOwner   | ItemGroup:Delete |           |
      | SuperUserSubRole | *:*              |           |
      | ReadItemGroup    | ItemGroup:Read   |           |


    And the following ItemGroups exist:
      | Code   | Name           | GroupLevel | ParentCode | ParentName     |
      | 000002 | Signmedia Inks | ROOT       |            |                |
      | 000021 | Laminated      | HIERARCHY  | 000002     | Signmedia Inks |
      | 000211 | Signmedia Inks | LEAF       | 000021     | Laminated      |


    # EBS-3965
  Scenario:(01) Delete ItemGroup by an authorized user who has one or two roles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete ItemGroup with code "000211"
    Then ItemGroup with code "000211" is deleted from the system
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-12"


  Scenario: (02) Delete ItemGroup where ItemGroup doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deletes ItemGroup with code "000211" successfully
    When "Gehan.Ahmed" requests to delete ItemGroup with code "000211"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (03) Delete ItemGroup by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete ItemGroup with code "000211"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  Scenario: (04) Delete ItemGroup by an unauthorized to delete user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz.itemGroupsRead"
    When "Mahmoud.Abdelaziz.itemGroupsRead" requests to delete ItemGroup with code "000211"
    Then "Mahmoud.Abdelaziz.itemGroupsRead" is logged out
    And "Mahmoud.Abdelaziz.itemGroupsRead" is forwarded to the error page


  @Future
  Scenario: (04) Delete ItemGroup when it is a parent (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete ItemGroup with code "000002"
    And ItemGroup with code "000021" has ParentCode with code "000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"




