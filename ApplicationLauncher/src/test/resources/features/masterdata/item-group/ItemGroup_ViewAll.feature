Feature: View all item groups

  Background:
    Given the following users and roles exist:
      | Name       | Role                        |
      | Amr.Khalil | PurchasingResponsible_Flexo |
      | Afaf       | FrontDesk                   |
    And the following roles and sub-roles exist:
      | Role                        | Subrole         |
      | PurchasingResponsible_Flexo | ItemGroupViewer |
    And the following sub-roles and permissions exist:
      | Subrole         | Permission        | Condition |
      | ItemGroupViewer | ItemGroup:ReadAll |           |
    And the following ItemGroups exist:
      | Code   | Name           | GroupLevel | ParentCode | ParentName     |
      | 000002 | Signmedia Inks | ROOT       |            |                |
      | 000021 | Laminated      | HIERARCHY  | 000002     | Signmedia Inks |
      | 000211 | Signmedia Inks | LEAF       | 000021     | Laminated      |

  Scenario: (01) View all item groups by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of item groups with 20 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code   | Name           | GroupLevel | ParentCode | ParentName     |
      | 000211 | Signmedia Inks | LEAF       | 000021     | Laminated      |
      | 000021 | Laminated      | HIERARCHY  | 000002     | Signmedia Inks |
      | 000002 | Signmedia Inks | ROOT       |            |                |
    And the total number of records in search results by "Amr.Khalil" are 3


  Scenario: (02) View all Items by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of item groups with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
