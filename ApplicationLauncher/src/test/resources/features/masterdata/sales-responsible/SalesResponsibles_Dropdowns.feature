Feature: View All SalesResponsibles

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                |
      | SalesSpecialist_Signmedia | SalesResponsibleViewer |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission               | Condition |
      | SalesResponsibleViewer | SalesResponsible:ReadAll |           |
    And the following SalesResponsibles exist:
      | Code | Name                |
      | 0001 | SeragEldin Meghawry |
      | 0002 | Mohammed            |
      | 0003 | Manar               |
      | 0004 | Ahmed.Al-Ashry      |

  Scenario: (01) Read list of SalesResponsibles dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all SalesResponsibles
    Then the following SalesResponsibles values will be presented to "Ahmed.Al-Ashry":
      | SalesResponsible           |
      | 0004 - Ahmed.Al-Ashry      |
      | 0003 - Manar               |
      | 0002 - Mohammed            |
      | 0001 - SeragEldin Meghawry |
    And total number of SalesResponsibles returned to "Ahmed.Al-Ashry" is equal to 4

  @Future
  Scenario: (02) Read list of SalesResponsibles dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesResponsibles
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page