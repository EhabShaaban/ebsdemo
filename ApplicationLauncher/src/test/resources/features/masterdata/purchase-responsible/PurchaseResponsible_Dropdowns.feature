#Author: Ahmad Hamed

Feature: View All Purchase Responsibles

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole       |
      | PurchasingResponsible_Signmedia | PurRespReader |

    And the following sub-roles and permissions exist:
      | Subrole       | Permission                    | Condition |
      | PurRespReader | PurchasingResponsible:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User | Permission                    |
      | Afaf | PurchasingResponsible:ReadAll |

    And the following are ALL PurchaseResponsibles that exist:
      | Code | Name            | State    |
      | 0001 | Amr.Khalil      | Active   |
      | 0002 | Gehan.Ahmed     | Active   |
      | 0003 | Marwa.Tawfeek   | Active   |
      | 0004 | Asmaa.Elkhodary | Active   |
      | 0005 | Khaled.Ahmed    | Active   |
      | 0006 | Obsolete.User   | Inactive |
      | 0007 | Draft.User      | Draft    |
    And the total number of existing PurchaseResponsibles are 7


    #EBS-5725
  Scenario: (01) Read list of Purchase Responsibles dropdown by authorized user with no condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all PurchaseResponsibles
    Then the following PurchaseResponsibles values will be presented to "Gehan.Ahmed":
      | Code | Name            | State    |
      | 0001 | Amr.Khalil      | Active   |
      | 0002 | Gehan.Ahmed     | Active   |
      | 0003 | Marwa.Tawfeek   | Active   |
      | 0004 | Asmaa.Elkhodary | Active   |
      | 0005 | Khaled.Ahmed    | Active   |
      | 0006 | Obsolete.User   | Inactive |
      | 0007 | Draft.User      | Draft    |
    And total number of PurchaseResponsibles returned to "Gehan.Ahmed" is equal to 7

  #EBS-5725
  Scenario: (02) Read list of Purchase Responsibles dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all PurchaseResponsibles
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
