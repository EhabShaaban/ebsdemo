# Author Dev: Eman Mansour
# Author Quality: Shirin Mahmoud

Feature: View All Asset Master Data Types

  Background:
    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Salah | AccountantHead_Signmedia |
      | Afaf         | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                         |
      | AccountantHead_Signmedia | AssetMasterDataViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | AssetMasterDataViewer_Signmedia | AssetMasterData:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | AssetMasterData:ReadAll |
    And the following AssetMasterData Types Exist:
      | Code      | Name      |
      | BUILDINGS | Buildings |
      | LANDS     | Lands     |
      | FURNITURE | Furniture |

    #EBS-8628
  Scenario: (01) Read list of Assets Master Data Types dropdown, by authorized user with condition (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read all AssetMasterData Types
    Then the following AssetMasterData Types values will be presented to "Ashraf.Salah":
      | Type                  |
      | FURNITURE - Furniture |
      | LANDS - Lands         |
      | BUILDINGS - Buildings |
    And total number of AssetMasterData Types returned to "Ashraf.Salah" is equal to 3

    #EBS-8628
  Scenario: (02) Read list of Assets Master Data Types dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all AssetMasterData Types
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page