# Author Dev: Order Team (16-Mar-2021)
# Author Quality: Shirin Mahmoud

Feature: View all  AssetMasterData

  Background:
    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Fathi | M.D                      |
      | Ashraf.Salah | AccountantHead_Signmedia |
      | Afaf         | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                         |
      | M.D                      | AssetMasterDataViewer           |
      | AccountantHead_Signmedia | AssetMasterDataViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | AssetMasterDataViewer           | AssetMasterData:ReadAll |                                |
      | AssetMasterDataViewer_Signmedia | AssetMasterData:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | AssetMasterData:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User         | Permission              | Condition                  |
      | Ashraf.Salah | AssetMasterData:ReadAll | [purchaseUnitName='Flexo'] |

     #@INSERT
    And the following GeneralData for AssetMasterData exist:
      | Code   | Type                  | Name          | OldNumber        | State  |
      | 000001 | BUILDINGS - Buildings | Building No 5 | 111111111111111  | Active |
      | 000002 | LANDS - Lands         | October Land  | 222222222222222  | Active |
      | 000003 | FURNITURE - Furniture | Disk          | 333333333333333  | Active |
      | 000004 | FURNITURE - Furniture | Disk 2        | 4444441444444444 | Active |
    #@INSERT
    And the following BusinessUnits for AssetMasterData exist:
      | Code   | BusinessUnit                    |
      | 000001 | 0002 - Signmedia, 0003 - Offset |
      | 000002 | 0001 - Flexo                    |
      | 000003 | 0001 - Flexo                    |
      | 000004 | 0002 - Signmedia                |

  # EBS-7745
  Scenario: (01) View all AssetMasterData, by an authorized user WITHOUT condition  (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with no filter applied with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name          | BusinessUnit                    | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2        | 0002 - Signmedia                | 4444441444444444 |
      | 000003 | FURNITURE - Furniture | Disk          | 0001 - Flexo                    | 333333333333333  |
      | 000002 | LANDS - Lands         | October Land  | 0001 - Flexo                    | 222222222222222  |
      | 000001 | BUILDINGS - Buildings | Building No 5 | 0002 - Signmedia, 0003 - Offset | 111111111111111  |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 4

  # EBS-7745
  Scenario: (02) View all AssetMasterData, by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of AssetMasterData with no filter applied with 10 records per page
    Then the following values will be presented to "Ashraf.Salah" in AssetMasterData Home Screen:
      | Code   | Type                  | Name          | BusinessUnit                    | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2        | 0002 - Signmedia                | 4444441444444444 |
      | 000001 | BUILDINGS - Buildings | Building No 5 | 0002 - Signmedia, 0003 - Offset | 111111111111111  |
    And the total number of records presented to "Ashraf.Salah" in Home Screen are 2

  # EBS-7745
  Scenario: (03) View all AssetMasterData, Filter by Code by authorized user using "Contains"
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with filter applied on Code which contains "004" with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name   | BusinessUnit     | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2 | 0002 - Signmedia | 4444441444444444 |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 1

  # EBS-7745
  Scenario: (04) View all AssetMasterData, Filter by Type by authorized user using "Equals"
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with filter applied on Type which equals "FURNITURE" with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name   | BusinessUnit     | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2 | 0002 - Signmedia | 4444441444444444 |
      | 000003 | FURNITURE - Furniture | Disk   | 0001 - Flexo     | 333333333333333  |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 2

  # EBS-7745
  Scenario: (05) View all AssetMasterData, Filter by Name by authorized user using "Contains"
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with filter applied on Name which contains "Disk" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name   | BusinessUnit     | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2 | 0002 - Signmedia | 4444441444444444 |
      | 000003 | FURNITURE - Furniture | Disk   | 0001 - Flexo     | 333333333333333  |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 2

  # EBS-7745
  Scenario: (06) View all AssetMasterData, Filter by BusinessUnit by authorized user using "Equals"
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with filter applied on BusinessUnit which contains "Flexo" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name         | BusinessUnit | OldNumber       |
      | 000003 | FURNITURE - Furniture | Disk         | 0001 - Flexo | 333333333333333 |
      | 000002 | LANDS - Lands         | October Land | 0001 - Flexo | 222222222222222 |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 2

  # EBS-7745
  Scenario: (07) View all AssetMasterData, Filter by OldNumber by authorized user using "Contains"
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of AssetMasterData with filter applied on OldNumber which contains "1" with 10 records per page
    Then the following values will be presented to "Ashraf.Fathi" in AssetMasterData Home Screen:
      | Code   | Type                  | Name          | BusinessUnit                    | OldNumber        |
      | 000004 | FURNITURE - Furniture | Disk 2        | 0002 - Signmedia                | 4444441444444444 |
      | 000001 | BUILDINGS - Buildings | Building No 5 | 0002 - Signmedia, 0003 - Offset | 111111111111111  |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 2

  # EBS-7745
  Scenario: (08) View all AssetMasterData, by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of AssetMasterData with no filter applied with 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
