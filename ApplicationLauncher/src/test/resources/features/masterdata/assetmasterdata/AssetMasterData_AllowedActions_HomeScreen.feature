# Author Dev: Order Team (16-Mar-2021)
# Author Quality: Shirin Mahmoud

Feature: AssetMasterData Home Screen Allowed Actions:

  Background:
    Given the following users and roles exist:
      | Name         | Role                     |
      | Ashraf.Fathi | M.D                      |
      | Ashraf.Salah | AccountantHead_Signmedia |
      | Afaf         | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                        |
      | M.D                      | AssetMasterDataViewer          |
      | AccountantHead_Signmedia | AssetMasterDataOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission              | Condition                      |
      | AssetMasterDataViewer          | AssetMasterData:ReadAll |                                |
      | AssetMasterDataOwner_Signmedia | AssetMasterData:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | AssetMasterData:ReadAll |

  # EBS-7746
  Scenario Outline: (01) Read allowed actions in AssetMasterData Home Screen, by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of AssetMasterData home screen
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User         | AllowedActions |
      | Ashraf.Fathi | ReadAll        |
      | Ashraf.Salah | ReadAll        |

  # EBS-7746
  Scenario: (02) Read allowed actions in AssetMasterData Home Screen, by Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of AssetMasterData home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"