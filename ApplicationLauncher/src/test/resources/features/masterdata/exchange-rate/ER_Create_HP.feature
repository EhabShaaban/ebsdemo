Feature: Create Exchange Rate (Happy Path)

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
    And the following roles and sub-roles exist:
      | Role                      | Subrole           |
      | Corporate_Accounting_Head | ExchangeRateOwner |
      | Corporate_Accounting_Head | CurrencyViewer    |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission          | Condition |
      | ExchangeRateOwner | ExchangeRate:Create |           |
      | CurrencyViewer    | Currency:ReadAll    |           |
    Given the following Currencies exist:
      | Code | Name                 | ISO |
      | 0001 | Egyptian Pound       | EGP |
      | 0002 | United States Dollar | USD |
      | 0003 | Euro                 | EUR |
      | 0004 | Chinese Yuan         | CNY |
      | 0005 | Japanese Yen         | JPY |
      | 0006 | Hong Kong Dollar     | HKD |

  # EBS-4944
  Scenario Outline: (01) Create ExchangeRate with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    And CurrentDateTime = "<CurrentDateTime>"
    And Last created ExchangeRate was with code "2018000006"
    When "Ashraf.Salah" creates ExchangeRate with following values:
      | FirstCurrency   | SecondValue   |
      | <FirstCurrency> | <SecondValue> |
    Then a new ExchangeRate is created as follows:
      | Code   | State  | CreatedBy    | LastUpdatedBy | CreationDate      | LastUpdateDate    | FirstValue | FirstCurrency   | SecondValue   | SecondCurrency | Type            | ValidationFrom    |
      | <Code> | Active | Ashraf.Salah | Ashraf.Salah  | <CurrentDateTime> | <CurrentDateTime> | 1.0        | <FirstCurrency> | <SecondValue> | 0001 - EGP     | USER_DAILY_RATE | <CurrentDateTime> |
    And a success notification is sent to "Ashraf.Salah" with the following message "Gen-msg-11"

    Examples:
      | CurrentDateTime      | Code       | FirstCurrency | SecondValue |
      | 01-Jan-2018 11:00 AM | 2018000007 | 0002          | 15.52       |
      | 01-Jan-2018 11:00 AM | 2018000007 | 0003          | 20.674538   |
      | 01-Jan-2020 11:00 AM | 2020000001 | 0002          | 15.52       |
