Feature: Create Exchange Rate (Validation)

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
    And the following roles and sub-roles exist:
      | Role                      | Subrole           |
      | Corporate_Accounting_Head | ExchangeRateOwner |
      | Corporate_Accounting_Head | CurrencyViewer    |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission          | Condition |
      | ExchangeRateOwner | ExchangeRate:Create |           |
      | CurrencyViewer    | Currency:ReadAll    |           |
    And the following Currencies exist:
      | Code | Name                 | ISO |
      | 0001 | Egyptian Pound       | EGP |
      | 0002 | United States Dollar | USD |

  # EBS-4871
  Scenario Outline: (01) Val Create Exchange Rate with missing mandatory fields or malicious input that violates business rules enforced by the client (Abuse Case)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates ExchangeRate with following values:
      | FirstCurrency   | SecondValue   |
      | <FirstCurrency> | <SecondValue> |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page

    Examples:
      | FirstCurrency | SecondValue    |
      | 0002          | -1.0           |
      | 0002          | 10000000000.00 |
      | 0002          | 10,000.00      |
      | 0002          | ay7aga         |
      | 0002          | ""             |
      | 0002          | 0.0            |
      | 0002          | 100.0037893    |
      | 0002          |                |
      | ay7aga        | 100.33         |
      | 0001          | 100.33         |
      |               | 100.33         |
      | ""            | 10.33          |
      | 0001          | 100.3          |
      |               |                |

  # EBS-4871
  Scenario: (02) Val Create Exchange Rate with incorrect input: Currency has been deleted (Validation Failure)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" creates ExchangeRate with following values:
      | FirstCurrency | SecondValue |
      | 9999          | 100.66      |
    Then a failure notification is sent to "Ashraf.Salah" with the following message "Gen-msg-05"
    And the following error message is attached to "firstCurrencyCode" field "Gen-msg-48" and sent to "Ashraf.Salah"

  # EBS-4871
  Scenario: (03) Val Create Exchange Rate with currency that already has an Exchange Rate valid from the same day (Validation Failure)
    Given user is logged in as "Ashraf.Salah"
    And CurrentDateTime = "07-Aug-2019 11:02 AM"
    And the following Exchange Rates data exist:
      | Code       | State  | FirstCurrency | SecondCurrency | Type            | ValidationFrom       |
      | 2018000004 | Active | 0002 - USD    | 0001 - EGP     | USER_DAILY_RATE | 07-Aug-2019 09:02 AM |
    When "Ashraf.Salah" creates ExchangeRate with following values:
      | FirstCurrency | SecondValue |
      | 0002          | 100.66      |
    Then an error notification is sent to "Ashraf.Salah" with the following message "ER-msg-01"
