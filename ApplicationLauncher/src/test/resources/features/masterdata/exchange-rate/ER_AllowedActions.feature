Feature: Allowed Action For Exchange Rate(Home Screen)

  Background:
    Given the following users and roles exist:
      | Name | Role      |
      | hr1  | SuperUser |
      | Afaf | FrontDesk |
    And the following roles and sub-roles exist:
      | Role      | Subrole          |
      | SuperUser | SuperUserSubRole |
    And the following sub-roles and permissions exist:
      | Subrole          | Permission | Condition |
      | SuperUserSubRole | *:*        |           |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | ExchangeRate:Create  |
      | Afaf | ExchangeRate:ReadAll |

  Scenario: (01) Read allowed actions in ExchangeRate Home Screen - (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read actions of ExchangeRate in home screen
    Then the following actions are displayed to "hr1":
      | AllowedActions |
      | ReadAll        |
      | Create         |

  Scenario: (02) Read allowed actions in ExchangeRate Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of ExchangeRate in home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"