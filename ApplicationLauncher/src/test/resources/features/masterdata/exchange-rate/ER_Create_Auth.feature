Feature: Create Exchange Rate (Authorization)

  Background:
    Given the following users and roles exist:
      | Name                    | Role                                             |
      | Ashraf.Salah            | Corporate_Accounting_Head                        |
      | Ashraf.Salah.NoCurrency | Corporate_Accounting_Head_CanNotReadCurrencyRole |
      | Afaf                    | FrontDesk                                        |

    And the following roles and sub-roles exist:
      | Role                                             | Subrole           |
      | Corporate_Accounting_Head                        | ExchangeRateOwner |
      | Corporate_Accounting_Head                        | CurrencyViewer    |
      | Corporate_Accounting_Head_CanNotReadCurrencyRole | ExchangeRateOwner |

    And the following sub-roles and permissions exist:
      | Subrole           | Permission          | Condition |
      | ExchangeRateOwner | ExchangeRate:Create |           |
      | CurrencyViewer    | Currency:ReadAll    |           |

    And the following users doesn't have the following permissions:
      | User                    | Permission          |
      | Ashraf.Salah.NoCurrency | Currency:ReadAll    |
      | Afaf                    | ExchangeRate:Create |

    And the following Currencies exist:
      | Code | Name                 | ISO |
      | 0003 | Euro                 | EUR |
      | 0002 | United States Dollar | USD |

  # EBS-4945
  Scenario: (01) Request Create ExchangeRate by an authorized user with authorized reads
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to create ExchangeRate
    Then the following authorized reads are returned to "Ashraf.Salah":
      | AuthorizedReads |
      | ReadCurrency    |

  # EBS-4945
  Scenario: (02) Request Create ExchangeRate by an authorized user with missing authorized reads
    Given user is logged in as "Ashraf.Salah.NoCurrency"
    When "Ashraf.Salah.NoCurrency" requests to create ExchangeRate
    Then there are no authorized reads returned to "Ashraf.Salah.NoCurrency"

  # EBS-4945
  Scenario: (03) Request Create ExchangeRate by an authorized user with missing authorized reads
    Given user is logged in as "Afaf"
    When "Afaf" requests to create ExchangeRate
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-4945
  Scenario Outline: (04) Request Create ExchangeRate by an unauthorized user OR authorized user with missing authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates ExchangeRate with following values:
      | FirstCurrency | SecondValue |
      | 0002          | 100.66      |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                    |
      | Afaf                    |
      | Ashraf.Salah.NoCurrency |

