# Author Dev: Ahmed Ali
# Author QA :
# Author PO :
Feature: Update Exchange Rate From Central Bank of Egypt

  Background:
    Given the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    And CurrentDateTime = "27-Dec-2020 09:00 AM"
    #@Insert
    #@Mock
    And the following ExchangeRate of the Central Bank of Egypt:
      | CurrencyISO | CurrencyPrice |
      | EUR         | 19.23         |
      | AED         | 4.29          |
      | USD         | 15.77         |
      | JPY         | 15.21         |
      | SAR         | 4.20          |
      | GBP         | 21.43         |
      | CNY         | 2.42          |

  # EBS-7821
  Scenario: (01) Update ExchangeRate from The Central Bank of Egypt (HP)
    And Last created ExchangeRate was with code "2018000006"
    Then The system retrieve currency prices from CBE and Create ExchangeRates with following values:
      | Code       | Type                  | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy |
      | 2018000007 | CENTRAL_BANK_OF_EGYPT | 0003 - EUR    | 0001 - EGP     | 19.23       | 27-Dec-2020 09:00 AM | System    |
      | 2018000008 | CENTRAL_BANK_OF_EGYPT | 0012 - AED    | 0001 - EGP     | 4.29        | 27-Dec-2020 09:00 AM | System    |
      | 2018000009 | CENTRAL_BANK_OF_EGYPT | 0002 - USD    | 0001 - EGP     | 15.77       | 27-Dec-2020 09:00 AM | System    |
      | 2018000010 | CENTRAL_BANK_OF_EGYPT | 0005 - JPY    | 0001 - EGP     | 15.21       | 27-Dec-2020 09:00 AM | System    |
      | 2018000011 | CENTRAL_BANK_OF_EGYPT | 0010 - SAR    | 0001 - EGP     | 4.20        | 27-Dec-2020 09:00 AM | System    |
      | 2018000012 | CENTRAL_BANK_OF_EGYPT | 0013 - GBP    | 0001 - EGP     | 21.43       | 27-Dec-2020 09:00 AM | System    |
      | 2018000013 | CENTRAL_BANK_OF_EGYPT | 0004 - CNY    | 0001 - EGP     | 2.42        | 27-Dec-2020 09:00 AM | System    |
