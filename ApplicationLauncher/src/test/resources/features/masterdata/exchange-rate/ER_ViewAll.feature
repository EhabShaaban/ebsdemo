#Auther Dev: Ahmed Ali (EBS-8079)

Feature: Exchange Rate (View All)

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
      | Afaf         | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole            |
      | Corporate_Accounting_Head | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission           | Condition |
      | ExchangeRateViewer | ExchangeRate:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | ExchangeRate:ReadAll |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    And the following ExchangeRates exist:
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type                  |
      | 2018000006 | 0004 - CNY    | 0001 - EGP     | 2.4162      | 27-Dec-2020 11:35 AM | System       | Central Bank Of Egypt |
      | 2018000005 | 0002 - USD    | 0001 - EGP     | 15.7684     | 27-Dec-2020 11:35 AM | System       | Central Bank Of Egypt |
      | 2018000004 | 0002 - USD    | 0001 - EGP     | 0.43        | 07-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate       |
      | 2018000003 | 0001 - EGP    | 0004 - CNY     | 0.63        | 06-Aug-2019 09:02 AM | Ahmed.Hamdi  | User Daily Rate       |
      | 2018000002 | 0001 - EGP    | 0004 - CNY     | 0.53        | 06-Aug-2019 09:00 AM | Ashraf.Salah | User Daily Rate       |
      | 2018000001 | 0001 - EGP    | 0004 - CNY     | 0.43        | 05-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate       |

  Scenario: (01) View all ExchangeRates by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with no filter applied with 5 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type                  |
      | 2018000006 | 0004 - CNY    | 0001 - EGP     | 2.4162      | 27-Dec-2020 11:35 AM | System       | Central Bank Of Egypt |
      | 2018000005 | 0002 - USD    | 0001 - EGP     | 15.7684     | 27-Dec-2020 11:35 AM | System       | Central Bank Of Egypt |
      | 2018000004 | 0002 - USD    | 0001 - EGP     | 0.43        | 07-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate       |
      | 2018000003 | 0001 - EGP    | 0004 - CNY     | 0.63        | 06-Aug-2019 09:02 AM | Ahmed.Hamdi  | User Daily Rate       |
      | 2018000002 | 0001 - EGP    | 0004 - CNY     | 0.53        | 06-Aug-2019 09:00 AM | Ashraf.Salah | User Daily Rate       |
    And the total number of records in search results by "Ashraf.Salah" are 6

  Scenario: (02) View all ExchangeRates by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 2 of ExchangeRates with no filter applied with 5 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type            |
      | 2018000001 | 0001 - EGP    | 0004 - CNY     | 0.43        | 05-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate |
    And the total number of records in search results by "Ashraf.Salah" are 6

  Scenario: (03) View all ExchangeRates by FirstCurrency by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on FirstCurrency which contains "USD" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type                  |
      | 2018000005 | 0002 - USD    | 0001 - EGP     | 15.7684     | 27-Dec-2020 11:35 AM | System       | Central Bank Of Egypt |
      | 2018000004 | 0002 - USD    | 0001 - EGP     | 0.43        | 07-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate       |
    And the total number of records in search results by "Ashraf.Salah" are 2

  Scenario: (04) View all ExchangeRates by SecondCurrency by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on SecondCurrency which contains "CNY" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type            |
      | 2018000003 | 0001 - EGP    | 0004 - CNY     | 0.63        | 06-Aug-2019 09:02 AM | Ahmed.Hamdi  | User Daily Rate |
      | 2018000002 | 0001 - EGP    | 0004 - CNY     | 0.53        | 06-Aug-2019 09:00 AM | Ashraf.Salah | User Daily Rate |
      | 2018000001 | 0001 - EGP    | 0004 - CNY     | 0.43        | 05-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate |
    And the total number of records in search results by "Ashraf.Salah" are 3

  Scenario: (05) View all ExchangeRates by SecondValue by authorized user using "equals" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on SecondValue which equals "0.53" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type            |
      | 2018000002 | 0001 - EGP    | 0004 - CNY     | 0.53        | 06-Aug-2019 09:00 AM | Ashraf.Salah | User Daily Rate |
    And the total number of records in search results by "Ashraf.Salah" are 1

  Scenario: (06) View all ExchangeRates by ValidFrom by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on ValidFrom which contains "05-Aug-2019" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy    | Type            |
      | 2018000001 | 0001 - EGP    | 0004 - CNY     | 0.43        | 05-Aug-2019 09:02 AM | Ashraf.Salah | User Daily Rate |
    And the total number of records in search results by "Ashraf.Salah" are 1

  Scenario: (07) View all ExchangeRates by CreatedBy by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on CreatedBy which contains "HaMDi" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy   | Type            |
      | 2018000003 | 0001 - EGP    | 0004 - CNY     | 0.63        | 06-Aug-2019 09:02 AM | Ahmed.Hamdi | User Daily Rate |
    And the total number of records in search results by "Ashraf.Salah" are 1

	#EBS-8079
  Scenario: (08) View all ExchangeRates by Type by authorized user using "equals" (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of ExchangeRates with filter applied on Type which equals "CENTRAL_BANK_OF_EGYPT" with 20 records per page
    Then the following values will be presented to "Ashraf.Salah":
      | Code       | FirstCurrency | SecondCurrency | SecondValue | ValidFrom            | CreatedBy | Type                  |
      | 2018000006 | 0004 - CNY    | 0001 - EGP     | 2.4162      | 27-Dec-2020 11:35 AM | System    | Central Bank Of Egypt |
      | 2018000005 | 0002 - USD    | 0001 - EGP     | 15.7684     | 27-Dec-2020 11:35 AM | System    | Central Bank Of Egypt |
    And the total number of records in search results by "Ashraf.Salah" are 2

  Scenario: (09) View all ExchangeRates by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of ExchangeRates with no filter applied with 5 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
