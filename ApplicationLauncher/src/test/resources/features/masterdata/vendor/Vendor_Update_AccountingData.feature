Feature: Update Vendor AccountingDetails section

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Gehan.Ahmed.NoAccounts | PurchasingResponsible_Signmedia_CannotViewAccountsRole |
      | hr1                    | SuperUser                                              |
      | Afaf                   | FrontDesk                                              |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole                      |
      | PurchasingResponsible_Signmedia                        | VendorOwner_Signmedia        |
      | PurchasingResponsible_Signmedia                        | ChartOfAccountsViewerLimited |
      | PurchasingResponsible_Signmedia_CannotViewAccountsRole | VendorOwner_Signmedia        |
      | SuperUser                                              | SuperUserSubRole             |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                     | Condition                      |
      | VendorOwner_Signmedia        | Vendor:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewerLimited | GLAccount:ReadAllLimited       |                                |
      | SuperUserSubRole             | *:*                            |                                |
    And the following users doesn't have the following permissions:
      | User | Permission                     |
      | Afaf | Vendor:UpdateAccountingDetails |
      | Afaf | GLAccount:ReadAllLimited       |
    And the following AccountingInfo for Vendors exist:
      | VendorCode | AccountCode | AccountName       | BusinessUnit |
      | 000010     | 10102       | Lands             | 0001         |
      | 000002     | 10221       | service vendors 2 | 0002         |
    And the following Accounts exist:
      | AccountCode | AccountName     | Level | State    |
      | 10102       | Lands           | 3     | Active   |
      | 10202       | Treasury        | 3     | Active   |
      | 10201       | Service Vendors | 3     | Active   |
      | 102         | Current Assets  | 2     | Active   |
      | 10101       | Machines        | 3     | InActive |
    And edit session is "30" minutes

  @ResetData
  Scenario:(01) Update Vendor AccountingDetails section, by an authorized user with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Vendor with code "000002"
    Then AccountingDetails section of Vendor with code "000002" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadAccounts    |
    And the following mandatory fields are returned to "Gehan.Ahmed":
      | MandatoriesFields |
      | accountCode       |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | accountCode    |

  Scenario:(02) Update Vendor AccountingDetails section, (No authorized reads)
    Given user is logged in as "Gehan.Ahmed.NoAccounts"
    When "Gehan.Ahmed.NoAccounts" requests to edit AccountingDetails section of Vendor with code "000002"
    Then AccountingDetails section of Vendor with code "000002" becomes in edit mode and locked by "Gehan.Ahmed.NoAccounts"
    And there are no authorized reads returned to "Gehan.Ahmed.NoAccounts"

  Scenario:(03) Update Vendor AccountingDetails section, that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the AccountingDetails section Vendor with code "000002" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Vendor with code "000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  @ResetData
  Scenario:(04) Update Vendor AccountingDetails section, of deleted vendor (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000052" successfully
    When "Gehan.Ahmed" requests to edit AccountingDetails section of Vendor with code "000052"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(05) Update Vendor AccountingDetails section, with unauthorized user with/without condition(Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit AccountingDetails section of Vendor with code "000010"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |

  @ResetData
  Scenario:(06) Update Vendor AccountingDetails section, within the edit session by an authorized user(Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released

  Scenario:(07) Update Vendor AccountingDetails section, after edit session is expire (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario:(08) Update Vendor AccountingDetails section, after edit session expire and section got locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And AccountingDetails section of Vendor with code "000002" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  @ResetData
  Scenario:(09) Update Vendor AccountingDetails section, after edit session is expire and Vendor doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor with code "000002" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(10) Update Vendor AccountingDetails section, with unauthorized user with/without condition(Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving AccountingDetails section of Vendor with code "000010"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |

  ####### Save Accounting Details section (Happy Path) ##################
  @ResetData
  Scenario:(11) Update Vendor AccountingDetails section, within edit session (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      | 10202   |
    Then Vendor AccountingDetails with code "000002" is updated as follows:
      | LastUpdatedBy | LastUpdateDate     | Account |
      | Gehan.Ahmed   | 7-Jan-2019 9:30 AM | 10202   |
    And the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  ####### Save Accounting Details section Incorrect Data Entry (Validation Failure) #############
  Scenario:(12) Update Vendor AccountingDetails section, with Incorrect Data: Account doesn't exist (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      | 99999   |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Account field "Gen-msg-48" and sent to "Gehan.Ahmed"

  Scenario:(13) Update Vendor AccountingDetails section, with Malicious Data: Account is string (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      | Ay7aga  |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(14) Update Vendor AccountingDetails section, with Malicious Data: Account is level 2 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      | 102     |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(15) Update Vendor AccountingDetails section, with Malicious Data: Account is inActive (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      | 10101   |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(16) Update Vendor AccountingDetails section, with Malicious Data: Account is empty (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:30 am" with the following values:
      | Account |
      |         |
    Then the lock by "Gehan.Ahmed" on AccountingDetails section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario:(17) Update Vendor AccountingDetails section, after edit session expired (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:41 am" with the following values:
      | Account |
      | 10202   |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  @ResetData
  Scenario:(18) Update Vendor AccountingDetails section, after edit session expired & Vendor is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 am"
    And "hr1" first deleted the Vendor with code "000002" successfully at "07-Jan-2019 09:41 am"
    When "Gehan.Ahmed" saves AccountingDetails section of Vendor with code "000002" at "07-Jan-2019 09:45 am" with the following values:
      | Account |
      | 10201   |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario Outline:(19) Update Vendor AccountingDetails section, by an unauthorized users due unmatched (with/without)condition (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves AccountingDetails section of Vendor with code "000010" with the following values:
      | Account |
      | 10102   |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |

  Scenario:(20) Update Vendor AccountingDetails section, by a user who is not authorized to read one of data entry (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed.NoAccounts"
    And AccountingDetails section of Vendor with code "000002" is locked by "Gehan.Ahmed.NoAccounts" at "07-Jan-2019 09:10 am"
    When "Gehan.Ahmed.NoAccounts" saves AccountingDetails section of Vendor with code "000002" with the following values:
      | Account |
      | 10102   |
    Then the lock by "Gehan.Ahmed.NoAccounts" on AccountingDetails section of Vendor with code "000002" is released
    Then "Gehan.Ahmed.NoAccounts" is logged out
    And "Gehan.Ahmed.NoAccounts" is forwarded to the error page
