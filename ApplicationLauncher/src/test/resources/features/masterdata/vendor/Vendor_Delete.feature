Feature: Delete Vendor

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo      |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission    | Condition                       |
      | VendorOwner_Signmedia  | Vendor:Delete | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo      | Vendor:Delete | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated | Vendor:Delete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole       | *:*           |                                 |
    And the following users doesn't have the following permissions:
      | User              | Permission    |
      | Mahmoud.Abdelaziz | Vendor:Delete |
    And the following users have the following permissions without the following conditions:
      | User        | Permission    | Condition                       |
      | Gehan.Ahmed | Vendor:Delete | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:Delete | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Vendor:Delete | [purchaseUnitName='Signmedia']  |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit                 |
      | 000001 | Siegwerk | 0001 - Flexo                   |
      | 000002 | Zhejiang | 0002 - Signmedia               |
      | 000008 | Vendor 2 | 0002 - Signmedia, 0001 - Flexo |
      | 000009 | Vendor 3 | 0002 - Signmedia               |
      | 000010 | Vendor 4 | 0001 - Flexo                   |
      | 000011 | Vendor 5 | 0006 - Corrugated              |
    And the following IVR exits:
      | Code   | ItemCode | VendorCode |
      | 000054 | 000001   | 000002     |
    And the following PurchaseOrders exist with Vendor:
      | Code       | VendorCode |
      | 2018000003 | 000001     |

  # EBS-1603
  @ResetData
  Scenario Outline:(01) Delete Vendor, by an authorized user who has one or two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Vendor with code "<VendorCode>"
    Then Vendor with code "<VendorCode>" is deleted from the system
    And a success notification is sent to "<User>" with the following message "Gen-msg-12"
    Examples:
      | User        | VendorCode |
      | Gehan.Ahmed | 000009     |
      | Gehan.Ahmed | 000008     |
      | Amr.Khalil  | 000008     |
      | Amr.Khalil  | 000010     |
      | Amr.Khalil  | 000011     |

  # EBS-1603
  @ResetData
  Scenario: (02) Delete Vendor, where Vendor doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000009" successfully
    When "Gehan.Ahmed" requests to delete Vendor with code "000009"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-1603
  Scenario Outline: (03) Delete Vendor, where Vendor is referenced by another object (e.g.IVR, PO) (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Vendor with code "<VendorCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-27"
    Examples:
      | User        | VendorCode |
      | Gehan.Ahmed | 000002     |
      | Amr.Khalil  | 000001     |

  # EBS‌-2490
  Scenario Outline: (04) Delete Vendor, that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<VendorSection>" of Vendor with code "000009" in edit mode
    When "Gehan.Ahmed" requests to delete Vendor with code "000009"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | VendorSection |
      | GeneralData   |
  
  # EBS-1603
  Scenario: (05) Delete Vendor, by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Vendor with code "000009"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  # EBS-1603
  Scenario: (06) Delete Vendor, by an unauthorized user due to unmatched condition (Exception Case: because there may be a case where user can view a record while not authorized to delete it based on condition)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to delete Vendor with code "000009"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

