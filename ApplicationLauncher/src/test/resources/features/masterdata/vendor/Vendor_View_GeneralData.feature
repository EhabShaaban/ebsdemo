# Author: Shrouk Alaa, Reviewer: Somaya Aboulwafa, 03-Dec-2018 11:00 am (EBS-1638)
# Updated by: Niveen Magdy. Reviewer: Somaya Ahmed (EBS-6240)

Feature: View Vendor GeneralData

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo      |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission             | Condition                       |
      | VendorOwner_Signmedia  | Vendor:ReadGeneralData | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo      | Vendor:ReadGeneralData | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated | Vendor:ReadGeneralData | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole       | *:*                    |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission             | Condition                       |
      | Amr.Khalil  | Vendor:ReadGeneralData | [purchaseUnitName='Signmedia']  |
      | Gehan.Ahmed | Vendor:ReadGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadGeneralData | [purchaseUnitName='Corrugated'] |

    And the following GeneralData for Vendors exist:
      | VendorCode | CreationDate         | VendorName | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType              |
      | 000001     | 02-Aug-2018 07:51 AM | Siegwerk   | 0001 - Flexo                   | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      | COMMERCIAL - Commercial |
      | 000002     | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      | COMMERCIAL - Commercial |
      | 000007     | 06-Aug-2018 10:43 AM | Vendor 1   | 0006 - Corrugated              | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      | COMMERCIAL - Commercial |
      | 000008     | 06-Aug-2018 10:43 AM | Vendor 2   | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      | COMMERCIAL - Commercial |
      | 000052     | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 722222222222222    | 2-ABC-Madina      | COMMERCIAL - Commercial |

  # EBS-1638 and # EBS-6240
  Scenario: (01) View Vendor GeneralData section, by an authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view GeneralData section of Vendor with code "000002"
    Then the following values of GeneralData section of Vendor with code "000002" are displayed to "Gehan.Ahmed":
      | CreationDate         | VendorName | BusinessUnit     | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType |
      | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      | COMMERCIAL |

  # EBS-1638 and # EBS-6240
  Scenario Outline: : (02) View Vendor GeneralData section, by an authorized user with two roles - AmrKhalil + Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view GeneralData section of Vendor with code "<VendorCode>"
    Then the following values of GeneralData section of Vendor with code "<VendorCode>" are displayed to "Amr.Khalil":
      | CreationDate   | VendorName   | BusinessUnit   | PurchasingResponsible   | OldVendorReference   | AccountWithVendor   | VendorType   |
      | <CreationDate> | <VendorName> | <BusinessUnit> | <PurchasingResponsible> | <OldVendorReference> | <AccountWithVendor> | <VendorType> |
    Examples:
      | VendorCode | CreationDate         | VendorName | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType |
      | 000001     | 02-Aug-2018 07:51 AM | Siegwerk   | 0001 - Flexo                   | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      | COMMERCIAL |
      | 000007     | 06-Aug-2018 10:43 AM | Vendor 1   | 0006 - Corrugated              | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      | COMMERCIAL |
      | 000008     | 06-Aug-2018 10:43 AM | Vendor 2   | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      | COMMERCIAL |

  # EBS-1638
  Scenario: (03) View Vendor GeneralData section, where vendor doesn'e exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000052" successfully
    When "Gehan.Ahmed" requests to view GeneralData section of Vendor with code "000052"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-1638
  Scenario: (04) View Vendor GeneralData section, by an unauthorized user due condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view GeneralData section of Vendor with code "000002"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
