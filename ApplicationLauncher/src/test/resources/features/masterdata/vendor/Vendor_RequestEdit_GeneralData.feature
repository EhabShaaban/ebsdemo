Feature: Request to edit and cancel GeneralData section in Vendor

  Background:

    Given the following users and roles exist:
      | AMe         | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Ahmed.Hamdi | Accountant_Flexo                 |
      | hr1         | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia    |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo        |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated   |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Signmedia  | PurRespReader            |
      | PurchasingResponsible_Flexo      | PurRespReader            |
      | PurchasingResponsible_Corrugated | PurRespReader            |
      | SuperUser                        | SuperUserSubRole         |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                    | Condition                       |
      | VendorOwner_Signmedia    | Vendor:UpdateGeneralData      | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo        | Vendor:UpdateGeneralData      | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated   | Vendor:UpdateGeneralData      | [purchaseUnitName='Corrugated'] |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll        |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll        |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll        |                                 |
      | PurRespReader            | PurchasingResponsible:ReadAll |                                 |
      | SuperUserSubRole         | *:*                           |                                 |
    And the following users doesn't have the following permissions:
      | User        | Permission               |
      | Ahmed.Hamdi | Vendor:UpdateGeneralData |
    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                       |
      | Gehan.Ahmed | Vendor:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Vendor:UpdateGeneralData | [purchaseUnitName='Signmedia']  |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit    |
      | 000001 | Siegwerk | 0001 - Flexo      |
      | 000002 | Zhejiang | 0002 - Signmedia  |
      | 000007 | Vendor 1 | 0006 - Corrugated |
      | 000052 | Zhejiang | 0002 - Signmedia  |
      | 000053 | Zhejiang | 0002 - Signmedia  |
    And edit session is "30" minutes

  ### Request to edit GeneralData section ###
  Scenario Outline:(01) Request Edit Vendor GeneralData section, by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit GeneralData section of Vendor with code "<VendorCode>"
    Then GeneralData section of Vendor with code "<VendorCode>" becomes in edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads           |
      | ReadPurchasingUnit        |
      | ReadPurchasingResponsible |
    Examples:
      | User        | VendorCode |
      | Gehan.Ahmed | 000002     |
      | Amr.Khalil  | 000001     |
      | Amr.Khalil  | 000007     |

  Scenario:(02) Request Edit Vendor GeneralData section, that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the Vendor with code "000002" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of Vendor with code "000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  @ResetData
  Scenario:(03) Request Edit Vendor GeneralData section, of deleted vendor (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000052" successfully
    When "Gehan.Ahmed" requests to edit GeneralData section of Vendor with code "000052"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario:(04) Request Edit Vendor GeneralData section, with unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit GeneralData section of Vendor with code "000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario:(05) Request Edit Vendor GeneralData section, with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit GeneralData section of Vendor with code "000001"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  ### Request to Cancel saving GeneralData section ###
  Scenario Outline:(06) Request Cancel Edit Vendor GeneralData section, by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of Vendor with code "<VendorCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving GeneralData section of Vendor with code "<VendorCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on GeneralData section of Vendor with code "<VendorCode>" is released
    Examples:
      | User        | VendorCode |
      | Gehan.Ahmed | 000002     |
      | Amr.Khalil  | 000001     |
      | Amr.Khalil  | 000007     |

  Scenario:(07) Request Cancel Edit Vendor GeneralData section, after lock session is expire
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving GeneralData section of Vendor with code "000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  @ResetData
  Scenario: (08) Request Cancel Edit Vendor GeneralData section, after lock session is expire and Vendor doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of Vendor with code "000053" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor with code "000053" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving GeneralData section of Vendor with code "000053" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario:(09) Request Cancel Edit Vendor GeneralData section, with unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" cancels saving GeneralData section of Vendor with code "000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario:(10) Request Cancel Edit Vendor GeneralData section, with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" cancels saving GeneralData section of Vendor with code "000001"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page