#Author : Marina  (19/12/2018)

Feature: Read Vendor Next and Previous

  Background:
    Given the following users and roles exist:
      | Name            | Role                             |
      | Ashraf.Fathi    | M.D                              |
      | Gehan.Ahmed     | PurchasingResponsible_Signmedia  |
      | Amr.Khalil      | PurchasingResponsible_Flexo      |
      | Amr.Khalil      | PurchasingResponsible_Corrugated |
      | Asmaa.Elkhodary | PurchasingResponsible_Digital    |
      | Afaf            | FrontDesk                        |
      | hr1             | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role               |
      | M.D                              | VendorViewer           |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo      |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission     | Condition                       |
      | VendorViewer           | Vendor:ReadAll |                                 |
      | VendorOwner_Signmedia  | Vendor:ReadAll | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo      | Vendor:ReadAll | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated | Vendor:ReadAll | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole       | *:*            |                                 |

    And the following users doesn't have the following permissions:
      | User | Permission     |
      | Afaf | Vendor:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User        | Permission     | Condition                       |
      | Gehan.Ahmed | Vendor:ReadAll | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadAll | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Vendor:ReadAll | [purchaseUnitName='Signmedia']  |

    And the following Vendors exist:
      | Code   | Name           | PurchasingUnit                 |
      | 000011 | Vendor 5       | 0006 - Corrugated              |
      | 000010 | Vendor 4       | 0001 - Flexo                   |
      | 000009 | Vendor 3       | 0002 - Signmedia               |
      | 000008 | Vendor 2       | 0002 - Signmedia, 0001 - Flexo |
      | 000007 | Vendor 1       | 0006 - Corrugated              |
      | 000005 | Vendor Textile | 0005 - Textile                 |
      | 000004 | Vendor Digital | 0004 - Digital                 |
      | 000003 | ABC            | 0003 - Offset                  |
      | 000002 | Zhejiang       | 0002 - Signmedia               |
      | 000001 | Siegwerk       | 0001 - Flexo                   |

  Scenario Outline: (01) Read Vendor Next/Previous Page, by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view next Vendor of current Vendor with code "<VendorCode>"
    Then the next Vendor code "<NextValue>" is displayed to "<User>"
    Examples:
      | User         | VendorCode | NextValue |
      | Gehan.Ahmed  | 000002     | 000008    |
      | Gehan.Ahmed  | 000008     | 000009    |
      | Amr.Khalil   | 000001     | 000007    |
      | Amr.Khalil   | 000007     | 000008    |
      | Amr.Khalil   | 000008     | 000010    |
      | Amr.Khalil   | 000010     | 000011    |
      | Ashraf.Fathi | 000004     | 000005    |
      | Ashraf.Fathi | 000009     | 000010    |
      | Ashraf.Fathi | 000010     | 000011    |
      | hr1          | 000003     | 000004    |
      | hr1          | 000004     | 000005    |

  Scenario Outline: (02) Read Vendor Next/Previous Page, by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view previous Vendor of current Vendor with code "<VendorCode>"
    Then the previous Vendor code "<PrevValue>" is displayed to "<User>"
    Examples:
      | User         | VendorCode | PrevValue |
      | Gehan.Ahmed  | 000008     | 000002    |
      | Gehan.Ahmed  | 000009     | 000008    |
      | Amr.Khalil   | 000007     | 000001    |
      | Amr.Khalil   | 000008     | 000007    |
      | Amr.Khalil   | 000010     | 000008    |
      | Amr.Khalil   | 000011     | 000010    |
      | Ashraf.Fathi | 000004     | 000003    |
      | Ashraf.Fathi | 000009     | 000008    |
      | Ashraf.Fathi | 000010     | 000009    |
      | Ashraf.Fathi | 000011     | 000010    |
      | hr1          | 000003     | 000002    |
      | hr1          | 000004     | 000003    |

  @ResetData
  Scenario: (03) Read Vendor Next/Previous Page, Navigate unexisted next record while it is the last record to display
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000011" successfully
    When "Amr.Khalil" requests to view next Vendor of current Vendor with code "000010"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-31"

  @ResetData
  Scenario: (04) Read Vendor Next/Previous Page, Navigate unexisted previous record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Amr.Khalil"
    And "Amr.Khalil" first deleted the Vendor with code "000001" successfully
    When "hr1" requests to view previous Vendor of current Vendor with code "000002"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  @ResetData
  Scenario: (05) Read Vendor Next/Previous Page, Navigate unexisted next record
    Given user is logged in as "hr1"
    And another user is logged in as "Asmaa.Elkhodary"
    And "Asmaa.Elkhodary" first deleted the Vendor with code "000004" successfully
    When "hr1" requests to view next Vendor of current Vendor with code "000003"
    Then the next Vendor code "000005" is displayed to "hr1"

  @ResetData
  Scenario: (06) Read Vendor Next/Previous Page, Navigate unexisted previous record
    Given user is logged in as "Ashraf.Fathi"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the Vendor with code "000002" successfully
    When "Ashraf.Fathi" requests to view previous Vendor of current Vendor with code "000003"
    Then the previous Vendor code "000001" is displayed to "Ashraf.Fathi"

  Scenario: (07) Read Vendor Next/Previous Page, Read Next/Previous by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view next Vendor of current Vendor with code "000002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page