# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmed Hamed due to update required by story  EBS-4839
# Updated by order team due to update required by story  EBS-8101
# Reviewed by: Somaya Ahmed

Feature: Create Commercial Vendor - Validations

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | PurRespReader           |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                    | Condition |
      | VendorOwner_Signmedia   | Vendor:Create                 |           |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll        |           |
      | PurRespReader           | PurchasingResponsible:ReadAll |           |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following PurchaseResponsibles exist:
      | Code | Name        |
      | 0002 | Gehan.Ahmed |
    And the following VendorTypes exist:
      | Code         |
      | COMMERCIAL   |
      | SERVICE      |
      | CONSUMABLE   |
      | FIXED_ASSETS |
    And the following PurchaseResponsibles doesn't exit:
      | Code | Name                        |
      | 9999 | PotatoPurchasingResponsible |
    And the following BusinessUnits doesn't exit:
      | Code | Name               |
      | 9999 | PotatoBusinessUnit |
    And the following VendorTypes doesn't exit:
      | Code          |
      | POTATO_VENDOR |

    #@INSERT
    And the following GeneralData for Vendor exist:
      | Code   | VendorName                                      | VendorType | AccountWithVendor | PurchasingResponsible | State  | OldVendorReference | CreatedBy | CreationDate         |
      | 000008 | Alex Professional Limited Company International | COMMERCIAL | Madina 12345-fgh  | 0002                  | Active | 111111111111111    | Admin     | 20-Oct-2019 11:00 AM |
   #@INSERT
    And the following BusinessUnits for Vendor exist:
      | Code   | BusinessUnit                    |
      | 000008 | 0002 - Signmedia, 0003 - Offset |

  ######## Create with missing mandatory fields (Abuse cases since create button is not enabled until all mandatories are entered)
  # EBS-2178
  # EBS-4839
  # EBS-8101
  Scenario Outline: (01) Create Vendor, with missing mandatory field (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName        | BusinessUnit        | PurchasingResponsible        | OldVendorReference        | AccountWithVendor        | VendorType        |
      | <VendorNameValue> | <BusinessUnitValue> | <PurchasingResponsibleValue> | <OldVendorReferenceValue> | <AccountWithVendorValue> | <VendorTypeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | VendorNameValue                                 | BusinessUnitValue | PurchasingResponsibleValue | OldVendorReferenceValue | AccountWithVendorValue | VendorTypeValue |
     # Vendor Name is missing in all vendor types
      |                                                 | 0002              | 0002                       | 1                       | Madina 12345-fgh       | COMMERCIAL      |
      | ""                                              | 0002              | 0002                       | 2                       | Madina 12345-fgh       | COMMERCIAL      |
      | N/A                                             | 0002              | 0002                       | 2                       | Madina 12345-fgh       | COMMERCIAL      |
      |                                                 | 0002              | 0002                       | 3                       | Madina 12345-fgh       | SERVICE         |
      | ""                                              | 0002              | 0002                       | 4                       | Madina 12345-fgh       | SERVICE         |
      | N/A                                             | 0002              | 0002                       | 4                       | Madina 12345-fgh       | SERVICE         |
      |                                                 | 0002              | 0002                       | 5                       | Madina 12345-fgh       | CONSUMABLE      |
      | ""                                              | 0002              | 0002                       | 6                       | Madina 12345-fgh       | CONSUMABLE      |
      | N/A                                             | 0002              | 0002                       | 6                       | Madina 12345-fgh       | CONSUMABLE      |
      |                                                 | 0002              | 0002                       | 7                       | Madina 12345-fgh       | FIXED_ASSETS    |
      | ""                                              | 0002              | 0002                       | 8                       | Madina 12345-fgh       | FIXED_ASSETS    |
      | N/A                                             | 0002              | 0002                       | 8                       | Madina 12345-fgh       | FIXED_ASSETS    |
    # Business Unit is missing in all vendor types
      | Alex Professional Limited Company International |                   | 0002                       | 9                       | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | ""                | 0002                       | 10                      | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | N/A               | 0002                       | 10                      | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International |                   | 0002                       | 11                      | Madina 12345-fgh       | SERVICE         |
      | Alex Professional Limited Company International | ""                | 0002                       | 12                      | Madina 12345-fgh       | SERVICE         |
      | Alex Professional Limited Company International | N/A               | 0002                       | 12                      | Madina 12345-fgh       | SERVICE         |
      | Alex Professional Limited Company International |                   | 0002                       | 13                      | Madina 12345-fgh       | CONSUMABLE      |
      | Alex Professional Limited Company International | ""                | 0002                       | 14                      | Madina 12345-fgh       | CONSUMABLE      |
      | Alex Professional Limited Company International | N/A               | 0002                       | 14                      | Madina 12345-fgh       | CONSUMABLE      |
      | Alex Professional Limited Company International |                   | 0002                       | 15                      | Madina 12345-fgh       | FIXED_ASSETS    |
      | Alex Professional Limited Company International | ""                | 0002                       | 16                      | Madina 12345-fgh       | FIXED_ASSETS    |
      | Alex Professional Limited Company International | N/A               | 0002                       | 16                      | Madina 12345-fgh       | FIXED_ASSETS    |
   # Business Unit is missing in all Commercial Vendor
      | Alex Professional Limited Company International |                   | 0002                       | 17                      | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | ""                | 0002                       | 18                      | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | N/A               | 0002                       | 18                      | Madina 12345-fgh       | COMMERCIAL      |
    # Type is missing
      | Alex Professional Limited Company International | 0002              | 0002                       | 19                      | Madina 12345-fgh       |                 |
      | Alex Professional Limited Company International | 0002              | 0002                       | 20                      | Madina 12345-fgh       | ""              |
      | Alex Professional Limited Company International | 0002              | 0002                       | 20                      | Madina 12345-fgh       | N/A             |
    # OldVendorReference is missing
      | Alex Professional Limited Company International | 0002              | 0002                       |                         | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | 0002              | 0002                       | ""                      | Madina 12345-fgh       | COMMERCIAL      |
      | Alex Professional Limited Company International | 0002              | 0002                       | N/A                     | Madina 12345-fgh       | COMMERCIAL      |

  ######## Create with incorrect input (Validation Failure)
  # EBS-2178 # EBS-4839
  Scenario Outline: (02) Create Vendor, Commercial Vendor with incorrect input: BusinessUnit doesn't exist >> Vendor-BR-01 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit        | PurchasingResponsible        | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | <BusinessUnitValue> | <PurchasingResponsibleValue> | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to BusinessUnit field "Vendor-msg-01" and sent to "Gehan.Ahmed"
    Examples:
      | BusinessUnitValue | VendorTypeValue | PurchasingResponsibleValue | OldVendorReference |
      | 0002, 9999        | COMMERCIAL      | 0002                       | 1                  |
      | 9999, 0002        | COMMERCIAL      | 0002                       | 2                  |
      | 9999              | COMMERCIAL      | 0002                       | 3                  |
      | 0002, 9999        | SERVICE         |                            | 4                  |
      | 9999, 0002        | SERVICE         |                            | 5                  |
      | 9999              | SERVICE         |                            | 6                  |
      | 0002, 9999        | CONSUMABLE      |                            | 7                  |
      | 9999, 0002        | CONSUMABLE      |                            | 8                  |
      | 9999              | CONSUMABLE      |                            | 9                  |
      | 0002, 9999        | FIXED_ASSETS    |                            | 10                 |
      | 9999, 0002        | FIXED_ASSETS    |                            | 11                 |
      | 9999              | FIXED_ASSETS    |                            | 12                 |

  # EBS-2178 # EBS-4839
  Scenario: (03) Create Vendor, with incorrect input: PurchasingResponsible doesn't exist >> Vendor-BR-02 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType |
      | Alex Professional Limited Company International | 0002         | 9999                  | 1234567890         | Madina 12345-fgh  | COMMERCIAL |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to PurchasingResponsible field "Vendor-msg-02" and sent to "Gehan.Ahmed"

  ######## Create with malicious input (Abuse Cases)
  # EBS-2178 # EBS-4839
  Scenario Outline: (04) Create Vendor, with malcious input: Name is invalid >> Gen-BR-01 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName        | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | <VendorNameValue> | 0002         | 0002                  | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | VendorNameValue                                                                                                                                 | VendorTypeValue | OldVendorReference |
  # Commercial Vendor
      | Alex Professional <Limited Company International                                                                                                | COMMERCIAL      | 1                  |
      | Alex Professional >Limited Company International                                                                                                | COMMERCIAL      | 2                  |
      | Alex Professional #Limited Company International                                                                                                | COMMERCIAL      | 3                  |
      | Alex Professional $Limited Company International                                                                                                | COMMERCIAL      | 4                  |
      | Alex Professional &Limited Company International                                                                                                | COMMERCIAL      | 5                  |
      | Alex Professional +Limited Company International                                                                                                | COMMERCIAL      | 6                  |
      | Alex Professional ^Limited Company International                                                                                                | COMMERCIAL      | 7                  |
      | Alex Professional @Limited Company International                                                                                                | COMMERCIAL      | 8                  |
      | Alex Professional !Limited Company International                                                                                                | COMMERCIAL      | 9                  |
      | Alex Professional =Limited Company International                                                                                                | COMMERCIAL      | 10                 |
      | Alex Professional ~Limited Company International                                                                                                | COMMERCIAL      | 11                 |
      | Alex Professional \|Limited Company International                                                                                               | COMMERCIAL      | 12                 |
      | Alex Professional \\Limited Company International                                                                                               | COMMERCIAL      | 13                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | COMMERCIAL      | 14                 |
  # Service Vendor
      | Alex Professional <Limited Company International                                                                                                | SERVICE         | 15                 |
      | Alex Professional >Limited Company International                                                                                                | SERVICE         | 16                 |
      | Alex Professional #Limited Company International                                                                                                | SERVICE         | 17                 |
      | Alex Professional $Limited Company International                                                                                                | SERVICE         | 18                 |
      | Alex Professional *Limited Company International                                                                                                | SERVICE         | 19                 |
      | Alex Professional &Limited Company International                                                                                                | SERVICE         | 20                 |
      | Alex Professional +Limited Company International                                                                                                | SERVICE         | 21                 |
      | Alex Professional ^Limited Company International                                                                                                | SERVICE         | 22                 |
      | Alex Professional @Limited Company International                                                                                                | SERVICE         | 23                 |
      | Alex Professional !Limited Company International                                                                                                | SERVICE         | 24                 |
      | Alex Professional =Limited Company International                                                                                                | SERVICE         | 25                 |
      | Alex Professional ~Limited Company International                                                                                                | SERVICE         | 26                 |
      | Alex Professional \|Limited Company International                                                                                               | SERVICE         | 27                 |
      | Alex Professional \\Limited Company International                                                                                               | SERVICE         | 28                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | SERVICE         | 29                 |
  # Consumables Vendor
      | Alex Professional <Limited Company International                                                                                                | CONSUMABLE      | 30                 |
      | Alex Professional >Limited Company International                                                                                                | CONSUMABLE      | 31                 |
      | Alex Professional #Limited Company International                                                                                                | CONSUMABLE      | 32                 |
      | Alex Professional $Limited Company International                                                                                                | CONSUMABLE      | 33                 |
      | Alex Professional *Limited Company International                                                                                                | CONSUMABLE      | 34                 |
      | Alex Professional &Limited Company International                                                                                                | CONSUMABLE      | 35                 |
      | Alex Professional +Limited Company International                                                                                                | CONSUMABLE      | 36                 |
      | Alex Professional ^Limited Company International                                                                                                | CONSUMABLE      | 37                 |
      | Alex Professional @Limited Company International                                                                                                | CONSUMABLE      | 38                 |
      | Alex Professional !Limited Company International                                                                                                | CONSUMABLE      | 39                 |
      | Alex Professional =Limited Company International                                                                                                | CONSUMABLE      | 40                 |
      | Alex Professional ~Limited Company International                                                                                                | CONSUMABLE      | 41                 |
      | Alex Professional \|Limited Company International                                                                                               | CONSUMABLE      | 42                 |
      | Alex Professional \\Limited Company International                                                                                               | CONSUMABLE      | 43                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | CONSUMABLE      | 44                 |
  # Fixed Assets Vendor
      | Alex Professional <Limited Company International                                                                                                | FIXED_ASSETS    | 45                 |
      | Alex Professional >Limited Company International                                                                                                | FIXED_ASSETS    | 46                 |
      | Alex Professional #Limited Company International                                                                                                | FIXED_ASSETS    | 47                 |
      | Alex Professional $Limited Company International                                                                                                | FIXED_ASSETS    | 48                 |
      | Alex Professional *Limited Company International                                                                                                | FIXED_ASSETS    | 49                 |
      | Alex Professional &Limited Company International                                                                                                | FIXED_ASSETS    | 50                 |
      | Alex Professional +Limited Company International                                                                                                | FIXED_ASSETS    | 51                 |
      | Alex Professional ^Limited Company International                                                                                                | FIXED_ASSETS    | 52                 |
      | Alex Professional @Limited Company International                                                                                                | FIXED_ASSETS    | 53                 |
      | Alex Professional !Limited Company International                                                                                                | FIXED_ASSETS    | 54                 |
      | Alex Professional =Limited Company International                                                                                                | FIXED_ASSETS    | 55                 |
      | Alex Professional ~Limited Company International                                                                                                | FIXED_ASSETS    | 56                 |
      | Alex Professional \|Limited Company International                                                                                               | FIXED_ASSETS    | 57                 |
      | Alex Professional \\Limited Company International                                                                                               | FIXED_ASSETS    | 58                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | FIXED_ASSETS    | 59                 |

  # EBS-2178 # EBS-4839
  Scenario Outline: (05) Create Vendor, with malicious input: BusinessUnit is string >> Vendor-BR-01 (AbsueCase\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit        | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | <BusinessUnitValue> | 0002                  | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | BusinessUnitValue | VendorTypeValue | OldVendorReference |
      | 0002, Ay7aga      | COMMERCIAL      | 2                  |
      | Ay7aga, 0002      | COMMERCIAL      | 3                  |
      | Ay7aga            | COMMERCIAL      | 4                  |
      | 0002, Ay7aga      | SERVICE         | 5                  |
      | Ay7aga, 0002      | SERVICE         | 6                  |
      | Ay7aga            | SERVICE         | 7                  |
      | 0002, Ay7aga      | CONSUMABLE      | 8                  |
      | Ay7aga, 0002      | CONSUMABLE      | 9                  |
      | Ay7aga            | CONSUMABLE      | 10                 |
      | 0002, Ay7aga      | FIXED_ASSETS    | 11                 |
      | Ay7aga, 0002      | FIXED_ASSETS    | 12                 |
      | Ay7aga            | FIXED_ASSETS    | 13                 |

  # EBS-2178 # EBS-4839
  Scenario Outline: (06) Create Vendor, with  with malicious input: PurchasingResponsible  (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible       | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | 0002         | PotatoPurchasingResponsible | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | VendorTypeValue | OldVendorReference |
      | COMMERCIAL      | 2                  |
      | SERVICE         | 3                  |
      | CONSUMABLE      | 4                  |
      | FIXED_ASSETS    | 5                  |

  # EBS-2178
  Scenario Outline: (07) Create Vendor, with malicious input: OldVendorReference is invalid >> Gen-BR-04 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference        | AccountWithVendor | VendorType |
      | Alex Professional Limited Company International | 0002         | 0002                  | <OldVendorReferenceValue> | Madina 12345-fgh  | COMMERCIAL |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | OldVendorReferenceValue |
      | 123 456465              |
      | 123adfgd                |
      | 123@%^&()               |
      | 1234567891011121314156  |

  # EBS-2178 # EBS-4839
  Scenario Outline: (08) Create Vendor, with malicious input: AccountWithVendor is invalid >> Gen-BR-01 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor        | VendorType        |
      | Alex Professional Limited Company International | 0002         | 0002                  | <OldVendorReference> | <AccountWithVendorValue> | <VendorTypeValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | AccountWithVendorValue                                                                                                                          | VendorTypeValue | OldVendorReference |
  # Commercial Vendors
      | Madina 12345-fgh <                                                                                                                              | COMMERCIAL      | 1                  |
      | Madina 12345-fgh >                                                                                                                              | COMMERCIAL      | 2                  |
      | Madina 12345-fgh #                                                                                                                              | COMMERCIAL      | 3                  |
      | Madina 12345-fgh $                                                                                                                              | COMMERCIAL      | 4                  |
      | Madina 12345-fgh &                                                                                                                              | COMMERCIAL      | 5                  |
      | Madina 12345-fgh +                                                                                                                              | COMMERCIAL      | 6                  |
      | Madina 12345-fgh ^                                                                                                                              | COMMERCIAL      | 7                  |
      | Madina 12345-fgh @                                                                                                                              | COMMERCIAL      | 8                  |
      | Madina 12345-fgh !                                                                                                                              | COMMERCIAL      | 9                  |
      | Madina 12345-fgh =                                                                                                                              | COMMERCIAL      | 10                 |
      | Madina 12345-fgh ~                                                                                                                              | COMMERCIAL      | 11                 |
      | Madina 12345-fgh \|                                                                                                                             | COMMERCIAL      | 12                 |
      | Madina 12345-fgh \\a                                                                                                                            | COMMERCIAL      | 13                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | COMMERCIAL      | 14                 |
  # Service Vendors
      | Madina 12345-fgh <                                                                                                                              | SERVICE         | 15                 |
      | Madina 12345-fgh >                                                                                                                              | SERVICE         | 16                 |
      | Madina 12345-fgh #                                                                                                                              | SERVICE         | 17                 |
      | Madina 12345-fgh $                                                                                                                              | SERVICE         | 18                 |
      | Madina 12345-fgh &                                                                                                                              | SERVICE         | 19                 |
      | Madina 12345-fgh +                                                                                                                              | SERVICE         | 20                 |
      | Madina 12345-fgh ^                                                                                                                              | SERVICE         | 21                 |
      | Madina 12345-fgh @                                                                                                                              | SERVICE         | 22                 |
      | Madina 12345-fgh !                                                                                                                              | SERVICE         | 23                 |
      | Madina 12345-fgh =                                                                                                                              | SERVICE         | 24                 |
      | Madina 12345-fgh ~                                                                                                                              | SERVICE         | 25                 |
      | Madina 12345-fgh \|                                                                                                                             | SERVICE         | 26                 |
      | Madina 12345-fgh \\a                                                                                                                            | SERVICE         | 27                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | SERVICE         | 28                 |
  # Consumable Vendors
      | Madina 12345-fgh <                                                                                                                              | CONSUMABLE      | 30                 |
      | Madina 12345-fgh >                                                                                                                              | CONSUMABLE      | 31                 |
      | Madina 12345-fgh #                                                                                                                              | CONSUMABLE      | 32                 |
      | Madina 12345-fgh $                                                                                                                              | CONSUMABLE      | 33                 |
      | Madina 12345-fgh &                                                                                                                              | CONSUMABLE      | 34                 |
      | Madina 12345-fgh +                                                                                                                              | CONSUMABLE      | 35                 |
      | Madina 12345-fgh ^                                                                                                                              | CONSUMABLE      | 36                 |
      | Madina 12345-fgh @                                                                                                                              | CONSUMABLE      | 37                 |
      | Madina 12345-fgh !                                                                                                                              | CONSUMABLE      | 38                 |
      | Madina 12345-fgh =                                                                                                                              | CONSUMABLE      | 39                 |
      | Madina 12345-fgh ~                                                                                                                              | CONSUMABLE      | 40                 |
      | Madina 12345-fgh \|                                                                                                                             | CONSUMABLE      | 41                 |
      | Madina 12345-fgh \\a                                                                                                                            | CONSUMABLE      | 42                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | CONSUMABLE      | 43                 |
  # Fixed Assets Vendors
      | Madina 12345-fgh <                                                                                                                              | FIXED_ASSETS    | 45                 |
      | Madina 12345-fgh >                                                                                                                              | FIXED_ASSETS    | 46                 |
      | Madina 12345-fgh #                                                                                                                              | FIXED_ASSETS    | 47                 |
      | Madina 12345-fgh $                                                                                                                              | FIXED_ASSETS    | 48                 |
      | Madina 12345-fgh &                                                                                                                              | FIXED_ASSETS    | 49                 |
      | Madina 12345-fgh +                                                                                                                              | FIXED_ASSETS    | 50                 |
      | Madina 12345-fgh ^                                                                                                                              | FIXED_ASSETS    | 51                 |
      | Madina 12345-fgh @                                                                                                                              | FIXED_ASSETS    | 52                 |
      | Madina 12345-fgh !                                                                                                                              | FIXED_ASSETS    | 53                 |
      | Madina 12345-fgh =                                                                                                                              | FIXED_ASSETS    | 54                 |
      | Madina 12345-fgh ~                                                                                                                              | FIXED_ASSETS    | 55                 |
      | Madina 12345-fgh \|                                                                                                                             | FIXED_ASSETS    | 56                 |
      | Madina 12345-fgh \\a                                                                                                                            | FIXED_ASSETS    | 57                 |
      | Alex Professional Limited Company International Alex Professional Limited Company International Alex Professional Limited Company International | FIXED_ASSETS    | 58                 |

  # EBS-4839
  Scenario: (09) Create Vendor, with malicious input: Type doesn't exist >> Vendor-BR-02 (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType    |
      | Alex Professional Limited Company International | 0002         | 0002                  | 1234567890         | Madina 12345-fgh  | POTATO_VENDOR |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    # EBS-8101
  Scenario: (10) Create Vendor, with incorrect input: There is already an existing Vendor record with same OldVendorReference (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType |
      | Alex Professional Limited Company International | 0002         | 0002                  | 111111111111111    | Madina 12345-fgh  | COMMERCIAL |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "oldVendorReference" field "Vendor-msg-04" and sent to "Gehan.Ahmed"
