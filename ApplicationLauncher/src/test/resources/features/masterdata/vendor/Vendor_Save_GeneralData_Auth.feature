#Author: Aya Sadek on Date: 08-Jan-2019, 02:38 PM
#Reviewer:

Feature: Save Vendor GeneralData Auth

  Background:
    Given the following users and roles exist:
      | Name                        | Role                                                  |
      | Shady.Abdelatif             | Accountant_Signmedia                                  |
      | Amr.Khalil                  | PurchasingResponsible_Flexo                           |
      | Amr.Khalil                  | PurchasingResponsible_Corrugated                      |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingResponsible_Signmedia_CannotReadPurUnitRole |
    And the following roles and sub-roles exist:
      | Role                                                  | Subrole                |
      | PurchasingResponsible_Flexo                           | VendorOwner_Flexo      |
      | PurchasingResponsible_Corrugated                      | VendorOwner_Corrugated |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole | VendorOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission               | Condition                       |
      | VendorOwner_Flexo      | Vendor:UpdateGeneralData | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated | Vendor:UpdateGeneralData | [purchaseUnitName='Corrugated'] |
    And the following users doesn't have the following permissions:
      | User            | Permission               |
      | Shady.Abdelatif | Vendor:UpdateGeneralData |
    And the following users have the following permissions without the following conditions:
      | User       | Permission               | Condition                      |
      | Amr.Khalil | Vendor:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit   |
      | 000002 | Zhejiang | 0002 - Signmedia |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |

  # Save section by a user who is not authorized to save
  Scenario: (01) Save Vendor GeneralData section, by an unauthorized users (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves GeneralData section of Vendor with code "000002" with the following values:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Test | 0001           | 0001                  | 123456            |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  # Save section by a user who is not authorized to save due to condition
  Scenario: (02) Save Vendor GeneralData section, by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves GeneralData section of Vendor with code "000002" with the following values:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Test | 0001           | 0001                  | 123456            |
    And "Amr.Khalil" is forwarded to the error page

  # Save section using values which the user is not authorized to read
  Scenario: (03) Save Vendor GeneralData section, User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoPurchaseUnits"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed.NoPurchaseUnits" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed.NoPurchaseUnits" saves GeneralData section of Vendor with code "000002" with the following values:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Test | 0001           | 0001                  | 123456            |
    And "Gehan.Ahmed.NoPurchaseUnits" is logged out
    And "Gehan.Ahmed.NoPurchaseUnits" is forwarded to the error page