# Author: Ahmad Hamed
# Reiewer: Somaya Ahmed

Feature: View Vendor Types

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole          |
      | PurchasingResponsible_Signmedia | VendorTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole          | Permission         | Condition |
      | VendorTypeViewer | VendorType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | VendorType:ReadAll |

    And the following VendorTypes exist:
      | Code         |
      | COMMERCIAL   |
      | SERVICE      |
      | CONSUMABLE   |
      | FIXED_ASSETS |
    And the total number of existing VendorTypes are 4

    #EBS-4839
  Scenario: (01) Read VendorTypes dropdown, by authorized (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read VendorTypes in a dropdown
    Then the following VendorTypes dropdown values will be presented to "Gehan.Ahmed":
      | Code         |
      | COMMERCIAL   |
      | SERVICE      |
      | CONSUMABLE   |
      | FIXED_ASSETS |
    And total number of records returned to "Gehan.Ahmed" in the dropdown is "4"

    #EBS-4839
  Scenario: (02) Read VendorTypes dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read VendorTypes in a dropdown
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page