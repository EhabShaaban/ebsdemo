# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmed Hamed due to update required by story  EBS-4839
# Reviewed by: Somaya Ahmed

Feature: Create Vendor - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia    |
      | PurchasingResponsible_Signmedia  | PurRespReader            |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | VendorTypeViewer         |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo        |
      | PurchasingResponsible_Flexo      | PurRespReader            |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | VendorTypeViewer         |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated   |
      | PurchasingResponsible_Corrugated | PurRespReader            |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | VendorTypeViewer         |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                    | Condition |
      | VendorOwner_Signmedia    | Vendor:Create                 |           |
      | VendorOwner_Flexo        | Vendor:Create                 |           |
      | VendorOwner_Corrugated   | Vendor:Create                 |           |
      | VendorTypeViewer         | VendorType:ReadAll            |           |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll        |           |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll        |           |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll        |           |
      | PurRespReader            | PurchasingResponsible:ReadAll |           |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following purchase responsible exist:
      | Code | Name        |
      | 0002 | Gehan.Ahmed |
    And the following VendorTypes exist:
      | Code         |
      | COMMERCIAL   |
      | SERVICE      |
      | CONSUMABLE   |
      | FIXED_ASSETS |

  ######## Create Happy Paths
  Scenario Outline: (01) Create Vendor, of Type Commercial, with all mandatory fields by an authorized user who has one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created Vendor was with code "000050"
    When "<User>" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit        | PurchasingResponsible | OldVendorReference   | AccountWithVendor        | VendorType |
      | Alex Professional Limited Company International | <BusinessUnitValue> | 0002                  | <OldVendorReference> | <AccountWithVendorValue> | COMMERCIAL |
    Then a new Vendor is created with the following values:
      | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | VendorCode | VendorName                                      | BusinessUnit        | PurchasingResponsible | OldVendorReference   | AccountWithVendor        | VendorType |
      | <User>    | <User>        | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051     | Alex Professional Limited Company International | <BusinessUnitValue> | 0002                  | <OldVendorReference> | <AccountWithVendorValue> | COMMERCIAL |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | AccountWithVendorValue | BusinessUnitValue            | OldVendorReference |
      | Gehan.Ahmed | Madina 12345-fgh       | 0002                         | 1                  |
      | Gehan.Ahmed |                        | 0002                         | 2                  |
      | Gehan.Ahmed | Madina 12345-fgh       | 0001, 0003, 0004, 0005, 0006 | 3                  |
      | Amr.Khalil  | Madina 12345-fgh       | 0001                         | 4                  |
      | Amr.Khalil  |                        | 0001                         | 5                  |
      | Amr.Khalil  | Madina 12345-fgh       | 0002, 0003, 0004, 0005       | 6                  |
      | Amr.Khalil  |                        | 0001, 0006                   | 7                  |
  
  #EBS-4839
  Scenario Outline: (02)  Create Vendor, of Type (Service|Consumable|Fixed_Assets) with all mandatory fields by an authorized (Happy Path)
  Difference from Commercial vendor is that PurchasingResponsible is Optional
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created Vendor was with code "000050"
    When "Gehan.Ahmed" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | 0002         |                       | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then a new Vendor is created with the following values:
      | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | VendorCode | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | Gehan.Ahmed | Gehan.Ahmed   | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | 000051     | Alex Professional Limited Company International | 0002         |                       | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"
    Examples:
      | VendorTypeValue | OldVendorReference |
      | SERVICE         | 1                  |
      | CONSUMABLE      | 2                  |
      | FIXED_ASSETS    | 3                  |