# Reviewer: Somaya Ahmed on 01-Jan-2019
# Updated by Ahmed Hamed due to update required by story  EBS-4839
# Reviewed by: Somaya Ahmed

Feature: Create Vendor - Authorization

  Background:
    Given the following users and roles exist:
      | Name                              | Role                                                         |
      | Mahmoud.Abdelaziz                 | Storekeeper_Signmedia                                        |
      | Gehan.Ahmed.NoPurchaseResponsible | PurchasingResponsible_Signmedia_CannotReadPurResponsibleRole |
      | Gehan.Ahmed.NoPurchaseUnits       | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        |
      | Gehan.Ahmed.NoVendorTypes         | PurchasingResponsible_Signmedia_CannotReadVendorType         |
    And the following roles and sub-roles exist:
      | Role                                                         | Subrole                 |
      | PurchasingResponsible_Signmedia_CannotReadPurResponsibleRole | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia_CannotReadPurResponsibleRole | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadPurResponsibleRole | VendorTypeViewer        |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | PurRespReader           |
      | PurchasingResponsible_Signmedia_CannotReadVendorType         | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia_CannotReadVendorType         | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadVendorType         | PurRespReader           |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                    | Condition |
      | VendorOwner_Signmedia   | Vendor:Create                 |           |
      | VendorTypeViewer        | VendorType:ReadAll            |           |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll        |           |
      | PurRespReader           | PurchasingResponsible:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User                              | Permission                    |
      | Mahmoud.Abdelaziz                 | Vendor:Create                 |
      | Gehan.Ahmed.NoPurchaseResponsible | PurchasingResponsible:ReadAll |
      | Gehan.Ahmed.NoPurchaseUnits       | PurchasingUnit:ReadAll        |
      | Gehan.Ahmed.NoVendorTypes         | VendorType:ReadAll            |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following purchase responsible exist:
      | Code | Name        |
      | 0002 | Gehan.Ahmed |
    And the following VendorTypes exist:
      | Code         |
      | COMMERCIAL   |
      | COMMERCIAL   |
      | SERVICE      |
      | CONSUMABLE   |
      | FIXED_ASSETS |

  ######## Create by an unauthorized user
  # EBS-2181 and # EBS-4839
  Scenario Outline: (01) Create Vendor, Commercial Vendor by an unauthorized user  (Abuse case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | 0002         | 0002                  | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | VendorTypeValue | OldVendorReference |
      | COMMERCIAL      | 1                  |
      | SERVICE         | 2                  |
      | CONSUMABLE      | 3                  |
      | FIXED_ASSETS    | 4                  |

  ######## Create using values which the user is not authorized to Read
  # EBS-2181 and # EBS-4839
  Scenario Outline: (02) Create Vendor, Commercial Vendor while user is not authorized to read: BusinessUnit/ Purchase Responsible/ VendorTypes (Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates Vendor with the following values:
      | VendorName                                      | BusinessUnit | PurchasingResponsible | OldVendorReference   | AccountWithVendor | VendorType        |
      | Alex Professional Limited Company International | 0002         | 0002                  | <OldVendorReference> | Madina 12345-fgh  | <VendorTypeValue> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                              | VendorTypeValue | OldVendorReference |
      | Gehan.Ahmed.NoPurchaseUnits       | COMMERCIAL      | 2                  |
      | Gehan.Ahmed.NoPurchaseResponsible | COMMERCIAL      | 3                  |
      | Gehan.Ahmed.NoVendorTypes         | COMMERCIAL      | 4                  |
      | Gehan.Ahmed.NoPurchaseUnits       | SERVICE         | 5                  |
      | Gehan.Ahmed.NoPurchaseResponsible | SERVICE         | 6                  |
      | Gehan.Ahmed.NoVendorTypes         | SERVICE         | 7                  |
      | Gehan.Ahmed.NoPurchaseUnits       | CONSUMABLE      | 8                  |
      | Gehan.Ahmed.NoPurchaseResponsible | CONSUMABLE      | 9                  |
      | Gehan.Ahmed.NoVendorTypes         | CONSUMABLE      | 10                 |
      | Gehan.Ahmed.NoPurchaseUnits       | FIXED_ASSETS    | 11                 |
      | Gehan.Ahmed.NoPurchaseResponsible | FIXED_ASSETS    | 12                 |
      | Gehan.Ahmed.NoVendorTypes         | FIXED_ASSETS    | 13                 |