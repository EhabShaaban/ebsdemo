#Auther: Hend Ahmed 06-Dec-201811:15

Feature: Read Allowed Actions for Vendor

  Background:
    Given the following users and roles exist:
      | Name               | Role                                                    |
      # One Role --> One Subrole without condition
      | VendorOwnerUser    | PurchasingResponsible                                   |
      # One Role --> One Subrole with condition
      | Gehan.Ahmed        | PurchasingResponsible_Signmedia                         |
      # Two Roles --> Each with one Subrole with condition - similar subroles
      | Amr.Khalil         | PurchasingResponsible_Flexo                             |
      | Amr.Khalil         | PurchasingResponsible_Corrugated                        |
      # Two Roles --> Each with one subrole, one with condition and the other without -similar subroles
      | TestUser1          | PurchasingResponsible_Signmedia                         |
      | TestUser1          | PurchasingResponsible                                   |
      # Two Roles --> Each with one subrole with condition - differet subroles
      | TestUser2          | PurchasingResponsible_Signmedia                         |
      | TestUser2          | LogisticsResponsible_Signmedia                          |
      # Two Roles --> Each with one subrole one with condition and the other without - differet subroles
      | TestUser3          | PurchasingResponsible_Signmedia                         |
      | TestUser3          | LogisticsResponsible                                    |
      # One Role --> Two Subroles with condition - Similar Subroles
      | TestUser4          | PurchasingResponsible_Signmedia_Flexo                   |
      # One Role --> Two Subroles one with condition and the other without - Similar Subroles
      | TestUser5          | PurchasingResponsible_ALL_Flexo                         |
      # One Role --> Two Subroles - Different Subroles with no conditions
      | TestUser6          | PurchasingResponsible_And_LogisticResponsible           |
      # One Role --> Two Subroles - Different Subroles with consitions
      | TestUser7          | PurchasingResponsible_Signmedia_And_LogisticResponsible |
      # One Role --> viewer
      | TestUser8          | LogisticsResponsible                                    |
      # User Authorized to create only
      | CreateOnlyUser     | CreateOnlyRole                                          |
      # Not authorized
      | Afaf               | FrontDesk                                               |
      #SuperUser
      | hr1                | SuperUser                                               |
      #User who is authorized to read only some sections
      | CannotReadSections | CannotReadSectionsRole                                  |

    And the following roles and sub-roles exist:
      | Role                                                    | Subrole                   |
      | CreateOnlyRole                                          | CreateOnlySubRole         |
      | PurchasingResponsible                                   | VendorOwner               |
      | PurchasingResponsible_Signmedia                         | VendorOwner_Signmedia     |
      | PurchasingResponsible_Flexo                             | VendorOwner_Flexo         |
      | PurchasingResponsible_Corrugated                        | VendorOwner_Corrugated    |

      | PurchasingResponsible_Offset                            | VendorOwner_Offset        |
      | PurchasingResponsible_Textile                           | VendorOwner_Textile       |
      | PurchasingResponsible_Digital                           | VendorOwner_Digital       |

      | LogisticsResponsible                                    | VendorViewer              |
      | LogisticsResponsible_Signmedia                          | VendorViewer_Signmedia    |
      | PurchasingResponsible_Signmedia_Flexo                   | VendorOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_Flexo                   | VendorOwner_Flexo         |
      | PurchasingResponsible_ALL_Flexo                         | VendorOwner               |
      | PurchasingResponsible_ALL_Flexo                         | VendorOwner_Flexo         |
      | PurchasingResponsible_And_LogisticResponsible           | VendorOwner               |
      | PurchasingResponsible_And_LogisticResponsible           | VendorViewer              |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | VendorOwner_Signmedia     |
      | PurchasingResponsible_Signmedia_And_LogisticResponsible | VendorViewer              |
      | SuperUser                                               | SuperUserSubRole          |
      | CannotReadSectionsRole                                  | CannotReadSectionsSubRole |

      | Accountant_Flexo                                        | VendorViewer_Flexo        |
      | Accountant_Corrugated                                   | VendorViewer_Corrugated   |
      | Accountant_Offset                                       | VendorViewer_Offset       |
      | Accountant_Textile                                      | VendorViewer_Textile      |
      | Accountant_Digital                                      | VendorViewer_Digital      |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                   | Condition                       |
      | CreateOnlySubRole         | Vendor:Create                |                                 |
      | VendorOwner               | Vendor:ReadAll               |                                 |
      | VendorOwner               | Vendor:ReadGeneralData       |                                 |
      | VendorOwner               | Vendor:UpdateGeneralData     |                                 |
      | VendorOwner               | Vendor:Delete                |                                 |
      | VendorOwner               | Vendor:Create                |                                 |
      | VendorOwner               | Vendor:ReadAccountingDetails |                                 |
      | VendorOwner_Signmedia     | Vendor:ReadAll               | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia     | Vendor:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia     | Vendor:UpdateGeneralData     | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia     | Vendor:Delete                | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia     | Vendor:Create                |                                 |
      | VendorOwner_Signmedia     | Vendor:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo         | Vendor:ReadAll               | [purchaseUnitName='Flexo']      |
      | VendorOwner_Flexo         | Vendor:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | VendorOwner_Flexo         | Vendor:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | VendorOwner_Flexo         | Vendor:Delete                | [purchaseUnitName='Flexo']      |
      | VendorOwner_Flexo         | Vendor:Create                |                                 |
      | VendorOwner_Flexo         | Vendor:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated    | Vendor:ReadAll               | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Corrugated    | Vendor:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Corrugated    | Vendor:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Corrugated    | Vendor:Delete                | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Corrugated    | Vendor:Create                |                                 |
      | VendorOwner_Corrugated    | Vendor:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Offset        | Vendor:ReadAccountingDetails | [purchaseUnitName='Offset']     |
      | VendorOwner_Textile       | Vendor:ReadAccountingDetails | [purchaseUnitName='Textile']    |
      | VendorOwner_Digital       | Vendor:ReadAccountingDetails | [purchaseUnitName='Digital']    |

      | VendorViewer_Flexo        | Vendor:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | VendorViewer_Corrugated   | Vendor:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | VendorViewer_Offset       | Vendor:ReadAccountingDetails | [purchaseUnitName='Offset']     |
      | VendorViewer_Textile      | Vendor:ReadAccountingDetails | [purchaseUnitName='Textile']    |
      | VendorViewer_Digital      | Vendor:ReadAccountingDetails | [purchaseUnitName='Digital']    |

      | VendorViewer              | Vendor:ReadAll               |                                 |
      | VendorViewer              | Vendor:ReadGeneralData       |                                 |
      | VendorViewer              | Vendor:ReadAccountingDetails |                                 |
      | VendorViewer_Signmedia    | Vendor:ReadAll               | [purchaseUnitName='Signmedia']  |
      | VendorViewer_Signmedia    | Vendor:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | VendorViewer_Signmedia    | Vendor:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | SuperUserSubRole          | *:*                          |                                 |
      | CannotReadSectionsSubRole | Vendor:Delete                |                                 |

    And the following users doesn't have the following permissions:
      | User               | Permission                   |
      | CreateOnlyUser     | Vendor:ReadAll               |
      | CreateOnlyUser     | Vendor:ReadGeneralData       |
      | CreateOnlyUser     | Vendor:UpdateGeneralData     |
      | CreateOnlyUser     | Vendor:Delete                |
      | CreateOnlyUser     | Vendor:ReadAccountingDetails |
      | Afaf               | Vendor:ReadAll               |
      | Afaf               | Vendor:ReadGeneralData       |
      | Afaf               | Vendor:UpdateGeneralData     |
      | Afaf               | Vendor:Delete                |
      | Afaf               | Vendor:Create                |
      | Afaf               | Vendor:ReadAccountingDetails |
      | CannotReadSections | Vendor:ReadAll               |
      | CannotReadSections | Vendor:ReadGeneralData       |
      | CannotReadSections | Vendor:ReadAccountingDetails |
      | CannotReadSections | Vendor:Create                |

    And the following users have the following permissions without the following conditions:
      | User        | Permission                   | Condition                       |
      | Gehan.Ahmed | Vendor:ReadAll               | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:UpdateGeneralData     | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:Delete                | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadAccountingDetails | [purchaseUnitName='Flexo']      |

      | Gehan.Ahmed | Vendor:ReadAll               | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Vendor:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Vendor:UpdateGeneralData     | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Vendor:Delete                | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Vendor:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |

      | Amr.Khalil  | Vendor:ReadAll               | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Vendor:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Vendor:UpdateGeneralData     | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Vendor:Delete                | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Vendor:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |

    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit                 |
      | 000001 | Siegwerk | 0001 - Flexo                   |
      | 000002 | Zhejiang | 0002 - Signmedia               |
      | 000008 | Vendor 2 | 0002 - Signmedia, 0001 - Flexo |
      | 000009 | Vendor 3 | 0002 - Signmedia               |
      | 000010 | Vendor 4 | 0001 - Flexo                   |
      | 000011 | Vendor 5 | 0006 - Corrugated              |

  # One Role --> One Subrole without condition (Scenario 01)

  Scenario Outline: (01) Vendor Allowed Actions, One Role (PurchaseResponsible) --> One Subrole with no condition (VendorOwner)
    Given user is logged in as "VendorOwnerUser"
    When "VendorOwnerUser" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "VendorOwnerUser":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000001 | 0001           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |
      | 000011 | 0006           |

  # One Role --> One Subrole with condition (Scenario 02)
  Scenario Outline: (02) Vendor Allowed Actions, One Role (PurchaseResponsible_Signmedia) --> One Subrole with condition (VendorOwner_Signmedia)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "Gehan.Ahmed":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |

  # Two Roles --> Each with one Subrole with condition - similar subroles (Scenario 03)

  Scenario Outline: (03) Vendor Allowed Actions, Two Roles (PurchaseResponsible_Signmedia \ VendorOwner_Signmedia) and (PurchaseResponsible_Flexo \ VendorOwner_Flexo)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "Amr.Khalil":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0001           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |
      | 000011 | 0006           |
  # Two Roles --> Each with one subrole, one with condition and the other without -similar subroles (Scenario 04)

  Scenario Outline: (04) Vendor Allowed Actions, VendorOwner with two roles one with condition and another without condition
    Given user is logged in as "TestUser1"
    When "TestUser1" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser1":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000001 | 0001           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |
      | 000011 | 0006           |
  # Two Roles --> Each with one subrole with condition - differet subroles (Scenario 05)

  Scenario Outline: (05) Vendor Allowed Actions, VendorOwner with condition and VendorViewer with condition
    Given user is logged in as "TestUser2"
    When "TestUser2" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser2":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |

  Scenario Outline: (06) Vendor Allowed Actions, VendorOwner with condition and VendorViewer without condition
    Given user is logged in as "TestUser3"
    When "TestUser3" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser3":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |

  Scenario Outline: (07) Vendor Allowed Actions, VendorOwner with condition and VendorViewer without condition
    Given user is logged in as "TestUser3"
    When "TestUser3" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser3":
      | AllowedActions        |
      | ReadGeneralData       |
      | ReadAll               |
      | ReadAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0001           |
      | 000010 | 0001           |
      | 000011 | 0006           |

  # One Role --> Two Subroles with condition - Similar Subroles (Scenario 08)
  Scenario Outline: (08) Vendor Allowed Actions, VendorOwner with condition and VendorViewer without condition
    Given user is logged in as "TestUser4"
    When "TestUser4" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser4":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000001 | 0001           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |

  # One Role --> Two Subroles one with condition and the other without - Similar Subroles (Scenario 09)
  Scenario Outline: (09) Vendor Allowed Actions, VendorOwner with condition and VendorViewer without condition
    Given user is logged in as "TestUser5"
    When "TestUser5" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser5":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000001 | 0001           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |
      | 000011 | 0006           |

  # One Role --> Two Subroles - Different Subroles with no conditions (Scenario 10)
  Scenario Outline: (10) Vendor Allowed Actions, VendorOwner with condition and VendorViewer without condition
    Given user is logged in as "TestUser6"
    When "TestUser6" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser6":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000001 | 0001           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |
      | 000010 | 0001           |
      | 000011 | 0006           |

  Scenario Outline: (11) Vendor Allowed Actions, One Role --> Two Subroles - Different Subroles with conditions - Part 1
    Given user is logged in as "TestUser7"
    When "TestUser7" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser7":
      | AllowedActions          |
      | ReadGeneralData         |
      | UpdateGeneralData       |
      | Delete                  |
      | ReadAll                 |
      | ReadAccountingDetails   |
      | UpdateAccountingDetails |

    Examples:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
      | 000009 | 0002           |
      | 000008 | 0001, 0002     |

  Scenario Outline: (12) Vendor Allowed Actions, One Role --> Two Subroles - Different Subroles with conditions - Part 2
    Given user is logged in as "TestUser7"
    When "TestUser7" requests to read actions of Vendor with code "<Code>"
    Then the following actions are displayed to "TestUser7":
      | AllowedActions        |
      | ReadGeneralData       |
      | ReadAll               |
      | ReadAccountingDetails |
    Examples:
      | Code   | PurchasingUnit |
      | 000001 | 0001           |
      | 000010 | 0001           |
      | 000011 | 0006           |


  Scenario: (13) Vendor Allowed Actions, unauthorized user requests to read actions of Vendor
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read actions of Vendor with code "000011"
    Then an authorization error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-29"

  Scenario: (14) Vendor Allowed Actions, user requests to read actions of Vendor that not exist
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000052" successfully
    When "Gehan.Ahmed" requests to read actions of Vendor with code "000052"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"


  Scenario: (15) Vendor Allowed Actions, User who is authorized to read only some sections
    Given user is logged in as "CannotReadSections"
    When "CannotReadSections" requests to read actions of Vendor with code "000002"
    Then the following actions are displayed to "CannotReadSections":
      | AllowedActions |
      | Delete         |

  Scenario Outline: (16) Vendor Allowed Actions, home screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Vendor home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User            | AllowedActions |
      # One Role --> One Subrole
      | VendorOwnerUser | ReadAll,Create |
      # Two Roles --> similar subroles
      | Amr.Khalil      | ReadAll,Create |
      # Two Roles --> differet subroles
      | TestUser2       | ReadAll,Create |
      # One Role --> One Subrole- viewer
      | TestUser8       | ReadAll        |
      # SuperUser
      | hr1             | ReadAll,Create |
      # user authorized to create only
      | CreateOnlyUser  | Create         |

  Scenario: (17) Vendor Allowed Actions, unauthorized user requests to read actions of Vendor
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Vendor home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
