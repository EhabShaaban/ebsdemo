# Author: Gehad Shady
# Reviewer: Somaya Ahmed
# updated by: Fatma Al Zahraa (EBS - 7423)


Feature: View All Vendors dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | TestUser3   | LogisticsResponsible            |
      | Afaf        | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole               |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia |
      | LogisticsResponsible            | VendorViewer          |

    And the following sub-roles and permissions exist:
      | Subrole               | Permission     | Condition                      |
      | VendorOwner_Signmedia | Vendor:ReadAll | [purchaseUnitName='Signmedia'] |
      | VendorViewer          | Vendor:ReadAll |                                |

    And the following users doesn't have the following permissions:
      | User | Permission     |
      | Afaf | Vendor:ReadAll |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |

    And the following Vendors exist:
      | Code   | Name              | PurchasingUnit                                                                                   |
      | 000055 | Zhejiang 6        | 0002 - Signmedia                                                                                 |
      | 000054 | Zhejiang 5        | 0002 - Signmedia                                                                                 |
      | 000013 | Zhejiang 4        | 0002 - Signmedia                                                                                 |
      | 000053 | Zhejiang          | 0002 - Signmedia                                                                                 |
      | 000052 | Zhejiang          | 0002 - Signmedia                                                                                 |
      | 000011 | Vendor 5          | 0006 - Corrugated                                                                                |
      | 000010 | Vendor 4          | 0001 - Flexo                                                                                     |
      | 000009 | Vendor 3          | 0002 - Signmedia                                                                                 |
      | 000008 | Vendor 2          | 0002 - Signmedia, 0001 - Flexo                                                                   |
      | 000007 | Vendor 1          | 0006 - Corrugated                                                                                |
      | 000006 | Vendor Corrugated | 0006 - Corrugated                                                                                |
      | 000005 | Vendor Textile    | 0005 - Textile                                                                                   |
      | 000004 | Vendor Digital    | 0004 - Digital                                                                                   |
      | 000003 | ABC               | 0003 - Offset                                                                                    |
      | 000002 | Zhejiang          | 0002 - Signmedia                                                                                 |
      | 000001 | Siegwerk          | 0001 - Flexo                                                                                     |
      | 000051 | Vendor 6          | 0001 - Flexo, 0002 - Signmedia, 0003 - Offset, 0004 - Digital, 0005 - Textile, 0006 - Corrugated |
    And the total number of existing Vendors are 18

  Scenario: (01) Read list of Vendors dropdown, by authorized with condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all Vendors
    Then the following Vendors values will be presented to "Gehan.Ahmed":
      | Vendor              |
      | 000055 - Zhejiang 6 |
      | 000054 - Zhejiang 5 |
      | 000053 - Zhejiang   |
      | 000052 - Zhejiang   |
      | 000051 - Vendor 6   |
      | 000013 - Zhejiang 4 |
      | 000009 - Vendor 3   |
      | 000008 - Vendor 2   |
      | 000002 - Zhejiang   |
    And total number of Vendors returned to "Gehan.Ahmed" is equal to 9

  #EBS - 7423
  Scenario: (02) Read list of Vendors dropdown by authorized user with selected Business Unit (Happy Path)
    Given user is logged in as "TestUser3"
    When "TestUser3" requests to read all Vendors That related to Business Unit with code "0002"
    Then the following Vendors values will be presented to "TestUser3":
      | Vendor              |
      | 000055 - Zhejiang 6 |
      | 000054 - Zhejiang 5 |
      | 000053 - Zhejiang   |
      | 000052 - Zhejiang   |
      | 000051 - Vendor 6   |
      | 000013 - Zhejiang 4 |
      | 000009 - Vendor 3   |
      | 000008 - Vendor 2   |
      | 000002 - Zhejiang   |
    And total number of Vendors returned to "TestUser3" is equal to 9

  @Future
  Scenario: (03) Read list of Vendors dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Vendors
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page