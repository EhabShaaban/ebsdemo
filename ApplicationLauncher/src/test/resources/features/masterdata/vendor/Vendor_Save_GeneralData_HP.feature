# Author: Shrouk Alaa, Marina on 18-Dec-2018
# Reviewer: Hosam Bayomy, Somaya Ahmed on 19-Dec-2018

Feature: Save Vendor GeneralData HP

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
      | PurchasingResponsible_Flexo     | VendorOwner_Flexo       |
      | PurchasingResponsible_Flexo     | PurUnitReader_Flexo     |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission               | Condition                      |
      | VendorOwner_Signmedia   | Vendor:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Flexo       | Vendor:UpdateGeneralData | [purchaseUnitName='Flexo']     |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll   |                                |
      | PurUnitReader_Flexo     | PurchasingUnit:ReadAll   |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                      |
      | Gehan.Ahmed | Vendor:UpdateGeneralData | [purchaseUnitName='Flexo']     |
      | Amr.Khalil  | Vendor:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
    And the following GeneralData for Vendors exist:
      | VendorCode | CreationDate         | VendorName | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType                  |
      | 000001     | 02-Aug-2018 07:51 AM | Siegwerk   | 0001 - Flexo                   | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      | COMMERCIAL - Commercial     |
      | 000002     | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      | COMMERCIAL - Commercial     |
      | 000007     | 06-Aug-2018 10:43 AM | Vendor 1   | 0006 - Corrugated              | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      | COMMERCIAL - Commercial     |
      | 000008     | 06-Aug-2018 10:43 AM | Vendor 2   | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      | COMMERCIAL - Commercial     |
      | 000013     | 02-Aug-2018 07:51 AM | Zhejiang 4 | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 322222222222222    | 2-ABC-Madina      | CONSUMABLE - Consumable     |
      | 000054     | 02-Aug-2018 07:51 AM | Zhejiang 5 | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 822222222222222    | 2-ABC-Madina      | FIXED_ASSETS - Fixed Assets |
      | 000055     | 02-Aug-2018 07:51 AM | Zhejiang 6 | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 622222222222222    | 2-ABC-Madina      | SERVICE - Service           |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0006 | Corrugated |
    And the following purchase responsible exist:
      | Code | Name          |
      | 0001 | Amr.Khalil    |
      | 0002 | Gehan.Ahmed   |
      | 0003 | Marwa.Tawfeek |
    And the following IVR exist:
      | ItemCode | VendorCode | PurchasingUnit |
      | 000051   | 000008     | 0002           |
    And edit session is "30" minutes

  ####### Save GeneralData section (Happy Path) ##############################################################
  #EBS-2029
  @ResetData
  Scenario Outline: (01) Save Vendor GeneralData section, within edit session by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And GeneralData section of Vendor with code "<VendorCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" saves GeneralData section of Vendor with code "<VendorCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | Name                 | PurchasingUnit           | PurchasingResponsible           | AccountWithVendor           |
      | <NewVendorNameValue> | <NewPurchasingUnitValue> | <NewPurchasingResponsibleValue> | <NewAccountWithVendorValue> |
    Then Vendor with code "<VendorCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | VendorCode   | CreationDate        | VendorName           | BusinessUnit        | PurchasingResponsible        | OldVendorReference        | AccountWithVendor           | VendorType   |
      | <User>        | 07-Jan-2019 09:30 AM | <VendorCode> | <CreationDateValue> | <NewVendorNameValue> | <BusinessUnitValue> | <PurchasingResponsibleValue> | <OldVendorReferenceValue> | <NewAccountWithVendorValue> | <VendorType> |
    And the lock by "<User>" on GeneralData section of Vendor with code "<VendorCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | User        | VendorCode | CreationDateValue    | NewVendorNameValue    | NewPurchasingUnitValue | NewPurchasingResponsibleValue | OldVendorReferenceValue | NewAccountWithVendorValue | VendorType                  | BusinessUnitValue                              | PurchasingResponsibleValue |
      | Gehan.Ahmed | 000002     | 02-Aug-2018 07:51 AM | Gehan Zhejiang        | 0002, 0001, 0004       | 0001                          | 522222222222222         | Test2-ABC-Madina          | COMMERCIAL - Commercial     | 0002 - Signmedia, 0001 - Flexo, 0004 - Digital | 0001 - Amr.Khalil          |
      | Gehan.Ahmed | 000002     | 02-Aug-2018 07:51 AM | Gehan Again Zhejiang  | 0002, 0001, 0004       | 0001                          | 522222222222222         |                           | COMMERCIAL - Commercial     | 0002 - Signmedia, 0001 - Flexo, 0004 - Digital | 0001 - Amr.Khalil          |
      | Gehan.Ahmed | 000008     | 06-Aug-2018 10:43 AM | Test Vendor 2         | 0001, 0002, 0004       | 0002                          | 888888888888888         | Test7-ABC-Madina          | COMMERCIAL - Commercial     | 0001 - Flexo, 0002 - Signmedia, 0004 - Digital | 0002 - Gehan.Ahmed         |
      | Amr.Khalil  | 000001     | 02-Aug-2018 07:51 AM | Test Siegwerk         | 0006                   | 0002                          | 111111111111111         | Test1-ABC-Madina          | COMMERCIAL - Commercial     | 0006 - Corrugated                              | 0002 - Gehan.Ahmed         |
      | Amr.Khalil  | 000007     | 06-Aug-2018 10:43 AM | Test Vendor 1         | 0001                   | 0002                          | 777777777777777         | Test7-ABC-Madina          | COMMERCIAL - Commercial     | 0001 - Flexo                                   | 0002 - Gehan.Ahmed         |
      | Gehan.Ahmed | 000008     | 06-Aug-2018 10:43 AM | Another Test Vendor 2 | 0001, 0002, 0003       | 0003                          | 888888888888888         | AnotherTest7-ABC-Madina   | COMMERCIAL - Commercial     | 0001 - Flexo, 0002 - Signmedia, 0003 - Offset  | 0003 - Marwa.Tawfeek       |
      | Gehan.Ahmed | 000013     | 02-Aug-2018 07:51 AM | Another Test Vendor 2 | 0002, 0003             | 0002                          | 322222222222222         | AnotherTest7-ABC-Madina   | CONSUMABLE - Consumable     | 0002 - Signmedia, 0003 - Offset                | 0002 - Gehan.Ahmed         |
      | Gehan.Ahmed | 000054     | 02-Aug-2018 07:51 AM | Another Test Vendor 2 | 0002, 0003             | 0002                          | 822222222222222         | AnotherTest7-ABC-Madina   | FIXED_ASSETS - Fixed Assets | 0002 - Signmedia, 0003 - Offset                | 0002 - Gehan.Ahmed         |
      | Gehan.Ahmed | 000055     | 02-Aug-2018 07:51 AM | Another Test Vendor 2 | 0002, 0003             | 0002                          | 622222222222222         | AnotherTest7-ABC-Madina   | SERVICE - Service           | 0002 - Signmedia, 0003 - Offset                | 0002 - Gehan.Ahmed         |

  #EBS-2976
  @ResetData
  Scenario: (02) Save Vendor GeneralData section, Delete purchasing unit of item that hasn't record in IVR  (Happy Path) User two roles
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000008" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000008" at "07-Jan-2019 09:30 AM" with the following values:
      | Name     | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Vendor 2 | 0002           | 0001                  |                   |
    Then Vendor with code "000008" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | VendorCode | CreationDate         | VendorName | BusinessUnit     | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType              |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 000008     | 06-Aug-2018 10:43 AM | Vendor 2   | 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    |                   | COMMERCIAL - Commercial |
    And the lock by "Gehan.Ahmed" on GeneralData section of Vendor with code "000008" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"