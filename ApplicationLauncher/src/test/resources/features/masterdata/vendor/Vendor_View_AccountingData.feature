Feature: View Vendor AccountingInfo section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo      |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission                   | Condition                       |
      | VendorOwner_Signmedia  | Vendor:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia  | Vendor:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo      | Vendor:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated | Vendor:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole       | *:*                          |                                 |

    And the following users doesn't have the following permissions:
      | User | Permission                   |
      | Afaf | Vendor:ReadGeneralData       |
      | Afaf | Vendor:ReadAccountingDetails |

    And the following users have the following permissions without the following conditions:
      | User        | Permission                   | Condition                       |
      | Gehan.Ahmed | Vendor:ReadGeneralData       | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadGeneralData       | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | Vendor:ReadAccountingDetails | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Vendor:ReadGeneralData       | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | Vendor:ReadAccountingDetails | [purchaseUnitName='Signmedia']  |

    And the following AccountingInfo for Vendors exist:
      | VendorCode | AccountCode | AccountName     | BusinessUnit |
      | 000001     | 10201       | Service Vendors | 0001         |

    # EBS-1383
  Scenario: (01) View Vendor AccountingInfo section, by an authorized user (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view AccountingInfo section of Vendor with code "000001"
    Then the following values of AccountingInfo section for Vendor with code "000001" are displayed to "Amr.Khalil":
      | AccountCode | AccountName     |
      | 10201       | Service Vendors |


  Scenario: (02) View Vendor AccountingInfo section, where Vendor doesn't exist (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor with code "000052" successfully
    When "Amr.Khalil" requests to view AccountingInfo section of Vendor with code "000052"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  Scenario: (03) View Vendor AccountingInfo section, an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view AccountingInfo section of Vendor with code "000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (04) View Vendor AccountingInfo section, by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view AccountingInfo section of Vendor with code "000001"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page