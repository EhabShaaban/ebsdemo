# Reviewer: hend and Somaya (06-Feb-2019 2:30 am)
# updated by: Eman Hesham , Reviewer: Somaya & Hosam (EBS - 6238)

Feature: View all Vendors

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | M.D                              | VendorViewer             |
      | M.D                              | VendorTypeViewer         |
      | M.D                              | PurUnitReader            |
      | M.D                              | PurRespReader            |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia    |
      | PurchasingResponsible_Signmedia  | VendorTypeViewer         |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | PurRespReader            |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo        |
      | PurchasingResponsible_Flexo      | VendorTypeViewer         |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | PurRespReader            |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated   |
      | PurchasingResponsible_Corrugated | VendorTypeViewer         |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | PurRespReader            |

    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                    | Condition                       |
      | VendorViewer             | Vendor:ReadAll                |                                 |
      | VendorOwner_Signmedia    | Vendor:ReadAll                | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo        | Vendor:ReadAll                | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated   | Vendor:ReadAll                | [purchaseUnitName='Corrugated'] |
      | VendorTypeViewer         | VendorType:ReadAll            |                                 |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll        |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll        |                                 |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll        |                                 |
      | PurRespReader            | PurchasingResponsible:ReadAll |                                 |

    And the following users doesn't have the following permissions:
      | User | Permission                    |
      | Afaf | Vendor:ReadAll                |
      | Afaf | VendorType:ReadAll            |
      | Afaf | PurchasingUnit:ReadAll        |
      | Afaf | PurchasingResponsible:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User        | Permission     | Condition                       |
      | Gehan.Ahmed | Vendor:ReadAll | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | Vendor:ReadAll | [purchaseUnitName='Corrugated'] |
      | Amr.Khalil  | Vendor:ReadAll | [purchaseUnitName='Signmedia']  |

    And the following ViewAll data for Vendors exist:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible  | OldVendorReference | AccountWithVendor |
      | 000001     | COMMERCIAL - Commercial | Siegwerk          | 0001 - Flexo                    | 0001 - Amr.Khalil      | 111111111111111    | 1-ABC-Madina      |
      | 000002     | COMMERCIAL - Commercial | Zhejiang          | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 522222222222222    | 2-ABC-Madina      |
      | 000003     | COMMERCIAL - Commercial | ABC               | 0003 - Offset                   | 0003 - Marwa.Tawfeek   | 333333333333333    | 3-ABC-Madina      |
      | 000004     | COMMERCIAL - Commercial | Vendor Digital    | 0004 - Digital                  | 0004 - Asmaa.Elkhodary | 444444444444444    | 4-ABC-Madina      |
      | 000005     | COMMERCIAL - Commercial | Vendor Textile    | 0005 - Textile                  | 0001 - Amr.Khalil      | 555555555555555    | 5-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil      | 666666666666666    | 6-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil      | 777777777777777    | 7-ABC-Madina      |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil      | 888888888888888    | 8-ABC-Madina      |
      | 000009     | COMMERCIAL - Commercial | Vendor 3          | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 999999999999999    | 9-ABC-Madina      |
      | 000010     | COMMERCIAL - Commercial | Vendor 4          | 0001 - Flexo                    | 0001 - Amr.Khalil      | 101010101010101    | 10-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil      | 110110110110111    | 11-ABC-Madina     |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil      | 11011011011011     | 11-ABC-Madina     |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4        | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 322222222222222    | 2-ABC-Madina      |
    And the total number of existing Vendors are 13

  # EBS - 6238
  Scenario: (01) View all Vendors, by an authorized user WITHOUT condition  (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view all Vendors in Home Screen
    Then the following values will be presented to "Ashraf.Fathi" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible  | OldVendorReference | AccountWithVendor |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4        | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 322222222222222    | 2-ABC-Madina      |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil      | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil      | 110110110110111    | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4          | 0001 - Flexo                    | 0001 - Amr.Khalil      | 101010101010101    | 10-ABC-Madina     |
      | 000009     | COMMERCIAL - Commercial | Vendor 3          | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 999999999999999    | 9-ABC-Madina      |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil      | 888888888888888    | 8-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil      | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil      | 666666666666666    | 6-ABC-Madina      |
      | 000005     | COMMERCIAL - Commercial | Vendor Textile    | 0005 - Textile                  | 0001 - Amr.Khalil      | 555555555555555    | 5-ABC-Madina      |
      | 000004     | COMMERCIAL - Commercial | Vendor Digital    | 0004 - Digital                  | 0004 - Asmaa.Elkhodary | 444444444444444    | 4-ABC-Madina      |
      | 000003     | COMMERCIAL - Commercial | ABC               | 0003 - Offset                   | 0003 - Marwa.Tawfeek   | 333333333333333    | 3-ABC-Madina      |
      | 000002     | COMMERCIAL - Commercial | Zhejiang          | 0002 - Signmedia                | 0002 - Gehan.Ahmed     | 522222222222222    | 2-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk          | 0001 - Flexo                    | 0001 - Amr.Khalil      | 111111111111111    | 1-ABC-Madina      |
    And the total number of records presented to "Ashraf.Fathi" in Home Screen are 13

# EBS - 6238
  Scenario: (02) View all Vendors, by an authorized user WITH condition: User has ONE role  (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view all Vendors in Home Screen
    Then the following values will be presented to "Gehan.Ahmed" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4 | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 322222222222222    | 2-ABC-Madina      |
      | 000009     | COMMERCIAL - Commercial | Vendor 3   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 999999999999999    | 9-ABC-Madina      |
      | 000008     | COMMERCIAL - Commercial | Vendor 2   | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000002     | COMMERCIAL - Commercial | Zhejiang   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      |
    And the total number of records presented to "Gehan.Ahmed" in Home Screen are 4

    # EBS - 6238
  Scenario: (03) View all Vendors, by an authorized user WITH condition: User has TWO role  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view all Vendors in Home Screen
    Then the following values will be presented to "Amr.Khalil" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4          | 0001 - Flexo                    | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil     | 666666666666666    | 6-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk          | 0001 - Flexo                    | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records presented to "Amr.Khalil" in Home Screen are 7

    # EBS - 6238
  Scenario: (04) View all Vendors, by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view all Vendors in Home Screen
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

        #### Filter with ---> Vendor Code - String - Contains

    #EBS-3512
  Scenario: (05) View all Vendors, - Filter with Vendor Code - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on VendorCode which contains "00001" with 8 records per page
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4 | 0002 - Signmedia                | 0002 - Gehan.Ahmed    | 322222222222222    | 2-ABC-Madina      |
      | 000012     | COMMERCIAL - Commercial | Vendor 6   | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5   | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4   | 0001 - Flexo                    | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
      | 000001     | COMMERCIAL - Commercial | Siegwerk   | 0001 - Flexo                    | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "hr1" are 5

    #EBS-3512
  Scenario: (06) View all Vendors, - Filter with Vendor Code - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on VendorCode which contains "2" with 8 records per page
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6   | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000002     | COMMERCIAL - Commercial | Zhejiang   | 0002 - Signmedia                | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      |
    And the total number of records in search results by "hr1" are 2

    #EBS-3512
  Scenario: (07) View all Vendors, - Filter with Vendor Code - by authorized user due to condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendors with filter applied on VendorCode which contains "00003" with 8 records per page
    Then no values are displayed to "Gehan.Ahmed" and empty grid state is viewed in Home Screen
    And the total number of records in search results by "Gehan.Ahmed" are 0

    #EBS-3512
  Scenario: (08) View all Vendors, - Filter with Vendor Code - by authorized user with 2 roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Vendors with filter applied on VendorCode which contains "00000" with 8 records per page
    Then the following values will be presented to "Amr.Khalil" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated              | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated              | 0001 - Amr.Khalil     | 666666666666666    | 6-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk          | 0001 - Flexo                   | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "Amr.Khalil" are 4

#    #### Filter with ---> Account with vendor - String - Contains

    #EBS-3512
  Scenario: (09) View all Vendors, - Filter with Account with vendor - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on AccountWithVendor which contains "0" with 8 records per page
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000010     | COMMERCIAL - Commercial | Vendor 4   | 0001 - Flexo | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
    And the total number of records in search results by "hr1" are 1

    #EBS-3512
  Scenario: (10) View all Vendors, - Filter with Account with vendor - Case Senistive - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of Vendors with filter applied on AccountWithVendor which contains "mADina" with 8 records per page
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName     | BusinessUnit     | PurchasingResponsible  | OldVendorReference | AccountWithVendor |
      | 000005     | COMMERCIAL - Commercial | Vendor Textile | 0005 - Textile   | 0001 - Amr.Khalil      | 555555555555555    | 5-ABC-Madina      |
      | 000004     | COMMERCIAL - Commercial | Vendor Digital | 0004 - Digital   | 0004 - Asmaa.Elkhodary | 444444444444444    | 4-ABC-Madina      |
      | 000003     | COMMERCIAL - Commercial | ABC            | 0003 - Offset    | 0003 - Marwa.Tawfeek   | 333333333333333    | 3-ABC-Madina      |
      | 000002     | COMMERCIAL - Commercial | Zhejiang       | 0002 - Signmedia | 0002 - Gehan.Ahmed     | 522222222222222    | 2-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk       | 0001 - Flexo     | 0001 - Amr.Khalil      | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "hr1" are 13


    #### Filter with ---> Old Vendor Number -- String Contains

  Scenario: (11) View all Vendors, - Filter with old vendor number - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on OldVendorNumber which contains "11011011011011" with 10 records per page
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6   | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5   | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
    And the total number of records in search results by "hr1" are 2

  #### Filter with ---> Vendor Name - Json - Contains - En

  Scenario: (12) View all Vendors, - Filter with vendor name in English - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on VendorName which contains "Vendor" with 8 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4          | 0001 - Flexo                    | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
      | 000009     | COMMERCIAL - Commercial | Vendor 3          | 0002 - Signmedia                | 0002 - Gehan.Ahmed    | 999999999999999    | 9-ABC-Madina      |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil     | 666666666666666    | 6-ABC-Madina      |
      | 000005     | COMMERCIAL - Commercial | Vendor Textile    | 0005 - Textile                  | 0001 - Amr.Khalil     | 555555555555555    | 5-ABC-Madina      |
    And the total number of records in search results by "hr1" are 9

  Scenario: (13) View all Vendors, - Filter with vendor name in Arabic - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on VendorName which contains "زاجاننج" with 8 records per page while current locale is "ar-EG"
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit     | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4 | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 322222222222222    | 2-ABC-Madina      |
      | 000002     | COMMERCIAL - Commercial | Zhejiang   | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      |
    And the total number of records in search results by "hr1" are 2

    #### Filter with ---> Purchase Responsible Name

  Scenario: (14) View all Vendors, - Filter with Purchase Responsible - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on PurchaseResponsible which contains "Amr.Khalil" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4          | 0001 - Flexo                    | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
      | 000008     | COMMERCIAL - Commercial | Vendor 2          | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil     | 666666666666666    | 6-ABC-Madina      |
      | 000005     | COMMERCIAL - Commercial | Vendor Textile    | 0005 - Textile                  | 0001 - Amr.Khalil     | 555555555555555    | 5-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk          | 0001 - Flexo                    | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "hr1" are 8

  #### Filter with ---> Purchase Unit Name

  Scenario: (15) View all Vendors, - Filter with Purchase Unit - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on PurchaseUnit which contains "Flexo" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6   | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000010     | COMMERCIAL - Commercial | Vendor 4   | 0001 - Flexo                    | 0001 - Amr.Khalil     | 101010101010101    | 10-ABC-Madina     |
      | 000008     | COMMERCIAL - Commercial | Vendor 2   | 0001 - Flexo, 0002 - Signmedia  | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
      | 000001     | COMMERCIAL - Commercial | Siegwerk   | 0001 - Flexo                    | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "hr1" are 4

  Scenario: (16) View all Vendors, - Filter with Purchase Unit - by authorized user due to condition (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Vendors with filter applied on PurchaseUnit which contains "Corrugated" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "Amr.Khalil" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName        | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6          | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
      | 000007     | COMMERCIAL - Commercial | Vendor 1          | 0006 - Corrugated               | 0001 - Amr.Khalil     | 777777777777777    | 7-ABC-Madina      |
      | 000006     | COMMERCIAL - Commercial | Vendor Corrugated | 0006 - Corrugated               | 0001 - Amr.Khalil     | 666666666666666    | 6-ABC-Madina      |
    And the total number of records in search results by "Amr.Khalil" are 4

  #### Filter with ---> Multiple - Filters

  Scenario: (17) View all Vendors, - Multi Filter - by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with 3 records per page and the following filters applied on Vendor while current locale is "en-US"
      | FieldName  | Operation | Value    |
      | vendorCode | contains  | 00001    |
      | vendorName | contains  | Siegwerk |
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000001     | COMMERCIAL - Commercial | Siegwerk   | 0001 - Flexo | 0001 - Amr.Khalil     | 111111111111111    | 1-ABC-Madina      |
    And the total number of records in search results by "hr1" are 1

  Scenario: (18) View all Vendors, - Multi Filter - by authorized user due to condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendors with 3 records per page and the following filters applied on Vendor while current locale is "en-US"
      | FieldName         | Operation | Value  |
      | vendorCode        | contains  | 00000  |
      | vendorName        | contains  | Vendor |
      | accountWithVendor | contains  | ABC    |
    Then the following values will be presented to "Gehan.Ahmed" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                   | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000009     | COMMERCIAL - Commercial | Vendor 3   | 0002 - Signmedia               | 0002 - Gehan.Ahmed    | 999999999999999    | 9-ABC-Madina      |
      | 000008     | COMMERCIAL - Commercial | Vendor 2   | 0001 - Flexo, 0002 - Signmedia | 0001 - Amr.Khalil     | 888888888888888    | 8-ABC-Madina      |
    And the total number of records in search results by "Gehan.Ahmed" are 2

  Scenario: (19) View all Vendors, - Multi Filter - by authorized user with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of Vendors with 3 records per page and the following filters applied on Vendor while current locale is "en-US"
      | FieldName         | Operation | Value  |
      | vendorCode        | contains  | 00001  |
      | oldVendorNumber   | contains  | 11     |
      | vendorName        | contains  | Vendor |
      | accountWithVendor | contains  | ABC    |
    Then the following values will be presented to "Amr.Khalil" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit                    | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000012     | COMMERCIAL - Commercial | Vendor 6   | 0001 - Flexo, 0006 - Corrugated | 0001 - Amr.Khalil     | 11011011011011     | 11-ABC-Madina     |
      | 000011     | COMMERCIAL - Commercial | Vendor 5   | 0006 - Corrugated               | 0001 - Amr.Khalil     | 110110110110111    | 11-ABC-Madina     |
    And the total number of records in search results by "Amr.Khalil" are 2

#### Filter with ---> Vendor Type

  Scenario: (20) View all Vendors, - Filter with Vendor Type - by authorized (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendors with filter applied on VendorType which equal "Consumable" with 8 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1" in Vendors Home Screen:
      | VendorCode | VendorType              | VendorName | BusinessUnit     | PurchasingResponsible | OldVendorReference | AccountWithVendor |
      | 000013     | CONSUMABLE - Consumable | Zhejiang 4 | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 322222222222222    | 2-ABC-Madina      |
    And the total number of records in search results by "hr1" are 1