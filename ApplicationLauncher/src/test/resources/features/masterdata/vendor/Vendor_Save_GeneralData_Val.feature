#Author: Aya Sadek on Date: 08-Jan-2019, 11:11 AM
#Reviewer: Hend Ahmed Date: 08-Jan-2019, 02:53 PM

Feature: Save Vendor GeneralData Val

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole               |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole      |
    And the following sub-roles and permissions exist:
      | Subrole               | Permission               | Condition                      |
      | VendorOwner_Signmedia | Vendor:UpdateGeneralData | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole      | *:*                      |                                |
    And the following GeneralData for Vendors exist:
      | VendorCode | CreationDate         | VendorName | BusinessUnit     | PurchasingResponsible | OldVendorReference | AccountWithVendor | VendorType              |
      | 000002     | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 522222222222222    | 2-ABC-Madina      | COMMERCIAL - Commercial |
      | 000052     | 02-Aug-2018 07:51 AM | Zhejiang   | 0002 - Signmedia | 0002 - Gehan.Ahmed    | 722222222222222    | 2-ABC-Madina      | COMMERCIAL - Commercial |
    And the following IVR exist:
      | ItemCode | VendorCode | PurchasingUnit |
      | 000051   | 000008     | 0002           |
    And edit session is "30" minutes

  ###### Save GeneralData section with Incorrect Data (Validation Failure)  ##############################################################
  #EBS-2028
  Scenario Outline: (01) Save Vendor GeneralData section, Incorrect data: Instance doesn't exist for Vendors (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000002" at "07-Jan-2019 09:30 AM" with the following values:
      | Name   | PurchasingUnit   | PurchasingResponsible   | AccountWithVendor   |
      | <Name> | <PurchasingUnit> | <PurchasingResponsible> | <AccountWithVendor> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<TargetField>" field "<ErrorMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor | TargetField             | ErrorMsg      |
      | Test | 9999           | 0001                  | Test7-ABC-Madina  | purchaseUnitCode        | Vendor-msg-01 |
      | Test | 9999, 0002     | 0001                  | Test7-ABC-Madina  | purchaseUnitCode        | Vendor-msg-01 |
      | Test | 0002           | 9999                  | Test7-ABC-Madina  | purchaseResponsibleCode | Vendor-msg-02 |

  #EBS-2976
  Scenario Outline: (02) Save Vendor GeneralData section, Delete purchasing unit of Vendor that has record in IVR with this purchsing unit (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000008" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000008" at "07-Jan-2019 09:30 AM" with the following values:
      | Name   | PurchasingUnit   | PurchasingResponsible | AccountWithVendor |
      | <Name> | <PurchasingUnit> | 0001                  |                   |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<TargetField>" field "<ErrorMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | Name     | PurchasingUnit | ErrorMsg      | TargetField      |
      | Vendor 2 | 0001           | Vendor-msg-03 | purchaseUnitCode |

  ####### Save GeneralData section with Malicious Input  (Abuse Cases )####################################################################
  #EBS-2028
  Scenario Outline: (03) Save Vendor GeneralData section, Malicious Input: Field is string for Vendors (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000002" at "07-Jan-2019 09:30 AM" with the following values:
      | Name                 | PurchasingUnit           | PurchasingResponsible           | AccountWithVendor           |
      | <NewVendorNameValue> | <NewPurchasingUnitValue> | <NewPurchasingResponsibleValue> | <NewAccountWithVendorValue> |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | NewVendorNameValue                                                                                    | NewPurchasingUnitValue | NewPurchasingResponsibleValue | NewAccountWithVendorValue                                                                             |
      | Test                                                                                                  | Ay 7aga                | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test                                                                                                  | 1234564                | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test                                                                                                  | 12345$#$               | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test@                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test#                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test$                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test^                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test&                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test*                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test~                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test+                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test<                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test>                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | "Test"                                                                                                | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | 'TEst'                                                                                                | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | [Test]                                                                                                | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | {Test}                                                                                                | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test?                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test=                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | \|Test                                                                                                | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | \|\|Test                                                                                              | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | \Test                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test!                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test:                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test;                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test¦                                                                                                 | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | TTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest | 0001, 0002             | 0001                          | Test7-ABC-Madina                                                                                      |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test@                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test#                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test$                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test^                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test&                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test*                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test~                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test+                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test<                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test>                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | "Test"                                                                                                |
      | Test                                                                                                  | 0001, 0002             | 0001                          | 'TEst'                                                                                                |
      | Test                                                                                                  | 0001, 0002             | 0001                          | [Test]                                                                                                |
      | Test                                                                                                  | 0001, 0002             | 0001                          | {Test}                                                                                                |
      | TEst                                                                                                  | 0001, 0002             | 0001                          | Test?                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test=                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | \|Test                                                                                                |
      | Test                                                                                                  | 0001, 0002             | 0001                          | \|\|Test                                                                                              |
      | Test                                                                                                  | 0001, 0002             | 0001                          | \Test                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test!                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test:                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test;                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | Test¦                                                                                                 |
      | Test                                                                                                  | 0001, 0002             | 0001                          | TTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest |
      | Test                                                                                                  | 0001, 0002             | Ay 7aga                       | Test7-ABC-Madina                                                                                      |
      | Test                                                                                                  | 0001, 0002             | 1234564                       | Test7-ABC-Madina                                                                                      |
      | Test                                                                                                  | 0001, 0002             | 12345$#$                      | Test7-ABC-Madina                                                                                      |

  ############ Save GeneralData section with missing mandatory fields (Abuse Cases) #########################################################
  #EBS-2028
  Scenario Outline: (04) Save Vendor GeneralData section, Missing mandatory Fields for Vendors  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000002" at "07-Jan-2019 09:30 AM" with the following values:
      | Name                 | PurchasingUnit           | PurchasingResponsible           |
      | <NewVendorNameValue> | <NewPurchasingUnitValue> | <NewPurchasingResponsibleValue> |
    Then the lock by "Gehan.Ahmed" on GeneralData section of Vendor with code "000002" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | NewVendorNameValue | NewPurchasingUnitValue | NewPurchasingResponsibleValue |
      | Test               |                        | 0001                          |
      |                    | 0001, 0002             | 0001                          |
      | Test               | 0001, 0002             |                               |

  ######### Save GeneralData section (After edit session expires) ################################################################################
  #EBS-2028
  Scenario: (05) Save Vendor GeneralData section, after edit session expired for Vendors (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And GeneralData section of Vendor with code "000002" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000002" at "07-Jan-2019 09:41 AM" with the following values:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Test | 0001, 0002     | 0001                  | Test7-ABC-Madina  |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-2028
  @ResetData
  Scenario: (06) Save Vendor GeneralData section, after edit session expired & Vendor is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And GeneralData section of Vendor with code "000052" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the Vendor with code "000052" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" saves GeneralData section of Vendor with code "000052" at "07-Jan-2019 09:45 AM" with the following values:
      | Name | PurchasingUnit | PurchasingResponsible | AccountWithVendor |
      | Test | 0001, 0002     | 0001                  | Test7-ABC-Madina  |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"