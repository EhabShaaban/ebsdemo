# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Create UOM - Authorization

  Background:
    Given the following users and roles exist:
      | Name | Role      |
      | Afaf | FrontDesk |
    And the following users doesn't have the following permissions:
      | User | Permission      |
      | Afaf | Measures:Create |

  # EBS-1499
  Scenario: (01) Create Unit Of Measure, by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" creates Unit Of Measure with the following values:
      | Name    | IsStandard |
      | Package | False      |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page