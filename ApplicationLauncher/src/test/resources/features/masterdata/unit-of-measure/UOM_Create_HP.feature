# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud

Feature: Create UOM - Happy Path

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole       |
      | PurchasingResponsible_Signmedia | MeasuresOwner |
    And the following sub-roles and permissions exist:
      | Subrole       | Permission      | Condition |
      | MeasuresOwner | Measures:Create |           |

  # EBS-3960
  Scenario: (01) Create Unit Of Measure, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created Unit Of Measure was with code "0001"
    When "Gehan.Ahmed" creates Unit Of Measure with the following values:
      | Name    | IsStandard |
      | Package | False      |
    Then a new Unit Of Measure is created with code "0002" as follows in the General Data:
      | CreationDate         | LastUpdateDate       | CreatedBy   | LastUpdatedBy | Name    | Symbol  | IsStandard | State  |
      | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | Gehan.Ahmed | Gehan.Ahmed   | Package | Package | False      | Active |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"