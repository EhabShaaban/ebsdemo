# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Create UOM - Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole       |
      | PurchasingResponsible_Signmedia | MeasuresOwner |
    And the following sub-roles and permissions exist:
      | Subrole       | Permission      | Condition |
      | MeasuresOwner | Measures:Create |           |


# Apply first scenario for all fields in Unit Tests

  # EBS-1633
  Scenario Outline: (01) Create Unit Of Measure, with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Unit Of Measure with the following values:
      | Name   | IsStandard |
      | <Name> | False      |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Name |
      |      |
      | ""   |
      | N/A  |

  # EBS-1633
  Scenario: (02) Create Unit Of Measure, with malicious input more than 50 char (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates Unit Of Measure with the following values:
      | Name                                                  | IsStandard |
      | 00001000010000100001000010000100001000010000100001-51 | False      |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
