Feature: View All BusinessUnits

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                 |
      | SalesSpecialist_Signmedia | PurUnitReader_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll       |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
    And the following Business Units exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |

  Scenario: (01) Read list of BusinessUnits dropdown by authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all BusinessUnits
    Then the following values will be presented to "Ahmed.Al-Ashry":
      | Code | Name      |
      | 0002 | Signmedia |
    And total number of BusinessUnits returned to "Ahmed.Al-Ashry" is equal to 1

  @Future
  Scenario: (02) Read list of BusinessUnits dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all BusinessUnits
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page