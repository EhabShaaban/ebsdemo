Feature: View All Companies

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole       |
      | SalesSpecialist_Signmedia | CompanyViewer |
    And the following sub-roles and permissions exist:
      | Subrole       | Permission      | Condition |
      | CompanyViewer | Company:ReadAll |           |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
      | 0003 | HPS       |

  Scenario: (01) Read list of Companies dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Companies
    Then the following Company values will be presented to "Ahmed.Al-Ashry":
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
      | 0003 | HPS       |
    And total number of Companies returned to "Ahmed.Al-Ashry" is equal to 3

  @Future
  Scenario: (02) Read list of Companies dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Companies
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page