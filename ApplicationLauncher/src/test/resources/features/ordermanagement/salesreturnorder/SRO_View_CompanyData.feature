#Author: Waseem Salama, Shirin Mahmoud

Feature: View SalesReturnOrder CompanyData section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Ahmed.Seif     | Quality_Specialist         |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole             |
      | Quality_Specialist         | SROViewer           |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia |
      | SuperUser                  | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                       | Condition                      |
      | SROViewer           | SalesReturnOrder:ReadCompanyData |                                |
      | SROViewer_Signmedia | SalesReturnOrder:ReadCompanyData | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole    | *:*                              |                                |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                       | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:ReadCompanyData | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State |
      | 2020000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
      | 2020000002 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000001 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2020000002 | 0001 - Flexo      | 0002 - DigiPro |


  # EBS-6709
  Scenario: (01) View Sales Return Order CompanyData section by an authorizd user who has one role with one condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view CompanyData section of SalesReturnOrder with code "2020000001"
    Then the following values of CompanyData section for SalesReturnOrder with code "2020000001" are displayed to "Manar.Mohammed":
      | BusinessUnit     | Company        |
      | 0002 - Signmedia | 0002 - DigiPro |

  # EBS-6709
  Scenario Outline: (02) View Sales Return Order CompanyData section by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view CompanyData section of SalesReturnOrder with code "<Code>"
    Then the following values of CompanyData section for SalesReturnOrder with code "<Code>" are displayed to "Ahmed.Seif":
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    Examples:
      | Code       | BusinessUnit     | Company        |
      | 2020000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro |

  # EBS-6723
  # Future
  Scenario: (03) View Sales Return Order CompanyData section where SalesReturnOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to view CompanyData section of SalesReturnOrder with code "2020000001"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-6709
  Scenario: (04) View Sales Return Order CompanyData section by unauthorized user due to condition(Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view CompanyData section of SalesReturnOrder with code "2020000002"
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
