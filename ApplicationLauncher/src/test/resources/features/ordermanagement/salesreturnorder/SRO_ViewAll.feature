#Author: Eman Mansour, Shirin Mahmoud

Feature: View All SalesReturnOrder

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Ahmed.Seif     | Quality_Specialist         |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Afaf           | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole             |
      | Quality_Specialist         | SROViewer           |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | SROViewer           | SalesReturnOrder:ReadAll |                                |
      | SROViewer_Signmedia | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | SalesReturnOrder:ReadAll |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2021000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2021000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2021000001 | 0001 - AL Madina | 0003 - Offset    |
      | 2021000002 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2021000003 | 0001 - AL Madina | 0001 - Flexo     |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner       | State                 |
      | 2021000001 | 25-May-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Draft                 |
      | 2021000002 | 16-Feb-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped               |
      | 2021000003 | 22-Sep-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | CreditNoteActivated   |
      | 2021000004 | 11-Mar-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | GoodsReceiptActivated |
      | 2021000005 | 11-Mar-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Completed             |
      | 2021000006 | 25-May-2021 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Draft                 |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000004 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2021000005 | 0003 - Offset    | 0001 - AL Madina |
      | 2021000006 | 0003 - Offset    | 0002 - DigiPro   |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer                       | CurrencyISO |
      | 2021000001 | 2021000002 | 000007 - مطبعة أكتوبر الهندسية | USD         |
      | 2021000002 | 2021000002 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2021000003 | 2021000003 | 000001 - Al Ahram              | EUR         |
      | 2021000004 | 2021000003 | 000001 - Al Ahram              | EUR         |
      | 2021000005 | 2021000001 | 000006 - المطبعة الأمنية       | USD         |
      | 2021000006 | 2021000001 | 000006 - المطبعة الأمنية       | EUR         |

  #EBS-6700
  #EBS-7394
  Scenario: (01) View all SalesReturnOrders, by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with no filter applied with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State                 |
      | 2021000006 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Draft                 |
      | 2021000005 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 11-Mar-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Completed             |
      | 2021000004 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 11-Mar-2021 01:51 PM | 0001 - Flexo     | 000001 - Al Ahram              | 2021000003 | GoodsReceiptActivated |
      | 2021000003 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 22-Sep-2021 01:51 PM | 0001 - Flexo     | 000001 - Al Ahram              | 2021000003 | CreditNoteActivated   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped               |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft                 |
    And the total number of records in search results by "Ahmed.Seif" are 6

  #EBS-6700
  #EBS-7394
  Scenario: (02) View all SalesReturnOrders, by an authorized user WITH condition [BusinessUnit] (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read page 1 of SalesReturnOrders with no filter applied with 20 records per page
    Then the following values will be presented to "Manar.Mohammed":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft   |
    And the total number of records in search results by "Manar.Mohammed" are 2

  #EBS-7394
  Scenario: (03) Filter View all SalesReturnOrders, by Code by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on SROCode which contains "00002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
    And the total number of records in search results by "Ahmed.Seif" are 1

  #EBS-7394
  Scenario: (04) Filter View all SalesReturnOrders, by Type by authorized user using "Equals"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on Type which equals "RETURN_SALES_ORDER_FOR_ITEMS" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State                 |
      | 2021000006 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Draft                 |
      | 2021000005 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 11-Mar-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Completed             |
      | 2021000004 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 11-Mar-2021 01:51 PM | 0001 - Flexo     | 000001 - Al Ahram              | 2021000003 | GoodsReceiptActivated |
      | 2021000003 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 22-Sep-2021 01:51 PM | 0001 - Flexo     | 000001 - Al Ahram              | 2021000003 | CreditNoteActivated   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped               |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft                 |
    And the total number of records in search results by "Ahmed.Seif" are 6

  #EBS-7394
  Scenario: (05) Filter View all SalesReturnOrders, by CreationDate by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on CreationDate which contains "25-May-2021" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State |
      | 2021000006 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Draft |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-7394
  Scenario: (06) Filter View all SalesReturnOrders, by BusinessUnit by authorized user using "Equals" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on BusinessUnit which equals "0002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft   |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-7394
  Scenario: (07) Filter View all SalesReturnOrders, by Customer Code by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on Customer which contains "07" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft   |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-7394
  Scenario: (08) Filter View all SalesReturnOrders, by Customer Name by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on Customer which contains "أكتوبر" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft   |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-7394
  Scenario: (09) Filter View all SalesReturnOrders, by SICode by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on SICode which contains "00002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State   |
      | 2021000002 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 16-Feb-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Shipped |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft   |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-7394
  Scenario: (10) Filter View all SalesReturnOrders, by State by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesReturnOrders with filter applied on State which contains "Draft" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                                                  | CreationDate         | BusinessUnit     | Customer                       | SICode     | State |
      | 2021000006 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0003 - Offset    | 000006 - المطبعة الأمنية       | 2021000001 | Draft |
      | 2021000001 | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | 25-May-2021 01:51 PM | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | 2021000002 | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-6700
  #EBS-7394
  Scenario: (11) View all SalesReturnOrders, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of SalesReturnOrders with no filter applied with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page