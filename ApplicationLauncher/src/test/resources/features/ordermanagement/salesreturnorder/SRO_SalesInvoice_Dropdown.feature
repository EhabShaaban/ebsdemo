#Author: Niveen Magdy, Ahmed Ali
#Reviewer: Hossam Hassan, Ahmad Hamed (04-11-2020)
Feature: Opened/Active SalesInvoices DropDown

  Background: 
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                      |
      | Accountant_Signmedia | SalesInvoiceViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission           | Condition                      |
      | SalesInvoiceViewer_Signmedia | SalesInvoice:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | SalesInvoice:ReadAll |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 01-Dec-2020 01:28 PM |Manar Mohammed |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 02-Dec-2020 01:28 PM |Manar Mohammed |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 03-Dec-2020 01:28 PM |Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 04-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2021000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Jun-2021 02:20 PM |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 02-Jun-2021 02:20 PM |
      | 2021000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft         | 03-Jun-2021 02:20 PM |
      | 2021000004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 04-Jun-2021 02:20 PM |
      | 2021000005 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 05-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000001 | 0002 - DigiPro | 0001 - Flexo     |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
      | 2021000003 | 0002 - DigiPro | 0002 - Signmedia |
      | 2021000004 | 0002 - DigiPro | 0002 - Signmedia |
      | 2021000005 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000001 | 000001 - Al Ahram | 2020000001 | 0001 - 20% Deposit, 80% T/T Before Loading | USD      |             |
      | 2021000002 | 000001 - Al Ahram | 2020000002 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2021000003 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2021000004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | USD      |             |
      | 2021000005 | 000001 - Al Ahram | 2020000004 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining   |
      | 2021000001 | 7000.879654 |
      | 2021000002 |     1250.55 |
      | 2021000003 |   89000.225 |
      | 2021000004 |        2000 |
      | 2021000005 |     400.250 |

  Scenario: (01) Read All SalesInvoices Dropdown in Create SalesReturn by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all Active SalesInvoices of businessUnit with code "0002" and customer with code "000001"
    Then the following SalesInvoices values will be presented to "Ahmed.Al-Ashry":
      | Code       | BusinessUnit     |
      | 2021000005 | 0002 - Signmedia |
      | 2021000004 | 0002 - Signmedia |
      | 2021000002 | 0002 - Signmedia |
    And total number of SalesInvoices returned to "Ahmed.Al-Ashry" is equal to 3

  Scenario: (02) Read All SalesInvoices Dropdown in Create SalesReturn by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Active SalesInvoices of businessUnit with code "0002" and customer with code "000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
