# Author: Eman Mansour, Shirin Mahmoud
# Reviewer: Somaya Ahmed (21-Jul-2020)

Feature: Sales Return Order - Save Edit Item HP

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia |
      | SalesCoordinator_Signmedia | ReturnReasonViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                   | Condition                      |
      | SROOwner_Signmedia | SalesReturnOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
      | ReturnReasonViewer | Reason:ReadAll               | [type='SRO_REASON']            |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State                 | CreationDate         |
      | 2019000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | SalesInvoiceActivated | 11-Feb-2019 01:28 PM |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company        | Store                     |
      | 2019000010 | 0002 - DigiPro | 0003 - DigiPro Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2019000010 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000001 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 12.000         | 10000.000       | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 100.000        | 999.000         | 0019 - M2       |
      | 2019000001 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State |
      | 2020000055 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer          | CurrencyISO |
      | 2020000055 | 2019000001 | 000001 - Al Ahram | EGP         |
    And Insert the following Item with Id for SalesReturnOrders exist:
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000055 | 11        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 0001 - Problem in product operation | 12.000         | 10000.000  |
      | 2020000055 | 12        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |                                     | 100.000        | 999.000    |
      | 2020000055 | 13        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |                                     | 100.000        | 999.000    |
      | 2020000055 | 14        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |                                     | 100.000        | 999.000    |

    And the following Reasons exist:
      | Reason                              | Type       |
      | 0001 - Problem in product operation | SRO_REASON |
    And edit session is "30" minutes

  # EBS-6727
  Scenario: (01) Save Edit SalesReturnOrder Item, with "editable" fields in "editable states" - with all fields - within edit session (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:50 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:55 AM" with the following values:
      | ReturnQuantity | ReturnReason                        |
      | 0.000001       | 0001 - Problem in product operation |
    Then SalesReturnOrder with code "2020000055" is updated as follows:
      | LastUpdatedBy  | LastUpdateDate       |
      | Manar.Mohammed | 09-Jul-2020 11:55 AM |
    And the lock by "Manar.Mohammed" on Items section of SalesReturnOrder with Code "2020000055" is released
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-04"
    And Items section of SalesReturnOrder with Code "2020000055" is updated as follows and displayed to "Manar.Mohammed":
      | SROItemId | Item                                                             | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice | ItemTotalAmount |
      | 11        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 0001 - Problem in product operation | 0.000001       | 10000.000  | 0.01            |
      | 12        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |                                     | 100.000        | 999.000    | 99900           |
      | 13        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |                                     | 100.000        | 999.000    | 99900           |
      | 14        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |                                     | 100.000        | 999.000    | 99900           |