# Author Dev: Fatma AL Zahraa,  Author Quality: Shirin Mahmoud
# Reviewer: Somaya Ahmed (8-Jul-2020)

Feature: Delete Sales Return Order

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia |
      | SuperUser                  | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                      |
      | SROOwner_Signmedia | SalesReturnOrder:Delete | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole   | *:*                     |                                |
    And the following users have the following permissions without the following conditions:
      | User           | Permission              | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:Delete | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner       | State                 |
      | 2020000054 | 05-Jul-2020 12:37 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | hr1                       | Manar Mohammed      | Draft                 |
      | 2020000053 | 29-Jun-2020 11:21 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | hr1                       | SeragEldin Meghawry | Shipped               |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Ahmed Al-Ashry      | Completed             |
      | 2020000004 | 15-Jun-2020 02:22 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | CreditNoteActivated   |
      | 2020000003 | 15-Jun-2020 02:11 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Ahmed Al-Ashry      | GoodsReceiptActivated |
      | 2020000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Draft                 |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000054 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000053 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000005 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro   |

  # EBS-6723
  Scenario: (01) Delete SalesReturnOrder, where SalesReturnOrder is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to delete SalesReturnOrder with code "2020000001"
    Then SalesReturnOrder with code "2020000001" is deleted
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-12"

  # EBS-6723
  Scenario: (02) Delete SalesReturnOrder, where sales order doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to delete SalesReturnOrder with code "2020000001"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-6723
  Scenario Outline: (03) Delete SalesReturnOrder, where action is not allowed in current state: CreditNoteActivated, GoodsReceiptActivated, Shipped, Completed (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to delete SalesReturnOrder with code "<Code>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-13"
    Examples:
      | Code       |
      | 2020000053 |
      | 2020000005 |
      | 2020000004 |
      | 2020000003 |

   # EBS-6723
  @Future
  Scenario Outline: (04) Delete SalesReturnOrder, where sales Return order is locked by another user (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<SROSection>" section of SalesReturnOrder with code "2020000001" in edit mode
    When "Manar.Mohammed" requests to delete SalesReturnOrder with code "2020000001"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-14"
    Examples:
      | SROSection         |
      | SalesReturnDetails |

  # EBS-6723
  Scenario: (05) Delete SalesReturnOrder, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to delete SalesReturnOrder with code "2020000054"
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
