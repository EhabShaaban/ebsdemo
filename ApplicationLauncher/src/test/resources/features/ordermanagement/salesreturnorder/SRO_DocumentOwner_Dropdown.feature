#Author: Zyad Ghorab, Sherien
#Reviewer: Shirin
Feature: ViewAll DocumentOwners Dropdown

  Background:
    Given the following users and roles exist:
      | Name                | Role                       |
      | Manar.Mohammed      | SalesCoordinator_Signmedia |
      | Manar.Mohammed      | SRODocumentOwnerRole       |
      | SeragEldin.Meghawry | SRODocumentOwnerRole       |
      | Ahmed.Al-Ashry      | SRODocumentOwnerRole       |
      | Afaf                | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                 |
      | SalesCoordinator_Signmedia | DocumentOwnerViewer     |
      | SRODocumentOwnerRole       | SRODocumentOwnerSubRole |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                          | Condition |
      | DocumentOwnerViewer     | DocumentOwner:ReadAll               |           |
      | SRODocumentOwnerSubRole | SalesReturnOrder:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id      | Name                | BusinessObject   | Permission         | Condition |
      | 1000093 | Manar Mohammed      | SalesReturnOrder | CanBeDocumentOwner |           |
      | 1000077 | SeragEldin Meghawry | SalesReturnOrder | CanBeDocumentOwner |           |
      | 1000072 | Ahmed Al-Ashry      | SalesReturnOrder | CanBeDocumentOwner |           |
    And the total number of existing "SalesReturnOrder" DocumentOwners are 3

  Scenario: (01) Read list of DocumentOwners dropdown, by authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read all DocumentOwners for "SalesReturn"
    Then the following DocumentOwners values will be presented to "Manar.Mohammed":
      | Name                |
      | Ahmed Al-Ashry      |
      | Manar Mohammed      |
      | SeragEldin Meghawry |
    And total number of DocumentOwners returned to "Manar.Mohammed" in a dropdown is equal to 3

  Scenario: (02) Read list of DocumentOwners dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "SalesReturn"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
