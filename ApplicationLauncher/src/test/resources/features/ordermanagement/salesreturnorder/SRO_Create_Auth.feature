# Author: Zyad (Dev) and Shirin (Quality)
Feature: Create SalesReturn - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                                                                                  | Role                                                                                                              |
      | Afaf                                                                                                  | FrontDesk                                                                                                         |
      | Manar.Mohammed                                                                                        | SalesCoordinator_Signmedia                                                                                        |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | SalesCoordinator_Signmedia_CannotViewReturnTypeBusinessUnitCustomerSalesInvoiceProductManagerAndDocumentOwnerRole |
      | Ahmed.Al-Ashry                                                                                        | SRODocumentOwnerRole                                                                                              |
    And the following roles and sub-roles exist:
      | Role                                                                                                              | Subrole                      |
      | SalesCoordinator_Signmedia                                                                                        | SROOwner_Signmedia           |
      | SalesCoordinator_Signmedia                                                                                        | PurUnitReader_Signmedia      |
      | SalesCoordinator_Signmedia                                                                                        | CustomerViewer_Signmedia     |
      | SalesCoordinator_Signmedia                                                                                        | SalesInvoiceViewer_Signmedia |
      | SalesCoordinator_Signmedia                                                                                        | DocumentOwnerViewer          |
      | SalesCoordinator_Signmedia                                                                                        | SalesReturnOrderTypeViewer   |
      | SalesCoordinator_Signmedia_CannotViewReturnTypeBusinessUnitCustomerSalesInvoiceProductManagerAndDocumentOwnerRole | SROOwner_Signmedia           |
      | SRODocumentOwnerRole                                                                                              | SRODocumentOwnerSubRole      |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                          | Condition                      |
      | SROOwner_Signmedia           | SalesReturnOrder:Create             |                                |
      | SalesReturnOrderTypeViewer   | SalesReturnOrderType:ReadAll        |                                |
      | PurUnitReader_Signmedia      | PurchasingUnit:ReadAll              | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia     | Customer:ReadAll                    | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia | SalesInvoice:ReadAll                | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll               |                                |
      | SRODocumentOwnerSubRole      | SalesReturnOrder:CanBeDocumentOwner |                                |
    And the following users doesn't have the following permissions:
      | User                                                                                                  | Permission                   |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | SalesReturnOrderType:ReadAll |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | PurchasingUnit:ReadAll       |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | Customer:ReadAll             |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | SalesInvoice:ReadAll         |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | DocumentOwner:ReadAll        |
    And the following users have the following permissions without the following conditions:
      | User           | Permission             | Condition                  |
      | Manar.Mohammed | PurchasingUnit:ReadAll | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | Customer:ReadAll       | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesInvoice:ReadAll   | [purchaseUnitName='Flexo'] |
    And the following SalesReturnOrderTypes exist:
      | Code                         | Name                   |
      | RETURN_SALES_ORDER_FOR_ITEMS | Sales return for items |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name     | BusinessUnit     |
      | 000001 | Al Ahram | 0002 - Signmedia |
      | 000002 | Client2  | 0001 - Flexo     |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject   | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | SalesReturnOrder | CanBeDocumentOwner |           |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100006 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019100006 | 0001 - AL Madina | 0001 - Flexo     |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019100006 | 000002 - Client2  | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |

  Scenario: (01) Create SalesReturnOrder, by an authorized user with authorized reads
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to creates SalesReturnOrder
    Then the following authorized reads are returned to "Manar.Mohammed":
      | AuthorizedReads    |
      | ReadSROTypes       |
      | ReadBusinessUnits  |
      | ReadCustomers      |
      | ReadSalesInvoices  |
      | ReadDocumentOwners |

  Scenario: (02) Create SalesReturnOrder, by an authorized user with no authorized reads
    Given user is logged in as "Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner"
    When "Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner" requests to creates SalesReturnOrder
    Then there are no authorized reads returned to "Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner"

  Scenario: (03) Create SalesReturnOrder, by an unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to creates SalesReturnOrder
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-6725
  Scenario Outline: (04) Create SalesReturnOrder, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" creates SalesReturnOrder with the following values:
      | Type   | SalesInvoice   | DocumentOwnerId |
      | <Type> | <SalesInvoice> | <DocumentOwner> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                                                                                                  | Type                         | SalesInvoice | DocumentOwner |
      | Manar.Mohammed                                                                                        | RETURN_SALES_ORDER_FOR_ITEMS | 2019100006   | 1000072       |
      | Manar.Mohammed.NoSalesReturnTypeNoBusinessUnitNoCustomerNoSalesInvoiceNoProductManagerNoDocumentOwner | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001   | 1000072       |
#  @Future
#  TODO: remove permission -> PurUnitReader_Signmedia -> PurchasingUnit:ReadAll -> no condition from the system
#      | Manar.Mohammed | RETURN_SALES_ORDER_FOR_ITEMS | 0001 | 000001 | 2019000001 | 0001 | 0005 |
