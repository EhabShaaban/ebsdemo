#Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Aboulwafa

Feature: View All Item OrderUnits

  Background:
    Given the following users and roles exist:
      | Name       | Role               |
      | Ahmed.Seif | Quality_Specialist |
      | Afaf       | FrontDesk          |

    And the following roles and sub-roles exist:
      | Role               | Subrole        |
      | Quality_Specialist | SROViewer      |
      | Quality_Specialist | MeasuresViewer |

    And the following sub-roles and permissions exist:
      | Subrole        | Permission                 | Condition |
      | SROViewer      | SalesReturnOrder:ReadItems |           |
      | MeasuresViewer | Measures:ReadAll           |           |

    And the following users doesn't have the following permissions:
      | User | Permission                 |
      | Afaf | SalesReturnOrder:ReadItems |
      | Afaf | Measures:ReadAll           |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100013 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit |
      | 2019100013 | 0002 - DigiPro | 0001 - Flexo   |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019100013 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item          | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100013 | 000051 - Ink5 | 0029 - Roll 2.20x50 | 12.000         | 22000.000       | 0019 - M2 |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner  | State   |
      | 2020000002 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed | Shipped |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit | Company        |
      | 2020000002 | 0001 - Flexo | 0002 - DigiPro |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer                 | CurrencyISO |
      | 2020000002 | 2019100013 | 000006 - المطبعة الأمنية | EGP         |
    And Insert the following Item for SalesReturnOrders exist:
      | Code       | Item          | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000002 | 000051 - Ink5 | 0029 - Roll 2.20x50 | 0001 - Problem in product operation | 10.000         | 22000.000  |

    And the total number of existing OrderUnits for Item "000051 - Ink5" in SalesReturnOrder with code "2020000002" is 1

    #EBS-6770
  Scenario: (01) Read list of SalesReturnOrder Item OrderUnits - dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read OrderUnits of Item "000051 - Ink5" in SalesReturnOrder with code "2020000002"
    Then the following Item OrderUnits will be presented to "Ahmed.Seif":
      | OrderUnit           |
      | 0029 - Roll 2.20x50 |
    And total number of OrderUnits returned to "Ahmed.Seif" equals  1

    #EBS-6770
  Scenario: (02) Read list of SalesReturnOrder Item OrderUnits -  dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read OrderUnits of Item "000051 - Ink5" in SalesReturnOrder with code "2020000002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page