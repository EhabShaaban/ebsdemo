# Author Dev: Fatma Al-Zahra , Zyad Ghorab
# Author Quality: Shirin Mahmoud
# Reviewers: Somaya Ahmed & Hosam Bayomy

Feature: Create SalesReturn - Happy Path

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SRODocumentOwnerRole       |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                      |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia           |
      | SalesCoordinator_Signmedia | PurUnitReader_Signmedia      |
      | SalesCoordinator_Signmedia | CustomerViewer_Signmedia     |
      | SalesCoordinator_Signmedia | SalesInvoiceViewer_Signmedia |
      | SalesCoordinator_Signmedia | DocumentOwnerViewer          |
      | SalesCoordinator_Signmedia | SalesReturnOrderTypeViewer   |
      | SRODocumentOwnerRole       | SRODocumentOwnerSubRole      |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                          | Condition                      |
      | SROOwner_Signmedia           | SalesReturnOrder:Create             |                                |
      | PurUnitReader_Signmedia      | PurchasingUnit:ReadAll              | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia     | Customer:ReadAll                    | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia | SalesInvoice:ReadAll                | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll               |                                |
      | SalesReturnOrderTypeViewer   | SalesReturnOrderType:ReadAll        |                                |
      | SRODocumentOwnerSubRole      | SalesReturnOrder:CanBeDocumentOwner |                                |
    And the following SalesReturnOrderTypes exist:
      | Code                         | Name                   |
      | RETURN_SALES_ORDER_FOR_ITEMS | Sales return for items |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name     | BusinessUnit     |
      | 000001 | Al Ahram | 0002 - Signmedia |

    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject   | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | SalesReturnOrder | CanBeDocumentOwner |           |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2020000009 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2020000009 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram              | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2020000009 | 000007 - مطبعة أكتوبر الهندسية | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000001 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 12.000         | 10000.000       | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 100.000        | 999.000         | 0019 - M2       |
      | 2019000001 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2020000009 | 000060 - Hot Laminated Frontlit Fabric roll 1                    | 0029 - Roll 2.20x50 | 49.000         | 65.000          | 0019 - M2       |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019000001 | 0007 - Vat                                   | 1.00          | 4197.000  |
      | 2019000001 | 0008 - Commercial and industrial profits tax | 0.5           | 2098.500  |
      | 2019000001 | 0009 - Real estate taxes                     | 10.00         | 41970.000 |
      | 2020000009 | 0007 - Vat                                   | 1.0           | 31.850    |
      | 2020000009 | 0008 - Commercial and industrial profits tax | 0.5           | 15.925    |
      | 2020000009 | 0009 - Real estate taxes                     | 10.0          | 318.500   |
      | 2020000009 | 0010 - Service tax                           | 12.0          | 382.200   |
      | 2020000009 | 0011 - Adding tax                            | 0.5           | 15.925    |
      | 2020000009 | 0012 - Sales tax                             | 12.0          | 382.200   |

  # EBS-6696
  Scenario: (01) Create SalesReturnOrder, by DocumentOwner (SalesResponsible/ SalesCoordinators) with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created SalesReturnOrder was with code "2020000050"
    When "Manar.Mohammed" creates SalesReturnOrder with the following values:
      | Type                         | SalesInvoice | DocumentOwnerId |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001   | 1000072         |
    Then a new SalesReturnOrder is created as follows:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type                                                  | DocumentOwner  |
      | 2020000051 | Draft | Manar.Mohammed | Manar.Mohammed | 01-Jan-2020 11:00 AM | 01-Jan-2020 11:00 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Ahmed Al-Ashry |
    And the Company Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | BusinessUnit     | Company        |
      | 0002 - Signmedia | 0002 - DigiPro |
    And the SalesReturnDetails Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | SICode     | Customer          | CurrencyISO |
      | 2019000001 | 000001 - Al Ahram | EGP         |
    And the Items Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | Item                                                             | OrderUnit           | ReturnReason | ReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kilogram     |              | 12.000         | 10000.000  | 120000.000      |
      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |              | 100.000        | 999.000    | 99900.000       |
      | 000008 - PRO-V-ST-2                                              | 0003 - Kilogram     |              | 100.000        | 999.000    | 99900.000       |
      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kilogram     |              | 100.000        | 999.000    | 99900.000       |
    And the Taxes Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | TaxName                                      | TaxPercentage | TaxAmount |
      | 0007 - Vat                                   | 1             | 4197.000  |
      | 0008 - Commercial and industrial profits tax | 0.5           | 2098.500  |
      | 0009 - Real estate taxes                     | 10            | 41970.000 |
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-11"

  # EBS-7195
  Scenario: (02) Create SalesReturnOrder, On SalesInvoice with fully returned item, by DocumentOwner (SalesResponsible/ SalesCoordinators) with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created SalesReturnOrder was with code "2020000050"
    When "Manar.Mohammed" creates SalesReturnOrder with the following values:
      | Type                         | SalesInvoice | DocumentOwnerId |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2020000009   | 1000072         |
    Then a new SalesReturnOrder is created as follows:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type                                                  | DocumentOwner  |
      | 2020000051 | Draft | Manar.Mohammed | Manar.Mohammed | 01-Jan-2020 11:00 AM | 01-Jan-2020 11:00 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Ahmed Al-Ashry |
    And the Company Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | BusinessUnit     | Company          |
      | 0002 - Signmedia | 0001 - AL Madina |
    And the SalesReturnDetails Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | SICode     | Customer                       | CurrencyISO |
      | 2020000009 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
    And the Items Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | Item                                          | OrderUnit           | ReturnReason | ReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000060 - Hot Laminated Frontlit Fabric roll 1 | 0029 - Roll 2.20x50 |              | 49.000         | 65.000     | 3185.000        |
    And the Taxes Section of SalesReturnOrder with code "2020000051" is updated as follows:
      | TaxName                                      | TaxPercentage | TaxAmount |
      | 0007 - Vat                                   | 1.0           | 31.850    |
      | 0008 - Commercial and industrial profits tax | 0.5           | 15.925    |
      | 0009 - Real estate taxes                     | 10.0          | 318.500   |
      | 0010 - Service tax                           | 12.0          | 382.200   |
      | 0011 - Adding tax                            | 0.5           | 15.925    |
      | 0012 - Sales tax                             | 12.0          | 382.200   |
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-11"
