# Author: Zyad (Dev) and Shirin (Quality)
# Reviewer: Somaya Ahmed (7-Jul-2020)

Feature: Object Screen Allowed Actions Sales Return Order

  Background:
    Given the following users and roles exist:
      | Name               | Role                       |
      | Manar.Mohammed     | SalesCoordinator_Signmedia |
      | Gehan.Ahmed        | SalesManager_Signmedia     |
      | CannotReadSections | CannotReadSectionsRole     |
      | hr1                | SuperUser                  |
      | Afaf               | FrontDesk                  |

    And the following roles and sub-roles exist:
      | Role                       | Subrole                   |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia       |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia        |
      | SalesManager_Signmedia     | SROViewer_Signmedia       |
      | SuperUser                  | SuperUserSubRole          |
      | CannotReadSectionsRole     | CannotReadSectionsSubRole |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | SuperUserSubRole          | *:*                                |                                |

      | SROOwner_Signmedia        | SalesReturnOrder:UpdateItems       | [purchaseUnitName='Signmedia'] |
      | SROOwner_Signmedia        | SalesReturnOrder:Delete            | [purchaseUnitName='Signmedia'] |
      | SROOwner_Signmedia        | SalesReturnOrder:MarkAsShipped     | [purchaseUnitName='Signmedia'] |
      | SROOwner_Signmedia        | SalesReturnOrder:ExportPDF         | [purchaseUnitName='Signmedia'] |

      | SROViewer_Signmedia       | SalesReturnOrder:ReadAll           | [purchaseUnitName='Signmedia'] |
      | SROViewer_Signmedia       | SalesReturnOrder:ReadCompanyData   | [purchaseUnitName='Signmedia'] |
      | SROViewer_Signmedia       | SalesReturnOrder:ReadReturnDetails | [purchaseUnitName='Signmedia'] |
      | SROViewer_Signmedia       | SalesReturnOrder:ReadItems         | [purchaseUnitName='Signmedia'] |

      | CannotReadSectionsSubRole | SalesReturnOrder:ReadAll           |                                |
      | CannotReadSectionsSubRole | SalesReturnOrder:Delete            |                                |
      | CannotReadSectionsSubRole | SalesReturnOrder:MarkAsShipped     |                                |
      | CannotReadSectionsSubRole | SalesReturnOrder:ExportPDF         |                                |

    And the following users doesn't have the following permissions:
      | User               | Permission                         |
      | Afaf               | SalesReturnOrder:ReadAll           |
      | Afaf               | SalesReturnOrder:ReadCompanyData   |
      | Afaf               | SalesReturnOrder:ReadReturnDetails |
      | Afaf               | SalesReturnOrder:ReadItems         |
      | Afaf               | SalesReturnOrder:UpdateItems       |
      | Afaf               | SalesReturnOrder:Delete            |
      | Afaf               | SalesReturnOrder:MarkAsShipped     |
      | Afaf               | SalesReturnOrder:ExportPDF         |

      | Gehan.Ahmed        | SalesReturnOrder:UpdateItems       |
      | Gehan.Ahmed        | SalesReturnOrder:Delete            |
      | Gehan.Ahmed        | SalesReturnOrder:MarkAsShipped     |
      | Gehan.Ahmed        | SalesReturnOrder:ExportPDF         |

      | CannotReadSections | SalesReturnOrder:UpdateItems       |
      | CannotReadSections | SalesReturnOrder:ReadCompanyData   |
      | CannotReadSections | SalesReturnOrder:ReadReturnDetails |
      | CannotReadSections | SalesReturnOrder:ReadItems         |

    And the following users have the following permissions without the following conditions:
      | User           | Permission                         | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:ReadAll           | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:ReadCompanyData   | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:ReadReturnDetails | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:ReadItems         | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:UpdateItems       | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:Delete            | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:MarkAsShipped     | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:ExportPDF         | [purchaseUnitName='Flexo'] |

      | Gehan.Ahmed    | SalesReturnOrder:ReadAll           | [purchaseUnitName='Flexo'] |
      | Gehan.Ahmed    | SalesReturnOrder:ReadCompanyData   | [purchaseUnitName='Flexo'] |
      | Gehan.Ahmed    | SalesReturnOrder:ReadReturnDetails | [purchaseUnitName='Flexo'] |
      | Gehan.Ahmed    | SalesReturnOrder:ReadItems         | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100014 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100013 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100012 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100011 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100010 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100014 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100013 | 0002 - DigiPro   | 0001 - Flexo     |
      | 2019100012 | 0001 - AL Madina | 0001 - Flexo     |
      | 2019100011 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100010 | 0001 - AL Madina | 0002 - Signmedia |
      | 2020000003 | 0001 - AL Madina | 0002 - Signmedia |
    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner       | State                 |
      | 2020000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Draft                 |
      | 2020000002 | 15-Jun-2020 02:05 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped               |
      | 2020000003 | 15-Jun-2020 02:11 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | GoodsReceiptActivated |
      | 2020000004 | 15-Jun-2020 02:22 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | CreditNoteActivated   |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Completed             |
      | 2020000006 | 29-Jun-2020 11:21 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped               |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2020000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000004 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000005 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000006 | 0002 - Signmedia | 0002 - DigiPro   |

  # EBS-6718
  Scenario Outline: (01) Read allowed actions in SalesReturnOrder Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesReturnOrder with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | AllowedActions                                                                                        | NoOfActions |
      | Manar.Mohammed     | 2020000001 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems, UpdateItems, Delete, MarkAsShipped, ExportPDF | 8           |
      | Manar.Mohammed     | 2020000004 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems, ExportPDF                                     | 5           |
      | Manar.Mohammed     | 2020000005 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems, ExportPDF                                     | 5           |
      | Manar.Mohammed     | 2020000006 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems, ExportPDF                                     | 5           |

      | Gehan.Ahmed        | 2020000001 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems                                                | 4           |
      | Gehan.Ahmed        | 2020000004 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems                                                | 4           |
      | Gehan.Ahmed        | 2020000005 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems                                                | 4           |
      | Gehan.Ahmed        | 2020000006 | ReadAll, ReadCompanyData, ReadReturnDetails, ReadItems                                                | 4           |

      | CannotReadSections | 2020000001 | ReadAll, Delete, MarkAsShipped, ExportPDF                                                             | 4           |
      | CannotReadSections | 2020000004 | ReadAll, ExportPDF                                                                                    | 2           |
      | CannotReadSections | 2020000005 | ReadAll, ExportPDF                                                                                    | 2           |
      | CannotReadSections | 2020000006 | ReadAll, ExportPDF                                                                                    | 2           |

  # EBS-6718
  Scenario Outline: (02) Read allowed actions in SalesReturnOrder Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesReturnOrder with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User           | Code       |
      | Manar.Mohammed | 2020000002 |
      | Manar.Mohammed | 2020000003 |
      | Gehan.Ahmed    | 2020000002 |
      | Gehan.Ahmed    | 2020000003 |
      | Afaf           | 2020000001 |
      | Afaf           | 2020000004 |
      | Afaf           | 2020000005 |
      | Afaf           | 2020000006 |
      | Afaf           | 2020000002 |
      | Afaf           | 2020000003 |

  # Future
  # EBS-6718
  Scenario: (03) Read allowed actions in SalesReturnOrder Object Screen - Object does not exist (Exception)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to read actions of SalesReturnOrder with code "2020000001"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"
