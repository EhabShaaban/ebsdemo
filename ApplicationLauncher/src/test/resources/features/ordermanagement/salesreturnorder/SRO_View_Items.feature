#Author: Eman Mansour, Shirin Mahmoud

Feature: View SalesReturnOrder Items section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Ahmed.Seif     | Quality_Specialist         |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole             |
      | Quality_Specialist         | SROViewer           |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia |
      | SuperUser                  | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                 | Condition                      |
      | SROViewer           | SalesReturnOrder:ReadItems |                                |
      | SROViewer_Signmedia | SalesReturnOrder:ReadItems | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole    | *:*                        |                                |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                 | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:ReadItems | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100014 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100013 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2020000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100014 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100013 | 0002 - DigiPro   | 0001 - Flexo     |
      | 2020000001 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                          | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100014 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 18.000         | 200.000         | 0019 - M2 |
      | 2019100013 | 000051 - Ink5                                 | 0029 - Roll 2.20x50 | 10.000         | 22000.000       | 0019 - M2 |
      | 2020000001 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 18.000         | 952.500         | 0019 - M2 |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner       | State   |
      | 2020000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Draft   |
      | 2020000002 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
      | 2020000051 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped |
      | 2020000052 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000051 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000052 | 0002 - Signmedia | 0003 - HPS     |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer                 | CurrencyISO |
      | 2020000001 | 2019100014 | 000001 - Al Ahram        | EGP         |
      | 2020000002 | 2019100013 | 000006 - المطبعة الأمنية | EGP         |
      | 2020000051 | 2019100014 | 000001 - Al Ahram        | EGP         |
      | 2020000052 | 2020000001 | 000001 - Al Ahram        | EGP         |
    And Insert the following Item for SalesReturnOrders exist:
      | Code       | Item                                          | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000001 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0001 - Problem in product operation | 16.000         | 200.000    |
      | 2020000002 | 000051 - Ink5                                 | 0029 - Roll 2.20x50 | 0001 - Problem in product operation | 10.000         | 22000.000  |
      | 2020000052 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 0005 - Items doesn't match the need | 25.000         | 952.500    |
    And Insert the following Taxes for SalesReturnOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000001 | 0003 - Real estate taxes                     | 10            |
      | 2020000001 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000001 | 0001 - Vat                                   | 1             |
      | 2020000002 | 0003 - Real estate taxes                     | 10            |
      | 2020000002 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000002 | 0001 - Vat                                   | 1             |
      | 2020000051 | 0003 - Real estate taxes                     | 10            |
      | 2020000051 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000051 | 0001 - Vat                                   | 1             |

    And the following SalesReturnOrders have no Items:
      | Code       |
      | 2020000051 |
    And the following SalesReturnOrders have no Taxes:
      | Code       |
      | 2020000052 |

  # EBS-6708
  Scenario: (01) View SalesReturnOrder Items section by an authorized user who has one role with one condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesReturnOrder with code "2020000001"
    Then the following values of Items section for SalesReturnOrder with code "2020000001" are displayed to "Manar.Mohammed":
      | Item                                          | OrderUnit | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 0001 - Problem in product operation | 16.000         | 18.000            | 200.000    | 3200.000        |
    And the following values for TaxesSummary for SalesReturnOrder with code "2020000001" are displayed to "Manar.Mohammed":
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 32.000    |
      | 0002 - Commercial and industrial profits tax | 0.5           | 16.000    |
      | 0003 - Real estate taxes                     | 10            | 320.000   |
    And the following values for ItemsSummary for salesReturnOrder with code "2020000001" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 3200.000               | 368.000    | 3568.000              |

  # EBS-6708
  Scenario: (02) View SalesReturnOrder Items section by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of SalesReturnOrder with code "2020000002"
    Then the following values of Items section for SalesReturnOrder with code "2020000002" are displayed to "Ahmed.Seif":
      | Item          | OrderUnit           | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000051 - Ink5 | 0029 - Roll 2.20x50 | 0001 - Problem in product operation | 10.000         | 10.000            | 22000.000  | 220000.000      |
    And the following values for TaxesSummary for SalesReturnOrder with code "2020000002" are displayed to "Ahmed.Seif":
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 2200.000  |
      | 0002 - Commercial and industrial profits tax | 0.5           | 1100.000  |
      | 0003 - Real estate taxes                     | 10            | 22000.000 |
    And the following values for ItemsSummary for salesReturnOrder with code "2020000002" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 220000.000             | 25300.000  | 245300.000            |

  # EBS-6708
  Scenario: (03) View SalesReturnOrder Items section by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of SalesReturnOrder with code "2020000001"
    Then the following values of Items section for SalesReturnOrder with code "2020000001" are displayed to "Ahmed.Seif":
      | Item                                          | OrderUnit | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 0001 - Problem in product operation | 16.000         | 18.000            | 200.000    | 3200.000        |
    And the following values for TaxesSummary for SalesReturnOrder with code "2020000001" are displayed to "Ahmed.Seif":
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 32.000    |
      | 0002 - Commercial and industrial profits tax | 0.5           | 16.000    |
      | 0003 - Real estate taxes                     | 10            | 320.000   |
    And the following values for ItemsSummary for salesReturnOrder with code "2020000001" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 3200.000               | 368.000    | 3568.000              |

  # EBS-6708
  Scenario: (04) View SalesReturnOrder Items section by an authorized user who has one role without conditions - No Items (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of SalesReturnOrder with code "2020000051"
    Then Items section for SalesReturnOrder with code "2020000051" is displayed empty to "Ahmed.Seif"
    And the following values for TaxesSummary for SalesReturnOrder with code "2020000051" are displayed to "Ahmed.Seif":
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             |           |
      | 0002 - Commercial and industrial profits tax | 0.5           |           |
      | 0003 - Real estate taxes                     | 10            |           |
    And the following values for ItemsSummary for salesReturnOrder with code "2020000051" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      |                        |            |                       |

  # EBS-6708
  Scenario: (05) View SalesReturnOrder Items section by an authorized user who has one role without conditions - No Taxes (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of SalesReturnOrder with code "2020000052"
    Then the following values of Items section for SalesReturnOrder with code "2020000052" are displayed to "Ahmed.Seif":
      | Item                                          | OrderUnit           | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 0005 - Items doesn't match the need | 25.000         | 18.000            | 952.500    | 23812.500       |
    And TaxesSummary section for SalesReturnOrder with code "2020000052" is displayed empty to "Ahmed.Seif"
    And the following values for ItemsSummary for salesReturnOrder with code "2020000052" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 23812.500              | 0.000      | 23812.500             |

  # Future
  # EBS-6723
  Scenario: (06) View SalesReturnOrder Items section where SalesReturnOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to view Items section of SalesReturnOrder with code "2020000001"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-6708
  Scenario: (07) View SalesReturnOrder Items section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesReturnOrder with code "2020000002"
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page