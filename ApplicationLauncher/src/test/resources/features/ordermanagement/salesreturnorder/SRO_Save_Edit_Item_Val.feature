# Author Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud

Feature: Sales Return Order - Save Edit Item Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | hr1            | SuperUser                  |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SuperUser                  | SuperUserSubRole   |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia  |
      | SalesCoordinator_Signmedia | ReturnReasonViewer |
      | SalesCoordinator_Signmedia | MeasuresViewer     |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission             | Condition                      |
      | SuperUserSubRole   | *:*                    |                                |
      | SOOwner_Signmedia  | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
      | ReturnReasonViewer | Reason:ReadAll         | [type='SRO_REASON']            |
      | MeasuresViewer     | Measures:ReadAll       |                                |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000001 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 12.000         | 10000.000       | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 100.000        | 999.000         | 0019 - M2       |
      | 2019000001 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State |
      | 2020000055 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer          | CurrencyISO |
      | 2020000055 | 2019000001 | 000001 - Al Ahram | EGP         |
    And Insert the following Item with Id for SalesReturnOrders exist:
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason | ReturnQuantity | SalesPrice |
      | 2020000055 | 11        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |              | 1.000          | 63436.500  |
      | 2020000055 | 12        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |              | 1.000          | 999.000    |
      | 2020000055 | 13        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |              | 1.000          | 63436.500  |
      | 2020000055 | 14        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           |              | 1.000          | 10000.000  |

    And the following Reasons exist:
      | Reason                              | Type       |
      | 0001 - Problem in product operation | SRO_REASON |
    And edit session is "30" minutes

  # EBS-6956
  Scenario: (01) Save Edit SalesReturnOrder Item, after edit session expires (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:00 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:31 AM" with the following values:
      | ReturnQuantity | ReturnReason                        |
      | 5.000          | 0001 - Problem in product operation |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-07"

  # EBS-6956
  Scenario: (02) Save Edit SalesReturnOrder Item, SRO doesn't exist (Exception)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:00 AM"
    And "hr1" first deleted the SalesReturnOrder with code "2020000055" successfully at "09-Jul-2020 11:31 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:32 AM" with the following values:
      | ReturnQuantity | ReturnReason                        |
      | 5.000          | 0001 - Problem in product operation |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-6956
  Scenario: (03) Save Edit SalesReturnOrder Item, after session expires and SROItem doesn't exist (Exception)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted SROItem with id 11 of SalesReturnOrder with code "2020000055" successfully
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:31 AM" with the following values:
      | ReturnQuantity | ReturnReason                        |
      | 5.000          | 0001 - Problem in product operation |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-20"

  # EBS-6956
  Scenario: (04) Save Edit SalesReturnOrder Item, SRO REASON doesn't exist (Validation Failure)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:00 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:02 AM" with the following values:
      | ReturnQuantity | ReturnReason                   |
      | 5.000          | 9999 - Not Exist Return Reason |
    Then a failure notification is sent to "Manar.Mohammed" with the following message "Gen-msg-05"
    And the following error message is attached to "returnReasonCode" field "Gen-msg-48" and sent to "Manar.Mohammed"

  # EBS-6956
  Scenario Outline: (05) Save Edit SalesReturnOrder Item, with Missing Mandatories (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:00 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:31 AM" with the following values:
      | ReturnQuantity   | ReturnReason   |
      | <ReturnQuantity> | <ReturnReason> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
    Examples:
      | ReturnReason                        | ReturnQuantity |
      | 0001 - Problem in product operation |                |
      | 0001 - Problem in product operation | ""             |

  # EBS-6956
  Scenario Outline: (06) Save Edit SalesReturnOrder Item, with malicious input  (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesReturnOrder with Code "2020000055" is locked by "Manar.Mohammed" at "09-Jul-2020 11:00 AM"
    When "Manar.Mohammed" saves the following edited SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with Code "2020000055" at "09-Jul-2020 11:31 AM" with the following values:
      | ReturnQuantity   | ReturnReason   |
      | <ReturnQuantity> | <ReturnReason> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
    Examples:
      | ReturnReason                        | ReturnQuantity |
      | ay7aga                              | 100.000        |
      | 0001 - Problem in product operation | 0              |
      | 0001 - Problem in product operation | -2.4           |
      | 0001 - Problem in product operation | Ay 7aga        |
