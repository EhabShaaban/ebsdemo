#Author: Eman Mansour, Shirin Mahmoud

Feature: Sales Return Order - Save Edit Item Auth

  Background:
    Given the following users and roles exist:
      | Name                                   | Role                                                            |
      | Manar.Mohammed                         | SalesCoordinator_Signmedia                                      |
      | Manar.Mohammed.NoReturnReasonNoMeasure | SalesCoordinator_Signmedia_CannotViewReturnReasonAndMeasureRole |
    And the following roles and sub-roles exist:
      | Role                                                            | Subrole            |
      | SalesCoordinator_Signmedia                                      | SROOwner_Signmedia |
      | SalesCoordinator_Signmedia                                      | ReturnReasonViewer |
      | SalesCoordinator_Signmedia                                      | MeasuresViewer     |
      | SalesCoordinator_Signmedia_CannotViewReturnReasonAndMeasureRole | SROOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                   | Condition                      |
      | SROOwner_Signmedia | SalesReturnOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
      | ReturnReasonViewer | Reason:ReadAll               | [type='SRO_REASON']            |
      | MeasuresViewer     | Measures:ReadAll             |                                |
    And the following users doesn't have the following permissions:
      | User                                   | Permission       |
      | Manar.Mohammed.NoReturnReasonNoMeasure | Reason:ReadAll   |
      | Manar.Mohammed.NoReturnReasonNoMeasure | Measures:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                   | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:UpdateItems | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100008 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019100008 | 0002 - DigiPro | 0001 - Flexo     |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State |
      | 2020000054 | 05-Jul-2020 12:37 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
      | 2020000055 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000054 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer          | CurrencyISO |
      | 2020000054 | 2019100008 | 000001 - Al Ahram | EGP         |
      | 2020000055 | 2019000001 | 000001 - Al Ahram | EGP         |
    And Insert the following Item with Id for SalesReturnOrders exist:
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason                      | ReturnQuantity | SalesPrice |
      | 2020000054 | 10        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 0002 - Problem in product quality | 1.000          | 6350.000   |
      | 2020000055 | 11        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |                                   | 1.000          | 63436.500  |
      | 2020000055 | 12        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |                                   | 1.000          | 999.000    |
      | 2020000055 | 13        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |                                   | 1.000          | 63436.500  |
      | 2020000055 | 14        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           |                                   | 1.000          | 10000.000  |

    And the following Reasons exist:
      | Reason                              | Type       |
      | 0001 - Problem in product operation | SRO_REASON |
    And edit session is "30" minutes

  # EBS-6957
  Scenario Outline: (01) Save Edit SalesReturnOrder Item, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves the following edited SROItem with id <SROItemId> in SalesReturnOrderItems section of SalesReturnOrder with Code "<Code>" at "09-Jul-2020 11:55 AM" with the following values:
      | ReturnQuantity | ReturnReason                        |
      | 5.000          | 0001 - Problem in product operation |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | Code       | SROItemId | User                                   |
      | 2020000054 | 10        | Manar.Mohammed                         |
      | 2020000055 | 11        | Manar.Mohammed.NoReturnReasonNoMeasure |
