# Author Dev: Fatma Al-Zahraa
# Author Quality: Shirin Mahmoud
# Reviewers: Somaya Ahmed & Hosam Bayomy

Feature: Create SalesReturnOrder - Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SRODocumentOwnerRole       |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                      |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia           |
      | SalesCoordinator_Signmedia | PurUnitReader_Signmedia      |
      | SalesCoordinator_Signmedia | CustomerViewer_Signmedia     |
      | SalesCoordinator_Signmedia | SalesInvoiceViewer_Signmedia |
      | SalesCoordinator_Signmedia | DocumentOwnerViewer          |
      | SalesCoordinator_Signmedia | SalesReturnOrderTypeViewer   |
      | SRODocumentOwnerRole       | SRODocumentOwnerSubRole      |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                          | Condition                      |
      | SROOwner_Signmedia           | SalesReturnOrder:Create             |                                |
      | PurUnitReader_Signmedia      | PurchasingUnit:ReadAll              | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia     | Customer:ReadAll                    | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia | SalesInvoice:ReadAll                | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll               |                                |
      | SalesReturnOrderTypeViewer   | SalesReturnOrderType:ReadAll        |                                |
      | SRODocumentOwnerSubRole      | SalesReturnOrder:CanBeDocumentOwner |                                |
    And the following SalesReturnOrderTypes exist:
      | Code                         | Name                   |
      | RETURN_SALES_ORDER_FOR_ITEMS | Sales return for items |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
      | 0001 | Flexo     |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000001 | Al Ahram              | 0002 - Signmedia |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |

    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject   | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | SalesReturnOrder | CanBeDocumentOwner |           |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |

  #EBS-6722
  Scenario Outline: (01) Create SalesReturnOrder, with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" creates SalesReturnOrder with the following values:
      | Type           | SalesInvoice           | DocumentOwnerId         |
      | <SelectedType> | <SelectedSalesInvoice> | <SelectedDocumentOwner> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page

    Examples:
      | SelectedType                 | SelectedSalesInvoice | SelectedDocumentOwner |
      |                              | 2019000001           | 1000072               |
      | ""                           | 2019000001           | 1000072               |
      | RETURN_SALES_ORDER_FOR_ITEMS |                      | 1000072               |
      | RETURN_SALES_ORDER_FOR_ITEMS | ""                   | 1000072               |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001           |                       |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001           | ""                    |

  #EBS-6722
  Scenario Outline: (02) Create SalesReturnOrder, with incorrect data entry (Validation Failure)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" creates SalesReturnOrder with the following values:
      | Type           | SalesInvoice           | DocumentOwnerId         |
      | <SelectedType> | <SelectedSalesInvoice> | <SelectedDocumentOwner> |
    Then a failure notification is sent to "Manar.Mohammed" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Manar.Mohammed"

    Examples:
      | SelectedType                 | SelectedSalesInvoice | SelectedDocumentOwner | Field           | ErrMsg     |
      # documentOwner does not exist
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001           | 99999999              | documentOwnerId | Gen-msg-48 |

  #EBS-6722
  Scenario Outline: (03) Create SalesReturnOrder, with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" creates SalesReturnOrder with the following values:
      | Type           | SalesInvoice           | DocumentOwnerId         |
      | <SelectedType> | <SelectedSalesInvoice> | <SelectedDocumentOwner> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page

    Examples:
      | SelectedType                 | SelectedSalesInvoice | SelectedDocumentOwner |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001           | 99999.999             |
      | RETURN_SALES_ORDER_FOR_ITEMS | 2019000001           | ay7aga                |
      | RETURN_SALES_ORDER_FOR_ITEMS | 20190000001          | 1000072               |
      | RETURN_SALES_ORDER_FOR_ITEMS | ay7aga               | 1000072               |
      | ay7aga                       | 2019000001           | 1000072               |
      # salesInvoice does not exist
      | RETURN_SALES_ORDER_FOR_ITEMS | 9999999999           | 1000072               |
