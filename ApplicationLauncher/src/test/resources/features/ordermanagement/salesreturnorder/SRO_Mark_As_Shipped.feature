# Author: Eman Mansour, Waseem Salama, Shirin Mahmoud (EBS-6720, EBS-6730, EBS-6732)
        # Author: Mohamed Aboelnour, Eslam Salam (EBS-7056)
        # Reviewer: Hosam Bayomy & Somaya Ahmed (10-Aug-2020)

Feature: Mark As Shipped Sales Return Order

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                     | Condition                      |
      | SROOwner_Signmedia | SalesReturnOrder:MarkAsShipped | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                     | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:MarkAsShipped | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100014 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2020000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100008 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019100014 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2020000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019100008 | 0001 - AL Madina | 0001 - Flexo     |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019100014 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2020000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019100008 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000001 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 12.000         | 10000.000       | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 100.000        | 999.000         | 0019 - M2       |
      | 2019000001 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.000        | 999.000         | 0003 - Kilogram |
      | 2019100014 | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0019 - M2           | 20.000         | 200.000         | 0019 - M2       |
      | 2020000001 | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 20.000         | 952.500         | 0019 - M2       |
      | 2019100008 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 20.000         | 6350.000        | 0003 - Kilogram |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner       | State                 |
      | 2020000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000051 | 25-Jun-2020 09:48 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000052 | 05-Jul-2020 12:37 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Draft                 |
      | 2020000054 | 05-Jul-2020 12:37 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Draft                 |
      | 2020000055 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | Completed             |
      | 2020000004 | 15-Jun-2020 02:22 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | CreditNoteActivated   |
      | 2020000003 | 15-Jun-2020 02:11 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | GoodsReceiptActivated |
      | 2020000002 | 15-Jun-2020 02:05 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Shipped               |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000051 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000052 | 0002 - Signmedia | 0003 - HPS       |
      | 2020000054 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000005 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000004 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000003 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000002 | 0002 - Signmedia | 0002 - DigiPro   |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer          | CurrencyISO |
      | 2020000001 | 2019100014 | 000001 - Al Ahram | EGP         |
      | 2020000051 | 2019100014 | 000001 - Al Ahram | EGP         |
      | 2020000052 | 2020000001 | 000001 - Al Ahram | EGP         |
      | 2020000054 | 2019100008 | 000001 - Al Ahram | EGP         |
      | 2020000055 | 2019000001 | 000001 - Al Ahram | EGP         |
      | 2020000005 | 2019000001 | 000001 - Al Ahram | EGP         |
      | 2020000004 | 2019000001 | 000001 - Al Ahram | EGP         |
      | 2020000003 | 2019000001 | 000001 - Al Ahram | EGP         |
      | 2020000002 | 2019000001 | 000001 - Al Ahram | EGP         |
    And Insert the following Item with Id for SalesReturnOrders exist:
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000001 | 1         | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0019 - M2           | 0001 - Problem in product operation | 16.000         | 200.000    |
      | 2020000052 | 7         | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 0005 - Items doesn't match the need | 25.000         | 952.500    |
      | 2020000054 | 10        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 0002 - Problem in product quality   | 1.000          | 6350.000   |
      | 2020000055 | 11        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |                                     | 1.000          | 63436.500  |
      | 2020000055 | 12        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |                                     | 1.000          | 999.000    |
      | 2020000055 | 13        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |                                     | 1.000          | 63436.500  |
      | 2020000055 | 14        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           |                                     | 1.000          | 10000.000  |

    And the following SalesReturnOrders have no Items:
      | Code       |
      | 2020000051 |

  #EBS-6720, EBS-7056
  Scenario: (01) MarkAsShipped SalesReturnOrder, where SalesReturnOrder is Draft, with all mandatory fields - by an authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "2020000001" at "17-Jun-2020 01:51 PM"
    Then SalesReturnOrder with code "2020000001" is updated with shipped state as follows:
      | LastUpdatedBy  | LastUpdateDate       | State   |
      | Manar.Mohammed | 17-Jun-2020 01:51 PM | Shipped |
    And CycleDates of SalesReturnOrder with code "2020000001" is updated as follows:
      | ShippingDate         |
      | 17-Jun-2020 01:51 PM |
    And ReturnQuantity field in SalesInvoiceItems section of SalesInvoice with code "2019100014" is updated as follows:
      | Item                                          | Qty    | ReturnQuantity | OrderUnit           | SalesPrice | TotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 20.000 | 16.000         | 0019 - Square Meter | 200.000    | 4000.000    |
    And a success notification is sent to "Manar.Mohammed" with the following message "SRO-msg-01"

        #Exception Cases
        #EBS-6730
  Scenario Outline: (02) MarkAsShipped SalesReturnOrder, where SalesReturnOrder is Draft, with missing mandatory fields - by an authorized user (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "<SROCode>" at "17-Jun-2020 01:51 PM"
    Then a failure notification is sent to "Manar.Mohammed" with the following message "Gen-msg-40"
    And the following fields "<Missing Fields>" which sent to "Manar.Mohammed" are marked as missing
    Examples:
      | SROCode    | Missing Fields   |
      | 2020000051 | ItemsList        |
      | 2020000055 | returnReasonCode |

        #EBS-6734
  Scenario: (03) MarkAsShipped SalesReturnOrder, where SalesReturnOrder is Draft, with item ReturnQuantity > MaxReturnQuantity that can be returned in SI - by an authorized user (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "2020000052" at "17-Jun-2020 01:51 PM"
    Then a failure notification is sent to "Manar.Mohammed" with the following message "SRO-msg-02"

        # EBS-6734
  Scenario Outline: (04) MarkAsShipped SalesReturnOrder, where action is not allowed in current state - All states except Draft (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to MarkAsShipped SalesReturnOrder with code "<SalesReturnOrderCode>" at "17-Jun-2020 01:51 PM"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-32"
    Examples:
      | SalesReturnOrderCode |
      | 2020000005           |
      | 2020000004           |
      | 2020000003           |
      | 2020000002           |

        #EBS-6734
  Scenario: (05) MarkAsShipped SalesReturnOrder, where SalesReturnOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "2020000001" at "17-Jun-2020 01:51 PM"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

        #EBS-6734
  Scenario: (06) MarkAsShipped SalesReturnOrder, when SalesReturnOrderItems section is locked by another user (Exception)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first requested to Edit SROItem with id 1 in SalesReturnOrderItems section of SalesReturnOrder with code "2020000001" successfully
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "2020000001" at "17-Jun-2020 01:51 PM"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-14"

        #Abuse Cases
        #EBS-6732
  Scenario: (07) MarkAsShipped SalesReturnOrder, by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to MarkAsShipped SalesReturnOrder with code "2020000054" at "17-Jun-2020 01:51 PM"
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page