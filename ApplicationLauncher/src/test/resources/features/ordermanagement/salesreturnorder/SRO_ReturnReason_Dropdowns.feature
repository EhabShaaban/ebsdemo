# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud
Feature: View All ReturnReasons

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Afaf           | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SalesCoordinator_Signmedia | ReturnReasonViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission     | Condition           |
      | ReturnReasonViewer | Reason:ReadAll | [type='SRO_REASON'] |
    And the following users doesn't have the following permissions:
      | User | Permission     |
      | Afaf | Reason:ReadAll |
    And the following Reasons exist:
      | Reason                                 | Type       |
      | 0001 - Problem in product operation    | SRO_REASON |
      | 0002 - Problem in product quality      | SRO_REASON |
      | 0003 - Quantity doesn't match the need | SRO_REASON |
      | 0004 - Price doesn't match the need    | SRO_REASON |
      | 0005 - Items doesn't match the need    | SRO_REASON |
      | 0006 - Problem in collection owner     | SRO_REASON |
      | 0007 - Updating customers' information | SRO_REASON |
    And the total number of existing ReturnReasons is 7

  Scenario: (01) Read list of ReturnReasons dropdown by authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read all SalesReturnOrder Reasons
    Then the following ReturnReasons values will be presented to "Manar.Mohammed":
      | Reason                                 |
      | 0007 - Updating customers' information |
      | 0006 - Problem in collection owner     |
      | 0005 - Items doesn't match the need    |
      | 0004 - Price doesn't match the need    |
      | 0003 - Quantity doesn't match the need |
      | 0002 - Problem in product quality      |
      | 0001 - Problem in product operation    |
    And total number of ReturnReasons returned to "Manar.Mohammed" is equal to 7

  Scenario: (02) Read list of ReturnReasons dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesReturnOrder Reasons
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  @Future
  Scenario: (03) Read list of ReturnReasons dropdown by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "??"
    When "??" requests to read all SalesReturnOrder Reasons
    Then "??" is logged out
    And "??" is forwarded to the error page