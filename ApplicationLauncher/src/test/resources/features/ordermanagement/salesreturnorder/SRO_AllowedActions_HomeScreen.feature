#Author: Eman Mansour, Shirin Mahmoud

Feature:Home Screen Allowed Actions Sales Return Order

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Seif     | Quality_Specialist         |
      | CreateOnlyUser | CreateOnlyRole             |
      | Afaf           | FrontDesk                  |

    And the following roles and sub-roles exist:
      | Role                       | Subrole             |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia  |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia |
      | Quality_Specialist         | SROViewer           |
      | CreateOnlyRole             | CreateOnlySubRole   |

    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | SROOwner_Signmedia  | SalesReturnOrder:Create  |                                |
      | SROViewer           | SalesReturnOrder:ReadAll |                                |
      | SROViewer_Signmedia | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole   | SalesReturnOrder:Create  |                                |

    And the following users doesn't have the following permissions:
      | User           | Permission               |
      | Ahmed.Seif     | SalesReturnOrder:Create  |
      | Afaf           | SalesReturnOrder:Create  |
      | Afaf           | SalesReturnOrder:ReadAll |
      | CreateOnlyUser | SalesReturnOrder:ReadAll |

  Scenario Outline: (01)  Read allowed actions in Sales Return Order Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesReturnOrder home screen
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User           | AllowedActions |
      | Manar.Mohammed | ReadAll,Create |
      | Ahmed.Seif     | ReadAll        |
      | CreateOnlyUser | Create         |

  Scenario: (02) Read allowed actions in Sales Return Order Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of SalesReturnOrder home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"