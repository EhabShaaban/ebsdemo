# Author Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud
# Reviewer: Somaya Ahmed (14-Jul-2020)

Feature: Delete SROItem From SalesReturnOrderItems Section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole             |
      | SalesCoordinator_Signmedia | SROOwner_Signmedia  |
      | SalesCoordinator_Signmedia | SROViewer_Signmedia |
      | SuperUser                  | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                   | Condition                      |
      | SROOwner_Signmedia  | SalesReturnOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
      | SROViewer_Signmedia | SalesReturnOrder:ReadItems   | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole    | *:*                          |                                |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                   | Condition                  |
      | Manar.Mohammed | SalesReturnOrder:UpdateItems | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesReturnOrder:ReadItems   | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000005 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019000006 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro | 0001 - Flexo     |
      | 2019000002 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019000003 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019000004 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019000005 | 0002 - DigiPro | 0001 - Flexo     |
      | 2019000006 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019000002 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019000003 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019000004 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019000005 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2019000006 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000001 | 000051 - Ink5                                                    | 0014 - Liter        | 10.000         | 130.000         | 0003 - Kilogram |
      | 2019000002 | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 30.000         | 12700.000       | 0019 - M2       |
      | 2019000003 | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 5.000          | 9525.000        | 0019 - M2       |
      | 2019000004 | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 1.000          | 10.000          | 0019 - M2       |
      | 2019000005 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 1.000          | 6350.000        | 0003 - Kilogram |
      | 2019000006 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 1.000          | 63436.500       | 0003 - Kilogram |
      | 2019000006 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 1.000          | 999.000         | 0003 - Kilogram |
      | 2019000006 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 1.000          | 63436.500       | 0019 - M2       |
      | 2019000006 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 1.000          | 10000.000       | 0003 - Kilogram |

    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State                 |
      | 2020000003 | 15-Jun-2020 02:11 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | GoodsReceiptActivated |
      | 2020000004 | 15-Jun-2020 02:22 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | CreditNoteActivated   |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Completed             |
      | 2020000053 | 29-Jun-2020 11:21 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Shipped               |
      | 2020000054 | 05-Jul-2020 12:37 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft                 |
      | 2020000055 | 09-Jul-2020 11:50 AM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Draft                 |
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000003 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000004 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000005 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000053 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000054 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer                       | CurrencyISO |
      | 2020000003 | 2019000001 | 000006 - المطبعة الأمنية       | EGP         |
      | 2020000004 | 2019000002 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2020000005 | 2019000003 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2020000053 | 2019000004 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2020000054 | 2019000005 | 000001 - Al Ahram              | EGP         |
      | 2020000055 | 2019000006 | 000001 - Al Ahram              | EGP         |
    And Insert the following Item with Id for SalesReturnOrders exist:
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000003 | 3         | 000051 - Ink5                                                    | 0014 - Liter        | 0001 - Problem in product operation | 10.000         | 130.000    |
      | 2020000004 | 4         | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 0001 - Problem in product operation | 30.000         | 12700.000  |
      | 2020000005 | 5         | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 0001 - Problem in product operation | 5.000          | 9525.000   |
      | 2020000053 | 8         | 000053 - Hot Laminated Frontlit Fabric roll 2                    | 0035 - Roll 1.27x50 | 0001 - Problem in product operation | 1.000          | 10.000     |
      | 2020000054 | 10        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 0002 - Problem in product quality   | 1.000          | 6350.000   |
      | 2020000055 | 11        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           |                                     | 1.000          | 63436.500  |
      | 2020000055 | 12        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |                                     | 1.000          | 999.000    |
      | 2020000055 | 13        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |                                     | 1.000          | 63436.500  |
      | 2020000055 | 14        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           |                                     | 1.000          | 10000.000  |

  # EBS-6724
  Scenario: (01) Delete SROItem from SalesReturnOrder, where SalesReturnOrder is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to delete SROItem with id 11 in SalesReturnOrder with code "2020000055"
    Then SROItem with id 11 from SalesReturnOrder with code "2020000055" is deleted
    And Only the following Items remaining in SalesReturnOrder with code "2020000055":
      | Code       | SROItemId | Item                                                             | OrderUnit           | ReturnReason | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 2020000055 | 12        | 000008 - PRO-V-ST-2                                              | 0003 - Kg           |              | 1.000          | 1.000             | 999.000    | 999.000         |
      | 2020000055 | 13        | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 |              | 1.000          | 1.000             | 63436.500  | 63436.500       |
      | 2020000055 | 14        | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           |              | 1.000          | 1.000             | 10000.000  | 10000.000       |
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-12"

  # EBS-6724
  Scenario: (02) Delete SROItem from SalesReturnOrder, where SROItem doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted SROItem with id 11 of SalesReturnOrder with code "2020000055" successfully
    When "Manar.Mohammed" requests to delete SROItem with id 11 in SalesReturnOrder with code "2020000055"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-20"

  # EBS-6724
  Scenario Outline: (03) Delete SROItem from SalesReturnOrder, where action is not allowed in current state - All states except Draft (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to delete SROItem with id <SROItemId> in SalesReturnOrder with code "<SalesReturnOrderCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-21"
    Examples:
      | SalesReturnOrderCode | SROItemId |
      | 2020000003           | 3         |
      | 2020000004           | 4         |
      | 2020000005           | 5         |
      | 2020000053           | 8         |

  # EBS-6724
  Scenario: (04) Delete SROItem from SalesReturnOrder, where SalesReturnOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesReturnOrder with code "2020000055" successfully
    When "Manar.Mohammed" requests to delete SROItem with id 11 in SalesReturnOrder with code "2020000055"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-6724
  Scenario: (05) Delete SROItem from SalesReturnOrder, when SalesReturnOrderItems section are locked by another user (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first requested to Edit SROItem with id 11 in SalesReturnOrderItems section of SalesReturnOrder with code "2020000055" successfully
    When "Manar.Mohammed" requests to delete SROItem with id 11 in SalesReturnOrder with code "2020000055"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-02"

  # EBS-6724
  Scenario Outline: (06) Delete SROItem from SalesReturnOrder, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete SROItem with id <SROItemId> in SalesReturnOrder with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | Code       | SROItemId |
      | Manar.Mohammed | 2020000054 | 10        |
