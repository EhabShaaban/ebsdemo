# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud
Feature: View All SalesReturnOrderTypes

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Afaf           | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                    |
      | SalesCoordinator_Signmedia | SalesReturnOrderTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                   | Condition |
      | SalesReturnOrderTypeViewer | SalesReturnOrderType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission                   |
      | Afaf | SalesReturnOrderType:ReadAll |
    And the following SalesReturnOrderTypes exist:
      | Code                         | Name                   |
      | RETURN_SALES_ORDER_FOR_ITEMS | Sales return for items |
    And the total number of existing SalesReturnOrderTypes is 1

  Scenario: (01) Read list of SalesReturnOrderTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read all SalesReturnOrderTypes
    Then the following SalesReturnOrderTypes values will be presented to "Manar.Mohammed":
      | SalesReturnOrderType                                  |
      | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items |
    And total number of SalesReturnOrderTypes returned to "Manar.Mohammed" is equal to 1

  Scenario: (02) Read list of SalesReturnOrderTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesReturnOrderTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page