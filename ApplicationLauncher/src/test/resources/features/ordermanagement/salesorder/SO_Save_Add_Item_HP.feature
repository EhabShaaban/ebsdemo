# Author Dev: Waseem Salama
# Author Quality: Shirin Mahmoud

Feature: Sales Order - Save New Item HP

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole              |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia    |
      | SalesCoordinator_Signmedia | ItemViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition                      |
      | SOOwner_Signmedia    | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia | Item:ReadAll           | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia | Item:ReadUoMData       | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                          | OrderUnit           | Qty     | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5           | 0020 - 20Kg         | 10.000  | 2000.000   |
      | 2020000005 | 000014 - Insurance service                    | 0020 - 20Kg         | 2.000   | 50.000     |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |

    And the following Items exist with the below details:
      | Item                                          | Type       | UOM       | PurchaseUnit     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000053 - Hot Laminated Frontlit Fabric roll 2":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And edit session is "30" minutes

  # EBS-5129
  Scenario: (01) Save Add SalesOrder Item, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                                          | OrderUnit | Qty        | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.123456 | 10.123456  |
    Then SalesOrder with Code "2020000005" is updated as follows:
      | LastUpdatedBy  | LastUpdateDate       |
      | Manar.Mohammed | 01-Jan-2019 11:10 AM |
    And the lock by "Manar.Mohammed" on Items section of SalesOrder with Code "2020000005" is released
    And a success notification is sent to "Manar.Mohammed" with the following message "Gen-msg-04"
    And Items section of SalesOrder with Code "2020000005" is updated as follows and displayed to "Manar.Mohammed":
      | SOCode     | Item                                          | OrderUnit           | Qty        | SalesPrice | TotalAmount       | LastSalesPrice | AvailableQty |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 100.123456 | 10.123456  | 1013.595401383936 |                | 0.000        |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000    | 4.000      | 400.000           |                | 0.000        |
      | 2020000005 | 000014 - Insurance service                    | 0020 - 20Kg         | 2.000      | 50.000     | 100.000           |                |              |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5           | 0020 - 20Kg         | 10.000     | 2000.000   | 20000.000         |                |              |
    And TaxesSummary section of SalesOrder with code "2020000005" is updated as follows and displayed to "Manar.Mohammed":
      | Tax                                          | TaxAmount           |
      | 0007 - Vat                                   | 215.13595401383936  |
      | 0008 - Commercial and industrial profits tax | 107.56797700691968  |
      | 0009 - Real estate taxes                     | 2151.3595401383936  |
      | 0010 - Service tax                           | 2581.63144816607232 |
      | 0011 - Adding tax                            | 107.56797700691968  |
      | 0012 - Sales tax                             | 2581.63144816607232 |
    And SalesOrderSummary section of SalesOrder with code "2020000005" is updated as follows and displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes          | TotalAmountAfterTaxes |
      | 21513.595401383936     | 7744.89434449821696 | 29258.48974588215296  |
