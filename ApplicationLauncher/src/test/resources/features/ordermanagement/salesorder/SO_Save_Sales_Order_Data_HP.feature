 # Author: Zyad Ghorab, Reviewer: Somaya Ahmed
# updated by: Fatma Al Zahraa (EBS - 6167)

 Feature: Save SalesOrderData section in SalesOrder-Happy Path

   Background:
     Given the following users and roles exist:
       | Name           | Role                      |
       | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
     And the following roles and sub-roles exist:
       | Role                      | Sub-role                       |
       | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
       | SalesSpecialist_Signmedia | CustomerViewer_Signmedia       |
       | SalesSpecialist_Signmedia | PaymentTermViewer              |
       | SalesSpecialist_Signmedia | CurrencyViewer                 |
     And the following sub-roles and permissions exist:
       | Subrole                        | Permission                      | Condition                                                                 |
       | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
       | CustomerViewer_Signmedia       | Customer:ReadAddress            | [purchaseUnitName='Signmedia']                                            |
       | CustomerViewer_Signmedia       | Customer:ReadContactPerson      | [purchaseUnitName='Signmedia']                                            |
       | PaymentTermViewer              | PaymentTerms:ReadAll            |                                                                           |
       | CurrencyViewer                 | Currency:ReadAll                |                                                                           |

     #@INSERT
     And the following GeneralData for SalesOrders exist:
       | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
       | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft    | 11-Feb-2020 01:28 PM |Manar Mohammed |
       | 2020000051 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Rejected | 11-Feb-2020 01:28 PM |Manar Mohammed |
     #@INSERT
     And the following Company for SalesOrders exist:
       | Code       | Company          | Store                      |
       | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
       | 2020000051 | 0001 - AL Madina | 0001 - AlMadina Main Store |
     #@INSERT
     And the following SalesOrderData for SalesOrders exist:
       | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
       | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 30-May-2020 09:28 AM |       |
       | 2020000051 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 02-Aug-2020 09:28 AM |       |

     And the following Currencies exist:
       | Code | Name           | ISO |
       | 0001 | Egyptian Pound | EGP |

     And the following PaymentTerms exist:
       | Code | Name                                |
       | 0001 | 20% Deposit, 80% T/T Before Loading |
       | 0002 | 20% advance, 80% Copy of B/L        |

     And the following Addresses for Customers exist:
       | Customer                       | CustomerAddressId | AddressName       | AddressDescription          |
       | 000007 - مطبعة أكتوبر الهندسية | 5                 | Giza              | 7th st, El Sheikh Zayed     |
       | 000007 - مطبعة أكتوبر الهندسية | 6                 | Heliopolis Branch | 99, El Hegaz St, Heliopolis |

     And the following ContactPersons for Customers exist:
       | Customer                       | CustomerContactPersonId | ContactPerson          |
       | 000007 - مطبعة أكتوبر الهندسية | 5                       | Wael Fathay            |
       | 000007 - مطبعة أكتوبر الهندسية | 8                       | October Contact Person |

     And edit session is "30" minutes

   #EBS-5405
   #EBS - 6167
   Scenario Outline: (01) Save SalesOrderData section within edit session by an authorized user for Draft/Rejected SalesOrders (Happy Path)
     Given user is logged in as "<User>"
     And SalesOrderData section of SalesOrder with code "<SalesOrderCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
     When "<User>" saves SalesOrderData section of SalesOrder with Code "<SalesOrderCode>" at "07-Jan-2019 09:30 AM" with the following values:
       | PaymentTerms   | CustomerAddress   | Currency   | ContactPerson   | ExpectedDeliveryDate   | Notes   |
       | <PaymentTerms> | <CustomerAddress> | <Currency> | <ContactPerson> | <ExpectedDeliveryDate> | <Notes> |
     Then SalesOrder with Code "<SalesOrderCode>" is updated as follows:
       | LastUpdatedBy | LastUpdateDate       |
       | <User>        | 07-Jan-2019 09:30 AM |
     And SalesOrderData section of SalesOrder with code "<SalesOrderCode>" is updated as follows:
       | PaymentTerms   | CustomerAddress   | Currency   | ContactPerson   | ExpectedDeliveryDate   | Notes   |
       | <PaymentTerms> | <CustomerAddress> | <Currency> | <ContactPerson> | <ExpectedDeliveryDate> | <Notes> |
     And the lock by "<User>" on SalesOrderData section of SalesOrder with code "<SalesOrderCode>" is released
     And a success notification is sent to "<User>" with the following message "Gen-msg-04"
     Examples:
       | User           | SalesOrderCode | PaymentTerms                               | CustomerAddress                 | Currency   | ContactPerson              | ExpectedDeliveryDate | Notes             |
      # Draft Sales Order
       | Ahmed.Al-Ashry | 2020000005     | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay            | 01-Jul-2020 12:44 PM | notes for save hp |
       | Ahmed.Al-Ashry | 2020000005     |                                            | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay            | 01-Jul-2020 12:44 PM | notes for save hp |
       | Ahmed.Al-Ashry | 2020000005     | 0001 - 20% Deposit, 80% T/T Before Loading |                                 | 0001 - EGP | 5 - Wael Fathay            | 01-Jul-2020 12:44 PM | notes for save hp |
       | Ahmed.Al-Ashry | 2020000005     | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP |                            | 01-Jul-2020 12:44 PM | notes for save hp |
       | Ahmed.Al-Ashry | 2020000005     | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay            |                      | notes for save hp |
       | Ahmed.Al-Ashry | 2020000005     | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay            | 01-Jul-2020 12:44 PM |                   |
      # Rejected Sales Order
       | Ahmed.Al-Ashry | 2020000051     | 0002 - 20% advance, 80% Copy of B/L        | 5 - 7th st, El Sheikh Zayed     | 0001 - EGP | 8 - October Contact Person | 02-Sep-2020 09:28 AM | notes for save hp |
       | Ahmed.Al-Ashry | 2020000051     | 0002 - 20% advance, 80% Copy of B/L        | 5 - 7th st, El Sheikh Zayed     | 0001 - EGP | 8 - October Contact Person | 02-Sep-2020 09:28 AM |                   |