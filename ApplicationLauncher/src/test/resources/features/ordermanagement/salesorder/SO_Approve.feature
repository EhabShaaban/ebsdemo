# Author Dev: Waseem Salama
# updated by: Fatma Al Zahraa (EBS - 6167)
# Author Quality: Shirin Mahmoud
# Reviewer: Somaya Ahmed

Feature: Approve Sales Order

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Gehan.Ahmed       | SalesManager_Signmedia |
      | Saad.Abdelmawgood | SalesManager_Flexo     |
      | Ahmed.Seif        | Quality_Specialist     |
    And the following roles and sub-roles exist:
      | Role                   | Subrole              |
      | SalesManager_Signmedia | SOApprover_Signmedia |
      | SalesManager_Flexo     | SOApprover_Flexo     |
      | Quality_Specialist     | SOApprover           |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission         | Condition                      |
      | SOApprover_Signmedia | SalesOrder:Approve | [purchaseUnitName='Signmedia'] |
      | SOApprover_Flexo     | SalesOrder:Approve | [purchaseUnitName='Flexo']     |
      | SOApprover           | SalesOrder:Approve |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission         | Condition                      |
      | Saad.Abdelmawgood | SalesOrder:Approve | [purchaseUnitName='Signmedia'] |

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM          | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00010002000200030000510014 | 0001 - Flexo     | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000051 - Ink5                                 | 0014 - Liter | UNRESTRICTED_USE | 3.0               |
      | UNRESTRICTED_USE00020002000200030000530019 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | UNRESTRICTED_USE | 2.0               |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | UNRESTRICTED_USE | 2.0               |

    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible           | CreatedBy                 | State                 | CreationDate         |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 01-Feb-2020 07:52 AM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Approved              | 04-Feb-2020 08:39 AM |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Rejected              | 04-Feb-2020 08:42 AM |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Canceled              | 04-Feb-2020 12:38 PM |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Expired               | 04-Feb-2020 12:39 PM |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | DeliveryComplete      | 04-Feb-2020 12:40 PM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed            | Admin from BDKCompanyCode | Draft                 | 11-Feb-2020 01:28 PM |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | SalesInvoiceActivated | 03-Aug-2020 01:31 PM |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | GoodsIssueActivated   | 03-Aug-2020 01:36 PM |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | ReadyForDelivery      | 03-Aug-2020 02:22 PM |
      | 2020000062 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 22-Mar-2020 09:24 AM |
      | 2020000063 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 09-Jun-2020 09:53 AM |
      | 2020000064 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 09-Jun-2020 09:53 AM |
      | 2020000065 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed            | Admin from BDKCompanyCode | Draft                 | 09-Jun-2020 09:54 AM |
      | 2020000066 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0002 - Mohammed            | Admin from BDKCompanyCode | Draft                 | 09-Jun-2020 11:45 AM |
    And the following CompanyAndStoreData for the following SalesOrders exist:
      | SOCode     | Company          | Store                      |
      | 2020000001 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
      | 2020000003 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
      | 2020000009 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000062 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000063 | 0001 - AL Madina |                            |
      | 2020000064 | 0001 - AL Madina |                            |
      | 2020000065 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000066 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
    And the following SalesOrderData for the following SalesOrders exist:
      | SOCode     | Customer                       | PaymentTerms                               | CustomerAddress             | CurrencyISO | ContactPerson           | ExpectedDeliveryDate | CreditLimit | Notes                                |
      | 2020000001 | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading | 66, Salah Salem St          | EGP         |                         | 05-Feb-2020 08:29 AM | 100000.0    | this is notes for sales order 000001 |
      | 2020000003 | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading | 7th st, El Sheikh Zayed     | EGP         | Client 2 Contact Person | 05-Apr-2020 08:42 AM | 100000.0    | this is notes for sales order 000003 |
      | 2020000009 | 000001 - Al Ahram              | 0002 - 20% advance, 80% Copy of B/L        | 66, Salah Salem St          | EGP         | Wael Fathay             | 07-Apr-2020 10:21 AM | 300000.0    | this is notes for sales order 000009 |
      | 2020000062 | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading | 66, Salah Salem St          | EGP         | Client 2 Contact Person |                      | 100000.0    |                                      |
      | 2020000063 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               | 99, El Hegaz St, Heliopolis | EGP         | Wael Fathay             | 09-Jun-2020 09:55 AM | 100000.0    |                                      |
      | 2020000064 | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |                             | EGP         | Wael Fathay             | 09-Jun-2020 09:57 AM | 100000.0    |                                      |
      | 2020000065 | 000007 - مطبعة أكتوبر الهندسية |                                            | 99, El Hegaz St, Heliopolis | EGP         | Wael Fathay             | 09-Jun-2020 09:58 AM | 100000.0    |                                      |
      | 2020000066 | 000004 - Client3               | 0001 - 20% Deposit, 80% T/T Before Loading | 7th st, El Sheikh Zayed     | EGP         | Wael Fathay             | 09-Jun-2020 11:51 AM |             |                                      |
    And the following SalesOrders have the following Items:
      | SOCode     | ItemId | ItemType                | Item                                          | OrderUnit           | Qty   | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000001 | 51     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000 | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000003 | 53     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000 | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000009 | 61     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 1.000 | 3.000      | 3.000       | 200.000        | 2.000        |
    And the following SalesOrders have no Items:
      | SOCode     |
      | 2020000063 |
      | 2020000064 |
      | 2020000065 |
      | 2020000066 |
  #HappyPath
  #EBS-6702
  Scenario: (01) Approve SalesOrder, where SalesOrder is Draft, with all mandatory fields - by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to approve SalesOrder with code "2020000009" at "07-Mar-2020 09:30 AM"
    Then SalesOrder with code "2020000009" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State    | TotalAmount | TotalRemaining |
      | Gehan.Ahmed   | 07-Mar-2020 09:30 AM | Approved | 3.000       | 3.000          |
    And CycleDates of SalesOrder with code "2020000009" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | ApprovalDate         |
      | Gehan.Ahmed   | 07-Mar-2020 09:30 AM | 07-Mar-2020 09:30 AM |
    And a success notification is sent to "Gehan.Ahmed" with the following message "SO-msg-03"

  #Exception Cases
  #EBS-6702
  #EBS-6167
  Scenario Outline: (02) Approve SalesOrder, where SalesOrder is Draft, with missing mandatory fields - by an authorized user (Exception Case)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to approve SalesOrder with code "<SOCode>" at "07-Mar-2020 09:30 AM"
    Then a failure notification is sent to "Ahmed.Seif" with the following message "Gen-msg-40"
    And the following fields "<Missing Fields>" which sent to "Ahmed.Seif" are marked as missing
    Examples:
      | SOCode     | Missing Fields                          |
      | 2020000063 | storeCode, ItemsList                    |
      | 2020000064 | storeCode, customerAddressId, ItemsList |
      | 2020000065 | paymentTermCode, ItemsList              |
      | 2020000066 | ItemsList                               |
      | 2020000062 | expectedDeliveryDate                    |
      | 2020000001 | contactPersonId                         |

  #EBS-6702
  Scenario: (03) Approve SalesOrder, where SalesOrder is Draft, with all mandatory fields, sales order does not exist - by an authorized user (Exception Case)
    Given user is logged in as "Ahmed.Seif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000009" successfully
    When "Ahmed.Seif" requests to approve SalesOrder with code "2020000009" at "07-Mar-2020 09:30 AM"
    Then an error notification is sent to "Ahmed.Seif" with the following message "Gen-msg-01"

  #EBS-6702
  Scenario Outline: (04) Approve SalesOrder, where SalesOrder is Draft, with all mandatory fields, sales order is locked by another user (Exception Case)
    Given user is logged in as "Ahmed.Seif"
    And another user is logged in as "hr1"
    And first "hr1" opens "<SOSection>" section of SalesOrder with code "2020000009" in edit mode
    When "Ahmed.Seif" requests to approve SalesOrder with code "2020000009" at "07-Mar-2020 09:30 AM"
    Then an error notification is sent to "Ahmed.Seif" with the following message "Gen-msg-14"
    Examples:
      | SOSection           |
      | CompanyAndStoreData |
      | SalesOrderData      |
      | Items               |

  #EBS-6702
  Scenario Outline: (05) Approve SalesOrder, where SalesOrder is where action is not allowed in current state: Approved, Rejected, Cancelled, Expired, GoodsIssueActivated, SalesInvoiceActivated, ReadyForDelivery, DeliveryComplete (Exception Case)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to approve SalesOrder with code "<SalesOrderCode>" at "07-Mar-2020 09:30 AM"
    Then an error notification is sent to "Ahmed.Seif" with the following message "Gen-msg-32"
    Examples:
      | SalesOrderCode |
      | 2020000003     |
      | 2020000004     |
      | 2020000006     |
      | 2020000007     |
      | 2020000008     |
      | 2020000010     |
      | 2020000011     |
      | 2020000012     |

  #Abuse Cases
  #EBS-6702
  Scenario: (06) Approve SalesOrder, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Saad.Abdelmawgood"
    When "Saad.Abdelmawgood" requests to approve SalesOrder with code "2020000009" at "07-Mar-2020 09:30 AM"
    Then "Saad.Abdelmawgood" is logged out
    And "Saad.Abdelmawgood" is forwarded to the error page
