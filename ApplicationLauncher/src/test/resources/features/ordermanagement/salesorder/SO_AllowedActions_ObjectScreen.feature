# Author: Zyad (Dev) and Shirin (Quality)

Feature: Object Screen Allowed Actions Sales Order

  Background:
    Given the following users and roles exist:
      | Name               | Role                      |
      | Ahmed.Al-Ashry     | SalesSpecialist_Signmedia |
      | Gehan.Ahmed        | SalesManager_Signmedia    |
      | CannotReadSections | CannotReadSectionsRole    |
      | hr1                | SuperUser                 |
      | Afaf               | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                         |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia  |
      | SalesSpecialist_Signmedia | SOViewer_LoggedInUser_Signmedia |
      | SalesManager_Signmedia    | SOViewer_Signmedia              |
      | SalesManager_Signmedia    | SOApprover_Signmedia            |
      | SuperUser                 | SuperUserSubRole                |
      | CannotReadSectionsRole    | CannotReadSectionsSubRole       |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                           | Condition                                                                 |
      | SuperUserSubRole                | *:*                                  |                                                                           |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:Delete                    | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:Cancel                    | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:UpdateSalesOrderData      | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:UpdateItems               | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:SubmitForApproval         | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |

      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll                   | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadCompanyAndStoreData   | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadSalesOrderData        | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadItems                 | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadApprovals             | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |

      | SOViewer_Signmedia              | SalesOrder:ReadAll                   | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_Signmedia              | SalesOrder:ReadCompanyAndStoreData   | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_Signmedia              | SalesOrder:ReadSalesOrderData        | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_Signmedia              | SalesOrder:ReadItems                 | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_Signmedia              | SalesOrder:ReadApprovals             | [purchaseUnitName='Signmedia']                                            |

      | SOApprover_Signmedia            | SalesOrder:Approve                   | [purchaseUnitName='Signmedia']                                            |
      | SOApprover_Signmedia            | SalesOrder:Reject                    | [purchaseUnitName='Signmedia']                                            |

      | CannotReadSectionsSubRole       | SalesOrder:ReadAll                   |                                                                           |
      | CannotReadSectionsSubRole       | SalesOrder:Delete                    |                                                                           |
      | CannotReadSectionsSubRole       | SalesOrder:Cancel                    |                                                                           |
      | CannotReadSectionsSubRole       | SalesOrder:SubmitForApproval         |                                                                           |
      | CannotReadSectionsSubRole       | SalesOrder:Approve                   |                                                                           |
      | CannotReadSectionsSubRole       | SalesOrder:Reject                    |                                                                           |
    And the following users doesn't have the following permissions:
      | User               | Permission                           |
      | Afaf               | SalesOrder:Delete                    |
      | Afaf               | SalesOrder:Cancel                    |
      | Afaf               | SalesOrder:SubmitForApproval         |
      | Afaf               | SalesOrder:Approve                   |
      | Afaf               | SalesOrder:Reject                    |
      | Afaf               | SalesOrder:ReadAll                   |
      | Afaf               | SalesOrder:ReadCompanyAndStoreData   |
      | Afaf               | SalesOrder:ReadSalesOrderData        |
      | Afaf               | SalesOrder:ReadItems                 |
      | Afaf               | SalesOrder:ReadApprovals             |
      | Afaf               | SalesOrder:UpdateSalesOrderData      |
      | Afaf               | SalesOrder:UpdateCompanyAndStoreData |
      | Afaf               | SalesOrder:UpdateItems               |

      | CannotReadSections | SalesOrder:UpdateSalesOrderData      |
      | CannotReadSections | SalesOrder:UpdateCompanyAndStoreData |
      | CannotReadSections | SalesOrder:UpdateItems               |
      | CannotReadSections | SalesOrder:ReadCompanyAndStoreData   |
      | CannotReadSections | SalesOrder:ReadSalesOrderData        |
      | CannotReadSections | SalesOrder:ReadItems                 |
      | CannotReadSections | SalesOrder:ReadApprovals             |

      | Ahmed.Al-Ashry     | SalesOrder:Approve                   |
      | Ahmed.Al-Ashry     | SalesOrder:Reject                    |

      | Gehan.Ahmed        | SalesOrder:Delete                    |
      | Gehan.Ahmed        | SalesOrder:Cancel                    |
      | Gehan.Ahmed        | SalesOrder:SubmitForApproval         |
      | Gehan.Ahmed        | SalesOrder:UpdateSalesOrderData      |
      | Gehan.Ahmed        | SalesOrder:UpdateCompanyAndStoreData |
      | Gehan.Ahmed        | SalesOrder:UpdateItems               |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                           | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:Delete                    | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:DeleteItem                | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:Cancel                    | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:UpdateSalesOrderData      | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:UpdateItems               | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:SubmitForApproval         | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:ReadAll                   | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:ReadCompanyAndStoreData   | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:ReadSalesOrderData        | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
      | Ahmed.Al-Ashry | SalesOrder:ReadItems                 | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

      | Gehan.Ahmed    | SalesOrder:ReadAll                   | [purchaseUnitName='Flexo']                                            |
      | Gehan.Ahmed    | SalesOrder:ReadCompanyAndStoreData   | [purchaseUnitName='Flexo']                                            |
      | Gehan.Ahmed    | SalesOrder:ReadSalesOrderData        | [purchaseUnitName='Flexo']                                            |
      | Gehan.Ahmed    | SalesOrder:ReadItems                 | [purchaseUnitName='Flexo']                                            |
      | Gehan.Ahmed    | SalesOrder:Approve                   | [purchaseUnitName='Flexo']                                            |
      | Gehan.Ahmed    | SalesOrder:Reject                    | [purchaseUnitName='Flexo']                                            |
    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000056 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 07-Aug-2020 09:28 AM | 0002 - Signmedia | DeliveryComplete      |
      | 2020000055 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 06-Aug-2020 09:28 AM | 0002 - Signmedia | Expired               |
      | 2020000054 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 05-Aug-2020 09:28 AM | 0002 - Signmedia | Canceled              |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved              |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 03-Aug-2020 09:28 AM | 0002 - Signmedia | WaitingApproval       |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 02-Aug-2020 09:28 AM | 0002 - Signmedia | Rejected              |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |

  # EBS-5411
  Scenario Outline: (01) Read allowed actions in SalesOrder Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesOrder with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | AllowedActions                                                                                                                                                          | NoOfActions |
      | Ahmed.Al-Ashry     | 2020000005 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals, UpdateSalesOrderData, UpdateCompanyAndStoreData, UpdateItems, Delete                    | 9           |
      | Ahmed.Al-Ashry     | 2020000052 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Ahmed.Al-Ashry     | 2020000053 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Ahmed.Al-Ashry     | 2020000051 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals, UpdateSalesOrderData, UpdateCompanyAndStoreData, UpdateItems, SubmitForApproval, Cancel | 10          |
      | Ahmed.Al-Ashry     | 2020000056 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Ahmed.Al-Ashry     | 2020000055 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Ahmed.Al-Ashry     | 2020000054 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Ahmed.Al-Ashry     | 2020000011 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |

      | Gehan.Ahmed        | 2020000005 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals, Approve                                                                                 | 6           |
      | Gehan.Ahmed        | 2020000052 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals, Approve, Reject                                                                         | 7           |
      | Gehan.Ahmed        | 2020000053 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000051 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000056 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000055 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000054 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000012 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000011 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |
      | Gehan.Ahmed        | 2020000010 | ReadAll, ReadCompanyAndStoreData, ReadSalesOrderData, ReadItems, ReadApprovals                                                                                          | 5           |

      | CannotReadSections | 2020000005 | ReadAll, Approve, Delete                                                                                                                                                | 3           |
      | CannotReadSections | 2020000010 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000011 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000012 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000051 | ReadAll, SubmitForApproval, Cancel                                                                                                                                      | 3           |
      | CannotReadSections | 2020000052 | ReadAll, Approve, Reject                                                                                                                                                | 3           |
      | CannotReadSections | 2020000053 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000054 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000055 | ReadAll                                                                                                                                                                 | 1           |
      | CannotReadSections | 2020000056 | ReadAll                                                                                                                                                                 | 1           |

  # EBS-5411
  Scenario Outline: (02) Read allowed actions in SalesOrder Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesOrder with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User           | Code       |
      | Ahmed.Al-Ashry | 2020000009 |
      | Ahmed.Al-Ashry | 2020000003 |
      | Gehan.Ahmed    | 2020000003 |
      | Afaf           | 2020000005 |
      | Afaf           | 2020000010 |
      | Afaf           | 2020000011 |
      | Afaf           | 2020000012 |
      | Afaf           | 2020000052 |
      | Afaf           | 2020000053 |
      | Afaf           | 2020000051 |
      | Afaf           | 2020000056 |
      | Afaf           | 2020000055 |
      | Afaf           | 2020000054 |
      | Afaf           | 2020000003 |

  # EBS-5411
  Scenario: (03) Read allowed actions in SalesOrder Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to read actions of SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
