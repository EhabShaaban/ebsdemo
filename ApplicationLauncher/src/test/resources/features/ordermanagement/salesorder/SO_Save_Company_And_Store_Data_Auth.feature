# Author Dev: Order Team (01-Mar-2021)
# Author Quality: Shirin Mahmoud
Feature: Save CompanyAndStore section in SalesOrder - Auth

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                           |
      | Ahmed.Al-Ashry         | SalesSpecialist_Signmedia                      |
      | Manar.Mohammed.NoStore | SalesCoordinator_Signmedia_CannotViewStoreRole |
    And the following roles and sub-roles exist:
      | Role                                           | Sub-role                       |
      | SalesSpecialist_Signmedia                      | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                      | StorehouseViewer               |
      | SalesCoordinator_Signmedia_CannotViewStoreRole | SOOwner_Signmedia              |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                           | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | StorehouseViewer               | Storehouse:ReadAll                   |                                                                           |
      | SOOwner_Signmedia              | SalesOrder:UpdateCompanyAndStoreData | [purchaseUnitName='Signmedia']                                            |
    And the following users doesn't have the following permissions:
      | User                   | Permission         |
      | Manar.Mohammed.NoStore | Storehouse:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User           | Permission                           | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State | CreationDate         |DocumentOwner  |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store |
      | 2020000004 | 0001 - AL Madina |       |
      | 2020000005 | 0001 - AL Madina |       |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000004 - Client3  | 0003 - 100% Advanced Payment | 000004 - 7th st, El Sheikh Zayed     | EGP         | 000004 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following Plants exist for Company "0001 - AL Madina":
      | Plant                    |
      | 0001 - Madina Tech Plant |
    And the following Storehouses exist for Plant "0001 - Madina Tech Plant":
      | Storehouse                      |
      | 0001 - AlMadina Main Store      |
      | 0002 - AlMadina Secondary Store |


  #EBS-6520
  Scenario Outline: (01)  Save CompanyAndStore section by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves CompanyAndStore section of SalesOrder with Code "<Code>" at "07-Jan-2019 09:30 AM" with the following values:
      | Storehouse                 |
      | 0001 - AlMadina Main Store |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | Code       | User                   |
      | 2020000005 | Ahmed.Al-Ashry         |
      | 2020000004 | Manar.Mohammed.NoStore |

