# Author Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud

Feature: Sales Order - Save New Item Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | hr1            | SuperUser                  |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole           |
      | SuperUser                  | SuperUserSubRole  |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission             | Condition                      |
      | SuperUserSubRole  | *:*                    |                                |
      | SOOwner_Signmedia | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                          | OrderUnit           | Qty     | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5           | 0020 - 20Kg         | 10.000  | 2000.000   |
      | 2020000005 | 000014 - Insurance service                    | 0020 - 20Kg         | 2.000   | 50.000     |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |

    And the following Items exist with the below details:
      | Item                                          | Type       | UOM          | PurchaseUnit      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | COMMERCIAL | 0019 - M2    | 0002 - Signmedia  |
      | 000013 - Ink3                                 | COMMERCIAL | 0014 - Liter | 0006 - Corrugated |
    And the following UOMs exist for Item "000053 - Hot Laminated Frontlit Fabric roll 2":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And the following UOMs exist for Item "000013 - Ink3":
      | UOM          |
      | 0014 - Liter |
    And edit session is "30" minutes

  #  Acceptance Criteria:
  #  1- Items exist in store with:
  #  1-1. Required Quantity <= Avl Qty && Required Quantity > 0.
  #  1-2. Same Order Unit.
  #  1-3. Same Item Exists.
  #  1-4. Same Company.
  #  1-5. Business Unit.
  #  1-6. Same Storehouse.
  #  1-7. Item in store transaction must be UNRESTRICTED.

  # EBS-6701
  @Future
  Scenario: (01) Save Add SalesOrder Item,  (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:32 AM" with the following values:
      | Item                                          | OrderUnit | Qty     | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 316.000 | 10.000     |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-05"

  # EBS-6701
  @Future
  Scenario Outline: (02) Save Add SalesOrder Item, with incorrect data entry (Validation Failure)
    Given user is logged in as "hr1"
    And Items section of SalesOrder with Code "2020000005" is locked by "hr1" at "01-Jan-2019 11:00 AM"
    When "hr1" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:20 AM" with the following values:
      | Item               | OrderUnit           | Qty     | SalesPrice |
      | <SelectedItemCode> | <SelectedOrderUnit> | 100.000 | 10.000     |
    Then an error notification is sent to "hr1" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "hr1"
    Examples:
      | SelectedItemCode | SelectedOrderUnit | Field         | ErrMsg     |
      # Item does not exist
      | 999999           | 0019              | itemCode      | Gen-msg-48 |
      # OrderUnit does not exist
      | 000053           | 9999              | orderUnitCode | Gen-msg-48 |
      # existing order unit but does not belong to the selected item
      | 000053           | 0014              | orderUnitCode | Gen-msg-49 |

  # EBS-6701
  Scenario: (03) Save Add SalesOrder Item, after edit session expired (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:31 AM" with the following values:
      | Item                                          | OrderUnit | Qty     | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | 10.000     |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-07"

  # EBS-6701
  Scenario: (04) Save Add SalesOrder Item, after edit session expired & SalesOrder is deleted (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully at "01-Jan-2019 11:31 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:32 AM" with the following values:
      | Item                                          | OrderUnit | Qty     | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | 10.000     |
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-7208
  Scenario: (05) Save Add SalesOrder Item, when saving an existing item with the same UOM (BusinessRule) (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                                          | OrderUnit           | Qty     | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 10.000     |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page

  # EBS-6701
  Scenario Outline: (06) Save Add SalesOrder Item, with Missing Mandatories (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:32 AM" with the following values:
      | Item           | OrderUnit   | Qty   | SalesPrice   |
      | <ItemCodeName> | <OrderUnit> | <Qty> | <SalesPrice> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
    Examples:
      | ItemCodeName                                  | OrderUnit | Qty     | SalesPrice |
      |                                               | 0019 - M2 | 100.000 | 10.000     |
      | ""                                            | 0019 - M2 | 100.000 | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 |           | 100.000 | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | ""        | 100.000 | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | ""      | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 |            |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | ""         |

  # EBS-6701
  Scenario Outline: (07) Save Add SalesOrder Item, with malicious input  (Abuse Case)
    Given user is logged in as "Manar.Mohammed"
    And Items section of SalesOrder with Code "2020000005" is locked by "Manar.Mohammed" at "01-Jan-2019 11:00 AM"
    When "Manar.Mohammed" saves the following new item to Items section of SalesOrder with Code "2020000005" at "01-Jan-2019 11:32 AM" with the following values:
      | Item           | OrderUnit   | Qty   | SalesPrice   |
      | <ItemCodeName> | <OrderUnit> | <Qty> | <SalesPrice> |
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
    Examples:
      | ItemCodeName                                  | OrderUnit | Qty     | SalesPrice |
      | ay7aga                                        | 0019 - M2 | 100.000 | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | ay7aga    | 100.000 | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 0       | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | -2.4    | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | Ay 7aga | 10.000     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | -2.4       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | Ay 7aga    |
