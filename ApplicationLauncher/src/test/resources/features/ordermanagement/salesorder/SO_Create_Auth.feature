# Author Dev: Order Team (23-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Create SalesOrder - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                                                               | Role                                                                                              |
      | Ahmed.Al-Ashry                                                                     | SalesSpecialist_Signmedia                                                                         |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | SalesSpecialist_Signmedia_CannotReadSalesOrderTypeBusinessUnitCustomerSalesResponsibleCompanyRole |
      | Afaf                                                                               | FrontDesk                                                                                         |
    And the following roles and sub-roles exist:
      | Role                                                                                              | Subrole                        |
      | SalesSpecialist_Signmedia                                                                         | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                                                                         | PurUnitReader_Signmedia        |
      | SalesSpecialist_Signmedia                                                                         | CompanyViewer                  |
      | SalesSpecialist_Signmedia                                                                         | CustomerViewer_Signmedia       |
      | SalesSpecialist_Signmedia                                                                         | SalesOrderTypeViewer           |
      | SalesSpecialist_Signmedia                                                                         | SalesResponsibleViewer         |
      | SalesSpecialist_Signmedia_CannotReadSalesOrderTypeBusinessUnitCustomerSalesResponsibleCompanyRole | SOOwner_LoggedInUser_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission               | Condition                      |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:Create        |                                |
      | CompanyViewer                  | Company:ReadAll          |                                |
      | SalesOrderTypeViewer           | SalesOrderType:ReadAll   |                                |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll   | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia       | Customer:ReadAll         | [purchaseUnitName='Signmedia'] |
      | SalesResponsibleViewer         | SalesResponsible:ReadAll |                                |

    And the following users doesn't have the following permissions:
      | User                                                                               | Permission               |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | Company:ReadAll          |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | SalesOrderType:ReadAll   |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | PurchasingUnit:ReadAll   |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | Customer:ReadAll         |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | SalesResponsible:ReadAll |
      | Afaf                                                                               | SalesOrder:Create        |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000002 | Client2               | 0001 - Flexo     |
    And the following SalesResponsibles exist:
      | Code | Name           |
      | 0004 | Ahmed.Al-Ashry |
    And the following SalesOrderTypes exist:
      | Code              | Name  |
      | LOCAL_SALES_ORDER | Local |

  # EBS-6776
  Scenario: (01) Request Create SalesOrder, by an authorized user with authorized reads
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to create SalesOrder
    Then the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads      |
      | ReadSalesOrderType   |
      | ReadBusinessUnit     |
      | ReadCustomer         |
      | ReadSalesResponsible |
      | ReadCompany          |

  # EBS-6776
  Scenario: (02) Request Create SalesOrder, by an authorized user with missing authorized reads
    Given user is logged in as "Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany"
    When "Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany" requests to create SalesOrder
    Then there are no authorized reads returned to "Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany"

  # EBS-6776
  Scenario: (03) Request Create SalesOrder, by an unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create SalesOrder
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-6776
  Scenario Outline: (04) Create SalesOrder, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" creates SalesOrder with the following values:
      | Type              | BusinessUnit   | Company | Customer   | SalesResponsible |
      | LOCAL_SALES_ORDER | <BusinessUnit> | 0001    | <Customer> | 0004             |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                                                                               | BusinessUnit | Customer |
      | Ahmed.Al-Ashry.NoSalesOrderTypeNoBusinessUnitNoCustomerNoSalesResponsibleNoCompany | 0002         | 000007   |
      | Ahmed.Al-Ashry                                                                     | 0002         | 000002   |

    #  @Future
#  TODO: remove permission -> PurUnitReader_Signmedia -> PurchasingUnit:ReadAll -> no condition from the system
#      | Ahmed.Al-Ashry                                                                     | 0001         |
