# Author Dev: Waseem Salama
# Author Quality: Aya Sadek

Feature: View CompanyAndStoreData section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                         |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia              |
      | SalesSpecialist_Signmedia  | SOViewer_LoggedInUser_Signmedia |
      | SuperUser                  | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                         | Condition                                                                 |
      | SOViewer_Signmedia              | SalesOrder:ReadCompanyAndStoreData | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadCompanyAndStoreData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole                | *:*                                |                                                                           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                         | Condition                                                             |
      | Manar.Mohammed | SalesOrder:ReadCompanyAndStoreData | [purchaseUnitName='Flexo']                                            |
      | Ahmed.Al-Ashry | SalesOrder:ReadCompanyAndStoreData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy                 | State    | CreationDate         |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 01-Feb-2020 07:52 AM |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 04-Feb-2020 09:05 AM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed       | Admin from BDKCompanyCode | Draft    | 11-Feb-2020 01:28 PM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Approved | 04-Feb-2020 08:39 AM |

    And the following CompanyAndStoreData for the following SalesOrders exist:
      | SOCode     | Company          | Store                      |
      | 2020000001 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
      | 2020000005 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
      | 2020000009 | 0001 - AL Madina | 0001 - AlMadina Main Store |

  # EBS-5932
  Scenario: (01) View CompanyAndStoreData section by an authorizd user who has one role with one condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view CompanyAndStoreData section of SalesOrder with code "2020000005"
    Then the following values of CompanyAndStoreData section for SalesOrder with code "2020000005" are displayed to "Manar.Mohammed":
      | SOCode     | Company        | Store                     |
      | 2020000005 | 0002 - DigiPro | 0003 - DigiPro Main Store |

  # EBS-5932
  Scenario: (02) View CompanyAndStoreData section by an authorizd user who has one role with two conditions (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view CompanyAndStoreData section of SalesOrder with code "2020000005"
    Then the following values of CompanyAndStoreData section for SalesOrder with code "2020000005" are displayed to "Ahmed.Al-Ashry":
      | SOCode     | Company        | Store                     |
      | 2020000005 | 0002 - DigiPro | 0003 - DigiPro Main Store |

  # EBS-5932
  Scenario: (03) View CompanyAndStoreData section where SalesOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Manar.Mohammed" requests to view CompanyAndStoreData section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-5932
  Scenario Outline: (04) View CompanyAndStoreData section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view CompanyAndStoreData section of SalesOrder with code "<SOCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | SOCode     |
      | Manar.Mohammed | 2020000001 |
      | Ahmed.Al-Ashry | 2020000009 |
      | Ahmed.Al-Ashry | 2020000003 |
