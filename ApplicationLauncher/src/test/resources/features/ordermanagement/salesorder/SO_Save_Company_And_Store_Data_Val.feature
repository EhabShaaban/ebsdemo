# Author Dev: Order Team (01-Mar-2021)
  # Author Quality: Shirin Mahmoud
Feature: Save CompanyAndStore section in SalesOrder - Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role                       |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia | StorehouseViewer               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                           | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | StorehouseViewer               | Storehouse:ReadAll                   |                                                                           |

  #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft    | 11-Feb-2020 01:28 PM |Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Rejected | 11-Feb-2020 01:28 PM |Manar Mohammed |
  #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000004 | 0001 - AL Madina |                            |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
  #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000004 - 7th st, El Sheikh Zayed     | EGP         | 000004 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following Plants exist for Company "0001 - AL Madina":
      | Plant                    |
      | 0001 - Madina Tech Plant |
    And the following Storehouses exist for Plant "0001 - Madina Tech Plant":
      | Storehouse                      |
      | 0001 - AlMadina Main Store      |
      | 0002 - AlMadina Secondary Store |

    And edit session is "30" minutes

  # EBS-6508
  Scenario: (01) Save CompanyAndStore section after edit session expired (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CompanyAndStore section of SalesOrder with code "2020000004" is locked by "Ahmed.Al-Ashry" at "07-Jan-2020 09:10 AM"
    When "Ahmed.Al-Ashry" saves CompanyAndStore section of SalesOrder with Code "2020000004" at "07-Jan-2020 09:41 AM" with the following values:
      | Storehouse                 |
      | 0001 - AlMadina Main Store |
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"
    And CompanyAndStore section of SalesOrder with Code "2020000004" remains as follows:
      | Storehouse |
      |            |

  # EBS-6508
  Scenario: (02) Save CompanyAndStore section after edit session expired and SO doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And CompanyAndStore section of SalesOrder with code "2020000004" is locked by "Ahmed.Al-Ashry" at "07-Jan-2020 09:10 AM"
    And "hr1" first deleted the SalesOrder with code "2020000004" successfully at "07-Jan-2020 09:41 AM"
    When "Ahmed.Al-Ashry" saves CompanyAndStore section of SalesOrder with Code "2020000004" at "07-Jan-2020 09:42 AM" with the following values:
      | Storehouse                 |
      | 0001 - AlMadina Main Store |
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  # EBS-6508
  Scenario Outline: (03) Save CompanyAndStore section with missing Mandatory field in Rejected state (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CompanyAndStore section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2020 09:10 AM"
    When "Ahmed.Al-Ashry" saves CompanyAndStore section of SalesOrder with Code "2020000005" at "07-Jan-2020 09:15 AM" with the following values:
      | Storehouse   |
      | <Storehouse> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | Storehouse |
      |            |
      | ""         |
      | N/A        |

  # EBS-6508
  Scenario Outline: (04) Save CompanyAndStore section with malicious input  (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CompanyAndStore section of SalesOrder with code "2020000004" is locked by "Ahmed.Al-Ashry" at "07-Jan-2020 09:10 AM"
    When "Ahmed.Al-Ashry" saves CompanyAndStore section of SalesOrder with Code "2020000004" at "07-Jan-2020 09:15 AM" with the following values:
      | Storehouse   |
      | <Storehouse> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | Storehouse                            |
      | 00001 - AlMadina Main Store not valid |
      | 9999  -  Store does not exist         |
      | ay7aga                                |