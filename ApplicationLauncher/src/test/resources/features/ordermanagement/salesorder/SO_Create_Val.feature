# Author Dev: Fatma Al-Zahra
# Author Quality: Aya

Feature: Create SalesOrder - Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                        |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission        | Condition |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:Create |           |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000001 | Al Ahram              | 0001 - Flexo     |
    And the following SalesResponsibles exist:
      | Code | Name           |
      | 0004 | Ahmed.Al-Ashry |

  #EBS-5120
  Scenario Outline: (01) Create SalesOrder with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" creates SalesOrder with the following values:
      | Type           | BusinessUnit           | Company           | Customer           | SalesResponsible           |
      | <SelectedType> | <SelectedBusinessUnit> | <SelectedCompany> | <SelectedCustomer> | <SelectedSalesResponsible> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SelectedType      | SelectedBusinessUnit | SelectedCompany | SelectedCustomer | SelectedSalesResponsible |
      |                   | 0002                 | 0001            | 000007           | 0004                     |
      | ""                | 0002                 | 0001            | 000007           | 0004                     |
      | LOCAL_SALES_ORDER |                      | 0001            | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | ""                   | 0001            | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 |                 | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | ""              | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | 0001            |                  | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | 0001            | ""               | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | 0001            | 000007           |                          |
      | LOCAL_SALES_ORDER | 0002                 | 0001            | 000007           | ""                       |

  #EBS-5120
  Scenario Outline: (02) Create SalesOrder with incorrect data entry (Validation Failure)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" creates SalesOrder with the following values:
      | Type           | BusinessUnit           | Company           | Customer           | SalesResponsible           |
      | <SelectedType> | <SelectedBusinessUnit> | <SelectedCompany> | <SelectedCustomer> | <SelectedSalesResponsible> |
    Then a failure notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Ahmed.Al-Ashry"
    Examples:
      | SelectedType      | SelectedBusinessUnit | SelectedCompany | SelectedCustomer | SelectedSalesResponsible | Field                | ErrMsg     |
      # business unit does not exist
      | LOCAL_SALES_ORDER | 9999                 | 0001            | 000007           | 0004                     | businessUnitCode     | Gen-msg-48 |
      # company does not exist
      | LOCAL_SALES_ORDER | 0002                 | 9999            | 000007           | 0004                     | companyCode          | Gen-msg-48 |
      # customer does not exist
      | LOCAL_SALES_ORDER | 0002                 | 0001            | 999999           | 0004                     | customerCode         | Gen-msg-48 |
      # SalesResponsible does not exist
      | LOCAL_SALES_ORDER | 0002                 | 0001            | 000007           | 9999                     | salesResponsibleCode | Gen-msg-48 |
    # existing customer but does not belong to the selected business unit
      | LOCAL_SALES_ORDER | 0001                 | 0001            | 000007           | 0004                     | customerCode         | Gen-msg-49 |


  #EBS-5120
  Scenario Outline: (03) Create SalesOrder with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" creates SalesOrder with the following values:
      | Type           | BusinessUnit           | Company           | Customer           | SalesResponsible           |
      | <SelectedType> | <SelectedBusinessUnit> | <SelectedCompany> | <SelectedCustomer> | <SelectedSalesResponsible> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SelectedType      | SelectedBusinessUnit | SelectedCompany | SelectedCustomer | SelectedSalesResponsible |
      | LOCAL_SALES_ORDER | 0002                 | 0002            | 000007           | 00004                    |
      | LOCAL_SALES_ORDER | 0002                 | 0002            | 000007           | ay7aga                   |
      | LOCAL_SALES_ORDER | 0002                 | 0002            | 0000007          | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | 0002            | ay7aga           | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | 00002           | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | 0002                 | ay7aga          | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | 00002                | 0002            | 000007           | 0004                     |
      | LOCAL_SALES_ORDER | ay7aga               | 0002            | 000007           | 0004                     |
      | ay7aga            | 0002                 | 0002            | 000007           | 0004                     |