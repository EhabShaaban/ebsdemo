Feature:Home Screen Allowed Actions Sales Order

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Gehan.Ahmed    | SalesManager_Signmedia    |
      | Ahmed.Seif     | Quality_Specialist        |
      | CreateOnlyUser | CreateOnlyRole            |
      | Afaf           | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                         |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia  |
      | SalesSpecialist_Signmedia | SOViewer_LoggedInUser_Signmedia |
      | SalesManager_Signmedia    | SOViewer_Signmedia              |
      | Quality_Specialist        | SOViewer                        |
      | CreateOnlyRole            | CreateOnlySubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission         | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:Create  |                                                                           |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_Signmedia              | SalesOrder:ReadAll | [purchaseUnitName='Signmedia']                                            |
      | SOViewer                        | SalesOrder:ReadAll |                                                                           |
      | CreateOnlySubRole               | SalesOrder:Create  |                                                                           |

    And the following users doesn't have the following permissions:
      | User           | Permission         |
      | Gehan.Ahmed    | SalesOrder:Create  |
      | Ahmed.Seif     | SalesOrder:Create  |
      | Afaf           | SalesOrder:Create  |
      | Afaf           | SalesOrder:ReadAll |
      | CreateOnlyUser | SalesOrder:ReadAll |

  Scenario Outline: (01)  Read allowed actions in Sales Order Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesOrder home screen
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User           | AllowedActions |
      | Ahmed.Al-Ashry | ReadAll,Create |
      | Gehan.Ahmed    | ReadAll        |
      | Ahmed.Seif     | ReadAll        |
      | CreateOnlyUser | Create         |

  Scenario: (02) Read allowed actions in Sales Order Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of SalesOrder home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"