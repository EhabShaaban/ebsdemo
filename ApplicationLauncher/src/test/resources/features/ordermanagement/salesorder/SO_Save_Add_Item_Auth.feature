# Author Dev: Order Team (03-Mar-2021)
# Author Quality: Shirin Mahmoud
Feature: Sales Order - Save Add Item Auth

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                           |
      | Ahmed.Al-Ashry         | SalesSpecialist_Signmedia                      |
      | Manar.Mohammed.NoItems | SalesCoordinator_Signmedia_CannotViewItemsRole |
    And the following roles and sub-roles exist:
      | Role                                           | Sub-role                       |
      | SalesSpecialist_Signmedia                      | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                      | ItemViewer_Signmedia           |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | SOOwner_Signmedia              |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission             | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateItems | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | ItemViewer_Signmedia           | Item:ReadAll           | [purchaseUnitName='Signmedia']                                            |
      | ItemViewer_Signmedia           | Item:ReadUoMData       | [purchaseUnitName='Signmedia']                                            |
      | SOOwner_Signmedia              | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia']                                            |

    And the following users doesn't have the following permissions:
      | User                   | Permission       |
      | Manar.Mohammed.NoItems | Item:ReadAll     |
      | Manar.Mohammed.NoItems | Item:ReadUoMData |

    And the following users have the following permissions without the following conditions:
      | User           | Permission           | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:ReadItems | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State | CreationDate         |DocumentOwner  |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store |
      | 2020000004 | 0001 - AL Madina |       |
      | 2020000005 | 0001 - AL Madina |       |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000004 - Client3  | 0003 - 100% Advanced Payment | 000004 - 7th st, El Sheikh Zayed     | EGP         | 000004 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

    And the following Items exist with the below details:
      | Item                                          | Type       | UOM       | PurchaseUnit     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000053 - Hot Laminated Frontlit Fabric roll 2":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |


  #EBS-6521
  Scenario Outline: (01) Save Add SalesOrder Item, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves the following new item to Items section of SalesOrder with Code "<Code>" at "03-Mar-2021 11:10 AM" with the following values:
      | Item                                          | OrderUnit | Qty     | SalesPrice |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 100.000 | 10.000     |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                   | Code       |
      | Ahmed.Al-Ashry         | 2020000005 |
      | Manar.Mohammed.NoItems | 2020000004 |
