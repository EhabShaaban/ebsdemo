# Author Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud

Feature: Delete Item From Items section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                         |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia               |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia              |
      | SalesSpecialist_Signmedia  | SOOwner_LoggedInUser_Signmedia  |
      | SalesSpecialist_Signmedia  | SOViewer_LoggedInUser_Signmedia |
      | SuperUser                  | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission             | Condition                                                                 |
      | SOOwner_Signmedia               | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_Signmedia              | SalesOrder:ReadItems   | [purchaseUnitName='Signmedia']                                            |
      | SOOwner_LoggedInUser_Signmedia  | SalesOrder:UpdateItems | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadItems   | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole                | *:*                    |                                                                           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission             | Condition                                                           |
      | Manar.Mohammed | SalesOrder:UpdateItems | [purchaseUnitName='Flexo']                                          |
      | Manar.Mohammed | SalesOrder:ReadItems   | [purchaseUnitName='Flexo']                                          |
      | Ahmed.Al-Ashry | SalesOrder:UpdateItems | [purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser'] |
      | Ahmed.Al-Ashry | SalesOrder:ReadItems   | [purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser'] |

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00010002000200030000510014 | 0001 - Flexo     | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 3.0               |
      | UNRESTRICTED_USE00010001000200030000510014 | 0001 - Flexo     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 3.0               |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | UNRESTRICTED_USE | 110.0             |
      | UNRESTRICTED_USE00020002000200030000530019 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | UNRESTRICTED_USE | 2.0               |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 19.0              |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | UNRESTRICTED_USE | 2.0               |
      | UNRESTRICTED_USE00020002000200030000530035 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 19.0              |

    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible           | CreatedBy                 | State                 | CreationDate         |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | ReadyForDelivery      | 03-Aug-2020 02:22 PM |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | GoodsIssueActivated   | 03-Aug-2020 01:36 PM |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | SalesInvoiceActivated | 03-Aug-2020 01:31 PM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed            | Admin from BDKCompanyCode | Draft                 | 11-Feb-2020 01:28 PM |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Canceled              | 04-Feb-2020 12:38 PM |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 04-Feb-2020 09:05 AM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Approved              | 04-Feb-2020 08:39 AM |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 01-Feb-2020 07:52 AM |

      | 2020000002 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | WaitingApproval       | 04-Feb-2020 08:37 AM |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Expired               | 04-Feb-2020 12:39 PM |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | DeliveryComplete      | 04-Feb-2020 12:40 PM |
    And the following SalesOrders have the following Items:
      | SOCode     | ItemId | ItemType                | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000012 | 65     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 10.000  | 40.000     | 400.000     | 25             | 110.000      |
      | 2020000011 | 64     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 10.000  | 33.000     | 330.000     | 25             | 110.000      |
      | 2020000010 | 63     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 10.000  | 25.000     | 250.000     | 25             | 110.000      |

      | 2020000009 | 61     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 1.000   | 3.000      | 3.000       | 200.000        | 2.000        |
      | 2020000006 | 59     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000005 | 58     | CONSUMABLE - Consumable | 000015 - Laptop HP Pro-book core i5           | 0019 - Square Meter | 10.000  | 2000.000   | 20000.000   |                |              |
      | 2020000005 | 57     | SERVICE - Service       | 000014 - Insurance service                    | 0019 - Square Meter | 2.000   | 50.000     | 100.000     |                |              |
      | 2020000005 | 56     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      | 19.000       |
      | 2020000003 | 53     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000001 | 51     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |

      | 2020000002 | 52     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000007 | 60     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |
      | 2020000008 | 62     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000   | 10.000     | 20.000      | 130.000        | 3.000        |

  # EBS-5130
  Scenario: (01) Delete Item from SalesOrder, where SalesOrder is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete Item with id "57" in SalesOrder with code "2020000005"
    Then Item with id "57" from SalesOrder with code "2020000005" is deleted
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-12"

  # EBS-5130
  Scenario: (02) Delete Item from SalesOrder, where Item doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted Item with id "57" of SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to delete Item with id "57" in SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-20"

  # EBS-5130
  Scenario Outline: (03) Delete Item from SalesOrder, where action is not allowed in current state: WaitingApproval, Approved, Expired, Cancelled, DeliveryComplete (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to delete Item with id "<ItemId>" in SalesOrder with code "<SalesOrderCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-21"
    Examples:
      | SalesOrderCode | ItemId |
      | 2020000002     | 52     |
      | 2020000003     | 53     |
      | 2020000006     | 59     |
      | 2020000007     | 60     |
      | 2020000008     | 62     |

  # EBS-5130
  Scenario: (04) Delete Item from SalesOrder, where SalesOrder doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to delete Item with id "57" in SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  # EBS-5130
  Scenario: (05) Delete Item from SalesOrder when SalesOrderItems section are locked by another user (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And first "hr1" opens SalesOrderItems section of SalesOrder with code "2020000005" in edit mode
    When "Ahmed.Al-Ashry" requests to delete Item with id "57" in SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-02"

  # EBS-5130
  Scenario Outline: (06) Delete Item from SalesOrder by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with id "<ItemId>" in SalesOrder with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | Code       | ItemId |
      | Ahmed.Al-Ashry | 2020000009 | 61     |
      | Ahmed.Al-Ashry | 2020000001 | 51     |
      | Manar.Mohammed | 2020000001 | 51     |
