 # Author: Eman Mansour , Shirin Mahmoud

 Feature: Save CompanyAndStore section in SalesOrder-Happy Path

   Background:
     Given the following users and roles exist:
       | Name           | Role                      |
       | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
     And the following roles and sub-roles exist:
       | Role                      | Sub-role                       |
       | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
       | SalesSpecialist_Signmedia | StorehouseViewer               |
     And the following sub-roles and permissions exist:
       | Subrole                        | Permission                           | Condition                                                                 |
       | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateCompanyAndStoreData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
       | StorehouseViewer               | Storehouse:ReadAll                   |                                                                           |

     #@INSERT
     And the following GeneralData for SalesOrders exist:
       | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         | DocumentOwner  |
       | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft    | 11-Feb-2019 01:28 PM | Manar Mohammed |
       | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Rejected | 11-Feb-2019 01:28 PM | Manar Mohammed |
    #@INSERT
     And the following Company for SalesOrders exist:
       | Code       | Company          | Store                      |
       | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |
       | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
     And the following SalesOrderData for SalesOrders exist:
       | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
       | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
       | 2020000005 | 000004 - Client3  | 0003 - 100% Advanced Payment | 000004 - 7th st, El Sheikh Zayed     | EGP         | 000004 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

     And the following Companies exist:
       | Code | Name      |
       | 0001 | AL Madina |
     And the following Plants exist for Company "0001 - AL Madina":
       | Plant                    |
       | 0001 - Madina Tech Plant |
     And the following Storehouses exist for Plant "0001 - Madina Tech Plant":
       | Storehouse                      |
       | 0001 - AlMadina Main Store      |
       | 0002 - AlMadina Secondary Store |

     And edit session is "30" minutes

  #EBS-6504
   Scenario Outline: (01) Save CompanyAndStore section within edit session by an authorized user for Draft/Rejected SalesOrders (Happy Path)
     Given user is logged in as "<User>"
     And CompanyAndStore section of SalesOrder with code "<SalesOrderCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
     When "<User>" saves CompanyAndStore section of SalesOrder with Code "<SalesOrderCode>" at "07-Jan-2019 09:30 AM" with the following values:
       | Storehouse   |
       | <Storehouse> |
     Then SalesOrder with Code "<SalesOrderCode>" is updated as follows:
       | LastUpdatedBy | LastUpdateDate       |
       | <User>        | 07-Jan-2019 09:30 AM |
     And CompanyAndStore section of SalesOrder with code "<SalesOrderCode>" is updated as follows:
       | Storehouse   |
       | <Storehouse> |
     And the lock by "<User>" on CompanyAndStore section of SalesOrder with code "<SalesOrderCode>" is released
     And a success notification is sent to "<User>" with the following message "Gen-msg-04"
     Examples:
       | User           | SalesOrderCode | Storehouse                      |
      # Draft Sales Order
       | Ahmed.Al-Ashry | 2020000004     | 0001 - AlMadina Main Store      |
       | Ahmed.Al-Ashry | 2020000004     |                                 |
      # Rejected Sales Order
       | Ahmed.Al-Ashry | 2020000005     | 0002 - AlMadina Secondary Store |
