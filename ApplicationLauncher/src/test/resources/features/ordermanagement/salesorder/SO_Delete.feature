# Author Dev: Zyad Ghorab,  Author Quality: Shirin Mahmoud, Reviewer: Somaya Ahmed

Feature: Delete Sales Order

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                        |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia              |
      | SalesSpecialist_Signmedia  | SOOwner_LoggedInUser_Signmedia |
      | SuperUser                  | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission        | Condition                                                                 |
      | SOOwner_Signmedia              | SalesOrder:Delete | [purchaseUnitName='Signmedia']                                            |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:Delete | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole               | *:*               |                                                                           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission        | Condition                                                           |
      | Manar.Mohammed | SalesOrder:Delete | [purchaseUnitName='Flexo']                                          |
      | Ahmed.Al-Ashry | SalesOrder:Delete | [purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser'] |
    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible           | CreatedBy                 | State                 | CreationDate         |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | ReadyForDelivery      | 03-Aug-2020 02:22 PM |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | GoodsIssueActivated   | 03-Aug-2020 01:36 PM |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | SalesInvoiceActivated | 03-Aug-2020 01:31 PM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed            | Admin from BDKCompanyCode | Draft                 | 11-Feb-2020 01:28 PM |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Canceled              | 04-Feb-2020 12:38 PM |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 04-Feb-2020 09:05 AM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Approved              | 04-Feb-2020 08:39 AM |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Draft                 | 01-Feb-2020 07:52 AM |

      | 2020000004 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Rejected              | 04-Feb-2020 08:42 AM |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | WaitingApproval       | 04-Feb-2020 08:37 AM |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | Expired               | 04-Feb-2020 12:39 PM |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | DeliveryComplete      | 04-Feb-2020 12:40 PM |

  # EBS-5410
  Scenario: (01) Delete SalesOrder, where SalesOrder is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete SalesOrder with code "2020000005"
    Then SalesOrder with code "2020000005" is deleted
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-12"

  # EBS-5410
  Scenario: (02) Delete SalesOrder, where sales order doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to delete SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  # EBS-5410
  Scenario Outline: (03) Delete SalesOrder, where action is not allowed in current state: WaitingApproval, Approved, Rejected, Cancelled, Expired, GoodsIssueActivated, SalesInvoiceActivated, ReadyForDelivery, DeliveryComplete (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to delete SalesOrder with code "<SalesOrderCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-13"
    Examples:
      | SalesOrderCode |
      | 2020000002     |
      | 2020000003     |
      | 2020000004     |
      | 2020000006     |
      | 2020000007     |
      | 2020000008     |
      | 2020000010     |
      | 2020000011     |
      | 2020000012     |

  # EBS-5410
  Scenario Outline: (04) Delete SalesOrder, where sales order is locked by another user (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And first "hr1" opens "<SOSection>" section of SalesOrder with code "2020000005" in edit mode
    When "Ahmed.Al-Ashry" requests to delete SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-14"
    Examples:
      | SOSection           |
      | CompanyAndStoreData |
      | SalesOrderData      |
      | Items               |

  # EBS-5410
  Scenario Outline: (05) Delete SalesOrder, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete SalesOrder with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | Code       |
      | Ahmed.Al-Ashry | 2020000009 |
      | Ahmed.Al-Ashry | 2020000001 |
      | Manar.Mohammed | 2020000001 |
