# Author: Waseem Salama , Eman Mansour
# Tester: Shirin Mahmoud

Feature: Read Item's UOMs Dropdown

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | hr1            | SuperUser                  |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
    And the following roles and sub-roles exist:
      | Role                       | Subrole              |
      | SuperUser                  | SuperUserSubRole     |
      | SalesCoordinator_Signmedia | ItemViewer_Signmedia |
      | SalesCoordinator_Signmedia | SOOwner_Signmedia    |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition                      |
      | SuperUserSubRole     | *:*                    |                                |
      | ItemViewer_Signmedia | Item:ReadUoMData       | [purchaseUnitName='Signmedia'] |
      | SOOwner_Signmedia    | SalesOrder:UpdateItems | [purchaseUnitName='Signmedia'] |
    And the following Items exist with the below details:
      | Item                                          | Type       | UOM       | PurchaseUnit     |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000015 - Laptop HP Pro-book core i5           | CONSUMABLE | 0019 - M2 | 0002 - Signmedia |
      | 000014 - Insurance service                    | SERVICE    | 0019 - M2 | 0002 - Signmedia |
      | 000051 - Ink5                                 | COMMERCIAL | 0019 - M2 | 0002 - Flexo     |
    And the following users have the following permissions without the following conditions:
      | User           | Permission             | Condition                  |
      | Manar.Mohammed | Item:ReadUoMData       | [purchaseUnitName='Flexo'] |
      | Manar.Mohammed | SalesOrder:UpdateItems | [purchaseUnitName='Flexo'] |
    And the following UOMs exist for Item "000053 - Hot Laminated Frontlit Fabric roll 2":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And the following UOMs exist for Item "000015 - Laptop HP Pro-book core i5":
      | UOM       |
      | 0019 - M2 |
    And the following UOMs exist for Item "000014 - Insurance service":
      | UOM       |
      | 0019 - M2 |
    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM       | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | UNRESTRICTED_USE | 2.0               |

    And the following SalesOrders with the following Company And Store exist:
      | SOCode     | Company          | Store                      | BusinessUnit     | Customer                       | State            |
      | 2020000059 | 0001 - AL Madina |                            | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | Draft            |
      | 2020000058 | 0001 - AL Madina |                            | 0002 - Signmedia | 000007 - مطبعة أكتوبر الهندسية | Draft            |
      | 2020000009 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia | 000001 - Al Ahram              | Draft            |
      | 2020000008 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     | 000006 - المطبعة الأمنية       | DeliveryComplete |
      | 2020000007 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     | 000006 - المطبعة الأمنية       | Expired          |
      | 2020000006 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     | 000006 - المطبعة الأمنية       | Canceled         |
      | 2020000003 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     | 000006 - المطبعة الأمنية       | Approved         |
      | 2020000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0001 - Flexo     | 000006 - المطبعة الأمنية       | WaitingApproval  |
      | 2020000001 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     | 000006 - المطبعة الأمنية       | Draft            |
    And the following SalesOrders have the following Items:
      | SOCode     | ItemId | ItemType                | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000059 | 73     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      |              |
      | 2020000058 | 72     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      |              |
      | 2020000009 | 61     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 1.000   | 3.000      | 3.000       | 200.000        | 2.000        |

  Scenario: (01) Read Item's UOMs dropdown , last sales price and Available Quantity by authorized user (Commercial Item)(filtered)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "000053" of SalesOrder with code "2020000009" in a dropdown
    Then the following UOM values will be presented to "Manar.Mohammed" in the dropdown:
      | UOM                 | LastSalesPrice | AvailableQty |
      | 0035 - Roll 1.27x50 | 952.5          | 0.000        |
    And total number of records returned to "Manar.Mohammed" is "1"

  Scenario: (02) Read Item's UOMs dropdown , last sales price and Available Quantity by authorized user (No item)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "999999" of SalesOrder with code "2020000009" in a dropdown
    Then total number of records returned to "Manar.Mohammed" is "0"

  Scenario: (03) Read Item's UOMs dropdown , last sales price and Available Quantity by authorized user (Commercial Item)(Company & No Store)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "000053" of SalesOrder with code "2020000059" in a dropdown
    Then the following UOM values will be presented to "Manar.Mohammed" in the dropdown:
      | UOM       | LastSalesPrice | AvailableQty |
      | 0019 - M2 | 25             |              |
    Then total number of records returned to "Manar.Mohammed" is "1"

  Scenario: (04) Read Item's UOMs dropdown , last sales price and Available Quantity by authorized user (Commercial Item)(No Company & No Store)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "000053" of SalesOrder with code "2020000058" in a dropdown
    Then the following UOM values will be presented to "Manar.Mohammed" in the dropdown:
      | UOM       | LastSalesPrice | AvailableQty |
      | 0019 - M2 | 25             |              |
    Then total number of records returned to "Manar.Mohammed" is "1"

  Scenario Outline: (05) Read Item's UOMs dropdown , last sales price and Available Quantity by authorized user (Service & Consumable Items)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "<ItemCode>" of SalesOrder with code "2020000009" in a dropdown
    Then the following UOM values will be presented to "Manar.Mohammed" in the dropdown:
      | UOM   | LastSalesPrice   | AvailableQty   |
      | <UOM> | <LastSalesPrice> | <AvailableQty> |
    Then total number of records returned to "Manar.Mohammed" is "<TotalNumber>"
    Examples:
      | ItemCode | UOM       | LastSalesPrice | AvailableQty | TotalNumber |
      | 000015   | 0019 - M2 |                |              | 1           |
      | 000014   | 0019 - M2 |                |              | 1           |

  Scenario Outline: (06) Read Item's UOMs dropdown , last sales price and Available Quantity when it is not allowed in current state: WaitingApproval, Approved, Expired, Cancelled, DeliveryComplete (Abuse case)
    Given user is logged in as "hr1"
    When "hr1" requests to read UOMs for Item with code "000051" of SalesOrder with code "<SalesOrderCode>" in a dropdown
    Then "hr1" is logged out
    And "hr1" is forwarded to the error page
    Examples:
      | SalesOrderCode |
      | 2020000002     |
      | 2020000003     |
      | 2020000006     |
      | 2020000007     |
      | 2020000008     |

  Scenario Outline: (07) Read Item's UOMs dropdown , last sales price and Available Quantity by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read UOMs for Item with code "<ItemCode>" of SalesOrder with code "<SalesOrderCode>" in a dropdown
    Then "Manar.Mohammed" is logged out
    And "Manar.Mohammed" is forwarded to the error page
    Examples:
      | SalesOrderCode | ItemCode |
      #salesOrder with different BU
      | 2020000001     | 000051   |
      #Item with different BU
      | 2020000009     | 000005   |
      #salesOrder Not Exist
      | 2020999999     | 000051   |
