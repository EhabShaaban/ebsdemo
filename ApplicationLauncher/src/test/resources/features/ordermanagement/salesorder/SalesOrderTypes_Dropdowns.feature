Feature: View All SalesOrderTypes

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole              |
      | SalesSpecialist_Signmedia | SalesOrderTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition |
      | SalesOrderTypeViewer | SalesOrderType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | SalesOrderType:ReadAll |
    And the following SalesOrderTypes exist:
      | Code              | Name  |
      | LOCAL_SALES_ORDER | Local |
    And the total number of existing SalesOrderTypes is 1

  Scenario: (01) Read list of SalesOrderTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all SalesOrderTypes
    Then the following SalesOrderTypes values will be presented to "Ahmed.Al-Ashry":
      | SalesOrderType            |
      | LOCAL_SALES_ORDER - Local |
    And total number of SalesOrderTypes returned to "Ahmed.Al-Ashry" is equal to 1

  Scenario: (02) Read list of SalesOrderTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesOrderTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page