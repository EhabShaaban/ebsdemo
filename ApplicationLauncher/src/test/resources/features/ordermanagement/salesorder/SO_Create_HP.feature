Feature: Create SalesOrder - Happy Path

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                        |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia | CompanyViewer                  |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission        | Condition |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:Create |           |
      | CompanyViewer                  | Company:ReadAll   |           |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
    And the following SalesResponsibles exist:
      | Code | Name           |
      | 0004 | Ahmed.Al-Ashry |
    And the Company "0001 - AL Madina" has the following Taxes:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |

  # EBS-6192
  Scenario: (01) Create SalesOrder with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created SalesOrder was with code "2020000050"
    When "Ahmed.Al-Ashry" creates SalesOrder with the following values:
      | Type              | BusinessUnit | Company | Customer | SalesResponsible |
      | LOCAL_SALES_ORDER | 0002         | 0001    | 000007   | 0004             |
    Then a new SalesOrder is created as follows:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type              | BusinessUnit     | SalesResponsible      |
      | 2020000051 | Draft | Ahmed.Al-Ashry | Ahmed.Al-Ashry | 01-Jan-2020 11:00 AM | 01-Jan-2020 11:00 AM | LOCAL_SALES_ORDER | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry |
    And the CompanyAndStore Section of SalesOrder with code "2020000051" is updated as follows:
      | Company          |
      | 0001 - AL Madina |
    And the SalesOrderData Section of SalesOrder with code "2020000051" is updated as follows:
      | Customer                       |
      | 000007 - مطبعة أكتوبر الهندسية |
    And the following values for Taxes for SalesOrder with code "2020000051" is updated as follows:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-11"