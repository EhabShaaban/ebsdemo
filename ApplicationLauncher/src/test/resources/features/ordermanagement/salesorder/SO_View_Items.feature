# Author Dev: Fatma Al-Zahra & Waseem Salama
# Author Quality: Shirin Mahmoud

Feature: View Items section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                         |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia              |
      | SalesSpecialist_Signmedia  | SOViewer_LoggedInUser_Signmedia |
      | SuperUser                  | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission           | Condition                                                                 |
      | SOViewer_Signmedia              | SalesOrder:ReadItems | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadItems | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole                | *:*                  |                                                                           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission           | Condition                                                           |
      | Manar.Mohammed | SalesOrder:ReadItems | [purchaseUnitName='Flexo']                                          |
      | Ahmed.Al-Ashry | SalesOrder:ReadItems | [purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser'] |


    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00010002000200030000510014 | 0001 - Flexo     | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 3.0               |
      | UNRESTRICTED_USE00010001000100010000510014 | 0001 - Flexo     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 3.0               |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 19.0              |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | UNRESTRICTED_USE | 2.0               |

    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy                 | State    | CreationDate         |
      | 2020000062 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 22-Mar-2020 09:24 AM |
      | 2020000061 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 22-Mar-2020 09:07 AM |
      | 2020000060 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 19-Mar-2020 08:13 AM |
      | 2020000059 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 19-Mar-2020 07:47 AM |
      | 2020000058 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 19-Mar-2020 07:47 AM |
      | 2020000057 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 19-Mar-2020 07:46 AM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed       | Admin from BDKCompanyCode | Draft    | 11-Feb-2020 01:28 PM |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar          | Admin from BDKCompanyCode | Canceled | 04-Feb-2020 12:38 PM |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 04-Feb-2020 09:05 AM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Approved | 04-Feb-2020 08:39 AM |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 01-Feb-2020 07:52 AM |
    And the following CompanyAndStoreData for the following SalesOrders exist:
      | SOCode     | Company          | Store                           |
      | 2020000062 | 0001 - AL Madina | 0001 - AlMadina Main Store      |
      | 2020000061 | 0001 - AL Madina | 0002 - AlMadina Secondary Store |
      | 2020000060 | 0001 - AL Madina |                                 |
      | 2020000059 | 0001 - AL Madina |                                 |
      | 2020000057 | 0001 - AL Madina |                                 |
      | 2020000058 | 0001 - AL Madina |                                 |
      | 2020000009 | 0001 - AL Madina | 0001 - AlMadina Main Store      |
      | 2020000006 | 0002 - DigiPro   | 0003 - DigiPro Main Store       |
      | 2020000005 | 0002 - DigiPro   | 0003 - DigiPro Main Store       |
      | 2020000003 | 0002 - DigiPro   | 0003 - DigiPro Main Store       |
      | 2020000001 | 0002 - DigiPro   | 0003 - DigiPro Main Store       |
    And the following SalesOrders exist with the following Customers:
      | SOCode     | Customer                       |
      | 2020000062 | 000006 - المطبعة الأمنية       |
      | 2020000061 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000060 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000059 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000058 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000057 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000009 | 000001 - Al Ahram              |
      | 2020000006 | 000006 - المطبعة الأمنية       |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000003 | 000006 - المطبعة الأمنية       |
      | 2020000001 | 000006 - المطبعة الأمنية       |
    And the following SalesOrders have the following Items:
      | SOCode     | ItemId | ItemType                | Item                                          | OrderUnit           | Qty           | SalesPrice  | TotalAmount            | LastSalesPrice | AvailableQty |
      | 2020000062 | 76     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000         | 4.000       | 8.000                  | 130.000        | 3.000        |
      | 2020000061 | 75     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.123456789 | 4.123456789 | 412.854747634750190521 | 12700.000      |              |
      | 2020000059 | 73     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000       | 4.000       | 400.000                | 12700.000      |              |
      | 2020000058 | 72     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000       | 4.000       | 400.000                | 12700.000      |              |
      | 2020000009 | 61     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 1.000         | 3.000       | 3.000                  | 200.000        | 2.000        |
      | 2020000006 | 59     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000         | 10.000      | 20.000                 | 130.000        | 3.000        |
      | 2020000005 | 58     | CONSUMABLE - Consumable | 000015 - Laptop HP Pro-book core i5           | 0019 - Square Meter | 10.000        | 2000.000    | 20000.000              |                |              |
      | 2020000005 | 57     | SERVICE - Service       | 000014 - Insurance service                    | 0019 - Square Meter | 2.000         | 50.000      | 100.000                |                |              |
      | 2020000005 | 56     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000       | 4.000       | 400.000                | 12700.000      | 19.000       |
      | 2020000003 | 53     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000         | 10.000      | 20.000                 | 130.000        | 3.000        |
      | 2020000001 | 51     | COMMERCIAL - Commercial | 000051 - Ink5                                 | 0014 - Liter        | 2.000         | 10.000      | 20.000                 | 130.000        | 3.000        |

    And the following SalesOrders have no Items:
      | SOCode     |
      | 2020000060 |
      | 2020000057 |
    And the following SalesOrders have the following TaxDetails:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000062 | 0007 - Vat                                   | 1             |
      | 2020000062 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000062 | 0009 - Real estate taxes                     | 10            |
      | 2020000062 | 0010 - Service tax                           | 12            |
      | 2020000062 | 0011 - Adding tax                            | 0.5           |
      | 2020000062 | 0012 - Sales tax                             | 12            |
      | 2020000061 | 0007 - Vat                                   | 1             |
      | 2020000061 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000061 | 0009 - Real estate taxes                     | 10.123456789  |
      | 2020000061 | 0010 - Service tax                           | 12            |
      | 2020000061 | 0011 - Adding tax                            | 0.5           |
      | 2020000061 | 0012 - Sales tax                             | 12            |
      | 2020000059 | 0007 - Vat                                   | 1             |
      | 2020000059 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000059 | 0009 - Real estate taxes                     | 10            |
      | 2020000059 | 0010 - Service tax                           | 12            |
      | 2020000059 | 0011 - Adding tax                            | 0.5           |
      | 2020000059 | 0012 - Sales tax                             | 12            |
      | 2020000009 | 0001 - Vat                                   | 1             |
      | 2020000009 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000009 | 0003 - Real estate taxes                     | 10            |
      | 2020000006 | 0001 - Vat                                   | 1             |
      | 2020000006 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000006 | 0003 - Real estate taxes                     | 10            |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
      | 2020000003 | 0001 - Vat                                   | 1             |
      | 2020000003 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0003 - Real estate taxes                     | 10            |
      | 2020000001 | 0001 - Vat                                   | 1             |
      | 2020000001 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000001 | 0003 - Real estate taxes                     | 10            |

  # EBS-5128
  Scenario: (01) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - Item & Company & Store & No Last SalesPrice (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to view Items section of SalesOrder with code "2020000062"
    Then the following values of Items section for SalesOrder with code "2020000062" are displayed to "hr1":
      | SOCode     | Item          | OrderUnit    | Qty   | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000062 | 000051 - Ink5 | 0014 - Liter | 2.000 | 4.000      | 8.000       | 130.000        | 3.000        |
    And the following values for TaxesSummary for SalesOrder with code "2020000062" are displayed to "hr1":
      | Tax                                          | TaxAmount |
      | 0007 - Vat                                   | 0.080     |
      | 0008 - Commercial and industrial profits tax | 0.040     |
      | 0009 - Real estate taxes                     | 0.800     |
      | 0010 - Service tax                           | 0.960     |
      | 0011 - Adding tax                            | 0.040     |
      | 0012 - Sales tax                             | 0.960     |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000062" are displayed to "hr1":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 8.000                  | 2.880      | 10.880                |

  # EBS-5128
  Scenario: (02) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - Item & Company & Store & No Available Qty (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000061"
    Then the following values of Items section for SalesOrder with code "2020000061" are displayed to "Manar.Mohammed":
      | SOCode     | Item                                          | OrderUnit           | Qty           | SalesPrice  | TotalAmount            | LastSalesPrice | AvailableQty |
      | 2020000061 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.123456789 | 4.123456789 | 412.854747634750190521 | 12700.000      | 0.000        |
    And the following values for TaxesSummary for SalesOrder with code "2020000061" are displayed to "Manar.Mohammed":
      | Tax                                          | TaxAmount                        |
      | 0007 - Vat                                   | 4.12854747634750190521           |
      | 0008 - Commercial and industrial profits tax | 2.064273738173750952605          |
      | 0009 - Real estate taxes                     | 41.79517197813893508548860897069 |
      | 0010 - Service tax                           | 49.54256971617002286252          |
      | 0011 - Adding tax                            | 2.064273738173750952605          |
      | 0012 - Sales tax                             | 49.54256971617002286252          |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000061" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes                        | TotalAmountAfterTaxes             |
      | 412.854747634750190521 | 149.13740636317398462094860897069 | 561.99215399792417514194860897069 |

  # EBS-5128
  Scenario: (03) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - Item & Company & Store (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000005"
    Then the following values of Items section for SalesOrder with code "2020000005" are displayed to "Manar.Mohammed":
      | SOCode     | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5           | 0019 - Square Meter | 10.000  | 2000.000   | 20000.000   |                |              |
      | 2020000005 | 000014 - Insurance service                    | 0019 - Square Meter | 2.000   | 50.000     | 100.000     |                |              |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      | 19.000       |
    And the following values for TaxesSummary for SalesOrder with code "2020000005" are displayed to "Manar.Mohammed":
      | Tax                                          | TaxAmount |
      | 0007 - Vat                                   | 205.000   |
      | 0008 - Commercial and industrial profits tax | 102.500   |
      | 0009 - Real estate taxes                     | 2050.000  |
      | 0010 - Service tax                           | 2460.000  |
      | 0011 - Adding tax                            | 102.500   |
      | 0012 - Sales tax                             | 2460.000  |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000005" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 20500.000              | 7380.000   | 27880.000             |

  # EBS-5128
  Scenario: (04) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - Item & Company & No Store (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000059"
    Then the following values of Items section for SalesOrder with code "2020000059" are displayed to "Manar.Mohammed":
      | SOCode     | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000059 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      |              |
    And the following values for TaxesSummary for SalesOrder with code "2020000059" are displayed to "Manar.Mohammed":
      | Tax                                          | TaxAmount |
      | 0007 - Vat                                   | 4.000     |
      | 0008 - Commercial and industrial profits tax | 2.000     |
      | 0009 - Real estate taxes                     | 40.000    |
      | 0010 - Service tax                           | 48.000    |
      | 0011 - Adding tax                            | 2.000     |
      | 0012 - Sales tax                             | 48.000    |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000059" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 400.000                | 144.000    | 544.000               |

  # EBS-5128
  Scenario: (05) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - Item & No Company & No Store (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000058"
    Then the following values of Items section for SalesOrder with code "2020000058" are displayed to "Manar.Mohammed":
      | SOCode     | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000058 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      |              |
    And TaxesSummary for SalesOrder with code "2020000058" is displayed empty to "Manar.Mohammed"
    And the following values for SalesOrderSummary for SalesOrder with code "2020000058" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 400.000                |            |                       |

  # EBS-5128
  Scenario: (06) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - No Item & Company & No Store (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000060"
    Then Items section for SalesOrder with code "2020000060" is displayed empty to "Manar.Mohammed"
    And the following values for TaxesSummary for SalesOrder with code "2020000060" are displayed to "Manar.Mohammed":
      | Tax                                          | TaxAmount |
      | 0007 - Vat                                   |           |
      | 0008 - Commercial and industrial profits tax |           |
      | 0009 - Real estate taxes                     |           |
      | 0010 - Service tax                           |           |
      | 0011 - Adding tax                            |           |
      | 0012 - Sales tax                             |           |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000060" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      |                        |            |                       |

  # EBS-5128
  Scenario: (07) View SalesOrder Items section by an authorized user who has one role with one condition - Draft SO - No Item & No Company & No Store (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000057"
    Then Items section for SalesOrder with code "2020000057" is displayed empty to "Manar.Mohammed"
    And TaxesSummary for SalesOrder with code "2020000057" is displayed empty to "Manar.Mohammed"
    And the following values for SalesOrderSummary for SalesOrder with code "2020000057" are displayed to "Manar.Mohammed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      |                        |            |                       |

  # EBS-5128
  Scenario: (08) View SalesOrder Items section by an authorized user who has one role with one condition - Approved SO (Happy Path)
    Given user is logged in as "hr1"
    And CurrentDateTime = "09-Mar-2020 01:00 PM"
    And ApprovalDate of SalesOrder with code "2020000003" is "09-Mar-2020 12:00 PM"
    And the lastSalesPrice data of item with code "000051" and the following conditions exist with total number 2:
      | OrderUnit           | Customer                 | BusinessUnit | SalesPrice | SalesPriceDate       |
      | 0014 - Liter        | 000006 - المطبعة الأمنية | 0001 - Flexo | 130.000    | 09-Mar-2020 11:21 AM |
      | 0029 - Roll 2.20x50 | 000006 - المطبعة الأمنية | 0001 - Flexo | 200.000    | 09-Mar-2020 12:53 PM |
    When "hr1" requests to view Items section of SalesOrder with code "2020000003"
    Then the following values of Items section for SalesOrder with code "2020000003" are displayed to "hr1":
      | SOCode     | Item          | OrderUnit    | Qty   | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000003 | 000051 - Ink5 | 0014 - Liter | 2.000 | 10.000     | 20.000      | 130.000        | 3.000        |
    And the following values for TaxesSummary for SalesOrder with code "2020000003" are displayed to "hr1":
      | Tax                                          | TaxAmount |
      | 0001 - Vat                                   | 0.200     |
      | 0002 - Commercial and industrial profits tax | 0.100     |
      | 0003 - Real estate taxes                     | 2.000     |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000003" are displayed to "hr1":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 20.000                 | 2.300      | 22.300                |

  # EBS-5128
  Scenario: (09) View SalesOrder Items section by an authorized user who has one role with one condition - Cancelled SO (Happy Path)
    Given user is logged in as "hr1"
    And CurrentDateTime = "09-Mar-2020 01:00 PM"
    And CancellationDate of SalesOrder with code "2020000006" is "09-Mar-2020 12:00 PM"
    And the lastSalesPrice data of item with code "000051" and the following conditions exist with total number 2:
      | OrderUnit           | Customer                 | BusinessUnit | SalesPrice | SalesPriceDate       |
      | 0014 - Liter        | 000006 - المطبعة الأمنية | 0001 - Flexo | 130.000    | 09-Mar-2020 11:21 AM |
      | 0029 - Roll 2.20x50 | 000006 - المطبعة الأمنية | 0001 - Flexo | 200.000    | 09-Mar-2020 12:53 PM |
    When "hr1" requests to view Items section of SalesOrder with code "2020000006"
    Then the following values of Items section for SalesOrder with code "2020000006" are displayed to "hr1":
      | SOCode     | Item          | OrderUnit    | Qty   | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000006 | 000051 - Ink5 | 0014 - Liter | 2.000 | 10.000     | 20.000      | 130.000        | 3.000        |
    And the following values for TaxesSummary for SalesOrder with code "2020000006" are displayed to "hr1":
      | Tax                                          | TaxAmount |
      | 0001 - Vat                                   | 0.200     |
      | 0002 - Commercial and industrial profits tax | 0.100     |
      | 0003 - Real estate taxes                     | 2.000     |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000006" are displayed to "hr1":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 20.000                 | 2.300      | 22.300                |

  # EBS-5128
  Scenario: (10) View SalesOrder Items section by an authorized user who has one role with two conditions - Draft SO (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Items section of SalesOrder with code "2020000005"
    Then the following values of Items section for SalesOrder with code "2020000005" are displayed to "Ahmed.Al-Ashry":
      | SOCode     | Item                                          | OrderUnit           | Qty     | SalesPrice | TotalAmount | LastSalesPrice | AvailableQty |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5           | 0019 - Square Meter | 10.000  | 2000.000   | 20000.000   |                |              |
      | 2020000005 | 000014 - Insurance service                    | 0019 - Square Meter | 2.000   | 50.000     | 100.000     |                |              |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 100.000 | 4.000      | 400.000     | 12700.000      | 19.000       |
    And the following values for TaxesSummary for SalesOrder with code "2020000005" are displayed to "Ahmed.Al-Ashry":
      | Tax                                          | TaxAmount |
      | 0007 - Vat                                   | 205.000   |
      | 0008 - Commercial and industrial profits tax | 102.500   |
      | 0009 - Real estate taxes                     | 2050.000  |
      | 0010 - Service tax                           | 2460.000  |
      | 0011 - Adding tax                            | 102.500   |
      | 0012 - Sales tax                             | 2460.000  |
    And the following values for SalesOrderSummary for SalesOrder with code "2020000005" are displayed to "Ahmed.Al-Ashry":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 20500.000              | 7380.000   | 27880.000             |

  # EBS-5128
  Scenario: (11) View SalesOrder Items section where SalesOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Manar.Mohammed" requests to view Items section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-5128
  Scenario Outline: (12) View SalesOrder Items section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Items section of SalesOrder with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | Code       |
      | Ahmed.Al-Ashry | 2020000009 |
      | Ahmed.Al-Ashry | 2020000003 |
      | Manar.Mohammed | 2020000001 |
