# Author Dev: Order Team (02-Mar-2021)
# Author Quality: Shirin Mahmoud
Feature: Save SalesOrderData section in SalesOrder - Auth

  Background:
    Given the following users and roles exist:
      | Name                                             | Role                                                                       |
      | Ahmed.Al-Ashry                                   | SalesSpecialist_Signmedia                                                  |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole |
    And the following roles and sub-roles exist:
      | Role                                                                       | Sub-role                       |
      | SalesSpecialist_Signmedia                                                  | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                                                  | CustomerViewer_Signmedia       |
      | SalesSpecialist_Signmedia                                                  | PaymentTermViewer              |
      | SalesSpecialist_Signmedia                                                  | CurrencyViewer                 |
      | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole | SOOwner_Signmedia              |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                      | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | CustomerViewer_Signmedia       | Customer:ReadAddress            | [purchaseUnitName='Signmedia']                                            |
      | CustomerViewer_Signmedia       | Customer:ReadContactPerson      | [purchaseUnitName='Signmedia']                                            |
      | PaymentTermViewer              | PaymentTerms:ReadAll            |                                                                           |
      | CurrencyViewer                 | Currency:ReadAll                |                                                                           |
      | SOOwner_Signmedia              | SalesOrder:UpdateSalesOrderData | [purchaseUnitName='Signmedia']                                            |
    And the following users doesn't have the following permissions:
      | User                                             | Permission                 |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Customer:ReadAddress       |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Customer:ReadContactPerson |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | PaymentTerms:ReadAll       |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Currency:ReadAll           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                      | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

 #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State | CreationDate         |DocumentOwner  |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Draft | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store |
      | 2020000004 | 0001 - AL Madina |       |
      | 2020000005 | 0001 - AL Madina |       |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000004 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000004 - Client3               | 0003 - 100% Advanced Payment |                                      | EGP         |                       | 05-Nov-2020 12:00 AM |       |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And the following Addresses for Customers exist:
      | Customer                       | CustomerAddressId | AddressName       | AddressDescription          |
      | 000007 - مطبعة أكتوبر الهندسية | 5                 | Giza              | 7th st, El Sheikh Zayed     |
      | 000007 - مطبعة أكتوبر الهندسية | 6                 | Heliopolis Branch | 99, El Hegaz St, Heliopolis |
      | 000004 - Client3               | 7                 | Giza              | 7th st, El Sheikh Zayed     |
    And the following ContactPersons for Customers exist:
      | Customer                       | CustomerContactPersonId | ContactPerson          |
      | 000007 - مطبعة أكتوبر الهندسية | 5                       | Wael Fathay            |
      | 000007 - مطبعة أكتوبر الهندسية | 8                       | October Contact Person |
      | 000004 - Client3               | 9                       | Wael Fathay            |

  #EBS-6522
  Scenario Outline: (01) Save SalesOrderData section by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves SalesOrderData section of SalesOrder with Code "<SalesOrderCode>" at "07-Jan-2021 09:30 AM" with the following values:
      | PaymentTerms                               | CustomerAddress   | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes             |
      | 0001 - 20% Deposit, 80% T/T Before Loading | <CustomerAddress> | 0001 - EGP | <ContactPerson> | 01-Jul-2021 12:44 PM | notes for save hp |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | SalesOrderCode | User                                             | CustomerAddress             | ContactPerson              |
      | 2020000005     | Ahmed.Al-Ashry                                   | 7 - 7th st, El Sheikh Zayed | 9 - Wael Fathay            |
      | 2020000004     | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | 5 - 7th st, El Sheikh Zayed | 8 - October Contact Person |