# updated by: Fatma Al Zahraa (EBS - 6167)

Feature: Request to edit and cancel SalesOrderData section in SalesOrder

  Background:
    Given the following users and roles exist:
      | Name                                             | Role                                                                       |
      | hr1                                              | SuperUser                                                                  |
      | Ahmed.Al-Ashry                                   | SalesSpecialist_Signmedia                                                  |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole |
    And the following roles and sub-roles exist:
      | Role                                                                       | Sub-role                       |
      | SuperUser                                                                  | SuperUserSubRole               |
      | SalesSpecialist_Signmedia                                                  | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                                                  | CustomerViewer_Signmedia       |
      | SalesSpecialist_Signmedia                                                  | PaymentTermViewer              |
      | SalesSpecialist_Signmedia                                                  | CurrencyViewer                 |
      | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole | SOOwner_Signmedia              |
      | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole | SalesOrderTypeViewer           |
      | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole | SOViewer_Signmedia             |
      | SalesCoordinator_Signmedia_CannotViewCustomerAndPaymentTermAndCurrencyRole | PurUnitReader_Signmedia        |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                      | Condition                                                                 |
      | SuperUserSubRole               | *:*                             |                                                                           |
      | SOOwner_Signmedia              | SalesOrder:UpdateSalesOrderData | [purchaseUnitName='Signmedia']                                            |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | CustomerViewer_Signmedia       | Customer:ReadAddress            | [purchaseUnitName='Signmedia']                                            |
      | CustomerViewer_Signmedia       | Customer:ReadContactPerson      | [purchaseUnitName='Signmedia']                                            |
      | PaymentTermViewer              | PaymentTerms:ReadAll            |                                                                           |
      | CurrencyViewer                 | Currency:ReadAll                |                                                                           |
      | SalesOrderTypeViewer           | SalesOrderType:ReadAll          |                                                                           |
      | SOViewer_Signmedia             | SalesOrder:ReadAll              | [purchaseUnitName='Signmedia']                                            |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll_ForPO    | [purchaseUnitName='Signmedia']                                            |

    And the following users doesn't have the following permissions:
      | User                                             | Permission                 |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Customer:ReadAddress       |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Customer:ReadContactPerson |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | PaymentTerms:ReadAll       |
      | Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency | Currency:ReadAll           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                      | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 02-Aug-2020 09:28 AM | 0002 - Signmedia | Rejected              |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Aug-2020 12:45 PM | 0001 - Flexo     | DeliveryComplete      |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jul-2020 12:44 PM | 0001 - Flexo     | Expired               |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jun-2020 12:43 PM | 0001 - Flexo     | Canceled              |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:39 AM | 0001 - Flexo     | WaitingApproval       |
    And edit session is "30" minutes

  ### Request to Edit Sales Order Data section ###

  #EBS - 6167
  Scenario:(01) Request to edit Sales Order Data section by an authorized user in Draft State (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to edit SalesOrderData section of SalesOrder with code "2020000005"
    Then SalesOrderData section of SalesOrder with code "2020000005" becomes in edit mode and locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads   |
      | ReadAddress       |
      | ReadContactPerson |
      | ReadPaymentTerms  |
      | ReadCurrency      |
    And there are no mandatory fields returned to "Ahmed.Al-Ashry"
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields       |
      | customerAddressId    |
      | contactPersonId      |
      | paymentTermCode      |
      | expectedDeliveryDate |
      | notes                |

  #EBS - 6167
  Scenario:(02) Request to edit Sales Order Data section by an authorized user in Rejected State (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to edit SalesOrderData section of SalesOrder with code "2020000051"
    Then SalesOrderData section of SalesOrder with code "2020000051" becomes in edit mode and locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads   |
      | ReadAddress       |
      | ReadContactPerson |
      | ReadPaymentTerms  |
      | ReadCurrency      |
    And the following mandatory fields are returned to "Ahmed.Al-Ashry":
      | MandatoriesFields    |
      | customerAddressId    |
      | contactPersonId      |
      | paymentTermCode      |
      | expectedDeliveryDate |
      | currencyCode         |
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields       |
      | customerAddressId    |
      | contactPersonId      |
      | paymentTermCode      |
      | expectedDeliveryDate |
      | notes                |


  Scenario:(03) Request to edit Sales Order Data section that is locked by another user (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first opened the SalesOrderData section of SalesOrder with code "2020000005" in the edit mode successfully
    When "Ahmed.Al-Ashry" requests to edit SalesOrderData section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-02"

  Scenario:(04) Request to edit Sales Order Data section of deleted SalesOrder (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to edit SalesOrderData section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario Outline: (05) Request to edit Sales Order Data section when it is not allowed in current state: WaitingApproval, Approved, Expired, Cancelled, GoodsIssueActivated, SalesInvoiceActivated, ReadyForDelivery, DeliveryComplete (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to edit SalesOrderData section of SalesOrder with code "<SalesOrderCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And SalesOrderData section of SalesOrder with code "<SalesOrderCode>" is not locked by "hr1"
    Examples:
      | SalesOrderCode |
      | 2020000002     |
      | 2020000003     |
      | 2020000006     |
      | 2020000007     |
      | 2020000008     |
      | 2020000010     |
      | 2020000011     |
      | 2020000012     |

  Scenario Outline:(06) Request to edit Sales Order Data section with unauthorized user (with condition - without condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to edit SalesOrderData section of SalesOrder with code "<SalesOrderCode>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SalesOrderCode |
      | 2020000009     |
      | 2020000001     |

  #EBS - 6167
  Scenario: (07) Request to edit Sales Order Data section  (No authorized reads)
    Given user is logged in as "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency"
    When "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency" requests to edit SalesOrderData section of SalesOrder with code "2020000005"
    Then SalesOrderData section of SalesOrder with code "2020000005" becomes in edit mode and locked by "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency"
    And there are no authorized reads returned to "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency"
    And there are no mandatory fields returned to "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency"
    And the following editable fields are returned to "Manar.Mohammed.NoCustomerNoPaymentTermNoCurrency":
      | EditablleFields      |
      | customerAddressId    |
      | contactPersonId      |
      | paymentTermCode      |
      | expectedDeliveryDate |
      | notes                |

  ### Request to Cancel Sales Order Data section ###

  Scenario:(08) Request to Cancel saving Sales Order Data section by an authorized user with one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderData section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:10 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderData section of SalesOrder with code "2020000005" at "07-Jan-2019 09:30 AM"
    Then the lock by "Ahmed.Al-Ashry" on SalesOrderData section of SalesOrder with code "2020000005" is released

  Scenario:(09) Request to Cancel saving Sales Order Data section after lock session is expire
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderData section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderData section of SalesOrder with code "2020000005" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"

  Scenario: (10) Request to Cancel saving Sales Order Data section after lock session is expire and SalesOrder doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And SalesOrderData section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully at "07-Jan-2019 09:31 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderData section of SalesOrder with code "2020000005" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario Outline:(11) Request to Cancel saving Sales Order Data section with unauthorized user (with condition - without condition (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderData section of SalesOrder with code "<SalesOrderCode>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SalesOrderCode |
      | 2020000009     |
      | 2020000001     |
