#Author: Zyad Ghorab, Sherien
#Reviewer: Sherien

Feature: View All SalesOrder

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Ahmed.Seif     | Quality_Specialist         |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | Afaf           | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                         |
      | Quality_Specialist         | SOViewer                        |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia              |
      | SalesSpecialist_Signmedia  | SOViewer_LoggedInUser_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission         | Condition                                                                 |
      | SOViewer                        | SalesOrder:ReadAll |                                                                           |
      | SOViewer_Signmedia              | SalesOrder:ReadAll | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | SalesOrder:ReadAll |
    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0002 - Mohammed            |                      | 0001 - Flexo     | Draft                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Aug-2020 12:45 PM | 0001 - Flexo     | DeliveryComplete      |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jul-2020 12:44 PM | 0001 - Flexo     | Expired               |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jun-2020 12:43 PM | 0001 - Flexo     | Canceled              |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:43 AM | 0001 - Flexo     | Rejected              |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:39 AM | 0001 - Flexo     | WaitingApproval       |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Feb-2020 08:29 AM | 0001 - Flexo     | Draft                 |
    And the total number of records of SalesOrders is 13

  #EBS-5413
  Scenario: (01) View all SalesOrders by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with no filter applied with 13 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0002 - Mohammed            |                      | 0001 - Flexo     | Draft                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Aug-2020 12:45 PM | 0001 - Flexo     | DeliveryComplete      |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jul-2020 12:44 PM | 0001 - Flexo     | Expired               |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jun-2020 12:43 PM | 0001 - Flexo     | Canceled              |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:43 AM | 0001 - Flexo     | Rejected              |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:39 AM | 0001 - Flexo     | WaitingApproval       |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Feb-2020 08:29 AM | 0001 - Flexo     | Draft                 |
    And the total number of records in search results by "Ahmed.Seif" are 13

  #EBS-5413
  Scenario: (02) View all SalesOrders by an authorized user WITH condition [BusinessUnit] (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read page 1 of SalesOrders with no filter applied with 20 records per page
    Then the following values will be presented to "Manar.Mohammed":
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
    And the total number of records in search results by "Manar.Mohammed" are 5

  #EBS-5413
  Scenario: (03) View all SalesOrders by an authorized user WITH condition [SalesRep][BusinessUnit] (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesOrders with no filter applied with 20 records per page
    Then the following values will be presented to "Ahmed.Al-Ashry":
      | Code       | Type                      | Customer                       | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit     | State               |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft               |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 2

  #EBS-5413
  Scenario: (04) Filter View all SalesOrders by Code by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on SOCode which contains "00002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                 | SalesResponsible | ExpectedDeliveryDate | BusinessUnit | State           |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar     | 10-Mar-2020 08:39 AM | 0001 - Flexo | WaitingApproval |
    And the total number of records in search results by "Ahmed.Seif" are 1

  #EBS-5413
  Scenario: (05) Filter View all SalesOrders by CustomerCode by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on CustomerCode which contains "006" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                 | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit | State            |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0002 - Mohammed       |                      | 0001 - Flexo | Draft            |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar          | 01-Aug-2020 12:45 PM | 0001 - Flexo | DeliveryComplete |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar          | 01-Jul-2020 12:44 PM | 0001 - Flexo | Expired          |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar          | 01-Jun-2020 12:43 PM | 0001 - Flexo | Canceled         |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar          | 10-Mar-2020 08:43 AM | 0001 - Flexo | Rejected         |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0004 - Ahmed.Al-Ashry | 05-Apr-2020 08:42 AM | 0001 - Flexo | Approved         |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar          | 10-Mar-2020 08:39 AM | 0001 - Flexo | WaitingApproval  |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0004 - Ahmed.Al-Ashry | 05-Feb-2020 08:29 AM | 0001 - Flexo | Draft            |
    And the total number of records in search results by "Ahmed.Seif" are 8

  #EBS-5413
  Scenario: (06) Filter View all SalesOrders by Customer Name by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on CustomerName which contains "الهندسية" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
    And the total number of records in search results by "Ahmed.Seif" are 4

  #EBS-5413 + Needs Review
  Scenario: (07) Filter View all SalesOrders by Sales Responsible by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on SalesResponsible which contains "Mohammed" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                 | SalesResponsible | ExpectedDeliveryDate | BusinessUnit     | State |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0002 - Mohammed  |                      | 0001 - Flexo     | Draft |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram        | 0002 - Mohammed  | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-5413
  Scenario: (08) Filter View all SalesOrders by ExpectedDeliveryDate by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on ExpectedDeliveryDate which contains "10-Mar-2020" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                 | SalesResponsible | ExpectedDeliveryDate | BusinessUnit | State           |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar     | 10-Mar-2020 08:43 AM | 0001 - Flexo | Rejected        |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية | 0003 - Manar     | 10-Mar-2020 08:39 AM | 0001 - Flexo | WaitingApproval |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #EBS-5413
  Scenario: (09) Filter View all SalesOrders by BusinessUnit by authorized user using "equals" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on BusinessUnit which equals "Signmedia" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
    And the total number of records in search results by "Ahmed.Seif" are 5

  #EBS-5413
  Scenario: (10) Filter View all SalesOrders by State by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of SalesOrders with filter applied on State which contains "Draft" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                      | Customer                       | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit     | State |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0002 - Mohammed       |                      | 0001 - Flexo     | Draft |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed       | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry | 05-Feb-2020 08:29 AM | 0001 - Flexo     | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 4

  #EBS-5413
  Scenario: (11) Filter View All SalesOrders by Code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of SalesOrders with filter applied on SOCode which contains "00001" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page