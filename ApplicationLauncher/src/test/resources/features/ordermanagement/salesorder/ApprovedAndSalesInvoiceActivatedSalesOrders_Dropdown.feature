#Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Ahmed

Feature: View Approved and SalesInvoiceActivated SalesOrders

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Afaf           | FrontDesk                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole            |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission         | Condition                      |
      | SOViewer_Signmedia | SalesOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | SalesOrder:ReadAll |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000070 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 28-Dec-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 05-Nov-2020 12:00 AM | 0002 - Signmedia | Approved              |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 31-Aug-2020 09:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-Jun-2020 09:00 AM | 0002 - Signmedia | Approved              |
      | 2020000066 | LOCAL_SALES_ORDER - Local | 000004 - Client3               | 0002 - Mohammed            | 09-Jun-2020 11:51 AM | 0001 - Flexo     | Draft                 |
      | 2020000065 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0002 - Mohammed            | 09-Jun-2020 09:58 AM | 0002 - Signmedia | Draft                 |
      | 2020000064 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 09-Jun-2020 09:57 AM | 0002 - Signmedia | Draft                 |
      | 2020000063 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 09-Jun-2020 09:55 AM | 0002 - Signmedia | Draft                 |
      | 2020000062 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      |                      | 0001 - Flexo     | Draft                 |
      | 2020000061 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      |                      | 0002 - Signmedia | Draft                 |
      | 2020000060 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      |                      | 0002 - Signmedia | Draft                 |
      | 2020000059 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      |                      | 0002 - Signmedia | Draft                 |
      | 2020000058 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      |                      | 0002 - Signmedia | Draft                 |
      | 2020000057 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      |                      | 0002 - Signmedia | Draft                 |
      | 2020000056 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 07-Aug-2020 09:28 AM | 0002 - Signmedia | DeliveryComplete      |
      | 2020000055 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 06-Aug-2020 09:28 AM | 0002 - Signmedia | Expired               |
      | 2020000054 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 05-Aug-2020 09:28 AM | 0002 - Signmedia | Canceled              |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved              |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 03-Aug-2020 09:28 AM | 0002 - Signmedia | WaitingApproval       |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 02-Aug-2020 09:28 AM | 0002 - Signmedia | Rejected              |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0002 - Mohammed            |                      | 0001 - Flexo     | Draft                 |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Aug-2020 12:45 PM | 0001 - Flexo     | DeliveryComplete      |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jul-2020 12:44 PM | 0001 - Flexo     | Expired               |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jun-2020 12:43 PM | 0001 - Flexo     | Canceled              |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:43 AM | 0001 - Flexo     | Rejected              |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 10-Mar-2020 08:39 AM | 0001 - Flexo     | WaitingApproval       |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Feb-2020 08:29 AM | 0001 - Flexo     | Draft                 |
    And the total number of records of SalesOrders is 33

    #EBS-6755 #EBS-7175
  Scenario: (01) Read list of SalesOrders - in state Approved And SalesInvoiceActivated by authorized user with condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to read list of Approved SalesOrders with BU "0002 - Signmedia"
    Then the following SalesOrder values will be presented to "Manar.Mohammed":
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 05-Nov-2020 12:00 AM | 0002 - Signmedia | Approved              |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 31-Aug-2020 09:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-Jun-2020 09:00 AM | 0002 - Signmedia | Approved              |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved              |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
    And total number of SalesOrders returned to "Manar.Mohammed" is equal to 5

    #EBS-6755
  Scenario: (02) Read list of SalesOrders - by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read list of Approved SalesOrders with BU "0002 - Signmedia"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page