# Author Dev: Zyad Ghorab
# Author Quality: Shirin

Feature: View SalesOrderData section

  Background:
    Given the following users and roles exist:
      | Name           | Role                       |
      | Manar.Mohammed | SalesCoordinator_Signmedia |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia  |
      | hr1            | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole                         |
      | SalesCoordinator_Signmedia | SOViewer_Signmedia              |
      | SalesSpecialist_Signmedia  | SOViewer_LoggedInUser_Signmedia |
      | SuperUser                  | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                    | Condition                                                                 |
      | SOViewer_Signmedia              | SalesOrder:ReadSalesOrderData | [purchaseUnitName='Signmedia']                                            |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadSalesOrderData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole                | *:*                           |                                                                           |
    And the following users have the following permissions without the following conditions:
      | User           | Permission                    | Condition                                                             |
      | Manar.Mohammed | SalesOrder:ReadSalesOrderData | [purchaseUnitName='Flexo']                                            |
      | Ahmed.Al-Ashry | SalesOrder:ReadSalesOrderData | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |
    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy                 | State    | CreationDate         |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 01-Feb-2020 07:52 AM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Approved | 04-Feb-2020 08:39 AM |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin from BDKCompanyCode | Draft    | 04-Feb-2020 09:05 AM |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0002 - Mohammed       | Admin from BDKCompanyCode | Draft    | 11-Feb-2020 01:28 PM |
    And the following SalesOrderData for the following SalesOrders exist:
      | SOCode     | Customer                       | PaymentTerms                               | CustomerAddress         | CurrencyISO | ContactPerson           | ExpectedDeliveryDate | CreditLimit | Notes                                |
      | 2020000001 | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading | 66, Salah Salem St      | EGP         |                         | 05-Feb-2020 08:29 AM | 100000.0    | this is notes for sales order 000001 |
      | 2020000003 | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading | 7th st, El Sheikh Zayed | EGP         | Client 2 Contact Person | 05-Apr-2020 08:42 AM | 100000.0    | this is notes for sales order 000003 |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        | 7th st, El Sheikh Zayed | EGP         | Wael Fathay             | 30-May-2020 09:28 AM | 100000.0    |                                      |
      | 2020000009 | 000001 - Al Ahram              | 0002 - 20% advance, 80% Copy of B/L        | 66, Salah Salem St      | EGP         | Wael Fathay             | 07-Apr-2020 10:21 AM | 300000.0    | this is notes for sales order 000009 |

  # EBS-5412
  Scenario: (01) View SalesOrderData section by an authorized user who has one role with one condition (Happy Path)
    Given user is logged in as "Manar.Mohammed"
    When "Manar.Mohammed" requests to view SalesOrderData section of SalesOrder with code "2020000005"
    Then the following values of SalesOrderData section for SalesOrder with code "2020000005" are displayed to "Manar.Mohammed":
      | SOCode     | Customer                       | PaymentTerms                        | CustomerAddress         | CurrencyISO | ContactPerson | ExpectedDeliveryDate | CreditLimit | Notes |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L | 7th st, El Sheikh Zayed | EGP         | Wael Fathay   | 30-May-2020 09:28 AM | 100000.0    |       |

  # EBS-5412
  Scenario: (02) View SalesOrderData section by an authorized user who has one role with two conditions (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view SalesOrderData section of SalesOrder with code "2020000005"
    Then the following values of SalesOrderData section for SalesOrder with code "2020000005" are displayed to "Ahmed.Al-Ashry":
      | SOCode     | Customer                       | PaymentTerms                        | CustomerAddress         | CurrencyISO | ContactPerson | ExpectedDeliveryDate | CreditLimit | Notes |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L | 7th st, El Sheikh Zayed | EGP         | Wael Fathay   | 30-May-2020 09:28 AM | 100000.0    |       |

  # EBS-5412
  Scenario: (03) View SalesOrderData section where SalesOrder doesn't exist (Exception Case)
    Given user is logged in as "Manar.Mohammed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Manar.Mohammed" requests to view SalesOrderData section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Manar.Mohammed" with the following message "Gen-msg-01"

  # EBS-5412
  Scenario Outline: (04) View SalesOrderData section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view SalesOrderData section of SalesOrder with code "<SOCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User           | SOCode     |
      | Manar.Mohammed | 2020000001 |
      | Ahmed.Al-Ashry | 2020000003 |
      | Ahmed.Al-Ashry | 2020000009 |
