# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud
# Reviewer: Somaya Ahmed

Feature: SO Request Add Items section

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                           |
      | hr1                    | SuperUser                                      |
      | Ahmed.Al-Ashry         | SalesSpecialist_Signmedia                      |
      | Manar.Mohammed.NoItems | SalesCoordinator_Signmedia_CannotViewItemsRole |

    And the following roles and sub-roles exist:
      | Role                                           | Sub-role                       |
      | SuperUser                                      | SuperUserSubRole               |
      | SalesSpecialist_Signmedia                      | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia                      | ItemViewer_Signmedia           |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | SOOwner_Signmedia              |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | SalesOrderTypeViewer           |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | SOViewer_Signmedia             |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | SOViewer_Signmedia             |
      | SalesCoordinator_Signmedia_CannotViewItemsRole | PurUnitReader_Signmedia        |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                   | Condition                                                                 |
      | SuperUserSubRole               | *:*                          |                                                                           |
      | SOOwner_Signmedia              | SalesOrder:UpdateItems       | [purchaseUnitName='Signmedia']                                            |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateItems       | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | ItemViewer_Signmedia           | Item:ReadAll                 | [purchaseUnitName='Signmedia']                                            |
      | ItemViewer_Signmedia           | Item:ReadUoMData             | [purchaseUnitName='Signmedia']                                            |
      | SalesOrderTypeViewer           | SalesOrderType:ReadAll       |                                                                           |
      | SOViewer_Signmedia             | SalesOrder:ReadAll           | [purchaseUnitName='Signmedia']                                            |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']                                            |

    And the following users doesn't have the following permissions:
      | User                   | Permission       |
      | Manar.Mohammed.NoItems | Item:ReadAll     |
      | Manar.Mohammed.NoItems | Item:ReadUoMData |

    And the following users have the following permissions without the following conditions:
      | User           | Permission           | Condition                                                             |
      | Ahmed.Al-Ashry | SalesOrder:ReadItems | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 03-Aug-2020 09:28 AM | 0002 - Signmedia | WaitingApproval       |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 02-Aug-2020 09:28 AM | 0002 - Signmedia | Rejected              |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery      |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated   |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft                 |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Aug-2020 12:45 PM | 0001 - Flexo     | DeliveryComplete      |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jul-2020 12:44 PM | 0001 - Flexo     | Expired               |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0003 - Manar               | 01-Jun-2020 12:43 PM | 0001 - Flexo     | Canceled              |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-May-2020 09:28 AM | 0002 - Signmedia | Draft                 |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Apr-2020 08:42 AM | 0001 - Flexo     | Approved              |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 000006 - المطبعة الأمنية       | 0004 - Ahmed.Al-Ashry      | 05-Feb-2020 08:29 AM | 0001 - Flexo     | Draft                 |

    And edit session is "30" minutes

  #EBS-6029
  Scenario: (01) Request to Add Item to SalesOrder in (Draft) state by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to add Item to SalesOrderItems section of SalesOrder with code "2020000005"
    Then a new add Item dialoge is opened and SalesOrderItems section of SalesOrder with code "2020000005" becomes locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Ahmed.Al-Ashry":
      | MandatoriesFields |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields    |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |

  #EBS-6029
  Scenario: (02) Request to Add Item to SalesOrder in (Rejected) state by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to add Item to SalesOrderItems section of SalesOrder with code "2020000051"
    Then a new add Item dialoge is opened and SalesOrderItems section of SalesOrder with code "2020000051" becomes locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Ahmed.Al-Ashry":
      | MandatoriesFields |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields    |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |


  #EBS-6029
  Scenario: (03) Request to Add Item to SalesOrder when SalesOrderItems section is locked by another user (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to SalesOrderItems section of SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to add Item to SalesOrderItems section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-02"

  Scenario: (04) Request to Add Item to to SalesOrder that doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully
    When "Ahmed.Al-Ashry" requests to add Item to SalesOrderItems section of SalesOrder with code "2020000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  #EBS-6029
  Scenario Outline: (05) Request to Add Item to SalesOrder in a state that doesn't allow this action - All states except Draft and Rejected (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to add Item to SalesOrderItems section of SalesOrder with code "<SalesOrderCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And SalesOrderItems section of SalesOrder with code "<SalesOrderCode>" is not locked by "hr1"
    Examples:
      | SalesOrderCode |
      | 2020000052     |
      | 2020000010     |
      | 2020000011     |
      | 2020000012     |
      | 2020000008     |
      | 2020000007     |
      | 2020000006     |
      | 2020000003     |

  #EBS-6029
  Scenario Outline: (06) Request to Add Item to SalesOrder by an unauthorized user with unauthorized user (with condition - without condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    When  "Ahmed.Al-Ashry" requests to add Item to SalesOrderItems section of SalesOrder with code "<SalesOrderCode>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SalesOrderCode |
      | 2020000009     |
      | 2020000001     |

  #EBS-6029
  Scenario: (07) Request to Add Item to SalesOrder by a User with no authorized reads - All allowed states (Happy Path)
    Given user is logged in as "Manar.Mohammed.NoItems"
    When "Manar.Mohammed.NoItems" requests to add Item to SalesOrderItems section of SalesOrder with code "2020000005"
    Then a new add Item dialoge is opened and SalesOrderItems section of SalesOrder with code "2020000005" becomes locked by "Manar.Mohammed.NoItems"
    And there are no authorized reads returned to "Manar.Mohammed.NoItems"
    And the following mandatory fields are returned to "Manar.Mohammed.NoItems":
      | MandatoriesFields |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |
    And the following editable fields are returned to "Manar.Mohammed.NoItems":
      | EditableFields    |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | salesPrice        |

   ######### Cancel saving SalesOrderItems section ########################################################################

  #EBS-6029
  Scenario: (08) Request to Cancel saving SalesOrder Items section within the edit session in Draft state by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderItems section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:10 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderItems section of SalesOrder with code "2020000005" at "07-Jan-2019 09:30 AM"
    Then the lock by "Ahmed.Al-Ashry" on SalesOrderItems section of SalesOrder with code "2020000005" is released

  #EBS-6029
  Scenario: (09) Request to Cancel saving SalesOrder Items section after lock session is expired
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderItems section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderItems section of SalesOrder with code "2020000005" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"

  Scenario: (10) Request to Cancel saving SalesOrder Items section after lock session is expire and SalesOrder doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And SalesOrderItems section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the SalesOrder with code "2020000005" successfully at "07-Jan-2019 09:31 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderItems section of SalesOrder with code "2020000005" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  #EBS-6029
  Scenario Outline: (11) Request to Cancel saving SalesOrder Items section with unauthorized user (with condition - without condition (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" cancels saving SalesOrderItems section of SalesOrder with code "<SalesOrderCode>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SalesOrderCode |
      | 2020000009     |
      | 2020000001     |
