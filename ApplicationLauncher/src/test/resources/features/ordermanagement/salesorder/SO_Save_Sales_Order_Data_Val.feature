# Author Dev: Order Team (01-March-2021)
# Author Quality: Shirin Mahmoud

Feature: Save SalesOrderData section in SalesOrder Validation

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role                       |
      | SalesSpecialist_Signmedia | SOOwner_LoggedInUser_Signmedia |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia       |
      | SalesSpecialist_Signmedia | PaymentTermViewer              |
      | SalesSpecialist_Signmedia | CurrencyViewer                 |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                      | Condition                                                                 |
      | SOOwner_LoggedInUser_Signmedia | SalesOrder:UpdateSalesOrderData | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | CustomerViewer_Signmedia       | Customer:ReadAddress            | [purchaseUnitName='Signmedia']                                            |
      | CustomerViewer_Signmedia       | Customer:ReadContactPerson      | [purchaseUnitName='Signmedia']                                            |
      | PaymentTermViewer              | PaymentTerms:ReadAll            |                                                                           |
      | CurrencyViewer                 | Currency:ReadAll                |                                                                           |

    And the following Currencies exist:
      | Code | Name                 | ISO |
      | 0001 | Egyptian Pound       | EGP |
      | 0002 | United States Dollar | USD |

    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |

    And the following Addresses for Customers exist:
      | Customer                       | CustomerAddressId | AddressName       | AddressDescription          |
      | 000007 - مطبعة أكتوبر الهندسية | 5                 | Giza              | 7th st, El Sheikh Zayed     |
      | 000007 - مطبعة أكتوبر الهندسية | 6                 | Heliopolis Branch | 99, El Hegaz St, Heliopolis |

    And the following ContactPersons for Customers exist:
      | Customer                       | CustomerContactPersonId | ContactPerson          |
      | 000007 - مطبعة أكتوبر الهندسية | 5                       | Wael Fathay            |
      | 000007 - مطبعة أكتوبر الهندسية | 8                       | October Contact Person |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Rejected | 11-Feb-2020 01:28 PM |Manar Mohammed |
      | 2019000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Draft    | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2019000010 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000004 - 7th st, El Sheikh Zayed     | EGP         | 000004 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2019000010 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    And edit session is "30" minutes

 #EBS-6167
  Scenario: (01) Save SalesOrderData section after edit session expired for SalesOrder (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderData section of SalesOrder with code "2019000010" is locked by "Ahmed.Al-Ashry" at "01-Mar-2021 09:10 AM"
    When "Ahmed.Al-Ashry" saves SalesOrderData section of SalesOrder with Code "2019000010" at "01-Mar-2021 09:41 AM" with the following values:
      | PaymentTerms                               | CustomerAddress                 | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes             |
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp |
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"
    And SalesOrderData section of SalesOrder with Code "2019000010" remains as follows:
      | PaymentTerms                 | CustomerAddress                 | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes |
      | 0003 - 100% Advanced Payment | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 05-Nov-2020 12:00 AM |       |


    #EBS-6167
  Scenario: (02) Save SalesOrderData section after edit session expired & SalesOrder is deleted (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And SalesOrderData section of SalesOrder with code "2019000010" is locked by "Ahmed.Al-Ashry" at "01-Mar-2021 09:10 AM"
    And "hr1" first deleted the SalesOrder with code "2019000010" successfully at "01-Mar-2021 09:41 AM"
    When "Ahmed.Al-Ashry" saves SalesOrderData section of SalesOrder with Code "2019000010" at "01-Mar-2021 09:42 AM" with the following values:
      | PaymentTerms                               | CustomerAddress                 | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes             |
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp |
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

    #EBS-6167
  Scenario Outline: (03) Save SalesOrderData section With wrong/malicious/unrelated data entry (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderData section of SalesOrder with code "2019000010" is locked by "Ahmed.Al-Ashry" at "01-Mar-2021 09:10 AM"
    When "Ahmed.Al-Ashry" saves SalesOrderData section of SalesOrder with Code "2019000010" at "01-Mar-2021 09:20 AM" with the following values:
      | PaymentTerms   | CustomerAddress   | Currency   | ContactPerson   | ExpectedDeliveryDate   | Notes   |
      | <PaymentTerms> | <CustomerAddress> | <Currency> | <ContactPerson> | <ExpectedDeliveryDate> | <Notes> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | PaymentTerms                               | CustomerAddress                 | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes                                                                                                                                                                                                                                                       |
      #malicious input on PaymentTerms And apply also on [CustomerAddress & ContactPerson & Currency]
      | 00001 - PaymentTermCodeInvalidPattern      | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp                                                                                                                                                                                                                                           |
      | ay7aga - ay7aga                            | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp                                                                                                                                                                                                                                           |
      | 9999 - PaymentTermDoesn'tExist             | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp                                                                                                                                                                                                                                           |
     #unrelated data on CustomerAddress And Apply also on [ContactPerson]
      | 0001 - 20% Deposit, 80% T/T Before Loading | 1 - 66, Salah Salem St          | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp                                                                                                                                                                                                                                           |
      #wrong Data on Currency [Local SalesOrder must have EGP]
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0002 - USD | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp                                                                                                                                                                                                                                           |
      #wrong Data on ExpectedDeliveryDate InvalidPattern
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | Ay7aga               | notes for save hp                                                                                                                                                                                                                                           |
      #wrong Data on Notes InvalidPattern
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | $                                                                                                                                                                                                                                                           |
      | 0001 - 20% Deposit, 80% T/T Before Loading | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250_ |

  #EBS-6167
  Scenario Outline: (04) Save SalesOrderData section with missing mandatory field in state [Rejected](Abuse Case\Client bypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesOrderData section of SalesOrder with code "2020000005" is locked by "Ahmed.Al-Ashry" at "01-Mar-2021 09:10 AM"
    When "Ahmed.Al-Ashry" saves SalesOrderData section of SalesOrder with Code "2020000005" at "01-Mar-2021 09:20 AM" with the following values:
      | PaymentTerms   | CustomerAddress                 | Currency   | ContactPerson   | ExpectedDeliveryDate | Notes             |
      | <PaymentTerms> | 6 - 99, El Hegaz St, Heliopolis | 0001 - EGP | 5 - Wael Fathay | 01-Jul-2021 12:44 PM | notes for save hp |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
    #missing input on PaymentTerms And apply also on [customerAddress && contactPerson && expectedDeliveryDate && currency]
      | PaymentTerms |
      |              |
      | ""           |
      | N/A          |
