# Reviewer: Somaya Ahmed on 02-Jan-2019, 11:00 AM
# updated by: Eman Mansour (EBS - 5958)
# updated by: Fatma Al Zahraa (EBS - 7411)


Feature: Create PurchaseOrder HP

  User should be able to create PurchaseOrder
  Rules:
  - User should be authenticated and authorized to create Local/Import/Serivce POs
  - User should be authorized to ReadAll Types, Reference POs, business units, vendors, document owner with OR without consition
  - User should enter all mandatory fields for creation
  - If an unauthorized user sends a create request, he/she shoud be logged out as this is considered as a security breach

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                  |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia        |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia       |
      | PurchasingResponsible_Signmedia  | PurUnitReader_Signmedia  |
      | PurchasingResponsible_Signmedia  | VendorOwner_Signmedia    |
      | PurchasingResponsible_Signmedia  | DocumentOwnerViewer      |
      | PurchasingResponsible_Flexo      | POOwner_Flexo            |
      | PurchasingResponsible_Flexo      | POViewer_Flexo           |
      | PurchasingResponsible_Flexo      | PurUnitReader_Flexo      |
      | PurchasingResponsible_Flexo      | VendorOwner_Flexo        |
      | PurchasingResponsible_Flexo      | DocumentOwnerViewer      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated       |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated      |
      | PurchasingResponsible_Corrugated | PurUnitReader_Corrugated |
      | PurchasingResponsible_Corrugated | VendorOwner_Corrugated   |
      | PurchasingResponsible_Corrugated | DocumentOwnerViewer      |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                   | Condition                       |
      | POOwner_Signmedia        | PurchaseOrder:Create         |                                 |
      | POOwner_Flexo            | PurchaseOrder:Create         |                                 |
      | POOwner_Corrugated       | PurchaseOrder:Create         |                                 |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']  |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']      |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Corrugated'] |
      | VendorOwner_Signmedia    | Vendor:ReadAll               | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Flexo        | Vendor:ReadAll               | [purchaseUnitName='Flexo']      |
      | VendorOwner_Corrugated   | Vendor:ReadAll               | [purchaseUnitName='Corrugated'] |
      | POViewer_Signmedia       | PurchaseOrder:ReadAll        | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo           | PurchaseOrder:ReadAll        | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated      | PurchaseOrder:ReadAll        | [purchaseUnitName='Corrugated'] |
      | DocumentOwnerViewer      | DocumentOwner:ReadAll        |                                 |
    And the following DocumentOwners exist:
      | Id | Name        | BusinessObject | Permission         | Condition |
      | 37 | Gehan Ahmed | PurchaseOrder  | CanBeDocumentOwner |           |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following vendors exist:
      | Code   | Name              | PurchasingUnit |
      | 000001 | Siegwerk          | 0001           |
      | 000002 | Zhejiang          | 0002           |
      | 000003 | ABC               | 0003           |
      | 000004 | Vendor Digital    | 0004           |
      | 000005 | Vendor Textile    | 0005           |
      | 000006 | Vendor Corrugated | 0006           |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000013 | LOCAL_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000014 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000013 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000014 | 0002 - Signmedia | 0003 - HPS       | 1516171819789 - EGP - Alex Bank |
    And the Company "0001 - AL Madina" has the following Taxes:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And the Company "0003 - HPS" has no Taxes:

  ######## Create Happy Paths
  #EBS-5958
  #EBS-7411
  #EBS-7410
  Scenario Outline: (01) Create PurchaseOrder with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "<CurrentDateTime>"
    And Last created PurchaseOrder was with code "2018000999"
    When "<User>" creates PurchaseOrder with following values:
      | Type        | ReferencePurchaseOrder        | BusinessUnit            | Vendor            | DocumentOwnerId        |
      | <TypeValue> | <ReferencePurchaseOrderValue> | <BusinessUnitCodeValue> | <VendorCodeValue> | <DocumentOwnerIdValue> |
    Then a new PurchaseOrder is created as follows:
      | Code        | State | CreatedBy | LastUpdatedBy | CreationDate      | LastUpdateDate    | Type        | DocumentOwner            | TotalRemaining |
      | <NewPOCode> | Draft | <User>    | <User>        | <CurrentDateTime> | <CurrentDateTime> | <TypeValue> | <DocumentOwnerNameValue> | 0.00           |
    And the Vendor Section of PurchaseOrder with code "<NewPOCode>" is updated as follows:
      | Vendor                | ReferencePO                   |
      | <VendorCodeNameValue> | <ReferencePurchaseOrderValue> |
    And the Company Section of PurchaseOrder with code "<NewPOCode>" is updated as follows:
      | Company                | BusinessUnit                |
      | <CompanyCodeNameValue> | <BusinessUnitCodeNameValue> |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | CurrentDateTime      | NewPOCode  | TypeValue | BusinessUnitCodeValue | BusinessUnitCodeNameValue | DocumentOwnerIdValue | DocumentOwnerNameValue | VendorCodeValue | VendorCodeNameValue | ReferencePurchaseOrderValue | CompanyCodeNameValue |
      | Gehan.Ahmed | 02-Jan-2018 11:00 AM | 2018001000 | IMPORT_PO | 0002                  | 0002 - Signmedia          | 37                   | Gehan Ahmed            | 000002          | 000002 - Zhejiang   |                             |                      |
      | Gehan.Ahmed | 01-Jan-2019 11:00 AM | 2019000001 | IMPORT_PO | 0002                  | 0002 - Signmedia          | 37                   | Gehan Ahmed            | 000002          | 000002 - Zhejiang   |                             |                      |
      | Gehan.Ahmed | 01-Jan-2019 11:00 AM | 2019000001 | LOCAL_PO  | 0002                  | 0002 - Signmedia          | 37                   | Gehan Ahmed            | 000002          | 000002 - Zhejiang   |                             |                      |
      | Gehan.Ahmed | 02-Jan-2018 11:00 AM | 2018001000 | LOCAL_PO  | 0002                  | 0002 - Signmedia          | 37                   | Gehan Ahmed            | 000002          | 000002 - Zhejiang   |                             |                      |

  #EBS-8732
  Scenario: (02) Create PurchaseOrder with all mandatory fields by an authorized user - Service PO for Reference Company (0001 - AL Madina) (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created PurchaseOrder was with code "2018000999"
    When "Gehan.Ahmed" creates PurchaseOrder with following values:
      | Type       | ReferencePurchaseOrder | BusinessUnit | Vendor | DocumentOwnerId |
      | SERVICE_PO | 2018000013             | 0002         | 000002 | 37              |
    Then a new PurchaseOrder is created as follows:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type       | DocumentOwner | TotalRemaining |
      | 2020000001 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 01-Jan-2020 11:00 AM | 01-Jan-2020 11:00 AM | SERVICE_PO | Gehan Ahmed   | 0.00           |
    And the Vendor Section of PurchaseOrder with code "2020000001" is updated as follows:
      | Vendor            | ReferencePO |
      | 000002 - Zhejiang | 2018000013  |
    And the Company Section of PurchaseOrder with code "2020000001" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the Taxes Section for Service PurchaseOrder with code "2020000001" is updated as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0007 - Vat                                   | 1.0           |           |
      | 0008 - Commercial and industrial profits tax | 0.5           |           |
      | 0009 - Real estate taxes                     | 10.0          |           |
      | 0010 - Service tax                           | 12.0          |           |
      | 0011 - Adding tax                            | 0.5           |           |
      | 0012 - Sales tax                             | 12.0          |           |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  ######## Create Happy Paths
  #EBS-5958
  #EBS-7411
  #EBS-7410
  #EBS-8732
  Scenario Outline: (03) Create PurchaseOrder with all mandatory fields by an authorized user - Service PO for Reference Company (0003 - HPS) (Happy Path)
    Given user is logged in as "<User>"
    And CurrentDateTime = "<CurrentDateTime>"
    And Last created PurchaseOrder was with code "2018000999"
    When "<User>" creates PurchaseOrder with following values:
      | Type        | ReferencePurchaseOrder        | BusinessUnit            | Vendor            | DocumentOwnerId        |
      | <TypeValue> | <ReferencePurchaseOrderValue> | <BusinessUnitCodeValue> | <VendorCodeValue> | <DocumentOwnerIdValue> |
    Then a new PurchaseOrder is created as follows:
      | Code        | State | CreatedBy | LastUpdatedBy | CreationDate      | LastUpdateDate    | Type        | DocumentOwner            | TotalRemaining |
      | <NewPOCode> | Draft | <User>    | <User>        | <CurrentDateTime> | <CurrentDateTime> | <TypeValue> | <DocumentOwnerNameValue> | 0.00           |
    And the Vendor Section of PurchaseOrder with code "<NewPOCode>" is updated as follows:
      | Vendor                | ReferencePO                   |
      | <VendorCodeNameValue> | <ReferencePurchaseOrderValue> |
    And the Company Section of PurchaseOrder with code "<NewPOCode>" is updated as follows:
      | Company                | BusinessUnit                |
      | <CompanyCodeNameValue> | <BusinessUnitCodeNameValue> |
    And the TaxesSummary section for PurchaseOrder with code "<NewPOCode>" is displayed empty to "<User>"
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | CurrentDateTime      | NewPOCode  | TypeValue  | BusinessUnitCodeValue | BusinessUnitCodeNameValue | DocumentOwnerIdValue | DocumentOwnerNameValue | VendorCodeValue | VendorCodeNameValue        | ReferencePurchaseOrderValue | CompanyCodeNameValue |
      | Amr.Khalil  | 01-Jan-2019 11:00 AM | 2019000001 | IMPORT_PO  | 0006                  | 0006 - Corrugated         | 37                   | Gehan Ahmed            | 000006          | 000006 - Vendor Corrugated |                             |                      |
      | Amr.Khalil  | 02-Jan-2018 11:00 AM | 2018001000 | IMPORT_PO  | 0006                  | 0006 - Corrugated         | 37                   | Gehan Ahmed            | 000006          | 000006 - Vendor Corrugated |                             |                      |
      | Amr.Khalil  | 01-Jan-2019 11:00 AM | 2019000001 | LOCAL_PO   | 0006                  | 0006 - Corrugated         | 37                   | Gehan Ahmed            | 000006          | 000006 - Vendor Corrugated |                             |                      |
      | Amr.Khalil  | 02-Jan-2018 11:00 AM | 2018001000 | LOCAL_PO   | 0006                  | 0006 - Corrugated         | 37                   | Gehan Ahmed            | 000006          | 000006 - Vendor Corrugated |                             |                      |
      | Gehan.Ahmed | 01-Jan-2020 11:00 AM | 2020000001 | SERVICE_PO | 0002                  | 0002 - Signmedia          | 37                   | Gehan Ahmed            | 000002          | 000002 - Zhejiang          | 2018000014                  | 0003 - HPS           |
