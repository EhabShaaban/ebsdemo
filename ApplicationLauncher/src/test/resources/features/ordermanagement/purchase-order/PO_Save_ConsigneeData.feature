Feature: Save and Validate data  data to Consignee section in PurchaseOrder

  Note: There is no scenarios for mandatory fields validation, becasue in the consignee section, all
  fields are optional in all states that that allow EditConsignee action

  Background:
    Given the following users and roles exist:
      | Name                     | Role                                                     |
      | Gehan.Ahmed              | PurchasingResponsible_Signmedia                          |
      | hr1                      | SuperUser                                                |
      | Shady.Abdelatif          | Accountant_Signmedia                                     |
      | Gehan.Ahmed.NoCompany    | PurchasingResponsible_Signmedia_CannotViewCompanyRole    |
      | Gehan.Ahmed.NoPlant      | PurchasingResponsible_Signmedia_CannotViewPlantRole      |
      | Gehan.Ahmed.NoStorehouse | PurchasingResponsible_Signmedia_CannotViewStorehouseRole |
    And the following roles and sub-roles exist:
      | Role                                                     | Subrole            |
      | PurchasingResponsible_Signmedia                          | POOwner_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | POOwner_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | POOwner_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | POOwner_Signmedia  |
      | PurchasingResponsible_Signmedia                          | CompanyViewer      |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | CompanyViewer      |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | CompanyViewer      |
      | PurchasingResponsible_Signmedia                          | PlantViewer        |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | PlantViewer        |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | PlantViewer        |
      | PurchasingResponsible_Signmedia                          | StorehouseViewer   |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | StorehouseViewer   |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | StorehouseViewer   |
      | Accountant_Signmedia                                     | POViewer_Signmedia |
      | SuperUser                                                | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                    | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdateConsignee | [purchaseUnitName='Signmedia'] |
      | CompanyViewer     | Company:ReadAll               |                                |
      | PlantViewer       | Plant:ReadAll                 |                                |
      | StorehouseViewer  | Storehouse:ReadAll            |                                |
      | SuperUserSubRole  | *:*                           |                                |

    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit | Consignee | Plant | Storehouse |
      | 2018000004 | Draft              | 0003           | _         | _     | _          |
      | 2018000021 | Draft              | 0002           | 0001      | 0001  | 0002       |
      | 2018000006 | OpenForUpdates     | 0002           | 0001      | 0001  | 0002       |
      | 2018000007 | WaitingApproval    | 0002           | _         | _     | _          |
      | 2018000005 | Approved           | 0002           | 0001      | 0001  | 0002       |
      | 2018000008 | Confirmed          | 0002           | 0001      | 0001  | 0002       |
      | 2018000009 | FinishedProduction | 0002           | 0001      | 0001  | 0002       |
      | 2018000010 | Shipped            | 0002           | 0001      | 0001  | 0002       |
      | 2018000011 | Arrived            | 0002           | 0001      | 0001  | 0002       |
      | 2018000001 | Cleared            | 0002           | 0001      | 0001  | 0002       |
      | 2018000002 | DeliveryComplete   | 0002           | _         | _     | _          |
      | 2018000012 | Cancelled          | 0002           | _         | _     | _          |

    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit | Consignee | Plant | Storehouse |
      | 2018000013 | Draft            | 0002           | 0001      | 0001  | 0002       |
      | 2018000014 | OpenForUpdates   | 0002           | 0001      | 0001  | 0002       |
      | 2018000015 | WaitingApproval  | 0002           | _         | _     | _          |
      | 2018000016 | Approved         | 0002           | 0001      | 0001  | 0002       |
      | 2018000017 | Confirmed        | 0002           | 0001      | 0001  | 0002       |
      | 2018000018 | Shipped          | 0002           | 0001      | 0001  | 0002       |
      | 2018000019 | DeliveryComplete | 0002           | _         | _     | _          |
      | 2018000020 | Cancelled        | 0002           | _         | _     | _          |
      | 2018000024 | Draft            | 0003           | _         | _     | _          |

    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following Plants exist:
      | Code | Name              |
      | 0002 | DigiPro Plant     |
      | 0001 | Madina Tech Plant |
    And the following Storehouses exist:
      | Code | Name                    |
      | 0004 | DigiPro Secondary Store |
      | 0001 | AlMadina Main Store     |
    And the following Plants are related to Company with code "0002":
      | Code |
      | 0002 |
    And the following Storehouses for Plant with code "0002" exist:
      | Code |
      | 0004 |
    And edit session is "30" minutes

  ####### Save Consignee section (Happy Path) ##############################################################

  Scenario Outline: (01) Save Consignee section within edit session (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then PurchaseOrder with Code "<POCode>" is updated as follows after save consignee:
      | LastUpdatedBy | LastUpdateDate     | Consignee | Plant | Storehouse |
      | Gehan.Ahmed   | 7-Jan-2019 9:30 am | 0002      | 0002  | 0004       |
    And the lock by "Gehan.Ahmed" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000006 |
      | 2018000005 |
      | 2018000008 |
      | 2018000009 |
      | 2018000010 |
      | 2018000011 |
      | 2018000001 |
      | 2018000013 |
      | 2018000014 |
      | 2018000016 |
      | 2018000017 |
      | 2018000018 |

  Scenario Outline: (01a) Save Consignee section within edit session with optional fields missing (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    Then PurchaseOrder with Code "<POCode>" is updated as follows after save consignee:
      | LastUpdatedBy | LastUpdateDate     | Consignee | Plant | Storehouse |
      | Gehan.Ahmed   | 7-Jan-2019 9:30 am |           |       |            |
    And the lock by "Gehan.Ahmed" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000006 |
      | 2018000005 |
      | 2018000008 |
      | 2018000009 |
      | 2018000010 |
      | 2018000011 |
      | 2018000013 |
      | 2018000014 |
      | 2018000016 |
      | 2018000017 |
      | 2018000018 |

  ####### Save Consignee section Incorrect Data Entry (Validation Failure) ##############################################################

  Scenario Outline: (02) Save Consignee section with Incorrect Data: Consignee doesn't exist (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse |
      | 9999      |       |            |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Consignee field "PO-msg-05" and sent to "Gehan.Ahmed"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (03) Save Consignee section with Incorrect Data: Plant doesn't exist or not related to selected consignee (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant       | Storehouse |
      | 0002      | <PlantCode> |            |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Plant field "PO-msg-10" and sent to "Gehan.Ahmed"
    Examples:
      | PlantCode | POCode     |
      | 9999      | 2018000021 |
      | 0001      | 2018000021 |
      | 9999      | 2018000013 |
      | 0001      | 2018000013 |

  Scenario Outline: (04) Save Consignee section with Incorrect Data: Storehouse doesn't exist or not related to selected Plant (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse       |
      | 0002      | 0002  | <StorehouseCode> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Storehouse field "PO-msg-11" and sent to "Gehan.Ahmed"
    Examples:
      | StorehouseCode | POCode     |
      | 9999           | 2018000021 |
      | 0001           | 2018000021 |
      | 9999           | 2018000013 |
      | 0001           | 2018000013 |

  ######## Save Consignee section Malicious Data Entry (Abuse Case)##############################################################

  Scenario Outline: (05) Save Consignee section with Malicious Data: Consignee is string value (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse |
      | Ay7aga    |       |            |
    Then the lock by "Gehan.Ahmed" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (06) Save Consignee section with Malicious Data: Plant is string (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant  | Storehouse |
      | 0002      | Ay7aga |            |
    Then the lock by "Gehan.Ahmed" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (07) Save Consignee section with Malicious Data: Storehouse is string (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 am" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | Ay7aga     |
    Then the lock by "Gehan.Ahmed" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  ###### Save Consignee section (After edit session expires) ##############################################################

  Scenario Outline: (08) Save Consignee section after edit session expired for both local & import regardless data entry is correct or not (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:41 am" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  @ResetData
  Scenario Outline:(09) Save Consignee section after edit session expired & PO is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 am"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 am"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 am" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  ####### Save Consignee section (Unauthorized Access) ##############################################################

  Scenario Outline: (10) Save Consignee section by an unauthorized users (Authorization\Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves Consignee section of PurchaseOrder with Code "<POCode>" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  Scenario Outline: (11) Save Consignee section by an unauthorized user due unmatched condition (Authorization\Abuse Case becoz user has no way to enter edit mode)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves Consignee section of PurchaseOrder with Code "<POCode>" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000004 |
      | 2018000024 |

  Scenario Outline: (12) Save Consignee section by a user who is not authorized to read one of data entry (Abuse Case\Clientbypassing)
    Given user is logged in as "<User>"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 9:10 AM"
    When "<User>" saves Consignee section of PurchaseOrder with Code "<POCode>" with the following values:
      | Consignee | Plant | Storehouse |
      | 0002      | 0002  | 0004       |
    Then the lock by "<User>" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                     | POCode     |
      | Gehan.Ahmed.NoCompany    | 2018000013 |
      | Gehan.Ahmed.NoPlant      | 2018000013 |
      | Gehan.Ahmed.NoStorehouse | 2018000013 |
      | Gehan.Ahmed.NoCompany    | 2018000021 |
      | Gehan.Ahmed.NoPlant      | 2018000021 |
      | Gehan.Ahmed.NoStorehouse | 2018000021 |