# Author: Shrouk Alaa (16-Jan-2019 12:31 PM)
# Reviewer: Somaya Ahmed and Zyad Ghorab (21-Jan-2019 09:30 AM)

Feature: Mark PO as Arrived to discharge port - for Import POs only

  Background:
    Given the following users and roles exist:
      | Name              | Role                            |
      | Lama.Maher        | LogisticsResponsible_Signmedia  |
      | Hosam.Hosni.Test  | LogisticsResponsible_Flexo      |
      | Hosam.Hosni.Test  | LogisticsResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia           |
      | hr1               | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                     |
      | LogisticsResponsible_Signmedia  | POLogisticsOwner_Signmedia  |
      | LogisticsResponsible_Flexo      | POLogisticsOwner_Flexo      |
      | LogisticsResponsible_Corrugated | POLogisticsOwner_Corrugated |
      | SuperUser                       | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                  | Condition                       |
      | POLogisticsOwner_Signmedia  | PurchaseOrder:MarkAsArrived | [purchaseUnitName='Signmedia']  |
      | POLogisticsOwner_Flexo      | PurchaseOrder:MarkAsArrived | [purchaseUnitName='Flexo']      |
      | POLogisticsOwner_Corrugated | PurchaseOrder:MarkAsArrived | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole            | *:*                         |                                 |

    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000042 | IMPORT_PO | Shipped            | 0001           |
      | 2018000043 | IMPORT_PO | Shipped            | 0006           |

    And the following PurchaseOrders exist with shipping date:
      | Code       | shippingDate         |
      | 2018000010 | 07-Aug-2018 09:02 AM |
      | 2018000042 | 07-Aug-2018 09:02 AM |
      | 2018000043 | 07-Aug-2018 09:02 AM |

    ###### Happy Paths

     #EBS-1392
  @ResetData
  Scenario Outline: (01) MarkAsArrived PO Shipped Import PO with all mandatory fields for Arrived state - by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "<POCode>" is updated with ArrivalDate as follows:
      | LastUpdatedBy | LastUpdateDate             | State   | ArrivalDate                |
      | <User>        | 07-Jan-2019 07:30 AM +0000 | Arrived | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-38"
    Examples:
      | POCode     | Type      | State   | PurchasingUnit | User             |
      | 2018000010 | IMPORT_PO | Shipped | 0002           | Lama.Maher       |
      | 2018000042 | IMPORT_PO | Shipped | 0001           | Hosam.Hosni.Test |
      | 2018000043 | IMPORT_PO | Shipped | 0006           | Hosam.Hosni.Test |

     #Exceptions

     #EBS-1392
  Scenario Outline: (02) MarkAsArrived PO Import PO in a state that doesn't allow this action - any state except the Shipped / Draft state (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-32"
    Examples:Lama.Maher
      | POCode     | State              | PurchasingUnit | User       |
      | 2018000005 | Approved           | 0002           | Lama.Maher |
      | 2018000006 | OpenForUpdates     | 0002           | Lama.Maher |
      | 2018000007 | WaitingApproval    | 0002           | Lama.Maher |
      | 2018000008 | Confirmed          | 0002           | Lama.Maher |
      | 2018000009 | FinishedProduction | 0002           | Lama.Maher |
      | 2018000011 | Arrived            | 0002           | Lama.Maher |
      | 2018000001 | Cleared            | 0002           | Lama.Maher |
      | 2018000002 | DeliveryComplete   | 0002           | Lama.Maher |
      | 2018000012 | Cancelled          | 0002           | Lama.Maher |

     #EBS-1392
  Scenario Outline: (03) MarkAsArrived PO Import PO in Draft state (Abuse Case -- becoz an shipped PO cannot return to be draft by any means)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |


     #EBS-1392
  Scenario Outline: (04) MarkAsArrived PO Shipped Import PO that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Lama.Maher"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "2018000010" in edit mode at "07-Jan-2019 9:20 AM"
    When "Lama.Maher" requests to MarkAsArrived the PurchaseOrder with Code "2018000010" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Lama.Maher" with the following message "Gen-msg-14"
    Examples:
      | POSection |
      | Company   |

     #EBS-1392
  Scenario: (05) MarkAsArrived PO Shipped Import PO that is doesn't exist (Abuse Case -- cannot happen normally because an Shipped PO cannot be deleted)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsArrived the PurchaseOrder with Code "9999999999" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page

     #EBS-1392
  Scenario Outline: (06) MarkAsArrived PO Shipped Import PO with missing or malicious Arrival date (Abuse case -- because user cannot send the request unless the arrival date is entered correctly)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate   |
      | <ArrivalDate> |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page
    Examples:
      | ArrivalDate | POCode     |
      |             | 2018000010 |
      | Ay 7aga     | 2018000010 |

     #EBS-1392
  Scenario Outline: (07) MarkAsArrived PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate          |
      | 06-Jan-2019 11:30 AM |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000010 | Mahmoud.Abdelaziz |
      | 2018000010 | Hosam.Hosni.Test  |


    ######################################################## MarkAsShipped PO with shipping date before Production date ###############################
  Scenario Outline: (08) MarkAsArrived PO Shipped Import PO with all mandatory fields for Arrived state with Arrival date before shipping date (validation error)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsArrived the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ArrivalDate   |
      | <ArrivalDate> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to Arrival date field "PO-msg-59" and sent to "<User>"

    Examples:
      | POCode     | Type      | State   | PurchasingUnit | User             | ArrivalDate          |
      | 2018000010 | IMPORT_PO | Shipped | 0002           | Lama.Maher       | 06-Jan-2017 11:30 AM |
      | 2018000042 | IMPORT_PO | Shipped | 0001           | Hosam.Hosni.Test | 10-Aug-2018 09:00 AM |
      | 2018000043 | IMPORT_PO | Shipped | 0006           | Hosam.Hosni.Test | 10-Aug-2018 08:58 AM |