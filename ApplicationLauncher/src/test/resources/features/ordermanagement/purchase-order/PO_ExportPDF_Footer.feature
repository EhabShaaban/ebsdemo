#Auther: Nancy and Hend  (18-Feb-2019)

Feature: PO_Export_PDF_Footer

  Background:
    Given the following users and roles exist:
      | Name       | Role                             |
      | Amr.Khalil | PurchasingResponsible_Flexo      |
      | Amr.Khalil | PurchasingResponsible_Corrugated |

    And the following roles and sub-roles exist:
      | Role                             | Sub-role           |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | POOwner_Flexo      | PurchaseOrder:ExportPDF | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:ExportPDF | [purchaseUnitName='Corrugated'] |

    And the following Import PurchaseOrders exist:
      | Code       | Type   | State | PurchasingUnit | ShippingInstructions | PurchasingResponsible |
      | 2018000025 | Import | Draft | 0001           | 0002                 | 0002                  |
      | 2018000026 | Import | Draft | 0006           | 0002                 | 0002                  |

    And the following Local PurchaseOrders exist:
      | Code       | Type  | State | PurchasingUnit | ShippingInstructions | PurchasingResponsible |
      | 2018000027 | Local | Draft | 0001           | 0002                 | 0002                  |
      | 2018000028 | Local | Draft | 0006           | 0002                 | 0002                  |
    And the following Purchasing Responsibles exist:
      | Code | Name        |
      | 0002 | Gehan.Ahmed |

    And the following ShippingInstructions exist:
      | Code | Name         | ShippingInstructions                                                                                                                                                                                                                                                                                                           |
      | 0002 | Refregirated | To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world. |

    And the following DocumentTypes exist:
      | Code | Name                                                |
      | 0001 | Performa Invoice                                    |
      | 0002 | Commercial Invoice                                  |
      | 0003 | Commercial Invoice Legalized by Chamber of Commerce |
      | 0004 | Bill of Lading (BL)                                 |
      | 0005 | Packing List for the Whole Order                    |

    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000025":
      | DocumentType | Copies |
      | 0001         | 2      |
      | 0002         | 4      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000026":
      | DocumentType | Copies |
      | 0003         | 1      |
      | 0004         | 2      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000027":
      | DocumentType | Copies |
      | 0001         | 3      |
      | 0003         | 2      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000028":
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |

  Scenario: (03) Request to export PurchaseOrder footer by an authorized user who has one role - Import PO - Flexo  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder footer of PurchaseOrder with code "2018000025" as PDF
    Then the following values of PurchaseOrder footer are displayed to "Amr.Khalil":
      | PurchasingResponsible | ShippingInstructions                                                                                                                                                                                                                                                                                                           |
      | Gehan Ahmed           | To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world. |
    And the following values of RequiredDocuments are displayed to "Amr.Khalil":
      | Name               | Copies |
      | Performa Invoice   | 2      |
      | Commercial Invoice | 4      |

  Scenario: (04) Request to export PurchaseOrder footer by an authorized user who has one role - Local PO - Flexo  (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder footer of PurchaseOrder with code "2018000027" as PDF
    Then the following values of PurchaseOrder footer are displayed to "Amr.Khalil":
      | PurchasingResponsible | ShippingInstructions                                                                                                                                                                                                                                                                                                           |
      | Gehan Ahmed           | To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world. |
    And the following values of RequiredDocuments are displayed to "Amr.Khalil":
      | Name                                                | Copies |
      | Performa Invoice                                    | 3      |
      | Commercial Invoice Legalized by Chamber of Commerce | 2      |

  Scenario: (05) Request to export PurchaseOrder footer by an authorized user who has one role - Import PO -  Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder footer of PurchaseOrder with code "2018000026" as PDF
    Then the following values of PurchaseOrder footer are displayed to "Amr.Khalil":
      | PurchasingResponsible | ShippingInstructions                                                                                                                                                                                                                                                                                                           |
      | Gehan Ahmed           | To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world. |
    And the following values of RequiredDocuments are displayed to "Amr.Khalil":
      | Name                                                | Copies |
      | Commercial Invoice Legalized by Chamber of Commerce | 1      |
      | Bill of Lading (BL)                                 | 2      |

  Scenario: (06) Request to export PurchaseOrder footer by an authorized user who has one role - Local PO -  Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder footer of PurchaseOrder with code "2018000028" as PDF
    Then the following values of PurchaseOrder footer are displayed to "Amr.Khalil":
      | PurchasingResponsible | ShippingInstructions                                                                                                                                                                                                                                                                                                           |
      | Gehan Ahmed           | To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world. |
    And the following values of RequiredDocuments are displayed to "Amr.Khalil":
      | Name                             | Copies |
      | Bill of Lading (BL)              | 4      |
      | Packing List for the Whole Order | 5      |
