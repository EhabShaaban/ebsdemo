# Author: Zyad Ghorab (03-July-2019)

Feature: ApprovalCycles Assign_Me_To_Approve

  Background:
    Given the following users and roles exist:
      | Name                | Role                             |
      | Ashraf.Salah        | AccountantHead_Signmedia         |
      | Ahmed.Seif          | Quality_Specialist               |
      | Gehan.Ahmed         | PurchasingResponsible_Signmedia  |
      | Mohamed.Abdelmoniem | BudgetController_Signmedia       |
      | Mahmoud.Abdelaziz   | Storekeeper_Signmedia            |
      | Amr.Khalil          | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                            | Subrole              |
      | AccountantHead_Signmedia        | POApprover_Signmedia |
      | Quality_Specialist              | POApprover_Signmedia |
      | PurchasingResponsible_Signmedia | POApprover_Signmedia |
      | BudgetController_Signmedia      | POApprover_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission                    | Condition                      |
      | POApprover_Signmedia | PurchaseOrder:Approve         | [purchaseUnitName='Signmedia'] |
      | POApprover_Signmedia | PurchaseOrder:ReplaceApprover | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000013 | Draft            | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
    And the following user information exist:
      | UserName            | ProfileName         |
      | Ashraf.Salah        | Ashraf Salah        |
      | Mohamed.Abdelmoniem | Mohamed Abdelmoniem |
      | Essam.Mahmoud       | Essam Mahmoud       |
      | Gehan.Ahmed         | Gehan Ahmed         |
    And the following control point users and alternative users exist:
      | ApprovalPolicy | ControlPoint | User                | Alternative  |
      | 0001           | 0001         | Ashraf.Salah        | Ahmed.Seif   |
      | 0001           | 0002         | Mohamed.Abdelmoniem | Ashraf.Salah |
      | 0001           | 0003         | Essam.Mahmoud       | Gehan.Ahmed  |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000007" has the following values:
      | ControlPoint | User          | DateTime             | Decision | Notes |
      | 0001         | Ashraf.Salah  | 21-Sep-2018 09:15 AM | Approved |       |
      | 0002         | Ashraf.Salah  |                      |          |       |
      | 0003         | Essam.Mahmoud |                      |          |       |

  @ResetData
  Scenario: (01) I want to assign myself (main approve) to approve Import PO (Happy Path)
    Given user is logged in as "Mohamed.Abdelmoniem"
    When "Mohamed.Abdelmoniem" requests to assign himself to Approve the PurchaseOrder with Code "2018000007" instead of "Ashraf.Salah" for control point with code "0002" at "07-Jan-2019 11:00 AM"
    Then PurchaseOrder with Code "2018000007" is updated as follows:
      | LastUpdatedBy       | LastUpdateDate       | State           |
      | Mohamed.Abdelmoniem | 07-Jan-2019 11:00 AM | WaitingApproval |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000007" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0001         | Ashraf.Salah        | 21-Sep-2018 09:15 AM | Approved |       |
      | 0002         | Mohamed.Abdelmoniem |                      |          |       |
      | 0003         | Essam.Mahmoud       |                      |          |       |
    And a success notification is sent to "Mohamed.Abdelmoniem" with the following message "PO-msg-63"

  @ResetData
  Scenario: (02) I want to assign myself (alternative approve) to approve Import PO (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to assign himself to Approve the PurchaseOrder with Code "2018000007" instead of "Essam.Mahmoud" for control point with code "0003" at "07-Jan-2019 11:00 AM"
    Then PurchaseOrder with Code "2018000007" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State           |
      | Gehan.Ahmed   | 07-Jan-2019 11:00 AM | WaitingApproval |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000007" has the following values:
      | ControlPoint | User         | DateTime             | Decision | Notes |
      | 0001         | Ashraf.Salah | 21-Sep-2018 09:15 AM | Approved |       |
      | 0002         | Ashraf.Salah |                      |          |       |
      | 0003         | Gehan.Ahmed  |                      |          |       |
    And a success notification is sent to "Gehan.Ahmed" with the following message "PO-msg-63"

  Scenario Outline: (03) I want to assign myself to approve Import / Local PO in state that does't allow this action (Exception)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to assign himself to Approve the PurchaseOrder with Code "<POCode>"
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      # Local
      | 2018000014 | OpenForUpdates     | 0002           |
      | 2018000016 | Approved           | 0002           |
      | 2018000017 | Confirmed          | 0002           |
      | 2018000018 | Shipped            | 0002           |
      | 2018000019 | DeliveryComplete   | 0002           |
      | 2018000020 | Cancelled          | 0002           |

  Scenario Outline: (04) I want to assign myself to approve PO while I'm not the main or the substitute user of the current approver (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to assign himself to Approve the PurchaseOrder with Code "<POCode>" instead of "<Approver>" for control point with code "<ControlPoint>"
    Then an error notification is sent to "<User>" with the following message "PO-msg-61"
    Examples:
      | POCode     | Approver     | User          | ControlPoint |
      | 2018000007 | Ashraf.Salah | Gehan.Ahmed   | 0002         |
      | 2018000007 | Ashraf.Salah | Essam.Mahmoud | 0002         |

  Scenario: (05) I want to assign myself to approve PO while the main approver/other substitute has already approve/reject the po (Exception)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to assign himself to Approve the PurchaseOrder with Code "2018000007" instead of "Ashraf.Salah" for control point with code "0001"
    Then an error notification is sent to "Ahmed.Seif" with the following message "PO-msg-60"

  Scenario: (06) I want to assign myself to approve PO while I'm already assigned (Exception)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to assign himself to Approve the PurchaseOrder with Code "2018000007" instead of "Ashraf.Salah" for control point with code "0002"
    Then an error notification is sent to "Ashraf.Salah" with the following message "PO-msg-62"

  Scenario Outline: (07) I want to assign myself to approve PO while I'm not authorized to , due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to assign himself to Approve the PurchaseOrder with Code "<POCode>" instead of "<Approver>" for control point with code "<ControlPoint>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | Approver     | User              | ControlPoint |
      | 2018000007 | Ashraf.Salah | Amr.Khalil        | 0002         |
      | 2018000007 | Ashraf.Salah | Mahmoud.Abdelaziz | 0002         |

  Scenario: (08) I want to assign myself to approve PO while PO in draft state (Abuse)
    Given user is logged in as "Mohamed.Abdelmoniem"
    When "Mohamed.Abdelmoniem" requests to assign himself to Approve the PurchaseOrder with Code "2018000021"
    Then "Mohamed.Abdelmoniem" is logged out
    And "Mohamed.Abdelmoniem" is forwarded to the error page

