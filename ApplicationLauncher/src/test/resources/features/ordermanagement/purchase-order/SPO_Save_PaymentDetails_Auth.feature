# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Save PaymentDetails section in Service PurchaseOrder - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                 | Role                                                               |
      | Gehan.Ahmed                          | PurchasingResponsible_Signmedia                                    |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | PurchasingResponsible_Signmedia_CannotReadPaymentTermsCurrencyRole |
    And the following roles and sub-roles exist:
      | Role                                                               | Sub-role          |
      | PurchasingResponsible_Signmedia                                    | POOwner_Signmedia |
      | PurchasingResponsible_Signmedia                                    | PaymentTermViewer |
      | PurchasingResponsible_Signmedia                                    | CurrencyViewer    |
      | PurchasingResponsible_Signmedia_CannotReadPaymentTermsCurrencyRole | POOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                       | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
      | PaymentTermViewer | PaymentTerms:ReadAll             |                                |
      | CurrencyViewer    | Currency:ReadAll                 |                                |
    And the following users doesn't have the following permissions:
      | User                                 | Permission           |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | PaymentTerms:ReadAll |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | Currency:ReadAll     |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2019 9:02 AM | Amr Khalil    |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000010 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2018000010 | 000001 - Siegwerk |             |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000005 | 000001 - Zhejiang | 2018000010  |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |

  #EBS-7337
  Scenario Outline: (01) Save SPO PaymentDetails section, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves PaymentDetails section of Service PurchaseOrder with Code "<Code>" at "07-Jan-2019 09:30 AM" with the following values:
      | Currency   | PaymentTerms                               |
      | 0001 - EGP | 0001 - 20% Deposit, 80% T/T Before Loading |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | Code       | User                                 |
      | 2020000005 | Gehan.Ahmed                          |
      | 2020000004 | Gehan.Ahmed.NoPaymentTermsNoCurrency |

