# Author: Aya Sadek (16-Jan-2019 02:31 PM)
# updated by: Fatma Al Zahraa (EBS - 8346)
# Reviewer: Niveen Magdy, Somaya Ahmed (21-Jan-2019 11:30 AM)

Feature: Mark PO as Cleared - for Import POs only

  Background:
    Given the following users and roles exist:
      | Name              | Role                           |
      | Lama.Maher        | LogisticsResponsible_Signmedia |
      | Hosam.Hosni.Test  | LogisticsResponsible_Flexo     |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia          |
      | hr1               | SuperUser                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                    |
      | LogisticsResponsible_Signmedia | POLogisticsOwner_Signmedia |
      | LogisticsResponsible_Flexo     | POLogisticsOwner_Flexo     |
      | SuperUser                      | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                  | Condition                      |
      | POLogisticsOwner_Signmedia | PurchaseOrder:MarkAsCleared | [purchaseUnitName='Signmedia'] |
      | POLogisticsOwner_Flexo     | PurchaseOrder:MarkAsCleared | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole           | *:*                         |                                |
    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000050 | IMPORT_PO | Arrived            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
    And the following PurchaseOrders exist with arrival date:
      | Code       | ArrivalDate          |
      | 2018000050 | 08-Aug-2018 09:00 AM |
      | 2018000011 | 07-Aug-2018 09:02 AM |

  ###### Happy Paths
  # EBS-1478
  @ResetData
  Scenario: (01) Mark PO as Cleared (Import) in Arrived state with all mandatory fields - by an authorized user (Happy Path)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "2018000050" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "2018000050" is updated as Cleared as follows:
      | LastUpdatedBy | LastUpdateDate             | State   | ClearanceDate              |
      | Lama.Maher    | 07-Jan-2019 07:30 AM +0000 | Cleared | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "Lama.Maher" with the following message "PO-msg-39"

  ###### Exceptions
  #EBS - 8346
  Scenario Outline: (02) Mark PO as Cleared (Import) in Arrived state with missing mandatory fields (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsCleared the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-33"
    And the following sections with missing fields are sent to "<User>":
      | SectionName   | MissingFields                            |
      | ConsigneeData | consigneeCode, plantCode, storehouseCode |
    Examples:
      | POCode     | State   | PurchasingUnit | User       |
      | 2018000011 | Arrived | 0002           | Lama.Maher |

  # EBS-1478
  Scenario Outline: (03) Mark PO as Cleared (Import) in a state that doesn't allow this action - any state except the Arrived / Draft state (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsCleared the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit | User       |
      | 2018000005 | Approved           | 0002           | Lama.Maher |
      | 2018000006 | OpenForUpdates     | 0002           | Lama.Maher |
      | 2018000007 | WaitingApproval    | 0002           | Lama.Maher |
      | 2018000008 | Confirmed          | 0002           | Lama.Maher |
      | 2018000009 | FinishedProduction | 0002           | Lama.Maher |
      | 2018000010 | Shipped            | 0002           | Lama.Maher |
      | 2018000001 | Cleared            | 0002           | Lama.Maher |
      | 2018000002 | DeliveryComplete   | 0002           | Lama.Maher |
      | 2018000012 | Cancelled          | 0002           | Lama.Maher |

  # EBS-1478
  Scenario Outline: (04) Mark PO as Cleared (Import) in Draft state (Abuse Case -- because the Arrived PO cannot return to be draft by any means)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |

  # EBS-1478
  Scenario Outline: (05) Mark PO as Cleared (Import) in Arrived state that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Lama.Maher"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "2018000011" in edit mode at "07-Jan-2019 09:20 AM"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "2018000011" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Lama.Maher" with the following message "Gen-msg-14"
    Examples:
      | POSection |
      | Company   |

  # EBS-1478
  Scenario: (06) Mark PO as Cleared (Import) in Arrived state that is doesn't exist (Abuse Case -- cannot happen normally because the Arrived PO cannot be deleted)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "9999999999" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page

  # EBS-1478
  Scenario Outline: (07) Mark PO as Cleared (Import) in Arrived state with missing or malicious Clearance date (Abuse case -- because user cannot send the request unless the clearance date is entered correctly)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate    |
      | <ClearanceDate > |
    Then "Lama.Maher" is logged out
    And "Lama.Maher" is forwarded to the error page
    Examples:
      | ClearanceDate | POCode     |
      |               | 2018000011 |
      | Ay 7aga       | 2018000011 |

  # EBS-1478
  Scenario Outline: (08) Mark PO as Cleared by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsCleared the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate        |
      | 06-Jan-2019 11:30 AM |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000011 | Mahmoud.Abdelaziz |
      | 2018000011 | Hosam.Hosni.Test  |
            
  ######################################################## MarkAsShipped PO with shipping date before Production date ###############################

  Scenario Outline: (09) Mark PO as Cleared (Import) in Arrived state with clearance date before arrival date (validation error)
    Given user is logged in as "Lama.Maher"
    When "Lama.Maher" requests to MarkAsCleared the PurchaseOrder with Code "2018000011" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ClearanceDate   |
      | <ClearenceDate> |
    Then a failure notification is sent to "Lama.Maher" with the following message "Gen-msg-05"
    And the following error message is attached to Clearance date field "PO-msg-59" and sent to "Lama.Maher"
    Examples:
      | ClearenceDate        |
      | 06-Jan-2017 11:30 AM |
      | 07-Aug-2018 09:02 AM |
      | 07-Aug-2018 09:00 AM |