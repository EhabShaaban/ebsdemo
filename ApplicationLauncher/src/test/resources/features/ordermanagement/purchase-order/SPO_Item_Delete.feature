# Author HP Dev: Fatma Al Zahraa
# Author Val & Auth Dev: Zyad Ghorab
# Author Quality: Shirin Mahmoud

Feature: Delete SPOItem From PurchaseOrderItems Section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole  |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission               | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole  | *:*                      |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                  |
      | Gehan.Ahmed | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000040 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000055 | IMPORT_PO  | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company | BankAccountNumber |
      | 2018000040 | 0001 - Flexo     |         |                   |
      | 2018000055 | 0002 - Signmedia |         |                   |
      | 2020000004 | 0002 - Signmedia |         |                   |
      | 2020000005 | 0001 - Flexo     |         |                   |
      | 2020000006 | 0002 - Signmedia |         |                   |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000055  |
      | 2020000005 | 000001 - Siegwerk | 2018000040  |
      | 2020000006 | 000002 - Zhejiang | 2018000055  |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000057 - Bank Fees          | 0019 - M2 | 16.000   | 200.000 | 3200.000        |
      | 2020000004 | 2        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
      | 2020000005 | 3        | 000056 - Shipment service   | 0019 - M2 | 20.000   |         |                 |
      | 2020000006 | 4        | 000057 - Bank Fees          | 0019 - M2 | 20.000   |         |                 |

  # EBS-7335
  Scenario: (01) Delete SPOItem from PurchaseOrder, where PurchaseOrder is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete SPOItem with id 1 in PurchaseOrder with code "2020000004"
    Then SPOItem with id 1 from PurchaseOrder with code "2020000004" is deleted
    And Only the following Items remaining in PurchaseOrder with code "2020000004":
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 2        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-12"

  # EBS-7561
  Scenario: (02) Delete SPOItem from PurchaseOrder, where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted SPOItem with id 1 of PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to delete SPOItem with id 1 in PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"

  # EBS-7561
  Scenario: (03) Delete SPOItem from PurchaseOrder, where action is not allowed in current state: Confirmed (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to delete SPOItem with id 1 in PurchaseOrder with code "2020000006"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-21"

  # EBS-7561
  Scenario: (04) Delete SPOItem from PurchaseOrder, where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to delete SPOItem with id 1 in PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-7561
  Scenario: (05) Delete SPOItem from PurchaseOrder, when PurchaseOrderItems section are locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened Items section of PurchaseOrder with code "2020000004" in edit mode
    When "Gehan.Ahmed" requests to delete SPOItem with id 1 in PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  # EBS-7561
  Scenario Outline: (06) Delete SPOItem from PurchaseOrder, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete SPOItem with id <ItemId> in PurchaseOrder with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | Code       | ItemId |
      | Gehan.Ahmed | 2020000005 | 3      |
