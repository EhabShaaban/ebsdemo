# Author: Marina (30-Jan-2019 10:00 AM)
# Reviewer: Somaya (03-Feb-2019 2:45 PM)

Feature: View ItemsWithoutPrices section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name              | Role                            |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia           |
      | Ali.Hasan         | Storekeeper_Flexo               |
      | Ali.Hasan         | Storekeeper_Corrugated          |
      | hr1               | SuperUser                       |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                   | Subrole                    |
      | Storekeeper_Flexo      | POViewerLimited_Flexo      |
      | Storekeeper_Signmedia  | POViewerLimited_Signmedia  |
      | Storekeeper_Corrugated | POViewerLimited_Corrugated |
      | SuperUser              | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                           | Condition                       |
      | POViewerLimited_Signmedia  | PurchaseOrder:ReadItemsWithoutPrices | [purchaseUnitName='Signmedia']  |
      | POViewerLimited_Flexo      | PurchaseOrder:ReadItemsWithoutPrices | [purchaseUnitName='Flexo']      |
      | POViewerLimited_Corrugated | PurchaseOrder:ReadItemsWithoutPrices | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole           | *:*                                  |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State   | PurchasingUnit |
      | 2018000022 | Shipped | 0002           |
      | 2018000025 | Draft   | 0001           |
      | 2018000026 | Draft   | 0006           |
      | 2018100004 | Draft   | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
      | 2018000027 | Draft | 0001           |
      | 2018000028 | Draft | 0006           |
      | 2018100006 | Draft | 0002           |
    And the following PurchaseOrders has an empty OrderItems section:
      | Code       |
      | 2018100004 |
      | 2018100006 |

    And PurchaseOrder with code "2018000022" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000022" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |

    And PurchaseOrder with code "2018000013" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | 0019 |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 3.20x50 | 3.0              | 0.0%     |
    And Item with code "000002" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |

    And PurchaseOrder with code "2018000025" has the following OrderItems:
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | 0019 |
    And Item with code "000005" PurchaseOrder with code "2018000025" has empty Quantities

    And PurchaseOrder with code "2018000026" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000026" has empty Quantities

    And PurchaseOrder with code "2018000027" has the following OrderItems:
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | 0019 |
    And Item with code "000006" PurchaseOrder with code "2018000027" has empty Quantities

    And PurchaseOrder with code "2018000028" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000028" has empty Quantities

    And the following Measures exist:
      | Code | Type     | Symbol       | Name         |
      | 0003 | Standard | Kg           | Kilogram     |
      | 0019 | Standard | M2           | Square Meter |
      | 0014 | Standard | Liter        | Liter        |
      | 0029 | Business | Roll 2.20x50 | Roll 2.20x50 |
      | 0033 | Business | Roll 2.70x50 | Roll 2.70x50 |
      | 0032 | Business | Roll 3.20x50 | Roll 3.20x50 |

  #### Happy Path  ####
  # EBS-1658
  Scenario: (01) View OrderItems WithoutPrices, by an authorized user who has one role - Import PO - Signmedia (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000022"
    Then the following OrderItemsWithoutPrices are displayed to "Mahmoud.Abdelaziz":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | M2   |
    And the following Quantities list for Item "000002" in PurchaseOrder with code "2018000022" is displayed to "Mahmoud.Abdelaziz":
      | Qty | OrderUnit    |
      | 100 | Roll 2.20x50 |
      | 50  | Roll 2.70x50 |

  # EBS-1658
  Scenario: (02) View OrderItems WithoutPrices, by an authorized user who has one role - Local PO - Signmedia (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000013"
    Then the following OrderItemsWithoutPrices are displayed to "Mahmoud.Abdelaziz":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | M2   |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | M2   |
    And the following Quantities list for Item "000001" in PurchaseOrder with code "2018000013" is displayed to "Mahmoud.Abdelaziz":
      | Qty | OrderUnit    |
      | 100 | Roll 2.20x50 |
      | 50  | Roll 3.20x50 |
    And the following Quantities list for Item "000002" in PurchaseOrder with code "2018000013" is displayed to "Mahmoud.Abdelaziz":
      | Qty | OrderUnit    |
      | 100 | Roll 2.20x50 |
      | 50  | Roll 2.70x50 |

  # EBS-1658
  Scenario: (03) View OrderItems WithoutPrices, by an authorized user who has one role - Import PO - Flexo (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000025"
    Then the following OrderItemsWithoutPrices are displayed to "Ali.Hasan":
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | M2   |
    And an empty Quantities list for Item "000005" in PurchaseOrder with code "2018000025" is displayed to "Ali.Hasan"

  # EBS-1658
  Scenario: (04) View OrderItems WithoutPrices, by an authorized user who has one role - Local PO - Flexo (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000026"
    Then the following OrderItemsWithoutPrices are displayed to "Ali.Hasan":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" in PurchaseOrder with code "2018000026" is displayed to "Ali.Hasan"

  # EBS-1658
  Scenario: (05) View OrderItems WithoutPrices, by an authorized user who has one role - Import PO - Corrugated (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000027"
    Then the following OrderItemsWithoutPrices are displayed to "Ali.Hasan":
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | M2   |
    And an empty Quantities list for Item "000006" in PurchaseOrder with code "2018000027" is displayed to "Ali.Hasan"

  # EBS-1658
  Scenario: (06) View OrderItems WithoutPrices, by an authorized user who has one role - Local PO - Corrugated (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "2018000028"
    Then the following OrderItemsWithoutPrices are displayed to "Ali.Hasan":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" in PurchaseOrder with code "2018000028" is displayed to "Ali.Hasan"

  # EBS-1658
  Scenario Outline: (07) View OrderItems WithoutPrices, by an authorized user - Local/Import - Empty Order Items (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "<POCode>"
    Then an empty OrderItemsWithoutPrices section in PurchaseOrder with code "<POCode>" is displayed to "Mahmoud.Abdelaziz"
    Examples:
      | POCode     |
      | 2018100004 |
      | 2018100006 |

  ##### Exception Cases###
  # EBS-1658
  @ResetData
  Scenario Outline: (08) View OrderItems WithoutPrices, where PurchaseOrder has been deleted by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Mahmoud.Abdelaziz" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018100004 |

  ##### Abuse Cases#####
  # EBS-1658
  Scenario Outline: (09) View OrderItems WithoutPrices, by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      | POCode     |
      | Afaf      | 2018000022 |
      | Afaf      | 2018000013 |
      | Ali.Hasan | 2018000022 |
      | Ali.Hasan | 2018000013 |