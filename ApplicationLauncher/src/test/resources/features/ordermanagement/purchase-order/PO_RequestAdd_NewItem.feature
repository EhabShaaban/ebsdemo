# Authors: Sara Mosad and Marina Salah, 16-Dec-2018

Feature: Request Add/Cancel Item in PO

  Background:
    Given the following users and roles exist:
      | Name                | Role                                                |
      | Gehan.Ahmed         | PurchasingResponsible_Signmedia                     |
      | Amr.Khalil          | PurchasingResponsible_Flexo                         |
      | Amr.Khalil          | PurchasingResponsible_Corrugated                    |
      | Mahmoud.Abdelaziz   | Storekeeper_Signmedia                               |
      | hr1                 | SuperUser                                           |
      | Gehan.Ahmed.NoItems | PurchasingResponsible_Signmedia_CannotReadItemsRole |
    And the following roles and sub-roles exist:
      | Role                                                | Subrole            |
      | PurchasingResponsible_Signmedia                     | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo                         | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated                    | POOwner_Corrugated |
      | SuperUser                                           | SuperUserSubRole   |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole | POOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                      |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018100000 | Draft              | 0002           |
      | 2018100002 | Draft              | 0002           |
      | 2018100004 | Draft              | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018100001 | Draft            | 0002           |
      | 2018100003 | Draft            | 0002           |
      | 2018100005 | Draft            | 0002           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000001 | 000001   |         |
      | 2018000002 | 000003   |         |
      | 2018000005 | 000001   |         |
      | 2018000006 | 000002   |         |
      | 2018000006 | 000003   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000025 | 000005   |         |
      | 2018000026 | 000012   |         |
      | 2018100002 | 000001   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
      | 2018000014 | 000003   |         |
      | 2018000015 | 000001   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000027 | 000006   |         |
      | 2018000028 | 000012   |         |
      | 2018100003 | 000001   |         |
    And edit session is "30" minutes

    ################################################ Request to add Item scenarios ################################################

  Scenario Outline: (01) Request Add item in Local/Import PurchaseOrder, in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then a new add Item dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadItems       |
    Examples:
      | POCode     | State          | User        | Type   |
      | 2018000021 | Draft          | Gehan.Ahmed | Import |
      | 2018000006 | OpenForUpdates | Gehan.Ahmed | Import |
      | 2018000013 | Draft          | Gehan.Ahmed | Local  |
      | 2018000014 | OpenForUpdates | Gehan.Ahmed | Local  |
      | 2018000025 | Draft          | Amr.Khalil  | Import |
      | 2018000026 | Draft          | Amr.Khalil  | Import |
      | 2018000027 | Draft          | Amr.Khalil  | Local  |
      | 2018000028 | Draft          | Amr.Khalil  | Local  |

  Scenario Outline: (02) Request Add item in Local/Import PurchaseOrder, when OrderItems section is locked by another user - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to OrderItems section of PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  @ResetData
  Scenario Outline: (03) Request Add item in Local/Import PurchaseOrder, that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018100000 |
      | 2018100001 |

  Scenario Outline: (04) Request Add item in Local/Import PurchaseOrder, in a state that doen't allow this action - All states except Draft and OpenForUpdates - for both Import and Local POs  (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And OrderItems section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     | State              |
      | 2018000005 | Approved           |
      | 2018000007 | WaitingApproval    |
      | 2018000008 | Confirmed          |
      | 2018000009 | FinishedProduction |
      | 2018000010 | Shipped            |
      | 2018000011 | Arrived            |
      | 2018000001 | Cleared            |
      | 2018000002 | DeliveryComplete   |
      | 2018000012 | Cancelled          |
      | 2018000015 | WaitingApproval    |
      | 2018000016 | Approved           |
      | 2018000017 | Confirmed          |
      | 2018000018 | Shipped            |
      | 2018000019 | DeliveryComplete   |
      | 2018000020 | Cancelled          |

  Scenario Outline: (05) Request Add item in Local/Import PurchaseOrder, by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When  "Mahmoud.Abdelaziz" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (06) Request Add item in Local/Import PurchaseOrder, by an unauthorized user due to unmatched condition (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When  "Gehan.Ahmed" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000025 |
      | 2018000027 |

  Scenario Outline: (07) Request Add item in Local/Import PurchaseOrder, by a User with one or more roles - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then a new add Item dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    And there are no authorized reads returned to "<User>"
    Examples:
      | POCode     | User                |
      | 2018000006 | Gehan.Ahmed.NoItems |
      | 2018000013 | Gehan.Ahmed.NoItems |

    ######### Cancel saving OrderItems section ########################################################################

  Scenario Outline: (08) Request Cancel Add item in Local/Import PurchaseOrder, within the edit session in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And OrderItems section of PurchaseOrder with code "<POCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" requests to cancel add item to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    Examples:
      | POCode     | State          | User        | Type   |
      | 2018000021 | Draft          | Gehan.Ahmed | Import |
      | 2018000006 | OpenForUpdates | Gehan.Ahmed | Import |
      | 2018000013 | Draft          | Gehan.Ahmed | Local  |
      | 2018000014 | OpenForUpdates | Gehan.Ahmed | Local  |
      | 2018000025 | Draft          | Amr.Khalil  | Import |
      | 2018000026 | Draft          | Amr.Khalil  | Import |
      | 2018000027 | Draft          | Amr.Khalil  | Local  |
      | 2018000028 | Draft          | Amr.Khalil  | Local  |

  Scenario Outline: (09) Request Cancel Add item in Local/Import PurchaseOrder, after edit session expire (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" requests to cancel add item to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:45 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  @ResetData
  Scenario Outline:(10) Request Cancel Add item in Local/Import PurchaseOrder, that doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And OrderItems section of PurchaseOrder with code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:45 AM"
    When "Gehan.Ahmed" requests to cancel add item to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:46 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018100004 |
      | 2018100005 |

  Scenario Outline: (11) Request Cancel Add item in Local/Import PurchaseOrder, by an unauthorized users (Authorization\Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to cancel add item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  Scenario Outline: (12) Request Cancel Add item in Local/Import PurchaseOrder, by an authorized user due unmatched condition: (Authorization\Abuse Case becoz user has no way to enter edit mode)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel add item to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000004 |
      | 2018000024 |
