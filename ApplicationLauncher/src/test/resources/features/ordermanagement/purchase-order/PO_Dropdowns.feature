Feature: PR Purchase Order Read All

  # updated by: Fatma Al Zahraa (EBS - 7424)
  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission            | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            | Condition |
      | Afaf | PurchaseOrder:ReadAll |           |
      #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State              | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000001 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | DeliveryComplete   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000005 | IMPORT_PO  | Approved           | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Confirmed          | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000009 | IMPORT_PO  | FinishedProduction | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Arrived            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000016 | LOCAL_PO   | Approved           | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000017 | LOCAL_PO   | Confirmed          | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000018 | LOCAL_PO   | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000019 | SERVICE_PO | DeliveryComplete   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000022 | IMPORT_PO  | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000023 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000044 | SERVICE_PO | DeliveryComplete   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000046 | LOCAL_PO   | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000047 | LOCAL_PO   | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000048 | LOCAL_PO   | Shipped            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000049 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000050 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000051 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000045 | SERVICE_PO | Confirmed          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000046 | SERVICE_PO | Confirmed          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000047 | SERVICE_PO | Confirmed          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000048 | LOCAL_PO   | Shipped            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000013 | IMPORT_PO  | Draft              | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000055 | IMPORT_PO  | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000056 | LOCAL_PO   | Cleared            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
    #@INSERT
    Given the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000001 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000002 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000005 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000009 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000010 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000011 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000016 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000017 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000018 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000019 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000022 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000023 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000044 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000046 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000047 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000048 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000049 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000050 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000051 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000046 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000047 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2020000048 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000013 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000055 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000056 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000001 | 000002 - Zhejiang |             |
      | 2018000002 | 000002 - Zhejiang |             |
      | 2018000005 | 000002 - Zhejiang |             |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2018000009 | 000002 - Zhejiang |             |
      | 2018000010 | 000002 - Zhejiang |             |
      | 2018000011 | 000002 - Zhejiang |             |
      | 2018000016 | 000002 - Zhejiang |             |
      | 2018000017 | 000002 - Zhejiang |             |
      | 2018000018 | 000002 - Zhejiang |             |
      | 2018000019 | 000002 - Zhejiang |             |
      | 2018000022 | 000002 - Zhejiang |             |
      | 2018000023 | 000002 - Zhejiang |             |
      | 2018000044 | 000002 - Zhejiang |             |
      | 2018000046 | 000002 - Zhejiang |             |
      | 2018000047 | 000002 - Zhejiang |             |
      | 2018000048 | 000002 - Zhejiang |             |
      | 2018000049 | 000002 - Zhejiang |             |
      | 2018000050 | 000002 - Zhejiang |             |
      | 2018000051 | 000002 - Zhejiang |             |
      | 2020000045 | 000002 - Zhejiang |             |
      | 2020000046 | 000002 - Zhejiang |             |
      | 2020000047 | 000002 - Zhejiang |             |
      | 2020000048 | 000002 - Zhejiang |             |
      | 2018000013 | 000002 - Zhejiang |             |
      | 2018000055 | 000001 - Zhejiang |             |
      | 2018000056 | 000002 - Zhejiang |             |

  Scenario: (01) Read All PurchaseOrders for DueDocument Dropdown in PaymentRequest by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all PurchaseOrders from Approved to Delivery Complete And not Cancelled for Payment with BusinessUnitCode "0002" and PaymentType "OTHER_PARTY_FOR_PURCHASE"
    Then the following purchase orders should be returned to "Gehan.Ahmed":
      | Code       | Company          |
      | 2018000055 | 0001 - AL Madina |
      | 2020000048 | 0001 - AL Madina |
      | 2020000047 | 0002 - DigiPro   |
      | 2020000046 | 0001 - AL Madina |
      | 2020000045 | 0001 - AL Madina |
      | 2018000051 | 0001 - AL Madina |
      | 2018000050 | 0002 - DigiPro   |
      | 2018000049 | 0001 - AL Madina |
      | 2018000048 | 0001 - AL Madina |
      | 2018000047 | 0001 - AL Madina |
      | 2018000046 | 0001 - AL Madina |
      | 2018000044 | 0002 - DigiPro   |
      | 2018000023 | 0002 - DigiPro   |
      | 2018000022 | 0001 - AL Madina |
      | 2018000019 | 0001 - AL Madina |
      | 2018000018 | 0001 - AL Madina |
      | 2018000017 | 0001 - AL Madina |
      | 2018000016 | 0002 - DigiPro   |
      | 2018000011 | 0001 - AL Madina |
      | 2018000010 | 0002 - DigiPro   |
      | 2018000009 | 0002 - DigiPro   |
      | 2018000008 | 0001 - AL Madina |
      | 2018000005 | 0002 - DigiPro   |
      | 2018000002 | 0002 - DigiPro   |
      | 2018000001 | 0001 - AL Madina |
    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 25

  #EBS-7424
  Scenario: (02) Read All Goods PurchaseOrders [Import / Local] in Dropdown by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all PurchaseOrders from Approved to Delivery Complete And not Cancelled for Payment with BusinessUnitCode "0002" and BusinessPartnerCode "000002" and PaymentType "VENDOR"
    Then the following purchase orders should be returned to "Gehan.Ahmed":
      | Code       | Company          |
      | 2020000048 | 0001 - AL Madina |
      | 2020000047 | 0002 - DigiPro   |
      | 2020000046 | 0001 - AL Madina |
      | 2020000045 | 0001 - AL Madina |
      | 2018000051 | 0001 - AL Madina |
      | 2018000050 | 0002 - DigiPro   |
      | 2018000049 | 0001 - AL Madina |
      | 2018000048 | 0001 - AL Madina |
      | 2018000047 | 0001 - AL Madina |
      | 2018000046 | 0001 - AL Madina |
      | 2018000044 | 0002 - DigiPro   |
      | 2018000023 | 0002 - DigiPro   |
      | 2018000022 | 0001 - AL Madina |
      | 2018000019 | 0001 - AL Madina |
      | 2018000018 | 0001 - AL Madina |
      | 2018000017 | 0001 - AL Madina |
      | 2018000016 | 0002 - DigiPro   |
      | 2018000011 | 0001 - AL Madina |
      | 2018000010 | 0002 - DigiPro   |
      | 2018000009 | 0002 - DigiPro   |
      | 2018000008 | 0001 - AL Madina |
      | 2018000005 | 0002 - DigiPro   |
      | 2018000002 | 0002 - DigiPro   |
      | 2018000001 | 0001 - AL Madina |

    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 24

  Scenario: (03) Read All PurchaseOrders for DueDocument Dropdown in PaymentRequest by unauthorized user (Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all PurchaseOrders from Approved to Delivery Complete And not Cancelled for Payment with BusinessUnitCode "0002" and BusinessPartnerCode "000002" and PaymentType "OTHER_PARTY_FOR_PURCHASE"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
