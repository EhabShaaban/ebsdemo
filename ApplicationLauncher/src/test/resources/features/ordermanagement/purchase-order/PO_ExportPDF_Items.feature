# Author : Hend Ahmed & Nancy - 11-02-2019
Feature: PO_Export_PDF_Items

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |

    And the following roles and sub-roles exist:
      | Role                             | Sub-role           |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:ExportPDF | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:ExportPDF | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:ExportPDF | [purchaseUnitName='Corrugated'] |

    And the following Import PurchaseOrders exist:
      | Code       | Type   | State   | PurchasingUnit |
      | 2018000022 | Import | Shipped | 0002           |
      | 2018000025 | Import | Draft   | 0001           |
      | 2018000026 | Import | Draft   | 0006           |

    And the following Local PurchaseOrders exist:
      | Code       | Type  | State | PurchasingUnit |
      | 2018000013 | Local | Draft | 0002           |
      | 2018000027 | Local | Draft | 0001           |
      | 2018000028 | Local | Draft | 0006           |

    And PurchaseOrder with code "2018000013" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | 0019 |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 3.20x50 | 3.0              | 0.0%     |
    And Item with code "000002" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |


    And PurchaseOrder with code "2018000022" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000022" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |


    And PurchaseOrder with code "2018000025" has the following OrderItems:
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | 0019 |
    And Item with code "000005" PurchaseOrder with code "2018000025" has empty Quantities

    And PurchaseOrder with code "2018000026" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000026" has empty Quantities

    And PurchaseOrder with code "2018000027" has the following OrderItems:
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | 0019 |
    And Item with code "000006" PurchaseOrder with code "2018000027" has empty Quantities

    And PurchaseOrder with code "2018000028" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000028" has empty Quantities

    And the following Measures exist:
      | Code | Type     | Symbol       | Name         |
      | 0003 | Standard | Kg           | Kilogram     |
      | 0019 | Standard | M2           | Square Meter |
      | 0014 | Standard | Liter        | Liter        |
      | 0029 | Business | Roll 2.20x50 | Roll 2.20x50 |
      | 0033 | Business | Roll 2.70x50 | Roll 2.70x50 |
      | 0032 | Business | Roll 3.20x50 | Roll 3.20x50 |

    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             |
      | 000014  | 000002   | 000002     | 0033    | GDF730-ddd       | 567891011        | 11500.80 | 0003         | 0002             |

  Scenario: (01) Request to export PurchaseOrder Items by an authorized user who has one role - Import PO - Signmedia (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000022" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | M2   |
    And the following values of Quantities list for Item "000002" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDB550-610       |
      | 50  | Roll 2.70x50 | 3                | 0%       | GDF730-ddd       |

  Scenario: (02) Request to export PurchaseOrder Items by an authorized user who has one role - Local PO - Signmedia (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000013" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | M2   |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | M2   |
    And the following values of Quantities list for Item "000001" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDF530-440       |
      | 50  | Roll 3.20x50 | 3                | 0%       | GDF630-440       |
    And the following values of Quantities list for Item "000002" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDB550-610       |
      | 50  | Roll 2.70x50 | 3                | 0%       | GDF730-ddd       |

  Scenario: (03) Request to export PurchaseOrder Items by an authorized user who has two roles - Import PO - Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000025" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Amr.Khalil":
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | M2   |
    And an empty Quantities list for Item "000005" is displayed to "Amr.Khalil"

  Scenario: (04) Request to export PurchaseOrder Items by an authorized user who has two roles - Import PO - Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000026" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Amr.Khalil":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" is displayed to "Amr.Khalil"

  Scenario: (05) Request to export PurchaseOrder Items by an authorized user who has two roles - Local PO - Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000027" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Amr.Khalil":
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | M2   |
    And an empty Quantities list for Item "000006" is displayed to "Amr.Khalil"

  Scenario: (06)  Request to export PurchaseOrder Items by an authorized user who has two roles - Local PO - Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to export PurchaseOrder Items of PurchaseOrder with code "2018000028" as PDF
    Then the following values of PurchaseOrder Items are displayed to "Amr.Khalil":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" is displayed to "Amr.Khalil"

