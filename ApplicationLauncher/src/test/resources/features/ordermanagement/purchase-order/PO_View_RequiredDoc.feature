# Reviewer: Marina (28-Jan-2019 11:10 AM)
# Reviewer: Somaya (29-Jan-2019 10:30 AM)

Feature: View RequiredDocuments section in PO

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                          | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadRequiredDocuments | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadRequiredDocuments | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadRequiredDocuments | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                                 |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000025 | Draft | 0001           |
      | 2018000026 | Draft | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
      | 2018000027 | Draft | 0001           |
      | 2018000028 | Draft | 0006           |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000021":
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000025":
      | DocumentType | Copies |
      | 0001         | 2      |
      | 0002         | 4      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000026":
      | DocumentType | Copies |
      | 0003         | 1      |
      | 0004         | 2      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000013":
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000027":
      | DocumentType | Copies |
      | 0001         | 3      |
      | 0003         | 2      |
    And the following RequiredDocuments section exist in PurchaseOrder with code "2018000028":
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the following DocumentTypes exist:
      | Code | Name                                                |
      | 0001 | Performa Invoice                                    |
      | 0002 | Commercial Invoice                                  |
      | 0003 | Commercial Invoice Legalized by Chamber of Commerce |
      | 0004 | Bill of Lading (BL)                                 |
      | 0005 | Packing List for the Whole Order                    |

  #### Happy Paths ####
  # EBS-1660
  Scenario Outline: (01) View RequiredDocuments section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then the following values of RequiredDocuments section are displayed to "Gehan.Ahmed":
      | DocumentType      | Copies      |
      | <R1_DocumentType> | <R1_Copies> |
      | <R2_DocumentType> | <R2_Copies> |
    Examples:
      | POCode     | R1_DocumentType                  | R1_Copies | R2_DocumentType     | R2_Copies |
      | 2018000013 | Packing List for the Whole Order | 5         | Bill of Lading (BL) | 4         |
      | 2018000021 | Packing List for the Whole Order | 5         | Bill of Lading (BL) | 4         |

  # EBS-1660
  Scenario Outline: (02) View RequiredDocuments section by an authorized user who has two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then the following values of RequiredDocuments section are displayed to "Amr.Khalil":
      | DocumentType      | Copies      |
      | <R1_DocumentType> | <R1_Copies> |
      | <R2_DocumentType> | <R2_Copies> |
    Examples:
      | POCode     | R1_DocumentType                                     | R1_Copies | R2_DocumentType                                     | R2_Copies |
      | 2018000025 | Performa Invoice                                    | 2         | Commercial Invoice                                  | 4         |
      | 2018000026 | Commercial Invoice Legalized by Chamber of Commerce | 1         | Bill of Lading (BL)                                 | 2         |
      | 2018000027 | Performa Invoice                                    | 3         | Commercial Invoice Legalized by Chamber of Commerce | 2         |
      | 2018000028 | Packing List for the Whole Order                    | 5         | Bill of Lading (BL)                                 | 4         |

  ##### Exception Cases ####
  # EBS-1660
  @ResetData
  Scenario Outline: (03) View RequiredDocuments section where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  #### Abuse Cases  ####
  # EBS-1660
  Scenario Outline: (04) View RequiredDocuments section by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     |
      | Mahmoud.Abdelaziz | 2018000013 |
      | Mahmoud.Abdelaziz | 2018000021 |
      | Amr.Khalil        | 2018000013 |
      | Amr.Khalil        | 2018000021 |
