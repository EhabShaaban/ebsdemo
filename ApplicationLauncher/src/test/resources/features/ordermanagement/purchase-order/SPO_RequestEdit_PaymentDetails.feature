# Author Dev: Fatma Al Zahraa - HP
# Author Dev: Eman - Zyad - Val & Auth
# Author Quality: Shirin Mahmoud

Feature: Request to edit and cancel PaymentDetails section in Service Purchase Order

  Background:
    Given the following users and roles exist:
      | Name                                 | Role                                                               |
      | Gehan.Ahmed                          | PurchasingResponsible_Signmedia                                    |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | PurchasingResponsible_Signmedia_CannotReadPaymentTermsCurrencyRole |
      | hr1                                  | SuperUser                                                          |
    And the following roles and sub-roles exist:
      | Role                                                               | Sub-role          |
      | PurchasingResponsible_Signmedia                                    | POOwner_Signmedia |
      | PurchasingResponsible_Signmedia                                    | PaymentTermViewer |
      | PurchasingResponsible_Signmedia                                    | CurrencyViewer    |
      | PurchasingResponsible_Signmedia_CannotReadPaymentTermsCurrencyRole | POOwner_Signmedia |
      | SuperUser                                                          | SuperUserSubRole  |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                       | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
      | PaymentTermViewer | PaymentTerms:ReadAll             |                                |
      | CurrencyViewer    | Currency:ReadAll                 |                                |
      | SuperUserSubRole  | *:*                              |                                |
    And the following users doesn't have the following permissions:
      | User                                 | Permission           |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | PaymentTerms:ReadAll |
      | Gehan.Ahmed.NoPaymentTermsNoCurrency | Currency:ReadAll     |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                       | Condition                  |
      | Gehan.Ahmed | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000040 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000055 | IMPORT_PO  | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000040 | 0001 - Flexo     |                  |                                 |
      | 2018000055 | 0002 - Signmedia |                  |                                 |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     |                  |                                 |
      | 2020000006 | 0002 - Signmedia |                  |                                 |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000005 | 000001 - Siegwerk | 2018000040  |
      | 2020000006 | 000002 - Zhejiang | 2018000055  |
    And edit session is "30" minutes

  ### Request to Edit Payment Details section ###
  # EBS-7404
  Scenario:(01) Request to edit PaymentDetails section in Service PurchaseOrder by an authorized user in Draft State (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000004"
    Then PaymentDetails section of Service PurchaseOrder with code "2020000004" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads  |
      | ReadPaymentTerms |
      | ReadCurrency     |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields  |
      | paymentTermCode |
      | currencyIso     |

  # EBS-7697
  Scenario:(02) Request to edit PaymentDetails section in Service PurchaseOrder that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the PaymentDetails section of Service PurchaseOrder with code "2020000004" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  # EBS-7697
  Scenario:(03) Request to edit PaymentDetails section in Service PurchaseOrder of deleted Service PurchaseOrder (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-7697
  Scenario: (04) Request to edit PaymentDetails section in Service PurchaseOrder when it is not allowed in current state: Confirmed (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000006"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And PaymentDetails section of Service PurchaseOrder with code "2020000006" is not locked by "hr1"

  # EBS-7697
  Scenario:(05) Request to edit PaymentDetails section in Service PurchaseOrder with unauthorized user (with condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000005"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  # EBS-7697
  Scenario: (06) Request to edit PaymentDetails section in Service PurchaseOrder  (No authorized reads)
    Given user is logged in as "Gehan.Ahmed.NoPaymentTermsNoCurrency"
    When "Gehan.Ahmed.NoPaymentTermsNoCurrency" requests to edit PaymentDetails section of Service PurchaseOrder with code "2020000004"
    Then PaymentDetails section of Service PurchaseOrder with code "2020000004" becomes in edit mode and locked by "Gehan.Ahmed.NoPaymentTermsNoCurrency"
    And there are no authorized reads returned to "Gehan.Ahmed.NoPaymentTermsNoCurrency"
    And there are no mandatory fields returned to "Gehan.Ahmed.NoPaymentTermsNoCurrency"
    And the following editable fields are returned to "Gehan.Ahmed.NoPaymentTermsNoCurrency":
      | EditableFields  |
      | paymentTermCode |
      | currencyIso     |

  ### Request to Cancel Payment Details section ###
  # EBS-7404
  Scenario:(07) Request to Cancel saving PaymentDetails section in Service PurchaseOrder by an authorized user with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving PaymentDetails section of Service PurchaseOrder with code "2020000004" at "07-Jan-2019 09:30 AM"
    Then the lock by "Gehan.Ahmed" on PaymentDetails section of Service PurchaseOrder with code "2020000004" is released

  # EBS-7697
  Scenario:(08) Request to Cancel saving PaymentDetails section in Service PurchaseOrder after lock session is expired
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving PaymentDetails section of Service PurchaseOrder with code "2020000004" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  # EBS-7697
  Scenario: (09) Request to Cancel saving PaymentDetails section in Service PurchaseOrder after lock session is expired and Service PurchaseOrder doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving PaymentDetails section of Service PurchaseOrder with code "2020000004" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-7697
  Scenario:(10) Request to Cancel saving PaymentDetails section in Service PurchaseOrder with unauthorized user (with condition)(Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" cancels saving PaymentDetails section of Service PurchaseOrder with code "2020000005"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

