# Reviewer: Marina (10-Feb-2019 - 03:00 pm)
# Reviewer: Somaya (11-Feb-2019 - 12:00 am)

Feature: Request Edit / Cancel PaymentAndDelivery section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name                              | Role                                                               |
      | Gehan.Ahmed                       | PurchasingResponsible_Signmedia                                    |
      | Amr.Khalil                        | PurchasingResponsible_Flexo                                        |
      | Amr.Khalil                        | PurchasingResponsible_Corrugated                                   |
      | hr1                               | SuperUser                                                          |
      | Gehan.Ahmed.NoIncoterm            | PurchasingResponsible_Signmedia_CannotViewIncotermRole             |
      | Gehan.Ahmed.NoCurrency            | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             |
      | Gehan.Ahmed.NoPaymentTerm         | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         |
      | Gehan.Ahmed.NoShippingInstruction | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole |
      | Gehan.Ahmed.NoContainersType      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       |
      | Gehan.Ahmed.NoModeOfTransport     | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      |
      | Gehan.Ahmed.NoPort                | PurchasingResponsible_Signmedia_CannotViewPortRole                 |
    And the following roles and sub-roles exist:
      | Role                                                               | Subrole                   |
      | PurchasingResponsible_Signmedia                                    | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia                                    | IncotermViewer            |
      | PurchasingResponsible_Signmedia                                    | CurrencyViewer            |
      | PurchasingResponsible_Signmedia                                    | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia                                    | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia                                    | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia                                    | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia                                    | PortViewer                |
      | PurchasingResponsible_Flexo                                        | POOwner_Flexo             |
      | PurchasingResponsible_Flexo                                        | IncotermViewer            |
      | PurchasingResponsible_Flexo                                        | CurrencyViewer            |
      | PurchasingResponsible_Flexo                                        | PaymentTermViewer         |
      | PurchasingResponsible_Flexo                                        | ShippingInstructionViewer |
      | PurchasingResponsible_Flexo                                        | ContainerTypeViewer       |
      | PurchasingResponsible_Flexo                                        | ModeofTransportViewer     |
      | PurchasingResponsible_Flexo                                        | PortViewer                |
      | PurchasingResponsible_Corrugated                                   | POOwner_Corrugated        |
      | PurchasingResponsible_Corrugated                                   | IncotermViewer            |
      | PurchasingResponsible_Corrugated                                   | CurrencyViewer            |
      | PurchasingResponsible_Corrugated                                   | PaymentTermViewer         |
      | PurchasingResponsible_Corrugated                                   | ShippingInstructionViewer |
      | PurchasingResponsible_Corrugated                                   | ContainerTypeViewer       |
      | PurchasingResponsible_Corrugated                                   | ModeofTransportViewer     |
      | PurchasingResponsible_Corrugated                                   | PortViewer                |
      | SuperUser                                                          | SuperUserSubRole          |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ModeofTransportViewer     |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                       | Condition                       |
      | POOwner_Signmedia         | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo             | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated        | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Corrugated'] |
      | IncotermViewer            | Incoterm:ReadAll                 |                                 |
      | CurrencyViewer            | Currency:ReadAll                 |                                 |
      | PaymentTermViewer         | PaymentTerms:ReadAll             |                                 |
      | ShippingInstructionViewer | ShippingInstructions:ReadAll     |                                 |
      | ContainerTypeViewer       | ContainerType:ReadAll            |                                 |
      | ModeofTransportViewer     | ModeOfTransport:ReadAll          |                                 |
      | PortViewer                | Port:ReadAll                     |                                 |
      | SuperUserSubRole          | *:*                              |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000024 | Draft            | 0003           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
    And edit session is "30" minutes

  ############# Request to open payment and delivery in edit mode ######################################################
  #EBS-1381
  Scenario Outline: (01) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadCurrency             |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadTransportModes       |
      | ReadPorts                |
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000021 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000006 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000005 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000010 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived            | 0002           | Gehan.Ahmed |
      | 2018000001 | Cleared            | 0002           | Gehan.Ahmed |
      | 2018000013 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000014 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000025 | Draft              | 0001           | Amr.Khalil  |
      | 2018000026 | Draft              | 0006           | Amr.Khalil  |
      | 2018000027 | Draft              | 0001           | Amr.Khalil  |
      | 2018000028 | Draft              | 0006           | Amr.Khalil  |

  Scenario Outline: (02) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoIncoterm"
    When "Gehan.Ahmed.NoIncoterm" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoIncoterm"
    And the following authorized reads are returned to "Gehan.Ahmed.NoIncoterm":
      | AuthorizedReads          |
      | ReadCurrency             |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadTransportModes       |
      | ReadPorts                |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (03) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoCurrency"
    When "Gehan.Ahmed.NoCurrency" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoCurrency"
    And the following authorized reads are returned to "Gehan.Ahmed.NoCurrency":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadTransportModes       |
      | ReadPorts                |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (04) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoPaymentTerm"
    When "Gehan.Ahmed.NoPaymentTerm" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoPaymentTerm"
    And the following authorized reads are returned to "Gehan.Ahmed.NoPaymentTerm":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadCurrency             |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadTransportModes       |
      | ReadPorts                |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (05) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoShippingInstruction"
    When "Gehan.Ahmed.NoShippingInstruction" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoShippingInstruction"
    And the following authorized reads are returned to "Gehan.Ahmed.NoShippingInstruction":
      | AuthorizedReads    |
      | ReadIncoTerms      |
      | ReadCurrency       |
      | ReadPaymentTerms   |
      | ReadContainerTypes |
      | ReadTransportModes |
      | ReadPorts          |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (06) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoContainersType"
    When "Gehan.Ahmed.NoContainersType" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoContainersType"
    And the following authorized reads are returned to "Gehan.Ahmed.NoContainersType":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadCurrency             |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadTransportModes       |
      | ReadPorts                |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (07) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoModeOfTransport"
    When "Gehan.Ahmed.NoModeOfTransport" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoModeOfTransport"
    And the following authorized reads are returned to "Gehan.Ahmed.NoModeOfTransport":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadCurrency             |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadPorts                |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  Scenario Outline: (08) Request to edit PaymentandDelivery section: User with one or more role - All states except WaitingApproval, Cancelled, DeliveryComplete - for both Import and Local POs  (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoPort"
    When "Gehan.Ahmed.NoPort" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then PaymentandDelivery section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoPort"
    And the following authorized reads are returned to "Gehan.Ahmed.NoPort":
      | AuthorizedReads          |
      | ReadIncoTerms            |
      | ReadCurrency             |
      | ReadPaymentTerms         |
      | ReadShippingInstructions |
      | ReadContainerTypes       |
      | ReadTransportModes       |
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  #EBS-1381
  Scenario Outline: (09) Request to edit PaymentandDelivery section when section is locked by another user: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the PaymentandDelivery section of PurchaseOrder with code "<POCode>" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-1381
  @ResetData
  Scenario Outline: (10) Request to edit PaymentandDelivery section of a PurchaseOrder that doesn't exist: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-1381
  Scenario Outline: (11) Request to edit PaymentandDelivery section when it is not allowed in current state:  Waiting Approval, Cancelled and DeliveryComplete for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentandDelivery section of PurchaseOrder with code "<PurCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And PaymentandDelivery section of PurchaseOrder with code "<PurCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | PurCode    |
      | 2018000007 |
      | 2018000012 |
      | 2018000002 |
      | 2018000015 |
      | 2018000019 |
      | 2018000020 |

  #EBS-1381
  Scenario Outline: (12) Request to edit PaymentandDelivery section by an unauthorized users due to condition or not (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |

  #########  Request to  Cancel payment and delivery  ######################################################
  #EBS-1381
  Scenario Outline: (13) Cancel save PaymentandDelivery section within the edit session with one or more role - All states except WaitingApproval. Cancelled and DeliveryComplete - for both Import and Local POs (Happy Path)
    Given user is logged in as "<User>"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 9:10 AM"
    When "<User>" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM"
    Then the lock by "<User>" on PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is released
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000021 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000006 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000005 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000010 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived            | 0002           | Gehan.Ahmed |
      | 2018000001 | Cleared            | 0002           | Gehan.Ahmed |
      | 2018000013 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000014 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000025 | Draft              | 0001           | Amr.Khalil  |
      | 2018000026 | Draft              | 0006           | Amr.Khalil  |
      | 2018000027 | Draft              | 0001           | Amr.Khalil  |
      | 2018000028 | Draft              | 0006           | Amr.Khalil  |

  #EBS-1381
  Scenario Outline: (14) Cancel save PaymentandDelivery section after edit session expire for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-1381
  Scenario Outline: (15) Cancel save PaymentandDelivery section when section is locked by another user: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-1381
  @ResetData
  Scenario Outline: (16) Cancel save PaymentandDelivery section while PurchaseOrder doesn't exsit for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # Not Reviewed Yet
  Scenario Outline: (17) Request to edit PaymentandDelivery section when it is not allowed in current state:  Waiting Approval, Cancelled and DeliveryComplete for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000007 |
      | 2018000012 |
      | 2018000002 |
      | 2018000015 |
      | 2018000019 |
      | 2018000020 |

  #EBS-1381
  Scenario Outline: (18) Cancel save PaymentandDelivery section by an unauthorized users due to condition or not for both Local and Import PO (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving PaymentandDelivery section of PurchaseOrder with Code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |
