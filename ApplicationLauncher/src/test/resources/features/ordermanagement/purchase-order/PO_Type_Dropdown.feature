#Author: Waseem Salama
#Reviewer: Shirin
Feature: ViewAll PurchaseOrder Types Dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission            | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | PurchaseOrder:ReadAll |

  #EBS-5958
  Scenario: (01) Read list of PurchaseOrder types dropdown, by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all PurchaseOrder types in a dropdown
    Then the following PurchaseOrder type values will be presented to "Gehan.Ahmed" in a dropdown:
      | Type       |
      | IMPORT_PO  |
      | LOCAL_PO   |
      | SERVICE_PO |
    And total number of PurchaseOrder types returned to "Gehan.Ahmed" in a dropdown is equal to 3

  #EBS-5958
  Scenario: (02) Read list of PurchaseOrder types dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all PurchaseOrder types in a dropdown
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
