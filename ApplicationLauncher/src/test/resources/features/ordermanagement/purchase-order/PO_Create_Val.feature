# Reviewer: Somaya Ahmed on 02-Jan-2019, 11:00 AM
# Updated By: Waseem Salama (EBS-6179)
Feature: Create PurchaseOrder Val

  User should be able to create PurchaseOrder
  Rules:
  - User should be authenticated and authorized to create Local or Import POs
  - User should be authorized to ReadAll vendors, purchase units, purchase responsible with OR without consition
  - User should enter all mandatory fields for creation
  - If an unauthorized user sends a create request, he/she shoud be logged out as this is considered as a security breach

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | hr1         | SuperUser                       |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | SuperUser                       | SuperUserSubRole  |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission           | Condition |
      | SuperUserSubRole  | *:*                  |           |
      | POOwner_Signmedia | PurchaseOrder:Create |           |
    And the following users doesn't have the following permissions:
      | User           | Permission                       |
      | Ahmed.Al-Ashry | PurchaseOrder:CanBeDocumentOwner |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following DocumentOwners exist:
      | Id | Name        | BusinessObject | Permission         | Condition |
      | 37 | Gehan Ahmed | PurchaseOrder  | CanBeDocumentOwner |           |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject   | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | SalesReturnOrder | CanBeDocumentOwner |           |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000012 | IMPORT_PO | Cancelled        | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO  | DeliveryComplete | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2018000012 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2018000019 | 0002 - Signmedia | 0001 - DigiPro | 1516171819789 - EGP - Alex Bank |
    And the following vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000002 | Zhejiang | 0002           |

  ######## Create with missing mandatory fields (Abuse cases since create button is not enabled until all mandatories are entered)

  Scenario Outline: (01) Create PurchaseOrder with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates PurchaseOrder with following values:
      | Type   | ReferencePurchaseOrder   | BusinessUnit   | Vendor   | DocumentOwnerId   |
      | <Type> | <ReferencePurchaseOrder> | <BusinessUnit> | <Vendor> | <DocumentOwnerId> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Type       | BusinessUnit | ReferencePurchaseOrder | Vendor | DocumentOwnerId |
      |            | 0002         |                        | 000002 | 37              |
      | ""         | 0002         |                        | 000002 | 37              |
      | N/A        | 0002         |                        | 000002 | 37              |
      | IMPORT_PO  |              |                        | 000002 | 37              |
      | LOCAL_PO   | ""           |                        | 000002 | 37              |
      | LOCAL_PO   | N/A          |                        | 000002 | 37              |
      | SERVICE_PO | 0002         |                        | 000002 | 37              |
      | SERVICE_PO | 0002         | ""                     | 000002 | 37              |
      | SERVICE_PO | 0002         | N/A                    | 000002 | 37              |
      | IMPORT_PO  | 0002         |                        |        | 37              |
      | LOCAL_PO   | 0002         |                        | ""     | 37              |
      | LOCAL_PO   | 0002         |                        | N/A    | 37              |
      | IMPORT_PO  | 0002         |                        | 000002 |                 |
      | LOCAL_PO   | 0002         |                        | 000002 | ""              |
      | LOCAL_PO   | 0002         |                        | 000002 | N/A             |

  ######## Create with incorrect (Validation Failure)

  Scenario Outline: (02) Create PurchaseOrder with incorrect data entry (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates PurchaseOrder with following values:
      | Type   | ReferencePurchaseOrder   | BusinessUnit   | Vendor   | DocumentOwnerId   |
      | <Type> | <ReferencePurchaseOrder> | <BusinessUnit> | <Vendor> | <DocumentOwnerId> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | Type       | BusinessUnit | ReferencePurchaseOrder | Vendor | DocumentOwnerId | Field           | ErrMsg     |
      # RefPurchaseOrder in invalid state
      | SERVICE_PO | 0002         | 2018000012             | 000002 | 37              | referencePOCode | Gen-msg-53 |
      | SERVICE_PO | 0002         | 2018000019             | 000002 | 37              | referencePOCode | Gen-msg-53 |
      # Vendor does not exist
      | LOCAL_PO   | 0002         |                        | 999999 | 37              | vendorCode      | Gen-msg-48 |
      # DocumentOwner does not belong to PO document owners
      | IMPORT_PO  | 0002         |                        | 000002 | 1000072         | documentOwnerId | Gen-msg-48 |

  ######## Create with malicious input (Abuse case)

  Scenario Outline: (03) Create PurchaseOrder with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "hr1"
    When "hr1" creates PurchaseOrder with following values:
      | Type   | ReferencePurchaseOrder   | BusinessUnit   | Vendor   | DocumentOwnerId   |
      | <Type> | <ReferencePurchaseOrder> | <BusinessUnit> | <Vendor> | <DocumentOwnerId> |
    Then "hr1" is logged out
    And "hr1" is forwarded to the error page
    Examples:
      | Type       | BusinessUnit | ReferencePurchaseOrder | Vendor  | DocumentOwnerId |
      | Ay7aga     | 0002         |                        | 000002  | 37              |
      | IMPORT_PO  | Ay7aga       |                        | 000002  | 37              |
      | LOCAL_PO   | 00002        |                        | 000002  | 37              |
      | LOCAL_PO   | 9999         |                        | 000002  | 37              |
      | SERVICE_PO | 0002         | Ay7aga                 | 000002  | 37              |
      | SERVICE_PO | 0002         | 20180000190            | 000002  | 37              |
      | SERVICE_PO | 0002         | 9999999999             | 000002  | 37              |
      # BusinessUnit not related to RefPurchaseOrder
      | SERVICE_PO | 0001         | 2018000019             | 000002  | 37              |
      | IMPORT_PO  | 0002         |                        | Ay7aga  | 37              |
      | LOCAL_PO   | 0002         |                        | 0000020 | 37              |
      | IMPORT_PO  | 0002         |                        | 000002  | 99999.999       |
      | LOCAL_PO   | 0002         |                        | 000002  | Ay7aga          |
