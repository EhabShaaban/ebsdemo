# Reviewer: Marina (22-Jan-2019 3:20 PM)
# Reviewer: Somaya (27-Jan-2019 3:30 PM)
# updated by: Fatma Al Zahraa (EBS - 7047)
Feature: View Company section in PO

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadCompany | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadCompany | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadCompany | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                       |                                 |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                | Condition                      |
      | Amr.Khalil | PurchaseOrder:ReadCompany | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000013 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000026 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000027 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000028 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000055 | LOCAL_PO   | Cleared | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft   | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000027 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000028 | 0006 - Corrugated | 0001 - AL Madina |                                 |
      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

  #### Happy Path  ####
  #EBS-1043
  #EBS-7047
  Scenario Outline: (01) View Company section by an authorized user who has one / two role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view Company section of PurchaseOrder with code "<POCodeValue>"
    Then the following values of Company section of PurchaseOrder with code "<POCode>" are displayed to "<User>":
      | POCode        | Company        | BusinessUnit   | BankAccountNumber        |
      | <POCodeValue> | <CompanyValue> | <BusinessUnit> | <BankAccountNumberValue> |
    Examples:
      | User        | POCodeValue | CompanyValue     | BusinessUnit      | BankAccountNumberValue          |
      | Gehan.Ahmed | 2018000013  | 0001 - AL Madina | 0002 - Signmedia  | 1516171819789 - EGP - Alex Bank |
      | Gehan.Ahmed | 2018000021  | 0001 - AL Madina | 0002 - Signmedia  | 1516171819789 - EGP - Alex Bank |
      | Amr.Khalil  | 2018000025  | 0001 - AL Madina | 0001 - Flexo      | 1516171819789 - EGP - Alex Bank |
      | Amr.Khalil  | 2018000026  | 0001 - AL Madina | 0006 - Corrugated | 1516171819789 - EGP - Alex Bank |
      | Amr.Khalil  | 2018000027  | 0001 - AL Madina | 0001 - Flexo      | 1516171819789 - EGP - Alex Bank |
      | Amr.Khalil  | 2018000028  | 0001 - AL Madina | 0006 - Corrugated |                                 |
      | Gehan.Ahmed | 2020000004  | 0001 - AL Madina | 0002 - Signmedia  | 1516171819789 - EGP - Alex Bank |

  #### Exceptions  ####
  #EBS-1043
  Scenario Outline: (03) View Company section where PurchaseOrder has been deleted by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view Company section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |
      | 2020000004 |


  #### Abuse Cases  ####
  #EBS-1043
  Scenario Outline: (04) View Company section by an unauthorized user due to condition OR not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Company section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User       |
      | 2018000013 | Amr.Khalil |
      | 2018000021 | Amr.Khalil |
