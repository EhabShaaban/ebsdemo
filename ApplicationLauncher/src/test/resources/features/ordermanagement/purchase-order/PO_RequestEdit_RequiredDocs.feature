# Reviewer: Shrouk (11-Feb-2019)
# Reviewer: Somaya & Hend (11-Feb-2019)

Feature: Request Edit / Cancel RequiredDocuments section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name                       | Role                                                       |
      | Gehan.Ahmed                | PurchasingResponsible_Signmedia                            |
      | Amr.Khalil                 | PurchasingResponsible_Flexo                                |
      | Amr.Khalil                 | PurchasingResponsible_Corrugated                           |
      | hr1                        | SuperUser                                                  |
      | Gehan.Ahmed.NoDocumentType | PurchasingResponsible_Signmedia_CannotViewDocumentTypeRole |
    And the following roles and sub-roles exist:
      | Role                                                       | Subrole             |
      | PurchasingResponsible_Signmedia                            | POOwner_Signmedia   |
      | PurchasingResponsible_Flexo                                | POOwner_Flexo       |
      | PurchasingResponsible_Corrugated                           | POOwner_Corrugated  |
      | PurchasingResponsible_Signmedia                            | DocumentTypesViewer |
      | PurchasingResponsible_Flexo                                | DocumentTypesViewer |
      | PurchasingResponsible_Corrugated                           | DocumentTypesViewer |
      | SuperUser                                                  | SuperUserSubRole    |
      | PurchasingResponsible_Signmedia_CannotViewDocumentTypeRole | POOwner_Signmedia   |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                            | Condition                       |
      | POOwner_Signmedia   | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo       | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated  | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Corrugated'] |
      | DocumentTypesViewer | DocumentType:ReadAll                  |                                 |
      | SuperUserSubRole    | *:*                                   |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
    And edit session is "30" minutes

  #########  Request to open Required Documents in edit mode ######################################################
  # EBS-1662
  Scenario Outline: (01) Request to edit RequiredDocuments section - User with one or more role - All states except WaitingApproval, DeliveryComplete and Cancelled - for both Import and Local (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then RequiredDocuments section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads      |
      | ReadAllDocumentTypes |
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000021 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000006 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000005 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000010 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived            | 0002           | Gehan.Ahmed |
      | 2018000001 | Cleared            | 0002           | Gehan.Ahmed |
      | 2018000013 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000014 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000025 | Draft              | 0001           | Amr.Khalil  |
      | 2018000026 | Draft              | 0006           | Amr.Khalil  |
      | 2018000027 | Draft              | 0001           | Amr.Khalil  |
      | 2018000028 | Draft              | 0006           | Amr.Khalil  |

  # EBS-1662
  Scenario Outline: (02) Request to edit RequiredDocuments section - User with one or more role - All states except WaitingApproval, DeliveryComplete and Cancelled - for both Import and Local (Authorization)
    Given user is logged in as "Gehan.Ahmed.NoDocumentType"
    When "Gehan.Ahmed.NoDocumentType" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then RequiredDocuments section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Gehan.Ahmed.NoDocumentType"
    And there are no authorized reads returned to "Gehan.Ahmed.NoDocumentType"
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  # EBS-1662
  Scenario Outline: (03) Request to edit RequiredDocuments section when section is locked by another user - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened RequiredDocuments section of PurchaseOrder with code "<POCode>" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-1662
  @ResetData
  Scenario Outline: (04) Request to edit RequiredDocuments section of a PurchaseOrder that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-1662
  Scenario Outline: (05) Request to edit RequiredDocuments section when it is not allowed in current state: WaitingApproval, DeliveryComplete, Cancelled for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And RequiredDocuments section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     |
      | 2018000007 |
      | 2018000012 |
      | 2018000002 |
      | 2018000015 |
      | 2018000019 |
      | 2018000020 |

  # EBS-1662
  Scenario Outline:(06) Request to edit RequiredDocuments section by an unauthorized user: for both Local/Import PO (Unauthorized/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit RequiredDocuments section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |

  ######### Cancel saving Required Documents   ########################################################################
  # EBS-1662
  Scenario Outline: (07) Cancel Request to edit RequiredDocuments section within the edit session- User with one or more role - All states except WaitingApproval, DeliveryComplete and Cancelled - for both Import and Local (Happy Path)
    Given user is logged in as "<User>"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000021 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000006 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000005 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000010 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived            | 0002           | Gehan.Ahmed |
      | 2018000001 | Cleared            | 0002           | Gehan.Ahmed |
      | 2018000013 | Draft              | 0002           | Gehan.Ahmed |
      | 2018000014 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000025 | Draft              | 0001           | Amr.Khalil  |
      | 2018000026 | Draft              | 0006           | Amr.Khalil  |
      | 2018000027 | Draft              | 0001           | Amr.Khalil  |
      | 2018000028 | Draft              | 0006           | Amr.Khalil  |

  # EBS-1662
  Scenario Outline: (08) Cancel Request to edit RequiredDocuments section after edit session expire - for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-1662
  Scenario Outline: (09)  Cancel Request to edit RequiredDocuments section after edit session expire and section got locked by another user for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-1662
  @ResetData
  Scenario Outline: (10) Cancel Request to edit RequiredDocuments section while PurchaseOrder doesn't exsit - for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |
  # Not Reviewed Yet

  Scenario Outline: (11) Cancel Request to edit RequiredDocuments section while cancel action not allowed in cuurent state in local&Import (i.e. edit session expired & marked by another user to the next state) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000007 |
      | 2018000012 |
      | 2018000002 |
      | 2018000015 |
      | 2018000019 |
      | 2018000020 |

  # EBS-1662
  Scenario Outline:(12) Cancel Request to edit RequiredDocuments section by an unauthorized users due to condition or not for local/Import PO (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |