# Author: Shrouk Alaa (05 Feb, 2019 11:55 AM)
# Reviewer: Niveen Magdi, Shrouk Alaa, Somaya Ahmed (03 March, 2019)
# Reviewer: Hend and Somaya (14-Mar-2019)
# TODO: In happy path, add cases for Local PO for Amr Khalil
Feature: Mark PO as Delivery Complete

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                           | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:MarkAsDeliveryComplete | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:MarkAsDeliveryComplete | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:MarkAsDeliveryComplete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                                  |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | OrderType | State              | PurchasingUnit |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000003 | IMPORT_PO | Cleared            | 0001           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000023 | IMPORT_PO | Cleared            | 0002           |
      | 2018000045 | IMPORT_PO | Cleared            | 0006           |
      | 2018000049 | IMPORT_PO | Cleared            | 0002           |
      | 2018000050 | IMPORT_PO | Cleared            | 0002           |
      | 2018000051 | IMPORT_PO | Cleared            | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | OrderType | State            | PurchasingUnit |
      | 2018000013 | LOCAL_PO  | Draft            | 0002           |
      | 2018000014 | LOCAL_PO  | OpenForUpdates   | 0002           |
      | 2018000015 | LOCAL_PO  | WaitingApproval  | 0002           |
      | 2018000016 | LOCAL_PO  | Approved         | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed        | 0002           |
      | 2018000018 | LOCAL_PO  | Shipped          | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled        | 0002           |
      | 2018000046 | LOCAL_PO  | Shipped          | 0002           |
      | 2018000047 | LOCAL_PO  | Shipped          | 0002           |
      | 2018000048 | LOCAL_PO  | Shipped          | 0002           |
    And the following GRs exist with the following GR-POData:
      | Code       | State  | Type           | POCode     |
      | 2018000013 | Active | PURCHASE_ORDER | 2018000018 |
      | 2018000018 | Active | PURCHASE_ORDER | 2018000023 |
      | 2018000017 | Active | PURCHASE_ORDER | 2018000045 |
      | 2018000014 | Draft  | PURCHASE_ORDER | 2018000047 |
      | 2018000021 | Draft  | PURCHASE_ORDER | 2018000051 |
      | 2018000019 | Active | PURCHASE_ORDER | 2018000046 |
      | 2018000020 | Active | PURCHASE_ORDER | 2018000050 |
      | 2018000012 | Draft  | PURCHASE_ORDER | 2018000045 |
        ## PO Local - Shipped - Signmedia - All Mandatories - has Active GR (for HappyPath)
    And PurchaseOrder with code "2018000018" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000018" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000018" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000018" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000018" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000018" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000018" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 3.20x50 | 3.0              | 0.0%     |
        #### PO Import - Cleared - Signmedia - All Mandatories - has Active GR (for HappyPath)
    And PurchaseOrder with code "2018000023" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000023" has the following Company section values:
      | Company | Bank | AccountNumber |
      | 0002    |      |               |
    And PurchaseOrder with code "2018000023" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000023" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0004         | 8      |
    And PurchaseOrder with code "2018000023" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000023" has the following OrderItems section values:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000023" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        ##### PO Import - Cleared - Flexo - All Mandatories - has Active GR (for HappyPath)
    And PurchaseOrder with code "2018000003" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0001           | 000001 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000003" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000003" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000003" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 10     |
    And PurchaseOrder with code "2018000003" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000003" has the following OrderItems section values:
      | ItemCode | ItemName                    | QtyInDN | Base |
      | 000003   | Flex Primer Varnish E33     | 100     | 0003 |
      | 000004   | Flex cold foil adhesive E01 | 100     | 0003 |
    And Item with code "000003" in PurchaseOrder with code "2018000003" has the following Quantities:
      | Qty  | OrderUnit | UnitPrice(Gross) | Discount |
      | 50.0 | Drum-50Kg | 4.0              | 0.0%     |
    And Item with code "000004" in PurchaseOrder with code "2018000003" has the following Quantities:
      | Qty  | OrderUnit | UnitPrice(Gross) | Discount |
      | 50.0 | Kg        | 3.0              | 0.0%     |
        ### PO Import - Cleared - Corrugated - All Mandatories - has Active GR (for HappyPath)
    And PurchaseOrder with code "2018000045" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0006           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000045" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000045" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000045" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 10     |
    And PurchaseOrder with code "2018000045" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000045" has the following OrderItems section values:
      | ItemCode | ItemName                    | QtyInDN | Base |
      | 000003   | Flex Primer Varnish E33     | 340.00  | 0003 |
      | 000004   | Flex cold foil adhesive E01 | 1500.50 | 0003 |
    And Item with code "000003" in PurchaseOrder with code "2018000045" has the following Quantities:
      | Qty  | OrderUnit | UnitPrice(Gross) | Discount |
      | 50.0 | Drum-50Kg | 4.0              | 0.0%     |
    And Item with code "000004" in PurchaseOrder with code "2018000045" has the following Quantities:
      | Qty  | OrderUnit | UnitPrice(Gross) | Discount |
      | 50.0 | Kg        | 3.0              | 0.0%     |
        #  PO Local - Shipped - Signmedia for draft GR
    And PurchaseOrder with code "2018000047" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000047" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000047" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000047" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000047" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000047" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000047" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        #  PO Import - Cleared - Signmedia for draft GR
    And PurchaseOrder with code "2018000051" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000051" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000051" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000051" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000051" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000051" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000051" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        #   PO Import - Cleared - Signmedia  has no GR
    And PurchaseOrder with code "2018000049" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000049" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000049" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000049" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000049" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000049" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000049" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        #    PO Local - shipped - Signmedia  has no GR
    And PurchaseOrder with code "2018000048" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000048" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000048" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000048" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000048" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000048" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000048" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        # PO Local - Shipped - Signmedia - with missimg mandatory fields
    And PurchaseOrder with code "2018000046" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000046" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000046" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000046" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000046" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000046" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000046" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
        ### PO Import - Cleared - Signmedia - with missimg mandatory fields
    And PurchaseOrder with code "2018000050" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000050" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000050" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000050" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000050" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000050" has the following OrderItems section values:
      | ItemCode | ItemName                           | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll | 18000.0 | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000050" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |

    And the following Import PurchaseOrders exist with clearance date:
      | Code       | clearanceDate        |
      | 2018000023 | 07-Aug-2018 09:02 AM |
      | 2018000003 | 07-Aug-2018 09:02 AM |
      | 2018000045 | 07-Aug-2018 09:02 AM |

    And the following Local PurchaseOrders exist with Shipping date:
      | Code       | ShippingDate         |
      | 2018000018 | 07-Aug-2018 09:02 AM |

  #### Happy Path
  # EBS-1655
  @ResetData
  Scenario Outline: (01) MarkAsDeliveryComplete PO Local/Import PO with all mandatory fields - by an authorized user with one/two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "<POCode>" is updated with DeliveryCompleteDate as follows:
      | LastUpdatedBy | LastUpdateDate             | State            | DeliveryCompleteDate       |
      | <User>        | 07-Jan-2019 07:30 AM +0000 | DeliveryComplete | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-40"

    Examples:
      | POCode     | Type      | State   | PurchasingUnit | User        |
      | 2018000018 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed |
      | 2018000023 | IMPORT_PO | Cleared | 0002           | Gehan.Ahmed |
      | 2018000003 | IMPORT_PO | Cleared | 0001           | Amr.Khalil  |
      | 2018000045 | IMPORT_PO | Cleared | 0006           | Amr.Khalil  |

  ###### Exceptions
  # EBS-1655
  Scenario Outline: (02) MarkAsDeliveryComplete PO for Import/Local PO that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-Jan-2019 9:20 AM"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-14"

    Examples:
      | POSection | POCode     | POType    | User        |
      | Company   | 2018000003 | IMPORT_PO | Amr.Khalil  |
      | Company   | 2018000018 | LOCAL_PO  | Gehan.Ahmed |

  # EBS-1655
  Scenario Outline: (03) MarkAsDeliveryComplete PO for Local/Import PO that has draft GR OR has no GR at all (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "<User>" with the following message "PO-msg-41"

    Examples:
      | POCode     | Type      | State   | PurchasingUnit | User        |
      | 2018000047 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed |
      | 2018000051 | IMPORT_PO | Cleared | 0002           | Gehan.Ahmed |
      | 2018000049 | IMPORT_PO | Cleared | 0002           | Gehan.Ahmed |
      | 2018000048 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed |

  # EBS-3522
  Scenario Outline: (04) MarkAsDeliveryComplete PO for Local/Import PO in a state that doesn't allow this action (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

    Examples:
      | POCode     | Type      | State            | PurchasingUnit |
      | 2018000002 | IMPORT_PO | DeliveryComplete | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled        | 0002           |

  # EBS-3522
  Scenario Outline: (05) MarkAsDeliveryComplete PO for Local/Import PO with missing mandattory fields
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-33"
    And the following fields "<Missing Fields>" which sent to "Gehan.Ahmed" are marked as missing

    Examples:
      | POCode     | Missing Fields                             |
      | 2018000046 | consigneeCode , plantCode , storehouseCode |
      | 2018000050 | consigneeCode , plantCode , storehouseCode |

  #### Abuse Cases
  # EBS-3522
  Scenario Outline: (06) MarkAsDeliveryComplete PO for Local/Import PO in Draft state (Abuse Case -- becoz an shipped/Cleared PO cannot return to be draft by any means)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | POCode     | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           |
      | 2018000014 | LOCAL_PO  | OpenForUpdates     | 0002           |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           |

  # EBS-1655
  Scenario: (07) MarkAsDeliveryComplete PO for Local/Import PO that is doesn't exist (Abuse Case -- cannot happen normally becoz an shipped/Cleared PO cannot be deleted)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "9999999999" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  # EBS-1655
  Scenario Outline: (08) MarkAsDeliveryComplete PO Local/Import PO with missing or malicious DeliveryCompleteDate (Abuse case -- becoz user cannot send the request unless the  Delivery Complete date is entered correctly)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate   |
      | <DeliveryCompleteDate> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | DeliveryCompleteDate | POCode     | User        |
      |                      | 2018000003 | Amr.Khalil  |
      | Ay 7aga              | 2018000003 | Amr.Khalil  |
      | ""                   | 2018000003 | Amr.Khalil  |
      |                      | 2018000018 | Gehan.Ahmed |
      | Ay 7aga              | 2018000018 | Gehan.Ahmed |
      | ""                   | 2018000018 | Gehan.Ahmed |

  # EBS-1655
  Scenario Outline: (09) MarkAsDeliveryComplete PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate |
      | 06-Jan-2019 11:30 AM |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | POCode     | User              |
      | 2018000018 | Mahmoud.Abdelaziz |
      | 2018000018 | Amr.Khalil        |
      | 2018000003 | Mahmoud.Abdelaziz |
      | 2018000003 | Gehan.Ahmed       |

  ######################################################## MarkAsDeliveryComplete PO with ProductionFinishedDate before confirmation date  ###############################
  Scenario Outline: (10) MarkAsDeliveryComplete PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsDeliveryComplete the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | DeliveryCompleteDate   |
      | <DeliveryCompleteDate> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to DeliveryComplete date field "PO-msg-59" and sent to "<User>"
    Examples:
      | POCode     | Type      | State   | PurchasingUnit | User        | DeliveryCompleteDate |
      | 2018000018 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed | 06-Jan-2017 11:30 AM |
      | 2018000018 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed | 07-Aug-2018 09:02 AM |
      | 2018000018 | LOCAL_PO  | Shipped | 0002           | Gehan.Ahmed | 07-Aug-2018 09:00 AM |
      | 2018000045 | IMPORT_PO | Cleared | 0006           | Amr.Khalil  | 06-Jan-2017 11:30 AM |
      | 2018000023 | IMPORT_PO | Cleared | 0002           | Gehan.Ahmed | 07-Aug-2018 09:02 AM |
      | 2018000003 | IMPORT_PO | Cleared | 0001           | Amr.Khalil  | 07-Aug-2018 09:00 AM |
