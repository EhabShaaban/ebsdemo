# Reviewer: Marina (28-Jan-2019 12:10 PM)
# Reviewer: Somaya (29-Jan-2019 05:30 PM)


Feature: View Attachments in PO

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia        |
      | PurchasingResponsible_Flexo      | POViewer_Flexo            |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated       |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia |
      | SuperUser                        | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                    | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadAttachments | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadAttachments | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadAttachments | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                           |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000025 | Draft | 0001           |
      | 2018000026 | Draft | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
      | 2018000027 | Draft | 0001           |
      | 2018000028 | Draft | 0006           |
    And the following Attachments section exist in PurchaseOrder with code "2018000021":
      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
      | 0005             | myfile.pdf | Amr.Khalil  | 21-Jan-2017 11:00 am |
      | 0004             | myfile.doc | Gehan.Ahmed | 21-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000025":
      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
      | 0005             | myfile.png | Amr.Khalil  | 25-Jan-2017 11:00 am |
      | 0004             | myfile.jpg | Gehan.Ahmed | 25-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000026":
      | DocumentTypeCode | Attachment  | UploadedBy        | UploadDateTime       |
      | 0005             | myfile.jpeg | Mahmoud.Abdelaziz | 26-Jan-2017 11:00 am |
      | 0004             | myfile.docx | Amr.Khalil        | 26-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000013":
      | DocumentTypeCode | Attachment | UploadedBy        | UploadDateTime       |
      | 0005             | myfile.odt | Mahmoud.Abdelaziz | 13-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Gehan.Ahmed       | 13-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000027":
      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
      | 0005             | myfile.pdf | Ashraf.Fathi | 27-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Amr.Khalil   | 27-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000028":
      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
      | 0005             | myfile.pdf | Ashraf.Fathi | 28-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Gehan.Ahmed  | 28-Jan-2017 11:00 am |
    And the following DocumentType exist:
      | DocumentTypeCode | DocumentTypeName                 |
      | 0004             | Bill of Lading (BL)              |
      | 0005             | Packing List for the Whole Order |
    And the following user information exist:
      | UserName          | ProfileName       |
      | Amr.Khalil        | Amr Khalil        |
      | Gehan.Ahmed       | Gehan Ahmed       |
      | Mahmoud.Abdelaziz | Mahmoud Abdelaziz |
      | Ashraf.Fathi      | Ashraf Fathi      |

  ##### Happy Paths ###
  #EBS-1043
  Scenario: (01) View Attachments with User has one role and PO is Import & Draft
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Attachments section of PurchaseOrder with code "2018000021"
    Then the following values of Attachments section are displayed to "Gehan.Ahmed":
      | DocumentTypeName                 | Attachment | UploadedBy  | UploadDateTime       |
      | Packing List for the Whole Order | myfile.pdf | Amr Khalil  | 21-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.doc | Gehan Ahmed | 21-Jan-2017 11:00 am |

  #EBS-1043
  Scenario: (02) View Attachments with User has one role and PO is Local & Draft
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Attachments section of PurchaseOrder with code "2018000013"
    Then the following values of Attachments section are displayed to "Gehan.Ahmed":
      | DocumentTypeName                 | Attachment | UploadedBy        | UploadDateTime       |
      | Packing List for the Whole Order | myfile.odt | Mahmoud Abdelaziz | 13-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.pdf | Gehan Ahmed       | 13-Jan-2017 11:00 am |

  #EBS-1043
  Scenario: (03) View Attachments with User has two roles and PO is Import & Draft & Flexo
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Attachments section of PurchaseOrder with code "2018000025"
    Then the following values of Attachments section are displayed to "Amr.Khalil":
      | DocumentTypeName                 | Attachment | UploadedBy  | UploadDateTime       |
      | Packing List for the Whole Order | myfile.png | Amr Khalil  | 25-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.jpg | Gehan Ahmed | 25-Jan-2017 11:00 am |

  #EBS-1043
  Scenario: (04) View Attachments with User has two roles and PO is Import & Draft & Corrugated
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Attachments section of PurchaseOrder with code "2018000026"
    Then the following values of Attachments section are displayed to "Amr.Khalil":
      | DocumentTypeName                 | Attachment  | UploadedBy        | UploadDateTime       |
      | Packing List for the Whole Order | myfile.jpeg | Mahmoud Abdelaziz | 26-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.docx | Amr Khalil        | 26-Jan-2017 11:00 am |

  #EBS-1043
  Scenario: (05) View Attachments with User has two roles and PO is Local & Draft & Flexo
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Attachments section of PurchaseOrder with code "2018000027"
    Then the following values of Attachments section are displayed to "Amr.Khalil":
      | DocumentTypeName                 | Attachment | UploadedBy   | UploadDateTime       |
      | Packing List for the Whole Order | myfile.pdf | Ashraf Fathi | 27-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.pdf | Amr Khalil   | 27-Jan-2017 11:00 am |

  #EBS-1043
  Scenario: (06) View Attachments with User has two roles and PO is Local & Draft & Corrugated
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Attachments section of PurchaseOrder with code "2018000028"
    Then the following values of Attachments section are displayed to "Amr.Khalil":
      | DocumentTypeName                 | Attachment | UploadedBy   | UploadDateTime       |
      | Packing List for the Whole Order | myfile.pdf | Ashraf Fathi | 28-Jan-2017 11:00 am |
      | Bill of Lading (BL)              | myfile.pdf | Gehan Ahmed  | 28-Jan-2017 11:00 am |

  #### Exceptions Cases ####
  #EBS-1043
  @ResetData
  Scenario Outline: (07) View Attachments where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view Attachments section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  #### Abuse Cases  ####
  #EBS-1166
  Scenario Outline: (08) View Attachments by an unauthorized user due to condition OR not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Attachments section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000013 | Mahmoud.Abdelaziz |
      | 2018000021 | Mahmoud.Abdelaziz |
      | 2018000013 | Amr.Khalil        |
      | 2018000021 | Amr.Khalil        |
