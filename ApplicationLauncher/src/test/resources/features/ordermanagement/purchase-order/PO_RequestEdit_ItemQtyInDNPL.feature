# Authors: Yara Ameen & Waseem Salama

Feature: Request Edit / Cancel Item Qty In DN/PL in PO

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                    |
      | LogisticsResponsible_Signmedia | POLogisticsOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                     | Condition                      |
      | POLogisticsOwner_Signmedia | PurchaseOrder:AddPL/DNQuantity | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000023 | Cleared            | 0002           |
      | 2018100000 | Draft              | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000029 | OpenForUpdates     | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018100001 | Draft            | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000031 | OpenForUpdates   | 0006           |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000001 | 000001   |         |
      | 2018000002 | 000003   |         |
      | 2018000005 | 000001   |         |
      | 2018000006 | 000002   |         |
      | 2018000006 | 000003   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000025 | 000005   |         |
      | 2018000026 | 000012   |         |
      | 2018100002 | 000001   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
      | 2018000014 | 000003   |         |
      | 2018000015 | 000001   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000027 | 000006   |         |
      | 2018000028 | 000012   |         |
      | 2018100003 | 000001   |         |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000002 | 000003   |         |
      | 2018000005 | 000001   |         |
      | 2018000006 | 000003   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000023 | 000002   | 18000   |
      | 2018000029 | 000012   | 2000.5  |
      | 2018100000 | 000012   | 2000.5  |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000014 | 000003   |         |
      | 2018000015 | 000001   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000031 | 000012   | 2000.5  |
      | 2018100001 | 000012   | 2000.5  |
    And edit session is "30" minutes

  ######## Request Edit Happy Paths
  # EBS-2872
  Scenario Outline: (01) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then edit Qty in DN/PL dialog is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    Examples:
      | User        | POCode     | ItemCode | State              | Type   |
      | Gehan.Ahmed | 2018000005 | 000001   | Approved           | Import |
      | Gehan.Ahmed | 2018000006 | 000003   | OpenForUpdates     | Import |
      | Gehan.Ahmed | 2018000007 | 000001   | WaitingApproval    | Import |
      | Gehan.Ahmed | 2018000008 | 000001   | Confirmed          | Import |
      | Gehan.Ahmed | 2018000009 | 000001   | FinishedProduction | Import |
      | Gehan.Ahmed | 2018000010 | 000002   | Shipped            | Import |
      | Gehan.Ahmed | 2018000011 | 000001   | Arrived            | Import |
      | Gehan.Ahmed | 2018000021 | 000002   | Draft              | Import |
      | Gehan.Ahmed | 2018000023 | 000002   | Cleared            | Import |

      | Gehan.Ahmed | 2018000013 | 000001   | Draft              | Local  |
      | Gehan.Ahmed | 2018000014 | 000003   | OpenForUpdates     | Local  |
      | Gehan.Ahmed | 2018000015 | 000001   | WaitingApproval    | Local  |
      | Gehan.Ahmed | 2018000016 | 000001   | Approved           | Local  |
      | Gehan.Ahmed | 2018000017 | 000001   | Confirmed          | Local  |
      | Gehan.Ahmed | 2018000018 | 000001   | Shipped            | Local  |

  ######## Request Edit Exception Cases
  # EBS-2872
  @ResetData
  Scenario Outline: (02) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, where PO doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     | ItemCode | State | Type   |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  |

  # EBS-2872
  @ResetData
  Scenario Outline: (03) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, where Item doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | User        | POCode     | ItemCode | State | Type   |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  |

  # EBS-2872
  Scenario Outline: (04) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, in state where this action is not allowed (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    Examples:
      | POCode     | ItemCode | State            | Type   |
      | 2018000002 | 000003   | DeliveryComplete | Import |
      | 2018000012 | 000001   | Cancelled        | Import |
      | 2018000019 | 000001   | DeliveryComplete | Local  |
      | 2018000020 | 000001   | Cancelled        | Local  |

  # EBS-2872
  Scenario Outline: (05) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, when OrderItems section is locked by another user - Import/Local PurchaseOrder (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-02"
    Examples:
      | User        | POCode     | ItemCode | State    | Type   |
      | Gehan.Ahmed | 2018000005 | 000001   | Approved | Import |
      | Gehan.Ahmed | 2018000013 | 000001   | Draft    | Local  |

  ######## Request Edit Abuse Cases
  # EBS-2872
  Scenario Outline: (06) Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder, - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     | ItemCode | State          | Type   |
      | Gehan.Ahmed | 2018000029 | 000012   | OpenForUpdates | Import |
      | Gehan.Ahmed | 2018000031 | 000012   | OpenForUpdates | Local  |
      | Afaf        | 2018000031 | 000012   | OpenForUpdates | Local  |

  ######## Request Cancel Happy Paths
  # EBS-2872
  Scenario Outline: (07) Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder, (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode
    When "<User>" requests to cancel Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then edit Qty in DN/PL dialog is closed and the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    Examples:
      | User        | POCode     | ItemCode | State              | Type   |
      | Gehan.Ahmed | 2018000005 | 000001   | Approved           | Import |
      | Gehan.Ahmed | 2018000006 | 000003   | OpenForUpdates     | Import |
      | Gehan.Ahmed | 2018000007 | 000001   | WaitingApproval    | Import |
      | Gehan.Ahmed | 2018000008 | 000001   | Confirmed          | Import |
      | Gehan.Ahmed | 2018000009 | 000001   | FinishedProduction | Import |
      | Gehan.Ahmed | 2018000010 | 000002   | Shipped            | Import |
      | Gehan.Ahmed | 2018000011 | 000001   | Arrived            | Import |
      | Gehan.Ahmed | 2018000021 | 000002   | Draft              | Import |
      | Gehan.Ahmed | 2018000023 | 000002   | Cleared            | Import |

      | Gehan.Ahmed | 2018000013 | 000001   | Draft              | Local  |
      | Gehan.Ahmed | 2018000014 | 000003   | OpenForUpdates     | Local  |
      | Gehan.Ahmed | 2018000015 | 000001   | WaitingApproval    | Local  |
      | Gehan.Ahmed | 2018000016 | 000001   | Approved           | Local  |
      | Gehan.Ahmed | 2018000017 | 000001   | Confirmed          | Local  |
      | Gehan.Ahmed | 2018000018 | 000001   | Shipped            | Local  |

  ######## Request Edit Exception Cases
  # EBS-2872
  @ResetData
  Scenario Outline: (08) Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder, where PO doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" requests to cancel Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 9:42 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     | ItemCode | State | Type   |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  |

  # EBS-2872
  @ResetData
  Scenario Outline: (09) Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder, where Item doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" requests to cancel Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 9:42 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | User        | POCode     | ItemCode | State | Type   |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  |

  # EBS-2872
  Scenario Outline: (10) Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder, after edit session expire (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" requests to cancel Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 9:41 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | User        | POCode     | ItemCode | State    | Type   |
      | Gehan.Ahmed | 2018000005 | 000001   | Approved | Import |
      | Gehan.Ahmed | 2018000013 | 000001   | Draft    | Local  |

  ######## Request Edit Abuse Cases
  # EBS-2872
  Scenario Outline: (11) Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder, - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to cancel Item Qty In DN/PL Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     | ItemCode | State          | Type   |
      | Gehan.Ahmed | 2018000029 | 000012   | OpenForUpdates | Import |
      | Gehan.Ahmed | 2018000031 | 000012   | OpenForUpdates | Local  |
      | Afaf        | 2018000031 | 000012   | OpenForUpdates | Local  |
