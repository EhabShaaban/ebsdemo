# Author : Hend Ahmed & Nancy - 4-2-2019
Feature: PO_Export_PDF_Header

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |

    And the following roles and sub-roles exist:
      | Role                             | Sub-role           |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:ExportPDF | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:ExportPDF | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:ExportPDF | [purchaseUnitName='Corrugated'] |

    And the following Countries exist:
      | Code | Name  |
      | 0002 | Egypt |
      | 0003 | China |

    And the following Cities exist:
      | Code | Name   |
      | 0002 | Harbin |
      | 0005 | Cairo  |

    And the following Companies exist with the following data :
      | Code | Name      | Address             | PostalCode | Country | City | Telephone      | Fax            |
      | 0001 | AL Madina | 70, Salah Salem St. | 11455      | 0002    | 0005 | +202 2531 6666 | +202 2531 4567 |
      | 0002 | DigiPro   | 66, Salah Salem St. | 11451      | 0003    | 0002 | +202 2531 3955 | +202 2531 3954 |

    And the following vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000002 | Zhejiang | 0002           |

    And the following ModeOfTransports exist:
      | Code | Name   |
      | 0002 | By Sea |

    And the following Incoterms exist:
      | Code | Name |
      | 0002 | CIF  |

    And the following Ports exist:
      | Code | Name                | Country |
      | 0001 | Alexandria old port | Egypt   |

    And the following PaymentTerms exist:
      | Code | Name                         |
      | 0002 | 20% advance, 80% Copy of B/L |

    And the following PurchaseOrders exist with the following data:
      | Code       | Type      | State | Company | Consignee | Vendor | ModeOfTransport | Incoterm | DischargePort | PaymentTerms | CollectionDate |
      | 2018000021 | IMPORT_PO | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |
      | 2018000025 | IMPORT_PO | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |
      | 2018000026 | IMPORT_PO | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |
      | 2018000013 | LOCAL_PO  | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |
      | 2018000027 | LOCAL_PO  | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |
      | 2018000028 | LOCAL_PO  | Draft | 0002    | 0001      | 000002 | 0002            | 0002     | 0001          | 0002         | 06-Oct-2020    |

  Scenario Outline: (01) Request to export PO Header to PDF (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to export PurchaseOrder header of PurchaseOrder with code "<POCode>" as PDF
    Then the following values of PurchaseOrder Company data are displayed to "<User>":
      | POCode   | Company   | CompanyAddress   | CompanyCountry   | CompanyCity   | CompanyPostalCode   | CompanyTelephone   | CompanyFax   |
      | <POCode> | <Company> | <CompanyAddress> | <CompanyCountry> | <CompanyCity> | <CompanyPostalCode> | <CompanyTelephone> | <CompanyFax> |
    Then the following values of PurchaseOrder Consignee data are displayed to "<User>":
      | POCode   | Consignee   | ConsigneeAddress   | ConsigneeCountry   | ConsigneeCity   | ConsigneePostalCode   | ConsigneeTelephone   | ConsigneeFax   |
      | <POCode> | <Consignee> | <ConsigneeAddress> | <ConsigneeCountry> | <ConsigneeCity> | <ConsigneePostalCode> | <ConsigneeTelephone> | <ConsigneeFax> |
    Then the following values of PurchaseOrder Vendor data are displayed to "<User>":
      | POCode   | Vendor   |
      | <POCode> | <Vendor> |
    Then the following values of PurchaseOrder PaymentAndDelivery data are displayed to "<User>":
      | Incoterm   | DischargePort   | PaymentTerms   | ModeOfTransport   | CollectionDate   |
      | <Incoterm> | <DischargePort> | <PaymentTerms> | <ModeOfTransport> | <CollectionDate> |
    Examples:
      | User        | POCode     | Company | CompanyAddress      | CompanyCountry | CompanyCity | CompanyPostalCode | CompanyTelephone | CompanyFax     | Consignee | ConsigneeAddress    | ConsigneeCountry | ConsigneeCity | ConsigneePostalCode | ConsigneeTelephone | ConsigneeFax   | Vendor   | Incoterm | DischargePort       | PaymentTerms                 | ModeOfTransport | CollectionDate |
      | Gehan.Ahmed | 2018000021 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |
      | Gehan.Ahmed | 2018000013 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |
      | Amr.Khalil  | 2018000025 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |
      | Amr.Khalil  | 2018000026 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |
      | Amr.Khalil  | 2018000027 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |
      | Amr.Khalil  | 2018000028 | DigiPro | 66, Salah Salem St. | China          | Harbin      | 11451             | +202 2531 3955   | +202 2531 3954 | AL Madina | 70, Salah Salem St. | Egypt            | Cairo         | 11455               | +202 2531 6666     | +202 2531 4567 | Zhejiang | CIF      | Alexandria old port | 20% advance, 80% Copy of B/L | By Sea          | 06-Oct-2020    |