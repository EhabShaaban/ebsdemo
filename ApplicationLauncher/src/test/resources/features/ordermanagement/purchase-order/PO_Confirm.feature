# Author: Marina Salah & Shrouk Alaa, 12-JAN-2019 11:35 AM
# Author Dev: Order Team (15-Feb-2021)
# Reviewer: Somaya Ahmed (14-Jan-2019 04:00 PM)
# Author & Reviewer: Order Team (01-Mar-2021) Validation Scenarios
Feature: Mark PO as Confirmed

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission            | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:Confirm | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:Confirm | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:Confirm | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                   |                                 |
    And the following users doesn't have the following permissions:
      | User              | Permission            |
      | Mahmoud.Abdelaziz | PurchaseOrder:Confirm |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State                                             | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000001 | IMPORT_PO  | Active,Cleared,NotReceived                        | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | Active,DeliveryComplete,Received                  | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000005 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000006 | IMPORT_PO  | Active,InOperation,OpenForUpdates,NotReceived     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000007 | IMPORT_PO  | Active,InOperation,WaitingApproval,NotReceived    | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Active,InOperation,Confirmed,NotReceived          | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000009 | IMPORT_PO  | Active,InOperation,FinishedProduction,NotReceived | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Active,InOperation,Shipped,NotReceived            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Active,Arrived,NotReceived                        | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO  | Inactive,Cancelled                                | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000013 | LOCAL_PO   | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000014 | LOCAL_PO   | Active,InOperation,OpenForUpdates,NotReceived     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000015 | LOCAL_PO   | Active,InOperation,WaitingApproval,NotReceived    | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000016 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000017 | LOCAL_PO   | Active,InOperation,Confirmed,NotReceived          | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000018 | LOCAL_PO   | Active,InOperation,Shipped,NotReceived            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO   | Active,DeliveryComplete,Received                  | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000020 | LOCAL_PO   | Inactive,Cancelled                                | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000026 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000034 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000035 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000036 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000037 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000004 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Confirmed                                         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000007 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000008 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000001 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000002 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000005 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000006 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000007 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000008 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000009 | 0002 - Signmedia  | 0001 - AL Madina |                                 |
      | 2018000010 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000011 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000012 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000014 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000015 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000016 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000017 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000018 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000019 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000020 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000034 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000035 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000036 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000037 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000007 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000008 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor                     | ReferencePO |
      | 2018000001 | 000002 - Zhejiang          |             |
      | 2018000002 | 000002 - Zhejiang          |             |
      | 2018000005 | 000002 - Zhejiang          |             |
      | 2018000006 | 000002 - Zhejiang          |             |
      | 2018000007 | 000002 - Zhejiang          |             |
      | 2018000008 | 000002 - Zhejiang          |             |
      | 2018000009 | 000002 - Zhejiang          |             |
      | 2018000010 | 000002 - Zhejiang          |             |
      | 2018000011 | 000002 - Zhejiang          |             |
      | 2018000012 | 000002 - Zhejiang          |             |
      | 2018000013 | 000002 - Zhejiang          |             |
      | 2018000014 | 000002 - Zhejiang          |             |
      | 2018000015 | 000002 - Zhejiang          |             |
      | 2018000016 | 000002 - Zhejiang          |             |
      | 2018000017 | 000002 - Zhejiang          |             |
      | 2018000018 | 000002 - Zhejiang          |             |
      | 2018000019 | 000002 - Zhejiang          |             |
      | 2018000020 | 000002 - Zhejiang          |             |
      | 2018000021 | 000002 - Zhejiang          |             |
      | 2018000025 | 000001 - Siegwerk          |             |
      | 2018000026 | 000006 - Vendor Corrugated |             |
      | 2018000034 | 000001 - Siegwerk          |             |
      | 2018000035 | 000006 - Vendor Corrugated |             |
      | 2018000036 | 000001 - Siegwerk          |             |
      | 2018000037 | 000006 - Vendor Corrugated |             |
      | 2020000004 | 000002 - Zhejiang          | 2018000001  |
      | 2020000006 | 000002 - Zhejiang          | 2018000018  |
      | 2020000007 | 000002 - Zhejiang          | 2018000012  |
      | 2020000008 | 000002 - Zhejiang          | 2018000001  |
    #@INSERT
    And the following CycleDates for PurchaseOrders exist:
      | Code       | PIRequestDate        | ConfirmationDate     | ProductionFinishedDate | ShippingDate         | ArrivalDate          | ClearanceDate        | DeliveryCompleteDate |
      | 2018000001 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM | 09-Aug-2018 09:07 AM | 09-Aug-2018 10:07 AM |                      |
      | 2018000002 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM | 09-Aug-2018 09:07 AM | 09-Aug-2018 10:07 AM | 10-Aug-2018 10:07 AM |
      | 2018000005 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000006 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000007 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000008 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM |                        |                      |                      |                      |                      |
      | 2018000009 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   |                      |                      |                      |                      |
      | 2018000010 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM |                      |                      |                      |
      | 2018000011 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM | 09-Aug-2018 09:07 AM |                      |                      |
      | 2018000014 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000015 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000016 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000017 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM |                        |                      |                      |                      |                      |
      | 2018000018 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM |                      |                      |                      |
      | 2018000019 | 07-Aug-2018 09:02 AM | 07-Aug-2018 09:07 AM | 08-Aug-2018 09:07 AM   | 08-Aug-2018 09:10 AM | 09-Aug-2018 09:07 AM | 09-Aug-2018 10:07 AM | 10-Aug-2018 10:07 AM |
      | 2018000034 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000035 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000036 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2018000037 | 07-Aug-2018 09:02 AM |                      |                        |                      |                      |                      |                      |
      | 2020000006 |                      | 07-Aug-2020 09:02 AM |                        |                      |                      |                      |                      |
    #@INSERT
    And the following ApprovalCycles for PurchaseOrders exist:
      | Code       | CycleNumber | StartDate            | EndDate              | FinalDecision |
      | 2018000005 | 1           | 21-Sep-2018 11:00 AM | 21-Sep-2018 11:15 AM | REJECTED      |
      | 2018000005 | 2           | 21-Sep-2018 03:00 PM | 21-Sep-2018 05:00 PM | APPROVED      |
      | 2018000007 | 1           | 21-Sep-2018 11:00 AM | 21-Sep-2018 11:15 AM |               |
      | 2018000008 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000009 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000010 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000011 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000016 | 1           | 20-Sep-2018 03:00 PM | 20-Sep-2018 03:15 PM | REJECTED      |
      | 2018000016 | 2           | 21-Sep-2018 11:00 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000017 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000018 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000034 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000035 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000036 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
      | 2018000037 | 1           | 07-Jan-2019 11:20 AM | 21-Sep-2018 01:00 PM | APPROVED      |
    #@INSERT
    And the following PaymentTermData for PurchaseOrders exist:
      | Code       | Currency   | PaymentTerms                        |
      | 2020000004 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2020000006 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2020000007 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2020000008 |            |                                     |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
      | 2020000006 | 2        | 000057 - Bank Fees          | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
      | 2020000007 | 3        | 000057 - Bank Fees          | 0019 - M2 | 20.000   | 200.000 | 4000.000        |

  ###### Happy Paths
  # EBS-1170
  Scenario Outline: (01) MarkAsConfirmed PO Approved Local/Import and Draft Service PO by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "<POCode>" is updated with ConfirmationDate as follows:
      | LastUpdatedBy | LastUpdateDate             | State     | ConfirmationDate           |
      | <User>        | 07-Jan-2019 07:30 AM +0000 | Confirmed | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-34"
    Examples:
      | POCode     | Type       | State    | PurchasingUnit | User        |
      | 2018000005 | IMPORT_PO  | Approved | 0002           | Gehan.Ahmed |
      | 2018000016 | LOCAL_PO   | Approved | 0002           | Gehan.Ahmed |
      | 2018000034 | IMPORT_PO  | Approved | 0001           | Amr.Khalil  |
      | 2018000035 | IMPORT_PO  | Approved | 0006           | Amr.Khalil  |
      | 2018000036 | LOCAL_PO   | Approved | 0001           | Amr.Khalil  |
      | 2018000037 | LOCAL_PO   | Approved | 0006           | Amr.Khalil  |
      | 2020000004 | SERVICE_PO | Draft    | 0002           | Gehan.Ahmed |

  ###### Exceptions
  # EBS-1170
  # EBS-2206
  Scenario Outline: (02) MarkAsConfirmed PO Local/Import/Service PO in a state that doesn't allow this action - any state except the Approved state in Local/Import and Draft in Service (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000014 | OpenForUpdates     | 0002           |
      | 2018000015 | WaitingApproval    | 0002           |
      | 2018000017 | Confirmed          | 0002           |
      | 2018000018 | Shipped            | 0002           |
      | 2018000019 | DeliveryComplete   | 0002           |
      | 2018000020 | Cancelled          | 0002           |
      | 2020000006 | Confirmed          | 0002           |

  # EBS-1170
  Scenario Outline: (03) MarkAsConfirmed PO Local/Import PO in Draft state (Abuse Case -- because an approved PO cannot return to be draft by any means)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

  # EBS-1170
  # EBS-2206
  Scenario Outline: (04) MarkAsConfirmed PO Approved Import/Local/Service PO that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-Jan-2019 09:20 AM"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | POSection | POCode     |
      | Company   | 2018000005 |
      | Company   | 2018000016 |
      | Company   | 2020000004 |

  # EBS-2206
  Scenario: (05) MarkAsConfirmed PO Draft Service PO that is doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "2020000004" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-2206
  Scenario: (06) MarkAsConfirmed PO Draft Service PO when related PO in Cancelled state (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "2020000007" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-53"

  # EBS-1170
  # EBS-2206
  Scenario Outline: (07) MarkAsConfirmed PO Approved Local/Import/Service PO with missing or malicious ConfirmationDate (Abuse case -- because user cannot send the request unless the confirmation date is entered correctly)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate   |
      | <ConfirmationDate> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ConfirmationDate | POCode     |
      |                  | 2018000005 |
      | Ay 7aga          | 2018000005 |
      | N/A              | 2018000005 |
      |                  | 2018000016 |
      | Ay 7aga          | 2018000016 |
      | N/A              | 2018000016 |
      |                  | 2020000004 |
      | Ay 7aga          | 2020000004 |
      | N/A              | 2020000004 |

  # EBS-2206
  Scenario Outline: (08) MarkAsConfirmed PO in Draft state (Service) with missing mandatory fields
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-40"
    And the following fields "<Missing Fields>" which sent to "Gehan.Ahmed" are marked as missing
    Examples:
      | POCode     | Missing Fields                               |
      | 2020000008 | currencyIso, paymentTermCode, OrderItemsList |

  # EBS-1170
  # EBS-1426
  Scenario Outline: (09) MarkAsConfirmed PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate     |
      | 06-Jan-2019 11:30 AM |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000005 | Mahmoud.Abdelaziz |
      | 2018000016 | Mahmoud.Abdelaziz |
      | 2018000005 | Amr.Khalil        |
      | 2018000016 | Amr.Khalil        |
      | 2020000004 | Amr.Khalil        |

  ######################################################## MarkAsConfirmed PO with confirmation date before approval date ###############################
  Scenario Outline: (10) MarkAsConfirmed PO with confirmation date before approval date (validation error)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsConfirmed the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ConfirmationDate   |
      | <ConfirmationDate> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to confirmation date field "PO-msg-59" and sent to "<User>"
    Examples:
      | POCode     | Type      | State    | PurchasingUnit | User        | ConfirmationDate     |
      | 2018000005 | IMPORT_PO | Approved | 0002           | Gehan.Ahmed | 06-Jan-2017 11:30 AM |
      | 2018000016 | LOCAL_PO  | Approved | 0002           | Gehan.Ahmed | 21-Sep-2018 11:00 AM |
      | 2018000034 | IMPORT_PO | Approved | 0001           | Amr.Khalil  | 21-Sep-2018 10:58 AM |
      | 2018000035 | IMPORT_PO | Approved | 0006           | Amr.Khalil  | 06-Jan-2017 11:30 AM |
      | 2018000036 | LOCAL_PO  | Approved | 0001           | Amr.Khalil  | 21-Sep-2018 11:00 AM |
      | 2018000037 | LOCAL_PO  | Approved | 0006           | Amr.Khalil  | 21-Sep-2018 10:58 AM |
