Feature: Request Edit / Cancel Item Quantities in PO

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Amr.Khalil             | PurchasingResponsible_Flexo                            |
      | Amr.Khalil             | PurchasingResponsible_Corrugated                       |
      | Mahmoud.Abdelaziz      | Storekeeper_Signmedia                                  |
      | hr1                    | SuperUser                                              |
      | Gehan.Ahmed.NoMeasures | PurchasingResponsible_Signmedia_CannotReadMeasuresRole |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole            |
      | PurchasingResponsible_Signmedia                        | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo                            | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated                       | POOwner_Corrugated |
      | SuperUser                                              | SuperUserSubRole   |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole | POOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                      |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000023 | Cleared            | 0002           |
      | 2018000029 | OpenForUpdates     | 0006           |
      | 2018000104 | Draft              | 0001           |
      | 2018100000 | Draft              | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000030 | OpenForUpdates   | 0001           |
      | 2018000031 | OpenForUpdates   | 0006           |
      | 2018100001 | Draft            | 0002           |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000002 | 000003   | 4     |
      | 2018000005 | 000001   | 50    |
      | 2018000006 | 000003   | 46    |
      | 2018000007 | 000001   | 51    |
      | 2018000008 | 000001   | 52    |
      | 2018000009 | 000001   | 53    |
      | 2018000010 | 000002   | 54    |
      | 2018000011 | 000001   | 55    |
      | 2018000012 | 000001   | 56    |
      | 2018000021 | 000002   | 18    |
      | 2018000023 | 000002   | 10    |
      | 2018000029 | 000012   | 42    |
      | 2018000104 | 000005   | 48    |
      | 2018100000 | 000012   | 44    |
      # Local
      | 2018000013 | 000001   | 20    |
      | 2018000014 | 000003   | 47    |
      | 2018000015 | 000001   | 57    |
      | 2018000016 | 000001   | 58    |
      | 2018000017 | 000001   | 59    |
      | 2018000018 | 000001   | 26    |
      | 2018000019 | 000001   | 60    |
      | 2018000020 | 000001   | 61    |
      | 2018000030 | 000006   | 49    |
      | 2018000031 | 000012   | 43    |
      | 2018100001 | 000012   | 45    |
    And edit session is "30" minutes

  Scenario Outline: (01) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then edit Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    Examples:
      | POCode     | ItemCode | State          | User        | Type   | QtyID |
      | 2018000021 | 000002   | Draft          | Gehan.Ahmed | Import | 18    |
      | 2018000013 | 000001   | Draft          | Gehan.Ahmed | Local  | 20    |
      | 2018000104 | 000005   | Draft          | Amr.Khalil  | Import | 48    |
      | 2018000029 | 000012   | OpenForUpdates | Amr.Khalil  | Import | 42    |

  @ResetData
  Scenario Outline: (02) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, where PO doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     | ItemCode | State | Type   | QtyID |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import | 44    |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  | 45    |

  @ResetData
  Scenario Outline: (03) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, where Item doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | User        | POCode     | ItemCode | State | Type   | QtyID |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import | 44    |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  | 45    |

  Scenario Outline: (04) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, in state where this action is not allowed (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    Examples:
      | POCode     | State              | ItemCode | QtyID |
      # Import
      | 2018000002 | DeliveryComplete   | 000003   | 4     |
      | 2018000005 | Approved           | 000001   | 50    |
      | 2018000007 | WaitingApproval    | 000001   | 51    |
      | 2018000008 | Confirmed          | 000001   | 52    |
      | 2018000009 | FinishedProduction | 000001   | 53    |
      | 2018000010 | Shipped            | 000002   | 54    |
      | 2018000011 | Arrived            | 000001   | 55    |
      | 2018000012 | Cancelled          | 000001   | 56    |
      | 2018000023 | Cleared            | 000002   | 10    |
      # Local
      | 2018000015 | WaitingApproval    | 000001   | 57    |
      | 2018000016 | Approved           | 000001   | 58    |
      | 2018000017 | Confirmed          | 000001   | 59    |
      | 2018000018 | Shipped            | 000001   | 26    |
      | 2018000019 | DeliveryComplete   | 000001   | 60    |
      | 2018000020 | Cancelled          | 000001   | 61    |

  # EBS-2872
  Scenario: (05) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, when OrderItems section is locked by another user - Import/Local PurchaseOrder (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" opened Item Quantity with id 20 for Item with code "000001" to OrderItems section of PurchaseOrder with code "2018000013" in edit mode at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" requests to Edit Item Quantity with id 20 for Item with code "000001" to OrderItems section of PurchaseOrder with code "2018000013" at "07-Jan-2019 09:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario Outline: (06) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode | QtyID |
      | Gehan.Ahmed       | 2018000029 | 000012   | 42    |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   | 20    |

  Scenario Outline: (07) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, by a User with one or more roles - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "Gehan.Ahmed.NoMeasures" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then edit Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    And there are no authorized reads returned to "<User>"
    Examples:
      | POCode     | User                   | ItemCode | QtyID |
      | 2018000021 | Gehan.Ahmed.NoMeasures | 000002   | 18    |
      | 2018000013 | Gehan.Ahmed.NoMeasures | 000001   | 20    |

  @ResetData
  Scenario Outline: (08) Request Edit Item Quantity for Item in Local/Import PurchaseOrder, where Quantity is deleted by another user (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted Item Quantity with id <QtyID> of Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to Edit Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | User        | POCode     | ItemCode | State | Type   | QtyID |
      | Gehan.Ahmed | 2018100000 | 000012   | Draft | Import | 44    |
      | Gehan.Ahmed | 2018100001 | 000012   | Draft | Local  | 45    |
  #unlock sections

  Scenario Outline: (08) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, within the edit session of Item Quantity to Local/Import PurchaseOrder in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    Examples:
      | POCode     | ItemCode | State          | User        | Type   | QtyID |
      | 2018000021 | 000002   | Draft          | Gehan.Ahmed | Import | 18    |
      | 2018000014 | 000003   | OpenForUpdates | Gehan.Ahmed | Local  | 47    |
      | 2018000030 | 000006   | Draft          | Amr.Khalil  | Import | 49    |
      | 2018000031 | 000012   | Draft          | Amr.Khalil  | Import | 43    |

  Scenario Outline: (09) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, after edit session expire: for both Local and Import PO (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | ItemCode | QtyID |
      | 2018000021 | 000002   | 18    |
      | 2018000013 | 000001   | 20    |

  @ResetData
  Scenario Outline:(10) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, while PurchaseOrder doesn't exist: for both Local and Import PO (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opened Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | ItemCode | QtyID |
      | 2018100000 | 000012   | 44    |
      | 2018100001 | 000012   | 45    |

  @ResetData
  Scenario Outline: (11) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, while Item that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opened Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode | QtyID |
      | 2018100000 | 000012   | 44    |
      | 2018100001 | 000012   | 45    |

  @ResetData
  Scenario Outline: (12) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, while quantity that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opened Item Quantity with id <QtyID> for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted Item Quantity with id<QtyID> of Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode | QtyID |
      | 2018100000 | 000012   | 44    |
      | 2018100001 | 000012   | 45    |

  Scenario Outline: (13) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, in a state that doen't allow this action - All states except Draft and OpenForUpdates - for both Import and Local POs  (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    And OrderItems section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     | State              | ItemCode | QtyID |
      # Import
      | 2018000002 | DeliveryComplete   | 000003   | 4     |
      | 2018000005 | Approved           | 000001   | 50    |
      | 2018000007 | WaitingApproval    | 000001   | 51    |
      | 2018000008 | Confirmed          | 000001   | 52    |
      | 2018000009 | FinishedProduction | 000001   | 53    |
      | 2018000010 | Shipped            | 000002   | 54    |
      | 2018000011 | Arrived            | 000001   | 55    |
      | 2018000012 | Cancelled          | 000001   | 56    |
      | 2018000023 | Cleared            | 000002   | 10    |
      # Local
      | 2018000015 | WaitingApproval    | 000001   | 57    |
      | 2018000016 | Approved           | 000001   | 58    |
      | 2018000017 | Confirmed          | 000001   | 59    |
      | 2018000018 | Shipped            | 000001   | 26    |

  Scenario Outline: (14) Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder, by an unauthorized user/ due to unmatched condition (Abuse Case)
    Given user is logged in as "<User>"
    When  "<User>" requests to cancel saving Item Quantity with id <QtyID> for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with Code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode | QtyID |
      | Mahmoud.Abdelaziz | 2018000021 | 000002   | 18    |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   | 20    |
      | Gehan.Ahmed       | 2018000030 | 000006   | 49    |
      | Gehan.Ahmed       | 2018000031 | 000012   | 43    |
