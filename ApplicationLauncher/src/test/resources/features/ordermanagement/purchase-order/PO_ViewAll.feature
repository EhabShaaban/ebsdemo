# updated by: Fatma Al Zahraa (EBS - 7616)
# Author Quality: Shirin Mahmoud

Feature: View all purchase orders

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | M.D                              | POViewer            |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission            | Condition                       |
      | POViewer            | PurchaseOrder:ReadAll |                                 |
      | POViewer_Signmedia  | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadAll | [purchaseUnitName='Corrugated'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000001 | IMPORT_PO  | Cleared          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | DeliveryComplete | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000006 | IMPORT_PO  | OpenForUpdates   | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000007 | IMPORT_PO  | WaitingApproval  | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Confirmed        | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Shipped          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Arrived          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO  | Cancelled        | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000026 | IMPORT_PO  | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000032 | IMPORT_PO  | WaitingApproval  | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000034 | IMPORT_PO  | Approved         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

      | 2018000013 | LOCAL_PO   | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000014 | LOCAL_PO   | OpenForUpdates   | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000015 | LOCAL_PO   | WaitingApproval  | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000016 | LOCAL_PO   | Approved         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000018 | LOCAL_PO   | Shipped          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO   | DeliveryComplete | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000020 | LOCAL_PO   | Cancelled        | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000027 | LOCAL_PO   | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000028 | LOCAL_PO   | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000030 | LOCAL_PO   | OpenForUpdates   | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

      | 2020000004 | SERVICE_PO | Draft            | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000001 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000002 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000006 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000007 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000008 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000010 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000011 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000012 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000032 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000034 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000014 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000015 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000016 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000018 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000019 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000020 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000027 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000028 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000030 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000001 | 000002 - Zhejiang |             |
      | 2018000002 | 000002 - Zhejiang |             |
      | 2018000006 | 000002 - Zhejiang |             |
      | 2018000007 | 000002 - Zhejiang |             |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2018000010 | 000002 - Zhejiang |             |
      | 2018000011 | 000002 - Zhejiang |             |
      | 2018000012 | 000002 - Zhejiang |             |
      | 2018000021 | 000002 - Zhejiang |             |
      | 2018000025 | 000002 - Zhejiang |             |
      | 2018000026 | 000002 - Zhejiang |             |
      | 2018000032 | 000001 - Siegwerk |             |
      | 2018000034 | 000002 - Zhejiang |             |

      | 2018000013 | 000002 - Zhejiang |             |
      | 2018000014 | 000002 - Zhejiang |             |
      | 2018000015 | 000002 - Zhejiang |             |
      | 2018000016 | 000002 - Zhejiang |             |
      | 2018000018 | 000002 - Zhejiang |             |
      | 2018000019 | 000002 - Zhejiang |             |
      | 2018000020 | 000002 - Zhejiang |             |
      | 2018000027 | 000002 - Zhejiang |             |
      | 2018000028 | 000002 - Zhejiang |             |
      | 2018000030 | 000002 - Zhejiang |             |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |

  #EBS-7616
  Scenario: (01) View all PurchaseOrders by authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view first page of PurchaseOrders with 20 records per page
    Then the following values will be presented to "Ashraf.Fathi":
      | Code       | Type       | Vendor   | ReferenceDocument                  | PurchasingUnit | State            |
      | 2020000004 | SERVICE_PO | Zhejiang | Import Purchase Order - 2018000008 | Signmedia      | Draft            |

      | 2018000034 | IMPORT_PO  | Zhejiang |                                    | Flexo          | Approved         |
      | 2018000032 | IMPORT_PO  | Siegwerk |                                    | Flexo          | WaitingApproval  |
      | 2018000030 | LOCAL_PO   | Zhejiang |                                    | Flexo          | OpenForUpdates   |
      | 2018000028 | LOCAL_PO   | Zhejiang |                                    | Corrugated     | Draft            |
      | 2018000027 | LOCAL_PO   | Zhejiang |                                    | Flexo          | Draft            |
      | 2018000026 | IMPORT_PO  | Zhejiang |                                    | Corrugated     | Draft            |
      | 2018000025 | IMPORT_PO  | Zhejiang |                                    | Flexo          | Draft            |
      | 2018000021 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Draft            |
      | 2018000020 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Cancelled        |
      | 2018000019 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | DeliveryComplete |
      | 2018000018 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Shipped          |
      | 2018000016 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Approved         |
      | 2018000015 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | WaitingApproval  |
      | 2018000014 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | OpenForUpdates   |
      | 2018000013 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Draft            |
      | 2018000012 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Cancelled        |
      | 2018000011 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Arrived          |
      | 2018000010 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Shipped          |
      | 2018000008 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Confirmed        |
      | 2018000007 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | WaitingApproval  |
    And the total number of records presented to "Ashraf.Fathi" are 24

  #EBS-7616
  Scenario: (02) View all PurchaseOrders by authorized user WITH condition: user has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view first page of PurchaseOrders with 20 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type       | Vendor   | ReferenceDocument                  | PurchasingUnit | State            |
      | 2020000004 | SERVICE_PO | Zhejiang | Import Purchase Order - 2018000008 | Signmedia      | Draft            |

      | 2018000021 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Draft            |
      | 2018000020 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Cancelled        |
      | 2018000019 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | DeliveryComplete |
      | 2018000018 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Shipped          |
      | 2018000016 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Approved         |
      | 2018000015 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | WaitingApproval  |
      | 2018000014 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | OpenForUpdates   |
      | 2018000013 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Draft            |
      | 2018000012 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Cancelled        |
      | 2018000011 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Arrived          |
      | 2018000010 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Shipped          |
      | 2018000008 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Confirmed        |
      | 2018000007 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | WaitingApproval  |
      | 2018000006 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | OpenForUpdates   |
      | 2018000002 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | DeliveryComplete |
      | 2018000001 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Cleared          |
    And the total number of records presented to "Gehan.Ahmed" are 17

  Scenario: (03) View all PurchaseOrders by authorized user WITH condition: user has two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view first page of PurchaseOrders with 20 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000034 | IMPORT_PO | Zhejiang |                   | Flexo          | Approved        |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
      | 2018000030 | LOCAL_PO  | Zhejiang |                   | Flexo          | OpenForUpdates  |
      | 2018000028 | LOCAL_PO  | Zhejiang |                   | Corrugated     | Draft           |
      | 2018000027 | LOCAL_PO  | Zhejiang |                   | Flexo          | Draft           |
      | 2018000026 | IMPORT_PO | Zhejiang |                   | Corrugated     | Draft           |
      | 2018000025 | IMPORT_PO | Zhejiang |                   | Flexo          | Draft           |
    And the total number of records presented to "Amr.Khalil" are 7

  Scenario: (04) View all PurchaseOrders by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view first page of PurchaseOrders with 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #########   Filter by ---> POCode (String)
  # EBS-2897
  Scenario: (05) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - Pagination - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on POCode which contains "00001" with 8 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State            |
      | 2018000019 | LOCAL_PO  | Zhejiang |                   | Signmedia      | DeliveryComplete |
      | 2018000018 | LOCAL_PO  | Zhejiang |                   | Signmedia      | Shipped          |
      | 2018000016 | LOCAL_PO  | Zhejiang |                   | Signmedia      | Approved         |
      | 2018000015 | LOCAL_PO  | Zhejiang |                   | Signmedia      | WaitingApproval  |
      | 2018000014 | LOCAL_PO  | Zhejiang |                   | Signmedia      | OpenForUpdates   |
      | 2018000013 | LOCAL_PO  | Zhejiang |                   | Signmedia      | Draft            |
      | 2018000012 | IMPORT_PO | Zhejiang |                   | Signmedia      | Cancelled        |
      | 2018000011 | IMPORT_PO | Zhejiang |                   | Signmedia      | Arrived          |
    And the total number of records in search results by "hr1" are 10

  # EBS-2897
  Scenario: (06) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser with filter-->Paging (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of PurchaseOrders with filter applied on POCode which contains "00001" with 8 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State   |
      | 2018000010 | IMPORT_PO | Zhejiang |                   | Signmedia      | Shipped |
      | 2018000001 | IMPORT_PO | Zhejiang |                   | Signmedia      | Cleared |
    And the total number of records in search results by "hr1" are 10

  # EBS-2897
  Scenario: (07) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser without Paging (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on POCode which contains "00002" with 20 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State            |
      | 2018000028 | LOCAL_PO  | Zhejiang |                   | Corrugated     | Draft            |
      | 2018000027 | LOCAL_PO  | Zhejiang |                   | Flexo          | Draft            |
      | 2018000026 | IMPORT_PO | Zhejiang |                   | Corrugated     | Draft            |
      | 2018000025 | IMPORT_PO | Zhejiang |                   | Flexo          | Draft            |
      | 2018000021 | IMPORT_PO | Zhejiang |                   | Signmedia      | Draft            |
      | 2018000020 | LOCAL_PO  | Zhejiang |                   | Signmedia      | Cancelled        |
      | 2018000002 | IMPORT_PO | Zhejiang |                   | Signmedia      | DeliveryComplete |
    And the total number of records in search results by "hr1" are 7

  # EBS-3510
  Scenario: (08) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - User with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of PurchaseOrders with filter applied on POCode which contains "00002" with 20 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State            |
      | 2018000021 | IMPORT_PO | Zhejiang |                   | Signmedia      | Draft            |
      | 2018000020 | LOCAL_PO  | Zhejiang |                   | Signmedia      | Cancelled        |
      | 2018000002 | IMPORT_PO | Zhejiang |                   | Signmedia      | DeliveryComplete |
    And the total number of records in search results by "Gehan.Ahmed" are 3

  # EBS-3510
  Scenario: (09) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - User with two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of PurchaseOrders with filter applied on POCode which contains "00002" with 20 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State |
      | 2018000028 | LOCAL_PO  | Zhejiang |                   | Corrugated     | Draft |
      | 2018000027 | LOCAL_PO  | Zhejiang |                   | Flexo          | Draft |
      | 2018000026 | IMPORT_PO | Zhejiang |                   | Corrugated     | Draft |
      | 2018000025 | IMPORT_PO | Zhejiang |                   | Flexo          | Draft |
    And the total number of records in search results by "Amr.Khalil" are 4

  ######### Filter by ---> State (StringList)
  # EBS-3545
  #EBS-7616
  Scenario: (10) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser without Paging (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on State which contains "Draft" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type       | Vendor   | ReferenceDocument                  | PurchasingUnit | State |
      | 2020000004 | SERVICE_PO | Zhejiang | Import Purchase Order - 2018000008 | Signmedia      | Draft |
      | 2018000028 | LOCAL_PO   | Zhejiang |                                    | Corrugated     | Draft |
      | 2018000027 | LOCAL_PO   | Zhejiang |                                    | Flexo          | Draft |
      | 2018000026 | IMPORT_PO  | Zhejiang |                                    | Corrugated     | Draft |
      | 2018000025 | IMPORT_PO  | Zhejiang |                                    | Flexo          | Draft |
      | 2018000021 | IMPORT_PO  | Zhejiang |                                    | Signmedia      | Draft |
      | 2018000013 | LOCAL_PO   | Zhejiang |                                    | Signmedia      | Draft |

    And the total number of records in search results by "hr1" are 7

  # EBS-3545
  Scenario: (11) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser without Paging (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on State which contains "WaitingApproval" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
      | 2018000015 | LOCAL_PO  | Zhejiang |                   | Signmedia      | WaitingApproval |
      | 2018000007 | IMPORT_PO | Zhejiang |                   | Signmedia      | WaitingApproval |
    And the total number of records in search results by "hr1" are 3


  ######## Filter by ---> OrderType (Json)
  # EBS-1202
  Scenario: (12) View all PurchaseOrders with Filter by OrderType using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on OrderType which equals "LOCAL_PO" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type     | Vendor   | ReferenceDocument | PurchasingUnit | State            |
      | 2018000030 | LOCAL_PO | Zhejiang |                   | Flexo          | OpenForUpdates   |
      | 2018000028 | LOCAL_PO | Zhejiang |                   | Corrugated     | Draft            |
      | 2018000027 | LOCAL_PO | Zhejiang |                   | Flexo          | Draft            |
      | 2018000020 | LOCAL_PO | Zhejiang |                   | Signmedia      | Cancelled        |
      | 2018000019 | LOCAL_PO | Zhejiang |                   | Signmedia      | DeliveryComplete |
      | 2018000018 | LOCAL_PO | Zhejiang |                   | Signmedia      | Shipped          |
      | 2018000016 | LOCAL_PO | Zhejiang |                   | Signmedia      | Approved         |
      | 2018000015 | LOCAL_PO | Zhejiang |                   | Signmedia      | WaitingApproval  |
      | 2018000014 | LOCAL_PO | Zhejiang |                   | Signmedia      | OpenForUpdates   |
      | 2018000013 | LOCAL_PO | Zhejiang |                   | Signmedia      | Draft            |
    And the total number of records in search results by "hr1" are 10

  ### Filter by ---> Purchase Unit (Json)
  # EBS-1202
  Scenario: (13) View all PurchaseOrders with Filter by Purchase Unit using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on PurchaseUnit which contains "Flexo" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000034 | IMPORT_PO | Zhejiang |                   | Flexo          | Approved        |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
      | 2018000030 | LOCAL_PO  | Zhejiang |                   | Flexo          | OpenForUpdates  |
      | 2018000027 | LOCAL_PO  | Zhejiang |                   | Flexo          | Draft           |
      | 2018000025 | IMPORT_PO | Zhejiang |                   | Flexo          | Draft           |
    And the total number of records in search results by "hr1" are 5

  Scenario: (14) View all PurchaseOrders with Filter by Purchase Unit using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on PurchaseUnit which contains "كوروجيتيد" with 10 records per page while current locale is "ar-EG"
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor  | ReferenceDocument | PurchasingUnit | State |
      | 2018000028 | LOCAL_PO  | زاجاننج |                   | كوروجيتيد      | Draft |
      | 2018000026 | IMPORT_PO | زاجاننج |                   | كوروجيتيد      | Draft |
    And the total number of records in search results by "hr1" are 2

  #### Filter by ---> Vendor (Mixed Values)
  #EBS-3476
  Scenario: (15) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on Vendor which contains "ieg" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
    And the total number of records in search results by "hr1" are 1

  #EBS-3476
  Scenario: (16) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on Vendor which contains "0001" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
    And the total number of records in search results by "hr1" are 1

  Scenario: (17) View all PurchaseOrders with Filter PurchaseOrders by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with filter applied on Vendor which contains "si" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | State           |
      | 2018000032 | IMPORT_PO | Siegwerk |                   | Flexo          | WaitingApproval |
    And the total number of records in search results by "hr1" are 1

  #### Filter by ---> Multiple Columns
  # EBS-1249
  Scenario: (18) View all PurchaseOrders with Filter POs by authorized user using - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with 3 records per page and the following filters applied while current locale is "en-US":
      | FieldName        | Operation | Value     |
      | userCode         | contains  | 00001     |
      | objectTypeCode   | equals    | IMPORT_PO |
      | vendor           | contains  | jian      |
      | purchaseUnitName | contains  | Signmedia |
      | currentStates    | contains  | Arrived   |
    Then the following values will be presented to "hr1":
      | Code       | Type      | Vendor   | ReferenceDocument | PurchasingUnit | Currency | ETA | State   |
      | 2018000011 | IMPORT_PO | Zhejiang |                   | Signmedia      |          |     | Arrived |
    And the total number of records in search results by "hr1" are 1

  # EBS-1249
  Scenario: (19) View all PurchaseOrders with Filter POs by authorized user using - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of PurchaseOrders with 3 records per page and the following filters applied while current locale is "en-US":
      | FieldName        | Operation | Value     |
      | userCode         | contains  | 00002     |
      | objectTypeCode   | equals    | LOCAL_PO  |
      | vendor           | contains  | jian      |
      | purchaseUnitName | contains  | Signmedia |
      | currentStates    | contains  | Cancelled |
    Then the following values will be presented to "hr1":
      | Code       | Type     | Vendor   | ReferenceDocument | PurchasingUnit | State     |
      | 2018000020 | LOCAL_PO | Zhejiang |                   | Signmedia      | Cancelled |
    And the total number of records in search results by "hr1" are 1

  #EBS-7616
  Scenario: (20) View all PurchaseOrders with Filter by ReferenceDocumentCode  by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of PurchaseOrders with filter applied on ReferenceDocument which contains "008" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type       | Vendor   | ReferenceDocument                  | PurchasingUnit | State |
      | 2020000004 | SERVICE_PO | Zhejiang | Import Purchase Order - 2018000008 | Signmedia      | Draft |
    And the total number of records in search results by "Gehan.Ahmed" are 1

  #EBS-7616
  Scenario: (21) View all PurchaseOrders with Filter by ReferenceDocumentType by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of PurchaseOrders with filter applied on ReferenceDocument which contains "Import" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type       | Vendor   | ReferenceDocument                  | PurchasingUnit | State |
      | 2020000004 | SERVICE_PO | Zhejiang | Import Purchase Order - 2018000008 | Signmedia      | Draft |
    And the total number of records in search results by "Gehan.Ahmed" are 1
