# Reviewer: Somaya Ahmed (27-Jan-2019 3:30 PM)

Feature: View Consignee section in PO

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                      | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadConsigneeData | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadConsigneeData | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadConsigneeData | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                             |                                 |

    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit | Consignee | Plant | Storehouse |
      | 2018000021 | Draft | 0002           | 0001      | 0001  | 0002       |
      | 2018000025 | Draft | 0001           | 0001      | 0001  | 0002       |
      | 2018000026 | Draft | 0006           | 0001      | 0001  | 0002       |

    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit | Consignee | Plant | Storehouse |
      | 2018000013 | Draft | 0002           | 0001      | 0001  | 0002       |
      | 2018000027 | Draft | 0001           | 0001      | 0001  | 0002       |
      | 2018000028 | Draft | 0006           | 0001      | 0001  | 0002       |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following Plants exist:
      | Code | Name              |
      | 0001 | Madina Tech Plant |
    And the following Storehouses exist:
      | Code | Name                     |
      | 0002 | AlMadina Secondary Store |

  #### Happy Path
  #EBS-214
  Scenario Outline: (01) View Consignee section by an authorized user who has one / two role(s) (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view Consignee section of PurchaseOrder with code "<POCode>"
    Then the following values of Consignee section of PurchaseOrder with code "<POCode>" are displayed to "<User>":
      | Code     | Consignee        | Plant        | Storehouse        |
      | <POCode> | <ConsigneeValue> | <PlantValue> | <StorehouseValue> |
    Examples:
      | User        | POCode     | ConsigneeValue | PlantValue        | StorehouseValue          |
      | Gehan.Ahmed | 2018000013 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |
      | Gehan.Ahmed | 2018000021 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |
      | Amr.Khalil  | 2018000025 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |
      | Amr.Khalil  | 2018000026 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |
      | Amr.Khalil  | 2018000027 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |
      | Amr.Khalil  | 2018000028 | AL Madina      | Madina Tech Plant | AlMadina Secondary Store |

  #### Exceptions
  #EBS-214
  @ResetData
  Scenario Outline: (02) View Consignee section where PurchaseOrder is deleted by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view Consignee section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  #EBS-214
  Scenario Outline: (03) View Consignee section by an unauthorized user due to consition or not (Unauthorized/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Consignee section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User       | POCode     |
      | Afaf       | 2018000013 |
      | Afaf       | 2018000021 |
      | Amr.Khalil | 2018000013 |
      | Amr.Khalil | 2018000021 |

