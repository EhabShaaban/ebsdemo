#Auther: Hossam bayomy , Hend Ahmed 12-Dec-2018 10:00 AM
# updated by: Eman Mansour & Waseem Salama (EBS - 6068)

Feature: Read Allowed Actions for PurchaseOrder Home Screen

  Background:
    Given the following users and roles exist:
      | Name           | Role                             |
      # One role- one subrole- with condition
      | TestUser10     | PurchasingResponsible_Signmedia  |
      # one role- one subrole- without condition
      | Ashraf.Fathi   | M.D                              |
      # Two Roles --> Two Subroles - Different Subroles with and without condition
      | TestUser9      | PurchasingResponsible_Signmedia  |
      | TestUser9      | AccountantHead                   |
      # Two Roles --> similar subroles
      | Amr.Khalil     | PurchasingResponsible_Flexo      |
      | Amr.Khalil     | PurchasingResponsible_Corrugated |
      # user authorized to create only
      | CreateOnlyUser | CreateOnlyRole                   |
      # Not authorized
      | Afaf           | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | M.D                              | POViewer            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo       |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated  |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | AccountantHead                   | POViewer            |
      | CreateOnlyRole                   | CreateOnlySubRole   |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission            | Condition                       |
      | POViewer            | PurchaseOrder:ReadAll |                                 |
      | POOwner_Signmedia   | PurchaseOrder:Create  |                                 |
      | POViewer_Signmedia  | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo       | PurchaseOrder:Create  |                                 |
      | POViewer_Flexo      | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated  | PurchaseOrder:Create  |                                 |
      | POViewer_Corrugated | PurchaseOrder:ReadAll | [purchaseUnitName='Corrugated'] |
      | CreateOnlySubRole   | PurchaseOrder:Create  |                                 |
    And the following users doesn't have the following permissions:
      | User           | Permission            |
      | Ashraf.Fathi   | PurchaseOrder:Create  |
      | CreateOnlyUser | PurchaseOrder:ReadAll |

  Scenario Outline: (01) Read PO Home Screen Allowed Actions  by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of PurchaseOrder home screen
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User           | AllowedActions | NoOfActions |
      # One Role --> One Subrole
      | TestUser10     | ReadAll,Create | 2           |
      # Two Roles --> similar subroles
      | Amr.Khalil     | ReadAll,Create | 2           |
      # Two Roles --> differet subroles
      | TestUser9      | ReadAll,Create | 2           |
      # One Role --> One Subrole- viewer
      | Ashraf.Fathi   | ReadAll        | 1           |
      # user authorized to create only
      | CreateOnlyUser | Create         | 1           |

  Scenario: (02) Read PO Home Screen Allowed Actions by unauthorized user (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of PurchaseOrder home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"