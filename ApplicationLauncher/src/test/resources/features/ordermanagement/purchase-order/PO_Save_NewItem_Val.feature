@Validation
Feature: Save Add Item Validations In OrderItems section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole  |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission               | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole  | *:*                      |                                |
    And the following Items exist:
      | Code   | Name       | PurchasingUnit |
      | 000007 | PRO-V-ST-1 | 0002           |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit | Vendor |
      | 2018000021 | Draft | 0002           | 000002 |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit | Vendor |
      | 2018000013 | Draft | 0002           | 000002 |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000021 | 000002   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
    And edit session is "30" minutes

  Scenario Outline: (01) Save Add Item In PurchaseOrder, after edit session expired for both Local and Import PO (regardless data entry is correct or not) (Exception Case)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:41 AM" with the following values:
      | ItemCode | QtyInDN |
      | 000004   | 100     |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  @ResetData
  Scenario: (02) Save Add Item In PurchaseOrder, after edit session expired & PO is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And OrderItems section of PurchaseOrder with code "2018000021" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "2018000021" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" at "07-Jan-2019 09:42 AM" with the following values:
      | ItemCode | QtyInDN |
      | 000004   | 100     |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (03) Save Add Item In PurchaseOrder, item that has no ivr record (Exception Case)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000021" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" at "07-Jan-2019 09:31 AM" with the following values:
      | ItemCode | QtyInDN |
      | 000007   | 100     |
    And Item with code "000007" has no IVR record
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Item field "PO-msg-42" and sent to "Gehan.Ahmed"

  Scenario Outline: (04) Save Add Item In PurchaseOrder, after edit session expired for both Local and Import PO (regardless data entry is correct or not) (Exception Case)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000021" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" at "07-Jan-2019 09:31 AM" with the following values:
      | ItemCode   | QtyInDN |
      | <ItemCode> | 100     |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCode |
      | ay7aga   |
      |          |
      | 000002   |

  Scenario Outline: (05) Save Add Item In PurchaseOrder, after edit session expired for both Local and Import PO (regardless data entry is correct or not) (Exception Case)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000021" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" at "07-Jan-2019 09:31 AM" with the following values:
      | ItemCode | QtyInDN   |
      | 000004   | <QtyInDN> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | QtyInDN |
      | -1      |
      | -2.4    |
      | Ay 7aga |