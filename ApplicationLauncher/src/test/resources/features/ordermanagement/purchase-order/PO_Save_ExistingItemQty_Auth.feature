@editAuthorization
Feature: Save Edit Item Quantities in PO Authorization

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Amr.Khalil             | PurchasingResponsible_Flexo                            |
      | Amr.Khalil             | PurchasingResponsible_Corrugated                       |
      | Gehan.Ahmed.NoMeasures | PurchasingResponsible_Signmedia_CannotReadMeasuresRole |
      | Shady.Abdelatif        | Accountant_Signmedia                                   |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole            |
      | PurchasingResponsible_Flexo                            | POOwner_Flexo      |
      | PurchasingResponsible_Flexo                            | MeasuresViewer     |
      | PurchasingResponsible_Corrugated                       | POOwner_Corrugated |
      | PurchasingResponsible_Corrugated                       | MeasuresViewer     |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole | POOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | MeasuresViewer     | Measures:ReadAll         |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000021 | 000002   | 18    |
      # Local
      | 2018000013 | 000001   | 20    |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000021 | 000002   | 18    |
      # Local
      | 2018000013 | 000001   | 20    |
    And the following Measures exist:
      | Code | Type     | Symbol       | Name         |
      | 0033 | Business | Roll 2.70x50 | Roll 2.70x50 |
      | 0032 | Business | Roll 3.20x50 | Roll 3.20x50 |

  Scenario Outline: (01) Save Edit Item Quantity to Local/Import PurchaseOrder, by an unauthorized user due to condition (Abuse Case)
    Given  user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     | ItemCode | State | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000021 | 000002   | Draft | Import | 18    | 300.0    | 0033           | 100.0          | 0.0%          |
      # Local
      | 2018000013 | 000001   | Draft | Local  | 20    | 800.0    | 0032           | 20.0           | 30.0%         |

  Scenario: (02) Save Edit Item Quantity to Local/Import PurchaseOrder, User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given  user is logged in as "Gehan.Ahmed.NoMeasures"
    When "Gehan.Ahmed.NoMeasures" saves Item Quantity with id "18" for Item with code "000002" to OrderItems section of PurchaseOrder with code "2018000021" with the following values:
      | Qty   | OrderUnit | UnitPrice(Gross) | Discount |
      | 300.0 | 0033      | 100.0            | 0.0%     |
    Then "Gehan.Ahmed.NoMeasures" is logged out
    And "Gehan.Ahmed.NoMeasures" is forwarded to the error page

  Scenario: (03) Save Edit Item Quantity to Local/Import PurchaseOrder, Save by an unauthorized users (Abuse Case)
    Given  user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves Item Quantity with id "18" for Item with code "000002" to OrderItems section of PurchaseOrder with code "2018000021" with the following values:
      | Qty   | OrderUnit | UnitPrice(Gross) | Discount |
      | 300.0 | 0033      | 100.0            | 0.0%     |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page