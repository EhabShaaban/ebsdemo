@Happy
Feature: Save Add Item In OrderItems section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State          | Type   | PurchasingUnit | Vendor |
      | 2018000021 | Draft          | Import | 0002           | 000002 |
      | 2018000006 | OpenForUpdates | Import | 0002           | 000002 |
    And the following Local PurchaseOrders exist:
      | Code       | State          | Type  | PurchasingUnit | Vendor |
      | 2018000013 | Draft          | Local | 0002           | 000002 |
      | 2018000014 | OpenForUpdates | Local | 0002           | 000002 |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000006 | 000002   |         |
      | 2018000006 | 000003   |         |
      | 2018000021 | 000002   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
      | 2018000014 | 000003   |         |

  #draft import with qtyInDn
  @ResetData
  Scenario Outline: (01) Save Add Item In PurchaseOrder, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000021" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
    Then PurchaseOrder with code "2018000021" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And OrderItems section of Import PurchaseOrder with code "2018000021" is updated as follows:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
      | 000002   |                |
    And the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "2018000021" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | QtyInDnValue |
      | 100          |
      |              |

  #OpenForUpdates import with qtyInDn
  @ResetData
  Scenario Outline: (02) Save Add Item In PurchaseOrder, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000006" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000006" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
    Then PurchaseOrder with code "2018000006" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And OrderItems section of Import PurchaseOrder with code "2018000006" is updated as follows:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
      | 000003   |                |
      | 000002   |                |
    And the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "2018000006" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | QtyInDnValue |
      | 100          |
      |              |

  #draft local with qtyInDn
  @ResetData
  Scenario Outline: (03) Save Add Item In PurchaseOrder, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000013" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000013" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
    Then PurchaseOrder with code "2018000013" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And OrderItems section of Local PurchaseOrder with code "2018000013" is updated as follows:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
      | 000002   |                |
      | 000001   |                |
    And the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "2018000013" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | QtyInDnValue |
      | 100          |
      |              |

  #OpenForUpdates local with qtyInDn
  @ResetData
  Scenario Outline: (04) Save Add Item In PurchaseOrder, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And OrderItems section of PurchaseOrder with code "2018000014" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following item to the OrderItems of PurchaseOrder with code "2018000014" at "07-Jan-2019 09:30 AM" with the following values:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
    Then PurchaseOrder with code "2018000014" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And OrderItems section of Local PurchaseOrder with code "2018000014" is updated as follows:
      | ItemCode | QtyInDN        |
      | 000004   | <QtyInDnValue> |
      | 000003   |                |
    And the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "2018000014" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | QtyInDnValue |
      | 100          |
      |              |