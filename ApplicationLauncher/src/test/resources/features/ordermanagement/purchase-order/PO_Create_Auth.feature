# Reviewer: Somaya Ahmed on 02-Jan-2019, 11:00 AM

Feature: Create PurchaseOrder Auth

  User should be able to create PurchaseOrder
  Rules:
  - User should be authenticated and authorized to create Local or Import POs
  - User should be authorized to ReadAll vendors, purchase units, purchase responsible with OR without consition
  - User should enter all mandatory fields for creation
  - If an unauthorized user sends a create request, he/she shoud be logged out as this is considered as a security breach

  Background:
    Given the following users and roles exist:
      | Name                                                           | Role                                                                                              |
      | Gehan.Ahmed                                                    | PurchasingResponsible_Signmedia                                                                   |
      | Amr.Khalil                                                     | PurchasingResponsible_Flexo                                                                       |
      | Amr.Khalil                                                     | PurchasingResponsible_Corrugated                                                                  |
      | Mahmoud.Abdelaziz                                              | Storekeeper_Signmedia                                                                             |
      | Gehan.Ahmed.NoDocumentOwner                                    | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole                                       |
      | Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner | PurchasingResponsible_Signmedia_CannotReadBusinessUnitAndVendorAndReferencePOAndDocumentOwnerRole |
    And the following roles and sub-roles exist:
      | Role                                                                                              | Subrole                   |
      | PurchasingResponsible_Signmedia                                                                   | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia                                                                   | PurUnitReader_Signmedia   |
      | PurchasingResponsible_Signmedia                                                                   | VendorOwner_Signmedia     |
      | PurchasingResponsible_Signmedia                                                                   | POViewer_Signmedia        |
      | PurchasingResponsible_Signmedia                                                                   | DocumentOwnerViewer       |

      | PurchasingResponsible_Flexo                                                                       | PurUnitReader_Flexo       |
      | PurchasingResponsible_Corrugated                                                                  | PurUnitReader_Corrugated  |

      | Storekeeper_Signmedia                                                                             | POViewerLimited_Signmedia |

      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole                                       | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole                                       | PurUnitReader_Signmedia   |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole                                       | VendorViewer_Signmedia    |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole                                       | POViewer_Signmedia        |

      | PurchasingResponsible_Signmedia_CannotReadBusinessUnitAndVendorAndReferencePOAndDocumentOwnerRole | POOwner_Signmedia         |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                   | Condition                       |
      | POOwner_Signmedia        | PurchaseOrder:Create         |                                 |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']  |
      | VendorOwner_Signmedia    | Vendor:ReadAll               | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia       | PurchaseOrder:ReadAll        | [purchaseUnitName='Signmedia']  |

      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']      |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Corrugated'] |

      | DocumentOwnerViewer      | DocumentOwner:ReadAll        |                                 |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                   | Condition                       |
      | Gehan.Ahmed | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']      |
      | Gehan.Ahmed | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Corrugated'] |
      | Gehan.Ahmed | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Offset']     |
      | Gehan.Ahmed | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Digital']    |
      | Gehan.Ahmed | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Textile']    |

      | Gehan.Ahmed | Vendor:ReadAll               | [purchaseUnitName='Flexo']      |

      | Gehan.Ahmed | PurchaseOrder:ReadAll        | [purchaseUnitName='Flexo']      |

      | Amr.Khalil  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']  |
      | Amr.Khalil  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Offset']     |
      | Amr.Khalil  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Digital']    |
      | Amr.Khalil  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Textile']    |
    And the following users doesn't have the following permissions:
      | User                                                           | Permission                   |
      | Mahmoud.Abdelaziz                                              | PurchaseOrder:Create         |

      | Gehan.Ahmed.NoDocumentOwner                                    | DocumentOwner:ReadAll        |

      | Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner | PurchasingUnit:ReadAll_ForPO |
      | Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner | Vendor:ReadAll               |

      | Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner | PurchaseOrder:ReadAll        |

      | Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner | DocumentOwner:ReadAll        |
    And the following DocumentOwners exist:
      | Id | Name        | BusinessObject | Permission         | Condition |
      | 37 | Gehan Ahmed | PurchaseOrder  | CanBeDocumentOwner |           |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000013 | LOCAL_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2019000001 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000013 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2019000001 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000001 | Siegwerk | 0001           |
      | 000002 | Zhejiang | 0002           |

  ######## Request to Create ##################
  Scenario: (01) Create PurchaseOrder, by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to creates PurchaseOrder
    Then the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadPOTypes            |
      | ReadBusinessUnits      |
      | ReadVendors            |
      | ReadReferenceDocuments |
      | ReadDocumentOwners     |

  Scenario: (02) Create PurchaseOrder, by an authorized user with no authorized reads
    Given user is logged in as "Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner"
    When "Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner" requests to creates PurchaseOrder
    Then there are no authorized reads returned to "Gehan.Ahmed.NoBusinessUnitNoVendorNoReferencePONoDocumentOwner"

  ######## Create by an unauthorized user
  Scenario Outline: (03) Create PurchaseOrder by an unauthorized user: user is not authorized to create (Unauthorized/Abuse case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates PurchaseOrder with following values:
      | Type   | ReferencePurchaseOrder        | BusinessUnit            | Vendor            | DocumentOwnerId   |
      | <Type> | <ReferencePurchaseOrderValue> | <BusinessUnitCodeValue> | <VendorCodeValue> | <DocumentOwnerId> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | Type       | ReferencePurchaseOrderValue | BusinessUnitCodeValue | VendorCodeValue | DocumentOwnerId |
      | IMPORT_PO  |                             | 0002                  | 000002          | 37              |
      | LOCAL_PO   |                             | 0002                  | 000002          | 37              |
      | SERVICE_PO | 2018000013                  | 0002                  | 000002          | 37              |

  ######## Create using values in which the user is not authorized to Read
  Scenario Outline: (04) Create PurchaseOrder by user chooses PurchasingUnit, Vendor, ReferenceDocument, DocumentOwner s/he is not authorized to read (Unauthorized/Abuse Case because user violates read permission of purchasing unit)
    Given user is logged in as "<User>"
    When "<User>" creates PurchaseOrder with following values:
      | Type   | ReferencePurchaseOrder        | BusinessUnit            | Vendor            | DocumentOwnerId   |
      | <Type> | <ReferencePurchaseOrderValue> | <BusinessUnitCodeValue> | <VendorCodeValue> | <DocumentOwnerId> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                        | BusinessUnitCodeValue | VendorCodeValue | ReferencePurchaseOrderValue | Type       | DocumentOwnerId |
      # User not authorized to read Purchase Unit
      | Gehan.Ahmed                 | 0001                  | 000002          |                             | LOCAL_PO   | 37              |
      | Gehan.Ahmed                 | 0003                  | 000002          |                             | IMPORT_PO  | 37              |
      | Gehan.Ahmed                 | 0004                  | 000002          |                             | LOCAL_PO   | 37              |
      | Gehan.Ahmed                 | 0005                  | 000002          |                             | IMPORT_PO  | 37              |
      | Gehan.Ahmed                 | 0006                  | 000002          |                             | LOCAL_PO   | 37              |
      | Amr.Khalil                  | 0002                  | 000001          |                             | IMPORT_PO  | 37              |
      | Amr.Khalil                  | 0003                  | 000001          |                             | LOCAL_PO   | 37              |
      | Amr.Khalil                  | 0004                  | 000001          |                             | IMPORT_PO  | 37              |
      | Amr.Khalil                  | 0005                  | 000001          |                             | LOCAL_PO   | 37              |
      # User not authorized to read Vendor
      | Gehan.Ahmed                 | 0002                  | 000001          |                             | LOCAL_PO   | 37              |
      | Gehan.Ahmed                 | 0002                  | 000001          |                             | IMPORT_PO  | 37              |
      # User not authorized to read Reference PO
      | Gehan.Ahmed                 | 0002                  | 000002          | 2019000001                  | SERVICE_PO | 37              |
      # User not authorized to read Document Owner
      | Gehan.Ahmed.NoDocumentOwner | 0002                  | 000002          |                             | IMPORT_PO  | 37              |
