# Author: Shrouk Alaa & Marina Salah (02-JAN-2019 2:07 PM)
# Reviewer: Somaya Ahmed and Nancy  (13-Jan-2019 10:00 AM)

Feature: PO Approve

  Background:
    Given the following users and roles exist:
      | Name                | Role                       |
      | Ashraf.Salah        | AccountantHead_Signmedia   |
      | Mohamed.Abdelmoniem | BudgetController_Flexo     |
      | Mohamed.Abdelmoniem | BudgetController_Signmedia |
      | Ahmed.Hussein       | AccountantHead_Flexo       |
      | Mahmoud.Abdelaziz   | Storekeeper_Signmedia      |
      | Yasser.Hussein      | B.U.Director_Flexo         |
      | hr1                 | SuperUser                  |
    And the following roles and sub-roles exist:
      | Role                       | Subrole              |
      | AccountantHead_Signmedia   | POApprover_Signmedia |
      | BudgetController_Flexo     | POApprover_Flexo     |
      | BudgetController_Signmedia | POApprover_Signmedia |
      | AccountantHead_Flexo       | POApprover_Flexo     |
      | B.U.Director_Flexo         | POApprover_Flexo     |
      | SuperUser                  | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission            | Condition                      |
      | POApprover_Signmedia | PurchaseOrder:Approve | [purchaseUnitName='Signmedia'] |
      | POApprover_Flexo     | PurchaseOrder:Approve | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole     | *:*                   |                                |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000032 | WaitingApproval    | 0001           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |

    # Local PO - Not Last Approver - Signmedia
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000015":
      | ApprovalCycleNo | StartDate            | EndDate | FinalDecision |
      | 1               | 07-Jan-2019 09:20 AM |         |               |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000015" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0006         | Essam.Mahmoud       | 07-Jan-2019 10:00 AM | Approved |       |
      | 0001         | Ashraf.Salah        |                      |          |       |
      | 0002         | Mohamed.Abdelmoniem |                      |          |       |

    # Import PO -  Not Last Approver - Flexo
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000032":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM |                      |               |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000032" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0005         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0006         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Rejected |       |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000032" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes                  |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 01:15 PM | Approved | No cash flow for Flexo |
      | 0005         | Mohamed.Abdelmoniem |                      |          |                        |
      | 0006         | Yasser.Hussein      |                      |          |                        |

  ###### Happy Paths
  @ResetData
  Scenario: (01) Approve the PO which in WaitingApproval state (Local) with all mandatory fields by an authorized user who is last approver (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    And another user is logged in as "Mohamed.Abdelmoniem"
    When "Ashraf.Salah" requests to Approve the PurchaseOrder with Code "2018000015" at "07-Jan-2019 11:00 AM" with the following values:
      | Notes                                          |
      | Total equal the business unit remaining budget |
    And "Mohamed.Abdelmoniem" requests to Approve the PurchaseOrder with Code "2018000015" at "07-Jan-2019 11:01 AM" with the following values:
      | Notes         |
      | within budget |
    Then PurchaseOrder with Code "2018000015" is updated as follows:
      | LastUpdatedBy       | LastUpdateDate       | State    |
      | Mohamed.Abdelmoniem | 07-Jan-2019 11:01 AM | Approved |
    And the following ApprovalCycles in PurchaseOrder with code "2018000015" is updated as follows:
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 1               | 07-Jan-2019 09:20 AM | 07-Jan-2019 11:01 AM | Approved      |
    And the following details of ApprovalCycleNo "1" in PurchaseOrder with code "2018000015" is updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes                                          |
      | 0006         | Essam.Mahmoud       | 07-Jan-2019 10:00 AM | Approved |                                                |
      | 0001         | Ashraf.Salah        | 07-Jan-2019 11:00 AM | Approved | Total equal the business unit remaining budget |
      | 0002         | Mohamed.Abdelmoniem | 07-Jan-2019 11:01 AM | Approved | within budget                                  |
    And a success notification is sent to "Ashraf.Salah" with the following message "PO-msg-31"
    And a success notification is sent to "Mohamed.Abdelmoniem" with the following message "PO-msg-30"

  @ResetData
  Scenario: (02) Approve the PO which in WaitingApproval state (Local) with all mandatory fields by an authorized user who is not last approver (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to Approve the PurchaseOrder with Code "2018000015" at "07-Jan-2019 11:00 AM" with the following values:
      | Notes |
      |       |
    Then PurchaseOrder with Code "2018000015" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State           |
      | Ashraf.Salah  | 07-Jan-2019 11:00 AM | WaitingApproval |
    And the following ApprovalCycles in PurchaseOrder with code "2018000015" is updated as follows:
      | ApprovalCycleNo | StartDate            | EndDate | FinalDecision |
      | 1               | 07-Jan-2019 09:20 AM |         |               |
    And the following details of ApprovalCycleNo "1" in PurchaseOrder with code "2018000015" is updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0006         | Essam.Mahmoud       | 07-Jan-2019 10:00 AM | Approved |       |
      | 0001         | Ashraf.Salah        | 07-Jan-2019 11:00 AM | Approved |       |
      | 0002         | Mohamed.Abdelmoniem |                      |          |       |
    And a success notification is sent to "Ashraf.Salah" with the following message "PO-msg-31"

  @ResetData
  Scenario: (03) Approve the PO which in WaitingApproval state (Import) with all mandatory fields by an authorized user who is the last approver (Happy Path)
    Given user is logged in as "Mohamed.Abdelmoniem"
    And another user is logged in as "Yasser.Hussein"
    When "Mohamed.Abdelmoniem" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 09:30 AM" with the following values:
      | Notes                                          |
      | Total equal the business unit remaining budget |
    And "Yasser.Hussein" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 09:31 AM" with the following values:
      | Notes         |
      | within budget |
    Then PurchaseOrder with Code "2018000032" is updated as follows:
      | LastUpdatedBy  | LastUpdateDate       | State    |
      | Yasser.Hussein | 07-Jan-2019 09:31 AM | Approved |
    And the following ApprovalCycles in PurchaseOrder with code "2018000032" is updated as follows:
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 07-Jan-2019 09:31 AM | Approved      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    And the following details of ApprovalCycleNo "1" in PurchaseOrder with code "2018000032" is not updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0005         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0006         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Rejected |       |
    And the following details of ApprovalCycleNo "2" in PurchaseOrder with code "2018000032" is updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes                                          |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 01:15 PM | Approved | No cash flow for Flexo                         |
      | 0005         | Mohamed.Abdelmoniem | 07-Jan-2019 09:30 AM | Approved | Total equal the business unit remaining budget |
      | 0006         | Yasser.Hussein      | 07-Jan-2019 09:31 AM | Approved | within budget                                  |
    And a success notification is sent to "Mohamed.Abdelmoniem" with the following message "PO-msg-31"
    And a success notification is sent to "Yasser.Hussein" with the following message "PO-msg-30"

  @ResetData
  Scenario: (04) Approve the PO which in WaitingApproval state (Import) with all mandatory fields by an authorized user who is not last approver (Happy Path)
    Given user is logged in as "Mohamed.Abdelmoniem"
    When "Mohamed.Abdelmoniem" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 09:30 AM" with the following values:
      | Notes |
      |       |
    Then PurchaseOrder with Code "2018000032" is updated as follows:
      | LastUpdatedBy       | LastUpdateDate       | State           |
      | Mohamed.Abdelmoniem | 07-Jan-2019 09:30 AM | WaitingApproval |
    And the following ApprovalCycles in PurchaseOrder with code "2018000032" is updated as follows:
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM |                      |               |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    And the following details of ApprovalCycleNo "1" in PurchaseOrder with code "2018000032" is not updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0005         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0006         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Rejected |       |
    And the following details of ApprovalCycleNo "2" in PurchaseOrder with code "2018000032" is updated as follows:
      | ControlPoint | User                | DateTime             | Decision | Notes                  |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 01:15 PM | Approved | No cash flow for Flexo |
      | 0005         | Mohamed.Abdelmoniem | 07-Jan-2019 09:30 AM | Approved |                        |
      | 0006         | Yasser.Hussein      |                      |          |                        |
    And a success notification is sent to "Mohamed.Abdelmoniem" with the following message "PO-msg-31"

  ###### Exception Cases
  Scenario: (05) Approve the PO which in WaitingApproval state (Import) with all mandatory fields by an authorized user his name not in approval cycle
    Given user is logged in as "hr1"
    When "hr1" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 09:30 AM" with the following values:
      | Notes |
      |       |
    Then an error notification is sent to "hr1" with the following message "PO-msg-33"

  Scenario: (06) Approve the PO which in WaitingApproval state (Import) with all mandatory fields by an authorized user not in his turn
    Given user is logged in as "Yasser.Hussein"
    When "Yasser.Hussein" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 09:30 AM" with the following values:
      | Notes |
      |       |
    Then an error notification is sent to "Yasser.Hussein" with the following message "PO-msg-32"

  Scenario Outline: (07) Approve the PO (Local/Import) in a state that doesn't allow this action - any state except the WaitingApprovals state (Exception)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to Approve the PurchaseOrder with Code "<Code>" at "07-Jan-2019 9:30 AM" with the following values:
      | Notes |
      |       |
    Then an error notification is sent to "Ashraf.Salah" with the following message "Gen-msg-32"
    Examples:
      | Code       | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000014 | OpenForUpdates     | 0002           |
      | 2018000016 | Approved           | 0002           |
      | 2018000017 | Confirmed          | 0002           |
      | 2018000018 | Shipped            | 0002           |
      | 2018000019 | DeliveryComplete   | 0002           |
      | 2018000020 | Cancelled          | 0002           |

  Scenario Outline: (08) Approve the PO (Local/Import) in a Draft State  (Abuse Case - becoz a PO in a WaitingForApproval state can never go back to draft state)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to Approve the PurchaseOrder with Code "<Code>" at "07-Jan-2019 9:30 AM" with the following values:
      | Notes |
      |       |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
    Examples:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
      | 2018000021 | Draft | 0002           |

  Scenario: (09) Approve the PO which in WaitingApproval state that is doesn't exist (Abuse Case - becasue POs in Waiting ForApproval cannot be deleted)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to Approve the PurchaseOrder with Code "9999999999" at "07-Jan-2019 9:30 AM" with the following values:
      | Notes                                          |
      | Total equal the business unit remaining budget |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page

  Scenario Outline: (10)  Approve the PO which in WaitingApproval state with missing or malicious NoteValue (Abuse case - UI prevents those inputs) Note: what applies for local will apply for import automatically
    Given user is logged in as "Mohamed.Abdelmoniem"
    When "Mohamed.Abdelmoniem" requests to Approve the PurchaseOrder with Code "2018000032" at "07-Jan-2019 9:30 AM" with the following values:
      | Notes        |
      | <NoteValues> |
    Then "Mohamed.Abdelmoniem" is logged out
    And "Mohamed.Abdelmoniem" is forwarded to the error page
    Examples:
      | NoteValues                                                                                                                                                                                                                                                                                |
      | Total equal the business <unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business >unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business #unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business $unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business *unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business &unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business +unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business ^unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business !unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business =unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business ~unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business \unit remaining budget                                                                                                                                                                                                                                           |
      | Total equal the business //unit remaining budget                                                                                                                                                                                                                                          |
      | Total equal the business unit remaining budget Total equal the business unit remaining budget Total equal the business unit remaining budget Total equal the business unit remaining budget Total equal the business unit remaining budget Total equal the business unit remaining budget |

  Scenario Outline: (11) Approve the PO which in WaitingApproval state (Local/Import) by an unauthorized user due to condition OR not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to Approve the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | Notes                                          |
      | Total equal the business unit remaining budget |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000015 | Mahmoud.Abdelaziz |
      | 2018000032 | Mahmoud.Abdelaziz |
      | 2018000032 | Ashraf.Salah      |
      | 2018000015 | Ahmed.Hussein     |
