# Author Dev: Fatma Al Zahraa
# Author Quality: Shirin Mahmoud

Feature: Service Purchase Order - Save New Item Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | hr1         | SuperUser                       |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | SuperUser                       | SuperUserSubRole    |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | SuperUserSubRole    | *:*                      |                                |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadUoMData         | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000006 | 000002 - Zhejiang | 2018000008  |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000057 - Bank Fees          | 0019 - M2 | 16.000   | 200.000 | 3200.000        |
      | 2020000004 | 2        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
    And the following Items exist with the below details:
      | Item                                        | Type       | UOM       | PurchaseUnit     |
      | 000057 - Bank Fees                          | SERVICE    | 0019 - M2 | 0002 - Signmedia |
      | 000001 - Hot Laminated Frontlit Fabric roll | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000057 - Bank Fees":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And the following UOMs exist for Item "000001 - Hot Laminated Frontlit Fabric roll":
      | UOM                 |
      | 0019 - M2           |
      | 0029 - Roll 2.20x50 |
      | 0032 - Roll 3.20x50 |
      | 0033 - Roll 2.70x50 |
    And edit session is "30" minutes

  # EBS-7405
  Scenario Outline: (01) Save New Service PurchaseOrder Item,  with incorrect data entry (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:10 AM" with the following values:
      | Item               | OrderUnit           | Quantity | Price  |
      | <SelectedItemCode> | <SelectedOrderUnit> | 100.000  | 10.000 |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | SelectedItemCode | SelectedOrderUnit | Field    | ErrMsg     |
      # Item does not exist
      | 999999           | 0019              | itemCode | Gen-msg-48 |
      # existing order unit but does not belong to the selected item
      # @Future | 000057           | 0029              | orderUnitCode     | Gen-msg-49 |

  # EBS-7405
  Scenario: (02) Save New Service PurchaseOrder Item, after edit session expired (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:31 AM" with the following values:
      | Item               | OrderUnit           | Quantity | Price  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000  | 10.000 |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (03) Save New Service PurchaseOrder Item, after edit session expired & PurchaseOrder is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully at "01-Jan-2021 11:31 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:31 AM" with the following values:
      | Item               | OrderUnit           | Quantity | Price  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000  | 10.000 |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-7405
  Scenario: (04) Save New Service PurchaseOrder Item, when it is not allowed in current state: Confirmed (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000006" with the following values:
      | Item               | OrderUnit           | Quantity | Price  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000  | 10.000 |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

  # EBS-7405
  Scenario: (05) Save New Service PurchaseOrder Item, when item is not Service (BusinessRule) (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:10 AM" with the following values:
      | Item                                        | OrderUnit           | Quantity | Price  |
      | 000001 - Hot Laminated Frontlit Fabric roll | 0029 - Roll 2.20x50 | 100.000  | 10.000 |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  # EBS-7405
  Scenario: (06) Save New Service PurchaseOrder Item, when saving an existing item with the same UOM (BusinessRule) (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:10 AM" with the following values:
      | Item               | OrderUnit | Quantity | Price  |
      | 000057 - Bank Fees | 0019 - M2 | 100.000  | 10.000 |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  # EBS-7405
  Scenario Outline: (07) Save New Service PurchaseOrder Item, with Missing Mandatories (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:10 AM" with the following values:
      | Item           | OrderUnit   | Quantity | Price   |
      | <ItemCodeName> | <OrderUnit> | <Qty>    | <Price> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeName       | OrderUnit           | Qty     | Price  |
      |                    | 0035 - Roll 1.27x50 | 100.000 | 10.000 |
      | ""                 | 0035 - Roll 1.27x50 | 100.000 | 10.000 |
      | N/A                | 0035 - Roll 1.27x50 | 100.000 | 10.000 |
      | 000057 - Bank Fees |                     | 100.000 | 10.000 |
      | 000057 - Bank Fees | ""                  | 100.000 | 10.000 |
      | 000057 - Bank Fees | N/A                 | 100.000 | 10.000 |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 |         | 10.000 |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | ""      | 10.000 |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | N/A     | 10.000 |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000 |        |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000 | ""     |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000 | N/A    |

  # EBS-7405
  Scenario Outline: (08) Save New Service PurchaseOrder Item, with malicious input  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2021 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2021 11:10 AM" with the following values:
      | Item           | OrderUnit   | Quantity | Price   |
      | <ItemCodeName> | <OrderUnit> | <Qty>    | <Price> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCodeName       | OrderUnit           | Qty     | Price   |
      | ay7aga             | 0035 - Roll 1.27x50 | 100.000 | 10.000  |
      | 000057 - Bank Fees | ay7aga              | 100.000 | 10.000  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 0       | 10.000  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | -2.4    | 10.000  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | Ay 7aga | 10.000  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000 | -2.4    |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000 | Ay 7aga |
      # OrderUnit does not exist
      | 000057 - Bank Fees | 9999 - Roll 1.57x50 | 100.000 | 10.000  |
