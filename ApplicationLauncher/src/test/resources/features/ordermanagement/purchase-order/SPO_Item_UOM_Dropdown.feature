# Author: Zyad Ghorab
# Tester: Shirin Mahmoud

Feature: Read Service Purchase Order Item's UOMs Dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadUoMData         | [purchaseUnitName='Signmedia'] |
    And the following Items exist with the below details:
      | Item                        | Type    | UOM       | PurchaseUnit     |
      | 000014 - Insurance service  | SERVICE | 0019 - M2 | 0002 - Signmedia |
      | 000055 - Insurance service2 | SERVICE | 0019 - M2 | 0002 - Signmedia |
      | 000056 - Shipment service   | SERVICE | 0019 - M2 | 0001 - Flexo     |
      | 000057 - Bank Fees          | SERVICE | 0019 - M2 | 0002 - Signmedia |
    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                  |
      | Gehan.Ahmed | Item:ReadAll             | [purchaseUnitName='Flexo'] |
      | Gehan.Ahmed | Item:ReadUoMData         | [purchaseUnitName='Flexo'] |
      | Gehan.Ahmed | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo'] |
    And the following UOMs exist for Item "000055 - Insurance service2":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And the following UOMs exist for Item "000056 - Shipment service":
      | UOM       |
      | 0019 - M2 |
    And the following UOMs exist for Item "000057 - Bank Fees":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     |                  |                                 |
      | 2020000006 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000055  |
      | 2020000005 | 000051 - Vendor 6 | 2018000040  |
      | 2020000006 | 000002 - Zhejiang | 2018000055  |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
      | 2020000006 | 2        | 000057 - Bank Fees          | 0019 - M2 | 20.000   |         |                 |
    And the following PurchaseOrders have no Items:
      | Code       |
      | 2020000005 |

  Scenario: (01) Read Service Purchase Order Item's UOMs dropdown, by authorized user
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read UOMs for Item with code "000057" of Service Purchase Order with code "2020000004" in a dropdown
    Then the following service item UOM values will be presented to "Gehan.Ahmed" in the dropdown:
      | UOM                 |
      | 0035 - Roll 1.27x50 |
      | 0019 - M2           |
    And total number of records returned to "Gehan.Ahmed" is "2"

  Scenario: (02) Read Service Purchase Order Item's UOMs dropdown, by authorized user (Filtered)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read UOMs for Item with code "000055" of Service Purchase Order with code "2020000004" in a dropdown
    Then the following service item UOM values will be presented to "Gehan.Ahmed" in the dropdown:
      | UOM                 |
      | 0035 - Roll 1.27x50 |
    And total number of records returned to "Gehan.Ahmed" is "1"

  Scenario: (03) Read Service Purchase Order Item's UOMs dropdown, by authorized user (No item)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read UOMs for Item with code "999999" of Service Purchase Order with code "2020000004" in a dropdown
    Then total number of records returned to "Gehan.Ahmed" is "0"

  Scenario: (04) Read Service Purchase Order Item's UOMs dropdown, where PurchaseOrder has been deleted by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to read UOMs for Item with code "000057" of Service Purchase Order with code "2020000004" in a dropdown
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (05) Read Service Purchase Order Item's UOMs dropdown, when it is not allowed in current state: Confirmed (Abuse case)
    Given user is logged in as "hr1"
    When "hr1" requests to read UOMs for Item with code "000057" of Service Purchase Order with code "2020000006" in a dropdown
    Then "hr1" is logged out
    And "hr1" is forwarded to the error page

  Scenario Outline: (06) Read Service Purchase Order Item's UOMs dropdown, by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read UOMs for Item with code "<ItemCode>" of Service Purchase Order with code "<POCode>" in a dropdown
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | ItemCode |
      # PO with different BU
      | 2020000005 | 000057   |
      # Item with different BU
      | 2020000004 | 000056   |
