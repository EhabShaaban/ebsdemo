# updated by: Eman Mansour (EBS - 7526)
Feature: Save PO Company

  Note:
  1- There are no scenarios for mandatory fields validation, because in the company section, all
  fields are optional in all states that allow EditCompany action  (until we add the parallel payment state machine)
  2- There are no scenarios for saving disabled fields, because in the company section, all
  fields are optional in all states that allow EditCompany action.

  Background:
    Given the following users and roles exist:
      | Name                  | Role                                                  |
      | hr1                   | SuperUser                                             |
      | Gehan.Ahmed           | LogisticsResponsible_Signmedia                        |
      | Gehan.Ahmed.NoCompany | PurchasingResponsible_Signmedia_CannotViewCompanyRole |
    And the following roles and sub-roles exist:
      | Role                                                  | Subrole                 |
      | SuperUser                                             | SuperUserSubRole        |
      | LogisticsResponsible_Signmedia                        | POOwner_Signmedia       |
      | LogisticsResponsible_Signmedia                        | CompanyViewer           |
      | LogisticsResponsible_Signmedia                        | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole | POOwner_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole | PurUnitReader_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | POOwner_Signmedia       | PurchaseOrder:UpdateCompany  | [purchaseUnitName='Signmedia'] |
      | CompanyViewer           | Company:ReadAll              |                                |
      | CompanyViewer           | Company:ReadBankDetails      |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole        | *:*                          |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                  | Condition                   |
      | Gehan.Ahmed | PurchaseOrder:UpdateCompany | [purchaseUnitName='Offset'] |
      | Gehan.Ahmed | PurchaseOrder:UpdateCompany | [purchaseUnitName='Flexo']  |
    And the following users doesn't have the following permissions:
      | User                  | Permission              |
      | Gehan.Ahmed.NoCompany | Company:ReadAll         |
      | Gehan.Ahmed.NoCompany | Company:ReadBankDetails |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000017 | LOCAL_PO   | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000004 | IMPORT_PO  | Draft     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000013 | LOCAL_PO   | Draft     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000024 | LOCAL_PO   | Draft     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000055 | LOCAL_PO   | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000017 | 0002 - Signmedia |                  |                                 |
      | 2018000004 | 0003 - Offset    |                  |                                 |
      | 2018000013 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2018000024 | 0003 - Offset    |                  |                                 |
      | 2018000055 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia |                  |                                 |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000017  |
      | 2020000005 | 000002 - Zhejiang | 2018000017  |
      | 2020000006 | 000002 - Zhejiang | 2018000017  |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
      | 0003 | HPS       |
    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank                                     | AccountNumber           |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819789 - EGP     |
      | 18 | 0002 - DigiPro   | 0004 - CTBC Bank CO., LTD.               | 2021222324253 - USD     |
      | 19 | 0002 - DigiPro   | 0005 - Qatar National Bank Al Ahli-Tanta | 26272829303132 - EGP    |
      | 1  | 0003 - HPS       | 0001 - Bank Misr                         | 14785-65626/233_7 - EGP |
      | 2  | 0003 - HPS       | 0001 - Bank Misr                         | Ay7aga - USD            |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893458 - EGP     |
      | 15 | 0001 - AL Madina | 0002 - National Bank of Egypt            | 1011121314678 - USD     |
    And edit session is "30" minutes

  ####### Save Company section (Happy Path) ##############################################################

  Scenario Outline: (01) Save Company section within edit session - for all PO types (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | CompanyCode   | BankAccountId   |
      | <CompanyCode> | <BankAccountId> |
    Then PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    Then Company section of PurchaseOrder with Code "<POCode>" is updated as follows:
      | Company   | BankAccountNumber   |
      | <Company> | <BankAccountNumber> |
    And the lock by "Gehan.Ahmed" on Company section of PurchaseOrder with Code "<POCode>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | POCode     | CompanyCode | BankAccountId | Company          | BankAccountNumber                   |
      | 2018000013 | 0003        | 2             | 0003 - HPS       | Ay7aga - USD - Bank Misr            |
      | 2018000013 | 0003        |               | 0003 - HPS       |                                     |
      | 2018000013 |             |               |                  |                                     |
      | 2018000021 | 0003        | 1             | 0003 - HPS       | 14785-65626/233_7 - EGP - Bank Misr |
      | 2018000021 | 0003        |               | 0003 - HPS       |                                     |
      | 2018000021 |             |               |                  |                                     |
      | 2020000004 | 0001        | 14            | 0001 - AL Madina | 1234567893458 - EGP - Bank Misr     |
      | 2020000004 | 0001        |               | 0001 - AL Madina |                                     |
      | 2020000006 |             |               |                  |                                     |

  ####### Save Company section: Malicious Data Entry (Abuse Case) ##############################################################

  Scenario Outline: (02) Save Company section with Malicious Input (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | CompanyCode   | BankAccountId   |
      | <CompanyCode> | <BankAccountId> |
    Then the lock by "Gehan.Ahmed" on Company section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | CompanyCode | BankAccountId |
      | 2018000021 | Ay7aga      |               |
      | 2018000013 | 00030       |               |
      | 2020000004 | ""          |               |
      | 2020000004 | 0001        | Ay7aga        |
      | 2018000013 | 0001        | 99999.999     |
      | 2018000021 | 0001        | ""            |
      # Change company in SERVICE_PO
      | 2020000004 | 0002        |               |
      | 2020000004 |             |               |
      # BankAccount is not related to Company or does not exist
      | 2018000013 | 0001        | 1             |
      | 2018000013 | 0001        | 9999          |
      | 2018000021 | 0001        | 1             |
      | 2018000021 | 0001        | 9999          |
      | 2020000004 | 0001        | 1             |
      | 2020000004 | 0001        | 9999          |
      # Send BankAccount without Company
      | 2018000013 |             | 17            |
      | 2018000021 |             | 17            |
      | 2020000004 |             | 17            |

  ####### Save Company section (After edit session expires) ##############################################################

  Scenario Outline: (03) Save Company section after edit session expired - for all PO types (with correct data entry) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:41 AM" with the following values:
      | CompanyCode | BankAccountId |
      | 0001        | 14            |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | Type       |
      | 2018000013 | IMPORT_PO  |
      | 2018000021 | LOCAL_PO   |
      | 2020000004 | SERVICE_PO |

  Scenario Outline: (04) Save Company section after edit session expired & PO is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 am"
    When "Gehan.Ahmed" saves Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 am" with the following values:
      | CompanyCode | BankAccountId |
      | 0001        | 14            |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | Type       |
      | 2018000013 | IMPORT_PO  |
      | 2018000021 | LOCAL_PO   |
      | 2020000004 | SERVICE_PO |

  ####### Save Company section (Unauthorized Access) ##############################################################

  Scenario Outline: (05) Save Company section by an unauthorized user due to condition - for all PO types (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves Company section of PurchaseOrder with Code "<POCode>" with the following values:
      | CompanyCode | BankAccountId |
      | 0001        | 14            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | Type       |
      | 2018000004 | IMPORT_PO  |
      | 2018000024 | LOCAL_PO   |
      | 2020000005 | SERVICE_PO |

  Scenario Outline: (07) Save Company section by a user who is not authorrized to read one of data entry - for all PO types (Abuse Case)
    Given user is logged in as "<User>"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 9:10 AM"
    When "<User>" saves Company section of PurchaseOrder with Code "<POCode>" with the following values:
      | CompanyCode | BankAccountId |
      | 0001        | 14            |
    Then the lock by "<User>" on Company section of PurchaseOrder with Code "<POCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                  | POCode     | Type       |
      | Gehan.Ahmed.NoCompany | 2018000013 | IMPORT_PO  |
      | Gehan.Ahmed.NoCompany | 2018000021 | LOCAL_PO   |
      | Gehan.Ahmed.NoCompany | 2020000004 | SERVICE_PO |