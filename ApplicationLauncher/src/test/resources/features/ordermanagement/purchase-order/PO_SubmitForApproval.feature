# Author: Nancy Shoukry 22-Apr-2019
# Reviewer:

Feature: SubmitForApproval Purchase Order

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia         |
      | PurchasingResponsible_Flexo      | POOwner_Flexo             |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated        |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia |
      | SuperUser                        | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                      | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:SubmitForApproval | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:SubmitForApproval | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:SubmitForApproval | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                             |                                 |
    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000029 | IMPORT_PO | OpenForUpdates     | 0006           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000025 | IMPORT_PO | Draft              | 0001           |
      | 2018000026 | IMPORT_PO | Draft              | 0006           |
      | 2018000105 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           |
      | 2018000030 | LOCAL_PO  | OpenForUpdates     | 0001           |
      | 2018000031 | LOCAL_PO  | OpenForUpdates     | 0006           |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           |
      | 2018000018 | LOCAL_PO  | Shipped            | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete   | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled          | 0002           |
      | 2018000027 | LOCAL_PO  | Draft              | 0001           |

    And the following control points exist:
      | ControlPoint | User                | IsMain |
      | 0001         | Ashraf.Salah        | true   |
      | 0001         | Ahmed.Seif          | false  |
      | 0002         | Mohamed.Abdelmoniem | true   |
      | 0003         | Essam.Mahmoud       | true   |
      | 0004         | Ahmed.Hussein       | true   |
      | 0005         | Mohamed.Abdelmoniem | true   |
    And the following approval policies exist:
      | Code | DocumentType | Condition                       |
      | 0001 | IMPORT_PO    | [purchaseUnitName='Signmedia']  |
      | 0002 | LOCAL_PO     | [purchaseUnitName='Flexo']      |
      | 0003 | LOCAL_PO     | [purchaseUnitName='Corrugated'] |
      | 0004 | LOCAL_PO     | [purchaseUnitName='Corrugated'] |
    And Approval policy "0001" has the following control points:
      | ControlPoint |
      | 0001         |
      | 0002         |
      | 0003         |
    And Approval policy "0002" has the following control points:
      | ControlPoint |
      | 0004         |
      | 0005         |
    And the following PurchaseOrders has an empty ApprovalCycles section:
      | Code       |
      | 2018000006 |
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000030":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |

  @ResetData
  Scenario: (01) SubmitForApproval PO (Import) by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to SubmitForApproval the PurchaseOrder with Code "2018000006" at "7-Jan-2019 9:30 AM"
    Then PurchaseOrder "2018000006" has the following approval cycles:
      | ApprovalCycleNo | StartDate            | EndDate | FinalDecision |
      | 1               | 07-Jan-2019 09:30 AM |         |               |
    And the new ApprovalCycleNo "1" in PurchaseOrder with code "2018000006" has the following values:
      | ControlPoint | User                | DateTime | Decision | Notes |
      | 0001         | Ashraf.Salah        |          |          |       |
      | 0002         | Mohamed.Abdelmoniem |          |          |       |
      | 0003         | Essam.Mahmoud       |          |          |       |
    And PurchaseOrder with Code "2018000006" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State           |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | WaitingApproval |
    And a success notification is sent to "Gehan.Ahmed" with the following message "PO-msg-44"

  @ResetData
  Scenario: (02) SubmitForApproval PO (Local) by an authorized user (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to SubmitForApproval the PurchaseOrder with Code "2018000030" at "7-Jan-2019 9:30 AM"
    Then PurchaseOrder "2018000030" has the following approval cycles:
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 3               | 07-Jan-2019 09:30 AM |                      |               |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the new ApprovalCycleNo "3" in PurchaseOrder with code "2018000030" has the following values:
      | ControlPoint | User                | DateTime | Decision | Notes |
      | 0004         | Ahmed.Hussein       |          |          |       |
      | 0005         | Mohamed.Abdelmoniem |          |          |       |
    And PurchaseOrder with Code "2018000030" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State           |
      | Amr.Khalil    | 07-Jan-2019 09:30 AM | WaitingApproval |
    And a success notification is sent to "Amr.Khalil" with the following message "PO-msg-44"

  Scenario: (03) SubmitForApproval PO with no matching approval policy (Exception Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to SubmitForApproval the PurchaseOrder with Code "2018000029" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "PO-msg-54"

  Scenario Outline: (04) SubmitForApproval PO (Import/Local) that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-Jan-2019 9:20 AM"
    When "<User>" requests to SubmitForApproval the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-14"
    Examples:
      | POSection | POCode     | POType    | User        |
      | Company   | 2018000006 | IMPORT_PO | Gehan.Ahmed |
      | Company   | 2018000030 | LOCAL_PO  | Amr.Khalil  |

  Scenario Outline: (05) SubmitForApproval PO (Import/Local) in Draft state (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to SubmitForApproval the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit | User        |
      | 2018000021 | Draft | 0002           | Gehan.Ahmed |
      | 2018000027 | Draft | 0002           | Amr.Khalil  |

  Scenario Outline: (06) SubmitForApproval PO (Import/Local) when action is not allowed in current state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to SubmitForApproval the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000015 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000016 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000017 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000018 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000019 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000020 | Cancelled          | 0002           |

  Scenario Outline: (7) SubmitForApproval PO by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to SubmitForApproval the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     |
      | Mahmoud.Abdelaziz | 2018000006 |
      | Mahmoud.Abdelaziz | 2018000030 |
      | Amr.Khalil        | 2018000006 |
      | Gehan.Ahmed       | 2018000030 |

  Scenario Outline: (08) SubmitForApproval PO with missing mandattory fields
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to SubmitForApproval the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-33"
    And the following fields "<Missing Fields>" which sent to "Gehan.Ahmed" are marked as missing
    Examples:
      | POCode     | Missing Fields                                                                                                                                |
      | 2018000105 | incotermCode , currencyCode , paymentTermCode , shippingInstructionsCode , collectionDate , transportModeCode , RequiredDocs , OrderItemsList |

  Scenario: (09) SubmitForApproval PO with multiple matching approval policies (Exception Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to SubmitForApproval the PurchaseOrder with Code "2018000031" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "PO-msg-56"
