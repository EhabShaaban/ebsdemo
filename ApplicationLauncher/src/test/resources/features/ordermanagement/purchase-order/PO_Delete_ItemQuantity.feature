# Author: Yara Ameen & Waseem Salama
# Reviewer:

Feature: Delete Item Quantity From PO

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000023 | Cleared            | 0002           |
      | 2018000029 | OpenForUpdates     | 0006           |
      | 2018000104 | Draft              | 0001           |
      | 2018100000 | Draft              | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000030 | OpenForUpdates   | 0001           |
      | 2018000031 | OpenForUpdates   | 0006           |
      | 2018100001 | Draft            | 0002           |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000002 | 000003   | 4     |
      | 2018000005 | 000001   | 50    |
      | 2018000006 | 000003   | 46    |
      | 2018000007 | 000001   | 51    |
      | 2018000008 | 000001   | 52    |
      | 2018000009 | 000001   | 53    |
      | 2018000010 | 000002   | 54    |
      | 2018000011 | 000001   | 55    |
      | 2018000012 | 000001   | 56    |
      | 2018000021 | 000002   | 18    |
      | 2018000023 | 000002   | 10    |
      | 2018000029 | 000012   | 42    |
      | 2018000104 | 000005   | 48    |
      | 2018100000 | 000012   | 44    |
      # Local
      | 2018000013 | 000001   | 20    |
      | 2018000014 | 000003   | 47    |
      | 2018000015 | 000001   | 57    |
      | 2018000016 | 000001   | 58    |
      | 2018000017 | 000001   | 59    |
      | 2018000018 | 000001   | 26    |
      | 2018000019 | 000001   | 60    |
      | 2018000020 | 000001   | 61    |
      | 2018000030 | 000006   | 49    |
      | 2018000031 | 000012   | 43    |
      | 2018100001 | 000012   | 45    |

  ######## Happy Paths
  # EBS-4029
  @ResetData
  Scenario Outline:(01) Delete Item Quantity from Import/Local PO, where PO is Draft/OpenForUpdates - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "7-Jan-2019 9:30 am"
    Then quantity with id "<QtyID>" is deleted successfully
    And Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | <User>        | 07-Jan-2019 09:30 AM |
    And PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | <User>        | 07-Jan-2019 09:30 AM |
    And a success notification is sent to "<User>" with the following message "Gen-msg-18"
    Examples:
      | QtyID | User        | POCode     | ItemCode |
      | 18    | Gehan.Ahmed | 2018000021 | 000002   |
      | 20    | Gehan.Ahmed | 2018000013 | 000001   |
      | 46    | Gehan.Ahmed | 2018000006 | 000003   |
      | 47    | Gehan.Ahmed | 2018000014 | 000003   |
      | 42    | Amr.Khalil  | 2018000029 | 000012   |
      | 43    | Amr.Khalil  | 2018000031 | 000012   |
      | 48    | Amr.Khalil  | 2018000104 | 000005   |
      | 49    | Amr.Khalil  | 2018000030 | 000006   |

  ######## Exceptions
  # EBS-4029
  @ResetData
  Scenario Outline: (02) Delete Item Quantity from Import/Local PO, where PO doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | QtyID | POCode     | ItemCode |
      | 44    | 2018100000 | 000012   |
      | 45    | 2018100001 | 000012   |

  # EBS-4029
  @ResetData
  Scenario Outline: (03) Delete Item Quantity from Import/Local PO, where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | QtyID | POCode     | ItemCode |
      | 44    | 2018100000 | 000012   |
      | 45    | 2018100001 | 000012   |

  # EBS-4029
  @ResetData
  Scenario Outline: (04) Delete Item Quantity from Import/Local PO, where Quantity doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Quantity with id "<QtyID>" of Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | QtyID | POCode     | ItemCode |
      | 44    | 2018100000 | 000012   |
      | 45    | 2018100001 | 000012   |

  # EBS-4029
  Scenario Outline: (05) Delete Item Quantity from Import/Local PO, in state where this action is not allowed (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-21"
    Examples:
      | POCode     | State              | ItemCode | QtyID |
      # Import
      | 2018000002 | DeliveryComplete   | 000003   | 4     |
      | 2018000005 | Approved           | 000001   | 50    |
      | 2018000007 | WaitingApproval    | 000001   | 51    |
      | 2018000008 | Confirmed          | 000001   | 52    |
      | 2018000009 | FinishedProduction | 000001   | 53    |
      | 2018000010 | Shipped            | 000002   | 54    |
      | 2018000011 | Arrived            | 000001   | 55    |
      | 2018000012 | Cancelled          | 000001   | 56    |
      | 2018000023 | Cleared            | 000002   | 10    |
      # Local
      | 2018000015 | WaitingApproval    | 000001   | 57    |
      | 2018000016 | Approved           | 000001   | 58    |
      | 2018000017 | Confirmed          | 000001   | 59    |
      | 2018000018 | Shipped            | 000001   | 26    |
      | 2018000019 | DeliveryComplete   | 000001   | 60    |
      | 2018000020 | Cancelled          | 000001   | 61    |

  # EBS-4029
  Scenario Outline: (06) Delete Item Quantity from Import/Local PO, when OrderItems section is locked by another user - Import/Local PurchaseOrder (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And OrderItems section of PurchaseOrder with code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "7-Jan-2019 9:30 am"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | QtyID | POCode     | ItemCode |
      | 18    | 2018000021 | 000002   |
      | 20    | 2018000013 | 000001   |

  ######## Abuse Cases
  # EBS-4029
  Scenario Outline: (07) Delete Item Quantity from Import/Local PO, by an unauthorized user (due to condition or not) -  Local/Import (Unauthorized access/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete the quantity with id "<QtyID>" of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode | QtyID |
      | Mahmoud.Abdelaziz | 2018000021 | 000002   | 18    |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   | 20    |
      | Amr.Khalil        | 2018000021 | 000002   | 18    |
      | Amr.Khalil        | 2018000013 | 000001   | 20    |
      | Afaf              | 2018000021 | 000002   | 18    |
      | Afaf              | 2018000013 | 000001   | 20    |
