# Author Dev: Fatma Al Zahraa
# Author Quality: Shirin Mahmoud

Feature: Service Purchase Order - Save New Item HP

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadUoMData         | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000004 | SERVICE_PO | Draft | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000057 - Bank Fees          | 0019 - M2 | 16.000   | 200.000 | 3200.000        |
      | 2020000004 | 2        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
    And the following Items exist with the below details:
      | Item               | Type    | UOM       | PurchaseUnit     |
      | 000057 - Bank Fees | SERVICE | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000057 - Bank Fees":
      | UOM                 |
      | 0019 - M2           |
      | 0035 - Roll 1.27x50 |
    And edit session is "30" minutes

  # EBS-5729
  Scenario: (01) Save New Service PurchaseOrder Item, with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And Items section of Service PurchaseOrder with Code "2020000004" is locked by "Gehan.Ahmed" at "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to Items section of Service PurchaseOrder with Code "2020000004" at "01-Jan-2019 11:10 AM" with the following values:
      | Item               | OrderUnit           | Quantity   | Price     |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.123456 | 10.123456 |
    Then Service PurchaseOrder with Code "2020000004" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 01-Jan-2019 11:10 AM |
    And the lock by "Gehan.Ahmed" on Items section of Service PurchaseOrder with Code "2020000004" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    And Items section of Service PurchaseOrder with Code "2020000004" is updated as follows and displayed to "Gehan.Ahmed":
      | SPOCode    | Item                        | OrderUnit           | Quantity   | Price     | ItemTotalAmount   |
      | 2020000004 | 000057 - Bank Fees          | 0035 - Roll 1.27x50 | 100.123456 | 10.123456 | 1013.595401383936 |
      | 2020000004 | 000055 - Insurance service2 | 0019 - M2           | 20.000     | 200.000   | 4000.000          |
      | 2020000004 | 000057 - Bank Fees          | 0019 - M2           | 16.000     | 200.000   | 3200.000          |


