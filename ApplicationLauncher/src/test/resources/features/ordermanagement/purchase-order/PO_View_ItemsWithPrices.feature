# Reviewer: Somaya (28-Jan-2019 10:20 AM)
# Reviewer: Somaya (05-Feb-2019 02:25 AM)
# Updated: Hend and Somaya (13-Mar-2019)

Feature: View ItemsWithPrices section PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                        | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadItemsWithPrices | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadItemsWithPrices | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadItemsWithPrices | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                               |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State   | PurchasingUnit | VendorCode |
      | 2018000022 | Shipped | 0002           | 000002     |
      | 2018000025 | Draft   | 0001           | 000002     |
      | 2018000026 | Draft   | 0006           | 000002     |
      | 2018100004 | Draft   | 0002           | 000002     |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit | VendorCode |
      | 2018000013 | Draft | 0002           | 000002     |
      | 2018000027 | Draft | 0001           | 000002     |
      | 2018000028 | Draft | 0006           | 000002     |
      | 2018100006 | Draft | 0002           | 000002     |
    And the following PurchaseOrders has an empty OrderItems section:
      | Code       |
      | 2018100004 |
      | 2018100006 |

    # we removed fields that are not supposed to be persisted: ItemName and BaseUnit
    # PO 2018000022
    And PurchaseOrder with code "2018000022" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000022" has the following Quantities:
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50  | Roll 2.70x50 | 3.0              | 0.0%     |

    # PO 2018000013
    And PurchaseOrder with code "2018000013" has the following OrderItems:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | 0019 |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50  | Roll 3.20x50 | 3.0              | 0.0%     |
    And Item with code "000002" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50  | Roll 2.70x50 | 3.0              | 0.0%     |

    # PO 2018000025
    And PurchaseOrder with code "2018000025" has the following OrderItems:
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | 0019 |
    And Item with code "000005" PurchaseOrder with code "2018000025" has empty Quantities

    # PO 2018000026
    And PurchaseOrder with code "2018000026" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000026" has empty Quantities

    # PO 2018000027
    And PurchaseOrder with code "2018000027" has the following OrderItems:
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | 0019 |
    And Item with code "000006" PurchaseOrder with code "2018000027" has empty Quantities

    # PO 2018000028
    And PurchaseOrder with code "2018000028" has the following OrderItems:
      | ItemCode | ItemName | QtyInDN | Base |
      | 000012   | Ink2     |         | 0014 |
    And Item with code "000012" PurchaseOrder with code "2018000028" has empty Quantities
    And the following Measures exist:
      | Code | Type     | Symbol       | Name         |
      | 0003 | Standard | Kg           | Kilogram     |
      | 0019 | Standard | M2           | Square Meter |
      | 0014 | Standard | Liter        | Liter        |
      | 0029 | Business | Roll 2.20x50 | Roll 2.20x50 |
      | 0033 | Business | Roll 2.70x50 | Roll 2.70x50 |
      | 0032 | Business | Roll 3.20x50 | Roll 3.20x50 |
    And the following Items exist with more data:
      | ItemCode | Name                                                    | ItemGroup | BaseUnit | BatchManaged | OldItemReference  | Purchase Unit | Type       |
      | 000001   | Hot Laminated Frontlit Fabric roll                      | 000003    | 0019     | False        | 12345678910111213 | 0002          | COMMERCIAL |
      | 000002   | Hot Laminated Frontlit Backlit roll                     | 000003    | 0019     | False        | 12345678910111213 | 0002          | COMMERCIAL |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 000003    | 0019     | False        | 12345678910111213 | 0001          | COMMERCIAL |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000003    | 0019     | False        | 12345678910111213 | 0001          | COMMERCIAL |
      | 000012   | Ink2                                                    | 000003    | 0014     | False        | 12345678910111213 | 0006          | COMMERCIAL |
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             |
      | 000014  | 000002   | 000002     | 0033    | GDF730-ddd       | 567891011        | 11500.80 | 0003         | 0002             |

  #### Happy Path  ####
  # EBS-1330
  Scenario: (01) View OrderItems WithPrices, by an authorized user who has one role - Import PO - Signmedia (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view OrderItems section of PurchaseOrder with code "2018000022"
    Then the following values of OrderItems section are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll | 18000.0 | M2   |
    And the following values of Quantities list for Item "000002" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDB550-610       |
      | 50  | Roll 2.70x50 | 3                | 0%       | GDF730-ddd       |

  # EBS-1330
  Scenario: (02) View OrderItems WithPrices, by an authorized user who has one role - Local PO - Signmedia (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view OrderItems section of PurchaseOrder with code "2018000013"
    Then the following values of OrderItems section are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | M2   |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | M2   |
    And the following values of Quantities list for Item "000001" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDF530-440       |
      | 50  | Roll 3.20x50 | 3                | 0%       | GDF630-440       |
    And the following values of Quantities list for Item "000002" are displayed to "Gehan.Ahmed":
      | Qty | OrderUnit    | UnitPrice(Gross) | Discount | ItemCodeAtVendor |
      | 100 | Roll 2.20x50 | 2                | 10%      | GDB550-610       |
      | 50  | Roll 2.70x50 | 3                | 0%       | GDF730-ddd       |

  # EBS-1330
  Scenario: (03) View OrderItems WithPrices, by an authorized user who has one role - Import PO - Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view OrderItems section of PurchaseOrder with code "2018000025"
    Then the following values of OrderItems section are displayed to "Amr.Khalil":
      | ItemCode | ItemName                                              | QtyInDN | Base |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 |         | M2   |
    And an empty Quantities list for Item "000005" is displayed to "Amr.Khalil"

  # EBS-1330
  Scenario: (04) View OrderItems WithPrices, by an authorized user who has one role - Local PO - Flexo (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view OrderItems section of PurchaseOrder with code "2018000026"
    Then the following values of OrderItems section are displayed to "Amr.Khalil":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" is displayed to "Amr.Khalil"

  # EBS-1330
  Scenario: (05) View OrderItems WithPrices, by an authorized user who has one role - Import PO - Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view OrderItems section of PurchaseOrder with code "2018000027"
    Then the following values of OrderItems section are displayed to "Amr.Khalil":
      | ItemCode | ItemName                                                | QtyInDN | Base |
      | 000006   | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 |         | M2   |
    And an empty Quantities list for Item "000006" is displayed to "Amr.Khalil"

  # EBS-1330
  Scenario: (06) View OrderItems WithPrices, by an authorized user who has one role - Local PO - Corrugated (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view OrderItems section of PurchaseOrder with code "2018000028"
    Then the following values of OrderItems section are displayed to "Amr.Khalil":
      | ItemCode | ItemName | QtyInDN | Base  |
      | 000012   | Ink2     |         | Liter |
    And an empty Quantities list for Item "000012" is displayed to "Amr.Khalil"

  # EBS-1330
  Scenario Outline: (07) View OrderItems WithPrices, by an authorized user - Local/Import - Empty Order Items (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view OrderItems section of PurchaseOrder with code "<POCode>"
    Then an empty OrderItems section is displayed to "Gehan.Ahmed"
    Examples:
      | POCode     |
      | 2018100004 |
      | 2018100006 |

  ##### Exception Cases
  # EBS-1330
  @ResetData
  Scenario Outline: (08) View OrderItems WithPrices, where PurchaseOrder has been deleted by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view OrderItems section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000004 |

  # EBS-1330
  Scenario Outline: (09) View OrderItems WithPrices, by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User       | POCode     |
      | Afaf       | 2018000022 |
      | Afaf       | 2018000013 |
      | Amr.Khalil | 2018000022 |
      | Amr.Khalil | 2018000013 |