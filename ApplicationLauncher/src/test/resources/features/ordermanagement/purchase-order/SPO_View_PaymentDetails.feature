# Author Dev: Eman Mansour, Zyad Ghorab
# Author Quality: Shirin Mahmoud
# updated by: Fatma Al Zahraa (EBS - 7642)

Feature: View Payment section in Service Purchase Order

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Ahmed.Seif  | Quality_Specialist              |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia |
      | Quality_Specialist              | POViewer           |
      | SuperUser                       | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                     | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadPaymentTerms | [purchaseUnitName='Signmedia'] |
      | POViewer           | PurchaseOrder:ReadPaymentTerms |                                |
      | SuperUserSubRole   | *:*                            |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                     | Condition                  |
      | Gehan.Ahmed | PurchaseOrder:ReadPaymentTerms | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000004 | SERVICE_PO | Draft | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     |                  |                                 |
    #@INSERT
    And the following PaymentTermData for PurchaseOrders exist:
      | Code       | Currency   | PaymentTerms                        |
      | 2020000004 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
    And the following PurchaseOrder with code "2020000005" has an empty PaymentDetails section


  #### Happy Path  ####
  #EBS-7336
  #EBS-7642
  Scenario Outline: (01) View Payment Details section by an authorized user who has one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view PaymentDetails section of PurchaseOrder with code "<POCodeValue>"
    Then the following values of Payment section are displayed to "<User>":
      | CurrencyISO     | PaymentTerms        |
      | <CurrencyValue> | <PaymentTermsValue> |
    Examples:
      | User        | POCodeValue | CurrencyValue | PaymentTermsValue                   |
      | Gehan.Ahmed | 2020000004  | USD           | 0002 - 20% advance, 80% Copy of B/L |
      | Ahmed.Seif  | 2020000004  | USD           | 0002 - 20% advance, 80% Copy of B/L |

  #### Exceptions  ####
  #EBS-7336
  Scenario: (02) View Payment Details section where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to view PaymentDetails section of PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-7642
  Scenario: (03) View Payment Details section, by an authorized user - Local/Import/Service - Empty Payment Details (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view PaymentDetails section of PurchaseOrder with code "2020000005"
    Then PaymentDetails Section is displayed to "Ahmed.Seif" with empty values

  ### Abuse Cases  ####
#  EBS-7336
  Scenario: (04) View Payment Details section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view PaymentDetails section of PurchaseOrder with code "2020000005"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
