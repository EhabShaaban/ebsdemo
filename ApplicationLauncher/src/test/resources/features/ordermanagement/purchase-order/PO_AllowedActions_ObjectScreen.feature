#Auther: Hossam bayomy , Hend Ahmed 12-Dec-2018 10:00 AM
# updated by: Fatma Al Zahraa (EBS - 8346)
#UpdatedBy: Waseem Salama, Shirin Mahmoud (EBS-6949)

Feature: Read Allowed Actions for PurchaseOrder Object Screen

  Background:
    Given the following users and roles exist:
      | Name                              | Role                             |
      # one role - one subrole - without condition
      | Ashraf.Fathi                      | M.D                              |
      # One role - one subrole - with condition
      | TestUser10                        | PurchasingResponsible_Signmedia  |
      # Two Roles --> similar subroles - Different conditions
      | Amr.Khalil                        | PurchasingResponsible_Flexo      |
      | Amr.Khalil                        | PurchasingResponsible_Corrugated |
      # Two Roles --> Two Subroles - Different Subroles with and without condition
      | TestUser9                         | PurchasingResponsible_Signmedia  |
      | TestUser9                         | AccountantHead                   |
      # Not authorized
      | Afaf                              | FrontDesk                        |
      #SuperUser
      | hr1                               | SuperUser                        |
      #User who is authorized to read only some sections
      | CannotReadSections                | CannotReadSectionsRole           |
      #User can reject, approve and add AddPL/DNQuantity
      | RejectApproveAddPL/DNQuantityUser | LogisticsResponsible_Signmedia   |
      | RejectApproveAddPL/DNQuantityUser | B.U.Director_Signmedia           |
      # User with limited permissions
      | Mahmoud.Abdelaziz                 | Storekeeper_Signmedia            |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                    |
      | M.D                              | POViewer                   |

      | PurchasingResponsible_Signmedia  | POOwner_Signmedia          |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia         |
      | PurchasingResponsible_Signmedia  | POApprover_Signmedia       |

      | PurchasingResponsible_Flexo      | POOwner_Flexo              |
      | PurchasingResponsible_Flexo      | POViewer_Flexo             |
      | PurchasingResponsible_Flexo      | POApprover_Flexo           |

      | PurchasingResponsible_Corrugated | POOwner_Corrugated         |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated        |
      | PurchasingResponsible_Corrugated | POApprover_Corrugated      |

      | AccountantHead                   | POPaymentDecsionMaker      |
      | AccountantHead                   | POViewer                   |

      | LogisticsResponsible_Signmedia   | POViewer_Signmedia         |
      | LogisticsResponsible_Signmedia   | POOwner_Signmedia          |
      | LogisticsResponsible_Signmedia   | POLogisticsOwner_Signmedia |

      | B.U.Director_Signmedia           | POViewer_Signmedia         |
      | B.U.Director_Signmedia           | POApprover_Signmedia       |

      | Storekeeper_Signmedia            | POViewerLimited_Signmedia  |
      | CannotReadSectionsRole           | CannotReadSectionsSubRole  |
      | SuperUser                        | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                             | Condition                       |
      | POViewer                   | PurchaseOrder:ReadAll                  |                                 |
      | POViewer                   | PurchaseOrder:ReadCompany              |                                 |
      | POViewer                   | PurchaseOrder:ReadConsigneeData        |                                 |
      | POViewer                   | PurchaseOrder:ReadVendor               |                                 |
      | POViewer                   | PurchaseOrder:ReadItemsWithPrices      |                                 |
      | POViewer                   | PurchaseOrder:ReadPaymentTerms         |                                 |
      | POViewer                   | PurchaseOrder:ReadRequiredDocuments    |                                 |
      | POViewer                   | PurchaseOrder:ReadApprovalCycles       |                                 |
      | POViewer                   | PurchaseOrder:ExportPDF                |                                 |

      | POViewer_Signmedia         | PurchaseOrder:ReadAll                  | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadCompany              | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadConsigneeData        | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadVendor               | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadItemsWithPrices      | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadPaymentTerms         | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadRequiredDocuments    | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ReadApprovalCycles       | [purchaseUnitName='Signmedia']  |
      | POViewer_Signmedia         | PurchaseOrder:ExportPDF                | [purchaseUnitName='Signmedia']  |

      | POOwner_Signmedia          | PurchaseOrder:UpdateCompany            | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:UpdateConsignee          | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:UpdatePaymentTerms       | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:UpdateItem               | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:UpdateRequiredDocuments  | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:MarkAsPIRequested        | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:OpenUpdates              | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:SubmitForApproval        | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:Confirm                  | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:MarkAsShipped            | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:MarkAsDeliveryComplete   | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:Cancel                   | [purchaseUnitName='Signmedia']  |
      | POOwner_Signmedia          | PurchaseOrder:Delete                   | [purchaseUnitName='Signmedia']  |

      | POViewer_Flexo             | PurchaseOrder:ReadAll                  | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadCompany              | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadConsigneeData        | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadVendor               | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadItemsWithPrices      | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadPaymentTerms         | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadRequiredDocuments    | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ReadApprovalCycles       | [purchaseUnitName='Flexo']      |
      | POViewer_Flexo             | PurchaseOrder:ExportPDF                | [purchaseUnitName='Flexo']      |

      | POOwner_Flexo              | PurchaseOrder:UpdateCompany            | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:UpdateConsignee          | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:UpdatePaymentTerms       | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:UpdateItem               | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:UpdateRequiredDocuments  | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:MarkAsPIRequested        | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:OpenUpdates              | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:SubmitForApproval        | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:Confirm                  | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:MarkAsShipped            | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:MarkAsDeliveryComplete   | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:Cancel                   | [purchaseUnitName='Flexo']      |
      | POOwner_Flexo              | PurchaseOrder:Delete                   | [purchaseUnitName='Flexo']      |

      | POViewer_Corrugated        | PurchaseOrder:ReadAll                  | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadCompany              | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadConsigneeData        | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadVendor               | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadItemsWithPrices      | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadPaymentTerms         | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadRequiredDocuments    | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ReadApprovalCycles       | [purchaseUnitName='Corrugated'] |
      | POViewer_Corrugated        | PurchaseOrder:ExportPDF                | [purchaseUnitName='Corrugated'] |

      | POOwner_Corrugated         | PurchaseOrder:UpdateCompany            | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:UpdateConsignee          | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:UpdatePaymentTerms       | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:UpdateItem               | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:UpdateRequiredDocuments  | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:MarkAsPIRequested        | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:OpenUpdates              | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:SubmitForApproval        | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:Confirm                  | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:MarkAsShipped            | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:MarkAsDeliveryComplete   | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:Cancel                   | [purchaseUnitName='Corrugated'] |
      | POOwner_Corrugated         | PurchaseOrder:Delete                   | [purchaseUnitName='Corrugated'] |

      | POPaymentDecsionMaker      | PurchaseOrder:UpdateCompany            |                                 |

      | CannotReadSectionsSubRole  | PurchaseOrder:ReadAll                  |                                 |
      | CannotReadSectionsSubRole  | PurchaseOrder:Delete                   |                                 |
      | CannotReadSectionsSubRole  | PurchaseOrder:ExportPDF                |                                 |

      | POLogisticsOwner_Signmedia | PurchaseOrder:MarkAsArrived            | [purchaseUnitName='Signmedia']  |
      | POLogisticsOwner_Signmedia | PurchaseOrder:MarkAsCleared            | [purchaseUnitName='Signmedia']  |
      | POLogisticsOwner_Signmedia | PurchaseOrder:AddPL/DNQuantity         | [purchaseUnitName='Signmedia']  |

      | POApprover_Signmedia       | PurchaseOrder:Approve                  | [purchaseUnitName='Signmedia']  |
      | POApprover_Signmedia       | PurchaseOrder:Reject                   | [purchaseUnitName='Signmedia']  |
      | POApprover_Signmedia       | PurchaseOrder:ReplaceApprover          | [purchaseUnitName='Signmedia']  |

      | POApprover_Flexo           | PurchaseOrder:Approve                  | [purchaseUnitName='Flexo']      |
      | POApprover_Flexo           | PurchaseOrder:Reject                   | [purchaseUnitName='Flexo']      |
      | POApprover_Flexo           | PurchaseOrder:ReplaceApprover          | [purchaseUnitName='Flexo']      |

      | POApprover_Corrugated      | PurchaseOrder:Approve                  | [purchaseUnitName='Corrugated'] |
      | POApprover_Corrugated      | PurchaseOrder:Reject                   | [purchaseUnitName='Corrugated'] |
      | POApprover_Corrugated      | PurchaseOrder:ReplaceApprover          | [purchaseUnitName='Corrugated'] |

      | POViewerLimited_Signmedia  | PurchaseOrder:ReadAll                  | [purchaseUnitName='Signmedia']  |
      | POViewerLimited_Signmedia  | PurchaseOrder:ReadItemsWithoutPrices   | [purchaseUnitName='Signmedia']  |
      | POViewerLimited_Signmedia  | PurchaseOrder:ExportPDFWithoutPrices   | [purchaseUnitName='Signmedia']  |
      | POViewerLimited_Signmedia  | PurchaseOrder:ReadConsigneeData        | [purchaseUnitName='Signmedia']  |
      | POViewerLimited_Signmedia  | PurchaseOrder:ReadVendor               | [purchaseUnitName='Signmedia']  |

      | SuperUserSubRole           | *:*                                    |                                 |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State                                             | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000025 | IMPORT_PO  | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO  | Inactive,Cancelled                                | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Active,Arrived,NotReceived                        | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Active,InOperation,Shipped,NotReceived            | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000009 | IMPORT_PO  | Active,InOperation,FinishedProduction,NotReceived | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Active,InOperation,Confirmed,NotReceived          | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000007 | IMPORT_PO  | Active,InOperation,WaitingApproval,NotReceived    | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000006 | IMPORT_PO  | Active,InOperation,OpenForUpdates,NotReceived     | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000005 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | Active,DeliveryComplete,Received                  | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000001 | IMPORT_PO  | Active,Cleared,NotReceived                        | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

      | 2018000027 | LOCAL_PO   | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000020 | LOCAL_PO   | Inactive,Cancelled                                | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO   | Active,DeliveryComplete,Received                  | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000018 | LOCAL_PO   | Active,InOperation,Shipped,NotReceived            | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000017 | LOCAL_PO   | Active,InOperation,Confirmed,NotReceived          | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000016 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000015 | LOCAL_PO   | Active,InOperation,WaitingApproval,NotReceived    | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000014 | LOCAL_PO   | Active,InOperation,OpenForUpdates,NotReceived     | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000013 | LOCAL_PO   | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

      | 2020000006 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Confirmed                                         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber                         |
      | 2018000025 | 0001 - Flexo     | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000021 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000012 | 0002 - Signmedia | 0002 - DigiPro   |                                           |
      | 2018000011 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000010 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000009 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000008 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000007 | 0002 - Signmedia |                  |                                           |
      | 2018000005 | 0002 - Signmedia | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000006 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000002 | 0002 - Signmedia | 0001 - AL Madina | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000001 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |

      | 2018000027 | 0001 - Flexo     | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000020 | 0002 - Signmedia | 0002 - DigiPro   |                                           |
      | 2018000019 | 0002 - Signmedia | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000018 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000017 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000016 | 0002 - Signmedia | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000015 | 0002 - Signmedia | 0002 - DigiPro   |                                           |
      | 2018000014 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000013 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |

      | 2020000006 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank           |
      | 2020000005 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank           |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank           |

  # EBS-6949
  # EBS-8346
  Scenario Outline: (01) Read PO Object Screen Allowed Actions by an authorized user - All PO States and Types (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of PurchaseOrder with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User                              | Code       | Type       | State                          | AllowedActions                                                                                                                                                                                                                                                                 | NoOfActions |
      | Ashraf.Fathi                      | 2018000025 | IMPORT_PO  | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |
      | Ashraf.Fathi                      | 2018000027 | LOCAL_PO   | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |
      | Ashraf.Fathi                      | 2020000004 | SERVICE_PO | Draft                          | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms                                                                                                                                                                                                            | 5           |

      | TestUser9                         | 2018000021 | IMPORT_PO  | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,UpdateRequiredDocuments,MarkAsPIRequested,Delete                          | 16          |
      | TestUser9                         | 2018000005 | IMPORT_PO  | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,Confirm,Cancel                                   | 16          |
      | TestUser9                         | 2018000008 | IMPORT_PO  | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsProductionFinished,Cancel                  | 16          |
      | TestUser9                         | 2018000009 | IMPORT_PO  | FinishedProduction,NotReceived | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsShipped,Cancel                             | 16          |
      | TestUser9                         | 2018000010 | IMPORT_PO  | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,Cancel                                                       | 14          |
      | TestUser9                         | 2018000011 | IMPORT_PO  | Arrived,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,Cancel                                                       | 14          |
      | TestUser9                         | 2018000001 | IMPORT_PO  | Cleared,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,MarkAsDeliveryComplete                                       | 14          |
      | TestUser9                         | 2018000002 | IMPORT_PO  | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | TestUser9                         | 2018000012 | IMPORT_PO  | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | TestUser9                         | 2018000013 | LOCAL_PO   | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,UpdateRequiredDocuments,MarkAsPIRequested,Delete                          | 16          |
      | TestUser9                         | 2018000016 | LOCAL_PO   | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,Confirm,Cancel                                   | 16          |
      | TestUser9                         | 2018000017 | LOCAL_PO   | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsShipped,Cancel                             | 16          |
      | TestUser9                         | 2018000018 | LOCAL_PO   | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,MarkAsDeliveryComplete,Cancel                                | 15          |
      | TestUser9                         | 2018000019 | LOCAL_PO   | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | TestUser9                         | 2018000020 | LOCAL_PO   | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | TestUser9                         | 2020000004 | SERVICE_PO | Draft                          | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,UpdateCompany,UpdatePaymentTerms,UpdateItem,Confirm,Delete                                                                                                                                                 | 10          |
      | TestUser9                         | 2020000005 | SERVICE_PO | Confirmed                      | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms                                                                                                                                                                                                            | 5           |

      | TestUser10                        | 2018000021 | IMPORT_PO  | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,UpdateRequiredDocuments,MarkAsPIRequested,Delete                          | 16          |
      | TestUser10                        | 2018000006 | IMPORT_PO  | OpenUpdates,NotReceived        | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,UpdateRequiredDocuments,SubmitForApproval,Cancel                          | 16          |
      | TestUser10                        | 2018000007 | IMPORT_PO  | WaitingApproval,NotReceived    | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,OpenUpdates,Approve,Reject,ReplaceApprover,Cancel                                                                                     | 14          |
      | TestUser10                        | 2018000005 | IMPORT_PO  | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,Confirm,Cancel                                   | 16          |
      | TestUser10                        | 2018000008 | IMPORT_PO  | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsProductionFinished,Cancel                  | 16          |
      | TestUser10                        | 2018000009 | IMPORT_PO  | FinishedProduction,NotReceived | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsShipped,Cancel                             | 16          |
      | TestUser10                        | 2018000010 | IMPORT_PO  | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,Cancel                                                       | 14          |
      | TestUser10                        | 2018000011 | IMPORT_PO  | Arrived,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,Cancel                                                       | 14          |
      | TestUser10                        | 2018000001 | IMPORT_PO  | Cleared,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,MarkAsDeliveryComplete                                       | 14          |
      | TestUser10                        | 2018000002 | IMPORT_PO  | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | TestUser10                        | 2018000012 | IMPORT_PO  | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | TestUser10                        | 2018000013 | LOCAL_PO   | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,UpdateItem,MarkAsPIRequested,Delete                          | 16          |
      | TestUser10                        | 2018000014 | LOCAL_PO   | OpenUpdates,NotReceived        | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,UpdateItem,SubmitForApproval,Cancel                          | 16          |
      | TestUser10                        | 2018000016 | LOCAL_PO   | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,Confirm,Cancel                                   | 16          |
      | TestUser10                        | 2018000017 | LOCAL_PO   | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,OpenUpdates,MarkAsShipped,Cancel                             | 16          |
      | TestUser10                        | 2018000018 | LOCAL_PO   | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,MarkAsDeliveryComplete,Cancel                                | 15          |
      | TestUser10                        | 2018000015 | LOCAL_PO   | WaitingApproval,NotReceived    | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,Approve,Reject,ReplaceApprover,OpenUpdates,Cancel                                                                                     | 14          |
      | TestUser10                        | 2018000019 | LOCAL_PO   | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | TestUser10                        | 2018000020 | LOCAL_PO   | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | TestUser10                        | 2020000004 | SERVICE_PO | Draft                          | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,UpdateCompany,UpdatePaymentTerms,UpdateItem,Confirm,Delete                                                                                                                                                 | 10          |
      | TestUser10                        | 2020000005 | SERVICE_PO | Confirmed                      | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms                                                                                                                                                                                                            | 5           |

      | RejectApproveAddPL/DNQuantityUser | 2018000021 | IMPORT_PO  | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,AddPL/DNQuantity,UpdateRequiredDocuments,MarkAsPIRequested,Delete         | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000006 | IMPORT_PO  | OpenUpdates,NotReceived        | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateItem,AddPL/DNQuantity,UpdateRequiredDocuments,SubmitForApproval,Cancel         | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000007 | IMPORT_PO  | WaitingApproval,NotReceived    | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,AddPL/DNQuantity,OpenUpdates,Approve,Reject,ReplaceApprover,Cancel                                                                    | 15          |
      | RejectApproveAddPL/DNQuantityUser | 2018000005 | IMPORT_PO  | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,OpenUpdates,Confirm,Cancel                  | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000008 | IMPORT_PO  | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,OpenUpdates,MarkAsProductionFinished,Cancel | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000009 | IMPORT_PO  | FinishedProduction,NotReceived | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,OpenUpdates,MarkAsShipped,Cancel            | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000010 | IMPORT_PO  | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,MarkAsArrived,Cancel                        | 16          |
      | RejectApproveAddPL/DNQuantityUser | 2018000011 | IMPORT_PO  | Arrived,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,MarkAsCleared,Cancel                        | 16          |
      | RejectApproveAddPL/DNQuantityUser | 2018000001 | IMPORT_PO  | Cleared,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,MarkAsDeliveryComplete                      | 15          |
      | RejectApproveAddPL/DNQuantityUser | 2018000002 | IMPORT_PO  | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | RejectApproveAddPL/DNQuantityUser | 2018000012 | IMPORT_PO  | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | RejectApproveAddPL/DNQuantityUser | 2018000013 | LOCAL_PO   | Draft                          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,UpdateItem,AddPL/DNQuantity,MarkAsPIRequested,Delete         | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000014 | LOCAL_PO   | OpenUpdates,NotReceived        | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,UpdateItem,AddPL/DNQuantity,SubmitForApproval,Cancel         | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000015 | LOCAL_PO   | WaitingApproval,NotReceived    | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,AddPL/DNQuantity,OpenUpdates,Approve,Reject,ReplaceApprover,Cancel                                                                    | 15          |
      | RejectApproveAddPL/DNQuantityUser | 2018000016 | LOCAL_PO   | Approved,NotReceived           | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,OpenUpdates,Confirm,Cancel                  | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000017 | LOCAL_PO   | Confirmed,NotReceived          | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,OpenUpdates,MarkAsShipped,Cancel            | 17          |
      | RejectApproveAddPL/DNQuantityUser | 2018000018 | LOCAL_PO   | Shipped,NotReceived            | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany,UpdateConsignee,UpdatePaymentTerms,UpdateRequiredDocuments,AddPL/DNQuantity,MarkAsDeliveryComplete,Cancel               | 16          |
      | RejectApproveAddPL/DNQuantityUser | 2018000019 | LOCAL_PO   | DeliveryComplete,Received      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF,UpdateCompany                                                                                                                         | 10          |
      | RejectApproveAddPL/DNQuantityUser | 2018000020 | LOCAL_PO   | Cancelled                      | ReadAll,ReadCompany,ReadConsigneeData,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,ReadRequiredDocuments,ReadApprovalCycles,ExportPDF                                                                                                                                       | 9           |

      | RejectApproveAddPL/DNQuantityUser | 2020000004 | SERVICE_PO | Draft                          | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms,UpdateCompany,UpdatePaymentTerms,UpdateItem,Confirm,Delete                                                                                                                                                 | 10          |
      | RejectApproveAddPL/DNQuantityUser | 2020000005 | SERVICE_PO | Confirmed                      | ReadAll,ReadCompany,ReadVendor,ReadItemsWithPrices,ReadPaymentTerms                                                                                                                                                                                                            | 5           |

      | CannotReadSections                | 2018000021 | IMPORT_PO  | Draft                          | ReadAll,Delete,ExportPDF                                                                                                                                                                                                                                                       | 3           |
      | CannotReadSections                | 2018000006 | IMPORT_PO  | OpenUpdates,NotReceived        | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000007 | IMPORT_PO  | WaitingApproval,NotReceived    | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000005 | IMPORT_PO  | Approved,NotReceived           | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000008 | IMPORT_PO  | Confirmed,NotReceived          | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000009 | IMPORT_PO  | FinishedProduction,NotReceived | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000010 | IMPORT_PO  | Shipped,NotReceived            | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000011 | IMPORT_PO  | Arrived,NotReceived            | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000001 | IMPORT_PO  | Cleared,NotReceived            | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000002 | IMPORT_PO  | DeliveryComplete,Received      | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000012 | IMPORT_PO  | Cancelled                      | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |

      | CannotReadSections                | 2018000013 | LOCAL_PO   | Draft                          | ReadAll,Delete,ExportPDF                                                                                                                                                                                                                                                       | 3           |
      | CannotReadSections                | 2018000014 | LOCAL_PO   | OpenUpdates,NotReceived        | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000015 | LOCAL_PO   | WaitingApproval,NotReceived    | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000016 | LOCAL_PO   | Approved,NotReceived           | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000017 | LOCAL_PO   | Confirmed,NotReceived          | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000018 | LOCAL_PO   | Shipped,NotReceived            | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000019 | LOCAL_PO   | DeliveryComplete,Received      | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |
      | CannotReadSections                | 2018000020 | LOCAL_PO   | Cancelled                      | ReadAll,ExportPDF                                                                                                                                                                                                                                                              | 2           |

      | CannotReadSections                | 2020000004 | SERVICE_PO | Draft                          | ReadAll,Delete                                                                                                                                                                                                                                                                 | 2           |
      | CannotReadSections                | 2020000005 | SERVICE_PO | Confirmed                      | ReadAll                                                                                                                                                                                                                                                                        | 1           |

      | Mahmoud.Abdelaziz                 | 2018000021 | IMPORT_PO  | Draft                          | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000006 | IMPORT_PO  | OpenUpdates,NotReceived        | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000007 | IMPORT_PO  | WaitingApproval,NotReceived    | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000005 | IMPORT_PO  | Approved,NotReceived           | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000008 | IMPORT_PO  | Confirmed,NotReceived          | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000009 | IMPORT_PO  | FinishedProduction,NotReceived | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000010 | IMPORT_PO  | Shipped,NotReceived            | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000011 | IMPORT_PO  | Arrived,NotReceived            | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000001 | IMPORT_PO  | Cleared,NotReceived            | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000002 | IMPORT_PO  | DeliveryComplete,Received      | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000012 | IMPORT_PO  | Cancelled                      | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |

      | Mahmoud.Abdelaziz                 | 2018000013 | LOCAL_PO   | Draft                          | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000014 | LOCAL_PO   | OpenUpdates,NotReceived        | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000015 | LOCAL_PO   | WaitingApproval,NotReceived    | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000016 | LOCAL_PO   | Approved,NotReceived           | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000017 | LOCAL_PO   | Confirmed,NotReceived          | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000018 | LOCAL_PO   | Shipped,NotReceived            | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000019 | LOCAL_PO   | DeliveryComplete,Received      | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |
      | Mahmoud.Abdelaziz                 | 2018000020 | LOCAL_PO   | Cancelled                      | ReadAll,ReadConsigneeData,ReadVendor,ReadItemsWithoutPrices                                                                                                                                                                                                                    | 4           |

      | Mahmoud.Abdelaziz                 | 2020000004 | SERVICE_PO | Draft                          | ReadAll,ReadVendor                                                                                                                                                                                                                                                             | 2           |
      | Mahmoud.Abdelaziz                 | 2020000005 | SERVICE_PO | Confirmed                      | ReadAll,ReadVendor                                                                                                                                                                                                                                                             | 2           |

  # EBS-6949
  Scenario Outline: (02) Read PO Object Screen Allowed Actions by unauthorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of PurchaseOrder with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User       | Code       |
      | Afaf       | 2018000021 |
      | Afaf       | 2018000013 |
      | Afaf       | 2020000004 |
      | Amr.Khalil | 2018000021 |
      | Amr.Khalil | 2018000013 |
      | Amr.Khalil | 2020000004 |

  # EBS-6949
  Scenario Outline: (03) Read PO Object Screen Allowed Actions for PurchaseOrder that not exist
    Given user is logged in as "TestUser10"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<Code>" successfully
    When "TestUser10" requests to read actions of PurchaseOrder with code "<Code>"
    Then an error notification is sent to "TestUser10" with the following message "Gen-msg-01"
    Examples:
      | Code       |
      | 2018000021 |
      | 2018000013 |
      | 2020000004 |