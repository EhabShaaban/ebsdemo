Feature: Upload Attachments in PO by an authorized user - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                      | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UploadAttachments | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UploadAttachments | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UploadAttachments | [purchaseUnitName='Corrugated'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |

    And the following Attachments section exist in PurchaseOrder with code "2018000021":
      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
      | 0005             | myfile.pdf | Amr.Khalil  | 21-Jan-2017 11:00 am |
      | 0004             | myfile.doc | Gehan.Ahmed | 21-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000005":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_05.pdf | Amr.Khalil  | 05-Feb-2005 11:00 am |
      | 0002             | myfile_05.doc | Gehan.Ahmed | 05-Feb-2005 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000006":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0003             | myfile_06.pdf | Amr.Khalil  | 06-Feb-2006 11:00 am |
      | 0004             | myfile_06.doc | Gehan.Ahmed | 06-Feb-2006 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000007":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0005             | myfile_07.pdf | Amr.Khalil  | 07-Feb-2007 11:00 am |
      | 0006             | myfile_07.doc | Gehan.Ahmed | 07-Feb-2007 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000008":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_08.pdf | Amr.Khalil  | 08-Feb-2008 11:00 am |
      | 0002             | myfile_08.doc | Gehan.Ahmed | 08-Feb-2008 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000009":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0003             | myfile_09.pdf | Amr.Khalil  | 09-Feb-2009 11:00 am |
      | 0004             | myfile_09.doc | Gehan.Ahmed | 09-Feb-2009 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000010":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0005             | myfile_10.pdf | Amr.Khalil  | 10-Feb-2010 11:00 am |
      | 0006             | myfile_10.doc | Gehan.Ahmed | 10-Feb-2010 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000011":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000001":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000002":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000012":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000025":
      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
      | 0005             | myfile.png | Amr.Khalil  | 25-Jan-2017 11:00 am |
      | 0004             | myfile.jpg | Gehan.Ahmed | 25-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000026":
      | DocumentTypeCode | Attachment  | UploadedBy        | UploadDateTime       |
      | 0005             | myfile.jpeg | Mahmoud.Abdelaziz | 26-Jan-2017 11:00 am |
      | 0004             | myfile.docx | Amr.Khalil        | 26-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000013":
      | DocumentTypeCode | Attachment | UploadedBy        | UploadDateTime       |
      | 0005             | myfile.odt | Mahmoud.Abdelaziz | 13-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Gehan.Ahmed       | 13-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000014":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000015":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000016":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000017":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000018":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000019":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000020":
      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000027":
      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
      | 0005             | myfile.pdf | Ashraf.Fathi | 27-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Amr.Khalil   | 27-Jan-2017 11:00 am |
    And the following Attachments section exist in PurchaseOrder with code "2018000028":
      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
      | 0005             | myfile.pdf | Ashraf.Fathi | 28-Jan-2017 11:00 am |
      | 0004             | myfile.pdf | Gehan.Ahmed  | 28-Jan-2017 11:00 am |

    And the following DocumentType exist:
      | DocumentTypeCode | DocumentTypeName                                    |
      | 0001             | Performa Invoice                                    |
      | 0002             | Commercial Invoice                                  |
      | 0003             | Commercial Invoice Legalized by Chamber of Commerce |
      | 0004             | Bill of Lading (BL)                                 |
      | 0005             | Packing List for the Whole Order                    |
      | 0006             | Packing List per Container                          |

  # User with one role in all PO states

  Scenario Outline: (01) Upload Attachments with User has one role and PO is Import & Draft
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000021" at "08-Nov-2018 01:00 pm" with following values:
      | DocumentTypeCode | Attachment | FileSize       |
      | 0003             | <File>     | <FileSizeinMB> |
    Then attachment section in PurchaseOrder with Code "2018000021" has the following attachements:
      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
      | 0005             | myfile.pdf | Amr.Khalil  | 21-Jan-2017 11:00 am |
      | 0004             | myfile.doc | Gehan.Ahmed | 21-Jan-2017 11:00 am |
      | 0003             | <File>     | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
    Examples:
      | FileSizeinMB | File       |
      | 10000        | Airway.pdf |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (02) User with one role and PO is Import & Approved
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000005" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000005" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0001             | myfile_05.pdf | Amr.Khalil  | 05-Feb-2005 11:00 am |
#      | 0002             | myfile_05.doc | Gehan.Ahmed | 05-Feb-2005 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (03) User with one role and PO is Import & OpenForUpdates
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000006" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000006" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0003             | myfile_06.pdf | Amr.Khalil  | 06-Feb-2006 11:00 am |
#      | 0004             | myfile_06.doc | Gehan.Ahmed | 06-Feb-2006 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (04) User with one role and PO is Import & WaitingApproval
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000007" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000007" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0005             | myfile_07.pdf | Amr.Khalil  | 07-Feb-2007 11:00 am |
#      | 0006             | myfile_07.doc | Gehan.Ahmed | 07-Feb-2007 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (05) User with one role and PO is Import & Confirmed
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000008" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000008" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0001             | myfile_08.pdf | Amr.Khalil  | 08-Feb-2008 11:00 am |
#      | 0002             | myfile_08.doc | Gehan.Ahmed | 08-Feb-2008 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (06) User with one role and PO is Import & FinsihedProduction
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000009" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000009" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0003             | myfile_09.pdf | Amr.Khalil  | 09-Feb-2009 11:00 am |
#      | 0004             | myfile_09.doc | Gehan.Ahmed | 09-Feb-2009 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (07) User with one role and PO is Import & Shipped
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000010" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000010" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0005             | myfile_10.pdf | Amr.Khalil  | 10-Feb-2010 11:00 am |
#      | 0006             | myfile_10.doc | Gehan.Ahmed | 10-Feb-2010 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (08) User with one role and PO is Import & Arrived
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000011" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000011" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0001             | myfile_11.pdf | Amr.Khalil  | 11-Feb-2011 11:00 am |
#      | 0002             | myfile_11.doc | Gehan.Ahmed | 11-Feb-2011 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (09) User with one role and PO is Import & Cleared
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000001" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000001" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0003             | myfile_01.pdf | Amr.Khalil  | 01-Feb-2001 11:00 am |
#      | 0004             | myfile_01.doc | Gehan.Ahmed | 01-Feb-2001 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (10) User with one role and PO is Import & DeliveryComplete
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000002" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000002" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0005             | myfile_02.pdf | Amr.Khalil  | 02-Feb-2002 11:00 am |
#      | 0006             | myfile_02.doc | Gehan.Ahmed | 02-Feb-2002 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (11) User with one role and PO is Import & Cancelled
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000012" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000012" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy  | UploadDateTime       |
#      | 0001             | myfile_12.pdf | Amr.Khalil  | 12-Feb-2012 11:00 am |
#      | 0002             | myfile_12.doc | Gehan.Ahmed | 12-Feb-2012 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (12) User with one role and PO is Local & Draft
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000013" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000013" has the following attachements:
#      | DocumentTypeCode | Attachment | UploadedBy        | UploadDateTime       |
#      | 0005             | myfile.odt | Mahmoud.Abdelaziz | 13-Jan-2017 11:00 am |
#      | 0004             | myfile.pdf | Gehan.Ahmed       | 13-Jan-2017 11:00 am |
#      | 0003             | <File>     | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (13) User with one role and PO is Local & OpenForUpdates
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000014" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000014" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0003             | myfile_14.odt | Mahmoud.Abdelaziz | 14-Mar-2014 11:00 am |
#      | 0004             | myfile_14.pdf | Gehan.Ahmed       | 14-Mar-2014 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (14) User with one role and PO is Local & WaitingApproval
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000015" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000015" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0005             | myfile_15.odt | Mahmoud.Abdelaziz | 15-Mar-2015 11:00 am |
#      | 0006             | myfile_15.pdf | Gehan.Ahmed       | 15-Mar-2015 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (15) User with one role and PO is Local & Approved
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000016" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000016" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0001             | myfile_16.odt | Mahmoud.Abdelaziz | 16-Mar-2016 11:00 am |
#      | 0002             | myfile_16.pdf | Gehan.Ahmed       | 16-Mar-2016 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (16) User with one role and PO is Local & Confirmed
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000017" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000017" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0003             | myfile_17.odt | Mahmoud.Abdelaziz | 17-Mar-2017 11:00 am |
#      | 0004             | myfile_17.pdf | Gehan.Ahmed       | 17-Mar-2017 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (17) User with one role and PO is Local & Shipped
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000018" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000018" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0005             | myfile_18.odt | Mahmoud.Abdelaziz | 18-Mar-2018 11:00 am |
#      | 0006             | myfile_18.pdf | Gehan.Ahmed       | 18-Mar-2018 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (18) User with one role and PO is Local & DeliveryComplete
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000019" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000019" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0001             | myfile_19.odt | Mahmoud.Abdelaziz | 19-Mar-2019 11:00 am |
#      | 0002             | myfile_19.pdf | Gehan.Ahmed       | 19-Mar-2019 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (19) User with one role and PO is Local & Cancelled
#    Given user is logged in as "Gehan.Ahmed"
#    And "Gehan.Ahmed" requests to upload attachment to PurchaseOrder with code "2018000020" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000020" has the following attachements:
#      | DocumentTypeCode | Attachment    | UploadedBy        | UploadDateTime       |
#      | 0005             | myfile_20.odt | Mahmoud.Abdelaziz | 20-Mar-2020 11:00 am |
#      | 0006             | myfile_20.pdf | Gehan.Ahmed       | 20-Mar-2020 11:00 am |
#      | 0003             | <File>        | Gehan.Ahmed       | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#
#  # User with two roles
#  Scenario Outline: (20) User with two roles and PO is Import & Draft & Flexo
#    Given user is logged in as "Amr.Khalil"
#    And "Amr.Khalil" requests to upload attachment to PurchaseOrder with code "2018000025" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000025" has the following attachements:
#      | DocumentTypeCode | Attachment | UploadedBy  | UploadDateTime       |
#      | 0005             | myfile.png | Amr.Khalil  | 25-Jan-2017 11:00 am |
#      | 0004             | myfile.jpg | Gehan.Ahmed | 25-Jan-2017 11:00 am |
#      | 0003             | <File>     | Amr.Khalil  | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (21) User with two roles and PO is Import & Draft & Corrugated
#    Given user is logged in as "Amr.Khalil"
#    And "Amr.Khalil" requests to upload attachment to PurchaseOrder with code "2018000026" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000026" has the following attachements:
#      | DocumentTypeCode | Attachment  | UploadedBy        | UploadDateTime       |
#      | 0005             | myfile.jpeg | Mahmoud.Abdelaziz | 26-Jan-2017 11:00 am |
#      | 0004             | myfile.docx | Amr.Khalil        | 26-Jan-2017 11:00 am |
#      | 0003             | <File>      | Amr.Khalil        | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (22) User with two roles and PO is Local & Draft & Flexo
#    Given user is logged in as "Amr.Khalil"
#    And "Amr.Khalil" requests to upload attachment to PurchaseOrder with code "2018000027" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000027" has the following attachements:
#      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
#      | 0005             | myfile.pdf | Ashraf.Fathi | 27-Jan-2017 11:00 am |
#      | 0004             | myfile.pdf | Amr.Khalil   | 27-Jan-2017 11:00 am |
#      | 0003             | <File>     | Amr.Khalil   | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
#
#  Scenario Outline: (23) User with two roles and PO is Local & Draft & Corrugated
#    Given user is logged in as "Amr.Khalil"
#    And "Amr.Khalil" requests to upload attachment to PurchaseOrder with code "2018000028" at "08-Nov-2018 01:00 pm" with following values:
#      | DocumentTypeCode | Attachment | File Size      |
#      | 0003             | <File>     | <FileSizeinMB> |
#    Then attachment section in PurchaseOrder with Code "2018000028" has the following attachements:
#      | DocumentTypeCode | Attachment | UploadedBy   | UploadDateTime       |
#      | 0005             | myfile.pdf | Ashraf.Fathi | 28-Jan-2017 11:00 am |
#      | 0004             | myfile.pdf | Gehan.Ahmed  | 28-Jan-2017 11:00 am |
#      | 0003             | <File>     | Amr.Khalil   | 08-Nov-2018 01:00 pm |
#    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-23"
#    Examples:
#      | FileSizeinMB | File        |
#      | 10000        | Airway.pdf  |
#      | 9999         | Airway.pdf  |
#      | 1            | Airway.pdf  |
#      | 2            | Airway.pdf  |
#      | 10000        | Airway.docx |
#      | 9999         | Airway.docx |
#      | 1            | Airway.docx |
#      | 2            | Airway.docx |
#      | 10000        | Airway.doc  |
#      | 9999         | Airway.doc  |
#      | 1            | Airway.doc  |
#      | 2            | Airway.doc  |
#      | 10000        | Airway.odt  |
#      | 9999         | Airway.odt  |
#      | 1            | Airway.odt  |
#      | 2            | Airway.odt  |
#      | 10000        | Airway.png  |
#      | 9999         | Airway.png  |
#      | 1            | Airway.png  |
#      | 2            | Airway.png  |
#      | 10000        | Airway.jpg  |
#      | 9999         | Airway.jpg  |
#      | 1            | Airway.jpg  |
#      | 2            | Airway.jpg  |
#      | 10000        | Airway.jpeg |
#      | 9999         | Airway.jpeg |
#      | 1            | Airway.jpeg |
#      | 2            | Airway.jpeg |
