Feature: Save RequiredDocuments section - Validations

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia  |
      | PurchasingResponsible_Signmedia | DocumentTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                            | Condition                      |
      | POOwner_Signmedia  | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Signmedia'] |
      | DocumentTypeViewer | DocumentType:ReadAll                  |                                |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000024 | Draft            | 0003           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
    And the PurchaseOrder with code "2018000021" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000006" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000005" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000008" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000009" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000010" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000011" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000001" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000013" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000014" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000016" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000017" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000018" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the following DocumentTypes exist:
      | Code | Name                             |
      | 0001 | Performa Invoice                 |
      | 0002 | Commercial Invoice               |
      | 0004 | Bill of Lading (BL)              |
      | 0005 | Packing List for the Whole Order |
    And edit session is "30" minutes

  ######## Save while entering data for disabled Fields, where PO state allows editing #########################

  # There is no editable state that has disabled fields in the Required documents sections

  ######## Save without mandatory Fields (Abuse Cases) ########################################

  Scenario Outline:(01) Save RequiredDocument section with missing mandatory fields in editable states (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType      | Copies      |
      | 0002              | 2           |
      | <DocumentType_R2> | <Copies_R2> |
    Then the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | State              | DocumentType_R2 | Copies_R2 |
      | 2018000021 | Draft              | 0001            |           |
      | 2018000013 | Draft              | 0001            |           |
      | 2018000021 | Draft              |                 | 11        |
      | 2018000013 | Draft              |                 | 11        |
      | 2018000006 | OpenForUpdates     | 0001            |           |
      | 2018000014 | OpenForUpdates     | 0001            |           |
      | 2018000006 | OpenForUpdates     |                 | 11        |
      | 2018000014 | OpenForUpdates     |                 | 11        |
      | 2018000005 | Approved           | 0001            |           |
      | 2018000016 | Approved           | 0001            |           |
      | 2018000005 | Approved           |                 | 11        |
      | 2018000016 | Approved           |                 | 11        |
      | 2018000008 | Confirmed          | 0001            |           |
      | 2018000017 | Confirmed          | 0001            |           |
      | 2018000008 | Confirmed          |                 | 11        |
      | 2018000017 | Confirmed          |                 | 11        |
      | 2018000009 | FinishedProduction | 0001            |           |
      | 2018000018 | FinishedProduction | 0001            |           |
      | 2018000009 | FinishedProduction |                 | 11        |
      | 2018000018 | FinishedProduction |                 | 11        |
      | 2018000010 | Shipped            | 0001            |           |
      | 2018000010 | Shipped            |                 | 11        |
      | 2018000011 | Arrived            | 0001            |           |
      | 2018000011 | Arrived            |                 | 11        |
      | 2018000001 | Cleared            | 0001            |           |
      | 2018000001 | Cleared            |                 | 11        |

  Scenario Outline:(02) Save RequiredDocument section with empty list where at least one record is required in editable states (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with empty values
    Then the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | State              |
      | 2018000006 | OpenForUpdates     |
      | 2018000014 | OpenForUpdates     |
      | 2018000005 | Approved           |
      | 2018000016 | Approved           |
      | 2018000008 | Confirmed          |
      | 2018000017 | Confirmed          |
      | 2018000009 | FinishedProduction |
      | 2018000018 | FinishedProduction |
      | 2018000010 | Shipped            |
      | 2018000011 | Arrived            |
      | 2018000001 | Cleared            |

  ######## Save with incorrect data, where PO state allows editing (Validation) #############################################

  Scenario Outline: (03) Save RequiredDocument section with Incorrect Data: Document Type doesn't exist >> PO-BR-21 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies |
      | 9999         | 1      |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to DocumentType field "PO-msg-21" and sent to "Gehan.Ahmed"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  ######## Save with malicious input (Abuse Cases) ############################################

  Scenario Outline: (04) Save RequiredDocument section with malicious input: Document Type is duplicated >> PO-BR-22 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies |
      | 0005         | 1      |
      | 0001         | 1      |
      | 0005         | 1      |
    Then the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (05) Save RequiredDocument section malicious data: DocumentType is string >> PO-BR-21 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies |
      | Ay7aga       | 1      |
    Then the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (06) Save RequiredDocument section with malicious data: Copies is incorrect >> Gen-BR-11 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies        |
      | 0001         | <CopiesValue> |
    Then the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | CopiesValue | POCode     |
      | 0           | 2018000021 |
      | -1          | 2018000021 |
      | 2.4         | 2018000021 |
      | -2.4        | 2018000021 |
      | 10000       | 2018000021 |
      | Ay 7aga     | 2018000021 |
      | 0           | 2018000013 |
      | -1          | 2018000013 |
      | 2.4         | 2018000013 |
      | -2.4        | 2018000013 |
      | 10000       | 2018000013 |
      | Ay 7aga     | 2018000013 |

  ######## Save after edit session expires ####################################################################

  Scenario Outline: (07) Save RequiredDocument section after edit session expired: Data is correct or incorrect (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:41 AM" with the following values:
      | DocumentType             | Copies |
      | <RequiredDocumentsValue> | 1      |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | RequiredDocumentsValue |
      | 2018000021 | 9999                   |
      | 2018000013 | 9999                   |
      | 2018000021 | 0001                   |
      | 2018000013 | 0001                   |

  @ResetData
  Scenario Outline:(08) Save RequiredDocument section after edit session expired & PO is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 9:42 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:45 AM" with the following values:
      | DocumentType | Copies |
      | 0005         | 6      |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |