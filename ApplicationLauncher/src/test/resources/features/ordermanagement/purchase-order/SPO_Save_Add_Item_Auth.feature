# Author Dev: Order Team (15-Feb-2021)
# Author Quality: Shirin Mahmoud

Feature: Service Purchase Order - Save New Item Authorization

  Background:
    Given the following users and roles exist:
      | Name                         | Role                                                       |
      | Gehan.Ahmed                  | PurchasingResponsible_Signmedia                            |
      | Gehan.Ahmed.NoItemsNoItemUoM | PurchasingResponsible_Signmedia_CannotReadItemsItemUoMRole |
    And the following roles and sub-roles exist:
      | Role                                                       | Subrole             |
      | PurchasingResponsible_Signmedia                            | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia                            | ItemOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemsItemUoMRole | POOwner_Signmedia   |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadUoMData         | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                         | Permission       |
      | Gehan.Ahmed.NoItemsNoItemUoM | Item:ReadAll     |
      | Gehan.Ahmed.NoItemsNoItemUoM | Item:ReadUoMData |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2019 9:02 AM | Amr Khalil    |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000010 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2018000010 | 000001 - Siegwerk |             |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000005 | 000001 - Zhejiang | 2018000010  |
    And the following Items exist with the below details:
      | Item               | Type    | UOM       | PurchaseUnit     |
      | 000057 - Bank Fees | SERVICE | 0019 - M2 | 0002 - Signmedia |
    And the following UOMs exist for Item "000057 - Bank Fees":
      | UOM                 |
      | 0035 - Roll 1.27x50 |

  # EBS-7403
  Scenario Outline: (01) Save New Service PurchaseOrder Item, by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves the following new item to Items section of Service PurchaseOrder with Code "<Code>" at "01-Jan-2019 11:10 AM" with the following values:
      | Item               | OrderUnit           | Quantity | Price  |
      | 000057 - Bank Fees | 0035 - Roll 1.27x50 | 100.000  | 10.000 |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                         | Code       |
      | Gehan.Ahmed                  | 2020000005 |
      | Gehan.Ahmed.NoItemsNoItemUoM | 2020000004 |
