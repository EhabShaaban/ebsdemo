# Author: Nancy Shoukry 15-Apr-2019
# Reviewer:

Feature: Cancel Purchase Order

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia         |
      | PurchasingResponsible_Flexo      | POOwner_Flexo             |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated        |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia |
      | SuperUser                        | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission           | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:Cancel | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:Cancel | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:Cancel | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                  |                                 |
    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000035 | IMPORT_PO | Approved           | 0006           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000025 | IMPORT_PO | Draft              | 0001           |
      | 2018000026 | IMPORT_PO | Draft              | 0006           |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           |
      | 2018000030 | LOCAL_PO  | OpenForUpdates     | 0001           |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           |
      | 2018000018 | LOCAL_PO  | Shipped            | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete   | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled          | 0002           |
      | 2018000027 | LOCAL_PO  | Draft              | 0001           |

  # OpenForUpdates , WaitingApproval , approved , confirmed,shipped
  # Import only : FinishedProduction , Arrived
  @ResetData
  Scenario Outline: (01) Cancel PO by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to cancel the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then PurchaseOrder with Code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State     |
      | <User>        | 07-Jan-2019 09:30 AM | Cancelled |
    And a success notification is sent to "<User>" with the following message "PO-msg-43"
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000006 | OpenForUpdates     | 0002           | Gehan.Ahmed |
      | 2018000030 | OpenForUpdates     | 0001           | Amr.Khalil  |
      | 2018000007 | WaitingApproval    | 0002           | Gehan.Ahmed |
      | 2018000015 | WaitingApproval    | 0002           | Gehan.Ahmed |
      | 2018000035 | Approved           | 0006           | Amr.Khalil  |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000010 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped            | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived            | 0002           | Gehan.Ahmed |

  Scenario: (02) Cancel PO (Import) in Cleared state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel the PurchaseOrder with Code "2018000001" at "07-Jan-2019 9:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

  Scenario Outline: (03) Cancel PO (Import / Local) in DeliveryComplete  state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State            | PurchasingUnit |
      | 2018000002 | DeliveryComplete | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000012 | Cancelled        | 0002           |
      | 2018000020 | Cancelled        | 0002           |

  # EBS-1655
  Scenario Outline: (04) Cancel PO (Import/Local) that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-Jan-2019 9:20 AM"
    When "<User>" requests to cancel the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-14"
    Examples:
      | POSection | POCode     | POType    | User        |
      | Company   | 2018000006 | IMPORT_PO | Gehan.Ahmed |
      | Company   | 2018000030 | LOCAL_PO  | Amr.Khalil  |

  Scenario Outline: (05) Cancel PO (Import/Local) in Draft state (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to cancel the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit | User        |
      | 2018000021 | Draft | 0002           | Gehan.Ahmed |
      | 2018000027 | Draft | 0002           | Amr.Khalil  |

  Scenario: (06) Cancel PO that doens't exist (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel the PurchaseOrder with Code "9999999999" at "7-Jan-2019 9:30 AM"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario Outline: (07) Cancel PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to cancel the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000006 | Amr.Khalil        |
      | 2018000006 | Mahmoud.Abdelaziz |

