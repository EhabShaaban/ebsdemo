# Reviewer: Marina (28-Jan-2019 12:40 PM)
# Reviewer: Somaya (28-Jan-2019 05:30 PM)

Feature: View General Data section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | hr1         | SuperUser                        |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission            | Condition                       |
      | SuperUserSubRole    | *:*                   |                                 |
      | POViewer_Signmedia  | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following users have the following permissions without the following conditions:
      | User       | Permission            | Condition                      |
      | Amr.Khalil | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000013 | LOCAL_PO   | Draft | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000026 | IMPORT_PO  | Draft | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000027 | LOCAL_PO   | Draft | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000028 | LOCAL_PO   | Draft | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000004 | SERVICE_PO | Draft | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000027 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000028 | 0006 - Corrugated | 0001 - AL Madina |                                 |
      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

  #### Happy Path  ####
  #EBS-848 , #EBS-6993
  Scenario Outline: (01) View General Data section by an authorized user who has one/two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view General Data section of PurchaseOrder with code "<POCodeValue>"
    Then the following values of General Data section are displayed to "<User>":
      | Code          | State        | CreatedBy        | CreationDate        | Type        | DocumentOwner        |
      | <POCodeValue> | <StateValue> | <CreatedByValue> | <CreationDateValue> | <TypeValue> | <DocumentOwnerValue> |
    Examples:
      | User        | POCodeValue | StateValue | CreatedByValue | CreationDateValue  | TypeValue  | DocumentOwnerValue |
      | Gehan.Ahmed | 2018000013  | Draft      | Admin          | 5-Aug-2018 9:02 AM | LOCAL_PO   | Gehan Ahmed        |
      | Gehan.Ahmed | 2018000021  | Draft      | Admin          | 5-Aug-2018 9:02 AM | IMPORT_PO  | Gehan Ahmed        |
      | Amr.Khalil  | 2018000025  | Draft      | Admin          | 5-Aug-2018 9:02 AM | IMPORT_PO  | Amr Khalil         |
      | Amr.Khalil  | 2018000026  | Draft      | Admin          | 5-Aug-2018 9:02 AM | IMPORT_PO  | Amr Khalil         |
      | Amr.Khalil  | 2018000027  | Draft      | Admin          | 5-Aug-2018 9:02 AM | LOCAL_PO   | Amr Khalil         |
      | Amr.Khalil  | 2018000028  | Draft      | Admin          | 5-Aug-2018 9:02 AM | LOCAL_PO   | Amr Khalil         |
      | Gehan.Ahmed | 2020000004  | Draft      | Admin          | 5-Aug-2020 9:02 AM | SERVICE_PO | Gehan Ahmed        |

  #### Exceptions Cases  ####
  #EBS-848 , #EBS-6993
  Scenario Outline: (02) View General Data section where PurchaseOrder doesn't exist: for Local , Import , Service (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to view General Data section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000013 |
      | Gehan.Ahmed | 2018000021 |
      | Gehan.Ahmed | 2020000004 |

  #### Abuse Cases  ####
  #EBS-848 , #EBS-6993
  Scenario Outline: (03) View General Data section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view General Data section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User       |
      | 2018000013 | Amr.Khalil |
      | 2018000021 | Amr.Khalil |
      | 2020000004 | Amr.Khalil |
