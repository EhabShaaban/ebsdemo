Feature: Save PaymentAndDelivery section in PurchaseOrder - Happy Paths

  Overview: Save Happy Paths cover he following cases:
  - Saving editable fields in states where Edit action is allowed
  - Saving without optional fields in states where Edit action is allowed
  - Saving without disabled fields even when they are supplied by user, in states where Edit action is allowed

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                       | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
    And the following PurchaseOrders exist with the following PaymentAndDelivery data:
      | Code       | Type      | State              | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000004 | IMPORT_PO | Draft              | 0003           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000021 | IMPORT_PO | Draft              | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000005 | IMPORT_PO | Approved           | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000024 | LOCAL_PO  | Draft              | 0003           | _        | _        | _            | _                    | _            | _              | _                           |                           | _               | _           | _             |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000014 | LOCAL_PO  | OpenForUpdates     | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000018 | LOCAL_PO  | Shipped            | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000019 | LOCAL_PO  | DeliveryComplete   | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000020 | LOCAL_PO  | Cancelled          | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And the following Incoterms exist:
      | Code | Name |
      | 0001 | FOB  |
      | 0002 | CIF  |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And the following ShippingInstructions exist:
      | Code | Name               |
      | 0001 | Ship by Shecartoon |
      | 0002 | Refregirated       |
    And the following ContainerTypes exist:
      | Code | Name |
      | 0001 | FCL  |
      | 0002 | LCL  |
    And the following ModeOfTransports exist:
      | Code | Name   |
      | 0001 | By Air |
      | 0002 | By Sea |
    And the following Ports exist:
      | Code | Name                | Country |
      | 0001 | Alexandria old port | Egypt   |
      | 0002 | Damietta            | Egypt   |

    And edit session is "30" minutes

  ####### Save PaymentandDelivery section (Happy Path) ##############################################################
  @ResetData
  Scenario Outline: (01) Save PaymentandDelivery section within edit session: Save Editable fields in states: Draft and OpenForUpdates / user enters CollectionDate_SpecificDate (Happy Path-1)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | <SpecificDateValue>         | 0001            | 0001        | 0002          |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | <SpecificDateValue>         |                           | 0001            | 0001        | 0002          |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State          | SpecificDateValue |
      | 2018000021 | Draft          | 05-Aug-2019       |
      | 2018000006 | OpenForUpdates | 05-Aug-2019       |
      | 2018000013 | Draft          | 05-Aug-2019       |
      | 2018000014 | OpenForUpdates | 05-Aug-2019       |

  @ResetData
  Scenario Outline: (02) Save PaymentandDelivery section within edit session: Save Editable fields in states: Draft and OpenForUpdates / user enters CollectionDate_TimePeriod (Happy Path-1)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | <TimePeriodValue>         | 0001            | 0001        | 0002          |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           |                             | <TimePeriodValue>         | 0001            | 0001        | 0002          |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State          | TimePeriodValue |
      | 2018000021 | Draft          | 30              |
      | 2018000006 | OpenForUpdates | 30              |
      | 2018000013 | Draft          | 30              |
      | 2018000014 | OpenForUpdates | 30              |

  @ResetData
  Scenario Outline: (03) Save PaymentandDelivery section within edit session: Save editable fields in states: Approved, Confimed, FinishedProduction, Shipped, Arrived, Cleared (Happy Path-2)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | CollectionDate_SpecificDate   | ModeOfTransport   | ShippingInstructions | ContainersNo | ContainersType | LoadingPort | DischargePort |
      | <Incoterm> | <Currency> | <PaymentTerms> | <CollectionDate_SpecificDate> | <ModeOfTransport> | 0001                 | 2            | 0001           | 0001        | 0002          |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate   | CollectionDate_TimePeriod   | ModeOfTransport   | LoadingPort | DischargePort |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | <Incoterm> | <Currency> | <PaymentTerms> | 0001                 | 2            | 0001           | <CollectionDate_SpecificDate> | <CollectionDate_TimePeriod> | <ModeOfTransport> | 0001        | 0002          |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State              | Incoterm | Currency | PaymentTerms | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport |
      | 2018000005 | Approved           | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000008 | Confirmed          | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000009 | FinishedProduction | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000010 | Shipped            | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000011 | Arrived            | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000001 | Cleared            | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000016 | Approved           | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000017 | Confirmed          | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |
      | 2018000018 | Shipped            | 0002     | 0002     | 0002         | 06-Oct-2020                 |                           | 0002            |

  @ResetData
  Scenario Outline: (04) Save PaymentandDelivery section within edit session: Save without optional fields in draft and OpenForUpdates states / user chooses to enter CollectionDate_SpecificDate (Happy Path-3)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | ModeOfTransport   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | CollectionDate_TimePeriod | ModeOfTransport   | LoadingPort   | DischargePort   |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> |                           | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State          | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000021 | Draft          |          |          |              |                      |              |                |                             |                 |             |               |
      | 2018000013 | Draft          |          |          |              |                      |              |                |                             |                 |             |               |
      | 2018000006 | OpenForUpdates | 0001     | 0001     | 0001         | 0001                 |              |                | 05-Aug-2019                 | 0001            |             |               |
      | 2018000014 | OpenForUpdates | 0001     | 0001     | 0001         |                      |              |                | 05-Aug-2019                 | 0001            |             |               |

  @ResetData
  Scenario Outline: (05) Save PaymentandDelivery section within edit session: Save without optional fields in draft and OpenForUpdates states / user chooses to enter CollectionDate_TimePeriod (Happy Path-3)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_TimePeriod   | ModeOfTransport   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_TimePeriod> | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate | CollectionDate_TimePeriod   | ModeOfTransport   | LoadingPort   | DischargePort   |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> |                             | <CollectionDate_TimePeriod> | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State          | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000021 | Draft          |          |          |              |                      |              |                |                             |                           |                 |             |               |
      | 2018000013 | Draft          |          |          |              |                      |              |                |                             |                           |                 |             |               |
      | 2018000006 | OpenForUpdates | 0001     | 0001     | 0001         | 0001                 |              |                |                             | 30                        | 0001            |             |               |
      | 2018000014 | OpenForUpdates | 0001     | 0001     | 0001         |                      |              |                |                             | 30                        | 0001            |             |               |

  @ResetData
  Scenario Outline: (06) Save PaymentandDelivery section within edit session: Save without optional fields in Approved, Confirmed, FinishedProduction, Shiiped, Arrived, Cleared states (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | CollectionDate_SpecificDate   | ModeOfTransport   | ShippingInstructions   | ContainersNo   | ContainersType   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <CollectionDate_SpecificDate> | <ModeOfTransport> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <LoadingPort> | <DischargePort> |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | CollectionDate_TimePeriod   | ModeOfTransport   | LoadingPort   | DischargePort   |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> | <CollectionDate_TimePeriod> | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State              | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000005 | Approved           | 0002     | 0002     | 0002         | 0001                 |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000016 | Approved           | 0002     | 0002     | 0002         |                      |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000008 | Confirmed          | 0002     | 0002     | 0002         | 0001                 |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000017 | Confirmed          | 0002     | 0002     | 0002         |                      |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000009 | FinishedProduction | 0002     | 0002     | 0002         | 0001                 |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000010 | Shipped            | 0002     | 0002     | 0002         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0002            | 0001        | 0002          |
      | 2018000018 | Shipped            | 0002     | 0002     | 0002         |                      |              |                | 06-Oct-2020                 |                           | 0002            |             |               |
      | 2018000011 | Arrived            | 0002     | 0002     | 0002         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0002            | 0001        | 0002          |
      | 2018000001 | Cleared            | 0002     | 0002     | 0002         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0002            | 0001        | 0002          |
