#Author: Shrouk & Sara #Date: 19-Dec-2018 12:29PM
#Reviewer: Hend Ahmed #Date: 20-Dec-2018 3:30PM

Feature: Read Next and Previous PO

  Background:
    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
      | hr1          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | M.D                              | POViewer            |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission            | Condition                       |
      | POViewer            | PurchaseOrder:ReadAll |                                 |
      | POViewer_Signmedia  | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadAll | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                   |                                 |
    And the following PurchaseOrders with the following order exist:
      | Code       | BusinessUnit      | State | Type      |
      | 2018100010 | 0002 - Signmedia  | Draft | IMPORT_PO |
      | 2018100009 | 0001 - Flexo      | Draft | IMPORT_PO |
      | 2018100008 | 0006 - Corrugated | Draft | IMPORT_PO |
      | 2018100007 | 0001 - Flexo      | Draft | IMPORT_PO |
      | 2018100006 | 0002 - Signmedia  | Draft | IMPORT_PO |
      | 2018100005 | 0002 - Signmedia  | Draft | LOCAL_PO  |
      | 2018100004 | 0001 - Flexo      | Draft | LOCAL_PO  |
      | 2018100003 | 0002 - Signmedia  | Draft | LOCAL_PO  |
      | 2018100002 | 0002 - Signmedia  | Draft | LOCAL_PO  |

  Scenario Outline: (01) Read Next PO by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view next PurchaseOrder of current PurchaseOrder with code "<PurchaseOrderCode>"
    Then the next PurchaseOrder code "<NextValue>" is displayed to "<User>"
    Examples:
      | User         | PurchaseOrderCode | NextValue  |
      | Gehan.Ahmed  | 2018100002        | 2018100003 |
      | Amr.Khalil   | 2018100004        | 2018100007 |
      | Amr.Khalil   | 2018100007        | 2018100008 |
      | Ashraf.Fathi | 2018100006        | 2018100007 |
      | hr1          | 2018100003        | 2018100004 |

  Scenario Outline: (02) Read previous PO by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view previous PurchaseOrder of current PurchaseOrder with code "<PurchaseOrderCode>"
    Then the previous PurchaseOrder code "<PrevValue>" is displayed to "<User>"
    Examples:
      | User         | PurchaseOrderCode | PrevValue  |
      | Gehan.Ahmed  | 2018100006        | 2018100005 |
      | Gehan.Ahmed  | 2018100003        | 2018100002 |
      | Amr.Khalil   | 2018100007        | 2018100004 |
      | Amr.Khalil   | 2018100009        | 2018100008 |
      | Ashraf.Fathi | 2018100006        | 2018100005 |
      | Ashraf.Fathi | 2018100007        | 2018100006 |
      | hr1          | 2018100003        | 2018100002 |

  @ResetData
  Scenario: (03) Read Next PO that isn't exist while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the PurchaseOrder with code "2018100010" successfully
    When "hr1" requests to view next PurchaseOrder of current PurchaseOrder with code "2018100009"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  @ResetData
  Scenario: (04) Read previous PO that isn't exist while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the PurchaseOrder with code "2018100002" successfully
    When "hr1" requests to view previous PurchaseOrder of current PurchaseOrder with code "2018100003"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-31"

  @ResetData
  Scenario: (05) Read Next PO that isn't exist
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the PurchaseOrder with code "2018100003" successfully
    When "hr1" requests to view next PurchaseOrder of current PurchaseOrder with code "2018100002"
    Then the next PurchaseOrder code "2018100004" is displayed to "hr1"

  @ResetData
  Scenario: (06) Read previous PO that isn't exist
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the PurchaseOrder with code "2018100003" successfully
    When "hr1" requests to view previous PurchaseOrder of current PurchaseOrder with code "2018100004"
    Then the previous PurchaseOrder code "2018100002" is displayed to "hr1"

  Scenario: (07) Read Next PO by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view next PurchaseOrder of current PurchaseOrder with code "2018100005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (08) Read previous PO by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view previous PurchaseOrder of current PurchaseOrder with code "2018100005"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
