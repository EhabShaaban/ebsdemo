# Reviewer: Aya Sadek (10 Feb, 2019 02:45 PM)
# Reviewer: Hend  (11-Feb-2019)
# Author: scnarios for users with not all authorized reads are added by Waseem (12-Feb-2019)
# Reviewer: Somaya (13-Feb-2019)

Feature: Request Edit / Cancel Consignee section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name                     | Role                                                     |
      | Gehan.Ahmed              | PurchasingResponsible_Signmedia                          |
      | Amr.Khalil               | PurchasingResponsible_Flexo                              |
      | Amr.Khalil               | PurchasingResponsible_Corrugated                         |
      | Gehan.Ahmed.NoCompany    | PurchasingResponsible_Signmedia_CannotViewCompanyRole    |
      | Gehan.Ahmed.NoPlant      | PurchasingResponsible_Signmedia_CannotViewPlantRole      |
      | Gehan.Ahmed.NoStorehouse | PurchasingResponsible_Signmedia_CannotViewStorehouseRole |
      | hr1                      | SuperUser                                                |
    And the following roles and sub-roles exist:
      | Role                                                     | Subrole                           |
      | PurchasingResponsible_Signmedia                          | POOwner_Signmedia                 |
      | PurchasingResponsible_Signmedia                          | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Signmedia                          | PlantViewer                       |
      | PurchasingResponsible_Signmedia                          | StorehouseViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | POOwner_Signmedia                 |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | PlantViewer                       |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole    | StorehouseViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | POOwner_Signmedia                 |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Signmedia_CannotViewPlantRole      | StorehouseViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | POOwner_Signmedia                 |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Signmedia_CannotViewStorehouseRole | PlantViewer                       |
      | PurchasingResponsible_Flexo                              | POOwner_Flexo                     |
      | PurchasingResponsible_Flexo                              | PlantViewer                       |
      | PurchasingResponsible_Flexo                              | StorehouseViewer                  |
      | PurchasingResponsible_Flexo                              | CompanyViewer_WithoutBankAccounts |
      | PurchasingResponsible_Corrugated                         | POOwner_Corrugated                |
      | PurchasingResponsible_Corrugated                         | PlantViewer                       |
      | PurchasingResponsible_Corrugated                         | StorehouseViewer                  |
      | PurchasingResponsible_Corrugated                         | CompanyViewer_WithoutBankAccounts |
      | SuperUser                                                | SuperUserSubRole                  |
    And the following sub-roles and permissions exist:
      | Subrole                           | Permission                    | Condition                       |
      | POOwner_Signmedia                 | PurchaseOrder:UpdateConsignee | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo                     | PurchaseOrder:UpdateConsignee | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated                | PurchaseOrder:UpdateConsignee | [purchaseUnitName='Corrugated'] |
      | CompanyViewer_WithoutBankAccounts | Company:ReadAll               |                                 |
      | PlantViewer                       | Plant:ReadAll                 |                                 |
      | StorehouseViewer                  | Storehouse:ReadAll            |                                 |
      | SuperUserSubRole                  | *:*                           |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
      | 2018000023 | Cleared            | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000024 | Draft            | 0003           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
      | 2018000030 | OpenForUpdates   | 0001           |
    And edit session is "30" minutes

  ########  Request to open Consignee section in edit mode ######################################################
  #EBS-80
  Scenario Outline: (01) Request to edit Consignee section: User with one or more role - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then Consignee section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads   |
      | ReadAllCompany    |
      | ReadAllPlant      |
      | ReadAllStorehouse |
    Examples:
      | POCode     | User        |
      | 2018000021 | Gehan.Ahmed |
      | 2018000006 | Gehan.Ahmed |
      | 2018000005 | Gehan.Ahmed |
      | 2018000008 | Gehan.Ahmed |
      | 2018000009 | Gehan.Ahmed |
      | 2018000010 | Gehan.Ahmed |
      | 2018000011 | Gehan.Ahmed |
      | 2018000001 | Gehan.Ahmed |
      | 2018000013 | Gehan.Ahmed |
      | 2018000014 | Gehan.Ahmed |
      | 2018000016 | Gehan.Ahmed |
      | 2018000017 | Gehan.Ahmed |
      | 2018000018 | Gehan.Ahmed |
      | 2018000025 | Amr.Khalil  |
      | 2018000026 | Amr.Khalil  |
      | 2018000027 | Amr.Khalil  |
      | 2018000028 | Amr.Khalil  |

  #EBS-80
  Scenario Outline: (01-a) Request to edit Consignee section: User with one or more role - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then Consignee section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads   |
      | ReadAllPlant      |
      | ReadAllStorehouse |
    Examples:
      | POCode     | User                  |
      | 2018000021 | Gehan.Ahmed.NoCompany |
      | 2018000013 | Gehan.Ahmed.NoCompany |

  #EBS-80
  Scenario Outline: (01-b) Request to edit Consignee section: User with one or more role - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then Consignee section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads   |
      | ReadAllCompany    |
      | ReadAllStorehouse |
    Examples:
      | POCode     | User                |
      | 2018000021 | Gehan.Ahmed.NoPlant |
      | 2018000013 | Gehan.Ahmed.NoPlant |

  #EBS-80
  Scenario Outline: (01-c) Request to edit Consignee section: User with one or more role - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then Consignee section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadAllCompany  |
      | ReadAllPlant    |
    Examples:
      | POCode     | User                     |
      | 2018000021 | Gehan.Ahmed.NoStorehouse |
      | 2018000013 | Gehan.Ahmed.NoStorehouse |

  #EBS-80
  Scenario Outline: (02-a) Request to edit Consignee section when section is locked by another user: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the Consignee section of PurchaseOrder with code "<POCode>" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     |
      | 2018000021 |

      #EBS-80
  @ResetDataForDelete
  Scenario Outline: (02-b) Request to edit Consignee section when section is locked by another user: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the Consignee section of PurchaseOrder with code "<POCode>" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     |
      | 2018000013 |

  #EBS-80
  @ResetDataForDelete
  Scenario Outline: (03) Request to edit Consignee section of a PurchaseOrder that doesn't exist: for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-80
  Scenario Outline: (04) Request to edit Consignee section when it is not allowed in current state: : Waiting Approval, Cancelled and DeliveryComplete for both Local/Import (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<PurCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And Consignee section of PurchaseOrder with code "<PurCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | PurCode    |
      | 2018000007 |
      | 2018000012 |
      | 2018000002 |
      | 2018000015 |
      | 2018000019 |
      | 2018000020 |

  #EBS-80
  Scenario Outline: (05) Request to edit Consignee section by an unauthorized user : for both Local/Import PO (Unauthorized/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Consignee section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |

  #EBS-80
  @ResetData
  Scenario Outline: (06) Request to edit Consignee section while edit action is not allowed in Current state for Import PO (i.e. marked as WaitingApproval by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first SubmittedForApproval the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    Examples:
      | POCode     |
      | 2018000006 |

  #EBS-80
  @ResetData
  Scenario Outline: (07) Request to edit Consignee section while edit action is not allowed in Current state for Local PO (i.e. marked as WaitingApproval by another user) (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first SubmittedForApproval the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Amr.Khalil" requests to edit Consignee section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-03"
    Examples:
      | POCode     |
      | 2018000030 |

  #EBS-80
  @ResetData
  Scenario Outline: (08) Request to edit Consignee section while edit action is not allowed in Current state for Local and Import PO (i.e. marked as DeliveryComplete by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first MarkedAsDeliveryComplete the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    Examples:
      | POCode     |
      | 2018000023 |
      | 2018000018 |

  #EBS-80
  @ResetData
  Scenario Outline: (09) Request to edit Consignee section while edit action is not allowed in Current state (i.e. marked as Cancelled by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first Cancelled the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" requests to edit Consignee section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    Examples:
      | POCode     |
      | 2018000006 |
      | 2018000014 |

  ######## Cancel saving Consignee section ########################################################################

  #EBS-80
  Scenario Outline: (10) Cancel Request to edit Consignee section within the edit session: User with one or more role - All states except WaitingApproval, DeliveryComplete and Cancelled - for both Import and Local POs (Happy Path)
    Given user is logged in as "<User>"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on Consignee section of PurchaseOrder with Code "<POCode>" is released
    Examples:
      | POCode     | User        |
      | 2018000021 | Gehan.Ahmed |
      | 2018000006 | Gehan.Ahmed |
      | 2018000005 | Gehan.Ahmed |
      | 2018000008 | Gehan.Ahmed |
      | 2018000009 | Gehan.Ahmed |
      | 2018000010 | Gehan.Ahmed |
      | 2018000011 | Gehan.Ahmed |
      | 2018000001 | Gehan.Ahmed |
      | 2018000013 | Gehan.Ahmed |
      | 2018000014 | Gehan.Ahmed |
      | 2018000016 | Gehan.Ahmed |
      | 2018000017 | Gehan.Ahmed |
      | 2018000018 | Gehan.Ahmed |
      | 2018000025 | Amr.Khalil  |
      | 2018000026 | Amr.Khalil  |
      | 2018000027 | Amr.Khalil  |
      | 2018000028 | Amr.Khalil  |

  #EBS-80
  Scenario Outline: (11) Cancel Request to edit Consignee section after edit session expire: for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-80
  Scenario Outline: (12-a) Cancel Request to edit Consignee section after edit session expire and section got locked by another user for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000021 |
  #EBS-80
  @ResetDataForDelete
  Scenario Outline: (12-b) Cancel Request to edit Consignee section after edit session expire and section got locked by another user for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000013 |

  #EBS-80
  @ResetDataForDelete
  Scenario Outline:(13) Cancel Request to edit Consignee section while PurchaseOrder doesn't exist: for both Local and Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  #EBS-80
  @ResetData
  Scenario Outline: (14) Cancel Request to edit Consignee section while cancel action is not allowed in Current state for Import PO (i.e. marked as WaitingApproval by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first SubmittedForApproval the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000006 |

  #EBS-80
  @ResetData
  Scenario Outline: (15) Cancel Request to edit Consignee section while cancel action is not allowed in Current state for Local PO (i.e. marked as WaitingApproval by another user) (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    And "hr1" first SubmittedForApproval the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Amr.Khalil" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000030 |

  #EBS-80
  @ResetData
  Scenario Outline: (16) Cancel Request to edit Consignee section while cancel action is not allowed in Current state for Local and Import PO (i.e. marked as DeliveryComplete by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first MarkedAsDeliveryComplete the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000023 |
      | 2018000018 |

  #EBS-80
  @ResetData
  Scenario Outline: (17) Cancel Request to edit Consignee section while cancel action is not allowed in Current state (i.e. marked as Cancelled by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Consignee section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first Cancelled the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" cancels saving Consignee section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     |
      | 2018000006 |
      | 2018000014 |

  #EBS-80
  Scenario Outline: (18) Cancel Request to edit Consignee section by an unauthorized users: for both Local and Import PO (Authorization\Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving Consignee section of PurchaseOrder with Code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000004 |
      | Gehan.Ahmed | 2018000024 |
