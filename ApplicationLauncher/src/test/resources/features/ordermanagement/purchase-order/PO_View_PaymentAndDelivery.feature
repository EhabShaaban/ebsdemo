# Reviewer: Marina (27-Jan-2019 4:00 PM)
# Reviewer: Somaya (28-Jan-2019 5:00 PM)

Feature: View PaymentAndDelivery section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | hr1         | SuperUser                        |
      | Afaf        | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                     | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadPaymentTerms | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadPaymentTerms | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadPaymentTerms | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                            |                                 |
    And the following PurchaseOrders exist with the following PaymentAndDelivery data:
      | Code       | Type      | State | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000021 | IMPORT_PO | Draft | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000025 | IMPORT_PO | Draft | 0001           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000026 | IMPORT_PO | Draft | 0006           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And the following PurchaseOrders exist with the following PaymentAndDelivery data:
      | Code       | Type     | State | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000013 | LOCAL_PO | Draft | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000027 | LOCAL_PO | Draft | 0001           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000028 | LOCAL_PO | Draft | 0006           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And the following Incoterms exist:
      | Code | Name |
      | 0002 | CIF  |
    And the following Currencies exist:
      | Code | Name                 |
      | 0002 | United States Dollar |
    And the following PaymentTerms exist:
      | Code | Name                         |
      | 0002 | 20% advance, 80% Copy of B/L |
    And the following ShippingInstructions exist:
      | Code | Name         |
      | 0002 | Refregirated |
    And the following ContainerTypes exist:
      | Code | Name |
      | 0002 | LCL  |
    And the following ModeOfTransports exist:
      | Code | Name   |
      | 0002 | By Sea |
    And the following Ports exist:
      | Code | Name                | Country |
      | 0001 | Alexandria old port | Egypt   |
      | 0002 | Damietta            | Egypt   |

  #### Happy Path  ####
  #EBS-1331
  Scenario Outline: (01) View PaymentandDelivery section by an authorized user who has one / teo roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view PaymentandDelivery section of PurchaseOrder with code "<POCodeValue>"
    Then the following values of PaymentandDelivery section are displayed to "<User>":
      | Incoterm        | Currency        | PaymentTerms        | ShippingInstructions        | ContainersNo   | ContainersType        | CollectionDate_SpecificDate   | CollectionDate_TimePeriod   | ModeOfTransport        | LoadingPort        | DischargePort        |
      | <IncotermValue> | <CurrencyValue> | <PaymentTermsValue> | <ShippingInstructionsValue> | <ContainersNo> | <ContainersTypeValue> | <CollectionDate_SpecificDate> | <CollectionDate_TimePeriod> | <ModeOfTransportValue> | <LoadingPortValue> | <DischargePortValue> |
    Examples:
      | User        | POCodeValue | IncotermValue | CurrencyValue        | PaymentTermsValue            | ShippingInstructionsValue | ContainersNo | ContainersTypeValue | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransportValue | LoadingPortValue | DischargePortValue  |
      | Gehan.Ahmed | 2018000021  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |
      | Gehan.Ahmed | 2018000013  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |
      | Amr.Khalil  | 2018000025  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |
      | Amr.Khalil  | 2018000026  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |
      | Amr.Khalil  | 2018000027  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |
      | Amr.Khalil  | 2018000028  | CIF           | United States Dollar | 20% advance, 80% Copy of B/L | Refregirated              | 10           | LCL                 | 06-Oct-2020                 |                           | By Sea               | Damietta         | Alexandria old port |

  #### Exceptions  ####
  #EBS-1331
  @ResetData
  Scenario Outline: (03) View PaymentandDelivery section where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  #### Abuse Cases  ####
  #EBS-1331
  Scenario Outline: (04) View PaymentandDelivery section by an unauthorized user  due to condition OR not (Unauthorized/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view PaymentandDelivery section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User       |
      | 2018000013 | Afaf       |
      | 2018000021 | Afaf       |
      | 2018000013 | Amr.Khalil |
      | 2018000021 | Amr.Khalil |
