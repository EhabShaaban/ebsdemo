Feature: Save PaymentandDelivery section in PurchaseOrder - Validations and Exceptions

  #  Overview: Save Validations and Exceptions Paths cover he following cases for both Local/Import POs:
  #      - Saving disabled fields
  #      - Saving with incorrect data
  #      - Saving with malicious Input
  #      - Saving with mandatory fields missing
  #      - Saving when edit session expired
  #      - Saving when edit session expired and instance is deleted
  #      - Saving when edit session expired and instance state changed to a state that doesn't allow editing

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                       | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | Type      | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000004 | Draft              | IMPORT_PO | 0003           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000021 | Draft              | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000006 | OpenForUpdates     | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000007 | WaitingApproval    | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000005 | Approved           | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000008 | Confirmed          | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000009 | FinishedProduction | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000010 | Shipped            | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000011 | Arrived            | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000001 | Cleared            | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000002 | DeliveryComplete   | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000012 | Cancelled          | IMPORT_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And the following Local PurchaseOrders exist:
      | Code       | State            | Type     | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000024 | Draft            | LOCAL_PO | 0003           | _        | _        | _            | _                    | _            | _              | _                           |                           | _               | _           | _             |
      | 2018000013 | Draft            | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000014 | OpenForUpdates   | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000015 | WaitingApproval  | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000016 | Approved         | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000017 | Confirmed        | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000018 | Shipped          | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000019 | DeliveryComplete | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
      | 2018000020 | Cancelled        | LOCAL_PO | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And the following Incoterms exist:
      | Code | Name |
      | 0001 | FOB  |
      | 0002 | CIF  |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And the following ShippingInstructions exist:
      | Code | Name               |
      | 0001 | Ship by Shecartoon |
      | 0002 | Refregirated       |
    And the following ContainerTypes exist:
      | Code | Name |
      | 0001 | FCL  |
      | 0002 | LCL  |
    And the following ModeOfTransports exist:
      | Code | Name   |
      | 0001 | By Air |
      | 0002 | By Sea |
    And the following Ports exist:
      | Code | Name                | Country |
      | 0001 | Alexandria old port | Egypt   |
      | 0002 | Damietta            | Egypt   |
    And edit session is "30" minutes

  ###### Save PaymentandDelivery section with Incorrect Data (Validation Failure)  ##############################################################
  @ResetData
  Scenario Outline: (01) Save PaymentandDelivery section disabled fields for Local/Import POs in states:
  Approved, Confimed, FinsihedProduction, Shipped, Arrived, Cleared (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 0002     | 0002     | 0002         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0002            | 0001        | 0002          |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State              |
      | 2018000005 | Approved           |
      | 2018000008 | Confirmed          |
      | 2018000009 | FinishedProduction |
      | 2018000010 | Shipped            |
      | 2018000011 | Arrived            |
      | 2018000001 | Cleared            |
      | 2018000016 | Approved           |
      | 2018000017 | Confirmed          |
      | 2018000018 | Shipped            |

  @ResetData
  Scenario Outline: (01a) Save PaymentandDelivery section disabled fields for Local/Import POs in states:
  Approved, Confimed, FinsihedProduction, Shipped, Arrived, Cleared (Happy Path) chnges ignored for disabled fields
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0001            | 0001        | 0002          |
    Then PaymentAndDelivery section of PurchaseOrder with Code "<Code>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 0002     | 0002     | 0002         | 0001                 | 2            | 0001           | 06-Oct-2020                 |                           | 0002            | 0001        | 0002          |
    And the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Code       | State              |
      | 2018000005 | Approved           |
      | 2018000008 | Confirmed          |
      | 2018000009 | FinishedProduction |
      | 2018000010 | Shipped            |
      | 2018000011 | Arrived            |
      | 2018000001 | Cleared            |
      | 2018000016 | Approved           |
      | 2018000017 | Confirmed          |
      | 2018000018 | Shipped            |

  ###### Save PaymentandDelivery section with Incorrect Data (Validation Failure)  ##############################################################
  Scenario Outline: (02) Save PaymentandDelivery section Incorrect data: Instance doesn't exist for Local/Import POs
  >> PO-BR-12 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | CollectionDate_TimePeriod | ModeOfTransport   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> |                           | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<TargetField>" field "<ErrorMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | Code       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort | TargetField          | ErrorMsg  |
      | 2018000021 | 9999     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | incoterm             | PO-msg-12 |
      | 2018000013 | 9999     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | incoterm             | PO-msg-12 |
      | 2018000021 | 0001     | 9999     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | currency             | PO-msg-13 |
      | 2018000013 | 0001     | 9999     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | currency             | PO-msg-13 |
      | 2018000021 | 0001     | 0001     | 9999         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | paymentTerms         | PO-msg-14 |
      | 2018000013 | 0001     | 0001     | 9999         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | paymentTerms         | PO-msg-14 |
      | 2018000021 | 0001     | 0001     | 0001         | 9999                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | shippingInstructions | PO-msg-15 |
      | 2018000013 | 0001     | 0001     | 0001         | 9999                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          | shippingInstructions | PO-msg-15 |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 9999           | 5-Aug-2019                  | 0001            | 0001        | 0002          | containersType       | PO-msg-16 |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 9999           | 5-Aug-2019                  | 0001            | 0001        | 0002          | containersType       | PO-msg-16 |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 9999            | 0001        | 0002          | modeOfTransport      | PO-msg-17 |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 9999            | 0001        | 0002          | modeOfTransport      | PO-msg-17 |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 9999        | 0002          | loadingPort          | PO-msg-19 |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 9999        | 0002          | loadingPort          | PO-msg-19 |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 9999          | dischargePort        | PO-msg-20 |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 9999          | dischargePort        | PO-msg-20 |

  ####### Save PaymentandDelivery section with Malicious Input  (Abuse Cases )##############################################################

  Scenario Outline: (3) Save PaymentandDelivery section Malicious Input: Field is string for Local/Import POs
  >> PO-BR-12 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | CollectionDate_TimePeriod | ModeOfTransport   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> |                           | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Code       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000021 | Ay 7aga  | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000013 | Ay 7aga  | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000021 | 0001     | Ay 7aga  | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000013 | 0001     | Ay 7aga  | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000021 | 0001     | 0001     | Ay 7aga      | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000013 | 0001     | 0001     | Ay 7aga      | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000021 | 0001     | 0001     | 0001         | Ay 7aga              | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000013 | 0001     | 0001     | 0001         | Ay 7aga              | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | Ay 7aga        | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | Ay 7aga        | 5-Aug-2019                  | 0001            | 0001        | 0002          |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | Ay 7aga         | 0001        | 0002          |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | Ay 7aga         | 0001        | 0002          |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | Ay 7aga     | 0002          |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | Ay 7aga     | 0002          |
      | 2018000021 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | Ay 7aga       |
      | 2018000013 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 0001            | 0001        | Ay 7aga       |

  Scenario Outline: (4) Save PaymentandDelivery section Malicious Input: ContainersNo is not Integer number greater than zero for Local/Import POs
  >> Gen-BR-11 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo        | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | <ContainersNoValue> | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ContainersNoValue | Code       |
      | 0                 | 2018000021 |
      | -1                | 2018000021 |
      | 2.4               | 2018000021 |
      | -2.4              | 2018000021 |
      | Ay 7aga           | 2018000021 |
      | 0                 | 2018000013 |
      | -1                | 2018000013 |
      | 2.4               | 2018000013 |
      | -2.4              | 2018000013 |
      | Ay 7aga           | 2018000013 |

  Scenario Outline: (5) Save PaymentandDelivery section Malicious Input: CollectionDate_SpecificDate is invalid date for Local/Import POs
  >> Gen-BR-10 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | <SpecificDateValue>         |                           | 0001            | 0001        | 0002          |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | SpecificDateValue | Code       |
      | Jan-25-2018       | 2018000021 |
      | 25-01-2018        | 2018000021 |
      | 25-January-2018   | 2018000021 |
      | 0                 | 2018000021 |
      | -2                | 2018000021 |
      | 2.4               | 2018000021 |
      | -2.4              | 2018000021 |
      | Ay 7aga           | 2018000021 |
      | Jan-25-2018       | 2018000013 |
      | 25-01-2018        | 2018000013 |
      | 25-January-2018   | 2018000013 |
      | 0                 | 2018000013 |
      | -2                | 2018000013 |
      | 2.4               | 2018000013 |
      | -2.4              | 2018000013 |
      | Ay 7aga           | 2018000013 |

  Scenario Outline: (6) Save PaymentandDelivery section Malicious Input: CollectionDate_TimePeriod is not Integer number greater than zero for Local/Import POs
  >> Gen-BR-11 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           |                             | <TimePeriodValue>         | 0001            | 0001        | 0002          |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | TimePeriodValue | Code       |
      | 0               | 2018000021 |
      | -1              | 2018000021 |
      | 2.4             | 2018000021 |
      | -2.4            | 2018000021 |
      | Ay 7aga         | 2018000021 |
      | 0               | 2018000013 |
      | -1              | 2018000013 |
      | 2.4             | 2018000013 |
      | -2.4            | 2018000013 |
      | Ay 7aga         | 2018000013 |

  Scenario Outline: (7) Save PaymentandDelivery section Malicious Input: User entered both CollectionDate_Specific and CollectionDate_TimePeriod for both Import/Local POs
  >> PO-BR-24 (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 AM" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  | 30                        | 0001            | 0001        | 0002          |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Code       |
      | 2018000021 |
      | 2018000013 |

  @Future
  Scenario Outline: (8) Save PaymentandDelivery section Malicious Input: Incorrect data:Loading Port and DischargePort have the same value for Local/Import POs >> PO-BR-25 (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 AM" with the following values:
      | LoadingPort | DischargePort |
      | 0001        | 0001          |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Code       |
      | 2018000021 |
      | 2018000013 |

  ############ Save with missing mandatory fields (Abuse Cases) ###########################################################################

  Scenario Outline: (9) Save PaymentandDelivery section Missing mandatory Fields for Local/Import POs
  >> PO in OpenForUpdates state (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm   | Currency   | PaymentTerms   | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate   | CollectionDate_TimePeriod   | ModeOfTransport   | LoadingPort   | DischargePort   |
      | <Incoterm> | <Currency> | <PaymentTerms> | <ShippingInstructions> | <ContainersNo> | <ContainersType> | <CollectionDate_SpecificDate> | <CollectionDate_TimePeriod> | <ModeOfTransport> | <LoadingPort> | <DischargePort> |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Code       | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000006 |          | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000014 |          | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000006 | 0001     |          | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000014 | 0001     |          | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000006 | 0001     | 0001     |              | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000014 | 0001     | 0001     |              | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000006 | 0001     | 0001     | 0001         |                      | 2            | 0001           | 5-Aug-2019                  |                           | 0001            | 0001        | 0002          |
      | 2018000006 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           |                             |                           | 0001            | 0001        | 0002          |
      | 2018000014 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           |                             |                           | 0001            | 0001        | 0002          |
      | 2018000006 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           |                 | 0001        | 0002          |
      | 2018000014 | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 5-Aug-2019                  |                           |                 | 0001        | 0002          |

  Scenario Outline: (10) Save PaymentandDelivery section Missing mandatory Fields for Local/Import POs
  >> PO in Approved, Confimed, FinsihedProduction, Shipped, Arrived, Cleared states (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:30 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions   | ContainersNo   | ContainersType   | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort   | DischargePort   |
      |          |          |              | <ShippingInstructions> | <ContainersNo> | <ContainersType> |                             |                           |                 | <LoadingPort> | <DischargePort> |
    Then the lock by "Gehan.Ahmed" on PaymentandDelivery section of PurchaseOrder with Code "<Code>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Code       | State              | ShippingInstructions | ContainersNo | ContainersType | LoadingPort | DischargePort |
      | 2018000005 | Approved           |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000008 | Confirmed          |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000009 | FinishedProduction |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000010 | Shipped            |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000011 | Arrived            |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000001 | Cleared            |                      | 2            | 0001           | 0001        | 0002          |
      | 2018000010 | Shipped            | 0001                 |              | 0001           | 0001        | 0002          |
      | 2018000011 | Arrived            | 0001                 |              | 0001           | 0001        | 0002          |
      | 2018000001 | Cleared            | 0001                 |              | 0001           | 0001        | 0002          |
      | 2018000010 | Shipped            | 0001                 | 2            |                | 0001        | 0002          |
      | 2018000011 | Arrived            | 0001                 | 2            |                | 0001        | 0002          |
      | 2018000001 | Cleared            | 0001                 | 2            |                | 0001        | 0002          |
      | 2018000010 | Shipped            | 0001                 | 2            | 0001           |             | 0002          |
      | 2018000011 | Arrived            | 0001                 | 2            | 0001           |             | 0002          |
      | 2018000001 | Cleared            | 0001                 | 2            | 0001           |             | 0002          |
      | 2018000010 | Shipped            | 0001                 | 2            | 0001           | 0001        |               |
      | 2018000011 | Arrived            | 0001                 | 2            | 0001           | 0001        |               |
      | 2018000001 | Cleared            | 0001                 | 2            | 0001           | 0001        |               |

  ######### Save PaymentandDelivery (After edit session expires) ##############################################################

  Scenario Outline: (11) Save PaymentandDelivery section after edit session expired for both Local and Import PO (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 9:41 am" with the following values:
      | Incoterm   | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | <Incoterm> | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0002          |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | Code       | Incoterm |
      | 2018000021 | 0001     |
      | 2018000013 | 0001     |
      | 2018000021 | 9999     |
      | 2018000013 | 9999     |

  @ResetData
  Scenario Outline: (12) Save PaymentandDelivery section after edit session expired & PO is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentandDelivery section of PurchaseOrder with Code "<Code>" is locked by "Gehan.Ahmed" at "7-Jan-2019 9:10 am"
    And "hr1" first deleted the PurchaseOrder with code "<Code>" successfully at "7-Jan-2019 11:00 am"
    When "Gehan.Ahmed" saves PaymentandDelivery section of PurchaseOrder with Code "<Code>" at "7-Jan-2019 11:10 am" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0002          |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | Code       |
      | 2018000021 |
      | 2018000013 |