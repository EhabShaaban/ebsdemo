# Author: Somaya Aboulwafa (26-Nov-2018 2:15 pm)
# Reviewer: Hend Aboulwafa, (27-Nov-2018)
# Reviewer: Sara Mosad (02-Dec-2018 3:30 PM)
# Reviewer: Marina (28-JAN-2019 11:45 AM)

Feature: View Approval Cycles

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                       | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadApprovalCycles | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadApprovalCycles | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadApprovalCycles | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                              |                                 |

    And the following Import PurchaseOrders exist:
      | Code       | State           | PurchasingUnit |
      | 2018000021 | Draft           | 0002           |
      | 2018000006 | OpenForUpdates  | 0002           |
      | 2018000005 | Approved        | 0002           |
      | 2018000032 | WaitingApproval | 0001           |
      | 2018000029 | OpenForUpdates  | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State          | PurchasingUnit |
      | 2018000013 | Draft          | 0002           |
      | 2018000014 | OpenForUpdates | 0002           |
      | 2018000016 | Approved       | 0002           |
      | 2018000030 | OpenForUpdates | 0001           |
      | 2018000031 | OpenForUpdates | 0006           |

    And the following user information exist:
      | UserName            | ProfileName         |
      | Ashraf.Salah        | Ashraf Salah        |
      | Mohamed.Abdelmoniem | Mohamed Abdelmoniem |
      | Essam.Mahmoud       | Essam Mahmoud       |
      | Ahmed.Hussein       | Ahmed Hussein       |
      | Yasser.Hussein      | Yasser Hussein      |

    And the following ControlPoints exist:
      | Code | Name                         |
      | 0001 | Signmedia Accountant Head    |
      | 0002 | Signmedia Budget Controller  |
      | 0003 | Signmedia Director           |
      | 0004 | Flexo Accountant Head        |
      | 0005 | Flexo Budget Controller      |
      | 0006 | Flexo Director               |
      | 0007 | Corrugated Accountant Head   |
      | 0008 | Corrugated Budget Controller |
      | 0009 | Corrugated Director          |

    # Local and  Import & Signmedia & No Approval Cycles
    And the following PurchaseOrders has an empty ApprovalCycles section:
      | Code       |
      | 2018000006 |
      | 2018000014 |

    # Import PO & Signmedia PO & Multiple Closed Approval Cycles
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000005":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 03:00 PM | Approved      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 09:15 AM | Rejected      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000005" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes                     |
      | 0001         | Ashraf.Salah        | 21-Sep-2018 09:15 AM | Rejected | No cash flow for Sigmedia |
      | 0002         | Mohamed.Abdelmoniem |                      |          |                           |
      | 0003         | Essam.Mahmoud       |                      |          |                           |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000005" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0001         | Ashraf.Salah        | 21-Sep-2018 01:30 PM | Approved |       |
      | 0002         | Mohamed.Abdelmoniem | 21-Sep-2018 02:00 PM | Approved |       |
      | 0003         | Essam.Mahmoud       | 21-Sep-2018 03:00 PM | Approved |       |

    # Import PO & Flexo PO & Multiple Approval Cycles with one still opened
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000032":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM |                      |               |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000032" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0005         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0006         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Rejected |       |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000032" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes                  |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 01:15 PM | Approved | No cash flow for Flexo |
      | 0005         | Mohamed.Abdelmoniem |                      |          |                        |
      | 0006         | Yasser.Hussein      |                      |          |                        |

    # Import PO & Corrugated PO & Multiple Approval Cycles
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000029":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000029" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0007         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0008         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0009         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000029" has the following values:
      | ControlPoint | User          | DateTime             | Decision | Notes                       |
      | 0007         | Ahmed.Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Corrugated |

    # Local PO & Signmedia PO & Multiple Approval Cycles
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000016":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
      | 1               | 20-Sep-2018 01:00 PM | 20-Sep-2018 01:15 PM | Rejected      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000016" has the following values:
      | ControlPoint | User         | DateTime             | Decision | Notes                     |
      | 0001         | Ashraf.Salah | 20-Sep-2018 01:15 PM | Rejected | No cash flow for Sigmedia |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000016" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0001         | Ashraf.Salah        | 21-Sep-2018 10:00 AM | Approved |       |
      | 0002         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0003         | Essam.Mahmoud       | 21-Sep-2018 11:00 AM | Approved |       |

    # Local PO & Flexo PO & Multiple Approval Cycles
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000030":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000030" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0004         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0005         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0006         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000030" has the following values:
      | ControlPoint | User          | DateTime             | Decision | Notes                  |
      | 0004         | Ahmed.Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Flexo |

    # Local PO & Corrugated PO & Multiple Approval Cycles
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000031":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the ApprovalCycleNo "1" in PurchaseOrder with code "2018000031" has the following values:
      | ControlPoint | User                | DateTime             | Decision | Notes |
      | 0007         | Ahmed.Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | 0008         | Mohamed.Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | 0009         | Yasser.Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the ApprovalCycleNo "2" in PurchaseOrder with code "2018000031" has the following values:
      | ControlPoint | User          | DateTime             | Decision | Notes                       |
      | 0007         | Ahmed.Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Corrugated |


  #### Happy Paths####
  # EBS-1167
  Scenario Outline: (01) View ApprovalCycles section with User has one role and PO is Import/Local has no Approval Cycles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view ApprovalCycles section of PurchaseOrder with code "<POCode>"
    Then an empty ApprovalCycles section is displayed to "Gehan.Ahmed"
    Examples:
      | POCode     |
      | 2018000006 |
      | 2018000014 |

  # EBS-1167
  Scenario: (02) View ApprovalCycles section with User has one role and PO is Import/Signmedia has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view ApprovalCycles section of PurchaseOrder with code "2018000005"
    Then the following values of ApprovalCycles section are displayed to "Gehan.Ahmed":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 03:00 PM | Approved      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 09:15 AM | Rejected      |
    And the following details of ApprovalCycleNo "1" are displayed to "Gehan.Ahmed":
      | ControlPoint                | User                | DateTime             | Decision | Notes                     |
      | Signmedia Accountant Head   | Ashraf Salah        | 21-Sep-2018 09:15 AM | Rejected | No cash flow for Sigmedia |
      | Signmedia Budget Controller | Mohamed Abdelmoniem |                      |          |                           |
      | Signmedia Director          | Essam Mahmoud       |                      |          |                           |

    And the following details of ApprovalCycleNo "2" are displayed to "Gehan.Ahmed":
      | ControlPoint                | User                | DateTime             | Decision | Notes |
      | Signmedia Accountant Head   | Ashraf Salah        | 21-Sep-2018 01:30 PM | Approved |       |
      | Signmedia Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 02:00 PM | Approved |       |
      | Signmedia Director          | Essam Mahmoud       | 21-Sep-2018 03:00 PM | Approved |       |

  # EBS-1167
  Scenario: (04) View ApprovalCycles section with User has one role and PO is Local/Signmedia has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view ApprovalCycles section of PurchaseOrder with code "2018000016"
    Then the following values of ApprovalCycles section are displayed to "Gehan.Ahmed":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
      | 1               | 20-Sep-2018 01:00 PM | 20-Sep-2018 01:15 PM | Rejected      |
    And the following details of ApprovalCycleNo "1" are displayed to "Gehan.Ahmed":
      | ControlPoint              | User         | DateTime             | Decision | Notes                     |
      | Signmedia Accountant Head | Ashraf Salah | 20-Sep-2018 01:15 PM | Rejected | No cash flow for Sigmedia |
    And the following details of ApprovalCycleNo "2" are displayed to "Gehan.Ahmed":
      | ControlPoint                | User                | DateTime             | Decision | Notes |
      | Signmedia Accountant Head   | Ashraf Salah        | 21-Sep-2018 10:00 AM | Approved |       |
      | Signmedia Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | Signmedia Director          | Essam Mahmoud       | 21-Sep-2018 11:00 AM | Approved |       |

  # EBS-1167
  Scenario: (05) View ApprovalCycles section with User has two roles and PO is Import/Flexo has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view ApprovalCycles section of PurchaseOrder with code "2018000032"
    Then the following values of ApprovalCycles section are displayed to "Amr.Khalil":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM |                      |               |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    And the following details of ApprovalCycleNo "1" are displayed to "Amr.Khalil":
      | ControlPoint            | User                | DateTime             | Decision | Notes |
      | Flexo Accountant Head   | Ahmed Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | Flexo Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | Flexo Director          | Yasser Hussein      | 21-Sep-2018 11:00 AM | Rejected |       |
    And the following details of ApprovalCycleNo "2" are displayed to "Amr.Khalil":
      | ControlPoint            | User                | DateTime             | Decision | Notes                  |
      | Flexo Accountant Head   | Ahmed Hussein       | 21-Sep-2018 01:15 PM | Approved | No cash flow for Flexo |
      | Flexo Budget Controller | Mohamed Abdelmoniem |                      |          |                        |
      | Flexo Director          | Yasser Hussein      |                      |          |                        |

  # EBS-1167
  Scenario: (06) View ApprovalCycles section with User has two roles and PO is Import/Corrugated has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view ApprovalCycles section of PurchaseOrder with code "2018000029"
    Then the following values of ApprovalCycles section are displayed to "Amr.Khalil":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the following details of ApprovalCycleNo "1" are displayed to "Amr.Khalil":
      | ControlPoint                 | User                | DateTime             | Decision | Notes |
      | Corrugated Accountant Head   | Ahmed Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | Corrugated Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | Corrugated Director          | Yasser Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the following details of ApprovalCycleNo "2" are displayed to "Amr.Khalil":
      | ControlPoint               | User          | DateTime             | Decision | Notes                       |
      | Corrugated Accountant Head | Ahmed Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Corrugated |

  # EBS-1167
  Scenario: (07) View ApprovalCycles section with User has two roles and PO is Local/Flexo has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view ApprovalCycles section of PurchaseOrder with code "2018000030"
    Then the following values of ApprovalCycles section are displayed to "Amr.Khalil":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the following details of ApprovalCycleNo "1" are displayed to "Amr.Khalil":
      | ControlPoint            | User                | DateTime             | Decision | Notes |
      | Flexo Accountant Head   | Ahmed Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | Flexo Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | Flexo Director          | Yasser Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the following details of ApprovalCycleNo "2" are displayed to "Amr.Khalil":
      | ControlPoint          | User          | DateTime             | Decision | Notes                  |
      | Flexo Accountant Head | Ahmed Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Flexo |

  # EBS-1167
  Scenario: (08) View ApprovalCycles section with User has two roles and PO is Local/Corrugated has multiple Approval Cycles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view ApprovalCycles section of PurchaseOrder with code "2018000031"
    Then the following values of ApprovalCycles section are displayed to "Amr.Khalil":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 21-Sep-2018 01:15 PM | Rejected      |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Approved      |
    And the following details of ApprovalCycleNo "1" are displayed to "Amr.Khalil":
      | ControlPoint                 | User                | DateTime             | Decision | Notes |
      | Corrugated Accountant Head   | Ahmed Hussein       | 21-Sep-2018 10:00 AM | Approved |       |
      | Corrugated Budget Controller | Mohamed Abdelmoniem | 21-Sep-2018 10:30 AM | Approved |       |
      | Corrugated Director          | Yasser Hussein      | 21-Sep-2018 11:00 AM | Approved |       |
    And the following details of ApprovalCycleNo "2" are displayed to "Amr.Khalil":
      | ControlPoint               | User          | DateTime             | Decision | Notes                       |
      | Corrugated Accountant Head | Ahmed Hussein | 21-Sep-2018 01:15 PM | Rejected | No cash flow for Corrugated |

  #### Exception Cases ####
  # EBS-1167
  @ResetData
  Scenario Outline: (09) View ApprovalCycles section where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to view ApprovalCycles section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |

  #### Abuse Cases  ####
  # EBS-1167
  Scenario Outline: (10) View ApprovalCycles section by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view ApprovalCycles section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     |
      | Mahmoud.Abdelaziz | 2018000005 |
      | Mahmoud.Abdelaziz | 2018000016 |
      | Amr.Khalil        | 2018000005 |
      | Amr.Khalil        | 2018000016 |
