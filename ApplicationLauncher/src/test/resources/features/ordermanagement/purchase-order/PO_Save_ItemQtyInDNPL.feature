# Authors: Yara Ameen

Feature: Save Item Qty In DN/PL in PO

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                    |
      | LogisticsResponsible_Signmedia | POLogisticsOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                     | Condition                      |
      | POLogisticsOwner_Signmedia | PurchaseOrder:AddPL/DNQuantity | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000023 | Cleared            | 0002           |
      | 2018100000 | Draft              | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000029 | OpenForUpdates     | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018100001 | Draft            | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000031 | OpenForUpdates   | 0006           |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000002 | 000003   |         |
      | 2018000005 | 000001   |         |
      | 2018000006 | 000003   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000023 | 000002   | 18000   |
      | 2018000029 | 000012   | 2000.5  |
      | 2018100000 | 000012   | 2000.5  |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000014 | 000003   |         |
      | 2018000015 | 000001   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000031 | 000012   | 2000.5  |
      | 2018100001 | 000012   | 2000.5  |
    And edit session is "30" minutes

  ####### Save Happy Paths
  # EBS-4449
  @ResetData
  Scenario Outline: (01) Save Item QtyInDnPl to Local/Import PurchaseOrder, within edit session by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then the Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" is updated as follows:
      | QtyInDN        | LastUpdatedBy | LastUpdateDate       |
      | <QtyInDnValue> | <User>        | 07-Jan-2019 09:30 AM |
    And the PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | <User>        | 07-Jan-2019 09:30 AM |
    And edit Qty in DN/PL dialog is closed and the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
    # Import
      | User        | POCode     | ItemCode | State              | QtyInDnValue |
      | Gehan.Ahmed | 2018000005 | 000001   | Approved           | 1800000      |
      | Gehan.Ahmed | 2018000006 | 000003   | OpenForUpdates     | 500.5        |
      | Gehan.Ahmed | 2018000007 | 000001   | WaitingApproval    | 70000        |
      | Gehan.Ahmed | 2018000008 | 000001   | Confirmed          | 12000        |
      | Gehan.Ahmed | 2018000009 | 000001   | FinishedProduction | 6342.5       |
      | Gehan.Ahmed | 2018000010 | 000002   | Shipped            | 89932        |
      | Gehan.Ahmed | 2018000011 | 000001   | Arrived            | 12300        |
      | Gehan.Ahmed | 2018000021 | 000002   | Draft              | 6000         |
      | Gehan.Ahmed | 2018000023 | 000002   | Cleared            | 30000        |
    # Local
      | Gehan.Ahmed | 2018000013 | 000001   | Draft              | 1000.5       |
      | Gehan.Ahmed | 2018000014 | 000003   | OpenForUpdates     | 30000        |
      | Gehan.Ahmed | 2018000015 | 000001   | WaitingApproval    | 1800000      |
      | Gehan.Ahmed | 2018000016 | 000001   | Approved           | 400000       |
      | Gehan.Ahmed | 2018000017 | 000001   | Confirmed          | 250          |
      | Gehan.Ahmed | 2018000018 | 000001   | Shipped            | 425          |

  ######## Save Authorization
  # EBS-4449
  Scenario Outline: (2) Save Item QtyInDnPl to Local/Import PurchaseOrder, - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | POCode     | ItemCode | QtyInDnValue |
      | Gehan.Ahmed | 2018000029 | 000012   | 50000        |
      | Gehan.Ahmed | 2018000031 | 000012   | 350.5        |
      | Afaf        | 2018000031 | 000012   | 1800000      |

  ####### Save Validations
  ####### Save Exception Cases
  #EBS-4449
  Scenario Outline: (3) Save Item QtyInDnPl to Local/Import PurchaseOrder, after edit session expire (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:41 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | User        | POCode     | ItemCode | QtyInDnValue |
      | Gehan.Ahmed | 2018000005 | 000001   | 350.5        |
      | Gehan.Ahmed | 2018000013 | 000001   | 1800000      |

  #EBS-4449
  @ResetData
  Scenario Outline: (4) Save Item QtyInDnPl to Local/Import PurchaseOrder, where PO doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     | ItemCode | QtyInDnValue |
      | Gehan.Ahmed | 2018100000 | 000012   | 6000000      |
      | Gehan.Ahmed | 2018100001 | 000012   | 173000       |

  #EBS-4449
  @ResetData
  Scenario Outline: (5) Save Item QtyInDnPl to Local/Import PurchaseOrder, where Item doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | User        | POCode     | ItemCode | QtyInDnValue |
      | Gehan.Ahmed | 2018100000 | 000012   | 6000000      |
      | Gehan.Ahmed | 2018100001 | 000012   | 173000       |

  #EBS-4449
  Scenario Outline: (6) Save Item QtyInDnPl to Local/Import PurchaseOrder, in state where this action is not allowed (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:10 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | ItemCode | State            | QtyInDnValue |
      | 2018000002 | 000003   | DeliveryComplete | 100000       |
      | 2018000012 | 000001   | Cancelled        | 520.5        |
      | 2018000019 | 000001   | DeliveryComplete | 700000       |
      | 2018000020 | 000001   | Cancelled        | 15000        |

  ####### Save Abuse Cases
  #EBS-4449
  Scenario Outline: (7) Save Item QtyInDnPl to Local/Import PurchaseOrder, with Missing mandatory Fields (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:10 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    And edit Qty in DN/PL dialog is closed and the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    And "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
    Examples:
      | POCode     | ItemCode | State            | QtyInDnValue |
      | 2018000002 | 000003   | DeliveryComplete |              |
      | 2018000023 | 000002   | Cleared          |              |
      | 2018000019 | 000001   | DeliveryComplete |              |

  Scenario Outline: (8) Save Item QtyInDnPl to Local/Import PurchaseOrder, with malicious input that violates business rules enforced by the client (Abuse Case)
    Given  user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves Qty in DN/PL of Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:11 AM" with the following values:
      | QtyInDN        |
      | <QtyInDnValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     | ItemCode | QtyInDnValue |
      | 2018000013 | 000001   | -1           |
      | 2018000005 | 000001   | -2.4         |
      | 2018000013 | 000001   | Ay 7aga      |
