# Author: Hend Ahmed,
# Reviewer: Somaya Ahmed, 31-December-2018

Feature: Mark PO as PI Requested from vendor

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                      | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:MarkAsPIRequested | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:MarkAsPIRequested | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:MarkAsPIRequested | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                             |                                 |

    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000025 | IMPORT_PO | Draft              | 0001           |
      | 2018000026 | IMPORT_PO | Draft              | 0006           |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           |
      | 2018000014 | LOCAL_PO  | OpenForUpdates     | 0002           |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           |
      | 2018000018 | LOCAL_PO  | Shipped            | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete   | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled          | 0002           |
      | 2018000027 | LOCAL_PO  | Draft              | 0001           |
      | 2018000028 | LOCAL_PO  | Draft              | 0006           |

    # A draft Import PO with all mandatory feild of OpenForUpdates state: 2018000021 - Signmedia

    And PurchaseOrder with code "2018000021" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000021" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000021" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000021" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000021" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000021" has the following OrderItems section values:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000021" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |


    # A draft Local PO with all mandatory feild of OpenForUpdates state: 2018000013

    And PurchaseOrder with code "2018000013" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000013" has the following Company section values:
      | Company | Bank | AccountNumber       |
      | 0002    | 0003 | 1516171819789 - EGP |
    And PurchaseOrder with code "2018000013" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      | 0001      | 0001  | 0002       |
    And PurchaseOrder with code "2018000013" has the following RequiredDocuments section values:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And PurchaseOrder with code "2018000013" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 |                           | 0002            | 0002        | 0001          |
    And PurchaseOrder with code "2018000013" has the following OrderItems section values:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000001   | Hot Laminated Frontlit Fabric roll  |         | 0019 |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000001" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |
    And Item with code "000002" in PurchaseOrder with code "2018000013" has the following Quantities:
      | Qty   | OrderUnit    | UnitPrice(Gross) | Discount |
      | 100.0 | Roll 2.20x50 | 2.0              | 10.0%    |
      | 50.0  | Roll 2.70x50 | 3.0              | 0.0%     |

    # A draft Import PO with missing Purchase Responsible, Incoterm, Currency, Payment Terms, Shipping Instructions, Collection Date, Mode of Transport, Required Docs, OrderItems

    And PurchaseOrder with code "2018000100" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000100" has the following Company section values:
      | Company | Bank | AccountNumber |
      |         |      |               |
    And PurchaseOrder with code "2018000100" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000100" has an empty RequiredDocuments section
    And PurchaseOrder with code "2018000100" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      |          |          |              |                      |              |                |                             |                           |                 |             |               |
    And PurchaseOrder with code "2018000100" has an empty OrderItems section

    # A draft Local PO with missing Purchase Responsible, Incoterm, Currency, Payment Terms, Collection Date, , Mode of Transport, Required Docs, OrderItems

    And PurchaseOrder with code "2018000101" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000101" has the following Company section values:
      | Company | Bank | AccountNumber |
      |         |      |               |
    And PurchaseOrder with code "2018000101" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000101" has an empty RequiredDocuments section
    And PurchaseOrder with code "2018000101" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      |          |          |              |                      |              |                |                             |                           |                 |             |               |
    And PurchaseOrder with code "2018000101" has an empty OrderItems section

    # A draft Import PO with missing Purchase Responsible, Incoterm, Currency, Payment Terms, Shipping Instructions, Collection Date, Mode of Transport, Required Docs, Quanitities in OrderItems

    And PurchaseOrder with code "2018000102" has the following Header section values:
      | Type      | PurchasingUnit | Vendor | PurchasingResponsible |
      | IMPORT_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000102" has the following Company section values:
      | Company | Bank | AccountNumber |
      |         |      |               |
    And PurchaseOrder with code "2018000102" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000102" has an empty RequiredDocuments section
    And PurchaseOrder with code "2018000102" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      |          |          |              |                      |              |                |                             |                           |                 |             |               |
    And PurchaseOrder with code "2018000102" has the following OrderItems section values:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000102" has an empty Quantities in OrderItems section


    #    # A draft Local PO with missing Purchase Responsible, Incoterm, Currency, Payment Terms, Collection Date, , Mode of Transport, Required Docs, Quanitities in Order Items

    And PurchaseOrder with code "2018000103" has the following Header section values:
      | Type     | PurchasingUnit | Vendor | PurchasingResponsible |
      | LOCAL_PO | 0002           | 000002 | Gehan Ahmed           |
    And PurchaseOrder with code "2018000103" has the following Company section values:
      | Company | Bank | AccountNumber |
      |         |      |               |
    And PurchaseOrder with code "2018000103" has the following Consignee section values:
      | Consignee | Plant | Storehouse |
      |           |       |            |
    And PurchaseOrder with code "2018000103" has an empty RequiredDocuments section
    And PurchaseOrder with code "2018000103" has the following PaymentTerms section values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | CollectionDate_TimePeriod | ModeOfTransport | LoadingPort | DischargePort |
      |          |          |              |                      |              |                |                             |                           |                 |             |               |
    And PurchaseOrder with code "2018000103" has the following OrderItems section values:
      | ItemCode | ItemName                            | QtyInDN | Base |
      | 000002   | Hot Laminated Frontlit Backlit roll |         | 0019 |
    And Item with code "000002" in PurchaseOrder with code "2018000103" has an empty Quantities in OrderItems section

  @ResetData
  Scenario Outline: (01) Mark PO as PI Requested in Draft state (Local/Import) with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "<POCode>" is updated as PIRequested as follows:
      | LastUpdatedBy | LastUpdateDate             | State          | PIRequestDate              |
      | <User>        | 07-Jan-2019 07:30 AM +0000 | OpenForUpdates | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-26"
    Examples:
      | POCode     | State | PurchasingUnit | User        |
      | 2018000021 | Draft | 0002           | Gehan.Ahmed |
      #| 2018000025 | Draft | 0001           | Amr.Khalil  |
      #| 2018000026 | Draft | 0006           | Amr.Khalil  |
      | 2018000013 | Draft | 0002           | Gehan.Ahmed |
      #| 2018000027 | Draft | 0001           | Amr.Khalil  |
      #| 2018000028 | Draft | 0006           | Amr.Khalil  |

  Scenario Outline: (02) Mark PO as PI Requested (Local/Import) in a state that doesn't allow this action - any state except the draft state (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000014 | OpenForUpdates     | 0002           |
      | 2018000015 | WaitingApproval    | 0002           |
      | 2018000016 | Approved           | 0002           |
      | 2018000017 | Confirmed          | 0002           |
      | 2018000018 | Shipped            | 0002           |
      | 2018000019 | DeliveryComplete   | 0002           |
      | 2018000020 | Cancelled          | 0002           |

  Scenario Outline: (03) Mark PO as PI Requested in Draft state (Import/Local) that has at least one section currently locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 9:20 AM"
    When "Gehan.Ahmed" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | POSection         | POCode     |
      | Company           | 2018000021 |
      | Consignee         | 2018000021 |
      | Paymentterms      | 2018000021 |
      | RequiredDocuments | 2018000021 |
      | OrderItems        | 2018000021 |
      | Company           | 2018000013 |
      | Consignee         | 2018000013 |
      | Paymentterms      | 2018000013 |
      | RequiredDocuments | 2018000013 |
      | OrderItems        | 2018000013 |

  @ResetData
  Scenario Outline: (04) Mark PO as PI Requested (Local/Import) that is doesn't exist - i.e. has been deleted by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (05) Mark PO as PI Requested in Draft state (Import) with missing mandatory fields
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-33"
    And the following fields "<Missing Fields>" which sent to "Gehan.Ahmed" are marked as missing
    Examples:
      | POCode     | Missing Fields                                                                                                                                        |
      | 2018000100 | incotermCode , currencyCode , paymentTermCode , shippingInstructionsCode , collectionDate , transportModeCode , RequiredDocs , OrderItemsList         |
      | 2018000101 | incotermCode , currencyCode , paymentTermCode , collectionDate , transportModeCode , RequiredDocs , OrderItemsList                                    |
      | 2018000102 | incotermCode , currencyCode , paymentTermCode , shippingInstructionsCode , collectionDate , transportModeCode , RequiredDocs , QuantitiesPerOrderItem |
      | 2018000103 | incotermCode , currencyCode , paymentTermCode , collectionDate , transportModeCode , RequiredDocs , QuantitiesPerOrderItem                            |

  Scenario Outline: (06) Mark PO as PI Requested in Draft state (Local/Import) with missing or malicious PIRequestDateValue (Abuse case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | <PIRequestDateValue> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | PIRequestDateValue | POCode     |
      |                    | 2018000021 |
      | Ay 7aga            | 2018000021 |
      |                    | 2018000013 |
      | Ay 7aga            | 2018000013 |

  Scenario Outline: (07) Mark PO as PI Requested by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  Scenario Outline: (08) Mark PO as PI Requested for for PO by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to MarkAsPIRequested the PurchaseOrder with Code "<POCode>" at "7-Jan-2019 07:30 AM +0000" with the following values:
      | PIRequestDate        |
      | 06-Jan-2019 11:30 AM |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |
