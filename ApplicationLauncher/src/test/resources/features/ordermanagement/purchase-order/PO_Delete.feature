# Updated by: Zyad Ghorab (18-Mar-2021)
# Reviewer: Shirin Mahmoud

Feature: Delete PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission           | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:Delete | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:Delete | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:Delete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                  |                                 |
    And the following users doesn't have the following permissions:
      | User              | Permission           |
      | Mahmoud.Abdelaziz | PurchaseOrder:Delete |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0003 | Offset     |
      | 0004 | Digital    |
      | 0005 | Textile    |
      | 0006 | Corrugated |
    And the following Vendors exist:
      | Code   | Name              | PurchasingUnit                     |
      | 000051 | Vendor 6          | 0001, 0002, 0003, 0004, 0005, 0006 |
      | 000001 | Siegwerk          | 0001                               |
      | 000002 | Zhejiang          | 0002                               |
      | 000003 | ABC               | 0003                               |
      | 000004 | Vendor Digital    | 0004                               |
      | 000005 | Vendor Textile    | 0005                               |
      | 000006 | Vendor Corrugated | 0006                               |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State                                             | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000004 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000005 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000006 | IMPORT_PO  | Active,InOperation,OpenForUpdates,NotReceived     | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000007 | IMPORT_PO  | Active,InOperation,WaitingApproval,NotReceived    | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Active,InOperation,Confirmed,NotReceived          | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000009 | IMPORT_PO  | Active,InOperation,FinishedProduction,NotReceived | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Active,InOperation,Shipped,NotReceived            | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Active,Arrived,NotReceived                        | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000001 | IMPORT_PO  | Active,Cleared,NotReceived                        | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | Active,DeliveryComplete,Received                  | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO  | Inactive,Cancelled                                | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000026 | IMPORT_PO  | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000024 | LOCAL_PO   | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000013 | LOCAL_PO   | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000014 | LOCAL_PO   | Active,InOperation,OpenForUpdates,NotReceived     | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000015 | LOCAL_PO   | Active,InOperation,WaitingApproval,NotReceived    | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000016 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000017 | LOCAL_PO   | Active,InOperation,Confirmed,NotReceived          | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000018 | LOCAL_PO   | Active,InOperation,Shipped,NotReceived            | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000019 | LOCAL_PO   | Active,DeliveryComplete,Received                  | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000020 | LOCAL_PO   | Inactive,Cancelled                                | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000027 | LOCAL_PO   | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000028 | LOCAL_PO   | Draft                                             | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000052 | LOCAL_PO   | Cleared                                           | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |

      | 2020000004 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Confirmed                                         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000007 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000004 | 0003 - Offset     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000005 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000006 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000007 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000008 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000009 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000010 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000011 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000001 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000002 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000012 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000024 | 0003 - Offset     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000014 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000015 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000016 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000017 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000018 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000019 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000020 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000027 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000028 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000052 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000007 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor                     | ReferencePO |
      | 2018000004 | 000003 - ABC               |             |
      | 2018000021 | 000002 - Zhejiang          |             |
      | 2018000005 | 000002 - Zhejiang          |             |
      | 2018000006 | 000002 - Zhejiang          |             |
      | 2018000007 | 000002 - Zhejiang          |             |
      | 2018000008 | 000002 - Zhejiang          |             |
      | 2018000009 | 000002 - Zhejiang          |             |
      | 2018000010 | 000002 - Zhejiang          |             |
      | 2018000011 | 000002 - Zhejiang          |             |
      | 2018000001 | 000002 - Zhejiang          |             |
      | 2018000002 | 000002 - Zhejiang          |             |
      | 2018000012 | 000002 - Zhejiang          |             |
      | 2018000025 | 000001 - Siegwerk          |             |
      | 2018000026 | 000006 - Vendor Corrugated |             |
      | 2018000024 | 000003 - ABC               |             |
      | 2018000013 | 000002 - Zhejiang          |             |
      | 2018000014 | 000002 - Zhejiang          |             |
      | 2018000015 | 000002 - Zhejiang          |             |
      | 2018000016 | 000002 - Zhejiang          |             |
      | 2018000017 | 000002 - Zhejiang          |             |
      | 2018000018 | 000002 - Zhejiang          |             |
      | 2018000019 | 000002 - Zhejiang          |             |
      | 2018000020 | 000002 - Zhejiang          |             |
      | 2018000027 | 000001 - Siegwerk          |             |
      | 2018000028 | 000006 - Vendor Corrugated |             |
      | 2020000052 | 000001 - Siegwerk          |             |

      | 2020000004 | 000002 - Zhejiang          | 2018000001  |
      | 2020000006 | 000002 - Zhejiang          | 2018000018  |
      | 2020000007 | 000001 - Siegwerk          | 2020000052  |


  Scenario Outline:(01) Delete PurchaseOrder, draft by an authorized user: user with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete PurchaseOrder with code "<POCode>"
    Then PurchaseOrder with code "<POCode>" is deleted from the system
    And a success notification is sent to "<User>" with the following message "Gen-msg-12"
    Examples:
      | POCode     | User        |
      | 2018000013 | Gehan.Ahmed |
      | 2018000021 | Gehan.Ahmed |
      | 2018000025 | Amr.Khalil  |
      | 2018000026 | Amr.Khalil  |
      | 2018000027 | Amr.Khalil  |
      | 2018000028 | Amr.Khalil  |
      | 2020000004 | Gehan.Ahmed |

  Scenario Outline: (02) Delete PurchaseOrder, in a state that doesn't allow delete: All states except draft (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-13"
    Examples:
      | POCode     |
      | 2018000005 |
      | 2018000006 |
      | 2018000007 |
      | 2018000008 |
      | 2018000009 |
      | 2018000010 |
      | 2018000011 |
      | 2018000001 |
      | 2018000002 |
      | 2018000012 |
      | 2018000014 |
      | 2018000015 |
      | 2018000016 |
      | 2018000017 |
      | 2018000018 |
      | 2018000019 |
      | 2018000020 |
      | 2020000006 |

  Scenario Outline: (03) Delete PurchaseOrder, draft, where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |
      | 2020000004 |

  Scenario Outline: (04) Delete PurchaseOrder, draft that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with code "<POCode>" in edit mode
    When "Gehan.Ahmed" requests to delete PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | POSection         | POCode     |
      | Company           | 2018000013 |
      | Consignee         | 2018000013 |
      | Paymentterms      | 2018000013 |
      | RequiredDocuments | 2018000013 |
      | Company           | 2018000021 |
      | Consignee         | 2018000021 |
      | Paymentterms      | 2018000021 |
      | RequiredDocuments | 2018000021 |
  #| OrderItems        | 2018000013 |
  #| OrderItems        | 2018000013 |
      | Company           | 2020000004 |

  Scenario Outline: (05) Delete PurchaseOrder, draft by an unauthorized user (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete PurchaseOrder with code "<POCode>"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000013 |
      | 2018000021 |
      | 2020000004 |

  Scenario Outline: (06) Delete PurchaseOrder, draft by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete PurchaseOrder with code "<POCode>"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000004 |
      | 2018000024 |
      | 2020000007 |
