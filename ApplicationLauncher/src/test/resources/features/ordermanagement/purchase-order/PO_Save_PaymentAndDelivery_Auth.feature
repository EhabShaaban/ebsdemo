# Author: Hend Aboulwafa
# Analysis Reviewer : Somaya Aboulwafa, 22-Nov-2018, 5:30 pm
# Quality Reviewer:

Feature: Save Authorization PaymentAndDelivery section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name                              | Role                                                               |
      | Shady.Abdelatif                   | Accountant_Signmedia                                               |
      | Gehan.Ahmed.NoIncoterm            | PurchasingResponsible_Signmedia_CannotViewIncotermRole             |
      | Gehan.Ahmed.NoCurrency            | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             |
      | Gehan.Ahmed.NoPaymentTerm         | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         |
      | Gehan.Ahmed.NoShippingInstruction | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole |
      | Gehan.Ahmed.NoContainersType      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       |
      | Gehan.Ahmed.NoModeOfTransport     | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      |
      | Gehan.Ahmed.NoPort                | PurchasingResponsible_Signmedia_CannotViewPortRole                 |
      | Amr.Khalil                        | PurchasingResponsible_Flexo                                        |
    And the following roles and sub-roles exist:
      | Role                                                               | Subrole                   |
      | PurchasingResponsible_Flexo                                        | POOwner_Flexo             |
      | PurchasingResponsible_Flexo                                        | IncotermViewer            |
      | PurchasingResponsible_Flexo                                        | CurrencyViewer            |
      | PurchasingResponsible_Flexo                                        | PaymentTermViewer         |
      | PurchasingResponsible_Flexo                                        | ShippingInstructionViewer |
      | PurchasingResponsible_Flexo                                        | ContainerTypeViewer       |
      | PurchasingResponsible_Flexo                                        | ModeofTransportViewer     |
      | PurchasingResponsible_Flexo                                        | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewIncotermRole             | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole             | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole         | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewShippingInstructionsRole | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | ModeofTransportViewer     |
      | PurchasingResponsible_Signmedia_CannotViewContainersTypeRole       | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewModeOfTransportRole      | PortViewer                |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | POOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | IncotermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | CurrencyViewer            |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | PaymentTermViewer         |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ShippingInstructionViewer |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ContainerTypeViewer       |
      | PurchasingResponsible_Signmedia_CannotViewPortRole                 | ModeofTransportViewer     |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                       | Condition                      |
      | POOwner_Flexo             | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Flexo']     |
      | POOwner_Signmedia         | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
      | IncotermViewer            | Incoterm:ReadAll                 |                                |
      | CurrencyViewer            | Currency:ReadAll                 |                                |
      | PaymentTermViewer         | PaymentTerms:ReadAll             |                                |
      | ShippingInstructionViewer | ShippingInstructions:ReadAll     |                                |
      | ContainerTypeViewer       | ContainerType:ReadAll            |                                |
      | ModeofTransportViewer     | ModeOfTransport:ReadAll          |                                |
      | PortViewer                | Port:ReadAll                     |                                |
    And the following Import PurchaseOrders exist:
      | Code       | Type      | State | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000021 | IMPORT_PO | Draft | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 | 0002            | 0002        | 0001          |
    And the following Local PurchaseOrders exist:
      | Code       | Type     | State | PurchasingUnit | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 2018000013 | LOCAL_PO | Draft | 0002           | 0002     | 0002     | 0002         | 0002                 | 10           | 0002           | 06-Oct-2020                 | 0002            | 0002        | 0001          |
    And the following Incoterms exist:
      | Code | Name |
      | 0001 | FOB  |
    And the following Currencies exist:
      | Code | Name           |
      | 0001 | Egyptian Pound |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
    And the following ShippingInstructions exist:
      | Code | Name               |
      | 0001 | Ship by Shecartoon |
    And the following ContainerTypes exist:
      | Code | Name |
      | 0001 | FCL  |
    And the following ModeOfTransports exist:
      | Code | Name   |
      | 0001 | By Air |
    And the following Ports exist:
      | Code | Name                | Country |
      | 0001 | Alexandria old port | Egypt   |
      | 0002 | Damietta            | Egypt   |
    And edit session is "30" minutes

  # Save section by a user who is not authorized to save
  # EBS-2205
  Scenario Outline: (01) Save PaymentandDelivery section by an unauthorized users (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves PaymentandDelivery section of PurchaseOrder with Code "<POCode>" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 05-Aug-2019                 | 0001            | 0001        | 0002          |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-2205
  Scenario Outline: (02) Save PaymentandDelivery section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves PaymentandDelivery section of PurchaseOrder with Code "<POCode>" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 05-Aug-2019                 | 0001            | 0001        | 0002          |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  ######## Save section using values which the user is not authorized to read
  # EBS-2205
  Scenario Outline:: (03) Save PaymentandDelivery section by User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given user is logged in as "<User>"
    And PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "7-Jan-2019 9:10 am"
    When "<User>" saves PaymentandDelivery section of PurchaseOrder with Code "<POCode>" with the following values:
      | Incoterm | Currency | PaymentTerms | ShippingInstructions | ContainersNo | ContainersType | CollectionDate_SpecificDate | ModeOfTransport | LoadingPort | DischargePort |
      | 0001     | 0001     | 0001         | 0001                 | 2            | 0001           | 05-Aug-2019                 | 0001            | 0001        | 0002          |
    Then the lock by "<User>" on PaymentandDelivery section of PurchaseOrder with Code "<POCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                              | POCode     |
      | Gehan.Ahmed.NoIncoterm            | 2018000021 |
      | Gehan.Ahmed.NoIncoterm            | 2018000013 |
      | Gehan.Ahmed.NoCurrency            | 2018000021 |
      | Gehan.Ahmed.NoCurrency            | 2018000013 |
      | Gehan.Ahmed.NoPaymentTerm         | 2018000021 |
      | Gehan.Ahmed.NoPaymentTerm         | 2018000013 |
      | Gehan.Ahmed.NoShippingInstruction | 2018000021 |
      | Gehan.Ahmed.NoShippingInstruction | 2018000013 |
      | Gehan.Ahmed.NoContainersType      | 2018000021 |
      | Gehan.Ahmed.NoContainersType      | 2018000013 |
      | Gehan.Ahmed.NoModeOfTransport     | 2018000021 |
      | Gehan.Ahmed.NoModeOfTransport     | 2018000013 |
      | Gehan.Ahmed.NoPort                | 2018000021 |
      | Gehan.Ahmed.NoPort                | 2018000013 |