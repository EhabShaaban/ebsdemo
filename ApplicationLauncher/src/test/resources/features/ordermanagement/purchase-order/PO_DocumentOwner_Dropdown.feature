#Author: Eman Mansour, Sherien
Feature: ViewAll DocumentOwners Dropdown

  Background:
    Given the following users and roles exist:
      | Name            | Role                            |
      | Gehan.Ahmed     | PurchasingResponsible_Signmedia |
      | Lama.Maher      | LogisticsResponsible_Signmedia  |
      | Shady.Abdelatif | Accountant_Signmedia            |
      | Afaf            | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | PODocumentOwner     |
      | LogisticsResponsible_Signmedia  | PODocumentOwner     |
      | Accountant_Signmedia            | PODocumentOwner     |
      | PurchasingResponsible_Signmedia | DocumentOwnerViewer |
      | LogisticsResponsible_Signmedia  | DocumentOwnerViewer |
      | Accountant_Signmedia            | DocumentOwnerViewer |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                       | Condition |
      | DocumentOwnerViewer | DocumentOwner:ReadAll            |           |
      | PODocumentOwner     | PurchaseOrder:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition |
      | 37 | Gehan Ahmed     | PurchaseOrder  | CanBeDocumentOwner |           |
      | 38 | Lama Maher      | PurchaseOrder  | CanBeDocumentOwner |           |
      | 40 | Shady.Abdelatif | PurchaseOrder  | CanBeDocumentOwner |           |
    And the total number of existing "PurchaseOrder" DocumentOwners are 3

  Scenario: (01) Read list of DocumentOwners dropdown, by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all DocumentOwners for "PurchaseOrder"
    Then the following DocumentOwners values will be presented to "Gehan.Ahmed":
      | Name            |
      | Gehan Ahmed     |
      | Lama Maher      |
      | Shady.Abdelatif |
    And total number of DocumentOwners returned to "Gehan.Ahmed" in a dropdown is equal to 3

  Scenario: (02) Read list of DocumentOwners dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "PurchaseOrder"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
