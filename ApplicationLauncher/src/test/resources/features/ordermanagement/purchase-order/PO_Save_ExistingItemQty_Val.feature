Feature: Save Edit Item Quantities in PO Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole  |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission               | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole  | *:*                      |                                |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000049 | Cleared            | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State           | PurchasingUnit |
      | 2018000013 | Draft           | 0002           |
      | 2018000015 | WaitingApproval | 0002           |
      | 2018000016 | Approved        | 0002           |
      | 2018000017 | Confirmed       | 0002           |
      | 2018000046 | Shipped         | 0002           |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000006 | 000003   | 46    |
      | 2018000021 | 000002   | 18    |
      | 2018000005 | 000001   | 50    |
      | 2018000007 | 000001   | 51    |
      | 2018000008 | 000001   | 52    |
      | 2018000009 | 000001   | 53    |
      | 2018000010 | 000002   | 54    |
      | 2018000011 | 000001   | 55    |
      | 2018000049 | 000001   | 37    |
      # Local
      | 2018000013 | 000001   | 20    |
      | 2018000015 | 000001   | 57    |
      | 2018000016 | 000001   | 58    |
      | 2018000017 | 000001   | 59    |
      | 2018000046 | 000001   | 30    |
    And the following Measures exist:
      | Code | Type     | Symbol        | Name          |
      | 0029 | Business | Roll 2.20x50  | Roll 2.20x50  |
      | 0032 | Business | Roll 3.20x50  | Roll 3.20x50  |
      | 0033 | Business | Roll 2.70x50  | Roll 2.70x50  |
      | 0034 | Business | Roll 1.07x50  | Roll 1.07x50  |
      | 0035 | Business | Roll 1.27x50  | Roll 1.27x50  |
      | 0036 | Business | Drum-10Kg     | Drum-10Kg     |
      | 0037 | Business | Drum-50Kg     | Drum-50Kg     |
      | 0042 | Business | Bottle-2Liter | Bottle-2Liter |
    And edit session is "30" minutes

  #EBS-4031
  Scenario Outline: (01) Save Edit Item Quantity to Local/Import PurchaseOrder Val, after edit session expire (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:41 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | POCode     | ItemCode | State          | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000006 | 000003   | OpenForUpdates | Gehan.Ahmed | Import | 46    | 700.0    | 0036           | 4.0            | 5.0%          |
      # Local
      | 2018000013 | 000001   | Draft          | Gehan.Ahmed | Local  | 20    | 800.0    | 0032           | 20.0           | 30.0%         |

  #EBS-4031
  @ResetData
  Scenario Outline: (02) Save Edit Item Quantity to Local/Import PurchaseOrder Val, where PO doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | POCode     | ItemCode | State | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    | 0033           | 100.0          | 0.0%          |
      # Local
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 800.0    | 0032           | 20.0           | 30.0%         |

  #EBS-4031
  @ResetData
  Scenario Outline: (03) Save Edit Item Quantity to Local/Import PurchaseOrder Val, where Item doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode | State | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    | 0033           | 100.0          | 0.0%          |
      # Local
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 800.0    | 0032           | 20.0           | 30.0%         |

  #EBS-4031
  Scenario Outline: (04) Save Edit Item Quantity to Local/Import PurchaseOrder Val, in state where this action is not allowed (Exception)
    Given user is logged in as "<User>"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:10 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | POCode     | ItemCode | State              | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000005 | 000001   | Approved           | Gehan.Ahmed | Import | 50    | 700.0    | 0037           | 4.0            | 5.0%          |
      | 2018000007 | 000001   | WaitingApproval    | Gehan.Ahmed | Import | 51    | 300.0    | 0037           | 100.0          | 0.0%          |
      | 2018000008 | 000001   | Confirmed          | Gehan.Ahmed | Import | 52    | 200.5    | 0037           | 33.0           | 11.0%         |
      | 2018000009 | 000001   | FinishedProduction | Gehan.Ahmed | Import | 53    | 150.0    | 0037           | 10.5           | 20.0%         |
      | 2018000010 | 000002   | Shipped            | Gehan.Ahmed | Import | 54    | 800.0    | 0037           | 20.0           | 30.0%         |
      | 2018000011 | 000001   | Arrived            | Gehan.Ahmed | Import | 55    | 45.5     | 0037           | 5.0            | 2.0%          |
      | 2018000049 | 000001   | Cleared            | Gehan.Ahmed | Import | 37    | 122.0    | 0033           | 6.0            | 7.0%          |
      # Local
      | 2018000015 | 000001   | WaitingApproval    | Gehan.Ahmed | Local  | 57    | 800.0    | 0037           | 20.0           | 30.0%         |
      | 2018000016 | 000001   | Approved           | Gehan.Ahmed | Local  | 58    | 45.5     | 0037           | 5.0            | 2.0%          |
      | 2018000017 | 000001   | Confirmed          | Gehan.Ahmed | Local  | 59    | 122.0    | 0037           | 6.0            | 7.0%          |
      | 2018000046 | 000001   | Shipped            | Gehan.Ahmed | Local  | 30    | 900.0    | 0029           | 30.5           | 4.0%          |

  #EBS-4031
  Scenario Outline: (05) Save Edit Item Quantity to Local/Import PurchaseOrder Val, with Missing mandatory Fields (Abuse Case)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    And edit Quantity dialog is closed and the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | ItemCode | State | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    |          | 0033           | 100.0          | 0.0%          |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    |                | 100.0          | 0.0%          |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    | 0033           |                | 0.0%          |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    | 0033           | 100.0          |               |
      # Local
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    |          | 0032           | 20.0           | 30.0%         |
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 800.0    |                | 20.0           | 30.0%         |
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 800.0    | 0032           |                | 30.0%         |
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 800.0    | 0032           | 20.0           |               |

  #EBS-4031
  Scenario Outline: (06) Save Edit Item Quantity to Local/Import PurchaseOrder Val, with malicious input that violates business rules enforced by the client (Abuse Case)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    And edit Quantity dialog is closed and the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | ItemCode | State | User        | Type   | QtyID | QtyValue       | OrderUnitValue | UnitPriceValue | DiscountValue  |
      # Import
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | -1             | 0033           | 100.0          | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | -2.4           | 0033           | 100.0          | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 10000000000001 | 0033           | 100.0          | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | ay 7aga        | 0033           | 100.0          | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | -1             | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | -2.4           | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 10000000000001 | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | ay 7aga        | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 100.1234567    | 0.0%           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 100.0          | -1             |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 100.0          | -2.4           |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 100.0          | 10000000000001 |
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 19    | 800.0          | 0033           | 100.0          | ay 7aga        |

  #EBS-4031
  Scenario Outline: (07) Save Edit Item Quantity to Local/Import PurchaseOrder Val, with order unit that does not have an IVR or deleted (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to orderUnitCode field "PO-msg-52" and sent to "<User>"
    Examples:
      | POCode     | ItemCode | State | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000021 | 000002   | Draft | Gehan.Ahmed | Import | 18    | 300.0    | 0037           | 100.0          | 0.0%          |
      # Local
      | 2018000013 | 000001   | Draft | Gehan.Ahmed | Local  | 20    | 300.0    | 0042           | 20.0           | 30.0%         |
