Feature: Save RequiredDocuments section - Happy Path

  Overview: Save Happy Paths cover he following cases:
  - Saving editable fields in states where Edit action is allowed
  - Saving without optional fields in states where Edit action is allowed
  - Saving without disabled fields even when they are supplied by user, in states where Edit action is allowed ==> No applied here

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole           |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                            | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000024 | Draft            | 0003           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
    And the PurchaseOrder with code "2018000021" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000006" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000005" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000008" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000009" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000010" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000011" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000001" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000013" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000014" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000016" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000017" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the PurchaseOrder with code "2018000018" has the following RequiredDocuments data:
      | DocumentType | Copies |
      | 0005         | 5      |
      | 0004         | 4      |
    And the following RequiredDocuments exist:
      | Code | Name                             |
      | 0001 | Performa Invoice                 |
      | 0002 | Commercial Invoice               |
      | 0004 | Bill of Lading (BL)              |
      | 0005 | Packing List for the Whole Order |
    And edit session is "30" minutes

  ####### Save RequiredDocuments section (Happy Path) ##############################################################
  @ResetData
  Scenario Outline: (01) Save RequiredDocument section within edit session: saving all fields in all editable states (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies |
      | 0001         | 1      |
      | 0002         | 2      |
      | 0004         | 8      |
    Then PurchaseOrder with Code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate      | State   |
      | Gehan.Ahmed   | 07-Jan-2019 9:30 AM | <State> |
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>"  is updated as follows:
      | DocumentType | Copies |
      | 0001         | 1      |
      | 0002         | 2      |
      | 0004         | 8      |
    And the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | POCode     | State              |
      | 2018000021 | Draft              |
      | 2018000006 | OpenForUpdates     |
      | 2018000005 | Approved           |
      | 2018000008 | Confirmed          |
      | 2018000009 | FinishedProduction |
      | 2018000010 | Shipped            |
      | 2018000011 | Arrived            |
      | 2018000001 | Cleared            |
      | 2018000013 | Draft              |
      | 2018000014 | OpenForUpdates     |
      | 2018000016 | Approved           |
      | 2018000017 | Confirmed          |
      | 2018000018 | Shipped            |

  @ResetData
  Scenario Outline: (02) Save RequiredDocument section within edit session: saving without optinal fields in Draft & OpenForUpdates (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with empty values
    Then PurchaseOrder with Code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate      | State   |
      | Gehan.Ahmed   | 07-Jan-2019 9:30 AM | <State> |
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>"  is empty
    And the lock by "Gehan.Ahmed" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | POCode     | State |
      | 2018000021 | Draft |
      | 2018000013 | Draft |
