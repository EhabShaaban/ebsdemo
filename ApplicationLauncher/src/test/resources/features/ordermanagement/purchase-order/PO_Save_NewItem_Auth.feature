@Authorization
Feature: Save Add Item Save_Authorization In OrderItems section in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name                | Role                                                |
      | Amr.Khalil          | PurchasingResponsible_Flexo                         |
      | Amr.Khalil          | PurchasingResponsible_Corrugated                    |
      | Gehan.Ahmed.NoItems | PurchasingResponsible_Signmedia_CannotReadItemsRole |
    And the following roles and sub-roles exist:
      | Role                                                | Subrole              |
      | PurchasingResponsible_Flexo                         | POOwner_Flexo        |
      | PurchasingResponsible_Flexo                         | ItemOwner_Flexo      |
      | PurchasingResponsible_Corrugated                    | POOwner_Corrugated   |
      | PurchasingResponsible_Corrugated                    | ItemOwner_Corrugated |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole | POOwner_Signmedia    |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission               | Condition                       |
      | POOwner_Flexo        | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated   | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | ItemOwner_Flexo      | Item:ReadAll             | [purchaseUnitName='Flexo']      |
      | ItemOwner_Corrugated | Item:ReadAll             | [purchaseUnitName='Corrugated'] |
    And the following Import PurchaseOrders exist:
      | Code       | State | Type   | PurchasingUnit | Vendor |
      | 2018000021 | Draft | Import | 0002           | 000002 |
    And the following Local PurchaseOrders exist:
      | Code       | State | Type  | PurchasingUnit | Vendor |
      | 2018000013 | Draft | Local | 0002           | 000002 |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000021 | 000002   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |

# Save section by a user who is not authorized to save due to condition
  Scenario Outline: (01) Save Add Item In PurchaseOrder, by an unauthorized user due to condition (Abuse Case)
    Given  user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves the following item to the OrderItems of PurchaseOrder with code "<POCode>" with the following values:
      | ItemCode | QtyInDN |
      | 000004   | 100     |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

# Save section using values which the user is not authorized to read
  Scenario: (02) Save Add Item In PurchaseOrder, User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given  user is logged in as "Gehan.Ahmed.NoItems"
    When "Gehan.Ahmed.NoItems" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" with the following values:
      | ItemCode | QtyInDN |
      | 000004   | 100     |
    Then "Gehan.Ahmed.NoItems" is logged out
    And "Gehan.Ahmed.NoItems" is forwarded to the error page

# Save section by a user who is not authorized to save
  Scenario: (03) Save Add Item In PurchaseOrder, by an unauthorized users due to condition (Abuse Case)
    Given  user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves the following item to the OrderItems of PurchaseOrder with code "2018000021" with the following values:
      | ItemCode | QtyInDN |
      | 000004   | 100     |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page