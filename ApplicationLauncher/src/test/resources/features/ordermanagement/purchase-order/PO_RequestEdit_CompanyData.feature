# Reviewer: Aya and Shrouk (10-Feb-2019)
# Reviewer: Hend and Somaya (10-Feb-2019)
# updated by: Fatma Al Zahraa (EBS-7525)

Feature: PO - Request Edit Company

  Background:
    Given the following users and roles exist:
      | Name                                    | Role                                                      |
      | hr1                                     | SuperUser                                                 |
      # 1st: One user with one role with different conditions
      | Amr.Khalil                              | PurchasingResponsible_Flexo                               |
      | Amr.Khalil                              | PurchasingResponsible_Corrugated                          |
      # 2nd: One user with one different role
      | Gehan.Ahmed                             | LogisticsResponsible_Signmedia                            |
      | Shady.Abdelatif.NoCompanyNoBusinessUnit | Accountant_Signmedia_CannotViewCompanyAndBusinessUnitRole |
    And the following roles and sub-roles exist:
      | Role                                                      | Subrole                  |
      | SuperUser                                                 | SuperUserSubRole         |
      | PurchasingResponsible_Flexo                               | POOwner_Flexo            |
      | PurchasingResponsible_Flexo                               | CompanyViewer            |
      | PurchasingResponsible_Flexo                               | PurUnitReader_Flexo      |
      | PurchasingResponsible_Corrugated                          | POOwner_Corrugated       |
      | PurchasingResponsible_Corrugated                          | CompanyViewer            |
      | PurchasingResponsible_Corrugated                          | PurUnitReader_Corrugated |
      | LogisticsResponsible_Signmedia                            | POOwner_Signmedia        |
      | LogisticsResponsible_Signmedia                            | CompanyViewer            |
      | LogisticsResponsible_Signmedia                            | PurUnitReader_Signmedia  |
      | Accountant_Signmedia_CannotViewCompanyAndBusinessUnitRole | POOwner_Signmedia        |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                   | Condition                       |
      | SuperUserSubRole         | *:*                          |                                 |
      | POOwner_Flexo            | PurchaseOrder:UpdateCompany  | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated       | PurchaseOrder:UpdateCompany  | [purchaseUnitName='Corrugated'] |
      | POOwner_Signmedia        | PurchaseOrder:UpdateCompany  | [purchaseUnitName='Signmedia']  |
      | CompanyViewer            | Company:ReadAll              |                                 |
      | CompanyViewer            | Company:ReadBankDetails      |                                 |
      | PurUnitReader_Flexo      | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']      |
      | PurUnitReader_Corrugated | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Corrugated'] |
      | PurUnitReader_Signmedia  | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']  |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                  | Condition                      |
      | Amr.Khalil | PurchaseOrder:UpdateCompany | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                                    | Permission                   |
      | Shady.Abdelatif.NoCompanyNoBusinessUnit | PurchasingUnit:ReadAll_ForPO |
      | Shady.Abdelatif.NoCompanyNoBusinessUnit | Company:ReadAll              |
      | Shady.Abdelatif.NoCompanyNoBusinessUnit | Company:ReadBankDetails      |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State                                             | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000001 | IMPORT_PO  | Active,Cleared,NotReceived                        | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000002 | IMPORT_PO  | Active,DeliveryComplete,NotReceived               | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000005 | IMPORT_PO  | Active,InOperation,Approved,NotReceived           | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000006 | IMPORT_PO  | Active,InOperation,OpenForUpdates,NotReceived     | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000007 | IMPORT_PO  | Active,InOperation,WaitingApproval,NotReceived    | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000008 | IMPORT_PO  | Active,InOperation,Confirmed,NotReceived          | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000009 | IMPORT_PO  | Active,InOperation,FinishedProduction,NotReceived | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO  | Active,InOperation,Shipped,NotReceived            | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO  | Active,Arrived,NotReceived                        | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO  | Inactive,Cancelled                                | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000026 | IMPORT_PO  | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000013 | LOCAL_PO   | Draft                                             | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000014 | LOCAL_PO   | Active,InOperation,OpenForUpdates,NotReceived     | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000015 | LOCAL_PO   | Active,InOperation,WaitingApproval,NotReceived    | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000016 | LOCAL_PO   | Active,InOperation,Approved,NotReceived           | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000017 | LOCAL_PO   | Active,InOperation,Confirmed,NotReceived          | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000018 | LOCAL_PO   | Active,InOperation,Shipped,NotReceived            | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO   | Active,DeliveryComplete,NotReceived               | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000020 | LOCAL_PO   | Inactive,Cancelled                                | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft                                             | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber                         |
      | 2018000001 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000002 | 0002 - Signmedia  | 0001 - AL Madina | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000005 | 0002 - Signmedia  | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000006 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000007 | 0002 - Signmedia  |                  |                                           |
      | 2018000008 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000009 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000010 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000011 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000012 | 0002 - Signmedia  | 0002 - DigiPro   |                                           |
      | 2018000021 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000025 | 0001 - Flexo      | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000026 | 0006 - Corrugated | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000013 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000014 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000015 | 0002 - Signmedia  | 0002 - DigiPro   |                                           |
      | 2018000016 | 0002 - Signmedia  | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000017 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000018 | 0002 - Signmedia  | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank           |
      | 2018000019 | 0002 - Signmedia  | 0002 - DigiPro   | 2021222324253 - USD - CTBC Bank CO., LTD. |
      | 2018000020 | 0002 - Signmedia  | 0002 - DigiPro   |                                           |
      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank           |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
    And edit session is "30" minutes

  ############# Request to open company in edit mode ######################################################
  #EBS-1373
  #EBS-7525
  Scenario Outline: (01) Request to edit Company section: for all states except WaitingApproval & Cancelled - for Import & Local POs (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then Company section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads       |
      | ReadAllCompany        |
      | ReadBankAccountNumber |
      | ReadBusinessUnits     |
    And there are no mandatory fields returned to "<User>"
    And the following editable fields are returned to "<User>":
      | EditableFields    |
      | companyCode       |
      | bankAccountNumber |
    Examples:
      | POCode     | User        | Type      | State              | BusinessUnit      |
      # User with different role
      | 2018000021 | Gehan.Ahmed | IMPORT_PO | Draft              | 0002 - Signmedia  |
      | 2018000006 | Gehan.Ahmed | IMPORT_PO | OpenForUpdates     | 0002 - Signmedia  |
      | 2018000005 | Gehan.Ahmed | IMPORT_PO | Approved           | 0002 - Signmedia  |
      | 2018000008 | Gehan.Ahmed | IMPORT_PO | Confirmed          | 0002 - Signmedia  |
      | 2018000009 | Gehan.Ahmed | IMPORT_PO | FinishedProduction | 0002 - Signmedia  |
      | 2018000010 | Gehan.Ahmed | IMPORT_PO | Shipped            | 0002 - Signmedia  |
      | 2018000011 | Gehan.Ahmed | IMPORT_PO | Arrived            | 0002 - Signmedia  |
      | 2018000001 | Gehan.Ahmed | IMPORT_PO | Cleared            | 0002 - Signmedia  |
      | 2018000002 | Gehan.Ahmed | IMPORT_PO | DeliveryComplete   | 0002 - Signmedia  |
      # User with different role
      | 2018000013 | Gehan.Ahmed | LOCAL_PO  | Draft              | 0002 - Signmedia  |
      | 2018000014 | Gehan.Ahmed | LOCAL_PO  | OpenForUpdates     | 0002 - Signmedia  |
      | 2018000016 | Gehan.Ahmed | LOCAL_PO  | Approved           | 0002 - Signmedia  |
      | 2018000017 | Gehan.Ahmed | LOCAL_PO  | Confirmed          | 0002 - Signmedia  |
      | 2018000018 | Gehan.Ahmed | LOCAL_PO  | Shipped            | 0002 - Signmedia  |
      | 2018000019 | Gehan.Ahmed | LOCAL_PO  | DeliveryComplete   | 0002 - Signmedia  |
      # User with role with different conditions
      | 2018000025 | Amr.Khalil  | IMPORT_PO | Draft              | 0001 - Flexo      |
      | 2018000026 | Amr.Khalil  | IMPORT_PO | Draft              | 0006 - Corrugated |

  #EBS-7525
  Scenario Outline: (02) Request to edit Company section: for all states in Service POs (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then Company section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads       |
      | ReadAllCompany        |
      | ReadBankAccountNumber |
      | ReadBusinessUnits     |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields    |
      | bankAccountNumber |
    Examples:
      | POCode     | User        | State | BusinessUnit     |
      # User with different role
      | 2020000004 | Gehan.Ahmed | Draft | 0002 - Signmedia |

  # EBS-1373
  Scenario Outline: (03) Request to edit Company section: User with one or more role - All states except WaitingApproval & Cancelled - for Import & Local POs (Authorization)
    Given user is logged in as "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    When "Shady.Abdelatif.NoCompanyNoBusinessUnit" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then Company section of PurchaseOrder with code "<POCode>" becomes in the edit mode and locked by "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And there are no authorized reads returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And there are no mandatory fields returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And the following editable fields are returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit":
      | EditableFields    |
      | companyCode       |
      | bankAccountNumber |
    Examples:
      | POCode     | Type      | State |
      | 2018000013 | IMPORT_PO | Draft |
      | 2018000021 | LOCAL_PO  | Draft |

  # EBS-7525
  Scenario: (04) Request to edit Company section: for all states in Service POs (Authorization)
    Given user is logged in as "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    When "Shady.Abdelatif.NoCompanyNoBusinessUnit" requests to edit Company section of PurchaseOrder with code "2020000004"
    Then Company section of PurchaseOrder with code "2020000004" becomes in the edit mode and locked by "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And there are no authorized reads returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And there are no mandatory fields returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit"
    And the following editable fields are returned to "Shady.Abdelatif.NoCompanyNoBusinessUnit":
      | EditableFields    |
      | bankAccountNumber |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (05) Request to edit Company section when section is locked by another user: for all PO types (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened Company section of PurchaseOrder with code "<POCode>" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     | Type       | State |
      | 2018000013 | IMPORT_PO  | Draft |
      | 2018000021 | LOCAL_PO   | Draft |
      | 2020000004 | SERVICE_PO | Draft |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (06) Request to edit Company section of a PurchaseOrder that doesn't exist: for all PO types  (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | Type       | State |
      | 2018000013 | IMPORT_PO  | Draft |
      | 2018000021 | LOCAL_PO   | Draft |
      | 2020000004 | SERVICE_PO | Draft |

  # EBS-1373
  Scenario Outline: (07) Request to edit Company section: action is not allowed in current state: Waiting Approval & Cancelled for [Local/Import] PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And Company section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     | Type      |
      | 2018000007 | IMPORT_PO |
      | 2018000012 | IMPORT_PO |
      | 2018000015 | LOCAL_PO  |
      | 2018000020 | LOCAL_PO  |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (08) Request to edit Company section: by an unauthorized user due to condition - for all PO types (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to edit Company section of PurchaseOrder with code "<POCode>"
    Then "Amr.Khalil" is logged out
    And  "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     | Type       | State | BusinessUnit     |
      | 2018000013 | IMPORT_PO  | Draft | 0002 - Signmedia |
      | 2018000021 | LOCAL_PO   | Draft | 0002 - Signmedia |
      | 2020000004 | SERVICE_PO | Draft | 0002 - Signmedia |

  ########## Cancel saving Company  ########################################################################
  # EBS-1373
  Scenario Outline: (09) Cancel Request to edit Company section: within the edit session - for all states in all PO types (Happy Path)
    Given user is logged in as "<User>"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "<User>" at "07-Jan-2019 9:10 AM"
    When "<User>" cancels saving Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM"
    Then the lock by "<User>" on Company section of PurchaseOrder with Code "<POCode>" is released
    Examples:
      | POCode     | User        | Type      | State              | BusinessUnit      |
      # User with different role
      | 2018000021 | Gehan.Ahmed | IMPORT_PO | Draft              | 0002 - Signmedia  |
      | 2018000006 | Gehan.Ahmed | IMPORT_PO | OpenForUpdates     | 0002 - Signmedia  |
      | 2018000005 | Gehan.Ahmed | IMPORT_PO | Approved           | 0002 - Signmedia  |
      | 2018000008 | Gehan.Ahmed | IMPORT_PO | Confirmed          | 0002 - Signmedia  |
      | 2018000009 | Gehan.Ahmed | IMPORT_PO | FinishedProduction | 0002 - Signmedia  |
      | 2018000010 | Gehan.Ahmed | IMPORT_PO | Shipped            | 0002 - Signmedia  |
      | 2018000011 | Gehan.Ahmed | IMPORT_PO | Arrived            | 0002 - Signmedia  |
      | 2018000001 | Gehan.Ahmed | IMPORT_PO | Cleared            | 0002 - Signmedia  |
      | 2018000002 | Gehan.Ahmed | IMPORT_PO | DeliveryComplete   | 0002 - Signmedia  |
      # User with different role
      | 2018000013 | Gehan.Ahmed | LOCAL_PO  | Draft              | 0002 - Signmedia  |
      | 2018000014 | Gehan.Ahmed | LOCAL_PO  | OpenForUpdates     | 0002 - Signmedia  |
      | 2018000016 | Gehan.Ahmed | LOCAL_PO  | Approved           | 0002 - Signmedia  |
      | 2018000017 | Gehan.Ahmed | LOCAL_PO  | Confirmed          | 0002 - Signmedia  |
      | 2018000018 | Gehan.Ahmed | LOCAL_PO  | Shipped            | 0002 - Signmedia  |
      | 2018000019 | Gehan.Ahmed | LOCAL_PO  | DeliveryComplete   | 0002 - Signmedia  |
      # User with role with different conditions
      | 2018000025 | Amr.Khalil  | IMPORT_PO | Draft              | 0001 - Flexo      |
      | 2018000026 | Amr.Khalil  | IMPORT_PO | Draft              | 0006 - Corrugated |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (10) Cancel Request to edit Company section: after edit session expire for all PO types (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed" cancels saving Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | Type       | State |
      | 2018000013 | IMPORT_PO  | Draft |
      | 2018000021 | LOCAL_PO   | Draft |
      | 2020000004 | SERVICE_PO | Draft |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (11) Cancel Request to edit Company section: after edit session expire and section is locked by another user for all PO types (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "hr1" at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | Type       | State |
      | 2018000013 | IMPORT_PO  | Draft |
      | 2018000021 | LOCAL_PO   | Draft |
      | 2020000004 | SERVICE_PO | Draft |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (12) Cancel Request to edit Company section: while PurchaseOrder doesn't exist for all PO types (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 9:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" cancels saving Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | Type       | State |
      | 2018000013 | IMPORT_PO  | Draft |
      | 2018000021 | LOCAL_PO   | Draft |
      | 2020000004 | SERVICE_PO | Draft |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (13) Cancel Request to edit Company section: action is not allowed in Current state for Import & Local PO (i.e. edit session expired & cancelled by another user) (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And Company section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first Cancelled the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" cancels saving Company section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 11:10 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | Type      | State          |
      | 2018000006 | IMPORT_PO | OpenForUpdates |
      | 2018000014 | LOCAL_PO  | OpenForUpdates |

  # EBS-1373
  # EBS-7525
  Scenario Outline: (14) Cancel Request to edit Company section: by an unauthorized user due to condition - for all PO types (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" cancels saving Company section of PurchaseOrder with Code "<POCode>"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     | Type       | State | BusinessUnit     |
      | 2018000013 | IMPORT_PO  | Draft | 0002 - Signmedia |
      | 2018000021 | LOCAL_PO   | Draft | 0002 - Signmedia |
      | 2020000004 | SERVICE_PO | Draft | 0002 - Signmedia |