Feature: Save Edit Item Quantities in PO Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
    And the following Import PurchaseOrders exist:
      | Code       | State          | PurchasingUnit |
      | 2018000006 | OpenForUpdates | 0002           |
      | 2018000021 | Draft          | 0002           |
      | 2018000029 | OpenForUpdates | 0006           |
      | 2018000104 | Draft          | 0001           |
    And the following Local PurchaseOrders exist:
      | Code       | State          | PurchasingUnit |
      | 2018000013 | Draft          | 0002           |
      | 2018000014 | OpenForUpdates | 0002           |
      | 2018000030 | OpenForUpdates | 0001           |
      | 2018000031 | OpenForUpdates | 0006           |
    And the following PurchaseOrders exist with the following Items and Quantities:
      | POCode     | ItemCode | QtyID |
      # Import
      | 2018000006 | 000003   | 46    |
      | 2018000021 | 000002   | 19    |
      | 2018000029 | 000013   | 63    |
      | 2018000104 | 000005   | 48    |
      # Local
      | 2018000013 | 000001   | 21    |
      | 2018000014 | 000003   | 47    |
      | 2018000030 | 000006   | 49    |
      | 2018000031 | 000013   | 64    |
    And the following Measures exist:
      | Code | Type     | Symbol        | Name          |
      | 0032 | Business | Roll 3.20x50  | Roll 3.20x50  |
      | 0033 | Business | Roll 2.70x50  | Roll 2.70x50  |
      | 0034 | Business | Roll 1.07x50  | Roll 1.07x50  |
      | 0035 | Business | Roll 1.27x50  | Roll 1.27x50  |
      | 0036 | Business | Drum-10Kg     | Drum-10Kg     |
      | 0042 | Business | Bottle-2Liter | Bottle-2Liter |
    And edit session is "30" minutes

  #EBS-4030
  @ResetData
  Scenario Outline: (01) Save Edit Item Quantity to Local/Import PurchaseOrder, in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Quantity with id "<QtyID>" for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> |
    Then Item Quantity with id "<QtyID>" in Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" is updated as follows:
      | Qty        | OrderUnit        | UnitPrice(Gross) | Discount        | LastUpdatedBy | LastUpdateDate       |
      | <QtyValue> | <OrderUnitValue> | <UnitPriceValue> | <DiscountValue> | <User>        | 07-Jan-2019 09:30 AM |
    And Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | <User>        | 07-Jan-2019 09:30 AM |
    And the PurchaseOrder with code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | <User>        | 07-Jan-2019 09:30 AM |
    And edit Qty dialog is closed and the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | POCode     | ItemCode | State          | User        | Type   | QtyID | QtyValue | OrderUnitValue | UnitPriceValue | DiscountValue |
      # Import
      | 2018000006 | 000003   | OpenForUpdates | Gehan.Ahmed | Import | 46    | 700.0    | 0036           | 4.0            | 5.0%          |
      | 2018000021 | 000002   | Draft          | Gehan.Ahmed | Import | 19    | 300.0    | 0033           | 0.92           | 0.0%          |
      | 2018000029 | 000013   | OpenForUpdates | Amr.Khalil  | Import | 63    | 200.5    | 0042           | 33.0           | 11.0%         |
      | 2018000104 | 000005   | Draft          | Amr.Khalil  | Import | 48    | 150.0    | 0034           | 10.5           | 20.0%         |
      # Local
      | 2018000013 | 000001   | Draft          | Gehan.Ahmed | Local  | 21    | 800.0    | 0032           | 20.0           | 30.0%         |
      | 2018000014 | 000003   | OpenForUpdates | Gehan.Ahmed | Local  | 47    | 45.5     | 0036           | 0.69           | 2.0%          |
      | 2018000030 | 000006   | OpenForUpdates | Amr.Khalil  | Local  | 49    | 122.0    | 0035           | 6.0            | 7.0%          |
      | 2018000031 | 000013   | OpenForUpdates | Amr.Khalil  | Local  | 64    | 900.0    | 0042           | 30.5           | 4.0%          |
