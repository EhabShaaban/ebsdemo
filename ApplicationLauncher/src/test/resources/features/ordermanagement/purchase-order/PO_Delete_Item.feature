# Author: Aya Sadek, Hend Ahmed 05-Dec-2018 10:00 AM
# Reviewer: Somaya Aboulwafa, 20-December-2018 1:30 PM

Feature: Delete Item in PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                      |                                 |
    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll                      | 0002           |
      | 000002 | Hot Laminated Frontlit Backlit roll                     | 0002           |
      | 000003 | Flex Primer Varnish E33                                 | 0002           |
      | 000005 | self adhesive vinal 100 micron 140 grm glossy 1.07x50   | 0001           |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000004 | Draft              | 0003           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000024 | Draft            | 0003           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000001 | 000001   |         |
      | 2018000001 | 000002   |         |
      | 2018000001 | 000003   |         |
      | 2018000002 | 000003   |         |
      | 2018000002 | 000004   |         |
      | 2018000005 | 000001   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000009 | 000005   | 53.5    |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000011 | 000012   | 66      |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000025 | 000005   |         |
      | 2018000026 | 000012   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
      | 2018000014 | 000003   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000027 | 000006   |         |
      | 2018000028 | 000012   |         |

  ######## Happy Paths
  @ResetData
  Scenario Outline:(01) Delete Item from Import/Local PO, where PO is Draft/OpenForUpdates - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an empty Item section of PurchaseOrder with code "<POCode>" is displayed to "<User>"
    And a success notification is sent to "<User>" with the following message "Gen-msg-18"
    Examples:
      | User        | POCode     | ItemCode |
      | Gehan.Ahmed | 2018000021 | 000002   |
      | Gehan.Ahmed | 2018000014 | 000003   |
      | Amr.Khalil  | 2018000025 | 000005   |
      | Amr.Khalil  | 2018000027 | 000006   |
      | Amr.Khalil  | 2018000026 | 000012   |
      | Amr.Khalil  | 2018000028 | 000012   |

  Scenario Outline: (02) Delete Item from Import/Local PO, with state where this action is not allowed (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-21"
    Examples:
      | POCode     | State              | ItemCode |
      | 2018000001 | Cleared            | 000003   |
      | 2018000002 | DeliveryComplete   | 000004   |
      | 2018000005 | Approved           | 000001   |
      | 2018000007 | WaitingApproval    | 000001   |
      | 2018000008 | Confirmed          | 000001   |
      | 2018000009 | FinishedProduction | 000005   |
      | 2018000010 | Shipped            | 000002   |
      | 2018000011 | Arrived            | 000012   |
      | 2018000012 | Cancelled          | 000001   |
      | 2018000015 | WaitingApproval    | 000001   |
      | 2018000016 | Approved           | 000001   |
      | 2018000017 | Confirmed          | 000001   |
      | 2018000018 | Shipped            | 000001   |
      | 2018000019 | DeliveryComplete   | 000001   |
      | 2018000020 | Cancelled          | 000001   |

  @ResetData
  Scenario Outline: (03) Delete Item from Import/Local PO, where PO doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | ItemCode |
      | 2018000021 | 000002   |
      | 2018000013 | 000001   |

  @ResetData
  Scenario Outline: (04) Delete Item from Import/Local PO, where Item doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode |
      | 2018000021 | 000002   |
      | 2018000013 | 000001   |

  Scenario Outline: (05) Delete Item from Import/Local PO, when OrderItems section are locked by another user - Import PurchaseOrder (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens OrderItems section of PurchaseOrder with code "<POCode>" in edit mode
    When "Gehan.Ahmed" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     | ItemCode |
      | 2018000021 | 000002   |
      | 2018000013 | 000001   |

  ######## Unauthorized Access
  Scenario Outline: (06) Delete Item from Import/Local PO, by an unauthorized user (due to condition or not) -  Local/Import (Unauthorized access/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with code "<ItemCode>" in PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode |
      | Mahmoud.Abdelaziz | 2018000021 | 000002   |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   |
      | Amr.Khalil        | 2018000021 | 000002   |
      | Amr.Khalil        | 2018000013 | 000001   |
