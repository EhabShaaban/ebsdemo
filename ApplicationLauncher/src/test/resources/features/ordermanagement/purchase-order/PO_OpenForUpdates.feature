# Author: Nancy Shoukry 06-May-2019
# Reviewer:

Feature: Open Purchase Order for updates

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                   |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia         |
      | PurchasingResponsible_Flexo      | POOwner_Flexo             |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated        |
      | Storekeeper_Signmedia            | POViewerLimited_Signmedia |
      | SuperUser                        | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:OpenUpdates | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:OpenUpdates | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:OpenUpdates | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                       |                                 |
    And the following PurchaseOrders exist:
      | Code       | Type      | State              | PurchasingUnit |
      | 2018000021 | IMPORT_PO | Draft              | 0002           |
      | 2018000005 | IMPORT_PO | Approved           | 0002           |
      | 2018000006 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000029 | IMPORT_PO | OpenForUpdates     | 0006           |
      | 2018000007 | IMPORT_PO | WaitingApproval    | 0002           |
      | 2018000008 | IMPORT_PO | Confirmed          | 0002           |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002           |
      | 2018000010 | IMPORT_PO | Shipped            | 0002           |
      | 2018000011 | IMPORT_PO | Arrived            | 0002           |
      | 2018000001 | IMPORT_PO | Cleared            | 0002           |
      | 2018000002 | IMPORT_PO | DeliveryComplete   | 0002           |
      | 2018000012 | IMPORT_PO | Cancelled          | 0002           |
      | 2018000025 | IMPORT_PO | Draft              | 0001           |
      | 2018000026 | IMPORT_PO | Draft              | 0006           |
      | 2018000105 | IMPORT_PO | OpenForUpdates     | 0002           |
      | 2018000032 | IMPORT_PO | WaitingApproval    | 0001           |
      | 2018000013 | LOCAL_PO  | Draft              | 0002           |
      | 2018000030 | LOCAL_PO  | OpenForUpdates     | 0001           |
      | 2018000031 | LOCAL_PO  | OpenForUpdates     | 0006           |
      | 2018000015 | LOCAL_PO  | WaitingApproval    | 0002           |
      | 2018000016 | LOCAL_PO  | Approved           | 0002           |
      | 2018000017 | LOCAL_PO  | Confirmed          | 0002           |
      | 2018000018 | LOCAL_PO  | Shipped            | 0002           |
      | 2018000019 | LOCAL_PO  | DeliveryComplete   | 0002           |
      | 2018000020 | LOCAL_PO  | Cancelled          | 0002           |
      | 2018000027 | LOCAL_PO  | Draft              | 0001           |
    And the following ApprovalCycles section exist in PurchaseOrder with code "2018000032":
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM |                      |               |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |

  @ResetData
  Scenario Outline: (01) Open PO for updates (from all allowed states) (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to OpenForUpdates the PurchaseOrder with Code "<POCode>" at "6-May-2019 9:30 AM"
    Then PurchaseOrder with Code "<POCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate      | State          |
      | <User>        | 06-May-2019 9:30 AM | OpenForUpdates |
    And a success notification is sent to "<User>" with the following message "PO-msg-57"
    Examples:
      | POCode     | State              | PurchasingUnit | User        |
      | 2018000005 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000016 | Approved           | 0002           | Gehan.Ahmed |
      | 2018000008 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000017 | Confirmed          | 0002           | Gehan.Ahmed |
      | 2018000009 | FinishedProduction | 0002           | Gehan.Ahmed |

  @ResetData
  Scenario: (02) Open PO for updates in WaitingApproval state (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to OpenForUpdates the PurchaseOrder with Code "2018000032" at "6-May-2019 9:30 AM"
    Then PurchaseOrder "2018000032" has the following approval cycles:
      | ApprovalCycleNo | StartDate            | EndDate              | FinalDecision |
      | 2               | 21-Sep-2018 01:00 PM | 06-May-2019 9:30 AM  | Cancelled     |
      | 1               | 21-Sep-2018 09:00 AM | 21-Sep-2018 11:00 AM | Rejected      |
    Then PurchaseOrder with Code "2018000032" is updated as follows:
      | LastUpdatedBy | LastUpdateDate      | State          |
      | Amr.Khalil    | 06-May-2019 9:30 AM | OpenForUpdates |
    And a success notification is sent to "Amr.Khalil" with the following message "PO-msg-57"

  Scenario Outline: (03) Open PO for updates (Import/Local) that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-May-2019 9:20 AM"
    When "Gehan.Ahmed" requests to OpenForUpdates the PurchaseOrder with Code "<POCode>" at "6-May-2019 9:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"
    Examples:
      | POSection | POCode     | POType    |
      | Company   | 2018000005 | IMPORT_PO |
      | Company   | 2018000016 | LOCAL_PO  |

  Scenario Outline: (04) Open PO for updates (Import / Local) when action is not allowed in current state (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to OpenForUpdates the PurchaseOrder with Code "<POCode>" at "6-May-2019 9:30 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State            | PurchasingUnit | User        |
      | 2018000006 | OpenForUpdates   | 0002           | Gehan.Ahmed |
      | 2018000030 | OpenForUpdates   | 0001           | Amr.Khalil  |
      | 2018000010 | Shipped          | 0002           | Gehan.Ahmed |
      | 2018000018 | Shipped          | 0002           | Gehan.Ahmed |
      | 2018000011 | Arrived          | 0002           | Gehan.Ahmed |
      | 2018000001 | Cleared          | 0002           | Gehan.Ahmed |
      | 2018000002 | DeliveryComplete | 0002           | Gehan.Ahmed |
      | 2018000019 | DeliveryComplete | 0002           | Gehan.Ahmed |
      | 2018000012 | Cancelled        | 0002           | Gehan.Ahmed |
      | 2018000020 | Cancelled        | 0002           | Gehan.Ahmed |

  Scenario Outline: (05) Open PO for updates (Import/Local) in Draft state (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to OpenForUpdates the PurchaseOrder with Code "<POCode>" at "6-May-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | State | PurchasingUnit | User        |
      | 2018000021 | Draft | 0002           | Gehan.Ahmed |
      | 2018000027 | Draft | 0002           | Amr.Khalil  |

  Scenario Outline: (6) Open PO for updates by an unauthorized user due to condition or not (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to OpenForUpdates the PurchaseOrder with Code "<POCode>" at "6-May-2019 9:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     |
      | Mahmoud.Abdelaziz | 2018000005 |
      | Mahmoud.Abdelaziz | 2018000016 |
      | Amr.Khalil        | 2018000005 |
      | Amr.Khalil        | 2018000016 |

