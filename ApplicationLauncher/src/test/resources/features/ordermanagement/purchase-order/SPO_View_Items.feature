#Author: Zyad Ghorab, Shirin

Feature: View Service Purchase Order Items section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Ahmed.Seif  | Quality_Specialist              |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia |
      | Quality_Specialist              | POViewer           |
      | SuperUser                       | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                        | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadItemsWithPrices | [purchaseUnitName='Signmedia'] |
      | POViewer           | PurchaseOrder:ReadItemsWithPrices |                                |
      | SuperUserSubRole   | *:*                               |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                        | Condition                  |
      | Gehan.Ahmed | PurchaseOrder:ReadItemsWithPrices | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000055 | IMPORT_PO  | Cleared   | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000040 | LOCAL_PO   | Confirmed | hr1       | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000005 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
      | 2020000006 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000055 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000040 | 0001 - Flexo     |                  |                                 |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000005 | 0001 - Flexo     |                  |                                 |
      | 2020000006 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000055  |
      | 2020000005 | 000051 - Vendor 6 | 2018000040  |
      | 2020000006 | 000002 - Zhejiang | 2018000055  |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000004 | 1        | 000057 - Bank Fees          | 0019 - M2 | 16.000   | 200.000 | 3200.000        |
      | 2020000004 | 2        | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
      | 2020000005 | 3        | 000056 - Shipment service   | 0019 - M2 | 20.000   |         |                 |
    And the following PurchaseOrders have no Items:
      | Code       |
      | 2020000006 |
    #@INSERT
    And the following PurchaseOrders have the following TaxDetails:
      | Code       | Tax                 | TaxPercentage |
      | 2020000004 | 0013 - Shipping tax | 10            |
      | 2020000004 | 0014 - Service tax  | 2             |
      | 2020000006 | 0013 - Shipping tax | 10            |
      | 2020000006 | 0014 - Service tax  | 2             |
    And the following PurchaseOrders have no Taxes:
      | Code       |
      | 2020000005 |

  # EBS-5730
  Scenario: (01) View Service Purchase Order Items section, by an authorized user who has one role with one condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Items section of PurchaseOrder with code "2020000004"
    Then the following values of Items section for PurchaseOrder with code "2020000004" are displayed to "Gehan.Ahmed":
      | Item                        | OrderUnit | Quantity | Price | ItemTotalAmount |
      | 000055 - Insurance service2 | 0019 - M2 | 20       | 200   | 4000            |
      | 000057 - Bank Fees          | 0019 - M2 | 16       | 200   | 3200            |
    And the following values for TaxesSummary for PurchaseOrder with code "2020000004" are displayed to "Gehan.Ahmed":
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 720       |
      | 0014 - Service tax  | 2             | 144       |
    And the following values for ItemsSummary for PurchaseOrder with code "2020000004" are displayed to "Gehan.Ahmed":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 7200                   | 864        | 8064                  |

  # EBS-5730
  Scenario: (02) View Service Purchase Order Items section, by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of PurchaseOrder with code "2020000004"
    Then the following values of Items section for PurchaseOrder with code "2020000004" are displayed to "Ahmed.Seif":
      | Item                        | OrderUnit | Quantity | Price | ItemTotalAmount |
      | 000055 - Insurance service2 | 0019 - M2 | 20       | 200   | 4000            |
      | 000057 - Bank Fees          | 0019 - M2 | 16       | 200   | 3200            |
    And the following values for TaxesSummary for PurchaseOrder with code "2020000004" are displayed to "Ahmed.Seif":
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 720       |
      | 0014 - Service tax  | 2             | 144       |
    And the following values for ItemsSummary for PurchaseOrder with code "2020000004" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 7200                   | 864        | 8064                  |

  # EBS-5730
  Scenario: (03) View Service Purchase Order Items section, by an authorized user who has one role without conditions - No Price - NO Taxes (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view Items section of PurchaseOrder with code "2020000005"
    Then the following values of Items section for PurchaseOrder with code "2020000005" are displayed to "Ahmed.Seif":
      | Item                      | OrderUnit | Quantity | Price | ItemTotalAmount |
      | 000056 - Shipment service | 0019 - M2 | 20       |       |                 |
    And TaxesSummary section for PurchaseOrder with code "2020000005" is displayed empty to "Ahmed.Seif"
    And the following values for ItemsSummary for PurchaseOrder with code "2020000005" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      |                        | 0          |                       |

  # EBS-5730
  Scenario: (04) View Service Purchase Order Items section, by an authorized user who has one role without conditions - No Items - has Taxes (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Items section of PurchaseOrder with code "2020000006"
    Then Items section for PurchaseOrder with code "2020000006" is displayed empty to "Gehan.Ahmed"
    And the following values for TaxesSummary for PurchaseOrder with code "2020000006" are displayed to "Gehan.Ahmed":
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            |           |
      | 0014 - Service tax  | 2             |           |
    And the following values for ItemsSummary for PurchaseOrder with code "2020000004" are displayed to "Ahmed.Seif":
      | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      |                        | 0          |                       |

  # EBS-5730
  Scenario: (05) View Service Purchase Order Items section, where PurchaseOrder doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to view Items section of PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-5730
  Scenario: (06) View Service Purchase Order Items section, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Items section of PurchaseOrder with code "2020000005"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page