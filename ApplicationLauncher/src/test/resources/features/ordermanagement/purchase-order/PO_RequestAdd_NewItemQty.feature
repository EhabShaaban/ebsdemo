# Authors: Zyad Ghorab, Shrouk Alaa 08-May-2018

Feature: Request Add/Cancel Item Quantities in PO

  Background:
    Given the following users and roles exist:
      | Name                   | Role                                                   |
      | Gehan.Ahmed            | PurchasingResponsible_Signmedia                        |
      | Amr.Khalil             | PurchasingResponsible_Flexo                            |
      | Amr.Khalil             | PurchasingResponsible_Corrugated                       |
      | Mahmoud.Abdelaziz      | Storekeeper_Signmedia                                  |
      | hr1                    | SuperUser                                              |
      | Gehan.Ahmed.NoMeasures | PurchasingResponsible_Signmedia_CannotReadMeasuresRole |
    And the following roles and sub-roles exist:
      | Role                                                   | Subrole            |
      | PurchasingResponsible_Signmedia                        | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo                            | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated                       | POOwner_Corrugated |
      | SuperUser                                              | SuperUserSubRole   |
      | PurchasingResponsible_Signmedia_CannotReadMeasuresRole | POOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:UpdateItem | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                      |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018100002 | Draft              | 0002           |
      | 2018000021 | Draft              | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018100003 | Draft            | 0002           |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
      | 2018000027 | Draft            | 0001           |
      | 2018000028 | Draft            | 0006           |
    And the following PurchaseOrders have the following items:
      | POCode     | ItemCode | QtyInDN |
      # Import
      | 2018000001 | 000001   |         |
      | 2018000002 | 000003   |         |
      | 2018000005 | 000001   |         |
      | 2018000006 | 000002   |         |
      | 2018000006 | 000003   |         |
      | 2018000007 | 000001   |         |
      | 2018000008 | 000001   |         |
      | 2018000009 | 000001   |         |
      | 2018000010 | 000002   |         |
      | 2018000011 | 000001   |         |
      | 2018000012 | 000001   |         |
      | 2018000021 | 000002   |         |
      | 2018000025 | 000005   |         |
      | 2018000026 | 000012   |         |
      | 2018100002 | 000001   |         |
      # Local
      | 2018000013 | 000001   |         |
      | 2018000013 | 000002   |         |
      | 2018000014 | 000003   |         |
      | 2018000015 | 000001   |         |
      | 2018000016 | 000001   |         |
      | 2018000017 | 000001   |         |
      | 2018000018 | 000001   | 18000   |
      | 2018000019 | 000001   |         |
      | 2018000020 | 000001   |         |
      | 2018000027 | 000006   |         |
      | 2018000028 | 000012   |         |
      | 2018100003 | 000001   |         |
    And edit session is "30" minutes

  ################################################ Request to add Item Quantity scenarios ################################################

  Scenario Outline: (01) Request Add Item Quantity to Item for Local/Import PurchaseOrder, in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then a new add Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadAllMeasures |
    Examples:
      | POCode     | ItemCode | State          | User        | Type   |
      | 2018000021 | 000002   | Draft          | Gehan.Ahmed | Import |
      | 2018000006 | 000003   | OpenForUpdates | Gehan.Ahmed | Import |
      | 2018000013 | 000001   | Draft          | Gehan.Ahmed | Local  |
      | 2018000014 | 000003   | OpenForUpdates | Gehan.Ahmed | Local  |
      | 2018000025 | 000005   | Draft          | Amr.Khalil  | Import |
      | 2018000026 | 000012   | Draft          | Amr.Khalil  | Import |
      | 2018000027 | 000006   | Draft          | Amr.Khalil  | Local  |
      | 2018000028 | 000012   | Draft          | Amr.Khalil  | Local  |

  @ResetData
  Scenario Outline: (02) Request Add Item Quantity to Item for Local/Import PurchaseOrder, that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | ItemCode |
      | 2018100002 | 000001   |
      | 2018100003 | 000001   |

  @ResetData
  Scenario Outline: (03) Request Add Item Quantity to Item for Local/Import PurchaseOrder, that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully
    When "Gehan.Ahmed" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode |
      | 2018000006 | 000003   |
      | 2018000014 | 000003   |

  Scenario Outline: (04) Request Add Item Quantity to Item for Local/Import PurchaseOrder, in a state that doesn't allow this action - All states except Draft and OpenForUpdates - for both Import and Local POs  (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And OrderItems section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     | State              | ItemCode |
      | 2018000005 | Approved           | 000001   |
      | 2018000007 | WaitingApproval    | 000001   |
      | 2018000008 | Confirmed          | 000001   |
      | 2018000009 | FinishedProduction | 000001   |
      | 2018000010 | Shipped            | 000002   |
      | 2018000011 | Arrived            | 000001   |
      | 2018000001 | Cleared            | 000001   |
      | 2018000002 | DeliveryComplete   | 000003   |
      | 2018000012 | Cancelled          | 000001   |
      | 2018000015 | WaitingApproval    | 000001   |
      | 2018000016 | Approved           | 000001   |
      | 2018000017 | Confirmed          | 000001   |
      | 2018000018 | Shipped            | 000001   |
      | 2018000019 | DeliveryComplete   | 000001   |
      | 2018000020 | Cancelled          | 000001   |

  Scenario Outline: (05) Request Add Item Quantity to Item for Local/Import PurchaseOrder, when OrderItems section is locked by another user - Import/Local PurchaseOrder (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
    Examples:
      | POCode     | ItemCode |
      | 2018000021 | 000002   |
      | 2018000013 | 000001   |

  Scenario Outline: (06) Request Add Item Quantity to Item for Local/Import PurchaseOrder, by an unauthorized user/ due to unmatched condition (Abuse Case)
    Given user is logged in as "<User>"
    When  "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode |
      | Mahmoud.Abdelaziz | 2018000021 | 000002   |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   |
      | Gehan.Ahmed       | 2018000025 | 000005   |
      | Gehan.Ahmed       | 2018000027 | 000006   |

  Scenario Outline: (07) Request Add Item Quantity to Item for Local/Import PurchaseOrder, by a User with one or more roles - All allowed states - Local/Import (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to OrderItems section of PurchaseOrder with code "<POCode>"
    Then a new add Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code "<POCode>" becomes locked by "<User>"
    And there are no authorized reads returned to "<User>"
    Examples:
      | POCode     | User                   | ItemCode |
      | 2018000006 | Gehan.Ahmed.NoMeasures | 000003   |
      | 2018000013 | Gehan.Ahmed.NoMeasures | 000001   |

  ######### Cancel saving OrderItems Quantity section ########################################################################

  Scenario Outline: (08) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, within the edit session of Item Quantity to Local/Import PurchaseOrder in (Draft,OpenForUpdates) state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on OrderItems section of PurchaseOrder with code "<POCode>" is released
    Examples:
      | POCode     | ItemCode | State          | User        | Type   |
      | 2018000021 | 000002   | Draft          | Gehan.Ahmed | Import |
      | 2018000006 | 000003   | OpenForUpdates | Gehan.Ahmed | Import |
      | 2018000013 | 000001   | Draft          | Gehan.Ahmed | Local  |
      | 2018000014 | 000003   | OpenForUpdates | Gehan.Ahmed | Local  |
      | 2018000025 | 000005   | Draft          | Amr.Khalil  | Import |
      | 2018000026 | 000012   | Draft          | Amr.Khalil  | Import |
      | 2018000027 | 000006   | Draft          | Amr.Khalil  | Local  |
      | 2018000028 | 000012   | Draft          | Amr.Khalil  | Local  |

  Scenario Outline: (09) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, after edit session expire: for both Local and Import PO (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    Examples:
      | POCode     | ItemCode |
      | 2018000021 | 000002   |
      | 2018000013 | 000001   |

  @ResetData
  Scenario Outline:(10) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, while PurchaseOrder doesn't exist: for both Local and Import PO (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
    Examples:
      | POCode     | ItemCode |
      | 2018100002 | 000001   |
      | 2018100003 | 000001   |

  @ResetData
  Scenario Outline: (11) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, while Item that doesn't exist - for both Local/Import PO (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode at "07-Jan-2019 09:10 AM"
    And "hr1" first deletes Item with code "<ItemCode>" of PurchaseOrder with code "<POCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" at "07-Jan-2019 09:42 AM"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"
    Examples:
      | POCode     | ItemCode |
      | 2018000006 | 000003   |
      | 2018000014 | 000003   |

  Scenario Outline: (12) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, in a state that doesn't allow this action - All states except Draft and OpenForUpdates - for both Import and Local POs  (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opens Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>" in edit mode
    When "Gehan.Ahmed" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"
    And OrderItems section of PurchaseOrder with code "<POCode>" is not locked by "Gehan.Ahmed"
    Examples:
      | POCode     | State              | ItemCode |
      | 2018000005 | Approved           | 000001   |
      | 2018000007 | WaitingApproval    | 000001   |
      | 2018000008 | Confirmed          | 000001   |
      | 2018000009 | FinishedProduction | 000001   |
      | 2018000010 | Shipped            | 000002   |
      | 2018000011 | Arrived            | 000001   |
      | 2018000001 | Cleared            | 000001   |
      | 2018000002 | DeliveryComplete   | 000003   |
      | 2018000012 | Cancelled          | 000001   |
      | 2018000015 | WaitingApproval    | 000001   |
      | 2018000016 | Approved           | 000001   |
      | 2018000017 | Confirmed          | 000001   |
      | 2018000018 | Shipped            | 000001   |
      | 2018000019 | DeliveryComplete   | 000001   |
      | 2018000020 | Cancelled          | 000001   |

  Scenario Outline: (13) Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder, by an unauthorized user/ due to unmatched condition (Abuse Case)
    Given user is logged in as "<User>"
    When  "<User>" requests to cancel saving Item Quantity for Item with code "<ItemCode>" in OrderItems section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | POCode     | ItemCode |
      | Mahmoud.Abdelaziz | 2018000021 | 000002   |
      | Mahmoud.Abdelaziz | 2018000013 | 000001   |
      | Gehan.Ahmed       | 2018000025 | 000005   |
      | Gehan.Ahmed       | 2018000027 | 000006   |
