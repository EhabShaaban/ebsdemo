Feature: Mark PO as Production Finished

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                             | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:MarkAsProductionFinished | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                                    |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000025 | Draft              | 0001           |
      | 2018000026 | Draft              | 0006           |
      | 2018000038 | Confirmed          | 0001           |
      | 2018000039 | Confirmed          | 0006           |
    And the following PurchaseOrders exist with Confirmation date:
      | Code       | confirmationDate     |
      | 2018000008 | 07-Aug-2018 09:02 AM |
      | 2018000038 | 07-Aug-2018 09:02 AM |
      | 2018000039 | 07-Aug-2018 09:02 AM |

  ###### Happy Paths
  #EBS-1394
  @ResetData
  Scenario Outline: (01) MarkAsProductionFinished PO Confirmed Local/Import PO by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsProductionFinished the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 7:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then PurchaseOrder with Code "<POCode>" is updated with ProductionFinishedDate as follows:
      | LastUpdatedBy | LastUpdateDate             | State              | ProductionFinishedDate     |
      | <User>        | 07-Jan-2019 07:30 AM +0000 | ProductionFinished | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-35"
    Examples:
      | POCode     | Type   | State     | PurchasingUnit | User        |
      | 2018000008 | Import | Confirmed | 0002           | Gehan.Ahmed |
      | 2018000038 | Import | Confirmed | 0001           | Amr.Khalil  |
      | 2018000039 | Import | Confirmed | 0006           | Amr.Khalil  |

  #### Exceptions
  #EBS-1394
  Scenario Outline: (02) MarkAsProductionFinished PO Local/Import PO in a state that doesn't allow this action - any state except the Confirmed / Draft state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsProductionFinished the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | POCode     | State              | PurchasingUnit |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000005 | Approved           | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |

  #EBS-1394
  Scenario: (03) MarkAsProductionFinished PO Local/Import PO in Draft state (Abuse Case -- because an Confirmed PO cannot return to be draft by any means)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsProductionFinished the PurchaseOrder with Code "2018000021" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-1394
  Scenario: (04) MarkAsProductionFinished PO Confirmed Import/Local PO that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "Company" of PurchaseOrder with Code "2018000008" in edit mode at "07-Jan-2019 9:20 AM"
    When "Gehan.Ahmed" requests to MarkAsProductionFinished the PurchaseOrder with Code "2018000008" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"

  #EBS-1394
  Scenario: (05) MarkAsProductionFinished PO Confirmed Local/Import PO that is doesn't exist (Abuse Case -- cannot happen normally because an Confirmed PO cannot be deleted)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsProductionFinished the PurchaseOrder with Code "9999999999" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-1394
  Scenario Outline: (06) MarkAsProductionFinished PO Confirmed Local/Import PO with missing or malicious ConfirmationDate (Abuse case -- becoz user cannot send the request unless the confirmation date is entered correctly)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsProductionFinished the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate   |
      | <ProductionFinishedDate> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ProductionFinishedDate | POCode     |
      |                        | 2018000008 |
      | Ay 7aga                | 2018000008 |

  #EBS-1394
  Scenario Outline: (07) MarkAsProductionFinished PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsProductionFinished the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate |
      | 06-Jan-2019 11:30 AM   |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User              |
      | 2018000008 | Mahmoud.Abdelaziz |
      | 2018000008 | Amr.Khalil        |

  ######################################################## MarkAsProductionFinished PO with ProductionFinishedDate before confirmation date  ###############################
  Scenario Outline: (08) MarkAsProductionFinished PO Import / Confirmed Local PO with shipping date before production date (validation error)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsProductionFinished the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ProductionFinishedDate   |
      | <ProductionFinishedDate> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to Production date field "PO-msg-59" and sent to "<User>"
    Examples:
      | POCode     | Type   | State     | PurchasingUnit | User        | ProductionFinishedDate |
      | 2018000008 | Import | Confirmed | 0002           | Gehan.Ahmed | 06-Jan-2017 11:30 AM   |
      | 2018000038 | Import | Confirmed | 0001           | Amr.Khalil  | 08-Aug-2018 09:00 AM   |
      | 2018000039 | Import | Confirmed | 0006           | Amr.Khalil  | 08-Aug-2018 08:58 AM   |
