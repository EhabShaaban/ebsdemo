# Authors: Zyad Ghorab and Shirin Mahmoud, 16-Nov-2020
# updated by: Fatma Al Zahraa (EBS - 7614)


Feature: Request Add/Cancel Item in Service PO

  Background:
    Given the following users and roles exist:
      | Name                | Role                                                |
      | hr1                 | SuperUser                                           |
      | Gehan.Ahmed         | PurchasingResponsible_Signmedia                     |
      | Gehan.Ahmed.NoItems | PurchasingResponsible_Signmedia_CannotReadItemsRole |
    And the following roles and sub-roles exist:
      | Role                                                | Subrole             |
      | SuperUser                                           | SuperUserSubRole    |
      | PurchasingResponsible_Signmedia                     | POOwner_Signmedia   |
      | PurchasingResponsible_Signmedia                     | ItemOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole | POOwner_Signmedia   |

    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | SuperUserSubRole    | *:*                      |                                |
      | POOwner_Signmedia   | PurchaseOrder:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadAll             | [purchaseUnitName='Signmedia'] |
      | ItemOwner_Signmedia | Item:ReadUoMData         | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User                | Permission       |
      | Gehan.Ahmed.NoItems | Item:ReadAll     |
      | Gehan.Ahmed.NoItems | Item:ReadUoMData |

    And the following users have the following permissions without the following conditions:
      | User        | Permission               | Condition                  |
      | Gehan.Ahmed | PurchaseOrder:UpdateItem | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000010 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2019 9:02 AM | Amr Khalil    |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2019 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000002 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000010 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000002 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000010 | 000001 - Siegwerk |             |
      | 2018000008 | 000002 - Zhejiang |             |
      | 2020000006 | 000002 - Zhejiang | 2018000008  |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000002 | 000001 - Zhejiang | 2018000010  |
    And edit session is "30" minutes

    ######## Request to Add Item HP ########################################################################################

  Scenario: (01) Request to Add Item in Service PurchaseOrder, by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000004"
    Then a new add Item dialoge is opened and OrderItems section of PurchaseOrder with code "2020000004" becomes locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Gehan.Ahmed":
      | MandatoriesFields |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | price             |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields    |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | price             |

  #EBS-7614
  Scenario: (02) Request to Add Item in Service PurchaseOrder, when PurchaseOrderItems section is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario: (03) Request to Add Item in Service PurchaseOrder, that doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully
    When "Gehan.Ahmed" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-7614
  Scenario: (04) Request to Add Item in Service PurchaseOrder, in a state that doesn't allow this action [Confirmed] (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000006"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And PurchaseOrderItems section of Service PurchaseOrder with code "2020000006" is not locked by "hr1"

  #EBS-7614
  Scenario: (05) Request to Add Item in Service PurchaseOrder, by an unauthorized user (with condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000002"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-7614
  Scenario: (06) Request to Add Item in Service PurchaseOrder, by a User with no authorized reads - All allowed states [Draft]
    Given user is logged in as "Gehan.Ahmed.NoItems"
    When "Gehan.Ahmed.NoItems" requests to add Item to OrderItems section of Service PurchaseOrder with code "2020000004"
    Then a new add Item dialoge is opened and OrderItems section of PurchaseOrder with code "2020000004" becomes locked by "Gehan.Ahmed.NoItems"
    And there are no authorized reads returned to "Gehan.Ahmed.NoItems"
    And the following mandatory fields are returned to "Gehan.Ahmed.NoItems":
      | MandatoriesFields |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | price             |
    And the following editable fields are returned to "Gehan.Ahmed.NoItems":
      | EditableFields    |
      | itemCode          |
      | unitOfMeasureCode |
      | quantity          |
      | price             |


    ######## Cancel Add Item HP ########################################################################

  Scenario: (07) Request to Cancel Add Item in Service PurchaseOrder, within the edit session by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And OrderItems section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2021 09:10 AM"
    When "Gehan.Ahmed" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code "2020000004" at "07-Jan-2021 09:30 AM"
    Then the lock by "Gehan.Ahmed" on OrderItems section of PurchaseOrder with code "2020000004" is released

  #EBS-7614
  Scenario: (08) Request to Cancel Add Item in Service PurchaseOrder, after lock session is expired
    Given user is logged in as "Gehan.Ahmed"
    And OrderItems section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2021 09:00 AM"
    When "Gehan.Ahmed" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code "2020000004" at "07-Jan-2021 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (09) Request to Cancel Add Item in Service PurchaseOrder, after lock session is expire and PurchaseOrder doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And OrderItems section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2021 09:00 AM"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully at "07-Jan-2021 09:31 AM"
    When "Gehan.Ahmed" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code "2020000004" at "07-Jan-2021 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-7614
  Scenario: (10) Request to Cancel Add Item in Service PurchaseOrder, by unauthorized user (with condition) (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code "2020000002"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page


