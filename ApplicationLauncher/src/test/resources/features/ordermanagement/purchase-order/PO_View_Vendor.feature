# Author Dev: Eman Mansour , Zyad Ghorab
# Author Quality: Shirin Mahmoud
# updated by: Fatma Al Zahraa (EBS - 7485)

Feature: View Vendor section

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | hr1         | SuperUser                        |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole             |
      | PurchasingResponsible_Signmedia  | POViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | POViewer_Flexo      |
      | PurchasingResponsible_Corrugated | POViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                       |
      | POViewer_Signmedia  | PurchaseOrder:ReadVendor | [purchaseUnitName='Signmedia']  |
      | POViewer_Flexo      | PurchaseOrder:ReadVendor | [purchaseUnitName='Flexo']      |
      | POViewer_Corrugated | PurchaseOrder:ReadVendor | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                      |                                 |
    And the following users have the following permissions without the following conditions:
      | User       | Permission               | Condition                      |
      | Amr.Khalil | PurchaseOrder:ReadVendor | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000013 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000021 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000025 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000026 | IMPORT_PO  | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000027 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000028 | LOCAL_PO   | Draft   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2018000055 | LOCAL_PO   | Cleared | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft   | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000013 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000021 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000025 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000026 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000027 | 0001 - Flexo      | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000028 | 0006 - Corrugated | 0001 - AL Madina |                                 |
      | 2018000055 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000004 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor                     | ReferencePO |
      | 2018000013 | 000002 - Zhejiang          |             |
      | 2018000021 | 000002 - Zhejiang          |             |
      | 2018000025 | 000001 - Siegwerk          |             |
      | 2018000026 | 000006 - Vendor Corrugated |             |
      | 2018000027 | 000001 - Siegwerk          |             |
      | 2018000028 | 000006 - Vendor Corrugated |             |
      | 2018000055 | 000002 - Zhejiang          |             |
      | 2020000004 | 000002 - Zhejiang          | 2018000055  |

  #EBS-6354
  #EBS-7485
  Scenario Outline: (01) View Vendor Data section by an authorized user who has one/two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view Vendor Data section of PurchaseOrder with code "<POCodeValue>"
    Then the following values of Vendor Data section are displayed to "<User>":
      | Vendor          | ReferencePO       |
      | <POVendorValue> | <ReferencePOCode> |
    Examples:
      | User        | POCodeValue | POVendorValue              | ReferencePOCode |
      | Gehan.Ahmed | 2018000013  | 000002 - Zhejiang          |                 |
      | Gehan.Ahmed | 2018000021  | 000002 - Zhejiang          |                 |
      | Amr.Khalil  | 2018000025  | 000001 - Siegwerk          |                 |
      | Amr.Khalil  | 2018000026  | 000006 - Vendor Corrugated |                 |
      | Amr.Khalil  | 2018000027  | 000001 - Siegwerk          |                 |
      | Amr.Khalil  | 2018000028  | 000006 - Vendor Corrugated |                 |
      | Gehan.Ahmed | 2020000004  | 000002 - Zhejiang          | 2018000055      |

 #EBS-6354
  Scenario Outline: (02) View Vendor Data section where PurchaseOrder doesn't exist: for Local , Import , Service (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PurchaseOrder with code "<POCode>" successfully
    When "<User>" requests to view Vendor Data section of PurchaseOrder with code "<POCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | User        | POCode     |
      | Gehan.Ahmed | 2018000013 |
      | Gehan.Ahmed | 2018000021 |
      | Gehan.Ahmed | 2020000004 |

  #EBS-6354
  Scenario Outline: (03) View Vendor Data section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Vendor Data section of PurchaseOrder with code "<POCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | POCode     | User       |
      | 2018000013 | Amr.Khalil |
      | 2018000021 | Amr.Khalil |
      | 2020000004 | Amr.Khalil |
