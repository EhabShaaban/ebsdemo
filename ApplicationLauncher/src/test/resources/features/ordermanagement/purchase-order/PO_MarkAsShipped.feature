# Author:Aya Sadek on 15 Jan, 2019 at 04:38 PM
# Reviewer:
Feature: Mark PO as Shipped from loading port

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia  |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | hr1               | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole            |
      | PurchasingResponsible_Signmedia  | POOwner_Signmedia  |
      | PurchasingResponsible_Flexo      | POOwner_Flexo      |
      | PurchasingResponsible_Corrugated | POOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                  | Condition                       |
      | POOwner_Signmedia  | PurchaseOrder:MarkAsShipped | [purchaseUnitName='Signmedia']  |
      | POOwner_Flexo      | PurchaseOrder:MarkAsShipped | [purchaseUnitName='Flexo']      |
      | POOwner_Corrugated | PurchaseOrder:MarkAsShipped | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                         |                                 |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit |
      | 2018000005 | Approved           | 0002           |
      | 2018000006 | OpenForUpdates     | 0002           |
      | 2018000007 | WaitingApproval    | 0002           |
      | 2018000008 | Confirmed          | 0002           |
      | 2018000009 | FinishedProduction | 0002           |
      | 2018000010 | Shipped            | 0002           |
      | 2018000011 | Arrived            | 0002           |
      | 2018000001 | Cleared            | 0002           |
      | 2018000002 | DeliveryComplete   | 0002           |
      | 2018000012 | Cancelled          | 0002           |
      | 2018000021 | Draft              | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit |
      | 2018000013 | Draft            | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000017 | Confirmed        | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |
    And the following Import PurchaseOrders exist with producation date:
      | Code       | productionDate       |
      | 2018000009 | 07-Aug-2018 09:02 AM |
    And the following Local PurchaseOrders exist with confirmation date:
      | Code       | confirmationDate     |
      | 2018000017 | 07-Aug-2018 09:02 AM |

    ############################################### MarkAsShipped PO with all mandatory fields and authorized user ############################
  @ResetData
  Scenario Outline: (01) MarkAsShipped PO Production Finished Import / Confirmed Local PO with all mandatory fields for Shipped state - by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2019 11:30 AM |
    Then PurchaseOrder with Code "<POCode>" is updated with ShippingDate as follows:
      | LastUpdatedBy | LastUpdateDate             | State   | ShippingDate               |
      | <User>        | 07-Jan-2019 09:30 AM +0000 | Shipped | 06-Jan-2019 09:30 AM +0000 |
    And a success notification is sent to "<User>" with the following message "PO-msg-37"

    Examples:
      | POCode     | Type   | State              | PurchasingUnit | User        |
      | 2018000009 | Import | FinishedProduction | 0002           | Gehan.Ahmed |
      | 2018000017 | Local  | Confirmed          | 0002           | Gehan.Ahmed |

    ################################################## State that doesn't allow MarkAsShipped action ###################################
  Scenario Outline: (02) MarkAsShipped PO Local/Import PO in a state that doesn't allow this action - any state except the Draft state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2020 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

    Examples:
      | POCode     | State            | PurchasingUnit |
      | 2018000006 | OpenForUpdates   | 0002           |
      | 2018000007 | WaitingApproval  | 0002           |
      | 2018000005 | Approved         | 0002           |
      | 2018000008 | Confirmed        | 0002           |
      | 2018000010 | Shipped          | 0002           |
      | 2018000011 | Arrived          | 0002           |
      | 2018000001 | Cleared          | 0002           |
      | 2018000002 | DeliveryComplete | 0002           |
      | 2018000012 | Cancelled        | 0002           |
      | 2018000014 | OpenForUpdates   | 0002           |
      | 2018000015 | WaitingApproval  | 0002           |
      | 2018000016 | Approved         | 0002           |
      | 2018000018 | Shipped          | 0002           |
      | 2018000019 | DeliveryComplete | 0002           |
      | 2018000020 | Cancelled        | 0002           |

    ###################################################### MarkAsShipped PO in "Draft" state ##################################################
  Scenario Outline: (03) MarkAsShipped PO Local/Import PO in Draft state (Abuse Case -- because the (finished production/Confirmed) PO cannot return to draft by any means)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2019 11:30 AM |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | POCode     | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
      | 2018000013 | Draft | 0002           |

    ####################################################### MarkAsShipped PO that has at least one section locked ###############################
  Scenario Outline: (04) MarkAsShipped PO (Finished Production/Confirmed) Import/Local PO that has at least one section currently locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<POSection>" of PurchaseOrder with Code "<POCode>" in edit mode at "07-Jan-2019 09:20 AM"
    When "Gehan.Ahmed" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 07:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2019 11:30 AM |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"

    Examples:
      | POSection | POCode     |
      | Company   | 2018000009 |
      | Company   | 2018000017 |

    ####################################################### MarkAsShipped PO that is deleted by another user ###############################
  Scenario: (05) MarkAsShipped PO (Finished production/Confirmed) Local/Import PO that is deleted by another user (Abuse Case -- cannot happen normally because the (production finished/Confirmed) PO cannot be deleted)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsShipped the PurchaseOrder with Code "9999999999" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2019 11:30 AM |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    ####################################################### MarkAsShipped PO that has a malicious input in Shipping date ###############################
  Scenario Outline: (06) MarkAsShipped PO (Finished production/Confirmed) Local/Import PO with missing or malicious input in ShippingDate (Abuse case -- because user cannot send the request unless the shipping date is entered correctly)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate   |
      | <ShippingDate> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | ShippingDate | POCode     |
      |              | 2018000009 |
      | Ay 7aga      | 2018000009 |
      |              | 2018000017 |
      | Ay 7aga      | 2018000017 |

    ####################################################### MarkAsShipped PO with unauthorized user ###############################
  Scenario Outline: (07) MarkAsShipped PO by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate         |
      | 06-Jan-2019 11:30 AM |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | POCode     | User              |
      | 2018000009 | Mahmoud.Abdelaziz |
      | 2018000017 | Mahmoud.Abdelaziz |
      | 2018000009 | Amr.Khalil        |
      | 2018000017 | Amr.Khalil        |



    ######################################################## MarkAsShipped PO with shipping date before Production date ###############################
  Scenario Outline: (08) MarkAsShipped PO Production Finished Import / Confirmed Local PO with shipping date before production date (validation error)
    Given user is logged in as "<User>"
    When "<User>" requests to MarkAsShipped the PurchaseOrder with Code "<POCode>" at "07-Jan-2019 09:30 AM +0000" with the following values:
      | ShippingDate   |
      | <ShippingDate> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to shipping date field "PO-msg-59" and sent to "<User>"

    Examples:
      | POCode     | Type   | State              | PurchasingUnit | User        | ShippingDate         |
      | 2018000009 | Import | FinishedProduction | 0002           | Gehan.Ahmed | 06-Jan-2017 11:30 AM |
      | 2018000009 | Import | FinishedProduction | 0002           | Gehan.Ahmed | 07-Aug-2018 09:02 AM |
      | 2018000009 | Import | FinishedProduction | 0002           | Gehan.Ahmed | 07-Aug-2018 09:00 AM |
      | 2018000017 | Local  | Confirmed          | 0002           | Gehan.Ahmed | 06-Jan-2017 11:30 AM |
      | 2018000017 | Local  | Confirmed          | 0002           | Gehan.Ahmed | 07-Aug-2018 09:02 AM |
      | 2018000017 | Local  | Confirmed          | 0002           | Gehan.Ahmed | 07-Aug-2018 09:00 AM |

