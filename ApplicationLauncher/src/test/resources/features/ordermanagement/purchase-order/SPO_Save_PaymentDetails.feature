# Author Dev: Fatma Al Zahraa (HP)
# Author Dev: Zyad Ghorab (Val)
# Author Quality: Shirin Mahmoud

Feature: Save PaymentDetails section in Service PurchaseOrder

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Sub-role          |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole  |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission                       | Condition                      |
      | POOwner_Signmedia | PurchaseOrder:UpdatePaymentTerms | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole  | *:*                              |                                |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000008 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000055 | IMPORT_PO  | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000004 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000055 | 0002 - Signmedia |                  |                                 |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000006 | 0002 - Signmedia |                  |                                 |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000004 | 000002 - Zhejiang | 2018000008  |
      | 2020000006 | 000002 - Zhejiang | 2018000055  |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And edit session is "30" minutes

  #EBS-7406
  Scenario Outline: (01) Save SPO PaymentDetails section, within edit session by an authorized user for Draft Service PurchaseOrder (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of Service PurchaseOrder with Code "2020000004" at "07-Jan-2019 09:30 AM" with the following values:
      | Currency   | PaymentTerms   |
      | <Currency> | <PaymentTerms> |
    Then Service PurchaseOrder with Code "2020000004" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM |
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is updated as follows:
      | Currency   | PaymentTerms   |
      | <Currency> | <PaymentTerms> |
    And the lock by "Gehan.Ahmed" on PaymentDetails section of Service PurchaseOrder with code "2020000004" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | Currency   | PaymentTerms                               |
      | 0001 - EGP | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 0001 - EGP |                                            |
      |            | 0001 - 20% Deposit, 80% T/T Before Loading |

  Scenario: (02) Save SPO PaymentDetails section, after edit session expired (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of Service PurchaseOrder with Code "2020000004" at "07-Jan-2019 09:41 AM" with the following values:
      | Currency   | PaymentTerms                               |
      | 0001 - EGP | 0001 - 20% Deposit, 80% T/T Before Loading |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  # EBS-7407
  Scenario: (03) Save SPO PaymentDetails section, after edit session expired and SPO doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PurchaseOrder with code "2020000004" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of Service PurchaseOrder with Code "2020000004" at "07-Jan-2019 09:42 AM" with the following values:
      | Currency   | PaymentTerms                               |
      | 0001 - EGP | 0001 - 20% Deposit, 80% T/T Before Loading |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-7697
  Scenario: (04) Save SPO PaymentDetails section, when it is not allowed in current state: Confirmed (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" saves PaymentDetails section of Service PurchaseOrder with Code "2020000006" with the following values:
      | Currency   | PaymentTerms                               |
      | 0001 - EGP | 0001 - 20% Deposit, 80% T/T Before Loading |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

  # EBS-7407
  Scenario Outline: (05) Save SPO PaymentDetails section, with malicious input  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of Service PurchaseOrder with code "2020000004" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of Service PurchaseOrder with Code "2020000004" with the following values:
      | Currency   | PaymentTerms   |
      | <Currency> | <PaymentTerms> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | Currency                      | PaymentTerms                               |
      | 9999 - Currency doesn't exist | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 0001 - EGP                    | 9999 - Payment Term doesn't exist          |
      | ay7aga                        | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 00001                         | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 0001 - EGP                    | ay7aga                                     |
      | 0001 - EGP                    | 00001                                      |
