Feature: Save RequiredDocuments section - Authorization

  Background:
    Given the following users and roles exist:
      | Name                       | Role                                                       |
      | Shady.Abdelatif            | Accountant_Signmedia                                       |
      | Amr.Khalil                 | PurchasingResponsible_Flexo                                |
      | Gehan.Ahmed.NoDocumentType | PurchasingResponsible_Signmedia_CannotViewDocumentTypeRole |
    And the following roles and sub-roles exist:
      | Role                                                       | Subrole            |
      | PurchasingResponsible_Flexo                                | POOwner_Flexo      |
      | PurchasingResponsible_Flexo                                | DocumentTypeViewer |
      | PurchasingResponsible_Signmedia_CannotViewDocumentTypeRole | POOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                            | Condition                      |
      | POOwner_Flexo      | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Flexo']     |
      | POOwner_Signmedia  | PurchaseOrder:UpdateRequiredDocuments | [purchaseUnitName='Signmedia'] |
      | DocumentTypeViewer | DocumentType:ReadAll                  |                                |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |
    And the following Local PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000013 | Draft | 0002           |
    And edit session is "30" minutes

  # Save section by a user who is not authorized to save
  # EBS-2210
  Scenario Outline: (01) Save RequiredDocument section by an unauthorized users (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" with the following values:
      | DocumentType | Copies |
      | 0001         | 1      |
      | 0002         | 2      |
      | 0004         | 8      |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  # EBS-2210
  Scenario Outline: (02) Save RequiredDocument section by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" with the following values:
      | DocumentType | Copies |
      | 0001         | 1      |
      | 0002         | 2      |
      | 0004         | 8      |
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |

  ######## Create using values which the user is not authorized to Read
  # EBS-2210
  Scenario Outline: (03) Save RequiredDocument section by User is not authorized to read: DocumentType (Abuse case)
    Given user is logged in as "Gehan.Ahmed.NoDocumentType"
    And RequiredDocuments section of PurchaseOrder with Code "<POCode>" is locked by "Gehan.Ahmed.NoDocumentType" at "07-Jan-2019 9:10 AM"
    When "Gehan.Ahmed.NoDocumentType" saves RequiredDocuments section of PurchaseOrder with Code "<POCode>" at "07-Jan-2019 9:30 AM" with the following values:
      | DocumentType | Copies |
      | 0001         | 1      |
      | 0002         | 2      |
      | 0004         | 8      |
    Then the lock by "Gehan.Ahmed.NoDocumentType" on RequiredDocuments section of PurchaseOrder with Code "<POCode>" is released
    And "Gehan.Ahmed.NoDocumentType" is logged out
    And "Gehan.Ahmed.NoDocumentType" is forwarded to the error page
    Examples:
      | POCode     |
      | 2018000021 |
      | 2018000013 |
