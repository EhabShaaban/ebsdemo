#Author: Aya Sadek on Date: 31-Dec-2018, 12:26 PM
#Reviewer: Ibrahim, Nancy, and Somaya on Date: 31-Dec-2018, 03:15 PM

Feature: Section lock is released when user is logged out

  Background:
    Given the following users and roles exist:
      | Name              | Role                            |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia           |
      | hr1               | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole               |
      | PurchasingResponsible_Signmedia | ItemOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia |
      | PurchasingResponsible_Signmedia | POOwner_Signmedia     |
      | Storekeeper_Signmedia           | GROwner_Signmedia     |
      | SuperUser                       | SuperUserSubRole      |

    And the following sub-roles and permissions exist:
      | Subrole               | Permission                    | Condition                      |
      | ItemOwner_Signmedia   | Item:UpdateGeneralData        | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia | Vendor:UpdateGeneralData      | [purchaseUnitName='Signmedia'] |
      | POOwner_Signmedia     | PurchaseOrder:UpdateConsignee | [purchaseUnitName='Signmedia'] |
      | GROwner_Signmedia     | GoodsReceipt:UpdateHeader     | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole      | *:*                           |                                |

    And the following Items exist:
      | Code   | Name                                | PurchasingUnit |
      | 000002 | Hot Laminated Frontlit Backlit roll | 0002           |

    And the following Vendors exist:
      | Code   | PurchasingUnit |
      | 000002 | 0002           |
    And the following Import PurchaseOrders exist:
      | Code       | State | PurchasingUnit |
      | 2018000021 | Draft | 0002           |

  Scenario: (01) The Lock of many sections in (PO, Item and Vendor), that are in edit mode at the same time, is released when user is logged out
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" opens ConsigneeData section of PO with code "2018000021" in edit mode successfully
    And "hr1" opens GeneralData section of Item with code "000002" in edit mode successfully
    And "hr1" opens GeneralData section of Vendor with code "000002" in edit mode successfully
    And "hr1" logs out
    When "Gehan.Ahmed" requests to open ConsigneeData section of PO with code "2018000021" in edit mode
    And "Gehan.Ahmed" requests to open GeneralData section of Item with code "000002" in edit mode
    And "Gehan.Ahmed" requests to open GeneralData section of Vendor with code "000002" in edit mode
    Then "ConsigneeData" section of PO with code "2018000021" becomes in edit mode and locked by "Gehan.Ahmed"
    And "GeneralData" section of Item with code "000002" becomes in edit mode and locked by "Gehan.Ahmed"
    And "GeneralData" section of Vendor with code "000002" becomes in edit mode and locked by "Gehan.Ahmed"

