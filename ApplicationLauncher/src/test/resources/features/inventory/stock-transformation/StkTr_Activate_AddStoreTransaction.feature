# Author: Eman Hesham and Niveen Magdy on 6/4/2020 - reviewed by Somaya && Hosam
# New Author: Hossam Hassan on 26/11/2020 - reviewed by

Feature: StockTransformation Activate - add Store transaction based on transformation

  Background:
    Given the following users and roles exist:
      | Name        | Role                |
      | Gehan.Ahmed | Marketeer_Signmedia |
    And the following roles and sub-roles exist:
      | Role                | Subrole                                |
      | Marketeer_Signmedia | StockTransformationViewer_Signmedia    |
      | Marketeer_Signmedia | StockTransformationActivator_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                                | Permission                   | Condition                      |
      | StockTransformationViewer_Signmedia    | StockTransformation:ReadAll  | [purchaseUnitName='Signmedia'] |
      | StockTransformationActivator_Signmedia | StockTransformation:Activate | [purchaseUnitName='Signmedia'] |

    And the following StoreTransactions exist:
      | STCode     | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item                                         | UOM                  | Qty | EstimateCost | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction |
      | 2019000016 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000051 - Ink5                                | 0040 - Bottle-1Liter | 2   | 100          | 100        | 2019000020 - Goods Receipt | 1            |                |
      | 2019000001 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50  | 10  | 200          | 200        | 2019000088 - Goods Receipt | 10           |                |

    And the following GeneralData section for StockTransformations exist:
      | Code       | State | Type                     | CreationDate         | CreatedBy | BusinessUnit     |
      | 2020000001 | Draft | STOCK_TYPE_TO_STOCK_TYPE | 03-Nov-2020 09:40 AM | hr1       | 0002 - Signmedia |
      | 2020000005 | Draft | STOCK_TYPE_TO_STOCK_TYPE | 06-Feb-2020 09:40 PM | Niveen    | 0002 - Signmedia |

    And the following StockTransformationDetails section for StockTransformations exist:
      | Code       | RefStoreTransaction | Qty | NewStockType     | StockTransformationReason           |
      | 2020000001 | 2019000001          | 2   | UNRESTRICTED_USE | 0002 - Item will be sold            |
      | 2020000005 | 2019000016          | 1   | DAMAGED_STOCK    | 0001 - Items damaged during storage |

  #EBS-6357 #EBS-6164 #EBS-7417
  Scenario: (01) Activate draft StockTransformation - by an authorized user with Transformation from Unrestricted to Damaged with the same cost (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And the Last StoreTransaction code is "2020000033"
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000001" at "12-Feb-2020 09:40 AM"
    Then the RemainingQty in the following StoreTransaction is updated as follows:
      | STCode     | LastUpdateDate       | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType     | Item                                         | UOM                 | Qty | EstimateCost | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction |
      | 2019000001 | 12-Feb-2020 09:40 AM | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10  | 200          | 200        | 2019000088 - Goods Receipt | 8            |                |
    And the following StoreTransactions are created:
      | STCode     | CreationDate         | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item                                         | UOM                 | Qty  | EstimateCost | ActualCost | ReferenceDocument                 | RemainingQty | RefTransaction |
      | 2020000034 | 12-Feb-2020 09:40 AM |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 2.00 |              |            | 2020000001 - Stock Transformation | 0.00         | 2019000001     |
      | 2020000035 | 12-Feb-2020 09:40 AM | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 2.00 | 200.00       | 200.00     | 2020000001 - Stock Transformation | 2.00         | 2019000001     |
    And StockTransformation with Code "2020000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  | NewStoreTransaction |
      | Gehan.Ahmed   | 12-Feb-2020 09:40 AM | Active | 2020000035          |

    And a success notification is sent to "Gehan.Ahmed" with the following message "TRAN-MSG-03"

  #EBS-6357 #EBS-6164 #EBS-7417
  Scenario: (02) Activate draft StockTransformation - by an authorized user with Transformation from Damaged to Unrestricted with the same cost (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And the Last StoreTransaction code is "2020000033"
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000005" at "12-Feb-2020 09:40 AM"
    Then the RemainingQty in the following StoreTransaction is updated as follows:
      | STCode     | LastUpdateDate       | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item          | UOM                  | Qty  | EstimateCost | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction |
      | 2019000016 | 12-Feb-2020 09:40 AM | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000051 - Ink5 | 0040 - Bottle-1Liter | 2.00 | 100.00       | 100.00     | 2019000020 - Goods Receipt | 0.00         |                |
    And the following StoreTransactions are created:
      | STCode     | CreationDate         | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item          | UOM                  | Qty  | EstimateCost | ActualCost | ReferenceDocument                 | RemainingQty | RefTransaction |
      | 2020000034 | 12-Feb-2020 09:40 AM |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000051 - Ink5 | 0040 - Bottle-1Liter | 1.00 |              |            | 2020000005 - Stock Transformation | 0.00         | 2019000016     |
      | 2020000035 | 12-Feb-2020 09:40 AM | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000051 - Ink5 | 0040 - Bottle-1Liter | 1.00 | 100.00       | 100.00     | 2020000005 - Stock Transformation | 1.00         | 2019000016     |
    And StockTransformation with Code "2020000005" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  | NewStoreTransaction |
      | Gehan.Ahmed   | 12-Feb-2020 09:40 AM | Active | 2020000035          |
    And a success notification is sent to "Gehan.Ahmed" with the following message "TRAN-MSG-03"
