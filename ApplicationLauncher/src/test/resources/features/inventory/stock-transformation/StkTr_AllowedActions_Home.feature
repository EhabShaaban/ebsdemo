# Author: Hossam Hassan
# Reviewer: Somaya

Feature: Allowed Action For Stock Transformation (Home Screen)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Afaf              | FrontDesk             |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Gehan.Ahmed       | Marketeer_Signmedia   |
      | CreateOnlyUser    | CreateOnlyRole        |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                             |
      | Storekeeper_Signmedia | StockTransformationOwner_Signmedia  |
      | Storekeeper_Signmedia | StockTransformationViewer_Signmedia |
      | Marketeer_Signmedia   | StockTransformationViewer_Signmedia |
      | CreateOnlyRole        | CreateOnlySubRole                   |

    And the following sub-roles and permissions exist:
      | Subrole                             | Permission                  | Condition                      |
      | StockTransformationOwner_Signmedia  | StockTransformation:Create  |                                |
      | StockTransformationViewer_Signmedia | StockTransformation:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole                   | StockTransformation:Create  |                                |

    And the following users doesn't have the following permissions:
      | User           | Permission                  |
      | Gehan.Ahmed    | StockTransformation:Create  |
      | CreateOnlyUser | StockTransformation:ReadAll |
      | Afaf           | StockTransformation:ReadAll |
      | Afaf           | StockTransformation:Create  |

  #EBS-6156
  Scenario: (01) Read allowed actions in StockTransformation Home Screen - User is authorized to Create & ReadAll  (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read actions of StockTransformation in home screen
    Then the following actions are displayed to "Mahmoud.Abdelaziz":
      | AllowedActions |
      | ReadAll        |
      | Create         |

  #EBS-6156
  Scenario: (02) Read allowed actions in StockTransformation Home Screen - User is authorized to ReadAll  (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read actions of StockTransformation in home screen
    Then the following actions are displayed to "Gehan.Ahmed":
      | AllowedActions |
      | ReadAll        |

  #EBS-6156
  Scenario: (04) Read allowed actions in StockTransformation Home Screen - User is authorized to Create only (Happy Path)
    Given user is logged in as "CreateOnlyUser"
    When "CreateOnlyUser" requests to read actions of StockTransformation in home screen
    Then the following actions are displayed to "CreateOnlyUser":
      | AllowedActions |
      | Create         |

  #EBS-6156
  Scenario: (05) Read allowed actions in StockTransformation Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of StockTransformation in home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"