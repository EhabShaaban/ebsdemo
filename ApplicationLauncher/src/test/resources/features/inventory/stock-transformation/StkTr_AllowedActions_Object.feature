#created by Eman Hesham 2/3/2020
Feature: Allowed Action For Stock Transformation (Object Screen)

  Background:
    Given the following users and roles exist:
      | Name        | Role                |
      | Afaf        | FrontDesk           |
      | Gehan.Ahmed | Marketeer_Signmedia |
      | Ali.Hasan   | Storekeeper_Flexo   |
    And the following roles and sub-roles exist:
      | Role                | Subrole                             |
      | Marketeer_Signmedia | StockTransformationViewer_Signmedia |
      | Marketeer_Signmedia | Activator_Signmedia                 |
      | Storekeeper_Flexo   | StockTransformationViewer_Flexo     |
    And the following sub-roles and permissions exist:
      | Subrole                             | Permission                   | Condition                      |
      | StockTransformationViewer_Signmedia | StockTransformation:ReadAll  | [purchaseUnitName='Signmedia'] |
      | StockTransformationViewer_Flexo     | StockTransformation:ReadAll  | [purchaseUnitName='Flexo']     |
      | Activator_Signmedia                 | StockTransformation:Activate | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User        | Permission                   |
      | Gehan.Ahmed | StockTransformation:Create   |
      | Afaf        | StockTransformation:ReadAll  |
      | Afaf        | StockTransformation:Create   |
      | Afaf        | StockTransformation:Activate |

    And the following StockTransformations exist:
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason | State  | BusinessUnit     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold  | Draft  | 0002 - Signmedia |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold  | Active | 0002 - Signmedia |

  # EBS-6326
  Scenario Outline: (01) Read allowed actions in StockTransformation Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of StockTransformation with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User        | Code       | AllowedActions   |
      | Gehan.Ahmed | 2020000001 | ReadAll,Activate |
      | Gehan.Ahmed | 2020000003 | ReadAll          |

 #EBS-6326
  Scenario Outline: (02) Read allowed actions in StockTransformation Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of StockTransformation with code "<Code>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User      | Code       |
      | Ali.Hasan | 2020000001 |
      | Afaf      | 2020000003 |


  @Future
  # EBS-6326
  Scenario: (03) Read allowed actions in StockTransformation Object Screen - Object does not exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the StockTransformation with code "2020000002" successfully
    When "Mahmoud.Abdelaziz" requests to read actions of StockTransformation with code "2020000001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"
