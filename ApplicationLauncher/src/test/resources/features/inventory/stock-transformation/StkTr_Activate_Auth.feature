# Author: Niveen Magdy on 24/3/2020, Reviewer: Somaya Ahmed

Feature: StockTransformation Activate - Authorization

  Background:
    Given the following users and roles exist:
      | Name              | Role            |
      | Assem.Abdelnasser | Marketeer_Flexo |
    And the following roles and sub-roles exist:
      | Role            | Subrole                            |
      | Marketeer_Flexo | StockTransformationViewer_Flexo    |
      | Marketeer_Flexo | StockTransformationActivator_Flexo |
    And the following sub-roles and permissions exist:
      | Subrole                            | Permission                   | Condition                  |
      | StockTransformationViewer_Flexo    | StockTransformation:ReadAll  | [purchaseUnitName='Flexo'] |
      | StockTransformationActivator_Flexo | StockTransformation:Activate | [purchaseUnitName='Flexo'] |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                   | Condition                      |
      | Assem.Abdelnasser | StockTransformation:Activate | [purchaseUnitName='Signmedia'] |

  # EBS-6359
  Scenario: (01) Activate StockTransformation - in state draft by an an unauthorized user due to condition  (Abuse Case)
    Given user is logged in as "Assem.Abdelnasser"
    When "Assem.Abdelnasser" Activates StockTransformation with code "2020000050" at "10-Jan-2020 09:30 AM"
    Then "Assem.Abdelnasser" is logged out
    And "Assem.Abdelnasser" is forwarded to the error page
