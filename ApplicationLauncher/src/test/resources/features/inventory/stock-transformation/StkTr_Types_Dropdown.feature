#Author: Hossam Hassan

Feature: View All Stock Transformation Types

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |

    And the following roles and sub-roles exist:
      | Role                  | Sub-role                      |
      | Storekeeper_Signmedia | StockTransformationTypeViewer |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                      | Condition |
      | StockTransformationTypeViewer | StockTransformationType:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User | Permission                      |
      | Afaf | StockTransformationType:ReadAll |

    And the following StockTransformationTypes exist:
      | Code                     | Name                     |
      | STOCK_TYPE_TO_STOCK_TYPE | Stock Type to Stock Type |
    And the total number of existing StockTransformation Types are 1


    #EBS-5725
  Scenario: (01) Read list of StockTransformationTypes dropdown by authorized user with no condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all StockTransformationTypes
    Then the following StockTransformation Types values will be presented to "Mahmoud.Abdelaziz":
      | Type                     |
      | Stock Type to Stock Type |
    And total number of StockTransformation Types returned to "Mahmoud.Abdelaziz" is equal to 1

    #EBS-5725
  Scenario: (02) Read list of StockTransformationTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all StockTransformationTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
