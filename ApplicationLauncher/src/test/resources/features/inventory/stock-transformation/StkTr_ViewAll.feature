#Author: Niveen Magdy
#Reviewer: Somaya and Hosam

Feature: View All StockTransformation

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Ahmed.Seif        | Quality_Specialist    |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                             |
      | Quality_Specialist    | StockTransformationViewer           |
      | Storekeeper_Signmedia | StockTransformationViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                             | Permission                  | Condition                      |
      | StockTransformationViewer           | StockTransformation:ReadAll |                                |
      | StockTransformationViewer_Signmedia | StockTransformation:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission                  |
      | Afaf | StockTransformation:ReadAll |

    And the following StoreTransactions exist:
      | STCode     | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item                                          | UOM          | Qty | EstimateCost | ActualCost | ReferenceDocument                 | RemainingQty | RefTransaction |
      | 2019000017 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter | 8   | 120          | 120        | Legacy System نظام المتكامل       | 3            |                |
      | 2020000006 | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0002 - Signmedia | DAMAGED_STOCK    | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 15  | 0            | 0          | 2020000004 - Goods Receipt        | 13           |                |
      | 2020000011 | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter | 5   |              |            | 2020000002 - Stock Transformation | 0            | 2019100007     |
      | 2020000012 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0001 - Flexo     | DAMAGED_STOCK    | 000051 - Ink5                                 | 0014 - Liter | 5   | 120          | 120        | 2020000002 - Stock Transformation | 5            | 2019100007     |
      | 2020000013 | TAKE            | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0002 - Signmedia | DAMAGED_STOCK    | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 2   |              |            | 2020000003 - Stock Transformation | 0            | 2020000006     |
      | 2020000014 | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 2   | 0            | 0          | 2020000003 - Stock Transformation | 2            | 2020000006     |

    And the following StockTransformations exist:
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold            | Draft  | 0002 - Signmedia |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold            | Active | 0002 - Signmedia |
    And total number of StockTransformations records is 3

#  EBS-6158
  Scenario: (01) View all StockTransformations - by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with no filter applied with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold            | Active | 0002 - Signmedia |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold            | Draft  | 0002 - Signmedia |
    And the total number of records in search results by "Ahmed.Seif" are 3

#  EBS-6158
  Scenario: (02) View all StockTransformations - by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 2 of StockTransformations with no filter applied with 2 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason | State | BusinessUnit     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold  | Draft | 0002 - Signmedia |
    And the total number of records in search results by "Ahmed.Seif" are 3

#  EBS-6158
  Scenario: (03) View all StockTransformations - by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of StockTransformations with no filter applied with 20 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold  | Active | 0002 - Signmedia |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold  | Draft  | 0002 - Signmedia |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 2

      #  EBS-6158
  Scenario: (04) View all StockTransformations - Filter StockTransformations by Code by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on Code which contains "0002" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo |
    And the total number of records in search results by "Ahmed.Seif" are 1

   #  EBS-6158
  Scenario: (04) View all StockTransformations - Filter StockTransformations by Type by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on Type which contains "Stock" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold            | Active | 0002 - Signmedia |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold            | Draft  | 0002 - Signmedia |
    And the total number of records in search results by "Ahmed.Seif" are 3

  #  EBS-6158
  Scenario: (05) View all StockTransformations - Filter StockTransformations by RefStoreTransactionCode by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on RefStoreTransactionCode which contains "000001" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason | State | BusinessUnit     |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold  | Draft | 0002 - Signmedia |
    And the total number of records in search results by "Ahmed.Seif" are 1

  #  EBS-6158
  Scenario: (06) View all StockTransformations - Filter StockTransformations by NewStoreTransactionCode by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on NewStoreTransactionCode which contains "2020" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold            | Active | 0002 - Signmedia |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo     |
    And the total number of records in search results by "Ahmed.Seif" are 2

  #  EBS-6158
  Scenario: (07) View all StockTransformations - Filter StockTransformations by State by authorized user using "contains" - SuperUser (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on State which contains "Active" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason           | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold            | Active | 0002 - Signmedia |
      | 2020000002 | STOCK_TYPE_TO_STOCK_TYPE | 2019000017          | 2020000012          | 0001 - Items damaged during storage | Active | 0001 - Flexo     |
    And the total number of records in search results by "Ahmed.Seif" are 2

    #  EBS-6158
  Scenario: (08) View all StockTransformations - Filter StockTransformations by BusinessUnit by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockTransformations with filter applied on BusinessUnit which equals "0002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | Type                     | RefStoreTransaction | NewStoreTransaction | StockTransformationReason | State  | BusinessUnit     |
      | 2020000003 | STOCK_TYPE_TO_STOCK_TYPE | 2020000006          | 2020000014          | 0002 - Item will be sold  | Active | 0002 - Signmedia |
      | 2020000001 | STOCK_TYPE_TO_STOCK_TYPE | 2019000001          |                     | 0002 - Item will be sold  | Draft  | 0002 - Signmedia |
    And the total number of records in search results by "Ahmed.Seif" are 2

 #  EBS-6158
  Scenario: (09) View all StockTransformations - by Code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of StockTransformations with no filter applied with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page