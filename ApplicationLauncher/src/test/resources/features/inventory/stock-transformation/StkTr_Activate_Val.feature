# Author: Ahmad Hamed & Engy Mohamed
# Reviewer: Somaya Ahmed and Hosam Bayomy

Feature: StockTransformation Activate - Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                |
      | Gehan.Ahmed | Marketeer_Signmedia |

    And the following roles and sub-roles exist:
      | Role                | Subrole                                |
      | Marketeer_Signmedia | StockTransformationViewer_Signmedia    |
      | Marketeer_Signmedia | StockTransformationActivator_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                                | Permission                   | Condition                      |
      | StockTransformationViewer_Signmedia    | StockTransformation:ReadAll  | [purchaseUnitName='Signmedia'] |
      | StockTransformationActivator_Signmedia | StockTransformation:Activate | [purchaseUnitName='Signmedia'] |

    And the following GeneralData section for StockTransformations exist:
      | Code       | State  | Type                     | CreationDate         | CreatedBy | BusinessUnit     |
      | 2020000049 | Draft  | STOCK_TYPE_TO_STOCK_TYPE | 03-Nov-2020 09:40 AM | A.Hamed   | 0002 - Signmedia |
      | 2020000048 | Active | STOCK_TYPE_TO_STOCK_TYPE | 03-Nov-2020 09:40 AM | A.Hamed   | 0002 - Signmedia |

    And the following StockTransformationDetails section for StockTransformations exist:
      | Code       | RefStoreTransaction | Qty | NewStockType  | StockTransformationReason           |
      | 2020000049 | 2019000016          | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | 2020000048 | 2019000014          | 2.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |

    And the RefStoreTransaction in above StockTransformations has the following data:
      | Code       | StockType        | BusinessUnit     | RemainingQty |
      | 2019000016 | UNRESTRICTED_USE | 0002 - Signmedia | 1.0          |
      | 2019000014 | UNRESTRICTED_USE | 0002 - Signmedia | 10.0         |

 #EBS-6358
  Scenario: (01) Activate StockTransformation - in state draft, where StockTransformation has quantity that exceeds remaining quantity in StoreTransaction for a specific business unit(Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000049" at "01-Jan-2020 09:30 AM"
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "TRAN-MSG-02"

  #EBS-6358
  Scenario: (02) Activate StockTransformation - in an invalid state - Active State (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000048" at "01-Jan-2020 09:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"

  @Future
  Scenario: (03) Activate StockTransformation - where StockTransformation doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the StockTransformation with code "2020000050" successfully
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000050" at "20-Nov-2019 09:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  @Future
  Scenario: (04) Activate StockTransformation - in state draft, where StockTransformation that has sections locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<sectionName>" section of StockTransformation with code "2019000004" in edit mode at "07-Jan-2019 9:20 AM"
    When "Gehan.Ahmed" Activates StockTransformation with code "2020000050" at "20-Nov-2019 09:30 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"