#Author: Hossam Hassan
Feature: View All Stock Transformation Reasons

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |

    And the following roles and sub-roles exist:
      | Role                  | Sub-role                        |
      | Storekeeper_Signmedia | StockTransformationReasonViewer |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                        | Condition |
      | StockTransformationReasonViewer | StockTransformationReason:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User | Permission                        |
      | Afaf | StockTransformationReason:ReadAll |

    And the following StockTransformationReasons exist:
      | Code | Name                         | RelatedStockTransformationType | FromStockType    | ToStockType      |
      | 0002 | Item will be sold            | STOCK_TYPE_TO_STOCK_TYPE       | DAMAGED_STOCK    | UNRESTRICTED_USE |
      | 0001 | Items damaged during storage | STOCK_TYPE_TO_STOCK_TYPE       | UNRESTRICTED_USE | DAMAGED_STOCK    |
    And the total number of existing StockTransformationReasons are 2

  #EBS-6154
  Scenario: (01) Read list of StockTransformationReason dropdown by authorized user with no condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all StockTransformationReason based on StockTransformationType with code "STOCK_TYPE_TO_STOCK_TYPE" from "UNRESTRICTED_USE" to "DAMAGED_STOCK"
    Then the following StockTransformationReason values will be presented to "Mahmoud.Abdelaziz":
      | StockTransformationReason           |
      | 0001 - Items damaged during storage |
    And total number of StockTransformationReason returned to "Mahmoud.Abdelaziz" is equal to 1

  #EBS-6154
  Scenario: (02) Read list of StockTransformationReason dropdown by authorized user with no condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all StockTransformationReason based on StockTransformationType with code "STOCK_TYPE_TO_STOCK_TYPE" from "DAMAGED_STOCK" to "UNRESTRICTED_USE"
    Then the following StockTransformationReason values will be presented to "Mahmoud.Abdelaziz":
      | StockTransformationReason |
      | 0002 - Item will be sold  |
    And total number of StockTransformationReason returned to "Mahmoud.Abdelaziz" is equal to 1

  #EBS-6154
  Scenario: (03) Read list of StockTransformationReason dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all StockTransformationReason based on StockTransformationType with code "STOCK_TYPE_TO_STOCK_TYPE" from "DAMAGED_STOCK" to "UNRESTRICTED_USE"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
