#Author: Ahmad Hamed & Engy Mohamed
#Reviewer: Somaya Ahmed and Hosam Bayomy

Feature: Create Stock Transformation - Happy Path

  Background:

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                             |
      | Storekeeper_Signmedia | StockTransformationOwner_Signmedia  |
      | Storekeeper_Signmedia | StockTransformationViewer_Signmedia |
      | Storekeeper_Signmedia | StockTransformationTypeViewer       |
      | Storekeeper_Signmedia | StockTransformationReasonViewer     |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia             |
      | Storekeeper_Signmedia | StoreTransactionViewer_Signmedia    |
      | Storekeeper_Signmedia | StockTypesViewer                    |

    And the following sub-roles and permissions exist:
      | Subrole                             | Permission                        | Condition                      |
      | StockTransformationOwner_Signmedia  | StockTransformation:Create        |                                |
      | StockTransformationViewer_Signmedia | StockTransformation:ReadAll       | [purchaseUnitName='Signmedia'] |
      | StockTransformationTypeViewer       | StockTransformationType:ReadAll   |                                |
      | StockTransformationReasonViewer     | StockTransformationReason:ReadAll |                                |
      | PurUnitReader_Signmedia             | PurchasingUnit:ReadAll            | [purchaseUnitName='Signmedia'] |
      | StoreTransactionViewer_Signmedia    | StoreTransactions:ReadAll         | [purchaseUnitName='Signmedia'] |
      | StockTypesViewer                    | StockType:ReadAll                 |                                |

    And the following StockTransformationTypes exist:
      | Code                     | Name                     |
      | STOCK_TYPE_TO_STOCK_TYPE | Stock Type to Stock Type |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |

    And the following StoreTransactions exist:
      | STCode     | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item                                         | UOM                 | Qty   | EstimateCost | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction |
      | 2019000007 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.50 | 20.00        | 20.00      | 2019000088 - Goods Receipt | 10.50        |                |

    And the following StockTransformationReasons exist:
      | Code | Name                         | RelatedStockTransformationType | FromStockType    | ToStockType   |
      | 0001 | Items damaged during storage | STOCK_TYPE_TO_STOCK_TYPE       | UNRESTRICTED_USE | DAMAGED_STOCK |

    #EBS-6154
  Scenario Outline: (01) Create StockTransformation - with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created StockTransformation was with code "2020000050"
    When "Mahmoud.Abdelaziz" creates StockTransformation with the following values:
      | Type                     | BusinessUnit     | RefStoreTransaction | Qty   | NewStockType  | StockTransformationReason           |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | <Qty> | DAMAGED_STOCK | 0001 - Items damaged during storage |
    Then a new StockTransformation is created with the following values:
      | Code       | State | CreatedBy         | LastUpdatedBy     | CreationDate         | LastUpdateDate       | Type                     | BusinessUnit     |
      | 2020000051 | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | 01-Jan-2020 11:00 AM | 01-Jan-2020 11:00 AM | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia |
    And the StockTransformationDetails Section of StockTransformation with code "2020000051" is created as follows:
      | RefStoreTransaction | Qty   | NewStockType  | StockTransformationReason           |
      | 2019000007          | <Qty> | DAMAGED_STOCK | 0001 - Items damaged during storage |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"
    Examples:
      | Qty  |
      | 5.0  |
      | 10.0 |