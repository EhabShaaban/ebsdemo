#Author: Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Ahmed, Hosam Bayomy

Feature: Create Stock Transformation - Validation

  Background:

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                             |
      | Storekeeper_Signmedia | StockTransformationOwner_Signmedia  |
      | Storekeeper_Signmedia | StockTransformationViewer_Signmedia |
      | Storekeeper_Signmedia | StockTransformationTypeViewer       |
      | Storekeeper_Signmedia | StockTransformationReasonViewer     |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia             |
      | Storekeeper_Signmedia | StoreTransactionViewer_Signmedia    |
      | Storekeeper_Signmedia | StockTypesViewer                    |

    And the following sub-roles and permissions exist:
      | Subrole                             | Permission                        | Condition                      |
      | StockTransformationOwner_Signmedia  | StockTransformation:Create        |                                |
      | StockTransformationViewer_Signmedia | StockTransformation:ReadAll       | [purchaseUnitName='Signmedia'] |
      | StockTransformationTypeViewer       | StockTransformationType:ReadAll   |                                |
      | StockTransformationReasonViewer     | StockTransformationReason:ReadAll |                                |
      | PurUnitReader_Signmedia             | PurchasingUnit:ReadAll            | [purchaseUnitName='Signmedia'] |
      | StoreTransactionViewer_Signmedia    | StoreTransactions:ReadAll         | [purchaseUnitName='Signmedia'] |
      | StockTypesViewer                    | StockType:ReadAll                 |                                |

    And the following StockTransformationTypes exist:
      | Code                     | Name                     |
      | STOCK_TYPE_TO_STOCK_TYPE | Stock Type to Stock Type |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |

    And the following StoreTransactions exist:
      | STCode     | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item                                         | UOM                 | Qty   | EstimateCost | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction |
      | 2019000007 | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.50 | 20.00        | 20.00      | 2019000088 - Goods Receipt | 10.50        |                |

    And the following StockTransformationReasons exist:
      | Code | Name                         | RelatedStockTransformationType | FromStockType    | ToStockType      |
      | 0001 | Items damaged during storage | STOCK_TYPE_TO_STOCK_TYPE       | UNRESTRICTED_USE | DAMAGED_STOCK    |
      | 0002 | Item will be sold            | STOCK_TYPE_TO_STOCK_TYPE       | DAMAGED_STOCK    | UNRESTRICTED_USE |

    #EBS-6155
  Scenario Outline: (01) Create StockTransformation - with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates StockTransformation with the following values:
      | Type   | BusinessUnit   | RefStoreTransaction   | Qty   | NewStockType   | StockTransformationReason   |
      | <Type> | <BusinessUnit> | <RefStoreTransaction> | <Qty> | <NewStockType> | <StockTransformationReason> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | Type                     | BusinessUnit     | RefStoreTransaction | Qty | NewStockType  | StockTransformationReason           |
      |                          | 0002 - Signmedia | 2019000007          | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | ""                       | 0002 - Signmedia | 2019000007          | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE |                  | 2019000007          | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | ""               | 2019000007          | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia |                     | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | ""                  | 5.0 | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          |     | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | ""  | DAMAGED_STOCK | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0 |               | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0 | ""            | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0 | DAMAGED_STOCK |                                     |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0 | DAMAGED_STOCK | ""                                  |

  Scenario Outline: (02) Create StockTransformation - with incorrect data entry (Validation Failure)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates StockTransformation with the following values:
      | Type   | BusinessUnit   | RefStoreTransaction   | Qty   | NewStockType   | StockTransformationReason   |
      | <Type> | <BusinessUnit> | <RefStoreTransaction> | <Qty> | <NewStockType> | <StockTransformationReason> |
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Mahmoud.Abdelaziz"
    Examples:
      | Type                     | BusinessUnit                     | RefStoreTransaction | Qty  | NewStockType           | StockTransformationReason           | Field                    | ErrMsg      |
      # purchase unit does not exist
      | STOCK_TYPE_TO_STOCK_TYPE | 9999 - Non Existing BusinessUnit | 2019000007          | 5.0  | DAMAGED_STOCK          | 0001 - Items damaged during storage | purchaseUnitCode         | Gen-msg-48  |
       # stocktype does not exist
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia                 | 2019000007          | 5.0  | NON_EXISTING_STOCKTYPE | 0001 - Items damaged during storage | newStockType             | Gen-msg-48  |
      # Reason does not exist
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia                 | 2019000007          | 5.0  | DAMAGED_STOCK          | 9999 - non Existing Reason          | transformationReasonCode | Gen-msg-48  |
      # Qty is more than storetransaction remaining qty
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia                 | 2019000007          | 11.0 | DAMAGED_STOCK          | 0001 - Items damaged during storage | transformedQty           | TRAN-MSG-02 |

  Scenario Outline: (03) Create StockTransformation - with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates StockTransformation with the following values:
      | Type   | BusinessUnit   | RefStoreTransaction   | Qty   | NewStockType   | StockTransformationReason   |
      | <Type> | <BusinessUnit> | <RefStoreTransaction> | <Qty> | <NewStockType> | <StockTransformationReason> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | Type                     | BusinessUnit     | RefStoreTransaction | Qty    | NewStockType     | StockTransformationReason           |
      | ay7aga                   | 0002 - Signmedia | 2019000007          | 5.0    | DAMAGED_STOCK    | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | ay7aga - ay7aga  | 2019000007          | 5.0    | DAMAGED_STOCK    | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | ay7aga              | 5.0    | DAMAGED_STOCK    | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | ay7aga | DAMAGED_STOCK    | 0001 - Items damaged during storage |
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0    | DAMAGED_STOCK    | ay7aga - ay7aga                     |
      # create sTKtR with QTY 4 digits after dot
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.2563 | DAMAGED_STOCK    | 0001 - Items damaged during storage |
      # create sTKtR with non matching reason and stocktype
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0    | DAMAGED_STOCK    | 0002 - Item will be sold            |
      # create sTKtR with the same stocktype
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 2019000007          | 5.0    | UNRESTRICTED_USE | 0001 - Items damaged during storage |
       # storetransaction does not exist
      | STOCK_TYPE_TO_STOCK_TYPE | 0002 - Signmedia | 9999999999          | 5.0    | DAMAGED_STOCK    | 0001 - Items damaged during storage | 
    


