Feature: Add Item Detail validation in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ali.Hasan         | Storekeeper_Corrugated |
    And the following roles and sub-roles exist:
      | Role                   | Subrole            |
      | Storekeeper_Signmedia  | GROwner_Signmedia  |
      | Storekeeper_Flexo      | GROwner_Flexo      |
      | Storekeeper_Corrugated | GROwner_Corrugated |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:UpdateItem | [purchaseUnitName='Corrugated'] |
    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |
    And GoodsReceipts "2018000008" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
      | 000003   |
    And Item "000001" in GoodsReceipt (Based On PurchaseOrder) "2018000008" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes          | StockType     |
      | 40.123456        | 0029 | Package Opened | DAMAGED_STOCK |
      | 10.123456        | 0032 | Package Opened | DAMAGED_STOCK |
    And Item "000003" in GoodsReceipt "2018000008" has the following batches:
      | BatchNo  | ProdDate    | ExpireDate  | BatchQty(UoE) | UoE  | StockType        | Notes                   |
      | 14536444 | 23-Jan-2018 | 23-Jan-2020 | 2.123456      | 0036 | UNRESTRICTED_USE |                         |
      | 14536777 | 25-Jan-2018 | 25-Jan-2020 | 9.123456      | 0036 | DAMAGED_STOCK    | Crashed while discharge |
    And the following SalesReturnData for GoodsReceipts exist:
      | GRCode     | SalesReturn | SalesReturnDocumentOwner        | SalesReturnCustomer      | Notes |
      | 2020000020 | 2020000002  | Manar.Mohammed - Manar Mohammed | 000006 - المطبعة الأمنية |       |
    And GoodsReceipts "2020000020" has the following ReceivedItems:
      | ItemCode |
      | 000051   |
    And Item "000051" in GoodsReceipt (Based On Sales Return) "2020000020" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 5.0         | 0029 |       | UNRESTRICTED_USE |
    And the following SalesReturnOrders have the following Items:
      | Code       | SROItemId | Item          | OrderUnit           | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
      | 2020000002 | 2         | 000051 - Ink5 | 0029 - Roll 2.20x50 | 0001 - Problem in product operation | 10.000         | 0.000             | 22000.000  | 220000.000      |


  #Failure Cases ###
  #EBS-4763
  Scenario Outline: (01) Add GR Item Detail Val - to GoodsReceipt with Received Qty unit that does not have an IVR or deleted (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to unitOfEntryCode field "GR-msg-11" and sent to "<User>"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 9999 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |

    #EBS-4838 #EBS-6770
  Scenario Outline: (02) Add GR Item Detail Val - to GoodsReceipt with the same Qty unit and StockType (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message "GR-msg-14" is returned and sent to "<User>"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType        | Notes |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |          |            | 30.123456        | 0032 | DAMAGED_STOCK    |       |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |          |            | 5.123456         | 0029 | UNRESTRICTED_USE |       |

  #EBS-6443
  Scenario Outline: (03) Add GR Item Detail Val - to GoodsReceipt with the same Qty, UOE, Batch NO. and StockType (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message "GR-msg-15" is returned and sent to "<User>"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo  | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType     | Notes                   |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 14536777 | 25-Jan-2018 | 25-Jan-2020 | 9.123456         | 0036 | DAMAGED_STOCK | Crashed while discharge |

  #Exception Cases ###
  #EBS-4763 #EBS-6770
  Scenario Outline: (04) Add GR Item Detail Val - to GoodsReceipt after edit session expires (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:00 AM"
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:31 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 5.123456         | 0014 | DAMAGED_STOCK    |                                        |

  #EBS-4763 #EBS-6770
  Scenario Outline: (05) Add GR Item Detail Val - to GoodsReceipt after edit session expires and GoodsReceipt does not exist  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully at "07-Jan-2019 09:31 AM"
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 5.123456         | 0014 | DAMAGED_STOCK    |                                        |

  #EBS-4763
  Scenario Outline: (06) Add GR Item Detail Val - to GoodsReceipt after edit session expires and item does not exist  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully at "07-Jan-2019 09:31 AM"
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities |

        #EBS-6770
  Scenario Outline: (07) Add GR Item Detail Val - to GoodsReceipt with Qty exceeds Qty in SalesReturn  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2020 09:00 AM"
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then the following error message "GR-msg-17" is returned and sent to "<User>"
    Examples:
      | GRCode     | ItemCode | User      | DetailType | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType     | Notes |
      | 2020000020 | 000051   | Ali.Hasan | Quantity   |         |          |            | 6.123456         | 0029 | DAMAGED_STOCK |       |

  #Abuse Cases ###
  #EBS-4763 #EBS-6770
  Scenario Outline: (08) Add GR Item Detail Val - to GoodsReceipt with missing mandatory fields (abuse case)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             |                  | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        |      | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 0032 |                  | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      |         | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 |             | 29-Jan-2019 | 50.123456        | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 |             | 50.123456        | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 |                  | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        |      | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0034 |                  | Samples taken by clearance authorities |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             |                  | 0014 | DAMAGED_STOCK    |                                        |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 5.123456         |      | DAMAGED_STOCK    |                                        |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 5.123456         | 0014 |                  |                                        |

  #EBS-4763 #EBS-6770
  Scenario Outline: (09) Add GR Item Detail Val - to GoodsReceipt with malicious input (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo                                                                                               | ProdDate    | ExpireDate  | ReceivedQty(UoE)  | UoE   | StockType        | Notes                                                                                                                                                                                                                                                       |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 1000000001.123456 | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 0.0               | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | -1.0              | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | ay7aga            | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 00032 | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 0032  | ay7aga           | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 0032  | UNRESTRICTED_USE | Package*Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 0032  | UNRESTRICTED_USE | Package$Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 0032  | UNRESTRICTED_USE | Package\Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |                                                                                                       |             |             | 30.0              | 0032  | UNRESTRICTED_USE | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTesttTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestttTestTestTestTestTestTestTestTestTestTestTestTest |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | @ay7aga                                                                                               | 19-Jan-2019 | 29-Jan-2019 | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestt | 19-Jan-2019 | 29-Jan-2019 | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645                                                                                               | ay7aga      | 29-Jan-2019 | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645                                                                                               | 19-01-2019  | 29-Jan-2019 | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645                                                                                               | 19-Jan-2019 | ay7aga      | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645                                                                                               | 19-Jan-2019 | 29-01-2019  | 50.0              | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 1000000001.0      | 0014  | DAMAGED_STOCK    |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 0.0               | 0014  | DAMAGED_STOCK    |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | -1.0              | 0014  | DAMAGED_STOCK    |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | ay7aga            | 0014  | DAMAGED_STOCK    |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 00014 | DAMAGED_STOCK    |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 0014  | ay7aga           |                                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 0014  | DAMAGED_STOCK    | Package*Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 0014  | DAMAGED_STOCK    | Package$Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 0014  | DAMAGED_STOCK    | Package\Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |                                                                                                       |             |             | 5                 | 0014  | DAMAGED_STOCK    | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTesttTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestttTestTestTestTestTestTestTestTestTestTestTestTest |

  #EBS-4763
  Scenario Outline: (10) Add GR Item Detail Val - to GoodsReceipt with ExpirationDate Before ProductionDate (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 30-Jan-2019 | 29-Jan-2019 | 50.0             | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |

