Feature: Delete Item in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | hr1               | SuperUser             |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ahmed.Seif        | Quality_Specialist    |
    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
      | Quality_Specialist    | GRViewer          |
      | SuperUser             | *                 |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GRViewer          | GoodsReceipt:ReadAll    |                                |
    And the following GoodsReceipts exist:
      | Code       | State  | Type                                                   | BusinessUnit     |
      | 2018000008 | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia |
      | 2018000002 | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia |
      | 2018000003 | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0001 - Flexo     |
    And GoodsReceipts "2018000008" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
      | 000003   |
    And GoodsReceipts "2018000002" has the following ReceivedItems:
      | ItemCode |
      | 000001   |

  Scenario: (1) Delete GR Item - by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000001" in GR with code "2018000008" at "07-Jan-2019 09:30 am"
    Then Item with code "000001" from GR with code "2018000008" is deleted
    And GoodsReceipt with Code "2018000008" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       |
      | Mahmoud.Abdelaziz | 07-Jan-2019 09:30 am |
    And the lock by "Mahmoud.Abdelaziz" on ReceivedItems in GR with code "2018000008" section is released
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-12"

  @ToBeUpdatedAfterActivateFeature
  Scenario: (2) Delete GR Item - when Delete Item action is not allowed (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000001" in GR with code "2018000002"
    Then the lock by "Mahmoud.Abdelaziz" on ReceivedItems in GR with code "2018000002" section is released
    And an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-21"

  Scenario: (3) Delete GR Item - when GR doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000001" in GR with code "2018000008"
    Then the lock by "Mahmoud.Abdelaziz" on ReceivedItems in GR with code "2018000008" section is released
    And an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (4) Delete GR Item - when Item doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000001" of GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000001" in GR with code "2018000008"
    Then the lock by "Mahmoud.Abdelaziz" on ReceivedItems in GR with code "2018000008" section is released
    And an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"

  Scenario: (5) Delete GR Item - when received items are locked by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first request to add item to ReceivedItems in GR with code "2018000008" and new record is opened in edit mode successfully
    When "Mahmoud.Abdelaziz" requests to delete Item with code "000001" in GR with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  Scenario Outline: (6) Delete GR Item - by an unauthorized user (Unauthorized access/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete Item with code "<ItemCode>" in GR with code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | GRCode     | ItemCode |
      | Ahmed.Seif        | 2018000008 | 000001   |
      | Mahmoud.Abdelaziz | 2018000003 | 000001   |
