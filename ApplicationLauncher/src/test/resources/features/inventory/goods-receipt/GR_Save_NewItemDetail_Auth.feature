Feature: Add Item Detail in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name                           | Role                                       |
      | Shady.Abdelatif                | Accountant_Signmedia                       |
      | Ali.Hasan                      | Storekeeper_Flexo                          |
      | Mahmoud.Abdelaziz              | Storekeeper_Signmedia                      |
      | Mahmoud.Abdelaziz_NoMeasures   | Storekeeper_Signmedia_CannotReadMeasures   |
      | Mahmoud.Abdelaziz_NoStockTypes | Storekeeper_Signmedia_CannotReadStockTypes |
      | Ali.Hasan_NoMeasures           | Storekeeper_Flexo_CannotReadMeasures       |
      | Ali.Hasan_NoStockTypes         | Storekeeper_Flexo_CannotReadStockTypes     |
    And the following roles and sub-roles exist:
      | Role                                       | Subrole              |
      | Accountant_Signmedia                       | GRViewer_Signmedia   |
      | Storekeeper_Flexo                          | GROwner_Flexo        |
      | Storekeeper_Signmedia                      | GROwner_Signmedia    |
      | Storekeeper_Signmedia_CannotReadMeasures   | GROwner_Signmedia    |
      | Storekeeper_Signmedia_CannotReadMeasures   | ItemViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadMeasures   | StockTypesViewer     |
      | Storekeeper_Signmedia_CannotReadStockTypes | GROwner_Signmedia    |
      | Storekeeper_Signmedia_CannotReadStockTypes | MeasuresViewer       |
      | Storekeeper_Flexo_CannotReadMeasures       | GROwner_Flexo        |
      | Storekeeper_Flexo_CannotReadMeasures       | StockTypesViewer     |
      | Storekeeper_Flexo_CannotReadStockTypes     | GROwner_Flexo        |
      | Storekeeper_Flexo_CannotReadStockTypes     | MeasuresViewer       |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission              | Condition                      |
      | GRViewer_Signmedia   | GoodsReceipt:ReadItems  | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo        | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | GROwner_Signmedia    | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | MeasuresViewer       | Measures:ReadAll        |                                |
      | StockTypesViewer     | StockType:ReadAll       |                                |

    And the following users doesn't have the following permissions:
      | User                           | Permission        |
      | Mahmoud.Abdelaziz_NoMeasures   | Measures:ReadAll  |
      | Ali.Hasan_NoMeasures           | Measures:ReadAll  |
      | Mahmoud.Abdelaziz_NoStockTypes | StockType:ReadAll |
      | Ali.Hasan_NoStockTypes         | StockType:ReadAll |


    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |


    And GoodsReceipts "2018000008" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
      | 000003   |

    And GoodsReceipts "2020000020" has the following ReceivedItems:
      | ItemCode |
      | 000051   |

  ######## Authorization
  # EBS-4764
  Scenario: (01) Add GR Item Detail Auth - to GoodsReceipt in Draft state by unauthorized user (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves Item Detail of type "Quantity" in Item with code "000001" to ReceivedItems section in GoodsReceipt with code "2018000008" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      |         |          |            | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    #EBS-6770
  Scenario Outline: (02) Add GR Item Detail Auth - to GoodsReceipt in Draft state by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves Item Detail of type "Quantity" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | GRCode     | ItemCode | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      | Ali.Hasan         | 2018000008 | 000001   | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | Mahmoud.Abdelaziz | 2020000020 | 000051   | 5.0              | 0014 | UNRESTRICTED_USE | No Notes        |

  Scenario Outline: (03) Add GR Item Detail Auth - User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given user is logged in as "<User>"
    When "<User>" saves Item Detail of type "Quantity" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                           | GRCode     | ItemCode | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      | Mahmoud.Abdelaziz_NoMeasures   | 2018000008 | 000001   | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | Mahmoud.Abdelaziz_NoStockTypes | 2018000008 | 000001   | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | Ali.Hasan_NoMeasures           | 2020000020 | 000051   | 5.0              | 0014 | UNRESTRICTED_USE | No Notes        |
      | Ali.Hasan_NoStockTypes         | 2020000020 | 000051   | 5.0              | 0014 | UNRESTRICTED_USE | No Notes        |