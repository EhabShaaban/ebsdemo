# Author Dev: Hossam Hassan - Ahmed Hamed (EBS-7270)
# Reviewer:
Feature: Activate Goods Receipt Validation

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ali.Hasan         | Storekeeper_Corrugated |
      | hr1               | SuperUser              |
    And the following roles and sub-roles exist:
      | Role                   | Subrole                    |
      | Storekeeper_Signmedia  | GRActivateOwner_Signmedia  |
      | Storekeeper_Flexo      | GRActivateOwner_Flexo      |
      | Storekeeper_Corrugated | GRActivateOwner_Corrugated |
      | SuperUser              | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission            | Condition                       |
      | GRActivateOwner_Signmedia  | GoodsReceipt:Activate | [purchaseUnitName='Signmedia']  |
      | GRActivateOwner_Flexo      | GoodsReceipt:Activate | [purchaseUnitName='Flexo']      |
      | GRActivateOwner_Corrugated | GoodsReceipt:Activate | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole           | *:*                   |                                 |

    And the following Import PurchaseOrders exist with values:
      | Code       | State   | GoodsState  | BusinessUnit      |
      | 2018000001 | Cleared | NotReceived | 0002 - Signmedia  |
      | 2018000045 | Cleared | NotReceived | 0006 - Corrugated |
      | 2018000003 | Cleared | NotReceived | 0001 - Flexo      |

    And the following Local PurchaseOrders exist with values:
      | Code       | State          | GoodsState  | BusinessUnit     |
      | 2018000057 | OpenForUpdates | NotReceived | 0002 - Signmedia |

    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                      | BusinessUnit      |
      | 2018000003 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0001 - Flexo      |
      | 2018000012 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0006 - Corrugated |
      | 2018000015 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia  |
      | 2018000024 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0006 - Corrugated |
      | 2020000030 | 0002 - DigiPro   | 0002 - AlMadina Secondary Store | 0002 - Signmedia  |

    And the following POData exists for GoodsReceipts:
      | GRCode     | Vendor            | PurchaseOrder | DeliveryNote/PL | PurchasingResponsible | PurchasingUnit    |
      | 2018000003 | 000001 - Siegwerk | 2018000003    |                 | Amr Khalil            | 0001 - Flexo      |
      | 2018000012 | 000002 - Zhejiang | 2018000045    | 123456          | Amr Khalil            | 0006 - Corrugated |
      | 2018000015 | 000002 - Zhejiang | 2018000001    | 123456          | Gehan Ahmed           | 0002 - Signmedia  |
      | 2018000024 | 000002 - Zhejiang | 2018000057    | 123456          | Amr Khalil            | 0006 - Corrugated |
      | 2020000030 | 000002 - Zhejiang | 2018000008    |                 | Amr Khalil            | 0002 - Signmedia  |

    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate         | Type           | Storekeeper              | State |
      | 2018000003 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |
      | 2018000012 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |
      | 2018000015 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0007 - Mahmoud.Abdelaziz | Draft |
      | 2018000024 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |
      | 2020000030 | hr1       | 27-Sep-2020 02:56 PM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |

    # Items in GR 2018000003
    And GoodsReceipt with code "2018000003" has the following received items:
      | Item   | DifferenceReason |
      | 000005 |                  |
    And Item "000005" in GoodsReceipt (Based On PurchaseOrder) "2018000003" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes                                                    | StockType        |
      | 1002.0           | 0034 | يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة | UNRESTRICTED_USE |

     # Items in GR 2018000012
    And GoodsReceipt with code "2018000012" has the following received items:
      | Item   | DifferenceReason                                                     |
      | 000012 | يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك |
    And Item "000012" in GoodsReceipt (Based On PurchaseOrder) "2018000012" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 14.123456        | 0042 |       | UNRESTRICTED_USE |
      | 15.123456        | 0043 |       | UNRESTRICTED_USE |

     # Items in GR 2018000015
    And GoodsReceipt with code "2018000015" has empty received items list

    # Items in GR 2018000024
    And GoodsReceipt with code "2018000024" has the following received items:
      | Item   | DifferenceReason                                                     |
      | 000012 | يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك |
    And Item "000012" in GoodsReceipt (Based On PurchaseOrder) "2018000012" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 14.123456        | 0042 |       | UNRESTRICTED_USE |
      | 15.123456        | 0043 |       | UNRESTRICTED_USE |

    # Items in GR 2020000030
    And GoodsReceipt with code "2020000030" has the following received items:
      | Item   | DifferenceReason |
      | 000060 |                  |

    And Item "000060" in GoodsReceipt (Based On PurchaseOrder) "2020000030" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 10.00            | 0033 |       | UNRESTRICTED_USE |


    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit     |
      | 2020000001 | Draft | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
    And the following LandedCostDetails for LandedCost exist:
      | LCCode     | GoodsInvoice | TotalAmount | PurchaseOrder | DamageStock |
      | 2020000001 | 2019000040   | 20008.8 USD | 2018000008    | true        |

     #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy | CreationDate        | DocumentOwner |
      | 2021000003 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |

   #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit      | Company          |
      | 2021000003 | 0006 - Corrugated | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000003 | 2018000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |

    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2021000003 | 0.0         | 0.0            | 500         |

    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item          | Qty(OrderUnit)       | Qty(Base)   | UnitPrice(Base) |
      | 2021000003 | 000012 - Ink2 | 14.0 Bottle-2Liter   | 28.00 Liter | 200             |
      | 2021000003 | 000012 - Ink2 | 15.0 Bottle-2.5Liter | 37.5 Liter  | 200             |

    #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice       | JournalEntryCode |
      | 2021000003 | Shady.Abdelatif | 22-Jun-2021 09:02 AM | 1.0 USD = 17.44 EGP | 2020000030       |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State       | CreatedBy  | CreationDate         | Remaining | DocumentOwner |
      | 2021000003 | VENDOR      | PaymentDone | Amr.Khalil | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company          |
      | 2021000003 | 0006 - Corrugated | 0001 - AL Madina |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury | ExpectedDueDate      | BankTransRef | Description        |
      | 2021000003 | BANK        | 000002 - Zhejiang | INVOICE         | 2021000003  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Sep-2018 09:02 AM |              | downpayment for po |

          #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant | PostingDate          | CurrencyPrice    | JournalEntryCode |
      | 2021000003 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 16 USD | 2021000005       |


  Scenario: (01) Activate GR Val - invalid state - Active State (Exception Case)
    Given user is logged in as "Ali.Hasan"
    And another user is logged in as "hr1"
    And first "hr1" activated GR with code "2018000012" at "07-Jan-2019 9:00 AM"
    When "Ali.Hasan" requests to activate GR with code "2018000012" at "07-Jan-2019 9:30 AM"
    Then an error notification is sent to "Ali.Hasan" with the following message "Gen-msg-32"

  Scenario: (02) Activate GR Val - one of the sections is locked (Exception Case)
    Given user is logged in as "Ali.Hasan"
    And another user is logged in as "hr1"
    And first "hr1" opens "ItemsData" of GR with Code "2018000003" in edit mode at "07-Jan-2019 9:20 AM"
    When "Ali.Hasan" requests to activate GR with code "2018000003" at "07-Jan-2019 9:30 AM"
    Then an error notification is sent to "Ali.Hasan" with the following message "Gen-msg-14"

  Scenario: (03) Activate GR Val - where GR doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000003" successfully
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2018000003" at "07-Jan-2019 9:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (04) Activate GR Val - with missing mandattory fields
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2018000015" at "07-Jan-2019 09:30 AM"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-40"
    And the following missing fields in section ItemsData are sent to "Mahmoud.Abdelaziz":
      | MissingFields |
      | ItemsList     |

  #EBS-7270
  Scenario Outline: (05) Activate GR Val - invalid LC for referenced PO (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to activate GR with code "<GRCode>" at "07-Jan-2019 9:30 AM"
    Then a failure notification is sent to "hr1" with the following message "GR-msg-18"
    Examples:
      | GRCode     |
      | 2020000030 |
      | 2018000024 |
