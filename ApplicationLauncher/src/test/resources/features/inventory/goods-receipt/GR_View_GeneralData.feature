# Reviewer: Somaya Ahmed (21-Jan-2019 2:30 PM)

@ViewGRHeader
Feature: View POGoodsReciept-Header section

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ali.Hasan         | Storekeeper_Corrugated |
      | hr1               | SuperUser              |
      | Afaf              | FrontDesk              |

    And the following roles and sub-roles exist:
      | Role                   | Subrole            |
      | Storekeeper_Signmedia  | GROwner_Signmedia  |
      | Storekeeper_Flexo      | GROwner_Flexo      |
      | Storekeeper_Corrugated | GROwner_Corrugated |
      | SuperUser              | SuperUserSubRole   |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | GROwner_Signmedia  | GoodsReceipt:ReadHeader | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:ReadHeader | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:ReadHeader | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                     |                                 |


    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate         | Type           | Storekeeper              | State |
      | 2018000008 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0007 - Mahmoud.Abdelaziz | Draft |
      | 2018000003 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |
      | 2018000011 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |


    #### Happy Path

    # EBS-1405
  Scenario Outline: (01) View GR HeaderData section - by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view Header section of GR with code "<GRCodeValue>"
    Then the following values of Header section of GR with code "<GRCodeValue>" are displayed to "<User>":
      | GRCode        | CreatedBy        | CreationDate            | Type        | Storekeeper        | State        |
      | <GRCodeValue> | <CreatedByValue> | <CreationDateTimeValue> | <TypeValue> | <StorekeeperValue> | <StateValue> |
    Examples:
      | User              | CreatedByValue | GRCodeValue | CreationDateTimeValue | TypeValue      | StorekeeperValue  | StateValue |
      | Mahmoud.Abdelaziz | Ahmed.Ali      | 2018000008  | 05-Jan-2018 11:00 AM  | PURCHASE_ORDER | Mahmoud.Abdelaziz | Draft      |
      | Ali.Hasan         | Ahmed.Ali      | 2018000003  | 05-Jan-2018 11:00 AM  | PURCHASE_ORDER | Ali.Hasan         | Draft      |
      | Ali.Hasan         | Ahmed.Ali      | 2018000011  | 05-Jan-2018 11:00 AM  | PURCHASE_ORDER | Ali.Hasan         | Draft      |

    #### Exceptions

    # EBS-1405
  Scenario: (02) View GR HeaderData section - where GR has been deleted by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to view Header section of GR with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

    #### Abuse Cases

    # EBS-1405
  Scenario Outline: (03) View GR HeaderData section - by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view Header section of GR with code "2018000008"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      |
      | Afaf      |
      | Ali.Hasan |
