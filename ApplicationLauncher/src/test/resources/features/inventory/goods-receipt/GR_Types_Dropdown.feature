#Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Ahmed

Feature: View All GoodsReceipt Types

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                |
      | Storekeeper_Signmedia | GoodsReceiptTypeViewer |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission               | Condition |
      | GoodsReceiptTypeViewer | GoodsReceiptType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | GoodsReceiptType:ReadAll |

    And the following GoodsReceiptTypes exist:
      | Code           | Name                                  |
      | SALES_RETURN   | Goods Receipt Based on Sales Return   |
      | PURCHASE_ORDER | Goods Receipt Based on Purchase Order |
    And the total number of existing GoodsReceiptTypes is 2

  #EBS-6786
  Scenario: (01) Read list of GoodsReceiptTypes - dropdown by authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all GoodsReceiptTypes
    Then the following GoodsReceiptTypes values will be presented to "Mahmoud.Abdelaziz":
      | Code           | Name                                  |
      | SALES_RETURN   | Goods Receipt Based on Sales Return   |
      | PURCHASE_ORDER | Goods Receipt Based on Purchase Order |
    And total number of GoodsReceiptTypes returned to "Mahmoud.Abdelaziz" is equal to 2

  #EBS-6786
  Scenario: (02) Read list of GoodsReceiptTypes - dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all GoodsReceiptTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page