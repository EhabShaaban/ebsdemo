#Author: Marina
#Date: 25-Dec-2018 02:13PM

Feature: Read Next and Previous

  Background:

    Given the following users and roles exist:
      | Name         | Role                             |
      | Ashraf.Fathi | M.D                              |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia  |
      | Amr.Khalil   | PurchasingResponsible_Flexo      |
      | Amr.Khalil   | PurchasingResponsible_Corrugated |
      | Afaf         | FrontDesk                        |
      | hr1          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role            |
      | M.D                              | GRViewer            |
      | PurchasingResponsible_Signmedia  | GRViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | GRViewer_Flexo      |
      | PurchasingResponsible_Corrugated | GRViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole    |

    And the following sub-roles and permissions exist:
      | Subrole             | Permission              | Condition                       |
      | GRViewer            | GoodsReceipt:ReadHeader |                                 |
      | GRViewer_Signmedia  | GoodsReceipt:ReadHeader | [purchaseUnitName='Signmedia']  |
      | GRViewer_Flexo      | GoodsReceipt:ReadHeader | [purchaseUnitName='Flexo']      |
      | GRViewer_Corrugated | GoodsReceipt:ReadHeader | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole    | *:*                     |                                 |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State  | Type                                                   |
      | 2020000032 | 0002 - Signmedia | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000031 | 0002 - Signmedia | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000009 | 0001 - Flexo     | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000008 | 0002 - Signmedia | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000004 | 0001 - Flexo     | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | 0001 - Flexo     | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000002 | 0002 - Signmedia | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000001 | 0002 - Signmedia | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000000 | 0002 - Signmedia | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |

  Scenario Outline: (01) Read Next/Previous GR - by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view next GR of current GR with code "<GRCode>"
    Then the next GR code "<NextValue>" is displayed to "<User>"
    Examples:
      | User         | GRCode     | NextValue  |
      | Gehan.Ahmed  | 2018000000 | 2018000001 |
      | Amr.Khalil   | 2018000004 | 2018000009 |
      | Ashraf.Fathi | 2018000003 | 2018000004 |
      | Ashraf.Fathi | 2018000008 | 2018000009 |
      | hr1          | 2018000000 | 2018000001 |

  Scenario Outline: (02) Read Next/Previous GR -  by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view previous GR of current GR with code "<GRCode>"
    Then the previous GR code "<PrevValue>" is displayed to "<User>"
    Examples:
      | User         | GRCode     | PrevValue  |
      | Gehan.Ahmed  | 2018000008 | 2018000002 |
      | Amr.Khalil   | 2018000009 | 2018000004 |
      | Ashraf.Fathi | 2018000009 | 2018000008 |
      | hr1          | 2018000004 | 2018000003 |

  Scenario: (03) Read Next/Previous GR - Navigate non-existing next record while it is the last record to display
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2020000032" successfully
    When "Gehan.Ahmed" requests to view next GR of current GR with code "2020000031"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-31"

  Scenario: (04) Read Next/Previous GR - Navigate non-existing previous record while it is the last record to display
    Given user is logged in as "hr1"
    And another user is logged in as "Gehan.Ahmed"
    And "hr1" first deleted the GoodsReceipt with code "2018000000" successfully
    When "Gehan.Ahmed" requests to view previous GR of current GR with code "2018000001"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-31"

  Scenario: (05) Read Next/Previous GR - Navigate non-existing next record
    Given user is logged in as "hr1"
    And another user is logged in as "Amr.Khalil"
    And "hr1" first deleted the GoodsReceipt with code "2018000009" successfully
    When "Amr.Khalil" requests to view next GR of current GR with code "2018000004"
    Then the next GR code "2018000011" is displayed to "Amr.Khalil"

  Scenario: (06) Read Next/Previous GR - Navigate non-existing previous record
    Given user is logged in as "Ashraf.Fathi"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Ashraf.Fathi" requests to view previous GR of current GR with code "2018000009"
    Then the previous GR code "2018000004" is displayed to "Ashraf.Fathi"

  Scenario: (07) Read Next/Previous GR - by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view next GR of current GR with code "2018000008"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario: (08) Read Next/Previous GR - by an unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view previous GR of current GR with code "2018000008"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
