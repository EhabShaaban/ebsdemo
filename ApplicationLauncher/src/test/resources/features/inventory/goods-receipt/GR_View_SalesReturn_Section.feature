# Author: Niveen Magdy
# Reviewer: Somaya Ahmed (26-Jul-2020)

Feature: View GR - SalesReturn section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Ali.Hasan       | Storekeeper_Flexo    |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role           |
      | Storekeeper_Flexo    | GRViewer_Flexo     |
      | Accountant_Signmedia | GRViewer_Signmedia |
      | SuperUser            | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                       | Condition                      |
      | GRViewer_Flexo     | GoodsReceipt:ReadSalesReturnData | [purchaseUnitName='Flexo']     |
      | GRViewer_Signmedia | GoodsReceipt:ReadSalesReturnData | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole   | *:*                              |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | GoodsReceipt:ReadSalesReturnData | [purchaseUnitName='Flexo'] |
    And the following GoodsReceipts exist:
      | Code       | Type                                               | BusinessUnit | State  |
      | 2020000020 | SALES_RETURN - Goods Receipt Based on Sales Return | 0001 - Flexo | Draft  |
      | 2020000021 | SALES_RETURN - Goods Receipt Based on Sales Return | 0001 - Flexo | Active |
    And the following SalesReturnData for GoodsReceipts exist:
      | GRCode     | SalesReturn | SalesReturnDocumentOwner        | SalesReturnCustomer      | Notes |
      | 2020000020 | 2020000002  | Manar.Mohammed - Manar Mohammed | 000006 - المطبعة الأمنية |       |
      | 2020000021 | 2020000002  | Manar.Mohammed - Manar Mohammed | 000006 - المطبعة الأمنية |       |

  # EBS-6768
  Scenario Outline: (01) View GR SalesReturn - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view SalesReturn section of GoodsReceipt with code "<Code>"
    Then the following values of SalesReturn section for GoodsReceipt with code "<Code>" are displayed to "Ali.Hasan":
      | SalesReturn | SalesReturnDocumentOwner | SalesReturnCustomer      | Notes |
      | 2020000002  | Manar Mohammed           | 000006 - المطبعة الأمنية |       |
      | 2020000002  | Manar Mohammed           | 000006 - المطبعة الأمنية |       |
    Examples:
      | Code       |
      | 2020000020 |
      | 2020000021 |
  
    # EBS-6768
  Scenario: (02) View GR SalesReturn section - where GoodsReceipt doesn't exist (Exception Case)
    Given user is logged in as "Ali.Hasan"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2020000020" successfully
    When "Ali.Hasan" requests to view SalesReturn section of GoodsReceipt with code "2020000020"
    Then an error notification is sent to "Ali.Hasan" with the following message "Gen-msg-01"
    
   # EBS-6768
  Scenario: (03) View GR SalesReturn section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view SalesReturn section of GoodsReceipt with code "2020000020"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

