Feature: Request Edit/Cancel Item Difference Reason in GR

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | Hosam.Hosni | LogisticsResponsible_Flexo     |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                            |
      | LogisticsResponsible_Signmedia | GRDifferenceReasonEditor_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                            | Permission                       | Condition                      |
      | GRDifferenceReasonEditor_Signmedia | GoodsReceipt:EditDifferentReason | [purchaseUnitName='Signmedia'] |
      | GRDifferenceReasonEditor_Signmedia | GoodsReceipt:EditDifferentReason | [purchaseUnitName='Signmedia'] |
    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State  |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
      | 2018000002 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Active |
      | 2018100016 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
    And GoodsReceipts has the following received items:
      | GoodsReceiptCode | Item                                         |
      | 2018000008       | 000001 - Hot Laminated Frontlit Fabric roll  |
      | 2018000002       | 000001 - Hot Laminated Frontlit Fabric roll  |
      | 2018100016       | 000002 - Hot Laminated Frontlit Backlit roll |
    And edit session is "30" minutes

  ######## Request Edit Happy Paths
  # EBS-2872
  Scenario Outline: (01) Request to Edit/Cancel GR Item Difference Reason - in GR (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Item Difference Reason Item with code "<ItemCode>" in GR-ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then edit Item Difference dialog is opened and GR-ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    Examples:
      | User        | GRCode     | ItemCode |
      | Gehan.Ahmed | 2018000008 | 000001   |
      | Gehan.Ahmed | 2018000002 | 000001   |

  ######## Request Edit Exception Cases
  # EBS-2872
  Scenario: (02) Request to Edit/Cancel GR Item Difference Reason - where GR doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018100016" successfully
    When "Gehan.Ahmed" requests to edit Item Difference Reason Item with code "000002" in GR-ReceivedItems section of GoodsReceipt with code "2018100016"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-2872
  Scenario: (03) Request to Edit/Cancel GR Item Difference Reason - where Item doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "000002" of GoodsReceipt with code "2018100016" successfully
    When "Gehan.Ahmed" requests to edit Item Difference Reason Item with code "000002" in GR-ReceivedItems section of GoodsReceipt with code "2018100016"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"

  # EBS-2872
  Scenario: (04) Request to Edit/Cancel GR Item Difference Reason - when GR-ReceivedItems section is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" opened Item with code "000001" in GoodsReceipt with code "2018000002" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "Gehan.Ahmed" requests to edit Item Difference Reason Item with code "000001" in GR-ReceivedItems section of GoodsReceipt with code "2018000002" at "07-Jan-2019 09:11 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"


  ######## Request Edit Abuse Cases
  # EBS-2872
  Scenario Outline: (05) Request to Edit/Cancel GR Item Difference Reason - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to edit Item Difference Reason Item with code "000001" in GR-ReceivedItems section of GoodsReceipt with code "2018000008"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Hosam.Hosni |
      | Afaf        |

  ######## Request Cancel Happy Paths
  # EBS-2872
  Scenario Outline: (06) Request to Edit/Cancel GR Item Difference Reason - (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "Gehan.Ahmed" requests to cancel Item Difference Reason Item with code "<ItemCode>" in GR-ReceivedItems section of GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:11 AM"
    Then edit Item Difference dialog is closed and the lock by "Gehan.Ahmed" on GR-ReceivedItems section of GoodsReceipt with code "<GRCode>" is released
    Examples:
      | GRCode     | ItemCode |
      | 2018000008 | 000001   |
      | 2018000002 | 000001   |

  # EBS-2872
  Scenario: (07) Request to Edit/Cancel GR Item Difference Reason - in GR, after edit session expire (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item with code "000001" in GoodsReceipt with code "2018000008" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "Gehan.Ahmed" requests to cancel Item Difference Reason Item with code "000001" in GR-ReceivedItems section of GoodsReceipt with code "2018000008" at "07-Jan-2019 9:42 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  ######## Request Edit Abuse Cases
  # EBS-2872
  Scenario Outline: (08) Request to Edit/Cancel GR Item Difference Reason - in GR - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to cancel Item Difference Reason Item with code "000001" in GR-ReceivedItems section of GoodsReceipt with code "2018000008" at "07-Jan-2019 9:42 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        |
      | Hosam.Hosni |
      | Afaf        |
