# Author: Nancy Shoukry (EBS-1602) 
# Author: Niveen Magdy and Engy Abdel-Aziz (EBS-6765)
# Reviewer: Somaya Ahmed and Hosam Bayomy (EBS-1602) 
# Reviewer: Somaya Ahmed and Hosam Bayomy (EBS-6765)(4-Aug-2020)

Feature: Create GoodsReceipt

  Rule:
  - User should be authenticated and authorized

    Background:
      Given the following users and roles exist:
        | Name              | Role                  |
        | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
        | Ali.Hasan         | Storekeeper_Flexo     |
      And the following roles and sub-roles exist:
        | Role                  | Subrole                   |
        | Storekeeper_Signmedia | GROwner_Signmedia         |
        | Storekeeper_Signmedia | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia | StorehouseViewer          |
        | Storekeeper_Signmedia | StorekeeperViewer         |
        | Storekeeper_Signmedia | CompanyViewer             |
        | Storekeeper_Flexo     | GROwner_Flexo             |
        | Storekeeper_Flexo     | SROViewer_Flexo           |
        | Storekeeper_Flexo     | StorehouseViewer          |
        | Storekeeper_Flexo     | StorekeeperViewer         |
        | Storekeeper_Flexo     | CompanyViewer             |

      And the following sub-roles and permissions exist:
        | Subrole                   | Permission               | Condition                      |
        | GROwner_Signmedia         | GoodsReceipt:Create      |                                |
        | GROwner_Flexo             | GoodsReceipt:Create      |                                |
        | POViewerLimited_Signmedia | PurchaseOrder:ReadAll    | [purchaseUnitName='Signmedia'] |
        | StorekeeperViewer         | Storekeeper:ReadAll      |                                |
        | StorehouseViewer          | Storehouse:ReadAll       |                                |
        | CompanyViewer             | Company:ReadAll          |                                |
        | SROViewer_Signmedia       | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |

      And the following Import PurchaseOrders exist:
        | Code       | State   | PurchasingUnit |
        | 2018000004 | Draft   | 0003           |
        | 2018000001 | Cleared | 0002           |

      And the following Local PurchaseOrders exist:
        | Code       | State   | PurchasingUnit |
        | 2018000024 | Draft   | 0003           |
        | 2018000018 | Shipped | 0002           |

      And the following PurchaseOrders exist with the following values needed for GR creation:
        | Code       | Type      | State   | Plant | PurchasingResponsible | Vendor | PurchasingUnit |
        | 2018000001 | IMPORT_PO | Cleared | 0001  | Gehan Ahmed           | 000002 | 0002           |
        | 2018000018 | LOCAL_PO  | Shipped | 0001  | Gehan Ahmed           | 000002 | 0002           |

      And ImportPurchaseOrder with code "2018000001" has the following items:
        | Code   |
        | 000001 |
        | 000002 |
        | 000003 |

      And ImportPurchaseOrder with code "2018000018" has the following items:
        | Code   |
        | 000001 |

      And the following items exist:
        | Code   | Name                                | Base      | IsBatchManaged |
        | 000001 | Hot Laminated Frontlit Fabric roll  | 0019 - M2 | false          |
        | 000002 | Hot Laminated Frontlit Backlit roll | 0019 - M2 | false          |
        | 000003 | Flex Primer Varnish E33             | 0003 - Kg | true           |

      And the following Storehouses exist:
        | Code | Name                | Plant |
        | 0001 | AlMadina Main Store | 0001  |
        | 0003 | DigiPro Main Store  | 0002  |

      And the following Storekeepers exist:
        | Code | Name              |
        | 0007 | Mahmoud.Abdelaziz |

      And the following SalesReturnOrders exist with the following values needed for GR creation:
        | Code       | BusinessUnit | SalesRepresentative             | Customer                 | Company        | State   |
        | 2020000002 | 0001 - Flexo | Manar.Mohammed - Manar Mohammed | 000006 - المطبعة الأمنية | 0002 - DigiPro | Shipped |

      And the following SalesReturnOrders have the following Items:
        | Code       | SROItemId | Item          | OrderUnit           | ReturnReason                        | ReturnQuantity | MaxReturnQuantity | SalesPrice | ItemTotalAmount |
        | 2020000002 | 2         | 000051 - Ink5 | 0029 - Roll 2.20x50 | 0001 - Problem in product operation | 10.000         | 0.000             | 22000.000  | 220000.000      |

  ######## Create Happy Paths
  # EBS-1602
    Scenario Outline: (01) Create GoodsReceipt with (all fields/ only mandatory fields) using Import Cleared-  by an authorized user (Happy Path)
      Given user is logged in as "Mahmoud.Abdelaziz"
      And CurrentDateTime = "<CurrentDateTime>"
      And Last created GoodsReceipt was with code "<LastGRCode>"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse   | Storekeeper   | Company   |
        | PURCHASE_ORDER | 2018000001    | <Storehouse> | <Storekeeper> | <Company> |
      Then a new GoodsReceipt is created with the following data:
        | Code        | Type           | State | CreatedBy         | LastUpdatedBy     | CreationDate      | LastUpdateDate    | Storekeeper   | Plant   |
        | <NewGRCode> | PURCHASE_ORDER | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | <CurrentDateTime> | <CurrentDateTime> | <Storekeeper> | <Plant> |
      And the Company Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | Storehouse   | BusinessUnit | Company   |
        | <Storehouse> | 0002         | <Company> |
      And the POData Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | PurchaseOrder | Vendor | PurchasingResponsible | DeliveryNote/PLNumber | Notes |
        | 2018000001    | 000002 | Gehan Ahmed           |                       |       |
      And GoodsReceipt with code "<NewGRCode>" has the following batch managed received items only:
        | Code   |
        | 000003 |
      And GoodsReceipt with code "<NewGRCode>" has the following ordinary received items only:
        | Item                                         | BaseUnit  |
        | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 |
        | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 |
      And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"
      Examples:
        | CurrentDateTime      | LastGRCode | NewGRCode  | Storehouse | Storekeeper | Company | Plant |
        | 01-Jan-2020 11:00 AM | 2020000032 | 2020000033 | 0001       | 0007        | 0001    | 0001  |

  # EBS-1602
    Scenario Outline: (02) Create GoodsReceipt with (all fields) using Local Shipped PO- by an authorized user (Happy Path)
      Given user is logged in as "Mahmoud.Abdelaziz"
      And CurrentDateTime = "<CurrentDateTime>"
      And Last created GoodsReceipt was with code "<LastGRCode>"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse   | Storekeeper   | Company   |
        | PURCHASE_ORDER | 2018000018    | <Storehouse> | <Storekeeper> | <Company> |
      Then a new GoodsReceipt is created with the following data:
        | Code        | Type           | State | CreatedBy         | LastUpdatedBy     | CreationDate      | LastUpdateDate    | Storekeeper   | Plant   |
        | <NewGRCode> | PURCHASE_ORDER | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | <CurrentDateTime> | <CurrentDateTime> | <Storekeeper> | <Plant> |
      And the Company Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | Storehouse   | BusinessUnit | Company   |
        | <Storehouse> | 0002         | <Company> |
      And the POData Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | PurchaseOrder | Vendor | PurchasingResponsible | DeliveryNote/PLNumber | Notes |
        | 2018000018    | 000002 | Gehan Ahmed           |                       |       |
      And GoodsReceipt with code "<NewGRCode>" has the following ordinary received items only:
        | Item                                        | BaseUnit  |
        | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |
      And GoodsReceipt with code "<NewGRCode>" has no batch managed received items
      And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"
      Examples:
        | CurrentDateTime      | LastGRCode | NewGRCode  | Storehouse | Storekeeper | Company | Plant |
        | 02-Jan-2020 11:00 AM | 2020000032 | 2020000033 | 0001       | 0007        | 0001    | 0001  |


  # EBS-6765
    Scenario Outline: (03) Create GoodsReceipt with (all fields) Based on Sales return- by an authorized user (Happy Path)
      Given user is logged in as "Ali.Hasan"
      And CurrentDateTime = "<CurrentDateTime>"
      And Last created GoodsReceipt was with code "<LastGRCode>"
      When "Ali.Hasan" creates GoodsReceipt with following values:
        | Type         | SalesReturn | Storehouse   | Storekeeper   | Company   |
        | SALES_RETURN | 2020000002  | <Storehouse> | <Storekeeper> | <Company> |
      Then a new GoodsReceipt is created with the following data:
        | Code        | Type         | State | CreatedBy | LastUpdatedBy | CreationDate      | LastUpdateDate    | Storekeeper   | Plant   |
        | <NewGRCode> | SALES_RETURN | Draft | Ali.Hasan | Ali.Hasan     | <CurrentDateTime> | <CurrentDateTime> | <Storekeeper> | <Plant> |
      And the Company Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | Storehouse   | BusinessUnit | Company   |
        | <Storehouse> | 0001         | <Company> |
      And the SalesReturnData Section of GoodsReceipt with code "<NewGRCode>" is updated as follows:
        | SalesReturn | SalesRepresentative             | Customer                 |
        | 2020000002  | Manar.Mohammed - Manar Mohammed | 000006 - المطبعة الأمنية |
      And GoodsReceipt with code "<NewGRCode>" has the following ordinary received items only:
        | Item          | BaseUnit  |
        | 000051 - Ink5 | 0019 - M2 |
      And GoodsReceipt with code "<NewGRCode>" has no batch managed received items
      And a success notification is sent to "Ali.Hasan" with the following message "Gen-msg-11"
      Examples:
        | CurrentDateTime      | LastGRCode | NewGRCode  | Storehouse | Storekeeper | Company | Plant |
        | 02-Jan-2020 11:00 AM | 2020000032 | 2020000033 | 0003       | 0007        | 0002    | 0002  |
