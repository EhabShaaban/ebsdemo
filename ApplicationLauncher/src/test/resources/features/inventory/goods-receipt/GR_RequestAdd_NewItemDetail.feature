# Author: Zyad Ghorab (16 Jun 2019)

Feature: Request Add Item Quantities in GR Happy Path

  Background:
    Given the following users and roles exist:
      | Name                           | Role                                       |
      | hr1                            | SuperUser                                  |
      | Mahmoud.Abdelaziz              | Storekeeper_Signmedia                      |
      | Shady.Abdelatif                | Accountant_Signmedia                       |
      | Ali.Hasan                      | Storekeeper_Flexo                          |
      | Mahmoud.Abdelaziz_NoMeasures   | Storekeeper_Signmedia_CannotReadMeasures   |
      | Ali.Hasan_NoMeasures           | Storekeeper_Flexo_CannotReadMeasures       |
      | Mahmoud.Abdelaziz_NoStockTypes | Storekeeper_Signmedia_CannotReadStockTypes |
      | Ali.Hasan_NoStockTypes         | Storekeeper_Flexo_CannotReadStockTypes     |
    And the following roles and sub-roles exist:
      | Role                                       | Subrole            |
      | Storekeeper_Signmedia                      | GROwner_Signmedia  |
      | Accountant_Signmedia                       | GRViewer_Signmedia |
      | Storekeeper_Flexo                          | GROwner_Flexo      |
      | Storekeeper_Flexo                          | GRViewer_Flexo     |
      | SuperUser                                  | *                  |
      | Storekeeper_Signmedia_CannotReadMeasures   | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadMeasures   | StockTypesViewer   |
      | Storekeeper_Signmedia_CannotReadStockTypes | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadStockTypes | MeasuresViewer     |
      | Storekeeper_Flexo_CannotReadMeasures       | GROwner_Flexo      |
      | Storekeeper_Flexo_CannotReadMeasures       | StockTypesViewer   |
      | Storekeeper_Flexo_CannotReadStockTypes     | GROwner_Flexo      |
      | Storekeeper_Flexo_CannotReadStockTypes     | MeasuresViewer     |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                      |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia | GoodsReceipt:ReadItems  | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo      | GoodsReceipt:ReadItems  | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo     | GoodsReceipt:ReadItems  | [purchaseUnitName='Flexo']     |
      | MeasuresViewer     | Measures:ReadAll        |                                |
      | StockTypesViewer   | StockType:ReadAll       |                                |

    And the following users doesn't have the following permissions:
      | User                           | Permission        |
      | Mahmoud.Abdelaziz_NoMeasures   | Measures:ReadAll  |
      | Ali.Hasan_NoMeasures           | Measures:ReadAll  |
      | Mahmoud.Abdelaziz_NoStockTypes | StockType:ReadAll |
      | Ali.Hasan_NoStockTypes         | StockType:ReadAll |

    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State  |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
      | 2018000002 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Active |
      | 2018000003 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0001 - Flexo     | Draft  |
      | 2020000020 | SALES_RETURN - Goods Receipt Based on Sales Return     | 0001 - Flexo     | Draft  |
      | 2020000021 | SALES_RETURN - Goods Receipt Based on Sales Return     | 0001 - Flexo     | Active |

    And GoodsReceipts "2018000008" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
      | 000003   |
    And GoodsReceipts "2018000002" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
    And GoodsReceipts "2018000003" has the following ReceivedItems:
      | ItemCode |
      | 000005   |
    And GoodsReceipts "2020000020" has the following ReceivedItems:
      | ItemCode |
      | 000051   |
    And GoodsReceipts "2020000021" has the following ReceivedItems:
      | ItemCode |
      | 000051   |

    And the following items exist:
      | Code   | Name                                                  | State  | Base | IsBatchManaged |
      | 000001 | Hot Laminated Frontlit Fabric roll                    | Active | 0019 | false          |
      | 000003 | Flex Primer Varnish E33                               | Active | 0003 | true           |
      | 000005 | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | Active | 0019 | false          |
      | 000051 | Ink5                                                  | Active | 0019 | false          |

    And edit session is "30" minutes

  ############################################## Request to Add Item quantity ###################################################

  Scenario Outline: (01) Request to Add/Cancel GR Item Detail - (Ordinary Item) in GoodsReceipt in (Draft) state by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then a new add Item Quantity side nav is opened and Items section of GoodsReceipt with code "<GRCode>" is locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
      | ReadStockTypes  |
    And the following mandatory fields are returned to "<User>":
      | MandatoriesFields |
      | receivedQtyUoE    |
      | unitOfEntryCode   |
      | stockType         |
    And the following editable fields are returned to "<User>":
      | EditableFields  |
      | receivedQtyUoE  |
      | unitOfEntryCode |
      | stockType       |
      | notes           |
    Examples:
      | GRCode     | ItemCode | User              | DetailType |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | NotBatched |
      | 2020000020 | 000051   | Ali.Hasan         | NotBatched |

  Scenario Outline: (02) Request to Add/Cancel GR Item Detail -to (Batched Item) in GoodsReceipt in (Draft) state by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then a new add Item Quantity side nav is opened and Items section of GoodsReceipt with code "<GRCode>" is locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
      | ReadStockTypes  |
    And the following mandatory fields are returned to "<User>":
      | MandatoriesFields |
      | batchCode         |
      | receivedQtyUoE    |
      | unitOfEntryCode   |
      | productionDate    |
      | expirationDate    |
      | stockType         |
    And the following editable fields are returned to "<User>":
      | EditableFields  |
      | receivedQtyUoE  |
      | productionDate  |
      | batchCode       |
      | expirationDate  |
      | unitOfEntryCode |
      | stockType       |
      | notes           |
    Examples:
      | GRCode     | ItemCode | User              | DetailType |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batched    |

  Scenario Outline: (03) Request to Add/Cancel GR Item Detail -to Item of GoodsReceipt that doesn't exist (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | GRCode     | ItemCode | User              |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz |
      | 2020000020 | 000051   | Ali.Hasan         |

  Scenario Outline: (04) Request to Add/Cancel GR Item Detail -to Item that doesn't exist in GoodsReceipt (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully
    When "Mahmoud.Abdelaziz" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode |
      | 2018000008 | 000001   |
      | 2018000008 | 000003   |

  Scenario Outline: (05) Request to Add/Cancel GR Item Detail -to Item when Add Item Quantity action is not allowed in current state (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-03"
    Examples:
      | GRCode     | ItemCode | User              |
      | 2018000002 | 000001   | Mahmoud.Abdelaziz |
      | 2020000021 | 000051   | Ali.Hasan         |


  Scenario Outline: (06) Request to Add/Cancel GR Item Detail -to Item when items section is locked by another user (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>" successfully
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-02"
    Examples:
      | User              | GRCode     | ItemCode |
      | Mahmoud.Abdelaziz | 2018000008 | 000001   |
      | Ali.Hasan         | 2020000020 | 000051   |

  Scenario Outline: (07) Request to Add/Cancel GR Item Detail -by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "000001" to Items section of GoodsReceipt with code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | GRCode     |
      | Shady.Abdelatif   | 2018000008 |
      | Mahmoud.Abdelaziz | 2018000003 |
      | Mahmoud.Abdelaziz | 2020000020 |

  Scenario Outline: (08) Request to Add/Cancel GR Item Detail - by a User with one or more roles (No Measures Permission) (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then a new add Item Quantity side nav is opened and Items section of GoodsReceipt with code "<GRCode>" is locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadStockTypes  |
    Examples:
      | GRCode     | User                         | ItemCode |
      | 2018000008 | Mahmoud.Abdelaziz_NoMeasures | 000001   |
      | 2018000008 | Mahmoud.Abdelaziz_NoMeasures | 000003   |
      | 2020000020 | Ali.Hasan_NoMeasures         | 000051   |

  Scenario Outline: (09) Request to Add/Cancel GR Item Detail -by a User with one or more roles (No StockType Permission) (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to add Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>"
    Then a new add Item Quantity side nav is opened and Items section of GoodsReceipt with code "<GRCode>" is locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
    Examples:
      | GRCode     | User                           | ItemCode |
      | 2018000008 | Mahmoud.Abdelaziz_NoStockTypes | 000001   |
      | 2018000008 | Mahmoud.Abdelaziz_NoStockTypes | 000003   |
      | 2020000020 | Ali.Hasan_NoStockTypes         | 000051   |

  ############################################## Request to Cancel Add Item quantity ###################################################

  Scenario Outline: (10) Request to Add/Cancel Item Detail - within the edit session Time in (Draft) state by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>" in edit mode at "12-Jun-2019 09:10 AM"
    When "<User>" cancels saving Item Quantity for Item with code "<ItemCode>" of GoodsReceipt with Code "<GRCode>" at "12-Jun-2019 09:30 AM"
    Then the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    Examples:
      | GRCode     | ItemCode | User              |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz |
      | 2020000020 | 000051   | Ali.Hasan         |

  Scenario Outline: (11) Request to Add/Cancel GR Item Detail - after edit session expire (Exception Case)
    Given user is logged in as "<User>"
    And "<User>" opened Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>" in edit mode at "12-Jun-2019 09:10 AM"
    When "<User>" cancels saving Item Quantity for Item with code "<ItemCode>" of GoodsReceipt with Code "<GRCode>" at "12-Jun-2019 09:41 AM"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released

    Examples:
      | GRCode     | ItemCode | User              |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz |
      | 2020000020 | 000051   | Ali.Hasan         |

  Scenario Outline: (12) Request to Add/Cancel GR Item Detail - for Item of GoodsReceipt deleted after session time out (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>" in edit mode at "12-Jun-2019 09:10 AM"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully at "12-Jun-2019 09:41 AM"
    When "<User>" cancels saving Item Quantity for Item with code "<ItemCode>" of GoodsReceipt with Code "<GRCode>" at "12-Jun-2019 09:42 AM"
    And an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | GRCode     | ItemCode | User              |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz |
      | 2020000020 | 000051   | Ali.Hasan         |

  Scenario Outline: (13) Request to Add/Cancel GR Item Detail - for Item that doesn't exist in GoodsReceipt (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "Mahmoud.Abdelaziz" opened Item Quantity for Item with code "<ItemCode>" to Items section of GoodsReceipt with code "<GRCode>" in edit mode at "12-Jun-2019 09:10 AM"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully at "12-Jun-2019 09:41 AM"
    When "Mahmoud.Abdelaziz" cancels saving Item Quantity for Item with code "<ItemCode>" of GoodsReceipt with Code "2018000008" at "12-Jun-2019 09:42 AM"
    And an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode |
      | 2018000008 | 000001   |
      | 2018000008 | 000003   |

  Scenario Outline: (14) Request to Add/Cancel GR Item Detail - by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving Item Quantity for Item with code "<ItemCode>" of GoodsReceipt with Code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | GRCode     | ItemCode |
      | Shady.Abdelatif   | 2018000008 | 000001   |
      | Mahmoud.Abdelaziz | 2018000003 | 000001   |
      | Mahmoud.Abdelaziz | 2020000020 | 000051   |



