# Author: Nancy Shoukry (EBS-1602) 
# Author: Niveen Magdy and Engy Abdel-Aziz (EBS-6765-EBS-6767)
# Reviewer: Somaya Ahmed and Hosam Bayomy (EBS-1602) 
# Reviewer: Somaya Ahmed and Hosam Bayomy (EBS-6765)(4-Aug-2020)

Feature: Create GoodsReceipt

  Rule:
  - If an unauthorized user sends a create request, he/she should be logged out as this is considered as a security breach

    Background:
      Given the following users and roles exist:
        | Name                                  | Role                                              |
        | Afaf                                  | FrontDesk                                         |
        | Mahmoud.Abdelaziz                     | Storekeeper_Signmedia                             |
        | Mahmoud.Abdelaziz.NoStoreHouse        | Storekeeper_Signmedia_CannotViewStorehouseRole    |
        | Mahmoud.Abdelaziz.NoStorekeeper       | Storekeeper_Signmedia_CannotViewStorekeeperRole   |
        | Mahmoud.Abdelaziz.NoPurchaseOrder     | Storekeeper_Signmedia_CannotViewPurchaseOrder     |
        | Mahmoud.Abdelaziz.NoSalesReturn       | Storekeeper_Signmedia_CannotViewSalesReturn       |
        | Mahmoud.Abdelaziz.NoGoodsReceiptTypes | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes |
        | Shady.Abdelatif                       | Accountant_Signmedia                              |
        | CreateOnlyUser                        | CreateOnlyRole                                    |

      And the following roles and sub-roles exist:
        | Role                                              | Subrole                   |
        | Storekeeper_Signmedia                             | GROwner_Signmedia         |
        | Storekeeper_Signmedia                             | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia                             | StorehouseViewer          |
        | Storekeeper_Signmedia                             | StorekeeperViewer         |
        | Storekeeper_Signmedia                             | CompanyViewer             |
        | Storekeeper_Signmedia                             | SROViewer_Signmedia       |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | GoodsReceiptTypeViewer    |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | GROwner_Signmedia         |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | StorekeeperViewer         |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | CompanyViewer             |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia_CannotViewStorehouseRole    | SROViewer_Signmedia       |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | GoodsReceiptTypeViewer    |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | GROwner_Signmedia         |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | StorehouseViewer          |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | CompanyViewer             |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia_CannotViewStorekeeperRole   | SROViewer_Signmedia       |
        | Storekeeper_Signmedia_CannotViewPurchaseOrder     | GoodsReceiptTypeViewer    |
        | Storekeeper_Signmedia_CannotViewPurchaseOrder     | GROwner_Signmedia         |
        | Storekeeper_Signmedia_CannotViewPurchaseOrder     | StorehouseViewer          |
        | Storekeeper_Signmedia_CannotViewPurchaseOrder     | StorekeeperViewer         |
        | Storekeeper_Signmedia_CannotViewPurchaseOrder     | CompanyViewer             |
        | Storekeeper_Signmedia_CannotViewSalesReturn       | GoodsReceiptTypeViewer    |
        | Storekeeper_Signmedia_CannotViewSalesReturn       | GROwner_Signmedia         |
        | Storekeeper_Signmedia_CannotViewSalesReturn       | StorehouseViewer          |
        | Storekeeper_Signmedia_CannotViewSalesReturn       | StorekeeperViewer         |
        | Storekeeper_Signmedia_CannotViewSalesReturn       | CompanyViewer             |
        | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes | SROViewer_Signmedia       |
        | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes | GROwner_Signmedia         |
        | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes | StorehouseViewer          |
        | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes | StorekeeperViewer         |
        | Storekeeper_Signmedia_CannotViewGoodsReceiptTypes | CompanyViewer             |
        | CreateOnlyRole                                    | CreateOnlySubRole         |
      And the following sub-roles and permissions exist:
        | Subrole                   | Permission               | Condition                      |
        | GROwner_Signmedia         | GoodsReceipt:Create      |                                |
        | GROwner_Flexo             | GoodsReceipt:Create      |                                |
        | GoodsReceiptTypeViewer    | GoodsReceiptType:ReadAll |                                |
        | POViewerLimited_Signmedia | PurchaseOrder:ReadAll    | [purchaseUnitName='Signmedia'] |
        | StorekeeperViewer         | Storekeeper:ReadAll      |                                |
        | StorehouseViewer          | Storehouse:ReadAll       |                                |
        | CompanyViewer             | Company:ReadAll          |                                |
        | SROViewer_Signmedia       | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |
        | CreateOnlySubRole         | GoodsReceipt:Create      |                                |

      And the following users doesn't have the following permissions:
        | User                                  | Permission               |
        | Mahmoud.Abdelaziz.NoPurchaseOrder     | PurchaseOrder:ReadAll    |
        | Mahmoud.Abdelaziz.NoSalesReturn       | SalesReturnOrder:ReadAll |
        | Mahmoud.Abdelaziz.NoGoodsReceiptTypes | GoodsReceiptType:ReadAll |
        | CreateOnlyUser                        | PurchaseOrder:ReadAll    |
        | CreateOnlyUser                        | SalesReturnOrder:ReadAll |
        | CreateOnlyUser                        | Company:ReadAll          |
        | CreateOnlyUser                        | Storehouse:ReadAll       |
        | CreateOnlyUser                        | Storekeeper:ReadAll      |

      And the following users have the following permissions without the following conditions:
        | User              | Permission               | Condition                  |
        | Mahmoud.Abdelaziz | PurchaseOrder:ReadAll    | [purchaseUnitName='Flexo'] |
        | Mahmoud.Abdelaziz | SalesReturnOrder:ReadAll | [purchaseUnitName='Flexo'] |

      And the following Import PurchaseOrders exist:
        | Code       | State   | PurchasingUnit |
        | 2018000004 | Draft   | 0003           |
        | 2018000001 | Cleared | 0002           |

      And the following Local PurchaseOrders exist:
        | Code       | State | PurchasingUnit |
        | 2018000024 | Draft | 0003           |

      And the following PurchaseOrders exist with the following values needed for GR creation:
        | Code       | Type      | State   | Plant | PurchasingResponsible | Vendor | PurchasingUnit |
        | 2018000001 | IMPORT_PO | Cleared | 0001  | Gehan Ahmed           | 000002 | 0002           |

      And ImportPurchaseOrder with code "2018000001" has the following items:
        | Code   |
        | 000001 |
        | 000002 |
        | 000003 |

      And the following SalesReturnOrders exist with the following values needed for GR creation:
        | Code       | BusinessUnit     | SalesRepresentative                       | Customer                       | Company          | State   |
        | 2020000002 | 0001 - Flexo     | Manar.Mohammed - Manar Mohammed           | 000006 - المطبعة الأمنية       | 0002 - DigiPro   | Shipped |
        | 2020000053 | 0002 - Signmedia | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية | 0001 - AL Madina | Shipped |


      And the following Storehouses exist:
        | Code | Name                | Plant |
        | 0001 | AlMadina Main Store | 0001  |
        | 0003 | DigiPro Main Store  | 0002  |

      And the following Storekeepers exist:
        | Code | Name              |
        | 0007 | Mahmoud.Abdelaziz |

  ######## Create by an unauthorized user

    # EBS-6767
    Scenario: (01) Create GoodsIssue - by an authorized user with authorized reads
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" requests to create GoodsReceipt
      Then the following authorized reads are returned to "Mahmoud.Abdelaziz":
        | AuthorizedReads          |
        | ReadGRType               |
        | ReadStorehouse           |
        | ReadStorekeeper          |
        | ReadCompany              |
        | ReadLocalPurchaseOrders  |
        | ReadImportPurchaseOrders |
        | ReadSalesReturnOrders    |
    # EBS-1602
    Scenario: (02) Create GoodsReceipt by an unauthorized user (Abuse case)
      Given user is logged in as "Shady.Abdelatif"
      When "Shady.Abdelatif" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | 2018000001    | 0001       | 0007        | 0001    |
      Then "Shady.Abdelatif" is logged out
      And "Shady.Abdelatif" is forwarded to the error page

  # EBS-1602
    Scenario Outline: (03) Create GoodsReceipt by a user with unauthorized read (Abuse case)
      Given user is logged in as "<User>"
      When "<User>" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder   | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | <PurchaseOrder> | 0001       | 0007        | 0001    |
      Then "<User>" is logged out
      And "<User>" is forwarded to the error page
      Examples:
        | User                              | PurchaseOrder |
        | Mahmoud.Abdelaziz                 | 2018000004    |
        | Mahmoud.Abdelaziz                 | 2018000024    |
        | Mahmoud.Abdelaziz.NoStoreHouse    | 2018000001    |
        | Mahmoud.Abdelaziz.NoStorekeeper   | 2018000001    |
        | Mahmoud.Abdelaziz.NoPurchaseOrder | 2018000001    |

  # EBS-6767
    Scenario: (04) Create GoodsReceipt - by an unauthorized user
      Given user is logged in as "Afaf"
      When "Afaf" requests to create GoodsReceipt
      Then "Afaf" is logged out
      And "Afaf" is forwarded to the error page

  # EBS-6767
    Scenario Outline: (05) Create GoodsReceipt - by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
      Given user is logged in as "<User>"
      When "<User>" creates GoodsReceipt with following values:
        | Type         | SalesReturn   | Storehouse | Storekeeper | Company |
        | SALES_RETURN | <SalesReturn> | 0001       | 0007        | 0001    |
      Then "<User>" is logged out
      And "<User>" is forwarded to the error page
      Examples:
        | User                                  | SalesReturn |
        | CreateOnlyUser                        | 2020000002  |
        | Mahmoud.Abdelaziz                     | 2020000002  |
        | Mahmoud.Abdelaziz.NoStoreHouse        | 2020000053  |
        | Mahmoud.Abdelaziz.NoStorekeeper       | 2020000053  |
        | Mahmoud.Abdelaziz.NoSalesReturn       | 2020000053  |
        | Mahmoud.Abdelaziz.NoGoodsReceiptTypes | 2020000053  |
