# Author: Niveen Magdy, Ahmad Hamed, Hossam Hassan , Eman Mansour & Engy Mohamed
# Reviewer: Somaya Ahmed
Feature: Activate Goods Receipt Sales Return

  Background:
    Given the following users and roles exist:
      | Name                         | Role                             |
      | Ali.Hasan                    | Storekeeper_Flexo                |
      | Mahmoud.Abdelaziz            | Storekeeper_Signmedia            |
      | Mahmoud.Abdelaziz.MadinaMain | Storekeeper_Signmedia_MadinaMain |

    And the following roles and sub-roles exist:
      | Role                             | Subrole                              |
      | Storekeeper_Flexo                | GRActivateOwner_Flexo                |
      | Storekeeper_Signmedia            | GRActivateOwner_Signmedia            |
      | Storekeeper_Signmedia_MadinaMain | GRActivateOwner_Signmedia_MadinaMain |

    And the following sub-roles and permissions exist:
      | Subrole                              | Permission            | Condition                                                   |
      | GRActivateOwner_Flexo                | GoodsReceipt:Activate | [purchaseUnitName='Flexo']                                  |
      | GRActivateOwner_Signmedia            | GoodsReceipt:Activate | [purchaseUnitName='Signmedia']                              |
      | GRActivateOwner_Signmedia_MadinaMain | GoodsReceipt:Activate | ([purchaseUnitName='Signmedia'] && [storehouseCode='0001']) |

    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate          | Type         | Storekeeper      | State |
      | 2020000026 | hr1       | 07-Aug-2020 10:00 AM  | SALES_RETURN | 0006 - Ahmed.Ali | Draft |
      | 2020000025 | hr1       | 29-July-2020 10:13 AM | SALES_RETURN | 0006 - Ahmed.Ali | Draft |
      | 2020000024 | hr1       | 29-July-2020 09:49 AM | SALES_RETURN | 0006 - Ahmed.Ali | Draft |

    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                 | BusinessUnit     |
      | 2020000026 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000025 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000024 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

    And the following SalesReturnData for GoodsReceipts exist:
      | GRCode     | SalesReturn | SalesReturnDocumentOwner                  | SalesReturnCustomer            | Notes |
      | 2020000026 | 2020000058  | Manar.Mohammed - Manar Mohammed           | 000007 - مطبعة أكتوبر الهندسية |       |
      | 2020000025 | 2020000057  | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية |       |
      | 2020000024 | 2020000053  | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية |       |

    # Items in GR 2020000026
    And GoodsReceipt with code "2020000026" has the following received items:
      | Item   | DifferenceReason |
      | 000051 |                  |

    And Item "000051 - Ink5" in GoodsReceipt (Based On SalesReturn) "2020000026" has the following received quantities:
      | ReceivedQty(UoE) | UoE                  | StockType        | Notes |
      | 1.0              | 0014 - Liter         | UNRESTRICTED_USE |       |
      | 1.0              | 0040 - Bottle-1Liter | UNRESTRICTED_USE |       |

    # Items in GR 2020000025
    And GoodsReceipt with code "2020000025" has the following received items:
      | Item   | DifferenceReason |
      | 000053 |                  |
      | 000051 |                  |

    And Item "000053 - Hot Laminated Frontlit Fabric roll 2" in GoodsReceipt (Based On SalesReturn) "2020000025" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | StockType     | Notes |
      | 99.0             | 0035 - Roll 1.27x50 | DAMAGED_STOCK |       |

    And Item "000051 - Ink5" in GoodsReceipt (Based On SalesReturn) "2020000025" has the following received quantities:
      | ReceivedQty(UoE) | UoE          | StockType     | Notes |
      | 1.0              | 0014 - Liter | DAMAGED_STOCK |       |

    # Items in GR 2020000024
    And GoodsReceipt with code "2020000024" has the following received items:
      | Item   | DifferenceReason                 |
      | 000053 | Item was damaged before delivery |

    And Item "000053 - Hot Laminated Frontlit Fabric roll 2" in GoodsReceipt (Based On SalesReturn) "2020000024" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | StockType        | Notes |
      | 1.0              | 0035 - Roll 1.27x50 | UNRESTRICTED_USE |       |

    And the following ReturnDetails for SalesReturnOrder exists:
      | Code       | SICode     | Customer                       | CurrencyISO |
      | 2020000058 | 2020000008 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2020000057 | 2020000004 | 000007 - مطبعة أكتوبر الهندسية | EGP         |
      | 2020000053 | 2020000003 | 000007 - مطبعة أكتوبر الهندسية | EGP         |

    And the following businessPartnerData for salesInvoics exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2020000008 | 000007 - مطبعة أكتوبر الهندسية | 2020000068 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2020000004 | 000007 - مطبعة أكتوبر الهندسية | 2020000067 | 0003 - 100% Advanced Payment | EGP      |             |
      | 2020000003 | 000007 - مطبعة أكتوبر الهندسية | 2020000053 | 0003 - 100% Advanced Payment | EGP      |             |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 31-Aug-2020 09:00 AM | 0002 - Signmedia | SalesInvoiceActivated |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 30-Jun-2020 09:00 AM | 0002 - Signmedia | Approved              |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved              |

    And the following SalesOrderData for GoodsIssues exist:
      | GICode     | Customer                       | Address                     | ContactPerson | RefDocument | SalesRepresentative | Comments |
      | 2020000011 | 000007 - مطبعة أكتوبر الهندسية | 66, Salah Salem St          | Wael Fathay   | 2020000068  | SeragEldin Meghawry |          |
      | 2020000006 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay   | 2020000067  | Ahmed.Al-Ashry      |          |
      | 2020000003 | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed     | Wael Fathay   | 2020000053  | Ahmed.Al-Ashry      |          |

    And the following GeneralData for GoodsIssues exist:
      | GICode     | Type                                                    | CreatedBy | State  | CreationDate         | Storekeeper              |
      | 2020000011 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1       | Active | 06-Aug-2020 12:01 PM | 0007 - Mahmoud.Abdelaziz |
      | 2020000006 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1       | Active | 27-Jul-2020 05:52 AM | 0006 - Ahmed.Ali         |
      | 2020000003 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | Admin     | Active | 06-Nov-2020 09:00 AM | 0007 - Mahmoud.Abdelaziz |

    And the following ItemData for GoodsIssues exist:
      | GICode     | Item                                          | UOM                  | Quantity | BaseUnit  | QtyBase | BatchNo | Price |
      | 2020000011 | 000051 - Ink5                                 | 0014 - Liter         | 1        | 0019 - M2 | 1       |         | 40.0  |
      | 2020000011 | 000051 - Ink5                                 | 0040 - Bottle-1Liter | 1        | 0019 - M2 | 1       |         | 30.0  |
      | 2020000006 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  | 99       | 0019 - M2 | 6286.5  |         | 2.0   |
      | 2020000006 | 000051 - Ink5                                 | 0014 - Liter         | 2        | 0019 - M2 | 2       |         | 2.0   |
      | 2020000003 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  | 2        | 0019 - M2 | 127     |         | 10.0  |

    And ONLY the following StoreTransaction records for RefDocument "Goods Issue - 2020000011" And Item "000051" with UOM "0040" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | StoreTransactionCode | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item          | UOM                  | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument        | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000030           |                    | 06-Aug-2020 12:01 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0040 - Bottle-1Liter |         | 1   |              | TAKE            |            | Goods Issue - 2020000011 | 0            | 2019000016     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And ONLY the following StoreTransaction records for RefDocument "Goods Issue - 2020000011" And Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | StoreTransactionCode | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item          | UOM          | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument        | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000029           |                    | 06-Aug-2020 12:01 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 1   |              | TAKE            |            | Goods Issue - 2020000011 | 0            | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And ONLY the following StoreTransaction records for RefDocument "Goods Issue - 2020000006" And Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | StoreTransactionCode | OriginalAddingDate | CreationDate          | StockType        | PurchaseUnit     | Item          | UOM          | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument        | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000025           |                    | 27-July-2020 03:52 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 2   |              | TAKE            |            | Goods Issue - 2020000006 | 0            | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And ONLY the following StoreTransaction records for RefDocument "Goods Issue - 2020000006" And Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | StoreTransactionCode | OriginalAddingDate | CreationDate          | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument        | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000024           |                    | 27-July-2020 07:52 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 1   |              | TAKE            |            | Goods Issue - 2020000006 | 0            | 2020000004     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000023           |                    | 27-July-2020 07:52 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 98  |              | TAKE            |            | Goods Issue - 2020000006 | 0            | 2020000002     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And ONLY the following StoreTransaction records for RefDocument "Goods Issue - 2020000003" And Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | StoreTransactionCode | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument        | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000022           |                    | 29-Jun-2020 11:08 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 2   |              | TAKE            |            | Goods Issue - 2020000003 | 0            | 2020000002     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And the following ADD (Reference) StoreTransactions Exist:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                  | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000004           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  |         | 20  | 0            | ADD             | 0          | 2020000003        | 19           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000002           | 05-Feb-2020 03:48 PM | 05-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  |         | 100 | 20           | ADD             | 20         | 2020000001        | 0            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000016           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0040 - Bottle-1Liter |         | 2   | 100          | ADD             | 100        | 2019000020        | 1            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000014           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0014 - Liter         |         | 13  | 100          | ADD             | 100        | 2019000020        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 10.0              |
      | DAMAGED_STOCK00020001000100010000530035    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | DAMAGED_STOCK    | 1.0               |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 0.0               |
    #EBS-7010 #EBS-6772 #EBS-6729
  Scenario Outline: (01) Activate GR - Update GR_SR State and create StoreTransaction by an authorized user - GR has only one item, one qty and one StoreTransaction (Happy Path)
    Given user is logged in as "<User>"
    And Last created store transaction was with code "2020000033"
    When "<User>" requests to activate GR with code "2020000024" at "07-Aug-2020 11:00 AM"
    Then GR with Code "2020000024" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  | ActivationDate       |
      | <User>        | 07-Aug-2020 11:00 AM | Active | 07-Aug-2020 11:00 AM |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate   | StockType        | BusinessUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Aug-2020 11:00 AM | 05-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 1.0 | 20           | ADD             | 20         | 2020000024        | 1.0          | 2020000022     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2020000034"
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 11.0              |

    And The State of SalesReturnOrder with code "2020000053" is changed to "GoodsReceiptActivated"
    And a success notification is sent to "<User>" with the following message "GR-msg-12"
    Examples:
      | User                         |
      | Mahmoud.Abdelaziz            |
      | Mahmoud.Abdelaziz.MadinaMain |

     #EBS-7010 #EBS-6772 #EBS-6729
  Scenario: (02) Activate GR - Update GR_SR State and create StoreTransaction by an authorized user - GR has multiple items (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2020000025" at "07-Aug-2020 11:00 AM"
    Then GR with Code "2020000025" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  | ActivationDate       |
      | Mahmoud.Abdelaziz | 07-Aug-2020 11:00 AM | Active | 07-Aug-2020 11:00 AM |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate   | StockType     | BusinessUnit     | Item                                          | UOM                 | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Aug-2020 11:00 AM | 07-Jan-2019 09:30 AM | DAMAGED_STOCK | 0002 - Signmedia | 000051 - Ink5                                 | 0014 - Liter        |         | 1.0  | 100          | ADD             | 100        | 2020000025        | 1.0          | 2020000025     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Aug-2020 11:00 AM | 05-Feb-2020 03:48 PM | DAMAGED_STOCK | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 98.0 | 20           | ADD             | 20         | 2020000025        | 98.0         | 2020000023     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Aug-2020 11:00 AM | 05-Feb-2020 03:49 PM | DAMAGED_STOCK | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 1.0  | 0            | ADD             | 0          | 2020000025        | 1.0          | 2020000024     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2020000036"
    And the following StockAvailabilities will be created or updated:
      | Code                                    | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType     | AvailableQuantity |
      | DAMAGED_STOCK00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | DAMAGED_STOCK | 100.0             |
      | DAMAGED_STOCK00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | DAMAGED_STOCK | 1.0               |
    And The State of SalesReturnOrder with code "2020000057" is changed to "GoodsReceiptActivated"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"

     #EBS-7010 #EBS-6772 #EBS-6729
  Scenario: (03) Activate GR - Update GR_SR State and create StoreTransaction by an authorized user - GR has one item with multiple quantities (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2020000026" at "07-Aug-2020 11:00 AM"
    Then GR with Code "2020000026" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  | ActivationDate       |
      | Mahmoud.Abdelaziz | 07-Aug-2020 11:00 AM | Active | 07-Aug-2020 11:00 AM |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate   | StockType        | BusinessUnit     | Item          | UOM                  | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Aug-2020 11:00 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter         |         | 1.0 | 100          | ADD             | 100        | 2020000026        | 1.0          | 2020000029     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Aug-2020 11:00 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0040 - Bottle-1Liter |         | 1.0 | 100          | ADD             | 100        | 2020000026        | 1.0          | 2020000030     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2020000035"
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item          | UOM                  | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0014 - Liter         | UNRESTRICTED_USE | 1.0               |
      | UNRESTRICTED_USE00020001000100010000510040 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0040 - Bottle-1Liter | UNRESTRICTED_USE | 1.0               |

    And The State of SalesReturnOrder with code "2020000058" is changed to "GoodsReceiptActivated"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"
