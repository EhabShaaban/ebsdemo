# Author: Ahmad Hamed, Engy Abd El Aziz
# Reviewer:

Feature: Read All PurchaseOrders For Goods Receipt
  """
    1. Return Local PurchaseOrders in state Shipped, NotReceived
    2. Return Import PurchaseOrders in state Cleared, NotReceived
    3. Should filter PurchaseOrders according to logged in user BusinessUnit
  """
  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                   |
      | Storekeeper_Signmedia | POViewerLimited_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission            | Condition                      |
      | POViewerLimited_Signmedia | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | PurchaseOrder:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User              | Permission            | Condition                       |
      | Mahmoud.Abdelaziz | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']      |
      | Mahmoud.Abdelaziz | PurchaseOrder:ReadAll | [purchaseUnitName='Corrugated'] |
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                           | CreatedBy | CreationDate       | DocumentOwner |
      # IMPORT
      | 2021000001 | IMPORT_PO | Draft                           | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000002 | IMPORT_PO | OpenForUpdates, NotReceived     | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000003 | IMPORT_PO | WaitingApproval, NotReceived    | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO | Approved, NotReceived           | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | IMPORT_PO | Rejected, NotReceived           | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000006 | IMPORT_PO | Confirmed, NotReceived          | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000007 | IMPORT_PO | FinishedProduction, NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000008 | IMPORT_PO | Shipped, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000009 | IMPORT_PO | Arrived, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Cleared PO
      | 2021000010 | IMPORT_PO | Cleared, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Cleared PO with different BusinessUnit
      | 2021000011 | IMPORT_PO | Cleared, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Cleared PO received before
      | 2021000012 | IMPORT_PO | Cleared, Received               | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000013 | IMPORT_PO | DeliveryComplete, Received      | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000014 | IMPORT_PO | Cancelled                       | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # LOCAL
      | 2021000015 | LOCAL_PO  | Draft                           | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000016 | LOCAL_PO  | OpenForUpdates, NotReceived     | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000017 | LOCAL_PO  | WaitingApproval, NotReceived    | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000018 | LOCAL_PO  | Approved, NotReceived           | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000019 | LOCAL_PO  | Rejected, NotReceived           | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000020 | LOCAL_PO  | Confirmed, NotReceived          | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Shipped PO
      | 2021000021 | LOCAL_PO  | Shipped, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Shipped PO with different BusinessUnit
      | 2021000022 | LOCAL_PO  | Shipped, NotReceived            | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      # Shipped PO received before
      | 2021000023 | LOCAL_PO  | Shipped, Received               | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000024 | LOCAL_PO  | DeliveryComplete, Received      | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000025 | LOCAL_PO  | Cancelled                       | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      # IMPORT
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000006 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000007 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000008 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000009 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000010 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000011 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000012 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000013 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000014 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      # LOCAL
      | 2021000015 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000016 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000017 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000018 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000019 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000020 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000021 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000022 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000023 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000024 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000025 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    And the total number of existing records of PurchaseOrders is 25

  #EBS-7361
  Scenario: (01) Read list of PurchaseOrders dropdown filtered by (Shipped,NotReceived | Cleared,NotReceived) states by an authorized user with condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read drop-down of PurchaseOrder for State (Shipped,NotReceived | Cleared,NotReceived)
    Then the following PurchaseOrders will be presented to "Mahmoud.Abdelaziz":
      | Code       |
      | 2021000021 |
      | 2021000010 |
    And the total number of PurchaseOrders returned to "Mahmoud.Abdelaziz" is 2

  #EBS-7361
  Scenario: (02) Read list of PurchaseOrders dropdown filtered by (Shipped,NotReceived | Cleared,NotReceived) states by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read drop-down of PurchaseOrder for State (Shipped,NotReceived | Cleared,NotReceived)
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page