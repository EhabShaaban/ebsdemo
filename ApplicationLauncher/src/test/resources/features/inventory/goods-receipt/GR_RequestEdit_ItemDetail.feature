Feature: Request Edit Detail in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name                           | Role                                       |
      | hr1                            | SuperUser                                  |
      | Mahmoud.Abdelaziz              | Storekeeper_Signmedia                      |
      | Ali.Hasan                      | Storekeeper_Flexo                          |
      | Ali.Hasan                      | Storekeeper_Corrugated                     |
      | Mahmoud.Abdelaziz_NoMeasures   | Storekeeper_Signmedia_CannotReadMeasures   |
      | Mahmoud.Abdelaziz_NoStockTypes | Storekeeper_Signmedia_CannotReadStockTypes |
      | Afaf                           | FrontDesk                                  |
    And the following roles and sub-roles exist:
      | Role                                       | Subrole            |
      | SuperUser                                  | SuperUserSubRole   |
      | Storekeeper_Signmedia                      | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadMeasures   | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadStockTypes | GROwner_Signmedia  |
      | Storekeeper_Flexo                          | GROwner_Flexo      |
      | Storekeeper_Corrugated                     | GROwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | SuperUserSubRole   | *:*                     |                                 |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:UpdateItem | [purchaseUnitName='Corrugated'] |
    And the following GoodsReceipts exist:
      | Code       | BusinessUnit      | State  | Type                                                   |
      | 2018000008 | 0002 - Signmedia  | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | 0001 - Flexo      | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000011 | 0006 - Corrugated | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000002 | 0002 - Signmedia  | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |

      | 2020000023 | 0002 - Signmedia  | Draft  | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2020000020 | 0001 - Flexo      | Draft  | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2020000022 | 0002 - Signmedia  | Active | SALES_RETURN - Goods Receipt Based on Sales Return     |


    And the following GoodsReceipts exist with the following Items and Quantities:
      | Code       | Item                                                           | QtyID |
      | 2018000008 | 000001 - Hot Laminated Frontlit Fabric roll                    | 1     |
      | 2018000003 | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 3     |
      | 2018000011 | 000012 - Ink2                                                  | 7     |
      | 2018000002 | 000001 - Hot Laminated Frontlit Fabric roll                    | 8     |

      | 2020000023 | 000053 - Hot Laminated Frontlit Fabric roll 2                  | 143   |
      | 2020000020 | 000051 - Ink5                                                  | 137   |
      | 2020000022 | 000053 - Hot Laminated Frontlit Fabric roll 2                  | 136   |

    And the following GoodsReceipts exist with the following Items and Batches:
      | Code       | ItemCode | BatchID |
      | 2018000008 | 000003   | 1       |
    And edit session is "30" minutes

  ######## Happy Paths
  # EBS-4034
  # EBS-7062
  Scenario Outline: (01-a) Request to Edit/Cancel GR Item Detail - where GR is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
      | ReadStockTypes  |
    And the following mandatory fields are returned to "<User>":
      | MandatoriesFields |
      | receivedQtyUoE    |
      | unitOfEntryCode   |
      | stockType         |
    And the following editable fields are returned to "<User>":
      | EditableFields  |
      | receivedQtyUoE  |
      | unitOfEntryCode |
      | stockType       |
      | notes           |
    Examples:
      | GRCode     | ItemCode | User              | DetailID |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | 1        |
      | 2018000003 | 000005   | Ali.Hasan         | 3        |
      | 2018000011 | 000012   | Ali.Hasan         | 7        |

      | 2020000023 | 000053   | Mahmoud.Abdelaziz | 143      |
      | 2020000020 | 000051   | Ali.Hasan         | 137      |
  # EBS-4034
  # EBS-7062
  Scenario Outline: (01-b) Request to Edit/Cancel GR Item Detail - where GR is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
      | ReadStockTypes  |
    And the following mandatory fields are returned to "<User>":
      | MandatoriesFields |
      | batchCode         |
      | receivedQtyUoE    |
      | unitOfEntryCode   |
      | productionDate    |
      | expirationDate    |
      | stockType         |
    And the following editable fields are returned to "<User>":
      | EditableFields  |
      | receivedQtyUoE  |
      | productionDate  |
      | batchCode       |
      | expirationDate  |
      | unitOfEntryCode |
      | stockType       |
      | notes           |
    Examples:
      | GRCode     | ItemCode | User              | DetailID |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | 1        |

   # EBS-4034
   # EBS-7062
  Scenario Outline: (01-c) Request to Edit/Cancel GR Item Detail - where GR is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadStockTypes  |
    Examples:
      | GRCode     | ItemCode | User                         | DetailID |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz_NoMeasures | 1        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz_NoMeasures | 1        |
      | 2020000023 | 000053   | Mahmoud.Abdelaziz_NoMeasures | 143      |

   # EBS-4034
   # EBS-7062
  Scenario Outline: (01-d) Request to Edit/Cancel GR Item Detail - where GR is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads |
      | ReadMeasures    |
    Examples:
      | GRCode     | ItemCode | User                           | DetailID |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz_NoStockTypes | 1        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz_NoStockTypes | 1        |
      | 2020000023 | 000053   | Mahmoud.Abdelaziz_NoStockTypes | 143      |

  ######## Exceptions
  # EBS-4034
  # EBS-7062
  Scenario Outline: (02) Request to Edit/Cancel GR Item Detail - where GR doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully
    When "Mahmoud.Abdelaziz" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"
    Examples:
      | GRCode     | ItemCode | DetailID |
      | 2018000008 | 000001   | 1        |
      | 2018000008 | 000003   | 1        |
      | 2020000023 | 000053   | 143      |


  # EBS-4034
  # EBS-7062
  Scenario Outline: (03) Request to Edit/Cancel GR Item Detail - where Item doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully
    When "Mahmoud.Abdelaziz" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | DetailID |
      | 2018000008 | 000001   | 1        |
      | 2018000008 | 000003   | 1        |

  # EBS-4034
  # EBS-7062
  Scenario Outline: (04) Request to Edit/Cancel GR Item Detail - where Detail is deleted by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted Item Detail with id "<DetailID>" with type "<DetailType>" of Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully at "07-Jan-2019 09:31 AM"
    When "Mahmoud.Abdelaziz" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | DetailID | DetailType |
      | 2018000008 | 000001   | 1        | Quantity   |
      | 2018000008 | 000003   | 1        | Batch      |
      | 2020000023 | 000053   | 143      | Quantity   |

  # EBS-4034
  # EBS-7062
  Scenario Outline: (05) Request to Edit/Cancel GR Item Detail - when ReceivedItems section is locked by another user - GR (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"
    Examples:
      | GRCode     | ItemCode | DetailID |
      | 2018000008 | 000001   | 1        |
      | 2020000023 | 000053   | 143      |

  # EBS-4034
  Scenario Outline: (06) Request to Edit/Cancel GR Item Detail - in state where this action is not allowed (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-03"
    Examples:
      | DetailID | GRCode     | ItemCode |
      | 8        | 2018000002 | 000001   |
      | 136      | 2020000022 | 000053   |

  ######## Abuse Cases
  # EBS-4034
  # EBS-7062
  Scenario Outline: (07) Request to Edit/Cancel GR Item Detail -by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to Edit Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      | GRCode     | ItemCode | DetailID |
      | Ali.Hasan | 2018000008 | 000001   | 1        |
      | Ali.Hasan | 2020000023 | 000053   | 143      |

  ######## Happy Paths
  # EBS-4034
  # EBS-7062
  Scenario Outline: (08) Request to Edit/Cancel GR Item Detail - where GR is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to Cancel Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    Examples:
      | GRCode     | ItemCode | User              | DetailID |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | 1        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | 1        |
      | 2018000003 | 000005   | Ali.Hasan         | 3        |
      | 2018000011 | 000012   | Ali.Hasan         | 7        |
      | 2020000023 | 000053   | Mahmoud.Abdelaziz | 143      |
      | 2020000020 | 000051   | Ali.Hasan         | 137      |

  ######## Exceptions
  # EBS-4034
  # EBS-7062
  Scenario Outline: (09) Request to Edit/Cancel GR Item Detail - where Item doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully
    When "Mahmoud.Abdelaziz" requests to Cancel Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | DetailID |
      | 2018000008 | 000001   | 1        |
      | 2018000008 | 000003   | 1        |

  # EBS-4034
  # EBS-7062
  Scenario Outline: (10) Request to Edit/Cancel GR Item Detail - after edit session expires (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And "Mahmoud.Abdelaziz" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" requests to Cancel Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with Code "<GRCode>" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"
    Examples:
      | GRCode     | ItemCode | DetailID |
      | 2018000008 | 000001   | 1        |
      | 2018000008 | 000003   | 1        |
      | 2020000023 | 000053   | 143      |

  # EBS-4034
  # EBS-7062
  Scenario Outline: (11) Request to Edit/Cancel GR Item Detail - in state where this action is not allowed (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to Cancel Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"
    Examples:
      | DetailID | GRCode     | ItemCode |
      | 8        | 2018000002 | 000001   |
      | 136      | 2020000022 | 000053   |

  ######## Abuse Cases
  # EBS-4034
  # EBS-7062
  Scenario Outline: (12) Request to Edit/Cancel GR Item Detail - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to Cancel Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      | GRCode     | ItemCode | DetailID |
      | Ali.Hasan | 2018000008 | 000001   | 1        |
      | Afaf      | 2018000008 | 000001   | 1        |
      | Ali.Hasan | 2020000023 | 000053   | 143      |
      | Afaf      | 2020000023 | 000053   | 143      |