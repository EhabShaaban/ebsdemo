# Author: Niveen Magdy (EBS-6785)
# Reviewer: Somaya Ahmed (14-Jul-2020)

Feature: Allowed Action For Goods Receipt (Home Screen)

  Background:
    Given the following users and roles exist:
      | Name           | Role                        |
      | Afaf           | FrontDesk                   |
      | Ali.Hasan      | Storekeeper_Flexo           |
      | Amr.Khalil     | PurchasingResponsible_Flexo |
      | CreateOnlyUser | CreateOnlyRole              |
    And the following roles and sub-roles exist:
      | Role                        | Subrole           |
      | PurchasingResponsible_Flexo | GRViewer_Flexo    |
      | Storekeeper_Flexo           | GROwner_Flexo     |
      | Storekeeper_Flexo           | GRViewer_Flexo    |
      | CreateOnlyRole              | CreateOnlySubRole |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission           | Condition                  |
      | GRViewer_Flexo    | GoodsReceipt:ReadAll | [purchaseUnitName='Flexo'] |
      | GROwner_Flexo     | GoodsReceipt:Create  |                            |
      | CreateOnlySubRole | GoodsReceipt:Create  |                            |
    And the following users doesn't have the following permissions:
      | User       | Permission           |
      | Afaf       | GoodsReceipt:ReadAll |
      | Afaf       | GoodsReceipt:Create  |
      | Amr.Khalil | GoodsReceipt:Create  |

  Scenario: (01) Read allowed actions in Goods Receipt Home Screen - User is authorized to Create & ReadAll  (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to read actions of GoodsReceipt in home screen
    Then the following actions are displayed to "Ali.Hasan":
      | AllowedActions |
      | ReadAll        |
      | Create         |

  Scenario: (02) Read allowed actions in Goods Receipt Home Screen - User is authorized to ReadAll only (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read actions of GoodsReceipt in home screen
    Then the following actions are displayed to "Amr.Khalil":
      | AllowedActions |
      | ReadAll        |

  Scenario: (03) Read allowed actions in Goods Receipt Home Screen - User is authorized to Create only (Happy Path)
    Given user is logged in as "CreateOnlyUser"
    When "CreateOnlyUser" requests to read actions of GoodsReceipt in home screen
    Then the following actions are displayed to "CreateOnlyUser":
      | AllowedActions |
      | Create         |

  Scenario: (04) Read allowed actions in Goods Receipt Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of GoodsReceipt in home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"