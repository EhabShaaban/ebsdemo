Feature: Delete Detail in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | hr1               | SuperUser              |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ali.Hasan         | Storekeeper_Corrugated |
      | Afaf              | FrontDesk              |
    And the following roles and sub-roles exist:
      | Role                   | Subrole            |
      | SuperUser              | SuperUserSubRole   |
      | Storekeeper_Signmedia  | GROwner_Signmedia  |
      | Storekeeper_Flexo      | GROwner_Flexo      |
      | Storekeeper_Corrugated | GROwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | SuperUserSubRole   | *:*                     |                                 |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:UpdateItem | [purchaseUnitName='Corrugated'] |
    And the following GoodsReceipts exist:
      | Code       | BusinessUnit      | State  | Type                                                   |
      | 2020000025 | 0002 - Signmedia  | Draft  | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2020000021 | 0001 - Flexo      | Active | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2018000011 | 0006 - Corrugated | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000008 | 0002 - Signmedia  | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | 0001 - Flexo      | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000002 | 0002 - Signmedia  | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | GoodsReceipt:UpdateItem |

    And the following GoodsReceipts exist with the following Items and Quantities:
      | Code       | Item                                                           | QtyID |
      | 2020000025 | 000053 - Hot Laminated Frontlit Fabric roll 2                  | 141   |
      | 2020000025 | 000051 - Ink5                                                  | 142   |
      | 2020000021 | 000051 - Ink5                                                  | 135   |
      | 2018000011 | 000012 - Ink2                                                  | 7     |
      | 2018000008 | 000001 - Hot Laminated Frontlit Fabric roll                    | 1     |
      | 2018000003 | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 3     |
      | 2018000002 | 000001 - Hot Laminated Frontlit Fabric roll                    | 8     |


    And the following GoodsReceipts exist with the following Items and Batches:
      | Code       | ItemCode | BatchID |
      | 2018000008 | 000003   | 1       |

  ######## Happy Paths
  #EBS-4039 #EBS-7061
  Scenario Outline:(01) Delete GR Item Detail - where GR is Draft by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" at "7-Aug-2020 9:30 am"
    Then detail with id "<DetailID>" with type "<DetailType>" is deleted successfully
    And Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate     |
      | <User>        | 7-Aug-2020 9:30 am |
    And GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate     |
      | <User>        | 7-Aug-2020 9:30 am |
    And a success notification is sent to "<User>" with the following message "Gen-msg-18"
    Examples:
      | DetailID | User              | GRCode     | ItemCode | DetailType |
      | 142      | Mahmoud.Abdelaziz | 2020000025 | 000051   | Quantity   |
      | 1        | Mahmoud.Abdelaziz | 2018000008 | 000001   | Quantity   |
      | 1        | Mahmoud.Abdelaziz | 2018000008 | 000003   | Batch      |
      | 3        | Ali.Hasan         | 2018000003 | 000005   | Quantity   |
      | 7        | Ali.Hasan         | 2018000011 | 000012   | Quantity   |

  ######## Exceptions
  #EBS-4039 #EBS-7061
  Scenario Outline: (02) Delete GR Item Detail - where GR doesn't exist (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | User              | DetailID | GRCode     | ItemCode |
      | Ali.Hasan         | 142      | 2020000025 | 000051   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000001   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000003   |

  #EBS-4039
  Scenario Outline: (03) Delete GR Item Detail - where Item doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully
    When "Mahmoud.Abdelaziz" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"
    Examples:
      | DetailID | GRCode     | ItemCode |
      | 1        | 2018000008 | 000001   |
      | 1        | 2018000008 | 000003   |

  #EBS-4039 #EBS-7061
  Scenario Outline: (04) Delete GR Item Detail - where Detail doesn't exist (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" first deletes detail with id "<DetailID>" with type "<DetailType>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" successfully
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | User              | DetailID | GRCode     | ItemCode | DetailType |
      | Mahmoud.Abdelaziz | 142      | 2020000025 | 000051   | Quantity   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000001   | Quantity   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000003   | Batch      |

  #EBS-4039 #EBS-7061
  Scenario Outline: (05) Delete GR Item Detail - in state where this action is not allowed (Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-21"
    Examples:
      | User              | DetailID | GRCode     | ItemCode |
      | Mahmoud.Abdelaziz | 8        | 2018000002 | 000001   |
      | Ali.Hasan         | 135      | 2020000021 | 000051   |

  #EBS-4039 #EBS-7061
  Scenario Outline: (06) Delete GR Item Detail - when ReceivedItems section is locked by another user (Exception Case)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "hr1" requests to add detail to Item with code "<ItemCode>" at "07-Aug-2020 09:10 AM" So ReceivedItems section of GoodsReceipt with code "<GRCode>" is locked
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" at "7-Aug-2020 9:30 am"
    Then an error notification is sent to "<User>" with the following message "Gen-msg-02"
    Examples:
      | User              | DetailID | GRCode     | ItemCode |
      | Mahmoud.Abdelaziz | 142      | 2020000025 | 000051   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000001   |
      | Mahmoud.Abdelaziz | 1        | 2018000008 | 000003   |

  ####### Abuse Cases
  #EBS-4039 #EBS-7061
  Scenario Outline: (07) Delete GR Item Detail - by an unauthorized user (due to condition or not) - (Unauthorized access/Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete detail with id "<DetailID>" of Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | DetailID | User              | GRCode     | ItemCode | DetailType |
      | 142      | Ali.Hasan         | 2020000025 | 000051   | Quantity   |
      | 1        | Mahmoud.Abdelaziz | 2018000003 | 000005   | Quantity   |
      | 1        | Ali.Hasan         | 2018000008 | 000003   | Batch      |
      | 1        | Afaf              | 2018000003 | 000005   | Quantity   |
      | 1        | Afaf              | 2018000008 | 000003   | Batch      |
