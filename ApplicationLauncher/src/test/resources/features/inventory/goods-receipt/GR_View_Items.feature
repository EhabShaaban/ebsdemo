# Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
# Reviewer: Somaya Ahmed and Hosam Bayomy (26-Jul- 2020)

Feature: ReadItems section in GR

  Background:
    Given the following users and roles exist:
      | Name              | Role                            |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia           |
      | Ali.Hasan         | Storekeeper_Flexo               |
      | Ali.Hasan         | Storekeeper_Corrugated          |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia |
      | Afaf              | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | Storekeeper_Signmedia           | GROwner_Signmedia  |
      | Storekeeper_Flexo               | GROwner_Flexo      |
      | Storekeeper_Corrugated          | GROwner_Corrugated |
      | PurchasingResponsible_Signmedia | GRViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission             | Condition                       |
      | GROwner_Signmedia  | GoodsReceipt:Delete    | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:ReadItems | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:ReadItems | [purchaseUnitName='Corrugated'] |
      | GRViewer_Signmedia | GoodsReceipt:ReadItems | [purchaseUnitName='Signmedia']  |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | GoodsReceipt:ReadItems |
    And the following users have the following permissions without the following conditions:
      | User      | Permission                       | Condition                      |
      | Ali.Hasan | GoodsReceipt:ReadSalesReturnData | [purchaseUnitName='Signmedia'] |

    And the following GoodsReceipts exist with the following BusinessPartner and ReferenceDoc data:
      | Code       | Type                                                   | BusinessUnit      | BusinessPartner                | State  | RefDocument |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia  | 000002 - Zhejiang              | Draft  | 2018000001  |
      | 2018000003 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0001 - Flexo      | 000001 - Siegwerk              | Draft  | 2018000003  |
      | 2018000012 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0006 - Corrugated | 000002 - Zhejiang              | Draft  | 2018000045  |
      | 2020000021 | SALES_RETURN - Goods Receipt Based on Sales Return     | 0001 - Flexo      | 000006 - المطبعة الأمنية       | Active | 2020000002  |
      | 2020000025 | SALES_RETURN - Goods Receipt Based on Sales Return     | 0002 - Signmedia  | 000007 - مطبعة أكتوبر الهندسية | Draft  | 2020000057  |

    #### GR 2018000008 (Based On Purchase Order)
    And GoodsReceipt with code "2018000008" has the following received items:
      | Item                                        | DifferenceReason                             |
      | 000001 - Hot Laminated Frontlit Fabric roll | Overdelivery to opltimize container capacity |
      | 000003 - Flex Primer Varnish E33            | Samples taken by clearance authorities       |
    And Item "000001 - Hot Laminated Frontlit Fabric roll" in GoodsReceipt (Based On PurchaseOrder) "2018000008" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | Notes          | StockType     |
      | 40.123456        | 0029 - Roll 2.20x50 | Package Opened | DAMAGED_STOCK |
      | 10.123456        | 0032 - Roll 3.20x50 | Package Opened | DAMAGED_STOCK |
    And Item "000003 - Flex Primer Varnish E33" in GoodsReceipt "2018000008" has the following batches:
      | BatchNo  | ProdDate    | ExpireDate  | BatchQty(UoE) | UoE              | StockType        | Notes                   |
      | 14536444 | 23-Jan-2018 | 23-Jan-2020 | 2.123456      | 0036 - Drum-10Kg | UNRESTRICTED_USE |                         |
      | 14536777 | 25-Jan-2018 | 25-Jan-2020 | 9.123456      | 0036 - Drum-10Kg | DAMAGED_STOCK    | Crashed while discharge |

    #### GR 2018000003 (Based On Purchase Order)
    And GoodsReceipt with code "2018000003" has the following received items:
      | Item                                                           | DifferenceReason |
      | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 |                  |
    And Item "000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50" in GoodsReceipt (Based On PurchaseOrder) "2018000003" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | Notes                                                    | StockType        |
      | 1002.0           | 0034 - Roll 1.07x50 | يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة | UNRESTRICTED_USE |

    #### GR 2018000012 (Based On Purchase Order)
    And GoodsReceipt with code "2018000012" has the following received items:
      | Item          | DifferenceReason                                                     |
      | 000012 - Ink2 | يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك |
    And Item "000012 - Ink2" in GoodsReceipt (Based On PurchaseOrder) "2018000012" has the following received quantities:
      | ReceivedQty(UoE) | UoE                    | Notes | StockType        |
      | 14.123456        | 0042 - Bottle-2Liter   |       | UNRESTRICTED_USE |
      | 15.123456        | 0043 - Bottle-2.5Liter |       | UNRESTRICTED_USE |

     #### GR 2020000021 (Based On Sales Return)
    And GoodsReceipt with code "2020000021" has the following received items:
      | Item          | DifferenceReason |
      | 000051 - Ink5 |                  |
    And Item "000051 - Ink5" in GoodsReceipt (Based On SalesReturn) "2020000021" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | StockType     | Notes |
      | 9                | 0029 - Roll 2.20x50 | DAMAGED_STOCK |       |

     #### GR 2020000025 (Based On Sales Return)
    And GoodsReceipt with code "2020000025" has the following received items:
      | Item                                          | DifferenceReason |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 |                  |
      | 000051 - Ink5                                 |                  |

    ### To related Items to their BaseUnits as well as Conversion factor to BaseUnits => be able to Calculate ReceivedQty(Base)
    And the following items exist:
      | Code   | Name                                                  | Base         | IsBatchManaged |
      | 000001 | Hot Laminated Frontlit Fabric roll                    | 0019 - M2    | false          |
      | 000003 | Flex Primer Varnish E33                               | 0003 - Kg    | true           |
      | 000005 | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 0019 - M2    | false          |
      | 000012 | Ink2                                                  | 0014 - Liter | false          |
      | 000051 | Ink5                                                  | 0019 - M2    | false          |
      | 000053 | Hot Laminated Frontlit Fabric roll 2                  | 0019 - M2    | false          |
    And the following UnitOfMeasures section exist in Item with code "000001":
      | AlternativeUnit     | ConversionFactor | OldItemNumber     |
      | 0029 - Roll 2.20x50 | 110              | 12345678910111225 |
      | 0032 - Roll 3.20x50 | 160              | 12345678910111226 |
    And the following UnitOfMeasures section exist in Item with code "000003":
      | AlternativeUnit  | ConversionFactor | OldItemNumber     |
      | 0037 - Drum-50Kg | 50               | 12345678910111233 |
      | 0036 - Drum-10Kg | 10               | 12345678910111232 |
    And the following UnitOfMeasures section exist in Item with code "000005":
      | AlternativeUnit     | ConversionFactor | OldItemNumber     |
      | 0034 - Roll 1.07x50 | 53.5             | 12345678910111230 |

    And the following UnitOfMeasures section exist in Item with code "000012":
      | AlternativeUnit        | ConversionFactor | OldItemNumber     |
      | 0042 - Bottle-2Liter   | 2                | 12345678910111238 |
      | 0043 - Bottle-2.5Liter | 2.5              | 12345678910111239 |

    And the following UnitOfMeasures section exist in Item with code "000051":
      | AlternativeUnit      | ConversionFactor | OldItemNumber     |
      | 0040 - Bottle-1Liter | 1                | 12345678910111231 |
      | 0029 - Roll 2.20x50  | 110              | 12345678910111231 |
      | 0014 - Liter         | 1                | 12345678910111231 |

    And the following UnitOfMeasures section exist in Item with code "000053":
      | AlternativeUnit     | ConversionFactor | OldItemNumber     |
      | 0035 - Roll 1.27x50 | 63.5             | 12345678910111231 |

    ### To be able to retreive QtyInDN/PL from POs and display them in GRs (based on POs)
    And PurchaseOrder with code "2018000001" has the following OrderItems:
      | ItemCode | QtyInDN | BaseUnit  |
      | 000001   |         | 0019 - M2 |
      | 000002   |         | 0019 - M2 |
      | 000003   |         | 0003 - Kg |

    And PurchaseOrder with code "2018000003" has the following OrderItems:
      | ItemCode | QtyInDN | BaseUnit  |
      | 000004   | 100     | 0003 - Kg |

    And PurchaseOrder with code "2018000045" has the following OrderItems:
      | ItemCode | QtyInDN | BaseUnit     |
      | 000003   | 340.00  | 0003 - Kg    |
      | 000004   | 1500.50 | 0003 - Kg    |
      | 000012   | 2000.50 | 0014 - Liter |
     ### To be able to retreive ReturnQtyinBaseUnit from SalesReturns and display them in GRs (based on Sales Returns)
    And SalesReturn with code "2020000002" has the following OrderItems:
      | Item          | ReturnQuantity | OrderUnit           | ReturnQtyinBaseUnit | BaseUnit  |
      | 000051 - Ink5 | 10.00          | 0029 - Roll 2.20x50 | 1100.00             | 0019 - M2 |

    ## To related Items to their ItemCodeAtVendor, and and display them in GRs (based on POs)
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000052  | 000012   | 000001     | 0042    | GDH670-aaa       | 100.00   | 0001         | 0006             | 02-Aug-2018 10:30 AM |
      | 000053  | 000012   | 000001     | 0043    | GDH670-bbb       | 100.00   | 0001         | 0006             | 02-Aug-2018 10:30 AM |
      | 000058  | 000005   | 000002     | 0034    | GDH670-eee       | 100.00   | 0001         | 0001             | 02-Aug-2018 10:30 AM |


  #### Happy Path  ####

  # EBS-1412
  Scenario: (01) View GR Received Items - by an authorized user who has one role - Item is in order with empty QtyinDNPL  (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view ReceivedItems section of GR with code "2018000008"
    Then the following values of ReceivedItems section are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                           | BaseUnit  | QtyInDN | DifferenceReason                             |
      | 000003   | Flex Primer Varnish E33            | 0003 - Kg |         | Samples taken by clearance authorities       |
      | 000001   | Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | Overdelivery to opltimize container capacity |
    And the following quantity data for Item "000001" is displayed to "Gehan.Ahmed":
      | ReceivedQty(UoE) | UoE                 | Notes          | StockType     | ItemCodeAtVendor |
      | 40.123456        | 0029 - Roll 2.20x50 | Package Opened | DAMAGED_STOCK | GDF530-440       |
      | 10.123456        | 0032 - Roll 3.20x50 | Package Opened | DAMAGED_STOCK | GDF630-440       |
    And the following batch data for Item "000003" is displayed to "Gehan.Ahmed":
      | BatchNo  | ProdDate    | ExpireDate  | BatchQty(UoE) | UoE              | StockType        | DefectReason            | ItemCodeAtVendor |
      | 14536444 | 23-Jan-2018 | 23-Jan-2020 | 2.123456      | 0036 - Drum-10Kg | UNRESTRICTED_USE |                         | GDH670-777       |
      | 14536777 | 25-Jan-2018 | 25-Jan-2020 | 9.123456      | 0036 - Drum-10Kg | DAMAGED_STOCK    | Crashed while discharge | GDH670-777       |

  # EBS-1412
  Scenario: (02) View GR Received Items - by an authorized user who has two role Flexo - Item is not in PO (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view ReceivedItems section of GR with code "2018000003"
    Then the following values of ReceivedItems section are displayed to "Ali.Hasan":
      | ItemCode | ItemName                                              | BaseUnit  | QtyInDN | DifferenceReason |
      | 000005   | self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 0019 - M2 |         |                  |
    And the following quantity data for Item "000005" is displayed to "Ali.Hasan":
      | ReceivedQty(UoE) | UoE                 | Notes                                                    | StockType        | ItemCodeAtVendor |
      | 1002.0           | 0034 - Roll 1.07x50 | يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة | UNRESTRICTED_USE | GDH670-ccc       |

  # EBS-1412
  Scenario: (03) View GR Received Items - by an authorized user who has two role Corrugated - Item is in order with a value in QtyinDNPL(Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view ReceivedItems section of GR with code "2018000012"
    Then the following values of ReceivedItems section are displayed to "Ali.Hasan":
      | ItemCode | ItemName | BaseUnit     | QtyInDN | DifferenceReason                                                     |
      | 000012   | Ink2     | 0014 - Liter | 2000.50 | يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك |
    And the following quantity data for Item "000012" is displayed to "Ali.Hasan":
      | ReceivedQty(UoE) | UoE                    | Notes | StockType        | ItemCodeAtVendor |
      | 14.123456        | 0042 - Bottle-2Liter   |       | UNRESTRICTED_USE | GDH670-aaa       |
      | 15.123456        | 0043 - Bottle-2.5Liter |       | UNRESTRICTED_USE | GDH670-bbb       |

  #EBS-6769
  Scenario: (04) View GR Received Items - of type GR-based on Sales Return, by an authorized user who has two role Flexo - Item is in SalesReturn (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view ReceivedItems section of GR with code "2020000021"
    Then the following values of ReceivedItems section are displayed to "Ali.Hasan":
      | ItemCode | ItemName | BaseUnit  | SalesReturnQty(BaseUnit) | DifferenceReason |
      | 000051   | Ink5     | 0019 - M2 | 1100.0                   |                  |
    And the following quantity data for Item "000051" is displayed to "Ali.Hasan":
      | ReceivedQty(UoE) | UoE                 | ReceivedQty(Base) | BaseUnit  | StockType     |
      | 9.0              | 0029 - Roll 2.20x50 | 990.00            | 0019 - M2 | DAMAGED_STOCK |

  #EBS-6769
  Scenario: (05) View GR Received Items - of type GR-based on Sales Return, by an authorized user  - multiple Items with no quantities in SalesReturn (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view ReceivedItems section of GR with code "2020000025"
    Then the following values of ReceivedItems section are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                             | BaseUnit  | SalesReturnQty(BaseUnit) | DifferenceReason |
      | 000053   | Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 6286.5                   |                  |
      | 000051   | Ink5                                 | 0019 - M2 | 2.00                     |                  |

  ##### Exception Cases

  # EBS-1412
  Scenario: (06) View GR Received Items - where GR has been deletd by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "Mahmoud.Abdelaziz"
    And "Mahmoud.Abdelaziz" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Gehan.Ahmed" requests to view ReceivedItems section of GR with code "2018000008"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  ##### Abuse Cases

  # EBS-1412
  Scenario Outline: (07) View GR Received Items - by an unauthorized user (Unauthorized Access / Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view ReceivedItems section of GR with code "2018000008"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      |
      | Afaf      |
      | Ali.Hasan |

