@DELETE_GR
Feature: Delete GoodsReceipt
  Overview:
  - User should be authenticated and authorized
  - If an unauthorized user sends a delete request, he/sher shoud be logged out as this is considered as a security breach

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ali.Hasan         | Storekeeper_Corrugated |
      | Ahmed.Seif        | Quality_Specialist     |
      | hr1               | SuperUser              |
    And the following roles and sub-roles exist:
      | Role                   | Subrole            |
      | Storekeeper_Signmedia  | GROwner_Signmedia  |
      | Storekeeper_Flexo      | GROwner_Flexo      |
      | Storekeeper_Corrugated | GROwner_Corrugated |
      | SuperUser              | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission          | Condition                       |
      | GROwner_Signmedia  | GoodsReceipt:Delete | [purchaseUnitName='Signmedia']  |
      | GROwner_Flexo      | GoodsReceipt:Delete | [purchaseUnitName='Flexo']      |
      | GROwner_Corrugated | GoodsReceipt:Delete | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole   | *:*                 |                                 |

    And the following GoodsReceipts exist:
      | Code       | State  | Type                                                   | BusinessUnit      |
      | 2018000008 | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia  |
      | 2018000002 | Active | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia  |
      | 2018000003 | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0001 - Flexo      |
      | 2018000011 | Draft  | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0006 - Corrugated |

    # EBS-1329
  Scenario Outline: (01) Delete GoodsReceipt in a draft state by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to delete GR with code "<GRCode>"
    Then GR with code "GRCode>" is deleted from the system
    And a success notification is sent to "<User>" with the following message "Gen-msg-12"
    Examples:
      | GRCode     | User              |
      | 2018000008 | Mahmoud.Abdelaziz |
      | 2018000003 | Ali.Hasan         |
      | 2018000011 | Ali.Hasan         |

    # EBS-1329   # ToBeUpdatedAfterActivateFeature
  Scenario: (02) Delete GoodsReceipt in a state that doesn't allow delete: Active (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete GR with code "2018000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-13"

  Scenario: (03) Delete GoodsReceipt in a draft state where GR doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to delete GR with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (04) Delete GoodsReceipt in a draft state that is locked by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And first "hr1" opens "ItemsData" section of GR with code "2018000008" in edit mode
    When "Mahmoud.Abdelaziz" requests to delete GR with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-14"

  Scenario: (05) Delete GoodsReceipt in a draft state by an unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to delete GR with code "2018000008"
    Then "Ahmed.Seif" is logged out
    And "Ahmed.Seif" is forwarded to the error page

  Scenario: (06) Delete GoodsReceipt in a draft state by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete GR with code "2018000003"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page