# Author: Niveen Magdy (EBS-6785)
# Reviewer: Somaya Ahmed (14-Jul-2020)

Feature: Allowed Action For Goods Receipt (Object Screen)

  This feature file covers the following GoodsReceipt Types:
  1-SALES_RETURN
  2-PURCHASE_ORDER

  Background:
    Given the following users and roles exist:
      | Name               | Role                            |
      | Ali.Hasan          | Storekeeper_Flexo               |
      | Gehan.Ahmed        | PurchasingResponsible_Signmedia |
      | Shady.Abdelatif    | Accountant_Signmedia            |
      | CannotReadSections | CannotReadSectionsRole          |
      | Afaf               | FrontDesk                       |
      | hr1                | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                            |
      | Storekeeper_Flexo               | GROwner_Flexo                      |
      | Storekeeper_Flexo               | GRActivateOwner_Flexo              |
      | Storekeeper_Flexo               | GRViewer_Flexo                     |
      | PurchasingResponsible_Signmedia | GRDifferenceReasonEditor_Signmedia |
      | PurchasingResponsible_Signmedia | GRViewer_Signmedia                 |
      | Accountant_Signmedia            | GRViewer_Signmedia                 |
      | CannotReadSectionsRole          | CannotReadSectionsSubRole          |
      | SuperUser                       | SuperUserSubRole                   |
    And the following sub-roles and permissions exist:
      | Subrole                            | Permission                                           | Condition                      |
      | GROwner_Flexo                      | GoodsReceipt:UpdateHeader                            | [purchaseUnitName='Flexo']     |
      | GROwner_Flexo                      | GoodsReceipt:UpdateItem                              | [purchaseUnitName='Flexo']     |
      | GROwner_Flexo                      | GoodsReceipt:Delete                                  | [purchaseUnitName='Flexo']     |
      | GRActivateOwner_Flexo              | GoodsReceipt:Activate                                | [purchaseUnitName='Flexo']     |
      | GROwner_Flexo                      | GoodsReceipt:ExportPDF                               | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadAll                                 | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadHeader                              | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadCompany                             | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadPOData                              | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadSalesReturnData                     | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadItems                               | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadAccountingDetails                   | [purchaseUnitName='Flexo']     |
      | GRViewer_Flexo                     | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Flexo']     |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadAll                                 | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadHeader                              | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadCompany                             | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadPOData                              | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadSalesReturnData                     | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadItems                               | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadAccountingDetails                   | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia                 | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |
      | GRDifferenceReasonEditor_Signmedia | GoodsReceipt:EditDifferentReason                     | [purchaseUnitName='Signmedia'] |
      | CannotReadSectionsSubRole          | GoodsReceipt:Delete                                  |                                |
      | CannotReadSectionsSubRole          | GoodsReceipt:Activate                                |                                |
      | CannotReadSectionsSubRole          | GoodsReceipt:ExportPDF                               |                                |
      | CannotReadSectionsSubRole          | GoodsReceipt:ReadHeader                              |                                |
      | CannotReadSectionsSubRole          | GoodsReceipt:ReadAll                                 |                                |
      | SuperUserSubRole                   | *:*                                                  |                                |

    And the following users doesn't have the following permissions:
      | User               | Permission                                           |
      | Afaf               | GoodsReceipt:ReadAll                                 |
      | Afaf               | GoodsReceipt:ReadHeader                              |
      | Afaf               | GoodsReceipt:ReadPOData                              |
      | Afaf               | GoodsReceipt:ReadSalesReturnData                     |
      | Afaf               | GoodsReceipt:ReadItems                               |
      | Afaf               | GoodsReceipt:ReadCompany                             |
      | Afaf               | GoodsReceipt:ReadAccountingDetails                   |
      | Afaf               | GoodsReceipt:ReadAccountingDocumentActivationDetails |
      | Afaf               | GoodsReceipt:UpdateHeader                            |
      | Afaf               | GoodsReceipt:UpdateItem                              |
      | Afaf               | GoodsReceipt:Delete                                  |
      | Afaf               | GoodsReceipt:Activate                                |
      | Afaf               | GoodsReceipt:ExportPDF                               |
      | Afaf               | GoodsReceipt:EditDifferentReason                     |
      | Ali.Hasan          | GoodsReceipt:EditDifferentReason                     |
      | Gehan.Ahmed        | GoodsReceipt:UpdateHeader                            |
      | Gehan.Ahmed        | GoodsReceipt:UpdateItem                              |
      | Gehan.Ahmed        | GoodsReceipt:Delete                                  |
      | Gehan.Ahmed        | GoodsReceipt:Activate                                |
      | Gehan.Ahmed        | GoodsReceipt:ExportPDF                               |
      | Shady.Abdelatif    | GoodsReceipt:UpdateHeader                            |
      | Shady.Abdelatif    | GoodsReceipt:UpdateItem                              |
      | Shady.Abdelatif    | GoodsReceipt:Delete                                  |
      | Shady.Abdelatif    | GoodsReceipt:Activate                                |
      | Shady.Abdelatif    | GoodsReceipt:ExportPDF                               |
      | Shady.Abdelatif    | GoodsReceipt:EditDifferentReason                     |
      | CannotReadSections | GoodsReceipt:ReadPOData                              |
      | CannotReadSections | GoodsReceipt:ReadSalesReturnData                     |
      | CannotReadSections | GoodsReceipt:ReadItems                               |
      | CannotReadSections | GoodsReceipt:ReadCompany                             |
      | CannotReadSections | GoodsReceipt:UpdateHeader                            |
      | CannotReadSections | GoodsReceipt:UpdateItem                              |
      | CannotReadSections | GoodsReceipt:ReadAccountingDetails                   |
      | CannotReadSections | GoodsReceipt:ReadAccountingDocumentActivationDetails |
      | CannotReadSections | GoodsReceipt:EditDifferentReason                     |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                                           | Condition                      |
      | Ali.Hasan       | GoodsReceipt:ReadAll                                 | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadHeader                              | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadPOData                              | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadSalesReturnData                     | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadItems                               | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadCompany                             | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadAccountingDetails                   | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:UpdateHeader                            | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:UpdateItem                              | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:Delete                                  | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:Activate                                | [purchaseUnitName='Signmedia'] |
      | Ali.Hasan       | GoodsReceipt:ExportPDF                               | [purchaseUnitName='Signmedia'] |
      | Gehan.Ahmed     | GoodsReceipt:ReadAll                                 | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadHeader                              | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadPOData                              | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadSalesReturnData                     | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadItems                               | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadCompany                             | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadAccountingDetails                   | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed     | GoodsReceipt:EditDifferentReason                     | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadAll                                 | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadHeader                              | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadPOData                              | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadSalesReturnData                     | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadItems                               | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | GoodsReceipt:ReadCompany                             | [purchaseUnitName='Flexo']     |

    And the following GoodsReceipts exist:
      | Code       | State  | BusinessUnit     | Type                                                   |
      | 2020000021 | Active | 0001 - Flexo     | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2020000020 | Draft  | 0001 - Flexo     | SALES_RETURN - Goods Receipt Based on Sales Return     |
      | 2018000004 | Active | 0001 - Flexo     | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | Draft  | 0001 - Flexo     | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000002 | Active | 0002 - Signmedia | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000022 | Active | 0002 - Signmedia | SALES_RETURN - Goods Receipt Based on Sales Return     |
      
     
  # EBS-6785
  Scenario Outline: (01) Read allowed actions in Goods Receipt Object Screen - All States and All Types  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of GR with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | AllowedActions                                                                                                                                                               | NoOfActions |
      | Ali.Hasan          | 2018000003 | UpdateHeader,UpdateItem,Delete,Activate,ExportPDF,ReadHeader,ReadAll,ReadCompany,ReadPOData,ReadItems,ReadAccountingDetails,ReadAccountingDocumentActivationDetails          | 12          |
      | Ali.Hasan          | 2018000004 | ExportPDF,ReadHeader,ReadCompany,ReadPOData,ReadAll,ReadItems ,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                                                 | 8           |
      | Ali.Hasan          | 2020000020 | UpdateHeader,UpdateItem,Delete,Activate,ExportPDF,ReadHeader,ReadAll,ReadCompany,ReadSalesReturnData,ReadItems,ReadAccountingDetails,ReadAccountingDocumentActivationDetails | 12          |
      | Ali.Hasan          | 2020000021 | ExportPDF,ReadHeader,ReadAll,ReadCompany,ReadSalesReturnData,ReadItems,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                                         | 8           |
      | Gehan.Ahmed        | 2018000002 | ReadHeader,ReadCompany,ReadPOData, ReadAll,ReadItems,EditDifferentReason ,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                                      | 8           |
      | Gehan.Ahmed        | 2020000022 | ReadHeader,ReadCompany,ReadSalesReturnData, ReadAll,ReadItems,EditDifferentReason,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                              | 8           |
      | Shady.Abdelatif    | 2018000002 | ReadAll,ReadHeader,ReadCompany,ReadPOData,ReadItems,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                                                            | 7           |
      | Shady.Abdelatif    | 2020000022 | ReadAll,ReadHeader,ReadCompany,ReadSalesReturnData,ReadItems,ReadAccountingDetails,ReadAccountingDocumentActivationDetails                                                   | 7           |
      | CannotReadSections | 2018000003 | Delete, Activate, ExportPDF, ReadHeader, ReadAll                                                                                                                             | 5           |
      | CannotReadSections | 2018000004 | ExportPDF, ReadHeader, ReadAll                                                                                                                                               | 3           |
      | CannotReadSections | 2020000020 | Delete, Activate, ExportPDF, ReadHeader, ReadAll                                                                                                                             | 5           |
      | CannotReadSections | 2020000021 | ExportPDF, ReadHeader, ReadAll                                                                                                                                               | 3           |
      | CannotReadSections | 2018000002 | ExportPDF, ReadHeader, ReadAll                                                                                                                                               | 3           |
      | CannotReadSections | 2020000022 | ExportPDF, ReadHeader, ReadAll                                                                                                                                               | 3           |

   # EBS-6785
  Scenario Outline: (02) Read allowed actions in Goods Receipt Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of GR with code "<GICode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | GICode     |
      | Ali.Hasan       | 2018000002 |
      | Ali.Hasan       | 2020000022 |
      | Gehan.Ahmed     | 2018000003 |
      | Gehan.Ahmed     | 2018000004 |
      | Gehan.Ahmed     | 2020000020 |
      | Gehan.Ahmed     | 2020000021 |
      | Shady.Abdelatif | 2018000003 |
      | Shady.Abdelatif | 2018000004 |
      | Shady.Abdelatif | 2020000020 |
      | Shady.Abdelatif | 2020000021 |
      | Afaf            | 2018000002 |
      | Afaf            | 2018000003 |
      | Afaf            | 2018000004 |
      | Afaf            | 2020000020 |
      | Afaf            | 2020000021 |
      | Afaf            | 2020000022 |

  # EBS-6785
  Scenario: (03) Read allowed actions in Goods Receipt Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ali.Hasan"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000003" successfully
    When "Ali.Hasan" requests to read actions of GR with code "2018000003"
    Then an error notification is sent to "Ali.Hasan" with the following message "Gen-msg-01"