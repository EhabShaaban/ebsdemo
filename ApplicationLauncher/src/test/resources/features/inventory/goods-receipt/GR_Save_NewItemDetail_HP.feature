Feature: Add Item Detail in GR ReceivedItems

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |
    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
      | Storekeeper_Flexo     | GROwner_Flexo     |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo     | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | MeasuresViewer    | Measures:ReadAll        |                                |
      | StockTypesViewer  | StockType:ReadAll       |                                |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | 0001 - Flexo     | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |
    And GoodsReceipts "2018000008" has the following ReceivedItems:
      | ItemCode |
      | 000001   |
      | 000003   |
    And GoodsReceipts "2018000003" has the following ReceivedItems:
      | ItemCode |
      | 000005   |
    And GoodsReceipts "2020000020" has the following ReceivedItems:
      | ItemCode |
      | 000051   |

  #EBS-4762 #EBS-6770
  Scenario Outline: (01) Add GR Item Detail HP - to GoodsReceipt (in Draft state by an authorized user - with one or more roles) while total received quantities are less than or equal to the quantity from the Referenced Document  (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" requests to add Item Detail for Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode
    When "<User>" saves Item Detail of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "<AddingDate>" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then Item Detail of type "<DetailType>" is added to Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" of type "<GR_Type>" with the following values:
      | CreatedBy | CreationDate | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <User>    | <AddingDate> | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate |
      | <User>        | <AddingDate>   |
    And GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate |
      | <User>        | <AddingDate>   |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  | AddingDate           | GR_Type        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   |         |             |             | 30.123456        | 0032 | UNRESTRICTED_USE | Package Damaged                        | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000003 | 000005   | Ali.Hasan         | Quantity   |         |             |             | 300.123456       | 0034 | DAMAGED_STOCK    | Samples taken by clearance authorities | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000003 | 000005   | Ali.Hasan         | Quantity   |         |             |             | 300.123456       | 0019 | DAMAGED_STOCK    | Samples taken by clearance authorities | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 3.123456         | 0029 | DAMAGED_STOCK    | Samples taken by clearance authorities | 30-Mar-2020 09:30 AM | SALES_RETURN   |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   |         |             |             | 4.123456         | 0029 | DAMAGED_STOCK    | Quantity Returned                      | 30-Mar-2020 09:30 AM | SALES_RETURN   |