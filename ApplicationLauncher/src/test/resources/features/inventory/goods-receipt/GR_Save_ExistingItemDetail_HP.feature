# Author: Ahmed Hamed, Hossam Hassan & Engy Mohamed (EBS-6771)
# Reviewer: Somaya Ahmed (EBS-6771)

Feature: Save Item Detail in GR ReceivedItems

  Background:

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |

    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
      | Storekeeper_Signmedia | MeasuresViewer    |
      | Storekeeper_Signmedia | StockTypesViewer  |
      | Storekeeper_Flexo     | GROwner_Flexo     |
      | Storekeeper_Flexo     | MeasuresViewer    |
      | Storekeeper_Flexo     | StockTypesViewer  |

    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo     | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | MeasuresViewer    | Measures:ReadAll        |                                |
      | StockTypesViewer  | StockType:ReadAll       |                                |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2018000003 | 0001 - Flexo     | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |

    And the following GoodsReceipts exist with the following Items and Quantities:
      | Code       | Item                                                           | QtyID |
      | 2018000008 | 000001 - Hot Laminated Frontlit Fabric roll                    | 1     |
      | 2018000003 | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 3     |
      | 2020000020 | 000051 - Ink5                                                  | 137   |

    And the following GoodsReceipts exist with the following Items and Batches:
      | Code       | ItemCode | BatchID |
      | 2018000008 | 000003   | 1       |

  ######## Happy Paths
  #EBS-4035 #EBS-6771 #EBS-8708
  Scenario Outline: (01) Save GR Item Detail HP - to GoodsReceipt in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "<RequestEditingDate>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "<EditingDate>" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then Item Detail with id "<QtyID>" of type "<DetailType>" in Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" of type "<GR_Type>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <User>        | <EditingDate>  | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate |
      | <User>        | <EditingDate>  |
    And GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate |
      | <User>        | <EditingDate>  |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  | RequestEditingDate   | EditingDate          | GR_Type        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456        | 0032 | UNRESTRICTED_USE | Package Damaged                        | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456        | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000003 | 000005   | Ali.Hasan         | Quantity   | 3        |         |             |             | 300.123456       | 0034 | DAMAGED_STOCK    | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2018000003 | 000005   | Ali.Hasan         | Quantity   | 3        |         |             |             | 300.123456       | 0019 | DAMAGED_STOCK    | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:30 AM | PURCHASE_ORDER |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456         | 0029 | UNRESTRICTED_USE | Sample Notes                           | 30-Mar-2020 09:10 AM | 30-Mar-2020 09:30 AM | SALES_RETURN   |
