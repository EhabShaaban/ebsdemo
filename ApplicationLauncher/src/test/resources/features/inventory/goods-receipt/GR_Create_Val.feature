# Author: Nancy Shoukry (EBS-1602)
# Author: Niveen Magdy (EBS-6766)

Feature: Create GoodsReceipt

  Rule:
  - User should enter all mandatory fields for creation

    Background:
      Given the following users and roles exist:
        | Name              | Role                  |
        | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
        | Ali.Hasan         | Storekeeper_Flexo     |
      And the following roles and sub-roles exist:
        | Role                  | Subrole                   |
        | Storekeeper_Signmedia | GROwner_Signmedia         |
        | Storekeeper_Signmedia | POViewerLimited_Signmedia |
        | Storekeeper_Signmedia | StorehouseViewer          |
        | Storekeeper_Signmedia | StorekeeperViewer         |
        | Storekeeper_Signmedia | CompanyViewer             |
        | Storekeeper_Flexo     | GROwner_Flexo             |
        | Storekeeper_Flexo     | SROViewer_Flexo           |
        | Storekeeper_Flexo     | StorehouseViewer          |
        | Storekeeper_Flexo     | StorekeeperViewer         |
        | Storekeeper_Flexo     | CompanyViewer             |

      And the following sub-roles and permissions exist:
        | Subrole                   | Permission               | Condition                      |
        | GROwner_Signmedia         | GoodsReceipt:Create      |                                |
        | GROwner_Flexo             | GoodsReceipt:Create      |                                |
        | POViewerLimited_Signmedia | PurchaseOrder:ReadAll    | [purchaseUnitName='Signmedia'] |
        | StorekeeperViewer         | Storekeeper:ReadAll      |                                |
        | StorehouseViewer          | Storehouse:ReadAll       |                                |
        | CompanyViewer             | Company:ReadAll          |                                |
        | SROViewer_Signmedia       | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |

      And the following Import PurchaseOrders exist:
        | Code       | State              | PurchasingUnit |
        | 2018000001 | Cleared            | 0002           |
        | 2018000002 | DeliveryComplete   | 0002           |
        | 2018000050 | Cleared            | 0002           |
        | 2018000021 | Draft              | 0002           |
        | 2018000006 | OpenForUpdates     | 0002           |
        | 2018000007 | WaitingApproval    | 0002           |
        | 2018000005 | Approved           | 0002           |
        | 2018000008 | Confirmed          | 0002           |
        | 2018000009 | FinishedProduction | 0002           |
        | 2018000010 | Shipped            | 0002           |
        | 2018000011 | Arrived            | 0002           |
        | 2018000012 | Cancelled          | 0002           |

      And the following Local PurchaseOrders exist:
        | Code       | State            | PurchasingUnit |
        | 2018000019 | DeliveryComplete | 0002           |
        | 2018000020 | Cancelled        | 0002           |
        | 2018000013 | Draft            | 0002           |
        | 2018000014 | OpenForUpdates   | 0002           |
        | 2018000015 | WaitingApproval  | 0002           |
        | 2018000016 | Approved         | 0002           |
        | 2018000017 | Confirmed        | 0002           |

      And the following PurchaseOrders exist with the following values needed for GR creation:
        | Code       | Type      | State   | Plant | PurchasingResponsible | Vendor | PurchasingUnit |
        | 2018000001 | IMPORT_PO | Cleared | 0001  | Gehan Ahmed           | 000002 | 0002           |
        | 2018000050 | IMPORT_PO | Cleared |       | Gehan Ahmed           | 000002 | 0002           |

      And ImportPurchaseOrder with code "2018000001" has the following items:
        | Code   |
        | 000001 |
        | 000002 |
        | 000003 |

      And the following items exist:
        | Code   | Name                                | Base      | IsBatchManaged |
        | 000001 | Hot Laminated Frontlit Fabric roll  | 0019 - M2 | false          |
        | 000002 | Hot Laminated Frontlit Backlit roll | 0019 - M2 | false          |
        | 000003 | Flex Primer Varnish E33             | 0003 - Kg | true           |

      And the following Storehouses exist:
        | Code | Name                | Plant |
        | 0001 | AlMadina Main Store | 0001  |
        | 0003 | DigiPro Main Store  | 0002  |

      And the following Storekeepers exist:
        | Code | Name              |
        | 0007 | Mahmoud.Abdelaziz |

      And the following SalesReturnOrders exist with the following values needed for GR creation:
        | Code       | BusinessUnit     | SalesRepresentative                       | Customer                       | Company          | State                 |
        | 2020000002 | 0001 - Flexo     | Manar.Mohammed - Manar Mohammed           | 000006 - المطبعة الأمنية       | 0002 - DigiPro   | Shipped               |
        | 2020000056 | 0002 - Signmedia | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية | 0001 - AL Madina | GoodsReceiptActivated |
        | 2020000005 | 0002 - Signmedia | Ahmed.Al-Ashry - Ahmed Al-Ashry           | 000007 - مطبعة أكتوبر الهندسية | 0001 - AL Madina | Completed             |
        | 2020000055 | 0002 - Signmedia | SeragEldin.Meghawry - SeragEldin Meghawry | 000001 - Al Ahram              | 0002 - DigiPro   | Draft                 |

  ######## Create with incorrect input (Validation Failure)

  # EBS-1602
    Scenario Outline: (01) Create GoodsReceipt Val -with incorrect data: IPO with state after cleared or LPO with state after shipped (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder   | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | <PurchaseOrder> | 0001       | 0007        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to PurchaseOrder field "Gen-msg-53" and sent to "Mahmoud.Abdelaziz"
      Examples:
        | PurchaseOrder |
        | 2018000002    |
        | 2018000019    |
        | 2018000020    |

  # EBS-6766
    Scenario Outline: (02) Create GoodsReceipt Val -with incorrect data: SR with state GoodsReceiptActivated/Completed (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type         | SalesReturn   | Storehouse   | Storekeeper | Company   |
        | SALES_RETURN | <SalesReturn> | <Storehouse> | 0007        | <Company> |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to SalesReturn field "Gen-msg-53" and sent to "Mahmoud.Abdelaziz"
      Examples:
        | SalesReturn | Storehouse | Company |
        | 2020000056  | 0001       | 0001    |
        | 2020000005  | 0001       | 0001    |
        | 2020000055  | 0002       | 0002    |

  # EBS-1602
    Scenario: (03) Create GoodsReceipt Val -with incorrect data: Storekeeper that has been deleted (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | 2018000001    | 0001       | 9999        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to Storekeeper field "GR-msg-07" and sent to "Mahmoud.Abdelaziz"

    # EBS-6766
    Scenario: (04) Create GoodsReceipt Val -with incorrect data: Storekeeper that has been deleted (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type         | SalesReturn | Storehouse | Storekeeper | Company |
        | SALES_RETURN | 2020000056  | 0001       | 9999        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to Storekeeper field "GR-msg-07" and sent to "Mahmoud.Abdelaziz"

  # EBS-1602
    Scenario: (05) Create GoodsReceipt Val -with incorrect data: PurchaseOrder Code that does not has a plant (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | 2018000050    | 0001       | 0007        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to PurchaseOrder field "GR-msg-13" and sent to "Mahmoud.Abdelaziz"

    Scenario: (06) Create GoodsReceipt Val -with incorrect data: storehouse not related to plant of selected PO (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | 2018000001    | 0003       | 0007        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to Storehouse field "GR-msg-06" and sent to "Mahmoud.Abdelaziz"

    Scenario: (07) Create GoodsReceipt Val -with incorrect data: storehouse not related to company of selected SRO (Validation Failure)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type         | SalesReturn | Storehouse | Storekeeper | Company |
        | SALES_RETURN | 2020000056  | 0003       | 0007        | 0001    |
      Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
      And the following error message is attached to Storehouse field "GR-msg-06" and sent to "Mahmoud.Abdelaziz"

  ######## Abuse cases

  # EBS-1602
    Scenario Outline: (08) Create GoodsReceipt Val -with malicious input: Type/ Purchase Order/ Storehouse/ Storekeeper value is garbage (Abuse Case)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type   | PurchaseOrder   | Storehouse   | Storekeeper   | Company   |
        | <Type> | <PurchaseOrder> | <Storehouse> | <Storekeeper> | <Company> |
      Then "Mahmoud.Abdelaziz" is logged out
      And "Mahmoud.Abdelaziz" is forwarded to the error page
      Examples:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        | Ay7aga         | 2018000001    | 0001       | 0007        | 0001    |
        | PURCHASE_ORDER | Ay7aga        | 0001       | 0007        | 0001    |
        | PURCHASE_ORDER | 2018000001    | Ay7aga     | 0007        | 0001    |
        | PURCHASE_ORDER | 2018000001    | 0001       | Ay7aga      | 0001    |
        | PURCHASE_ORDER | 2018000001    | 0001       | 0007        | Ay7aga  |
        | ""             | 2018000001    | 0001       | 0007        | 0001    |
        | PURCHASE_ORDER | ""            | 0001       | 0007        | 0001    |
        | PURCHASE_ORDER | 2018000001    | ""         | 0007        | 0001    |
        | PURCHASE_ORDER | 2018000001    | 0001       | ""          | 0001    |
        | PURCHASE_ORDER | 2018000001    | 0001       | 0007        | ""      |

      #EBS-6766
    Scenario Outline: (09) Create GoodsReceipt Val -with malicious input: Type/ Sales Return/ Storehouse/ Storekeeper value is garbage (Abuse Case)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type   | SalesReturn   | Storehouse   | Storekeeper   | Company   |
        | <Type> | <SalesReturn> | <Storehouse> | <Storekeeper> | <Company> |
      Then "Mahmoud.Abdelaziz" is logged out
      And "Mahmoud.Abdelaziz" is forwarded to the error page
      Examples:
        | Type         | SalesReturn | Storehouse | Storekeeper | Company |
        | Ay7aga       | 2020000056  | 0001       | 0007        | 0001    |
        | SALES_RETURN | Ay7aga      | 0001       | 0007        | 0001    |
        | SALES_RETURN | 2020000056  | Ay7aga     | 0007        | 0001    |
        | SALES_RETURN | 2020000056  | 0001       | Ay7aga      | 0001    |
        | SALES_RETURN | 2020000056  | 0001       | 0007        | Ay7aga  |
        | ""           | 2020000056  | 0001       | 0007        | 0001    |
        | SALES_RETURN | ""          | 0001       | 0007        | 0001    |
        | SALES_RETURN | 2020000056  | ""         | 0007        | 0001    |
        | SALES_RETURN | 2020000056  | 0001       | ""          | 0001    |
        | SALES_RETURN | 2020000056  | 0001       | 0007        | ""      |

  # EBS-1602
    Scenario Outline: (10) Create GoodsReceipt Val -with incorrect data: IPO in any state except cleared/DeliveryComplete or LPO inany state except cleared/DeliveryComplete/Cancelled  (Abuse Case)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type           | PurchaseOrder   | Storehouse | Storekeeper | Company |
        | PURCHASE_ORDER | <PurchaseOrder> | 0001       | 0007        | 0001    |
      Then "Mahmoud.Abdelaziz" is logged out
      And "Mahmoud.Abdelaziz" is forwarded to the error page
      Examples:
        | PurchaseOrder |
        | 2018000021    |
        | 2018000006    |
        | 2018000007    |
        | 2018000005    |
        | 2018000008    |
        | 2018000009    |
        | 2018000010    |
        | 2018000011    |
        | 2018000012    |
        | 2018000013    |
        | 2018000014    |
        | 2018000015    |
        | 2018000016    |
        | 2018000017    |

  # EBS-1602
    Scenario Outline: (11) Create GoodsReceipt Val -with missing mandatory fields Type/Purchase Order (Abuse Case)
      Given user is logged in as "Mahmoud.Abdelaziz"
      When "Mahmoud.Abdelaziz" creates GoodsReceipt with following values:
        | Type   | PurchaseOrder   | Storehouse   | Storekeeper   | Company   |
        | <Type> | <PurchaseOrder> | <Storehouse> | <Storekeeper> | <Company> |
      Then "Mahmoud.Abdelaziz" is logged out
      And "Mahmoud.Abdelaziz" is forwarded to the error page
      Examples:
        | Type           | PurchaseOrder | Storehouse | Storekeeper | Company |
        |                | 2018000001    | 0001       | 0007        | 0001    |
        | PURCHASE_ORDER |               | 0001       | 0007        | 0001    |

    # EBS-6766
    Scenario Outline: (12) Create GoodsReceipt Val -with missing mandatory fields  (Abuse Case)
      Given user is logged in as "Ali.Hasan"
      When "Ali.Hasan" creates GoodsReceipt with following values:
        | Type   | SalesReturn   | Storehouse   | Storekeeper   | Company   |
        | <Type> | <SalesReturn> | <Storehouse> | <Storekeeper> | <Company> |
      Then "Ali.Hasan" is logged out
      And "Ali.Hasan" is forwarded to the error page
      Examples:
        | Type         | SalesReturn | Storehouse | Storekeeper | Company |
        |              | 2020000002  | 0001       | 0007        | 0001    |
        | SALES_RETURN |             | 0001       | 0007        | 0001    |
        | SALES_RETURN | 2020000002  |            | 0007        | 0001    |
        | SALES_RETURN | 2020000002  | 0001       |             | 0001    |
