# Author: Ahmed Hamed, Hossam Hassan & Engy Mohamed (EBS-6771)
# Reviewer: Somaya Ahmed (EBS-6771)

Feature: Save Item Detail in GR ReceivedItems

  Background:

    Given the following users and roles exist:
      | Name                           | Role                                       |
      | Shady.Abdelatif                | Accountant_Signmedia                       |
      | Mahmoud.Abdelaziz              | Storekeeper_Signmedia                      |
      | Ali.Hasan                      | Storekeeper_Flexo                          |
      | Mahmoud.Abdelaziz_NoMeasures   | Storekeeper_Signmedia_CannotReadMeasures   |
      | Mahmoud.Abdelaziz_NoStockTypes | Storekeeper_Signmedia_CannotReadStockTypes |
      | Ali.Hasan_NoMeasures           | Storekeeper_Flexo_CannotReadMeasures       |
      | Ali.Hasan_NoStockTypes         | Storekeeper_Flexo_CannotReadStockTypes     |

    And the following roles and sub-roles exist:
      | Role                                       | Subrole            |
      | Accountant_Signmedia                       | GRViewer_Signmedia |
      | Storekeeper_Signmedia                      | GROwner_Signmedia  |
      | Storekeeper_Signmedia                      | MeasuresViewer     |
      | Storekeeper_Signmedia                      | StockTypesViewer   |
      | Storekeeper_Flexo                          | GROwner_Flexo      |
      | Storekeeper_Flexo                          | MeasuresViewer     |
      | Storekeeper_Flexo                          | StockTypesViewer   |
      | Storekeeper_Signmedia_CannotReadMeasures   | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadMeasures   | StockTypesViewer   |
      | Storekeeper_Signmedia_CannotReadStockTypes | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadStockTypes | MeasuresViewer     |
      | Storekeeper_Flexo_CannotReadMeasures       | GROwner_Flexo      |
      | Storekeeper_Flexo_CannotReadMeasures       | StockTypesViewer   |
      | Storekeeper_Flexo_CannotReadStockTypes     | GROwner_Flexo      |
      | Storekeeper_Flexo_CannotReadStockTypes     | MeasuresViewer     |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission              | Condition                      |
      | GRViewer_Signmedia   | GoodsReceipt:ReadItems  | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo        | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | GROwner_Signmedia    | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia | Item:ReadAll            | [purchaseUnitName='Signmedia'] |
      | MeasuresViewer       | Measures:ReadAll        |                                |
      | StockTypesViewer     | StockType:ReadAll       |                                |

    And the following users doesn't have the following permissions:
      | User                           | Permission              |
      | Shady.Abdelatif                | GoodsReceipt:UpdateItem |
      | Mahmoud.Abdelaziz_NoMeasures   | Measures:ReadAll        |
      | Ali.Hasan_NoMeasures           | Measures:ReadAll        |
      | Mahmoud.Abdelaziz_NoStockTypes | StockType:ReadAll       |
      | Ali.Hasan_NoStockTypes         | StockType:ReadAll       |

    And the following users have the following permissions without the following conditions:
      | User              | Permission              | Condition                      |
      | Mahmoud.Abdelaziz | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | Ali.Hasan         | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |

    And the following GoodsReceipts exist with the following Items and Quantities:
      | Code       | Item                                        | QtyID |
      | 2018000008 | 000001 - Hot Laminated Frontlit Fabric roll | 1     |
      | 2020000020 | 000051 - Ink5                               | 137   |

    And the following GoodsReceipts exist with the following Items and Batches:
      | Code       | ItemCode | BatchID |
      | 2018000008 | 000003   | 1       |

  ######## Authorization
  # EBS-4037 #EBS-6771
  Scenario Outline: (01) Save GR Item Detail Auth - to GoodsReceipt in Draft state by unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "30-Mar-2020 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User            | DetailType | DetailID | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      | 2018000008 | 000001   | Shady.Abdelatif | Quantity   | 1        |         |          |            | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | 2020000020 | 000051   | Shady.Abdelatif | Quantity   | 137      |         |          |            | 3.0              | 0029 | UNRESTRICTED_USE | Sample Notes    |

  # Save section by a user who is not authorized to save due to condition
  #EBS-6771
  Scenario Outline: (02) Save GR Item Detail Auth - to GoodsReceipt in Draft state by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "30-Mar-2020 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      | 2018000008 | 000001   | Ali.Hasan         | Quantity   | 1        |         |          |            | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | 2020000020 | 000051   | Mahmoud.Abdelaziz | Quantity   | 137      |         |          |            | 3.0              | 0029 | UNRESTRICTED_USE | Sample Notes    |

  # Save section using values which the user is not authorized to read
  #EBS-6771
  Scenario Outline: (03) Save GR Item Detail Auth - User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given user is logged in as "<User>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "30-Mar-2020 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User                           | DetailType | DetailID | BatchNo | ProdDate | ExpireDate | ReceivedQty(UoE) | UoE  | StockType        | Notes           |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz_NoMeasures   | Quantity   | 1        |         |          |            | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz_NoStockTypes | Quantity   | 1        |         |          |            | 30.0             | 0032 | UNRESTRICTED_USE | Package Damaged |
      | 2020000020 | 000051   | Ali.Hasan_NoMeasures           | Quantity   | 137      |         |          |            | 3.0              | 0029 | UNRESTRICTED_USE | Sample Notes    |
      | 2020000020 | 000051   | Ali.Hasan_NoStockTypes         | Quantity   | 137      |         |          |            | 3.0              | 0029 | UNRESTRICTED_USE | Sample Notes    |
