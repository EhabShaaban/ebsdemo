@GR_Save_Item_Validation
Feature: Save Item to Received Items in GR (Validation)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State   | PurchasingUnit |
      | 2018000001 | Cleared | 0002           |
    And ImportPurchaseOrder with code "2018000001" has the following items:
      | Code   |
      | 000001 |
      | 000002 |
      | 000003 |
    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft |
    And GoodsReceipts has the following values:
      | Code       | POCode     | VendorCode |
      | 2018000008 | 2018000001 | 000002     |
    And the following items exist:
      | Code   | Name                        | Base | IsBatchManaged |
      | 000003 | Flex Primer Varnish E33     | 0003 | true           |
      | 000004 | Flex cold foil adhesive E01 | 0003 | true           |
    And the following ItemVendorRecords exist:
      | ItemCode | VendorCode | ItemVendorCode |
      | 000003   | 000002     | GDH670-777     |
      | 000004   | 000002     | GDH670-888     |
    And edit session is "30" minutes

  Scenario: (01) Save GR Item Val - after edit session expired for GR (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And ReceivedItems of GR with Code "2018000008" is locked by "Mahmoud.Abdelaziz" at "7-Jan-2019 9:10 am"
    And "Mahmoud.Abdelaziz" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    When "Mahmoud.Abdelaziz" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:41 am"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"


  Scenario: (02) Save GR Item Val - after edit session expired & GR is deleted (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And ReceivedItems of GR with Code "2018000008" is locked by "Mahmoud.Abdelaziz" at "7-Jan-2019 9:10 am"
    And "Mahmoud.Abdelaziz" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully at "07-Jan-2019 09:41 AM"
    When "Mahmoud.Abdelaziz" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:42 am"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (03) Save GR Item Val - item that has no ivr record (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And ReceivedItems of GR with Code "2018000008" is locked by "Mahmoud.Abdelaziz" at "7-Jan-2019 9:10 am"
    And "Mahmoud.Abdelaziz" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000007   |
    And Item with code "000007" has no IVR record
    When "Mahmoud.Abdelaziz" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:31 am"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
    And the following error message is attached to Item field "GR-msg-10" and sent to "Mahmoud.Abdelaziz"

  Scenario Outline: (04) Save GR Item Val - non-existent Item in GR (Abuse Case)
    Given  user is logged in as "Mahmoud.Abdelaziz"
    And ReceivedItems of GR with Code "2018000008" is locked by "Mahmoud.Abdelaziz" at "7-Jan-2019 9:10 am"
    And "Mahmoud.Abdelaziz" entered the following data in GR with Code "2018000008":
      | ItemCode   |
      | <ItemCode> |
    When "Mahmoud.Abdelaziz" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:31 am"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
    Examples:
      | ItemCode |
      | ay7aga   |
      |          |
      | 000003   |
