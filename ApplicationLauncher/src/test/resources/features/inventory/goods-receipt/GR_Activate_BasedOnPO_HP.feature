Feature: Activate Goods Receipt Happy path

  Background:
  Goods Receipt (Import Purchase Order) Activate for Non-Batched Items should do the following:
  1- Change state of Goods receipt
  2- Create new Costing Document in active state
  2.1- Set General and Company Data
  2.2- Calculate and Save the Costing Exchange Rate
  2.2- Calculate and Save the Item Costs
  2.2- Set Accounting Details (Closing Journal)
  2.3- Set Activate Detaild
  3- Create Store Transactions
  5- Create Journal Entry for Closing Balances
  4- Update Stock Availability (increase balances)
  2- Change state of PO State State

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                   |
      | Storekeeper_Signmedia | GRActivateOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission            | Condition                      |
      | GRActivateOwner_Signmedia | GoodsReceipt:Activate | [purchaseUnitName='Signmedia'] |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 000037 | USD              | EGP               | 16.32       | 19-Mar-2021 10:00 PM | User Daily Rate |

    And the following Companies have the following local Currencies:
      | Code | Name      | CurrencyISO |
      | 0001 | AL Madina | EGP         |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                      | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State | BusinessUnit     |
      | 2021000001 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Draft | 0002 - Signmedia |
      | 2021000002 | GR_PO | Admin     | 11-Mar-2021 9:02 AM | Draft | 0002 - Signmedia |
      | 2021000003 | GR_PO | Admin     | 11-Mar-2021 9:02 AM | Draft | 0002 - Signmedia |

    #@INSERT
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000001 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000002 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000003 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |

    #@INSERT
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000001 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000002 | 2021000004    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000003 | 2021000005    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    And insert the following GoodsReceipt Items:
      | Code       | Item                                         | DifferenceReason | Type |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |
      | 2021000003 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |

    And insert the following GoodsReceipt ItemQuantities:
      | Code       | Item                                         | UoE       | ReceivedQty(UoE) | StockType        | Notes |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 50               | UNRESTRICTED_USE |       |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 50               | UNRESTRICTED_USE |       |
      | 2021000003 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000003 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 50               | UNRESTRICTED_USE |       |

    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy | CreationDate        | DocumentOwner |
      | 2021000001 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |
      | 2021000002 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |
      | 2021000003 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |

    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000001 | 2021000003    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |
      | 2021000002 | 2021000004    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |
      | 2021000003 | 2021000005    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |

    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 100.00 M2      | 100.00 M2 | 10.00           | 10.00                |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 50.00 M2       | 50.00 M2  | 30.00           | 30.00                |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 100.00 M2      | 100.00 M2 | 10.00           | 10.00                |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 50.00 M2       | 50.00 M2  | 30.00           | 30.00                |
      | 2021000003 | 000001 - Hot Laminated Frontlit Fabric roll  | 100.00 M2      | 100.00 M2 | 10.00           | 10.00                |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll | 50.00 M2       | 50.00 M2  | 30.00           | 30.00                |

    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2021000001 | 2000.00     | 0.0            | 2500        |
      | 2021000002 | 500.00      | 1500.00        | 2500        |
      | 2021000003 | 0.0         | 3000.00        | 3000        |

    #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice       | JournalEntryCode |
      | 2021000001 | Shady.Abdelatif | 22-Jun-2021 09:02 AM | 1.0 USD = 17.44 EGP | 2020000023       |
      | 2021000002 | Shady.Abdelatif | 22-Jun-2021 09:02 AM | 1.0 USD = 17.44 EGP | 2020000024       |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy   | CreationDate         | Remaining | DocumentOwner |
      | 2021000001 | VENDOR                   | PaymentDone | Amr.Khalil  | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000002 | VENDOR                   | PaymentDone | Gehan.Ahmed | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000003 | VENDOR                   | PaymentDone | Amr.Khalil  | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000004 | VENDOR                   | PaymentDone | Gehan.Ahmed | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000005 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Gehan.Ahmed | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000005 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury | ExpectedDueDate      | BankTransRef | Description                 |
      | 2021000001 | BANK        | 000002 - Zhejiang | INVOICE         | 2021000001  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Sep-2018 09:02 AM |              | downpayment for po          |
      | 2021000002 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2021000003  | 2000.00   | USD         |                | Alex Bank - 1516171819789 |          | 05-Oct-2018 09:02 AM | 195326512    | Installment No 2 in invoice |
      | 2021000003 | BANK        | 000002 - Zhejiang | INVOICE         | 2021000002  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Sep-2018 09:02 AM |              | downpayment for po          |
      | 2021000004 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2021000004  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Oct-2018 09:02 AM | 195326512    | Installment No 2 in invoice |
      | 2021000005 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2021000003  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Oct-2018 09:02 AM | 195326512    | other party for purchase    |

    #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant | PostingDate          | CurrencyPrice    | JournalEntryCode |
      | 2021000001 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 16 USD | 2021000003       |
      | 2021000002 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 17 USD | 2021000004       |
      | 2021000003 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 16 USD | 2021000005       |
      | 2021000004 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 17 USD | 2021000006       |
      | 2021000005 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 17 USD | 2021000007       |

    #@INSERT
    And the following JournalEntries Details exist:
      | JournalEntryCode | JournalDate          | Subledger     | GLAccount                 | GLSubAccount                    | Credit  | Debit   | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |

      | 2021000005       | 01-Nov-2021 10:47 AM | BANK          | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 500     | 0       | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2021000005       | 01-Nov-2021 10:47 AM | Local_Vendors | 10221 - service vendors 2 | 000002 - Zhejiang               | 0       | 500     | USD                | EGP               | 10                        | EGP             | 10                      |

      | 2021000006       | 01-Nov-2021 10:47 AM | PO            | 10207 - PO                | 2018000044                      | 0       | 52857.6 | USD                | EGP               | 16.5                      | EGP             | 16.5                    |
      | 2021000006       | 01-Nov-2021 10:47 AM | Local_Vendors | 10221 - service vendors 2 | 000002 - Zhejiang               | 52857.6 | 0       | USD                | EGP               | 16.5                      | EGP             | 16.5                    |


    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Amr.Khalil | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
      | 2021000002 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted    | Amr.Khalil | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
      | 2021000003 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Amr.Khalil | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode       |
      | 2021000001 | 2021000003 | 2021000001        | true        | IMPORT_GOODS_INVOICE |
      | 2021000002 | 2021000004 | 2021000002        | true        | IMPORT_GOODS_INVOICE |
      | 2021000003 | 2021000005 | 2021000003        | true        | IMPORT_GOODS_INVOICE |

    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                      | EstimatedValue | ActualValue | Difference  |
      | 2021000001 | 000056 - Shipment service | 1800.00 EGP    | 0.00 EGP    |             |
      | 2021000001 | 000057 - Bank Fees        | 20.00 EGP      | 0.00 EGP    |             |
      | 2021000002 | 000056 - Shipment service | 1800.00 EGP    | 1170.00 EGP | 630.00 EGP  |
      | 2021000002 | 000057 - Bank Fees        | 20.00 EGP      | 400.00 EGP  | -380.00 EGP |
      | 2021000003 | 000056 - Shipment service | 1800.00 EGP    | 1170.00 EGP | 630.00 EGP  |
      | 2021000003 | 000057 - Bank Fees        | 20.00 EGP      | 400.00 EGP  | -380.00 EGP |

    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2021000001 | 1820 EGP       | 0 EGP       | 1820 EGP   |
      | 2021000002 | 1820 EGP       | 1570 EGP    | 250 EGP    |
      | 2021000003 | 1820 EGP       | 1570 EGP    | 250 EGP    |

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                         | UOM       | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000020019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | UNRESTRICTED_USE | 10.0              |

    And the following AccountingDetails for Items exist:
      | ItemCode | AccountCode | AccountName | BusinessUnit |
      | 000001   | 10207       | PO          | Signmedia    |
      | 000002   | 10207       | PO          | Signmedia    |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

  Scenario: (01) Activate GR- LandedCost is Estimate - Vendor Invoice Fully Paid - No Damaged + Damaged - Item doesn't exist/exist in Stock avaiability +
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2021000001" at "07-Sep-2021 9:30 AM"
    Then GR with Code "2021000001" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate      | State  | ActivationDate      |
      | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Active | 07-Sep-2021 9:30 AM |
    And a new Costing Document is Created As Follows:
      | Code       | CreatedBy         | CreationDate        | State | DocumentOwner     |
      | 2021000001 | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Draft | Mahmoud Abdelaziz |
    And the CompanyData of Costing Document is Created As Follows:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      # exchange rate = [(firstPaymentRequest amount * first exchangerate ) + (secondpaymentRequest amount * second exchangerate )] / (total vendor invoice [after taxes?])
    And the Costing Details of Costing Document is Created As Follows:
      | Code       | GoodsReceiptCode | CostingExchangeRateEstimateTime | CostingExchangeRateActualTime |
      | 2021000001 | 2021000001       | 16.8 EGP                        |                               |
      # weight = item total / invoice total without taxes
      # UnitPriceInLocalCurrency = [(price(Order Unit) * quantity(Order Unit) / (unrestricted Quantity)] * costing exchange rate
      # UnitLandedCost = ( total landed cost (Actual if exist otherwise estimate)* item weight ) / (item unrestricted quantities)
    And the Costing Items of Costing Document is Created As Follows:
      | Code       | Item                                         | UOM       | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPriceInLocalCurrency(Estimate) | UnitPriceInLocalCurrency(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 40               | 90               | UNRESTRICTED_USE | 186.66666666648                    |                                  | 8.0888888888             |                        |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 0                | 10               | DAMAGED_STOCK    | 0                                  |                                  | 0                        |                        |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 60               | 50               | UNRESTRICTED_USE | 504                                |                                  | 21.84                    |                        |
      # Estimate Cost values : unitPrice + UnitLandedCost from CostingItems
    And new StoreTransactions created as follows:
      | CreationDate        | OriginalAddingDate  | StockType        | BusinessUnit     | Item                                         | UOM                 | BatchNo | Qty  | EstimateCost    | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - Square Meter |         | 90.0 | 194.75555555528 | ADD             |            | 2021000001        | 90.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - Square Meter |         | 10.0 | 0.0             | ADD             |            | 2021000001        | 10.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - Square Meter |         | 50.0 | 525.84          | ADD             |            | 2021000001        | 50.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2021000003"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"

  Scenario: (02) Activate GR- LandedCost is Actual - Vendor Invoice Partially Paid - Multiple Payments - No Damaged + Damaged - Item doesn't exist/exist in Stock avaiability +
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2021000002" at "07-Sep-2021 9:30 AM"
    Then GR with Code "2021000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate      | State  | ActivationDate      |
      | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Active | 07-Sep-2021 9:30 AM |
    And a new Costing Document is Created As Follows:
      | Code       | CreatedBy         | CreationDate        | State | DocumentOwner     |
      | 2021000001 | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Draft | Mahmoud Abdelaziz |
    And the CompanyData of Costing Document is Created As Follows:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      # exchange rate  = [(firstPaymentRequest amount * first exchangeRate ) + (secondPaymentRequest amount * second exchangeRate ) + (vendor invoice remaining * currentExchangeRate)] / (total vendor invoice [after taxes?])
    And the Costing Details of Costing Document is Created As Follows:
      | Code       | GoodsReceiptCode | CostingExchangeRateEstimateTime | CostingExchangeRateActualTime |
      | 2021000001 | 2021000002       |                                 | 16.392 EGP                    |
      # weight = item total / invoice total without taxes
      # UnitPriceInLocalCurrency = [(price(Order Unit) * quantity(Order Unit) / (unrestricted Quantity)] * costing exchange rate
      # UnitLandedCost = ( total landed cost (Actual if exist otherwise estimate)* item weight ) / (item unrestricted quantities)
    And the Costing Items of Costing Document is Created As Follows:
      | Code       | Item                                         | UOM       | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPriceInLocalCurrency(Estimate) | UnitPriceInLocalCurrency(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 40               | 90               | UNRESTRICTED_USE |                                    | 182.1333333331512                |                          | 6.9777777777           |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 0                | 10               | DAMAGED_STOCK    |                                    | 0                                |                          | 0                      |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 60               | 50               | UNRESTRICTED_USE |                                    | 491.760                          |                          | 18.84                  |
    And new StoreTransactions created as follows:
      | CreationDate        | OriginalAddingDate  | StockType        | BusinessUnit     | Item                                         | UOM                 | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost        | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - Square Meter |         | 90.0 |              | ADD             | 189.1111111108512 | 2021000002        | 90.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - Square Meter |         | 10.0 |              | ADD             | 0.0               | 2021000002        | 10.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - Square Meter |         | 50.0 |              | ADD             | 510.60            | 2021000002        | 50.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2021000003"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"

  @Future
  Scenario: (03) Activate GR- LandedCost is Estimate - Vendor Invoice Not Paid - No Damaged + Damaged - Item doesn't exist/exist in Stock availability +
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2021000003" at "07-Sep-2021 9:30 AM"
    Then GR with Code "2021000003" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate      | State  | ActivationDate      |
      | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Active | 07-Sep-2021 9:30 AM |
    And a new Costing Document is Created As Follows:
      | Code       | CreatedBy         | CreationDate        | State |
      | 2021000001 | Mahmoud.Abdelaziz | 07-Sep-2021 9:30 AM | Draft |
    And the CompanyData of Costing Document is Created As Follows:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    And the Costing Details of Costing Document is Created As Follows:
      # exchange rate  = current exchange rate (GR activation day exchange rate)
      | Code       | GoodsReceiptCode | CostingExchangeRateEstimateTime | CostingExchangeRateActualTime |
      | 2021000001 | 2021000003       | 16.32 EGP                       |                               |
    And new StoreTransactions created as follows:
      | CreationDate        | OriginalAddingDate  | StockType        | BusinessUnit     | Item                                         | UOM       | BatchNo | Qty  | EstimateCost      | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 |         | 90.0 | 190.2222222219512 | ADD             |            | 2021000001        | 90.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 |         | 10.0 | 0.0               | ADD             |            | 2021000001        | 10.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Sep-2021 9:30 AM | 07-Sep-2021 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 |         | 50.0 | 513.6             | ADD             |            | 2021000001        | 50.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2021000003"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"
