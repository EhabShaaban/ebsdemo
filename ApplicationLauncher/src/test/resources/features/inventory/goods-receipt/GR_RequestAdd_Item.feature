Feature: Add Item to Received Items in GR

  Background:
    Given the following users and roles exist:
      | Name                                  | Role                                      |
      | Mahmoud.Abdelaziz                     | Storekeeper_Signmedia                     |
      | Mahmoud.Abdelaziz.NoItemsNoStockTypes | Storekeeper_Signmedia_CannotViewItemsRole |
      | Shady.Abdelatif                       | Accountant_Signmedia                      |
      | hr1                                   | SuperUser                                 |
    And the following roles and sub-roles exist:
      | Role                                      | Subrole            |
      | Storekeeper_Signmedia                     | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotViewItemsRole | GROwner_Signmedia  |
      | Accountant_Signmedia                      | GRViewer_Signmedia |
      | SuperUser                                 | *                  |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                      |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GRViewer_Signmedia | GoodsReceipt:ReadItems  | [purchaseUnitName='Signmedia'] |

    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State  |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
      | 2018000002 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Active |
      | 2018000003 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0001 - Flexo     | Draft  |

  ################################################ Request to add item scenarios ################################################

  Scenario: (01) Request to Add/Cancel Item to a GR - (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to add item to GoodsReceipt with code "2018000008"
    Then a new add Item side nav is opened and Items section of GoodsReceipt with code "2018000008" is locked by "Mahmoud.Abdelaziz"
    And the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads |
      | ReadItems       |
      | ReadStockTypes  |

  Scenario: (02) Request to Add/Cancel Item to a GR - when received items list is locked by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first requests to add item to GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to add item to GoodsReceipt with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  Scenario: (03) Request to Add/Cancel Item to a GR - that doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully at "12-Jun-2019 09:00 AM"
    When "Mahmoud.Abdelaziz" requests to add item to GoodsReceipt with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (04) Request to Add/Cancel Item to a GR - when Add Item action is not allowed in current state (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to add item to GoodsReceipt with code "2018000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-03"

  Scenario Outline: (05) Request to Add Item to a GR - by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to add item to GoodsReceipt with code "<Code>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | Code       |
      | Shady.Abdelatif   | 2018000008 |
      | Mahmoud.Abdelaziz | 2018000003 |

  Scenario: (06) Request to Add/Cancel Item to a GR - (No authorized reads)
    Given user is logged in as "Mahmoud.Abdelaziz.NoItemsNoStockTypes"
    When "Mahmoud.Abdelaziz.NoItemsNoStockTypes" requests to add item to GoodsReceipt with code "2018000008"
    Then a new add Item side nav is opened and Items section of GoodsReceipt with code "2018000008" is locked by "Mahmoud.Abdelaziz.NoItemsNoStockTypes"
    And there are no authorized reads returned to "Mahmoud.Abdelaziz.NoItemsNoStockTypes"

  ##################################### Cancel Save ###############################################

  Scenario: (07) Request to Add/Cancel Item to a GR - within the edit session Item (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And  Items section of GoodsReceipt with code "2018000008" is locked by "Mahmoud.Abdelaziz" at "12-Jun-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving Items section of GoodsReceipt with code "2018000008" at "12-Jun-2019 09:30 AM"
    Then the lock by "Mahmoud.Abdelaziz" on Items section of GoodsReceipt with code "2018000008" is released

  Scenario: (08) Request to Add/Cancel Item to a GR - after edit session expire (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And  Items section of GoodsReceipt with code "2018000008" is locked by "Mahmoud.Abdelaziz" at "12-Jun-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving Items section of GoodsReceipt with code "2018000008" at "12-Jun-2019 09:45 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"
    And the lock by "Mahmoud.Abdelaziz" on Items section of GoodsReceipt with code "2018000008" is released

  Scenario:(09) Request to Add/Cancel Item to a GR - when GoodsReciept doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And  Items section of GoodsReceipt with code "2018000008" is locked by "Mahmoud.Abdelaziz" at "12-Jun-2019 09:10 AM"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully at "12-Jun-2019 09:45 AM"
    When "Mahmoud.Abdelaziz" cancels saving Items section of GoodsReceipt with code "2018000008" at "12-Jun-2019 09:50 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario Outline: (10) Request to Add/Cancel Item to a GR - by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving Items section of GoodsReceipt with code "<Code>" at "12-Jun-2019 09:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              | Code       |
      | Shady.Abdelatif   | 2018000008 |
      | Mahmoud.Abdelaziz | 2018000003 |