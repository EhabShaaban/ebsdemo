@Authorization
Feature: Save_Authorization Item to Received Items in GR

  Background:
    Given the following users and roles exist:
      | Name                      | Role                                  |
      | Shady.Abdelatif           | Accountant_Signmedia                  |
      | Ali.Hasan                 | Storekeeper_Flexo                     |
      | Mahmoud.Abdelaziz.NoItems | Storekeeper_Signmedia_CannotReadItems |

    And the following roles and sub-roles exist:
      | Role                                  | Subrole            |
      | Accountant_Signmedia                  | GRViewer_Signmedia |
      | Storekeeper_Flexo                     | GROwner_Flexo      |
      | Storekeeper_Signmedia_CannotReadItems | GROwner_Signmedia  |
      | Storekeeper_Signmedia_CannotReadItems | MeasuresViewer     |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                      |
      | GRViewer_Signmedia | GoodsReceipt:ReadItems  | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo      | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | GROwner_Signmedia  | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
    And ImportPurchaseOrder with code "2018000001" has the following items:
      | Code   |
      | 000001 |
      | 000002 |
      | 000003 |
    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft |
    And GoodsReceipts has the following values:
      | Code       | POCode     | VendorCode |
      | 2018000008 | 2018000001 | 000002     |
    And the following items exist:
      | Code   | Name                        | Base | IsBatchManaged |
      | 000003 | Flex Primer Varnish E33     | 0003 | true           |
      | 000004 | Flex cold foil adhesive E01 | 0003 | true           |
    And the following ItemVendorRecords exist:
      | ItemCode | VendorCode | ItemVendorCode |
      | 000003   | 000002     | GDH670-777     |
      | 000004   | 000002     | GDH670-888     |


  # Save section by a user who is not authorized to save due to condition
  Scenario: (01)  Save GR Item Auth - by an unauthorized user due to condition (Abuse Case)
    Given  user is logged in as "Ali.Hasan"
    And "Ali.Hasan" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    When "Ali.Hasan" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:30 am"
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page

  # Save section using values which the user is not authorized to read
  Scenario: (02) Save GR Item Auth - User is not authorized to read the possible values of the data entry fields (Abuse case)
    Given  user is logged in as "Mahmoud.Abdelaziz.NoItems"
    And "Mahmoud.Abdelaziz.NoItems" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    When "Mahmoud.Abdelaziz.NoItems" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:30 am"
    Then "Mahmoud.Abdelaziz.NoItems" is logged out
    And "Mahmoud.Abdelaziz.NoItems" is forwarded to the error page

  # Save section by a user who is not authorized to save
  Scenario: (03) Save GR Item Auth - by an unauthorized users (Abuse Case)
    Given  user is logged in as "Shady.Abdelatif"
    And "Shady.Abdelatif" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    When "Shady.Abdelatif" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:30 am"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page