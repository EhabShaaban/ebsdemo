#Author: Niveen Magdy (EBS-7361)
#Reviewer: Ahmad Hamed (15-12-2020)

Feature: Activate Goods Receipt (Auth)

  Background:
    Given the following users and roles exist:
      | Name                          | Role                              |
      | Ali.Hasan                     | Storekeeper_Flexo                 |
      | Gehan.Ahmed                   | PurchasingResponsible_Signmedia   |
      | Mahmoud.Abdelaziz.DigiProMain | Storekeeper_Signmedia_DigiProMain |

    And the following roles and sub-roles exist:
      | Role                              | Subrole                               |
      | Storekeeper_Flexo                 | GRActivateOwner_Flexo                 |
      | Storekeeper_Signmedia_DigiProMain | GRActivateOwner_Signmedia_DigiProMain |
      | PurchasingResponsible_Signmedia   | GRViewer_Signmedia                    |

    And the following sub-roles and permissions exist:
      | Subrole                               | Permission            | Condition                                                   |
      | GRActivateOwner_Signmedia_DigiProMain | GoodsReceipt:Activate | ([purchaseUnitName='Signmedia'] && [storehouseCode='0003']) |
      | GRActivateOwner_Flexo                 | GoodsReceipt:Activate | [purchaseUnitName='Flexo']                                  |
      | GRViewer_Signmedia                    | GoodsReceipt:ReadAll  | [purchaseUnitName='Signmedia']                              |

    And the following users doesn't have the following permissions:
      | User        | Permission            |
      | Gehan.Ahmed | GoodsReceipt:Activate |

    And the following users have the following permissions without the following conditions:
      | User                          | Permission            | Condition                                                   |
      | Ali.Hasan                     | GoodsReceipt:Activate | [purchaseUnitName='Signmedia']                              |
      | Mahmoud.Abdelaziz.DigiProMain | GoodsReceipt:Activate | ([purchaseUnitName='Signmedia'] && [storehouseCode='0001']) |

    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate          | Type         | Storekeeper      | State |
      | 2020000024 | hr1       | 29-July-2020 09:49 AM | SALES_RETURN | 0006 - Ahmed.Ali | Draft |

    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                 | BusinessUnit     |
      | 2020000024 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

  # EBS-7361 #EBS-8712
  Scenario Outline: (01) Activate GR - Auth - by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to activate GR with code "2020000024" at "07-Aug-2020 11:00 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                          |
      | Ali.Hasan                     |
      | Gehan.Ahmed                   |
      | Mahmoud.Abdelaziz.DigiProMain |