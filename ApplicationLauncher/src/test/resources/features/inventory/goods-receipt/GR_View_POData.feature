# Author: Aya Sadek (22-Jan-2019 03:40 PM)
# Reviewer: Hend Aboulwafa (03-Feb-2019 10:40 am)

Feature:View POGoodsReciept-header section

  Background:
    Given the following users and roles exist:
      | Name              | Role                   |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia  |
      | Ali.Hasan         | Storekeeper_Corrugated |
      | Ali.Hasan         | Storekeeper_Flexo      |
      | Ahmed.Ali         | Storekeeper_Offset     |
      | Ahmed.Seif        | Quality_Specialist     |
      | Afaf              | FrontDesk              |
    And the following roles and sub-roles exist:
      | Role                   | Subrole            |
      | Storekeeper_Signmedia  | GROwner_Signmedia  |
      | Storekeeper_Corrugated | GROwner_Corrugated |
      | Storekeeper_Flexo      | GROwner_Flexo      |
      | Storekeeper_Offset     | GROwner_Offset     |
      | Quality_Specialist     | GRViewer           |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition                       |
      | GROwner_Signmedia  | GoodsReceipt:ReadPOData | [purchaseUnitName='Signmedia']  |
      | GROwner_Corrugated | GoodsReceipt:ReadPOData | [purchaseUnitName='Corrugated'] |
      | GROwner_Flexo      | GoodsReceipt:ReadPOData | [purchaseUnitName='Flexo']      |
      | GROwner_Signmedia  | GoodsReceipt:Delete     | [purchaseUnitName='Signmedia']  |
      | GROwner_Offset     | GoodsReceipt:ReadPOData | [purchaseUnitName='Offset']     |
      | GRViewer           | GoodsReceipt:ReadPOData |                                 |
    And the following GRs exist with the following GR-POData:
      | Code       | State  | Type           | POCode     | Vendor | PurchasingResponsible | DeliveryNote/PLNumber | Notes |
      | 2018000008 | Draft  | PURCHASE_ORDER | 2018000001 | 000002 | Gehan Ahmed           | 123456                |       |
      | 2018000003 | Draft  | PURCHASE_ORDER | 2018000003 | 000001 | Amr Khalil            |                       |       |
      | 2018000011 | Draft  | PURCHASE_ORDER | 2018000044 | 000002 | Gehan Ahmed           |                       |       |
      | 2018000017 | Active | PURCHASE_ORDER | 2018000045 | 000002 | Amr Khalil            |                       |       |

    And the following Vendors exist:
      | Code   | Name     |
      | 000001 | Siegwerk |
      | 000002 | Zhejiang |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |
    And the following PurchasingResponsibles exist:
      | Code | Name        |
      | 0001 | Amr.Khalil  |
      | 0002 | Gehan.Ahmed |
        #@INSERT
    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2018000001 | Admin     | 10-Mar-2018 9:02 AM | Active |
    #@INSERT
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2018000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    #@INSERT
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2018000001 | 2018000017   | 11.44 EGP                 |                         |


  #### Happy Path

  # EBS-1327
  Scenario Outline: (01) View GR POData section - by an authorized user who has two roles (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view POData section of GR with code "<GRCodeValue>"
    Then the following values of POData section are displayed to "<User>":
      | POCode        | Vendor        | PurchasingResponsible        | DeliveryNote/PLNumber        | Notes        | CostingDocumentCode        |
      | <POCodeValue> | <VendorValue> | <PurchasingResponsibleValue> | <DeliveryNote/PLNumberValue> | <NotesValue> | <CostingDocumentCodeValue> |
    Examples:
      | User              | GRCodeValue | POCodeValue | VendorValue | PurchasingResponsibleValue | DeliveryNote/PLNumberValue | NotesValue | CostingDocumentCodeValue |
      | Ali.Hasan         | 2018000003  | 2018000003  | Siegwerk    | Amr Khalil                 |                            |            |                          |
      | Ali.Hasan         | 2018000003  | 2018000003  | Siegwerk    | Amr Khalil                 |                            |            |                          |
      | Ali.Hasan         | 2018000011  | 2018000044  | Zhejiang    | Gehan Ahmed                |                            |            |                          |
      | Mahmoud.Abdelaziz | 2018000008  | 2018000001  | Zhejiang    | Gehan Ahmed                | 123456                     |            |                          |
      | Ali.Hasan         | 2018000017  | 2018000045  | Zhejiang    | Amr Khalil                 |                            |            | 2018000001               |

  #### Exceptions

  # EBS-1327
  Scenario: (02) View GR POData section - by an authorized user, where GR has been deleted by another user (Exception Case)
    Given user is logged in as "Ahmed.Seif"
    And another user is logged in as "Mahmoud.Abdelaziz"
    And "Mahmoud.Abdelaziz" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Ahmed.Seif" requests to view POData section of GR with code "2018000008"
    Then an error notification is sent to "Ahmed.Seif" with the following message "Gen-msg-01"

  #### Abuse Cases

  # EBS-1327
  Scenario Outline: (03) View GR POData section - by an unauthorized user (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to view POData section of GR with code "2018000008"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User      |
      | Afaf      |
      | Ahmed.Ali |
