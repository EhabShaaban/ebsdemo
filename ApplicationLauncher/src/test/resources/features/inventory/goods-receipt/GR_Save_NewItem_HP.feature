@GR_Save_Item_Happy_Path
Feature: Save Item to Received Items in GR (Happy Paths)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
    And the following Import PurchaseOrders exist:
      | Code       | State   | PurchasingUnit |
      | 2018000001 | Cleared | 0002           |
    And ImportPurchaseOrder with code "2018000001" has the following items:
      | Code   |
      | 000001 |
      | 000002 |
      | 000003 |
    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft |
    And GoodsReceipts has the following values:
      | Code       | POCode     | VendorCode |
      | 2018000008 | 2018000001 | 000002     |
    And the following items exist:
      | Code   | Name                        | Base | IsBatchManaged |
      | 000003 | Flex Primer Varnish E33     | 0003 | true           |
      | 000004 | Flex cold foil adhesive E01 | 0003 | true           |
    And the following ItemVendorRecords exist:
      | ItemCode | VendorCode | ItemVendorCode |
      | 000003   | 000002     | GDH670-777     |
      | 000004   | 000002     | GDH670-888     |

    And edit session is "30" minutes

  Scenario: (01) Save GR Item HP - and item is in PO (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And ReceivedItems of GR with Code "2018000008" is locked by "Mahmoud.Abdelaziz" at "7-Jan-2019 9:10 am"
    And "Mahmoud.Abdelaziz" entered the following data in GR with Code "2018000008":
      | ItemCode |
      | 000002   |
    When "Mahmoud.Abdelaziz" saves ordinary item to GR with code "2018000008" at "7-Jan-2019 9:30 am"
    Then GR with code "2018000008" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       |
      | Mahmoud.Abdelaziz | 07-Jan-2019 09:30 am |
    And a new item is added to ReceivedItems of GR with code "2018000008" as follows:
      | ItemCode |
      | 000002   |
    And the lock by "Mahmoud.Abdelaziz" on ReceivedItems section in GoodsReceipt with Code "2018000008" is released
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-04"