#Auther Dev: Hossam Hassan
#Auther QA:
#Reviewer:
Feature: GR-StoreTransaction Recosting (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                 | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:ConvertToActual | [purchaseUnitName='Signmedia'] |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 000036 | USD              | EGP               | 17.0        | 18-Mar-2021 10:00 PM | User Daily Rate |
      | 000037 | USD              | EGP               | 17.9        | 19-Mar-2021 10:00 PM | User Daily Rate |
    And the following Companies have the following local Currencies:
      | Code | Name      | CurrencyISO |
      | 0001 | AL Madina | EGP         |
    #@INSERT
    And the following GeneralData for GoodsIssues SalesOrder exist:
      | GICode     | Type                                                    | CreatedBy                 | State  | CreationDate         | BusinessUnit     |
      | 2021000001 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | Admin from BDKCompanyCode | Active | 01-Jan-2019 09:02 AM | 0002 - Signmedia |
    #@INSERT
    And the following CompanyData for GoodsIssues SalesOrder exist:
      | GICode     | Company          | Storehouse                 | BusinessUnit     | Storekeeper              | Plant                    |
      | 2021000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia | 0007 - Mahmoud.Abdelaziz | 0001 - Madina Tech Plant |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                   | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Active,Cleared,Received | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000001 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
      | 2021000002 | GR_SR | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
    #@INSERT
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000001 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000002 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    #@INSERT
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000001 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
    And insert the following GoodsReceipt Items:
      | Code       | Item                                        | DifferenceReason | Type |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll |                  |      |
    And insert the following GoodsReceipt ItemQuantities:
      | Code       | Item                                        | UoE       | ReceivedQty(UoE) | StockType        | Notes |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy | CreationDate        | DocumentOwner |
      | 2021000001 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |
    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000001 | 2021000003    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |
    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 100.00 M2      | 100.00 M2 | 10.00           | 10.00                |
    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2021000001 | 0.0         | 2500           | 2500        |
    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Amr.Khalil | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode       |
      | 2021000001 | 2021000003 | 2021000001        | true        | IMPORT_GOODS_INVOICE |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                      | EstimatedValue | ActualValue | Difference  |
      | 2021000001 | 000056 - Shipment service | 1800.00 EGP    | 1170.00 EGP | 630.00 EGP  |
      | 2021000001 | 000057 - Bank Fees        | 20.00 EGP      | 400.00 EGP  | -380.00 EGP |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2021000001 | 1820 EGP       | 1570 EGP    | 250 EGP    |
    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate         | State  |
      | 2021000001 | Admin     | 22-Dec-2020 09:30 AM | Active |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000001   | 17.0 EGP                  |                         |
    And insert the following Costing Items:
      | Code       | Item                                        | UOM       | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) | UnitPrice(Actual) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 100              | 90               | UNRESTRICTED_USE | 188.8888888888      | 20.2222222222            |                        |                   |                        |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 0                | 10               | DAMAGED_STOCK    | 0                   | 0                        |                        |                   |                        |
  #Use ORM createStoreTransaction
    And insert the following StoreTransactions:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                        | UOM       | BatchNo | Qty | EstimateCost  | TransactionType | ActualCost | ReferenceDocument | ReferenceDocumentType | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 10  | 0             | ADD             |            | 2021000001        | GoodsReceipt          | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000035           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 90  | 209.111111111 | ADD             |            | 2021000001        | GoodsReceipt          | 85           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000036           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 5   |               | TAKE            |            | 2021000001        | GoodsIssue            |              | 2020000034     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000037           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 5   | 209.111111111 | ADD             |            | 2021000002        | GoodsReceipt          | 5            | 2020000036     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |

  #EBS-7267
  Scenario: (01) GR Recosting - Re-calculate Costing details/Costing items/StoreTransactions after converting landed cost to actual - by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToActual LandedCost with code "2021000001" at "19-Mar-2021 10:00 PM":
    Then the Costing Details of Costing Document is Updated As Follows:
      | Code       | GoodsReceiptCode | CostingExchangeRateEstimateTime | CostingExchangeRateActualTime |
      | 2021000001 | 2021000001       | 17.0 EGP                        | 17.9 EGP                      |
    And the following values of Items section for Costing with code "2021000001" are updated as follows:
      | Code       | Item                                        | UOM       | WeightPercentage | ReceivedQty(UoE) | UnitPrice(Actual) | StockType        | UnitPrice(Estimate) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) | ItemTotalCost(Estimate) | ItemTotalCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 100              | 90               | 198.88888888869   | UNRESTRICTED_USE | 188.8888888888      | 20.2222222222            | 1744.4444444444        | 18819.99999999          | 174899.999999988      |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 | 0                | 10               | 0                 | DAMAGED_STOCK    | 0                   | 0                        | 0                      | 0                       | 0                     |
    Then ActualCost for StoreTransactions with Goods Receipt RefDocumentCode "2021000001" are Updated as Follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                        | UOM       | BatchNo | Qty | EstimateCost  | TransactionType | ActualCost       | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 10  | 0             | ADD             | 0                | 2021000001        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000035           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 90  | 209.111111111 | ADD             | 1943.33333333309 | 2021000001        | 85           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    Then ActualCost for StoreTransactions with Goods Receipt RefDocumentCode "2021000002" are Updated as Follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                        | UOM       | BatchNo | Qty | EstimateCost  | TransactionType | ActualCost       | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000037           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 5   | 209.111111111 | ADD             | 1943.33333333309 | 2021000002        | 5            | 2020000036     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    Then ActualCost for StoreTransactions with Goods Issue RefDocumentCode "2021000001" is not changed as Follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                        | UOM       | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000036           | 19-Mar-2021 10:00 PM | 19-Mar-2021 10:00 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0019 - M2 |         | 5   |              | TAKE            |            | 2021000001        |              | 2020000034     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And a success notification is sent to "Shady.Abdelatif" with the following message "LC-msg-54"
