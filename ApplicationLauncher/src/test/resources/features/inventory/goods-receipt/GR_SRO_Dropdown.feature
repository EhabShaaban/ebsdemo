# Author: Niveen Magdy, Engy Abd El Aziz & Ahmad Hamed
# Reviewers: Somaya Ahmed & Hosam Bayomy

Feature: View All (Shipped | CreditNoteActivated) SalesReturnOrder

  Background:

    Given the following users and roles exist:
      | Name      | Role              |
      | Ali.Hasan | Storekeeper_Flexo |
      | hr1       | SuperUser         |
      | Afaf      | FrontDesk         |

    And the following roles and sub-roles exist:
      | Role              | Subrole          |
      | Storekeeper_Flexo | SROViewer_Flexo  |
      | SuperUser         | SuperUserSubRole |

    And the following sub-roles and permissions exist:
      | Subrole          | Permission               | Condition                  |
      | SROViewer_Flexo  | SalesReturnOrder:ReadAll | [purchaseUnitName='Flexo'] |
      | SuperUserSubRole | *:*                      |                            |

    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | SalesReturnOrder:ReadAll |

    And the following are ALL existing SalesReturnOrders:
      | Code       | BusinessUnit     | SalesRepresentative | Customer                       | State                 |
      | 2020000060 | 0002 - Signmedia | Ahmed Al-Ashry      | 000007 - مطبعة أكتوبر الهندسية | Shipped               |
      | 2020000059 | 0002 - Signmedia | Ahmed Al-Ashry      | 000001 - Al Ahram              | Shipped               |
      | 2020000058 | 0002 - Signmedia | Manar Mohammed      | 000007 - مطبعة أكتوبر الهندسية | Shipped               |
      | 2020000057 | 0002 - Signmedia | SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية | Shipped               |
      | 2020000056 | 0002 - Signmedia | SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية | GoodsReceiptActivated |
      | 2020000055 | 0002 - Signmedia | SeragEldin Meghawry | 000001 - Al Ahram              | Draft                 |
      | 2020000054 | 0001 - Flexo     | Manar Mohammed      | 000001 - Al Ahram              | Draft                 |
      | 2020000053 | 0002 - Signmedia | SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية | Shipped               |
      | 2020000052 | 0002 - Signmedia | Ahmed Al-Ashry      | 000001 - Al Ahram              | Draft                 |
      | 2020000051 | 0002 - Signmedia | SeragEldin Meghawry | 000001 - Al Ahram              | Draft                 |
      | 2020000005 | 0002 - Signmedia | Ahmed Al-Ashry      | 000007 - مطبعة أكتوبر الهندسية | Completed             |
      | 2020000004 | 0002 - Signmedia | Manar Mohammed      | 000007 - مطبعة أكتوبر الهندسية | CreditNoteActivated   |
      | 2020000003 | 0001 - Flexo     | Ahmed Al-Ashry      | 000006 - المطبعة الأمنية       | GoodsReceiptActivated |
      | 2020000002 | 0001 - Flexo     | Manar Mohammed      | 000006 - المطبعة الأمنية       | Shipped               |
      | 2020000001 | 0002 - Signmedia | SeragEldin Meghawry | 000001 - Al Ahram              | Draft                 |
    And the total number of existing records of SalesReturnOrders is 15

  #EBS-7199
  Scenario: (01) Read list of SalesReturnOrders dropdown for GR filtered by (Shipped | CreditNoteActivated) states by an authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read drop-down of SalesReturnOrder for State (Shipped | CreditNoteActivated)
    Then the following SalesReturnOrders will be presented to "hr1":
      | Code       |
      | 2020000060 |
      | 2020000059 |
      | 2020000058 |
      | 2020000057 |
      | 2020000053 |
      | 2020000004 |
      | 2020000002 |
    And the total number of records in search results by "hr1" are 7

  #EBS-6765
  Scenario: (02) Read list of SalesReturnOrders dropdown for GR filtered by (Shipped | CreditNoteActivated) states by an authorized user (Happy Path)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to read drop-down of SalesReturnOrder for State (Shipped | CreditNoteActivated)
    Then the following SalesReturnOrders will be presented to "Ali.Hasan":
      | Code       |
      | 2020000002 |
    And the total number of records in search results by "Ali.Hasan" are 1

  #EBS-6765
  Scenario: (03) Read list of SalesReturnOrders dropdown for GR filtered by (Shipped | CreditNoteActivated) states by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read drop-down of SalesReturnOrder for State (Shipped | CreditNoteActivated)
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page