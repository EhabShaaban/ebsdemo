Feature: Activate Goods Receipt Happy path

  Background:
    Given the following users and roles exist:
      | Name                         | Role                             |
      | Mahmoud.Abdelaziz.MadinaMain | Storekeeper_Signmedia_MadinaMain |
      | Mahmoud.Abdelaziz            | Storekeeper_Signmedia            |
      | Ali.Hasan                    | Storekeeper_Flexo                |
      | Ali.Hasan                    | Storekeeper_Corrugated           |
      | Ahmed.Seif                   | Quality_Specialist               |
      | hr1                          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                              |
      | Storekeeper_Signmedia_MadinaMain | GRActivateOwner_Signmedia_MadinaMain |
      | Storekeeper_Signmedia            | GRActivateOwner_Signmedia            |
      | Storekeeper_Flexo                | GRActivateOwner_Flexo                |
      | Storekeeper_Corrugated           | GRActivateOwner_Corrugated           |
      | SuperUser                        | SuperUserSubRole                     |
    And the following sub-roles and permissions exist:
      | Subrole                              | Permission            | Condition                                                   |
      | GRActivateOwner_Signmedia_MadinaMain | GoodsReceipt:Activate | ([purchaseUnitName='Signmedia'] && [storehouseCode='0001']) |
      | GRActivateOwner_Signmedia            | GoodsReceipt:Activate | [purchaseUnitName='Signmedia']                              |
      | GRActivateOwner_Flexo                | GoodsReceipt:Activate | [purchaseUnitName='Flexo']                                  |
      | GRActivateOwner_Corrugated           | GoodsReceipt:Activate | [purchaseUnitName='Corrugated']                             |
      | SuperUserSubRole                     | *:*                   |                                                             |
    And the following Import PurchaseOrders exist with values:
      | Code       | State   | GoodsState  | BusinessUnit     |
      | 2018000001 | Cleared | NotReceived | 0002 - Signmedia |
      | 2018000003 | Cleared | NotReceived | 0001 - Flexo     |
    And the following GeneralData for LandedCosts exist:
      | LCCode     | Type                               | BusinessUnit     | CreatedBy   | State                    | CreationDate         | PurchaseOrder | DocumentOwner   |
      | 2020000004 | IMPORT - Landed Cost For Import PO | 0001 - Flexo     | Gehan.Ahmed | ActualValuesCompleted    | 24-Jun-2020 09:02 AM | 2018000003    | Shady.Abdelatif |
      | 2020100006 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia | hr1         | EstimatedValuesCompleted | 06-Dec-2020 02:10 PM | 2018000001    | Shady.Abdelatif |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020000004 | 000056 - Shipment service   | 1800 EGP       | 1800 EGP    | 0 EGP      |
      | 2020100006 | 000055 - Insurance service2 | 5000 EGP       | null EGP    | null EGP   |
      | 2020100006 | 000057 - Bank Fees          | 4000 EGP       | null EGP    | null EGP   |
    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                      | BusinessUnit     |
      | 2018000009 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0001 - Flexo     |
      | 2018000025 | 0002 - DigiPro   | 0003 - DigiPro Main Store       | 0002 - Signmedia |
      | 2019000001 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia |
      | 2019000002 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia |
    And the following POData exists for GoodsReceipts:
      | GRCode     | Vendor            | PurchaseOrder | DeliveryNote/PL | PurchasingResponsible | PurchasingUnit   |
      | 2018000009 | 000001 - Siegwerk | 2018000003    |                 | Amr Khalil            | 0001 - Flexo     |
      | 2018000025 | 000002 - Zhejiang | 2018000047    | 123456          | Gehan Ahmed           | 0002 - Signmedia |
      | 2019000001 | 000002 - Zhejiang | 2018000001    |                 | Gehan Ahmed           | 0002 - Signmedia |
      | 2019000002 | 000002 - Zhejiang | 2018000001    |                 | Gehan Ahmed           | 0002 - Signmedia |
    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate         | Type           | Storekeeper              | State |
      | 2018000009 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0008 - Ali.Hasan         | Draft |
      | 2018000025 | Ahmed.Ali | 05-Jan-2018 11:00 AM | PURCHASE_ORDER | 0007 - Mahmoud.Abdelaziz | Draft |
      | 2019000001 | hr1       | 20-Nov-2019 01:34 PM | PURCHASE_ORDER | 0007 - Mahmoud.Abdelaziz | Draft |
      | 2019000002 | hr1       | 20-Nov-2019 01:38 PM | PURCHASE_ORDER | 0007 - Mahmoud.Abdelaziz | Draft |
    # Items in GR 2018000025
    And GoodsReceipt with code "2018000025" has the following received items:
      | Item   | DifferenceReason |
      | 000001 |                  |
    And Item "000001" in GoodsReceipt (Based On PurchaseOrder) "2018000025" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 20.00            | 0029 |       | DAMAGED_STOCK    |
      | 10.00            | 0033 |       | DAMAGED_STOCK    |
      | 80.00            | 0029 |       | UNRESTRICTED_USE |
      | 40.00            | 0033 |       | UNRESTRICTED_USE |
    # Items in GR 2019000001
    And GoodsReceipt with code "2019000001" has the following received items:
      | Item   | DifferenceReason |
      | 000001 |                  |
      | 000002 |                  |
    And Item "000001" in GoodsReceipt (Based On PurchaseOrder) "2019000001" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 10.00            | 0032 |       | UNRESTRICTED_USE |
    And Item "000002" in GoodsReceipt (Based On PurchaseOrder) "2019000001" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType     |
      | 10.00            | 0029 |       | DAMAGED_STOCK |
    # Items in GR 2019000002
    And GoodsReceipt with code "2019000002" has the following received items:
      | Item   | DifferenceReason |
      | 000003 |                  |
    And Item "000003" in GoodsReceipt "2019000002" has the following batches:
      | BatchNo  | ProdDate    | ExpireDate  | BatchQty(UoE) | UoE  | StockType        | Notes                   |
      | 14536777 | 06-Aug-2019 | 10-Oct-2020 | 9.00          | 0036 | DAMAGED_STOCK    | Crashed while discharge |
      | 14536444 | 06-Aug-2019 | 10-Oct-2020 | 2.00          | 0036 | UNRESTRICTED_USE |                         |
    And insert the following StockAvailabilities exist:
      | Code                                    | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                         | UOM                 | StockType     | AvailableQuantity |
      | DAMAGED_STOCK00020001000100010000020029 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | DAMAGED_STOCK | 10.0              |
 #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy | CreationDate        | DocumentOwner |
      | 2021000003 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |

   #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company          |
      | 2021000003 | 0001 - Flexo | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000003 | 2018000003    | 000001 - Siegwerk | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |

    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2020000001 | 0.0         | 0.0            | 500.00      |
      | 2019000040 | 0.0         | 0.0            | 2000.00     |
      | 2021000003 | 0.0         | 0.0            | 500.00      |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State       | CreatedBy   | CreationDate         | Remaining | DocumentOwner |
      | 2021000001 | VENDOR      | PaymentDone | Amr.Khalil  | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000002 | VENDOR      | PaymentDone | Gehan.Ahmed | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000003 | VENDOR      | PaymentDone | Amr.Khalil  | 05-Aug-2021 09:02 AM |           | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0001 - Flexo     | 0001 - AL Madina |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury | ExpectedDueDate      | BankTransRef | Description                 |
      | 2021000001 | BANK        | 000002 - Zhejiang | INVOICE         | 2020000001  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Sep-2018 09:02 AM |              | downpayment for po          |
      | 2021000002 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000040  | 2000.00   | USD         |                | Alex Bank - 1516171819789 |          | 05-Oct-2018 09:02 AM | 195326512    | Installment No 2 in invoice |
      | 2021000003 | BANK        | 000001 - Siegwerk | INVOICE         | 2021000003  | 500.00    | USD         |                | Alex Bank - 1516171819789 |          | 05-Sep-2018 09:02 AM |              | downpayment for po          |

          #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant | PostingDate          | CurrencyPrice    | JournalEntryCode |
      | 2021000001 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 16 USD | 2021000003       |
      | 2021000002 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 17 USD | 2021000004       |
      | 2021000003 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 16 USD | 2021000005       |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

  #EBS-5944
  #EBS-8508
  Scenario Outline: (01) Activate GR - Update POState and create StoreTransaction by an authorized user (non-batched items)(Happy Path)
    Given user is logged in as "<User>"
    And Last created store transaction was with code "2020000033"
    When "<User>" requests to activate GR with code "2019000001" at "07-Jan-2020 9:30 AM"
    Then GR with Code "2019000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate      | State  | ActivationDate      |
      | <User>        | 07-Jan-2020 9:30 AM | Active | 07-Jan-2020 9:30 AM |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate  | StockType        | BusinessUnit     | Item                                         | UOM                 | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll  | 0032 - Roll 3.20x50 |         | 10.0 | 0.0          | ADD             | 0.0        | 2019000001        | 10.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 |         | 10.0 | 0.0          | ADD             | 0.0        | 2019000001        | 10.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2020000035"
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                         | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000010032 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000001 - Hot Laminated Frontlit Fabric roll  | 0032 - Roll 3.20x50 | UNRESTRICTED_USE | 10.0              |
      | DAMAGED_STOCK00020001000100010000020029    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | DAMAGED_STOCK    | 20.0              |
    And GoodsState of PurchaseOrder with Code "2018000001" is changed to Received
    And a success notification is sent to "<User>" with the following message "GR-msg-12"

    Examples:
      | User                         |
      | Mahmoud.Abdelaziz            |
      | Mahmoud.Abdelaziz.MadinaMain |

  #EBS-5944
  # TODO: handle stock availability for batched item when implemented
  Scenario: (02) Activate GR - Update POState and create StoreTransaction by an authorized user (batched items)(Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2019000002" at "07-Jan-2020 9:30 AM"
    Then GR with Code "2019000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate      | State  | ActivationDate      |
      | Mahmoud.Abdelaziz | 07-Jan-2020 9:30 AM | Active | 07-Jan-2020 9:30 AM |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate  | StockType        | BusinessUnit     | Item                             | UOM              | BatchNo  | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000003 - Flex Primer Varnish E33 | 0036 - Drum-10Kg | 14536444 | 2.0 | 0.0          | ADD             | 0.0        | 2019000002        | 2.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000003 - Flex Primer Varnish E33 | 0036 - Drum-10Kg | 14536777 | 9.0 | 0.0          | ADD             | 0.0        | 2019000002        | 9.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And Last created store transaction is with code "2020000035"
    And GoodsState of PurchaseOrder with Code "2018000001" is changed to Received
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"

  @Future
  Scenario Outline: (03) Activate GR - Update PO State, and create GRActualCostDocument by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And last created Costing is "2019000100"
    When "<User>" requests to activate GR with code "<GRCode>" at "<LastUpdateDate>"
    Then GR with Code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate   | State  | ActivationDate   |
      | <User>        | <LastUpdateDate> | Active | <LastUpdateDate> |
    And GoodsState of PurchaseOrder with Code "<POCodeinGR>" is changed to Received
    #    And Costing is created as follows:
    #      | CostingCode | State | CreatedBy | CreationDate     | LastUpdatedBy | LastUpdateDate   | BusinessUnit | Company        |
    #      | 2019000101  | Draft | <User>    | <LastUpdateDate> | <User>        | <LastUpdateDate> | 0001 - Flexo | 0002 - DigiPro |
    #    And CostingDetails section in Costing with code "2019000101" is created as follows:
    #      | GRCode   |
    #      | <GRCode> |
    #    And CostingItems section in Costing with code "2019000101" is created as follows:
    #      | Item                                                           | UOM                 | Qty | StockType        | %ToBeConsideredinCosting | ItemActualCost |
    #      | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 0034 - Roll 1.07x50 | 2.0 | UNRESTRICTED_USE | 0.00                     | 0.00           |
    #    Then the following values of AccountingDetails section for Costing with code "2019000101" are displayed to "<User>":
    #      | Credit/Debit | Account                   | SubAccount |
    #      | DEBIT        | 70101 - Import Purchasing | 2018000003 |
    #      | CREDIT       | 10207 - PO                | 2018000003 |
    And a success notification is sent to "<User>" with the following message "GR-msg-12"

    Examples:
      | GRCode     | POCodeinGR | User      | LastUpdateDate      |
      | 2018000009 | 2018000003 | Ali.Hasan | 07-Jan-2019 9:30 AM |

  #EBS-7268
  Scenario: (04) Activate GR - Calculate Items Total Final Cost (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2018000025" at "07-Jan-2020 9:30 AM"
    Then GR with Code "2018000025" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate      | State  | ActivationDate      |
      | Mahmoud.Abdelaziz | 07-Jan-2020 9:30 AM | Active | 07-Jan-2020 9:30 AM |
    And the items single unit final cost with damaged items cost are exist as following:
      | ItemCode | ItemFinalCost      |
      | 000001   | 4318.7377584586475 |
      | 000001   | 65.43542058270677  |
    And new StoreTransactions created as follows:
      | CreationDate         | OriginalAddingDate  | StockType        | BusinessUnit     | Item                                        | UOM                 | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost         | ReferenceDocument | RemainingQty | RefTransaction | Company        | Plant                | Storehouse                |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0029 - Roll 2.20x50 |         | 80.0 | 0.0          | ADD             | 4318.7377584586475 | 2018000025        | 80.0         |                | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0033 - Roll 2.70x50 |         | 40.0 | 0.0          | ADD             | 65.43542058270677  | 2018000025        | 40.0         |                | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0029 - Roll 2.20x50 |         | 20.0 | 0.0          | ADD             | 0.0                | 2018000025        | 20.0         |                | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store |
      | 07-Jan-2020 09:30 AM | 07-Jan-2020 9:30 AM | DAMAGED_STOCK    | 0002 - Signmedia | 000001 - Hot Laminated Frontlit Fabric roll | 0033 - Roll 2.70x50 |         | 10.0 | 0.0          | ADD             | 0.0                | 2018000025        | 10.0         |                | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store |
    And Last created store transaction is with code "2020000037"
#    And the following Journal Entry is Created:
#      | Code       | CreationDate        | CreatedBy         | DueDate             | BusinessUnit     | ReferenceDocument       | Company        |
#      | 2020000057 | 07-Jan-2020 9:30 AM | Mahmoud.Abdelaziz | 07-Jan-2020 9:30 AM | 0002 - Signmedia | 2020000003 - LandedCost | 0002 - DigiPro |
#    And The following Journal Items for Journal Entry with code "2020000057":
#      | DueDate             | SubLedger                  | GLAccount                         | SubAccount | Credit   | Debit                  | Currency | Currency(Company) | CurrencyPrice | ExchangeRateCode |
#      | 07-Jan-2020 9:30 AM | UnrealizedCurrencyGainLoss | 40102 - un realized exchange rate |            | 0        | 23853971.5624999999292 | EGP      | EGP             | 1             | 2018000013       |
#      | 07-Jan-2020 9:30 AM | PO                         | 70102 - Local Purchasing          | 2018000047 | 0        | 348116.4375000000708   | EGP      | EGP             | 1             | 2018000013       |
#      | 07-Jan-2020 9:30 AM | PO                         | 10207 - PO                        | 2018000047 | 24202088 | 0                      | EGP      | EGP             | 1             | 2018000013       |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GR-msg-12"
