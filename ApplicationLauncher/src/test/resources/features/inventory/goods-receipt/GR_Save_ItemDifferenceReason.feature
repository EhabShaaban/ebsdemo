# Authors: Mohammed Adel

Feature: Save Item Difference Reason in GR

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | Hosam.Hosni | LogisticsResponsible_Flexo     |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                            |
      | LogisticsResponsible_Signmedia | GRDifferenceReasonEditor_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                            | Permission                       | Condition                      |
      | GRDifferenceReasonEditor_Signmedia | GoodsReceipt:EditDifferentReason | [purchaseUnitName='Signmedia'] |
      | GRDifferenceReasonEditor_Signmedia | GoodsReceipt:EditDifferentReason | [purchaseUnitName='Signmedia'] |
    And the following GoodsReceipts exist:
      | Code       | Type                                                   | BusinessUnit     | State  |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
      | 2018000002 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Active |
      | 2018100016 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 0002 - Signmedia | Draft  |
    And GoodsReceipts has the following received items:
      | GoodsReceiptCode | Item                                         |
      | 2018000008       | 000001 - Hot Laminated Frontlit Fabric roll  |
      | 2018000002       | 000001 - Hot Laminated Frontlit Fabric roll  |
      | 2018100016       | 000002 - Hot Laminated Frontlit Backlit roll |
    And edit session is "30" minutes
   ####### Save Happy Paths
  # EBS-4653
  Scenario Outline: (01) Save GR Item Difference Reason - within edit session by an authorized user (Happy Path)
    Given user is logged in as "<User>"
    And "<User>" opened Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "<User>" saves Difference Reason of Item with code "<ItemCode>" in goodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | DifferenceReason   |
      | <DifferenceReason> |
    Then Item with code "<ItemCode>" in GoodsReceipt with code "<GRCode>" is updated as follows:
      | DifferenceReason   | LastUpdatedBy | LastUpdateDate       |
      | <DifferenceReason> | <User>        | 07-Jan-2019 09:30 AM |
    And GoodsReceipt with code "<GRCode>" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State   |
      | <User>        | 07-Jan-2019 09:30 AM | <State> |
    And edit Difference Reason dialog is closed and the lock by "<User>" on Items section of goodsreceipt with code "<GRCode>" is released
    And a success notification is sent to "<User>" with the following message "Gen-msg-04"
    Examples:
      | User        | GRCode     | ItemCode | State  | DifferenceReason |
      | Gehan.Ahmed | 2018000008 | 000001   | Draft  | Vendor will send |
      | Gehan.Ahmed | 2018000002 | 000001   | Active | Vendor will send |

  ######## Save Authorization
  # EBS-4653

  Scenario Outline: (2) Save GR Item Difference Reason - by an unauthorized user (due to condition or not) - (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves Difference Reason of Item with code "<ItemCode>" in goodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | DifferenceReason   |
      | <DifferenceReason> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User        | GRCode     | ItemCode | DifferenceReason |
      | Hosam.Hosni | 2018000008 | 000001   | Vendor will send |
      | Afaf        | 2018000002 | 000001   | Vendor will send |
  ####### Save Validations
  ####### Save Exception Cases
  #EBS-4653
  Scenario: (3) Save GR Item Difference Reason - after edit session expire (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item with code "000001" in GoodsReceipt with code "2018000008" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "Gehan.Ahmed" saves Difference Reason of Item with code "000001" in goodsReceipt with code "2018000008" at "07-Jan-2019 09:41 AM" with the following values:
      | DifferenceReason |
      | Vendor will send |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-4653
  Scenario: (4) Save GR Item Difference Reason - where GR doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opened Item with code "000002" in GoodsReceipt with code "2018100016" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    And "hr1" first deleted the GoodsReceipt with code "2018100016" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves Difference Reason of Item with code "000002" in goodsReceipt with code "2018100016" at "07-Jan-2019 09:42 AM" with the following values:
      | DifferenceReason |
      | Vendor will send |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-4653
  Scenario: (5) Save GR Item Difference Reason - where Item doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "Gehan.Ahmed" opened Item with code "000002" in GoodsReceipt with code "2018100016" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    And "hr1" first deleted the Item with code "000002" of GoodsReceipt with code "2018100016" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves Difference Reason of Item with code "000002" in goodsReceipt with code "2018100016" at "07-Jan-2019 09:42 AM" with the following values:
      | DifferenceReason |
      | Vendor will send |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"

  Scenario Outline: (6) Save GR Item Difference Reason - with malicious input that violates business rules enforced by the client (Abuse Case)
    Given  user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" opened Item with code "000001" in GoodsReceipt with code "2018000008" in edit mode at "07-Jan-2019 09:10 AM" to edit Item DifferenceReason
    When "Gehan.Ahmed" saves Difference Reason of Item with code "000001" in goodsReceipt with code "2018000008" at "07-Jan-2019 09:15 AM" with the following values:
      | DifferenceReason   |
      | <DifferenceReason> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | DifferenceReason                                                                                                                                                                                                                                                                                                                             |
      | Vendor will send  <them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  >them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  #them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  $them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  *them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  &them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  +them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  ^them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  !them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  =them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  ~them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  \them in next order                                                                                                                                                                                                                                                                                                        |
      | Vendor will send  //them in next order                                                                                                                                                                                                                                                                                                       |
      | Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order Vendor will send  them in next order |