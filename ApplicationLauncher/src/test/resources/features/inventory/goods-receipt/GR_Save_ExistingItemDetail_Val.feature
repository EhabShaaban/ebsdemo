# Author: Ahmed Hamed, Hossam Hassan & Engy Mohamed (EBS-6771)
# Reviewer: Somaya Ahmed (EBS-6771)

Feature: Save Item Detail validation in GR ReceivedItems

  Background:

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |

    And the following roles and sub-roles exist:
      | Role                  | Subrole           |
      | Storekeeper_Signmedia | GROwner_Signmedia |
      | Storekeeper_Signmedia | MeasuresViewer    |
      | Storekeeper_Signmedia | StockTypesViewer  |
      | Storekeeper_Flexo     | GROwner_Flexo     |
      | Storekeeper_Flexo     | MeasuresViewer    |
      | Storekeeper_Flexo     | StockTypesViewer  |

    And the following sub-roles and permissions exist:
      | Subrole           | Permission              | Condition                      |
      | GROwner_Signmedia | GoodsReceipt:UpdateItem | [purchaseUnitName='Signmedia'] |
      | GROwner_Flexo     | GoodsReceipt:UpdateItem | [purchaseUnitName='Flexo']     |
      | MeasuresViewer    | Measures:ReadAll        |                                |
      | StockTypesViewer  | StockType:ReadAll       |                                |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |
      | 2020000020 | 0001 - Flexo     | Draft | SALES_RETURN - Goods Receipt Based on Sales Return     |

    And the following GoodsReceipts exist with the following Items and Quantities:
      | Code       | Item                                        | QtyID |
      | 2018000008 | 000001 - Hot Laminated Frontlit Fabric roll | 1     |
      | 2020000020 | 000051 - Ink5                               | 137   |

    And the following GoodsReceipts exist with the following Items and Batches:
      | Code       | ItemCode | BatchID |
      | 2018000008 | 000003   | 1       |

  ### Failure Cases ###
  ### EBS-4036 #EBS-8708
  Scenario Outline: (01) Save GR Item Detail Val - to GoodsReceipt with Received Qty  unit that does not have an IVR or deleted (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM"
    Then edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code "<GRCode>" becomes locked by "<User>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-05"
    And the following error message is attached to unitOfEntryCode field "GR-msg-11" and sent to "<User>"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 9999 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |

  ### Exception Cases ###
  ### EBS-4036 #EBS-6771 #EBS-8708
  Scenario Outline: (02) Save GR Item Detail Val - to GoodsReceipt after edit session expires (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "<RequestEditingDate>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "<EditingDate>" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-07"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  | RequestEditingDate   | EditingDate          |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 0032 | UNRESTRICTED_USE | Package Damaged                        | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:41 AM |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:41 AM |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456789      | 0029 | UNRESTRICTED_USE | Sample Notes                           | 30-Mar-2020 09:10 AM | 30-Mar-2020 09:41 AM |

  ### EBS-4036 #EBS-6771 #EBS-8708
  Scenario Outline: (03) Save GR Item Detail Val - to GoodsReceipt after edit session expires and GoodsReceipt does not exist  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "<RequestEditingDate>"
    And "hr1" first deleted the GoodsReceipt with code "<GRCode>" successfully at "<DeletionDate>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "<EditingDate>" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-01"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  | RequestEditingDate   | EditingDate          | DeletionDate         |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 0032 | UNRESTRICTED_USE | Package Damaged                        | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:51 AM | 07-Jan-2019 09:41 AM |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:51 AM | 07-Jan-2019 09:41 AM |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456789      | 0029 | UNRESTRICTED_USE | Sample Notes                           | 30-Mar-2020 09:10 AM | 30-Mar-2020 09:51 AM | 30-Mar-2020 09:41 AM |

  ### EBS-4036 #EBS-8708
  Scenario Outline: (04) Save GR Item Detail Val - to GoodsReceipt after edit session expires and item does not exist  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully at "07-Jan-2019 09:31 AM"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:40 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities |

  ### EBS-4036 #EBS-6771 #EBS-8708
  Scenario Outline: (05) Save GR Item Detail Val - to GoodsReceipt after edit session expires and ItemDetail does not exist  (Exception)
    Given user is logged in as "<User>"
    And another user is logged in as "hr1"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "<RequestEditingDate>"
    And "hr1" first deleted Item Detail with id "<DetailID>" with type "<DetailType>" of Item with code "<ItemCode>" of GoodsReceipt with code "<GRCode>" successfully at "<DeletionDate>"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "<EditingDate>" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    Then an error notification is sent to "<User>" with the following message "Gen-msg-20"
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  | RequestEditingDate   | EditingDate          | DeletionDate         |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 0032 | UNRESTRICTED_USE | Package Damaged                        | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:51 AM | 07-Jan-2019 09:41 AM |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0036 | UNRESTRICTED_USE | Samples taken by clearance authorities | 07-Jan-2019 09:10 AM | 07-Jan-2019 09:51 AM | 07-Jan-2019 09:41 AM |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456789      | 0029 | UNRESTRICTED_USE | Sample Notes                           | 30-Mar-2020 09:10 AM | 30-Mar-2020 09:51 AM | 30-Mar-2020 09:41 AM |

  ### Abuse Cases ###
  ### EBS-4036 #EBS-6771 #EBS-8708
  Scenario Outline: (06) Save GR Item Detail Val - to GoodsReceipt with missing mandatory fields (abuse case)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "30-Mar-2020 09:10 AM"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "30-Mar-2020 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             |                  | 0032 | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     |      | UNRESTRICTED_USE | Package Damaged                        |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |         |             |             | 30.123456789     | 0032 |                  | Package Damaged                        |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        |         | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 |             | 29-Jan-2019 | 50.123456789     | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 |             | 50.123456789     | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 |                  | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     |      | UNRESTRICTED_USE | Samples taken by clearance authorities |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 19-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0034 |                  | Samples taken by clearance authorities |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             |                  | 0029 | UNRESTRICTED_USE | Sample Notes                           |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456789      |      | UNRESTRICTED_USE | Sample Notes                           |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |         |             |             | 3.123456789      | 0029 |                  | Sample Notes                           |

  ### EBS-4036 #EBS-6771
  Scenario Outline: (07) Save GR Item Detail Val - to GoodsReceipt with malicious input (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo                                                                                               | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE   | StockType        | Notes                                                                                                                                                                                                                                                       |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 1000000001.0     | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 0.0              | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | -1.0             | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | ay7aga           | 0032  | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 00032 | UNRESTRICTED_USE | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 0032  | ay7aga           | Package Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 0032  | UNRESTRICTED_USE | Package*Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 0032  | UNRESTRICTED_USE | Package$Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 0032  | UNRESTRICTED_USE | Package\Damaged                                                                                                                                                                                                                                             |
      | 2018000008 | 000001   | Mahmoud.Abdelaziz | Quantity   | 1        |                                                                                                       |             |             | 30.0             | 0032  | UNRESTRICTED_USE | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTesttTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestttTestTestTestTestTestTestTestTestTestTestTestTest |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | @ay7aga                                                                                               | 19-Jan-2019 | 29-Jan-2019 | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestt | 19-Jan-2019 | 29-Jan-2019 | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645                                                                                               | ay7aga      | 29-Jan-2019 | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645                                                                                               | 19-01-2019  | 29-Jan-2019 | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645                                                                                               | 19-Jan-2019 | ay7aga      | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645                                                                                               | 19-Jan-2019 | 29-01-2019  | 50.0             | 0034  | UNRESTRICTED_USE | Samples taken by clearance authorities                                                                                                                                                                                                                      |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 1000000001.0     | 0029  | UNRESTRICTED_USE | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 0.0              | 0029  | UNRESTRICTED_USE | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | -1.0             | 0029  | UNRESTRICTED_USE | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | MaliciousInput   | 0029  | UNRESTRICTED_USE | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 00029 | UNRESTRICTED_USE | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 0029  | MaliciousInput   | Sample Notes                                                                                                                                                                                                                                                |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 0029  | UNRESTRICTED_USE | Package*Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 0029  | UNRESTRICTED_USE | Package$Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 0029  | UNRESTRICTED_USE | Package\Damaged                                                                                                                                                                                                                                             |
      | 2020000020 | 000051   | Ali.Hasan         | Quantity   | 137      |                                                                                                       |             |             | 3.0              | 0029  | UNRESTRICTED_USE | TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTesttTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestttTestTestTestTestTestTestTestTestTestTestTestTest |

  ### EBS-4036 #EBS-8708
  Scenario Outline: (08) Save GR Item Detail Val - to GoodsReceipt with ExpirationDate Before ProductionDate (Exception)
    Given user is logged in as "<User>"
    And "<User>" opened Item Detail with id "<DetailID>" of Item with code "<ItemCode>" in ReceivedItems section of GoodsReceipt with code "<GRCode>" in edit mode at "07-Jan-2019 09:10 AM"
    When "<User>" saves Item Detail with id "<DetailID>" of type "<DetailType>" in Item with code "<ItemCode>" to ReceivedItems section in GoodsReceipt with code "<GRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | BatchNo   | ProdDate   | ExpireDate   | ReceivedQty(UoE)   | UoE   | StockType   | Notes   |
      | <BatchNo> | <ProdDate> | <ExpireDate> | <ReceivedQty(UoE)> | <UoE> | <StockType> | <Notes> |
    And the lock by "<User>" on ReceivedItems section in GoodsReceipt with Code "<GRCode>" is released
    And "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | GRCode     | ItemCode | User              | DetailType | DetailID | BatchNo | ProdDate    | ExpireDate  | ReceivedQty(UoE) | UoE  | StockType        | Notes                                  |
      | 2018000008 | 000003   | Mahmoud.Abdelaziz | Batch      | 1        | 6425645 | 30-Jan-2019 | 29-Jan-2019 | 50.123456789     | 0034 | UNRESTRICTED_USE | Samples taken by clearance authorities |