Feature: View POGoodsReciept-Company section

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |
      | hr1               | SuperUser             |

    And the following roles and sub-roles exist:
      | Role                  | Subrole            |
      | Storekeeper_Signmedia | GRViewer_Signmedia |
      | Storekeeper_Flexo     | GRViewer_Flexo     |
      | SuperUser             | SuperUserSubRole   |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission               | Condition                      |
      | GRViewer_Signmedia | GoodsReceipt:ReadCompany | [purchaseUnitName='Signmedia'] |
      | GRViewer_Flexo     | GoodsReceipt:ReadCompany | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                      |                                |
    And the following users have the following permissions without the following conditions:
      | User      | Permission               | Condition                      |
      | Ali.Hasan | GoodsReceipt:ReadCompany | [purchaseUnitName='Signmedia'] |

    And the following GoodsReceipts exist:
      | Code       | BusinessUnit     | State | Type                                                   |
      | 2018000008 | 0002 - Signmedia | Draft | PURCHASE_ORDER - Goods Receipt Based on Purchase Order |

    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                      | BusinessUnit     |
      | 2018000008 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0002 - Signmedia |

    #### Happy Path

    # EBS-6086
  Scenario: (01) View GR CompanyData section - by an authorized user with one role (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to view Company section of GoodsReceipt with code "2018000008"
    Then the following values of Company section for GoodsReceipt with code "2018000008" are displayed to "Mahmoud.Abdelaziz":
      | Company   | Storehouse               | BusinessUnit |
      | AL Madina | AlMadina Secondary Store | Signmedia    |

    #### Exceptions

    # EBS-6086
  Scenario: (02) View GR CompanyData section - where GoodsReceipt has been deleted by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsReceipt with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to view Company section of GoodsReceipt with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

    #### Abuse Cases

    # EBS-6086
  Scenario: (03) View GR CompanyData section - by an unauthorized user due to condition or not (Abuse Case)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view Company section of GoodsReceipt with code "2018000008"
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page

