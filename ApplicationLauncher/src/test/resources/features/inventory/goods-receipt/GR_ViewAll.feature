#Author: Ahmad Hamed
#Reviewer: Somaya Ahmed

Feature: View all Goods Receipts

  Background:
    Given the following users and roles exist:
      | Name              | Role                             |
      | Ashraf.Fathi      | M.D                              |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia            |
      | Amr.Khalil        | PurchasingResponsible_Flexo      |
      | Amr.Khalil        | PurchasingResponsible_Corrugated |
      | hr1               | SuperUser                        |
      | Afaf              | FrontDesk                        |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role               |
      | M.D                              | GRViewer               |
      | M.D                              | GoodsReceiptTypeViewer |
      | Storekeeper_Signmedia            | GROwner_Signmedia      |
      | Storekeeper_Signmedia            | GoodsReceiptTypeViewer |
      | PurchasingResponsible_Flexo      | GRViewer_Flexo         |
      | PurchasingResponsible_Flexo      | GoodsReceiptTypeViewer |
      | PurchasingResponsible_Corrugated | GRViewer_Corrugated    |
      | PurchasingResponsible_Corrugated | GoodsReceiptTypeViewer |
      | SuperUser                        | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission               | Condition                       |
      | GRViewer               | GoodsReceipt:ReadAll     |                                 |
      | GROwner_Signmedia      | GoodsReceipt:ReadAll     | [purchaseUnitName='Signmedia']  |
      | GRViewer_Flexo         | GoodsReceipt:ReadAll     | [purchaseUnitName='Flexo']      |
      | GRViewer_Corrugated    | GoodsReceipt:ReadAll     | [purchaseUnitName='Corrugated'] |
      | GoodsReceiptTypeViewer | GoodsReceiptType:ReadAll |                                 |
      | GoodsReceiptTypeViewer | GoodsReceiptType:ReadAll |                                 |
      | SuperUserSubRole       | *:*                      |                                 |

    And the following Vendors exist:
      | Code   | VendorName |
      | 000001 | Siegwerk   |
      | 000002 | Zhejiang   |

    And the following Customers exist:
      | Code   | Name            | BusinessUnit |
      | 000006 | المطبعة الأمنية | 0001 - Flexo |

    And the following Storehouses exist:
      | Code | Name                     |
      | 0001 | AlMadina Main Store      |
      | 0002 | AlMadina Secondary Store |
      | 0003 | DigiPro Main Store       |

    And the following PurchaseUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |

    And the following GoodsReceiptTypes exist:
      | Code           | Name                                  |
      | SALES_RETURN   | Goods Receipt Based on Sales Return   |
      | PURCHASE_ORDER | Goods Receipt Based on Purchase Order |

    And the following GoodsReceipts exist with more data:
      | Code       | Type                                                   | BusinessPartner          | Storehouse                      | ActivationDate       | State  | BusinessUnit      |
      | 2020000021 | SALES_RETURN - Goods Receipt Based on Sales Return     | 000006 - المطبعة الأمنية | 0003 - DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | 0001 - Flexo      |
      | 2020000020 | SALES_RETURN - Goods Receipt Based on Sales Return     | 000006 - المطبعة الأمنية | 0003 - DigiPro Main Store       |                      | Draft  | 0001 - Flexo      |
      | 2018000999 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000001 - Siegwerk        | 0002 - AlMadina Secondary Store |                      | Draft  | 0001 - Flexo      |
      | 2018000016 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0001 - AlMadina Main Store      |                      | Draft  | 0002 - Signmedia  |
      | 2018000015 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0001 - AlMadina Main Store      |                      | Draft  | 0002 - Signmedia  |
      | 2018000012 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store |                      | Draft  | 0006 - Corrugated |
      | 2018000011 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store |                      | Draft  | 0006 - Corrugated |
      | 2018000009 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000001 - Siegwerk        | 0002 - AlMadina Secondary Store |                      | Draft  | 0001 - Flexo      |
      | 2018000008 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store |                      | Draft  | 0002 - Signmedia  |
      | 2018000004 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000001 - Siegwerk        | 0002 - AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | 0001 - Flexo      |
      | 2018000003 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000001 - Siegwerk        | 0002 - AlMadina Secondary Store |                      | Draft  | 0001 - Flexo      |
      | 2018000002 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | 0002 - Signmedia  |
      | 2018000001 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store |                      | Draft  | 0002 - Signmedia  |
      | 2018000000 | PURCHASE_ORDER - Goods Receipt Based on Purchase Order | 000002 - Zhejiang        | 0002 - AlMadina Secondary Store |                      | Draft  | 0002 - Signmedia  |

  Scenario: (01) ViewAll GoodsReceipt - by authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of GRs with 20 records per page
    Then the following values will be presented to "Ashraf.Fathi":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       |                      | Draft  | Flexo        |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000000 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "Ashraf.Fathi" are 14

  Scenario: (02) ViewAll GoodsReceipt - by authorized user WITH condition: user has one role (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GRs with 20 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000000 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 6


  Scenario: (03) ViewAll GoodsReceipt - by authorized user WITH condition: user has two roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of GRs with 20 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       |                      | Draft  | Flexo        |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "Amr.Khalil" are 8

##### Filter by Type
  Scenario: (04) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on GRCode which contains "00000" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       |                      | Draft  | Flexo        |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "hr1" are 9

  Scenario: (05) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of GRs with filter applied on GRCode which contains "00000" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "hr1" are 9

  Scenario: (06) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on GRCode which contains "00001" with 20 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate | State | BusinessUnit |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                | Draft | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                | Draft | Signmedia    |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Signmedia    |
    And the total number of records in search results by "hr1" are 5

  Scenario: (07) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GRs with filter applied on GRCode which contains "00000" with 3 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 4

  Scenario: (08) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of GRs with filter applied on GRCode which contains "00000" with 20 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       |                      | Draft  | Flexo        |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "Amr.Khalil" are 5

##### Filter by Type

#EBS-6786
  Scenario: (09) ViewAll GoodsReceipt - by authorized user using "Contains"
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on Type which contains "Ord" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000000 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "hr1" are 12


   #### Filter by State

  Scenario: (10) ViewAll GoodsReceipt - by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on State which contains "dRAft" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate | State | BusinessUnit |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       |                | Draft | Flexo        |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                | Draft | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                | Draft | Signmedia    |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Signmedia    |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Signmedia    |
    And the total number of records in search results by "hr1" are 11


  Scenario: (11) ViewAll GoodsReceipt - by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on State which contains "Active" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store       | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
    And the total number of records in search results by "hr1" are 3


 #### Filter by Storehouse

  Scenario: (12) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on StorehouseName which contains "Sec" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate | State | BusinessUnit |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Corrugated   |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                | Draft | Signmedia    |
    And the total number of records in search results by "hr1" are 10

  Scenario: (13) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on StorehouseName which contains "الثانوي" with 3 records per page while current locale is "ar-EG"
    Then the following values will be presented to "hr1":
      | Code       | Type                  | BusinessPartner | Storehouse           | ActivationDate | State | BusinessUnit |
      | 2018000999 | إذن إضافة من أمر شراء | Siegwerk        | مخزن المدينه الثانوي |                | Draft | Flexo        |
      | 2018000012 | إذن إضافة من أمر شراء | Zhejiang        | مخزن المدينه الثانوي |                | Draft | Corrugated   |
      | 2018000011 | إذن إضافة من أمر شراء | Zhejiang        | مخزن المدينه الثانوي |                | Draft | Corrugated   |
    And the total number of records in search results by "hr1" are 10


  Scenario: (14) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of GRs with filter applied on StorehouseName which contains "Sec" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000000 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "hr1" are 10

  Scenario: (15) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on StorehouseName which contains "Mai" with 20 records per page while current locale is "en"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse          | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store  | 29-Mar-2020 09:00 AM | Active | Flexo        |
      | 2020000020 | Goods Receipt Based on Sales Return   | المطبعة الأمنية | DigiPro Main Store  |                      | Draft  | Flexo        |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store |                      | Draft  | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "hr1" are 4

  Scenario: (16) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GRs with filter applied on StorehouseName which contains "Sec" with 3 records per page while current locale is "en"
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 4

  Scenario: (17) ViewAll GoodsReceipt - by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of GRs with filter applied on StorehouseName which contains "Sec" with 1 records per page while current locale is "en"
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate | State | BusinessUnit |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                | Draft | Flexo        |
    And the total number of records in search results by "Amr.Khalil" are 6

#### Filter by GR Activation Date

  Scenario: (18) ViewAll GoodsReceipt - by authorized user using "equals" (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on GRActivationDate which equals "06-Jan-2018" with 2 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
    And the total number of records in search results by "hr1" are 1

  Scenario: (20) ViewAll GoodsReceipt - by authorized user using "equals" (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GRs with filter applied on GRActivationDate which equals "06-Jan-2018" with 3 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 1

  Scenario: (21) ViewAll GoodsReceipt - by authorized user using "equals" (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read page 1 of GRs with filter applied on GRActivationDate which equals "29-Mar-2020" with 3 records per page
    Then the following values will be presented to "Amr.Khalil":
      | Code       | Type                                | BusinessPartner | Storehouse         | ActivationDate       | State  | BusinessUnit |
      | 2020000021 | Goods Receipt Based on Sales Return | المطبعة الأمنية | DigiPro Main Store | 29-Mar-2020 09:00 AM | Active | Flexo        |
    And the total number of records in search results by "Amr.Khalil" are 1

 #### Filter by ---> Multiple Columns

  Scenario: (22) ViewAll GoodsReceipt - by authorized user using - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with 3 records per page and the following filters applied on GR while current locale is "en-US":
      | FieldName      | Operation | Value       |
      | userCode       | contains  | 00000       |
      | storehouseName | contains  | Sec         |
      | activationDate | contains  | 06-Jan-2018 |
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
    And the total number of records in search results by "hr1" are 1

 ### Filter by ---> BusinessPartner name and code (Mixed Values)

    #EBS-3565
  Scenario: (23) ViewAll GoodsReceipt - by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on BusinessPartner which contains "ieg" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "hr1" are 4

    #EBS-3565
  Scenario: (24) ViewAll GoodsReceipt - by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on BusinessPartner which contains "0002" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000016 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000015 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Main Store      |                      | Draft  | Signmedia    |
      | 2018000012 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000011 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Corrugated   |
      | 2018000008 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000002 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store | 06-Jan-2018 01:00 PM | Active | Signmedia    |
      | 2018000001 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
      | 2018000000 | Goods Receipt Based on Purchase Order | Zhejiang        | AlMadina Secondary Store |                      | Draft  | Signmedia    |
    And the total number of records in search results by "hr1" are 8

      #EBS-3565
  Scenario: (25) ViewAll GoodsReceipt - by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of GRs with filter applied on BusinessPartner which contains "si" with 20 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                                  | BusinessPartner | Storehouse               | ActivationDate       | State  | BusinessUnit |
      | 2018000999 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000009 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
      | 2018000004 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store | 02-Dec-2019 08:09 AM | Active | Flexo        |
      | 2018000003 | Goods Receipt Based on Purchase Order | Siegwerk        | AlMadina Secondary Store |                      | Draft  | Flexo        |
    And the total number of records in search results by "hr1" are 4

  Scenario: (26) ViewAll GoodsReceipt - by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of GRs with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page