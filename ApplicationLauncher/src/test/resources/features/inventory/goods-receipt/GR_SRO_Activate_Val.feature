# Author: Niveen Magdy & ENgy Mohammed
# Reviewer:
Feature: Activate Goods Receipt Sales Return (Val)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                   |
      | Storekeeper_Signmedia | GRActivateOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission            | Condition                      |
      | GRActivateOwner_Signmedia | GoodsReceipt:Activate | [purchaseUnitName='Signmedia'] |

    And the following GRs exist with the following GeneralData:
      | Code       | CreatedBy | CreationDate          | Type         | Storekeeper      | State  |
      | 2020000022 | hr1       | 15-July-2020 01:58 PM | SALES_RETURN | 0006 - Ahmed.Ali | Active |
      | 2020000027 | hr1       | 18-Aug-2020 02:47 PM  | SALES_RETURN | 0006 - Ahmed.Ali | Draft  |
      | 2020000028 | hr1       | 18-Aug-2020 02:56 PM  | SALES_RETURN | 0006 - Ahmed.Ali | Draft  |

    And the following CompanyData for GoodsReceipts exist:
      | GRCode     | Company          | Storehouse                 | BusinessUnit     |
      | 2020000022 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000027 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000028 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | Completed  | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

    And the following SalesReturnData for GoodsReceipts exist:
      | GRCode     | SalesReturn | SalesReturnDocumentOwner                  | SalesReturnCustomer            | Notes |
      | 2020000022 | 2020000056  | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية |       |
      | 2020000027 | 2020000053  | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية |       |
      | 2020000028 | 2020000056  | SeragEldin.Meghawry - SeragEldin Meghawry | 000007 - مطبعة أكتوبر الهندسية |       |
      | 2020000029 | 2020000005  | Ahmed.Al-Ashry - Ahmed Al-Ashry           | 000007 - مطبعة أكتوبر الهندسية |       |

        # Items in GR 2020000027
    And GoodsReceipt with code "2020000027" has the following received items:
      | Item   | DifferenceReason |
      | 000053 |                  |

    And Item "000053 - Hot Laminated Frontlit Fabric roll 2" in GoodsReceipt (Based On SalesReturn) "2020000027" has no received quantities:

        # Items in GR 2020000028
    And GoodsReceipt with code "2020000028" has the following received items:
      | Item   | DifferenceReason |
      | 000053 |                  |

    And Item "000053 - Hot Laminated Frontlit Fabric roll 2" in GoodsReceipt (Based On SalesReturn) "2020000028" has the following received quantities:
      | ReceivedQty(UoE) | UoE                 | StockType        | Notes |
      | 1.0              | 0035 - Roll 1.27x50 | UNRESTRICTED_USE |       |

  #EBS-6773
  Scenario: (01) Activate GR Val - with missing mandatoried (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2020000027" at "20-Nov-2020 09:30 AM"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-40"

  #EBS-6773
  Scenario: (02) Activate GR Val - with an invalid state - Active State (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to activate GR with code "2020000022" at "20-Nov-2020 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-32"

  #EBS-6773
  Scenario: (03) Activate GR Val - where GoodsReceipt doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    When "hr1" first deleted the GoodsReceipt with code "2020000028" successfully
    And "Mahmoud.Abdelaziz" requests to activate GR with code "2020000028" at "20-Nov-2020 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  #EBS-6773
  Scenario: (04) Activate GR Val - when one of the sections is locked (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And first "hr1" requests to add Item Quantity for Item with code "000053" to Items section of GoodsReceipt with code "2020000028" at "20-Nov-2020 09:30 AM"
    And "Mahmoud.Abdelaziz" requests to activate GR with code "2020000028" at "20-Nov-2020 09:25 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-14"

  #EBS-6773
  Scenario Outline: (05) Activate GR Val - where SalesReturnOrder state is (GoodsReceiptActivated/Completed)(Exception Case)
    Given user is logged in as "<User>"
    When "<User>" requests to activate GR with code "<GRCode>" at "20-Nov-2020 09:25 AM"
    Then a failure notification is sent to "<User>" with the following message "Gen-msg-53"
    Examples:
      | GRCode     | User              |
      | 2020000028 | Mahmoud.Abdelaziz |
      | 2020000029 | Mahmoud.Abdelaziz |
