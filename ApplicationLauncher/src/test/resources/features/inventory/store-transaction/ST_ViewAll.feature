# Updated by Niveen and Eman Hesham, Reviewed by: Somaya Ahmed (EBS-6448)

Feature: View All Store Transaction

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | hr1         | SuperUser                      |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole                          |
      | LogisticsResponsible_Signmedia | StoreTransactionViewer_Signmedia |
      | LogisticsResponsible_Signmedia | StockTypesViewer                 |
      | LogisticsResponsible_Signmedia | CompanyViewer                    |
      | LogisticsResponsible_Signmedia | PurUnitReader_Signmedia          |
      | SuperUser                      | SuperUserSubRole                 |
    And the following sub-roles and permissions exist:
      | Subrole                          | Permission                | Condition                      |
      | StoreTransactionViewer_Signmedia | StoreTransactions:ReadAll | [purchaseUnitName='Signmedia'] |
      | StockTypesViewer                 | StockType:ReadAll         |                                |
      | CompanyViewer                    | Company:ReadAll           |                                |
      | PurUnitReader_Signmedia          | PurchasingUnit:ReadAll    |                                |
      | SuperUserSubRole                 | *:*                       |                                |
    And the following users doesn't have the following permissions:
      | User | Permission                |
      | Afaf | StoreTransactions:ReadAll |
    And the ONLY StoreTransactions with View All data exist:
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM                 | Qty           | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty  | RefTransaction |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.12345678   | 200.123456789 | 200.12345678  | 2019000088 - Goods Receipt  | 10.123456789  |                |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 30.123456789  |               |               | 2019000003 - Goods Issue    | 0.123456789   | 2020000001     |
      | 2019000005 |                      | TAKE            | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter        | 5.123456789   |               |               | 2019000001 - Goods Issue    | 0.123456789   | 2019000018     |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.123456789  | 20.123456789  | 20.123456789  | 2019000088 - Goods Receipt  | 10.12345678   |                |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter        | 10.12345678   | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789   |                |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 150.123456789 | 0.123456789   | 0.123456789   | 2020000001 - Goods Receipt  | 120.123456789 |                |

  Scenario: (01) View All StoreTransactions by authorized user due to condition + No Paging + No Filter (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of StoreTransactions with no filter applied with 50 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM                 | Qty           | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 150.123456789 | 0.123456789   | 0.123456789  | 2020000001 - Goods Receipt | 120.123456789 |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.123456789  | 20.123456789  | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678   |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 30.123456789  |               |              | 2019000003 - Goods Issue   | 0.123456789   |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.12345678   | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789  |
    And the total number of records in search results by "Gehan.Ahmed" are 4

  Scenario: (02) View All StoreTransactions by authorized user without condition + Paging + No Filter (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with no filter applied with 5 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM                 | Qty           | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 150.123456789 | 0.123456789   | 0.123456789   | 2020000001 - Goods Receipt  | 120.123456789 |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter        | 10.12345678   | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789   |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.123456789  | 20.123456789  | 20.123456789  | 2019000088 - Goods Receipt  | 10.12345678   |
      | 2019000005 |                      | TAKE            | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter        | 5.123456789   |               |               | 2019000001 - Goods Issue    | 0.123456789   |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 30.123456789  |               |               | 2019000003 - Goods Issue    | 0.123456789   |
    And the total number of records in search results by "hr1" are 6

  Scenario: (03) View All StoreTransactions - Filter StoreTransactions by TransactionCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on TransactionCode which contains "002" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM       | Qty          | EstimateCost | ActualCost | ReferenceDocument        | RemainingQty |
      | 2019000002 |                    | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 30.123456789 |              |            | 2019000003 - Goods Issue | 0.123456789  |
    And the total number of records in search results by "hr1" are 1

  Scenario: (04) Filter StoreTransactions by TransactionType by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on TransactionType which equals "TAKE" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM          | Qty          | EstimateCost | ActualCost | ReferenceDocument        | RemainingQty |
      | 2019000005 |                    | TAKE            | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter | 5.123456789  |              |            | 2019000001 - Goods Issue | 0.123456789  |
      | 2019000002 |                    | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 30.123456789 |              |            | 2019000003 - Goods Issue | 0.123456789  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (05) View All StoreTransactions - Filter StoreTransactions by Company by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Company which equals "0001" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM                 | Qty           | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 150.123456789 | 0.123456789   | 0.123456789  | 2020000001 - Goods Receipt | 120.123456789 |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.123456789  | 20.123456789  | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678   |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 30.123456789  |               |              | 2019000003 - Goods Issue   | 0.123456789   |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.12345678   | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789  |
    And the total number of records in search results by "hr1" are 4

  Scenario: (06) View All StoreTransactions - Filter StoreTransactions by Plant by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Plant which contains "iPro Plan" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company        | Plant                | Storehouse                | PurchaseUnit | StockType        | Item          | UOM          | Qty         | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 10.12345678 | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789  |
      | 2019000005 |                      | TAKE            | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 5.123456789 |               |               | 2019000001 - Goods Issue    | 0.123456789  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (07) View All StoreTransactions - Filter StoreTransactions by Storehouse by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Storehouse which contains "Pro Ma" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company        | Plant                | Storehouse                | PurchaseUnit | StockType        | Item          | UOM          | Qty         | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 10.12345678 | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789  |
      | 2019000005 |                      | TAKE            | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 5.123456789 |               |               | 2019000001 - Goods Issue    | 0.123456789  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (08) View All StoreTransactions - Filter StoreTransactions by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on BusinessUnit which equals "0001" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company        | Plant                | Storehouse                | PurchaseUnit | StockType        | Item          | UOM          | Qty         | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 10.12345678 | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789  |
      | 2019000005 |                      | TAKE            | 0002 - DigiPro | 0002 - DigiPro Plant | 0003 - DigiPro Main Store | 0001 - Flexo | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 5.123456789 |               |               | 2019000001 - Goods Issue    | 0.123456789  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (09) View All StoreTransactions - Filter StoreTransactions by StockType by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on StockType which equals "DAMAGED_STOCK" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType     | Item                                         | UOM                 | Qty         | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678 | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789 |
    And the total number of records in search results by "hr1" are 1

  Scenario: (10.123456789) View All StoreTransactions - Filter StoreTransactions by ItemCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Item which contains "002" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty          | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.123456789 | 20.123456789  | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678  |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678  | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789 |
    And the total number of records in search results by "hr1" are 2

  Scenario: (11) View All StoreTransactions - Filter StoreTransactions by ItemName by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Item which contains "bric" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM       | Qty           | EstimateCost | ActualCost  | ReferenceDocument          | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 150.123456789 | 0.123456789  | 0.123456789 | 2020000001 - Goods Receipt | 120.123456789 |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 30.123456789  |              |             | 2019000003 - Goods Issue   | 0.123456789   |
    And the total number of records in search results by "hr1" are 2

  Scenario: (12) View All StoreTransactions - Filter StoreTransactions by UnitOfMeasureName by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on UnitOfMeasureName which contains "M2" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                      | PurchaseUnit     | StockType        | Item                                          | UOM       | Qty           | EstimateCost | ActualCost  | ReferenceDocument          | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 150.123456789 | 0.123456789  | 0.123456789 | 2020000001 - Goods Receipt | 120.123456789 |
      | 2019000002 |                      | TAKE            | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 30.123456789  |              |             | 2019000003 - Goods Issue   | 0.123456789   |
    And the total number of records in search results by "hr1" are 2

  Scenario: (13) View All StoreTransactions - Filter StoreTransactions by Quantity by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on Quantity which equals "10.12345678" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty         | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                | 0014 - Liter        | 10.12345678 | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789  |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678 | 200.123456789 | 200.12345678  | 2019000088 - Goods Receipt  | 10.123456789 |
    And the total number of records in search results by "hr1" are 2

  Scenario: (14) View All StoreTransactions - Filter StoreTransactions by EstimateCost by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on EstimateCost which equals "20.123456789" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty          | EstimateCost | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.123456789 | 20.123456789 | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678  |
    And the total number of records in search results by "hr1" are 1

  Scenario: (15) View All StoreTransactions - Filter StoreTransactions by ActualCost by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on ActualCost which equals "200.12345678" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType     | Item                                         | UOM                 | Qty         | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678 | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789 |
    And the total number of records in search results by "hr1" are 1

  Scenario: (16) View All StoreTransactions - Filter StoreTransactions by RefDocumentCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on RefDocument which contains "0088" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty          | EstimateCost  | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.123456789 | 20.123456789  | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678  |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678  | 200.123456789 | 200.12345678 | 2019000088 - Goods Receipt | 10.123456789 |
    And the total number of records in search results by "hr1" are 2

  Scenario: (17) View All StoreTransactions - Filter StoreTransactions by RefDocumentType by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on RefDocument which contains "eipt" with 50 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                          | UOM                 | Qty           | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty  |
      | 2020000001 | 05-Feb-2020 03:48 PM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 150.123456789 | 0.123456789   | 0.123456789   | 2020000001 - Goods Receipt  | 120.123456789 |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                 | 0014 - Liter        | 10.12345678   | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789   |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.123456789  | 20.123456789  | 20.123456789  | 2019000088 - Goods Receipt  | 10.12345678   |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 10.12345678   | 200.123456789 | 200.12345678  | 2019000088 - Goods Receipt  | 10.123456789  |
    And the total number of records in search results by "hr1" are 4

  Scenario: (18) View All StoreTransactions - Filter StoreTransactions by RemainingQuantity by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on RemainingQuantity which equals "10.12345678" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty          | EstimateCost | ActualCost   | ReferenceDocument          | RemainingQty |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.123456789 | 20.123456789 | 20.123456789 | 2019000088 - Goods Receipt | 10.12345678  |
    And the total number of records in search results by "hr1" are 1

 # EBS-6448
  Scenario: (19) View All StoreTransactions - Filter StoreTransactions by OriginalAddingDate by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of StoreTransactions with filter applied on OriginalAddingDate which equals "07-Jan-2019" with 50 records per page
    Then the following values will be presented to "hr1":
      | STCode     | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | PurchaseUnit     | StockType        | Item                                         | UOM                 | Qty          | EstimateCost  | ActualCost    | ReferenceDocument           | RemainingQty |
      | 2019000018 | 07-Jan-2019 09:32 AM | ADD             | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  | 0001 - Flexo     | UNRESTRICTED_USE | 000051 - Ink5                                | 0014 - Liter        | 10.12345678  | 120.123456789 | 120.123456789 | Legacy System نظام المتكامل | 3.123456789  |
      | 2019000007 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.123456789 | 20.123456789  | 20.123456789  | 2019000088 - Goods Receipt  | 10.12345678  |
      | 2019000001 | 07-Jan-2019 09:30 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 10.12345678  | 200.123456789 | 200.12345678  | 2019000088 - Goods Receipt  | 10.123456789 |
    And the total number of records in search results by "hr1" are 3

  Scenario: (20.123456789) View All StoreTransactions - Filter StoreTransactions by TransactionCode by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of StoreTransactions with filter applied on TransactionCode which contains "002" with 50 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page