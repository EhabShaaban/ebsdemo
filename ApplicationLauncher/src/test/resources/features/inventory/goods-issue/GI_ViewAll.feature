Feature: View All GoodsIssues

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Ahmed.Seif        | Quality_Specialist    |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                 |
      | Quality_Specialist    | GIViewer                |
      | Storekeeper_Signmedia | GIViewer_Signmedia      |
      | Storekeeper_Signmedia | GoodsIssueTypeViewer    |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                 | Permission             | Condition                      |
      | GIViewer                | GoodsIssue:ReadAll     |                                |
      | GIViewer_Signmedia      | GoodsIssue:ReadAll     | [purchaseUnitName='Signmedia'] |
      | GoodsIssueTypeViewer    | GoodsIssueType:ReadAll |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | GoodsIssue:ReadAll |

    And the following GoodsIssues exist with the following data:
      | Code       | PurchasingUnit   | State  | CreationDate         | Type                                                    | Storehouse                      | Customer                       |
      | 2020000002 | 0001 - Flexo     | Active | 08-Nov-2020 09:00 AM | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | 0003 - DigiPro Main Store       | 000006 - المطبعة الأمنية       |
      | 2020000001 | 0002 - Signmedia | Draft  | 06-Nov-2020 09:00 AM | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | 0001 - AlMadina Main Store      | 000007 - مطبعة أكتوبر الهندسية |
      | 2019000004 | 0001 - Flexo     | Draft  | 10-Oct-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer              | 0001 - AlMadina Main Store      | 000001 - Al Ahram              |
      | 2019000003 | 0002 - Signmedia | Active | 06-Aug-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer              | 0002 - AlMadina Secondary Store | 000001 - Al Ahram              |
      | 2019000002 | 0002 - Signmedia | Draft  | 05-Aug-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer              | 0001 - AlMadina Main Store      | 000001 - Al Ahram              |
      | 2019000001 | 0001 - Flexo     | Active | 03-Jul-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer              | 0003 - DigiPro Main Store       | 000006 - المطبعة الأمنية       |

  Scenario: (01) View all GoodsIssues - by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with no filter applied with 4 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                             | Storehouse               | Customer              |
      | 2020000002 | Flexo        | Active | 08-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | DigiPro Main Store       | المطبعة الأمنية       |
      | 2020000001 | Signmedia    | Draft  | 06-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | AlMadina Main Store      | مطبعة أكتوبر الهندسية |
      | 2019000004 | Flexo        | Draft  | 10-Oct-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store      | Al Ahram              |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Secondary Store | Al Ahram              |
    And the total number of records in search results by "Ahmed.Seif" are 6

  Scenario: (02) View all GoodsIssues - by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GoodsIssues with no filter applied with 20 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                             | Storehouse               | Customer              |
      | 2020000001 | Signmedia    | Draft  | 06-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | AlMadina Main Store      | مطبعة أكتوبر الهندسية |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Secondary Store | Al Ahram              |
      | 2019000002 | Signmedia    | Draft  | 05-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store      | Al Ahram              |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 3

  Scenario: (03) View all GoodsIssues - Filter by Code by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on GICode which contains "00002" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                             | Storehouse          | Customer        |
      | 2020000002 | Flexo        | Active | 08-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | DigiPro Main Store  | المطبعة الأمنية |
      | 2019000002 | Signmedia    | Draft  | 05-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store | Al Ahram        |
    And the total number of records in search results by "Ahmed.Seif" are 2

  Scenario: (04) View all GoodsIssues - Filter by State by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on State which contains "Draft" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State | CreationDate         | Type                             | Storehouse          | Customer              |
      | 2020000001 | Signmedia    | Draft | 06-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | AlMadina Main Store | مطبعة أكتوبر الهندسية |
      | 2019000004 | Flexo        | Draft | 10-Oct-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store | Al Ahram              |
      | 2019000002 | Signmedia    | Draft | 05-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store | Al Ahram              |
    And the total number of records in search results by "Ahmed.Seif" are 3

  Scenario: (05) View all GoodsIssues - Filter by Creation Date by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on Creation date which contains "05-Aug-2019" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State | CreationDate         | Type                | Storehouse          | Customer |
      | 2019000002 | Signmedia    | Draft | 05-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Main Store | Al Ahram |
    And the total number of records in search results by "Ahmed.Seif" are 1

  Scenario: (06) View all GoodsIssues - Filter by Storehouse by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on Storehouse which contains "Secondary" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                | Storehouse               | Customer |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Secondary Store | Al Ahram |
    And the total number of records in search results by "Ahmed.Seif" are 1

  Scenario: (07) View all GoodsIssues - Filter by Customer by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on Customer which contains "Al Ahram" with 20 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                | Storehouse               | Customer |
      | 2019000004 | Flexo        | Draft  | 10-Oct-2019 09:02 AM | Deliver To Customer | AlMadina Main Store      | Al Ahram |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Secondary Store | Al Ahram |
      | 2019000002 | Signmedia    | Draft  | 05-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Main Store      | Al Ahram |
    And the total number of records in search results by "Ahmed.Seif" are 3

  Scenario: (08) View all GoodsIssues - Filter by PurchaseUnit by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of GoodsIssues with filter applied on PurchaseUnit which contains "Signmedia" with 5 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                             | Storehouse               | Customer              |
      | 2020000001 | Signmedia    | Draft  | 06-Nov-2020 09:00 AM | Goods Issue Based On Sales Order | AlMadina Main Store      | مطبعة أكتوبر الهندسية |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Secondary Store | Al Ahram              |
      | 2019000002 | Signmedia    | Draft  | 05-Aug-2019 09:02 AM | Deliver To Customer              | AlMadina Main Store      | Al Ahram              |
    And the total number of records in search results by "Ahmed.Seif" are 3

  Scenario: (09) View all GoodsIssues - Filter by GoodsIssue type by authorized user using "contains" (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of GoodsIssues with filter applied on GoodsIssueType which contains "Deliver To Customer" with 5 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | PurchaseUnit | State  | CreationDate         | Type                | Storehouse               | Customer |
      | 2019000003 | Signmedia    | Active | 06-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Secondary Store | Al Ahram |
      | 2019000002 | Signmedia    | Draft  | 05-Aug-2019 09:02 AM | Deliver To Customer | AlMadina Main Store      | Al Ahram |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 2

  Scenario: (10) View all GoodsIssues - Filter by Code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of GoodsIssues with filter applied on GICode which contains "00001" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page