# Author: Ahmad Hamed & Engy Mohamed (EBS-5572)
# Reviewer: Somaya Ahmed

Feature: GoodsIssue - Activate (Auth)

  Background:

    Given the following users and roles exist:
      | Name                 | Role                         |
      | Mahmoud.Abdelaziz    | Storekeeper_Signmedia        |
      | Ali.Hasan.MadinaMain | Storekeeper_Flexo_MadinaMain |

    And the following roles and sub-roles exist:
      | Role                         | Subrole                          |
      | Storekeeper_Signmedia        | GIActivateOwner_Signmedia        |
      | Storekeeper_Flexo_MadinaMain | GIActivateOwner_Flexo_MadinaMain |

    And the following sub-roles and permissions exist:
      | Subrole                          | Permission          | Condition                                               |
      | GIActivateOwner_Signmedia        | GoodsIssue:Activate | [purchaseUnitName='Signmedia']                          |
      | GIActivateOwner_Flexo_MadinaMain | GoodsIssue:Activate | ([purchaseUnitName='Flexo'] && [storehouseCode='0001']) |

    And the following users have the following permissions without the following conditions:
      | User                 | Permission          | Condition                                               |
      | Mahmoud.Abdelaziz    | GoodsIssue:Activate | [purchaseUnitName='Flexo']                              |
      | Ali.Hasan.MadinaMain | GoodsIssue:Activate | ([purchaseUnitName='Flexo'] && [storehouseCode='0003']) |

    And the following GeneralData for GoodsIssues exist:
      | GICode     | Type                                                    | CreatedBy | State | CreationDate         | Storekeeper              |
      | 2020000004 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | Admin     | Draft | 05-Aug-2020 09:00 AM | 0007 - Mahmoud.Abdelaziz |

    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company        | Storehouse                | BusinessUnit |
      | 2020000004 | 0002 - DigiPro | 0003 - DigiPro Main Store | 0001 - Flexo |


  # EBS-5572 # EBS-8643
  Scenario Outline: (01)  Activate GoodsIssue Auth - in state draft by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" Activates GoodsIssue with code "2020000004" at "20-Nov-2020 09:30 AM"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                 |
      | Mahmoud.Abdelaziz    |
      | Ali.Hasan.MadinaMain |
