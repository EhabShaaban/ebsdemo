Feature: Request to edit and cancel SalesInvoice section in GoodsIssue

  Background:
    Given the following users and roles exist:
      | Name                                                                   | Role                                                                              |
      | hr1                                                                    | SuperUser                                                                         |
      | Ahmed.Al-Ashry                                                         | SalesSpecialist_Signmedia                                                         |
      | Rabie.abdelghafar                                                      | SalesSpecialist_Flexo                                                             |
      | Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative | SalesSpecialist_Signmedia_NoCustomerNoAddressNoContactPersonNoSalesRepresentative |
    And the following roles and sub-roles exist:
      | Role                                                                              | Sub-role                  |
      | SuperUser                                                                         | SuperUserSubRole          |
      | SalesSpecialist_Signmedia                                                         | GIOwner_Signmedia         |
      | SalesSpecialist_Signmedia                                                         | CustomerViewer_Signmedia  |
      | SalesSpecialist_Signmedia                                                         | SalesRepresentativeViewer |
      | SalesSpecialist_Flexo                                                             | GIOwner_Flexo             |
      | SalesSpecialist_Signmedia_NoCustomerNoAddressNoContactPersonNoSalesRepresentative | GIOwner_Signmedia         |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                        | Condition                      |
      | SuperUserSubRole          | *:*                               |                                |
      | GIOwner_Signmedia         | GoodsIssue:UpdateSalesInvoiceData | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia  | Customer:ReadAll                  | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia  | Customer:ReadAddress              | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia  | Customer:ReadContactPerson        | [purchaseUnitName='Signmedia'] |
      | SalesRepresentativeViewer | SalesRepresentative:ReadAll       |                                |
      | GIOwner_Flexo             | GoodsIssue:UpdateSalesInvoiceData | [purchaseUnitName='Flexo']     |
    And the following users doesn't have the following permissions:
      | User                                                                   | Permission                  |
      | Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative | Customer:ReadAll            |
      | Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative | Customer:ReadAddress        |
      | Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative | Customer:ReadContactPerson  |
      | Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative | SalesRepresentative:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User              | Permission                        | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:UpdateSalesInvoiceData | [purchaseUnitName='Signmedia'] |

    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State  |
      | 2019000002 | 0002 - Signmedia | Draft  |
      | 2019000003 | 0002 - Signmedia | Active |
    And the following SalesInvoiceData for GoodsIssues exist:
      | GICode     | Customer          | Address            | ContactPerson | SalesOrder | SalesRepresentative        | KAP         | Comments             |
      | 2019000002 | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 1111       | 0001 - SeragEldin Meghawry | 007 - Fakhr | For GI No 2019000002 |
      | 2019000003 | 000001 - Al Ahram | 66, Salah Salem St |               | 1111       | 0001 - SeragEldin Meghawry | 007 - Fakhr |                      |
    And edit session is "30" minutes

  ### Request to Edit SalesInvoice section ###

  Scenario:(01) Request to edit SalesInvoice section by an authorized user with one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to edit the SalesInvoice section of GoodsIssue with code "2019000002"
    Then SalesInvoice section of GoodsIssue with code "2019000002" becomes in edit mode and locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads         |
      | ReadSalesRepresentative |
      | ReadContactPerson       |
      | ReadCustomers           |
      | ReadAddress             |
    And there are no mandatory fields returned to "Ahmed.Al-Ashry"
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields          |
      | salesRepresentativeCode |
      | contactPersonName       |
      | customerCode            |
      | salesOrder              |
      | comments                |
      | address                 |

  Scenario:(02) Request to edit SalesInvoice section that is locked by another user (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first opened the SalesInvoice section of GoodsIssue with code "2019000002" in the edit mode successfully
    When "Ahmed.Al-Ashry" requests to edit the SalesInvoice section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-02"

  Scenario:(03) Request to edit SalesInvoice section of deleted GoodsIssue (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000003" successfully
    When "Ahmed.Al-Ashry" requests to edit the SalesInvoice section of GoodsIssue with code "2019000003"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  @Future
  Scenario: (04) Request to edit SalesInvoice section when it is not allowed in current state: Active (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    When "hr1" first Activated GoodsIssue with code "2019000002" at "07-Jan-2019 09:30 AM" successfully with the following values:
  ???
    And "Ahmed.Al-Ashry" requests to edit the SalesInvoice section of GoodsIssue with code "2019000002" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-03"
    And SalesInvoice section of GoodsIssue with code "2019000002" is not locked by "Ahmed.Al-Ashry"

  Scenario:(05) Request to edit the SalesInvoice section with unauthorized user (with condition - without condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to edit the SalesInvoice section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  Scenario: (06) Request to edit SalesInvoice section  (No authorized reads)
    Given user is logged in as "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative"
    When "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative" requests to edit the SalesInvoice section of GoodsIssue with code "2019000002"
    Then SalesInvoice section of GoodsIssue with code "2019000002" becomes in edit mode and locked by "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative"
    And there are no authorized reads returned to "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative"
    And there are no mandatory fields returned to "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative"
    And the following editable fields are returned to "Ahmed.Al-Ashry.NoCustomerNoAddressNoContactPersonNoSalesRepresentative":
      | EditablleFields         |
      | salesRepresentativeCode |
      | contactPersonName       |
      | customerCode            |
      | salesOrder              |
      | comments                |
      | address                 |

  ### Request to Cancel SalesInvoice section ###

  Scenario:(10) Request to Cancel saving SalesInvoice section by an authorized user with one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesInvoice section of GoodsIssue with code "2019000002" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:10 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesInvoice section of GoodsIssue with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Ahmed.Al-Ashry" on SalesInvoice section of GoodsIssue with code "2019000002" is released

  Scenario:(11) Request to Cancel saving SalesInvoice section after lock session is expire
    Given user is logged in as "Ahmed.Al-Ashry"
    And SalesInvoice section of GoodsIssue with code "2019000002" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesInvoice section of GoodsIssue with code "2019000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"

  Scenario: (12) Request to Cancel saving SalesInvoice section after lock session is expire and GoodsIssue doesn't exsit (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And SalesInvoice section of GoodsIssue with code "2019000003" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the GoodsIssue with code "2019000003" successfully at "07-Jan-2019 09:31 AM"
    When "Ahmed.Al-Ashry" cancels saving SalesInvoice section of GoodsIssue with code "2019000003" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario:(13) Request to Cancel saving SalesInvoice section with unauthorized user (with condition - without condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" cancels saving SalesInvoice section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page


