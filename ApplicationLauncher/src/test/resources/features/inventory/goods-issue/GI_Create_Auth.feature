# Author: Hossam Hassan, Ahmed Hamed, Engy Mohamed (EBS-6757)
# Reviewer: Somaya Ahmed
Feature: Create GoodsIssue - Authorization

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Afaf           | FrontDesk                 |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | CreateOnlyUser | CreateOnlyRole            |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                         |
      | SalesSpecialist_Signmedia | GIOwner_Signmedia               |
      | SalesSpecialist_Signmedia | PurUnitReader_Signmedia         |
      | SalesSpecialist_Signmedia | CompanyViewer                   |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia        |
      | SalesSpecialist_Signmedia | StorehouseViewer                |
      | SalesSpecialist_Signmedia | StorekeeperViewer               |
      | SalesSpecialist_Signmedia | GoodsIssueTypeViewer            |
      | SalesSpecialist_Signmedia | SOViewer_LoggedInUser_Signmedia |
      | CreateOnlyRole            | CreateOnlySubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                   | Condition                                                                 |
      | GIOwner_Signmedia               | GoodsIssue:Create            |                                                                           |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']                                            |
      | CompanyViewer                   | Company:ReadAll              |                                                                           |
      | CustomerViewer_Signmedia        | Customer:ReadAll             | [purchaseUnitName='Signmedia']                                            |
      | StorehouseViewer                | Storehouse:ReadAll           |                                                                           |
      | StorekeeperViewer               | Storekeeper:ReadAll          |                                                                           |
      | GoodsIssueTypeViewer            | GoodsIssueType:ReadAll       |                                                                           |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll           | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | CreateOnlySubRole               | GoodsIssue:Create            |                                                                           |

    And the following users doesn't have the following permissions:
      | User           | Permission             |
      | CreateOnlyUser | GoodsIssueType:ReadAll |
      | CreateOnlyUser | PurchasingUnit:ReadAll |
      | CreateOnlyUser | Customer:ReadAll       |
      | CreateOnlyUser | Company:ReadAll        |
      | CreateOnlyUser | Storehouse:ReadAll     |
      | CreateOnlyUser | Storekeeper:ReadAll    |
      | CreateOnlyUser | SalesOrder:ReadAll     |

    And the following users have the following permissions without the following conditions:
      | User           | Permission                   | Condition                                                             |
      | Ahmed.Al-Ashry | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']                                            |
      | Ahmed.Al-Ashry | Customer:ReadAll             | [purchaseUnitName='Flexo']                                            |
      | Ahmed.Al-Ashry | SalesOrder:ReadAll           | ([purchaseUnitName='Flexo'] && [salesResponsibleName='LoggedInUser']) |

    And the following GoodsIssue Types exist:
      | Code                 | Name                             |
      | BASED_ON_SALES_ORDER | Goods Issue Based On Sales Order |

    And the following Storekeepers exist:
      | Code | Name              |
      | 0007 | Mahmoud.Abdelaziz |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |

    And the following Plants are related to Company with code "0001":
      | Code |
      | 0001 |

    And the following Storehouses for Plant with code "0001" exist:
      | Code |
      | 0001 |
      | 0002 |

    And the following Storehouses for Plant with code "0002" exist:
      | Code |
      | 0003 |
      | 0004 |

    And the following Plants are related to Company with code "0002":
      | Code |
      | 0002 |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000006 | المطبعة الأمنية       | 0001 - Flexo     |

    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible           | CreatedBy                 | State                 | CreationDate         |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | Approved              | 17-Aug-2020 12:37 PM |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | SalesInvoiceActivated | 03-Aug-2020 01:31 PM |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry      | Admin from BDKCompanyCode | Approved              | 04-Feb-2020 08:39 AM |

    And the following CompanyAndStoreData for the following SalesOrders exist:

      | SOCode     | Company          | Store                      |
      | 2020000069 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000010 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000003 | 0002 - DigiPro   | 0003 - DigiPro Main Store  |

  # EBS-6757
  Scenario: (01) Create GoodsIssue - by an authorized user with authorized reads
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to create GoodsIssue
    Then the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads   |
      | ReadGITypes       |
      | ReadStorehouses   |
      | ReadStorekeepers  |
      | ReadCompanies     |
      | ReadBusinessUnits |
      | ReadCustomers     |
      | ReadSalesOrders   |

  # EBS-6757
  Scenario: (02) Create GoodsIssue - by an authorized user with no authorized reads
    Given user is logged in as "CreateOnlyUser"
    When "CreateOnlyUser" requests to create GoodsIssue
    Then there are no authorized reads returned to "CreateOnlyUser"

  # EBS-6757
  Scenario: (03) Create GoodsIssue - by an unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create GoodsIssue
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  # EBS-6757
  Scenario Outline: (04) Create GoodsIssue - by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" creates GoodsIssue with the following values:
      | Type                 | PurchaseUnit   | RefDocument   | Company   | Storehouse   | Storekeeper              |
      | BASED_ON_SALES_ORDER | <PurchaseUnit> | <RefDocument> | <Company> | <Storehouse> | 0007 - Mahmoud.Abdelaziz |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User           | PurchaseUnit     | RefDocument | Company         | Storehouse                 |
      | CreateOnlyUser | 0002 - Signmedia | 2020000069  | 0001 - AlMadina | 0001 - AlMadina Main Store |
      | Ahmed.Al-Ashry | 0001 - Flexo     | 2020000069  | 0001 - AlMadina | 0001 - AlMadina Main Store |
      | Ahmed.Al-Ashry | 0002 - Signmedia | 2020000003  | 0002 - DigiPro  | 0003 - DigiPro Main Store  |
      # LoggedInUser is not authorized to view RefDocument
      | Ahmed.Al-Ashry | 0002 - Signmedia | 2020000010  | 0001 - AlMadina | 0001 - AlMadina Main Store |
