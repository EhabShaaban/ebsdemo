#Author: Niveen Magdy
#Reviewer:

Feature: View All GI types (Dropdown)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |

    And the following roles and sub-roles exist:
      | Role                  | Sub-role             |
      | Storekeeper_Signmedia | GoodsIssueTypeViewer |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition |
      | GoodsIssueTypeViewer | GoodsIssueType:ReadAll |           |

    And the following GoodsIssue Types exist:
      | Code                 | Name                             |
      | DELIVERY_TO_CUSTOMER | Deliver To Customer              |
      | BASED_ON_SALES_ORDER | Goods Issue Based On Sales Order |
    And the total number of existing GoodsIssue Types are 2

    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | GoodsIssueType:ReadAll |

    #EBS-6764
  Scenario: (01) Read list of GoodsIsuueTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all GoodsIssueTypes
    Then the following GoodsIssue Types values will be presented to "Mahmoud.Abdelaziz":
      | Code                 |
      | DELIVERY_TO_CUSTOMER |
      | BASED_ON_SALES_ORDER |
    And total number of GoodsIssue Types returned to "Mahmoud.Abdelaziz" is equal to 2

   #EBS-6764
  Scenario: (02) Read list of GoodsIsuueTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all GoodsIssueTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
