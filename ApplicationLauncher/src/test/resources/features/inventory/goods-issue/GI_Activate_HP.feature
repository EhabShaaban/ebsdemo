# Author: Ahmed Hamed, Engy Mohamed (EBS-6758)
# Reviewer: Somaya Ahmed (07-Jul-2020)

Feature: Goods Issue - Activate

  Activating GI has the following effects:
  1- Update the state, update details, and activation details:
  2- Works based on FIFO
  2.1- Add new StoreTransactions records (TAKE)
  2.2- Update existing (ADD) StoreTransactions records that have been affected by the added StoreTransactions records (TAKE) in step (2)

  Background:
    Given the following users and roles exist:
      | Name                         | Role                             |
      | Mahmoud.Abdelaziz            | Storekeeper_Signmedia            |
      | Mahmoud.Abdelaziz.MadinaMain | Storekeeper_Signmedia_MadinaMain |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                              |
      | Storekeeper_Signmedia            | GIActivateOwner_Signmedia            |
      | Storekeeper_Signmedia_MadinaMain | GIActivateOwner_Signmedia_MadinaMain |
    And the following sub-roles and permissions exist:
      | Subrole                              | Permission          | Condition                                                   |
      | GIActivateOwner_Signmedia            | GoodsIssue:Activate | [purchaseUnitName='Signmedia']                              |
      | GIActivateOwner_Signmedia_MadinaMain | GoodsIssue:Activate | ([purchaseUnitName='Signmedia'] && [storehouseCode='0001']) |

    And the following GeneralData for GoodsIssues exist:
      | GICode     | Type                                                    | CreatedBy                 | State | CreationDate         | Storekeeper              |
      | 2019000010 | DELIVERY_TO_CUSTOMER - Deliver To Customer              | Admin from BDKCompanyCode | Draft | 01-Jan-2019 09:02 AM | 0007 - Mahmoud.Abdelaziz |
      | 2020000001 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | Admin                     | Draft | 06-Nov-2020 09:00 AM | 0007 - Mahmoud.Abdelaziz |
      | 2020000005 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | A.Hamed                   | Draft | 09-Nov-2020 09:00 AM | 0007 - Mahmoud.Abdelaziz |
      | 2020000010 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft | 06-Aug-2020 11:35 AM | 0007 - Mahmoud.Abdelaziz |

    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company          | Storehouse                 | BusinessUnit     |
      | 2019000010 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000010 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

    And the following SalesInvoiceData for GoodsIssues exist:
      | GICode     | Customer         | Address            | ContactPerson | RefDocument | SalesRepresentative | KAP               | Comments             |
      | 2019000010 | 000002 - Client2 | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 0011 - Mohamedeen | For GI No 2019000010 |

    And the following SalesOrderData for GoodsIssues exist:
      | GICode     | Customer                       | Address                     | ContactPerson | RefDocument | SalesRepresentative | Comments |
      | 2020000001 | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed     | Wael Fathay   | 2020000053  | Ahmed.Al-Ashry      |          |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay   | 2020000067  | Ahmed.Al-Ashry      |          |
      | 2020000010 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay   | 2020000010  | SeragEldin Meghawry |          |

    And the following ItemData for GoodsIssues exist:
      | GICode     | Item                                          | UOM                 | Quantity | BaseUnit  | QtyBase | BatchNo | Price |
      | 2019000010 | 000051 - Ink5                                 | 0014 - Liter        | 14       | 0019 - M2 | 14      |         | 22.0  |
      | 2020000001 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 2        | 0019 - M2 | 127     |         | 10.0  |
      | 2020000005 | 000051 - Ink5                                 | 0014 - Liter        | 2        | 0019 - M2 | 2       |         | 2.0   |
      | 2020000005 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 5        | 0019 - M2 | 317.5   |         | 2.0   |
      | 2020000010 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 10       | 0019 - M2 | 10      |         | 25.0  |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved              |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 30-Jun-2020 09:00 AM | 0002 - Signmedia | Approved              |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 31-Aug-2020 12:00 AM | 0002 - Signmedia | SalesInvoiceActivated |

    And ONLY the following StoreTransaction records for Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | STCode     | OriginalAddingDate   | CreationDate          | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument          | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000002 | 05-Feb-2020 03:48 PM | 05-Feb-2020 03:48 PM  | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 100 | 20           | ADD             | 20         | Goods Receipt - 2020000001 | 0            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000004 | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM  | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 20  | 0            | ADD             | 0          | Goods Receipt - 2020000003 | 19           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000022 |                      | 29-June-2020 11:08 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 2   |              | TAKE            |            | Goods Issue - 2020000003   | 0            | 2020000002     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000023 |                      | 27-July-2020 03:52 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 98  |              | TAKE            |            | Goods Issue - 2020000006   | 0            | 2020000002     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000024 |                      | 27-July-2020 03:52 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 1   |              | TAKE            |            | Goods Issue - 2020000006   | 0            | 2020000004     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And the total number of StoreTransaction records for Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" is 5

    And ONLY the following StoreTransaction records for Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | STCode     | OriginalAddingDate   | CreationDate          | StockType        | PurchaseUnit     | Item          | UOM          | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument                 | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2019000014 | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM  | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 13  | 100          | ADD             | 100        | Goods Receipt - 2019000020        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000019 | 07-Jan-2019 09:33 AM | 07-Jan-2019 09:33 AM  | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 8   | 120          | ADD             | 120        | Goods Receipt - 2019000023        | 6            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000017 |                      | 03-Nov-2020 07:41 AM  | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 2   |              | TAKE            |            | Stock Transformation - 2020000048 | 0            | 2019000019     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000018 | 03-Nov-2020 07:41 AM | 03-Nov-2020 07:41 AM  | DAMAGED_STOCK    | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 2   | 120          | ADD             | 120        | Stock Transformation - 2020000048 | 2            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000019 | 07-Dec-2020 09:30 AM | 07-Dec-2020 09:30 AM  | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 10  | 120          | ADD             | 120        | Goods Receipt - 2019000021        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000025 |                      | 27-July-2020 03:52 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 2   |              | TAKE            |            | Goods Issue - 2020000006          | 0            | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000029 |                      | 06-Aug-2020 12:01 PM  | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 1   |              | TAKE            |            | Goods Issue - 2020000011          | 0            | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And the total number of StoreTransaction records for Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" is 7

    And ONLY the following StoreTransaction records for Item "000053" with UOM "0019" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | STCode     | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM       | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000001 | 05-Feb-2020 03:48 PM | 05-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 150 | 0            | ADD             | 0          | 2020000001        | 100          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000005 | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 10  | 0            | ADD             | 0          | 2020000003        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000027 |                      | 05-Aug-2020 01:44 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 10  |              | TAKE            |            | 2020000008        | 0            | 2020000001     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000028 |                      | 05-Aug-2020 01:45 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 10  |              | TAKE            |            | 2020000009        | 0            | 2020000001     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And the total number of StoreTransaction records for Item "000053" with UOM "0019" in company "0001", in storehouse "0001", for BusinessUnit "0002" is 4

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 19.0              |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 26.0              |
      | DAMAGED_STOCK00020001000100010000510014    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | DAMAGED_STOCK    | 2.0               |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | UNRESTRICTED_USE | 110.0             |

  #EBS-5574 #EBS-5573 #EBS-8643
  Scenario Outline: (01) Activate GoodsIssue HP - (Based on SalesInvoice) with FIFO when items quantities LESS THAN store quantities by an authorized user (Happy Path)

  This scenario is testing the following:
  1- Only one item in the GI
  2- Issuing the Item from 2 different store transaction records
  3- Qty issued is less than quantity remaining

    Given user is logged in as "<User>"
    And Last created store transaction was with code "2020000033"
    And the remaining "UNRESTRICTED_USE" qty of for Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | RemainingInStore |
      | 26.0             |
    When "<User>" Activates GoodsIssue with code "2019000010" at "10-Jan-2020 09:30 AM"
    Then GoodsIssue with Code "2019000010" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | <User> | 10-Jan-2020 09:30 AM | Active |
    And ActivationDetails in GoodsIssue with Code "2019000010" is updated as follows:
      | ActivationDate       | ActivatedBy       |
      | 10-Jan-2020 09:30 AM | <User> |
    And the following new StoreTransaction records are added as follows:
      | StoreTransactionCode | CreatedBy         | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item          | UOM          | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | <User> |                    | 10-Jan-2020 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 10.0 |              | TAKE            |            | 2019000010        | 0.0          | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000035           | <User> |                    | 10-Jan-2020 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 4.0  |              | TAKE            |            | 2019000010        | 0.0          | 2019000019     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records are updated as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item          | UOM          | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2019000019           | 07-Jan-2019 09:33 AM | 07-Jan-2019 09:33 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 8   | 120          | ADD             | 120        | 2019000023        | 2.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000014           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0014 - Liter |         | 13  | 100          | ADD             | 100        | 2019000020        | 0.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records remains as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item          | UOM                  | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument           | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2019000016           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5 | 0040 - Bottle-1Liter |         | 2.0  | 100.0        | ADD             | 100.0      | 2019000020                  | 1.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000017           | 07-Jan-2019 09:31 AM | 07-Jan-2019 09:31 AM | UNRESTRICTED_USE | 0001 - Flexo     | 000051 - Ink5 | 0014 - Liter         |         | 8.0  | 120.0        | ADD             | 120.0      | Legacy System نظام المتكامل | 3.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000018           | 07-Jan-2019 09:32 AM | 07-Jan-2019 09:32 AM | UNRESTRICTED_USE | 0001 - Flexo     | 000051 - Ink5 | 0014 - Liter         |         | 10.0 | 120.0        | ADD             | 120.0      | Legacy System نظام المتكامل | 3.0          |                | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store  |
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item          | UOM          | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0014 - Liter | UNRESTRICTED_USE | 12.0              |
    And a success notification is sent to "<User>" with the following message "GI-MSG-03"
    Examples:
      | User                         |
      | Mahmoud.Abdelaziz            |
      | Mahmoud.Abdelaziz.MadinaMain |

  #EBS-6758
  Scenario: (02) Activate GoodsIssue HP - (Based on SalesOrder) with with FIFO when quantities less than store quantities by an authorized user (1 Commercial Item) (Happy Path)

  This scenario is testing the following:
  1- Only one item in the GI
  2- Issuing the Item from only 1 transaction records
  3- Qty issued is less than quantity remaining

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    And the remaining "UNRESTRICTED_USE" qty of for Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | RemainingInStore |
      | 19.0             |
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2020000001" at "06-Feb-2020 03:48 PM"
    Then GoodsIssue with Code "2020000001" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 06-Feb-2020 03:48 PM | Active |
    And ActivationDetails in GoodsIssue with Code "2020000001" is updated as follows:
      | ActivationDate       | ActivatedBy       |
      | 06-Feb-2020 03:48 PM | Mahmoud.Abdelaziz |
    And the following new StoreTransaction records are added as follows:
      | StoreTransactionCode | CreatedBy         | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | Mahmoud.Abdelaziz |                    | 06-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 2.0 |              | TAKE            |            | 2020000001        | 0.0          | 2020000004     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records are updated as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000004           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 20  | 0            | ADD             | 0          | 2020000003        | 17.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And The State of SalesOrder with code "2020000053" is changed to "GoodsIssueActivated"
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 17.0              |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-MSG-03"

  #EBS-6758
  Scenario: (03) Activate GoodsIssue HP - (Based on SalesOrder) with items quantities equal store quantities by an authorized user (Mixed Items [Commercial, Consumable, Service] ) (Happy Path)

  This scenario is testing the following:
  1- Multiple Items the GI
  2- Issuing from only 1 transaction records for each Item
  3- Qty issued is EQUAL quantity remaining

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    And the remaining "UNRESTRICTED_USE" qty of for Item "000051" with UOM "0014" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | RemainingInStore |
      | 26.0             |
    And the remaining "UNRESTRICTED_USE" qty of for Item "000053" with UOM "0035" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | RemainingInStore |
      | 19.0             |
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2020000005" at "10-Jan-2020 09:30 AM"
    Then GoodsIssue with Code "2020000005" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 10-Jan-2020 09:30 AM | Active |
    And ActivationDetails in GoodsIssue with Code "2020000005" is updated as follows:
      | ActivationDate       | ActivatedBy       |
      | 10-Jan-2020 09:30 AM | Mahmoud.Abdelaziz |
    And the following new StoreTransaction records are added as follows:
      | StoreTransactionCode | CreatedBy         | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | Mahmoud.Abdelaziz |                    | 10-Jan-2020 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 5.0 |              | TAKE            |            | 2020000005        | 0.0          | 2020000004     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2020000035           | Mahmoud.Abdelaziz |                    | 10-Jan-2020 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0014 - Liter        |         | 2.0 |              | TAKE            |            | 2020000005        | 0.0          | 2019000014     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records are updated as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000004           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 |         | 20  | 0            | ADD             | 0          | 2020000003        | 14.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
      | 2019000014           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0014 - Liter        |         | 13  | 100.0        | ADD             | 100.0      | 2019000020        | 8.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records remains as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                  | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument           | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                      |
      | 2020000005           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            |         | 10   | 0            | ADD             | 0          | 2020000003                  | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      |
      | 2020000003           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            |         | 100  | 0            | ADD             | 0          | 2020000002                  | 100          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store |
      | 2020000001           | 05-Feb-2020 03:48 PM | 05-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            |         | 150  | 0            | ADD             | 0          | 2020000001                  | 100          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      |
      | 2019000019           | 07-Jan-2019 09:33 AM | 07-Jan-2019 09:33 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0014 - Liter         |         | 8    | 120          | ADD             | 120        | 2019000023                  | 6            |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      |
      | 2019000018           | 07-Jan-2019 09:32 AM | 07-Jan-2019 09:32 AM | UNRESTRICTED_USE | 0001 - Flexo     | 000051 - Ink5                                 | 0014 - Liter         |         | 10.0 | 120.0        | ADD             | 120.0      | Legacy System نظام المتكامل | 3.0          |                | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       |
      | 2019000017           | 07-Jan-2019 09:31 AM | 07-Jan-2019 09:31 AM | UNRESTRICTED_USE | 0001 - Flexo     | 000051 - Ink5                                 | 0014 - Liter         |         | 8.0  | 120.0        | ADD             | 120.0      | Legacy System نظام المتكامل | 3.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      |
      | 2019000016           | 07-Jan-2019 09:30 AM | 07-Jan-2019 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000051 - Ink5                                 | 0040 - Bottle-1Liter |         | 2.0  | 100.0        | ADD             | 100.0      | 2019000020                  | 1.0          |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      |
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                 | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | UNRESTRICTED_USE | 14.0              |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0014 - Liter        | UNRESTRICTED_USE | 24.0              |
    And The State of SalesOrder with code "2020000067" is changed to "GoodsIssueActivated"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-MSG-03"

     #EBS-5718
  Scenario: (04) Activate GoodsIssue HP - (Based on SalesOrder) with with FIFO when quantities less than store quantities by an authorized user (1 Commercial Item) (Happy Path)

  This scenario is testing the following:
  1- Multiple Items the GI
  2- Issuing from only 1 transaction records for each Item
  3- Qty issued is EQUAL quantity remaining

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last created store transaction was with code "2020000033"
    And the remaining "UNRESTRICTED_USE" qty of for Item "000053" with UOM "0019" in company "0001", in storehouse "0001", for BusinessUnit "0002" exist:
      | RemainingInStore |
      | 110.0            |
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2020000010" at "10-Jan-2020 09:30 AM"
    Then GoodsIssue with Code "2020000010" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 10-Jan-2020 09:30 AM | Active |
    And ActivationDetails in GoodsIssue with Code "2020000010" is updated as follows:
      | ActivationDate       | ActivatedBy       |
      | 10-Jan-2020 09:30 AM | Mahmoud.Abdelaziz |
    And the following new StoreTransaction records are added as follows:
      | StoreTransactionCode | CreatedBy         | OriginalAddingDate | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM                 | BatchNo | Qty  | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000034           | Mahmoud.Abdelaziz |                    | 10-Jan-2020 09:30 AM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter |         | 10.0 |              | TAKE            |            | 2020000010        | 0.0          | 2020000001     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records are updated as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM       | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000001           | 05-Feb-2020 03:48 PM | 05-Feb-2020 03:48 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 150 | 0.0          | ADD             | 0.0        | 2020000001        | 90.0         |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And then the following StoreTransaction records remains as follows:
      | StoreTransactionCode | OriginalAddingDate   | CreationDate         | StockType        | PurchaseUnit     | Item                                          | UOM       | BatchNo | Qty | EstimateCost | TransactionType | ActualCost | ReferenceDocument | RemainingQty | RefTransaction | Company          | Plant                    | Storehouse                 |
      | 2020000005           | 05-Feb-2020 03:49 PM | 05-Feb-2020 03:49 PM | UNRESTRICTED_USE | 0002 - Signmedia | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 |         | 10  | 0            | ADD             | 0          | 2020000003        | 10           |                | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store |
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM       | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | UNRESTRICTED_USE | 100.0             |
    And The State of SalesOrder with code "2020000010" is changed to "ReadyForDelivery"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-MSG-03"

