Feature: View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role           |
      | SalesSpecialist_Signmedia | GIViewer_Signmedia |
      | SalesSpecialist_Flexo     | GIViewer_Flexo     |
      | SuperUser                 | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                 | Condition                      |
      | GIViewer_Signmedia | GoodsIssue:ReadGeneralData | [purchaseUnitName='Signmedia'] |
      | GIViewer_Flexo     | GoodsIssue:ReadGeneralData | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                        |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                 | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:ReadGeneralData | [purchaseUnitName='Signmedia'] |
    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company          | Storehouse                 | BusinessUnit     |
      | 2019000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
    And the following GeneralData for GoodsIssues exist:
      | GICode     | Type                                       | CreatedBy                 | State | CreationDate         | Storekeeper              |
      | 2019000002 | DELIVERY_TO_CUSTOMER - Deliver To Customer | Admin from BDKCompanyCode | Draft | 05-Aug-2019 09:02 AM | 0007 - Mahmoud.Abdelaziz |

  # EBS-4473
  Scenario: (01) View GI GeneralData section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view GeneralData section of GoodsIssue with code "2019000002"
    Then the following values of GeneralData section for GoodsIssue with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | GICode     | Type                | PurchasingUnit | CreatedBy                 | State | CreationDate         | Storekeeper       |
      | 2019000002 | Deliver To Customer | Signmedia      | Admin from BDKCompanyCode | Draft | 05-Aug-2019 09:02 AM | Mahmoud.Abdelaziz |

  # EBS-5438
  Scenario: (02) View GI GeneralData section - where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to view GeneralData section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  # EBS-5438
  Scenario: (03) View GI GeneralData section - by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view GeneralData section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
