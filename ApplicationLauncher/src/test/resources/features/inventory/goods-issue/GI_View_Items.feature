Feature: View Items section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role           |
      | SalesSpecialist_Signmedia | GIViewer_Signmedia |
      | SalesSpecialist_Flexo     | GIViewer_Flexo     |
      | SuperUser                 | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission          | Condition                      |
      | GIViewer_Signmedia | GoodsIssue:ReadItem | [purchaseUnitName='Signmedia'] |
      | GIViewer_Flexo     | GoodsIssue:ReadItem | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                 |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission          | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:ReadItem | [purchaseUnitName='Signmedia'] |
    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State |
      | 2019000002 | 0002 - Signmedia | Draft |
      | 2020000001 | 0002 - Signmedia | Draft |
    And the following ItemData for GoodsIssues exist:
      | GICode     | Item                                          | UOM                 | Quantity | BaseUnit  | QtyBase | BatchNo | Price |
      | 2019000002 | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2           | 100.0    | 0019 - M2 | 100.0   |         |       |
      | 2019000002 | 000008 - PRO-V-ST-2                           | 0030 - PC           | 136.0    | 0019 - M2 | 136.0   |         |       |
      | 2019000002 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 500.0    | 0019 - M2 | 500.0   |         |       |
      | 2020000001 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 2.0      | 0019 - M2 | 127.0   |         | 10    |

  # EBS-5437
  Scenario: (01) View GI ItemData section - based on sales invoice by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Item section of GoodsIssue with code "2019000002"
    Then the following values of Item section for GoodsIssue with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | ItemCode | Item                                 | UOM       | Quantity | BatchNo | BaseUnit  | QtyBase |
      | 000008   | PRO-V-ST-2                           | 0030 - PC | 136.0    |         | 0019 - M2 | 136.0   |
      | 000053   | Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 500.0    |         | 0019 - M2 | 500.0   |
      | 000054   | Hot Laminated Frontlit Fabric roll 3 | 0019 - M2 | 100.0    |         | 0019 - M2 | 100.0   |

  # EBS-5437
  Scenario: (02) View GI ItemData section - based on sales invoice by unauthorized user due to condition (Abuse error)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view Item section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

     # EBS-6761
  Scenario: (03) View GI ItemData section - based on sales order by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Item section of GoodsIssue with code "2020000001"
    Then the following values of Item section for GoodsIssue with code "2020000001" are displayed to "Ahmed.Al-Ashry":
      | ItemCode | Item                                 | UOM                 | Quantity | BaseUnit  | QtyBase | BatchNo |
      | 000053   | Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 2.0      | 0019 - M2 | 127.0   |         |

      # EBS-5437
  Scenario: (04) View GI ItemData section - based on sales invoice based on sales invoice where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to view Item section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
