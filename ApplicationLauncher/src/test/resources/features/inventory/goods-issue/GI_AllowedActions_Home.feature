Feature: Allowed Action For Goods Issue (Home Screen)

  Background:
    Given the following users and roles exist:
      | Name                                                                | Role                                                            |
      | Afaf                                                                | FrontDesk                                                       |
      | Ahmed.Al-Ashry                                                      | SalesSpecialist_Signmedia                                       |
      | Ashraf.Fathi                                                        | M.D                                                             |
      | CreateOnlyUser                                                      | CreateOnlyRole                                                  |
      | canReadGoodsIssuesCanReadBusinessUnitsCannotReadGoodsIssueTypesUser | canReadGoodsIssuesCanReadBusinessUnitsCannotReadGoodsIssueTypes |
    And the following roles and sub-roles exist:
      | Role                                                            | Subrole                 |
      | M.D                                                             | GIViewer                |
      | M.D                                                             | PurUnitReader_Signmedia |
      | M.D                                                             | GoodsIssueTypeViewer    |
      | canReadGoodsIssuesCanReadBusinessUnitsCannotReadGoodsIssueTypes | GIViewer                |
      | canReadGoodsIssuesCanReadBusinessUnitsCannotReadGoodsIssueTypes | PurUnitReader_Signmedia |
      | SalesSpecialist_Signmedia                                       | GIViewer_Signmedia      |
      | SalesSpecialist_Signmedia                                       | GIOwner_Signmedia       |
      | SalesSpecialist_Signmedia                                       | PurUnitReader_Signmedia |
      | SalesSpecialist_Signmedia                                       | GoodsIssueTypeViewer    |
      | CreateOnlyRole                                                  | CreateOnlySubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | GIViewer_Signmedia      | GoodsIssue:ReadAll           | [purchaseUnitName='Signmedia'] |
      | GIOwner_Signmedia       | GoodsIssue:Create            |                                |
      | GIViewer                | GoodsIssue:ReadAll           |                                |
      | GoodsIssueTypeViewer    | GoodsIssueType:ReadAll       |                                |
      | CreateOnlySubRole       | GoodsIssue:Create            |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll       |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                                                                | Permission             |
      | Afaf                                                                | GoodsIssue:ReadAll     |
      | Afaf                                                                | GoodsIssue:Create      |
      | canReadGoodsIssuesCanReadBusinessUnitsCannotReadGoodsIssueTypesUser | GoodsIssueType:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User         | Permission         | Condition                      |
      | Ashraf.Fathi | GoodsIssue:ReadAll | [purchaseUnitName='Signmedia'] |

  Scenario: (01) Read allowed actions in Goods Issue Home Screen - User is authorized to Create & ReadAll actions and authorized to ReadAll dropdowns (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read Authorized actions and Reads of GoodsIssue in home screen
    Then the following actions are displayed to "Ahmed.Al-Ashry":
      | AllowedActions      |
      | ReadAll             |
      | Create              |
      | ReadBusinessUnits   |
      | ReadGoodsIssueTypes |

  Scenario: (02) Read allowed actions in Goods Issue Home Screen - User is authorized to ReadAll Action only and authorized to ReadAll dropdowns (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read Authorized actions and Reads of GoodsIssue in home screen
    Then the following actions are displayed to "Ashraf.Fathi":
      | AllowedActions      |
      | ReadAll             |
      | ReadBusinessUnits   |
      | ReadGoodsIssueTypes |

  Scenario: (03) Read allowed actions in Goods Issue Home Screen - User is authorized ReadAll Action only but not authorized to Read One of the dropdowns (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read Authorized actions and Reads of GoodsIssue in home screen
    Then the following actions are displayed to "Ashraf.Fathi":
      | AllowedActions      |
      | ReadAll             |
      | ReadBusinessUnits   |
      | ReadGoodsIssueTypes |

  Scenario: (04) Read allowed actions in Goods Issue Home Screen - User is authorized to Create only Action only but not authorized to all of the dropdowns(Happy Path)
    Given user is logged in as "CreateOnlyUser"
    When "CreateOnlyUser" requests to read Authorized actions and Reads of GoodsIssue in home screen
    Then the following actions are displayed to "CreateOnlyUser":
      | AllowedActions |
      | Create         |

  Scenario: (05) Read allowed actions in Goods Issue Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read Authorized actions and Reads of GoodsIssue in home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"