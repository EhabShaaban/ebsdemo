# Author: Hossam Hassan, Ahmad Hamed, Engy Mohamed (EBS-6759)
# Reviewer: Somaya Ahmed
Feature: Create GoodsIssue - Validation

  Background:

    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | hr1            | SuperUser                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                         |
      | SuperUser                 | SuperUserSubRole                |
      | SalesSpecialist_Signmedia | GIOwner_Signmedia               |
      | SalesSpecialist_Signmedia | PurUnitReader_Signmedia         |
      | SalesSpecialist_Signmedia | CompanyViewer                   |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia        |
      | SalesSpecialist_Signmedia | PlantViewer                     |
      | SalesSpecialist_Signmedia | StorehouseViewer                |
      | SalesSpecialist_Signmedia | StorekeeperViewer               |
      | SalesSpecialist_Signmedia | SOViewer_LoggedInUser_Signmedia |


    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                   | Condition                                                                 |
      | GIOwner_Signmedia               | GoodsIssue:Create            |                                                                           |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia']                                            |
      | CompanyViewer                   | Company:ReadAll              |                                                                           |
      | CustomerViewer_Signmedia        | Customer:ReadAll             | [purchaseUnitName='Signmedia']                                            |
      | PlantViewer                     | Plant:ReadAll                |                                                                           |
      | StorehouseViewer                | Storehouse:ReadAll           |                                                                           |
      | StorekeeperViewer               | Storekeeper:ReadAll          |                                                                           |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll           | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |
      | SuperUserSubRole                | *:*                          |                                                                           |


    And the following GoodsIssue Types exist:
      | Code                 | Name                             |
      | DELIVERY_TO_CUSTOMER | Deliver To Customer              |
      | BASED_ON_SALES_ORDER | Goods Issue Based On Sales Order |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Storekeepers exist:
      | Code | Name              |
      | 0007 | Mahmoud.Abdelaziz |

    And the following SalesInvoices exist:
      | Code       | State              | Type                                                              | BusinessUnit     |
      | 2019100004 | ReadyForDelivering | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100002 | Active             | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |

    And the following Plants exist:
      | Code | Name              |
      | 0001 | Madina Tech Plant |
      | 0002 | DigiPro Plant     |

    And the following Storehouses exist:
      | Code | Name                     |
      | 0001 | AlMadina Main Store      |
      | 0002 | AlMadina Secondary Store |
      | 0004 | DigiPro Secondary Store  |

    And the following Plants are related to Company with code "0001":
      | Code |
      | 0001 |

    And the following Storehouses for Plant with code "0001" exist:
      | Code |
      | 0001 |
      | 0002 |

    And the following Company for SalesInvoice with code "2019100004" exist:
      | Company          |
      | 0001 - AL Madina |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible           | ExpectedDeliveryDate | BusinessUnit     | State               |
      | 2020000056 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 07-Aug-2020 09:28 AM | 0002 - Signmedia | DeliveryComplete    |
      | 2020000055 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 06-Aug-2020 09:28 AM | 0002 - Signmedia | Expired             |
      | 2020000054 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 05-Aug-2020 09:28 AM | 0002 - Signmedia | Canceled            |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 04-Aug-2020 09:28 AM | 0002 - Signmedia | Approved            |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 03-Aug-2020 09:28 AM | 0002 - Signmedia | WaitingApproval     |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 02-Aug-2020 09:28 AM | 0002 - Signmedia | Rejected            |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0001 - SeragEldin Meghawry | 29-Aug-2020 12:00 AM | 0002 - Signmedia | ReadyForDelivery    |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry      | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated |
      | 2020000009 | LOCAL_SALES_ORDER - Local | 000001 - Al Ahram              | 0002 - Mohammed            | 07-Apr-2020 10:21 AM | 0002 - Signmedia | Draft               |

    And the following CompanyAndStoreData for the following SalesOrders exist:
      | SOCode     | Company          | Store                      |
      | 2020000056 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000055 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000054 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000053 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000052 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000051 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000012 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000011 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000009 | 0001 - AL Madina | 0001 - AlMadina Main Store |

  #EBS-5720
  Scenario Outline: (01) Create GoodsIssue - with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" creates GoodsIssue with the following values:
      | Type           | PurchaseUnit           | RefDocument   | Company           | Storehouse           | Storekeeper           |
      | <SelectedType> | <SelectedPurchaseUnit> | <RefDocument> | <SelectedCompany> | <SelectedStorehouse> | <SelectedStorekeeper> |
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | SelectedType         | SelectedPurchaseUnit | RefDocument | SelectedCompany | SelectedStorehouse | SelectedStorekeeper |
      |                      | 0002                 | 2019100004  | 0001            | 0002               | 0007                |
      | ""                   | 0002                 | 2019100004  | 0001            | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER |                      | 2019100004  | 0001            | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | ""                   | 2019100004  | 0001            | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 |             | 0001            | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | ""          | 0001            | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  |                 | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | ""              | 0002               | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            |                    | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | ""                 | 0007                |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0002               |                     |
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0002               | ""                  |
      #EBS-6759
      | BASED_ON_SALES_ORDER |                      | 2020000053  | 0001            | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | ""                   | 2020000053  | 0001            | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 |             | 0001            | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | ""          | 0001            | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  |                 | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | ""              | 0001               | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            |                    | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | ""                 | 0007                |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 0001               |                     |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 0001               | ""                  |

  #EBS-5720
  Scenario Outline: (02) Create GoodsIssue - with incorrect data entry (Validation Failure)
    Given user is logged in as "hr1"
    When "hr1" creates GoodsIssue with the following values:
      | Type           | PurchaseUnit           | RefDocument   | Company           | Storehouse           | Storekeeper           |
      | <SelectedType> | <SelectedPurchaseUnit> | <RefDocument> | <SelectedCompany> | <SelectedStorehouse> | <SelectedStorekeeper> |
    Then a failure notification is sent to "hr1" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "hr1"
    Examples:
      | SelectedType         | SelectedPurchaseUnit | RefDocument | SelectedCompany | SelectedStorehouse | SelectedStorekeeper | Field            | ErrMsg     |
      # purchase unit does not exist
      | DELIVERY_TO_CUSTOMER | 9999                 | 2019100004  | 0001            | 0002               | 0007                | purchaseUnitCode | Gen-msg-48 |
      # sales invoice does not exist
      | DELIVERY_TO_CUSTOMER | 0002                 | 9999999999  | 0001            | 0002               | 0007                | refDocumentCode  | Gen-msg-48 |
      # existing sales invoice but does not belong to the selected purchase unit
      | DELIVERY_TO_CUSTOMER | 0001                 | 2019100004  | 0001            | 0002               | 0007                | refDocumentCode  | Gen-msg-48 |
      # sales invoice in state delivered to customer
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019000001  | 0002            | 0004               | 0007                | refDocumentCode  | Gen-msg-32 |
      # storehouse does not exist
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 9999               | 0007                | storeHouseCode   | Gen-msg-48 |
      # existing storehouse but does not belong to the selected plant
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0004               | 0007                | storeHouseCode   | Gen-msg-48 |
      # storekeeper does not exist
      | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0002               | 9999                | storeKeeperCode  | Gen-msg-48 |
      #EBS-6759
      # sales order state has changed (states after allowed states)
      | BASED_ON_SALES_ORDER | 0002                 | 2020000056  | 0001            | 0001               | 0007                | refDocumentCode  | Gen-msg-32 |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000011  | 0001            | 0001               | 0007                | refDocumentCode  | Gen-msg-32 |
      | BASED_ON_SALES_ORDER | 0002                 | 2020000012  | 0001            | 0001               | 0007                | refDocumentCode  | Gen-msg-32 |

  #EBS-5720
  Scenario Outline: (03) Create GoodsIssue - with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "<User>"
    When "<User>" creates GoodsIssue with the following values:
      | Type           | PurchaseUnit           | RefDocument   | Company           | Storehouse           | Storekeeper           |
      | <SelectedType> | <SelectedPurchaseUnit> | <RefDocument> | <SelectedCompany> | <SelectedStorehouse> | <SelectedStorekeeper> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User | SelectedType         | SelectedPurchaseUnit | RefDocument | SelectedCompany | SelectedStorehouse | SelectedStorekeeper |
      | hr1  | DELIVERY_TO_CUSTOMER | ay7aga               | 2019100004  | 0001            | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 00002                | 2019100004  | 0001            | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | ay7aga      | 0001            | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 20191000004 | 0001            | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | ay7aga          | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 00001           | 0002               | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | ay7aga             | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 00002              | 0007                |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0002               | ay7aga              |
      | hr1  | DELIVERY_TO_CUSTOMER | 0002                 | 2019100004  | 0001            | 0002               | 00007               |
      | hr1  | ay7aga               | 0002                 | 2019100004  | 0001            | 0002               | 0007                |
      #EBS-6759
      # input data doesn't belong to referenced document
      | hr1  | BASED_ON_SALES_ORDER | 0001                 | 2020000053  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0002            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 0002               | 0007                |
      #malicious input
      | hr1  | BASED_ON_SALES_ORDER | 00002                | 2020000053  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | ay7aga               | 2020000053  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 20200000053 | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | ay7aga      | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 00001           | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | ay7aga          | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 00001              | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | ay7aga             | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 0001               | 00007               |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000053  | 0001            | 0001               | ay7aga              |
      # sales order state has changed (states before allowed states)
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000055  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000054  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000052  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000051  | 0001            | 0001               | 0007                |
      | hr1  | BASED_ON_SALES_ORDER | 0002                 | 2020000009  | 0002            | 0003               | 0007                |