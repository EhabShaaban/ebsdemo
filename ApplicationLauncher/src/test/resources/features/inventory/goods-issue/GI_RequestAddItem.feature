Feature: GI Request Add Items section

  Background:
    Given the following users and roles exist:
      | Name                                      | Role                                                              |
      | Ahmed.Al-Ashry                            | SalesSpecialist_Signmedia                                         |
      | Rabie.abdelghafar                         | SalesSpecialist_Flexo                                             |
      | Ahmed.Al-Ashry.NoItemsNoStoreTransactions | SalesSpecialist_Signmedia_CannotReadItemsAndStoreTransactionsRole |
      | hr1                                       | SuperUser                                                         |
    And the following roles and sub-roles exist:
      | Role                                                              | Sub-role                |
      | SalesSpecialist_Signmedia                                         | GIOwner_Signmedia       |
      | SalesSpecialist_Flexo                                             | GIOwner_Flexo           |
      | SalesSpecialist_Signmedia_CannotReadItemsAndStoreTransactionsRole | GIOwner_Signmedia       |
      | SalesSpecialist_Signmedia                                         | ItemViewer_Signmedia    |
      | SalesSpecialist_Signmedia                                         | StoreTransactionsViewer |
      | SuperUser                                                         | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                | Condition                      |
      | GIOwner_Signmedia       | GoodsIssue:UpdateItem     | [purchaseUnitName='Signmedia'] |
      | GIOwner_Flexo           | GoodsIssue:UpdateItem     | [purchaseUnitName='Flexo']     |
      | ItemViewer_Signmedia    | Item:ReadAll              | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia    | Item:ReadUoMData          | [purchaseUnitName='Signmedia'] |
      | StoreTransactionsViewer | StoreTransactions:ReadAll |                                |
      | SuperUserSubRole        | *:*                       |                                |
    And the following users doesn't have the following permissions:
      | User                                      | Permission                |
      | Ahmed.Al-Ashry.NoItemsNoStoreTransactions | Item:ReadAll              |
      | Ahmed.Al-Ashry.NoItemsNoStoreTransactions | StoreTransactions:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User              | Permission            | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:UpdateItem | [purchaseUnitName='Signmedia'] |

    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State  |
      | 2019000002 | 0002 - Signmedia | Draft  |
      | 2019000003 | 0002 - Signmedia | Active |
      | 2019100001 | 0002 - Signmedia | Draft  |

  Scenario: (01) Request to Add Item to GoodsIssue in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019000002"
    Then a new add Item dialoge is opened and GoodsIssueItems section of GoodsIssue with code "2019000002" becomes locked by "Ahmed.Al-Ashry"
    And the following authorized reads are returned to "Ahmed.Al-Ashry":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
      | ReadBatchNumber |
    And the following mandatory fields are returned to "Ahmed.Al-Ashry":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | batchNumber         |
    And the following editable fields are returned to "Ahmed.Al-Ashry":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | batchNumber         |

  Scenario: (02) Request to Add Item to GoodsIssue when GoodsIssueItems section is locked by another user (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to GoodsIssueItems section of GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-02"

  Scenario: (03) Request to Add Item to to GoodsIssue that doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019100001" successfully
    When "Ahmed.Al-Ashry" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019100001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario: (04) Request to Add Item to GoodsIssue in a state that doesn't allow this action - All states except Draft (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019000003"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-03"
    And GoodsIssueItems section of GoodsIssue with code "2019000006" is not locked by "Ahmed.Al-Ashry"

  Scenario: (05) Request to Add Item by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When  "Rabie.abdelghafar" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  Scenario: (06) Request to Add Item by a User with no authorized reads - All allowed states (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry.NoItemsNoStoreTransactions"
    When "Ahmed.Al-Ashry.NoItemsNoStoreTransactions" requests to add Item to GoodsIssueItems section of GoodsIssue with code "2019000002"
    Then a new add Item dialoge is opened and GoodsIssueItems section of GoodsIssue with code "2019000002" becomes locked by "Ahmed.Al-Ashry.NoItemsNoStoreTransactions"
    And there are no authorized reads returned to "Ahmed.Al-Ashry.NoItemsNoStoreTransactions"
    And the following mandatory fields are returned to "Ahmed.Al-Ashry.NoItemsNoStoreTransactions":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | batchNumber         |
    And the following editable fields are returned to "Ahmed.Al-Ashry.NoItemsNoStoreTransactions":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | batchNumber         |

    ######### Cancel saving GoodsIssueItems section ########################################################################

  Scenario: (07) Cancel save within the edit session of Item to GoodsIssue in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And GoodsIssueItems section of GoodsIssue with code "2019000002" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:10 AM"
    When "Ahmed.Al-Ashry" cancels saving GoodsIssueItems section of GoodsIssue with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Ahmed.Al-Ashry" on GoodsIssueItems section of GoodsIssue with code "2019000002" is released
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-04"

  Scenario: (08) Request to Cancel saving GoodsIssue Items section after lock session is expired
    Given user is logged in as "Ahmed.Al-Ashry"
    And GoodsIssueItems section of GoodsIssue with code "2019000002" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    When "Ahmed.Al-Ashry" cancels saving GoodsIssueItems section of GoodsIssue with code "2019000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-07"

  Scenario: (09) Request to Cancel saving GoodsIssueItems section after lock session is expire and GoodsIssue doesn't exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And GoodsIssueItems section of GoodsIssue with code "2019100001" is locked by "Ahmed.Al-Ashry" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the GoodsIssue with code "2019100001" successfully at "07-Jan-2019 09:31 AM"
    When "Ahmed.Al-Ashry" cancels saving GoodsIssueItems section of GoodsIssue with code "2019100001" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"


  Scenario: (10) Request to Cancel saving GoodsIssueItems section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" cancels saving GoodsIssueItems section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
