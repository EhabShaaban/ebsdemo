#Author: Niveen Magdy
#Reviewer: Somaya Aboulwafa

Feature: View GI - SalesOrder section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role           |
      | SalesSpecialist_Signmedia | GIViewer_Signmedia |
      | SalesSpecialist_Flexo     | GIViewer_Flexo     |
      | SuperUser                 | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                    | Condition                      |
      | GIViewer_Signmedia | GoodsIssue:ReadSalesOrderData | [purchaseUnitName='Signmedia'] |
      | GIViewer_Flexo     | GoodsIssue:ReadSalesOrderData | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                           |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                    | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:ReadSalesOrderData | [purchaseUnitName='Signmedia'] |
    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State |
      | 2020000001 | 0002 - Signmedia | Draft |
    And the following SalesOrderData for GoodsIssues exist:
      | GICode     | Customer                       | Address                 | ContactPerson | RefDocument | SalesRepresentative | Comments |
      | 2020000001 | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed | Wael Fathay   | 2020000053  | Ahmed.Al-Ashry      |          |

  # EBS-6760
  Scenario: (01) View GI SalesOrder section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view SalesOrder section of GoodsIssue with code "2020000001"
    Then the following values of SalesOrder section for GoodsIssue with code "2020000001" are displayed to "Ahmed.Al-Ashry":
      | Customer                       | Address                 | ContactPerson | RefDocument | SalesRepresentative | Comments |
      | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed | Wael Fathay   | 2020000053  | Ahmed.Al-Ashry      |          |

  # EBS-6760
  Scenario: (02) View GI SalesOrder section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view SalesOrder section of GoodsIssue with code "2020000001"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

    # EBS-6760
  Scenario: (03) View GI SalesOrder section - where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2020000001" successfully
    When "Ahmed.Al-Ashry" requests to view SalesOrder section of GoodsIssue with code "2020000001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
