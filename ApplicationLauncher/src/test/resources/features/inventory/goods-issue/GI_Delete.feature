Feature: Delete GoodsIssue

  Background:

    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole           |
      | SalesSpecialist_Signmedia | GIOwner_Signmedia |
      | SalesSpecialist_Flexo     | GIOwner_Flexo     |
      | SuperUser                 | SuperUserSubRole  |

    And the following sub-roles and permissions exist:
      | Subrole           | Permission        | Condition                      |
      | GIOwner_Signmedia | GoodsIssue:Delete | [purchaseUnitName='Signmedia'] |
      | GIOwner_Flexo     | GoodsIssue:Delete | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole  | *:*               |                                |

    And the following users have the following permissions without the following conditions:
      | User              | Permission        | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:Delete | [purchaseUnitName='Signmedia'] |

    And the following GoodsIssues exist with the following data:
      | Code       | PurchasingUnit   | State  | CreationDate         | Type                                       | Storehouse                      | Customer          |
      | 2019000003 | 0002 - Signmedia | Active | 06-Aug-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer | 0002 - AlMadina Secondary Store | 000001 - Al Ahram |
      | 2019000002 | 0002 - Signmedia | Draft  | 05-Aug-2019 09:02 AM | DELIVERY_TO_CUSTOMER - Deliver To Customer | 0001 - AlMadina Main Store      | 000001 - Al Ahram |

  # EBS-5443
  Scenario: (01) Delete GoodsIssue - (draft) by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete GoodsIssue with code "2019000002"
    Then GoodsIssue with code "2019000002" is deleted from the system
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-12"


  Scenario: (02) Delete GoodsIssue - in a state that doesn't allow delete: Active (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete GoodsIssue with code "2019000003"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-13"


  Scenario: (03) Delete GoodsIssue - (draft) where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to delete GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  @Future
  Scenario Outline: (04) Delete GoodsIssue - that is locked by another user (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And first "hr1" opens "<GISection>" section of GoodsIssue with code "2019000002" in edit mode
    When "Ahmed.Al-Ashry" requests to delete GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-14"
    Examples:
      | GISection       |
      | SalesInvoice    |
      | Store           |
      | GoodsIssueItems |

  Scenario: (05) Delete GoodsIssue - by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to delete GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page