Feature: View Company section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role           |
      | SalesSpecialist_Signmedia | GIViewer_Signmedia |
      | SalesSpecialist_Flexo     | GIViewer_Flexo     |
      | SuperUser                 | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission             | Condition                      |
      | GIViewer_Signmedia | GoodsIssue:ReadCompany | [purchaseUnitName='Signmedia'] |
      | GIViewer_Flexo     | GoodsIssue:ReadCompany | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                    |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission             | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:ReadCompany | [purchaseUnitName='Signmedia'] |
    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State |
      | 2019000002 | 0002 - Signmedia | Draft |
    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company          | Storehouse                 | BusinessUnit     |
      | 2019000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

  # EBS-5437
  Scenario: (01) View GI CompanyData section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Company section of GoodsIssue with code "2019000002"
    Then the following values of Company section for GoodsIssue with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | Company   | Storehouse          | BusinessUnit |
      | AL Madina | AlMadina Main Store | Signmedia    |

  # EBS-5437
  Scenario: (02) View GI CompanyData section - by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view Company section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  # EBS-5437
  Scenario: (03) View GI CompanyData section - where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to view Company section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
