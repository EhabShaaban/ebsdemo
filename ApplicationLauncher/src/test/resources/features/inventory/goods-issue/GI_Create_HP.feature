#Author: Ahmad Hamed, Hossam Hassan & Engy Mohamed
#Reviewer: Somaya Ahmed

Feature: Create GoodsIssue - Happy Path

  Background:

    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                         |
      | SalesSpecialist_Signmedia | GIOwner_Signmedia               |
      | SalesSpecialist_Signmedia | PurUnitReader_Signmedia         |
      | SalesSpecialist_Signmedia | CompanyViewer                   |
      | SalesSpecialist_Signmedia | CustomerViewer_Signmedia        |
      | SalesSpecialist_Signmedia | StorehouseViewer                |
      | SalesSpecialist_Signmedia | StorekeeperViewer               |
      | SalesSpecialist_Signmedia | GoodsIssueTypeViewer            |
      | SalesSpecialist_Signmedia | SOViewer_LoggedInUser_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission             | Condition                                                                 |
      | GIOwner_Signmedia               | GoodsIssue:Create      |                                                                           |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll | [purchaseUnitName='Signmedia']                                            |
      | CompanyViewer                   | Company:ReadAll        |                                                                           |
      | CustomerViewer_Signmedia        | Customer:ReadAll       | [purchaseUnitName='Signmedia']                                            |
      | StorehouseViewer                | Storehouse:ReadAll     |                                                                           |
      | StorekeeperViewer               | Storekeeper:ReadAll    |                                                                           |
      | GoodsIssueTypeViewer            | GoodsIssueType:ReadAll |                                                                           |
      | SOViewer_LoggedInUser_Signmedia | SalesOrder:ReadAll     | ([purchaseUnitName='Signmedia'] && [salesResponsibleName='LoggedInUser']) |


    And the following GoodsIssue Types exist:
      | Code                 | Name                             |
      | DELIVERY_TO_CUSTOMER | Deliver To Customer              |
      | BASED_ON_SALES_ORDER | Goods Issue Based On Sales Order |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Storekeepers exist:
      | Code | Name              |
      | 0007 | Mahmoud.Abdelaziz |

    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000006 | المطبعة الأمنية       | 0001 - Flexo     |
      | 000002 | Client2               | 0001 - Flexo     |


    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |

    And the following Storehouses exist:
      | Code | Name                     |
      | 0001 | AlMadina Main Store      |
      | 0002 | AlMadina Secondary Store |

    And the following SalesInvoices exist:
      | Code       | State              | Type                                                              | BusinessUnit     |
      | 2019100004 | ReadyForDelivering | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |

    And SalesInvoice with code "2019100004" has the following Items:
      | Item                | OrderUnit    | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000008 - PRO-V-ST-2 | 0030 - Piece | 136 | 22                   | 22              | 2992        |

    And the following Customer for SalesInvoice with code "2019100004" exist:
      | Customer         |
      | 000002 - Client2 |

    And the following Company for SalesInvoice with code "2019100004" exist:
      | Company          |
      | 0001 - AL Madina |

    And the following Plants are related to Company with code "0001":
      | Code |
      | 0001 |

    And the following Storehouses for Plant with code "0001" exist:
      | Code |
      | 0001 |
      | 0002 |

    And the following Storehouses for Plant with code "0002" exist:
      | Code |
      | 0003 |
      | 0004 |

    And the following Plants are related to Company with code "0002":
      | Code |
      | 0002 |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit     | State                 |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 05-Nov-2020 12:00 AM | 0002 - Signmedia | Approved              |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 31-Aug-2020 09:00 AM | 0002 - Signmedia | SalesInvoiceActivated |

    And the following SalesOrderData for the following SalesOrders exist:
      | SOCode     | Customer                       | PaymentTerms                 | CustomerAddress             | CurrencyISO | ContactPerson | ExpectedDeliveryDate | CreditLimit | Notes  |
      | 2020000069 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 99, El Hegaz St, Heliopolis | EGP         | Wael Fathay   | 05-Nov-2020 12:00 AM | 100000.0    | potato |
      | 2020000068 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 99, El Hegaz St, Heliopolis | EGP         | Wael Fathay   | 31-Aug-2020 09:00 AM | 100000.0    |        |

    And the following SalesOrders have the following Items:
      | SOCode     | ItemId | ItemType                | Item          | OrderUnit            | Qty         | SalesPrice | TotalAmount   |
      | 2020000069 | 87     | COMMERCIAL - Commercial | 000051 - Ink5 | 0040 - Bottle-1Liter | 5.123456789 | 29.000     | 148.580246881 |
      | 2020000068 | 86     | COMMERCIAL - Commercial | 000051 - Ink5 | 0040 - Bottle-1Liter | 1.000       | 30.000     | 30.000        |
      | 2020000068 | 85     | COMMERCIAL - Commercial | 000051 - Ink5 | 0014 - Liter         | 1.000       | 40.000     | 40.000        |

    And the following CompanyAndStoreData for the following SalesOrders exist:
      | SOCode     | Company          | Store                      |
      | 2020000069 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000068 | 0001 - AL Madina | 0001 - AlMadina Main Store |


  Scenario: (01) Create GoodsIssue - with all mandatory fields by an authorized user based on SalesInvoice (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created GoodsIssue was with code "2021000002"
    When "Ahmed.Al-Ashry" creates GoodsIssue with the following values:
      | Type                 | PurchaseUnit     | RefDocument | Company          | Storehouse                      | Storekeeper              |
      | DELIVERY_TO_CUSTOMER | 0002 - Signmedia | 2019100004  | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0007 - Mahmoud.Abdelaziz |
    Then a new GoodsIssue is created with the following values:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type                 | Storekeeper              |
      | 2021000003 | Draft | Ahmed.Al-Ashry | Ahmed.Al-Ashry | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | DELIVERY_TO_CUSTOMER | 0007 - Mahmoud.Abdelaziz |
    And the SalesInvoice Section of GoodsIssue with code "2021000003" is updated as follows:
      | Customer         | Address            | ContactPerson           | RefDocument | SalesRepresentative | KAP               |
      | 000002 - Client2 | 66, Salah Salem St | Client 2 Contact Person | 2019100004  | Ahmed.Al-Ashry      | 0011 - Mohamedeen |
    And the Company Section of GoodsIssue with code "2021000003" is updated as follows:
      | Company          | Storehouse                      | PurchaseUnit     |
      | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0002 - Signmedia |
    And the Items Section of GoodsIssue with code "2021000003" is updated as follows:
      | Item                | OrderUnit | Qty | BatchNo |
      | 000008 - PRO-V-ST-2 | 0030 - PC | 136 |         |

    #EBS-6755
  Scenario: (02) Create GoodsIssue - with all mandatory fields by an authorized user based on Approved SalesOrder (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created GoodsIssue was with code "2021000002"
    When "Ahmed.Al-Ashry" creates GoodsIssue with the following values:
      | Type                 | PurchaseUnit     | RefDocument | Company         | Storehouse                 | Storekeeper              |
      | BASED_ON_SALES_ORDER | 0002 - Signmedia | 2020000069  | 0001 - AlMadina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    Then a new GoodsIssue is created with the following values:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type                 | Storekeeper              |
      | 2021000003 | Draft | Ahmed.Al-Ashry | Ahmed.Al-Ashry | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | BASED_ON_SALES_ORDER | 0007 - Mahmoud.Abdelaziz |
    And the SalesOrder Section of GoodsIssue with code "2021000003" is updated as follows:
      | RefDocument | Customer                       | Address                     | ContactPerson | SalesRepresentative | Notes  |
      | 2020000069  | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay   | Ahmed.Al-Ashry      | potato |
    And the Company Section of GoodsIssue with code "2021000003" is updated as follows:
      | Company         | Storehouse                 | PurchaseUnit     |
      | 0001 - AlMadina | 0001 - AlMadina Main Store | 0002 - Signmedia |
    And the Items Section of GoodsIssue with code "2021000003" is updated as follows:
      | Item          | OrderUnit            | Qty         | BatchNo |
      | 000051 - Ink5 | 0040 - Bottle-1Liter | 5.123456789 |         |
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-11"

    #EBS-7175
  Scenario: (03) Create GoodsIssue - with all mandatory fields by an authorized user based on SalesInvoiceActivated SalesOrder (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created GoodsIssue was with code "2021000002"
    When "Ahmed.Al-Ashry" creates GoodsIssue with the following values:
      | Type                 | PurchaseUnit     | RefDocument | Company         | Storehouse                 | Storekeeper              |
      | BASED_ON_SALES_ORDER | 0002 - Signmedia | 2020000068  | 0001 - AlMadina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    Then a new GoodsIssue is created with the following values:
      | Code       | State | CreatedBy      | LastUpdatedBy  | CreationDate         | LastUpdateDate       | Type                 | Storekeeper              |
      | 2021000003 | Draft | Ahmed.Al-Ashry | Ahmed.Al-Ashry | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | BASED_ON_SALES_ORDER | 0007 - Mahmoud.Abdelaziz |
    And the SalesOrder Section of GoodsIssue with code "2021000003" is updated as follows:
      | RefDocument | Customer                       | Address                     | ContactPerson | SalesRepresentative | Notes |
      | 2020000068  | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay   | Ahmed.Al-Ashry      |       |
    And the Company Section of GoodsIssue with code "2021000003" is updated as follows:
      | Company         | Storehouse                 | PurchaseUnit     |
      | 0001 - AlMadina | 0001 - AlMadina Main Store | 0002 - Signmedia |
    And the Items Section of GoodsIssue with code "2021000003" is updated as follows:
      | Item          | OrderUnit            | Qty | BatchNo |
      | 000051 - Ink5 | 0040 - Bottle-1Liter | 1   |         |
      | 000051 - Ink5 | 0014 - Liter         | 1   |         |
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-11"
