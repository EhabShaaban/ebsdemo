Feature: View SalesInvoice section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role           |
      | SalesSpecialist_Signmedia | GIViewer_Signmedia |
      | SalesSpecialist_Flexo     | GIViewer_Flexo     |
      | SuperUser                 | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                      | Condition                      |
      | GIViewer_Signmedia | GoodsIssue:ReadSalesInvoiceData | [purchaseUnitName='Signmedia'] |
      | GIViewer_Flexo     | GoodsIssue:ReadSalesInvoiceData | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                             |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                      | Condition                      |
      | Rabie.abdelghafar | GoodsIssue:ReadSalesInvoiceData | [purchaseUnitName='Signmedia'] |
    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State |
      | 2019000002 | 0002 - Signmedia | Draft |
    And the following SalesInvoiceData for GoodsIssues exist:
      | GICode     | Customer          | Address            | ContactPerson | RefDocument | SalesRepresentative | KAP         | Comments             |
      | 2019000002 | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 007 - Fakhr | For GI No 2019000002 |

  # EBS-5550
  Scenario: (01) View GI SalesInvoice section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view SalesInvoice section of GoodsIssue with code "2019000002"
    Then the following values of SalesInvoice section for GoodsIssue with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | Customer          | Address            | ContactPerson | RefDocument | SalesRepresentative | KAP         | Comments             |
      | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 007 - Fakhr | For GI No 2019000002 |

  # EBS-5550
  Scenario: (02) View GI SalesInvoice section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view SalesInvoice section of GoodsIssue with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

    # EBS-5550
  Scenario: (03) View GI SalesInvoice section - where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Ahmed.Al-Ashry" requests to view SalesInvoice section of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"