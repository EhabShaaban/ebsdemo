# Author: Ahmed Hamed, Hossam Hassan & Engy Mohamed (EBS-6756)
# Reviewer: Somaya Ahmed

Feature: Activate GoodsIssue (Val)

  Background:

    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |
      | hr1               | SuperUser             |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                   |
      | Storekeeper_Signmedia | GIActivateOwner_Signmedia |
      | Storekeeper_Flexo     | GIActivateOwner_Flexo     |
      | SuperUser             | SuperUserSubRole          |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission          | Condition                      |
      | GIActivateOwner_Signmedia | GoodsIssue:Activate | [purchaseUnitName='Signmedia'] |
      | GIActivateOwner_Flexo     | GoodsIssue:Activate | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole          | *:*                 |                                |

    And the following GoodsIssues exist:
      | GICode     | BusinessUnit     | State  |
      | 2020000014 | 0002 - Signmedia | Draft  |
      | 2020000013 | 0002 - Signmedia | Draft  |
      | 2020000012 | 0002 - Signmedia | Draft  |
      | 2020000011 | 0002 - Signmedia | Active |
      | 2020000010 | 0002 - Signmedia | Draft  |
      | 2020000004 | 0001 - Flexo     | Draft  |
      | 2019000006 | 0002 - Signmedia | Draft  |
      | 2019000005 | 0002 - Signmedia | Active |
      | 2019000002 | 0002 - Signmedia | Draft  |
      | 2021000001 | 0002 - Signmedia | Draft  |

    And the following GeneralData for GoodsIssues exist:
      | GICode     | Type                                                    | CreatedBy                 | State  | CreationDate         | Storekeeper              |
      | 2021000001 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft  | 02-Feb-2021 12:40 PM | 0006 - Ahmed.Ali         |
      | 2020000014 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft  | 17-Aug-2020 12:40 PM | 0009 - Hamada.khalaf     |
      | 2020000013 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft  | 17-Aug-2020 12:40 PM | 0009 - Hamada.khalaf     |
      | 2020000012 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft  | 17-Aug-2020 12:39 PM | 0009 - Hamada.khalaf     |
      | 2020000010 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Draft  | 06-Aug-2020 11:35 AM | 0007 - Mahmoud.Abdelaziz |
      | 2020000011 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | hr1                       | Active | 06-Aug-2020 12:01 PM | 0007 - Mahmoud.Abdelaziz |
      | 2020000004 | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order | Admin                     | Draft  | 05-Aug-2020 09:00 AM | 0007 - Mahmoud.Abdelaziz |
      | 2019000006 | DELIVERY_TO_CUSTOMER - Deliver To Customer              | Admin from BDKCompanyCode | Draft  | 19-Nov-2019 09:02 AM | 0006 - Ahmed.Ali         |
      | 2019000005 | DELIVERY_TO_CUSTOMER - Deliver To Customer              | Admin from BDKCompanyCode | Active | 19-Nov-2019 09:02 AM | 0006 - Ahmed.Ali         |
      | 2019000002 | DELIVERY_TO_CUSTOMER - Deliver To Customer              | Admin from BDKCompanyCode | Draft  | 05-Aug-2019 09:02 AM | 0007 - Mahmoud.Abdelaziz |

    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company          | Storehouse                 | BusinessUnit     |
      | 2021000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000014 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000013 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000012 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000011 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000010 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2020000004 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0001 - Flexo     |
      | 2019000006 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2019000005 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |
      | 2019000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0002 - Signmedia |

    And the following SalesInvoiceData for GoodsIssues exist:
      | GICode     | Customer          | Address            | ContactPerson | RefDocument | SalesRepresentative | KAP         | Comments             |
      | 2019000006 | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 007 - Fakhr |                      |
      | 2019000005 | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 007 - Fakhr |                      |
      | 2019000002 | 000001 - Al Ahram | 66, Salah Salem St | Wael Fathay   | 2019100004  | SeragEldin Meghawry | 007 - Fakhr | For GI No 2019000002 |

    And the following SalesOrderData for GoodsIssues exist:
      | GICode     | Customer                       | Address                     | ContactPerson           | RefDocument | SalesRepresentative | Comments |
      | 2021000001 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay             | 2020000070  | Ahmed.Al-Ashry      |          |
      | 2020000014 | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed     | Wael Fathay             | 2020000012  | SeragEldin Meghawry |          |
      | 2020000013 | 000007 - مطبعة أكتوبر الهندسية | 7th st, El Sheikh Zayed     | Wael Fathay             | 2020000011  | Ahmed.Al-Ashry      |          |
      | 2020000012 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay             | 2020000069  | Ahmed.Al-Ashry      |          |
      | 2020000011 | 000007 - مطبعة أكتوبر الهندسية | 66, Salah Salem St          | Wael Fathay             | 2020000068  | SeragEldin Meghawry |          |
      | 2020000010 | 000007 - مطبعة أكتوبر الهندسية | 99, El Hegaz St, Heliopolis | Wael Fathay             | 2020000010  | SeragEldin Meghawry |          |
      | 2020000004 | 000006 - المطبعة الأمنية       | 66, Salah Salem St          | Client 2 Contact Person | 2020000008  | Manar               |          |

    And the following GeneralData for SalesOrder exists:
      | Code       | Type                      | BusinessUnit     | SalesResponsible           | CreatedBy                 | State                 | CreationDate         |
      | 2020000070 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | SalesInvoiceActivated | 30-Aug-2020 09:55 AM |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | Approved              | 17-Aug-2020 12:37 PM |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | SalesInvoiceActivated | 06-Aug-2020 09:00 AM |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | ReadyForDelivery      | 03-Aug-2020 02:22 PM |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry      | hr1                       | GoodsIssueActivated   | 03-Aug-2020 01:36 PM |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0001 - SeragEldin Meghawry | hr1                       | SalesInvoiceActivated | 03-Aug-2020 01:31 PM |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0003 - Manar               | Admin from BDKCompanyCode | DeliveryComplete      | 04-Feb-2020 12:40 PM |

    And the following ItemData for GoodsIssues exist:
      | GICode     | Item                                          | UOM                  | Quantity | BaseUnit  | QtyBase | BatchNo | Price |
      | 2021000001 | 000060 - Hot Laminated Frontlit Fabric roll 1 | 0029 - Roll 2.20x50  | 50.00    | 0019 - M2 | 5500.0  |         | 65    |
      | 2021000001 | 000058 - Flex Primer Varnish E22              | 0019 - M2            | 50.00    | 0019 - M2 | 50.0    |         | 95    |
      | 2019000002 | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2            | 100.0    | 0019 - M2 | 100.0   |         |       |
      | 2019000002 | 000008 - PRO-V-ST-2                           | 0030 - PC            | 136.0    | 0019 - M2 | 136.0   |         |       |
      | 2019000002 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 500.0    | 0019 - M2 | 500.0   |         |       |
      | 2019000005 | 000008 - PRO-V-ST-2                           | 0030 - PC            | 136.0    | 0019 - M2 | 136.0   |         | 22.0  |
      | 2019000006 | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2            | 200.0    | 0019 - M2 | 200.0   |         |       |
      | 2019000006 | 000008 - PRO-V-ST-2                           | 0030 - PC            | 136.0    | 0019 - M2 | 136.0   |         |       |
      | 2020000004 | 000051 - Ink5                                 | 0014 - Liter         | 2.0      | 0019 - M2 | 2.0     |         | 10.0  |
      | 2020000010 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 10       | 0019 - M2 | 10      |         | 25.0  |
      | 2020000011 | 000051 - Ink5                                 | 0014 - Liter         | 1.0      | 0019 - M2 | 1.0     |         | 40.0  |
      | 2020000011 | 000051 - Ink5                                 | 0040 - Bottle-1Liter | 1.0      | 0019 - M2 | 1.0     |         | 30.0  |
      | 2020000012 | 000051 - Ink5                                 | 0040 - Bottle-1Liter | 5.0      | 0019 - M2 | 5.0     |         | 29.0  |
      | 2020000013 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 10       | 0019 - M2 | 10      |         | 33.0  |
      | 2020000014 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 10       | 0019 - M2 | 10      |         | 40.0  |

    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item                                          | UOM                  | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000540019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2            | UNRESTRICTED_USE | 149.0             |
      | UNRESTRICTED_USE00020001000100010000510040 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5                                 | 0040 - Bottle-1Liter | UNRESTRICTED_USE | 1.0               |
      | UNRESTRICTED_USE00020001000100010000080030 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000008 - PRO-V-ST-2                           | 0030 - PC            | UNRESTRICTED_USE | 136.0             |

    #EBS-6756
  Scenario Outline: (01) Activate GoodsIssue Val - with an invalid state - Active State (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "<GICode>" at "20-Nov-2020 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-32"
    Examples:
      | GICode     |
      | 2019000005 |
      | 2020000011 |

  @Future
  Scenario Outline: (02) Activate GoodsIssue Val - when one of the sections is locked (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And first "hr1" opens "<SectionName>" of Invoice with Code "2019000002" in edit mode at "20-Nov-2019 9:20 AM"
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2019000002" at "20-Nov-2019 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-14"
    Examples:
      | SectionName  |
      | SalesInvoice |
      | GIItems      |

  Scenario Outline: (03) Activate GoodsIssue Val - where GoodsIssue doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "<GICode>" successfully
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "<GICode>" at "20-Nov-2020 09:30 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"
    Examples:
      | GICode     |
      | 2019000002 |
      | 2020000010 |

  #EBS-8509
  Scenario: (04) Activate GoodsIssue Val - where GoodsIssue (Based On SalesInvoice) has item quantity that exceeds remaining in store for a specific business unit(Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2019000006" at "20-Nov-2019 09:30 AM"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-msg-01"
    And the following items which has quantity remaining in store less than quantity in GoodsIssue with code "2019000006" are sent to "Mahmoud.Abdelaziz"
      | Item En                                                  | Item Ar                                                  |
      | Item: 000054 - Hot Laminated Frontlit Fabric roll 3 - M2 | الصنف: 000054 - هوت لمنيتيت فرونت ليت فابريك رول 3  - م م |

  #EBS-6756
  #EBS-8509
  Scenario: (05) Activate GoodsIssue Val - where GoodsIssue (Based On SalesOrder) has item quantity that exceeds remaining in store for a specific business unit(Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2020000012" at "20-Nov-2020 09:30 AM"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-msg-01"
    And the following items which has quantity remaining in store less than quantity in GoodsIssue with code "2020000012" are sent to "Mahmoud.Abdelaziz"
      | Item En                             | Item Ar                                |
      | Item: 000051 - Ink5 - Bottle-1Liter | الصنف: 000051 - Ink5 - Bottle-1Liter |

  #EBS-6756
  Scenario Outline: (06) Activate GoodsIssue Val - where SalesOrder state is not (Approved or SalesInvoiceActivated)(Exception Case)
    Given user is logged in as "<User>"
    When "<User>" Activates GoodsIssue with code "<GICode>" at "20-Nov-2020 09:30 AM"
    Then a failure notification is sent to "<User>" with the following message "GI-msg-05"
    Examples:
      | GICode     | GIState | SOCode     | SOState             | User              |
      | 2020000004 | Draft   | 2020000008 | DeliveryComplete    | Ali.Hasan         |
      | 2020000013 | Draft   | 2020000011 | GoodsIssueActivated | Mahmoud.Abdelaziz |
      | 2020000014 | Draft   | 2020000012 | ReadyForDelivery    | Mahmoud.Abdelaziz |

  #EBS-8338
  Scenario: (07) Activate GoodsIssue Val - where No records exist in stock availability for this item(Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" Activates GoodsIssue with code "2021000001" at "02-Feb-2021 10:30 AM"
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "GI-msg-01"
