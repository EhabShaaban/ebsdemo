# Author: Niveen Magdy (EBS-6763)
# Reviewer: Somaya Aboulwafa

Feature: Allowed Action For Goods Issue (Object Screen)

  This feature file covers the following GoodsIssue Types:
  1- DELIVERY_TO_CUSTOMER
  2- BASED_ON_SALES_ORDER

  Background:
    Given the following users and roles exist:
      | Name                         | Role                             |
      | Ahmed.Al-Ashry               | SalesSpecialist_Signmedia        |
      | Mahmoud.Abdelaziz            | Storekeeper_Signmedia            |
      | Mahmoud.Abdelaziz.MadinaMain | Storekeeper_Signmedia_MadinaMain |
      | CannotReadSections           | CannotReadSectionsRole           |
      | Afaf                         | FrontDesk                        |
      | hr1                          | SuperUser                        |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                              |
      | SalesSpecialist_Signmedia        | GIOwner_Signmedia                    |
      | SalesSpecialist_Signmedia        | GIViewer_Signmedia                   |
      | Storekeeper_Signmedia            | GIActivateOwner_Signmedia            |
      | Storekeeper_Signmedia            | GIViewer_Signmedia                   |
      | Storekeeper_Signmedia_MadinaMain | GIActivateOwner_Signmedia_MadinaMain |
      | CannotReadSectionsRole           | CannotReadSectionsSubRole            |
      | SuperUser                        | SuperUserSubRole                     |
    And the following sub-roles and permissions exist:
      | Subrole                              | Permission                        | Condition                                                   |
      | GIOwner_Signmedia                    | GoodsIssue:Delete                 | [purchaseUnitName='Signmedia']                              |
      | GIOwner_Signmedia                    | GoodsIssue:UpdateSalesInvoiceData | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadAll                | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadGeneralData        | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadSalesInvoiceData   | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadSalesOrderData     | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadItem               | [purchaseUnitName='Signmedia']                              |
      | GIViewer_Signmedia                   | GoodsIssue:ReadCompany            | [purchaseUnitName='Signmedia']                              |
      | GIActivateOwner_Signmedia            | GoodsIssue:Activate               | [purchaseUnitName='Signmedia']                              |
      | GIActivateOwner_Signmedia            | GoodsIssue:ExportPDF              | [purchaseUnitName='Signmedia']                              |
      | GIActivateOwner_Signmedia_MadinaMain | GoodsIssue:Activate               | ([purchaseUnitName='Signmedia'] && [storehouseCode='0001']) |
      | CannotReadSectionsSubRole            | GoodsIssue:Delete                 |                                                             |
      | CannotReadSectionsSubRole            | GoodsIssue:Activate               |                                                             |
      | CannotReadSectionsSubRole            | GoodsIssue:ExportPDF              |                                                             |
      | CannotReadSectionsSubRole            | GoodsIssue:ReadGeneralData        |                                                             |
      | SuperUserSubRole                     | *:*                               |                                                             |

    And the following users doesn't have the following permissions:
      | User               | Permission                        |
      | Afaf               | GoodsIssue:Delete                 |
      | Afaf               | GoodsIssue:UpdateSalesInvoiceData |
      | Afaf               | GoodsIssue:Activate               |
      | Afaf               | GoodsIssue:ExportPDF              |
      | Afaf               | GoodsIssue:ReadAll                |
      | Afaf               | GoodsIssue:ReadGeneralData        |
      | Afaf               | GoodsIssue:ReadSalesInvoiceData   |
      | Afaf               | GoodsIssue:ReadSalesOrderData     |
      | Afaf               | GoodsIssue:ReadItem               |
      | Afaf               | GoodsIssue:ReadCompany            |
      | CannotReadSections | GoodsIssue:UpdateSalesInvoiceData |
      | CannotReadSections | GoodsIssue:ReadSalesInvoiceData   |
      | CannotReadSections | GoodsIssue:ReadSalesOrderData     |
      | CannotReadSections | GoodsIssue:ReadItem               |
      | CannotReadSections | GoodsIssue:ReadCompany            |
      | Ahmed.Al-Ashry     | GoodsIssue:Activate               |
      | Ahmed.Al-Ashry     | GoodsIssue:ExportPDF              |
      | Mahmoud.Abdelaziz  | GoodsIssue:Delete                 |
      | Mahmoud.Abdelaziz  | GoodsIssue:UpdateSalesInvoiceData |

    And the following users have the following permissions without the following conditions:
      | User                         | Permission                        | Condition                                                   |
      | Ahmed.Al-Ashry               | GoodsIssue:Delete                 | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:UpdateSalesInvoiceData | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadAll                | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadGeneralData        | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadSalesInvoiceData   | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadSalesOrderData     | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadItem               | [purchaseUnitName='Flexo']                                  |
      | Ahmed.Al-Ashry               | GoodsIssue:ReadCompany            | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:Activate               | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz.MadinaMain | GoodsIssue:Activate               | ([purchaseUnitName='Signmedia'] && [storehouseCode='0002']) |
      | Mahmoud.Abdelaziz            | GoodsIssue:ExportPDF              | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadAll                | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadGeneralData        | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadSalesInvoiceData   | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadSalesOrderData     | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadItem               | [purchaseUnitName='Flexo']                                  |
      | Mahmoud.Abdelaziz            | GoodsIssue:ReadCompany            | [purchaseUnitName='Flexo']                                  |



    And the following GoodsIssues exist:

      | GICode     | State  | BusinessUnit     | Type                                                    |
      | 2019000002 | Draft  | 0002 - Signmedia | DELIVERY_TO_CUSTOMER - Deliver To Customer              |
      | 2019000003 | Active | 0002 - Signmedia | DELIVERY_TO_CUSTOMER - Deliver To Customer              |
      | 2019000004 | Draft  | 0001 - Flexo     | DELIVERY_TO_CUSTOMER - Deliver To Customer              |
      | 2019000001 | Active | 0001 - Flexo     | DELIVERY_TO_CUSTOMER - Deliver To Customer              |
      | 2020000001 | Draft  | 0002 - Signmedia | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order |
      | 2020000003 | Active | 0002 - Signmedia | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order |
      | 2020000004 | Draft  | 0001 - Flexo     | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order |
      | 2020000002 | Active | 0001 - Flexo     | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order |
      | 2021000002 | Draft  | 0002 - Signmedia | BASED_ON_SALES_ORDER - Goods Issue Based On Sales Order |

    And the following CompanyData for GoodsIssues exist:
      | GICode     | Company          | Storehouse                      | BusinessUnit     |
      | 2019000002 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia |
      | 2019000003 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0002 - Signmedia |
      | 2019000004 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0001 - Flexo     |
      | 2019000001 | 0002 - DigiPro   | 0003 - DigiPro Main Store       | 0001 - Flexo     |
      | 2020000001 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store      | 0002 - Signmedia |
      | 2020000004 | 0002 - DigiPro   | 0003 - DigiPro Main Store       | 0001 - Flexo     |
      | 2020000002 | 0002 - DigiPro   | 0003 - DigiPro Main Store       | 0001 - Flexo     |
      | 2021000002 | 0001 - AL Madina | 0002 - AlMadina Secondary Store | 0002 - Signmedia |


  # EBS-5557
  # EBS-6763
  Scenario Outline: (01) Read allowed actions in Goods Issue Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of GoodsIssue with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User                         | Code       | AllowedActions                                                                                       |
      | Ahmed.Al-Ashry               | 2019000002 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesInvoiceData, ReadItem, UpdateSalesInvoiceData, Delete |
      | Ahmed.Al-Ashry               | 2019000003 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesInvoiceData, ReadItem                                 |
      | Ahmed.Al-Ashry               | 2020000001 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem, Delete                           |
      | Ahmed.Al-Ashry               | 2020000003 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem                                   |
      | Mahmoud.Abdelaziz            | 2019000002 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesInvoiceData, ReadItem, Activate, ExportPDF            |
      | Mahmoud.Abdelaziz            | 2019000003 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesInvoiceData, ReadItem, ExportPDF                      |
      | Mahmoud.Abdelaziz            | 2020000001 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem, Activate, ExportPDF              |
      | Mahmoud.Abdelaziz            | 2020000003 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem, ExportPDF                        |
      | Mahmoud.Abdelaziz.MadinaMain | 2021000002 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem                                   |
      | Mahmoud.Abdelaziz.MadinaMain | 2020000001 | ReadAll, ReadGeneralData,ReadCompany, ReadSalesOrderData, ReadItem, Activate, ExportPDF              |
      | CannotReadSections           | 2019000002 | Delete, Activate, ExportPDF, ReadGeneralData                                                         |
      | CannotReadSections           | 2019000003 | ExportPDF, ReadGeneralData                                                                           |
      | CannotReadSections           | 2019000004 | Delete, Activate, ExportPDF, ReadGeneralData                                                         |
      | CannotReadSections           | 2019000001 | ExportPDF, ReadGeneralData                                                                           |
      | CannotReadSections           | 2020000001 | Delete, Activate, ExportPDF, ReadGeneralData                                                         |
      | CannotReadSections           | 2020000003 | ExportPDF, ReadGeneralData                                                                           |
      | CannotReadSections           | 2020000004 | Delete, Activate, ExportPDF, ReadGeneralData                                                         |
      | CannotReadSections           | 2020000002 | ExportPDF, ReadGeneralData                                                                           |

   # EBS-5557
   # EBS-6763
  Scenario Outline: (02) Read allowed actions in Goods Issue Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of GoodsIssue with code "<GICode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User              | GICode     |
      | Ahmed.Al-Ashry    | 2019000004 |
      | Ahmed.Al-Ashry    | 2019000001 |
      | Ahmed.Al-Ashry    | 2020000004 |
      | Ahmed.Al-Ashry    | 2020000002 |
      | Mahmoud.Abdelaziz | 2019000004 |
      | Mahmoud.Abdelaziz | 2019000001 |
      | Mahmoud.Abdelaziz | 2020000004 |
      | Mahmoud.Abdelaziz | 2020000002 |
      | Afaf              | 2019000002 |
      | Afaf              | 2019000003 |
      | Afaf              | 2019000004 |
      | Afaf              | 2019000001 |
      | Afaf              | 2020000001 |
      | Afaf              | 2020000002 |
      | Afaf              | 2020000003 |
      | Afaf              | 2020000004 |

  # EBS-5557
  Scenario: (03) Read allowed actions in Goods Issue Object Screen - Object does not exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GoodsIssue with code "2019000002" successfully
    When "Mahmoud.Abdelaziz" requests to read actions of GoodsIssue with code "2019000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"
