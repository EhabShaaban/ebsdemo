Feature: Create Initial Stock Upload - Happy Path

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole            |
      | Storekeeper_Signmedia | ISUOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                | Condition |
      | ISUOwner_Signmedia | InitialStockUpload:Create |           |
    #@INSERT
    And the following GeneralData for InitialStockUploads exist:
      | Code       | Type | BusinessUnit     | CreatedBy | CreationDate         | State |
      | 2021000004 | ISU  | 0002 - Signmedia | Admin     | 01-Jan-2021 09:02 AM | Draft |
    #@INSERT
    And the following CompanyData for InitialStockUploads exist:
      | Code       | Company          | Storehouse                 | Storekeeper              |
      | 2021000004 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    #@INSERT
    And Last created InitialStockUpload has code "2021000004"
    And the following IVRs exist:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price | CurrencyCode | PurchaseUnitCode |
      | 000054  | 000001   | 000002     | 0043    | GDF530-440       | 12843            | 50    | 0001         | 0002             |
      | 000055  | 000002   | 000002     | 0043    | GDB550-610       | 20090            | 100   | 0001         | 0002             |

  #EBS-1420 #EBS-8102, #EBS-8745
  Scenario: (01) Create InitialStockUpload - with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CurrentDateTime = "31-Jan-2021 11:00 AM"
    And Last created InitialStockUpload was with code "2021000004"
    When "Mahmoud.Abdelaziz" creates InitialStockUpload with the following values:
      | BusinessUnit     | Company          | Storehouse                 | Storekeeper              | Attachment        |
      | 0002 - Signmedia | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz | initial-stock.csv |
    Then a new InitialStockUpload is created with the following values:
      | Code       | State | CreatedBy         | LastUpdatedBy     | CreationDate         | LastUpdateDate       | Storekeeper              |
      | 2021000005 | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | 31-Jan-2021 11:00 AM | 31-Jan-2021 11:00 AM | 0007 - Mahmoud.Abdelaziz |
    And the Company Section of InitialStockUpload with code "2021000005" is created as follows:
      | Company          | BusinessUnit     | Storehouse                 |
      | 0001 - AL Madina | 0002 - Signmedia | 0001 - AlMadina Main Store |
    And the Items Section of InitialStockUpload with code "2021000005" is created as follows:
      | OriginalAddingDate | StockType        | Item                                         | UOM                    | Qty        | EstimateCost | ActualCost  | RemainingQty |
      | 27-Feb-2020        | UNRESTRICTED_USE | 000001 - Hot Laminated Frontlit Fabric roll  | 0043 - Bottle-2.5Liter | 100.123456 |              | 1802.742467 | 100.123456   |
      | 16-Feb-2020        | DAMAGED_STOCK    | 000002 - Hot Laminated Frontlit Backlit roll | 0043 - Bottle-2.5Liter | 110.123456 | 1041.504217  |             | 110.123456   |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"
