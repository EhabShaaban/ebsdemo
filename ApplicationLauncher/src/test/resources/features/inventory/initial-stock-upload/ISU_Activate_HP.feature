Feature: Activate Initial Stock Upload - Happy Path

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole            |
      | Storekeeper_Signmedia | ISUOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                  | Condition                      |
      | ISUOwner_Signmedia | InitialStockUpload:Activate | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for InitialStockUploads exist:
      | Code       | Type | BusinessUnit     | CreatedBy | CreationDate         | State |
      | 2021000001 | ISU  | 0002 - Signmedia | Admin     | 01-Jan-2021 09:02 AM | Draft |
    #@INSERT

    And the following CompanyData for InitialStockUploads exist:
      | Code       | Company          | Storehouse                 | Storekeeper              |
      | 2021000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    #@INSERT
    And the following ItemsData for InitialStockUploads exist:
      | Code       | OriginalAddingDate | StockType        | Item          | UOM          | Qty       | EstimateCost | ActualCost  | RemainingQty |
      | 2021000001 | 31-Jan-2021        | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 10.123456 |              | 1802.742467 | 9.123456     |
      | 2021000001 | 31-Jan-2021        | DAMAGED_STOCK    | 000051 - Ink5 | 0014 - Liter | 10.123456 | 1041.504217  |             | 9.123456     |
    #@INSERT
    And insert the following StockAvailabilities exist:
      | Code                                    | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item          | UOM          | StockType     | AvailableQuantity |
      | DAMAGED_STOCK00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0014 - Liter | DAMAGED_STOCK | 10.532535353      |
  #EBS-1420 #EBS-4340 #EBS-8849, #EBS-8730
  Scenario: (01) Activate InitialStockUpload - by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And the Last StoreTransaction code is "2020000033"
    When "Mahmoud.Abdelaziz" activates InitialStockUpload with code "2021000001" at "31-Jan-2021 11:00 AM"
    Then InitialStockUpload with code "2021000001" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 31-Jan-2021 11:00 AM | Active |
        #TODO: Fix Store Transactions values when refactoring quantity/monetary values to be bigDecimal
    And the following StoreTransactions are created:
      | STCode     | CreationDate         | OriginalAddingDate   | TransactionType | Company          | Plant                    | Storehouse                 | BusinessUnit     | StockType        | Item          | UOM          | Qty       | EstimateCost | ActualCost  | ReferenceDocument                 | RemainingQty | RefTransaction |
      | 2021000001 | 31-Jan-2021 11:00 AM | 31-Jan-2021 12:00 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | UNRESTRICTED_USE | 000051 - Ink5 | 0014 - Liter | 10.123456 | 0.00         | 1802.742467 | 2021000001 - Initial Stock Upload | 9.123456     |                |
      | 2021000002 | 31-Jan-2021 11:00 AM | 31-Jan-2021 12:00 AM | ADD             | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0002 - Signmedia | DAMAGED_STOCK    | 000051 - Ink5 | 0014 - Liter | 10.123456 | 1041.504217  | 0.00        | 2021000001 - Initial Stock Upload | 9.123456     |                |
    And the following StockAvailabilities will be created or updated:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                 | Item          | UOM          | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0014 - Liter | UNRESTRICTED_USE | 9.123456          |
      | DAMAGED_STOCK00020001000100010000510014    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 000051 - Ink5 | 0014 - Liter | DAMAGED_STOCK    | 19.655991353      |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-54"
