Feature: Read Allowed Actions for InitialStockUpload Object Screen

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ahmed.Seif        | Quality_Specialist    |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole             |
      | Quality_Specialist    | ISUViewer           |
      | Storekeeper_Signmedia | ISUOwner_Signmedia  |
      | Storekeeper_Signmedia | ISUViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                  | Condition                      |
      | ISUViewer           | InitialStockUpload:ReadAll  |                                |
      | ISUOwner_Signmedia  | InitialStockUpload:Activate | [purchaseUnitName='Signmedia'] |
      | ISUViewer_Signmedia | InitialStockUpload:ReadAll  | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User       | Permission                  |
      | Ahmed.Seif | InitialStockUpload:Activate |
      | Afaf       | InitialStockUpload:Activate |
      | Afaf       | InitialStockUpload:ReadAll  |

    And the following users have the following permissions without the following conditions:
      | User              | Permission                  | Condition                  |
      | Mahmoud.Abdelaziz | InitialStockUpload:Activate | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | InitialStockUpload:ReadAll  | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for InitialStockUploads exist:
      | Code       | Type | BusinessUnit     | CreatedBy | CreationDate         | State  |
      | 2021000002 | ISU  | 0002 - Signmedia | Admin     | 09-Jan-2021 09:00 AM | Active |
      | 2021000001 | ISU  | 0002 - Signmedia | Admin     | 09-Jan-2021 09:00 AM | Draft  |
       #@INSERT
    And the following CompanyData for InitialStockUploads exist:
      | Code       | Company          | Storehouse                 | Storekeeper              |
      | 2021000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |

  # EBS-4309
  Scenario Outline: (01) Read allowed actions in InitialStockUpload Object Screen (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of InitialStockUpload with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User              | Code       | AllowedActions    | NoOfActions |
      | Mahmoud.Abdelaziz | 2021000001 | ReadAll, Activate | 2           |
      | Mahmoud.Abdelaziz | 2021000002 | ReadAll           | 1           |
      | Ahmed.Seif        | 2021000001 | ReadAll           | 1           |

  # EBS-4309
  Scenario: (02) Read allowed actions in InitialStockUpload Object Screen, - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of InitialStockUpload with code "2021000001"
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
