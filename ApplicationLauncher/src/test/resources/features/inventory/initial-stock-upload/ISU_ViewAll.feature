Feature: View All Initial Stock Uploads

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Ahmed.Seif        | Quality_Specialist    |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole             |
      | Quality_Specialist    | ISUViewer           |
      | Storekeeper_Signmedia | ISUViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                 | Condition                      |
      | ISUViewer           | InitialStockUpload:ReadAll |                                |
      | ISUViewer_Signmedia | InitialStockUpload:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission                 |
      | Afaf | InitialStockUpload:ReadAll |
    #@INSERT
    And the following GeneralData for InitialStockUploads exist:
      | Code       | Type | BusinessUnit     | CreatedBy | CreationDate         | State |
      | 2021000004 | ISU  | 0002 - Signmedia | Admin     | 01-Jan-2021 09:02 AM | Draft |
      | 2021000003 | ISU  | 0002 - Signmedia | Admin     | 06-Jan-2021 09:00 AM | Draft |
      | 2021000002 | ISU  | 0002 - Signmedia | Admin     | 09-Jan-2021 09:00 AM | Draft |
      | 2021000001 | ISU  | 0001 - Flexo     | Admin     | 10-Jan-2021 11:35 AM | Draft |
    #@INSERT
    And the following CompanyData for InitialStockUploads exist:
      | Code       | Company          | Storehouse                 | Storekeeper              |
      | 2021000004 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000003 | 0002 - DigiPro   | 0003 - DigiPro Main Store  | 0007 - Mahmoud.Abdelaziz |
      | 2021000002 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000001 | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |

  Scenario: (01) View all InitialStockUploads - by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of InitialStockUploads with no filter applied with 4 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | CreationDate         | CreatedBy | BusinessUnit | Company          | Storehouse          | State |
      | 2021000004 | 01-Jan-2021 09:02 AM | Admin     | Signmedia    | 0001 - AL Madina | AlMadina Main Store | Draft |
      | 2021000003 | 06-Jan-2021 09:00 AM | Admin     | Signmedia    | 0002 - DigiPro   | DigiPro Main Store  | Draft |
      | 2021000002 | 09-Jan-2021 09:00 AM | Admin     | Signmedia    | 0001 - AL Madina | AlMadina Main Store | Draft |
      | 2021000001 | 10-Jan-2021 11:35 AM | Admin     | Flexo        | 0001 - AL Madina | AlMadina Main Store | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 4
#
  Scenario: (02) View all InitialStockUploads - by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of InitialStockUploads with no filter applied with 20 records per page
    Then the following values will be presented to "Mahmoud.Abdelaziz":
      | Code       | CreationDate         | CreatedBy | BusinessUnit | Company          | Storehouse          | State |
      | 2021000004 | 01-Jan-2021 09:02 AM | Admin     | Signmedia    | 0001 - AL Madina | AlMadina Main Store | Draft |
      | 2021000003 | 06-Jan-2021 09:00 AM | Admin     | Signmedia    | 0002 - DigiPro   | DigiPro Main Store  | Draft |
      | 2021000002 | 09-Jan-2021 09:00 AM | Admin     | Signmedia    | 0001 - AL Madina | AlMadina Main Store | Draft |
    And the total number of records in search results by "Mahmoud.Abdelaziz" are 3

  Scenario: (03) View all InitialStockUploads - Filter by Code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of InitialStockUploads with no filter applied with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page