Feature: Create Initial Stock Upload - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                                               | Role                                                                              |
      | Mahmoud.Abdelaziz                                                  | Storekeeper_Signmedia                                                             |
      | Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper | Storekeeper_Signmedia_CannotReadBusinessUnitAndCompanyAndStorehouseAndStorekeeper |
    And the following roles and sub-roles exist:
      | Role                                                                              | Subrole                 |
      | Storekeeper_Signmedia                                                             | ISUOwner_Signmedia      |
      | Storekeeper_Signmedia                                                             | PurUnitReader_Signmedia |
      | Storekeeper_Signmedia                                                             | CompanyViewer           |
      | Storekeeper_Signmedia                                                             | StorehouseViewer        |
      | Storekeeper_Signmedia                                                             | StorekeeperViewer       |
      | Storekeeper_Signmedia_CannotReadBusinessUnitAndCompanyAndStorehouseAndStorekeeper | ISUOwner_Signmedia      |
      | Storekeeper_Signmedia_CannotReadBusinessUnitAndCompanyAndStorehouseAndStorekeeper | ISUViewer_Signmedia     |

    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | ISUOwner_Signmedia      | InitialStockUpload:Create    |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | CompanyViewer           | Company:ReadAll              |                                |
      | StorehouseViewer        | Storehouse:ReadAll           |                                |
      | StorekeeperViewer       | Storekeeper:ReadAll          |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                   | Condition                  |
      | Mahmoud.Abdelaziz | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                                                               | Permission                   |
      | Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper | PurchasingUnit:ReadAll_ForPO |
      | Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper | Company:ReadAll              |
      | Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper | Storehouse:ReadAll           |
      | Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper | Storekeeper:ReadAll          |

  #EBS-2580
  Scenario: (01) Create InitialStockUpload, by an authorized user with authorized reads
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to create InitialStockUpload
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads   |
      | ReadBusinessUnits |
      | ReadCompanies     |
      | ReadStoreHouses   |
      | ReadStoreKeepers  |

  #EBS-2580
  Scenario: (02) Create InitialStockUpload, by an authorized user with no authorized reads
    Given user is logged in as "Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper"
    When "Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper" requests to create InitialStockUpload
    Then there are no authorized reads returned to "Mahmoud.Abdelaziz.NoBusinessUnitNoCompanyNoStorehouseNoStorekeeper"

  #EBS-2580
  Scenario: (03) Create InitialStockUpload by an unauthorized user: user is not authorized to create (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates InitialStockUpload with the following values:
      | BusinessUnit     | Company          | Storehouse                 | Storekeeper              | Attachment        |
      | 0002 - Signmedia | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz | initial-stock.csv |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-2580
  Scenario: (04) Create InitialStockUpload by user chooses BusinessUnit s/he is not authorized to read (Unauthorized/Abuse Case because user violates read permission of business unit)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates InitialStockUpload with the following values:
      | BusinessUnit | Company          | Storehouse                 | Storekeeper              | Attachment        |
      | 0001 - Flexo | 0001 - AL Madina | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz | initial-stock.csv |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page