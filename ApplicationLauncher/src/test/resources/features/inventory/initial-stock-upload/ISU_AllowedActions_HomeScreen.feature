Feature: Read Allowed Actions for InitialStockUpload Home Screen

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ahmed.Seif        | Quality_Specialist    |
      | CreateOnlyUser    | CreateOnlyRole        |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole             |
      | Quality_Specialist    | ISUViewer           |
      | Storekeeper_Signmedia | ISUOwner_Signmedia  |
      | Storekeeper_Signmedia | ISUViewer_Signmedia |
      | CreateOnlyRole        | CreateOnlySubRole   |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                 | Condition                      |
      | ISUViewer           | InitialStockUpload:ReadAll |                                |
      | ISUOwner_Signmedia  | InitialStockUpload:Create  |                                |
      | ISUViewer_Signmedia | InitialStockUpload:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole   | InitialStockUpload:Create  |                                |
    And the following users doesn't have the following permissions:
      | User           | Permission                 |
      | Ahmed.Seif     | InitialStockUpload:Create  |
      | CreateOnlyUser | InitialStockUpload:ReadAll |
      | Afaf           | InitialStockUpload:Create  |
      | Afaf           | InitialStockUpload:ReadAll |

  #EBS-4300
  Scenario Outline: (01) Read ISU Home Screen Allowed Actions by an authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of InitialStockUpload home screen
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User              | AllowedActions | NoOfActions |
      | Mahmoud.Abdelaziz | ReadAll,Create | 2           |
      | Ahmed.Seif        | ReadAll        | 1           |
      | CreateOnlyUser    | Create         | 1           |

  #EBS-4300
  Scenario: (02) Read ISU Home Screen Allowed Actions by an unauthorized user (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of InitialStockUpload home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"