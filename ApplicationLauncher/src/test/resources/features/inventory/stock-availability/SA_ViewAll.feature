# Updated by: Niveen Magdy and Eman Hesham, Reviewed by: Somaya Ahmed (EBS-6448)

Feature: View All StockAvailability

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Ahmed.Seif        | Quality_Specialist    |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                           |
      | Quality_Specialist    | StockAvailabilityViewer           |
      | Quality_Specialist    | StockTypesViewer                  |
      | Quality_Specialist    | CompanyViewer                     |
      | Quality_Specialist    | PurUnitReader_Signmedia           |
      | Storekeeper_Signmedia | StockAvailabilityViewer_Signmedia |
      | Storekeeper_Signmedia | StockTypesViewer                  |
      | Storekeeper_Signmedia | CompanyViewer                     |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia           |
    And the following sub-roles and permissions exist:
      | Subrole                           | Permission                | Condition                      |
      | StockAvailabilityViewer           | StockAvailability:ReadAll |                                |
      | StockAvailabilityViewer_Signmedia | StockAvailability:ReadAll | [purchaseUnitName='Signmedia'] |
      | StockTypesViewer                  | StockType:ReadAll         |                                |
      | CompanyViewer                     | Company:ReadAll           |                                |
      | PurUnitReader_Signmedia           | PurchasingUnit:ReadAll    |                                |
    And the following users doesn't have the following permissions:
      | User | Permission                |
      | Afaf | StockAvailability:ReadAll |
    And insert the following StockAvailabilities exist:
      | Code                                       | BusinessUnit     | Company          | Plant                    | Storehouse                      | Item                                          | UOM                  | StockType        | AvailableQuantity |
      | UNRESTRICTED_USE00020001000100010000540019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2            | UNRESTRICTED_USE | 149.123456789     |
      | UNRESTRICTED_USE00020001000100010000530035 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  | UNRESTRICTED_USE | 19                |
      | UNRESTRICTED_USE00020002000200030000530019 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | UNRESTRICTED_USE | 2                 |
      | DAMAGED_STOCK00020002000200030000530019    | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | DAMAGED_STOCK    | 13                |
      | UNRESTRICTED_USE00020001000100020000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | UNRESTRICTED_USE | 100               |
      | UNRESTRICTED_USE00020001000100010000530019 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | UNRESTRICTED_USE | 110               |
      | UNRESTRICTED_USE00020001000100010000510040 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000051 - Ink5                                 | 0040 - Bottle-1Liter | UNRESTRICTED_USE | 1                 |
      | UNRESTRICTED_USE00020002000200030000510014 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 000051 - Ink5                                 | 0014 - Liter         | UNRESTRICTED_USE | 10                |
      | UNRESTRICTED_USE00020001000100010000510014 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000051 - Ink5                                 | 0014 - Liter         | UNRESTRICTED_USE | 26                |
      | DAMAGED_STOCK00020001000100010000510014    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000051 - Ink5                                 | 0014 - Liter         | DAMAGED_STOCK    | 2                 |
      | UNRESTRICTED_USE00010002000200030000510014 | 0001 - Flexo     | 0002 - DigiPro   | 0002 - DigiPro Plant     | 0003 - DigiPro Main Store       | 000051 - Ink5                                 | 0014 - Liter         | UNRESTRICTED_USE | 3                 |
      | UNRESTRICTED_USE00010001000100010000510014 | 0001 - Flexo     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000051 - Ink5                                 | 0014 - Liter         | UNRESTRICTED_USE | 3                 |
      | DAMAGED_STOCK00010001000100010000510014    | 0001 - Flexo     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000051 - Ink5                                 | 0014 - Liter         | DAMAGED_STOCK    | 5                 |
      | UNRESTRICTED_USE00010001000100020000120014 | 0001 - Flexo     | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 000012 - Ink2                                 | 0014 - Liter         | UNRESTRICTED_USE | 1002              |
      | UNRESTRICTED_USE00020001000100010000080030 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000008 - PRO-V-ST-2                           | 0030 - PC            | UNRESTRICTED_USE | 0                 |
      | UNRESTRICTED_USE00020001000100010000020029 | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50  | UNRESTRICTED_USE | 11                |
      | DAMAGED_STOCK00020001000100010000020029    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50  | DAMAGED_STOCK    | 10.5              |
      | DAMAGED_STOCK00020001000100020000010025    | 0002 - Signmedia | 0001 - AL Madina | 0001 - Madina Tech Plant | 0002 - AlMadina Secondary Store | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50  | DAMAGED_STOCK    | 40                |

  Scenario: (01) View all StockAvailabilities - by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with no filter applied with 4 records per page
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM                 | PurchaseUnit     | Storehouse                 | Company          | Plant                    | StockType        | TotalQty |
      | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2           | 0002 - Signmedia | 0001 - AlMadina Main Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 149.123456789      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 0002 - Signmedia | 0001 - AlMadina Main Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 19       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0003 - DigiPro Main Store  | 0002 - DigiPro   | 0002 - DigiPro Plant     | UNRESTRICTED_USE | 2        |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0003 - DigiPro Main Store  | 0002 - DigiPro   | 0002 - DigiPro Plant     | DAMAGED_STOCK    | 13       |
    And the total number of records in search results by "Ahmed.Seif" are 18

  Scenario: (02) View all StockAvailabilities - by an authorized user WITH condition - No Paging (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read page 1 of StockAvailabilities with no filter applied with 20 records per page
    Then the following StockAvailabilities values will be presented to "Mahmoud.Abdelaziz":
      | Item                                          | UOM                  | PurchaseUnit     | Storehouse                      | Company          | Plant                    | StockType        | TotalQty |
      | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2            | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 149.123456789      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50  | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 19       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | UNRESTRICTED_USE | 2        |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | DAMAGED_STOCK    | 13       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 100      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2            | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 110      |
      | 000051 - Ink5                                 | 0040 - Bottle-1Liter | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 1        |
      | 000051 - Ink5                                 | 0014 - Liter         | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | UNRESTRICTED_USE | 10       |
      | 000051 - Ink5                                 | 0014 - Liter         | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 26       |
      | 000051 - Ink5                                 | 0014 - Liter         | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 2        |
      | 000008 - PRO-V-ST-2                           | 0030 - PC            | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 0        |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50  | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 11       |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50  | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 10.5     |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50  | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 40       |

    And the total number of records in search results by "Mahmoud.Abdelaziz" are 14

  Scenario: (03) View all StockAvailabilities - Filter StockAvailabilities by ItemCode by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on Item which contains "00002" with 20 records per page while current locale is "en-US"
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                         | UOM                 | PurchaseUnit     | Storehouse                 | Company          | Plant                    | StockType        | TotalQty |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 11       |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 10.5     |
    And the total number of records in search results by "Ahmed.Seif" are 2

  Scenario: (04) View all StockAvailabilities - Filter StockAvailabilities by ItemName by authorized user using "Contains"  (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on Item which contains "Fabric" with 20 records per page while current locale is "en-US"
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM                 | PurchaseUnit     | Storehouse                      | Company          | Plant                    | StockType        | TotalQty |
      | 000054 - Hot Laminated Frontlit Fabric roll 3 | 0019 - M2           | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 149.123456789      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 19       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | UNRESTRICTED_USE | 2        |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | DAMAGED_STOCK    | 13       |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 100      |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 110      |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50 | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 40       |
    And the total number of records in search results by "Ahmed.Seif" are 7

  Scenario: (05) View all StockAvailabilities - Filter StockAvailabilities by UOM by authorized user using "Contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on UOM which contains "Roll" with 20 records per page while current locale is "en-US"
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM                 | PurchaseUnit     | Storehouse                      | Company          | Plant                    | StockType        | TotalQty |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 19       |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 11       |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 10.5     |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50 | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 40       |
    And the total number of records in search results by "Ahmed.Seif" are 4

  Scenario: (06) View all StockAvailabilities - Filter StockAvailabilities by TotalQty by authorized user using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on TotalQty which equals "11" with 20 records per page
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                         | UOM                 | PurchaseUnit     | Storehouse                 | Company          | Plant                    | StockType        | TotalQty |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 11       |
    And the total number of records in search results by "Ahmed.Seif" are 1

  Scenario: (07) View all StockAvailabilities - Filter StockAvailabilities by StockType by authorized user using "Equals" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on StockType which equals "DAMAGED_STOCK" with 20 records per page while current locale is "en-US"
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM                 | PurchaseUnit     | Storehouse                      | Company          | Plant                    | StockType     | TotalQty |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | DAMAGED_STOCK | 13       |
      | 000051 - Ink5                                 | 0014 - Liter        | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK | 2        |
      | 000051 - Ink5                                 | 0014 - Liter        | 0001 - Flexo     | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK | 5        |
      | 000002 - Hot Laminated Frontlit Backlit roll  | 0029 - Roll 2.20x50 | 0002 - Signmedia | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK | 10.5     |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50 | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK | 40       |
    And the total number of records in search results by "Ahmed.Seif" are 5

  Scenario: (08) View all StockAvailabilities - Filter StockAvailabilities by PurchaseUnit by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on PurchaseUnit which equals "0001" with 5 records per page
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item          | UOM          | PurchaseUnit | Storehouse                      | Company          | Plant                    | StockType        | TotalQty |
      | 000051 - Ink5 | 0014 - Liter | 0001 - Flexo | 0003 - DigiPro Main Store       | 0002 - DigiPro   | 0002 - DigiPro Plant     | UNRESTRICTED_USE | 3        |
      | 000051 - Ink5 | 0014 - Liter | 0001 - Flexo | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 3        |
      | 000051 - Ink5 | 0014 - Liter | 0001 - Flexo | 0001 - AlMadina Main Store      | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 5        |
      | 000012 - Ink2 | 0014 - Liter | 0001 - Flexo | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 1002     |
    And the total number of records in search results by "Ahmed.Seif" are 4

  Scenario: (09) View all StockAvailabilities - Filter StockAvailabilities by Storehouse by authorized user using "contains" (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on Storehouse which contains "Secondary" with 20 records per page while current locale is "en-US"
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM                 | PurchaseUnit     | Storehouse                      | Company          | Plant                    | StockType        | TotalQty |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2           | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 100      |
      | 000012 - Ink2                                 | 0014 - Liter        | 0001 - Flexo     | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | UNRESTRICTED_USE | 1002     |
      | 000001 - Hot Laminated Frontlit Fabric roll   | 0025 - Roll 2.20 50 | 0002 - Signmedia | 0002 - AlMadina Secondary Store | 0001 - AL Madina | 0001 - Madina Tech Plant | DAMAGED_STOCK    | 40       |
    And the total number of records in search results by "Ahmed.Seif" are 3

  Scenario: (10) View all StockAvailabilities - Filter StoreTransactions by Company by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of StockAvailabilities with filter applied on Company which equal "0002" with 50 records per page
    Then the following StockAvailabilities values will be presented to "Ahmed.Seif":
      | Item                                          | UOM          | PurchaseUnit     | Storehouse                | Company        | Plant                | StockType        | TotalQty |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 0002 - Signmedia | 0003 - DigiPro Main Store | 0002 - DigiPro | 0002 - DigiPro Plant | UNRESTRICTED_USE | 2        |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2    | 0002 - Signmedia | 0003 - DigiPro Main Store | 0002 - DigiPro | 0002 - DigiPro Plant | DAMAGED_STOCK    | 13       |
      | 000051 - Ink5                                 | 0014 - Liter | 0002 - Signmedia | 0003 - DigiPro Main Store | 0002 - DigiPro | 0002 - DigiPro Plant | UNRESTRICTED_USE | 10       |
      | 000051 - Ink5                                 | 0014 - Liter | 0001 - Flexo     | 0003 - DigiPro Main Store | 0002 - DigiPro | 0002 - DigiPro Plant | UNRESTRICTED_USE | 3        |
    And the total number of records in search results by "Ahmed.Seif" are 4

  Scenario: (11) View all StockAvailabilities - Filter View All StockAvailabilities by Code by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of StockAvailabilities with filter applied on Item which contains "00001" with 3 records per page while current locale is "en-US"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
