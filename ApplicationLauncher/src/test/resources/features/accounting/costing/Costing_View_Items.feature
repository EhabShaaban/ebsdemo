Feature: Costing - View Items Section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | Accountant_Flexo     |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                 |
      | Accountant_Signmedia | CostingViewer_Signmedia |
      | Accountant_Flexo     | CostingViewer_Flexo     |
      | SuperUser            | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission        | Condition                      |
      | CostingViewer_Signmedia | Costing:ReadItems | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Flexo     | Costing:ReadItems | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole        | *:*               |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission        | Condition                      |
      | Ahmed.Hamdi | Costing:ReadItems | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Jan-2021 09:00 AM | User Daily Rate |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                      | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Arrived                    | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
      | 2021000010 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000010 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000010 | 2021000004    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
    And insert the following GoodsReceipt Items:
      | Code       | Item                                         | DifferenceReason | Type |
      | 2021000009 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000009 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |
      | 2021000010 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000010 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |
    And insert the following GoodsReceipt ItemQuantities:
      | Code       | Item                                         | UoE         | ReceivedQty(UoE) | StockType        | Notes |
      | 2021000009 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 100              | UNRESTRICTED_USE |       |
      | 2021000009 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 50               | UNRESTRICTED_USE |       |
      | 2021000010 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 100              | UNRESTRICTED_USE |       |
      | 2021000010 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 50               | UNRESTRICTED_USE |       |

    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Active |
      | 2021000002 | Admin     | 10-Mar-2021 9:02 AM | Active |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |             |
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000009   | 11.44 EGP                 |                         |
      | 2021000002 | 2021000010   |                           | 11.44 EGP               |
    And insert the following Costing Items:
      | Code       | Item                                         | UOM         | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | .40123456        | 100.123456       | UNRESTRICTED_USE | 25.123456 EGP       |                   | 2000.123456 EGP          |                        |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | .20123456        | 50.123456        | UNRESTRICTED_USE | 15.123456 EGP       |                   | 1000.123456 EGP          |                        |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | .40123456        | 100.123456       | UNRESTRICTED_USE |                     | 25.123456 EGP     |                          | 2000.123456 EGP        |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | .20123456        | 50.123456        | UNRESTRICTED_USE |                     | 15.123456 EGP     |                          | 1000.123456 EGP        |

  #### Happy Path
  # EBS-8281
  Scenario: (01) View Costing Items section - for estimate landed cost by an authorized user with one role with condition or without condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Items section for Costing with code "2021000001"
    Then the following values of Items section for Costing with code "2021000001" are displayed to "Shady.Abdelatif":
      | Code       | Item                                         | UOM         | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) | ItemTotalCost(Estimate) | ItemTotalCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 40.12345600      | 100.123456       | UNRESTRICTED_USE | 25.123456 EGP       |                   | 2000.123456 EGP          |                        | 202774.720082767872 EGP |                       |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 20.12345600      | 50.123456        | UNRESTRICTED_USE | 15.123456 EGP       |                   | 1000.123456 EGP          |                        | 50887.683922767872 EGP  |                       |


 # EBS-9197
  Scenario: (02) View Costing Items section - for actual landed cost  by an authorized user with one role with condition or without condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Items section for Costing with code "2021000002"
    Then the following values of Items section for Costing with code "2021000002" are displayed to "Shady.Abdelatif":
      | Code       | Item                                         | UOM         | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) | ItemTotalCost(Estimate) | ItemTotalCost(Actual)   |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 40.12345600      | 100.123456       | UNRESTRICTED_USE |                     | 25.123456 EGP     |                          | 2000.123456 EGP        |                         | 202774.720082767872 EGP |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 20.12345600      | 50.123456        | UNRESTRICTED_USE |                     | 15.123456 EGP     |                          | 1000.123456 EGP        |                         | 50887.683922767872 EGP  |

  #### Exceptions
  # EBS-8281
  @Future
  Scenario: (03) View Costing Items section - where Costing has been deleted by another user (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Costing with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view Items section for Costing with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #### Abuse Cases
  # EBS-8281
  Scenario: (04)  View Costing Items section - by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view Items section for Costing with code "2021000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page
