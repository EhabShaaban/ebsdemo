Feature: Costing Object Screen Allowed Actions

  Background:
    Given the following users and roles exist:
      | Name               | Role                   |
      | Shady.Abdelatif    | Accountant_Signmedia   |
      | Ahmed.Hamdi        | Accountant_Flexo       |
      | CannotReadSections | CannotReadSectionsRole |
      | hr1                | SuperUser              |

    And the following roles and sub-roles exist:
      | Role                   | Subrole                        |
      | Accountant_Signmedia   | CostingActivateOwner_Signmedia |
      | Accountant_Flexo       | CostingActivateOwner_Flexo     |
      | CannotReadSectionsRole | CannotReadSectionsSubRole      |
      | SuperUser              | SuperUserSubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                                      | Condition                      |
      | CostingViewer_Signmedia        | Costing:ReadAll                                 | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadGeneralData                         | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadCompany                             | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadAccountingDetails                   | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadDetails                             | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Signmedia        | Costing:ReadItems                               | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Flexo            | Costing:ReadAll                                 | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadGeneralData                         | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadCompany                             | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadAccountingDetails                   | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadDetails                             | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Flexo']     |
      | CostingViewer_Flexo            | Costing:ReadItems                               | [purchaseUnitName='Flexo']     |
      | CostingViewer                  | Costing:ReadAll                                 |                                |
      | CostingViewer                  | Costing:ReadGeneralData                         |                                |
      | CostingViewer                  | Costing:ReadCompany                             |                                |
      | CostingViewer                  | Costing:ReadAccountingDetails                   |                                |
      | CostingViewer                  | Costing:ReadDetails                             |                                |
      | CostingViewer                  | Costing:ReadAccountingDocumentActivationDetails |                                |
      | CostingViewer                  | Costing:ReadItems                               |                                |
      | CostingActivateOwner_Signmedia | Costing:Activate                                | [purchaseUnitName='Signmedia'] |
      | CostingActivateOwner_Flexo     | Costing:Activate                                | [purchaseUnitName='Flexo']     |
      | CannotReadSectionsSubRole      | Costing:Activate                                |                                |
      | CannotReadSectionsSubRole      | Costing:ReadAll                                 |                                |
      | SuperUserSubRole               | *:*                                             |                                |

    And the following users doesn't have the following permissions:
      | User               | Permission                                      |
      | CannotReadSections | Costing:ReadGeneralData                         |
      | CannotReadSections | Costing:ReadCompany                             |
      | CannotReadSections | Costing:ReadAccountingDetails                   |
      | CannotReadSections | Costing:ReadDetails                             |
      | CannotReadSections | Costing:ReadAccountingDocumentActivationDetails |
      | CannotReadSections | Costing:ReadItems                               |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                                      | Condition                      |
      | Ahmed.Hamdi     | Costing:ReadAll                                 | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadGeneralData                         | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadCompany                             | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadAccountingDetails                   | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadDetails                             | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |
      | Ahmed.Hamdi     | Costing:ReadItems                               | [purchaseUnitName='Signmedia'] |
      | Shady.Abdelatif | Costing:ReadAll                                 | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadGeneralData                         | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadCompany                             | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadAccountingDetails                   | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadDetails                             | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Flexo']     |
      | Shady.Abdelatif | Costing:ReadItems                               | [purchaseUnitName='Flexo']     |

    #Costing with code "2021000001" draft Signmedia with PO 2021000003 , GR 2021000009
    #Costing with code "2021000002" Active Signmedia with PO 2021000004, GR 2021000010
    #Costing with code "2021000003" Draft Flexo with PO 2021000005, GR 2021000011


    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
      | 2021000010 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
      | 2021000011 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0001 - Flexo     |
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000010 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000011 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000010 | 2021000004    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000011 | 2021000005    | 000002 - Zhejiang | Amr Khalil            | 123456          |       |

    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Draft  |
      | 2021000002 | Admin     | 10-Mar-2021 9:02 AM | Active |
      | 2021000003 | Admin     | 10-Mar-2021 9:02 AM | Draft  |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |             |
      | 2021000003 | 0001 - Flexo     | 0001 - AL Madina |             |
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000009   | 11.44 EGP                 |                         |
      | 2021000002 | 2021000010   | 11.44 EGP                 |                         |
      | 2021000003 | 2021000011   | 11.44 EGP                 |                         |
    And insert the following Costing AccountingDetails:
      | Code       | Credit/Debit | Subledger | Account            | SubAccount | Value |
      | 2021000001 | DEBIT        | NONE      | 70103 - Purchasing |            | 2000  |
      | 2021000001 | CREDIT       | PO        | 10207 - PO         | 2021000003 | 2000  |
      | 2021000002 | DEBIT        | NONE      | 70103 - Purchasing |            | 2000  |
      | 2021000002 | CREDIT       | PO        | 10207 - PO         | 2021000004 | 2000  |
      | 2021000003 | DEBIT        | NONE      | 70103 - Purchasing |            | 2000  |
      | 2021000003 | CREDIT       | PO        | 10207 - PO         | 2021000005 | 2000  |
    And insert the following Costing Items:
      | Code       | Item                                         | UOM         | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 40.123456        | 100.123456       | UNRESTRICTED_USE | 25.123456 EGP       |                   | 2000.123456 EGP          |                        |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 20.123456        | 50.123456        | UNRESTRICTED_USE | 15.123456 EGP       |                   | 1000.123456 EGP          |                        |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 40.123456        | 100.123456       | UNRESTRICTED_USE | 25.123456 EGP       |                   | 2000.123456 EGP          |                        |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 20.123456        | 50.123456        | UNRESTRICTED_USE | 15.123456 EGP       |                   | 1000.123456 EGP          |                        |
      | 2021000003 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2   | 40.123456        | 100.123456       | UNRESTRICTED_USE | 25.123456 EGP       |                   | 2000.123456 EGP          |                        |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll | 0005 - Roll | 20.123456        | 50.123456        | UNRESTRICTED_USE | 15.123456 EGP       |                   | 1000.123456 EGP          |                        |

    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy | JournalDate          | ReferenceDocument    | Company          | BusinessUnit     |
      | 2021000002 | 04-Jan-2021 09:45 AM | Admin     | 10-Mar-2021 09:02 AM | 2021000002 - Costing | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following JournalEntries Details exist:
      | JournalEntryCode | JournalDate          | Subledger | GLAccount          | GLSubAccount | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2021000002       | 10-Mar-2021 00:00 PM |           | 70103 - Purchasing |              | 0      | 2000  | EGP                | EGP               | 1                         | EGP             | 1                       |
      | 2021000002       | 10-Mar-2021 00:00 PM | PO        | 10207 - PO         | 2021000004   | 2000   | 0     | EGP                | EGP               | 1                         | EGP             | 1                       |
    And insert the following Costing ActivationDetails:
      | Code       | Accountant      | ActivationDate       | CurrencyPrice       | JournalEntryCode | FiscalYear |
      | 2021000002 | Shady.Abdelatif | 10-Mar-2021 10:30 AM | 1.0 USD = 11.44 EGP | 2021000001       | 2021       |

    
  #EBS-9205
  Scenario Outline: (01) Read allowed actions in Costing Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Costing with code "<CostingCode>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | CostingCode | AllowedActions                                                                                                                         | NoOfActions |
      | Shady.Abdelatif    | 2021000001  | Activate,ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadDetails, ReadAccountingDocumentActivationDetails, ReadItems | 8           |
      | Shady.Abdelatif    | 2021000002  | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadDetails, ReadAccountingDocumentActivationDetails, ReadItems          | 7           |
      | Ahmed.Hamdi        | 2021000003  | Activate,ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadDetails, ReadAccountingDocumentActivationDetails, ReadItems | 8           |

      | CannotReadSections | 2021000001  | Activate, ReadAll                                                                                                                      | 2           |
      | CannotReadSections | 2021000002  | ReadAll                                                                                                                                | 1           |
      | CannotReadSections | 2021000003  | Activate, ReadAll                                                                                                                      | 2           |

#EBS-9205
  Scenario Outline: (02) Read allowed actions in Costing Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Costing with code "<CostingCode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | CostingCode |
      | Ahmed.Hamdi     | 2021000001  |
      | Shady.Abdelatif | 2021000003  |
      | Ahmed.Hamdi     | 2021000002  |

 