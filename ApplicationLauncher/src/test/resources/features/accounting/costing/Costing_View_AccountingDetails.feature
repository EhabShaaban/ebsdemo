Feature: Costing - View Accounting Details Section for GoodsReceipt

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |
      | hr1               | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole            |
      | Storekeeper_Signmedia | GRViewer_Signmedia |
      | Storekeeper_Flexo     | GRViewer_Flexo     |
      | SuperUser             | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                         | Condition                      |
      | GRViewer_Signmedia | GoodsReceipt:ReadAccountingDetails | [purchaseUnitName='Signmedia'] |
      | GRViewer_Flexo     | GoodsReceipt:ReadAccountingDetails | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                                |                                |
    And the following users have the following permissions without the following conditions:
      | User      | Permission                         | Condition                      |
      | Ali.Hasan | GoodsReceipt:ReadAccountingDetails | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Jan-2021 09:00 AM | User Daily Rate |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
    #@INSERT
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    #@INSERT
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    #@INSERT
    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Active |
    #@INSERT
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    #@INSERT
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000009   |                           | 11.44 EGP               |
    #@INSERT
    And insert the following Costing AccountingDetails:
      | Code       | Credit/Debit | Subledger | Account            | SubAccount | Value                             |
      | 2021000001 | DEBIT        | NONE      | 70103 - Purchasing |            | 2000.1234567899876543211234567898 |
      | 2021000001 | CREDIT       | PO        | 10207 - PO         | 2021000003 | 2000.1234567899876543211234567898 |

  #### Happy Path
  # EBS-8281
  Scenario Outline: (01) View Costing AccountingDetails section for GoodsReceipt - by an authorized user with one role with condition or without condition (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view AccountingDetails section for GoodsReceipt with code "<GRCodeValue>"
    Then the following values of AccountingDetails section for GoodsReceipt with code "<GRCodeValue>" are displayed to "<User>":
      | Credit/Debit | Account         | SubAccount         | Value         |
      | DEBIT        | <DebitAccount>  | <DebitSubAccount>  | <DebitValue>  |
      | CREDIT       | <CreditAccount> | <CreditSubAccount> | <CreditValue> |
    Examples:
      | User              | GRCodeValue | DebitAccount       | DebitSubAccount | CreditAccount | CreditSubAccount | DebitValue                            | CreditValue                           |
      | Mahmoud.Abdelaziz | 2021000009  | 70103 - Purchasing |                 | 10207 - PO    | 2021000003       | 2000.1234567899876543211234567898 EGP | 2000.1234567899876543211234567898 EGP |

  #### Exceptions
  # EBS-8281
  @Future
  Scenario: (02) View Costing AccountingDetails section for GoodsReceipt - where GRAccountingDocument has been deleted by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GRAccountingDocument with code "2021000008" successfully
    When "Mahmoud.Abdelaziz" requests to view AccountingDetails section for GoodsReceipt with code "202100by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view AccountingDetails section for GoodsReceipt with code "2021000101"
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page0008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  #### Abuse Cases
  # EBS-8281
  Scenario: (03)  View Costing AccountingDetails section for GoodsReceipt - by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view AccountingDetails section for GoodsReceipt with code "2021000009"
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page
