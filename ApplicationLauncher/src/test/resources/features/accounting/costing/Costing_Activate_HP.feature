Feature: Costing - Activate HP #EBS-9206

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | CostingActivateOwner_Signmedia |
      | SuperUser            | SuperUserSubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission       | Condition                      |
      | CostingActivateOwner_Signmedia | Costing:Activate | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole               | *:*              |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission       | Condition                      |
      | Ahmed.Hamdi | Costing:Activate | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                      | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000004 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State | BusinessUnit     |
      | 2021000002 | GR_PO | Admin     | 11-Mar-2021 9:02 AM | Draft | 0002 - Signmedia |

    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000002 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |

    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000002 | 2021000004    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    And insert the following GoodsReceipt Items:
      | Code       | Item                                         | DifferenceReason | Type |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |

    And insert the following GoodsReceipt ItemQuantities:
      | Code       | Item                                         | UoE       | ReceivedQty(UoE) | StockType        | Notes |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 50               | UNRESTRICTED_USE |       |

    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy | CreationDate        | DocumentOwner |
      | 2021000002 | IMPORT_GOODS_INVOICE | Posted | A.Hamed   | 24-Apr-2021 8:31 AM | Ashraf Salah  |

    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000002 | 2021000004    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2021 06:31 AM | USD      |

    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 100.00 M2      | 100.00 M2 | 10.00           | 10.00                |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 50.00 M2       | 50.00 M2  | 30.00           | 30.00                |

    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2021000002 | 500.00      | 1500.00        | 2500        |

    #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice       | JournalEntryCode |
      | 2021000002 | Shady.Abdelatif | 22-Jun-2021 09:02 AM | 1.0 USD = 17.44 EGP | 2020000024       |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2021000002 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted | Amr.Khalil | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode       |
      | 2021000002 | 2021000004 | 2021000002        | true        | IMPORT_GOODS_INVOICE |

    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                      | EstimatedValue | ActualValue | Difference  |
      | 2021000002 | 000056 - Shipment service | 1800.00 EGP    | 1170.00 EGP | 630.00 EGP  |
      | 2021000002 | 000057 - Bank Fees        | 20.00 EGP      | 400.00 EGP  | -380.00 EGP |

    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2021000002 | 1820 EGP       | 1570 EGP    | 250 EGP    |

    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Draft |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000002   |                           | 16.392                  |
    And insert the following Costing Items:
      | Code       | Item                                         | UOM       | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate)   | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 40               | 90.0             | UNRESTRICTED_USE | 182.1333333331512 EGP |                   | 8.0888888888 EGP         |                        |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 0                | 10.0             | DAMAGED_STOCK    | 0 EGP                 |                   | 0 EGP                    |                        |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 60               | 50.0             | UNRESTRICTED_USE | 491.760 EGP           |                   | 21.84 EGP                |                        |

  #EBS-9206 EBS-9201 EBS-9202 EBS-9200
  Scenario: (01) Activate Costing - HP
    Given user is logged in as "Shady.Abdelatif"
    And the last created JournalEntry was with code "2021000001"
    When "Shady.Abdelatif" activates Costing Document with code "2021000001" and DueDate "06-Sep-2021 09:30 AM" at "07-Sep-2021 9:30 AM"
    Then Costing Document with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate      | State  |
      | Shady.Abdelatif | 07-Sep-2021 9:30 AM | Active |
    And the Costing Accounting Details of Costing Document is Created as Follows:
      | Code       | Credit/Debit | Account                                  | SubAccount | Value |
      | 2021000001 | DEBIT        | 70103 - Purchasing                       |            | 42550 |
      | 2021000001 | CREDIT       | 10207 - PO                               | 2021000004 | 16392 |
      | 2021000001 | CREDIT       | 10207 - PO                               | 2021000004 | 24588 |
      | 2021000001 | CREDIT       | 10207 - PO                               | 2021000004 | 1570  |
      | 2021000001 | CREDIT       | 10207 - PO                               | 2021000004 | 1048  |
      | 2021000001 | CREDIT       | 10207 - PO                               | 2021000004 | 1570  |
      | 2021000001 | DEBIT        | 41101 - Realized Differences on Exchange |            | 2620  |
    And the following Journal Entry is Created:
      | Code       | CreationDate        | CreatedBy       | JournalDate         | BusinessUnit     | ReferenceDocument    | Company          | FiscalYear |
      | 2021000002 | 07-Sep-2021 9:30 AM | Shady.Abdelatif | 06-Sep-2021 9:30 AM | 0002 - Signmedia | 2021000001 - Costing | 0001 - AL Madina | 2021       |
    And The following Journal Items for Journal Entry with code "2021000002":
      | JournalDate         | GLAccount                                | SubAccount | Credit  | Debit   | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Sep-2021 9:30 AM | 70103 - Purchasing                       |            | 0.0     | 42550.0 | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 10207 - PO                               | 2021000004 | 16392.0 | 0.0     | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 10207 - PO                               | 2021000004 | 24588.0 | 0.0     | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 10207 - PO                               | 2021000004 | 1570.0  | 0.0     | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 10207 - PO                               | 2021000004 | 1048.0  | 0.0     | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 10207 - PO                               | 2021000004 | 1570.0  | 0.0     | EGP                | EGP               | 1.0                       | 2018000013       |
      | 06-Sep-2021 9:30 AM | 41101 - Realized Differences on Exchange |            | 0.0     | 2620.0  | EGP                | EGP               | 1.0                       | 2018000013       |
    And the following ActivationDetails for AccountingDocument with DocumentType "Costing" exist:
      | Code       | CreationDate        | CreatedBy       | Accountant      | ExchangeRateCode | JournalEntryCode | CurrencyPrice | ObjectTypeCode | FiscalPeriod |
      | 2021000001 | 07-Sep-2021 9:30 AM | Shady.Abdelatif | Shady.Abdelatif | 2018000013       | 2021000002       | 1.0 EGP       | Costing        | 2021         |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47"