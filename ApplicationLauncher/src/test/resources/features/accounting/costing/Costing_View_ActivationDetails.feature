Feature: Costing - View Activation Details Section for GoodsReceipt

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Ali.Hasan         | Storekeeper_Flexo     |
      | hr1               | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole            |
      | Storekeeper_Signmedia | GRViewer_Signmedia |
      | Storekeeper_Flexo     | GRViewer_Flexo     |
      | SuperUser             | SuperUserSubRole   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                                           | Condition                      |
      | GRViewer_Signmedia | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |
      | GRViewer_Flexo     | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole   | *:*                                                  |                                |
    And the following users have the following permissions without the following conditions:
      | User      | Permission                                           | Condition                      |
      | Ali.Hasan | GoodsReceipt:ReadAccountingDocumentActivationDetails | [purchaseUnitName='Signmedia'] |


    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Jan-2021 09:00 AM | User Daily Rate |

   #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |


    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Active |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000009   |                           | 11.44 EGP               |
    And insert the following Costing AccountingDetails:
      | Code       | Credit/Debit | Subledger | Account            | SubAccount | Value |
      | 2021000001 | DEBIT        | NONE      | 70103 - Purchasing |            | 2000  |
      | 2021000001 | CREDIT       | PO        | 10207 - PO         | 2021000003 | 2000  |

    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy | JournalDate          | ReferenceDocument    | Company          | BusinessUnit     |
      | 2021000001 | 04-Jan-2021 09:45 AM | Admin     | 10-Mar-2021 09:02 AM | 2021000001 - Costing | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following JournalEntries Details exist:
      | JournalEntryCode | JournalDate          | Subledger | GLAccount          | GLSubAccount | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2021000001       | 10-Mar-2021 00:00 PM |           | 70103 - Purchasing |              | 0      | 2000  | EGP                | EGP               | 1                         | EGP             | 1                       |
      | 2021000001       | 10-Mar-2021 00:00 PM | PO        | 10207 - PO         | 2021000003   | 2000   | 0     | EGP                | EGP               | 1                         | EGP             | 1                       |
    And insert the following Costing ActivationDetails:
      | Code       | Accountant      | ActivationDate       | CurrencyPrice       | JournalEntryCode | FiscalYear |
      | 2021000001 | Shady.Abdelatif | 10-Mar-2021 10:30 AM | 1.0 USD = 11.44 EGP | 2021000001       | 2021       |

  #### Happy Path
  # EBS-8297
  Scenario Outline: (01) View Costing ActivationDetails section for GoodsReceipt - by an authorized user with one role with condition or without condition (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view ActivationDetails section for GoodsReceipt with code "<GRCodeValue>"
    Then the following values of ActivationDetails section for GoodsReceipt with code "<GRCodeValue>" are displayed to "<User>":
      | Code   | Accountant   | ActivationDate   | JournalEntryCode   | FiscalYear   | CurrencyPrice   |
      | <Code> | <Accountant> | <ActivationDate> | <JournalEntryCode> | <FiscalYear> | <CurrencyPrice> |
    Examples:
      | User              | Code       | Accountant      | ActivationDate       | JournalEntryCode | FiscalYear | CurrencyPrice       | GRCodeValue |
      | Mahmoud.Abdelaziz | 2021000001 | Shady.Abdelatif | 10-Mar-2021 10:30 AM | 2021000001       | 2021       | 1.0 USD = 11.44 EGP | 2021000009  |

  #### Exceptions
  # EBS-8297
  @Future
  Scenario: (02) View Costing ActivationDetails section for GoodsReceipt - where GRAccountingDocument has been deleted by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the GRAccountingDocument with code "2018000008" successfully
    When "Mahmoud.Abdelaziz" requests to view ActivationDetails section for GoodsReceipt with code "2018000008"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  #### Abuse Cases
  # EBS-8297
  Scenario: (03)  View Costing ActivationDetails section for GoodsReceipt - by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" requests to view ActivationDetails section for GoodsReceipt with code "2021000009"
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page