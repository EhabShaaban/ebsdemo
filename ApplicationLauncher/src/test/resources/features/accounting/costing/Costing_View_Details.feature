Feature: DObCosting - View Details Section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | Accountant_Flexo     |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                 |
      | Accountant_Signmedia | CostingViewer_Signmedia |
      | Accountant_Flexo     | CostingViewer_Flexo     |
      | SuperUser            | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition                      |
      | CostingViewer_Signmedia | Costing:ReadDetails | [purchaseUnitName='Signmedia'] |
      | CostingViewer_Flexo     | Costing:ReadDetails | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole        | *:*                 |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission          | Condition                      |
      | Ahmed.Hamdi | Costing:ReadDetails | [purchaseUnitName='Signmedia'] |


    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 000035 | USD              | EGP               | 11.44       | 01-Jan-2021 09:00 AM | User Daily Rate |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000003 | IMPORT_PO | Arrived | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
    #@INSERT
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
    #@INSERT
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    #@INSERT
    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Active |
    #@INSERT
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
    #@INSERT
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2021000009   | 11.44 EGP                 |                         |
  #### Happy Path
  # EBS-8756
  Scenario: (01) View CostingDocument Details section - by an authorized user with one role with condition or without condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Details section for Costing with code "2021000001"
    Then the following values of Details section for Costing with code "2021000001" are displayed to "Shady.Abdelatif":
      | GoodsReceiptCode | CurrencyPriceEstimateTime | CurrencyPriceActualTime | State  |
      | 2021000009       | 11.44 EGP                 |                         | Active |

  #### Abuse Cases
  # EBS-8756
  Scenario: (03)  View CostingDocument Details section - by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view Details section for Costing with code "2021000001"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page
