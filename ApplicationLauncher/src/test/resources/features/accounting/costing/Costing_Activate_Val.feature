Feature: Costing - Activate Val #EBS-9203

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | CostingActivateOwner_Signmedia |
      | SuperUser            | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission       | Condition                      |
      | CostingActivateOwner_Signmedia | Costing:Activate | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole               | *:*              |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission       | Condition                      |
      | Ahmed.Hamdi | Costing:Activate | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State                      | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000004 | IMPORT_PO | Active,Cleared,NotReceived | Admin     | 2-Mar-2021 9:02 AM | Gehan Ahmed   |
      | 2021000003 | IMPORT_PO | Arrived                    | Admin     | 1-Mar-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

    And insert the following GoodsReceipt GeneralData:
      | Code       | Type  | CreatedBy | CreationDate        | State  | BusinessUnit     |
      | 2021000009 | GR_PO | Admin     | 10-Mar-2021 9:02 AM | Active | 0002 - Signmedia |
      | 2021000002 | GR_PO | Admin     | 11-Mar-2021 9:02 AM | Draft  | 0002 - Signmedia |

    #@INSERT
    And insert the following GoodsReceipt CompanyData:
      | Code       | Company          | Plant                    | Storehouse                 | Storekeeper              |
      | 2021000009 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |
      | 2021000002 | 0001 - AL Madina | 0001 - Madina Tech Plant | 0001 - AlMadina Main Store | 0007 - Mahmoud.Abdelaziz |

    #@INSERT
    And insert the following GoodsReceipt PurchaseOrderData:
      | Code       | PurchaseOrder | Vendor            | PurchasingResponsible | DeliveryNote/PL | Notes |
      | 2021000009 | 2021000003    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |
      | 2021000002 | 2021000004    | 000002 - Zhejiang | Gehan Ahmed           | 123456          |       |

    And insert the following GoodsReceipt Items:
      | Code       | Item                                         | DifferenceReason | Type |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  |                  |      |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll |                  |      |

    And insert the following GoodsReceipt ItemQuantities:
      | Code       | Item                                         | UoE       | ReceivedQty(UoE) | StockType        | Notes |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 90               | UNRESTRICTED_USE |       |
      | 2021000002 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 10               | DAMAGED_STOCK    |       |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 50               | UNRESTRICTED_USE |       |

    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate        | State  |
      | 2021000002 | Admin     | 10-Mar-2021 9:02 AM | Active |
      | 2021000001 | Admin     | 10-Mar-2021 9:02 AM | Draft  |
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |             |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |

    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000002 | 2021000009   |                           | 11.44 EGP               |
      | 2021000001 | 2021000002   | 16.392                    |                         |
    And insert the following Costing Items:
      | Code       | Item                                         | UOM       | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate)   | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 40               | 90.0             | UNRESTRICTED_USE | 182.1333333331512 EGP |                   | 8.0888888888 EGP         |                        |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll  | 0019 - M2 | 0                | 10.0             | DAMAGED_STOCK    | 0 EGP                 |                   | 0 EGP                    |                        |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 60               | 50.0             | UNRESTRICTED_USE | 491.760 EGP           |                   | 21.84 EGP                |                        |

  #EBS-9203
  Scenario Outline: (01) Activate Costing - Val - invalid input (abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" activates Costing Document with code "<CostingCode>" and DueDate "<DueDate>" at "07-Sep-2021 9:30 AM"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | CostingCode | DueDate              |
      | 2021000001  |                      |
      | 2021000001  | ay7aga               |
      | 2021000001  | 06-Sep-2019 09:30 AM |
      #costing document doesn't exist
      | 2021000012  | 06-Sep-2021 09:30 AM |

  Scenario: (02) Activate Costing - Val - landed cost is estimate
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" activates Costing Document with code "2021000001" and DueDate "06-Sep-2021 09:30 AM" at "07-Sep-2021 9:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Costing-msg-01"

  Scenario: (03) Activate Costing - Val - Costing  Document invalid state
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" activates Costing Document with code "2021000002" and DueDate "06-Sep-2021 09:30 AM" at "07-Sep-2021 9:30 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-32"
