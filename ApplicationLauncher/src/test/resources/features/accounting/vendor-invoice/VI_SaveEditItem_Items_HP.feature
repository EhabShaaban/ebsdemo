#Author: Yara Ameen, Mohamed Aboelnour
#Reviewer: Karim Rostom

Feature: Save Invoice Edit Item

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia | IVRViewer_Signmedia           |

    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |
      | IVRViewer_Signmedia           | ItemVendorRecord:ReadAll  | [purchaseUnitName='Signmedia'] |

    And the following Vendor Invoices with details exist:
      | Code       | Type                | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000133 | LOCAL_GOODS_INVOICE | 0002         | 000013     | 2018000017    | 1200120301023100 | Draft |

    And VendorInvoice with code "2019000133" has the following InvoiceItems:
      | ItemCode | ItemName                            | QtyOrder | OrderUnit | OrderUnitSymbol | QtyBase | BaseUnit | BaseUnitSymbol | UnitPrice(Base) | UnitPrice(OrderUnit) | ItemTotalAmount |
      | 000002   | Hot Laminated Frontlit Backlit roll | 50.0     | 0037      | Drum-50Kg       | 50.00   | 0019     | M2             | 4.00            | 4.00                 | 200             |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000133"
      | Tax                      | TaxPercentage | TaxAmount |
      | 0001 - Vat               | 1             | 2.00      |
      | 0003 - Real estate taxes | 10            | 20.00     |

      #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000133 | 0.00        | 200.00         |

    ################################################ Request to save Item scenarios ################################################

  #EBS-6223
  Scenario: (01) Save "editable" fields in "editable states" for VendorInvoice- with all fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000133" is locked by "Gehan.Ahmed" at "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" saves the following edited item with id "14" to the InvoiceItems of Vendor Invoice with Code "2019000133" at "01-Jan-2019 11:10 AM" with the following values:
      | QtyOrder | UnitPrice(Base) |
      | 150.00   | 3.50            |
    Then VendorInvoice with Code "2019000133" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State |
      | Gehan.Ahmed   | 01-Jan-2019 11:10 AM | Draft |
    And InvoiceItems section of Vendor Invoice with Code "2019000133" and ItemId "14" is updated as follows:
      | ItemId | Item                                         | QtyOrder | OrderUnit | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 14     | 000002 - Hot Laminated Frontlit Backlit roll | 150.0    | 0037      | 3.50            | 3.50                 |

    And InvoiceTaxes section of Vendor Invoice with Code "2019000133" is updated as follows:
      | Tax                      | TaxPercentage | TaxAmount |
      | 0001 - Vat               | 1             | 5.25      |
      | 0003 - Real estate taxes | 10            | 52.5      |

    And the lock by "Gehan.Ahmed" on InvoiceItems section of Vendor Invoice with Code "2019000133" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"