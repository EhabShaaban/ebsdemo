Feature: Request to edit and cancel Invoice details section in Invoice

  Background:
    Given the following users and roles exist:
      | User                                         | Role                                                                      |
      | Gehan.Ahmed                                  | PurchasingResponsible_Signmedia                                           |
      | Gehan.Ahmed.NoPaymentTerms.NoCurrencies.NoPR | PurchasingResponsible_Signmedia_CannotReadPaymentTerms_And_Currencies_PRs |
      | Amr.Khalil                                   | PurchasingResponsible_Flexo                                               |
      | Amr.Khalil                                   | PurchasingResponsible_Corrugated                                          |
      | Ahmed.Hamdi                                  | Accountant_Flexo                                                          |
      | hr1                                          | SuperUser                                                                 |
    And the following roles and sub-roles exist:
      | Role                                                                      | Subrole                        |
      | PurchasingResponsible_Signmedia                                           | VendorInvoiceOwner_Signmedia   |
      | PurchasingResponsible_Signmedia                                           | PaymentTermViewer              |
      | PurchasingResponsible_Signmedia                                           | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia                                           | LimitedPaymentViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadPaymentTerms_And_Currencies_PRs | VendorInvoiceOwner_Signmedia   |
      | PurchasingResponsible_Flexo                                               | VendorInvoiceOwner_Flexo       |
      | PurchasingResponsible_Corrugated                                          | VendorInvoiceOwner_Corrugated  |
      | SuperUser                                                                 | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                         | Condition                                                         |
      | VendorInvoiceOwner_Signmedia   | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia']                                    |
      | VendorInvoiceOwner_Flexo       | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Flexo']                                        |
      | VendorInvoiceOwner_Corrugated  | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Corrugated']                                   |
      | LimitedPaymentViewer_Signmedia | PaymentRequest:ReadAll             | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | CurrencyViewer                 | Currency:ReadAll                   |                                                                   |
      | PaymentTermViewer              | PaymentTerms:ReadAll               |                                                                   |
      | SuperUserSubRole               | *:*                                |                                                                   |
    And the following users doesn't have the following permissions:
      | User                                         | Permission                         |
      | Ahmed.Hamdi                                  | VendorInvoice:UpdateInvoiceDetails |
      | Gehan.Ahmed.NoPaymentTerms.NoCurrencies.NoPR | Currency:ReadAll                   |
      | Gehan.Ahmed.NoPaymentTerms.NoCurrencies.NoPR | PaymentTerms:ReadAll               |
      | Gehan.Ahmed.NoPaymentTerms.NoCurrencies.NoPR | PaymentRequest:ReadAll             |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                         | Condition                      |
      | Amr.Khalil | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |

    And the following Invoice exists:
      | Code       | State |
      | 2019000103 | Draft |

    And edit session is "30" minutes

  ### Request to edit InvoiceDetails section ###
  Scenario Outline: (01) Request to edit/Cancel Vendor Invoice - DetailsRequest to edit Invoice Details section by an authorized user with one role incase state is DRAFT (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to edit InvoiceDetails section of Invoice with code "<InvoiceCode>"
    Then InvoiceDetails section of Invoice with code "<InvoiceCode>" becomes in edit mode and locked by "<User>"
    And the following authorized reads are returned to "<User>":
      | AuthorizedReads    |
      | ReadPaymentTerms   |
      | ReadCurrency       |
      | ReadPaymentRequest |
    And there are no mandatory fields returned to "<User>"
    And the following editable fields are returned to "<User>":
      | EditableFields |
      | paymentTerm    |
      | invoiceNumber  |
      | invoiceDate    |
      | currency       |
      | downPayment    |
    Examples:
      | User        | InvoiceCode |
      | Gehan.Ahmed | 2019000103  |

  Scenario: (02) Request to edit/Cancel Vendor Invoice - Request to edit Invoice Details section that is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened the Invoice with code "2019000103" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit InvoiceDetails section of Invoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario: (03) Request to edit/Cancel Vendor Invoice - Request to edit Invoice Details section of deleted Invoice (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to edit InvoiceDetails section of Invoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (04) Request to edit/Cancel Vendor Invoice - Request to edit Invoice Details section with unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit InvoiceDetails section of Invoice with code "2019000103"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario: (05) Request to edit/Cancel Vendor Invoice - Request to edit Invoice Details section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to edit InvoiceDetails section of Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

  # Request to Cancel saving InvoiceDetails section ###
  Scenario Outline: (06) Request to edit/Cancel Vendor Invoice - Request to Cancel saving Invoice Details section by an authorized user with one role (Happy Path)
    Given user is logged in as "<User>"
    And InvoiceDetails section of Invoice with code "<InvoiceCode>" is locked by "<User>" at "07-Jan-2019 09:10 AM"
    When "<User>" cancels saving InvoiceDetails section of Invoice with code "<InvoiceCode>" at "07-Jan-2019 09:30 AM"
    Then the lock by "<User>" on InvoiceDetails section of Invoice with code "<InvoiceCode>" is released

    Examples:
      | User        | InvoiceCode |
      | Gehan.Ahmed | 2019000103  |

  Scenario: (07) Request to edit/Cancel Vendor Invoice - Request to Cancel saving Invoice Details section after lock session is expire
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceDetails section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving InvoiceDetails section of Invoice with code "2019000103" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (08) Request to edit/Cancel Vendor Invoice - Request to Cancel saving Invoice Details section after lock session is expire and Invoice doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And InvoiceDetails section of Invoice with code "2019100003" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor Invoice with code "2019100003" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving InvoiceDetails section of Invoice with code "2019100003" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (09) Request to edit/Cancel Vendor Invoice - Request to Cancel saving Invoice Details section with unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" cancels saving InvoiceDetails section of Invoice with code "2019000103"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

  Scenario: (10) Request to edit/Cancel Vendor Invoice - Request to Cancel saving Invoice Details section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" cancels saving InvoiceDetails section of Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
