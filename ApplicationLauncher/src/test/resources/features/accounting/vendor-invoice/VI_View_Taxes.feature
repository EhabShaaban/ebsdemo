@Author: Yara Ameen
Feature: Read Vendor Invoice Taxes section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
      | hr1         | SuperUser                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Flexo     | VendorInvoiceViewer_Flexo     |
      | SuperUser                       | SuperUserSubRole              |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission              | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadTaxes | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Flexo     | VendorInvoice:ReadTaxes | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole              | *:*                     |                                |

    And the following Vendor Invoices with details exist:
      | Code       | Type                | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000133 | LOCAL_GOODS_INVOICE | 0002         | 000013     | 2018000017    | 1200120301023100 | Draft |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000133"
      | Tax                      | TaxPercentage | TaxAmount |
      | 0001 - Vat               | 1             | 2         |
      | 0003 - Real estate taxes | 10            | 20        |

  Scenario: (01) View VendorInvoiceTaxes by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Taxes section of VendorInvoice with code "2019000133"
    Then the following tax values for VendorInvoice with code "2019000133" are displayed to "Gehan.Ahmed":
      | TaxName                  | TaxPercentage | TaxAmount |
      | 0001 - Vat               | 1             | 2         |
      | 0003 - Real estate taxes | 10            | 20        |

  Scenario: (02) View VendorInvoiceTaxes section where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to view Taxes section of VendorInvoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (03) View VendorInvoiceTaxes section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Taxes section of VendorInvoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page