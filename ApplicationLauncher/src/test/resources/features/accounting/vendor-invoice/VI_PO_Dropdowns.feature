# Author Dev: Evram Hany
# Author Quality: Khadra Ali
# Reviewer : Yara Ameen

Feature: VI Purchase Orders DropDown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
      | Afaf        | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole            |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia |
      | PurchasingResponsible_Flexo     | POViewer_Flexo     |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission            | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
      | POViewer_Flexo     | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']     |

    And the following users doesn't have the following permissions:
      | User | Permission            | Condition |
      | Afaf | PurchaseOrder:ReadAll |           |


    And the following Vendors exist:
      | Code   | Name     | PurchaseUnit                   |
      | 000001 | Siegwerk | 0001 - Flexo                   |
      | 000002 | Zhejiang | 0002 - Signmedia               |
      | 000008 | Vendor 2 | 0002 - Signmedia, 0001 - Flexo |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000051 | LOCAL_PO   | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000052 | LOCAL_PO   | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000053 | LOCAL_PO   | Draft     | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000054 | LOCAL_PO   | Shipped   | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000055 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000056 | IMPORT_PO  | Cleared   | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000057 | IMPORT_PO  | Draft     | Admin     | 5-Aug-2018 9:02 AM | Amr Khalil    |
      | 2020000058 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000059 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000060 | SERVICE_PO | Draft     | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000062 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Amr Khalil    |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000051 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000052 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000053 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000054 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000055 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000056 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000057 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000058 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000059 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000060 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000062 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000051 | 000002 - Zhejiang |             |
      | 2020000052 | 000001 - Siegwerk |             |
      | 2020000053 | 000001 - Siegwerk |             |
      | 2020000054 | 000002 - Zhejiang |             |
      | 2020000055 | 000002 - Zhejiang |             |
      | 2020000056 | 000001 - Siegwerk |             |
      | 2020000057 | 000001 - Siegwerk |             |
      | 2020000058 | 000002 - Zhejiang | 2020000051  |
      | 2020000059 | 000002 - Zhejiang | 2020000055  |
      | 2020000060 | 000002 - Zhejiang | 2020000052  |
      | 2020000062 | 000001 - Siegwerk | 2020000052  |


  Scenario Outline: (01) Read All Vendor Invoice PurchaseOrders for service PurchaseOrder DropDown in VendorInvoice by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all purchase orders After Selecting Vendor Invoice Type With Code "<invoiceTypeCode>"  & Vendor With Code "000002"
    Then the following purchase orders should be returned in the DropDown to "Gehan.Ahmed":
      | Code       | Type       | State     |
      | 2020000059 | SERVICE_PO | Confirmed |
      | 2020000058 | SERVICE_PO | Confirmed |
    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 2
    Examples:
      | invoiceTypeCode        |
      | SHIPMENT_INVOICE       |
      | CUSTOM_TRUSTEE_INVOICE |
      | INSURANCE_INVOICE      |


  #EBS-7424
  Scenario: (02) Read All Vendor Invoice PurchaseOrders for [Import] PurchaseOrder DropDown in VendorInvoice by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all purchase orders After Selecting Vendor Invoice Type With Code "IMPORT_GOODS_INVOICE"  & Vendor With Code "000002"
    Then the following purchase orders should be returned in the DropDown to "Gehan.Ahmed":
      | Code       | Type      | State     |
      | 2020000055 | IMPORT_PO | Confirmed |
    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 1


  #EBS-7424
  Scenario: (03) Read All Vendor Invoice PurchaseOrders for [Local] PurchaseOrder DropDown in VendorInvoice by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all purchase orders After Selecting Vendor Invoice Type With Code "LOCAL_GOODS_INVOICE"  & Vendor With Code "000002"
    Then the following purchase orders should be returned in the DropDown to "Gehan.Ahmed":
      | Code       | Type     | State     |
      | 2020000054 | LOCAL_PO | Shipped   |
      | 2020000051 | LOCAL_PO | Confirmed |
    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 2


  Scenario: (03) Read All Vendor Invoice PurchaseOrders for PurchaseOrder Dropdown in VendorInvoice by unauthorized user (Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all purchase orders After Selecting Vendor Invoice Type With Code "GOODS_INVOICE"  & Vendor With Code "000002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
