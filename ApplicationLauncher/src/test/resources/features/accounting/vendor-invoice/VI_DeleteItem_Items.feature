Feature: Delete Item in InvoiceVendorInvoiceCreateSuccessEvent

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Afaf        | FrontDesk                        |
      | hr1         | SuperUser                        |

    And the following roles and sub-roles exist:
      | Role                             | Subrole                       |
      | PurchasingResponsible_Signmedia  | VendorInvoiceOwner_Signmedia  |
      | PurchasingResponsible_Corrugated | VendorInvoiceOwner_Corrugated |
      | SuperUser                        | SuperUserSubRole              |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                | Condition                       |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Corrugated | VendorInvoice:UpdateItems | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole              | *:*                       |                                 |

    And the following Items exist:
      | Code   | Name                                | PurchasingUnit |
      | 000001 | Hot Laminated Frontlit Fabric roll  | 0002           |
      | 000002 | Hot Laminated Frontlit Backlit roll | 0002           |

    And the following Items in Invoice with code "2019000107" exist:
      | ItemCode | ItemId |
      | 000003   | 9      |
      | 000004   | 8      |
      | 000012   | 7      |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000107"
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 1035.00   |
      | 0014 - Service tax  | 2             | 207.00    |

    And the following Items in Invoice with code "2019000104" exist:
      | ItemCode | ItemId |
      | 000002   | 8      |
      | 000008   | 9      |

          #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000107 | 100.00      | 9592.00        |
      | 2019000104 | 0.00        | 18736.00       |

  Scenario: (01) Delete Item from Invoice , where Invoice is Draft - by an authorized user - user with one/more roles (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to delete Item with code "000004" and id "8" in Invoice with code "2019000107"
    Then Item with code "000004" and id "8" from Invoice with code "2019000107" is deleted
    And InvoiceTaxes section of Vendor Invoice with Code "2019000107" is updated as follows:
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 1020.00   |
      | 0014 - Service tax  | 2             | 204.00    |
    And InvoiceSummaries section of VendorInvoice with Code "2019000107" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 10200.00               | 1224.00   | 11424.00  | 100.00      | 11342.00       |
    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-12"


  Scenario Outline: (02) Delete Item from Invoice , where Item doesn't exist (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted Item with code "<ItemCode>" and id "<ItemId>" of Invoice with code "<InvoiceCode>" successfully
    When "Amr.Khalil" requests to delete Item with code "<ItemCode>" and id "<ItemId>" in Invoice with code "<InvoiceCode>"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-20"
    Examples:
      | InvoiceCode | ItemCode | ItemId |
      | 2019000107  | 000004   | 8      |

  Scenario Outline: (03) Delete Item from Invoice , where action not allowed per current state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    When "Gehan.Ahmed" requests to delete Item with code "<ItemCode>" and id "<ItemId>" in Invoice with code "<InvoiceCode>"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-32"
    Examples:
      | InvoiceCode | ItemCode | ItemId |
      | 2019000104  | 000002   | 10     |

  Scenario: (04) Delete Item from Invoice by Unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to delete Item with code "000004" and id "8" in Invoice with code "2019000107"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  Scenario Outline: (05) Delete Item from Invoice, where Invoice doesn't exist (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "<InvoiceCode>" successfully
    When "Amr.Khalil" requests to delete Item with code "<ItemCode>" and id "<ItemId>" in Invoice with code "<InvoiceCode>"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"
    Examples:
      | InvoiceCode | ItemCode | ItemId |
      | 2019000107  | 000004   | 8      |

  @Future
  Scenario Outline: (06) Delete Item from Invoice when InvoiceItems section are locked by another user (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And first "hr1" opens InvoiceItems section of Invoice with code "<InvoiceCode>" in edit mode
    When "Amr.Khalil" requests to delete Item with code "<ItemCode>" and id "<ItemId>" in Invoice with code "<InvoiceCode>"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-02"
    Examples:
      | InvoiceCode | ItemCode | ItemId |
      | 2019000107  | 000004   | 8      |
