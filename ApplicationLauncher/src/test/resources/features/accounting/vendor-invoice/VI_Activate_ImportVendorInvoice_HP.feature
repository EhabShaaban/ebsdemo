#Dev Author: Evram Hany, Yara Ameen
#QA Reviewer: Ehab Shaaban
#Reviewer: Somaya Ahmed and Hosam Bayomy

Feature: Activate Import VendorInvoice (HP)
  Activating a Vendor Invoice (based on Import PO) results in the following:
  - Vendor Invoice state is changed to Active (EBS-5158)
  - Vendor Invoice - Accounting Details are determined (EBS-8539 for import)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document based on Subledgers
  ---- A record for realized Exchange Rate is added to handle multicurrency if applied (EBS-7742 for import)
  - Journal Entries for Vendor Invoice is created (EBS-4649)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8167)
  ---- The latest ExchangeRate is retrieved based on the default strategy (EBS-4320)
  ---- Multicurrency and realized exchange rates are handled (EBS-7742)
  -------- when VendorInvoice Currency is different from the Company Local Currency (in case of Import)
  -------- when Collection (DownPayment) Currency is different from the currency of both the SalesInvoice  and the Company
  - Vendor Invoice Activation Details are determined (Done)
  - Vendor Invoice Remaining is set as Vendor Invoice  minus any previous:
  ---- Payments (EBS-8437)
  ---- Notes Payables (Future)
  - The Purchase Order State is changed to Invoiced or Completed (Future Work)
  - The Remaining of Purchase Order is set to Zero (Future Work)
  - Increase & Decrease related balances (EBS-8514) (Future Work)
  - Realized Differences on Exchange will be calculated as below (EBS-7742)
  (Total of Previous Payment in Foreign Currency * current ExRate of invoice ) - (Sum of (Previous Payment in Foreign Currency * ExRate at Payment Time of each))
  if > 0 (Positive) ==> Credit the Realized ExRate  and Debit the Vendor
  If negative, then Debit Realized ExRate and Credit Vendor

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | JournalCreator_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                       | Condition                      |
      | JournalCreator_Signmedia | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Signmedia'] |

    And the following Companies have the following local Currencies:
      | Code | Name    | CurrencyISO |
      | 0002 | DigiPro | EGP         |

    And the following AccountingInfo for Items exist:
      | ItemCode | AccountCode | AccountName      |
      | 000001   | 10207       | PO               |
      | 000002   | 10207       | PO               |
      | 000004   | 70102       | Local Purchasing |

    And the following AccountingInfo for Vendors exist:
      | VendorCode | AccountCode | AccountName       |
      | 000002     | 10221       | service vendors 2 |

    And the following GLAccounts has the following Subledgers:
      | GLAccount                | Subledger |
      | 10207 - PO               | PO        |
      | 70102 - Local Purchasing | PO        |

      #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |

    #@INSERT
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And  the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Feb-2019 10:00 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2019 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2019 10:00 PM | Central Bank Of Egypt |


   #EBS-8437
  Scenario: (01) Activate Vendor Invoice for Import PurchaseOrder with down payment by an authorized user (Happy Path)

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State       | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000012 | VENDOR      | PaymentDone | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000013 | VENDOR      | PaymentDone | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury                  | Description  |
      | 2021000012 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2018000045  | 1000      | USD         |                |                   | 0002 - Signmedia Treasury | down payment |
      | 2021000013 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2018000045  | 500       | USD         |                |                   | 0002 - Signmedia Treasury | down payment |


      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000102 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000102 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000102 | 2018000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)    | Qty(Base)  | UnitPrice(Base) |
      | 2019000102 | 000001 - Hot Laminated Frontlit Fabric roll  | 40.0 Roll 2.70x50 | 40.00 M2   | 100             |
      | 2019000102 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 Roll 2.70x50 | 2700.00 M2 | 2025            |
      | 2019000102 | 000004 - Flex cold foil adhesive E01         | 100.0 Drum-50Kg   | 100.00 Kg  | 750             |

     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000102 | 0.00        | 5546500.00     |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2019100012 | IMPORT - Landed Cost For Import PO | Draft | Gehan.Ahmed | 03-Jan-2019 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2019100012 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2019100012 | 2018000045 |                   | true        |

    And the last created JournalEntry was with code "2019000000"
    When "Shady.Abdelatif" Activates VendorInvoice of type "IMPORT_GOODS_INVOICE" with code "2019000102" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then VendorInvoice with Code "2019000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | Posted |
    And ActivationDetails in VendorInvoice with Code "2019000102" is updated as follows:
      | Accountant      | PostingDate          | DueDate              | LatestExchangeRateCode | CurrencyPrice |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 2018000026             | 17.44         |
    Then InvoiceDetails of VendorInvoice with Code "2019000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment                     |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD         | 2021000012-1000, 2021000013-500 |
    Then Summary section of VendorInvoice with Code "2019000102" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount  | DownPayment | TotalRemaining |
      | 5546500.00             | 0.00      | 5546500.00 | 1500.00     | 5545000.00     |
    And AccountingDetails in "VendorInvoice" with Code "2019000102" is updated as follows:
      | Credit/Debit | GLAccount                 | GLSubAccount      | Amount  |
      | CREDIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 5546500 |
      | DEBIT        | 70102 - Local Purchasing  | 2018000045        | 75000   |
      | DEBIT        | 10207 - PO                | 2018000045        | 5471500 |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument          | Company          |
      | 2019000001 | 07-Jan-2019 09:30 AM | Shady.Abdelatif | 06-Jan-2019 12:00 AM | 0002 - Signmedia | 2019       | 2019000102 - VendorInvoice | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000001":
      | GLAccount                 | GLSubAccount      | Credit     | Debit      | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10221 - service vendors 2 | 000002 - Zhejiang | 5546500.00 | 0.00       | USD                | EGP             | 17.44                     | 2018000026       |
      | 70102 - Local Purchasing  | 2018000045        | 0.00       | 75000.00   | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO                | 2018000045        | 0.00       | 5471500.00 | USD                | EGP             | 17.44                     | 2018000026       |
    And LandedCost with Code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | Draft |
    And the LandedCostDetails section of LandedCost with code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PurchaseOrder | GoodsInvoice | DamageStock |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 2018000045    | 2019000102   | true        |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47" with the following JournalEntry code "2019000001"

   #EBS-8706
  Scenario: (02) Activate Vendor Invoice for Import PurchaseOrder with multiple downpayments with different states by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000046 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000046 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000102 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2021 11:00 AM | Ashraf Salah  |
      | 2021000045 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2021 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000102 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000045 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000102 | 2021000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | USD      |
      | 2021000045 | 2021000046    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 2763276372637    | 24-Apr-2021 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)    | Qty(Base)  | UnitPrice(Base) |
      | 2021000102 | 000001 - Hot Laminated Frontlit Fabric roll  | 40.0 Roll 2.70x50 | 40.00 M2   | 100             |
      | 2021000102 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 Roll 2.70x50 | 2700.00 M2 | 2025            |
      | 2021000102 | 000004 - Flex cold foil adhesive E01         | 100.0 Drum-50Kg   | 100.00 Kg  | 750             |
      | 2021000045 | 000001 - Hot Laminated Frontlit Fabric roll  | 15.0 Roll 2.70x50 | 15.00 M2   | 100             |

     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2021000102 | 0.00        | 5546500.00     |
      | 2021000045 | 0.00        | 1500.00        |
     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000021 | VENDOR                   | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000022 | VENDOR                   | Draft       | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000023 | VENDOR                   | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000024 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000021 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000022 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000023 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000024 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber | Treasury                  | Description             |
      | 2021000021 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2021000045  | 500       | USD         |                            |                   | 0002 - Signmedia Treasury | active down payment     |
      | 2021000022 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2021000045  | 300       | USD         |                            |                   | 0002 - Signmedia Treasury | draft down payment      |
      | 2021000023 | CASH        | 000002 - Zhejiang | INVOICE         | 2021000045  | 70        | USD         |                            |                   | 0002 - Signmedia Treasury | payment against invoice |
      | 2021000024 | CASH        |                   | PURCHASEORDER   | 2021000045  | 30        | USD         | 000014 - Insurance service |                   | 0002 - Signmedia Treasury | general payment         |

    #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant      | PostingDate          | CurrencyPrice       | JournalEntryCode |
      | 2021000021 | Shady.Abdelatif | 05-Aug-2021 09:02 AM | 1.0 EGP = 16.50 USD | 2020000024       |
      | 2021000024 | Shady.Abdelatif | 06-Aug-2021 09:02 AM | 1.0 EGP = 20.50 USD | 2020000025       |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2019100012 | IMPORT - Landed Cost For Import PO | Draft | Gehan.Ahmed | 03-Jan-2019 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2019100012 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2019100012 | 2021000045 |                   | true        |

    And the last created JournalEntry was with code "2019000000"
    When "Shady.Abdelatif" Activates VendorInvoice of type "IMPORT_GOODS_INVOICE" with code "2021000102" at "07-Jan-2021 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2021 12:00 AM | 17.85         |
    Then VendorInvoice with Code "2021000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Posted |
    And ActivationDetails in VendorInvoice with Code "2021000102" is updated as follows:
      | Accountant      | PostingDate          | DueDate              | LatestExchangeRateCode | CurrencyPrice |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 06-Jan-2021 12:00 AM | 2018000026             | 17.44         |
    Then InvoiceDetails of VendorInvoice with Code "2021000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment    |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | USD         | 2021000021-500 |
    Then Summary section of VendorInvoice with Code "2021000102" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount  | DownPayment | TotalRemaining |
      | 5546500.00             | 0.00      | 5546500.00 | 500.00      | 5546000.00     |
    And AccountingDetails in "VendorInvoice" with Code "2021000102" is updated as follows:
      | Credit/Debit | GLAccount                 | GLSubAccount      | Amount  |
      | CREDIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 5546500 |
      | DEBIT        | 70102 - Local Purchasing  | 2021000045        | 75000   |
      | DEBIT        | 10207 - PO                | 2021000045        | 5471500 |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument          | Company          |
      | 2021000001 | 07-Jan-2021 09:30 AM | Shady.Abdelatif | 06-Jan-2021 12:00 AM | 0002 - Signmedia | 2021       | 2021000102 - VendorInvoice | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2021000001":
      | GLAccount                                | GLSubAccount      | Credit     | Debit      | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10221 - service vendors 2                | 000002 - Zhejiang | 5546500.00 | 0.00       | USD                | EGP             | 17.44                     | 2018000026       |
      | 70102 - Local Purchasing                 | 2021000045        | 0.00       | 75000.00   | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO                               | 2021000045        | 0.00       | 5471500.00 | USD                | EGP             | 17.44                     | 2018000026       |
      | 41101 - Realized Differences on Exchange |                   | 470.00     | 0.00       | EGP                | EGP             | 1.00                      |                  |
      | 10221 - service vendors 2                | 000002 - Zhejiang | 0.00       | 470.00     | EGP                | EGP             | 1.00                      |                  |
    And LandedCost with Code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Draft |
    And the LandedCostDetails section of LandedCost with code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PurchaseOrder | GoodsInvoice | DamageStock |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 2021000045    | 2021000102   | true        |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47" with the following JournalEntry code "2021000001"

  #EBS-7742
  Scenario: (03) Activate Vendor Invoice for Import PurchaseOrder with multiple downpayments with different states by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000046 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000046 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000102 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2021 11:00 AM | Ashraf Salah  |
      | 2021000045 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2021 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000102 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000045 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000102 | 2021000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | USD      |
      | 2021000045 | 2021000046    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 2763276372637    | 24-Apr-2021 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)    | Qty(Base)  | UnitPrice(Base) |
      | 2021000102 | 000001 - Hot Laminated Frontlit Fabric roll  | 40.0 Roll 2.70x50 | 40.00 M2   | 100             |
      | 2021000102 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 Roll 2.70x50 | 2700.00 M2 | 2025            |
      | 2021000102 | 000004 - Flex cold foil adhesive E01         | 100.0 Drum-50Kg   | 100.00 Kg  | 750             |
      | 2021000045 | 000001 - Hot Laminated Frontlit Fabric roll  | 15.0 Roll 2.70x50 | 15.00 M2   | 100             |

     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2021000102 | 0.00        | 5546500.00     |
      | 2021000045 | 0.00        | 1500.00        |
     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000021 | VENDOR                   | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000022 | VENDOR                   | Draft       | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000023 | VENDOR                   | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |
      | 2021000024 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000021 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000022 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000023 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000024 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber | Treasury                  | Description             |
      | 2021000021 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2021000045  | 500       | USD         |                            |                   | 0002 - Signmedia Treasury | active down payment     |
      | 2021000022 | CASH        | 000002 - Zhejiang | PURCHASEORDER   | 2021000045  | 300       | USD         |                            |                   | 0002 - Signmedia Treasury | draft down payment      |
      | 2021000023 | CASH        | 000002 - Zhejiang | INVOICE         | 2021000045  | 70        | USD         |                            |                   | 0002 - Signmedia Treasury | payment against invoice |
      | 2021000024 | CASH        |                   | PURCHASEORDER   | 2021000045  | 30        | USD         | 000014 - Insurance service |                   | 0002 - Signmedia Treasury | general payment         |

    #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant      | PostingDate          | CurrencyPrice       | JournalEntryCode |
      | 2021000021 | Shady.Abdelatif | 05-Aug-2021 09:02 AM | 1.0 EGP = 25.50 USD | 2020000024       |
      | 2021000024 | Shady.Abdelatif | 06-Aug-2021 09:02 AM | 1.0 EGP = 30.50 USD | 2020000025       |
    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2019100012 | IMPORT - Landed Cost For Import PO | Draft | Gehan.Ahmed | 03-Jan-2019 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2019100012 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2019100012 | 2021000045 |                   | true        |

    And the last created JournalEntry was with code "2019000000"
    When "Shady.Abdelatif" Activates VendorInvoice of type "IMPORT_GOODS_INVOICE" with code "2021000102" at "07-Jan-2021 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2021 12:00 AM | 17.85         |
    Then VendorInvoice with Code "2021000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Posted |
    And ActivationDetails in VendorInvoice with Code "2021000102" is updated as follows:
      | Accountant      | PostingDate          | DueDate              | LatestExchangeRateCode | CurrencyPrice |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 06-Jan-2021 12:00 AM | 2018000026             | 17.44         |
    Then InvoiceDetails of VendorInvoice with Code "2021000102" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment    |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | USD         | 2021000021-500 |
    Then Summary section of VendorInvoice with Code "2021000102" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount  | DownPayment | TotalRemaining |
      | 5546500.00             | 0.00      | 5546500.00 | 500.00      | 5546000.00     |
    And AccountingDetails in "VendorInvoice" with Code "2021000102" is updated as follows:
      | Credit/Debit | GLAccount                 | GLSubAccount      | Amount  |
      | CREDIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 5546500 |
      | DEBIT        | 70102 - Local Purchasing  | 2021000045        | 75000   |
      | DEBIT        | 10207 - PO                | 2021000045        | 5471500 |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument          | Company          |
      | 2021000001 | 07-Jan-2021 09:30 AM | Shady.Abdelatif | 06-Jan-2021 12:00 AM | 0002 - Signmedia | 2021       | 2021000102 - VendorInvoice | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2021000001":
      | GLAccount                                | GLSubAccount      | Credit     | Debit      | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10221 - service vendors 2                | 000002 - Zhejiang | 5546500.00 | 0.00       | USD                | EGP             | 17.44                     | 2018000026       |
      | 70102 - Local Purchasing                 | 2021000045        | 0.00       | 75000.00   | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO                               | 2021000045        | 0.00       | 5471500.00 | USD                | EGP             | 17.44                     | 2018000026       |
      | 41101 - Realized Differences on Exchange |                   | 0.00       | 4030.00    | EGP                | EGP             | 1.00                      |                  |
      | 10221 - service vendors 2                | 000002 - Zhejiang | 4030.00    | 0.00       | EGP                | EGP             | 1.00                      |                  |
    And LandedCost with Code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Draft |
    And the LandedCostDetails section of LandedCost with code "2019100012" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PurchaseOrder | GoodsInvoice | DamageStock |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 2021000045    | 2021000102   | true        |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47" with the following JournalEntry code "2021000001"
