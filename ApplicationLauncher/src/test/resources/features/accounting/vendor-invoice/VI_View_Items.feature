Feature: ReadItems section VendorInvoice

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
      | hr1         | SuperUser                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Flexo     | VendorInvoiceViewer_Flexo     |
      | SuperUser                       | SuperUserSubRole              |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission              | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadItems | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Flexo     | VendorInvoice:ReadItems | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole              | *:*                     |                                |

    And the following Vendor Invoices with details exist:
      | Code       | Type                 | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000103 | IMPORT_GOODS_INVOICE | 0002         | 000002     | 2018000022    | 1200120301023120 | Draft |

    And the following VendorInvoice has an empty InvoiceItems section:
      | Code       |
      | 2019000105 |

    And VendorInvoice with code "2019000103" has the following InvoiceItems:
      | ItemCode | ItemName                            | QtyOrder | OrderUnit | OrderUnitSymbol | QtyBase  | BaseUnit | BaseUnitSymbol | UnitPrice(Base) | UnitPrice(OrderUnit) | ItemTotalAmount |
      | 000002   | Hot Laminated Frontlit Backlit roll | 50.00    | 0029      | Roll 2.20x50    | 5500.00  | 0019     | M2             | 3.00            | 330.0                | 16500.00        |
      | 000002   | Hot Laminated Frontlit Backlit roll | 100.00   | 0029      | Roll 2.20x50    | 11000.00 | 0019     | M2             | 2.00            | 220.0                | 22000.00        |

  Scenario: (01) View InvoiceItems by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view InvoiceItems section of VendorInvoice with code "2019000103"
    Then the following values of InvoiceItems section for VendorInvoice with code "2019000103" are displayed to "Gehan.Ahmed":
      | ItemCode | ItemName                            | QtyOrder | OrderUnit | OrderUnitSymbol | QtyBase  | BaseUnit | BaseUnitSymbol | UnitPrice(Base) | UnitPrice(OrderUnit) | ItemTotalAmount |
      | 000002   | Hot Laminated Frontlit Backlit roll | 100.00   | 0029      | Roll 2.20x50    | 11000.00 | 0019     | M2             | 2.00            | 220.0                | 22000.00        |
      | 000002   | Hot Laminated Frontlit Backlit roll | 50.00    | 0029      | Roll 2.20x50    | 5500.00  | 0019     | M2             | 3.00            | 330.0                | 16500.00        |

  Scenario: (02) View InvoiceItems by an authorized user where VendorInvoice has an empty InvoiceItems section (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view InvoiceItems section of VendorInvoice with code "2019000105"
    Then an empty InvoiceItems section is displayed to "Gehan.Ahmed"

  Scenario: (03) View InvoiceItems section where VendorInvoice has an empty Items section (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to view InvoiceItems section of VendorInvoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (04) View InvoiceItems section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view InvoiceItems section of VendorInvoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page