#Author: Yara Ameen, Mohamed Aboelnour
#Reviewer: Karim Rostom

Feature: Request Edit Item in Vendor Invoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                                              |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia                   |
      | Amr.Khalil        | PurchasingResponsible_Flexo                       |
      | Amr.Khalil        | PurchasingResponsible_Corrugated                  |
      | Gehan.Ahmed.NoIVR | PurchasingResponsible_Signmedia_CannotReadIVRRole |
      | hr1               | SuperUser                                         |
    And the following roles and sub-roles exist:
      | Role                                              | Subrole                       |
      | PurchasingResponsible_Signmedia                   | VendorInvoiceOwner_Signmedia  |
      | PurchasingResponsible_Signmedia                   | IVRViewer_Signmedia            |
      | PurchasingResponsible_Flexo                       | VendorInvoiceOwner_Flexo      |
      | PurchasingResponsible_Corrugated                  | VendorInvoiceOwner_Corrugated |
      | PurchasingResponsible_Signmedia_CannotReadIVRRole | VendorInvoiceOwner_Signmedia  |
      | SuperUser                                         | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                | Condition                       |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Flexo      | VendorInvoice:UpdateItems | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Corrugated | VendorInvoice:UpdateItems | [purchaseUnitName='Corrugated'] |
      | IVRViewer_Signmedia            | ItemVendorRecord:ReadAll  | [purchaseUnitName='Signmedia']  |
      | SuperUserSubRole              | *:*                       |                                 |
    And the following users doesn't have the following permissions:
      | User              | Permission               |
      | Gehan.Ahmed.NoIVR | ItemVendorRecord:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                         | Condition                      |
      | Amr.Khalil | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |

    And the following Vendor Invoices with details exist:
      | Code       | Type                 | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000103 | IMPORT_GOODS_INVOICE | 0002         | 000002     | 2018000022    | 1200120301023120 | Draft |

    And VendorInvoice with code "2019000103" has the following OrderItems:
      | ItemId | Item                                         | QtyOrder | OrderUnit | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 1      | 000002 - Hot Laminated Frontlit Backlit roll | 50.00    | 0029      | 3.00            | 330.0                |
      | 2      | 000002 - Hot Laminated Frontlit Backlit roll | 100.00   | 0029      | 2.00            | 220.0                |

          #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000103 | 0.00        | 143.00         |

    And edit session is "30" minutes

  ################################################ Request to edit Item scenarios ################################################

  #EBS-4796
  Scenario: (01) Request to Edit/Cancel Item to Vendor Invoice in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    Then edit Item dialoge is opened and VendorInvoiceItems section of Vendor Invoice with code "2019000103" becomes locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields      |
      | quantityInOrderUnit |
      | price               |

  #EBS-4796
  Scenario: (02) Request to Edit/Cancel Item to Vendor Invoice when VendorInvoiceItems section is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first requested to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  #EBS-4796
  Scenario: (03) Request to Edit/Cancel Item to to Vendor Invoice that doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-4796
  @Future
  Scenario: (04) Request to Edit/Cancel Item to Vendor Invoice in a state that doesn't allow this action - All states except Draft (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000006"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And VendorInvoiceItems section of Vendor Invoice with code "2019000006" is not locked by "Gehan.Ahmed"

  #EBS-4796
  Scenario: (05) Request to Edit/Cancel Item to Vendor Invoice by an unauthorized user due to unmatched condition (Exception)
    Given user is logged in as "Amr.Khalil"
    When  "Amr.Khalil" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page


  #EBS-4796
  Scenario: (06) Request to Edit/Cancel Item to Vendor Invoice  after lock session is expire and Vendor Invoice Item doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    When "Gehan.Ahmed" requests to edit Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"

  ################################################ Cancel saving InvoiceItems section ################################################

  #EBS-4796
  Scenario: (07) Request to Edit/Cancel Item to Vendor Invoice in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And VendorInvoiceItems section of Vendor Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" at "07-Jan-2019 09:30 AM"
    Then the lock by "Gehan.Ahmed" on VendorInvoiceItems section of Vendor Invoice with code "2019000103" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  #EBS-4796
  Scenario: (08) Request to Edit/Cancel Item to Vendor Invoice Items section after lock session is expired
    Given user is logged in as "Gehan.Ahmed"
    And VendorInvoiceItems section of Vendor Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  #EBS-4796
  Scenario: (09) Request to Edit/Cancel Item to Vendor Invoice Items section after lock session is expire and Vendor Invoice doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And VendorInvoiceItems section of Vendor Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-4796
  Scenario: (10) Request to Edit/Cancel Item to Vendor Invoice Items section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

  #EBS-4796
  Scenario: (11) Request to Edit/Cancel Item to Vendor Invoice Items section after lock session is expire and Vendor Invoice Item doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And VendorInvoiceItems section of Vendor Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id "1" in VendorInvoiceItems section of Vendor Invoice with code "2019000103" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"