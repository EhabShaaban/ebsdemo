# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
# Reviewer: Somaya Ahmed (??-Feb-2021)
Feature: Delete Vendor Invoice

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Lama.Maher  | PurchasingResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                      | Permission           | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:Delete | [purchaseUnitName='Signmedia'] |

    And the following users have the following permissions without the following conditions:
      | User        | Permission           | Condition                       |
      | Gehan.Ahmed | VendorInvoice:Delete | [purchaseUnitName='Corrugated'] |

    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000002 | IMPORT_GOODS_INVOICE | Draft  | Gehan.Ahmed | 24-Apr-2018 11:00 AM | Ashraf Salah  |
      | 2021000003 | IMPORT_GOODS_INVOICE | Posted | Gehan.Ahmed | 24-Apr-2018 11:00 AM | Ashraf Salah  |
      | 2021000004 | IMPORT_GOODS_INVOICE | Draft  | Amr.Khalil  | 24-Apr-2018 11:00 AM | Ashraf Salah  |

    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit      | Company        |
      | 2021000002 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000004 | 0006 - Corrugated | 0002 - DigiPro |

  #EBS-4807
  Scenario: (01) Delete VendorInvoice draft by an authorized user  (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete VendorInvoice with code "2021000002"
    Then VendorInvoice with code "2021000002" is deleted from the system
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-12"

  #EBS-4807
  Scenario: (02) Delete VendorInvoice draft, where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "Lama.Maher"
    And "Lama.Maher" first deleted the Vendor Invoice with code "2021000002" successfully
    When "Gehan.Ahmed" requests to delete VendorInvoice with code "2021000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-4807
  Scenario: (03) Delete VendorInvoice, where action not allowed per current state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete VendorInvoice with code "2021000003"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-13"

  #EBS-4807
  Scenario: (04) Delete VendorInvoice that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "Lama.Maher"
    And first "Lama.Maher" opens InvoiceDetails section of VendorInvoice with code "2021000002" in edit mode
    When "Gehan.Ahmed" requests to delete VendorInvoice with code "2021000002"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"

  #EBS-4807
  Scenario: (05) Delete VendorInvoice draft by an unauthorized user (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete VendorInvoice with code "2021000004"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page