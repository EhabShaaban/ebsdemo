@Author: Mohamed Aboelnour
Feature: Read Vendor Invoice Summary section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
      | hr1         | SuperUser                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Flexo     | VendorInvoiceViewer_Flexo     |
      | SuperUser                       | SuperUserSubRole              |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadSummary | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Flexo     | VendorInvoice:ReadSummary | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole              | *:*                       |                                |

    And the following Vendor Invoices with details exist:
      | Code       | Type                 | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000103 | IMPORT_GOODS_INVOICE | 0002         | 000002     | 2018000022    | 1200120301023120 | Draft |

    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000103 | 0.00        | 38536          |


  Scenario: (01) View VendorInvoiceSummary by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view Summary section of VendorInvoice with code "2019000103"
    Then the following summary values for VendorInvoice with code "2019000103" are displayed to "Gehan.Ahmed":
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 38500                  | 36.00     | 38536     | 0.00        | 38536          |

  Scenario: (02) View VendorInvoiceSummary section where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to view Summary section of VendorInvoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (03) View VendorInvoiceSummary section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view Summary section of VendorInvoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page