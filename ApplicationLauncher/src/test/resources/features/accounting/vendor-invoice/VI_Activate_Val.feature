Feature: Post Invoice (Val)

  Background:
    Given the following users and roles exist:
      | Name            | Role                  |
      | Shady.Abdelatif | Accountant_Signmedia  |
      | Ahmed.Hamdi     | Accountant_Flexo      |
      | Ahmed.Hamdi     | Accountant_Corrugated |
      | hr1             | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                   |
      | Accountant_Signmedia  | JournalCreator_Signmedia  |
      | Accountant_Flexo      | JournalCreator_Flexo      |
      | Accountant_Corrugated | JournalCreator_Corrugated |
      | SuperUser             | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                       | Condition                      |
      | JournalCreator_Signmedia | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Signmedia'] |
      | JournalCreator_Flexo     | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole         | *:*                              |                                |

        #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000022 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000049 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000017 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000045 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit      | Company          | BankAccountNumber               |
      | 2018000022 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000049 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000017 | 0002 - Signmedia  | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000045 | 0006 - Corrugated | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type             | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000103 | SHIPMENT_INVOICE | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000034 | SHIPMENT_INVOICE | Posted | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000133 | SHIPMENT_INVOICE | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000107 | SHIPMENT_INVOICE | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000131 | SHIPMENT_INVOICE | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor              | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000103 | 2018000022    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000034 | 2018000049    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023700 | 24-Apr-2018 06:31 AM | USD      |
      | 2019000133 | 2018000017    | 000013 - Zhejiang 4 | 20% advance, 80% Copy of B/L | 1200120301023100 | 24-Apr-2018 06:31 AM | USD      |
      | 2019000107 | 2018000045    | 000011 - Vendor 5   | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

       #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit      | Company        |
      | 2019000103 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000034 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000133 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000107 | 0006 - Corrugated | 0002 - DigiPro |
      | 2019000131 | 0001 - Flexo      | 0002 - DigiPro |

     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) |
      | 2019000103 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000034 | 000001 - Hot Laminated Frontlit Fabric roll  | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000133 | 000001 - Hot Laminated Frontlit Fabric roll  | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000107 | 000012 - Ink2                                | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000107 | 000004 - Flex cold foil adhesive E01         | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000107 | 000003 - Flex Primer Varnish E33             | 20.0 M2        | 20.00 M2  | 200             |

    And the following VendorInvoice has an empty InvoiceItems section:
      | Code       |
      | 2019000131 |

   #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |


  Scenario: (01) Activate Vendor Invoice with missing items account (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000107" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Inv-msg-14"

  Scenario: (02) Activate Vendor Invoice with missing vendor account (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000133" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Inv-msg-13"

  Scenario Outline: (03) Activate Vendor Invoice with missing mandatory fields
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000131" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-40"
    And the following fields "<MissingFields>" which sent to "Ahmed.Hamdi" are marked as missing
    Examples:
      | MissingFields                                      |
      | invoiceNumber,invoiceDate,currencyISO,InvoiceItems |

  Scenario: (04) Activate Vendor Invoice , where Invoice doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Shady.Abdelatif" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000103" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"


  Scenario: (05) Activate Vendor Invoice that doesn't allow this action due to state - (Exception)
   Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000034" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-32"

  #EBS-4900
  Scenario Outline: (06) Activate Vendor Invoice of type ServiceVendorInvoice with not existing LandedCost (Exception)

    #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000002 | IMPORT_PO  | DeliveryComplete | Admin     | 2-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000003 | SERVICE_PO | Confirmed        | admin     | 3-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO  | DeliveryComplete | Admin     | 2-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | SERVICE_PO | Confirmed        | admin     | 3-Aug-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2021000003 | 000002 - Zhejiang | 2021000002  |
      | 2021000005 | 000002 - Zhejiang | 2021000004  |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | Draft                 | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
      | 2021000002 | LOCAL - Landed Cost For Local PO   | ActualValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2021000002 |                   | true        |
      | 2021000002 | 2021000004 |                   | true        |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type             | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001 | SHIPMENT_INVOICE | Draft | Shady.Abdelatif | 01-Jan-2021 11:00 AM | Ashraf Salah  |

       #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |

     #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000001 | <DueDocument> | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2021 11:00 AM | USD      |

     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 M2        | 20.00 M2  | 200             |

    And there is no LandedCost in state "EstimatedValuesCompleted" for the following PurchaseOrders:
      | PurchaseOrder |
      | 2021000002    |
      | 2021000004    |

    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2021000001" at "21-Jun-2021 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 07-Oct-2019 09:30 AM | 17.85         |
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Inv-msg-15"

    Examples:
      | DueDocument |
      #ServicePO has LandedCost in state Draft
      | 2021000003  |
      #ServicePO has LandedCost in state ActualValuesCompleted
      | 2021000005  |