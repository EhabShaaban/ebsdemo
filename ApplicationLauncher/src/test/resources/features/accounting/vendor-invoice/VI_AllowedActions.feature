Feature: Allowed Actions Vendor Invoice

  Background:
    Given the following users and roles exist:
      | Name               | Role                            |
      | Gehan.Ahmed        | PurchasingResponsible_Signmedia |
      | Gehan.Ahmed        | LogisticsResponsible_Signmedia  |
      | Shady.Abdelatif    | Accountant_Signmedia            |
      | hr1                | SuperUser                       |
      | Afaf               | FrontDesk                       |
      | CannotReadSections | CannotReadSectionsRole          |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia  |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | LogisticsResponsible_Signmedia  | VendorInvoiceOwner_Signmedia  |
      | LogisticsResponsible_Signmedia  | VendorInvoiceViewer_Signmedia |
      | Accountant_Signmedia            | VendorInvoiceViewer_Signmedia |
      | Accountant_Signmedia            | JournalCreator_Signmedia      |
      | SuperUser                       | SuperUserSubRole              |
      | CannotReadSectionsRole          | CannotReadSectionsSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                         | Condition                      |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:Create               |                                |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateItems          | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateInstallments   | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:Delete               | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadAll              | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadGeneralData      | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadCompany          | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadInvoiceDetail    | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadPostingDetail    | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadItems            | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia       | VendorInvoice:ReadTaxes            | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia       | VendorInvoice:ReadSummary          | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia       | VendorInvoice:ReadInstallments     | [purchaseUnitName='Signmedia'] |
      | JournalCreator_Signmedia      | VendorInvoice:CreateJournalEntry   | [purchaseUnitName='Signmedia'] |
      | CannotReadSectionsSubRole     | VendorInvoice:Create               |                                |
      | CannotReadSectionsSubRole     | VendorInvoice:Delete               |                                |
    And the following Vendor Invoices exist:
      | Code       | State  |
      | 2019000103 | Draft  |
      | 2019000002 | Posted |

  Scenario Outline: (01) Read allowed actions in Vendor Invoice for Authorized Users in Home Screen
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Vendor Invoice home screen
    Then the "<AllowedActions>" are displayed to "<User>":

    Examples:
      | User               | AllowedActions |
      | Gehan.Ahmed        | ReadAll,Create |
      | Shady.Abdelatif    | ReadAll        |
      | CannotReadSections | Create         |

  Scenario: (02) Read allowed actions in Vendor Invoice Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Vendor Invoice home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"

  Scenario Outline: (03) Read allowed actions in Vendor Invoice Object Screen - All States
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Vendor Invoice with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>":

    Examples:
      | User               | Code       | State  | AllowedActions                                                                                                                                                                      |
      | Gehan.Ahmed        | 2019000103 | Draft  | UpdateInvoiceDetails,UpdateItems,UpdateInstallments,ReadGeneralData,ReadCompany,ReadInvoiceDetail,ReadPostingDetail,ReadItems,ReadTaxes,ReadSummary,ReadInstallments,ReadAll,Delete |
      | CannotReadSections | 2019000103 | Draft  | Delete                                                                                                                                                                              |
      | Gehan.Ahmed        | 2019000002 | Posted | ReadGeneralData,ReadCompany,ReadInvoiceDetail,ReadPostingDetail,ReadItems,ReadTaxes,ReadSummary,ReadInstallments,ReadAll                                                            |
      | Shady.Abdelatif    | 2019000103 | Draft  | ReadGeneralData,ReadCompany,ReadInvoiceDetail,ReadPostingDetail,ReadItems,ReadTaxes,ReadSummary,ReadInstallments,ReadAll,CreateJournalEntry                                         |
      | Shady.Abdelatif    | 2019000002 | Posted | ReadGeneralData,ReadCompany,ReadInvoiceDetail,ReadPostingDetail,ReadItems,ReadTaxes,ReadSummary,ReadInstallments,ReadAll                                                            |

  Scenario: (04) Read allowed actions in Vendor Invoice Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Vendor Invoice with code "2019000103"
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"

  Scenario: (05) Read allowed actions in Vendor Invoice Object Screen - Object does not exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to read actions of Vendor Invoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
