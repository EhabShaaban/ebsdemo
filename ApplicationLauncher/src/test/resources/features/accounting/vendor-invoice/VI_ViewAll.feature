Feature: View All Invoice - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | SuperUser                       | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission            | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole              | *:*                   |                                |
    And the following Vendors exist:
      | Code   | Name     | PurchaseUnit     |
      | 000002 | Zhejiang | 0002 - Signmedia |
      | 000001 | Siegwerk | 0001 - Flexo     |
    And the following PurchaseUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |

       #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                   | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000133 | LOCAL_GOODS_INVOICE    | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000131 | CUSTOM_TRUSTEE_INVOICE | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000109 | INSURANCE_INVOICE      | Posted | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000105 | INSURANCE_INVOICE      | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000104 | IMPORT_GOODS_INVOICE   | Posted | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000103 | IMPORT_GOODS_INVOICE   | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2019000102 | IMPORT_GOODS_INVOICE   | Draft  | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
      | 2020000002 | SHIPMENT_INVOICE       | Posted | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000133 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000131 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000109 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000105 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000103 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000102 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000002 | 0002 - Signmedia | 0002 - DigiPro |

     #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor              | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000133 | 2018000017    | 000013 - Zhejiang 4 | 20% advance, 80% Copy of B/L | 1200120301023100 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000131 | 2018000042    | 000001 - Siegwerk   | 20% advance, 80% Copy of B/L |                  | 01-Mar-2019 11:00 AM | USD      |
      | 2019000109 | 2018000003    | 000001 - Siegwerk   | 20% advance, 80% Copy of B/L | 1200120301023400 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000105 | 2020000046    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023800 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000104 | 2018000049    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023110 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000103 | 2018000022    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000102 | 2018000001    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023130 | 01-Mar-2019 11:00 AM | USD      |
      | 2020000002 | 2020000045    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 202045202045     | 01-Mar-2019 11:00 AM | USD      |

  Scenario: (01) Filter View All Vendor Invoices by InvoiceCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendor Invoices with filter applied on InvoiceCode which contains "13" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                   | PurchaseUnitName | Vendor              | PurchaseOrder | InvoiceNumber    | State |
      | 2019000133 | LOCAL_GOODS_INVOICE    | Signmedia        | 000013 - Zhejiang 4 | 2018000017    | 1200120301023100 | Draft |
      | 2019000131 | CUSTOM_TRUSTEE_INVOICE | Flexo            | 000001 - Siegwerk   | 2018000042    |                  | Draft |
    And the total number of records in search results by "hr1" are 2

  Scenario: (02) Filter View All Vendor Invoices by Vendor by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendor Invoices with filter applied on Vendor which contains "iang" with 3 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type                | PurchaseUnitName | Vendor              | PurchaseOrder | InvoiceNumber    | State  |
      | 2020000002 | SHIPMENT_INVOICE    | Signmedia        | 000002 - Zhejiang   | 2020000045    | 202045202045     | Posted |
      | 2019000133 | LOCAL_GOODS_INVOICE | Signmedia        | 000013 - Zhejiang 4 | 2018000017    | 1200120301023100 | Draft  |
      | 2019000105 | INSURANCE_INVOICE   | Signmedia        | 000002 - Zhejiang   | 2020000046    | 1200120301023800 | Draft  |
    And the total number of records in search results by "Gehan.Ahmed" are 6

  Scenario: (03) Filter View All Vendor Invoices by PurchaseUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Vendor Invoices with filter applied on PurchaseUnitName which contains "Flexo" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                   | PurchaseUnitName | Vendor            | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000131 | CUSTOM_TRUSTEE_INVOICE | Flexo            | 000001 - Siegwerk | 2018000042    |                  | Draft  |
      | 2019000109 | INSURANCE_INVOICE      | Flexo            | 000001 - Siegwerk | 2018000003    | 1200120301023400 | Posted |
    Then the total number of records in search results by "hr1" are 2

  Scenario: (04) Filter View All Vendor Invoices by InvoiceType by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 2 of Vendor Invoices with filter applied on InvoiceType which equals "IMPORT_GOODS_INVOICE" with 2 records per page while current locale is "en-US"
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type                 | PurchaseUnitName | Vendor            | PurchaseOrder | InvoiceNumber    | State |
      | 2019000102 | IMPORT_GOODS_INVOICE | Signmedia        | 000002 - Zhejiang | 2018000001    | 1200120301023130 | Draft |
    And the total number of records in search results by "Gehan.Ahmed" are 3

  Scenario: (05) Filter View All Vendor Invoices by PurchaseOrder by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendor Invoices with filter applied on PurchaseOrder which contains "49" with 3 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type                 | PurchaseUnitName | Vendor            | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000104 | IMPORT_GOODS_INVOICE | Signmedia        | 000002 - Zhejiang | 2018000049    | 1200120301023110 | Posted |
    And the total number of records in search results by "Gehan.Ahmed" are 1

  Scenario: (06) Filter View All Vendor Invoices by InvoiceNumber by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendor Invoices with filter applied on InvoiceNumber which contains "31" with 3 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type                 | PurchaseUnitName | Vendor              | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000133 | LOCAL_GOODS_INVOICE  | Signmedia        | 000013 - Zhejiang 4 | 2018000017    | 1200120301023100 | Draft  |
      | 2019000104 | IMPORT_GOODS_INVOICE | Signmedia        | 000002 - Zhejiang   | 2018000049    | 1200120301023110 | Posted |
      | 2019000103 | IMPORT_GOODS_INVOICE | Signmedia        | 000002 - Zhejiang   | 2018000022    | 1200120301023120 | Draft  |
    And the total number of records in search results by "Gehan.Ahmed" are 4

  Scenario: (07) Filter View All Vendor Invoices by State by authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of Vendor Invoices with filter applied on State which equals "Draft" with 3 records per page
    Then the following values will be presented to "Gehan.Ahmed":
      | Code       | Type                 | PurchaseUnitName | Vendor              | PurchaseOrder | InvoiceNumber    | State |
      | 2019000133 | LOCAL_GOODS_INVOICE  | Signmedia        | 000013 - Zhejiang 4 | 2018000017    | 1200120301023100 | Draft |
      | 2019000105 | INSURANCE_INVOICE    | Signmedia        | 000002 - Zhejiang   | 2020000046    | 1200120301023800 | Draft |
      | 2019000103 | IMPORT_GOODS_INVOICE | Signmedia        | 000002 - Zhejiang   | 2018000022    | 1200120301023120 | Draft |
    And the total number of records in search results by "Gehan.Ahmed" are 4

  Scenario: (08) Filter View All Vendor Invoices by InvoiceCode by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Vendor Invoices with filter applied on InvoiceCode which contains "1" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
