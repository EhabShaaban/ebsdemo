Feature: Request Save Invoice Details section in Invoice

  Background:
    Given the following users and roles exist:
      | Name                         | Role                                                         |
      | Amr.Khalil                   | PurchasingResponsible_Flexo                                  |
      | Amr.Khalil                   | PurchasingResponsible_Corrugated                             |
      | Ahmed.Hamdi                  | Accountant_Flexo                                             |
      | Gehan.Ahmed.NoCurrency       | PurchasingResponsible_Signmedia_CannotViewCurrencyRole       |
      | Gehan.Ahmed.NoPaymentTerm    | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole   |
      | Gehan.Ahmed.NoPaymentRequest | PurchasingResponsible_Signmedia_CannotViewPaymentRequestRole |
    And the following roles and sub-roles exist:
      | Role                                                         | Subrole                      |
      | PurchasingResponsible_Flexo                                  | VendorInvoiceOwner_Flexo     |
      #
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole       | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole       | PaymentTermViewer            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole       | PaymentViewer_Signmedia      |
      #
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole   | VendorInvoiceViewer_Signmedia      |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole   | CurrencyViewer               |
      | PurchasingResponsible_Signmedia_CannotViewPaymentTermsRole   | PaymentViewer_Signmedia      |
      #
      | PurchasingResponsible_Signmedia_CannotViewPaymentRequestRole | VendorInvoiceViewer_Signmedia      |
      | PurchasingResponsible_Signmedia_CannotViewPaymentRequestRole | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewPaymentRequestRole | CurrencyViewer               |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                         | Condition                      |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll              | [purchaseUnitName='Signmedia'] |
      | PaymentViewer_Signmedia  | PaymentRequest:ReadAll             | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceOwner_Flexo | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Flexo']     |
      | PaymentTermViewer        | PaymentTerms:ReadAll               |                                |
      | CurrencyViewer           | Currency:ReadAll                   |                                |
    And the following Invoice exists:
      | Code       | State |
      | 2019000103 | Draft |
    And edit session is "30" minutes

  ####### Save Invoice Details section (Happy Path) ##############################################################
  Scenario Outline: (01) Save by an unauthorized users , unauthorized due to condition or authorized user with unauthorized reads  (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves InvoiceDetails section of Invoice with Code "2019000103" at "07-Jan-2019 09:30 AM" with the following values:
      | PaymentTerms | InvoiceNumber    | InvoiceDate          | CurrencyCode | DownPayment |
      | 0002         | 1200120301023125 | 21-Sep-2018 11:00 AM | 0001         | 2019000103  |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                         |
      | Amr.Khalil                   |
      | Ahmed.Hamdi                  |
      | Gehan.Ahmed.NoCurrency       |
      | Gehan.Ahmed.NoPaymentTerm    |
      | Gehan.Ahmed.NoPaymentRequest |
