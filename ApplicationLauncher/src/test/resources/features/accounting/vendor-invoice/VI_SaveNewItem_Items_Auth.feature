Feature: Save Invoice Item (Authorization)

  Background:
    Given the following users and roles exist:
      | Name              | Role                                              |
      | Amr.Khalil        | PurchasingResponsible_Flexo                       |
      | Amr.Khalil        | PurchasingResponsible_Corrugated                  |
      | Gehan.Ahmed.NoIVR | PurchasingResponsible_Signmedia_CannotReadIVRRole |
    And the following roles and sub-roles exist:
      | Role                                              | Subrole                 |
      | PurchasingResponsible_Flexo                       | VendorInvoiceOwner_Flexo      |
      | PurchasingResponsible_Flexo                       | IVROwner_Flexo          |
      | PurchasingResponsible_Corrugated                  | VendorInvoiceOwner_Corrugated |
      | PurchasingResponsible_Flexo                       | IVROwner_Corrugated     |
      | PurchasingResponsible_Signmedia_CannotReadIVRRole | VendorInvoiceOwner_Signmedia  |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                | Condition                       |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Flexo      | VendorInvoice:UpdateItems | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Corrugated | VendorInvoice:UpdateItems | [purchaseUnitName='Corrugated'] |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                | Condition                      |
      | Amr.Khalil | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User              | Permission               |
      | Gehan.Ahmed.NoIVR | ItemVendorRecord:ReadAll |

         #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

        #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000103 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000103 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000103 | 2018000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)    | Qty(Base)  | UnitPrice(Base) |
      | 2019000103 | 000002 - Hot Laminated Frontlit Backlit roll | 100.0 Roll 2.20x50 | 11000.00 M2 | 2.00            |

    And edit session is "30" minutes

  ################################################ Save New Item (Authorization) ################################################

  Scenario Outline: (01) Save new Item in VendorInvoice by an unauthorizd user OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "01-Jan-2019 11:10 AM" with the following values:
      | ItemCode | Qty    | OrderUnit | UnitPrice(Gross) |
      | 000003   | 150.00 | 0036      | 3.50             |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User              |
      | Amr.Khalil        |
      | Gehan.Ahmed.NoIVR |
     
