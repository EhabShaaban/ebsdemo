Feature: Post Invoice (Auth)

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Ahmed.Hamdi | Accountant_Flexo                |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia  |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | Accountant_Flexo                | JournalCreator_Flexo          |
      | Accountant_Flexo                | VendorInvoiceViewer_Flexo     |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission                       | Condition                  |
      | JournalCreator_Flexo | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Flexo'] |

         #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000102 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000102 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000102 | 2018000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)    | Qty(Base)  | UnitPrice(Base) |
      | 2019000102 | 000001 - Hot Laminated Frontlit Fabric roll  | 40.0 Roll 2.70x50 | 40.00 M2   | 100             |
      | 2019000102 | 000002 - Hot Laminated Frontlit Backlit roll | 20.0 Roll 2.70x50 | 2700.00 M2 | 2025            |
      | 2019000102 | 000004 - Flex cold foil adhesive E01         | 100.0 Drum-50Kg   | 5000.00 Kg | 750             |

  # Post invoice by a user who is not authorized to save
  Scenario: (01) Activate Vendor Invoice by an unauthorized user (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" Activates VendorInvoice of type "IMPORT_GOODS_INVOICE" with code "2019000102" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    # Post invoice by a user who is not authorized to save due to condition
  Scenario: (02) Activate Vendor Invoice by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates VendorInvoice of type "IMPORT_GOODS_INVOICE" with code "2019000102" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page
