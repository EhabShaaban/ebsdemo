#Dev Author: Evram Hany, Yara Ameen
#QA Reviewer: Khadra Ali
#Reviewer: Somaya Ahmed and Hosam Bayomy


   #TODO: Rename the file to be VI_Activate_PurchaseServiceInvoice_HP or VI_Activate_PurServiceInvoice_HP
   #TODO: Update scenario (1) to be with at least two DownPayments for 60% of the invoice, to ensure that the remaining is updated
   #TODO: In the givens add more than one fiscal year, to ensure that the system is selecting the right one accroding to the Journal date
   #TODO: In the givens, add the ExchangeRate strategies, and update the statement of the ExchangeRates records to include the (Type), finally make sure there are more than one record for the same strategy with different (ValidFrom) date

Feature: Activate  PurService VendorInvoice (HP)
  Activating a Vendor Invoice (based on Service PO) results in the following:
  - VendorInvoice state is changed to Active
  - VendorInvoice - Accounting Details are determined (Not Developed yet)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document based on Subledgers
  ---- A record for Realized Exchange Rate is added to handle multicurrency if applied (EBS-6934)
  -------- when VendorInvoice Currency is different from the Company Local Currency (in case of Import)
  -------- when Payment (DownPayment) Currency is diffrent from the currency of both the VendorInvoice  and the Company
  - Journal Entries for VendorInvoice is created
  ---- The retrieved fiscal year is the active one where the due date belongs to it
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used to calculate the realized ExRate value)
  - VendorInvoice Activation Details are determined (Done)
  - VendorInvoive Remaining is set as Vendor Invoice minus any previous:
  ---- Payments
  ---- Notes Payables (Future Work)
  - The Purchase Order State is changed to Invoiced or Completed (Future Work)
  - The Remaining of Purchase Order is set to Zero (Future Work)
  - Increase & Decrease related balances (EBS-8514) (Future Work)
  - The actual cost of the CostFactorItems in the invoice are added tot the related LandedCost

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | JournalCreator_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                       | Condition                      |
      | JournalCreator_Signmedia | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Signmedia'] |
    And the following Companies have the following local Currencies:
      | Code | Name    | CurrencyISO |
      | 0002 | DigiPro | EGP         |
    And the following Exchange Rates exist:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       |
      | 2018000007 | USD              | EGP               | 16.32       | 18-Sep-2019 09:02 AM |
      | 2018000008 | USD              | EGP               | 17.54       | 05-Jun-2019 09:02 AM |
      | 2018000010 | USD              | EGP               | 17.54       | 18-Sep-2019 09:00 AM |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Sep-2019 10:00 PM |
    And the following AccountingInfo for Items exist:
      | ItemCode | AccountCode | AccountName      |
      | 000001   | 10207       | PO               |
      | 000002   | 10207       | PO               |
      | 000004   | 70102       | Local Purchasing |
    And the following AccountingInfo for Vendors exist:
      | VendorCode | AccountCode | AccountName       |
      | 000002     | 10221       | service vendors 2 |
    And the following GLAccounts has the following Subledgers:
      | GLAccount                | Subledger |
      | 10207 - PO               | PO        |
      | 70102 - Local Purchasing | PO        |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
    #@INSERT
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Feb-2019 10:00 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2019 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2019 10:00 PM | Central Bank Of Egypt |

  #EBS-7284
  Scenario: (01) Activate Vendor Invoice for Service PurchaseOrder without down payment by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000101 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000100 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000101 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000100 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000101 | 000002 - Zhejiang |             |
      | 2020000100 | 000002 - Zhejiang | 2020000101  |
    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2020000101 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue     | Difference      |
      | 2021000001 | 000056 - Shipment service   | 5000 EGP       | 0 EGP           | 5000 EGP        |
      | 2021000001 | 000055 - Insurance service2 | 4000 EGP       | 1324.365214 EGP | 2675.634786 EGP |
    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type             | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000100 | SHIPMENT_INVOICE | Draft | Shady.Abdelatif | 01-Jan-2019 11:00 AM | Ashraf Salah  |
    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000100 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000100 | 2020000100    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                        | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) |
      | 2019000100 | 000056 - Shipment service   | 20.0 M2        | 20.00 M2  | 200             |
      | 2019000100 | 000055 - Insurance service2 | 10.0 M2        | 10.00 M2  | 60              |
      | 2019000100 | 000057 - Bank Fees          | 10.0 M2        | 10.00 M2  | 40              |
    #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000100 | 0           | 5000           |
    And the last created JournalEntry was with code "2019000000"
    When "Shady.Abdelatif" Activates VendorInvoice of type "SHIPMENT_INVOICE" with code "2019000100" at "07-Sep-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 07-Oct-2019 09:30 AM | 17.85         |
    Then VendorInvoice with Code "2019000100" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Sep-2019 09:30 AM | Posted |
    And ActivationDetails in VendorInvoice with Code "2019000100" is updated as follows:
      | Accountant      | ActivationDate       | DueDate              | LatestExchangeRateCode | CurrencyPrice | FiscalYear |
      | Shady.Abdelatif | 07-Sep-2019 09:30 AM | 07-Oct-2019 09:30 AM | 2018000026             | 17.44         | 2019       |
    Then InvoiceDetails of VendorInvoice with Code "2019000100" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment |
      | Shady.Abdelatif | 07-Sep-2019 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD         |             |
    Then Summary section of VendorInvoice with Code "2019000100" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 5000                   | 0         | 5000      | 0           | 5000           |
    And CostFactorItem "000056 - Shipment service" in LandedCost "2021000001" updated as follows:
      | EstimatedValue | ActualValue | ReferenceDocument             |
      | 5000           | 4000        | SHIPMENT_INVOICE - 2019000100 |
    And CostFactorItem "000055 - Insurance service2" in LandedCost "2021000001" updated as follows:
      | EstimatedValue | ActualValue | ReferenceDocument             |
      | 4000           | 1924.365214 | SHIPMENT_INVOICE - 2019000100 |
    And CostFactorItem "000057 - Bank Fees" in LandedCost "2021000001" updated as follows:
      | EstimatedValue | ActualValue | ReferenceDocument             |
      | 0              | 400         | SHIPMENT_INVOICE - 2019000100 |
    And AccountingDetails in "VendorInvoice" with Code "2019000100" is updated as follows:
      | Credit/Debit | GLAccount                 | GLSubAccount      | Amount |
      | CREDIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 5000   |
      | DEBIT        | 10207 - PO                | 2020000101        | 5000   |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument          | Company        |
      | 2019000001 | 07-Sep-2019 09:30 AM | Shady.Abdelatif | 07-Oct-2019 09:30 AM | 0002 - Signmedia | 2019       | 2019000100 - VendorInvoice | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000001":
      | GLAccount                 | GLSubAccount      | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10221 - service vendors 2 | 000002 - Zhejiang | 5000   | 0     | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO                | 2020000101        | 0      | 5000  | USD                | EGP             | 17.44                     | 2018000026       |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47" with the following JournalEntry code "2019000001"
