Feature: PR Invoice Read All

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission            | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            | Condition |
      | Afaf | VendorInvoice:ReadAll |           |

#    @INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000002 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000004 | LOCAL_GOODS_INVOICE  | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000040 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000105 | LOCAL_GOODS_INVOICE  | Draft  | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000107 | IMPORT_GOODS_INVOICE | Draft  | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000108 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000109 | LOCAL_GOODS_INVOICE  | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000131 | IMPORT_GOODS_INVOICE | Draft  | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000133 | IMPORT_GOODS_INVOICE | Draft  | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor              | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000002 | 2018000002    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000004 | 2018000002    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000040 | 2018000002    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000104 | 2018000002    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000105 | 2018000002    | 000002 - Zhejiang   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000107 | 2018000002    | 000011 - Vendor 5   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000108 | 2018000002    | 000011 - Vendor 5   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000109 | 2018000002    | 000001 - Siegwerk   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000131 | 2018000002    | 000001 - Siegwerk   | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |
      | 2019000133 | 2018000002    | 000013 - Zhejiang 4 | 20% advance, 80% Copy of B/L | 1200120301023120 | 01-Mar-2019 11:00 AM | USD      |


#    @INSERT
    Given the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000002 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000040 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000105 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000107 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2019000108 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2019000109 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2019000131 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000133 | 0001 - Flexo     | 0002 - DigiPro   |

          #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000002 | 0.00        | 224            |
      | 2019000004 | 0.00        | 11385          |
      | 2019000040 | 0.00        | 20008.8        |
      | 2019000104 | 0.00        | 422            |
      | 2019000105 | 0.00        | 0              |
      | 2019000107 | 0.00        | 10376          |
      | 2019000108 | 0.00        | 52857.6        |
      | 2019000109 | 0.00        | 720            |
      | 2019000131 | 0.00        | 0              |
      | 2019000133 | 0.00        | 200            |

  Scenario: (01) Read All Invoices for DueDocument Dropdown in PaymentRequest by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all activated VendorInvoices for Payment with Remaining greater than zero and BusinessUnit with Code "0002" and  Vendor with Code "000002"
    Then the following invoices should be returned to "Gehan.Ahmed":
      | Code       | Company        |
      | 2019000104 | 0002 - DigiPro |
      | 2019000040 | 0002 - DigiPro |
      | 2019000004 | 0002 - DigiPro |
      | 2019000002 | 0002 - DigiPro |


    And total number of invoices returned to "Gehan.Ahmed" is equal to 4

  Scenario: (02) Read All Invoices for DueDocument Dropdown in PaymentRequest by unauthorized user (Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all activated VendorInvoices for Payment with Remaining greater than zero and BusinessUnit with Code "0002" and  Vendor with Code "000002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
