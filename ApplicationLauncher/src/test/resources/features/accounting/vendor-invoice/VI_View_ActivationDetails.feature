# Author Dev: Mohamed Aboelnour
# Author Quality: Ehab S.
Feature: View Activation Details section in Vendor Invoice

  Background:
    Given the following users and roles exist:
      | Name            | Role                            |
      | Shady.Abdelatif | Accountant_Signmedia            |
      | Gehan.Ahmed     | PurchasingResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Sub-role                      |
      | Accountant_Signmedia            | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                      | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadPostingDetail | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadPostingDetail | [purchaseUnitName='Signmedia'] |

    And the following Vendor Invoices exist:
      | Code       | Type                   | PurchaseUnit     | Vendor            | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000002 | IMPORT_GOODS_INVOICE   | 0002 - Signmedia | 000002 - Zhejiang | 2018000010    | 1200120301023700 | Posted |
      | 2019000004 | CUSTOM_TRUSTEE_INVOICE | 0002 - Signmedia | 000002 - Zhejiang | 2020000046    | 1200120301023700 | Posted |
      | 2019000105 | INSURANCE_INVOICE      | 0002 - Signmedia | 000002 - Zhejiang | 2020000046    | 1200120301023800 | Draft  |
      | 2019000108 | LOCAL_GOODS_INVOICE    | 0001 - Flexo     | 000011 - Vendor 5 | 2018000044    | 1200120301023500 | Posted |

    And the following PostingDetails for AccountingDocument with DocumentType "VendorInvoice" exist:
      | Code       | CreationDate         | CreatedBy | Accountant | ExchangeRateCode | JournalEntryCode | CurrencyPrice |
      | 2019000002 | 28-Oct-2019 09:30 AM | hr1       | hr1        | 2018000026       | 2020000026       | 10 EGP        |
      | 2019000004 | 28-Oct-2019 09:30 AM | hr1       | hr1        | 2018000026       | 2020000027       | 10 EGP        |
      | 2019000108 | 27-Jul-2019 09:30 AM | hr1       | hr1        | 2018000026       | 2020000021       | 16.5 EGP      |


  Scenario Outline: (01) View PostingDetails section in VendorInvoice by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view PostingDetails section of VendorInvoice with code "<Code>"
    Then the following values of PostingDetails section for VendorInvoice with code "<Code>" are displayed to "Shady.Abdelatif":
      | Code   | PostingDate   | PostedBy   | JournalEntryCode   | CurrencyPrice   | ExchangeRateCode   | FiscalPeriod   |
      | <Code> | <PostingDate> | <PostedBy> | <JournalEntryCode> | <CurrencyPrice> | <ExchangeRateCode> | <FiscalPeriod> |

    Examples:
      | Code       | PostingDate          | PostedBy | JournalEntryCode | CurrencyPrice      | ExchangeRateCode | FiscalPeriod |
      | 2019000002 | 02-Sep-2020 09:37 AM | hr1      | 2020000026       | 1.0 USD = 10.0 EGP | 2018000026       | 2020         |
      | 2019000004 | 03-Sep-2020 08:07 AM | hr1      | 2020000027       | 1.0 USD = 10.0 EGP | 2018000026       | 2020         |

  Scenario: (02) View PostingDetails section in VendorInvoice where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "Gehan.Ahmed"
    And "Gehan.Ahmed" first deleted the Vendor Invoice with code "2019000105" successfully
    When "Shady.Abdelatif" requests to view PostingDetails section of VendorInvoice with code "2019000105"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  Scenario: (03) View PostingDetails section in VendorInvoice by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view PostingDetails section of VendorInvoice with code "2019000108"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
