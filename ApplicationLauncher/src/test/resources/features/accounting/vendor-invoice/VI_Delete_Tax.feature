# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam
# Reviewer: Somaya Ahmed (?-Aug-2020)
Feature: Delete Tax in Vendor Invoice

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Sub-role                     |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole             |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateTaxes | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole             | *:*                       |                                |

    And the following users have the following permissions without the following conditions:
      | User        | Permission                | Condition                  |
      | Gehan.Ahmed | VendorInvoice:UpdateTaxes | [purchaseUnitName='Flexo'] |

    And the following Vendor Invoices exist:
      | Code       | Type                   | PurchaseUnit     | Vendor            | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000133 | LOCAL_GOODS_INVOICE    | 0002             | 000013            | 2018000017    | 1200120301023100 | Draft  |
      | 2019000034 | SHIPMENT_INVOICE       | 0002 - Signmedia | 000002 - Zhejiang | 2018000049    | 1200120301023700 | Posted |
      | 2019000131 | CUSTOM_TRUSTEE_INVOICE | 0001 - Flexo     | 000002 - Zhejiang | 2018000042    | 1200120301023300 | Draft  |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000133"
      | Tax                      | TaxPercentage | TaxAmount |
      | 0001 - Vat               | 1             | 2.00      |
      | 0003 - Real estate taxes | 10            | 20.00     |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000034"
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 24.00     |
      | 0014 - Service tax  | 2             | 4.80      |

    And the following TaxDetails exist in taxes section for VendorInvoice "2019000131"
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 0.00      |
      | 0014 - Service tax  | 2             | 0.00      |

 #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000133 | 0.00        | 200.00         |
      | 2019000034 | 0.00        | 350            |
      | 2019000131 | 0.00        | 0              |

  #EBS-6868
  Scenario: (01) Delete Tax from VendorInvoice, where VendorInvoice is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete Tax with code "0001" in VendorInvoice with code "2019000133"
    Then Tax with code "0001" from VendorInvoice with code "2019000133" is deleted
    And InvoiceTaxes section of VendorInvoice with Code "2019000133" is updated as follows:
      | Tax                      | TaxPercentage | TaxAmount |
      | 0003 - Real estate taxes | 10            | 20.00     |
    And InvoiceSummaries section of VendorInvoice with Code "2019000133" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 200.00                 | 20.00     | 220.00    | 0.00        | 220.00         |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-12"
#
  #EBS-6868
  Scenario: (02) Delete Tax from VendorInvoice, where Tax doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted Tax with code "0001" of VendorInvoice with code "2019000133" successfully
    When "Gehan.Ahmed" requests to delete Tax with code "0001" in VendorInvoice with code "2019000133"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-20"

  #EBS-6868
  Scenario: (03) Delete Tax from VendorInvoice, where action not allowed per current state (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete Tax with code "0014" in VendorInvoice with code "2019000034"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-13"

  #EBS-6868
  Scenario: (04) Delete Tax from VendorInvoice, where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000133" successfully
    When "Gehan.Ahmed" requests to delete Tax with code "0001" in VendorInvoice with code "2019000133"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #EBS-6868
  Scenario: (05) Delete Tax from VendorInvoice by Unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to delete Tax with code "0001" in VendorInvoice with code "2019000133"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-6868
  Scenario: (06) Delete Tax from VendorInvoice by Unauthorized user due condition(Unauthorized access/Abuse case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete Tax with code "0014" in VendorInvoice with code "2019000131"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-6868
  @Future
  Scenario: (07) Delete Tax from VendorInvoice when section is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first opened Taxes section of VendorInvoice with code "2019000133" in the edit mode successfully
    When "Gehan.Ahmed" requests to edit Taxes section of VendorInvoice with code "2019000133"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"
