Feature: Save Invoice Item

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia | IVRViewer_Signmedia           |

    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |
      | IVRViewer_Signmedia           | ItemVendorRecord:ReadAll  | [purchaseUnitName='Signmedia'] |

  #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type     | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000046 | LOCAL_PO | Shipped | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2018000046 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001 | LOCAL_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000001 | 2018000046    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | EGP      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit)     | Qty(Base)   | UnitPrice(Base) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 100.0 Roll 2.20x50 | 11000.00 M2 | 1.80            |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 50.0 Roll 2.70x50  | 50.00 M2    | 3.00            |

     #@INSERT
    And the following VendorInvoices Taxes exist:
      | Code       | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000001 | 0003 - Real estate taxes                     | 10            | 1995.00   |
      | 2021000001 | 0001 - Vat                                   | 1             | 199.50    |
      | 2021000001 | 0002 - Commercial and industrial profits tax | 0.5           | 99.75     |
     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2021000001 | 0.00        | 22244.25       |

    And the following IVRs exit:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 34567891011      | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000004  | 000004   | 000002     | 0039    | GDH670-888       | 4567891011       | 50.00    | 0002         | 0002             | 02-Aug-2018 10:30 AM |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000006  | 000002   | 000002     | 0032    | GDF730-440       | 67891011         | 600.40   | 0004         | 0002             | 02-Aug-2018 10:30 AM |
      | 000007  | 000003   | 000002     | 0037    | GDF830-440       | 78910111         | 50.30    | 0005         | 0002             | 02-Aug-2018 10:30 AM |
      | 000009  | 000004   | 000002     | 0038    | GDF530-200       | 9101112          | 100.30   | 0006         | 0002             | 02-Aug-2018 10:30 AM |

    ################################################ Request to add Item scenarios ################################################
  Scenario: (01) Save new Item in VendorInvoice - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2021000001" is locked by "Gehan.Ahmed" at "01-Jan-2019 11:00 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2021000001" at "01-Jan-2019 11:10 AM" with the following values:
      | ItemCode | Qty    | OrderUnit | UnitPrice(Gross) |
      | 000003   | 150.00 | 0036      | 3.50             |
    Then VendorInvoice with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State |
      | Gehan.Ahmed   | 01-Jan-2019 11:10 AM | Draft |
    And InvoiceItems section of Vendor Invoice with Code "2021000001" is updated as follows:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) | UnitPrice(OrderUnit) |
      | 000001   | 100.00   | 0029      | 1.8             | 198.0                |
      | 000001   | 50.00    | 0033      | 3.0             | 3.0                  |
      | 000003   | 150.00   | 0036      | 3.50            | 35.0                 |
    And InvoiceTaxes section of Vendor Invoice with Code "2021000001" is updated as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0003 - Real estate taxes                     | 10            | 2520.00   |
      | 0001 - Vat                                   | 1             | 252.00    |
      | 0002 - Commercial and industrial profits tax | 0.5           | 126.00    |
    And InvoiceSummaries section of VendorInvoice with Code "2021000001" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 25200.00               | 2898.00   | 28098.00  | 0.00        | 28098.00       |
    And the lock by "Gehan.Ahmed" on InvoiceItems section of Vendor Invoice with Code "2021000001" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-16"


