# Author Dev: Yara Ameen, Mohamed Aboelnour, Evram Hany
# Author Quality: Shrouk Alaa , Marina Salah, Khadrah Ali

Feature: Create VendorInvoice (HP)
  - Creating a VendorInvoice results in the following:
  - 1) a new VendorInvoice record is created with state draft  (EBS-4359)
  - 2) the following data are retrieved from the reference PurchaseOrder of the VendorInvoice:
  - 2.1) PurchaseOrder BusinessUnit (EBS-4359)
  - 2.2) PurchaseOrder Company (EBS-4359)
  - 2.3) PurchaseOrder Currency (EBS-4359)
  - 2.4) PurchaseOrder PaymentTerm (EBS-4359)
  - 2.5) PurchaseOrder items (EBS-6045) (EBS-7282)
  - 3) the VendorInvoice Taxes are retrieved with the following criteria:
  - 3.1) Service VendorInvoice retrieves the Taxes from the related Vendor
  - 3.2) Local VendorInvoice retrieves the Taxes from the Local Company (EBS-8196)
  - 3.3) Import VendorInvoice does Not retrieve Taxes (EBS-8733)

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia           |
      | PurchasingResponsible_Signmedia | VendorViewer_Signmedia       |
      | PurchasingResponsible_Signmedia | DocumentOwnerViewer          |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission            | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:Create  |                                |
      | POViewer_Signmedia           | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
      | VendorViewer_Signmedia       | Vendor:ReadAll        | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll |                                |

    And the following Vendors exist:
      | Code   | Name     | PurchaseUnit     |
      | 000002 | Zhejiang | 0002 - Signmedia |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | VendorInvoice  | CanBeDocumentOwner |           |

  Scenario: (01) Create VendorInvoice for Local Goods, with all mandatory fields, by an authorized user, and entering PO in Confirmed state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type     | State     | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000017 | LOCAL_PO | Confirmed | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000017" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000001   | 50.00    | 0037      | 4.00            |
    And the Company "0002 - DigiPro" has the following Taxes:
      | Tax                                          | TaxPercentage |
      | 0001 - Vat                                   | 1.0           |
      | 0002 - Commercial and industrial profits tax | 0.5           |
      | 0003 - Real estate taxes                     | 10.0          |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                | Vendor | PurchaseOrder | DocumentOwner |
      | LOCAL_GOODS_INVOICE | 000002 | 2018000017    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | LOCAL_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000017    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                        | Qty(OrderUnit)  | Qty(Base) | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000001 - Hot Laminated Frontlit Fabric roll | 50.00 Drum-50Kg | 50.00 M2  | 4.00                 | 4.00            |
    And the Taxes Section of VendorInvoice with code "2020000005" is created as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1.00          | 2.00      |
      | 0002 - Commercial and industrial profits tax | 0.5           | 1.00      |
      | 0003 - Real estate taxes                     | 10.00         | 20.00     |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  Scenario: (02) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in FinishedProduction state (Happy Path)

    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State              | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000009 | IMPORT_PO | FinishedProduction | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000009" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000005   | 2.00     | 0034      | 2.00            |
      | 000001   | 50.00    | 0037      | 4.00            |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000009    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000009    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                                           | Qty(OrderUnit)    | Qty(Base) | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000001 - Hot Laminated Frontlit Fabric roll                    | 50.00 Drum-50Kg   | 50.00 M2  | 4.00                 | 4.00            |
      | 000005 - self adhesive vinal 100 micron 140 grm glossy 1.07x50 | 2.00 Roll 1.07x50 | 107.00 M2 | 107.00               | 2.00            |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  Scenario: (03) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in Shipped state (Happy Path)

    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State   | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000010 | IMPORT_PO | Shipped | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000010" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000002   | 50.00    | 0037      | 4.00            |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000010    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000010    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                         | Qty(OrderUnit)  | Qty(Base) | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000002 - Hot Laminated Frontlit Backlit roll | 50.00 Drum-50Kg | 50.00 M2  | 4.00                 | 4.00            |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"


  Scenario: (04) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in Arrived state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State   | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000011 | IMPORT_PO | Arrived | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000011" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000012   | 33.00    | 0042      | 11.00           |
      | 000001   | 50.00    | 0037      | 4.00            |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000011    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000011    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                        | Qty(OrderUnit)      | Qty(Base)   | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000012 - Ink2                               | 33.00 Bottle-2Liter | 66.00 Liter | 22.00                | 11.00           |
      | 000001 - Hot Laminated Frontlit Fabric roll | 50.00 Drum-50Kg     | 50.00 M2    | 4.00                 | 4.00            |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"


  Scenario: (05) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in Cleared state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State   | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000001 | IMPORT_PO | Cleared | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000001" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000003   | 10.00    | 0037      | 15.00           |
      | 000002   | 20.00    | 0033      | 15.00           |
      | 000001   | 40.00    | 0029      | 100.00          |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000001    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000001    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                         | Qty(OrderUnit)     | Qty(Base)  | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000003 - Flex Primer Varnish E33             | 10.00 Drum-50Kg    | 500.00 Kg  | 750.00               | 15.00           |
      | 000002 - Hot Laminated Frontlit Backlit roll | 20.00 Roll 2.70x50 | 2700.00 M2 | 2025.00              | 15.00           |
      | 000001 - Hot Laminated Frontlit Fabric roll  | 40.00 Roll 2.20x50 | 4400.00 M2 | 11000.00             | 100.00          |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  Scenario: (06) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in Delivery Complete state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State            | Currency   | PaymentTerms                        | Company          | PurchaseUnit     | Vendor            |
      | 2018000002 | IMPORT_PO | DeliveryComplete | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0001 - AL Madina | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000002" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000004   | 100.00   | 0003      | 12.00           |
      | 000003   | 100.00   | 0003      | 10.00           |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000002    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000002    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                 | Qty(OrderUnit) | Qty(Base) | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000004 - Flex cold foil adhesive E01 | 100.00 Kg      | 100.00 Kg | 12.00                | 12.00           |
      | 000003 - Flex Primer Varnish E33     | 100.00 Kg      | 100.00 Kg | 10.00                | 10.00           |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  Scenario: (07) Create VendorInvoice for Import Goods, with all mandatory fields, by an authorized user, and entering PO in Cleared state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type      | State   | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000050 | IMPORT_PO | Cleared | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    And PurchaseOrder with code "2018000050" has the following OrderItems:
      | ItemCode | QtyOrder | OrderUnit | UnitPrice(Base) |
      | 000001   | 50.00    | 0033      | 3.00            |
      | 000001   | 100.00   | 0029      | 1.80            |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type                 | Vendor | PurchaseOrder | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 000002 | 2018000050    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type                 | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | IMPORT_GOODS_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2018000050    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                                        | Qty(OrderUnit)      | Qty(Base)   | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000001 - Hot Laminated Frontlit Fabric roll | 50.00 Roll 2.70x50  | 50.00 M2    | 3.00                 | 3.00            |
      | 000001 - Hot Laminated Frontlit Fabric roll | 100.00 Roll 2.20x50 | 11000.00 M2 | 198.00               | 1.80            |
    And the Taxes Section of VendorInvoice with code "2020000005" is displayed empty
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"

  Scenario: (08) Create VendorInvoice for Purchase Local Services, with all mandatory fields, by an authorized user, and entering PO in Confirmed state (Happy Path)
    Given the following PurchaseOrders exist with the company data:
      | Code       | Type     | State     | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000017 | LOCAL_PO | Confirmed | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
        #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000044 | SERVICE_PO | Confirmed | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2020000044 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000044 | 000002 - Zhejiang | 2018000017  |
    #@INSERT
    And the following PaymentTermData for PurchaseOrders exist:
      | Code       | Currency   | PaymentTerms                        |
      | 2020000044 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
    #@INSERT
    And the following ItemsData for PurchaseOrders exist:
      | Code       | POItemId | Item                        | OrderUnit | Quantity | Price   | ItemTotalAmount |
      | 2020000044 | 100      | 000057 - Bank Fees          | 0019 - M2 | 16.000   | 200.000 | 3200.000        |
      | 2020000044 | 102      | 000055 - Insurance service2 | 0019 - M2 | 20.000   | 200.000 | 4000.000        |
    #@INSERT
    And the following PurchaseOrders have the following TaxDetails:
      | Code       | Tax                 | TaxPercentage |
      | 2020000044 | 0013 - Shipping tax | 10            |
      | 2020000044 | 0014 - Service tax  | 2             |
    And the Vendor "000002 - Zhejiang" has the following Taxes:
      | Tax                 | TaxPercentage |
      | 0013 - Shipping tax | 10            |
      | 0014 - Service tax  | 2             |
    Given user is logged in as "Gehan.Ahmed"
    And CurrentDateTime = "10-Dec-2020 11:00 AM"
    And Last created VendorInvoice was with code "2020000004"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type             | Vendor | PurchaseOrder | DocumentOwner |
      | SHIPMENT_INVOICE | 000002 | 2020000044    | 34            |
    Then a new VendorInvoice is created with the following values:
      | Code       | State | CreatedBy   | LastUpdatedBy | CreationDate         | LastUpdateDate       | Type             | DocumentOwner |
      | 2020000005 | Draft | Gehan.Ahmed | Gehan.Ahmed   | 10-Dec-2020 11:00 AM | 10-Dec-2020 11:00 AM | SHIPMENT_INVOICE | Ashraf Salah  |
    And the Company Section of VendorInvoice with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    Then the Details Section of VendorInvoice with Code "2020000005" is created as follows:
      | PurchaseOrder | Vendor            | PaymentTerms                        | InvoiceNumber | InvoiceDate | Currency   | DownPayment |
      | 2020000044    | 000002 - Zhejiang | 0002 - 20% advance, 80% Copy of B/L |               |             | 0002 - USD |             |
    And the Items Section of VendorInvoice with code "2020000005" is created as follows:
      | Item                        | Qty(OrderUnit) | Qty(Base) | UnitPrice(OrderUnit) | UnitPrice(Base) |
      | 000057 - Bank Fees          | 16.00 M2       | 16.00 M2  | 200.00               | 200.00          |
      | 000055 - Insurance service2 | 20.00 M2       | 20.00 M2  | 200.00               | 200.00          |
    And the Taxes Section of VendorInvoice with code "2020000005" is created as follows:
      | Tax                 | TaxPercentage | TaxAmount |
      | 0013 - Shipping tax | 10            | 720.00    |
      | 0014 - Service tax  | 2             | 144.00    |
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-11"
