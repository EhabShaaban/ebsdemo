Feature: Request Add/Edit/Cancel Item in Invoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                                              |
      | Gehan.Ahmed       | PurchasingResponsible_Signmedia                   |
      | Amr.Khalil        | PurchasingResponsible_Flexo                       |
      | Amr.Khalil        | PurchasingResponsible_Corrugated                  |
      | Gehan.Ahmed.NoIVR | PurchasingResponsible_Signmedia_CannotReadIVRRole |
      | hr1               | SuperUser                                         |
    And the following roles and sub-roles exist:
      | Role                                              | Subrole                       |
      | PurchasingResponsible_Signmedia                   | VendorInvoiceOwner_Signmedia  |
      | PurchasingResponsible_Signmedia                   | IVRViewer_Signmedia            |
      | PurchasingResponsible_Flexo                       | VendorInvoiceOwner_Flexo      |
      | PurchasingResponsible_Corrugated                  | VendorInvoiceOwner_Corrugated |
      | PurchasingResponsible_Signmedia_CannotReadIVRRole | VendorInvoiceOwner_Signmedia  |
      | SuperUser                                         | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                | Condition                       |
      | VendorInvoiceOwner_Signmedia  | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceOwner_Flexo      | VendorInvoice:UpdateItems | [purchaseUnitName='Flexo']      |
      | VendorInvoiceOwner_Corrugated | VendorInvoice:UpdateItems | [purchaseUnitName='Corrugated'] |
      | IVRViewer_Signmedia            | ItemVendorRecord:ReadAll  | [purchaseUnitName='Signmedia']  |
      | SuperUserSubRole              | *:*                       |                                 |
    And the following users doesn't have the following permissions:
      | User              | Permission               |
      | Gehan.Ahmed.NoIVR | ItemVendorRecord:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User       | Permission                         | Condition                      |
      | Amr.Khalil | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |

    And the following Vendor Invoices with details exist:
      | Code       | Type                 | PurchaseUnit | VendorCode | PurchaseOrder | InvoiceNumber    | State |
      | 2019000103 | IMPORT_GOODS_INVOICE | 0002         | 000002     | 2018000022    | 1200120301023120 | Draft |

    And edit session is "30" minutes

    ################################################ Request to add Item scenarios ################################################

  Scenario: (01) Request to Add/Cancel Item to Invoice in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item to InvoiceItems section of Invoice with code "2019000103"
    Then a new add Item dialoge is opened and InvoiceItems section of Invoice with code "2019000103" becomes locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Gehan.Ahmed":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |

  Scenario: (02) Request to Add/Cancel Item to Invoice when InvoiceItems section is locked by another user (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to InvoiceItems section of Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to add Item to InvoiceItems section of Invoice with code "2019000103"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-02"

  Scenario: (03) Request to Add/Cancel Item to Invoice that doesn't exist (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to add Item to InvoiceItems section of Invoice with code "2019000103"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  @Future
  Scenario: (04) Request to Add/Cancel Item to Invoice in a state that doesn't allow this action - All states except Draft (Exception)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to add Item to InvoiceItems section of Invoice with code "2019000106"
    And an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-03"
    And InvoiceItems section of Invoice with code "2019000106" is not locked by "Gehan.Ahmed"


  Scenario: (05) Request to Add/Cancel Item to Invoice by an unauthorized user due to unmatched condition (Exception)
    Given user is logged in as "Amr.Khalil"
    When  "Amr.Khalil" requests to add Item to InvoiceItems section of Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page

  Scenario: (07) Request to Add/Cancel Item to Invoice by a User with one or more roles - All allowed states (Happy Path)
    Given user is logged in as "Gehan.Ahmed.NoIVR"
    When "Gehan.Ahmed.NoIVR" requests to add Item to InvoiceItems section of Invoice with code "2019000103"
    Then a new add Item dialoge is opened and InvoiceItems section of Invoice with code "2019000103" becomes locked by "Gehan.Ahmed.NoIVR"
    And there are no authorized reads returned to "Gehan.Ahmed.NoIVR"
    And the following mandatory fields are returned to "Gehan.Ahmed.NoIVR":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |
    And the following editable fields are returned to "Gehan.Ahmed.NoIVR":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |



    ######### Cancel saving InvoiceItems section ########################################################################

  Scenario: (08) Request to Add/Cancel Item to Invoice in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" cancels saving InvoiceItems section of Invoice with code "2019000103" at "07-Jan-2019 09:30 AM"
    Then the lock by "Gehan.Ahmed" on InvoiceItems section of Invoice with code "2019000103" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

  Scenario: (09) Request to Add/Cancel Item to Invoice Items section after lock session is expired
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    When "Gehan.Ahmed" cancels saving InvoiceItems section of Invoice with code "2019000103" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (10) Request to Add/Cancel Item to Invoice Items section after lock session is expire and Invoice doesn't exsit (Exception)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully at "07-Jan-2019 09:31 AM"
    When "Gehan.Ahmed" cancels saving InvoiceItems section of Invoice with code "2019000103" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"


  Scenario: (11) Request to Add/Cancel Item to Invoice Items section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" cancels saving InvoiceItems section of Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
