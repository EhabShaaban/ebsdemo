#Dev Author: Evram Hany, Yara Ameen
#QA Reviewer: Ehab Shaaban
#Reviewer: Somaya Ahmed and Hosam Bayomy

Feature: Activate Local VendorInvoice (HP)
  Activating a Vendor Invoice (based on Local PO) results in the following:
  - Vendor Invoice state is changed to Active (EBS-5158)
  - Vendor Invoice - Accounting Details are determined (EBS-4850 for local)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document based on Subledgers
  - Journal Entries for Vendor Invoice is created (EBS-4649)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8167)
  ---- The latest ExchangeRate is retrieved based on the default strategy (EBS-4320)
  - Vendor Invoice Activation Details are determined (Done)
  - Vendor Invoive Remaining is set as Vendor Invoice  minus any previous:
  ---- Payments (EBS-8437)
  ---- Notes Payables (Future)
  - The Purchase Order State is changed to Invoiced or Completed (Future Work)
  - The Remaining of Purchase Order is set to Zero (Future Work)
  - Increase & Decrease related balances (EBS-8514) (Future Work)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                  |
      | Accountant_Signmedia | JournalCreator_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission                       | Condition                      |
      | JournalCreator_Signmedia | VendorInvoice:CreateJournalEntry | [purchaseUnitName='Signmedia'] |

    And the following Companies have the following local Currencies:
      | Code | Name    | CurrencyISO |
      | 0002 | DigiPro | EGP         |

    And the following AccountingInfo for Items exist:
      | ItemCode | AccountCode | AccountName      |
      | 000001   | 10207       | PO               |
      | 000002   | 10207       | PO               |
      | 000004   | 70102       | Local Purchasing |

    And the following AccountingInfo for Vendors exist:
      | VendorCode | AccountCode | AccountName       |
      | 000002     | 10221       | service vendors 2 |

    And the following GLAccounts has the following Subledgers:
      | GLAccount                | Subledger |
      | 10207 - PO               | PO        |
      | 70102 - Local Purchasing | PO        |

      #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |

    #@INSERT
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And  the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Feb-2019 10:00 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2019 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2019 10:00 PM | Central Bank Of Egypt |

    #EBS-8907
  Scenario: (01) Activate Vendor Invoice for Local PurchaseOrder with down payment by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type     | State   | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000046 | LOCAL_PO | Shipped | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2018000046 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001 | LOCAL_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2021000001 | 2018000046    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit)     | Qty(Base)   | UnitPrice(Base) |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 100.0 Roll 2.20x50 | 11000.00 M2 | 1.80            |
      | 2021000001 | 000001 - Hot Laminated Frontlit Fabric roll | 50.0 Roll 2.70x50  | 50.00 M2    | 3.00            |

     #@INSERT
    And the following VendorInvoices Taxes exist:
      | Code       | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000001 | 0003 - Real estate taxes                     | 10            | 1995.00   |
      | 2021000001 | 0001 - Vat                                   | 1             | 199.50    |
      | 2021000001 | 0002 - Commercial and industrial profits tax | 0.5           | 99.75     |
     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2021000001 | 0.00        | 22244.25       |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                           | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2019100013 | LOCAL - Landed Cost For Local PO | Draft | Gehan.Ahmed | 03-Jan-2019 12:10 PM | Shady.Abdelatif |
    #@INSERT|
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2019100013 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2019100013 | 2018000046 |                   | true        |

    And the last created JournalEntry was with code "2019000000"
    When "Shady.Abdelatif" Activates VendorInvoice of type "LOCAL_GOODS_INVOICE" with code "2021000001" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              | CurrencyPrice |
      | 06-Jan-2019 12:00 AM | 17.85         |
    Then VendorInvoice with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | Posted |
    And ActivationDetails in VendorInvoice with Code "2021000001" is updated as follows:
      | Accountant      | PostingDate          | DueDate              | LatestExchangeRateCode | CurrencyPrice |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 2018000026             | 17.44         |
    Then InvoiceDetails of VendorInvoice with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD         |             |
    Then Summary section of VendorInvoice with Code "2021000001" is updated as follows:
      | TotalAmountBeforeTaxes | TaxAmount | NetAmount | DownPayment | TotalRemaining |
      | 19950.00               | 2294.25   | 22244.25  | 00.00       | 22244.25       |
    And AccountingDetails in "VendorInvoice" with Code "2021000001" is updated as follows:
      | Credit/Debit | GLAccount                 | GLSubAccount      | Amount   |
      | CREDIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 22244.25 |
      | DEBIT        | 10207 - PO                | 2018000046        | 22244.25 |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument          | Company        |
      | 2019000001 | 07-Jan-2019 09:30 AM | Shady.Abdelatif | 06-Jan-2019 12:00 AM | 0002 - Signmedia | 2019       | 2021000001 - VendorInvoice | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000001":
      | GLAccount                 | GLSubAccount      | Credit   | Debit    | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10221 - service vendors 2 | 000002 - Zhejiang | 22244.25 | 0.00     | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO                | 2018000046        | 0.00     | 22244.25 | USD                | EGP             | 17.44                     | 2018000026       |
    And LandedCost with Code "2019100013" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | Draft |
    And the LandedCostDetails section of LandedCost with code "2019100013" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | PurchaseOrder | GoodsInvoice | DamageStock |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | 2018000046    | 2021000001   | true        |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47" with the following JournalEntry code "2019000001"
