#Dev Author: Ahmed Ali
#QA Reviewer: Khadrah Ali
Feature: View Vendor Invoice Company Data - Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Ahmed.Seif  | Quality_Specialist              |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | Quality_Specialist              | VendorInvoiceViewer           |
      | SuperUser                       | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                | Condition                      |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadCompany | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer           | VendorInvoice:ReadCompany |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                | Condition                  |
      | Gehan.Ahmed | VendorInvoice:ReadCompany | [purchaseUnitName='Flexo'] |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
    And the following PurchaseUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following CompanyData for VendorInvoice exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000103 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000039 | 0001 - Flexo     | 0002 - DigiPro |

  #EBS-7177
  Scenario: (01) View CompanyData section in VendorInvoice, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view CompanyData section of VendorInvoice with code "2019000103"
    Then the following values of CompanyData section for VendorInvoice with code "2019000103" are displayed to "Gehan.Ahmed":
      | BusinessUnit | Company |
      | Signmedia    | DigiPro |

  #EBS-7177
  Scenario Outline: (02) View CompanyData section in VendorInvoice, by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view CompanyData section of VendorInvoice with code "<Code>"
    Then the following values of CompanyData section for VendorInvoice with code "<Code>" are displayed to "Ahmed.Seif":
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |

    Examples:
      | Code       | BusinessUnit | Company |
      | 2019000103 | Signmedia    | DigiPro |
      | 2019000039 | Flexo        | DigiPro |

  #EBS-7177
  Scenario: (03) View CompanyData section in VendorInvoice, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view CompanyData section of VendorInvoice with code "2019000039"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  Scenario: (04) View CompanyData section in VendorInvoice, where VendorInvoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to view CompanyData section of VendorInvoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
