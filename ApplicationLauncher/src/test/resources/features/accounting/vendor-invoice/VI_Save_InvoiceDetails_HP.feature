Feature: Request Save Invoice Details section in Invoice

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission                         | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |
    And the following Invoice exists:
      | Code       | State |
      | 2019000105 | Draft |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And edit session is "30" minutes

  ####### Save Invoice Details section (Happy Path) ##############################################################
  Scenario: (01) Save VendorInvoiceDetails section -  within edit session by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceDetails section of Invoice with Code "2019000105" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves InvoiceDetails section of Invoice with Code "2019000105" at "07-Jan-2019 09:30 AM" with the following values:
      | PaymentTerms | InvoiceNumber    | InvoiceDate          | CurrencyCode |
      | 0002         | 1200120301023125 | 21-Sep-2019 11:00 AM | 0002         |
    Then InvoiceDetails of VendorInvoice with Code "2019000105" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | CurrencyISO | DownPayment |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 20% advance, 80% Copy of B/L | 1200120301023125 | 21-Sep-2019 11:00 AM | USD         |             |
    And the lock by "Gehan.Ahmed" on InvoiceDetails section of Invoice with Code "2019000105" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
