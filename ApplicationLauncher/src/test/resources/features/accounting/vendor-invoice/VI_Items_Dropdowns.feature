# Author Dev: Yara Ameen
Feature: VendorInvoice Items DropDown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | IVRViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | IVRViewer_Signmedia | ItemVendorRecord:ReadAll | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission               | Condition |
      | Afaf | ItemVendorRecord:ReadAll |           |

    #@INSERT
    And the following GeneralData for Vendor exist:
      | Code   | VendorName | VendorType | AccountWithVendor | PurchasingResponsible | State  | OldVendorReference | CreatedBy | CreationDate         |
      | 000202 | Zhejiang   | COMMERCIAL | Madina 12345-fgh  | 0002                  | Active | 5310               | Admin     | 20-Oct-2019 11:00 AM |
    #@INSERT
    And the following BusinessUnits for Vendor exist:
      | Code   | BusinessUnit                    |
      | 000202 | 0002 - Signmedia, 0003 - Offset |

    #INSERT
    And insert the following Items GeneralData exist:
      | Code   | ItemName                            | ItemType   | ProductManager | IsBatchManaged | BaseUnit     | OldItemNumber | ItemClass            | MarketName                          |
      | 000100 | Hot Laminated Frontlit Fabric roll  | Commercial | Mohamed Nabil  | Not Required   | Square Meter | 765632        | White Signmedia Inks | Hot Laminated Frontlit Fabric roll  |
      | 000102 | Hot Laminated Frontlit Backlit roll | Commercial | Mohamed Nabil  | Not Required   | Square Meter | 765633        | White Signmedia Inks | Hot Laminated Frontlit Backlit roll |
      | 000103 | Flex Primer Varnish E33             | Commercial | Mohamed Nabil  | Not Required   | Kilogram     | 5482154       | White Signmedia Inks | Flex Primer Varnish E33             |
      | 000104 | Flex cold foil adhesive E01         | Commercial | Mohamed Nabil  | Not Required   | Kilogram     | 863562        | White Signmedia Inks | Flex cold foil adhesive E01         |

        #@INSERT
    And the following Items BusinessUnits exist:
      | Code   | BusinessUnit                    |
      | 000100 | 0002 - Signmedia                |
      | 000102 | 0002 - Signmedia, 0003 - Offset |
      | 000103 | 0002 - Signmedia                |
      | 000104 | 0002 - Signmedia, 0003 - Offset |


     #INSERT
    And insert the following Items AlternateUnitOfMeasures exist:
      | ItemCode | AlternativeUnit | ConversionFactor |
      | 000100   | Roll 2.20x50    | 110              |
      | 000100   | Roll 3.20x50    | 210              |
      | 000102   | Roll 3.20x50    | 110              |
      | 000103   | Drum-10Kg       | 10               |

    #INSERT
    And insert the following ItemVendorRecords GeneralData exist:
      | Code   | Item                                         | Vendor            | UnitOfMeasure | BusinessUnit | ItemCodeAtVendor | OldItemNumber | UnitPrice   |
      | 000300 | 000100 - Hot Laminated Frontlit Fabric roll  | 000202 - Zhejiang | Square Meter  | Signmedia    | GDH670-888       | 765632        | 45.00 EGP   |
      | 000301 | 000100 - Hot Laminated Frontlit Fabric roll  | 000202 - Zhejiang | Roll 2.20x50  | Signmedia    | GDH670-889       | 765632        | 4950.00 EGP |
      | 000302 | 000100 - Hot Laminated Frontlit Fabric roll  | 000202 - Zhejiang | Roll 3.20x50  | Signmedia    | GDH670-810       | 765632        | 9450.00 EGP |
      | 000303 | 000102 - Hot Laminated Frontlit Backlit roll | 000202 - Zhejiang | Square Meter  | Signmedia    | GDH670-513       | 765633        | 30.00 EGP   |
      | 000304 | 000102 - Hot Laminated Frontlit Backlit roll | 000202 - Zhejiang | Roll 3.20x50  | Signmedia    | GDH670-514       | 765633        | 3300.00 EGP |
      | 000305 | 000103 - Flex Primer Varnish E33             | 000202 - Zhejiang | Kilogram      | Signmedia    | FDH670-213       | 5482154       | 100.00 EGP  |
      | 000306 | 000103 - Flex Primer Varnish E33             | 000202 - Zhejiang | Drum-10Kg     | Signmedia    | FDH670-214       | 5482154       | 1000.00 EGP |

       #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000103 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000103 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000103 | 2018000045    | 000202 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit)     | Qty(Base)         | UnitPrice(Base) |
      | 2019000103 | 000100 - Hot Laminated Frontlit Fabric roll | 100.0 Roll 2.20x50 | 11000.00 M2       | 2.00            |
      | 2019000103 | 000100 - Hot Laminated Frontlit Fabric roll | 100.0 M2           | 100.00 M2         | 50.00           |
      | 2019000103 | 000103 - Flex Primer Varnish E33            | 100.0 Kg           | 100.00 Kg         | 50.00           |
      | 2019000103 | 000103 - Flex Primer Varnish E33            | 100.0 Drum-10Kg    | 1000.00 Drum-10Kg | 300.00          |


  Scenario: (01) Read All VendorItems for VendorInvoice by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all VendorItems for VendorInvoice with Code "2019000103" and Vendor with Code "000202" and BusinessUnit with Code "0002"
    Then the following VendorItems are displayed to "Gehan.Ahmed":
      | Item                                         |
      | 000102 - Hot Laminated Frontlit Backlit roll |
      | 000100 - Hot Laminated Frontlit Fabric roll  |
    And total number of purchase orders returned to "Gehan.Ahmed" is equal to 2

  Scenario: (02) Read All VendorItems for VendorInvoice by unauthorized user (Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all VendorItems for VendorInvoice with Code "2019000103" and Vendor with Code "000202" and BusinessUnit with Code "0002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
