Feature: View Invoice General Data

  Background:

    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | hr1         | SuperUser                        |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
      | Afaf        | FrontDesk                        |

    And the following roles and sub-roles exist:
      | Role                             | Subrole                        |
      | PurchasingResponsible_Signmedia  | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorInvoiceViewer_Flexo      |
      | PurchasingResponsible_Corrugated | VendorInvoiceViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                    | Condition                       |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadGeneralData | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Flexo      | VendorInvoice:ReadGeneralData | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Corrugated | VendorInvoice:ReadGeneralData | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole               | *:*                           |                                 |

    And the following users have the following permissions without the following conditions:
      | User       | Permission                    | Condition                      |
      | Amr.Khalil | VendorInvoice:ReadGeneralData | [purchaseUnitName='Signmedia'] |

    #@INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000103 | IMPORT_GOODS_INVOICE | Draft | Gehan.Ahmed | 01-Jan-2019 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000103 | 0002 - Signmedia | 0002 - DigiPro |

  Scenario: (01) View GeneralData section of VendorInvoice by an authorized user who has one role (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view GeneralData section of Invoice with code "2019000103"
    Then the following values of GeneralData section for VendorInvoice with code "2019000103" are displayed to "Gehan.Ahmed":
      | Type                 | BusinessUnit     | CreatedBy   | CreationDate         | State | DocumentOwner |
      | IMPORT_GOODS_INVOICE | 0002 - Signmedia | Gehan.Ahmed | 01-Jan-2019 11:00 AM | Draft | Ashraf Salah  |

  Scenario: (02) View GeneralData section of VendorInvoice where Invoice doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully
    When "Gehan.Ahmed" requests to view GeneralData section of Invoice with code "2019000103"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  Scenario: (03) View GeneralData section of VendorInvoice by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view GeneralData section of Invoice with code "2019000103"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
