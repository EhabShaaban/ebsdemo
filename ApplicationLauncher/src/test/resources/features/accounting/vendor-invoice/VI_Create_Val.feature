#updated by (dev): Evram Hany
#updated by (quality): Khadrah Ali
#updates reviewed by : Yara Ameen

Feature: Create VendorInvoice - Validation

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission           | Condition |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:Create |           |

    And the following PurchaseOrders exist with the company data:
      | Code       | State              | Currency   | PaymentTerms                        | Company        | PurchaseUnit     | Vendor            |
      | 2018000010 | Shipped            | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000007 | WaitingApproval    | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |                | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000006 | OpenForUpdates     | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000015 | WaitingApproval    | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000016 | Approved           | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000012 | Cancelled          | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000021 | Draft              | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000009 | FinishedProduction | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |

      | 2018000001 | Cleared            | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000017 | Confirmed          | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000019 | DeliveryComplete   | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
      | 2018000049 | Cleared            | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia | 000002 - Zhejiang |
    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State           | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000044 | SERVICE_PO | Confirmed       | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000045 | SERVICE_PO | Confirmed       | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000046 | SERVICE_PO | Shipped         | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

      | 2020000047 | SERVICE_PO | WaitingApproval | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000049 | SERVICE_PO | Cancelled       | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000050 | SERVICE_PO | Approved        | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000051 | SERVICE_PO | OpenForUpdates  | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

      | 2020000060 | SERVICE_PO | Draft           | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000061 | IMPORT_PO  | Confirmed       | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000062 | LOCAL_PO   | Confirmed       | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000044 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2020000046 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |

      | 2020000047 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2020000049 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2020000050 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2020000051 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |

      | 2020000060 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000062 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000044 | 000002 - Zhejiang | 2018000010  |
      | 2020000046 | 000002 - Zhejiang | 2018000009  |

      | 2020000047 | 000002 - Zhejiang | 2018000001  |
      | 2020000049 | 000002 - Zhejiang | 2018000019  |
      | 2020000050 | 000002 - Zhejiang | 2018000049  |
      | 2020000051 | 000002 - Zhejiang | 2018000017  |

      | 2020000060 | 000002 - Zhejiang | 2020000052  |
      | 2020000061 | 000002 - Zhejiang |             |
      | 2020000062 | 000002 - Zhejiang |             |

    And the following Vendors exist:
      | Code   | Name     | PurchaseUnit                   |
      | 000002 | Zhejiang | 0002 - Signmedia               |
      | 000008 | Vendor 2 | 0002 - Signmedia, 0001 - Flexo |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | VendorInvoice  | CanBeDocumentOwner |           |

#EBS-7282, #EBS-8805
  Scenario Outline: (01) Create VendorInvoice with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type           | Vendor           | PurchaseOrder           | DocumentOwner           |
      | <SelectedType> | <SelectedVendor> | <SelectedPurchaseOrder> | <SelectedDocumentOwner> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | SelectedType         | SelectedVendor | SelectedPurchaseOrder | SelectedDocumentOwner |
      |                      | 000002         | 2018000010            | 34                    |
      | ""                   | 000002         | 2018000010            | 34                    |
      | IMPORT_GOODS_INVOICE |                | 2018000010            | 34                    |
      | IMPORT_GOODS_INVOICE | ""             | 2018000010            | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         |                       | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         | ""                    | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         | 2018000010            |                       |
      | IMPORT_GOODS_INVOICE | 000002         | 2018000010            | ""                    |
#EBS-7282
  Scenario Outline: (02) Create VendorInvoice with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type           | Vendor           | PurchaseOrder           | DocumentOwner           |
      | <SelectedType> | <SelectedVendor> | <SelectedPurchaseOrder> | <SelectedDocumentOwner> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | SelectedType         | SelectedVendor | SelectedPurchaseOrder | SelectedDocumentOwner |
   #Invalid data entry due to schema: Type
      | ay7aga               | 000002         | 2018000010            | 34                    |
      | 00001                | 000002         | 2018000010            | 34                    |
   #Invalid data entry due to schema: Vendor
      | IMPORT_GOODS_INVOICE | ay7aga         | 2018000010            | 34                    |
      | IMPORT_GOODS_INVOICE | 0000002        | 2018000010            | 34                    |
   #Invalid data entry due to schema: PO
      | IMPORT_GOODS_INVOICE | 000002         | ay7agaaaaa            | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         | 20180000010           | 34                    |
   #creating a VendorInvoice with a type that doen't exist
      | 9999                 | 000002         | 2018000010            | 34                    |
   #creating goods invoice with a vendor that doens't exist
      | IMPORT_GOODS_INVOICE | 999999         | 2018000010            | 34                    |
   #creating goods/service invoice with a PO that doesn't exist
      | IMPORT_GOODS_INVOICE | 000002         | 9999999999            | 34                    |
      | SHIPMENT_INVOICE     | 000008         | 9999999999            | 34                    |
   #creating goods/service invoice with draft PO
      | IMPORT_GOODS_INVOICE | 000002         | 2018000021            | 34                    |
      | SHIPMENT_INVOICE     | 000008         | 2020000060            | 34                    |
   #creating goods invoice with PO that has a vendor different from the selected vendor
      | IMPORT_GOODS_INVOICE | 000008         | 20180000010           | 34                    |
   #create service invoice with import/local PO
      | SHIPMENT_INVOICE     | 000002         | 2018000017            | 34                    |
   #create Goods invoice with Service PO
      | LOCAL_GOODS_INVOICE  | 000002         | 2020000044            | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         | 2020000044            | 34                    |
      #create Local Goods invoice with Import PO
      | LOCAL_GOODS_INVOICE  | 000002         | 2020000061            | 34                    |
        #create Import Goods invoice with Local PO
      | IMPORT_GOODS_INVOICE | 000002         | 2020000062            | 34                    |
   #create service invoice with selected po with state after confirmed
      | SHIPMENT_INVOICE     | 000002         | 2020000046            | 34                    |
        #create service invoice with selected po with state before confirmed
      | SHIPMENT_INVOICE     | 000002         | 2020000047            | 34                    |
      | SHIPMENT_INVOICE     | 000002         | 2020000049            | 34                    |
      | SHIPMENT_INVOICE     | 000002         | 2020000050            | 34                    |
      | SHIPMENT_INVOICE     | 000002         | 2020000051            | 34                    |
      | SHIPMENT_INVOICE     | 000002         | 2020000046            | 34                    |
      # create goods invoice with selected PO is either OpenForUpdates, WaitingApprovals, Apprroved, or cancelled
      | IMPORT_GOODS_INVOICE | 000002         | 2018000006            | 34                    |
      | LOCAL_GOODS_INVOICE  | 000002         | 2018000015            | 34                    |
      | LOCAL_GOODS_INVOICE  | 000002         | 2018000016            | 34                    |
      | IMPORT_GOODS_INVOICE | 000002         | 2018000012            | 34                    |
      #invalid documentOwner
      | IMPORT_GOODS_INVOICE | 000002         | 2018000012            | ay7aga                |
      | IMPORT_GOODS_INVOICE | 000002         | 2018000012            | 999999                |
#EBS-7282
  Scenario Outline: (03) Create VendorInvoice for Goods with incorrect data entry (Validation Failure)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates VendorInvoice with the following values:
      | Type           | Vendor           | PurchaseOrder           | DocumentOwner           |
      | <SelectedType> | <SelectedVendor> | <SelectedPurchaseOrder> | <SelectedDocumentOwner> |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Gehan.Ahmed"
    Examples:
      | SelectedType         | SelectedVendor | SelectedPurchaseOrder | SelectedDocumentOwner | Field             | ErrMsg     |
      # create goods/service invoice with selected PO that has no company value
      | IMPORT_GOODS_INVOICE | 000002         | 2020000061            | 34                    | purchaseOrderCode | Inv-msg-02 |
      | SHIPMENT_INVOICE     | 000002         | 2020000045            | 34                    | purchaseOrderCode | Inv-msg-02 |
      # creating service invoice with a vendor that does not exist
      | SHIPMENT_INVOICE     | 999999         | 2020000044            | 34                    | vendorCode        | Gen-msg-48 |
