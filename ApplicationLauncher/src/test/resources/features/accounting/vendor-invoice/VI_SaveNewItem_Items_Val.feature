Feature: Save Invoice New Item (Validation)

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                      |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia | IVRViewer_Signmedia           |

    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |
      | IVRViewer_Signmedia           | ItemVendorRecord:ReadAll  | [purchaseUnitName='Signmedia'] |

       #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |

     #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000045 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

        #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000103 | IMPORT_GOODS_INVOICE | Draft | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000103 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000103 | 2018000045    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                         | Qty(OrderUnit)     | Qty(Base)   | UnitPrice(Base) |
      | 2019000103 | 000002 - Hot Laminated Frontlit Backlit roll | 100.0 Roll 2.20x50 | 11000.00 M2 | 2.00            |

    And the following IVRs exit:
      | IVRCode | ItemCode | VendorCode | UOMCode | ItemCodeAtVendor | OldItemReference | Price    | CurrencyCode | PurchaseUnitCode | CreationDate         |
      | 000001  | 000001   | 000002     | 0029    | GDF530-440       | 12345678910      | 50.00    | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000002  | 000002   | 000002     | 0029    | GDB550-610       | 234567891011     | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000003  | 000003   | 000002     | 0036    | GDH670-777       | 34567891011      | 100.00   | 0001         | 0002             | 02-Aug-2018 10:30 AM |
      | 000004  | 000004   | 000002     | 0039    | GDH670-888       | 4567891011       | 50.00    | 0002         | 0002             | 02-Aug-2018 10:30 AM |
      | 000005  | 000001   | 000002     | 0032    | GDF630-440       | 567891011        | 11500.80 | 0003         | 0002             | 02-Aug-2018 10:30 AM |
      | 000006  | 000002   | 000002     | 0032    | GDF730-440       | 67891011         | 600.40   | 0004         | 0002             | 02-Aug-2018 10:30 AM |
      | 000007  | 000003   | 000002     | 0037    | GDF830-440       | 78910111         | 50.30    | 0005         | 0002             | 02-Aug-2018 10:30 AM |
      | 000009  | 000004   | 000002     | 0038    | GDF530-200       | 9101112          | 100.30   | 0006         | 0002             | 02-Aug-2018 10:30 AM |

    And edit session is "30" minutes
#
  Scenario: (01) Save new Item in VendorInvoice after edit session expired (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "07-Jan-2019 09:41 AM" with the following values:
      | ItemCode | Qty    | OrderUnit | UnitPrice(Gross) |
      | 000003   | 150.00 | 0036      | 3.50             |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (02) Save new Item in VendorInvoice after edit session expired & VI is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "07-Jan-2019 09:42 AM" with the following values:
      | ItemCode | Qty    | OrderUnit | UnitPrice(Gross) |
      | 000003   | 150.00 | 0036      | 3.50             |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"
#
  Scenario Outline: (03) Save new Item in VendorInvoice item that has no ivr record (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "07-Jan-2019 09:20 AM" with the following values:
      | ItemCode   | Qty    | OrderUnit   | UnitPrice(Gross) |
      | <ItemCode> | 150.00 | <OrderUnit> | 3.50             |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to Item field and orderUnit field "Gen-msg-48" and sent to "Gehan.Ahmed"
    Examples:
      | ItemCode | OrderUnit |
      | 000008   | 0019      |
      | 000003   | 9999      |
#
  Scenario Outline: (04) Save new Item in VendorInvoice item with malicious input  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "07-Jan-2019 09:20 AM" with the following values:
      | ItemCode   | Qty   | OrderUnit   | UnitPrice(Gross)   |
      | <ItemCode> | <Qty> | <OrderUnit> | <UnitPrice(Gross)> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | ItemCode | Qty     | OrderUnit | UnitPrice(Gross) |
      | ay7aga   | 150.00  | 0036      | 3.50             |
      |          | 150.00  | 0036      | 3.50             |
      | 000003   | -2.4    | 0036      | 3.50             |
      | 000003   | Ay 7aga | 0036      | 3.50             |
      | 000003   |         | 0036      | 3.50             |
      | 000003   | 150.00  |           | 3.50             |
      | 000003   | 150.00  | 0036      | -2.4             |
      | 000003   | 150.00  | 0036      | Ay 7aga          |
      | 000003   | 150.00  | 0036      |                  |

  #EBS-9320
  Scenario: (05) Save new Item in VendorInvoice That already added before with the same unit of measure  (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceItems section of Invoice with code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves the following new item to the InvoiceItems of Vendor Invoice with Code "2019000103" at "07-Jan-2019 09:20 AM" with the following values:
      | ItemCode | Qty | OrderUnit | UnitPrice(Gross) |
      | 000002   | 30  | 0029      | 10.00            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
