Feature: Request Save Invoice Details section in InvoiceUpdateInvoiceDetails

  Background:
    Given the following users and roles exist:
      | AMe         | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                |
      | PurchasingResponsible_Signmedia | VendorInvoiceOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission                         | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:UpdateInvoiceDetails | [purchaseUnitName='Signmedia'] |
    And the following Invoice exists:
      | Code       | State |
      | 2019000103 | Draft |
      | 2019000105 | Draft |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And edit session is "30" minutes
#
  Scenario: (01) Save VendorInvoiceDetails section - Save after edit session expired for InvoiceDetails (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceDetails section of Invoice with Code "2019000105" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves InvoiceDetails section of Invoice with Code "2019000103" at "07-Jan-2019 09:41 AM" with the following values:
      | PaymentTerms | InvoiceNumber    | InvoiceDate          | CurrencyCode |
      | 0002         | 1200120301023125 | 21-Sep-2018 11:00 AM | 0002         |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

  Scenario: (02) Save VendorInvoiceDetails section - Save after edit session expired & Invoice is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And InvoiceDetails section of Invoice with Code "2019000103" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the Vendor Invoice with code "2019000103" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves InvoiceDetails section of Invoice with Code "2019000103" at "07-Jan-2019 09:42 AM" with the following values:
      | PaymentTerms | InvoiceNumber    | InvoiceDate          | CurrencyCode |
      | 0002         | 1200120301023125 | 21-Sep-2018 11:00 AM | 0001         |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  #
  Scenario Outline: (03) Save VendorInvoiceDetails section - Save incorrect Data (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceDetails section of Invoice with Code "2019000105" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves InvoiceDetails section of Invoice with Code "2019000105" at "07-Jan-2019 09:30 AM" with the following values:
      | PaymentTerms   | InvoiceNumber   | InvoiceDate   | CurrencyCode   |
      | <PaymentTerms> | <InvoiceNumber> | <InvoiceDate> | <CurrencyCode> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | PaymentTerms | InvoiceNumber    | InvoiceDate          | CurrencyCode |
      | ay7aga       | 1200120301023125 | 21-Sep-2018 11:00 AM | 0002         |
      | 0002         | 1200120301023125 | ay7aga               | 0002         |
      | 0002         | 1200120301023125 | 21-Sep-2018 11:00 AM | ay7aga       |
#
#
#
  Scenario Outline: (04) Save VendorInvoiceDetails section - Save incorrect Data (invalid format)
    Given user is logged in as "Gehan.Ahmed"
    And InvoiceDetails section of Invoice with Code "2019000105" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves InvoiceDetails section of Invoice with Code "2019000105" at "07-Jan-2019 09:30 AM" with the following values:
      | PaymentTerms | InvoiceNumber   | InvoiceDate   | CurrencyCode |
      | 0002         | <InvoiceNumber> | <InvoiceDate> | 0002         |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | InvoiceNumber                                                                                          | InvoiceDate          |
      | 12<125                                                                                                 | 21-Sep-2018 11:00 AM |
      | 111>7aga                                                                                               | 21-Sep-2018 11:00 AM |
      | ay#7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay$7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay&7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay+7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay^7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay@7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay!7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay=7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay~7aga                                                                                                | 21-Sep-2018 11:00 AM |
      | ay\\7aga                                                                                               | 21-Sep-2018 11:00 AM |
      | ay \|7aga                                                                                              | 21-Sep-2018 11:00 AM |
      | ay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7agaay7aga | 21-Sep-2018 11:00 AM |
      | 1200120301023125                                                                                       | 21-02-2018 11:00 AM  |
