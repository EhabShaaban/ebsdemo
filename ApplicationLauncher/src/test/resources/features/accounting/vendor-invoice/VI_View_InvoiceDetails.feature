Feature: view invoice details

  Background:
    Given the following users and roles exist:
      | Name        | Role                             |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia  |
      | hr1         | SuperUser                        |
      | Amr.Khalil  | PurchasingResponsible_Flexo      |
      | Amr.Khalil  | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Subrole                        |
      | PurchasingResponsible_Signmedia  | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Flexo      | VendorInvoiceViewer_Flexo      |
      | PurchasingResponsible_Corrugated | VendorInvoiceViewer_Corrugated |
      | SuperUser                        | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                      | Condition                       |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadInvoiceDetail | [purchaseUnitName='Signmedia']  |
      | VendorInvoiceViewer_Flexo      | VendorInvoice:ReadInvoiceDetail | [purchaseUnitName='Flexo']      |
      | VendorInvoiceViewer_Corrugated | VendorInvoice:ReadInvoiceDetail | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole               | *:*                             |                                 |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000012 | VENDOR      | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000012 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                   | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2019000107 | CUSTOM_TRUSTEE_INVOICE | Draft | Amr.Khalil | 24-Apr-2018 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit      | Company        |
      | 2019000107 | 0006 - Corrugated | 0002 - DigiPro |

       #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury | Description                 |
      | 2019000012 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000002  | 2000      | USD         |                | 1516171819789 - Alex Bank |          | for save payment details hp |
    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency | DownPayment     |
      | 2019000107 | 2018000045    | 000011 - Vendor 5 | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      | 2019000012-2000 |

  Scenario: (01) View InvoiceDetails section by an authorizd user (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view InvoiceDetails section of Invoice with code "2019000107"
    Then the following values of InvoiceDetails section for Invoice with code "2019000107" are displayed to "Amr.Khalil":
      | POCode     | VendorCode | VendorName | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency | DownPayment     |
      | 2018000045 | 000011     | Vendor 5   | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2018 06:31 AM | USD      | 2019000012-2000 |

  Scenario: (02) View InvoiceDetails section where Invoice doesn't exist (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Vendor Invoice with code "2019000107" successfully
    When "Amr.Khalil" requests to view InvoiceDetails section of Invoice with code "2019000107"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  Scenario: (03) View InvoiceDetails section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to view InvoiceDetails section of Invoice with code "2019000107"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
