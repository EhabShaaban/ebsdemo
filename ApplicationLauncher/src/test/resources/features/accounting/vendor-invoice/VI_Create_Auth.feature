# Author Dev: Yara Ameen
# Author Quality: Marina Salah, Khadrah Ali

Feature: Create VendorInvoice (Auth)

  Background:

    Given the following users and roles exist:
      | Name                        | Role                                                        |
      | Gehan.Ahmed                 | PurchasingResponsible_Signmedia                             |
      | Amr.Khalil                  | PurchasingResponsible_Flexo                                 |
      | Gehan.Ahmed.NoVendor        | PurchasingResponsible_Signmedia_CannotReadVendor            |
      | Gehan.Ahmed.NoPurchaseOrder | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder     |
      | Gehan.Ahmed.NoDocumentOwner | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole |

      | Afaf                        | FrontDesk                                                   |
    And the following roles and sub-roles exist:
      | Role                                                        | SubRole                      |
      | PurchasingResponsible_Signmedia                             | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia                             | POViewer_Signmedia           |
      | PurchasingResponsible_Signmedia                             | VendorViewer_Signmedia       |
      | PurchasingResponsible_Signmedia                             | DocumentOwnerViewer          |
      | PurchasingResponsible_Flexo                                 | VendorInvoiceOwner_Flexo     |
      | PurchasingResponsible_Flexo                                 | POViewer_Flexo               |
      | PurchasingResponsible_Flexo                                 | VendorOwner_Flexo            |
      | PurchasingResponsible_Flexo                                 | DocumentOwnerViewer          |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder     | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder     | DocumentOwnerViewer          |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder     | VendorViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadVendor            | VendorInvoiceOwner_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadVendor            | DocumentOwnerViewer          |
      | PurchasingResponsible_Signmedia_CannotReadVendor            | DocumentOwnerViewer          |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole | VendorInvoiceOwner_Signmedia |


    And the following sub-roles and permissions exist:
      | Subrole                      | Permission            | Condition                      |
      | VendorInvoiceOwner_Signmedia | VendorInvoice:Create  |                                |
      | POViewer_Signmedia           | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
      | VendorViewer_Signmedia       | Vendor:ReadAll        | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceOwner_Flexo     | VendorInvoice:Create  |                                |
      | POViewer_Flexo               | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo']     |
      | VendorViewer_Flexo           | Vendor:ReadAll        | [purchaseUnitName='Flexo']     |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll |                                |

    And the following users doesn't have the following permissions:
      | User                        | Permission            |
      | Afaf                        | VendorInvoice:Create  |
      | Gehan.Ahmed.NoVendor        | Vendor:ReadAll        |
      | Gehan.Ahmed.NoPurchaseOrder | PurchaseOrder:ReadAll |
      | Gehan.Ahmed.NoDocumentOwner | DocumentOwner:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User       | Permission            | Condition                      |
      | Amr.Khalil | Vendor:ReadAll        | [purchaseUnitName='Signmedia'] |
      | Amr.Khalil | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |

    And the following PurchaseOrders exist with the company data:
      | Code       | State   | Currency   | PaymentTerms                        | Company        | PurchaseUnit     |
      | 2018000010 | Shipped | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L | 0002 - DigiPro | 0002 - Signmedia |
      | 2018000042 | Shipped | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |                | 0001 - Flexo     |

    And the following Vendors exist:
      | Code   | Name     | PurchaseUnit                   |
      | 000002 | Zhejiang | 0002 - Signmedia               |
      | 000008 | Vendor 2 | 0002 - Signmedia, 0001 - Flexo |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | VendorInvoice  | CanBeDocumentOwner |           |

#EBS-4346, #EBS-8805
  Scenario Outline: (01) Create VendorInvoice by an authorized user with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates VendorInvoice with the following values:
      | Type             | Vendor   | PurchaseOrder   | DocumentOwner |
      | SHIPMENT_INVOICE | <Vendor> | <PurchaseOrder> | Ashraf Salah  |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                        | Vendor | PurchaseOrder |
      | Afaf                        | 000002 | 2018000010    |
      | Gehan.Ahmed.NoVendor        | 000002 | 2018000010    |
      | Gehan.Ahmed.NoPurchaseOrder | 000002 | 2018000010    |
      | Gehan.Ahmed.NoDocumentOwner | 000002 | 2018000010    |
      | Amr.Khalil                  | 000002 | 2018000042    |
      | Amr.Khalil                  | 000001 | 2018000010    |

#EBS-4346
  Scenario: (02) Request Create VendorInvoice by an authorized user with ReadPurchaseOrders authorized read ONLY
    Given user is logged in as "Gehan.Ahmed.NoVendor"
    When "Gehan.Ahmed.NoVendor" requests to create VendorInvoice
    Then the following authorized reads are returned to "Gehan.Ahmed.NoVendor":
      | AuthorizedReads          |
      | ReadImportPurchaseOrders |
      | ReadLocalPurchaseOrders  |
      | ReadDocumentOwners       |

#EBS-4346
  Scenario: (03) Request Create VendorInvoice by an authorized user with ReadVendor authorized read ONLY
    Given user is logged in as "Gehan.Ahmed.NoPurchaseOrder"
    When "Gehan.Ahmed.NoPurchaseOrder" requests to create VendorInvoice
    Then the following authorized reads are returned to "Gehan.Ahmed.NoPurchaseOrder":
      | AuthorizedReads    |
      | ReadVendors        |
      | ReadDocumentOwners |

#EBS-4346, #EBS-8805
  Scenario: (04) Request Create VendorInvoice by an authorized user with ReadVendor and ReadPurchaseOrders authorized reads ONLY
    Given user is logged in as "Gehan.Ahmed.NoDocumentOwner"
    When "Gehan.Ahmed.NoDocumentOwner" requests to create VendorInvoice
    Then the following authorized reads are returned to "Gehan.Ahmed.NoDocumentOwner":
      | AuthorizedReads          |
      | ReadVendors              |
      | ReadImportPurchaseOrders |
      | ReadLocalPurchaseOrders  |

#EBS-4346
  Scenario Outline: (05) Request Create VendorInvoice by an authorized user that has all authorized reads
    Given user is logged in as "<User>"
    When "<User>" requests to create VendorInvoice
    Then the following authorized reads are returned to "<User>":
      | AuthorizedReads          |
      | ReadVendors              |
      | ReadImportPurchaseOrders |
      | ReadLocalPurchaseOrders  |
      | ReadDocumentOwners       |
    Examples:
      | User        |
      | Amr.Khalil  |
      | Gehan.Ahmed |
 #EBS-4346
  Scenario: (06) Request Create VendorInvoice by unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create VendorInvoice
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
