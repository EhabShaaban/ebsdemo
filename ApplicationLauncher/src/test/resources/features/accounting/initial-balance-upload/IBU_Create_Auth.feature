Feature: Create Initial Balance Upload - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                                               | Role                                                                               |
      | Shady.Abdelatif                                                    | Accountant_Signmedia                                                               |
      | Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner | Accountant_Signmedia_CannotReadBusinessUnitAndCompanyAndFiscalYearAndDocumentOwner |
    And the following roles and sub-roles exist:
      | Role                                                                               | Subrole                 |
      | Accountant_Signmedia                                                               | IBUOwner_Signmedia      |
      | Accountant_Signmedia                                                               | PurUnitReader_Signmedia |
      | Accountant_Signmedia                                                               | CompanyViewer           |
      | Accountant_Signmedia                                                               | DocumentOwnerViewer     |
      | Accountant_Signmedia                                                               | FiscalYearViewer        |
      | Accountant_Signmedia_CannotReadBusinessUnitAndCompanyAndFiscalYearAndDocumentOwner | IBUOwner_Signmedia      |
      | Accountant_Signmedia_CannotReadBusinessUnitAndCompanyAndFiscalYearAndDocumentOwner | IBUViewer_Signmedia     |

    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | IBUOwner_Signmedia      | InitialBalanceUpload:Create  |                                |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | CompanyViewer           | Company:ReadAll              |                                |
      | DocumentOwnerViewer     | DocumentOwner:ReadAll        |                                |
      | FiscalYearViewer        | FiscalYear:ReadAll           |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                   | Condition                  |
      | Shady.Abdelatif | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                                                               | Permission                   |
      | Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner | PurchasingUnit:ReadAll_ForPO |
      | Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner | Company:ReadAll              |
      | Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner | FiscalYear:ReadAll           |
      | Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner | DocumentOwner:ReadAll        |

  #EBS-2580
  Scenario: (01) Create InitialBalanceUpload, by an authorized user with authorized reads
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to create InitialBalanceUpload
    Then the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads    |
      | ReadBusinessUnits  |
      | ReadCompanies      |
      | ReadFiscalYears    |
      | ReadDocumentOwners |

  #EBS-2580
  Scenario: (02) Create InitialBalanceUpload, by an authorized user with no authorized reads
    Given user is logged in as "Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner"
    When "Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner" requests to create InitialBalanceUpload
    Then there are no authorized reads returned to "Shady.Abdelatif.NoBusinessUnitNoCompanyNoFiscalYearNoDocumentOwner"

  #EBS-2580
  Scenario: (03) Create InitialBalanceUpload by an unauthorized user: user is not authorized to create (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates InitialBalanceUpload with the following values:
      | BusinessUnit     | Company          | FiscalPeriod | DocumentOwnerId | Attachment          |
      | 0002 - Signmedia | 0001 - AL Madina | 0002 - 2020  | 40              | initial-balance.csv |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-2580
  Scenario: (04) Create InitialBalanceUpload by user chooses BusinessUnit s/he is not authorized to read (Unauthorized/Abuse Case because user violates read permission of business unit)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates InitialBalanceUpload with the following values:
      | BusinessUnit | Company          | FiscalPeriod | DocumentOwnerId | Attachment          |
      | 0001 - Flexo | 0001 - AL Madina | 0002 - 2020  | 40              | initial-balance.csv |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page