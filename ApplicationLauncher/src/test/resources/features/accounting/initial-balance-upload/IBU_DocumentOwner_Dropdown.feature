Feature: InitialBalanceUpload ViewAll DocumentOwners Dropdown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | Accountant_Flexo     |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole             |
      | Accountant_Signmedia | DocumentOwnerViewer |
      | Accountant_Signmedia | IBUDocumentOwner    |
      | Accountant_Flexo     | DocumentOwnerViewer |
      | Accountant_Flexo     | IBUDocumentOwner    |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                              | Condition |
      | DocumentOwnerViewer | DocumentOwner:ReadAll                   |           |
      | IBUDocumentOwner    | InitialBalanceUpload:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission                              |
      | Afaf | DocumentOwner:ReadAll                   |
      | Afaf | InitialBalanceUpload:CanBeDocumentOwner |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject       | Permission         | Condition |
      | 16 | Ahmed Hamdy     | InitialBalanceUpload | CanBeDocumentOwner |           |
      | 40 | Shady.Abdelatif | InitialBalanceUpload | CanBeDocumentOwner |           |
    And the total number of existing "InitialBalanceUpload" DocumentOwners are 2

  Scenario: (01) Read list of DocumentOwners dropdown, by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all DocumentOwners for "InitialBalanceUpload"
    Then the following DocumentOwners values will be presented to "Shady.Abdelatif":
      | Name            |
      | Ahmed Hamdy     |
      | Shady.Abdelatif |
    And total number of DocumentOwners returned to "Shady.Abdelatif" in a dropdown is equal to 2

  Scenario: (02) Read list of DocumentOwners dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "InitialBalanceUpload"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
