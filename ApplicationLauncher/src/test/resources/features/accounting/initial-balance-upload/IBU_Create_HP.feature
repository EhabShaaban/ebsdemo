Feature: Create Initial Balance Upload - Happy Path

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole            |
      | Accountant_Signmedia | IBUOwner_Signmedia |
      | Accountant_Signmedia | IBUDocumentOwner   |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                              | Condition |
      | IBUOwner_Signmedia | InitialBalanceUpload:Create             |           |
      | IBUDocumentOwner   | InitialBalanceUpload:CanBeDocumentOwner |           |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject       | Permission         | Condition |
      | 40 | Shady.Abdelatif | InitialBalanceUpload | CanBeDocumentOwner |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName | AccountType | Level | AccountParent             | Credit/Debit | State  | Subledger     | OldAccountCode | MappedAccount          |
      | 30202       | Vendors     |             | 3     | 102 - Current Assets      | DEBIT        | Active | Local_Vendors | 10403          |                        |
      | 10207       | PO          |             | 3     | 102 - Current Liabilities | CREDIT       | Active | PO            | 21009          | PurchaseOrderGLAccount |

    #@INSERT
    And the following GeneralData for Vendor exist:
      | Code   | VendorName | VendorType | AccountWithVendor | PurchasingResponsible | State  | OldVendorReference | CreatedBy | CreationDate         |
      | 000002 | Zhejiang   | COMMERCIAL | Madina 12345-fgh  | 0002                  | Active | 5310               | Admin     | 20-Oct-2019 11:00 AM |
    #@INSERT
    And the following BusinessUnits for Vendor exist:
      | Code   | BusinessUnit                    |
      | 000002 | 0002 - Signmedia, 0003 - Offset |

  #EBS-1064
  Scenario: (01) Create InitialBalanceUpload - with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "31-Jan-2021 11:00 AM"
    And Last created InitialBalanceUpload has code "2021000001"
    When "Shady.Abdelatif" creates InitialBalanceUpload with the following values:
      | BusinessUnit     | Company          | FiscalPeriod | DocumentOwnerId | Attachment          |
      | 0002 - Signmedia | 0001 - AL Madina | 0002 - 2020  | 40              | initial-balance.csv |
    Then a new InitialBalanceUpload is created with the following values:
      | Code       | State | CreatedBy       | LastUpdatedBy   | CreationDate         | LastUpdateDate       | DocumentOwner   | FiscalPeriod |
      | 2021000002 | Draft | Shady.Abdelatif | Shady.Abdelatif | 31-Jan-2021 11:00 AM | 31-Jan-2021 11:00 AM | Shady.Abdelatif | 2020         |
    And the Company Section of InitialBalanceUpload with code "2021000002" is created as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the Items Section of InitialBalanceUpload with code "2021000002" is created as follows:
      | GLAccount       | Subledger     | SubAccount        | Debit    | Credit  | Currency | CurrencyPrice |
      | 30202 - Vendors | Local_Vendors | 000002 - Zhejiang | 6999.984 |         | EGP      | 1             |
      | 10207 - PO      | PO            |                   |          | 4939.46 | EGP      | 1             |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"

