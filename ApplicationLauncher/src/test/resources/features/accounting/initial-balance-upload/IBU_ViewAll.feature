Feature: View All Initial Balance Uploads

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Ahmed.Seif      | Quality_Specialist   |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole             |
      | Quality_Specialist   | IBUViewer           |
      | Accountant_Signmedia | IBUViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole             | Permission                   | Condition                      |
      | IBUViewer           | InitialBalanceUpload:ReadAll |                                |
      | IBUViewer_Signmedia | InitialBalanceUpload:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission                   |
      | Afaf | InitialBalanceUpload:ReadAll |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    #@INSERT
    And the following GeneralData for InitialBalanceUploads exist:
      | Code       | Type | CreatedBy | CreationDate         | DocumentOwner   | State | FiscalYear |
      | 2021000004 | IBU  | Admin     | 01-Jan-2021 09:02 AM | Shady.Abdelatif | Draft | 2021       |
      | 2021000003 | IBU  | Admin     | 06-Jan-2021 09:00 AM | Shady.Abdelatif | Draft | 2021       |
      | 2021000002 | IBU  | Admin     | 09-Jan-2021 09:00 AM | Shady.Abdelatif | Draft | 2021       |
      | 2021000001 | IBU  | Admin     | 10-Jan-2021 11:35 AM | Shady.Abdelatif | Draft | 2021       |
    #@INSERT
    And the following CompanyData for InitialBalanceUploads exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000001 | 0001 - Flexo     | 0001 - AL Madina |

  #EBS-1176
  Scenario: (01) View all InitialBalanceUploads - by an authorized user WITHOUT condition - Paging (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to read page 1 of InitialBalanceUploads with no filter applied with 4 records per page
    Then the following values will be presented to "Ahmed.Seif":
      | Code       | CreationDate         | DocumentOwner   | BusinessUnit | Company          | FiscalYear | State |
      | 2021000004 | 01-Jan-2021 09:02 AM | Shady.Abdelatif | Signmedia    | 0001 - AL Madina | 2021       | Draft |
      | 2021000003 | 06-Jan-2021 09:00 AM | Shady.Abdelatif | Signmedia    | 0002 - DigiPro   | 2021       | Draft |
      | 2021000002 | 09-Jan-2021 09:00 AM | Shady.Abdelatif | Signmedia    | 0001 - AL Madina | 2021       | Draft |
      | 2021000001 | 10-Jan-2021 11:35 AM | Shady.Abdelatif | Flexo        | 0001 - AL Madina | 2021       | Draft |
    And the total number of records in search results by "Ahmed.Seif" are 4

  #EBS-1176
  Scenario: (02) View all InitialBalanceUploads - by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of InitialBalanceUploads with no filter applied with 20 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | DocumentOwner   | BusinessUnit | Company          | FiscalYear | State |
      | 2021000004 | 01-Jan-2021 09:02 AM | Shady.Abdelatif | Signmedia    | 0001 - AL Madina | 2021       | Draft |
      | 2021000003 | 06-Jan-2021 09:00 AM | Shady.Abdelatif | Signmedia    | 0002 - DigiPro   | 2021       | Draft |
      | 2021000002 | 09-Jan-2021 09:00 AM | Shady.Abdelatif | Signmedia    | 0001 - AL Madina | 2021       | Draft |
    And the total number of records in search results by "Shady.Abdelatif" are 3

  #EBS-1176
  Scenario: (03) View all InitialBalanceUploads - by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of InitialBalanceUploads with no filter applied with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page