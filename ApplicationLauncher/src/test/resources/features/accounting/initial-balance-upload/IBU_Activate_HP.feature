Feature: Activate Initial Balance Upload - Happy Path

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole            |
      | Accountant_Signmedia | IBUOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission                    | Condition                      |
      | IBUOwner_Signmedia | InitialBalanceUpload:Activate | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    #@INSERT
    And the following GLAccounts exist:
      | AccountCode | AccountName | AccountType | Level | AccountParent             | Credit/Debit | State  | Subledger     | OldAccountCode | MappedAccount          |
      | 30202       | Vendors     |             | 3     | 102 - Current Assets      | DEBIT        | Active | Local_Vendors | 10403          |                        |
      | 10207       | PO          |             | 3     | 102 - Current Liabilities | CREDIT       | Active | PO            | 21009          | PurchaseOrderGLAccount |

    #@INSERT
    And the following GeneralData for Vendor exist:
      | Code   | VendorName | VendorType | AccountWithVendor | PurchasingResponsible | State  | OldVendorReference | CreatedBy | CreationDate         |
      | 000002 | Zhejiang   | COMMERCIAL | Madina 12345-fgh  | 0002                  | Active | 5310               | Admin     | 20-Oct-2019 11:00 AM |
    #@INSERT
    And the following BusinessUnits for Vendor exist:
      | Code   | BusinessUnit                    |
      | 000002 | 0002 - Signmedia, 0003 - Offset |

     #@INSERT
    And the following GeneralData for InitialBalanceUploads exist:
      | Code       | Type | CreatedBy | CreationDate         | DocumentOwner   | State | FiscalYear |
      | 2021000001 | IBU  | Admin     | 10-Jan-2021 11:35 AM | Shady.Abdelatif | Draft | 2021       |
    #@INSERT
    And the following CompanyData for InitialBalanceUploads exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following ItemsData for InitialBalanceUploads exist:
      | Code       | GLAccount       | Subledger     | SubAccount        | Debit    | Credit  | Currency | CurrencyPrice |
      | 2021000001 | 30202 - Vendors | Local_Vendors | 000002 - Zhejiang | 6999.984 |         | EGP      | 1             |
      | 2021000001 | 10207 - PO      | PO            |                   |          | 4939.46 | EGP      | 1             |


  Scenario: (01) Activate InitialBalanceUpload - by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And the last created JournalEntry was with code "2021000001"
    When "Shady.Abdelatif" activates InitialBalanceUpload with code "2021000001" at "31-Jan-2021 11:00 AM"
    Then InitialBalanceUpload with code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 31-Jan-2021 11:00 AM | Active |
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument                 | Company          |
      | 2021000002 | 31-Jan-2021 11:00 AM | Shady.Abdelatif | 31-Jan-2021 11:00 AM | 0002 - Signmedia | 2021       | 2021000001 - InitialBalanceUpload | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000002":
      | JournalDate          | GLAccount       | SubAccount        | Debit                                       | Credit                                        | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 31-Jan-2021 11:00 AM | 30202 - Vendors | 000002 - Zhejiang | 6999.98400000000037834979593753814697265625 | 0                                             | EGP                | EGP             | 1.0                       |                  |
      | 31-Jan-2021 11:00 AM | 10207 - PO      |                   | 0                                           | 4939.4600000000000363797880709171295166015625 | EGP                | EGP             | 1.0                       |                  |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47"