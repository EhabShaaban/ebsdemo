# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud

Feature: View All AccountingNote Types

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | AccountingNoteViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission             | Condition                      |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | AccountingNote:ReadAll |

  #EBS-7979
  Scenario: (01) Read list of AccountingNote NoteTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all AccountingNote NoteTypes
    Then the following AccountingNote NoteTypes values will be presented to "Shady.Abdelatif":
      | NoteType |
      | CREDIT   |
      | DEBIT    |
    And total number of AccountingNote NoteTypes returned to "Shady.Abdelatif" is equal to 2

  #EBS-7979
  Scenario: (02) Read list of AccountingNote Types dropdown For DebitNote by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all AccountingNote Types for "DEBIT" NoteType in a dropdown
    Then the following AccountingNote Types values will be presented to "Shady.Abdelatif" in a dropdown:
      | Type                           |
      | DEBIT_NOTE_BASED_ON_COMMISSION |
    And total number of AccountingNote Types returned to "Shady.Abdelatif" is equal to 1

  #EBS-8044
  Scenario: (03) Read list of AccountingNote Types dropdown For CreditNote by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all AccountingNote Types for "CREDIT" NoteType in a dropdown
    Then the following AccountingNote Types values will be presented to "Shady.Abdelatif" in a dropdown:
      | Type                              |
      | CREDIT_NOTE_BASED_ON_SALES_RETURN |
    And total number of AccountingNote Types returned to "Shady.Abdelatif" is equal to 1


  #EBS-7979
  Scenario: (04) Read list of AccountingNote NoteTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all AccountingNote NoteTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7979
  Scenario: (05) Read list of AccountingNote Types dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all AccountingNote Types for "DEBIT" NoteType in a dropdown
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-8044
  Scenario: (06) Read list of AccountingNote Types dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all AccountingNote Types for "CREDIT" NoteType in a dropdown
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page