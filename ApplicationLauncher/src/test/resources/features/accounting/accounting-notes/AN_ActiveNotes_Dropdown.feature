# Author Dev: Hossam Hassan
# Updated: Mohamed Aboelnour
# Author Quality: Khadra Ali

Feature: View Active Notes

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | AccountingNoteViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission             | Condition                      |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | AccountingNote:ReadAll |

       #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000015 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000014 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Posted | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 3000      |
      | 2019000013 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Posted | Admin     | 02-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000012 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000011 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000010 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 3000      |
      | 2019000009 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Posted | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 6006.12   |
      | 2019000007 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 12-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000006 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000005 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Posted | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 0         |
      | 2019000004 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000003 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000002 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 11-Oct-2019 11:00 AM | Ashraf Salah  | 2000      |
      | 2019000001 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 20-Oct-2019 11:00 AM | Ashraf Salah  | 3000      |
      #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000015 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000014 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000013 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000012 | 0003 - Offset    | 0001 - AL Madina |
      | 2019000011 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000010 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000009 | 0003 - Offset    | 0001 - AL Madina |
      | 2019000008 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000007 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000006 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000005 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000004 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2019000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000001 | 0003 - Offset    | 0001 - AL Madina |
     #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner          | Amount  | Currency   |
      | 2019000015 | 000001 - Al Ahram        | 1000    | 0006 - HKD |
      | 2019000014 | 000051 - Vendor 6        | 3000    | 0003 - EUR |
      | 2019000013 | 000051 - Vendor 6        | 1000    | 0003 - EUR |
      | 2019000012 | 000003 - ABC             | 1000    | 0005 - JPY |
      | 2019000011 | 000001 - Siegwerk        | 1000    | 0004 - CNY |
      | 2019000010 | 000001 - Al Ahram        | 3000    | 0003 - EUR |
      | 2019000009 | 000006 - المطبعة الأمنية | 1000    | 0002 - USD |
      | 2019000008 | 000001 - Al Ahram        | 6006.12 | 0006 - HKD |
      | 2019000007 | 000001 - Al Ahram        | 1000    | 0003 - EUR |
      | 2019000006 | 000002 - Zhejiang        | 1000    | 0003 - EUR |
      | 2019000005 | 000001 - Siegwerk        | 3000    | 0005 - JPY |
      | 2019000004 | 000051 - Vendor 6        | 1000    | 0003 - EUR |
      | 2019000003 | 000051 - Vendor 6        | 1000    | 0003 - EUR |
      | 2019000002 | 000001 - Al Ahram        | 2000    | 0001 - EGP |
      | 2019000001 | 000006 - المطبعة الأمنية | 3000    | 0002 - USD |
    And the total number of records of AccountingNotes is 15

    #EBS-7622
  Scenario: (01) Read All DebitNotes Dropdown in Collection creation - in state Posted by authorized user with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all DebitNotes with state Posted remaining is greater than Zero according to user's BusinessUnit
    Then the following DebitNotes for Collections values will be presented to "Shady.Abdelatif":
      | Code       | NoteType | State  | BusinessUnit     | Remaining |
      | 2019000013 | DEBIT    | Posted | 0002 - Signmedia | 1000      |
    And total number of DebitNotes returned to "Shady.Abdelatif" is equal to 1

  #EBS-8080
  Scenario: (02) Read All CreditNotes Dropdown in PaymentRequest Details - in state Posted by authorized user with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all CreditNotes with state Posted remaining is greater than Zero according to user's BusinessUnit
    Then the following CreditNotes for PaymentRequest values will be presented to "Shady.Abdelatif":
      | Code       | NoteType | State  | BusinessUnit     | Remaining |
      | 2019000008 | CREDIT   | Posted | 0002 - Signmedia | 6006.12   |
    And total number of CreditNotes returned to "Shady.Abdelatif" is equal to 1

    #EBS-7622
  Scenario: (03) Read All DebitNotes Dropdown in Collection creation - by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DebitNotes with state Posted remaining is greater than Zero according to user's BusinessUnit
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-8080
  Scenario: (04) Read All CreditNotes Dropdown in PaymentRequest Details - by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all CreditNotes with state Posted remaining is greater than Zero according to user's BusinessUnit
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page