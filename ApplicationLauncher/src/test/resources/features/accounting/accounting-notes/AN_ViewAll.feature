# Author Dev: Order Team (17-Jan-2021)
# Author Quality: Shirin Mahmoud

Feature: View All Credit/Debit Notes

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                       |
      | Accountant_Signmedia | AccountingNoteViewer_Signmedia |
      | Accountant_Signmedia | PurUnitReader_Signmedia        |
      | SuperUser            | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission             | Condition                      |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole               | *:*                    |                                |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | AccountingNote:ReadAll |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State               | CreationDate         |
      | 2021000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | DeliveredToCustomer | 01-Dec-2019 02:20 PM |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | DeliveredToCustomer | 01-Dec-2019 02:20 PM |
      | 2021000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | DeliveredToCustomer | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2021000001 | 0001 - AL Madina | 0003 - Offset    |
      | 2021000002 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2021000003 | 0001 - AL Madina | 0001 - Flexo     |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                         | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 300.000        | 100.000              | 0019 - M2 |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 300.000        | 100.000              | 0019 - M2 |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 300.000        | 100.000              | 0019 - M2 |
    #@INSERT
    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy                 | DocumentOwner       | State   |
      | 2021000001 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
      | 2021000002 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped |
      | 2021000003 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
      | 2021000004 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped |
      | 2021000005 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
      | 2021000006 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | SeragEldin Meghawry | Shipped |
      | 2021000007 | 15-Jun-2020 01:51 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin from BDKCompanyCode | Manar Mohammed      | Shipped |
    #@INSERT
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0003 - Offset    | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000004 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000005 | 0003 - Offset    | 0001 - AL Madina |
      | 2021000006 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000007 | 0002 - Signmedia | 0002 - DigiPro   |
    #@INSERT
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer                 | CurrencyISO |
      | 2021000001 | 2021000001 | 000006 - المطبعة الأمنية | USD         |
      | 2021000002 | 2021000002 | 000001 - Al Ahram        | EGP         |
      | 2021000003 | 2021000003 | 000001 - Al Ahram        | EUR         |
      | 2021000004 | 2021000003 | 000001 - Al Ahram        | EUR         |
      | 2021000005 | 2021000001 | 000006 - المطبعة الأمنية | USD         |
      | 2021000006 | 2021000003 | 000001 - Al Ahram        | EUR         |
      | 2021000007 | 2021000002 | 000001 - Al Ahram        | EGP         |
    #@INSERT
    And Insert the following Item for SalesReturnOrders exist:
      | Code       | Item                                         | OrderUnit | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2021000001 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 70.000         | 100.000    |
      | 2021000002 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 60.000         | 100.000    |
      | 2021000003 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 50.000         | 100.000    |
      | 2021000004 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 40.000         | 100.000    |
      | 2021000005 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 30.000         | 100.000    |
      | 2021000006 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 20.000         | 100.000    |
      | 2021000007 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 10.000         | 100.000    |
    #@INSERT
    And Insert the following Taxes for SalesReturnOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2021000001 | 0001 - Vat                                   | 1             |
      | 2021000002 | 0001 - Vat                                   | 1             |
      | 2021000003 | 0001 - Vat                                   | 1             |
      | 2021000004 | 0001 - Vat                                   | 1             |
      | 2021000005 | 0001 - Vat                                   | 1             |
      | 2021000005 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2021000005 | 0003 - Real estate taxes                     | 10            |
      | 2021000006 | 0003 - Real estate taxes                     | 10            |
      | 2021000007 | 0003 - Real estate taxes                     | 10            |
    And the following values for ItemsSummary for salesReturnOrders exist:
      | Code       | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 2021000001 | 7000.000               | 70.000     | 7070.000              |
      | 2021000002 | 6000.000               | 60.000     | 6060.000              |
      | 2021000003 | 5000.000               | 50.000     | 5050.000              |
      | 2021000004 | 4000.000               | 40.000     | 4040.000              |
      | 2021000005 | 3000.000               | 345.000    | 3345.000              |
      | 2021000006 | 2000.000               | 200.000    | 2200.000              |
      | 2021000007 | 1000.000               | 100.000    | 1100.000              |
    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner   | Remaining |
      | 2021000001 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 20-Oct-2019 11:00 AM | Ashraf Salah    |           |
      | 2021000002 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 11-Oct-2019 11:00 AM | Shady.Abdelatif |           |
      | 2021000003 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah    |           |
      | 2021000004 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif |           |
      | 2021000005 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Active | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah    | 3000.000  |
      | 2021000006 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif |           |
      | 2021000007 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 12-Aug-2019 11:00 AM | Ashraf Salah    |           |
      | 2021000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Active | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif | 4040.000  |
      | 2021000009 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah    |           |
      | 2021000010 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif |           |
      | 2021000011 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah    |           |
      | 2021000012 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Draft  | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif |           |
      | 2021000013 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Active | Admin     | 02-Aug-2019 11:00 AM | Ashraf Salah    | 1000.000  |
      | 2021000014 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION    | Active | Admin     | 10-Aug-2019 11:00 AM | Shady.Abdelatif | 3000.000  |
      | 2021000015 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft  | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah    |           |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0003 - Offset    | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000004 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2021000005 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000006 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000007 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000008 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000009 | 0003 - Offset    | 0001 - AL Madina |
      | 2021000010 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000011 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000012 | 0003 - Offset    | 0002 - DigiPro   |
      | 2021000013 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000014 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2021000015 | 0002 - Signmedia | 0002 - DigiPro   |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner          | ReferenceDocument               | Amount   | Currency   |
      | 2021000001 | 000006 - المطبعة الأمنية | Sales Return Order - 2021000001 | 7070.000 | 0002 - USD |
      | 2021000002 | 000001 - Al Ahram        | Sales Return Order - 2021000002 | 6060.000 | 0001 - EGP |
      | 2021000003 | 000051 - Vendor 6        |                                 | 1000.000 | 0003 - EUR |
      | 2021000004 | 000051 - Vendor 6        |                                 | 1000.000 | 0003 - EUR |
      | 2021000005 | 000001 - Siegwerk        |                                 | 3000.000 | 0005 - JPY |
      | 2021000006 | 000002 - Zhejiang        |                                 | 1000.000 | 0003 - EUR |
      | 2021000007 | 000001 - Al Ahram        | Sales Return Order - 2021000003 | 5050.000 | 0003 - EUR |
      | 2021000008 | 000001 - Al Ahram        | Sales Return Order - 2021000004 | 4040.000 | 0006 - HKD |
      | 2021000009 | 000006 - المطبعة الأمنية | Sales Return Order - 2021000005 | 3345.000 | 0002 - USD |
      | 2021000010 | 000001 - Al Ahram        | Sales Return Order - 2021000006 | 2200.000 | 0003 - EUR |
      | 2021000011 | 000001 - Siegwerk        |                                 | 1000.000 | 0004 - CNY |
      | 2021000012 | 000003 - ABC             |                                 | 1000.000 | 0005 - JPY |
      | 2021000013 | 000051 - Vendor 6        |                                 | 1000.000 | 0003 - EUR |
      | 2021000014 | 000051 - Vendor 6        |                                 | 3000.000 | 0003 - EUR |
      | 2021000015 | 000001 - Al Ahram        | Sales Return Order - 2021000007 | 1100.000 | 0006 - HKD |
  #EBS-7412
  Scenario: (01) View all Credit/Debit Notes, by an authorized user WITHOUT condition - Super user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with no filter applied with 20 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner                     | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State  |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Shady.Abdelatif |                                 | 3000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000012 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000003 - ABC               | Shady.Abdelatif |                                 | 1000.000 - JPY | 0003 - Offset    | Draft  |
      | 2021000011 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk          | Ashraf Salah    |                                 | 1000.000 - CNY | 0001 - Flexo     | Draft  |
      | 2021000010 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000006 | 2200.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000009 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000005 | 3345.000 - USD | 0003 - Offset    | Draft  |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo     | Active |
      | 2021000007 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000003 | 5050.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000006 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000002 - Zhejiang          | Shady.Abdelatif |                                 | 1000.000 - EUR | 0002 - Signmedia | Draft  |
      | 2021000005 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk          | Ashraf Salah    |                                 | 3000.000 - JPY | 0001 - Flexo     | Active |
      | 2021000004 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Shady.Abdelatif |                                 | 1000.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000003 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000002 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000002 | 6060.000 - EGP | 0002 - Signmedia | Draft  |
      | 2021000001 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000001 | 7070.000 - USD | 0003 - Offset    | Draft  |
    And the total number of records in search results by "hr1" are 15

  #EBS-7412
  Scenario: (02) View all Credit/Debit Notes, by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of Credit/Debit Notes with no filter applied with 10 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | Type                              | BusinessPartner              | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Ashraf Salah    | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft |
      | 2021000006 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000002 - Zhejiang   | Shady.Abdelatif |                                 | 1000.000 - EUR | 0002 - Signmedia | Draft |
      | 2021000002 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000002 | 6060.000 - EGP | 0002 - Signmedia | Draft |
    And the total number of records in search results by "Shady.Abdelatif" are 3

  #EBS-7412
  Scenario: (03) View all Credit/Debit Notes, Filter by Code by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on Code which contains "00001" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner                     | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State  |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Shady.Abdelatif |                                 | 3000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000012 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000003 - ABC               | Shady.Abdelatif |                                 | 1000.000 - JPY | 0003 - Offset    | Draft  |
      | 2021000011 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk          | Ashraf Salah    |                                 | 1000.000 - CNY | 0001 - Flexo     | Draft  |
      | 2021000010 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000006 | 2200.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000001 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000001 | 7070.000 - USD | 0003 - Offset    | Draft  |
    And the total number of records in search results by "hr1" are 7

  #EBS-7412
  Scenario: (04) View all Credit/Debit Notes, Filter by Type by authorized user using "Equals" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on Type which equals "CREDIT_NOTE_BASED_ON_SALES_RETURN" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner                     | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State  |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft  |
      | 2021000010 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000006 | 2200.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000009 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000005 | 3345.000 - USD | 0003 - Offset    | Draft  |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo     | Active |
      | 2021000007 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000003 | 5050.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000002 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000002 | 6060.000 - EGP | 0002 - Signmedia | Draft  |
      | 2021000001 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000001 | 7070.000 - USD | 0003 - Offset    | Draft  |
    And the total number of records in search results by "hr1" are 7

  #EBS-7412
  Scenario: (05) View all Credit/Debit Notes, Filter by BusinessPartnerType by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on BusinessPartner which contains "vendor" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner            | DocumentOwner   | ReferenceDocument | Amount         | BusinessUnit     | State  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Shady.Abdelatif |                   | 3000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Ashraf Salah    |                   | 1000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000012 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000003 - ABC      | Shady.Abdelatif |                   | 1000.000 - JPY | 0003 - Offset    | Draft  |
      | 2021000011 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000001 - Siegwerk | Ashraf Salah    |                   | 1000.000 - CNY | 0001 - Flexo     | Draft  |
      | 2021000006 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000002 - Zhejiang | Shady.Abdelatif |                   | 1000.000 - EUR | 0002 - Signmedia | Draft  |
      | 2021000005 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000001 - Siegwerk | Ashraf Salah    |                   | 3000.000 - JPY | 0001 - Flexo     | Active |
      | 2021000004 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Shady.Abdelatif |                   | 1000.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000003 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Ashraf Salah    |                   | 1000.000 - EUR | 0001 - Flexo     | Draft  |
    And the total number of records in search results by "hr1" are 8

  #EBS-7412
  Scenario: (06) View all Credit/Debit Notes, Filter by BusinessPartnerName by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on BusinessPartner which contains "dor 6" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner            | DocumentOwner   | ReferenceDocument | Amount         | BusinessUnit | State  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Shady.Abdelatif |                   | 3000.000 - EUR | 0001 - Flexo | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Ashraf Salah    |                   | 1000.000 - EUR | 0001 - Flexo | Active |
      | 2021000004 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Shady.Abdelatif |                   | 1000.000 - EUR | 0001 - Flexo | Draft  |
      | 2021000003 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000051 - Vendor 6 | Ashraf Salah    |                   | 1000.000 - EUR | 0001 - Flexo | Draft  |
    And the total number of records in search results by "hr1" are 4

  #EBS-7412
  Scenario: (07) View all Credit/Debit Notes, Filter by BusinessPartnerCode by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on BusinessPartner which contains "003" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner       | DocumentOwner   | ReferenceDocument | Amount         | BusinessUnit  | State |
      | 2021000012 | DEBIT_NOTE_BASED_ON_COMMISSION | Vendor - 000003 - ABC | Shady.Abdelatif |                   | 1000.000 - JPY | 0003 - Offset | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-7412
  Scenario: (08) View all Credit/Debit Notes, Filter by DocumentOwner by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on DocumentOwner which contains "Ashraf Salah" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner                     | DocumentOwner | ReferenceDocument               | Amount         | BusinessUnit     | State  |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah  | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft  |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Ashraf Salah  |                                 | 1000.000 - EUR | 0001 - Flexo     | Active |
      | 2021000011 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk          | Ashraf Salah  |                                 | 1000.000 - CNY | 0001 - Flexo     | Draft  |
      | 2021000009 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah  | Sales Return Order - 2021000005 | 3345.000 - USD | 0003 - Offset    | Draft  |
      | 2021000007 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah  | Sales Return Order - 2021000003 | 5050.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000005 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk          | Ashraf Salah  |                                 | 3000.000 - JPY | 0001 - Flexo     | Active |
      | 2021000003 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6          | Ashraf Salah  |                                 | 1000.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000001 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah  | Sales Return Order - 2021000001 | 7070.000 - USD | 0003 - Offset    | Draft  |
    And the total number of records in search results by "hr1" are 8

  #EBS-7412
  Scenario: (09) View all Credit/Debit Notes, Filter by ReferenceDocumentCode  by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit with filter applied on ReferenceDocument which contains "002" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner              | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State |
      | 2021000002 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000002 | 6060.000 - EGP | 0002 - Signmedia | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-7412
  Scenario: (10) View all Credit/Debit Notes, Filter by ReferenceDocumentType by authorized user using "Contains" - SuperUser (case sensitive)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit with filter applied on ReferenceDocument which contains "Sales" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner                     | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit     | State  |
      | 2021000015 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000007 | 1100.000 - HKD | 0002 - Signmedia | Draft  |
      | 2021000010 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000006 | 2200.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000009 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000005 | 3345.000 - USD | 0003 - Offset    | Draft  |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo     | Active |
      | 2021000007 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Ashraf Salah    | Sales Return Order - 2021000003 | 5050.000 - EUR | 0001 - Flexo     | Draft  |
      | 2021000002 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram        | Shady.Abdelatif | Sales Return Order - 2021000002 | 6060.000 - EGP | 0002 - Signmedia | Draft  |
      | 2021000001 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000006 - المطبعة الأمنية | Ashraf Salah    | Sales Return Order - 2021000001 | 7070.000 - USD | 0003 - Offset    | Draft  |
    And the total number of records in search results by "hr1" are 7

  #EBS-7412
  Scenario: (11) View all Credit/Debit Notes, Filter by Amount by authorized user using "Equals" - SuperUser
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on Amount which equals "4040" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner              | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit | State  |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo | Active |
    And the total number of records in search results by "hr1" are 1

  #EBS-7412
  Scenario: (12) View all Credit/Debit Notes, Filter by BusinessUnit by authorized user using "Equals" - SuperUser
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on BusinessUnit which equals "0001" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner              | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit | State  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Shady.Abdelatif |                                 | 3000.000 - EUR | 0001 - Flexo | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo | Active |
      | 2021000011 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk   | Ashraf Salah    |                                 | 1000.000 - CNY | 0001 - Flexo | Draft  |
      | 2021000010 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000006 | 2200.000 - EUR | 0001 - Flexo | Draft  |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo | Active |
      | 2021000007 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Ashraf Salah    | Sales Return Order - 2021000003 | 5050.000 - EUR | 0001 - Flexo | Draft  |
      | 2021000005 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk   | Ashraf Salah    |                                 | 3000.000 - JPY | 0001 - Flexo | Active |
      | 2021000004 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Shady.Abdelatif |                                 | 1000.000 - EUR | 0001 - Flexo | Draft  |
      | 2021000003 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo | Draft  |
    And the total number of records in search results by "hr1" are 9

  #EBS-7412
  Scenario: (13) View all Credit/Debit Notes, Filter by State by authorized user using "Contains" - SuperUser (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Credit/Debit Notes with filter applied on State which contains "Active" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                              | BusinessPartner              | DocumentOwner   | ReferenceDocument               | Amount         | BusinessUnit | State  |
      | 2021000014 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Shady.Abdelatif |                                 | 3000.000 - EUR | 0001 - Flexo | Active |
      | 2021000013 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000051 - Vendor 6   | Ashraf Salah    |                                 | 1000.000 - EUR | 0001 - Flexo | Active |
      | 2021000008 | CREDIT_NOTE_BASED_ON_SALES_RETURN | Customer - 000001 - Al Ahram | Shady.Abdelatif | Sales Return Order - 2021000004 | 4040.000 - HKD | 0001 - Flexo | Active |
      | 2021000005 | DEBIT_NOTE_BASED_ON_COMMISSION    | Vendor - 000001 - Siegwerk   | Ashraf Salah    |                                 | 3000.000 - JPY | 0001 - Flexo | Active |
    And the total number of records in search results by "hr1" are 4

 #EBS-7412
  Scenario: (14) View all Credit/Debit Notes, by an unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Credit/Debit Notes with no filter applied with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page