# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud

Feature: Create Credit/Debit Notes - Happy Path

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                      |
      | Accountant_Signmedia | AccountingNoteOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission            | Condition |
      | AccountingNoteOwner_Signmedia | AccountingNote:Create |           |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit |
      | 000002 | Zhejiang | 0002           |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | AccountingNote | CanBeDocumentOwner |           |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State                 | CreationDate         |DocumentOwner  |
      | 2019000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | SalesInvoiceActivated | 11-Feb-2019 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company        | Store                     |
      | 2019000010 | 0002 - DigiPro | 0003 - DigiPro Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2019000010 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000001 - 99, El Hegaz St, Heliopolis | EGP         | 000001 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State               | CreationDate         |
      | 2019100013 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | DeliveredToCustomer | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019100013 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2019100013 | 000001 - Al Ahram | 2019000010 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                         | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100013 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 100.000        | 100.000              | 0019 - M2 |
    #@INSERT
    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner  | State   |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed | Shipped |
    #@INSERT
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000005 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And Insert the following ReturnDetails for SalesReturnOrders exist:
      | Code       | SICode     | Customer          | CurrencyISO |
      | 2020000005 | 2019100013 | 000001 - Al Ahram | EGP         |
    #@INSERT
    And Insert the following Item for SalesReturnOrders exist:
      | Code       | Item                                         | OrderUnit | ReturnReason                        | ReturnQuantity | SalesPrice |
      | 2020000005 | 000002 - Hot Laminated Frontlit Backlit roll | 0019 - M2 | 0005 - Items doesn't match the need | 30.000         | 100.000    |
    #@INSERT
    And Insert the following Taxes for SalesReturnOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0001 - Vat                                   | 1             |
      | 2020000005 | 0002 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0003 - Real estate taxes                     | 10            |
    And the following values for ItemsSummary for salesReturnOrders exist:
      | Code       | TotalAmountBeforeTaxes | TotalTaxes | TotalAmountAfterTaxes |
      | 2020000005 | 3000.000               | 345.000    | 3345.000              |

  # EBS-7723
  # EBS-7972
  # EBS-7973
  # EBS-5408
  # EBS-8027
  Scenario Outline: (01) Create Credit/Debit Note , with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created Credit/Debit Note was with code "2021000001"
    When "Shady.Abdelatif" creates Credit/Debit Note with the following values:
      | NoteType        | Type        | ReferenceDocument        | BusinessPartner        | Company        | BusinessUnit        | Amount        | Currency        | DocumentOwnerId        |
      | <NoteTypeValue> | <TypeValue> | <ReferenceDocumentValue> | <BusinessPartnerValue> | <CompanyValue> | <BusinessUnitValue> | <AmountValue> | <CurrencyValue> | <DocumentOwnerIdValue> |
    Then a new Credit/Debit Note is created as follows:
      | Code       | CreationDate         | LastUpdateDate       | CreatedBy       | LastUpdatedBy   | Type        | DocumentOwner | State |
      | 2021000002 | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | Shady.Abdelatif | Shady.Abdelatif | <TypeValue> | Ashraf Salah  | Draft |
    And the CompanyDetails Section of Credit/Debit Note with code "2021000002" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the NoteDetails Section of Credit/Debit Note with code "2021000002" is updated as follows:
      | BusinessPartner   | Amount   | Currency   | ReferenceDocument   |
      | <BusinessPartner> | <Amount> | <Currency> | <ReferenceDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | NoteTypeValue | TypeValue                         | ReferenceDocumentValue | BusinessPartnerValue | CompanyValue | BusinessUnitValue | AmountValue | CurrencyValue | DocumentOwnerIdValue | BusinessUnit     | Company        | BusinessPartner   | Amount    | Currency | ReferenceDocument               |
      | DEBIT         | DEBIT_NOTE_BASED_ON_COMMISSION    |                        | 000002               | 0002         | 0002              | 15000.000   | EGP           | 34                   | 0002 - Signmedia | 0002 - DigiPro | 000002 - Zhejiang | 15000.000 | EGP      |                                 |
      | CREDIT        | CREDIT_NOTE_BASED_ON_SALES_RETURN | 2020000005             |                      |              |                   |             |               | 34                   | 0002 - Signmedia | 0002 - DigiPro | 000001 - Al Ahram | 3345.000  | EGP      | Sales Return Order - 2020000005 |
