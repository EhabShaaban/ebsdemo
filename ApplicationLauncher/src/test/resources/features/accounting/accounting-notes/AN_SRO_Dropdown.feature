# Author Dev: Order Team (17-Jan-2021)
# Author Quality: Shirin Mahmoud

Feature: Accounting Note - SalesReturnOrder (Shipped | GoodsReceiptActivated) Dropdown

  Background:

    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole             |
      | Accountant_Signmedia | SROViewer_Signmedia |
      | SuperUser            | SuperUserSubRole    |

    And the following sub-roles and permissions exist:
      | Subrole             | Permission               | Condition                      |
      | SROViewer_Signmedia | SalesReturnOrder:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole    | *:*                      |                                |

    And the following users doesn't have the following permissions:
      | User | Permission               |
      | Afaf | SalesReturnOrder:ReadAll |

    #INSERT
    And Insert the following GeneralData for SalesReturnOrders exist:
      | Code       | CreationDate         | Type                                                  | CreatedBy | DocumentOwner       | State                 |
      | 2020000001 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000002 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Shipped               |
      | 2020000003 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | GoodsReceiptActivated |
      | 2020000004 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | CreditNoteActivated   |
      | 2020000005 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | Completed             |
      | 2020000051 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000052 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | Draft                 |
      | 2020000053 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Shipped               |
      | 2020000054 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Draft                 |
      | 2020000055 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Draft                 |
      | 2020000056 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | GoodsReceiptActivated |
      | 2020000057 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | SeragEldin Meghawry | Shipped               |
      | 2020000058 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Manar Mohammed      | Shipped               |
      | 2020000059 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | Shipped               |
      | 2020000060 | 15-Jun-2020 02:27 PM | RETURN_SALES_ORDER_FOR_ITEMS - Sales return for items | Admin     | Ahmed Al-Ashry      | Shipped               |
    #INSERT
    And Insert the following CompanyData for SalesReturnOrders exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000002 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2020000003 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000005 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000051 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000052 | 0002 - Signmedia | 0003 - HPS       |
      | 2020000053 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000054 | 0001 - Flexo     | 0001 - AL Madina |
      | 2020000055 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000056 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000057 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000058 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000059 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2020000060 | 0002 - Signmedia | 0001 - AL Madina |
    And the total number of existing records of SalesReturnOrders is 15

  #EBS-8052
  Scenario: (01) Read list of SalesReturnOrders dropdown for AN filtered by (Shipped | GoodsReceiptActivated) states by an authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read drop-down of SalesReturnOrder for State (Shipped | GoodsReceiptActivated)
    Then the following SalesReturnOrders will be presented to "hr1":
      | Code       |
      | 2020000060 |
      | 2020000059 |
      | 2020000058 |
      | 2020000057 |
      | 2020000056 |
      | 2020000053 |
      | 2020000003 |
      | 2020000002 |
    And the total number of records in search results by "hr1" are 8

  #EBS-8052
  Scenario: (02) Read list of SalesReturnOrders dropdown for AN filtered by (Shipped | GoodsReceiptActivated) states by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read drop-down of SalesReturnOrder for State (Shipped | GoodsReceiptActivated)
    Then the following SalesReturnOrders will be presented to "Shady.Abdelatif":
      | Code       |
      | 2020000060 |
      | 2020000059 |
      | 2020000058 |
      | 2020000057 |
      | 2020000056 |
      | 2020000053 |
    And the total number of records in search results by "Shady.Abdelatif" are 6

  #EBS-8052
  Scenario: (03) Read list of SalesReturnOrders dropdown for AN filtered by (Shipped | GoodsReceiptActivated) states by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read drop-down of SalesReturnOrder for State (Shipped | GoodsReceiptActivated)
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page