Feature: View All Journal Entry

  Background:

    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | AccountantOwner_Signmedia |
      | SuperUser            | SuperUserSubRole          |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission           | Condition                      |
      | AccountantOwner_Signmedia | JournalEntry:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole          | *:*                  |                                |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000049 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000044 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000003 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000045 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000045 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

         #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2021000002 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2019000108 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2019000109 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2020000002 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2020 11:00 AM | Ashraf Salah  |

    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy  | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument           | Company        |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1        | 02-Sep-2019 10:00 PM | 0002 - Signmedia  | 2019         | 2019000104 - VendorInvoice  | 0002 - DigiPro |
      | 2020000021 | 01-Sep-2020 11:53 AM | hr1        | 02-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000108 - VendorInvoice  | 0002 - DigiPro |
      | 2020000022 | 01-Sep-2020 11:57 AM | hr1        | 02-Sep-2019 10:00 PM | 0001 - Flexo      | 2019         | 2019000109 - VendorInvoice  | 0002 - DigiPro |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000003 - PaymentRequest | 0002 - DigiPro |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1        | 06-Dec-2020 10:00 PM | 0002 - Signmedia  | 2020         | 2020000002 - VendorInvoice  | 0002 - DigiPro |
      | 2021000001 | 04-Jan-2021 09:45 AM | hr1        | 04-Jan-2021 10:00 PM | 0002 - Signmedia  | 2021         | 2021000002 - PaymentRequest | 0002 - DigiPro |

  Scenario: (01) Filter View All JournalEntries by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntries with no filter applied with 10 records per page
    Then the following JournalEntries will be presented to "hr1":
      | Code       | CreationDate         | CreatedBy  | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument           | Company |
      | 2021000001 | 04-Jan-2021 09:45 AM | hr1        | 04-Jan-2021 10:00 PM | 0002 - Signmedia  | 2021         | 2021000002 - PaymentRequest | DigiPro |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1        | 06-Dec-2020 10:00 PM | 0002 - Signmedia  | 2020         | 2020000002 - VendorInvoice  | DigiPro |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000003 - PaymentRequest | DigiPro |
      | 2020000022 | 01-Sep-2020 11:57 AM | hr1        | 02-Sep-2019 10:00 PM | 0001 - Flexo      | 2019         | 2019000109 - VendorInvoice  | DigiPro |
      | 2020000021 | 01-Sep-2020 11:53 AM | hr1        | 02-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000108 - VendorInvoice  | DigiPro |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1        | 02-Sep-2019 10:00 PM | 0002 - Signmedia  | 2019         | 2019000104 - VendorInvoice  | DigiPro |
    And the total number of records in search results by "hr1" are 6

  Scenario: (02) Filter View All JournalEntries by JournalEntryCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntries with filter applied on JournalEntryCode which contains "28" with 3 records per page
    Then the following JournalEntries will be presented to "hr1":
      | Code       | CreationDate         | CreatedBy  | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument           | Company |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000003 - PaymentRequest | DigiPro |
    And the total number of records in search results by "hr1" are 1

  Scenario: (03) Filter View All JournalEntries by CreationDate by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntries with filter applied on CreationDate which equals "22-Sep-2020" with 3 records per page
    Then the following JournalEntries will be presented to "hr1":
      | Code       | CreationDate         | CreatedBy  | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument           | Company |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 10:00 PM | 0006 - Corrugated | 2019         | 2019000003 - PaymentRequest | DigiPro |
    And the total number of records in search results by "hr1" are 1

  Scenario: (04) Filter View All JournalEntries by CreatedBy by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 2 of JournalEntries with filter applied on CreatedBy which contains "Amr" with 3 records per page
    Then the following JournalEntries will be presented to "hr1":
      | Code       | CreationDate         | CreatedBy  | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument           | Company |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 12:00 AM | 0006 - Corrugated | 2019         | 2019000003 - PaymentRequest | DigiPro |
    And the total number of records in search results by "hr1" are 1

  Scenario: (05) Filter View All JournalEntries by JournalDate by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntries with filter applied on JournalDate which equals "02-Sep-2019" with 3 records per page
    Then the following JournalEntries will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument          | Company |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1       | 02-Sep-2019 10:00 PM | 0002 - Signmedia | 2019         | 2019000104 - VendorInvoice | DigiPro |
    And the total number of records in search results by "Shady.Abdelatif" are 1

  Scenario: (06) Filter View All JournalEntries by ReferenceDocument by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntries with filter applied on ReferenceDocument which contains "2019000104" with 3 records per page
    Then the following JournalEntries will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument          | Company |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1       | 02-Sep-2019 10:00 PM | 0002 - Signmedia | 2019         | 2019000104 - VendorInvoice | DigiPro |
    And the total number of records in search results by "Shady.Abdelatif" are 1

  Scenario: (07) Filter View All JournalEntries by Company by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntries with filter applied on Company which contains "giP" with 3 records per page while current locale is "en-US"
    Then the following JournalEntries will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument           | Company |
      | 2021000001 | 04-Jan-2021 09:45 AM | hr1       | 04-Jan-2021 10:00 PM | 0002 - Signmedia | 2021         | 2021000002 - PaymentRequest | DigiPro |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1       | 06-Dec-2020 10:00 PM | 0002 - Signmedia | 2020         | 2020000002 - VendorInvoice  | DigiPro |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1       | 02-Sep-2019 10:00 PM | 0002 - Signmedia | 2019         | 2019000104 - VendorInvoice  | DigiPro |
    And the total number of records in search results by "Shady.Abdelatif" are 3

  Scenario: (08) Filter View All JournalEntries by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntries with filter applied on BusinessUnit which contains "Sign" with 3 records per page while current locale is "en-US"
    Then the following JournalEntries will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument           | Company |
      | 2021000001 | 04-Jan-2021 09:45 AM | hr1       | 04-Jan-2021 10:00 PM | 0002 - Signmedia | 2021         | 2021000002 - PaymentRequest | DigiPro |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1       | 06-Dec-2020 10:00 PM | 0002 - Signmedia | 2020         | 2020000002 - VendorInvoice  | DigiPro |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1       | 02-Sep-2019 10:00 PM | 0002 - Signmedia | 2019         | 2019000104 - VendorInvoice  | DigiPro |
    And the total number of records in search results by "Shady.Abdelatif" are 3

  Scenario: (09) Filter View All JournalEntries by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntries with filter applied on FiscalPeriod which contains "2020" with 3 records per page while current locale is "en-US"
    Then the following JournalEntries will be presented to "Shady.Abdelatif":
      | Code       | CreationDate         | CreatedBy | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument          | Company |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1       | 06-Dec-2020 10:00 PM | 0002 - Signmedia | 2020         | 2020000002 - VendorInvoice | DigiPro |
    And the total number of records in search results by "Shady.Abdelatif" are 1

  Scenario: (10) Filter View All JournalEntries by JournalEntryCode by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of JournalEntries with filter applied on JournalEntryCode which contains "03" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page