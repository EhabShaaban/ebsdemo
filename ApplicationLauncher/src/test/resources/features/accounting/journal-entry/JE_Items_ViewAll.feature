Feature: View All Vendor Journal Entry Items

  Background:

    Given the following users and roles exist:
      | Name            | Role                      |
      | Shady.Abdelatif | Accountant_Signmedia      |
      | Ahmed.Hussein   | AccountantHead_Corrugated |
      | Afaf            | FrontDesk                 |
      | hr1             | SuperUser                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                    |
      | Accountant_Signmedia      | AccountantOwner_Signmedia  |
      | AccountantHead_Corrugated | AccountantOwner_Corrugated |
      | SuperUser                 | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                   | Condition                       |
      | AccountantOwner_Signmedia  | JournalEntry:ReadJournalItem | [purchaseUnitName='Signmedia']  |
      | AccountantOwner_Corrugated | JournalEntry:ReadJournalItem | [purchaseUnitName='Corrugated'] |
      | SuperUserSubRole           | *:*                          |                                 |

        #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000049 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000044 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000003 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2018000045 | IMPORT_PO  | Confirmed | Admin     | 5-Aug-2018 9:02 AM | Gehan Ahmed   |
      | 2020000045 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2021000002 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Shady.Abdelatif | 05-Aug-2021 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2019000108 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2019000109 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 11:00 AM | Ashraf Salah  |
      | 2020000002 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2020 11:00 AM | Ashraf Salah  |


    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy  | JournalDate          | ReferenceDocument           | Company          | BusinessUnit      |
      | 2020000020 | 01-Sep-2020 10:47 AM | hr1        | 02-Sep-2019 10:00 AM | 2019000104 - VendorInvoice  | 0002 - DigiPro   | 0002 - Signmedia  |
      | 2020000021 | 01-Sep-2020 11:53 AM | hr1        | 02-Aug-2019 10:00 AM | 2019000108 - VendorInvoice  | 0002 - DigiPro   | 0006 - Corrugated |
      | 2020000022 | 01-Sep-2020 11:57 AM | hr1        | 02-Sep-2019 10:00 AM | 2019000109 - VendorInvoice  | 0002 - DigiPro   | 0001 - Flexo      |
      | 2020000028 | 22-Sep-2020 10:30 AM | Amr.Khalil | 07-Aug-2019 10:00 AM | 2019000003 - PaymentRequest | 0001 - AL Madina | 0006 - Corrugated |
      | 2020000056 | 06-Dec-2020 11:06 AM | hr1        | 06-Dec-2020 10:00 AM | 2020000002 - VendorInvoice  | 0002 - DigiPro   | 0002 - Signmedia  |
      | 2021000001 | 04-Jan-2021 09:45 AM | hr1        | 04-Jan-2021 10:00 AM | 2021000002 - PaymentRequest | 0002 - DigiPro   | 0002 - Signmedia  |

    #@INSERT
    And the following JournalEntries Details exist:
      | JournalEntryCode | JournalDate          | Subledger     | GLAccount                 | GLSubAccount                    | Credit  | Debit   | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |

      | 2020000020       | 02-Sep-2019 12:00 AM | PO            | 10207 - PO                | 2018000049                      | 0       | 422     | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000020       | 02-Sep-2019 12:00 AM | Local_Vendors | 10221 - service vendors 2 | 000002 - Zhejiang               | 422     | 0       | USD                | EGP               | 10                        | EGP             | 10                      |

      | 2020000021       | 02-Aug-2019 12:00 AM | PO            | 10207 - PO                | 2018000044                      | 0       | 52857.6 | USD                | EGP               | 16.5                      | EGP             | 16.5                    |
      | 2020000021       | 02-Aug-2019 12:00 AM | Local_Vendors | 10221 - service vendors 2 | 000002 - Zhejiang               | 52857.6 | 0       | USD                | EGP               | 16.5                      | EGP             | 16.5                    |

      | 2020000022       | 02-Sep-2019 12:00 AM | PO            | 10207 - PO                | 2018000003                      | 0       | 720     | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000022       | 02-Sep-2019 12:00 AM | Local_Vendors | 10201 - Service Vendors   | 000001 - Siegwerk               | 720     | 0       | USD                | EGP               | 10                        | EGP             | 10                      |

      | 2020000028       | 07-Aug-2019 10:00 AM | PO            | 10207 - PO                | 2018000045                      | 0       | 100     | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
      | 2020000028       | 07-Aug-2019 10:00 AM | Banks         | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 100     | 0       | USD                | EUR               | 17.44                     | EGP             | 17.44                   |

      | 2020000056       | 06-Dec-2020 10:00 AM | PO            | 10207 - PO                | 2020000045                      | 0       | 8064    | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000056       | 06-Dec-2020 10:00 AM | Local_Vendors | 10221 - service vendors 2 | 000002 - Zhejiang               | 8064    | 0       | USD                | EGP               | 10                        | USD             | 1                       |

      | 2021000001       | 04-Jan-2021 10:00 AM | PO            | 10207 - PO                |                                 | 0       | 1000    | EGP                | EGP               | 1                         | EGP             | 1                       |
      | 2021000001       | 04-Jan-2021 10:00 AM | Banks         | 10431 - Bank              | 1516171819712 - USD - Alex Bank | 1000    | 0       | EGP                | EGP               | 1                         | EGP             | 1                       |

  Scenario: (01) Filter View All JournalEntriesItems by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with no filter applied with 5 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount                 | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2021000001 | 04-Jan-2021 10:00 AM | 0002 - DigiPro   | 0002 - Signmedia  | 10431 - Bank              | 1516171819712 - USD - Alex Bank | 0     | 1000   | EGP                | EGP               | 1                         | EGP             | 1                       |
      | 2021000001 | 04-Jan-2021 10:00 AM | 0002 - DigiPro   | 0002 - Signmedia  | 10207 - PO                |                                 | 1000  | 0      | EGP                | EGP               | 1                         | EGP             | 1                       |

      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro   | 0002 - Signmedia  | 10221 - service vendors 2 | 000002 - Zhejiang               | 0     | 8064   | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro   | 0002 - Signmedia  | 10207 - PO                | 2020000045                      | 8064  | 0      | USD                | EGP               | 10                        | USD             | 1                       |

      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |

    And the total number of records in search results by "hr1" are 12

  Scenario: (02) Filter View All JournalEntriesItems by JournalEntryCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on JournalEntryCode which contains "22" with 4 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company        | BusinessUnit | GLAccount               | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000022 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0001 - Flexo | 10201 - Service Vendors | 000001 - Siegwerk | 0     | 720    | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000022 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0001 - Flexo | 10207 - PO              | 2018000003        | 720   | 0      | USD                | EGP               | 10                        | EGP             | 10                      |

    And the total number of records in search results by "hr1" are 2

  Scenario: (03) Filter View All JournalEntriesItems by JournalDate by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on JournalDate which equals "02-Sep-2019" with 3 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10207 - PO                | 2018000049        | 422   | 0      | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "Shady.Abdelatif" are 2
#
  Scenario: (04) Filter View All JournalEntriesItems by GLAccount by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Hussein"
    When "Ahmed.Hussein" requests to read page 1 of JournalEntriesItems with filter applied on GLAccount which contains "Bank" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Hussein":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount    | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
    And the total number of records in search results by "Ahmed.Hussein" are 1
#Ahmed.Hussein
  Scenario: (05) Filter View All JournalEntriesItems by GLAccount by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Hussein"
    When "Ahmed.Hussein" requests to read page 1 of JournalEntriesItems with filter applied on GLAccount which contains "10431" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Hussein":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount    | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
    And the total number of records in search results by "Ahmed.Hussein" are 1

  Scenario: (06) Filter View All JournalEntriesItems by GLSubAccount by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on GLSubAccount which contains "ej" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 8064   | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "Shady.Abdelatif" are 2

  Scenario: (07) Filter View All JournalEntriesItems by GLSubAccount by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on GLSubAccount which contains "000002" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 8064   | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "Shady.Abdelatif" are 2

#
  Scenario: (08) Filter View All JournalEntriesItems by GLSubAccount by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Hussein"
    When "Ahmed.Hussein" requests to read page 1 of JournalEntriesItems with filter applied on GLSubAccount which contains "2018000045" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "Ahmed.Hussein":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount  | GLSubAccount | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10207 - PO | 2018000045   | 100   | 0      | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
    And the total number of records in search results by "Ahmed.Hussein" are 1

#
  Scenario: (09) Filter View All JournalEntriesItems by Debit by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on Debit which equals "422" with 3 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount  | GLSubAccount | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10207 - PO | 2018000049   | 422   | 0      | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "Shady.Abdelatif" are 1
#
#
  Scenario: (10) Filter View All JournalEntriesItems by Credit by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on Credit which equals "422" with 3 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "Shady.Abdelatif" are 1


  Scenario: (11) Filter View All JournalEntriesItems by Currency(Document) by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 2 of JournalEntriesItems with filter applied on Currency(Document) which equals "0002" with 2 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10207 - PO                | 2018000049        | 422   | 0      | USD                | EGP               | 10                        | EGP             | 10                      |

    And the total number of records in search results by "Shady.Abdelatif" are 4
#
#
  Scenario: (12) Filter View All JournalEntriesItems by ExRateToCurrency(Company) by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of JournalEntriesItems with filter applied on ExRateToCurrency(Company) which equals "10" with 3 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 8064   | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10207 - PO                | 2020000045        | 8064  | 0      | USD                | EGP               | 10                        | USD             | 1                       |

      | 2020000020 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 422    | USD                | EGP               | 10                        | EGP             | 10                      |

    And the total number of records in search results by "Shady.Abdelatif" are 4

  Scenario: (13) Filter View All JournalEntriesItems by Currency(Company) by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on Currency(Company) which equals "0003" with 2 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount    | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10207 - PO   | 2018000045                      | 100   | 0      | USD                | EUR               | 17.44                     | EGP             | 17.44                   |

    And the total number of records in search results by "hr1" are 2

  Scenario: (14) Filter View All JournalEntriesItems by Currency(CompanyGroup) by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on Currency(CompanyGroup) which equals "0002" with 2 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company        | BusinessUnit     | GLAccount                 | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10221 - service vendors 2 | 000002 - Zhejiang | 0     | 8064   | USD                | EGP               | 10                        | USD             | 1                       |
      | 2020000056 | 06-Dec-2020 10:00 AM | 0002 - DigiPro | 0002 - Signmedia | 10207 - PO                | 2020000045        | 8064  | 0      | USD                | EGP               | 10                        | USD             | 1                       |

    And the total number of records in search results by "hr1" are 2

  Scenario: (15) Filter View All JournalEntriesItems by ExRateToCurrency(CompanyGroup) by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on ExRateToCurrency(CompanyGroup) which equals "17.44" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount    | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10207 - PO   | 2018000045                      | 100   | 0      | USD                | EUR               | 17.44                     | EGP             | 17.44                   |

    And the total number of records in search results by "hr1" are 2

  Scenario: (16) Filter View All JournalEntriesItems by Company by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on Company which equals "0001" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company          | BusinessUnit      | GLAccount    | GLSubAccount                    | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 0     | 100    | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
      | 2020000028 | 07-Aug-2019 10:00 AM | 0001 - AL Madina | 0006 - Corrugated | 10207 - PO   | 2018000045                      | 100   | 0      | USD                | EUR               | 17.44                     | EGP             | 17.44                   |
    And the total number of records in search results by "hr1" are 2


  Scenario: (17) Filter View All JournalEntriesItems by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of JournalEntriesItems with filter applied on BusinessUnit which equals "0001" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | JournalDate          | Company        | BusinessUnit | GLAccount               | GLSubAccount      | Debit | Credit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | Currency(Group) | ExRateToCurrency(Group) |
      | 2020000022 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0001 - Flexo | 10201 - Service Vendors | 000001 - Siegwerk | 0     | 720    | USD                | EGP               | 10                        | EGP             | 10                      |
      | 2020000022 | 02-Sep-2019 10:00 AM | 0002 - DigiPro | 0001 - Flexo | 10207 - PO              | 2018000003        | 720   | 0      | USD                | EGP               | 10                        | EGP             | 10                      |
    And the total number of records in search results by "hr1" are 2


  Scenario: (18) Filter View All JournalEntriesItems by JournalEntryCode by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of JournalEntriesItems with filter applied on JournalEntryCode which contains "01" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

#
