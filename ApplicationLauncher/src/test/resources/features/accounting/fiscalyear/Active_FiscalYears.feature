Feature: ViewAll FiscalYears Dropdown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole          |
      | Accountant_Signmedia | FiscalYearViewer |
    And the following sub-roles and permissions exist:
      | Subrole          | Permission         | Condition |
      | FiscalYearViewer | FiscalYear:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | FiscalYear:ReadAll |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |

  Scenario: (01) Read list of FiscalYears dropdown, by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read Active FiscalYears
    Then the following FiscalYears values will be presented to "Shady.Abdelatif":
      | Code | Name | From                | To                   |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM |

  Scenario: (02) Read list of FiscalYears dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read Active FiscalYears
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
