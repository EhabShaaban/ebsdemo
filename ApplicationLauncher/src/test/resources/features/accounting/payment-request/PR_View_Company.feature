Feature: View CompanyData section

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | hr1         | SuperUser                       |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
    And the following roles and sub-roles exist:
      | Role                            | Sub-role                       |
      | SuperUser                       | SuperUserSubRole               |
      | PurchasingResponsible_Signmedia | LimitedPaymentViewer_Signmedia |
      | PurchasingResponsible_Flexo     | LimitedPaymentViewer_Flexo     |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                     | Condition                                                         |
      | SuperUserSubRole               | *:*                            |                                                                   |
      | LimitedPaymentViewer_Signmedia | PaymentRequest:ReadAll         | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | LimitedPaymentViewer_Flexo     | PaymentRequest:ReadCompanyData | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser'])     |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
      | 0003 | Offset    |
   #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | Remaining | DocumentOwner |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |
  # EBS-4473
  Scenario: (01) View PR CompanyData, section by an authorized user who has one role (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to view CompanyData section of PaymentRequest with code "2019000004"
    Then the following values of CompanyData section for PaymentRequest with code "2019000004" are displayed to "hr1":
      | PRCode     | BusinessUnit     | Company        |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |

  # EBS-4473
  Scenario: (02) View PR CompanyData, section where PaymentRequest doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000004" successfully
    When "Gehan.Ahmed" requests to view CompanyData section of PaymentRequest with code "2019000004"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-4473
  Scenario: (03) View PR CompanyData, section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view CompanyData section of PaymentRequest with code "2019000004"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
