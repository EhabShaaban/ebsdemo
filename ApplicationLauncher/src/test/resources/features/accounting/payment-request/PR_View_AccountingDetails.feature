Feature: View Accounting Details section

  Background:
    Given the following users and roles exist:
      | Name            | Role                  |
      | hr1             | SuperUser             |
      | Ahmed.Hamdi     | Accountant_Flexo      |
      | Ashraf.Farouk   | AccountantHead_Offset |
      | Shady.Abdelatif | Accountant_Signmedia  |
    And the following roles and sub-roles exist:
      | Role                  | Sub-role                |
      | SuperUser             | SuperUserSubRole        |
      | Accountant_Flexo      | PaymentViewer_Flexo     |
      | AccountantHead_Offset | PaymentViewer_Offset    |
      | Accountant_Signmedia  | PaymentViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                           | Condition                      |
      | SuperUserSubRole        | *:*                                  |                                |
      | PaymentViewer_Flexo     | PaymentRequest:ReadAccountingDetails | [purchaseUnitName='Flexo']     |
      | PaymentViewer_Flexo     | PaymentRequest:ReadCompanyData       | [purchaseUnitName='Flexo']     |
      | PaymentViewer_Flexo     | PaymentRequest:ReadAll               | [purchaseUnitName='Flexo']     |
      | PaymentViewer_Offset    | PaymentRequest:ReadAccountingDetails | [purchaseUnitName='Offset']    |
      | PaymentViewer_Offset    | PaymentRequest:ReadCompanyData       | [purchaseUnitName='Offset']    |
      | PaymentViewer_Offset    | PaymentRequest:ReadAll               | [purchaseUnitName='Offset']    |
      | PaymentViewer_Signmedia | PaymentRequest:ReadAccountingDetails | [purchaseUnitName='Signmedia'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following users have the following permissions without the following conditions:
      | User          | Permission                           | Condition                  |
      | Ashraf.Farouk | PaymentRequest:ReadAccountingDetails | [purchaseUnitName='flexo'] |
    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy                   | CreationDate         | Remaining | DocumentOwner |
      | 2019000014 | VENDOR                   | PaymentDone | Gehan.Ahmed                 | 06-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000013 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000030 | VENDOR                   | Draft       | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
    #@INSERTAshraf Salah  |
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000014 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000013 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000030 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests AccountingDetails exist:
      | PRCode     | Credit/Debit | Account                   | SubAccount                      | Value      |
      | 2019000014 | DEBIT        | 10201 - Service Vendors   | 000002 - Zhejiang               | 540.256398 |
      | 2019000014 | CREDIT       | 10430 - Cash              | 0002 - Signmedia Treasury       | 540.256398 |
      | 2019000013 | DEBIT        | 10207 - PO                | 2018000018                      | 1000       |
      | 2019000013 | CREDIT       | 10430 - Cash              | 0002 - Signmedia Treasury       | 1000       |
      | 2019000030 | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000       |
      | 2019000030 | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000       |

  # EBS-4473
  Scenario Outline: (01) View AccountingDetails section by an authorized user who has one role (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to view AccountingDetails section of PaymentRequest with code "<PRCode>"
    Then the following values of AccountingDetails section for PaymentRequest with code "<PRCode>" are displayed to "<User>":
      | AccountType | GLAccount            | GLSubAccount            | Amount        |
      | DEBIT       | <DebitAccountValue>  | <DebitSubAccountValue>  | <AmountValue> |
      | CREDIT      | <CreditAccountValue> | <CreditSubAccountValue> | <AmountValue> |

    Examples:
      | User            | PRCode     | DebitAccountValue       | DebitSubAccountValue | CreditAccountValue | CreditSubAccountValue     | AmountValue |
      | Shady.Abdelatif | 2019000014 | 10201 - Service Vendors | 000002 - Zhejiang    | 10430 - Cash       | 0002 - Signmedia Treasury | 540.256398  |
      | hr1             | 2019000013 | 10207 - PO              | 2018000018           | 10430 - Cash       | 0002 - Signmedia Treasury | 1000        |

  # EBS-4473
  Scenario: (02) View AccountingDetails section where PaymentRequest doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000030" successfully
    When "Ahmed.Hamdi" requests to view AccountingDetails section of PaymentRequest with code "2019000030"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"

  # EBS-4473
  Scenario: (03) View AccountingDetails section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Ashraf.Farouk"
    When "Ashraf.Farouk" requests to view AccountingDetails section of PaymentRequest with code "2019000014"
    Then "Ashraf.Farouk" is logged out
    And "Ashraf.Farouk" is forwarded to the error page
