# Author Dev: Ibrahim Khidr, Zyad Ghorab, Waseem Salama, Yara Ameen
# Author Quality: Eslam Ayman, Khadrah Ali

  #TODO: Rename "PaymentRequest" to "Payment" (EBS-8562)
  #TODO: Rename "PaymentDone" to "Active" (EBS-8562)
  #TODO: Rename subrole "PaymentResponsible" to "PaymentActivator" (EBS-8562)

Feature: Activate Payment Request (Payment Based On Credit Note) (HP)

  - Activating a PaymentRequest (Payment Based On Credit Note) results in the following:
  - 1) PaymentRequest state is changed to PaymentDone  (EBS-4867)
  - 2) PaymentRequest Accounting Details are determined (EBS-8166) (Not developed yet)
  - 3) Journal Entries for PaymentRequest is created, and Activation Details are determined where: (EBS-5254)
  - 3.1) the retrieved fiscal year is the active one and the due date belongs to it (EBS-8557) (Not developed yet)
  - 3.2) latest ExchangeRate is retrieved based on of default strategy (EBS-4852)
  - 4) Related Reference Document Remaining is updated according to PaymentRequest amount (EBS-7266) (Not developed yet)
  - 5) Related JournalBalances are decreased according to PaymentRequest amount (EBS-8457)


  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2018 | 1-Jan-2018 12:00 AM | 31-Dec-2018 11:59 PM | Closed |
      | 0002 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
      | 0003 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |


    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Mar-2018 10:01 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2018 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2018 10:00 PM | Central Bank Of Egypt |

     #@INSERT
    Given the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Posted | Admin     | 10-Aug-2018 11:00 AM | Ashraf Salah  | 800000    |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit | Company          |
      | 2019000008 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2019000008 | 000001 - Al Ahram | 800000 | 0002 - USD |

  Scenario: (01) Activate Payment Request against CreditNote (Happy Path)
  """
  - PaymentRequest netAmount is less than or equal CreditNote amount
  - PaymentForm is BANK
  - All mandatory fields
  - By an authorized user
  """
        #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit | Company          | Currency   | BankAccountNumber               | Balance |
      | BANK00010001000200011234567893499 | 0001 - Flexo | 0001 - AL Madina | 0002 - USD | 1234567893499 - USD - Bank Misr | 900000  |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType          | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2018100004 | BASED_ON_CREDIT_NOTE | Draft | Amr.Khalil | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company          |
      | 2018100004 | 0001 - Flexo | 0001 - AL Madina |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                  |
      | 2018100004 | BANK        | 000001 - Siegwerk | CREDITNOTE      | 2019000008  | 800000    | USD         |                | 1234567893499 - USD - Bank Misr |          | payment based on credit note |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "BASED_ON_CREDIT_NOTE" of method "BANK" with code "2018100004" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2018100004" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And ActivationDetails in PaymentRequest with Code "2018100004" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.44         |                  | 2019       |
    And JournalBalance with code "BANK00010001000200011234567893499" is updated as follows "100000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  Scenario: (02) Activate Payment Request against CreditNote (Happy Path)
  """
  - PaymentRequest netAmount is less than or equal CreditNote amount
  - PaymentForm is CASH
  - All mandatory fields
  - By an authorized user
  """
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit | Company          | Currency   | Treasury              | Balance |
      | TREASURY0001000100020001 | 0001 - Flexo | 0001 - AL Madina | 0002 - USD | 0001 - Flexo Treasury | 900000  |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType          | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2018100005 | BASED_ON_CREDIT_NOTE | Draft | Amr.Khalil | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company          |
      | 2018100005 | 0001 - Flexo | 0001 - AL Madina |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury              | Description                  |
      | 2018100005 | CASH        | 000001 - Siegwerk | CREDITNOTE      | 2019000008  | 800000    | USD         |                |                   | 0001 - Flexo Treasury | payment based on credit note |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "BASED_ON_CREDIT_NOTE" of method "CASH" with code "2018100005" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2018100005" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And ActivationDetails in PaymentRequest with Code "2018100005" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.44         |                  | 2019       |
    And JournalBalance with code "TREASURY0001000100020001" is updated as follows "100000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"


