# Author Dev: Yara Ameen
# Author Quality: Khadra Ali
Feature: PaymentRequest - View Employees Dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    #@INSERT
    And the following Employees exist:
      | Code   | Name         |
      | 000001 | Afaf         |
      | 000002 | Ahmed Hamdi  |
      | 000003 | Amr Khalil   |
      | 000004 | Marwa Tawfik |

  #EBS-7958
  Scenario: (01) Read all Employees by an authorized user
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all Employees
    Then the following values of Employees are displayed to "Gehan.Ahmed":
      | Employee              |
      | 000004 - Marwa Tawfik |
      | 000003 - Amr Khalil   |
      | 000002 - Ahmed Hamdi  |
      | 000001 - Afaf         |
    And total number of Employees returned to "Gehan.Ahmed" is equal to 4

  @Future
  Scenario: (02) Read all Employees by unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Employees
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
