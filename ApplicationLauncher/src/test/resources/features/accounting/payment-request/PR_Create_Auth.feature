# Author Dev: Ibrahim Khidr, Waseem Salama, Hossam Hassan, Evram Hany, Yara Ameen
Feature: Create Payment Request - Authorization

  Background:
    Given the following users and roles exist:
      | Name                         | Role                                                         |
      | Afaf                         | FrontDesk                                                    |
      | Gehan.Ahmed                  | PurchasingResponsible_Signmedia                              |
      | Gehan.Ahmed.NoPurchaseUnits  | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        |
      | Gehan.Ahmed.NoVendor         | PurchasingResponsible_Signmedia_CannotReadVendor             |
      | Gehan.Ahmed.NoDocumentOwner  | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  |
      | Gehan.Ahmed.NoPurchaseOrder  | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      |
      | Gehan.Ahmed.NoInvoice        | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        |
      | Gehan.Ahmed.NoAccountingNote | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole |
      | Gehan.Ahmed.NoCompany        | PurchasingResponsible_Signmedia_CannotViewCompanyRole        |
      | Gehan.Ahmed.NoEmployee       | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       |

    And the following roles and sub-roles exist:
      | Role                                                         | Subrole                        |
      | PurchasingResponsible_Signmedia                              | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia                              | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia                              | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia                              | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia                              | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia                              | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia                              | CompanyViewer                  |
      | PurchasingResponsible_Signmedia                              | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotReadPurUnitRole        | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotReadVendor             | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotReadDocumentOwnerRole  | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole        | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder      | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole        | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia_CannotViewAccountingNoteRole | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | VendorViewer_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | CompanyViewer                  |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewEmployeeRole       | DocumentOwnerViewer            |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                   | Condition                      |
      | PaymentOwner_Signmedia         | PaymentRequest:Create        |                                |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | VendorViewer_Signmedia         | Vendor:ReadAll               | [purchaseUnitName='Signmedia'] |
      | EmployeeViewer_Signmedia       | Employee:ReadAll             | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                  | Company:ReadAll              |                                |
      | DocumentOwnerViewer            | DocumentOwner:ReadAll        |                                |
      | POViewer_Signmedia             | PurchaseOrder:ReadAll        | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll        | [purchaseUnitName='Signmedia'] |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll       | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User                         | Permission                   |
      | Afaf                         | PaymentRequest:Create        |
      | Gehan.Ahmed.NoDocumentOwner  | DocumentOwner:ReadAll        |
      | Gehan.Ahmed.NoPurchaseUnits  | PurchasingUnit:ReadAll_ForPO |
      | Gehan.Ahmed.NoVendor         | Vendor:ReadAll               |
      | Gehan.Ahmed.NoEmployee       | Employee:ReadAll             |
      | Gehan.Ahmed.NoCompany        | Company:ReadAll              |
      | Gehan.Ahmed.NoPurchaseOrder  | PurchaseOrder:ReadAll        |
      | Gehan.Ahmed.NoInvoice        | VendorInvoice:ReadAll        |
      | Gehan.Ahmed.NoAccountingNote | AccountingNote:ReadAll       |

    And the following users have the following permissions without the following conditions:
      | User                        | Permission                   | Condition                      |
      | Gehan.Ahmed.NoPurchaseUnits | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | Gehan.Ahmed.NoVendor        | Vendor:ReadAll               | [purchaseUnitName='Signmedia'] |
      | Gehan.Ahmed                 | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Flexo']     |
      | Gehan.Ahmed                 | Vendor:ReadAll               | [purchaseUnitName='Flexo']     |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | VendorInvoice  | CanBeDocumentOwner |           |

    #    @INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000105 | IMPORT_GOODS_INVOICE | Posted | Amr.Khalil      | 24-Apr-2019 8:31 AM | Ashraf Salah  |
#    @INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000105 | 0001 - Flexo     | 0002 - DigiPro |
        #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000002 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000003 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Ashraf Salah  |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000002 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000003 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000002 | 000002 - Zhejiang |             |
      | 2018000003 | 000001 - Siegwerk |             |

    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner   | Remaining |
      | 2021000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Active | Admin     | 10-Aug-2021 11:00 AM | Shady.Abdelatif | 4040.000  |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000008 | 0002 - Signmedia | 0001 - AL Madina |
     #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2021000008 | 000002 - Zhejiang | 5000   | 0001 - EGP |

    # EBS-4923
  Scenario: (01) Create Payment by an unauthorized user (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates PaymentRequest with the following values:
      | PaymentType | PaymentForm | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument |
      | VENDOR      | BANK        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

    # EBS-4923
  Scenario Outline: (02) Create Payment by an authorized user with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType   | PaymentForm | BusinessUnit   | BusinessPartner   | RefDocumentType   | RefDocument   | Company   | DocumentOwner |
      | <PaymentType> | CASH        | <BusinessUnit> | <BusinessPartner> | <RefDocumentType> | <RefDocument> | <Company> | 34            |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                         | PaymentType              | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument | Company          |
      | Gehan.Ahmed                  | OTHER_PARTY_FOR_PURCHASE | 0001 - Flexo     |                   | PURCHASEORDER   | 2021000003  |                  |
      | Gehan.Ahmed                  | VENDOR                   | 0002 - Signmedia | 000001 - Siegwerk | INVOICE         | 2019000105  |                  |
      | Gehan.Ahmed.NoPurchaseUnits  | OTHER_PARTY_FOR_PURCHASE | 0002 - Signmedia |                   | PURCHASEORDER   | 2021000002  |                  |
      | Gehan.Ahmed.NoVendor         | VENDOR                   | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  |                  |
      | Gehan.Ahmed.NoDocumentOwner  | VENDOR                   | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  |                  |
      | Gehan.Ahmed.NoInvoice        | VENDOR                   | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  |                  |
      | Gehan.Ahmed.NoPurchaseOrder  | OTHER_PARTY_FOR_PURCHASE | 0002 - Signmedia |                   | PURCHASEORDER   | 2021000002  |                  |
      | Gehan.Ahmed.NoAccountingNote | VENDOR                   | 0002 - Signmedia | 000002 - Zhejiang | CREDITNOTE      | 2021000008  |                  |
      | Gehan.Ahmed.NoCompany        | UNDER_SETTLEMENT         | 0002 - Signmedia | 000001 - Afaf     |                 |             | 0001 - AL Madina |
      | Gehan.Ahmed.NoEmployee       | UNDER_SETTLEMENT         | 0002 - Signmedia | 000001 - Afaf     |                 |             | 0001 - AL Madina |

    # EBS-4923
  Scenario: (03) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoPurchaseUnits"
    When "Gehan.Ahmed.NoPurchaseUnits" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoPurchaseUnits":
      | AuthorizedReads     |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-4923
  Scenario: (04) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoVendor"
    When "Gehan.Ahmed.NoVendor" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoVendor":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-8805
  Scenario: (05) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoDocumentOwner"
    When "Gehan.Ahmed.NoDocumentOwner" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoDocumentOwner":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |


    # EBS-4923
  Scenario: (06) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-8749
  Scenario: (07) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoEmployee"
    When "Gehan.Ahmed.NoEmployee" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoEmployee":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-8749
  Scenario: (08) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoCompany"
    When "Gehan.Ahmed.NoCompany" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoCompany":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-8749
  Scenario: (09) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoPurchaseOrder"
    When "Gehan.Ahmed.NoPurchaseOrder" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoPurchaseOrder":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadVendorInvoices  |
      | ReadAccountingNotes |

    # EBS-8749
  Scenario: (10) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoInvoice"
    When "Gehan.Ahmed.NoInvoice" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoInvoice":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadAccountingNotes |

    # EBS-8749
  Scenario: (11) Request Create Payment by an authorized user with authorized reads
    Given user is logged in as "Gehan.Ahmed.NoAccountingNote"
    When "Gehan.Ahmed.NoAccountingNote" requests to create PaymentRequest
    Then the following authorized reads are returned to "Gehan.Ahmed.NoAccountingNote":
      | AuthorizedReads     |
      | ReadPurchasingUnits |
      | ReadVendors         |
      | ReadDocumentOwners  |
      | ReadCompanies       |
      | ReadEmployees       |
      | ReadPurchaseOrders  |
      | ReadVendorInvoices  |

    # EBS-4923
  Scenario: (12) Request Create Payment by unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create PaymentRequest
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page