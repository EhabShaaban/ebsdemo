# Author Dev: Ibrahim Khidr, Zyad Ghorab, Waseem Salama, Yara Ameen
# Author Quality: Eslam Ayman, Khadrah Ali
# Reviewer: Somaya Ahmed (31-Mar-2021)

  #TODO: Rename "PaymentRequest" to "Payment" (EBS-8562)
  #TODO: Rename "PaymentDone" to "Active" (EBS-8562)
  #TODO: Rename subrole "PaymentResponsible" to "PaymentActivator" (EBS-8562)
  #TODO: Add to the givens the COA Configuration for Banks, Treasuries, Vendors

Feature: Activate Payment (Against VendorInvoice) (HP)

  Activating a Payment (Against VendorInvoice) results in the following:
  - Payment state is changed to Active  (EBS-4867)
  - Payment Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration (EBS-8166)
  ---- GLSubAccounts are retrieved from the Payment document (EBS-8166)
  ---- GL records for Realized ExRate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Payment Currency is same as Vendor Invoice currency, but different from the Company Local Currency (Future)
  -------- when Payment Currency is different from both Vendor Invoice and Company Local Currencies
  - Journal Entries for Payment is created(EBS-5254)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8557)
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations) (EBS-4852)
  - Payment Activation Details are determined
  - Related Vendor Invoice Remaining is updated according to Payment amount (EBS-7266)
  - Related JournalBalances are decreased according to Payment amount (EBS-8457)
  - Realized Differences on Exchange will be calculated as below (EBS-3942)
  (Total current payment * Invoice exchange rate) - Total current payment * current exchange rate)
  if > 0 (Positive) ==> Debit the Vendor and Credit the Realized ExRate
  If negative, then Debit Realized ExRate and Credit Vendor

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2018 | 1-Jan-2018 12:00 AM | 31-Dec-2018 11:59 PM | Closed |
      | 0002 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
      | 0003 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Mar-2018 10:01 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2018 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2018 10:00 PM | Central Bank Of Egypt |

  Scenario: (01) Activate Payment Request of type VENDOR against VendorInvoice (Happy Path)
  """
  - PaymentForm is BANK
  - PaymentRequest netAmount is less than or equal VendorInvoice remaining amount
  - PaymentRequest netAmount is less than or equal specified BankAccount balance
  - VendorInvoice currencyPrice is less than current currencyPrice
  - Journal entry details will have two more records for difference in currency price
  - All mandatory field exist and correct
  - By an authorized user
  """
        #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00010002000200031516171819781 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 2500    |

     #@INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type             | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000039 | SHIPMENT_INVOICE | Posted | Shady.Abdelatif | 05-Jan-2019 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company        |
      | 2019000039 | 0001 - Flexo | 0002 - DigiPro |
      #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000039 | 2018000056    | 000001 - Siegwerk | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |
     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                      | Qty(OrderUnit)      | Qty(Base) | UnitPrice(Base) |
      | 2019000039 | 000056 - Shipment service | 130.00 Roll 2.20x50 | 130.00 M2 | 90.00           |
     #@INSERT
    And the following VendorInvoices Taxes exist:
      | Code       | Tax                 | TaxPercentage | TaxAmount |
      | 2019000039 | 0013 - Shipping tax | 10            | 1170.00   |
      | 2019000039 | 0010 - Service tax  | 2             | 234.00    |
     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000039 | 00.00       | 13104.00       |
     #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice        | JournalEntryCode |
      | 2019000039 | Shady.Abdelatif | 06-Jan-2019 11:00 AM | 1.00 USD = 16.50 EGP | 2020000024       |
     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2019000005 | VENDOR      | Draft | Amr.Khalil | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000005 | 0001 - Flexo | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                 |
      | 2019000005 | BANK        | 000001 - Siegwerk | INVOICE         | 2019000039  | 500       | USD         |                | 1516171819781 - USD - Alex Bank |          | Installment No 2 in invoice |
  #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000056 | 48000     |
    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "BANK" Against "INVOICE" with code "2019000005" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 07-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000005" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000005" is updated as follows:
      | AccountType | GLAccount               | GLSubAccount                    | Amount |
      | DEBIT       | 10201 - Service Vendors | 000001 - Siegwerk               | 500    |
      | CREDIT      | 10431 - Bank            | 1516171819781 - USD - Alex Bank | 500    |
    And ActivationDetails in PaymentRequest with Code "2019000005" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 07-Jan-2019 12:00 AM | 17.44         | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 07-Jan-2019 12:00 AM | 0001 - Flexo | 2019       | 2019000005 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount                                | GLSubAccount                    | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10431 - Bank                             | 1516171819781 - USD - Alex Bank | 500    | 0     | USD                | EGP             | 17.44                     | 2018000026       |
      | 10201 - Service Vendors                  | 000001 - Siegwerk               | 0      | 500   | USD                | EGP             | 17.44                     | 2018000026       |
      | 41101 - Realized Differences on Exchange |                                 | 0      | 470   | EGP                | EGP             | 1                         |                  |
      | 10201 - Service Vendors                  | 000001 - Siegwerk               | 470    | 0     | EGP                | EGP             | 1                         |                  |
    And Remaining of VendorInvoice with code "2019000039" is updated as follows "12604"
    And JournalBalance with code "BANK00010002000200031516171819781" is updated as follows "2000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  Scenario: (02) Activate Payment Request of type VENDOR against VendorInvoice (Happy Path)
  """
  - PaymentForm is CASH
  - PaymentRequest netAmount is less than or equal VendorInvoice remaining amount
  - PaymentRequest netAmount is less than or equal specified Treasury balance
  - VendorInvoice currencyPrice is same as current currencyPrice
  - All mandatory fields exist and correct
  - By an authorized user
  """
        #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company        | Currency   | Treasury                  | Balance |
      | TREASURY0002000200020002 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 0002 - Signmedia Treasury | 2500    |

         #@INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                | State  | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000108 | LOCAL_GOODS_INVOICE | Posted | Gehan.Ahmed | 05-Jan-2019 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000108 | 0002 - Signmedia | 0002 - DigiPro |
      #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000108 | 2018000044    | 000011 - Vendor 5 | 20% advance, 80% Copy of B/L | 1200120301023500 | 24-Apr-2018 06:31 AM | USD      |
     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit)     | Qty(Base) | UnitPrice(Base) |
      | 2019000108 | 000001 - Hot Laminated Frontlit Fabric roll | 06.00 Roll 2.20x50 | 06.00 M2  | 8800.00         |

     #@INSERT
    And the following VendorInvoices Taxes exist:
      | Code       | Tax                      | TaxPercentage | TaxAmount |
      | 2019000108 | 0003 - Real estate taxes | 10            | 5280.00   |
      | 2019000108 | 0001 - Vat               | 1             | 528.00    |

     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000108 | 00.00       | 59136.00       |

     #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy | ActivationDate       | CurrencyPrice        | JournalEntryCode |
      | 2019000108 | Amr.Khalil  | 06-Aug-2019 09:02 AM | 1.00 USD = 17.44 EGP | 2019000027       |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000038 | VENDOR      | Draft | Gehan.Ahmed | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000038 | 0002 - Signmedia | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury                  | Description                 |
      | 2019000038 | CASH        | 000002 - Zhejiang | INVOICE         | 2019000108  | 300       | USD         |                |                   | 0002 - Signmedia Treasury | Installment No 2 in invoice |
  #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000044 | 48000     |
    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "CASH" Against "INVOICE" with code "2019000038" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 07-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000038" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000038" is updated as follows:
      | AccountType | GLAccount                 | GLSubAccount              | Amount |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang         | 300    |
      | CREDIT      | 10430 - Cash              | 0002 - Signmedia Treasury | 300    |
    And ActivationDetails in PaymentRequest with Code "2019000038" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 07-Jan-2019 12:00 AM | 17.44         | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 07-Jan-2019 12:00 AM | 0002 - Signmedia | 2019       | 2019000038 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount                 | GLSubAccount              | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10430 - Cash              | 0002 - Signmedia Treasury | 300    | 0     | USD                | EGP             | 17.44                     | 2018000026       |
      | 10221 - service vendors 2 | 000002 - Zhejiang         | 0      | 300   | USD                | EGP             | 17.44                     | 2018000026       |
    And Remaining of VendorInvoice with code "2019000108" is updated as follows "58836"
    And JournalBalance with code "TREASURY0002000200020002" is updated as follows "2200"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  Scenario: (03) Activate Payment Request of type VENDOR against VendorInvoice (Happy Path)
  """
  - PaymentForm is BANK
  - PaymentRequest netAmount is less than or equal VendorInvoice remaining amount
  - PaymentRequest netAmount is less than or equal specified BankAccount balance
  - VendorInvoice currencyPrice is greater than current currencyPrice
  - Journal entry details will have two more records for diffrence in currency price
  - All mandatory field exist and correct
  - By an authorized user
  """
        #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00010002000200031516171819781 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 2500.00 |

     #@INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type             | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000039 | SHIPMENT_INVOICE | Posted | Shady.Abdelatif | 05-Jan-2019 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company        |
      | 2019000039 | 0001 - Flexo | 0002 - DigiPro |
      #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000039 | 2018000056    | 000001 - Siegwerk | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |
     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                      | Qty(OrderUnit)      | Qty(Base) | UnitPrice(Base) |
      | 2019000039 | 000056 - Shipment service | 130.00 Roll 2.20x50 | 130.00 M2 | 90.00           |
     #@INSERT
    And the following VendorInvoices Taxes exist:
      | Code       | Tax                 | TaxPercentage | TaxAmount |
      | 2019000039 | 0013 - Shipping tax | 10            | 1170.00   |
      | 2019000039 | 0010 - Service tax  | 2             | 234.00    |
     #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000039 | 00.00       | 13104.00       |
     #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice        | JournalEntryCode |
      | 2019000039 | Shady.Abdelatif | 06-Jan-2019 11:00 AM | 1.00 USD = 18.50 EGP | 2020000024       |
     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2019000005 | VENDOR      | Draft | Amr.Khalil | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000005 | 0001 - Flexo | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                 |
      | 2019000005 | BANK        | 000001 - Siegwerk | INVOICE         | 2019000039  | 500.00    | USD         |                | 1516171819781 - USD - Alex Bank |          | Installment No 2 in invoice |
  #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000056 | 48000     |
    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "BANK" Against "INVOICE" with code "2019000005" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 07-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000005" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000005" is updated as follows:
      | AccountType | GLAccount               | GLSubAccount                    | Amount |
      | DEBIT       | 10201 - Service Vendors | 000001 - Siegwerk               | 500.00 |
      | CREDIT      | 10431 - Bank            | 1516171819781 - USD - Alex Bank | 500.00 |
    And ActivationDetails in PaymentRequest with Code "2019000005" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 07-Jan-2019 12:00 AM | 17.44         | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 07-Jan-2019 12:00 AM | 0001 - Flexo | 2019       | 2019000005 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount                                | GLSubAccount                    | Credit | Debit  | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10431 - Bank                             | 1516171819781 - USD - Alex Bank | 500.00 | 0.00   | USD                | EGP             | 17.44                     | 2018000026       |
      | 10201 - Service Vendors                  | 000001 - Siegwerk               | 0.00   | 500.00 | USD                | EGP             | 17.44                     | 2018000026       |
      | 41101 - Realized Differences on Exchange |                                 | 530    | 0.00   | EGP                | EGP             | 1.00                      |                  |
      | 10201 - Service Vendors                  | 000001 - Siegwerk               | 0.00   | 530    | EGP                | EGP             | 1.00                      |                  |
    And Remaining of VendorInvoice with code "2019000039" is updated as follows "12604"
    And JournalBalance with code "BANK00010002000200031516171819781" is updated as follows "2000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
