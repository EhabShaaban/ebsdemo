# Author Dev: Yara Ameen
Feature: PaymentRequest DocumentOwner DropDown

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | Afaf        | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole             |
      | PurchasingResponsible_Signmedia | DocumentOwnerViewer |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                        | Condition |
      | DocumentOwnerViewer         | DocumentOwner:ReadAll             |           |
      | PaymentRequestDocumentOwner | PaymentRequest:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah    | PaymentRequest | CanBeDocumentOwner |           |
      | 40 | Shady.Abdelatif | PaymentRequest | CanBeDocumentOwner |           |

  #EBS-8805
  Scenario: (01) Read All DocumentOwners Dropdown in PaymentRequest creation by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all DocumentOwners for "PaymentRequest"
    Then the following DocumentOwners values will be presented to "Gehan.Ahmed":
      | Name            |
      | Ashraf Salah    |
      | Shady.Abdelatif |
    And total number of DocumentOwners returned to "Gehan.Ahmed" in a dropdown is equal to 2

   #EBS-8805
  Scenario: (02) Read All DocumentOwners in PaymentRequest creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "PaymentRequest"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
