# Author Dev: Yara Ameen
# Author Quality: Khadra Ali
# Reviewer: Eslam Ayman
Feature: PaymentRequest - View Treasuries Dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                        |
      | Amr.Khalil  | PurchasingResponsible_Flexo |
      | Ahmed.Hamdi | AdminTreasury               |
      | Afaf        | FrontDesk                   |
    And the following roles and sub-roles exist:
      | Role                        | Subrole       |
      | PurchasingResponsible_Flexo | CompanyViewer |
    And the following sub-roles and permissions exist:
      | Subrole       | Permission      | Condition |
      | CompanyViewer | Company:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission      |
      | Afaf | Company:ReadAll |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy  | CreationDate         | Remaining | DocumentOwner |
      | 2019000040 | VENDOR      | Draft | Amr.Khalil | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000040 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury              | Description                  |
      | 2019000040 | CASH        | 000001 - Siegwerk | PURCHASEORDER   | 2018000003  | 10        | USD         |                |                   | 0001 - Flexo Treasury | Downpayent to a confirmed PO |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |

  #EBS-7277
  Scenario: (01) Read all Treasuries by an authorized user
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to read all Treasuries of DueDocument with code "2018000003" and dueDocumentType "PURCHASEORDER"
    Then the following values of Treasuries are displayed to "Amr.Khalil":
      | Treasury                   |
      | 0006 - Corrugated Treasury |
      | 0005 - Textile Treasury    |
      | 0004 - Digital Treasury    |
      | 0003 - Offset Treasury     |
      | 0002 - Signmedia Treasury  |
      | 0001 - Flexo Treasury      |
    And total number of Treasuries returned to "Amr.Khalil" is equal to 6

  #EBS-5484
  Scenario: (02) Read all Treasuries by an authorized user for collection document by company code
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to read all Treasuries of Company with code "0001"
    Then the following values of Treasuries are displayed to "Ahmed.Hamdi":
      | Treasury                  |
      | 0004 - Digital Treasury   |
      | 0003 - Offset Treasury    |
      | 0002 - Signmedia Treasury |
      | 0001 - Flexo Treasury     |
    And total number of Treasuries returned to "Ahmed.Hamdi" is equal to 4

  #EBS-7277
  Scenario: (03) Read all Treasuries by unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Treasuries of DueDocument with code "2018000003" and dueDocumentType "PURCHASEORDER"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-5484
  Scenario: (04) Read all Treasuries by unauthorized user for collection document by company code
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Treasuries of Company with code "0001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
