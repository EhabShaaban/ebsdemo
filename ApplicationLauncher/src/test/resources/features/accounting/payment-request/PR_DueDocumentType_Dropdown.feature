# Author Dev: Mohamed Aboelnour
# Author Quality: Khadra Ali
# Author PO: Somaya Ahmed

Feature: PaymentRequest - View Due Document Types Dropdown

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia   |
      | PurchasingResponsible_Signmedia | CompanyViewer           |
      | SuperUser                       | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                   | Condition                      |
      | PurUnitReader_Signmedia | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia   | Vendor:ReadAll               | [purchaseUnitName='Signmedia'] |
      | CompanyViewer           | Company:ReadAll              |                                |

  #EBS-8740
  Scenario Outline: (01) Read all DueDocumentTypes per payment type by an authorized user
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read all DueDocumentTypes for PaymentRequest with type "<PaymentType>"
    Then the "<DueDocumentType>" values are displayed to "Gehan.Ahmed":

    Examples:
      | PaymentType              | DueDocumentType        |
      | OTHER_PARTY_FOR_PURCHASE | PURCHASEORDER          |
      | VENDOR                   | INVOICE, PURCHASEORDER |
      | OTHER_PARTY_FOR_PURCHASE | PURCHASEORDER          |
      | UNDER_SETTLEMENT         |                        |
      | BASED_ON_CREDIT_NOTE     | CREDITNOTE             |
