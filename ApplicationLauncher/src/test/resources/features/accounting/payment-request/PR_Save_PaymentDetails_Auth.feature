# Author Dev: Ahmed Ali
# Author Quality: Khadrah Ali
# Reviewer: Mohamed Aboelnour
Feature: Save Payment Request Details (Auth)

  Background:
    Given the following users and roles exist:
      | Name                        | Role                                                           |
      | Afaf                        | FrontDesk                                                      |
      | Gehan.Ahmed                 | PurchasingResponsible_Signmedia                                |
      | Gehan.Ahmed.NoCompany       | PurchasingResponsible_Signmedia_CannotViewCompanyRole          |
      | Gehan.Ahmed.EditWithoutRead | PurchasingResponsible_Signmedia_EditPaymentDetailsWithoutReads |
    And the following roles and sub-roles exist:
      | Role                                                           | Sub-role                       |
      | PurchasingResponsible_Signmedia                                | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia                                | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia                                | CompanyViewer                  |
      | PurchasingResponsible_Signmedia                                | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia                                | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | LimitedPaymentViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewCompanyRole          | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_EditPaymentDetailsWithoutReads | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_EditPaymentDetailsWithoutReads | LimitedPaymentViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                        | Condition                                                         |
      | PaymentOwner_Signmedia         | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | LimitedPaymentViewer_Signmedia | PaymentRequest:ReadPaymentDetails | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | CurrencyViewer                 | Currency:ReadAll                  |                                                                   |
      | CompanyViewer                  | Company:ReadAll                   |                                                                   |
      | POViewer_Signmedia             | PurchaseOrder:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | ItemOwner_Signmedia            | Item:ReadAll                      | [purchaseUnitName='Signmedia']                                    |
    And the following users doesn't have the following permissions:
      | User                        | Permission                        |
      | Afaf                        | PaymentRequest:EditPaymentDetails |
      | Gehan.Ahmed.NoCompany       | Company:ReadAll                   |
      | Gehan.Ahmed.EditWithoutRead | VendorInvoice:ReadAll             |
      | Gehan.Ahmed.EditWithoutRead | Currency:ReadAll                  |
      | Gehan.Ahmed.EditWithoutRead | Company:ReadAll                   |
      | Gehan.Ahmed.EditWithoutRead | PurchaseOrder:ReadAll             |
      | Gehan.Ahmed.EditWithoutRead | Item:ReadAll                      |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                        | Condition                                                     |
      | Gehan.Ahmed | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser']) |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy                   | CreationDate         | Remaining | DocumentOwner |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed                 | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000040 | VENDOR                   | Draft | Amr.Khalil                  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000051 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed.EditWithoutRead | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000052 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed.NoCompany       | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000040 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000051 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000052 | 0002 - Signmedia | 0002 - DigiPro |
 #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber         | Treasury              | Description                  |
      | 2019000004 | BANK        | 000001 - Siegwerk | PURCHASEORDER   | 2018000034  | 1000.00   | EUR         |                            | Alex Bank - 1516171819789 |                       | For PO No 2018000010         |
      | 2019000040 | CASH        | 000001 - Siegwerk | PURCHASEORDER   | 2018000003  | 10.00     | USD         |                            |                           | 0001 - Flexo Treasury | Downpayent to a confirmed PO |
      | 2019000051 | BANK        |                   | PURCHASEORDER   | 2018000010  | 250.00    | EGP         | 000014 - Insurance service | Alex Bank - 1516171819789 |                       | مصاريف بنكية                 |
      | 2019000052 | CASH        |                   | PURCHASEORDER   | 2018000010  | 1000.00   | USD         |                            |                           |                       | for save payment details hp  |

  #EBS-7560
  Scenario: (01) Request to edit Payment details section by unauthorized user (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to edit PaymentDetails section of PaymentRequest with code "2019000004"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7560
  Scenario: (02) Request to edit Payment details section by unauthorized user due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2019000040"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-7560
  Scenario: (03) Request to edit Payment details section by an authorized user, but without all authorized read (No authorized reads)
    Given user is logged in as "Gehan.Ahmed.EditWithoutRead"
    When "Gehan.Ahmed.EditWithoutRead" requests to edit PaymentDetails section of PaymentRequest with code "2019000051"
    Then PaymentDetails section of PaymentRequest with code "2019000051" becomes in edit mode and locked by "Gehan.Ahmed.EditWithoutRead"
    And there are no authorized reads returned to "Gehan.Ahmed.EditWithoutRead"

  #EBS-7560
  Scenario: (04) Request to edit Payment details section by an authorized user, but without authorized company read
    Given user is logged in as "Gehan.Ahmed.NoCompany"
    When "Gehan.Ahmed.NoCompany" requests to edit PaymentDetails section of PaymentRequest with code "2019000052"
    Then PaymentDetails section of PaymentRequest with code "2019000052" becomes in edit mode and locked by "Gehan.Ahmed.NoCompany"
    And the following authorized reads are returned to "Gehan.Ahmed.NoCompany":
      | AuthorizedReads |
      | ReadCurrency    |
      | ReadItems       |
  #EBS-8749
  Scenario: (05) Request to edit Payment details section by an authorized user
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2019000004"
    Then PaymentDetails section of PaymentRequest with code "2019000004" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadTreasuries         |
      | ReadCompanyBankDetails |
