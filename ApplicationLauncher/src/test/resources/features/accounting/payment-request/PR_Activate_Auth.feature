Feature: Activate Payment Request Authorization

  Background:
    Given the following users and roles exist:
      | Name         | Role                      |
      | Ashraf.Salah | Corporate_Accounting_Head |
    And the following users doesn't have the following permissions:
      | User         | Permission              |
      | Ashraf.Salah | PaymentRequest:Activate |

   #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy              | CreationDate         | DocumentOwner |
      | 2019000008 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed.NoCurrency | 05-Aug-2018 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000008 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 000001 | EGP              | CNY               | 0.43        | 05-Aug-2019 09:02 AM | User Daily Rate |

  Scenario: (01) Activate Payment Request with by an unauthorized user (Abuse Case)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "CASH" with code "2019000008" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then "Ashraf.Salah" is logged out
    And "Ashraf.Salah" is forwarded to the error page
