# Author Dev: Ibrahim Khidr, Waseem Salama, Zyad Ghorab, Evram Hany, Mohamed Aboelnour, Ahmed Ali, Yara Ameen
# Author Quality: Khadra Ali
Feature: Activate Payment Request Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 2018000001 | EGP              | CNY               | 0.43        | 05-Aug-2019 09:02 AM | User Daily Rate |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Sep-2019 10:00 PM | User Daily Rate |
      | 2018000002 | EGP              | EGP               | 01          | 19-Sep-2019 10:00 PM | User Daily Rate |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |


    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 1000    |
      | BANK00020002000100031516171819789 | 0002 - Signmedia | 0002 - DigiPro   | 0001 - EGP | 1516171819789 - EGP - Alex Bank | 1000    |

    And insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 1000    |

  Scenario Outline: (01) Activate Payment Request with missing or malicious input (Abuse case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2020000002 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2020000002 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2020000002 | 2018000010 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 000055 - Insurance service2 | 200.25 EGP     | 0 EGP       | 0 EGP      |

    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury | Description  |
      | 2019000004 | BANK        |                 | PURCHASEORDER   | 2018000010  | 150       | EGP         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |          | مصاريف بنكية |
   #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000010 | 88000     |

    When "Ahmed.Hamdi" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "BANK" with code "<PRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate   |
      | <DueDate> |
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

    Examples:
      | PRCode     | DueDate              |
      | 2019000004 |                      |
      | 2019000004 | ""                   |
      | 2019000004 | Ay7aga               |
      | 2019000004 | 08-Jan-2019 12:00 AM |

  Scenario: (02) Activate Payment Request of type VENDOR with amount greater than invoice remaining amount (Validation Failure)
     #@INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                                        | Qty(OrderUnit)   | Qty(Base) | UnitPrice(Base) |
      | 2019000104 | 000001 - Hot Laminated Frontlit Fabric roll | 100 Roll 2.70x50 | 100 M2    | 2               |
      | 2019000104 | 000001 - Hot Laminated Frontlit Fabric roll | 50 Roll 2.70x50  | 50 M2     | 3               |

    #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice     | JournalEntryCode |
      | 2019000104 | Shady.Abdelatif | 06-Aug-2019 09:02 AM | 1 USD = 17.44 EGP | 2019000026       |
      #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000104 | 150         | 272            |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000012 | VENDOR      | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2021000012 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                 |
      | 2021000012 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000104  | 273       | USD         |                | 1516171819789 - EGP - Alex Bank |          | for save payment details hp |

    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00020002000200031516171819789 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819789 - EGP - Alex Bank | 1000    |

    Given user is logged in as "Ahmed.Hamdi"
    And Remaining amount of Invoice with code "2019000104" is "272"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "BANK" Against "INVOICE" with code "2021000012" at "07-Jan-2021 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2021 12:00 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Pr-msg-07"

  Scenario: (03) Activate Payment Request of type VENDOR with amount greater than total purchase order amount (Validation Failure)
    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000010 | VENDOR      | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2021000010 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                 |
      | 2021000010 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000001  | 488010    | USD         |                | 1516171819789 - EGP - Alex Bank |          | for save payment details hp |

    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00020002000200031516171819789 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819789 - EGP - Alex Bank | 500000  |

  #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000001 | 488000    |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "BANK" Against "PURCHASEORDER" with code "2021000010" at "07-Jan-2021 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2021 12:00 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Pr-msg-06"

  Scenario: (04) Activate Payment Request that doesn't allow this action due to state - (Exception)

        #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2020000007 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit      | Company        |
      | 2020000007 | 0006 - Corrugated | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2020000007 | 2018000045 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                       | EstimatedValue | ActualValue | Difference |
      | 2020000007 | 000014 - Insurance service | 300 EGP        | 100 EGP     | 200 EGP    |

    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy  | CreationDate         | DocumentOwner |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company        |
      | 2019000003 | 0006 - Corrugated | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury | Description        |
      | 2019000003 | BANK        |                 | PURCHASEORDER   | 2018000045  | 100       | USD         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |          | downpayment for po |
      #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000045 | 10150     |

    Given user is logged in as "hr1"
    When "hr1" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "BANK" with code "2019000003" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then an error notification is sent to "hr1" with the following message "Gen-msg-32"

  #EBS-7277
  Scenario Outline: (05) Activate Payment Request with missing mandatory fields

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2020000002 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2020000002 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2020000002 | 2018000010 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 000055 - Insurance service2 | 200.25 EGP     | 0 EGP       | 0 EGP      |

    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy                   | CreationDate         | DocumentOwner |
      | 2019000008 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed.NoCurrency      | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000021 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000008 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000021 | 0002 - Signmedia |                |

    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury | Description                 |
      | 2019000008 | CASH        |                 | PURCHASEORDER   | 2018000010  | 1000      | USD         |                |                   |          | for save payment details hp |
      | 2019000021 | BANK        |                 | PURCHASEORDER   |             |           |             |                |                   |          |                             |
  #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000010 | 88000     |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "<Method>" with code "<PRCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-40"
    And the following fields "<MissingFields>" which sent to "Ahmed.Hamdi" are marked as missing

    Examples:
      | PRCode     | Method | MissingFields                                                                              |
      | 2019000008 | CASH   | treasuryCode, costFactorItemCode                                                           |
      | 2019000021 | BANK   | dueDocumentCode, description, costFactorItemCode, bankAccountCode, netAmount, currencyCode |

  #EBS-8169
  Scenario: (06) Activate Payment Request with amount greater than bank balance (Exception)
    #@INSERT
    Given the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2021000001 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Posted | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 6006.12   |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount  | Currency   |
      | 2021000001 | 000001 - Al Ahram | 6006.12 | 0001 - EGP |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType          | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000001 | BASED_ON_CREDIT_NOTE | Draft | Amr.Khalil | 01-Mar-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description          |
      | 2021000001 | BANK        | 000001 - Siegwerk | CREDITNOTE      | 2021000001  | 1500      | EGP         |                | 1234567893458 - EGP - Bank Misr |          | payment for EBS-8169 |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "BASED_ON_CREDIT_NOTE" of method "BANK" with code "2021000001" at "01-Mar-2021 09:30 AM" with the following values:
      | DueDate              |
      | 01-Mar-2021 12:00 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Pr-msg-08"

  #EBS-8169
  Scenario: (07) Activate Payment Request with amount greater than treasury balance (Exception)

    #@INSERT
    Given the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2021000001 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Posted | Admin     | 10-Aug-2019 11:00 AM | Ashraf Salah  | 6006.12   |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount  | Currency   |
      | 2021000001 | 000001 - Al Ahram | 6006.12 | 0001 - EGP |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType          | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000002 | BASED_ON_CREDIT_NOTE | Draft | Amr.Khalil | 01-Mar-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury                  | Description          |
      | 2021000002 | CASH        | 000002 - Zhejiang | CREDITNOTE      | 2021000001  | 1500      | EGP         |                |                   | 0002 - Signmedia Treasury | payment for EBS-8169 |

    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "BASED_ON_CREDIT_NOTE" of method "CASH" with code "2021000002" at "01-Mar-2021 09:30 AM" with the following values:
      | DueDate              |
      | 01-Mar-2021 12:00 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Pr-msg-08"

  #EBS-9411
  Scenario Outline: (08) Activate Payment Request of type GeneralForPO with not existing LandedCost (Exception)
     #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | Draft                 | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |
      | 2021000002 | LOCAL - Landed Cost For Local PO   | ActualValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2021000004 |                   | true        |
      | 2021000002 | 2021000006 |                   | true        |
     #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000001 | IMPORT_PO  | DeliveryComplete | Admin     | 1-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000002 | IMPORT_PO  | DeliveryComplete | Admin     | 2-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000003 | SERVICE_PO | Confirmed        | admin     | 3-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000004 | IMPORT_PO  | DeliveryComplete | Admin     | 2-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | LOCAL_PO   | Confirmed        | Admin     | 2-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000006 | SERVICE_PO | Confirmed        | admin     | 3-Aug-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000004 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
      | 2021000006 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2021000003 | 000002 - Zhejiang | 2021000002  |
      | 2021000006 | 000002 - Zhejiang | 2021000005  |
    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000001 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument   | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury | Description |
      | 2021000001 | BANK        |                 | PURCHASEORDER   | <DueDocument> | 150       | EGP         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |          | Bank Fees   |
    And there is no LandedCost in state "EstimatedValuesCompleted" for the following PurchaseOrders:
      | PurchaseOrder |
      | 2021000001    |
      | 2021000002    |
      | 2021000004    |
      | 2021000005    |
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "BANK" with code "2021000001" at "13-Aug-2021 09:02 AM" with the following values:
      | DueDate              |
      | 12-Aug-2021 09:02 AM |
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Pr-msg-09"

    Examples:
      | DueDocument |
      #ImportPO
      | 2021000001  |
      #ServicePO
      | 2021000003  |
      #ImportPO has LandedCost in state draft
      | 2021000004  |
      #ServicePO has LandedCost in state ActualValuesCompleted
      | 2021000006  |
