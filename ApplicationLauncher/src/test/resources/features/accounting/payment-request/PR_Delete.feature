# Author: Gehad Shady,  QA Reviwer: Karim Rostom, Reviewer: Somaya Ahmad
# Updated by: Gehad Shady, Reviewer: Somaya Ahmad
Feature: Delete Payment Request

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                |
      | PurchasingResponsible_Signmedia | PaymentOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole       |
    And the following sub-roles and permissions exist:
      | Subrole                | Permission            | Condition                                                         |
      | PaymentOwner_Signmedia | PaymentRequest:Delete | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | SuperUserSubRole       | *:*                   |                                                                   |
    And the following users have the following permissions without the following conditions:
      | User        | Permission            | Condition                      |
      | Amr.Khalil  | PaymentRequest:Delete | [purchaseUnitName='Signmedia'] |
      | Gehan.Ahmed | PaymentRequest:Delete | [purchaseUnitName='Flexo']     |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy                   | CreationDate         | Remaining | DocumentOwner |
      | 2019000010 | VENDOR                   | Draft       | Gehan.Ahmed                 | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000014 | VENDOR                   | PaymentDone | Gehan.Ahmed                 | 06-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000021 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000030 | VENDOR                   | Draft       | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
   #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000010 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000014 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000021 | 0002 - Signmedia |                |
      | 2019000030 | 0002 - Signmedia | 0002 - DigiPro |

      #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury | Description                 |
      | 2019000010 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000001  | 488010    | USD         |                | 1516171819789 - Alex Bank |          | for save payment details hp |

  # EBS-4321
  Scenario: (01) Delete PaymentRequests in draft state by an authorized user (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to delete PaymentRequests with code "2019000010"
    Then PaymentRequests with code "2019000010" is deleted from the system
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-12"

  #EBS-4321 and # EBS- 6227
  Scenario Outline: (02) Delete PaymentRequests in a state that doesn't allow delete (Exception Case)
    Given user is logged in as "hr1"
    When "hr1" requests to delete PaymentRequests with code "<PRCode>"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-13"

    Examples:
      | PRCode     | State       |
      | 2019000014 | PaymentDone |

  #EBS-4321
  Scenario: (03) Delete PaymentRequests in draft state, where PaymentRequests doesn't exist (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000010" successfully
    When "Gehan.Ahmed" requests to delete PaymentRequests with code "2019000010"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

  # EBS-4321
  Scenario Outline: (04) Delete PaymentRequests in draft state that is locked by another user (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And first "hr1" opens "<GISection>" section of PaymentRequests with code "2019000010" in edit mode
    When "Gehan.Ahmed" requests to delete PaymentRequests with code "2019000010"
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-14"

    Examples:
      | GISection      |
      | PaymentDetails |

  # EBS-4321
  Scenario Outline: (05) Delete PaymentRequests in draft state by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" requests to delete PaymentRequests with code "<PRCode>"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User        | PRCode     |
      | Gehan.Ahmed | 2019000021 |
      | Gehan.Ahmed | 2019000030 |
