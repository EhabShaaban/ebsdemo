# Author Dev: Ahmed Ali
# Author Quality: Eslam Salam
# Reviewer: Mohamed Aboelnour
Feature: PaymentRequest - View CostFactorItems Dropdown

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | hr1         | SuperUser                      |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole              |
      | LogisticsResponsible_Signmedia | ItemViewer_Signmedia |
      | SuperUser                      | SuperUserSubRole     |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission   | Condition                      |
      | ItemViewer_Signmedia | Item:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole     | *:*          |                                |
    And the following users have the following permissions without the following conditions:
      | User        | Permission   | Condition                  |
      | Gehan.Ahmed | Item:ReadAll | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User | Permission   |
      | Afaf | Item:ReadAll |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
      #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | Remaining | DocumentOwner |
      | 2019000002 | VENDOR                   | Draft | Amr.Khalil  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |
    And the following Service Items exist:
      | Code   | Type    | MarketName        | BusinessUnit | CostFactor |
      | 000014 | SERVICE | Insurance service | 0002         | true       |
      | 000055 | SERVICE | Insurance service | 0002         | true       |
      | 000056 | SERVICE | Shipment service  | 0001         | true       |
      | 000057 | SERVICE | Bank Fees         | 0002         | true       |

  #EBS-6496
  Scenario: (01) Read all CostFactorItems by an authorized user
    Given user is logged in as "hr1"
    When "hr1" request to read all CostFactorItems for PaymentRequest of code "2019000004"
    Then the following values of CostFactorItems are displayed to "hr1":
      | CostFactorItem    |
      | Bank Fees         |
      | Insurance service |
      | Insurance service |
    And total number of CostFactorItems returned to "hr1" is equal to 3

  #EBS-6496
  Scenario: (02) Read all CostFactorItems by an authorized user with condition
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" request to read all CostFactorItems for PaymentRequest of code "2019000004"
    Then the following values of CostFactorItems are displayed to "Gehan.Ahmed":
      | CostFactorItem    |
      | Bank Fees         |
      | Insurance service |
      | Insurance service |
    And total number of CostFactorItems returned to "Gehan.Ahmed" is equal to 3

  #EBS-6496
  Scenario: (03) Read all CostFactorItems by an authorized user with condition
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" request to read all CostFactorItems for PaymentRequest of code "2019000002"
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

  #EBS-6496
  Scenario: (04) Read all CostFactorItems by unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" request to read all CostFactorItems for PaymentRequest of code "2019000004"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
