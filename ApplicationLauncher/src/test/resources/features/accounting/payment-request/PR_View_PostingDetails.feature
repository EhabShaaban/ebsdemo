Feature: View PostingDetails section

  Background:
    Given the following users and roles exist:
      | Name        | Role                  |
      | hr1         | SuperUser             |
      | Ahmed.Hamdi | Accountant_Corrugated |
      | Ahmed.Hamed | Accountant_Offset     |
    And the following roles and sub-roles exist:
      | Role                  | Sub-role                              |
      | SuperUser             | SuperUserSubRole                      |
      | Accountant_Corrugated | PaymentAccountDeterminator_Corrugated |
      | Accountant_Offset     | PaymentAccountDeterminator_Offset     |
    And the following sub-roles and permissions exist:
      | Subrole                               | Permission                        | Condition                       |
      | SuperUserSubRole                      | *:*                               |                                 |
      | PaymentAccountDeterminator_Corrugated | PaymentRequest:ReadPostingDetails | [purchaseUnitName='Corrugated'] |
      | PaymentAccountDeterminator_Offset     | PaymentRequest:ReadPostingDetails | [purchaseUnitName='Offset']     |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                        | Condition                       |
      | Ahmed.Hamed | PaymentRequest:ReadPostingDetails | [purchaseUnitName='Corrugated'] |

   #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy  | CreationDate         | Remaining | DocumentOwner |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil | 05-Aug-2018 09:02 AM | 100.000   | Ashraf Salah  |

    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000003 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests PostingDetails exist:
      | PRCode     | Accountant | PostingDate          | CurrencyPrice           | JournalEntryCode |
      | 2019000003 | Amr.Khalil | 06-Aug-2019 09:02 AM | 1.0 EGP = 17.563893 CNY | 2020000028       |

  # EBS-4397
  Scenario: (01) View Payment PostingDetails section by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view PostingDetails section of PaymentRequest with code "2019000003"
    Then the following values of PostingDetails section for PaymentRequest with code "2019000003" are displayed to "Ahmed.Hamdi":
      | Accountant | PostingDate          | CurrencyPrice | JournalEntryCode |
      | Amr.Khalil | 06-Aug-2019 09:02 AM | 17.563893     | 2020000028       |

  # EBS-4397
  Scenario: (02) View Payment PostingDetails section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Ahmed.Hamed"
    When "Ahmed.Hamed" requests to view PostingDetails section of PaymentRequest with code "2019000003"
    Then "Ahmed.Hamed" is logged out
    And "Ahmed.Hamed" is forwarded to the error page
