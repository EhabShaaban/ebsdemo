# Author Dev: Hossam Hassan, Ibrahim Khidr, Waseem Salama, Zyad Ghorab, Gehad Shady, Evram Hany, Ahmed Ali, Yara Ameen
# Author Quality: Khadra Ali & Eslam Ayman
# Author PO: Somaya Abolwafaa
Feature: Save Payment Details section HappyPath in Payment Request

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
      | Amr.Khalil  | PurchasingResponsible_Flexo     |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | PaymentOwner_Signmedia        |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia            |
      | PurchasingResponsible_Signmedia | CurrencyViewer                |
      | PurchasingResponsible_Signmedia | CompanyViewer                 |
      | PurchasingResponsible_Flexo     | PaymentOwner_Flexo            |
      | PurchasingResponsible_Flexo     | POViewer_Flexo                |
      | PurchasingResponsible_Flexo     | CurrencyViewer                |
      | PurchasingResponsible_Flexo     | CompanyViewer                 |
      | SuperUser                       | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                        | Condition                                                         |
      | PaymentOwner_Signmedia        | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | PaymentOwner_Flexo            | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser'])     |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | POViewer_Signmedia            | PurchaseOrder:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | POViewer_Flexo                | PurchaseOrder:ReadAll             | [purchaseUnitName='Flexo']                                        |
      | CurrencyViewer                | Currency:ReadAll                  |                                                                   |
      | CompanyViewer                 | Company:ReadAll                   |                                                                   |
      | SuperUserSubRole              | *:*                               |                                                                   |

      #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000002 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    Given the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2018000002 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000002 | 000002 - Zhejiang |             |


    # @INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
#    @INSERT
    Given the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
 #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000104 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |


        #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000010 | VENDOR                   | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000021 | OTHER_PARTY_FOR_PURCHASE | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000029 | VENDOR                   | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000040 | VENDOR                   | Draft | Amr.Khalil  | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000001 | UNDER_SETTLEMENT         | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000003 | UNDER_SETTLEMENT         | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000010 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000021 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000029 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000040 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury              | Description                  |
      | 2019000004 | BANK        |                   | PURCHASEORDER   | 2018000002  | 250       | EGP         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |                       | مصاريف بنكية                 |
      | 2019000010 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000002  | 488010    | USD         |                            | 1516171819789 - EGP - Alex Bank |                       | for save payment details hp  |
      | 2019000021 | BANK        |                   | PURCHASEORDER   | 2018000002  |           |             |                            |                                 |                       |                              |
      | 2019000029 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000104  | 2000      | USD         |                            | 1516171819789 - EGP - Alex Bank |                       | for save payment details hp  |
      | 2019000040 | CASH        | 000001 - Siegwerk | PURCHASEORDER   | 2018000002  | 10        | USD         |                            |                                 | 0001 - Flexo Treasury | Downpayent to a confirmed PO |
      | 2021000001 | CASH        | 000001 - Siegwerk | NONE            |             |           | USD         |                            |                                 |                       |                              |
      | 2021000003 | BANK        | 000001 - Siegwerk | NONE            |             |           | USD         |                            |                                 |                       |                              |

    And the following Currencies exist:
      | Code | Name                 | ISO |
      | 0001 | Egyptian Pound       | EGP |
      | 0002 | United States Dollar | USD |

    And the following Service Items exist:
      | Code   | Type    | MarketName        | BusinessUnit | CostFactor |
      | 000014 | SERVICE | Insurance service | 0002         | true       |
      | 000057 | SERVICE | Bank Fees         | 0002         | true       |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0006 | Corrugated Treasury |

    And edit session is "30" minutes

  ##### Save Payment Details section (Happy Path) ##############################################################
  #EBS-7379, EBS-5057
  Scenario Outline: (01) Save PaymentDetails section within edit session by an authorized user for a draft paymentRequest bank payment for vendor (invoice)(Happy Path)
    Given user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "2019000029" is locked by "hr1" at "07-Jan-2019 09:10 AM"
    When "hr1" saves PaymentDetails section of PaymentRequest with Code "2019000029" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | Description   | CompanyBankId   |
      | <NetAmount> | <Description> | <CompanyBankId> |
    Then PaymentRequest of type "VENDOR" with Code "2019000029" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | DueDocument | NetAmount   | CurrencyISO | Bank   | Description   |
      | hr1           | 07-Jan-2019 09:30 AM | 2019000104  | <NetAmount> | USD         | <Bank> | <Description> |
    And the lock by "hr1" on PaymentDetails section of PaymentRequest with code "2019000029" is released
    And a success notification is sent to "hr1" with the following message "Gen-msg-04"
    Examples:
      | NetAmount   | Description         | CompanyBankId | Bank                            |
      | 2000.124596 | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP |
      | 2000        | changed for save hp |               |                                 |
      |             | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP |
      | 2000        |                     | 17            | Alex Bank - 1516171819789 - EGP |
      |             |                     |               |                                 |

  # EBS-7379, EBS-5057
  Scenario Outline: (02) Save PaymentDetails section within edit session by an authorized user for bank payment for vendor (downpayment)(Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "2019000010" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "2019000010" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | Description   | CompanyBankId   |
      | <NetAmount> | <Description> | <CompanyBankId> |
    Then PaymentRequest of type "VENDOR" with Code "2019000010" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | DueDocument | NetAmount   | CurrencyISO | Bank   | Description   |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 2018000002  | <NetAmount> | USD         | <Bank> | <Description> |
    And the lock by "Gehan.Ahmed" on PaymentDetails section of PaymentRequest with code "2019000010" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

    Examples:
      | NetAmount | Description         | CompanyBankId | Bank                                                     |
      | 2000      | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP                          |
      |           | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP                          |
      | 2000      | changed for save hp |               |                                                          |
      | 2000      |                     | 17            | Alex Bank - 1516171819789 - EGP                          |
      |           |                     |               |                                                          |
      | 2000      | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP                          |
      | 2000      | changed for save hp | 19            | Qatar National Bank Al Ahli-Tanta - 26272829303132 - EGP |

  #
  #EBS-7379, EBS-6496
  Scenario Outline: (03) Save PaymentDetails section within edit session by an authorized user for a draft paymentRequest bank general payment(Happy Path)
    Given user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "2019000021" is locked by "hr1" at "07-Jan-2019 09:10 AM"
    When "hr1" saves PaymentDetails section of PaymentRequest with Code "2019000021" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | CurrencyCode   | Description   | CompanyBankId   | CostFactorItem |
      | <NetAmount> | <CurrencyCode> | <Description> | <CompanyBankId> | <ItemCode>     |
    Then PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" with Code "2019000021" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | DueDocument | NetAmount   | CurrencyISO   | Bank   | Description   | CostFactorItem |
      | hr1           | 07-Jan-2019 09:30 AM | 2018000002  | <NetAmount> | <CurrencyISO> | <Bank> | <Description> | <ItemCode>     |
    And the lock by "hr1" on PaymentDetails section of PaymentRequest with code "2019000021" is released
    And a success notification is sent to "hr1" with the following message "Gen-msg-04"

    Examples:
      #Draft
      | NetAmount | CurrencyCode | CurrencyISO | Description         | CompanyBankId | Bank                            | ItemCode |
      | 2000      | 0002         | USD         | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP | 000057   |
      | 2000      |              |             | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP | 000057   |
      | 2000      |              |             | changed for save hp |               |                                 | 000057   |
      |           | 0001         | EGP         | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP | 000057   |
      | 2000      |              |             | changed for save hp |               |                                 | 000057   |
      | 2000      | 0002         | USD         |                     | 17            | Alex Bank - 1516171819789 - EGP | 000057   |
      | 2000      | 0001         | EGP         | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP |          |
      |           |              |             |                     |               |                                 |          |

  #EBS-6496
  Scenario: (04) Save PaymentDetails section within edit session by an authorized user for a draft paymentRequest Bank Fees(Happy Path)
    Given user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "2019000004" is locked by "hr1" at "07-Jan-2019 09:10 AM"
    When "hr1" saves PaymentDetails section of PaymentRequest with Code "2019000004" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount | CurrencyCode | CompanyBankId | CostFactorItem | Description  |
      | 250       | 0001         | 17            | 000057         | مصاريف بنكية |
    Then PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" with Code "2019000004" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | DueDocument | NetAmount | CurrencyISO | Bank                            | CostFactorItem | Description  |
      | hr1           | 07-Jan-2019 09:30 AM | 2018000002  | 250       | EGP         | Alex Bank - 1516171819789 - EGP | 000057         | مصاريف بنكية |
    And the lock by "hr1" on PaymentDetails section of PaymentRequest with code "2019000004" is released
    And a success notification is sent to "hr1" with the following message "Gen-msg-04"

  #EBS-7277, #EBS-7276
  Scenario: (05) Save PaymentDetails section within edit session by an authorized user for a draft paymentRequest cash payment (Happy Path)
    Given user is logged in as "Amr.Khalil"
    And PaymentDetails section of PaymentRequest with code "2019000040" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    When "Amr.Khalil" saves PaymentDetails section of PaymentRequest with Code "2019000040" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount | CurrencyCode | Treasury | CostFactorItem | Description      |
      | 700       | 0002         | 0006     |                | cash Downpayment |
    Then PaymentRequest of type "VENDOR" with Code "2019000040" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | DueDocument | NetAmount | CurrencyISO | CostFactorItem | Treasury                   | Description      |
      | Amr.Khalil    | 07-Jan-2019 09:30 AM | 2018000002  | 700       | USD         |                | 0006 - Corrugated Treasury | cash Downpayment |
    And the lock by "Amr.Khalil" on PaymentDetails section of PaymentRequest with code "2019000040" is released
    And a success notification is sent to "Amr.Khalil" with the following message "Gen-msg-04"

    #EBS-7980
  Scenario Outline: (06) Save PaymentDetails section within edit session by an authorized user for a draft Under Settlement paymentRequest bank payment (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "2021000003" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "2021000003" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | CurrencyCode   | Description   | CompanyBankId   |
      | <NetAmount> | <CurrencyCode> | <Description> | <CompanyBankId> |
    Then PaymentRequest of type "UNDER_SETTELMENT" with Code "2021000003" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | NetAmount   | CurrencyISO   | Bank   | Description   |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | <NetAmount> | <CurrencyISO> | <Bank> | <Description> |
    And the lock by "Gehan.Ahmed" on PaymentDetails section of PaymentRequest with code "2021000003" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"
    Examples:
      | NetAmount | CurrencyCode | CurrencyISO | Description         | CompanyBankId | Bank                            |
      | 2000      | 0002         | USD         | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP |
      | 2000      |              |             | changed for save hp |               |                                 |
      |           | 0002         | USD         | changed for save hp | 17            | Alex Bank - 1516171819789 - EGP |
      | 2000      | 0002         | USD         |                     | 17            | Alex Bank - 1516171819789 - EGP |
      |           |              |             |                     |               |                                 |

# #EBS-7980
  Scenario: (07) Save PaymentDetails section within edit session by an authorized user for a draft Under Settlement paymentRequest cash payment (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "2021000001" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "2021000001" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount | CurrencyCode | Treasury | Description           |
      | 700       | 0002         | 0006     | cash Under settlement |
    Then PaymentRequest of type "UNDER_SETTLEMENT" with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | NetAmount | CurrencyISO | Treasury                   | Description           |
      | Gehan.Ahmed   | 07-Jan-2019 09:30 AM | 700       | USD         | 0006 - Corrugated Treasury | cash Under settlement |
    And the lock by "Gehan.Ahmed" on PaymentDetails section of PaymentRequest with code "2021000001" is released
    And a success notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-04"

