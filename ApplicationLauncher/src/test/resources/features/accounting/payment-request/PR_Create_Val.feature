Feature: Create Payment Request - Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                        |
      | PurchasingResponsible_Signmedia | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia | CompanyViewer                  |
      | PurchasingResponsible_Signmedia | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia | AccountingNoteViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                   | Condition                      |
      | PaymentOwner_Signmedia         | PaymentRequest:Create        |                                |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia          | Vendor:ReadAll               | [purchaseUnitName='Signmedia'] |
      | EmployeeViewer_Signmedia       | Employee:ReadAll             | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                  | Company:ReadAll              |                                |
      | DocumentOwnerViewer            | DocumentOwner:ReadAll        |                                |
      | POViewer_Signmedia             | PurchaseOrder:ReadAll        | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll        | [purchaseUnitName='Signmedia'] |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll       | [purchaseUnitName='Signmedia'] |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | VendorInvoice  | CanBeDocumentOwner |           |
     #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000002 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000002 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
 #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000002 | 000002 - Zhejiang |             |
    # @INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |

#    @INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000104 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |

 #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner   | Remaining |
      | 2021000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Active | Admin     | 10-Aug-2021 11:00 AM | Shady.Abdelatif | 4040.000  |
#@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000008 | 0002 - Signmedia | 0001 - AL Madina |
     #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2021000008 | 000002 - Zhejiang | 5000   | 0001 - EGP |


  # EBS-4916
  Scenario Outline: (01) Create Payment with missing mandatory field (Abuse Case\Clientbypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates PaymentRequest with the following values:
      | PaymentType   | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType   | RefDocument   | DocumentOwner   |
      | <PaymentType> | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | <RefDocumentType> | <RefDocument> | <DocumentOwner> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | PaymentType              | PaymentForm | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument | DocumentOwner |
      |                          | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  | 34            |
      | ""                       | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  | 34            |
      | OTHER_PARTY_FOR_PURCHASE |             | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | ""          | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        |                  |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | ""               |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   |                 | 2019000104  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | ""              | 2019000104  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang |                 | 2019000104  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia |                   | INVOICE         | 2019000104  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | ""                | INVOICE         | 2019000104  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  |               |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  | ""            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         |             | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   |             | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   |             | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2019000104  | 34            |

  # EBS-4916
  Scenario Outline: (02) Create Payment Malicious input or entering disabled field (Abuse Case\Client bypassing)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates PaymentRequest with the following values:
      | PaymentType   | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType   | RefDocument   | DocumentOwner   |
      | <PaymentType> | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | <RefDocumentType> | <RefDocument> | <DocumentOwner> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | PaymentType              | PaymentForm | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument | DocumentOwner |
      | ay7aga                   | CASH        | 0002 - Signmedia |                   |                 |             | 34            |
      | OTHER_PARTY_FOR_PURCHASE | ay7aga      | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | ay7aga           |                   | PURCHASEORDER   | 2018000002  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | ay7aga            | INVOICE         | 2019000104  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | ay7aga          | 2019000104  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000104  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | INVOICE         | 2019000104  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000002  | ay7aga        |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000002  | 999999        |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 9999999999  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | ay7aga      | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 9999999999  | 34            |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | ay7aga      | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 9999             |                   | PURCHASEORDER   | 2018000002  | 34            |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia | 999999            | PURCHASEORDER   | 2018000002  | 34            |
  # EBS-8749
  Scenario Outline: (03) Create Payment With Incorrect DueDocument Reference (Validation Failure)

       #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State           | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000014 | IMPORT_PO | WaitingApproval | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2018000013 | IMPORT_PO | OpenForUpdates  | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2018000012 | IMPORT_PO | Cancelled       | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2018000011 | IMPORT_PO | OpenForUpdates  | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000014 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000013 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000012 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000011 | 0002 - Signmedia |                  |                                 |

 #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000014 | 000002 - Zhejiang |             |
      | 2018000013 | 000002 - Zhejiang |             |
      | 2018000012 | 000002 - Zhejiang |             |
      | 2018000011 | 000002 - Zhejiang |             |

    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "<PaymentRequestCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" creates PaymentRequest with the following values:
      | PaymentType   | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType   | RefDocument   | DocumentOwner |
      | <PaymentType> | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | <RefDocumentType> | <RefDocument> | 34            |
    Then a failure notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-05"
    And the following error message is attached to "dueDocumentCode" field "<ErrorMessage>" and sent to "Gehan.Ahmed"
    Examples:
      | PaymentType              | PaymentForm | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument | ErrorMessage |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000014  | Gen-msg-32   |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000013  | Gen-msg-32   |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000012  | Gen-msg-32   |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000011  | Inv-msg-02   |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000014  | Gen-msg-32   |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000013  | Gen-msg-32   |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000012  | Gen-msg-32   |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000011  | Inv-msg-02   |

  # EBS-8749
  Scenario Outline: (04) Create Payment With Incorrect DueDocument Reference (Abuse Case\Client bypassing)

       #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000015 | IMPORT_PO | Draft            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2018000016 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2018000017 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2018000015 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000016 | 0001 - Flexo     | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2018000017 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

 #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000015 | 000002 - Zhejiang |             |
      | 2018000016 | 000002 - Zhejiang |             |
      | 2018000017 | 000001 - Siegwerk |             |

    # @INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000105 | IMPORT_GOODS_INVOICE | Draft  | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000106 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
      | 2019000107 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |

#    @INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000105 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000106 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000107 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000105 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |
      | 2019000106 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |
      | 2019000107 | 2018000002    | 000001 - Siegwerk | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |

    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" creates PaymentRequest with the following values:
      | PaymentType   | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType   | RefDocument   | DocumentOwner |
      | <PaymentType> | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | <RefDocumentType> | <RefDocument> | 34            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
    Examples:
      | PaymentType              | PaymentForm | BusinessUnit     | BusinessPartner   | RefDocumentType | RefDocument |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000105  |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000106  |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | INVOICE         | 2019000107  |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000015  |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000016  |
      | VENDOR                   | CASH        | 0002 - Signmedia | 000002 - Zhejiang | PURCHASEORDER   | 2018000017  |
      | OTHER_PARTY_FOR_PURCHASE | CASH        | 0002 - Signmedia |                   | PURCHASEORDER   | 2018000016  |
