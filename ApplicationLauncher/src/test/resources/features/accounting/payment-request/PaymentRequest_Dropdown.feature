Feature: Payment Request Dropdown
"""
This feature file is for PaymentRequest drop-downs in:
  1. Create Collection
"""

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Afaf            | FrontDesk            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | Accountant_Flexo     |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                 |
      | Accountant_Signmedia | PaymentViewer_Signmedia |
      | Accountant_Flexo     | PaymentViewer_Flexo     |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission             | Condition                      |
      | PaymentViewer_Signmedia | PaymentRequest:ReadAll | [purchaseUnitName='Signmedia'] |
      | PaymentViewer_Flexo     | PaymentRequest:ReadAll | [purchaseUnitName='Flexo']     |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | PaymentRequest:ReadAll |
    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000009 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit | Company          |
      | 2019000009 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2019000009 | 000001 - Siegwerk | 1000   | 0001 - EGP |
    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType          | State       | CreatedBy    | CreationDate         | Remaining | DocumentOwner |
      | 2021000001 | BASED_ON_CREDIT_NOTE | PaymentDone | Amr.Khalil   | 05-Aug-2018 09:02 AM | 500       | Ashraf Salah  |
      | 2021000002 | UNDER_SETTLEMENT     | Draft       | Ashraf.Salah | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000003 | UNDER_SETTLEMENT     | PaymentDone | Ashraf.Salah | 04-Jan-2021 09:02 AM | 0         | Ashraf Salah  |
      | 2021000004 | UNDER_SETTLEMENT     | PaymentDone | Ashraf.Salah | 04-Jan-2021 09:02 AM | 500       | Ashraf Salah  |
      | 2021000005 | UNDER_SETTLEMENT     | PaymentDone | Ashraf.Salah | 04-Jan-2021 09:02 AM | 500       | Ashraf Salah  |
      | 2021000006 | UNDER_SETTLEMENT     | PaymentDone | Ashraf.Salah | 04-Jan-2021 09:02 AM | 700       | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000004 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000005 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000006 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber         | Treasury              | Description               |
      | 2021000001 | CASH        | 000001 - Siegwerk | CREDITNOTE      | 2019000009  | 1000      | EGP         |                | Alex Bank - 1516171819789 |                       | payment under credit note |
      | 2021000002 | CASH        | 000001 - Afaf     |                 |             | 1000      | USD         |                |                           | 0001 - Flexo Treasury | Under settlement          |
      | 2021000003 | CASH        | 000001 - Afaf     |                 |             | 1000      | EGP         |                | Alex Bank - 1516171819789 |                       | Under settlement          |
      | 2021000004 | CASH        | 000001 - Afaf     |                 |             | 1000      | EGP         |                | Alex Bank - 1516171819789 |                       | Under settlement          |
      | 2021000005 | CASH        | 000001 - Afaf     |                 |             | 1000      | EGP         |                | Alex Bank - 1516171819789 |                       | Under settlement          |
      | 2021000006 | CASH        | 000001 - Afaf     |                 |             | 1000      | EGP         |                | Alex Bank - 1516171819789 |                       | Under settlement          |

  ####################################### CREATE COLLECTION ############################################
  Scenario: (01) Read all PaymentRequests for create Collection (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all PaymentDone PaymentRequests with remaining greater than zero and according to user’s BusinessUnit
    Then the following PaymentRequests will be presented to "Shady.Abdelatif" in a dropdown:
      | Code       | State       | Type             | BusinessUnit     | Remaining |
      | 2021000006 | PaymentDone | UNDER_SETTLEMENT | 0002 - Signmedia | 700       |
      | 2021000005 | PaymentDone | UNDER_SETTLEMENT | 0002 - Signmedia | 500       |
    And the total number of records returned to "Shady.Abdelatif" are 2

  Scenario: (02) Read all PaymentRequests for create Collection (Happy Path)
  """
    Read all PaymentRequests with the following conditions:
      1. In PaymentDone state
      2. Has remaining > 0
      3. Matches logged-in user's business unit
  """
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all PaymentRequests for create Collection
    Then the following PaymentRequests will be presented to "Shady.Abdelatif" in a dropdown:
      | Code       | State       | Type             | BusinessUnit     | Remaining |
      | 2021000006 | PaymentDone | UNDER_SETTLEMENT | 0002 - Signmedia | 700       |
      | 2021000005 | PaymentDone | UNDER_SETTLEMENT | 0002 - Signmedia | 500       |
    And the total number of records returned to "Shady.Abdelatif" are 2

  Scenario: (03) Read all PaymentRequests for create Collection - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all PaymentRequests for create Collection
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
