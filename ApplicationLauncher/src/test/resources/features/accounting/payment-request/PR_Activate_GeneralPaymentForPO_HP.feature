# Author Dev: Ibrahim Khidr, Zyad Ghorab, Waseem Salama, Yara Ameen, Mohamed Aboelnour
# Author Quality: Eslam Ayman, Khadrah Ali, Ehab Shaban

  #TODO: Rename "PaymentRequest" to "Payment" (EBS-8562)
  #TODO: Rename "PaymentDone" to "Active" (EBS-8562)
  #TODO: Rename subrole "PaymentResponsible" to "PaymentActivator" (EBS-8562)
  #TODO: Change PO in JournalEntryDetails assertion from 2020000006 to 2018000056 (not developed yet)
  #TODO: Add to the givens the COA Configuration for Banks, Treasuries, Vendors

Feature: Activate Payment (General Payment For PO) (HP)
  Activating a Payment (Against VendorInvoice) results in the following:
  - Payment state is changed to Active  (EBS-4867)
  - Payment Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration (EBS-8166)
  ---- GLSubAccounts are retrieved from the Payment document (EBS-8166)
  ---- GL records for Realized ExRate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Payment Currency is same as Vendor Invoice currency, but different from the Company Local Currency (Future)
  -------- when Payment Currency is different from both Vendor Invoice and Company Local Currencies
  - Journal Entries for Payment is created(EBS-5254)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8557)
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations) (EBS-4852)
  - Payment Activation Details are determined
  - Related JournalBalances are decreased according to Payment amount (EBS-8457)
  - Add Actual Cost to LandedCost (EBS-4888)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2018 | 1-Jan-2018 12:00 AM | 31-Dec-2018 11:59 PM | Closed |
      | 0002 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
      | 0003 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.50       | 19-Mar-2018 10:01 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Mar-2018 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.44       | 19-Mar-2018 10:00 PM | Central Bank Of Egypt |

  Scenario: (01) Activate Payment Request of type GENERAL Payment For PurchaseOrder (Happy Path)
  """
  - PaymentRequest netAmount is less than or equal PurchaseOrder amount
  - PaymentForm is BANK
  - All mandatory fields
  - By an authorized user
  - Update landed cost for cost factor item (actual += payment amount)
  """
     #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00010002000200031516171819781 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 5000000 |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State     | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000006 | SERVICE_PO | Confirmed | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |

    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit | Company        | BankAccountNumber               |
      | 2020000006 | 0001 - Flexo | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000006 | 000001 - Siegwerk | 2018000056  |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit | Company        |
      | 2021000001 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2018000056 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2021000001 | 000055 - Insurance service2 | 1000 EGP       | 0 EGP       | 1000 EGP   |
      | 2021000001 | 000057 - Bank Fees          | 490000 EGP     | 0 EGP       | 490000 EGP |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000009 | OTHER_PARTY_FOR_PURCHASE | Draft | Ahmed.Hamdi | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000009 | 0001 - Flexo | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem     | BankAccountNumber               | Treasury | Description                 |
      | 2019000009 | BANK        |                 | PURCHASEORDER   | 2020000006  | 490000    | USD         | 000057 - Bank Fees | 1516171819781 - USD - Alex Bank |          | for save payment details hp |

    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "BANK" with code "2019000009" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000009" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000009" is updated as follows:
      | AccountType | GLAccount    | GLSubAccount                    | Amount |
      | DEBIT       | 10207 - PO   | 2018000056                      | 1000   |
      | CREDIT      | 10431 - Bank | 1516171819781 - USD - Alex Bank | 1000   |
    And ActivationDetails in PaymentRequest with Code "2019000009" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.44         | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 06-Jan-2019 12:00 AM | 0001 - Flexo | 2019       | 2019000009 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount    | GLSubAccount                    | Credit | Debit  | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10431 - Bank | 1516171819781 - USD - Alex Bank | 490000 | 0      | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO   | 2020000006                      | 0      | 490000 | USD                | EGP             | 17.44                     | 2018000026       |
    And CostFactorItem "000057 - Bank Fees" in LandedCost "2021000001" updated as follows:
      | EstimatedValue | ActualValue | ReferenceDocument    |
      | 490000         | 490000      | Payment - 2019000009 |
    And JournalBalance with code "BANK00010002000200031516171819781" is updated as follows "4510000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  Scenario: (02) Activate Payment Request of type GENERAL Payment For PurchaseOrder (Happy Path)
  """
  - PaymentRequest netAmount is less than or equal PurchaseOrder amount
  - PaymentForm is CASH
  - All mandatory fields
  - By an authorized user
  - Insert new row in landed cost with cost factor item (estimated = 0, actual = payment amount)
  """
        #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit | Company        | Currency   | Treasury              | Balance |
      | TREASURY0001000200020001 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 0001 - Flexo Treasury | 5000000 |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit | Company        |
      | 2021000001 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2018000036 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item               | EstimatedValue | ActualValue | Difference |
      | 2021000001 | 000057 - Bank Fees | 1000 EGP       | 0 EGP       | 1000 EGP   |

    #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000036 | 48000     |
     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000016 | OTHER_PARTY_FOR_PURCHASE | Draft | Ahmed.Hamdi | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000016 | 0001 - Flexo | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem              | BankAccountNumber | Treasury              | Description  |
      | 2019000016 | CASH        |                 | PURCHASEORDER   | 2018000036  | 490000    | USD         | 000055 - Insurance service2 |                   | 0001 - Flexo Treasury | مصاريف بنكية |

    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "OTHER_PARTY_FOR_PURCHASE" of method "CASH" with code "2019000016" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000016" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000016" is updated as follows:
      | AccountType | GLAccount    | GLSubAccount          | Amount |
      | DEBIT       | 10207 - PO   | 2018000036            | 1000   |
      | CREDIT      | 10430 - Cash | 0001 - Flexo Treasury | 1000   |
    And ActivationDetails in PaymentRequest with Code "2019000016" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.44         | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 06-Jan-2019 12:00 AM | 0001 - Flexo | 2019       | 2019000016 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount    | GLSubAccount          | Credit | Debit  | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10430 - Cash | 0001 - Flexo Treasury | 490000 | 0      | USD                | EGP             | 17.44                     | 2018000026       |
      | 10207 - PO   | 2018000036            | 0      | 490000 | USD                | EGP             | 17.44                     | 2018000026       |
    And CostFactorItem "000055 - Insurance service2" in LandedCost "2021000001" updated as follows:
      | EstimatedValue | ActualValue | ReferenceDocument    |
      | 0              | 490000      | Payment - 2019000016 |
    And JournalBalance with code "TREASURY0001000200020001" is updated as follows "4510000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
