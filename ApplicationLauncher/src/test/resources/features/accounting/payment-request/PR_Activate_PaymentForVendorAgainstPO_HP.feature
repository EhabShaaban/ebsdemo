# Author Dev: Ibrahim Khidr, Zyad Ghorab, Waseem Salama, Yara Ameen
# Author Quality: Eslam Ayman, Khadrah Ali
# Reviewer: Somaya Ahmed (31-Mar-2021)

  #TODO: Rename "PaymentRequest" to "Payment" (EBS-8562)
  #TODO: Rename "PaymentDone" to "Active" (EBS-8562)
  #TODO: Rename subrole "PaymentResponsible" to "PaymentActivator" (EBS-8562)
  #TODO: Add to the givens the COA Configuration for Banks, Treasuries, Vendors

Feature: Activate Payment (Against PurchaseOrder) (HP)
  Activating a Payment (Against VendorInvoice) results in the following:
  - Payment state is changed to Active  (EBS-4867)
  - Payment Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration (EBS-8559)
  ---- GLSubAccounts are retrieved from the Payment document (EBS-8559)
  ---- GL records for Realized ExRate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Payment Currency is same as Vendor Invoice currency, but different from the Company Local Currency (Future)
  -------- when Payment Currency is different from both Vendor Invoice and Company Local Currencies
  - Journal Entries for Payment is created(EBS-5254)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8557)
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations) (EBS-4852)
  - Payment Activation Details are determined
  - Related Purchase Order Remaining is updated according to Payment amount (EBS-8553)
  - Related JournalBalances are decreased according to Payment amount (EBS-8457)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2018 | 1-Jan-2018 12:00 AM | 31-Dec-2018 11:59 PM | Closed |
      | 0002 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
      | 0003 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2018000012 | USD              | EGP               | 16.578362   | 19-Mar-2018 10:01 PM | User Daily Rate       |
      | 2018000026 | USD              | EGP               | 17.126738   | 19-Mar-2018 10:00 PM | User Daily Rate       |
      | 2018000037 | USD              | EGP               | 15.754383   | 19-Mar-2018 10:00 PM | Central Bank Of Egypt |


  Scenario: (01) Activate Payment Request of type VENDOR against DownPayment (Happy Path)
  """
   - PaymentForm is BANK
   - PaymentRequest netAmount is less than or equal PurchaseOrder remaining amount
   - PaymentRequest netAmount is less than or equal specified BANKACCOUNT balance
   - All mandatory fields
   - By an authorized user
  """

    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00020002000200031516171819781 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 2500    |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy                   | CreationDate         | DocumentOwner |
      | 2019000030 | VENDOR      | Draft | Gehan.Ahmed.NoPurchaseOrder | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000030 | 0002 - Signmedia | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber               | Treasury | Description                 |
      | 2019000030 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000001  | 1000      | USD         |                | 1516171819781 - USD - Alex Bank |          | for save payment details hp |
     #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000001 | 488000    |

    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "BANK" Against "PURCHASEORDER" with code "2019000030" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000030" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000030" is updated as follows:
      | AccountType | GLAccount                 | GLSubAccount                    | Amount |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000   |
      | CREDIT      | 10431 - Bank              | 1516171819781 - USD - Alex Bank | 1000   |
    And ActivationDetails in PaymentRequest with Code "2019000030" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.126738     | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 06-Jan-2019 12:00 AM | 0002 - Signmedia | 2019       | 2019000030 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount                 | GLSubAccount                    | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10431 - Bank              | 1516171819781 - USD - Alex Bank | 1000   | 0     | USD                | EGP             | 17.126738                 | 2018000026       |
      | 10221 - service vendors 2 | 000002 - Zhejiang               | 0      | 1000  | USD                | EGP             | 17.126738                 | 2018000026       |
    And JournalBalance with code "BANK00020002000200031516171819781" is updated as follows "1500"
    And Remaining of PurchaseOrder with code "2018000001" is updated as follows "487000"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  Scenario: (02) Activate Payment Request of type VENDOR against DownPayment (Happy Path)
  """
   - PaymentForm is CASH
   - PaymentRequest netAmount is less than or equal PurchaseOrder remaining amount
   - PaymentRequest netAmount is less than or equal specified BANKACCOUNT balance
   - All mandatory fields
   - By an authorized user
  """
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit | Company        | Currency   | Treasury              | Balance |
      | TREASURY0001000200020001 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 0001 - Flexo Treasury | 2500    |

     #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2019000040 | VENDOR      | Draft | Amr.Khalil | 05-Jan-2019 09:02 AM | Ashraf Salah  |
      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit | Company        |
      | 2019000040 | 0001 - Flexo | 0002 - DigiPro |
   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury              | Description                  |
      | 2019000040 | CASH        | 000001 - Siegwerk | PURCHASEORDER   | 2018000003  | 10        | USD         |                |                   | 0001 - Flexo Treasury | Downpayent to a confirmed PO |
 #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000003 | 10150     |
    Given user is logged in as "Ahmed.Hamdi"
    And the last created JournalEntry was with code "2019000001"
    When "Ahmed.Hamdi" activates the PaymentRequest of type "VENDOR" of method "CASH" Against "PURCHASEORDER" with code "2019000040" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDate              |
      | 06-Jan-2019 12:00 AM |
    Then PaymentRequest with Code "2019000040" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State       |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | PaymentDone |
    And the AccountingDetails Section of PaymentRequest with code "2019000040" is updated as follows:
      | AccountType | GLAccount               | GLSubAccount          | Amount |
      | DEBIT       | 10201 - Service Vendors | 000001 - Siegwerk     | 10     |
      | CREDIT      | 10430 - Cash            | 0001 - Flexo Treasury | 10     |
    And ActivationDetails in PaymentRequest with Code "2019000040" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalYear |
      | Ahmed.Hamdi | 07-Jan-2019 09:30 AM | 06-Jan-2019 12:00 AM | 17.126738     | 2019000002       | 2019       |
    Then a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument           | Company        |
      | 2019000002 | 07-Jan-2019 09:30 AM | Ahmed.Hamdi | 06-Jan-2019 12:00 AM | 0001 - Flexo | 2019       | 2019000040 - PaymentRequest | 0002 - DigiPro |
    And the following new JournalEntryDetails are created for JournalEntry with code "2019000002":
      | GLAccount               | GLSubAccount          | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 10430 - Cash            | 0001 - Flexo Treasury | 10     | 0     | USD                | EGP             | 17.126738                 | 2018000026       |
      | 10201 - Service Vendors | 000001 - Siegwerk     | 0      | 10    | USD                | EGP             | 17.126738                 | 2018000026       |
    And JournalBalance with code "TREASURY0001000200020001" is updated as follows "2490"
    And Remaining of PurchaseOrder with code "2018000003" is updated as follows "10140"
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"


