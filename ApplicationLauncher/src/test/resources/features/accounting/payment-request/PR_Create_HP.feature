# Author Dev: Zyad Ghorab, Yara Ameen, Mohamed Aboelnour
# Author Quality: Khadra Ali
# Author PO: Somaya Ahmed

Feature: Create Payment (HP)

  Background:

    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                        |
      | PurchasingResponsible_Signmedia | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia | PurUnitReader_Signmedia        |
      | PurchasingResponsible_Signmedia | VendorOwner_Signmedia          |
      | PurchasingResponsible_Signmedia | EmployeeViewer_Signmedia       |
      | PurchasingResponsible_Signmedia | CompanyViewer                  |
      | PurchasingResponsible_Signmedia | DocumentOwnerViewer            |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia | AccountingNoteViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                   | Condition                      |
      | PaymentOwner_Signmedia         | PaymentRequest:Create        |                                |
      | PurUnitReader_Signmedia        | PurchasingUnit:ReadAll_ForPO | [purchaseUnitName='Signmedia'] |
      | VendorOwner_Signmedia          | Vendor:ReadAll               | [purchaseUnitName='Signmedia'] |
      | EmployeeViewer_Signmedia       | Employee:ReadAll             | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                  | Company:ReadAll              |                                |
      | DocumentOwnerViewer            | DocumentOwner:ReadAll        |                                |
      | POViewer_Signmedia             | PurchaseOrder:ReadAll        | [purchaseUnitName='Signmedia'] |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll        | [purchaseUnitName='Signmedia'] |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll       | [purchaseUnitName='Signmedia'] |

    And the following BusinessUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Vendors exist:
      | Code   | Name     | PurchasingUnit   |
      | 000002 | Zhejiang | 0002 - Signmedia |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition |
      | 34 | Ashraf Salah | PaymentRequest | CanBeDocumentOwner |           |

    #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000003 | OTHER_PARTY_FOR_PURCHASE | Draft | Ahmed.Hamdi | 05-Jan-2021 09:02 AM | Ashraf Salah  |

  # EBS-4318, #EBS-8749
  Scenario Outline: (01) Create Payment of type VendorInvoice with all mandatory fields by an authorized user (Happy Path)

     #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000002 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |

#    @INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
#    @INSERT
    Given the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
 #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000104 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |

    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created PaymentRequest was with code "2021000003"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType | RefDocument   | DocumentOwner |
      | VENDOR      | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | INVOICE         | <RefDocument> | 34            |
    Then a new PaymentRequest is created as follows:
      | Code       | State | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | PaymentType | PaymentForm   | DocumentOwner |
      | 2021000004 | Draft | <User>    | <User>        | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | VENDOR      | <PaymentForm> | Ashraf Salah  |
    And the CompanyData Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the PaymentDetails Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessPartner   | RefDocumentType | RefDocument   | Currency                   |
      | <BusinessPartner> | INVOICE         | <RefDocument> | USD - United States Dollar |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PaymentForm | BusinessUnit     | Company        | BusinessPartner   | RefDocument |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro | 000002 - Zhejiang | 2019000104  |

  # EBS-4318, #EBS-8749
  Scenario Outline: (02) Create Payment of type DownPayment with all mandatory fields by an authorized user (Happy Path)

    #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State              | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000002 | IMPORT_PO | DeliveryComplete   | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | IMPORT_PO | Approved           | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000008 | IMPORT_PO | Confirmed          | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000009 | IMPORT_PO | FinishedProduction | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000010 | LOCAL_PO  | Shipped            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000011 | IMPORT_PO | Arrived            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000023 | IMPORT_PO | Cleared            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    Given the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000008 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000009 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000010 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000011 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000023 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2021000002 | 000002 - Zhejiang |             |
      | 2021000005 | 000002 - Zhejiang |             |
      | 2021000008 | 000009 - Vendor 3 |             |
      | 2021000009 | 000002 - Zhejiang |             |
      | 2021000010 | 000002 - Zhejiang |             |
      | 2021000011 | 000002 - Zhejiang |             |
      | 2021000023 | 000002 - Zhejiang |             |
      #@INSERT
    And the following PaymentTermData for PurchaseOrders exist:
      | Code       | Currency   | PaymentTerms                        |
      | 2021000002 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000005 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000008 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000009 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000010 | 0001 - EGP | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000011 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |
      | 2021000023 | 0002 - USD | 0002 - 20% advance, 80% Copy of B/L |

    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created PaymentRequest was with code "2021000003"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType | RefDocument   | DocumentOwner |
      | VENDOR      | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | PURCHASEORDER   | <RefDocument> | 34            |
    Then a new PaymentRequest is created as follows:
      | Code       | State | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | PaymentType | PaymentForm   | DocumentOwner |
      | 2021000004 | Draft | <User>    | <User>        | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | VENDOR      | <PaymentForm> | Ashraf Salah  |
    And the CompanyData Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the PaymentDetails Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessPartner   | RefDocumentType | RefDocument   | Currency   |
      | <BusinessPartner> | PURCHASEORDER   | <RefDocument> | <Currency> |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PaymentForm | BusinessUnit     | Company          | BusinessPartner   | RefDocument | Currency                   |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0001 - AL Madina | 000002 - Zhejiang | 2021000002  | USD - United States Dollar |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 000002 - Zhejiang | 2021000005  | USD - United States Dollar |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0002 - DigiPro   | 000009 - Vendor 3 | 2021000008  | USD - United States Dollar |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0001 - AL Madina | 000002 - Zhejiang | 2021000009  | USD - United States Dollar |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0002 - DigiPro   | 000002 - Zhejiang | 2021000010  | EGP - Egyptian Pound       |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 000002 - Zhejiang | 2021000011  | USD - United States Dollar |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0001 - AL Madina | 000002 - Zhejiang | 2021000023  | USD - United States Dollar |

  # EBS-4318, #EBS-8749
  Scenario Outline: (03) Create Payment of type General with all mandatory fields by an authorized user (Happy Path)
    #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State              | CreatedBy | CreationDate       | DocumentOwner |
      | 2021000002 | IMPORT_PO | DeliveryComplete   | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000005 | IMPORT_PO | Approved           | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000008 | IMPORT_PO | Confirmed          | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000009 | IMPORT_PO | FinishedProduction | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000010 | LOCAL_PO  | Shipped            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000011 | IMPORT_PO | Arrived            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
      | 2021000023 | IMPORT_PO | Cleared            | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |
    #@INSERT
    Given the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000005 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000008 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000009 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2021000010 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000011 | 0002 - Signmedia | 0002 - DigiPro   | 1516171819789 - EGP - Alex Bank |
      | 2021000023 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following VendorData for PurchaseOrders exist:
      | Code       | Vendor             | ReferencePO |
      | 2021000002 | 000002 - Zhejiang  |             |
      | 2021000005 | 000002 - Zhejiang  |             |
      | 2021000008 | 000009 - Vendor 3  |             |
      | 2021000009 | 000002 - Zhejiang  |             |
      | 2021000010 | 000002 - Zhejia`ng |             |
      | 2021000011 | 000002 - Zhejiang  |             |
      | 2021000023 | 000002 - Zhejiang  |             |

    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created PaymentRequest was with code "2021000003"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType              | PaymentForm   | BusinessUnit   | BusinessPartner | RefDocumentType | RefDocument   | DocumentOwner |
      | OTHER_PARTY_FOR_PURCHASE | <PaymentForm> | <BusinessUnit> |                 | PURCHASEORDER   | <RefDocument> | 34            |
    Then a new PaymentRequest is created as follows:
      | Code       | State | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | PaymentType              | PaymentForm   | DocumentOwner |
      | 2021000004 | Draft | <User>    | <User>        | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | OTHER_PARTY_FOR_PURCHASE | <PaymentForm> | Ashraf Salah  |
    And the CompanyData Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the PaymentDetails Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessPartner | RefDocumentType | RefDocument   |
      |                 | PURCHASEORDER   | <RefDocument> |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PaymentForm | BusinessUnit     | Company          | RefDocument |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0001 - AL Madina | 2021000002  |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0002 - DigiPro   | 2021000005  |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 2021000008  |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0001 - AL Madina | 2021000009  |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 2021000010  |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0002 - DigiPro   | 2021000011  |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0001 - AL Madina | 2021000023  |

  #EBS-7958, #EBS-8749
  Scenario Outline: (04) Create Payment of type Settlement with all mandatory fields by an authorized user (Happy Path)
    #@INSERT
    Given the following Employees exist:
      | Code   | Name        |
      | 000001 | Afaf        |
      | 000002 | Ahmed Hamdi |
    Given the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |
    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created PaymentRequest was with code "2021000003"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType      | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType | RefDocument | Company   | DocumentOwner |
      | UNDER_SETTLEMENT | <PaymentForm> | <BusinessUnit> | <BusinessPartner> |                 |             | <Company> | 34            |
    Then a new PaymentRequest is created as follows:
      | Code       | State | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | PaymentType      | PaymentForm   | DocumentOwner |
      | 2021000004 | Draft | <User>    | <User>        | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | UNDER_SETTLEMENT | <PaymentForm> | Ashraf Salah  |
    And the CompanyData Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the PaymentDetails Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessPartner   | RefDocumentType | RefDocument |
      | <BusinessPartner> |                 |             |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PaymentForm | BusinessUnit     | Company          | BusinessPartner      |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0001 - AL Madina | 000001 - Afaf        |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 000002 - Ahmed Hamdi |

   #EBS-5126, #EBS-8749
  Scenario Outline: (05) Create Payment of type CreditNote with all mandatory fields by an authorized user (Happy Path)
        #@INSERT
    Given the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State  | CreatedBy | CreationDate         | DocumentOwner   | Remaining |
      | 2021000008 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Active | Admin     | 10-Aug-2021 11:00 AM | Shady.Abdelatif | 4040.000  |
      | 2021000009 | CREDIT   | DEBIT_NOTE_BASED_ON_COMMISSION    | Active | Admin     | 10-Aug-2021 11:00 AM | Shady.Abdelatif | 3000.000  |
    #@INSERT
    Given the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000008 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000009 | 0002 - Signmedia | 0002 - DigiPro   |
     #@INSERT
    Given the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2021000008 | 000002 - Zhejiang | 5000   | 0001 - EGP |
      | 2021000009 | 000002 - Zhejiang | 6000   | 0001 - EGP |

    Given user is logged in as "<User>"
    And CurrentDateTime = "01-Jan-2021 11:00 AM"
    And Last created PaymentRequest was with code "2021000003"
    When "<User>" creates PaymentRequest with the following values:
      | PaymentType          | PaymentForm   | BusinessUnit   | BusinessPartner   | RefDocumentType | RefDocument   | DocumentOwner |
      | BASED_ON_CREDIT_NOTE | <PaymentForm> | <BusinessUnit> | <BusinessPartner> | CREDITNOTE      | <RefDocument> | 34            |
    Then a new PaymentRequest is created as follows:
      | Code       | State | CreatedBy | LastUpdatedBy | CreationDate         | LastUpdateDate       | PaymentType          | PaymentForm   | DocumentOwner |
      | 2021000004 | Draft | <User>    | <User>        | 01-Jan-2021 11:00 AM | 01-Jan-2021 11:00 AM | BASED_ON_CREDIT_NOTE | <PaymentForm> | Ashraf Salah  |
    And the CompanyData Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    And the PaymentDetails Section of PaymentRequest with code "2021000004" is updated as follows:
      | BusinessPartner   | RefDocumentType | RefDocument   |
      | <BusinessPartner> | CREDITNOTE      | <RefDocument> |
    And a success notification is sent to "<User>" with the following message "Gen-msg-11"
    Examples:
      | User        | PaymentForm | BusinessUnit     | Company          | BusinessPartner   | RefDocument |
      | Gehan.Ahmed | BANK        | 0002 - Signmedia | 0001 - AL Madina | 000002 - Zhejiang | 2021000008  |
      | Gehan.Ahmed | CASH        | 0002 - Signmedia | 0002 - DigiPro   | 000002 - Zhejiang | 2021000009  |
