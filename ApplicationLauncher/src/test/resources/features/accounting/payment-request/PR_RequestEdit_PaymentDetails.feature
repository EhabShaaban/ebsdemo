# Author Dev: Gehad Shady, Ahmed Ali, Yara Ameen
# Updated: Evraam hany, Mohamed Aboelnour
# Author Quality: Khadra Ali
# Author PO: Somaya Aboelwafaa
Feature: Request to edit and cancel PaymentDetails section in PaymentRequest

  Background:
    Given the following users and roles exist:
      | Name                        | Role                                                    |
      | hr1                         | SuperUser                                               |
      | Afaf                        | FrontDesk                                               |
      | Amr.Khalil                  | PurchasingResponsible_Flexo                             |
      | Gehan.Ahmed                 | PurchasingResponsible_Signmedia                         |
      | Gehan.Ahmed.NoInvoice       | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   |
      | Gehan.Ahmed.NoCurrency      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  |
      | Gehan.Ahmed.NoPurchaseOrder | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder |
      | Gehan.Ahmed.NoItems         | PurchasingResponsible_Signmedia_CannotReadItemsRole     |

    And the following roles and sub-roles exist:
      | Role                                                    | Sub-role                       |
      | SuperUser                                               | SuperUserSubRole               |
      | PurchasingResponsible_Flexo                             | PaymentOwner_Flexo             |
      | PurchasingResponsible_Flexo                             | VendorInvoiceViewer_Flexo      |
      | PurchasingResponsible_Flexo                             | CurrencyViewer                 |
      | PurchasingResponsible_Flexo                             | CompanyViewer                  |
      | PurchasingResponsible_Flexo                             | POViewer_Flexo                 |
      | PurchasingResponsible_Flexo                             | ItemOwner_Flexo                |
      | PurchasingResponsible_Signmedia                         | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia                         | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia                         | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia                         | CompanyViewer                  |
      | PurchasingResponsible_Signmedia                         | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia                         | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia                         | AccountingNoteViewer_Signmedia |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   | POOwner_Signmedia              |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewInvoiceRole   | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  | POOwner_Signmedia              |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  | POViewer_Signmedia             |
      | PurchasingResponsible_Signmedia_CannotViewCurrencyRole  | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadPurchaseOrder | ItemOwner_Signmedia            |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole     | PaymentOwner_Signmedia         |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole     | VendorInvoiceViewer_Signmedia  |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole     | CurrencyViewer                 |
      | PurchasingResponsible_Signmedia_CannotReadItemsRole     | POViewer_Signmedia             |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                        | Condition                                                         |
      | SuperUserSubRole               | *:*                               |                                                                   |
      | PaymentOwner_Flexo             | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser'])     |
      | PaymentOwner_Signmedia         | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | VendorInvoiceViewer_Flexo      | VendorInvoice:ReadAll             | [purchaseUnitName='Flexo']                                        |
      | VendorInvoiceViewer_Signmedia  | VendorInvoice:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | CurrencyViewer                 | Currency:ReadAll                  |                                                                   |
      | CompanyViewer                  | Company:ReadAll                   |                                                                   |
      | POViewer_Flexo                 | PurchaseOrder:ReadAll             | [purchaseUnitName='Flexo']                                        |
      | POViewer_Signmedia             | PurchaseOrder:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | ItemOwner_Signmedia            | Item:ReadAll                      | [purchaseUnitName='Signmedia']                                    |
      | AccountingNoteViewer_Signmedia | AccountingNote:ReadAll            | [purchaseUnitName='Signmedia']                                    |
    And the following users doesn't have the following permissions:
      | User                        | Permission                        |
      | Afaf                        | PaymentRequest:EditPaymentDetails |
      | Gehan.Ahmed.NoInvoice       | VendorInvoice:ReadAll             |
      | Gehan.Ahmed.NoCurrency      | Currency:ReadAll                  |
      | Gehan.Ahmed.NoPurchaseOrder | PurchaseOrder:ReadAll             |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                        | Condition                                                     |
      | Gehan.Ahmed | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser']) |
      | Gehan.Ahmed | Item:ReadAll                      | [purchaseUnitName='Flexo']                                    |

    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                           | State  | CreatedBy | CreationDate         | DocumentOwner |
      | 2019000013 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION | Posted | Admin     | 02-Aug-2019 11:00 AM | Ashraf Salah  |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000013 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2019000013 | 000002 - Zhejiang | 1000   | 0001 - EGP |

     #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                | State  | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000108 | LOCAL_GOODS_INVOICE | Posted | Gehan.Ahmed | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit      | Company        |
      | 2019000108 | 0006 - Corrugated | 0002 - DigiPro |

       #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy                   | CreationDate         | Remaining | DocumentOwner |
      | 2019000002 | VENDOR                   | Draft       | Amr.Khalil                  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed                 | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000021 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000012 | VENDOR                   | Draft       | Gehan.Ahmed                 | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000008 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed.NoCurrency      | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000029 | VENDOR                   | Draft       | Gehan.Ahmed.NoInvoice       | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000030 | VENDOR                   | Draft       | Gehan.Ahmed.NoPurchaseOrder | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil                  | 05-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000050 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed.NoItems         | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000040 | VENDOR                   | Draft       | Amr.Khalil                  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000038 | VENDOR                   | Draft       | Gehan.Ahmed                 | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2021000001 | UNDER_SETTLEMENT         | Draft       | Ashraf.Salah                | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000002 | UNDER_SETTLEMENT         | PaymentDone | Ashraf.Salah                | 04-Jan-2021 09:02 AM | 1000      | Ashraf Salah  |
      | 2021000003 | UNDER_SETTLEMENT         | Draft       | Ashraf.Salah                | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000004 | BASED_ON_CREDIT_NOTE     | Draft       | Gehan.Ahmed                 | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000005 | BASED_ON_CREDIT_NOTE     | Draft       | Gehan.Ahmed                 | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000006 | BASED_ON_CREDIT_NOTE     | PaymentDone | Gehan.Ahmed                 | 04-Jan-2021 09:02 AM | 1000      | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company        |
      | 2019000002 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000004 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000021 | 0002 - Signmedia  |                |
      | 2019000012 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000008 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000029 | 0002 - Signmedia  |                |
      | 2019000030 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000003 | 0006 - Corrugated | 0002 - DigiPro |
      | 2019000050 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000040 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000038 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000001 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000004 | 0002 - Signmedia  |                |
      | 2021000005 | 0002 - Signmedia  |                |
      | 2021000006 | 0002 - Signmedia  | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber         | Treasury                  | Description                  |
      | 2019000002 | BANK        | 000001 - Siegwerk | PURCHASEORDER   | 2018000034  | 1000      | EUR         |                            | Alex Bank - 1516171819789 |                           | For PO No 2018000010         |
      | 2019000004 | BANK        |                   | PURCHASEORDER   | 2018000010  | 250       | EGP         | 000014 - Insurance service | Alex Bank - 1516171819789 |                           | مصاريف بنكية                 |
      | 2019000021 | BANK        |                   | INVOICE         |             | 10        | USD         |                            |                           | 0001 - Flexo Treasury     | Downpayent to a confirmed PO |
      | 2019000012 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000002  | 2000      | USD         |                            | 1516171819789 - Alex Bank |                           | for save payment details hp  |
      | 2019000008 | CASH        |                   | PURCHASEORDER   | 2018000010  | 1000      | USD         |                            |                           |                           | for save payment details hp  |
      | 2019000029 | BANK        | 000002 - Zhejiang | INVOICE         |             | 2000      | USD         |                            | 1516171819789 - Alex Bank |                           | for save payment details hp  |
      | 2019000030 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000001  | 1000      | USD         |                            | 1516171819789 - Alex Bank |                           | for save payment details hp  |
      | 2019000003 | BANK        |                   | PURCHASEORDER   | 2018000045  | 100       | USD         | 000014 - Insurance service | Alex Bank - 1516171819789 |                           | downpayment for po           |
      | 2019000050 | BANK        |                   | PURCHASEORDER   | 2018000010  | 250       | EGP         | 000014 - Insurance service | Alex Bank - 1516171819789 |                           | مصاريف بنكية                 |
      | 2019000040 | CASH        | 000001 - Siegwerk | PURCHASEORDER   | 2018000003  | 10        | USD         |                            |                           | 0001 - Flexo Treasury     | Downpayent to a confirmed PO |
      | 2019000038 | CASH        | 000002 - Zhejiang | INVOICE         | 2019000108  | 300       | USD         |                            |                           | 0002 - Signmedia Treasury | Installment No 2 in invoice  |
      | 2021000001 | CASH        | 000001 - Afaf     |                 |             | 1000      | USD         |                            |                           | 0001 - Flexo Treasury     | Under settlement             |
      | 2021000002 | BANK        | 000001 - Afaf     |                 |             | 1000      | EGP         |                            | Alex Bank - 1516171819789 |                           | Under settlement             |
      | 2021000003 | BANK        |                   |                 |             | 1000      | EGP         |                            | 1516171819712 - Alex Bank |                           | Under settlement             |
      | 2021000004 | BANK        | 000002 - Zhejiang | CREDITNOTE      |             | 1000      | EGP         |                            | Alex Bank - 1516171819789 |                           | BASED ON CREDIT NOTE         |
      | 2021000005 | CASH        | 000002 - Zhejiang | CREDITNOTE      |             | 1000      | EGP         |                            |                           | 0002 - Signmedia Treasury | BASED ON CREDIT NOTE         |
      | 2021000006 | BANK        | 000002 - Zhejiang | CREDITNOTE      | 2019000013  | 1000      | EGP         |                            | 1516171819712 - Alex Bank |                           | BASED ON CREDIT NOTE         |



    And edit session is "30" minutes

  ## Request to Edit Payment Details section ###
  Scenario: (01) Request to edit Payment details section by an authorized user with one role in case of Vendor Payment & BANK Form (Happy Path) - DownPayment
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to edit PaymentDetails section of PaymentRequest with code "2019000002"
    Then PaymentDetails section of PaymentRequest with code "2019000002" becomes in edit mode and locked by "Amr.Khalil"
    And the following authorized reads are returned to "Amr.Khalil":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Amr.Khalil"
    And the following editable fields are returned to "Amr.Khalil":
      | EditableFields |
      | netAmount      |
      | description    |
      | companyBankId  |

  #EBS-7277
  Scenario: (02) Request to edit Payment details section by an authorized user with one role in case of Vendor Payment & CASH Form (Happy Path) - DownPayment
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to edit PaymentDetails section of PaymentRequest with code "2019000040"
    Then PaymentDetails section of PaymentRequest with code "2019000040" becomes in edit mode and locked by "Amr.Khalil"
    And the following authorized reads are returned to "Amr.Khalil":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Amr.Khalil"
    And the following editable fields are returned to "Amr.Khalil":
      | EditableFields |
      | netAmount      |
      | description    |
      | treasuryCode   |

  Scenario: (03) Request to edit Payment details section by an authorized user with one role in case of Vendor Payment & BANK Form (Happy Path) - Invoice
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2019000012"
    Then PaymentDetails section of PaymentRequest with code "2019000012" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | netAmount      |
      | description    |
      | companyBankId  |

  #EBS-7277
  Scenario: (04) Request to edit Payment details section by an authorized user with one role in case of Vendor Payment & CASH Form (Happy Path) - Invoice
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2019000038"
    Then PaymentDetails section of PaymentRequest with code "2019000038" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | netAmount      |
      | description    |
      | treasuryCode   |

  Scenario Outline: (05) Request to edit Payment details section in Draft - by an authorized user with one role in case of General Payment & BANK Form (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to edit PaymentDetails section of PaymentRequest with code "<PRCode>"
    Then PaymentDetails section of PaymentRequest with code "<PRCode>" becomes in edit mode and locked by "hr1"
    And the following authorized reads are returned to "hr1":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "hr1"
    And the following editable fields are returned to "hr1":
      | EditableFields     |
      | netAmount          |
      | currencyCode       |
      | description        |
      | costFactorItemCode |
      | companyBankId      |

    Examples:
      | PRCode     |
      | 2019000004 |

  Scenario: (06) Request to edit Payment details section that is locked by another user (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first opened the PaymentRequest with code "2019000002" in the edit mode successfully
    When "Amr.Khalil" requests to edit PaymentDetails section of PaymentRequest with code "2019000002"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-02"

  Scenario: (07) Request to edit Payment details section of deleted PaymentRequest (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000002" successfully
    When "Amr.Khalil" requests to edit PaymentDetails section of PaymentRequest with code "2019000002"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  #EBS-6227
  Scenario Outline: (08) Request to edit Payment details section when it is not allowed in current state (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to edit PaymentDetails section of PaymentRequest with code "<PRCode>" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And PaymentDetails section of PaymentRequest with code "<PRCode>" is not locked by "hr1"

    Examples:
      | PRCode     |
      | 2019000003 |
      | 2021000002 |
      | 2021000006 |

  Scenario Outline: (09) Request to edit Payment details section with unauthorized user (with condition - without condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "<User>"
    When "<User>" requests to edit PaymentDetails section of PaymentRequest with code "2019000002"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |

  Scenario: (10) Request to edit Payment details section  (No authorized reads for Currency) - General PR & CASH Form
    Given user is logged in as "Gehan.Ahmed.NoCurrency"
    When "Gehan.Ahmed.NoCurrency" requests to edit PaymentDetails section of PaymentRequest with code "2019000008"
    Then PaymentDetails section of PaymentRequest with code "2019000008" becomes in edit mode and locked by "Gehan.Ahmed.NoCurrency"
    And there are no mandatory fields returned to "Gehan.Ahmed.NoCurrency"
    And the following authorized reads are returned to "Gehan.Ahmed.NoCurrency":
      | AuthorizedReads        |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And the following editable fields are returned to "Gehan.Ahmed.NoCurrency":
      | EditableFields     |
      | netAmount          |
      | currencyCode       |
      | description        |
      | costFactorItemCode |
      | treasuryCode       |

  Scenario: (11) Request to edit Payment details section  (No authorized reads for cost factor Items) - General PR & BANK Form
    Given user is logged in as "Gehan.Ahmed.NoItems"
    When "Gehan.Ahmed.NoItems" requests to edit PaymentDetails section of PaymentRequest with code "2019000050"
    Then PaymentDetails section of PaymentRequest with code "2019000050" becomes in edit mode and locked by "Gehan.Ahmed.NoItems"
    And there are no mandatory fields returned to "Gehan.Ahmed.NoItems"
    And the following authorized reads are returned to "Gehan.Ahmed.NoItems":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And the following editable fields are returned to "Gehan.Ahmed.NoItems":
      | EditableFields     |
      | netAmount          |
      | currencyCode       |
      | description        |
      | costFactorItemCode |
      | companyBankId      |

#EBS-7978
  Scenario: (12) Request to edit Payment details section by an authorized user with one role in case of Under Settlement Payment & CASH Form (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to edit PaymentDetails section of PaymentRequest with code "2021000001"
    Then PaymentDetails section of PaymentRequest with code "2021000001" becomes in edit mode and locked by "hr1"
    And the following authorized reads are returned to "hr1":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "hr1"
    And the following editable fields are returned to "hr1":
      | EditableFields |
      | netAmount      |
      | description    |
      | treasuryCode   |
      | currencyCode   |
#  EBS-7978
  Scenario: (13) Request to edit Payment details section by an authorized user with one role in case of Under Settlement Payment & BANK Form (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to edit PaymentDetails section of PaymentRequest with code "2021000003"
    Then PaymentDetails section of PaymentRequest with code "2021000003" becomes in edit mode and locked by "hr1"
    And the following authorized reads are returned to "hr1":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "hr1"
    And the following editable fields are returned to "hr1":
      | EditableFields |
      | netAmount      |
      | description    |
      | currencyCode   |
      | companyBankId  |

  #EBS-7748, EBS-8080
  Scenario: (14) Request to edit Payment details section by an authorized user with one role in case of Based On Credit Note Payment & BANK Form (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2021000004"
    Then PaymentDetails section of PaymentRequest with code "2021000004" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | netAmount      |
      | description    |
      | currencyCode   |
      | companyBankId  |

  #EBS-7748, EBS-8080
  Scenario: (15) Request to edit Payment details section by an authorized user with one role in case of Based On Credit Note Payment & CASH Form (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to edit PaymentDetails section of PaymentRequest with code "2021000005"
    Then PaymentDetails section of PaymentRequest with code "2021000005" becomes in edit mode and locked by "Gehan.Ahmed"
    And the following authorized reads are returned to "Gehan.Ahmed":
      | AuthorizedReads        |
      | ReadCurrency           |
      | ReadItems              |
      | ReadCompanyBankDetails |
      | ReadTreasuries         |
    And there are no mandatory fields returned to "Gehan.Ahmed"
    And the following editable fields are returned to "Gehan.Ahmed":
      | EditableFields |
      | netAmount      |
      | description    |
      | treasuryCode   |
      | currencyCode   |

  #Request to Cancel Payment Details section ###
  Scenario: (16) Request to Cancel saving Payment details section by an authorized user with one role (Happy Path)
    Given user is logged in as "Amr.Khalil"
    And PaymentDetails section of PaymentRequest with code "2019000002" is locked by "Amr.Khalil" at "07-Jan-2019 09:10 AM"
    When "Amr.Khalil" cancels saving PaymentDetails section of PaymentRequest with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Amr.Khalil" on PaymentDetails section of PaymentRequest with code "2019000002" is released

  Scenario: (17) Request to Cancel saving Payment details section after lock session is expire
    Given user is logged in as "Amr.Khalil"
    And PaymentDetails section of PaymentRequest with code "2019000002" is locked by "Amr.Khalil" at "07-Jan-2019 09:00 AM"
    When "Amr.Khalil" cancels saving PaymentDetails section of PaymentRequest with code "2019000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-07"

  Scenario: (18) Request to Cancel saving Payment details section after lock session is expire and PaymentRequest doesn't exsit (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "2019000002" is locked by "Amr.Khalil" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the PaymentRequest with code "2019000002" successfully at "07-Jan-2019 09:31 AM"
    When "Amr.Khalil" cancels saving PaymentDetails section of PaymentRequest with code "2019000002" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  Scenario Outline: (19) Request to Cancel saving Payment details section with unauthorized user (with condition - without condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving PaymentDetails section of PaymentRequest with code "2019000002"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User        |
      | Afaf        |
      | Gehan.Ahmed |
