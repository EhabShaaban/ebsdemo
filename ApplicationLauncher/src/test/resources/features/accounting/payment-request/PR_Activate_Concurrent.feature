Feature: Activate Payment Request Concurrent Happy Path

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole            |
      | AdminTreasury | PaymentResponsible |
      | AdminTreasury | ExchangeRateViewer |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission              | Condition |
      | PaymentResponsible | PaymentRequest:Activate |           |
      | ExchangeRateViewer | ExchangeRate:ReadAll    |           |

     #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000039 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |
     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company        |
      | 2019000039 | 0001 - Flexo | 0002 - DigiPro |


     #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                      | Qty(OrderUnit)      | Qty(Base) | UnitPrice(Base) |
      | 2019000039 | 000056 - Shipment service | 130.00 Roll 2.20x50 | 130.00 M2 | 90.00           |

        #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining |
      | 2019000039 | 00.00       | 13233.60       |

     #@INSERT
    And the following VendorInvoices ActivationDetails exist:
      | Code       | ActivatedBy     | ActivationDate       | CurrencyPrice       | JournalEntryCode |
      | 2019000039 | Shady.Abdelatif | 06-Aug-2019 09:02 AM | 1.0 USD = 17.44 EGP | 2020000024       |
      #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2018 | 1-Jan-2018 12:00 AM | 31-Dec-2018 11:59 PM | Closed |
      | 0002 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Active |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

      #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State | CreatedBy                                           | CreationDate         | DocumentOwner |
      | 2019000005 | VENDOR                   | Draft | Amr.Khalil                                          | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000009 | OTHER_PARTY_FOR_PURCHASE | Draft | Ahmed.Hamdi.NoCompanyAndNoBankAccountAndNoGLAccount | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2019000030 | VENDOR                   | Draft | Gehan.Ahmed.NoPurchaseOrder                         | 05-Aug-2018 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000005 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000009 | 0001 - Flexo     | 0002 - DigiPro |
      | 2019000030 | 0002 - Signmedia | 0002 - DigiPro |

   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem     | BankAccountNumber               | Treasury | Description                 |
       # PR with Amount < Invoice Remaining Amount
      | 2019000005 | BANK        | 000001 - Siegwerk | INVOICE         | 2019000039  | 2000      | USD         |                    | 1516171819781 - USD - Alex Bank |          | Installment No 2 in invoice |
      # PR of General Payment
      | 2019000009 | BANK        |                   | PURCHASEORDER   | 2018000056  | 1000      | EGP         | 000057 - Bank Fees | 1234567893458 - EGP - Bank Misr |          | for save payment details hp |
      # PR with Amount < Order Amount
      | 2019000030 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000001  | 1000      | USD         |                    | 1516171819781 - USD - Alex Bank |          | for save payment details hp |

        #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000039 | 2018000056    | 000001 - Siegwerk | 20% advance, 80% Copy of B/L | 1200120301023110 | 24-Apr-2018 06:31 AM | USD      |

   #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 2018000026 | USD              | EGP               | 17.44       | 19-Sep-2019 10:00 PM | User Daily Rate |
      | 2018000027 | EGP              | EGP               | 1           | 19-Sep-2019 10:00 PM | User Daily Rate |

    And the GLAccounts configuration exit:
      | GLAccountConfig   | GLAccount                                |
      | BankGLAccount     | 10431 - Bank                             |
      | CashGLAccount     | 10430 - Cash                             |
      | RealizedGLAccount | 41101 - Realized Differences on Exchange |

    And the following Service Items exist:
      | Code   | Type    | MarketName | BusinessUnit | CostFactor |
      | 000057 | SERVICE | Bank Fees  | 0002         | true       |

    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00020002000200031516171819781 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 3000    |
      | BANK00010002000100011234567893458 | 0001 - Flexo     | 0002 - DigiPro | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 3000    |
      | BANK00010002000200031516171819781 | 0001 - Flexo     | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 3000    |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000001 | IMPORT - Landed Cost For Import PO | EstimatedValuesCompleted | Gehan.Ahmed | 24-Jun-2021 09:02 AM | Shady.Abdelatif |

    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit | Company        |
      | 2021000001 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock |
      | 2021000001 | 2018000056 |                   | true        |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item               | EstimatedValue | ActualValue | Difference |
      | 2021000001 | 000057 - Bank Fees | 1000 EGP       | 0 EGP       | 1000 EGP   |

     #@INSERT
    And the following Remaining for PurchaseOrders exist:
      | Code       | Remaining |
      | 2018000056 | 48000     |
      | 2018000001 | 48000     |

  Scenario: (01) Activate Payment Request with all mandatory fields by concurrent authorized users (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And another user is logged in as "hr2"
    And the last created JournalEntry was with code "2020000056"
    When the users post PaymentRequests at "07-Jan-2020 09:30 AM" with the following values:
      | User        | PRCode     | DueDate              | PaymentType              | PaymentForm | DueDocumentType |
      | Ahmed.Hamdi | 2019000005 | 06-Jan-2020 12:00 AM | VENDOR                   | BANK        | INVOICE         |
      | hr1         | 2019000030 | 07-Jan-2020 09:30 AM | VENDOR                   | BANK        | PURCHASEORDER   |
      | hr2         | 2019000009 | 07-Jan-2020 09:30 AM | OTHER_PARTY_FOR_PURCHASE | BANK        | PURCHASEORDER   |
    Then the following JournalEntries are created as follows:
      | CreationDate         | CreatedBy   | DueDate              | ReferenceDocumentCode | ReferenceDocumentName | ReferenceDocumentCompany |
      | 07-Jan-2020 09:30 AM | Ahmed.Hamdi | 06-Jan-2020 12:00 AM | 2019000005            | PaymentRequest        | 0002 - DigiPro           |
      | 07-Jan-2020 09:30 AM | hr1         | 07-Jan-2020 09:30 AM | 2019000030            | PaymentRequest        | 0002 - DigiPro           |
      | 07-Jan-2020 09:30 AM | hr2         | 07-Jan-2020 09:30 AM | 2019000009            | PaymentRequest        | 0002 - DigiPro           |
    And the following JournalEntries Codes are generated as follows:
      | Code       |
      | 2020000057 |
      | 2020000058 |
      | 2020000059 |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47" with one of the following JournalEntry codes:
      | Code       |
      | 2020000057 |
      | 2020000058 |
      | 2020000059 |
    And a success notification is sent to "hr1" with the following message "Gen-msg-47" with one of the following JournalEntry codes:
      | Code       |
      | 2020000057 |
      | 2020000058 |
      | 2020000059 |
