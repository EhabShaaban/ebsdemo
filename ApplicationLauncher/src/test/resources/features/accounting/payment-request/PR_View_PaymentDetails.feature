# Author Dev: Yara Ameen, Ahmed Ali, Evram Hany
# Author Quality: Khadrah Ali
# Author PO: Somaya Aboulwafa
Feature: View PaymentDetails section

  Background:
    Given the following users and roles exist:
      | Name       | Role                             |
      | hr1        | SuperUser                        |
      | Amr.Khalil | PurchasingResponsible_Flexo      |
      | Amr.Khalil | PurchasingResponsible_Corrugated |
    And the following roles and sub-roles exist:
      | Role                             | Sub-role                        |
      | SuperUser                        | SuperUserSubRole                |
      | PurchasingResponsible_Flexo      | LimitedPaymentViewer_Flexo      |
      | PurchasingResponsible_Corrugated | LimitedPaymentViewer_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                        | Condition                                                          |
      | SuperUserSubRole                | *:*                               |                                                                    |
      | LimitedPaymentViewer_Flexo      | PaymentRequest:ReadPaymentDetails | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser'])      |
      | LimitedPaymentViewer_Corrugated | PaymentRequest:ReadPaymentDetails | ([purchaseUnitName='Corrugated'] && [creationInfo='LoggedInUser']) |
    And the following Employees exist:
      | Code   | Name |
      | 000001 | Afaf |

     #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000009 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
      | 2019000010 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit | Company          |
      | 2019000009 | 0001 - Flexo | 0001 - AL Madina |
      | 2019000010 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner          | Amount | Currency   |
      | 2019000009 | 000006 - المطبعة الأمنية | 1000   | 0001 - EGP |
      | 2019000010 | 000006 - المطبعة الأمنية | 1000   | 0001 - EGP |

        #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type             | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000039 | SHIPMENT_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company        |
      | 2019000039 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy   | CreationDate         | Remaining | DocumentOwner |
      | 2019000002 | VENDOR                   | Draft       | Amr.Khalil  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil  | 05-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000005 | VENDOR                   | Draft       | Amr.Khalil  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000040 | VENDOR                   | Draft       | Amr.Khalil  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2021000001 | UNDER_SETTLEMENT         | Draft       | Amr.Khalil  | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000002 | UNDER_SETTLEMENT         | PaymentDone | Amr.Khalil  | 04-Jan-2021 09:02 AM | 1000      | Ashraf Salah  |
      | 2021000010 | BASED_ON_CREDIT_NOTE     | Draft       | Amr.Khalil  | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
      | 2021000011 | BASED_ON_CREDIT_NOTE     | Draft       | Amr.Khalil  | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company        |
      | 2019000002 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000003 | 0006 - Corrugated | 0002 - DigiPro |
      | 2019000005 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000040 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000004 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000001 | 0001 - Flexo      | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo      | 0002 - DigiPro |
      | 2021000010 | 0001 - Flexo      | 0002 - DigiPro |
      | 2021000011 | 0001 - Flexo      | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner          | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | ExpectedDueDate      | BankTransRef | Treasury              | Description                  |
      | 2019000002 | BANK        | 000001 - Siegwerk        | PURCHASEORDER   | 2018000034  | 1000      | EUR         |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | For PO No 2018000010         |
      | 2019000003 | BANK        |                          | PURCHASEORDER   | 2018000045  | 100       | USD         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank | 15-Mar-2021 09:02 AM | 195326512    |                       | downpayment for po           |
      | 2019000005 | BANK        | 000001 - Siegwerk        | INVOICE         | 2019000039  | 2000      | USD         |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | Installment No 2 in invoice  |
      | 2019000040 | CASH        | 000001 - Siegwerk        | PURCHASEORDER   | 2018000003  | 10        | USD         |                            |                                 |                      |              | 0001 - Flexo Treasury | Downpayent to a confirmed PO |
      | 2019000004 | BANK        |                          | PURCHASEORDER   | 2018000010  | 250       | EGP         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |                      |              |                       | مصاريف بنكية                 |
      | 2021000001 | CASH        | 000001 - Afaf            |                 |             | 1000      | USD         |                            |                                 |                      |              | 0001 - Flexo Treasury | Under settlement             |
      | 2021000002 | BANK        | 000001 - Afaf            |                 |             | 1000      | EGP         |                            | 1516171819789 - EGP - Alex Bank |                      | 195326920    |                       | Under settlement             |
      | 2021000010 | BANK        | 000006 - المطبعة الأمنية | CREDITNOTE      | 2019000009  | 1000      | EGP         |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | payment under credit note    |
      | 2021000011 | CASH        | 000006 - المطبعة الأمنية | CREDITNOTE      | 2019000010  | 1000      | EGP         |                            |                                 |                      |              | 0001 - Flexo Treasury | payment under credit note    |

  # EBS-2625, EBS-7277, EBS-7977, EBS-8332
  Scenario Outline: (01) View PaymentDetails section by an authorizd user (Happy Path)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view PaymentDetails section of PaymentRequest with code "<PRCode>"
    Then the following values of PaymentDetails section for PaymentRequest with code "<PRCode>" are displayed to "Amr.Khalil":
      | BusinessPartner   | DueDocumentType   | DueDocument   | NetAmount   | Currency   | CostFactorItem   | BankAccountNumber   | ExpectedDueDate   | BankTransRef   | Treasury   | Description   |
      | <BusinessPartner> | <DueDocumentType> | <DueDocument> | <NetAmount> | <Currency> | <CostFactorItem> | <BankAccountNumber> | <ExpectedDueDate> | <BankTransRef> | <Treasury> | <Description> |
    Examples:
      | PRCode     | BusinessPartner          | DueDocumentType | DueDocument | NetAmount | Currency                   | CostFactorItem             | BankAccountNumber               | ExpectedDueDate      | BankTransRef | Treasury              | Description                  |
      | 2019000002 | 000001 - Siegwerk        | PURCHASEORDER   | 2018000034  | 1000      | EUR - Euro                 |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | For PO No 2018000010         |
      | 2019000003 |                          | PURCHASEORDER   | 2018000045  | 100       | USD - United States Dollar | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank | 15-Mar-2021 09:02 AM | 195326512    |                       | downpayment for po           |
      | 2019000005 | 000001 - Siegwerk        | INVOICE         | 2019000039  | 2000      | USD - United States Dollar |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | Installment No 2 in invoice  |
      | 2019000040 | 000001 - Siegwerk        | PURCHASEORDER   | 2018000003  | 10        | USD - United States Dollar |                            |                                 |                      |              | 0001 - Flexo Treasury | Downpayent to a confirmed PO |
      | 2021000001 | 000001 - Afaf            |                 |             | 1000      | USD - United States Dollar |                            |                                 |                      |              | 0001 - Flexo Treasury | Under settlement             |
      | 2021000002 | 000001 - Afaf            |                 |             | 1000      | EGP - Egyptian Pound       |                            | 1516171819789 - EGP - Alex Bank |                      | 195326920    |                       | Under settlement             |
     #EBS-7743
      | 2021000010 | 000006 - المطبعة الأمنية | CREDITNOTE      | 2019000009  | 1000      | EGP - Egyptian Pound       |                            | 1516171819789 - EGP - Alex Bank |                      |              |                       | payment under credit note    |
      | 2021000011 | 000006 - المطبعة الأمنية | CREDITNOTE      | 2019000010  | 1000      | EGP - Egyptian Pound       |                            |                                 |                      |              | 0001 - Flexo Treasury | payment under credit note    |

  # EBS-2625
  Scenario: (03) View PaymentDetails section where PaymentRequest doesn't exist (Exception Case)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000002" successfully
    When "Amr.Khalil" requests to view PaymentDetails section of PaymentRequest with code "2019000002"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  # EBS-2625
  Scenario: (04) View PaymentDetails section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Amr.Khalil"
    When "Amr.Khalil" requests to view PaymentDetails section of PaymentRequest with code "2019000004"
    Then "Amr.Khalil" is logged out
    And "Amr.Khalil" is forwarded to the error page
