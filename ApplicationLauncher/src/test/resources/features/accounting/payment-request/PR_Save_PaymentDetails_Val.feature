# Author Dev: Hossam Hassan, Ibrahim Khidr, Waseem Salama, Zyad Ghorab, Gehad Shady, Evram Hany, Mohamed Aboelnour, Ahmed Ali, Yara Ameen
# Author Quality: Khadra Ali
Feature: Save Payment Details section Validation in Payment Request

  Background:
    Given the following users and roles exist:
      | Name        | Role                            |
      | Gehan.Ahmed | PurchasingResponsible_Signmedia |
      | hr1         | SuperUser                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                       |
      | PurchasingResponsible_Signmedia | PaymentOwner_Signmedia        |
      | PurchasingResponsible_Signmedia | VendorInvoiceViewer_Signmedia |
      | PurchasingResponsible_Signmedia | CurrencyViewer                |
      | PurchasingResponsible_Signmedia | POViewer_Signmedia            |
      | PurchasingResponsible_Signmedia | CompanyViewer                 |
      | SuperUser                       | SuperUserSubRole              |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission                        | Condition                                                         |
      | PaymentOwner_Signmedia        | PaymentRequest:EditPaymentDetails | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | VendorInvoiceViewer_Signmedia | VendorInvoice:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | POViewer_Signmedia            | PurchaseOrder:ReadAll             | [purchaseUnitName='Signmedia']                                    |
      | CurrencyViewer                | Currency:ReadAll                  |                                                                   |
      | CompanyViewer                 | Company:ReadAll                   |                                                                   |
      | SuperUserSubRole              | *:*                               |                                                                   |

          #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000002 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2021 9:02 AM | Gehan Ahmed   |

    #@INSERT
    Given the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company        | BankAccountNumber               |
      | 2018000002 | 0002 - Signmedia | 0002 - DigiPro | 1516171819789 - EGP - Alex Bank |

     #@INSERT
    Given the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2018000002 | 000002 - Zhejiang |             |

    # @INSERT
    Given the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2019 8:31 AM | Ashraf Salah  |
#    @INSERT
    Given the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000104 | 0002 - Signmedia | 0002 - DigiPro |
 #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2019000104 | 2018000002    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 04-Jan-2019 11:00 AM | USD      |

        #@INSERT
    Given the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy   | CreationDate         | DocumentOwner |
      | 2019000010 | VENDOR                   | Draft       | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2019000012 | VENDOR                   | Draft       | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2019000021 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2019000038 | VENDOR                   | Draft       | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |
      | 2019000013 | VENDOR                   | PaymentDone | Gehan.Ahmed | 05-Aug-2019 09:02 AM | Ashraf Salah  |

      #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company        |
      | 2019000010 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000004 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000012 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000021 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000038 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000013 | 0002 - Signmedia | 0002 - DigiPro |

   #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury                  | Description                 |
      | 2019000010 | BANK        | 000002 - Zhejiang | PURCHASEORDER   | 2018000002  | 488010    | USD         |                            | 1516171819789 - EGP - Alex Bank |                           | for save payment details hp |
      | 2019000004 | BANK        |                   | PURCHASEORDER   | 2018000002  | 250       | EGP         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |                           | مصاريف بنكية                |
      | 2019000012 | BANK        | 000002 - Zhejiang | INVOICE         | 2019000104  | 2000      | USD         |                            | 1516171819789 - EGP - Alex Bank |                           | for save payment details hp |
      | 2019000021 | BANK        |                   | PURCHASEORDER   | 2018000002  |           |             |                            |                                 |                           |                             |
      | 2019000038 | CASH        | 000002 - Zhejiang | INVOICE         | 2019000104  | 300       | USD         |                            |                                 | 0002 - Signmedia Treasury | Installment No 2 in invoice |
      | 2019000013 | CASH        | 000002 - Zhejiang | INVOICE         | 2019000104  | 300       | USD         |                            |                                 | 0002 - Signmedia Treasury | Installment No 2 in invoice |

    And the following Currencies exist:
      | Code | Name                 | ISO |
      | 0002 | United States Dollar | USD |
    And the following Service Items exist:
      | Code   | Type       | MarketName                         | BusinessUnit | CostFactor |
      | 000001 | COMMERCIAL | Hot Laminated Frontlit Fabric roll | 0002         | false      |
      | 000014 | SERVICE    | Insurance service                  | 0002         | true       |
      | 000057 | SERVICE    | Bank Fees                          | 0002         | true       |
    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |
    And the Company "0003 - HPS" has the following Treasuries:
      | Code | Name         |
      | 0007 | IVD Treasury |

    And edit session is "30" minutes

  #EBS-5058
  Scenario Outline: (01) Save PaymentDetails section after edit session expired for PaymentDetails (regardless data entry is correct or not) (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "<PaymentRequestCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "<PaymentRequestCode>" at "07-Jan-2019 09:41 AM" with the following values:
      | NetAmount   | Description   |
      | <NetAmount> | <Description> |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-07"

    Examples:
      | PaymentRequestCode | NetAmount | Description         |
      | 2019000010         | 2000      | changed for save hp |

  Scenario Outline: (02) Save PaymentDetails section after edit session expired & PaymentRequest is deleted (Exception Case)
    Given user is logged in as "Gehan.Ahmed"
    And another user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "<PaymentRequestCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    And "hr1" first deleted the PaymentRequest with code "<PaymentRequestCode>" successfully at "07-Jan-2019 09:41 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "<PaymentRequestCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | Description   |
      | <NetAmount> | <Description> |
    Then an error notification is sent to "Gehan.Ahmed" with the following message "Gen-msg-01"

    Examples:
      | PaymentRequestCode | NetAmount | Description         |
      | 2019000010         | 2000      | changed for save hp |

  Scenario Outline: (03) Save PaymentDetails section With malicious data entry or missing data due to dependency (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "<PaymentRequestCode>" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "<PaymentRequestCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | CurrencyCode   | Description   | CompanyBankId   | CostFactorItem   | Treasury   |
      | <NetAmount> | <CurrencyCode> | <Description> | <CompanyBankId> | <CostFactorItem> | <Treasury> |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page

    Examples:
      | PaymentRequestCode | NetAmount    | CurrencyCode | CompanyBankId | CostFactorItem | Treasury | Description                                                                                                                                                                                                                                                         |
      # Malicious Input: Amount
      | 2019000010         | 2000000000   |              | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      | 2019000010         | 6734.2227634 |              | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      | 2019000010         | -8345        |              | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      | 2019000010         | 0            |              | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      | 2019000010         | Ay7aga       |              | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      # Malicious Input: Currency
      | 2019000004         | 2000         | Ay7aga       | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      | 2019000004         | 2000         | 9999         | 17            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      # Malicious Input: Description
      | 2019000010         | 2000         |              | 17            |                |          | changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp changed for save hp |
      | 2019000010         | 2000         |              | 17            |                |          | changed for < save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for > save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for "" save hp                                                                                                                                                                                                                                              |
      | 2019000010         | 2000         |              | 17            |                |          | changed for ' save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for { save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for } save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for ? save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for = save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for \|\| save hp                                                                                                                                                                                                                                            |
      | 2019000010         | 2000         |              | 17            |                |          | changed for \| save hp                                                                                                                                                                                                                                              |
      | 2019000010         | 2000         |              | 17            |                |          | changed for ! save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for : save hp                                                                                                                                                                                                                                               |
      | 2019000010         | 2000         |              | 17            |                |          | changed for ; save hp                                                                                                                                                                                                                                               |
      #EBS-7277 enter treasury in case of bank PR
      | 2019000010         | 2000         |              | 17            |                | 0002     | changed for save hp                                                                                                                                                                                                                                                 |
      # Bank Account not belong to the company
      | 2019000012         | 2000         |              | 15            |                |          | changed for save hp                                                                                                                                                                                                                                                 |
      # EBS-7277 Treasury not belong to the company
      | 2019000038         | 300          | 0002         |               |                | 0007     | changed for save hp                                                                                                                                                                                                                                                 |
      # EBS-7277 Treasury schema violation
      | 2019000038         | 300          | 0002         |               |                | Ay7aga   | changed for save hp                                                                                                                                                                                                                                                 |
      # EBS-7277 enter bank in case of Cash PR
      | 2019000038         | 300          | 0002         | 17            |                | 0002     | changed for save hp                                                                                                                                                                                                                                                 |
      #      # Malicious Input: CostFactorITem
      | 2019000004         | 250          | 0001         | 17            | TextValue      |          | مصاريف بنكية                                                                                                                                                                                                                                                        |
      | 2019000004         | 250          | 0001         | 17            | 7777777        |          | مصاريف بنكية                                                                                                                                                                                                                                                        |
      #      # Not exist CostFactorITem
      | 2019000004         | 250          | 0001         | 17            | 124515         |          | مصاريف بنكية                                                                                                                                                                                                                                                        |
      #      # Commercial CostFactorITem
      | 2019000004         | 250          | 0001         | 17            | 000001         |          | مصاريف بنكية                                                                                                                                                                                                                                                        |

  Scenario Outline: (04) Save PaymentDetails section in state where this action is not allowed (Exception Case)
    Given user is logged in as "hr1"
    And PaymentDetails section of PaymentRequest with code "<PaymentRequestCode>" is locked by "hr1" at "07-Jan-2019 09:10 AM"
    When "hr1" saves PaymentDetails section of PaymentRequest with Code "<PaymentRequestCode>" at "07-Jan-2019 09:30 AM" with the following values:
      | NetAmount   | Description   |
      | <NetAmount> | <Description> |
    Then an error notification is sent to "hr1" with the following message "Gen-msg-07"

    Examples:
      | PaymentRequestCode | state       | NetAmount | Description |
      | 2019000013         | PaymentDone | 2000      | ay7aga      |

  Scenario: (05) Save PaymentDetails section, BankAccount when Payment Form is not BANK (Abuse Case)
    Given user is logged in as "Gehan.Ahmed"
    And PaymentDetails section of PaymentRequest with code "2019000038" is locked by "Gehan.Ahmed" at "07-Jan-2019 09:10 AM"
    When "Gehan.Ahmed" saves PaymentDetails section of PaymentRequest with Code "2019000038" at "07-Jan-2019 09:30 AM" with the following values:
      | DueDocumentType | NetAmount | Description                 | CompanyBankId |
      | INVOICE         | 200       | Installment No 2 in invoice | 14            |
    Then "Gehan.Ahmed" is logged out
    And "Gehan.Ahmed" is forwarded to the error page
