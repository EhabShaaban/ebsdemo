# Author Dev: Gehad Shady, Yara Ameen, Mohamed Aboelnour, Evram Hany
# Author Quality: Khadra Ali
# Author PO : Somaya Abolwafaa
Feature: View All Payment Request

  Background:
    Given the following users and roles exist:
      | Name         | Role                            |
      | Gehan.Ahmed  | PurchasingResponsible_Signmedia |
      | Ashraf.Salah | Corporate_Accounting_Head       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                        |
      | PurchasingResponsible_Signmedia | LimitedPaymentViewer_Signmedia |
      | Corporate_Accounting_Head       | PaymentViewer                  |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission             | Condition                                                         |
      | LimitedPaymentViewer_Signmedia | PaymentRequest:ReadAll | ([purchaseUnitName='Signmedia'] && [creationInfo='LoggedInUser']) |
      | PaymentViewer                  | PaymentRequest:ReadAll |                                                                   |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | PaymentRequest:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User        | Permission             | Condition                                                          |
      | Gehan.Ahmed | PaymentRequest:ReadAll | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser'])      |
      | Gehan.Ahmed | PaymentRequest:ReadAll | ([purchaseUnitName='Corrugated'] && [creationInfo='LoggedInUser']) |
      | Gehan.Ahmed | PaymentRequest:ReadAll | ([purchaseUnitName='Offset'] && [creationInfo='LoggedInUser'])     |
     #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                              | State | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000009 | CREDIT   | CREDIT_NOTE_BASED_ON_SALES_RETURN | Draft | Admin     | 06-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit | Company          |
      | 2019000009 | 0001 - Flexo | 0001 - AL Madina |

     #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner          | Amount | Currency   |
      | 2019000009 | 000006 - المطبعة الأمنية | 1000   | 0001 - EGP |

         #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type             | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2019000039 | SHIPMENT_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |

     #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit | Company        |
      | 2019000039 | 0001 - Flexo | 0002 - DigiPro |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy    | CreationDate         | Remaining | DocumentOwner |
      | 2019000005 | VENDOR                   | Draft       | Amr.Khalil   | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE | Draft       | Gehan.Ahmed  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil   | 05-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000002 | VENDOR                   | Draft       | Amr.Khalil   | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2021000001 | UNDER_SETTLEMENT         | Draft       | Gehan.Ahmed  | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
      | 2021000002 | UNDER_SETTLEMENT         | PaymentDone | Ashraf.Salah | 04-Jan-2021 09:02 AM | 1000      | Ashraf Salah  |
      | 2021000010 | BASED_ON_CREDIT_NOTE     | Draft       | Amr.Khalil   | 04-Jan-2021 09:02 AM |           | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company        |
      | 2019000005 | 0001 - Flexo      | 0002 - DigiPro |
      | 2019000004 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2019000003 | 0006 - Corrugated | 0002 - DigiPro |
      | 2019000002 | 0001 - Flexo      | 0002 - DigiPro |
      | 2021000001 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia  | 0002 - DigiPro |
      | 2021000010 | 0001 - Flexo      | 0002 - DigiPro |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner          | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber         | Treasury              | ExpectedDueDate      | BankTransRef | Description                  |
      | 2019000005 | BANK        | 000001 - Siegwerk        | INVOICE         | 2019000039  | 2000      | USD         | 000014 - Insurance service | Alex Bank - 1516171819789 |                       | 05-Sep-2018 09:02 AM |              | downpayment for po           |
      | 2019000004 | BANK        |                          | PURCHASEORDER   | 2018000010  | 250       | EGP         |                            | Alex Bank - 1516171819789 |                       | 05-Oct-2018 09:02 AM | 195326512    | Installment No 2 in invoice  |
      | 2019000003 | CASH        |                          | PURCHASEORDER   | 2018000045  | 100       | USD         |                            |                           | 0001 - Flexo Treasury | 05-Nov-2018 09:02 AM |              | Downpayent to a confirmed PO |
      | 2019000002 | BANK        | 000001 - Siegwerk        | PURCHASEORDER   | 2018000034  | 1000      | EUR         | 000014 - Insurance service | Alex Bank - 1516171819789 |                       | 05-Dec-2018 09:02 AM |              | مصاريف بنكية                 |
      | 2021000001 | CASH        | 000002 - Ahmed Hamdi     |                 |             | 1000      | USD         |                            |                           | 0001 - Flexo Treasury | 05-Jan-2019 09:02 AM |              | Under settlement             |
      | 2021000002 | BANK        | 000002 - Ahmed Hamdi     |                 |             | 1000      | EGP         |                            | Alex Bank - 1516171819789 |                       | 04-Feb-2020 09:02 AM | 195326920    | Under settlement             |
      | 2021000010 | BANK        | 000006 - المطبعة الأمنية | CREDITNOTE      | 2019000009  | 1000      | EGP         |                            | Alex Bank - 1516171819789 |                       | 04-Mar-2021 09:02 AM |              | payment under credit note    |
    And the total number of existing records are 7

  # EBS-6227, #EBS-7962
  Scenario: (01) View All PaymentRequests by authorized user without condition (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with no filter applied with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner          | CreatedBy    | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit      | State       |
      | 2021000010 | BASED_ON_CREDIT_NOTE     | 000006 - المطبعة الأمنية | Amr.Khalil   | Credit Note - 2019000009    | 1000 EGP | 04-Mar-2021 09:02 AM |              | 0001 - Flexo      | Draft       |
      | 2021000002 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi     | Ashraf.Salah |                             | 1000 EGP | 04-Feb-2020 09:02 AM | 195326920    | 0002 - Signmedia  | PaymentDone |
      | 2021000001 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi     | Gehan.Ahmed  |                             | 1000 USD | 05-Jan-2019 09:02 AM |              | 0002 - Signmedia  | Draft       |
      | 2019000005 | VENDOR                   | 000001 - Siegwerk        | Amr.Khalil   | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo      | Draft       |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                          | Gehan.Ahmed  | Purchase Order - 2018000010 | 250 EGP  | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia  | Draft       |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE |                          | Amr.Khalil   | Purchase Order - 2018000045 | 100 USD  | 05-Nov-2018 09:02 AM |              | 0006 - Corrugated | PaymentDone |
      | 2019000002 | VENDOR                   | 000001 - Siegwerk        | Amr.Khalil   | Purchase Order - 2018000034 | 1000 EUR | 05-Dec-2018 09:02 AM |              | 0001 - Flexo      | Draft       |
    And the total number of records in search results by "Ashraf.Salah" are 7

  # EBS-6227,  #EBS-7275
  Scenario: (02) View All PaymentRequests by authorized user due to condition on (Business Unit and CreatedBy Info) (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of PaymentRequests with no filter applied with 10 records per page
    Then the following PaymentRequests will be presented to "Gehan.Ahmed":
      | Code       | Type                     | BusinessPartner      | CreatedBy   | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit     | State |
      | 2021000001 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Gehan.Ahmed |                             | 1000 USD | 05-Jan-2019 09:02 AM |              | 0002 - Signmedia | Draft |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                      | Gehan.Ahmed | Purchase Order - 2018000010 | 250 EGP  | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia | Draft |
    And the total number of records in search results by "Gehan.Ahmed" are 2

  # EBS-6227,  #EBS-7275
  Scenario: (03) View All PaymentRequests filtered by Code by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on Code which contains "000004" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner | CreatedBy   | ReferenceDocument           | Amount  | ExpectedDueDate      | BankTransRef | BusinessUnit     | State |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                 | Gehan.Ahmed | Purchase Order - 2018000010 | 250 EGP | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 1

  # EBS-7275
  Scenario: (04) View All PaymentRequests filtered by Type by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on Type which equals "VENDOR" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type   | BusinessPartner   | CreatedBy  | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2019000005 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo | Draft |
      | 2019000002 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Purchase Order - 2018000034 | 1000 EUR | 05-Dec-2018 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 2

  # EBS-7275
  Scenario: (05) View All PaymentRequests filtered by BusinessPartner by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on BusinessPartner which equals "000001" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type   | BusinessPartner   | CreatedBy  | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2019000005 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo | Draft |
      | 2019000002 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Purchase Order - 2018000034 | 1000 EUR | 05-Dec-2018 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 2

  # EBS-7275
  Scenario: (06) View All PaymentRequests filtered by CreatedBy by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on CreatedBy which contains "Gehan" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner      | CreatedBy   | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit     | State |
      | 2021000001 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Gehan.Ahmed |                             | 1000 USD | 05-Jan-2019 09:02 AM |              | 0002 - Signmedia | Draft |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                      | Gehan.Ahmed | Purchase Order - 2018000010 | 250 EGP  | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 2

  # EBS-7275
  Scenario: (07) View All PaymentRequests filtered by ReferenceDocumentCode by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on ReferenceDocument which contains "03" with 10 records per page while current locale is "en-US"
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type   | BusinessPartner   | CreatedBy  | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2019000005 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo | Draft |
      | 2019000002 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Purchase Order - 2018000034 | 1000 EUR | 05-Dec-2018 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 2

  # EBS-7275 & EBS-7741
  Scenario Outline: (08) View All PaymentRequests filtered by ReferenceDocumentName by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on ReferenceDocument which contains "<ReferenceDocumentType>" with 10 records per page while current locale is "en-US"
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code   | Type   | BusinessPartner   | CreatedBy   | ReferenceDocument   | Amount   | BusinessUnit   | State   | ExpectedDueDate   | BankTransRef   |
      | <Code> | <Type> | <BusinessPartner> | <CreatedBy> | <ReferenceDocument> | <Amount> | <BusinessUnit> | <State> | <ExpectedDueDate> | <BankTransRef> |
    And the total number of records in search results by "Ashraf.Salah" are 1
    Examples:
      | ReferenceDocumentType | Code       | Type                 | BusinessPartner          | CreatedBy  | ReferenceDocument           | Amount   | BusinessUnit | State | ExpectedDueDate      | BankTransRef |
      | Vendor Invoice        | 2019000005 | VENDOR               | 000001 - Siegwerk        | Amr.Khalil | Vendor Invoice - 2019000039 | 2000 USD | 0001 - Flexo | Draft | 05-Sep-2018 09:02 AM |              |
      | Credit Note           | 2021000010 | BASED_ON_CREDIT_NOTE | 000006 - المطبعة الأمنية | Amr.Khalil | Credit Note - 2019000009    | 1000 EGP | 0001 - Flexo | Draft | 04-Mar-2021 09:02 AM |              |

  # EBS-7275
  Scenario: (09) View All PaymentRequests filtered by Amount by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on Amount which equals "100" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner | CreatedBy  | ReferenceDocument           | Amount  | ExpectedDueDate      | BankTransRef | BusinessUnit      | State       |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE |                 | Amr.Khalil | Purchase Order - 2018000045 | 100 USD | 05-Nov-2018 09:02 AM |              | 0006 - Corrugated | PaymentDone |
    And the total number of records in search results by "Ashraf.Salah" are 1

  # EBS-7275
  Scenario: (10) View All PaymentRequests filtered by Currency by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on Currency which equals "USD" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner      | CreatedBy   | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit      | State       |
      | 2021000001 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Gehan.Ahmed |                             | 1000 USD | 05-Jan-2019 09:02 AM |              | 0002 - Signmedia  | Draft       |
      | 2019000005 | VENDOR                   | 000001 - Siegwerk    | Amr.Khalil  | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo      | Draft       |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE |                      | Amr.Khalil  | Purchase Order - 2018000045 | 100 USD  | 05-Nov-2018 09:02 AM |              | 0006 - Corrugated | PaymentDone |
    And the total number of records in search results by "Ashraf.Salah" are 3

  # EBS-7275
  Scenario: (11) View All PaymentRequests filtered by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on BusinessUnit which equals "0002" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner      | CreatedBy    | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit     | State       |
      | 2021000002 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Ashraf.Salah |                             | 1000 EGP | 04-Feb-2020 09:02 AM | 195326920    | 0002 - Signmedia | PaymentDone |
      | 2021000001 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Gehan.Ahmed  |                             | 1000 USD | 05-Jan-2019 09:02 AM |              | 0002 - Signmedia | Draft       |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                      | Gehan.Ahmed  | Purchase Order - 2018000010 | 250 EGP  | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia | Draft       |
    And the total number of records in search results by "Ashraf.Salah" are 3

  # EBS-7741
  Scenario: (12) View All PaymentRequests filtered by Type by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on Type which equals "BASED_ON_CREDIT_NOTE" with 10 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                 | BusinessPartner          | CreatedBy  | ReferenceDocument        | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2021000010 | BASED_ON_CREDIT_NOTE | 000006 - المطبعة الأمنية | Amr.Khalil | Credit Note - 2019000009 | 1000 EGP | 04-Mar-2021 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 1

  # EBS-6227,  #EBS-7275
  Scenario: (13) View All PaymentRequests filtered by State by authorized user + Paging (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 2 of PaymentRequests with filter applied on State which contains "Draft" with 2 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type   | BusinessPartner   | CreatedBy  | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2019000005 | VENDOR | 000001 - Siegwerk | Amr.Khalil | Vendor Invoice - 2019000039 | 2000 USD | 05-Sep-2018 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 5

  # EBS-8333
  Scenario: (14) View All PaymentRequests filtered by BankTransRef by authorized user + Paging (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on BankTransRef which contains "195326" with 4 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                     | BusinessPartner      | CreatedBy    | ReferenceDocument           | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit     | State       |
      | 2021000002 | UNDER_SETTLEMENT         | 000002 - Ahmed Hamdi | Ashraf.Salah |                             | 1000 EGP | 04-Feb-2020 09:02 AM | 195326920    | 0002 - Signmedia | PaymentDone |
      | 2019000004 | OTHER_PARTY_FOR_PURCHASE |                      | Gehan.Ahmed  | Purchase Order - 2018000010 | 250 EGP  | 05-Oct-2018 09:02 AM | 195326512    | 0002 - Signmedia | Draft       |
    And the total number of records in search results by "Ashraf.Salah" are 2

  # EBS-8333
  Scenario: (15) View All PaymentRequests filtered by ExpectedDueDate by authorized user + Paging (Happy Path)
    Given user is logged in as "Ashraf.Salah"
    When "Ashraf.Salah" requests to read page 1 of PaymentRequests with filter applied on ExpectedDueDate from "10-Feb-2021" to "10-Mar-2021" with 2 records per page
    Then the following PaymentRequests will be presented to "Ashraf.Salah":
      | Code       | Type                 | BusinessPartner          | CreatedBy  | ReferenceDocument        | Amount   | ExpectedDueDate      | BankTransRef | BusinessUnit | State |
      | 2021000010 | BASED_ON_CREDIT_NOTE | 000006 - المطبعة الأمنية | Amr.Khalil | Credit Note - 2019000009 | 1000 EGP | 04-Mar-2021 09:02 AM |              | 0001 - Flexo | Draft |
    And the total number of records in search results by "Ashraf.Salah" are 1

  # EBS-7275
  Scenario: (16) View All PaymentRequests filtered BusinessUnit by authorized user - No search results (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read page 1 of PaymentRequests with filter applied on BusinessUnit which equals "0006" with 10 records per page
    Then no values are displayed to "Gehan.Ahmed" and empty grid state is viewed in Home Screen
    And the total number of records in search results by "Gehan.Ahmed" are 0

  # EBS-6227,  #EBS-7275
  Scenario: (17) View All PaymentRequests by unauthorized user due to role (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of PaymentRequests with no filter applied with 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
    
    
