Feature: Allowed Action For Payment Request(Home Screen And Object Screen)

  Background:
    Given the following users and roles exist:
      | Name               | Role                        |
      | hr1                | SuperUser                   |
      | Afaf               | FrontDesk                   |
      | CreateOnlyUser     | CreateOnlyRole              |
      | CannotReadSections | CannotReadSectionsRole      |
      | Amr.Khalil         | PurchasingResponsible_Flexo |
    And the following roles and sub-roles exist:
      | Role                        | Subrole                   |
      | SuperUser                   | SuperUserSubRole          |
      | CreateOnlyRole              | CreateOnlySubRole         |
      | CannotReadSectionsRole      | CannotReadSectionsSubRole |
      | PurchasingResponsible_Flexo | PaymentOwner_Flexo        |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission              | Condition                                                     |
      | SuperUserSubRole          | *:*                     |                                                               |
      | CreateOnlySubRole         | PaymentRequest:Create   |                                                               |
      | CannotReadSectionsSubRole | PaymentRequest:Delete   |                                                               |
      | CannotReadSectionsSubRole | PaymentRequest:Activate |                                                               |
      | CannotReadSectionsSubRole | PaymentRequest:ReadAll  |                                                               |
      | PaymentOwner_Flexo        | PaymentRequest:Delete   | ([purchaseUnitName='Flexo'] && [creationInfo='LoggedInUser']) |

    And the following users doesn't have the following permissions:
      | User               | Permission                           |
      | Afaf               | PaymentRequest:Create                |
      | Afaf               | PaymentRequest:ReadAll               |
      | Afaf               | PaymentRequest:ReadCompanyData       |
      | Afaf               | PaymentRequest:ReadPaymentDetails    |
      | Afaf               | PaymentRequest:ReadAccountingDetails |
      | Afaf               | PaymentRequest:ReadPostingDetails    |
      | Afaf               | PaymentRequest:EditPaymentDetails    |
      | Afaf               | PaymentRequest:Delete                |
      | Afaf               | PaymentRequest:Activate              |
      | CreateOnlyUser     | PaymentRequest:ReadAll               |
      | CreateOnlyUser     | PaymentRequest:ReadCompanyData       |
      | CreateOnlyUser     | PaymentRequest:ReadPaymentDetails    |
      | CreateOnlyUser     | PaymentRequest:ReadAccountingDetails |
      | CreateOnlyUser     | PaymentRequest:ReadPostingDetails    |
      | CreateOnlyUser     | PaymentRequest:EditPaymentDetails    |
      | CreateOnlyUser     | PaymentRequest:Delete                |
      | CreateOnlyUser     | PaymentRequest:Activate              |
      | CannotReadSections | PaymentRequest:ReadCompanyData       |
      | CannotReadSections | PaymentRequest:ReadPaymentDetails    |
      | CannotReadSections | PaymentRequest:ReadAccountingDetails |
      | CannotReadSections | PaymentRequest:ReadPostingDetails    |
      | CannotReadSections | PaymentRequest:EditPaymentDetails    |

     #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy  | CreationDate         | Remaining | DocumentOwner |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil | 05-Aug-2018 09:02 AM | 100       | Ashraf Salah  |
      | 2019000002 | VENDOR                   | Draft       | Amr.Khalil | 05-Aug-2018 09:02 AM |           | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit      | Company        |
      | 2019000003 | 0006 - Corrugated | 0002 - DigiPro |
      | 2019000002 | 0001 - Flexo      | 0002 - DigiPro |

  Scenario Outline: (01) Read allowed actions in Payment Request Object Screen - All States - (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of PaymentRequest with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User               | Code       | State       | AllowedActions                                                                                                         |
      | hr1                | 2019000002 | Draft       | EditPaymentDetails,Delete,ReadAll,ReadCompanyData,ReadPaymentDetails,ReadAccountingDetails,ReadPostingDetails,Activate |
      | hr1                | 2019000003 | PaymentDone | ReadAll,ReadCompanyData,ReadPaymentDetails,ReadAccountingDetails,ReadPostingDetails                                    |
      | CannotReadSections | 2019000002 | Draft       | ReadAll,Delete, Activate                                                                                               |
      | CannotReadSections | 2019000003 | PaymentDone | ReadAll                                                                                                                |


  Scenario Outline: (02) Read allowed actions in Payment Request Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of PaymentRequest with code "<PRCode>"
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
    Examples:
      | PRCode     |
      | 2019000002 |
      | 2019000003 |

  Scenario: (03) Read allowed actions in Payment Request Object Screen - Object does not exist (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the PaymentRequests with code "2019000002" successfully
    When "Amr.Khalil" requests to read actions of PaymentRequest with code "2019000002"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"

  Scenario: (04) Read allowed actions in Payment Request Home Screen - (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read actions of PaymentRequest in home screen
    Then the following actions are displayed to "hr1":
      | AllowedActions |
      | ReadAll        |
      | Create         |

  Scenario: (05) Read allowed actions in Payment Request Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of PaymentRequest in home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
