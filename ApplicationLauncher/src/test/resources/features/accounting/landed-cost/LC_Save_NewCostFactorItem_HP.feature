# Author Dev: Evram Hany
# Author Quality: Kahdra Ali
# Reviewer: Mohamed Aboelnour (13-Oct-2020)
Feature: LC Save CostFactorItem (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | Accountant_Signmedia | ItemViewer_Signmedia      |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                      | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia      | Item:ReadAll                    | [purchaseUnitName='Signmedia'] |
    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit     |
      | 2020100002 | Draft | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia |
      | 2020100004 | Draft | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 000055 - Insurance service2 | 300 EGP        | null EGP    | null EGP   |
      | 2020100002 | 000057 - Bank Fees          | 600 EGP        | null EGP    | null EGP   |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 900 EGP        | null EGP    | null EGP   |
    And the following CostFactorItems exist with the following details:
      | Item                        | PurchaseUnit     |
      | 000055 - Insurance service2 | 0002 - Signmedia |
      | 000056 - Shipment service   | 0001 - Flexo     |
      | 000057 - Bank Fees          | 0002 - Signmedia |
      | 000014 - Insurance service  | 0002 - Signmedia |

  #EBS-5425
  Scenario: (01) Save new LandedCost CostFactorItem "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session In Import PO (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100004" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100004" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                       | Value      |
      | 000014 - Insurance service | 200.125465 |
    Then LandedCost with Code "2020100004" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2019 11:10 AM | Draft |
    And CostFactorItems section of LandedCot with code "2020100004" is created as follows:
      | Item                       | EstimatedValue | ActualValue | Difference |
      | 000014 - Insurance service | 200.125465 EGP | null EGP    | null EGP   |
    And CostFactorItemsSummary section is updated as follows:
      | LCCode     | EstimatedValue | ActualValue | Difference  |
      | 2020100004 | 200.125465 EGP | null.00 EGP | null.00 EGP |
    And the lock by "Shady.Abdelatif" on CostFactorItems section of LandedCost with Code "2020100004" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-16"

  #EBS-5425
  Scenario: (02) Save new LandedCost CostFactorItem "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session In Local PO (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                       | Value |
      | 000014 - Insurance service | 200   |
    Then LandedCost with Code "2020100002" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2019 11:10 AM | Draft |
    And CostFactorItems section of LandedCot with code "2020100002" is created as follows:
      | Item                        | EstimatedValue | ActualValue | Difference |
      | 000057 - Bank Fees          | 600 EGP        | null EGP    | null EGP   |
      | 000055 - Insurance service2 | 300 EGP        | null EGP    | null EGP   |
      | 000014 - Insurance service  | 200.0 EGP      | null EGP    | null EGP   |
    And CostFactorItemsSummary section is updated as follows:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 1100.0 EGP     | null EGP    | null EGP   |
    And the lock by "Shady.Abdelatif" on CostFactorItems section of LandedCost with Code "2020100002" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-16"
