#@Author: Mohamed Aboelnour
# updated by: Fatma Al Zahraa (EBS - 6851)
#@Reviewer: Marina Salah, Eslam Ayman
Feature: Allowed Action For Landed Cost (Object Screen)

  Background:
    Given the following users and roles exist:
      | Name               | Role                   |
      | Shady.Abdelatif    | Accountant_Signmedia   |
      | CannotReadSections | CannotReadSectionsRole |
      | Afaf               | FrontDesk              |
      | hr1                | SuperUser              |
    And the following roles and sub-roles exist:
      | Role                   | Subrole                    |
      | Accountant_Signmedia   | LandedCostViewer_Signmedia |
      | Accountant_Signmedia   | LandedCostOwner_Signmedia  |
      | CannotReadSectionsRole | CannotReadSectionsSubRole  |
      | SuperUser              | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | LandedCostOwner_Signmedia  | LandedCost:Delete                | [purchaseUnitName='Signmedia'] |
      | LandedCostOwner_Signmedia  | LandedCost:UpdateCostFactorItem  | [purchaseUnitName='Signmedia'] |
      | LandedCostOwner_Signmedia  | LandedCost:ConvertToEstimate     | [purchaseUnitName='Signmedia'] |
      | LandedCostOwner_Signmedia  | LandedCost:ConvertToActual       | [purchaseUnitName='Signmedia'] |
      | LandedCostOwner_Signmedia  | LandedCost:CollectActualCost     | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer_Signmedia | LandedCost:ReadAll               | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer_Signmedia | LandedCost:ReadGeneralData       | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer_Signmedia | LandedCost:ReadCompanyData       | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer_Signmedia | LandedCost:ReadLandedCostDetails | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer_Signmedia | LandedCost:ReadCostFactorItem    | [purchaseUnitName='Signmedia'] |
      | CannotReadSectionsSubRole  | LandedCost:Delete                |                                |
      | CannotReadSectionsSubRole  | LandedCost:ConvertToEstimate     |                                |
      | CannotReadSectionsSubRole  | LandedCost:ConvertToActual       |                                |
      | SuperUserSubRole           | *:*                              |                                |
    And the following users doesn't have the following permissions:
      | User               | Permission                       |
      | Afaf               | LandedCost:Delete                |
      | Afaf               | LandedCost:UpdateCostFactorItem  |
      | Afaf               | LandedCost:ConvertToEstimate     |
      | Afaf               | LandedCost:ConvertToActual       |
      | Afaf               | LandedCost:CollectActualCost     |
      | Afaf               | LandedCost:ReadAll               |
      | Afaf               | LandedCost:ReadGeneralData       |
      | Afaf               | LandedCost:ReadCompanyData       |
      | Afaf               | LandedCost:ReadLandedCostDetails |
      | Afaf               | LandedCost:ReadCostFactorItem    |
      | CannotReadSections | LandedCost:UpdateCostFactorItem  |
      | CannotReadSections | LandedCost:CollectActualCost     |
      | CannotReadSections | LandedCost:ReadAll               |
      | CannotReadSections | LandedCost:ReadCompanyData       |
      | CannotReadSections | LandedCost:ReadLandedCostDetails |
      | CannotReadSections | LandedCost:ReadCostFactorItem    |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | LandedCost:Delete                | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:UpdateCostFactorItem  | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ConvertToEstimate     | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ConvertToActual       | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:CollectActualCost     | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ReadAll               | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ReadGeneralData       | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ReadCompanyData       | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ReadLandedCostDetails | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | LandedCost:ReadCostFactorItem    | [purchaseUnitName='Flexo'] |
    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit     |
      | 2020000001 | Draft                    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020000002 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020000003 | ActualValuesCompleted    | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia |
      | 2020000004 | ActualValuesCompleted    | IMPORT - Landed Cost For Import PO | 0001 - Flexo     |

  # EBS-6775
  Scenario Outline: (01) Read allowed actions in Landed Cost Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of LandedCost with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | AllowedActions                                                                                                                        | NoOfActions |
      | Shady.Abdelatif    | 2020000001 | ReadAll, ReadGeneralData, ReadCompanyData, ReadLandedCostDetails, ReadCostFactorItem, Delete, UpdateCostFactorItem, ConvertToEstimate | 8           |
      | Shady.Abdelatif    | 2020000002 | ReadAll, ReadGeneralData, ReadCompanyData, ReadLandedCostDetails, ReadCostFactorItem, CollectActualCost, ConvertToActual              | 7           |
      | Shady.Abdelatif    | 2020000003 | ReadAll, ReadGeneralData, ReadCompanyData, ReadLandedCostDetails, ReadCostFactorItem                                                  | 5           |
      | CannotReadSections | 2020000001 | Delete, ConvertToEstimate, ReadGeneralData                                                                                            | 3           |
      | CannotReadSections | 2020000002 | ConvertToActual, ReadGeneralData                                                                                                      | 2           |
      | CannotReadSections | 2020000003 | ReadGeneralData                                                                                                                       | 1           |

  # EBS-6775
  Scenario Outline: (02) Read allowed actions in Landed Cost Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of LandedCost with code "<LCCode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | LCCode     |
      | Shady.Abdelatif | 2020000004 |
      | Afaf            | 2020000001 |
      | Afaf            | 2020000002 |
      | Afaf            | 2020000003 |
      | Afaf            | 2020000004 |

  # EBS-6775
  Scenario: (03) Read allowed actions in Landed Cost Object Screen - Object does not exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020000001" successfully
    When "Shady.Abdelatif" requests to read actions of LandedCost with code "2020000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
