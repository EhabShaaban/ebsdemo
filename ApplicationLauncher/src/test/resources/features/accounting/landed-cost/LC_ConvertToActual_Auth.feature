# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
# Reviewer:

Feature: LandedCost ConvertToActual (Auth)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                 | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:ConvertToActual | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission                 |
      | Afaf | LandedCost:ConvertToActual |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                       |
      | Shady.Abdelatif | LandedCost:ConvertToActual | [purchaseUnitName='Corrugated'] |

    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0006 | Corrugated |

    And the following LandedCosts exist:
      | Code       | State                    | Type                             | BusinessUnit      |
      | 2020100003 | EstimatedValuesCompleted | LOCAL - Landed Cost For Local PO | 0006 - Corrugated |

  #EBS-7604
  Scenario: (01) LandedCost ConvertToActual by unauthorized user (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" ConvertToActual LandedCost with code "2020100003" at "07-Jan-2019 09:30 AM":
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7604
  Scenario: (02) LandedCost ConvertToActual by unauthorized user due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToActual LandedCost with code "2020100003" at "07-Jan-2019 09:30 AM":
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
