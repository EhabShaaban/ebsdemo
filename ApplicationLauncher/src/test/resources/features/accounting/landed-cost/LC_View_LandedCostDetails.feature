# Author Dev: Evraam Hany
# Reviewer Quality: Khadrah Ali and Eslam Salam
# Reviewer: Somaya Ahmed and Hosam Bayomy (4-Aug-2020)

Feature: View LandedCostDetails section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Seif      | Quality_Specialist   |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | Quality_Specialist   | LandedCostViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | SuperUserSubRole           | *:*                              |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadLandedCostDetails | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer           | LandedCost:ReadLandedCostDetails |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | LandedCost:ReadGeneralData | [purchaseUnitName='Flexo'] |
    And the following LandedCosts exist:
      | Code       | State                 | Type                               | BusinessUnit     |
      | 2020000001 | Draft                 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020100004 | Draft                 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020000004 | ActualValuesCompleted | IMPORT - Landed Cost For Import PO | 0001 - Flexo     |
    And the following LandedCostDetails for LandedCost exist:
      | LCCode     | GoodsInvoice | TotalAmount | PurchaseOrder | DamageStock |
      | 2020000001 | 2019000040   | 20008.8 USD | 2018000008    | true        |
      | 2020100004 | 2019000002   | 224.00 USD  | 2018000010    | true        |
      | 2020000004 | 2019000109   | 720.0 USD   | 2018000003    | false       |


  # EBS-6719
  Scenario: (01) View landedCostDetails section in LandedCost by an authorized user due to condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view landedCostDetails section of LandedCost with code "2020000001"
    Then the following values of LandedCostSection for LandedCost with code "2020000001" are displayed to "Shady.Abdelatif":
      | LCCode     | GoodsInvoice | TotalAmount | PurchaseOrder | DamageStock |
      | 2020000001 | 2019000040   | 20008.8 USD | 2018000008    | true        |

  # EBS-6719
  Scenario Outline: (02) View landedCostDetails section in LandedCost by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view landedCostDetails section of LandedCost with code "<LCCode>"
    Then the following values of LandedCostSection for LandedCost with code "<LCCode>" are displayed to "Ahmed.Seif":
      | LCCode   | GoodsInvoice   | TotalAmount   | PurchaseOrder   | DamageStock   |
      | <LCCode> | <GoodsInvoice> | <TotalAmount> | <PurchaseOrder> | <DamageStock> |
    Examples:
      | LCCode     | GoodsInvoice | TotalAmount | PurchaseOrder | DamageStock |
      | 2020000001 | 2019000040   | 20008.8 USD | 2018000008    | true        |
      | 2020000004 | 2019000109   | 720.0 USD   | 2018000003    | false       |

  # EBS-6719
  Scenario: (04) View landedCostDetails section in LandedCost where LandedCost doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "Shady.Abdelatif"
    And "Shady.Abdelatif" first deleted the LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to view landedCostDetails section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
    
   # EBS-6719
  Scenario: (03) View landedCostDetails section in LandedCost by unauthorized  user due to condition (Authorization error)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view landedCostDetails section of LandedCost with code "2020000004"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
