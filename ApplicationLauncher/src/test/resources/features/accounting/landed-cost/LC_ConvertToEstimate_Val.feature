#Auther Dev: Ahmed Ali
#Auther QA: Eslam Salam
#Reviewer: Khadra Ali
Feature: LandedCost ConvertToEstimat (Val)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | SuperUser            | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                   | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:ConvertToEstimate | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole          | *:*                          |                                |
    And the following Import PurchaseOrders exist:
      | Code       | State     | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000008 | Confirmed | 0002           | 0002    |      |               |
      | 2018000010 | Shipped   | 0002           | 0002    |      |               |
    And the following Local PurchaseOrders exist:
      | Code       | State   | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000047 | Shipped | 0002           | 0002    |      |               |
    And the following GeneralData for LandedCosts exist:
      | LCCode     | Type                               | BusinessUnit     | CreatedBy   | State                    | CreationDate         | PurchaseOrder | DocumentOwner   |
      | 2020000001 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia | Gehan.Ahmed | Draft                    | 24-Jun-2020 09:02 AM | 2018000008    | Shady.Abdelatif |
      | 2020000002 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia | Gehan.Ahmed | EstimatedValuesCompleted | 24-Jun-2020 09:02 AM | 2018000010    | Shady.Abdelatif |
      | 2020000003 | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia | Gehan.Ahmed | ActualValuesCompleted    | 24-Jun-2020 09:02 AM | 2018000047    | Shady.Abdelatif |
      | 2020100004 | IMPORT - Landed Cost For Import PO | 0002 - Signmedia | Gehan.Ahmed | Draft                    | 06-Jul-2020 12:10 PM | 2018000010    | Shady.Abdelatif |
      | 2020100000 | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia | Gehan.Ahmed | Draft                    | 06-Mar-2020 12:10 PM | 2018000047    | Shady.Abdelatif |

  #EBS-6523
  Scenario: (01) LandedCost ConvertToEstimate that delete by another user (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020000001" successfully
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020000001" at "07-Sep-2020 09:30 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-6523
  Scenario: (02) LandedCost ConvertToEstimate that already EstimatedValuesCompleted (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020000002" at "07-Sep-2020 09:30 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-32"

  #EBS-6523
  Scenario: (03) LandedCost ConvertToEstimate that already ActualValuesCompleted (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020000003" at "07-Sep-2020 09:30 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-32"

  #EBS-6523
  Scenario: (04) LandedCost ConvertToEstimate that is Draft but its PO has Estimated LC (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020100004" at "07-Sep-2020 09:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "purchaseOrderCode" field "Lc-msg-01" and sent to "Shady.Abdelatif"

  #EBS-6523
  Scenario: (05) LandedCost ConvertToEstimate that is Draft but its PO in Cancelled state (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" requests to cancel the PurchaseOrder with Code "2018000008" at "07-Sep-2020 9:30 AM"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020000001" at "07-Sep-2020 09:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "purchaseOrderCode" field "Gen-msg-32" and sent to "Shady.Abdelatif"

  #EBS-6523
  Scenario: (06) LandedCost ConvertToEstimate that has no cost factor items (Validation failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020100000" at "07-Sep-2020 09:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "itemCode" field "Gen-msg-40" and sent to "Shady.Abdelatif"
