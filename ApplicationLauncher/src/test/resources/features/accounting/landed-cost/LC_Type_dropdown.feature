# Author Dev: Evraam Hany
# Author Quality: Eslam Salam
# Reviewer: Somaya Ahmed (24-Jul-2020)

Feature: View All LC types (Dropdown)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role             |
      | Accountant_Signmedia | LandedCostTypeViewer |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition |
      | LandedCostTypeViewer | LandedCostType:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | LandedCostType:ReadAll |

    And the following are ALL exisitng LandedCostTypes:
      | Code   | Name                      |
      | IMPORT | Landed Cost For Import PO |
      | LOCAL  | Landed Cost For Local PO  |
    And the total number of existing LandedCostTypes are 2

    #EBS-6779
  Scenario: (01) Read list of LandedCostTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all LandedCostTypes
    Then the following LandedCost Types values will be presented to "Shady.Abdelatif":
      | LandedCostTypes                    |
      | IMPORT - Landed Cost For Import PO |
      | LOCAL - Landed Cost For Local PO   |
    And total number of LandedCost Types returned to "Shady.Abdelatif" is equal to 2

   #EBS-6779
  Scenario: (02) Read list of LandedCostTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all LandedCostTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
