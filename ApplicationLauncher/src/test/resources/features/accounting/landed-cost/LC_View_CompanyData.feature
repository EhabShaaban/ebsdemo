#Dev Author: Ahmed Ali
#Tester Author: khadrah Ali
Feature: LandedCost - View CompanyData section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Seif      | Quality_Specialist   |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | Quality_Specialist   | LandedCostViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                      |
      | SuperUserSubRole           | *:*                        |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadCompanyData | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer           | LandedCost:ReadCompanyData |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | LandedCost:ReadCompanyData | [purchaseUnitName='Flexo'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following ComapnyData for LandedCosts exist:
      | LCCode     | BusinessUnit     | Company        |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020100004 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000004 | 0001 - Flexo     | 0002 - DigiPro |

  #EBS-6731
  Scenario: (01) View CompanyData section in LandedCost, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view CompanyData section of LandedCost with code "2020000001"
    Then the following values of CompanyData section for LandedCost with code "2020000001" are displayed to "Shady.Abdelatif":
      | BusinessUnit | Company |
      | Signmedia    | DigiPro |

  #EBS-6731
  Scenario Outline: (02) View CompanyData section in LandedCost, by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view CompanyData section of LandedCost with code "<LCCode>"
    Then the following values of CompanyData section for LandedCost with code "<LCCode>" are displayed to "Ahmed.Seif":
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |

    Examples:
      | LCCode     | BusinessUnit | Company |
      | 2020000001 | Signmedia    | DigiPro |
      | 2020000004 | Flexo        | DigiPro |

  #EBS-6731
  Scenario: (03) View CompanyData section in LandedCost, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view CompanyData section of LandedCost with code "2020000004"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  Scenario: (04) View CompanyData section in LandedCost, where LandedCost doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to view CompanyData section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
