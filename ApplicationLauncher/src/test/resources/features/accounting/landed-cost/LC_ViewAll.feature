#@Author: Yara, Ahmed Ali
#@Reviewer: Eslam Ayman
#@Reviewer: Somaya Ahmed (8-Jul-2020)
Feature: View All Landed Cost

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | M.D                  | LandedCostViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | LandedCostViewer_Signmedia | LandedCost:ReadAll | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer           | LandedCost:ReadAll |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission         | Condition                       |
      | Shady.Abdelatif | LandedCost:ReadAll | [purchaseUnitName='Corrugated'] |

    #@INSERT
    And the following GeneralData for PurchaseOrders exist:
      | Code       | Type      | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2018000045 | IMPORT_PO | DeliveryComplete | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000010 | IMPORT_PO | Shipped          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000037 | IMPORT_PO | Arrived          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000019 | LOCAL_PO  | Approved         | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000049 | LOCAL_PO  | Shipped          | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2018000047 | LOCAL_PO  | DeliveryComplete | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000100 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000101 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000102 | LOCAL_GOODS_INVOICE  | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000103 | LOCAL_GOODS_INVOICE  | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000104 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000105 | LOCAL_GOODS_INVOICE  | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                    | CreatedBy    | CreationDate         | DocumentOwner   |
      | 2020100005 | IMPORT - Landed Cost For Import PO | Draft                    | Amr.Khalil   | 06-Aug-2020 12:10 PM | Shady.Abdelatif |
      | 2020100004 | IMPORT - Landed Cost For Import PO | Draft                    | Gehan.Ahmed  | 06-Jul-2020 12:10 PM | Shady.Abdelatif |
      | 2020100003 | LOCAL - Landed Cost For Local PO   | EstimatedValuesCompleted | Amr.Khalil   | 06-Jun-2020 12:10 PM | Shady.Abdelatif |
      | 2020100002 | LOCAL - Landed Cost For Local PO   | Draft                    | Amr.Khalil   | 06-May-2020 12:10 PM | Shady.Abdelatif |
      | 2020100001 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted    | Gehan.Ahmed  | 06-Apr-2020 12:10 PM | Shady.Abdelatif |
      | 2020100000 | LOCAL - Landed Cost For Local PO   | Draft                    | Ashraf.Salah | 06-Mar-2020 12:10 PM | Shady.Abdelatif |

    #@INSERT|
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit      | Company          |
      | 2020100005 | 0006 - Corrugated | 0001 - AL Madina |
      | 2020100004 | 0002 - Signmedia  | 0001 - AL Madina |
      | 2020100003 | 0006 - Corrugated | 0001 - AL Madina |
      | 2020100002 | 0002 - Signmedia  | 0001 - AL Madina |
      | 2020100001 | 0002 - Signmedia  | 0002 - DigiPro   |
      | 2020100000 | 0002 - Signmedia  | 0002 - DigiPro   |

       #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode |
      | 2020100005 | 2018000045 | 2019000100        | true        | VendorInvoice  |
      | 2020100004 | 2018000010 | 2019000101        | false       | VendorInvoice  |
      | 2020100003 | 2018000037 | 2019000102        | false       | VendorInvoice  |
      | 2020100002 | 2018000019 | 2019000103        | true        | VendorInvoice  |
      | 2020100001 | 2018000049 | 2019000104        | true        | VendorInvoice  |
      | 2020100000 | 2018000047 | 2019000105        | false       | VendorInvoice  |
    And the total number of all exisitng LandedCosts records are 6

  #EBS-6779
  Scenario: (01) View All LandedCosts by authorized user due to condition on (Business Unit) (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of LandedCosts with no filter applied 10 records per page
    Then the following LandedCosts will be presented to "Shady.Abdelatif":
      | Code       | State                 | CreationDate         | Type                               | PurchaseOrder | BusinessUnit     |
      | 2020100004 | Draft                 | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia |
      | 2020100002 | Draft                 | 06-May-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000019    | 0002 - Signmedia |
      | 2020100001 | ActualValuesCompleted | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia |
      | 2020100000 | Draft                 | 06-Mar-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000047    | 0002 - Signmedia |
    And the total number of records in search results by "Shady.Abdelatif" are 4

  #EBS-6779
  Scenario: (02) View All LandedCosts by authorized user due to condition on (Business Unit) + Paging (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 2 of LandedCosts with no filter applied 2 records per page
    Then the following LandedCosts will be presented to "Shady.Abdelatif":
      | Code       | State                 | CreationDate         | Type                               | PurchaseOrder | BusinessUnit     |
      | 2020100001 | ActualValuesCompleted | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia |
      | 2020100000 | Draft                 | 06-Mar-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000047    | 0002 - Signmedia |
    And the number of records in search results by "Shady.Abdelatif" on page "2" are 2
    And the total number of records in search results by "Shady.Abdelatif" are 4

  #EBS-6779
  Scenario: (03) View All LandedCosts by authorized user without condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with no filter applied 10 records per page
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State                    | CreationDate         | Type                               | PurchaseOrder | BusinessUnit      |
      | 2020100005 | Draft                    | 06-Aug-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000045    | 0006 - Corrugated |
      | 2020100004 | Draft                    | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia  |
      | 2020100003 | EstimatedValuesCompleted | 06-Jun-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000037    | 0006 - Corrugated |
      | 2020100002 | Draft                    | 06-May-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000019    | 0002 - Signmedia  |
      | 2020100001 | ActualValuesCompleted    | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia  |
      | 2020100000 | Draft                    | 06-Mar-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000047    | 0002 - Signmedia  |
    And the total number of records in search results by "Ashraf.Fathi" are 6

  #EBS-6779
  Scenario: (04) View All LandedCosts filtered by Code by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on Code which contains "004" with 10 records per page
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State | CreationDate         | Type                               | PurchaseOrder | BusinessUnit     |
      | 2020100004 | Draft | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-6779
  Scenario: (05) View All LandedCosts filtered by State by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on State which equals "Draft" with 10 records per page
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State | CreationDate         | Type                               | PurchaseOrder | BusinessUnit      |
      | 2020100005 | Draft | 06-Aug-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000045    | 0006 - Corrugated |
      | 2020100004 | Draft | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia  |
      | 2020100002 | Draft | 06-May-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000019    | 0002 - Signmedia  |
      | 2020100000 | Draft | 06-Mar-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000047    | 0002 - Signmedia  |
    And the total number of records in search results by "Ashraf.Fathi" are 4

  #EBS-6779
  Scenario: (06) View All LandedCosts filtered by CreationDate by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on CreationDate which equals "06-Apr-2020" with 10 records per page
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State                 | CreationDate         | Type                               | PurchaseOrder | BusinessUnit     |
      | 2020100001 | ActualValuesCompleted | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-6779
  Scenario: (07) View All LandedCosts filtered by Type by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on Type which equals "IMPORT" with 10 records per page while current locale is "en-US"
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State                 | CreationDate         | Type                               | PurchaseOrder | BusinessUnit      |
      | 2020100005 | Draft                 | 06-Aug-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000045    | 0006 - Corrugated |
      | 2020100004 | Draft                 | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia  |
      | 2020100001 | ActualValuesCompleted | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia  |
    And the total number of records in search results by "Ashraf.Fathi" are 3

  #EBS-6779
  Scenario: (08) View All LandedCosts filtered PurchaseOrder by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on PurchaseOrder which contains "2018000045" with 10 records per page
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State | CreationDate         | Type                               | PurchaseOrder | BusinessUnit      |
      | 2020100005 | Draft | 06-Aug-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000045    | 0006 - Corrugated |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-6779
  Scenario: (09) View All LandedCosts filtered BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of LandedCosts with filter applied on BusinessUnit which equals "0002" with 10 records per page while current locale is "en-US"
    Then the following LandedCosts will be presented to "Ashraf.Fathi":
      | Code       | State                 | CreationDate         | Type                               | PurchaseOrder | BusinessUnit     |
      | 2020100004 | Draft                 | 06-Jul-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000010    | 0002 - Signmedia |
      | 2020100002 | Draft                 | 06-May-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000019    | 0002 - Signmedia |
      | 2020100001 | ActualValuesCompleted | 06-Apr-2020 12:10 PM | IMPORT - Landed Cost For Import PO | 2018000049    | 0002 - Signmedia |
      | 2020100000 | Draft                 | 06-Mar-2020 12:10 PM | LOCAL - Landed Cost For Local PO   | 2018000047    | 0002 - Signmedia |
    And the total number of records in search results by "Ashraf.Fathi" are 4

  #EBS-6779
  Scenario: (10) View All LandedCosts filtered BusinessUnit by authorized user - No search results (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of LandedCosts with filter applied on BusinessUnit which equals "0006" with 10 records per page while current locale is "en-US"
    Then no values are displayed to "Shady.Abdelatif" and empty grid state is viewed in Home Screen
    And the total number of records in search results by "Shady.Abdelatif" are 0

  #EBS-6779
  Scenario: (11) View All LandedCosts by unauthorized user due to role (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of LandedCosts with no filter applied 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
