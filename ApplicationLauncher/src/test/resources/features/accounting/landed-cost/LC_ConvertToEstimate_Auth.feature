# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
# Reviewer:

Feature: LandedCost ConvertToEstimat (Auth)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                   | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:ConvertToEstimate | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission                   |
      | Afaf | LandedCost:ConvertToEstimate |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                   | Condition                       |
      | Shady.Abdelatif | LandedCost:ConvertToEstimate | [purchaseUnitName='Corrugated'] |

    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0006 | Corrugated |

    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit      |
      | 2020100005 | Draft | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |

  #EBS-7604
  Scenario: (01) LandedCost ConvertToEstimate by unauthorized user (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" ConvertToEstimate LandedCost with code "2020100005" at "07-Jan-2019 09:30 AM"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7604
  Scenario: (02) LandedCost ConvertToEstimate by unauthorized user due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "2020100005" at "07-Jan-2019 09:30 AM"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
