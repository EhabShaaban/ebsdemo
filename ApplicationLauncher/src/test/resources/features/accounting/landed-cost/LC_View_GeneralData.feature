#Author: Evram Hany and Eslam salam
#Reviewer: Somaya Ahmed (8-Jul-2020)

Feature: LandedCost - View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Seif      | Quality_Specialist   |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | Quality_Specialist   | LandedCostViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                      |
      | SuperUserSubRole           | *:*                        |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadGeneralData | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer           | LandedCost:ReadGeneralData |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | LandedCost:ReadGeneralData | [purchaseUnitName='Flexo'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
      | 0001 | Flexo     |
        #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2020000001 | IMPORT - Landed Cost For Import PO | Draft                 | Gehan.Ahmed | 24-Jun-2020 09:02 AM | Shady.Abdelatif |
      | 2020100004 | IMPORT - Landed Cost For Import PO | Draft                 | Gehan.Ahmed | 06-Jul-2020 12:10 PM | Shady.Abdelatif |
      | 2020000004 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted | Gehan.Ahmed | 24-Jun-2020 09:02 AM | Shady.Abdelatif |

        #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020100004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000004 | 0001 - Flexo     | 0001 - AL Madina |

#EBS-6731
  Scenario: (01) View GeneralData section in LandedCost, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of LandedCost with code "2020000001"
    Then the following values of GeneralData section for LandedCost with code "2020000001" are displayed to "Shady.Abdelatif":
      | LCCode     | Type                               | CreatedBy   | State | CreationDate         | DocumentOwner   |
      | 2020000001 | IMPORT - Landed Cost For Import PO | Gehan.Ahmed | Draft | 24-Jun-2020 09:02 AM | Shady.Abdelatif |

#EBS-6731
  Scenario Outline: (02) View GeneralData section in LandedCost, by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view GeneralData section of LandedCost with code "<Code>"
    Then the following values of GeneralData section for LandedCost with code "<Code>" are displayed to "Ahmed.Seif":
      | LCCode | Type   | CreatedBy   | State   | CreationDate   | DocumentOwner   |
      | <Code> | <Type> | <CreatedBy> | <State> | <CreationDate> | Shady.Abdelatif |
    Examples:
      | Code       | Type                               | CreatedBy   | State                 | CreationDate         |
      | 2020000001 | IMPORT - Landed Cost For Import PO | Gehan.Ahmed | Draft                 | 24-Jun-2020 09:02 AM |
      | 2020000004 | IMPORT - Landed Cost For Import PO | Gehan.Ahmed | ActualValuesCompleted | 24-Jun-2020 09:02 AM |

#EBS-6731
  Scenario: (03) View GeneralData section in LandedCost, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of LandedCost with code "2020000004"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  Scenario: (04) View GeneralData section in LandedCost, where LandedCost doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to view GeneralData section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
