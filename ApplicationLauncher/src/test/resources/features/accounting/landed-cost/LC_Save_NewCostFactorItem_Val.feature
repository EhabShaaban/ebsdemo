# Author Dev: Yara Ameen
# Author Quality: Khadra Ali
# Reviewer: Mohamed Aboelnour

Feature: LC Save CostFactorItem (Val)

  Background:
    Given the following users and roles exist:
      | Name            | Role                     |
      | Shady.Abdelatif | Accountant_Signmedia     |
      | Ashraf.Salah    | AccountantHead_Signmedia |

    And the following roles and sub-roles exist:
      | Role                     | Subrole                   |
      | Accountant_Signmedia     | LandedCostOwner_Signmedia |
      | AccountantHead_Signmedia | LandedCostOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                      | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |

    And the following LandedCosts exist:
      | Code       | State | Type                             | BusinessUnit     |
      | 2020100002 | Draft | LOCAL - Landed Cost For Local PO | 0002 - Signmedia |

    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 000055 - Insurance service2 | 300 EGP        | null EGP    | null EGP   |
      | 2020100002 | 000057 - Bank Fees          | 600 EGP        | null EGP    | null EGP   |

    And the following CostFactorItems exist with the following details:
      | Item                        | PurchaseUnit     |
      | 000014 - Insurance service  | 0002 - Signmedia |
      | 000055 - Insurance service2 | 0002 - Signmedia |
      | 000057 - Bank Fees          | 0002 - Signmedia |

    And the following Items are not Cost Factor Items:
      | Item                                        | PurchaseUnit     |
      | 000001 - Hot Laminated Frontlit Fabric roll | 0002 - Signmedia |

   #EBS-7414
  Scenario: (01) Save new LandedCost CostFactorItem after edit session expires (Exception Case)
    Given  user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:31 AM" with the following values:
      | Item                       | Value  |
      | 000014 - Insurance service | 200.00 |
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-07"

   #EBS-7414
  Scenario: (02) Save new LandedCost CostFactorItem when landed cost doesn't exist (Exception Case)
    Given  user is logged in as "Shady.Abdelatif"
    And another user is logged in as "Ashraf.Salah"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    And "Ashraf.Salah" first deleted the LandedCost with code "2020100002" successfully at "01-Jan-2019 11:31 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:32 AM" with the following values:
      | Item                       | Value  |
      | 000014 - Insurance service | 200.00 |
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"


   #EBS-7414
  Scenario: (03) Save new LandedCost CostFactorItem when the selected cost factor item doesn't exist (Failure Case)
    Given  user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:05 AM" with the following values:
      | Item                       | Value  |
      | 999999 - not existing item | 200.00 |
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "itemCode" field "Gen-msg-48" and sent to "Shady.Abdelatif"

  #EBS-7414
  Scenario Outline: (04) Save new LandedCost CostFactorItem with Missing Mandatories (Abuse Case)
    Given  user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:05 AM" with the following values:
      | Item   | Value   |
      | <Item> | <Value> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | Item                       | Value  |
      |                            | 200.00 |
      | ""                         | 200.00 |
      | 000014 - Insurance service |        |
      | 000014 - Insurance service | ""     |

   #EBS-7414
  Scenario Outline: (05) Save new LandedCost CostFactorItem with malicious input  (Abuse Case)
    Given  user is logged in as "Shady.Abdelatif"
    And CostFactorItems section of LandedCost with Code "2020100002" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:05 AM" with the following values:
      | Item   | Value   |
      | <Item> | <Value> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | Item                                         | Value        |
      #duplicate item that already exists in the landed cost
      | 000055 - Insurance service2                  | 200.00       |
      # item that is not cost factor item
      | 000001 - Hot Laminated Frontlit Fabric roll  | 200.00       |
      # item invalid code format
      | ay7aga - Hot Laminated Frontlit Fabric roll  | 200.00       |
      | 999 - Hot Laminated Frontlit Fabric roll     | 200.00       |
      | 9999999 - Hot Laminated Frontlit Fabric roll | 200.00       |
      # estimated value is less than the boundaries
      | 000014 - Insurance service                   | -1.0         |
      | 000014 - Insurance service                   | 0.0          |
      # estimated value exceeds the boundaries
      | 000014 - Insurance service                   | 1000000001.0 |
       # invalid estimated value
      | 000014 - Insurance service                   | ay7aga       |

