# Author Dev: Evram Hany
# Author Quality: ?
# Reviewer: ?

Feature: LC Service Items Read All

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole              |
      | Accountant_Signmedia | ItemViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole              | Permission   | Condition                      |
      | ItemViewer_Signmedia | Item:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission   | Condition |
      | Afaf | Item:ReadAll |           |

    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit     |
      | 2020100002 | Draft | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia |
      | 2020100004 | Draft | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |


    And the following CostFactorItems exist with the following details:
      | Item                        | PurchaseUnit     |
      | 000055 - Insurance service2 | 0002 - Signmedia |
      | 000056 - Shipment service   | 0001 - Flexo     |
      | 000057 - Bank Fees          | 0002 - Signmedia |
      | 000014 - Insurance service  | 0002 - Signmedia |

#
  Scenario: (01) Read All Items for LandedCost ServiceItems Dropdown in LandedCost by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all Items That marked as Service items for landed Cost With Code "2020100002"
    Then the following Items should be returned to "Shady.Abdelatif":
      | Item                        |
      | 000055 - Insurance service2 |
      | 000057 - Bank Fees          |
      | 000014 - Insurance service  |
    And total number of service items returned to "Shady.Abdelatif" is equal to 3

  Scenario: (02) Read All Items for LandedCost ServiceItems Dropdown in LandedCost by unauthorized user (Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Items That marked as Service items for landed Cost With Code "2020100002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
