# Author Dev: Ahmed Ali (EBS-6781)
# Author QA: Eslam Salam (EBS-6781)
Feature: Create LC (Val)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | Accountant_Signmedia | LandedCostTypeViewer      |
      | Accountant_Signmedia | POViewer_Signmedia        |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission             | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:Create      |                                |
      | LandedCostTypeViewer      | LandedCostType:ReadAll |                                |
      | POViewer_Signmedia        | PurchaseOrder:ReadAll  | [purchaseUnitName='Signmedia'] |
    And the following are ALL exisitng LandedCostTypes:
      | Code   | Name                      |
      | IMPORT | Landed Cost For Import PO |
      | LOCAL  | Landed Cost For Local PO  |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following Import PurchaseOrders exist:
      | Code       | State          | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000006 | OpenForUpdates | 0002           | 0002    |      |               |
      | 2018000012 | Cancelled      | 0002           | 0002    |      |               |
      | 2018000021 | Draft          | 0002           | 0002    |      |               |
      | 2018000022 | Shipped        | 0002           |         |      |               |
    And the following Local PurchaseOrders exist:
      | Code       | State   | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000013 | Draft   | 0002           | 0002    |      |               |
      | 2018000047 | Shipped | 0002           | 0002    |      |               |

    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2019000105 | LOCAL_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |

  #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                           | State                 | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2020000003 | LOCAL - Landed Cost For Local PO | ActualValuesCompleted | Amr.Khalil | 24-Jun-2020 09:02 AM | Shady.Abdelatif |

   #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode |
      | 2020000003 | 2018000047 | 2019000105        | true        | VendorInvoice  |

    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition |
      | 40 | Shady.Abdelatif | LandedCost     | CanBeDocumentOwner |           |

  #EBS-6781
  Scenario Outline: (01) Create LandedCost with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner   |
      | <Type> | <PurchaseOrder> | <DocumentOwner> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    Examples:
      | Type   | PurchaseOrder | DocumentOwner |
      |        | 2018000006    | 40            |
      | ""     | 2018000013    | 40            |
      | N/A    | 2018000006    | 40            |
      | IMPORT |               | 40            |
      | IMPORT | ""            | 40            |
      | LOCAL  | N/A           | 40            |
      | LOCAL  | 2018000006    |               |
      | LOCAL  | 2018000006    | ""            |

  #EBS-6781
  Scenario Outline: (02) Create LandedCost with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner   |
      | <Type> | <PurchaseOrder> | <DocumentOwner> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    Examples:
      | Type           | PurchaseOrder | DocumentOwner |
      | NOT_EXIST_TYPE | 2018000006    | 40            |
      #Local_PurchaseOrder_But_Type_Import
      | IMPORT         | 2018000013    | 40            |
      #Not_Exist_PurchaseOrder
      | IMPORT         | 2030000000    | 40            |
      #Draft_PurchaseOrder
      | IMPORT         | 2018000021    | 40            |
      #PurchaseOrder_Without_Company
      | IMPORT         | 2018000022    | 40            |
      | IMPORT         | 2018000022    | ay7aga        |
      | IMPORT         | 2018000022    | 9999999       |

  #EBS-6781
  Scenario Outline: (03) Create LandedCost with incorrect data entry (Validation Failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner |
      | <Type> | <PurchaseOrder> | 40            |
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Shady.Abdelatif"

    Examples:
      | Type   | PurchaseOrder | Field             | ErrMsg     |
      #Cancelled_PurchaseOrder
      | IMPORT | 2018000012    | purchaseOrderCode | Gen-msg-32 |
      #PurchaseOrder_With_Posted_LandedCost
      | LOCAL  | 2018000047    | purchaseOrderCode | Lc-msg-01  |
