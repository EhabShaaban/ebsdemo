# Author Dev: Evram Hany
# Author Quality: Eslam Salam

Feature: LC Request Add Items section

  Background:
    Given the following users and roles exist:
      | Name                    | Role                                     |
      | Shady.Abdelatif         | Accountant_Signmedia                     |
      | Shady.Abdelatif.NoItems | Accountant_Signmedia_CannotReadItemsRole |
      | Ahmed.Hamdi             | Accountant_Flexo                         |
      | hr1                     | SuperUser                                |
    And the following roles and sub-roles exist:
      | Role                                     | Sub-role                  |
      | Accountant_Signmedia                     | LandedCostOwner_Signmedia |
      | Accountant_Signmedia_CannotReadItemsRole | LandedCostOwner_Signmedia |
      | Accountant_Flexo                         | LandedCostOwner_Flexo     |
      | Accountant_Signmedia                     | ItemViewer_Signmedia      |
      | Accountant_Flexo                         | ItemViewer_Flexo          |
      | SuperUser                                | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                      | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |
      | LandedCostOwner_Flexo     | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Flexo']     |
      | ItemViewer_Signmedia      | Item:ReadAll                    | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Flexo          | Item:ReadAll                    | [purchaseUnitName='Flexo']     |
      | SuperUserSubRole          | *:*                             |                                |
    And the following users doesn't have the following permissions:
      | User                    | Permission   |
      | Shady.Abdelatif.NoItems | Item:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User        | Permission                      | Condition                      |
      | Ahmed.Hamdi | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |
    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit     |
      | 2020100001 | ActualValuesCompleted    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020100004 | Draft                    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020000001 | Draft                    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |
      | 2020000002 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0002 - Signmedia |

  #EBS-7413
  Scenario: (01) Request to Add/Cancel Item to LandedCost in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add Item to LandedCostFactorItems section of LandedCost with code "2020000001"
    Then a new add Item dialoge is opened and LandedCostFactorItems section of LandedCost with code "2020000001" becomes locked by "Shady.Abdelatif"
    And the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads |
      | ReadItems       |
    And the following mandatory fields are returned to "Shady.Abdelatif":
      | MandatoriesFields |
      | itemCode          |
      | estimatedValue    |
    And the following editable fields are returned to "Shady.Abdelatif":
      | EditableFields |
      | itemCode       |
      | estimatedValue |

  #EBS-7413
  Scenario: (02) Request to Add/Cancel Item to LandedCost when LandedCostFactorItems section is locked by another user (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to LandedCostFactorItems section of LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to add Item to LandedCostFactorItems section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

 #EBS-7413
  Scenario: (03) Request to Add/Cancel Item to LandedCost that doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to add Item to LandedCostFactorItems section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-7413
  Scenario Outline: (04) Request to Add/Cancel Item to LandedCost in a state that doesn't allow this action - All states except Draft (Exception)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add Item to LandedCostFactorItems section of LandedCost with code "<Code>"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-03"
    And LandedCostFactorItems section of LandedCost with code "<Code>" is not locked by "Shady.Abdelatif"
    Examples:
      | Code       |
      | 2020100001 |
      | 2020000002 |

  #EBS-7413
  Scenario: (05) Request to Add/Cancel Item to LandedCost by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When  "Ahmed.Hamdi" requests to add Item to LandedCostFactorItems section of LandedCost with code "2020100004"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

   #EBS-7413
  Scenario: (06) Request to Add/Cancel Item to LandedCost by a User with no authorized reads - All allowed states (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoItems"
    When "Shady.Abdelatif.NoItems" requests to add Item to LandedCostFactorItems section of LandedCost with code "2020000001"
    Then a new add Item dialoge is opened and LandedCostFactorItems section of LandedCost with code "2020000001" becomes locked by "Shady.Abdelatif.NoItems"
    And there are no authorized reads returned to "Shady.Abdelatif.NoItems"
    And the following mandatory fields are returned to "Shady.Abdelatif.NoItems":
      | MandatoriesFields |
      | itemCode          |
      | estimatedValue    |
    And the following editable fields are returned to "Shady.Abdelatif.NoItems":
      | EditableFields |
      | itemCode       |
      | estimatedValue |

################################################Cancel Scenario##########################

  #EBS-7413
  Scenario: (07) Request to Add/Cancel Item to LandedCost While the edit session of Item to LandedCost in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And LandedCostFactorItems section of LandedCost with code "2020100004" is locked by "Shady.Abdelatif" at "07-Jan-2019 09:10 AM"
    When "Shady.Abdelatif" cancels saving LandedCostFactorItems section of LandedCost with code "2020100004" at "07-Jan-2019 09:30 AM"
    Then the lock by "Shady.Abdelatif" on LandedCostFactorItems section of LandedCost with code "2020100004" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"

  #EBS-7413
  Scenario: (08) Request to Add/Cancel Item to LandedCost after lock session is expired
    Given user is logged in as "Shady.Abdelatif"
    And LandedCostFactorItems section of LandedCost with code "2020100004" is locked by "Shady.Abdelatif" at "07-Jan-2019 09:00 AM"
    When "Shady.Abdelatif" cancels saving LandedCostFactorItems section of LandedCost with code "2020100004" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-07"

  #EBS-7413
  Scenario: (09) Request to Add/Cancel Item to LandedCost after lock session is expire and LandedCost doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And LandedCostFactorItems section of LandedCost with code "2020100004" is locked by "Shady.Abdelatif" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the LandedCost with code "2020100004" successfully at "07-Jan-2019 09:31 AM"
    When "Shady.Abdelatif" cancels saving LandedCostFactorItems section of LandedCost with code "2020100004" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-7413
  Scenario: (10) Request to Add/Cancel Item to LandedCost with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" cancels saving LandedCostFactorItems section of LandedCost with code "2020100004"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page
