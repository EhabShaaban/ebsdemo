# Author Dev: Yara Ameen
# Author Quality: Eslam Ayman
Feature: LC Delete CostFactorItem

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | SuperUser            | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                      | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole          | *:*                             |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                      | Condition                       |
      | Shady.Abdelatif | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Corrugated'] |
    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit      |
      | 2020100002 | Draft                    | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia  |
      | 2020100005 | Draft                    | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |
      | 2020100001 | ActualValuesCompleted    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020000002 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
    And the following LandedCosts have the following CostFactorItems with ids:
      | LCCode     | LCItemId | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 8        | 000055 - Insurance service2 | 300 EGP        | null EGP    | null EGP   |
      | 2020100002 | 9        | 000057 - Bank Fees          | 600 EGP        | null EGP    | null EGP   |
      | 2020100005 | 10       | 000056 - Shipment service   | 150 EGP        | null EGP    | null EGP   |
      | 2020100005 | 11       | 000055 - Insurance service2 | 700 EGP        | null EGP    | null EGP   |
      | 2020100001 | 1        | 000057 - Bank Fees          | 900.75 EGP     | 1000.25 EGP | -99.50 EGP |
      | 2020100001 | 2        | 000055 - Insurance service2 | 200.25 EGP     | 150.75 EGP  | 49.50 EGP  |
      | 2020000002 | 7        | 000055 - Insurance service2 | 200.25 EGP     | null EGP    | null EGP   |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 900 EGP        | null EGP    | null EGP   |
      | 2020100005 | 850 EGP        | null EGP    | null EGP   |
      | 2020100001 | 1101.00 EGP    | 1151.00 EGP | -50.00 EGP |
      | 2020000002 | 200.25 EGP     | null EGP    | null EGP   |

  #EBS-7190
  Scenario: (01) Delete CostFactorItem from LandedCost, where LandedCost is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "8" in LandedCost with code "2020100002"
    Then CostFactorItem with id "8" from LandedCost with code "2020100002" is deleted
    And Only the following CostFactorItems remaining in LandedCost with code "2020100002":
      | LCCode     | LCItemId | Item               | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 9        | 000057 - Bank Fees | 600 EGP        | null EGP    | null EGP   |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-12"
    And CostFactorItemsSummary section is updated as follows:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 600 EGP        | null EGP    | null EGP   |

  #EBS-7190
  Scenario: (02) Delete CostFactorItem from LandedCost, where CostFactorItem doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted CostFactorItem with id "8" of LandedCost with code "2020100002" successfully
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "8" in LandedCost with code "2020100002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-20"

  #EBS-7190
  Scenario Outline: (03) Delete CostFactorItem from LandedCost, where action is not allowed in current state - All states except Draft (Exception)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "<LCItemId>" in LandedCost with code "<LCCode>"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-21"

    Examples:
      | LCCode     | LCItemId |
      | 2020100001 | 2        |
      | 2020000002 | 7        |

  #EBS-7190
  Scenario: (04) Delete CostFactorItem from LandedCost, where LandedCost doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020100002" successfully
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "8" in LandedCost with code "2020100002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-7190
  Scenario: (05) Delete CostFactorItem from LandedCost, when LandedCostFactorItems section is locked by another user (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to LandedCostFactorItems section of LandedCost with code "2020100002" successfully
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "8" in LandedCost with code "2020100002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

  #EBS-7190
  Scenario: (06) Delete CostFactorItem from LandedCost, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete CostFactorItem with id "10" in LandedCost with code "2020100005"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-7190
  Scenario: (07) Delete CostFactorItem from LandedCost, by Unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to delete CostFactorItem with id "8" in LandedCost with code "2020100002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
