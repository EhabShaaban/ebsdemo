# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
# Reviewer: Ahmed Ali (23-Dec-2020)

Feature: LC Save CostFactorItem (Auth)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | Accountant_Signmedia | ItemViewer_Signmedia      |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                      | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Signmedia'] |
      | ItemViewer_Signmedia      | Item:ReadAll                    | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission                      |
      | Afaf | LandedCost:UpdateCostFactorItem |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                      | Condition                       |
      | Shady.Abdelatif | LandedCost:UpdateCostFactorItem | [purchaseUnitName='Corrugated'] |

    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit      |
      | 2020100002 | Draft | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia  |
      | 2020100004 | Draft | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020100005 | Draft | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |

    And the following CostFactorItems exist with the following details:
      | Item                        | PurchaseUnit     |
      | 000055 - Insurance service2 | 0002 - Signmedia |
      | 000056 - Shipment service   | 0001 - Flexo     |
      | 000057 - Bank Fees          | 0002 - Signmedia |
      | 000014 - Insurance service  | 0002 - Signmedia |

  #EBS-7415
  Scenario: (01) Save new LandedCost CostFactorItem In Import PO by unauthorized user (Unauthorized/Abuse case)
    Given  user is logged in as "Afaf"
    When "Afaf" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100004" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                       | Value  |
      | 000014 - Insurance service | 200.00 |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7415
  Scenario: (02) Save new LandedCost CostFactorItem In Local PO by unauthorized user (Unauthorized/Abuse case)
    Given  user is logged in as "Afaf"
    When "Afaf" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100002" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                       | Value  |
      | 000014 - Insurance service | 200.00 |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-7415
  Scenario: (03) Save new LandedCost CostFactorItem In Import PO by unauthorized user due to condition (Unauthorized/Abuse case)
    Given  user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code "2020100005" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                       | Value  |
      | 000014 - Insurance service | 200.00 |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
