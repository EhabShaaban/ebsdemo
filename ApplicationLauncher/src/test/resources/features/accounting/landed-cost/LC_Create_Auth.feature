# Author Dev: Ahmed Ali (EBS-6780)
# Reviewer: Eslam Salam
Feature: Create LC (Auth)

  Background:
    Given the following users and roles exist:
      | Name                                | Role                                                    |
      | Shady.Abdelatif                     | Accountant_Signmedia                                    |
      | Shady.Abdelatif.NoLandedCostType    | Accountant_Signmedia_CannotReadLandedCostType           |
      | Shady.Abdelatif.NoPurchaseOrder     | Accountant_Signmedia_CannotReadPurchaseOrder            |
      | Shady.Abdelatif.NoCreateWithAllRead | Accountant_Signmedia_CannotCreateLandedCostWithAllReads |
      | Shady.Abdelatif.CreateWithNoRead    | Accountant_Signmedia_CreateLandedCostWithNoReads        |
      | Shady.Abdelatif.NoDocumentOwner     | Accountant_Signmedia_CannotReadDocumentOwner            |
      | Afaf                                | FrontDesk                                               |
    And the following roles and sub-roles exist:
      | Role                                                    | Sub-role                   |
      | Accountant_Signmedia                                    | LandedCostOwner_Signmedia  |
      | Accountant_Signmedia                                    | LandedCostViewer_Signmedia |
      | Accountant_Signmedia                                    | LandedCostTypeViewer       |
      | Accountant_Signmedia                                    | POViewer_Signmedia         |
      | Accountant_Signmedia_CannotReadLandedCostType           | LandedCostOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadLandedCostType           | LandedCostViewer_Signmedia |
      | Accountant_Signmedia_CannotReadLandedCostType           | POViewer_Signmedia         |
      | Accountant_Signmedia_CannotReadLandedCostType           | DocumentOwnerViewer        |
      | Accountant_Signmedia_CannotReadPurchaseOrder            | LandedCostOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadPurchaseOrder            | LandedCostViewer_Signmedia |
      | Accountant_Signmedia_CannotReadPurchaseOrder            | LandedCostTypeViewer       |
      | Accountant_Signmedia_CannotReadPurchaseOrder            | DocumentOwnerViewer        |
      | Accountant_Signmedia_CannotCreateLandedCostWithAllReads | LandedCostViewer_Signmedia |
      | Accountant_Signmedia_CannotCreateLandedCostWithAllReads | LandedCostTypeViewer       |
      | Accountant_Signmedia_CannotCreateLandedCostWithAllReads | POViewer_Signmedia         |
      | Accountant_Signmedia_CreateLandedCostWithNoReads        | LandedCostOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadDocumentOwner            | LandedCostOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadDocumentOwner            | POViewer_Signmedia         |
      | Accountant_Signmedia_CannotReadDocumentOwner            | LandedCostTypeViewer       |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission             | Condition                      |
      | LandedCostOwner_Signmedia  | LandedCost:Create      |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadAll     | [purchaseUnitName='Signmedia'] |
      | LandedCostTypeViewer       | LandedCostType:ReadAll |                                |
      | POViewer_Signmedia         | PurchaseOrder:ReadAll  | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer        | DocumentOwner:ReadAll  |                                |

    And the following users doesn't have the following permissions:
      | User                                | Permission             |
      | Shady.Abdelatif.NoLandedCostType    | LandedCostType:ReadAll |
      | Shady.Abdelatif.NoPurchaseOrder     | PurchaseOrder:ReadAll  |
      | Shady.Abdelatif.NoCreateWithAllRead | LandedCost:Create      |
      | Shady.Abdelatif.CreateWithNoRead    | LandedCostType:ReadAll |
      | Shady.Abdelatif.CreateWithNoRead    | PurchaseOrder:ReadAll  |
      | Afaf                                | LandedCost:Create      |
      | Shady.Abdelatif.NoDocumentOwner     | DocumentOwner:ReadAll  |

    And the following users have the following permissions without the following conditions:
      | User            | Permission            | Condition                  |
      | Shady.Abdelatif | PurchaseOrder:ReadAll | [purchaseUnitName='Flexo'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following are ALL exisitng LandedCostTypes:
      | Code   | Name                      |
      | IMPORT | Landed Cost For Import PO |
      | LOCAL  | Landed Cost For Local PO  |
    And the following Import PurchaseOrders exist:
      | Code       | State    | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000001 | Cleared  | 0002           | 0002    |      |               |
      | 2018000034 | Approved | 0001           | 0002    |      |               |
    And the following Local PurchaseOrders exist:
      | Code       | State           | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000015 | WaitingApproval | 0002           | 0002    |      |               |
    And the following Vendor Invoices exist:
      | Code       | Type                 | PurchaseUnit | Vendor | PurchaseOrder | InvoiceNumber    | State  |
      | 2019000002 | IMPORT_GOODS_INVOICE | 0002         | 000002 | 2018000010    | 1200120301023700 | Posted |

    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition |
      | 40 | Shady.Abdelatif | LandedCost     | CanBeDocumentOwner |           |

  #EBS-6780
  Scenario Outline: (01) Request Create LandedCost by an unauthorized to create user but with all authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif.NoCreateWithAllRead"
    When "Shady.Abdelatif.NoCreateWithAllRead" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner |
      | <Type> | <PurchaseOrder> | 40            |
    Then "Shady.Abdelatif.NoCreateWithAllRead" is logged out
    And "Shady.Abdelatif.NoCreateWithAllRead" is forwarded to the error page

    Examples:
      | Type   | PurchaseOrder |
      | IMPORT | 2018000001    |
      | LOCAL  | 2018000015    |

  #EBS-6780
  Scenario Outline: (02) Request Create LandedCost by an authorized user to create, but without unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner |
      | <Type> | <PurchaseOrder> | 40            |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                             | Type   | PurchaseOrder |
      | Shady.Abdelatif.NoLandedCostType | IMPORT | 2018000001    |
      | Shady.Abdelatif.NoPurchaseOrder  | LOCAL  | 2018000015    |
      | Shady.Abdelatif.NoDocumentOwner  | LOCAL  | 2018000015    |
      | Shady.Abdelatif.CreateWithNoRead | LOCAL  | 2018000015    |

  #EBS-6780
  Scenario: (03) Request Create LandedCost by an unauthorized user to create due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates LandedCost with the following values:
      | Type   | PurchaseOrder |
      | IMPORT | 2018000034    |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-6780
  Scenario: (04) Request Create LandedCost by an unauthorized user to create with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates LandedCost with the following values:
      | Type   | PurchaseOrder |
      | IMPORT | 2018000034    |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-6780
  Scenario: (05) Request Create LandedCost by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to create LandedCost
    Then the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads     |
      | ReadLandedCostTypes |
      | ReadPurchaseOrders  |
      | ReadDocumentOwners  |

  #EBS-6780
  Scenario: (06) Request Create LandedCost by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoLandedCostType"
    When "Shady.Abdelatif.NoLandedCostType" requests to create LandedCost
    Then the following authorized reads are returned to "Shady.Abdelatif.NoLandedCostType":
      | AuthorizedReads    |
      | ReadPurchaseOrders |
      | ReadDocumentOwners |

  #EBS-6780
  Scenario: (07) Request Create LandedCost by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoPurchaseOrder"
    When "Shady.Abdelatif.NoPurchaseOrder" requests to create LandedCost
    Then the following authorized reads are returned to "Shady.Abdelatif.NoPurchaseOrder":
      | AuthorizedReads     |
      | ReadLandedCostTypes |
      | ReadDocumentOwners  |
  #EBS-6780
  Scenario: (08) Request Create LandedCost by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoDocumentOwner"
    When "Shady.Abdelatif.NoDocumentOwner" requests to create LandedCost
    Then the following authorized reads are returned to "Shady.Abdelatif.NoDocumentOwner":
      | AuthorizedReads     |
      | ReadLandedCostTypes |
      | ReadPurchaseOrders  |
