# Author Dev: Ahmed Ali (EBS-6815)
# Author Quality: Khadrah Ali
# Reviewer:
Feature: Create LC (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | Accountant_Signmedia | LandedCostTypeViewer      |
      | Accountant_Signmedia | POViewer_Signmedia        |
      | Accountant_Signmedia | DocumentOwnerViewer       |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission             | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:Create      |                                |
      | LandedCostTypeViewer      | LandedCostType:ReadAll |                                |
      | POViewer_Signmedia        | PurchaseOrder:ReadAll  | [purchaseUnitName='Signmedia'] |
      | DocumentOwnerViewer       | DocumentOwner:ReadAll  |                                |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following are ALL exisitng LandedCostTypes:
      | Code   | Name                      |
      | IMPORT | Landed Cost For Import PO |
      | LOCAL  | Landed Cost For Local PO  |
    And the following Import PurchaseOrders exist:
      | Code       | State              | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000001 | Cleared            | 0002           | 0002    |      |               |
      | 2018000005 | Approved           | 0002           | 0002    |      |               |
      | 2018000006 | OpenForUpdates     | 0002           | 0002    |      |               |
      | 2018000008 | Confirmed          | 0002           | 0002    |      |               |
      | 2018000009 | FinishedProduction | 0002           | 0002    |      |               |
      | 2018000010 | Shipped            | 0002           | 0002    |      |               |
      | 2018000011 | Arrived            | 0002           | 0002    |      |               |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000015 | WaitingApproval  | 0002           | 0002    |      |               |
      | 2018000019 | DeliveryComplete | 0002           | 0002    |      |               |

        #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate        | DocumentOwner |
      | 2020000001 | IMPORT_GOODS_INVOICE | Posted | A.Hamed         | 24-Apr-2018 8:31 AM | Ashraf Salah  |
      | 2019000002 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 8:31 AM | Ashraf Salah  |
   #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company        |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000002 | 0002 - Signmedia | 0002 - DigiPro |

    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2020000001 | 2018000001    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023130 | 24-Apr-2018 06:31 AM | USD      |
      | 2019000002 | 2018000010    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023110 | 24-Apr-2018 06:31 AM | USD      |

    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2020000004 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted | Amr.Khalil | 24-Jun-2020 09:02 AM | Shady.Abdelatif |

    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition |
      | 40 | Shady.Abdelatif | LandedCost     | CanBeDocumentOwner |           |

  #EBS-6815
  Scenario Outline: (01) Create LandedCost, with all mandatory fields, by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "28-Sep-2020 02:00 PM"
    And Last created LandedCost was with code "2020000004"
    When "Shady.Abdelatif" creates LandedCost with the following values:
      | Type   | PurchaseOrder   | DocumentOwner |
      | <Type> | <PurchaseOrder> | 40            |
    Then a new LandedCost is created with the following values:
      | Code       | State | Type   | CreatedBy       | LastUpdatedBy   | CreationDate         | LastUpdateDate       | DocumentOwner   |
      | 2020000005 | Draft | <Type> | Shady.Abdelatif | Shady.Abdelatif | 28-Sep-2020 02:00 PM | 28-Sep-2020 02:00 PM | Shady.Abdelatif |
    And the Company Section of LandedCost with code "2020000005" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    And the Details Section of LandedCost with code "2020000005" is created as follows:
      | PurchaseOrder   | VendorInvoice   | DamageStockIncludedInCost |
      | <PurchaseOrder> | <VendorInvoice> | unchecked                 |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"

    Examples:
      | Type   | PurchaseOrder | VendorInvoice |
      | IMPORT | 2018000001    | 2020000001    |
      | IMPORT | 2018000005    |               |
      | IMPORT | 2018000006    |               |
      | IMPORT | 2018000008    |               |
      | IMPORT | 2018000009    |               |
      | IMPORT | 2018000010    | 2019000002    |
      | IMPORT | 2018000011    |               |
      | LOCAL  | 2018000015    |               |
      | LOCAL  | 2018000019    |               |
