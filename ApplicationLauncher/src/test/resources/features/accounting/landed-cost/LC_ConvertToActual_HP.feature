#Auther Dev: Ahmed Ali, Yara Ameen
#Auther QA: Khadrah Ali
#Reviewer: Mohamed Aboelnour
Feature: LandedCost ConvertToActual (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                  |
      | Shady.Abdelatif | Accountant_Signmedia  |
      | Ahmed.Hamdi     | Accountant_Corrugated |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                    |
      | Accountant_Signmedia  | LandedCostOwner_Signmedia  |
      | Accountant_Corrugated | LandedCostOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                       |
      | LandedCostOwner_Signmedia  | LandedCost:ConvertToActual | [purchaseUnitName='Signmedia']  |
      | LandedCostOwner_Corrugated | LandedCost:ConvertToActual | [purchaseUnitName='Corrugated'] |
    And the following GRs exist with the following GR-POData:
      | Code       | State  | Type           | POCode     |
      | 2018000017 | Active | PURCHASE_ORDER | 2018000045 |
    And GoodsReceipt with code "2018000017" has the following received items:
      | Item   | DifferenceReason |
      | 000012 |                  |
      | 000004 |                  |
      | 000003 |                  |
    And Item "000012" in GoodsReceipt (Based On PurchaseOrder) "2018000017" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes                                                    | StockType        |
      | 50.0             | 0037 | يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة | UNRESTRICTED_USE |
    And Item "000004" in GoodsReceipt (Based On PurchaseOrder) "2018000017" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 50.0             | 0003 |       | UNRESTRICTED_USE |
    And Item "000003" in GoodsReceipt (Based On PurchaseOrder) "2018000017" has the following received quantities:
      | ReceivedQty(UoE) | UoE  | Notes | StockType        |
      | 50.0             | 0037 |       | UNRESTRICTED_USE |
    And VendorInvoice with code "2019000107" has the following InvoiceItems:
      | ItemCode | ItemName                    | QtyOrder | OrderUnit | OrderUnitSymbol | QtyBase | BaseUnit | BaseUnitSymbol | UnitPrice(Base) | UnitPrice(OrderUnit) | ItemTotalAmount |
      | 000012   | Ink2                        | 50.00    | 0037      | Drum-50Kg       | 50.00   | 0014     | Liter          | 4.00            | 4.00                 | 200.00          |
      | 000004   | Flex cold foil adhesive E01 | 50.00    | 0003      | Kg              | 50.00   | 0003     | Kg             | 3.00            | 3.00                 | 150.00          |
      | 000003   | Flex Primer Varnish E33     | 50.00    | 0037      | Drum-50Kg       | 2500.00 | 0003     | Kg             | 4.00            | 200.00               | 10000.00        |
                #@INSERT
    And the following VendorInvoice  Summary exist:
      | Code       | DownPayment | TotalRemaining | TotalAmount |
      | 2019000107 | 350.00      | 11242.0        | 11592.0     |
    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit      |
      | 2020000002 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020000007 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 000055 - Insurance service2 | 200.25 EGP     | null EGP    | null EGP   |
      | 2020000007 | 000014 - Insurance service  | 300 EGP        | 350 EGP     | -50 EGP    |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 200.25 EGP     | null EGP    | null EGP   |
      | 2020000007 | 300 EGP        | 350 EGP     | -50 EGP    |
    And the following PurchaseOrders exist:
      | Code       | State   | BusinessUnit      | Vendor            | Currency                   | Amount   |
      | 2018000045 | Cleared | 0006 - Corrugated | 000002 - Zhejiang | USD - United States Dollar | 10150.00 |
    And the following PaymentRequests exist:
      | Code       | PaymentType              | PaymentForm | BusinessUnit      | State       | RefDocument | RefDocumentType | CurrencyPrice |
      | 2019000053 | OTHER_PARTY_FOR_PURCHASE | BANK        | 0006 - Corrugated | PaymentDone | 2018000045  | PURCHASEORDER   | 17.44         |
      | 2019000003 | OTHER_PARTY_FOR_PURCHASE | BANK        | 0006 - Corrugated | PaymentDone | 2018000045  | PURCHASEORDER   | 17.85         |
    And the following PaymentDetails for PaymentRequests exist:
      | PRCode     | BusinessPartner | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem             | BankAccountNumber               | Treasury | Description        |
      | 2019000053 |                 | PURCHASEORDER   | 2018000045  | 250.00    | USD         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |          | downpayment for po |
      | 2019000003 |                 | PURCHASEORDER   | 2018000045  | 100.00    | USD         | 000014 - Insurance service | 1516171819789 - EGP - Alex Bank |          | downpayment for po |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
        #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type            |
      | 000036 | USD              | EGP               | 17.0        | 06-Jan-2019 09:30 AM | User Daily Rate |
      | 000037 | USD              | EGP               | 17.9        | 07-Jan-2019 09:30 AM | User Daily Rate |
    And insert the following Costing GeneralData:
      | Code       | CreatedBy | CreationDate         | State  |
      | 2021000001 | Admin     | 07-Jan-2019 09:30 AM | Active |
#  @INSERT
    And insert the following Costing CompanyData:
      | Code       | BusinessUnit     | Company          | BankAccount |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |             |
#  @INSERT
    And insert the following Costing Details:
      | Code       | GoodsReceipt | CurrencyPriceEstimateTime | CurrencyPriceActualTime |
      | 2021000001 | 2018000017   | 17.0168219461 EGP         |                         |
#  Amount Before Taxes: 10350
    And insert the following Costing Items:
      | Code       | Item                                 | UOM              | WeightPercentage | ReceivedQty(UoE) | StockType        | UnitPrice(Estimate) | UnitPrice(Actual) | UnitLandedCost(Estimate) | UnitLandedCost(Actual) |
      | 2021000001 | 000012 - Ink2                        | 0037 - Drum-50Kg | 1.9323671497     | 50.0             | UNRESTRICTED_USE | 68.0672877844 EGP   |                   | 0.115942028982 EGP       |                        |
      | 2021000001 | 000004 - Flex cold foil adhesive E01 | 0003 - Kg        | 1.4492753623     | 50.0             | UNRESTRICTED_USE | 51.0504658383 EGP   |                   | 0.086956521738 EGP       |                        |
      | 2021000001 | 000003 - Flex Primer Varnish E33     | 0037 - Drum-50Kg | 96.6183574879    | 50.0             | UNRESTRICTED_USE | 3403.36438922 EGP   |                   | 5.797101449274 EGP       |                        |

  #EBS-6536, EBS-7776
  Scenario: (01) LandedCost ConvertToActual by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToActual LandedCost with code "2020000002" at "07-Jan-2019 09:30 AM":
    Then LandedCost with Code "2020000002" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State                 |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | ActualValuesCompleted |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 000055 - Insurance service2 | 200.25 EGP     | null EGP    | null EGP   |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020000002 | 200.25 EGP     | null EGP    | null EGP   |
    And a success notification is sent to "Shady.Abdelatif" with the following message "LC-msg-54"

  #EBS-7193, EBS-7776
  Scenario: (02) LandedCost ConvertToActual to calculate item cost where there is an active GR  (Happy Path)/(Costing)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" ConvertToActual LandedCost with code "2020000007" at "07-Jan-2019 09:30 AM":
    Then LandedCost with Code "2020000007" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State                 |
      | Ahmed.Hamdi   | 07-Jan-2019 09:30 AM | ActualValuesCompleted |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                       | EstimatedValue | ActualValue | Difference |
      | 2020000007 | 000014 - Insurance service | 300 EGP        | 350 EGP     | -50 EGP    |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020000007 | 300 EGP        | 350 EGP     | -50 EGP    |
    And the items single unit final cost with damaged items cost are exist as following:
      | ItemCode | ItemFinalCost     |
      | 000003   | 3494.763285024155 |
      | 000004   | 52.42144927536232 |
      | 000012   | 69.8952657004831  |
     And a success notification is sent to "Ahmed.Hamdi" with the following message "LC-msg-54"
