# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
# Reviewer: Somaya Ahmed (??-Sep-2020)
Feature: Delete Landed Cost

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
      | SuperUser            | SuperUserSubRole          |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission        | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:Delete | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole          | *:*               |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission        | Condition                       |
      | Shady.Abdelatif | LandedCost:Delete | [purchaseUnitName='Corrugated'] |

    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit      |
      | 2020000001 | Draft                    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020000002 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020000003 | ActualValuesCompleted    | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia  |
      | 2020100005 | Draft                    | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |

  #EBS-6778
  Scenario: (01) Delete LandedCost draft by an authorized user  (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete LandedCost with code "2020000001"
    Then LandedCost with code "2020000001" is deleted from the system
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-12"

  #EBS-6778
  Scenario: (02) Delete LandedCost draft, where LandedCost doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020000001" successfully
    When "Shady.Abdelatif" requests to delete LandedCost with code "2020000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-6778
  Scenario Outline: (03) Delete LandedCost, where action not allowed per current state (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete LandedCost with code "<LCCode>"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-13"

    Examples:
      | LCCode     |
      | 2020000002 |
      | 2020000003 |

  #EBS-6778
  @Future
  Scenario Outline: (04) Delete LandedCost that is locked by another user (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And first "hr1" opens "<LCSection>" section of LandedCost with code "2020000001" in edit mode
    When "Shady.Abdelatif" requests to delete LandedCost with code "2020000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-14"

    Examples:
      | LCSection         |
      | LandedCostDetails |

  #EBS-6778
  Scenario: (05) Delete LandedCost draft by an unauthorized user (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete LandedCost with code "2020100005"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page