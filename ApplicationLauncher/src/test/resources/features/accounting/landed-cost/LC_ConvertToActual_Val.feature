#Auther Dev: Evram Hany
#Auther QA:
#Reviewer: Ahmed Ali
Feature: LandedCost ConvertToActual (Val)

  Background:
    Given the following users and roles exist:
      | Name            | Role                  |
      | Shady.Abdelatif | Accountant_Signmedia  |
      | Ahmed.Hamdi     | Accountant_Corrugated |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                    |
      | Accountant_Signmedia  | LandedCostOwner_Signmedia  |
      | Accountant_Corrugated | LandedCostOwner_Corrugated |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                       |
      | LandedCostOwner_Signmedia  | LandedCost:ConvertToActual | [purchaseUnitName='Signmedia']  |
      | LandedCostOwner_Corrugated | LandedCost:ConvertToActual | [purchaseUnitName='Corrugated'] |
    And the following LandedCosts exist:
      | Code       | State                    | Type                               | BusinessUnit      |
      # allowed state but the VI not activated
      | 2020000007 | EstimatedValuesCompleted | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |
      # not allowed state
      | 2020000001 | Draft                    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020100001 | ActualValuesCompleted    | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020100003 | EstimatedValuesCompleted | LOCAL - Landed Cost For Local PO   | 0006 - Corrugated |

  #EBS-5987
  Scenario Outline: (01) LandedCost ConvertToActual when LandedCost in unallowed state (client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToActual LandedCost with code "<code>" at "07-Jan-2019 09:30 AM":
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-32"
    Examples:
      | code       |
      | 2020000001 |
      | 2020100001 |

  #EBS-5987
  Scenario: (02) LandedCost ConvertToActual where VI Is Not Activated (validation error)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" ConvertToActual LandedCost with code "2020000007" at "07-Jan-2019 09:30 AM":
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-05"
    And the following error message is attached to "goodsInvoiceCode" field "Lc-msg-03" and sent to "Ahmed.Hamdi"

  #EBS-5987
  Scenario: (03) LandedCost ConvertToActual where LandedCost Is not Exist (client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToActual LandedCost with code "2020202020" at "07-Jan-2019 09:30 AM":
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-5987
  @Future
  Scenario: (04) LandedCost ConvertToActual that has items don’t have actual cost  (validation failure)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" ConvertToActual LandedCost with code "2020100003" at "07-Jan-2019 09:30 AM":
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "?"



