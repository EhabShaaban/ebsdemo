#Auther Dev: Ahmed Ali & Evraam Hany
#Auther QA:
#Reviewer: Yara Amin
Feature: LandedCost ConvertToEstimat (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | LandedCostOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                   | Condition                      |
      | LandedCostOwner_Signmedia | LandedCost:ConvertToEstimate | [purchaseUnitName='Signmedia'] |
    And the following GRs exist with the following GR-POData:
      | Code       | State  | Type           | POCode     |
      | 2018000017 | Active | PURCHASE_ORDER | 2018000045 |
    And VendorInvoice with code "2019000108" has the following InvoiceItems:
      | ItemCode | ItemName                           | QtyOrder | OrderUnit | OrderUnitSymbol | QtyBase | BaseUnit | BaseUnitSymbol | UnitPrice(Base) | UnitPrice(OrderUnit) | ItemTotalAmount |
      | 000001   | Hot Laminated Frontlit Fabric roll | 6.00     | 0029      | Roll 2.20x50    | 660.00  | 0019     | M2             | 80.00           | 8800.00              | 52800.00        |
    And the following LandedCosts exist:
      | Code       | State | Type                               | BusinessUnit      |
      | 2020100002 | Draft | LOCAL - Landed Cost For Local PO   | 0002 - Signmedia  |
      | 2020000001 | Draft | IMPORT - Landed Cost For Import PO | 0002 - Signmedia  |
      | 2020100005 | Draft | IMPORT - Landed Cost For Import PO | 0006 - Corrugated |
    And the following CostFactorItems for LandedCosts exist:
      | LCCode     | Item                        | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 000055 - Insurance service2 | 300 EGP        | null EGP    | null EGP   |
      | 2020100002 | 000057 - Bank Fees          | 600 EGP        | null EGP    | null EGP   |
      | 2020000001 | 000055 - Insurance service2 | 200 EGP        | null EGP    | null EGP   |
      | 2020100005 | 000055 - Insurance service2 | 700 EGP        | null EGP    | null EGP   |
      | 2020100005 | 000056 - Shipment service   | 150 EGP        | null EGP    | null EGP   |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue | ActualValue | Difference |
      | 2020100002 | 900 EGP        | null EGP    | null EGP   |
      | 2020000001 | 200 EGP        | null EGP    | null EGP   |
      | 2020100005 | 850 EGP        | null EGP    | null EGP   |

  #EBS-6536
  Scenario Outline: (01) LandedCost ConvertToEstimate by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" ConvertToEstimate LandedCost with code "<code>" at "07-Jan-2019 09:30 AM"
    Then LandedCost with Code "<code>" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State                    |
      | Shady.Abdelatif | 07-Jan-2019 09:30 AM | EstimatedValuesCompleted |
    And a success notification is sent to "Shady.Abdelatif" with the following message "LC-msg-54"

    Examples:
      | code       |
      | 2020100002 |
      | 2020000001 |
