# Author Dev: Ahmed Ali (EBS-6815, EBS-6780)
# Reviewer: Khadrah Ali
Feature: Purchase Orders Dropdown for LC Creation

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role           |
      | Accountant_Signmedia | POViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission            | Condition                      |
      | POViewer_Signmedia | PurchaseOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | PurchaseOrder:ReadAll |
    And the following PurchasingUnits exist:
      | Code | Name       |
      | 0001 | Flexo      |
      | 0002 | Signmedia  |
      | 0006 | Corrugated |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following Import PurchaseOrders exist:
      | Code       | State            | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000001 | Cleared          | 0002           |         |      |               |
      | 2018000002 | DeliveryComplete | 0002           |         |      |               |
      | 2018000006 | OpenForUpdates   | 0002           | 0002    |      |               |
      | 2018000007 | WaitingApproval  | 0002           | 0002    |      |               |
      | 2018000008 | Confirmed        | 0002           | 0002    |      |               |
      | 2018000010 | Shipped          | 0002           | 0002    |      |               |
      | 2018000011 | Arrived          | 0002           | 0002    |      |               |
      | 2018000012 | Cancelled        | 0002           | 0002    |      |               |
      | 2018000021 | Draft            | 0002           | 0002    |      |               |
      | 2018000025 | Draft            | 0001           | 0002    |      |               |
      | 2018000026 | Draft            | 0006           | 0002    |      |               |
      | 2018000032 | WaitingApproval  | 0001           |         |      |               |
      | 2018000034 | Approved         | 0001           | 0002    |      |               |
    And the following Local PurchaseOrders exist:
      | Code       | State            | PurchasingUnit | Company | Bank | AccountNumber |
      | 2018000013 | Draft            | 0002           | 0002    |      |               |
      | 2018000014 | OpenForUpdates   | 0002           | 0002    |      |               |
      | 2018000015 | WaitingApproval  | 0002           | 0002    |      |               |
      | 2018000016 | Approved         | 0002           | 0002    |      |               |
      | 2018000018 | Shipped          | 0002           | 0002    |      |               |
      | 2018000019 | DeliveryComplete | 0002           | 0002    |      |               |
      | 2018000020 | Cancelled        | 0002           | 0002    |      |               |
      | 2018000027 | Draft            | 0001           | 0002    |      |               |
      | 2018000028 | Draft            | 0006           | 0002    |      |               |
      | 2018000030 | OpenForUpdates   | 0001           |         |      |               |

  #EBS-6815
  Scenario: (01) Read all Import PurchaseOrders for LandedCost creation by an authorized user
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" request to read all "IMPORT" PurchaseOrders for LandedCost creation
    Then the following PurchaseOrders will be presented to "Shady.Abdelatif":
      | PurchaseOrder                |
      | 2018000011 - Arrived         |
      | 2018000010 - Shipped         |
      | 2018000008 - Confirmed       |
      | 2018000007 - WaitingApproval |
      | 2018000006 - OpenForUpdates  |
    And total number of PurchaseOrders for LandedCost creation returned to "Shady.Abdelatif" is equal to 5

  #EBS-6815
  Scenario: (02) Read all Local PurchaseOrders for LandedCost creation by an authorized user
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" request to read all "LOCAL" PurchaseOrders for LandedCost creation
    Then the following PurchaseOrders will be presented to "Shady.Abdelatif":
      | PurchaseOrder                 |
      | 2018000019 - DeliveryComplete |
      | 2018000018 - Shipped          |
      | 2018000016 - Approved         |
      | 2018000015 - WaitingApproval  |
      | 2018000014 - OpenForUpdates   |
    And total number of PurchaseOrders for LandedCost creation returned to "Shady.Abdelatif" is equal to 5

  #EBS-6780
  Scenario: (03) Read All Local PurchaseOrders for LandedCost creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" request to read all "LOCAL" PurchaseOrders for LandedCost creation
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
