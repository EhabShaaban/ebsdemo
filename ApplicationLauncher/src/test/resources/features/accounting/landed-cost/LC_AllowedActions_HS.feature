# Author Dev: Evraam Hany
# Author Quality: Khadrah Ali
# Reviewer: Somaya Ahmed (?-Aug-2020)

Feature: Allowed Actions Landed Cost - Home Screen

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
      | CreateOnlyUser  | CreateOnlyRole       |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | Accountant_Signmedia | LandedCostOwner_Signmedia  |
      | M.D                  | LandedCostViewer           |
      | CreateOnlyRole       | CreateOnlySubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | LandedCostOwner_Signmedia  | LandedCost:Create  |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole          | LandedCost:Create  |                                |
      | LandedCostViewer           | LandedCost:ReadAll |                                |
    And the following users doesn't have the following permissions:
      | User           | Permission         |
      | Afaf           | LandedCost:Create  |
      | Afaf           | LandedCost:ReadAll |
      | CreateOnlyUser | LandedCost:ReadAll |
      | Ashraf.Fathi   | LandedCost:Create  |

  #EBS-6777
  Scenario Outline: (01)  Read allowed actions in Landed Cost Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Landed Cost home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User            | AllowedActions |
      | Shady.Abdelatif | ReadAll,Create |
      | Ashraf.Fathi    | ReadAll        |
      | CreateOnlyUser  | Create         |

  #EBS-6777
  Scenario: (02) Read allowed actions in Landed Cost Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Landed Cost home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"