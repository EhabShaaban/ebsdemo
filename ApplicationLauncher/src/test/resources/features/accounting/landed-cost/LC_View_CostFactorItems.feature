# Author Dev: Yara Ameen & Evram Hany
# Author Quality: Eslam Ayman & Khadra Ali & Ehab Shaaban
# Reviewer: Hosam Bayomy & Somaya Ahmed (10-Aug-2020)
# Reviewer: Hosam Bayomy & Somaya Ahmed (21-Mar-2021)
Feature: LandedCost - View CostFactorItems Section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Seif      | Quality_Specialist   |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | LandedCostViewer_Signmedia |
      | Quality_Specialist   | LandedCostViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                    | Condition                      |
      | SuperUserSubRole           | *:*                           |                                |
      | LandedCostViewer_Signmedia | LandedCost:ReadCostFactorItem | [purchaseUnitName='Signmedia'] |
      | LandedCostViewer           | LandedCost:ReadCostFactorItem |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                    | Condition                  |
      | Shady.Abdelatif | LandedCost:ReadCostFactorItem | [purchaseUnitName='Flexo'] |

  #EBS-6705, #EBS-6828
  Scenario: (01) View CostFactorItems section in LandedCost, where all values exist, by an authorized user with condition (Happy Path)
    #@INSERT
    Given the following GeneralData for PurchaseOrders exist:
      | Code       | Type       | State            | CreatedBy | CreationDate       | DocumentOwner |
      | 2020000049 | IMPORT_PO  | DeliveryComplete | Admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
      | 2020000050 | SERVICE_PO | Confirmed        | admin     | 5-Aug-2020 9:02 AM | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for PurchaseOrders exist:
      | Code       | BusinessUnit     | Company          | BankAccountNumber               |
      | 2020000049 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
      | 2020000050 | 0002 - Signmedia | 0001 - AL Madina | 1516171819789 - EGP - Alex Bank |
    #@INSERT
    And the following VendorData for PurchaseOrders exist:
      | Code       | Vendor            | ReferencePO |
      | 2020000050 | 000002 - Zhejiang | 2020000049  |
    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType              | State       | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000012 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil | 04-Jan-2021 09:02 AM | Ashraf Salah  |
      | 2021000013 | OTHER_PARTY_FOR_PURCHASE | PaymentDone | Amr.Khalil | 04-Jan-2021 09:05 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner   | DueDocumentType | DueDocument | NetAmount  | CurrencyISO | CostFactorItem     | BankAccountNumber         | Treasury | Description                 |
      | 2021000012 | BANK        | 000001 - Siegwerk | PURCHASEORDER   | 2020000049  | 300.452165 | EGP         | 000057 - Bank Fees | 1516171819789 - Alex Bank |          | for save payment details hp |
      | 2021000013 | BANK        | 000001 - Siegwerk | PURCHASEORDER   | 2020000049  | 100.00     | EGP         | 000057 - Bank Fees | 1516171819789 - Alex Bank |          | for save payment details hp |
    #@INSERT
    And the following VendorInvoices GeneralData exist:
      | Code       | Type                 | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2020000001 | IMPORT_GOODS_INVOICE | Posted | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |
      | 2020000005 | SHIPMENT_INVOICE     | Posted | Shady.Abdelatif | 24-Apr-2018 11:00 AM | Ashraf Salah  |
    #@INSERT
    And the following VendorInvoices CompanyData exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000005 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following VendorInvoices Details exist:
      | Code       | PurchaseOrder | Vendor            | PaymentTerms                 | InvoiceNumber    | InvoiceDate          | Currency |
      | 2020000001 | 2020000049    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | EGP      |
      | 2020000005 | 2020000050    | 000002 - Zhejiang | 20% advance, 80% Copy of B/L | 1200120301023600 | 24-Apr-2021 06:31 AM | EGP      |
    #@INSERT
    And the following VendorInvoices Items exist:
      | Code       | Item                      | Qty(OrderUnit) | Qty(Base) | UnitPrice(Base) |
      | 2020000005 | 000056 - Shipment service | 130.00 M2      | 130.00 M2 | 9.00            |
    #@INSERT
    And the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2020100010 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted | Shady.Abdelatif | 06-Aug-2020 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2020100010 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following LandedCosts Details exist:
      | LCCode     | POCode     | VendorInvoiceCode | DamageStock | ObjectTypeCode |
      | 2020100010 | 2020000049 | 2020000001        | true        | VendorInvoice  |
    #@INSERT
    And the following LandedCosts CostFactorItems exist:
      | LCCode     | Item                      | EstimatedValue  | ActualValue    | Difference      |
      | 2020100010 | 000056 - Shipment service | 1800.145872 EGP | 1170.00 EGP    | 630.145872 EGP  |
      | 2020100010 | 000057 - Bank Fees        | 0.00 EGP        | 400.452165 EGP | -400.452165 EGP |
    #@INSERT
    And the following LandedCosts Items ActualDetails exist:
      | LCCode     | Item                      | DocumentInformation           | ActualValue |
      | 2020100010 | 000056 - Shipment service | 2020000005 - SHIPMENT_INVOICE | 1170.00     |
      | 2020100010 | 000057 - Bank Fees        | 2021000012 - Payment          | 300.452165  |
      | 2020100010 | 000057 - Bank Fees        | 2021000013 - Payment          | 100.00      |
    And the following CostFactorItemSummary for LandedCosts exist:
      | LCCode     | EstimatedValue  | ActualValue     | Difference     |
      | 2020100010 | 1800.145872 EGP | 1570.452165 EGP | 229.693707 EGP |
    And user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view CostFactorItems section of LandedCost with code "2020100010"
    Then the following values of CostFactorItems section for LandedCost with code "2020100010" are displayed to "Shady.Abdelatif":
      | LCCode     | Item                      | EstimatedValue  | ActualValue    | Difference      |
      | 2020100010 | 000057 - Bank Fees        | 0.0 EGP         | 400.452165 EGP | -400.452165 EGP |
      | 2020100010 | 000056 - Shipment service | 1800.145872 EGP | 1170.0 EGP     | 630.145872 EGP  |
    And the total number of existing records in CostFactorItems section for LandedCost with code "2020100010" is "2"
    And the following values of ActualDetails in CostFactorItems section for LandedCost with code "2020100010" are displayed to "Shady.Abdelatif":
      | Item                      | DocumentInformation           | ActualValue |
      | 000056 - Shipment service | 2020000005 - SHIPMENT_INVOICE | 1170.00     |
      | 000057 - Bank Fees        | 2021000012 - Payment          | 300.452165  |
      | 000057 - Bank Fees        | 2021000013 - Payment          | 100.00      |
    And the total number of existing ActualDetails in CostFactorItem "000056 - Shipment service" in LandedCost with code "2020100010" is "1"
    And the total number of existing ActualDetails in CostFactorItem "000057 - Bank Fees" in LandedCost with code "2020100010" is "2"
    And the following values for CostFactorItems Summary section for LandedCost with code "2020100010" are displayed to "Shady.Abdelatif":
      | EstimateTotalAmount | ActualTotalAmount | DifferenceTotalAmount |
      | 1800.145872 EGP     | 1570.452165 EGP   | 229.693707 EGP        |

  #EBS-6705
  Scenario: (02) View CostFactorItems section in LandedCost, where no values exist yet, by an authorized user (Happy Path)
    #@INSERT
    Given the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2020100004 | IMPORT - Landed Cost For Import PO | Draft | Shady.Abdelatif | 06-Aug-2020 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2020100004 | 0002 - Signmedia | 0001 - AL Madina |
    And user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view CostFactorItems section of LandedCost with code "2020100004"
    Then an empty CostFactorItems section for LandedCost with code "2020100004" is displayed to "Shady.Abdelatif"
    And the total number of existing records in CostFactorItems section for LandedCost with code "2020100004" is "0"
    And empty values for CostFactorItems Summary section for LandedCost with code "2020100004" are displayed to "Shady.Abdelatif"

  Scenario: (03) View CostFactorItems section in LandedCost, where LandedCost doesn't exist (Exception Case)
    #@INSERT
    Given the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2020100004 | IMPORT - Landed Cost For Import PO | Draft | Shady.Abdelatif | 06-Aug-2020 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit     | Company          |
      | 2020100004 | 0002 - Signmedia | 0001 - AL Madina |
    And user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the LandedCost with code "2020100004" successfully
    When "Shady.Abdelatif" requests to view CostFactorItems section of LandedCost with code "2020100004"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-6705
  Scenario: (04) View CostFactorItems section in LandedCost, by unauthorized user due to condition (Abuse Case)
    #@INSERT
    Given the following LandedCosts GeneralData exist:
      | LCCode     | LCType                             | State                 | CreatedBy  | CreationDate         | DocumentOwner   |
      | 2020100011 | IMPORT - Landed Cost For Import PO | ActualValuesCompleted | Amr.Khalil | 06-Aug-2020 12:10 PM | Shady.Abdelatif |
    #@INSERT
    And the following LandedCosts CompanyData exist:
      | LCCode     | BusinessUnit | Company          |
      | 2020100011 | 0001 - Flexo | 0001 - AL Madina |
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view CostFactorItems section of LandedCost with code "2020100011"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
