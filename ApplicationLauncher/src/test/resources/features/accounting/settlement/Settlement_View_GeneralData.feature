# Author Dev: Ahmed Ali
# Author Quality: Khadrah Ali
Feature: Settlement - View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | M.D                  | SettlementViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                      |
      | SuperUserSubRole           | *:*                        |                                |
      | SettlementViewer_Signmedia | Settlement:ReadGeneralData | [purchaseUnitName='Signmedia'] |
      | SettlementViewer           | Settlement:ReadGeneralData |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | Settlement:ReadGeneralData | [purchaseUnitName='Flexo'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft | Shady.Abdelatif | 15-Feb-2021 09:02 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Draft | hr1             | 15-Feb-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0001 - Flexo     | 0001 - AL Madina |

  #EBS-4374
  Scenario: (01) View GeneralData section in Settlement, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of Settlement with code "2021000001"
    Then the following values of GeneralData section for Settlement with code "2021000001" are displayed to "Shady.Abdelatif":
      | SettlementCode | SettlementType | CreatedBy       | State | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Shady.Abdelatif | Draft | 15-Feb-2021 09:02 AM | Ashraf Salah  |

  #EBS-4374
  Scenario Outline: (02) View GeneralData section in Settlement, by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view GeneralData section of Settlement with code "<Code>"
    Then the following values of GeneralData section for Settlement with code "<Code>" are displayed to "Ashraf.Fathi":
      | SettlementCode | SettlementType   | CreatedBy   | State   | CreationDate   | DocumentOwner   |
      | <Code>         | <SettlementType> | <CreatedBy> | <State> | <CreationDate> | <DocumentOwner> |

    Examples:
      | Code       | SettlementType | CreatedBy       | State | CreationDate         | DocumentOwner |
      | 2021000001 | SETTLEMENTS    | Shady.Abdelatif | Draft | 15-Feb-2021 09:02 AM | Ashraf Salah  |
      | 2021000002 | SETTLEMENTS    | hr1             | Draft | 15-Feb-2021 09:02 AM | Ashraf Salah  |

  #EBS-4374
  Scenario: (03) View GeneralData section in Settlement, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of Settlement with code "2021000002"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  @Future
  Scenario: (04) View GeneralData section in Settlement, where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view GeneralData section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
