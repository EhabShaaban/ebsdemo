  # Author: Yara Ameen and Khadrah Ali
  # Reviewer: Somaya Ahmed (01-Mar-2021)

  Feature: Activate Settlement (Auth)

    Background:
      Given the following users and roles exist:
        | Name            | Role                 |
        | Shady.Abdelatif | Accountant_Signmedia |
        | Afaf            | FrontDesk            |
      And the following roles and sub-roles exist:
        | Role                 | Subrole                   |
        | Accountant_Signmedia | SettlementOwner_Signmedia |
      And the following sub-roles and permissions exist:
        | Subrole                   | Permission      | Condition                      |
        | SettlementOwner_Signmedia | Settlement:Post | [purchaseUnitName='Signmedia'] |
      And the following users doesn't have the following permissions:
        | User | Permission      |
        | Afaf | Settlement:Post |
      And the following users have the following permissions without the following conditions:
        | User            | Permission      | Condition                       |
        | Shady.Abdelatif | Settlement:Post | [purchaseUnitName='Corrugated'] |

      #@INSERT
      And the following Settlements GeneralData exist:
        | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner   |
        | 2021000021     | SETTLEMENTS    | Draft | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
        | 2021000022     | SETTLEMENTS    | Draft | Ashraf Salah    | 05-Aug-2018 09:02 AM | Ashraf Salah    |
      #@INSERT
      And the following Settlements CompanyData exist:
        | SettlementCode | BusinessUnit      | Company          |
        | 2021000021     | 0002 - Signmedia  | 0001 - AL Madina |
        | 2021000022     | 0006 - Corrugated | 0001 - AL Madina |

  #EBS-1312
    Scenario Outline: (01) Activate Settlement by unauthorized user (Unauthorized/Abuse case)
      Given user is logged in as "<User>"
      When "<User>" activates the Settlement with Code "2021000021" and JournalEntryDate "17-Jun-2022 11:00 AM" at "01-Mar-2021 09:30 AM"
      Then "<User>" is logged out
      And "<User>" is forwarded to the error page
      Examples:
        | User |
        | Afaf |

  #EBS-1312
    Scenario: (02) Activate Settlement by unauthorized user due to condition (Unauthorized/Abuse case)
      Given user is logged in as "Shady.Abdelatif"
      When "Shady.Abdelatif" activates the Settlement with Code "2021000022" and JournalEntryDate "17-Jun-2022 11:00 AM" at "01-Mar-2021 09:30 AM"
      Then "Shady.Abdelatif" is logged out
      And "Shady.Abdelatif" is forwarded to the error page
