# Author: Evram Hany, Ahmed Ali & Ehab Shabaan
# Reviewer: Somaya Ahmed (01-Mar-2021)

Feature: Settlement View ActivationDetails section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | AdminTreasury        | SettlementViewer           |
      | SuperUser            | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | SettlementViewer_Signmedia | Settlement:ReadActivationDetails | [purchaseUnitName='Signmedia'] |
      | SettlementViewer           | Settlement:ReadActivationDetails |                                |
      | SuperUserSubRole           | *:*                              |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | Settlement:ReadActivationDetails | [purchaseUnitName='Flexo'] |
    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft  | Shady.Abdelatif | 01-Mar-2021 10:00 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Posted | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000022     | SETTLEMENTS    | Posted | Ashraf Salah    | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit      | Company          |
      | 2021000001     | 0002 - Signmedia  | 0001 - AL Madina |
      | 2021000002     | 0002 - Signmedia  | 0001 - AL Madina |
      | 2021000022     | 0006 - Corrugated | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000001     | USD      |
      | 2021000002     | EGP      |
      | 2021000022     | USD      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value |
      | 2021000001     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000  |
      | 2021000001     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000  |
      | 2021000002     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 300   |
      | 2021000002     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 300   |
      | 2021000022     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 540   |
      | 2021000022     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 540   |

      #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

     #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument       | Company          |
      | 2021000101 | 01-Mar-2021 10:30 AM | Shady.Abdelatif | 02-Mar-2021 10:30 AM | 0002 - Signmedia  | 2021         | 2021000002 - Settlement | 0001 - AL Madina |
      | 2021000102 | 01-Mar-2021 10:30 AM | Admin           | 02-Mar-2021 10:30 AM | 0006 - Corrugated | 2021         | 2021000022 - Settlement | 0001 - AL Madina |

    #@INSERT
    And the following Settlements ActivationDetails exist:
      | Code       | Accountant      | ActivationDate       | CurrencyPrice       | JournalEntryCode | FiscalYear |
      | 2021000002 | Shady.Abdelatif | 01-Mar-2021 10:30 AM | 1.0 EGP = 1.0 EGP   | 2021000101       | 2021       |
      | 2021000022 | Admin           | 01-Mar-2021 10:30 AM | 1.0 USD = 17.44 EGP | 2021000102       | 2021       |



  #EBS-4379
  Scenario: (01) View ActivationDetails section in Settlement - by an authorized user who has one role with condition(Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ActivationDetails section of Settlement with code "2021000002"
    Then the following values of ActivationDetails section of Settlement with code "2021000002" are displayed to "Shady.Abdelatif":
      | Code       | Accountant      | ActivationDate       | JournalEntryCode | FiscalYear | CurrencyPrice     |
      | 2021000002 | Shady.Abdelatif | 01-Mar-2021 10:30 AM | 2021000101       | 2021       | 1.0 EGP = 1.0 EGP |

  #EBS-4379
  Scenario Outline: (02) View ActivationDetails section in Settlement - by an authorized user who has one role with no condition(Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view ActivationDetails section of Settlement with code "<Code>"
    Then the following values of ActivationDetails section of Settlement with code "<Code>" are displayed to "Ahmed.Hamdi":
      | Code   | Accountant   | ActivationDate   | JournalEntryCode   | FiscalYear   | CurrencyPrice   |
      | <Code> | <Accountant> | <ActivationDate> | <JournalEntryCode> | <FiscalYear> | <CurrencyPrice> |
    Examples:
      | Code       | Accountant      | ActivationDate       | JournalEntryCode | FiscalYear | CurrencyPrice       |
      | 2021000002 | Shady.Abdelatif | 01-Mar-2021 10:30 AM | 2021000101       | 2021       | 1.0 EGP = 1.0 EGP   |
      | 2021000022 | Admin           | 01-Mar-2021 10:30 AM | 2021000102       | 2021       | 1.0 USD = 17.44 EGP |

  #EBS-4379
  Scenario: (03) View ActivationDetails section in Settlement - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ActivationDetails section of Settlement with code "2021000022"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-4379
  @Future
  Scenario: (04) View ActivationDetails section in Settlement - where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view ActivationDetails section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
