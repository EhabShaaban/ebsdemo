# Author: Yara Ameen and Khadrah Ali
# Reviewer: Somaya Ahmed (01-Mar-2021)

Feature: Settlement - View Company section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                      |
      | SuperUserSubRole           | *:*                        |                                |
      | SettlementViewer_Signmedia | Settlement:ReadCompanyData | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | Settlement:ReadCompanyData | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User | Permission                 |
      | Afaf | Settlement:ReadCompanyData |

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft | Shady.Abdelatif | 15-Feb-2021 09:02 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Draft | hr1             | 15-Feb-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0001 - Flexo     | 0001 - AL Madina |

  #EBS-4375
  Scenario: (01) View Company section in Settlement, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Company section of Settlement with code "2021000001"
    Then the following values of CompanyData section for Settlement with code "2021000001" are displayed to "Shady.Abdelatif":
      | BusinessUnit | Company   |
      | Signmedia    | AL Madina |

  #EBS-4375
  Scenario: (02) View Company section in Settlement, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Company section of Settlement with code "2021000002"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-4375
  Scenario: (03) View Company section in Settlement, by unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to view Company section of Settlement with code "2021000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-4375
  @Future
  Scenario: (04) View Company section in Settlement, where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view Company section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
