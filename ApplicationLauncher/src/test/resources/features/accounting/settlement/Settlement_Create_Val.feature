# Author Dev: Yara Ameen
# Author Quality:
Feature: Create Settlement (Val)

  Background:

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission        | Condition |
      | SettlementOwner_Signmedia | Settlement:Create |           |

    And the following BusinessUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Settlement Types exist:
      | Code        |
      | SETTLEMENTS |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition                      |
      | 34 | Ashraf Salah | Settlement     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |

  #EBS-8595
  Scenario: (01) Create Settlement, with missing Mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Settlement with the following values:
      | Type        | BusinessUnit | Company | Currency | DocumentOwner |
      | SETTLEMENTS | 0002         |         | 0001     | 34            |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-8595
  Scenario: (02) Create Settlement, with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Settlement with the following values:
      | Type   | BusinessUnit | Company | Currency | DocumentOwner |
      | ay7aga | 0002         | 0002    | 0001     | 34            |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

