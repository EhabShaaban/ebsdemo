# Author Dev: Yara Ameen
# Author Quality: Khadrah Ali
Feature: Settlement Document Owner DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole             |
      | Accountant_Signmedia | DocumentOwnerViewer |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                    | Condition |
      | DocumentOwnerViewer     | DocumentOwner:ReadAll         |           |
      | SettlementDocumentOwner | Settlement:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject | Permission         | Condition                      |
      | 34 | Ashraf Salah    | Settlement     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
      | 40 | Shady.Abdelatif | Settlement     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |

  #EBS-1063
  Scenario: (01) Read All DocumentOwners Dropdown in Settlement creation by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all DocumentOwners for "Settlement"
    Then the following DocumentOwners values will be presented to "Shady.Abdelatif":
      | Name            |
      | Ashraf Salah    |
      | Shady.Abdelatif |
    And total number of DocumentOwners returned to "Shady.Abdelatif" in a dropdown is equal to 2

  #EBS-1063
  Scenario: (02) Read All DocumentOwners in Settlement creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "Settlement"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
