# Author Dev: Evram Hany
# Author Quality: Ehab Shabaan
Feature: View Accounting Details section

  Background: 
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | SuperUserSubRole           | *:*                              |                                |
      | SettlementViewer_Signmedia | Settlement:ReadAll               | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadAccountingDetails | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                       |
      | Shady.Abdelatif | Settlement:ReadAccountingDetails | [purchaseUnitName='Corrugated'] |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner   |
      |     2021000021 | SETTLEMENTS    | Draft  | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
      |     2021000022 | SETTLEMENTS    | Posted | Ashraf Salah    | 05-Aug-2018 09:02 AM | Ashraf Salah    |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit      | Company          |
      |     2021000021 | 0002 - Signmedia  | 0001 - AL Madina |
      |     2021000022 | 0006 - Corrugated | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      |     2021000021 | EGP      |
      |     2021000022 | USD      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value       |
      |     2021000021 | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.000322 |
      |     2021000021 | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000.000322 |
      |     2021000022 | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               |         540 |
      |     2021000022 | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank |         540 |

  # EBS-4376
  Scenario: (01) View Settlement AccountingDetails section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view AccountingDetails section of Settlement with code "2021000021"
    Then the following values of AccountingDetails section for Settlement with code "2021000021" are displayed to "Shady.Abdelatif":
      | AccountType | GLAccount                 | GLSubAccount                    | Amount      |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.000322 |
      | CREDIT      | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000.000322 |

  # EBS-4376
  @Future
  Scenario: (02) View AccountingDetails section where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2019000030" successfully
    When "Ahmed.Hamdi" requests to view AccountingDetails section of Settlement with code "2019000030"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"

  # EBS-4376
  Scenario: (03) View Settlement AccountingDetails section by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view AccountingDetails section of Settlement with code "2021000022"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
