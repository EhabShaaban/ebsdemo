# Author Dev: Yara Ameen, Khadrah Ali (EBS-1317)
# Reviewer: Hosam Bayomy and Somaya Ahmed (01-Mar-2021)

Feature: Settlement Save Accounting Details (Auth)

  Background:
    Given the following users and roles exist:
      | Name                       | Role                                          |
      | Afaf                       | FrontDesk                                     |
      | Shady.Abdelatif            | Accountant_Signmedia                          |
      | Shady.Abdelatif.NoAccounts | Accountant_Signmedia_CannotReadGLAccountsRole |
    And the following roles and sub-roles exist:
      | Role                                          | Sub-role                  |
      | Accountant_Signmedia                          | SettlementOwner_Signmedia |
      | Accountant_Signmedia                          | ChartOfAccountsViewer     |
      | Accountant_Signmedia_CannotReadGLAccountsRole | SettlementOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | SettlementOwner_Signmedia | Settlement:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewer     | GLAccount:ReadAll                  |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Settlement:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                       | Permission                         |
      | Shady.Abdelatif.NoAccounts | GLAccount:ReadAll                  |
      | Afaf                       | Settlement:UpdateAccountingDetails |
      | Afaf                       | GLAccount:ReadAll                  |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000003     | SETTLEMENTS    | Draft | hr1             | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003     | 0001 - Flexo     | 0001 - AL Madina |

    # Add a background statement for the used GLAcounts, Company BankAccounts (related to El-madina company), and Vendors as follows "The following .... exist" ( reuse existing statements)

     #EBS-1317
  Scenario: (01) Save new Settlement AccountingDetails by unauthorized user (Unauthorized/Abuse case)
    Given  user is logged in as "Afaf"
    When "Afaf" saves the following new AccountingDetails record in Settlement with code "2021000001" at "01-Mar-2021 11:10 AM" with the following values:
      | AccountType | GLAccount    | GLSubAccount                    | Amount |
      | CREDIT      | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 1000   |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

    #EBS-1317
  Scenario: (02) Save new Settlement AccountingDetails by unauthorized user due to condition (Unauthorized/Abuse case)
    Given  user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" saves the following new AccountingDetails record in Settlement with code "2021000003" at "01-Mar-2021 11:10 AM" with the following values:
      | AccountType | GLAccount                 | GLSubAccount      | Amount |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 1000   |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    #EBS-1317
  Scenario: (03) Save new Settlement AccountingDetails by user that is not authorized to read the possible values of the data entry fields (Abuse case)
    Given  user is logged in as "Shady.Abdelatif.NoAccounts"
    When "Shady.Abdelatif.NoAccounts" saves the following new AccountingDetails record in Settlement with code "2021000001" at "01-Mar-2021 11:10 AM" with the following values:
      | AccountType | GLAccount                 | GLSubAccount      | Amount |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang | 1000   |
    Then "Shady.Abdelatif.NoAccounts" is logged out
    And "Shady.Abdelatif.NoAccounts" is forwarded to the error page



