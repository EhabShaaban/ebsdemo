# Author: Ahmed Ali - Evram Hany
# Reviewer: Hosam Bayomy and Somaya Ahmed (01-Mar-2021)
Feature: Settlement Save AccountingDetails (HP)

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
      | Accountant_Signmedia | ChartOfAccountsViewer     |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | SettlementOwner_Signmedia | Settlement:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewer     | GLAccount:ReadAll                  |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Settlement:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000003     | SETTLEMENTS    | Draft | hr1             | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value   |
      | 2021000001     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.256943 |
      | 2021000001     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000.256943 |
      | 2021000003     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000.00 |

  #EBS-1314
  Scenario: (01) Save new Settlement AccountingDetails "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session where account has subLeager
    Given user is logged in as "Shady.Abdelatif"
    And AccountingDetails section of Settlement with code "2021000001" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new AccountingDetails record in Settlement with code "2021000001" at "01-Jan-2019 11:10 AM" with the following values:
      | AccountType | GLAccount    | GLSubAccount                    | Amount |
      | DEBIT       | 10431 - Bank | 1516171819789 - EGP - Alex Bank | 200.00 |
    Then Settlement with code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2019 11:10 AM | Draft |
    And AccountingDetails section of Settlement with code "2021000001" is Updated as follows:
      | AccountType | GLAccount                 | GLSubAccount                    | Amount  |
      | DEBIT       | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.256943 |
      | CREDIT      | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000.256943 |
      | DEBIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 200.00  |
    And the lock by "Shady.Abdelatif" on AccountingDetails section of Settlement with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-16"

  #EBS-1314
  Scenario: (02) Save new Settlement AccountingDetails "editable" Fields in "editable states" - with all fields or only mandatory fields - within edit session where account doesnt has subLeager
    Given user is logged in as "2021000003"
    And AccountingDetails section of Settlement with code "2021000003" is locked by "Shady.Abdelatif" at "01-Jan-2019 11:00 AM"
    When "Shady.Abdelatif" saves the following new AccountingDetails record in Settlement with code "2021000003" at "01-Jan-2019 11:10 AM" with the following values:
      | AccountType | GLAccount              | GLSubAccount | Amount |
      | DEBIT       | 30101 - sales expenses |              | 500.00 |
    Then Settlement with code "2021000003" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2019 11:10 AM | Draft |
    And AccountingDetails section of Settlement with code "2021000003" is Updated as follows:
      | AccountType | GLAccount              | GLSubAccount                    | Amount  |
      | DEBIT       | 30101 - sales expenses |                                 | 500.00  |
      | CREDIT      | 10431 - Bank           | 1516171819789 - EGP - Alex Bank | 1000.00 |
    And the lock by "Shady.Abdelatif" on AccountingDetails section of Settlement with code "2021000003" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-16"
