# Author: Evram Hany, Khadra Ali (EBS-1309), Ahmed Ali (EBS-8516)
# Author: Mohamed Aboelnour, Ehab Shaban (EBS-1310)
# Author: Mohamed Aboelnour (EBS-4579)
# Reviewer: Hosam Bayomy and Somaya Ahmed (01-Mar-2021)
# updated by: Fatma Al Zahraa (EBS - 8967)
Feature: Activate Settlement (Happy Path)

  - Activating Settlement results in the following:
  - Settlement state is changed to Active
  - Journal Entries for Settlement is created, and Activation Details determined where
  - the retrieved fiscal year is the active one where the due date belongs to it
  - latest ExchangeRate is retrieved based on of default strategy
  - Settlement Accounting Details are determined
  - Increase & Decrease related balances in case of CASH or BANK GLs (EBS-8516)
  - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date EBS-8967

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission      | Condition                      |
      | SettlementOwner_Signmedia | Settlement:Post | [purchaseUnitName='Signmedia'] |
    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank             | AccountNumber       |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr | 1234567893458 - EGP |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank | 1516171819789 - EGP |
    And the following GLAccounts has the following Subledgers:
      | GLAccount                                | Subledger     |
      | 10221 - service vendors 2                | Local_Vendors |
      | 10431 - Bank                             | Banks         |
      | 41101 - Realized Differences on Exchange |               |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44332    | 19-Feb-2021 10:00 PM | User Daily Rate       |
      | 2020000036 | USD              | EGP               | 17.447259   | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 19-Mar-2021 10:00 PM | Central Bank Of Egypt |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |


  #EBS-1309, #EBS-1310
  Scenario: (01) Activate Settlement with all mandatory fields by an authorized user (Happy Path)
  """
    - Total debit amount equal total credit amount
    - All mandatory fields
    - By an authorized user
    - Accounting Details - Credit Bank
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber                            | Balance |
      | BANK00020001000200021011121314678 | 0002 - Signmedia | 0001 - AL Madina | 0002 - USD | 1011121314678 - USD - National Bank of Egypt | 40000   |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2021000021     | SETTLEMENTS    | Draft | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000021     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000021     | USD      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                                   | Value       |
      | 2021000021     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang                            | 1000.478921 |
      | 2021000021     | CREDIT       | 10431 - Bank              | 1011121314678 - USD - National Bank of Egypt | 1000.478921 |
    And the last created JournalEntry was with code "2021000001"
    When "Shady.Abdelatif" activates the Settlement with Code "2021000021" and JournalEntryDate "17-Jun-2022 11:00 AM" at "07-Jan-2021 09:30 AM"
    Then Settlement with Code "2021000021" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Active |
    And Activation Details in Settlement with Code "2021000021" is updated as follows:
      | Accountant      | ActivationDate       | DueDate              | LatestExchangeRateCode | CurrencyPrice | FiscalYear |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 17-Jun-2022 11:00 AM | 2020000036             | 17.447259     | 2022       |
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000002 | 07-Jan-2021 09:30 AM | Shady.Abdelatif | 17-Jun-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000021 - Settlement | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2021000002":
      | DueDate              | GLAccount                 | GLSubAccount                                 | Credit      | Debit       | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 17-Jun-2022 11:00 AM | 10221 - service vendors 2 | 000002 - Zhejiang                            | 0           | 1000.478921 | USD                | EGP             | 17.447259                 | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10431 - Bank              | 1011121314678 - USD - National Bank of Egypt | 1000.478921 | 0           | USD                | EGP             | 17.447259                 | 2020000036       |
    And JournalBalance with code "BANK00020001000200021011121314678" is updated as follows "38999.521079"
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47"

  #EBS-8463
  Scenario: (02) Activate Settlement with all mandatory fields by an authorized user (Happy Path)
  """
    - Total debit amount equal total credit amount
    - Accounting sub ledger is empty
    - All mandatory fields
    - By an authorized user
    - With accounting subledger is 'NONE'
    - Accounting Details - Credit Bank
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber                            | Balance |
      | BANK00020001000200021011121314678 | 0002 - Signmedia | 0001 - AL Madina | 0002 - USD | 1011121314678 - USD - National Bank of Egypt | 40000   |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000024     | SETTLEMENTS    | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000024     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000024     | USD      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                                  | SubAccount                                   | Value |
      | 2021000024     | DEBIT        | 41101 - Realized Differences on Exchange |                                              | 500   |
      | 2021000024     | CREDIT       | 10431 - Bank                             | 1011121314678 - USD - National Bank of Egypt | 500   |
    And the last created JournalEntry was with code "2021000001"
    When "Shady.Abdelatif" activates the Settlement with Code "2021000024" and JournalEntryDate "17-Jun-2022 11:00 AM" at "07-Jan-2021 09:30 AM"
    Then Settlement with Code "2021000024" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Active |
    And Activation Details in Settlement with Code "2021000024" is updated as follows:
      | Accountant      | ActivationDate       | DueDate              | LatestExchangeRateCode | CurrencyPrice | FiscalYear |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 17-Jun-2022 11:00 AM | 2020000036             | 17.447259     | 2022       |
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000002 | 07-Jan-2021 09:30 AM | Shady.Abdelatif | 17-Jun-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000024 - Settlement | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2021000002":
      | DueDate              | GLAccount                                | GLSubAccount                                 | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 17-Jun-2022 11:00 AM | 41101 - Realized Differences on Exchange |                                              | 0      | 500   | USD                | EGP             | 17.447259                 | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10431 - Bank                             | 1011121314678 - USD - National Bank of Egypt | 500    | 0     | USD                | EGP             | 17.447259                 | 2020000036       |
    And JournalBalance with code "BANK00020001000200021011121314678" is updated as follows "39500"
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47"

  #EBS-8516
  Scenario: (03) Activate Settlement with all mandatory fields by an authorized user (Happy Path)
  """
    - Total debit amount equal total credit amount
    - All mandatory fields
    - By an authorized user
    - Accounting Details - Debit Treasury
    - Accounting Details - Credit Bank
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber                            | Balance |
      | BANK00020001000200021011121314678 | 0002 - Signmedia | 0001 - AL Madina | 0002 - USD | 1011121314678 - USD - National Bank of Egypt | 40000   |
    #@INSERT
    And insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100020002 | 0002 - Signmedia | 0001 - AL Madina | 0002 - USD | 0002 - Signmedia Treasury | 10000   |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000024     | SETTLEMENTS    | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000024     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000024     | USD      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account      | SubAccount                                   | Value |
      | 2021000024     | DEBIT        | 10430 - Cash | 0002 - Signmedia Treasury                    | 500   |
      | 2021000024     | CREDIT       | 10431 - Bank | 1011121314678 - USD - National Bank of Egypt | 500   |
    And the last created JournalEntry was with code "2021000001"
    When "Shady.Abdelatif" activates the Settlement with Code "2021000024" and JournalEntryDate "17-Jun-2022 11:00 AM" at "07-Jan-2021 09:30 AM"
    Then Settlement with Code "2021000024" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State  |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Active |
    And Activation Details in Settlement with Code "2021000024" is updated as follows:
      | Accountant      | ActivationDate       | DueDate              | LatestExchangeRateCode | CurrencyPrice | FiscalYear |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | 17-Jun-2022 11:00 AM | 2020000036             | 17.447259     | 2022       |
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000002 | 07-Jan-2021 09:30 AM | Shady.Abdelatif | 17-Jun-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000024 - Settlement | 0001 - AL Madina |
    And the following new JournalEntryDetails are created for JournalEntry with code "2021000002":
      | JournalDate          | GLAccount    | GLSubAccount                                 | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 17-Jun-2022 11:00 AM | 10430 - Cash | 0002 - Signmedia Treasury                    | 0      | 500   | USD                | EGP             | 17.447259                 | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10431 - Bank | 1011121314678 - USD - National Bank of Egypt | 500    | 0     | USD                | EGP             | 17.447259                 | 2020000036       |
    And JournalBalance with code "TREASURY0002000100020002" is updated as follows "10500"
    And JournalBalance with code "BANK00020001000200021011121314678" is updated as follows "39500"
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-47"
