# Author: Mohamed Aboelnour
# Author Quality: Khadra Ali
# Reviewer: Somaya Ahmed (??-Mar-2021)

Feature: Settlement View Summary section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | SuperUser            | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission             | Condition                      |
      | SettlementViewer_Signmedia | Settlement:ReadSummary | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole           | *:*                    |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission             | Condition                  |
      | Shady.Abdelatif | Settlement:ReadSummary | [purchaseUnitName='Flexo'] |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft  | Shady.Abdelatif | 01-Mar-2021 10:00 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Posted | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000022     | SETTLEMENTS    | Posted | Ashraf Salah    | 05-Aug-2018 09:02 AM | Ashraf Salah  |

    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000022     | 0001 - Flexo     | 0001 - AL Madina |

    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000001     | USD      |
      | 2021000002     | EGP      |
      | 2021000022     | USD      |

    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value   |
      | 2021000001     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.00 |
      | 2021000001     | CREDIT       | 10431 - Bank              | 1516171819712 - USD - Alex Bank | 700.00  |
      | 2021000001     | CREDIT       | 10431 - Bank              | 1234567893499 - USD - Bank Misr | 300.00  |

      | 2021000002     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 300.00  |
      | 2021000002     | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 100.00  |
      | 2021000002     | CREDIT       | 10431 - Bank              | 1234567893458 - EGP - Bank Misr | 200.00  |

      | 2021000022     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 540.00  |
      | 2021000022     | CREDIT       | 10431 - Bank              | 1516171819712 - USD - Alex Bank | 240.00  |
      | 2021000022     | CREDIT       | 10431 - Bank              | 1234567893499 - USD - Bank Misr | 300.00  |

  #EBS-1318
  Scenario Outline: (01) View Summary section in Settlement - by an authorized user who has one role with condition(Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Summary section of Settlement with code "<Code>"
    Then the following values of Summary section of Settlement with code "<Code>" are displayed to "Shady.Abdelatif":
      | Code   | TotalDebit   | TotalCredit   |
      | <Code> | <TotalDebit> | <TotalCredit> |
    Examples:
      | Code       | TotalDebit  | TotalCredit |
      | 2021000001 | 1000.00 USD | 1000.00 USD |
      | 2021000002 | 300.00 EGP  | 300.00 EGP  |

  #EBS-1318
  Scenario: (02) View Summary section in Settlement - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Summary section of Settlement with code "2021000022"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-1318
  @Future
  Scenario: (03) View Summary section in Settlement - where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view Summary section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
