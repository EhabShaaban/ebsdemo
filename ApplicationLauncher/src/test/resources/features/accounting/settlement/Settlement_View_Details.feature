# Author Dev: Ahmed Ali
# Author Quality: Khadrah Ali
# Author PO : Somaya Abolwafaa
Feature: View Settlement Details

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | M.D                  | SettlementViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | SuperUserSubRole           | *:*                              |                                |
      | SettlementViewer_Signmedia | Settlement:ReadSettlementDetails | [purchaseUnitName='Signmedia'] |
      | SettlementViewer           | Settlement:ReadSettlementDetails |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | Settlement:ReadSettlementDetails | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft | Amr.Khalil | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Draft | Amr.Khalil | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0001 - Flexo     | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000001     | USD      |
      | 2021000002     | EGP      |

  # EBS-1190
  Scenario: (01) View SettlementDetails section in Settlement by an authorized user due to condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view SettlementDetails section of Settlement with code "2021000001"
    Then the following values of SettlementDetails for Settlement with code "2021000001" are displayed to "Shady.Abdelatif":
      | SettlementCode | Currency                   |
      | 2021000001     | USD - United States Dollar |

  # EBS-1190
  Scenario: (02) View SettlementDetails section in Settlement by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view SettlementDetails section of Settlement with code "2021000002"
    Then the following values of SettlementDetails for Settlement with code "2021000002" are displayed to "Ashraf.Fathi":
      | SettlementCode | Currency             |
      | 2021000002     | EGP - Egyptian Pound |

  # EBS-1190
  Scenario: (03) View SettlementDetails section in Settlement by unauthorized user due to condition (Authorization error)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view SettlementDetails section of Settlement with code "2021000002"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  @Future
  Scenario: (04) View SettlementDetails section in Settlement where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "Shady.Abdelatif"
    And "Shady.Abdelatif" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view SettlementDetails section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
