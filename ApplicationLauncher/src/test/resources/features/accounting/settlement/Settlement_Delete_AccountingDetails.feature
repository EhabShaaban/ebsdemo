# Author Dev: Yara Ameen
# Author Quality: Khadra Ali
Feature: Settlement Delete Accounting Details section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                  |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
      | SuperUser            | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | SettlementOwner_Signmedia | Settlement:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Settlement:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User | Permission                         |
      | Afaf | Settlement:UpdateAccountingDetails |

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft  | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Posted | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000003     | SETTLEMENTS    | Draft  | hr1             | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003     | 0001 - Flexo     | 0001 - AL Madina |

      #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | DetailId | Credit/Debit | Account                   | SubAccount                      | Value |
      | 2021000001     | 1        | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000  |
      | 2021000001     | 2        | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 1000  |
      | 2021000002     | 3        | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 300   |
      | 2021000002     | 4        | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 300   |
      | 2021000003     | 5        | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 750   |
      | 2021000003     | 6        | CREDIT       | 10431 - Bank              | 1516171819789 - EGP - Alex Bank | 750   |




  # EBS-4341
  Scenario: (01) Delete Settlement AccountingDetails, where Settlement is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "2" in Settlement with code "2021000001"
    Then AccountingDetail with id "2" from Settlement with code "2021000001" is deleted
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-12"

  # EBS-4341
  Scenario: (02) Delete Settlement AccountingDetails, where AccountingDetail doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted AccountingDetail with id "2" of Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "2" in Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-20"

  # EBS-4341
  Scenario: (03) Delete Settlement AccountingDetails, where action is not allowed in current state - All states except Draft (Exception)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "3" in Settlement with code "2021000002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-21"


  # EBS-4341
  @Future
  Scenario: (04) Delete Settlement AccountingDetails, where Settlement doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "2" in Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
    
    
  # EBS-4341
  Scenario: (05) Delete Settlement AccountingDetails, when AccountingDetails section is locked by another user (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    When "hr1" first requested to add a record to AccountingDetails section of Settlement with code "2021000001"
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "2" in Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

  # EBS-4341
  Scenario: (06) Delete Settlement AccountingDetails, by Unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete AccountingDetail with id "6" in Settlement with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  # EBS-4341
  Scenario: (07) Delete Settlement AccountingDetails, by Unauthorized user (Abuse Case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to delete AccountingDetail with id "2" in Settlement with code "2021000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page


