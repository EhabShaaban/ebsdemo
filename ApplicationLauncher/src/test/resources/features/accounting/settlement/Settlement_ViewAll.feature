# Author Dev: Evram Hany
# Author Quality: Khadra Ali
# Author PO : Somaya Abolwafaa
Feature: View All Settlement

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | M.D                  | SettlementViewer           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | SettlementViewer_Signmedia | Settlement:ReadAll | [purchaseUnitName='Signmedia'] |
      | SettlementViewer           | Settlement:ReadAll |                                |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | Settlement:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User            | Permission         | Condition                       |
      | Shady.Abdelatif | Settlement:ReadAll | [purchaseUnitName='Flexo']      |
      | Shady.Abdelatif | Settlement:ReadAll | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadAll | [purchaseUnitName='Offset']     |

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy    | CreationDate         | DocumentOwner   |
      | 2021000020     | SETTLEMENTS    | Draft  | Amr.Khalil   | 05-Aug-2018 09:02 AM | Ashraf Salah    |
      | 2021000021     | SETTLEMENTS    | Draft  | Gehan.Ahmed  | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
      | 2021000022     | SETTLEMENTS    | Posted | Amr.Khalil   | 05-Aug-2018 09:02 AM | Ashraf Salah    |
      | 2021000023     | SETTLEMENTS    | Draft  | Amr.Khalil   | 05-Aug-2018 09:02 AM | Ashraf Salah    |
      | 2021000024     | SETTLEMENTS    | Draft  | Gehan.Ahmed  | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
      | 2021000025     | SETTLEMENTS    | Posted | Ashraf.Salah | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
      | 2021000026     | SETTLEMENTS    | Draft  | Amr.Khalil   | 04-Jan-2021 09:02 AM | Ashraf Salah    |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit      | Company          |
      | 2021000020     | 0001 - Flexo      | 0001 - AL Madina |
      | 2021000021     | 0002 - Signmedia  | 0001 - AL Madina |
      | 2021000022     | 0006 - Corrugated | 0001 - AL Madina |
      | 2021000023     | 0001 - Flexo      | 0001 - AL Madina |
      | 2021000024     | 0002 - Signmedia  | 0002 - DigiPro   |
      | 2021000025     | 0002 - Signmedia  | 0002 - DigiPro   |
      | 2021000026     | 0001 - Flexo      | 0002 - DigiPro   |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000020     | USD      |
      | 2021000021     | EGP      |
      | 2021000022     | USD      |
      | 2021000023     | EUR      |
      | 2021000024     | USD      |
      | 2021000025     | EGP      |
      | 2021000026     | EGP      |

       #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |

     #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy       | JournalDate          | BusinessUnit      | FiscalPeriod | ReferenceDocument       | Company          |
      | 2021000122 | 06-Aug-2019 09:02 AM | Ashraf.Fathi    | 07-Aug-2019 09:02 AM | 0006 - Corrugated | 2019         | 2021000022 - Settlement | 0001 - AL Madina |
      | 2021000125 | 06-Aug-2019 09:02 AM | Shady.Abdelatif | 02-Mar-2021 11:30 AM | 0002 - Signmedia  | 2019         | 2021000025 - Settlement | 0002 - DigiPro   |


     #@INSERT
    And the following Settlements ActivationDetails exist:
      | Code       | Accountant      | ActivationDate       | CurrencyPrice       | JournalEntryCode | FiscalYear |
      | 2021000022 | Ashraf.Fathi    | 06-Aug-2019 09:02 AM | 1.0 USD = 17.44 EGP | 2021000122       | 2019       |
      | 2021000025 | Shady.Abdelatif | 06-Aug-2019 09:02 AM | 1.0 EGP = 1.0 EGP   | 2021000125       | 2019       |

    And the total number of existing records are 7


  #EBS-1173
  Scenario: (01) View All Settlements by authorized user without condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with no filter applied with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy     | BusinessUnit      | State  | Company          | CreationDate         |
      | 2021000026 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0002 - DigiPro   | 04-Jan-2021 09:02 AM |
      | 2021000025 | SETTLEMENTS | Shady.Abdelatif | 0002 - Signmedia  | Posted | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000024 | SETTLEMENTS |                 | 0002 - Signmedia  | Draft  | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000023 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000022 | SETTLEMENTS | Ashraf.Fathi    | 0006 - Corrugated | Posted | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000021 | SETTLEMENTS |                 | 0002 - Signmedia  | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000020 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 7

  #EBS-1173
  Scenario: (02) View All Settlements by authorized user due to condition on (Business Unit) (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of Settlements with no filter applied with 10 records per page
    Then the following Settlements will be presented to "Shady.Abdelatif":
      | Code       | Type        | ActivatedBy     | BusinessUnit     | State  | Company          | CreationDate         |
      | 2021000025 | SETTLEMENTS | Shady.Abdelatif | 0002 - Signmedia | Posted | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000024 | SETTLEMENTS |                 | 0002 - Signmedia | Draft  | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000021 | SETTLEMENTS |                 | 0002 - Signmedia | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Shady.Abdelatif" are 3

  #EBS-1173
  Scenario: (03) View All Settlements filtered by Code by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on Code which contains "00020" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy | BusinessUnit | State | Company          | CreationDate         |
      | 2021000020 | SETTLEMENTS |             | 0001 - Flexo | Draft | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-1173
  Scenario: (04) View All Settlements filtered by Type by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on Type which equals "SETTLEMENTS" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy     | BusinessUnit      | State  | Company          | CreationDate         |
      | 2021000026 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0002 - DigiPro   | 04-Jan-2021 09:02 AM |
      | 2021000025 | SETTLEMENTS | Shady.Abdelatif | 0002 - Signmedia  | Posted | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000024 | SETTLEMENTS |                 | 0002 - Signmedia  | Draft  | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000023 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000022 | SETTLEMENTS | Ashraf.Fathi    | 0006 - Corrugated | Posted | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000021 | SETTLEMENTS |                 | 0002 - Signmedia  | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000020 | SETTLEMENTS |                 | 0001 - Flexo      | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 7

  #EBS-1173
  Scenario: (05) View All Settlements filtered by Company by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on Company which contains "Digi" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy     | BusinessUnit     | State  | Company        | CreationDate         |
      | 2021000026 | SETTLEMENTS |                 | 0001 - Flexo     | Draft  | 0002 - DigiPro | 04-Jan-2021 09:02 AM |
      | 2021000025 | SETTLEMENTS | Shady.Abdelatif | 0002 - Signmedia | Posted | 0002 - DigiPro | 05-Aug-2018 09:02 AM |
      | 2021000024 | SETTLEMENTS |                 | 0002 - Signmedia | Draft  | 0002 - DigiPro | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 3

  #EBS-1173
  Scenario: (06) View All Settlements filtered by CreatedBy by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on ActivatedBy which contains "Ashraf" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy  | BusinessUnit      | State  | Company          | CreationDate         |
      | 2021000022 | SETTLEMENTS | Ashraf.Fathi | 0006 - Corrugated | Posted | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-1173
  Scenario: (07) View All Settlements filtered by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on BusinessUnit which equals "0002" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy     | BusinessUnit     | State  | Company          | CreationDate         |
      | 2021000025 | SETTLEMENTS | Shady.Abdelatif | 0002 - Signmedia | Posted | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000024 | SETTLEMENTS |                 | 0002 - Signmedia | Draft  | 0002 - DigiPro   | 05-Aug-2018 09:02 AM |
      | 2021000021 | SETTLEMENTS |                 | 0002 - Signmedia | Draft  | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 3

  #EBS-1173
  Scenario: (08) View All Settlements filtered by CreationDate by authorized user (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on CreationDate which equals "04-Jan-2021" with 10 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy | BusinessUnit | State | Company        | CreationDate         |
      | 2021000026 | SETTLEMENTS |             | 0001 - Flexo | Draft | 0002 - DigiPro | 04-Jan-2021 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 1

  #EBS-1173
  Scenario: (09) View All Settlements filtered by State by authorized user + Paging (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 2 of Settlements with filter applied on State which contains "Draft" with 2 records per page
    Then the following Settlements will be presented to "Ashraf.Fathi":
      | Code       | Type        | ActivatedBy | BusinessUnit     | State | Company          | CreationDate         |
      | 2021000023 | SETTLEMENTS |             | 0001 - Flexo     | Draft | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
      | 2021000021 | SETTLEMENTS |             | 0002 - Signmedia | Draft | 0001 - AL Madina | 05-Aug-2018 09:02 AM |
    And the total number of records in search results by "Ashraf.Fathi" are 5

  #EBS-1173
  Scenario: (10) View All Settlements filtered BusinessUnit by authorized user - No search results (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of Settlements with filter applied on BusinessUnit which equals "003" with 10 records per page
    Then no values are displayed to "Ashraf.Fathi" and empty grid state is viewed in Home Screen
    And the total number of records in search results by "Ashraf.Fathi" are 0

  #EBS-1173
  Scenario: (11) View All Settlements by unauthorized user due to role (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Settlements with no filter applied with 10 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
#

