# Author Dev: Evram Hany
# Author Quality: Khadra Ali
# Author PO : Somaya Abolwafaa

Feature: Allowed Action For Settlement (Object Screen)

  Background:
    Given the following users and roles exist:
      | Name               | Role                   |
      | hr1                | SuperUser              |
      | Afaf               | FrontDesk              |
      | CannotReadSections | CannotReadSectionsRole |
      | Shady.Abdelatif    | Accountant_Signmedia   |
      | hr1                | SuperUser              |

    And the following roles and sub-roles exist:
      | Role                   | Subrole                    |
      | SuperUser              | SuperUserSubRole           |
      | CannotReadSectionsRole | CannotReadSectionsSubRole  |
      | Accountant_Signmedia   | SettlementOwner_Signmedia  |
      | Accountant_Signmedia   | SettlementViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                         | Condition                      |
      | SettlementOwner_Signmedia  | Settlement:Delete                  | [purchaseUnitName='Signmedia'] |
      | SettlementOwner_Signmedia  | Settlement:UpdateSettlementDetails | [purchaseUnitName='Signmedia'] |
      | SettlementOwner_Signmedia  | Settlement:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | SettlementOwner_Signmedia  | Settlement:Post                    | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadAll                 | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadSummary             | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadGeneralData         | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadCompanyData         | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadAccountingDetails   | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadSettlementDetails   | [purchaseUnitName='Signmedia'] |
      | SettlementViewer_Signmedia | Settlement:ReadActivationDetails   | [purchaseUnitName='Signmedia'] |
      | CannotReadSectionsSubRole  | Settlement:Delete                  |                                |
      | CannotReadSectionsSubRole  | Settlement:Post                    |                                |
      | CannotReadSectionsSubRole  | Settlement:ReadAll                 |                                |
      | CannotReadSectionsSubRole  | Settlement:ReadGeneralData         |                                |
      | SuperUserSubRole           | *:*                                |                                |


    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                       |
      | Shady.Abdelatif | Settlement:Delete                  | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:UpdateSettlementDetails | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:UpdateAccountingDetails | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadAll                 | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadSummary             | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadGeneralData         | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadCompanyData         | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadAccountingDetails   | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadSettlementDetails   | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:ReadActivationDetails   | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:Post                    | [purchaseUnitName='Corrugated'] |
      | Shady.Abdelatif | Settlement:Create                  | [purchaseUnitName='Corrugated'] |

    And the following users doesn't have the following permissions:
      | User               | Permission                         |
      | Afaf               | Settlement:UpdateSettlementDetails |
      | Afaf               | Settlement:UpdateAccountingDetails |
      | Afaf               | Settlement:Post                    |
      | Afaf               | Settlement:ReadAll                 |
      | Afaf               | Settlement:ReadSummary             |
      | Afaf               | Settlement:ReadGeneralData         |
      | Afaf               | Settlement:ReadCompanyData         |
      | Afaf               | Settlement:ReadAccountingDetails   |
      | Afaf               | Settlement:ReadSettlementDetails   |
      | CannotReadSections | Settlement:ReadSummary             |
      | CannotReadSections | Settlement:ReadCompanyData         |
      | CannotReadSections | Settlement:ReadAccountingDetails   |
      | CannotReadSections | Settlement:ReadSettlementDetails   |

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2021000021     | SETTLEMENTS    | Draft  | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
      | 2021000022     | SETTLEMENTS    | Posted | Ashraf Salah    | 05-Aug-2018 09:02 AM | Ashraf Salah    |
      | 2021000024     | SETTLEMENTS    | Posted | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |

      #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit      | Company          |
      | 2021000021     | 0002 - Signmedia  | 0001 - AL Madina |
      | 2021000022     | 0006 - Corrugated | 0001 - AL Madina |
      | 2021000024     | 0002 - Signmedia  | 0002 - DigiPro   |

       #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000021     | EGP      |
      | 2021000022     | USD      |
      | 2021000024     | USD      |
#
#EBS-1175
  Scenario Outline: (01) Read allowed actions in Settlement Object Screen - All States - (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Settlement with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User               | Code       | AllowedActions                                                                                                                                                                    |
      | hr1                | 2021000021 | Delete,Post,ReadAll,ReadGeneralData,ReadSummary,UpdateSettlementDetails,UpdateAccountingDetails,ReadCompanyData,ReadAccountingDetails,ReadSettlementDetails,ReadActivationDetails |
      | Shady.Abdelatif    | 2021000021 | Delete,Post,ReadAll,ReadGeneralData,ReadSummary,UpdateSettlementDetails,UpdateAccountingDetails,ReadCompanyData,ReadAccountingDetails,ReadSettlementDetails,ReadActivationDetails |
      | Shady.Abdelatif    | 2021000024 | ReadAll,ReadSummary,ReadGeneralData,ReadCompanyData,ReadAccountingDetails,ReadSettlementDetails,ReadActivationDetails                                                             |
      | CannotReadSections | 2021000021 | ReadAll,ReadGeneralData,Delete,Post                                                                                                                                               |

#EBS-1175
  Scenario Outline: (02) Read allowed actions in Settlement Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Settlement with code "<SettlementCode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | SettlementCode |
      | Afaf            | 2021000021     |
      | Afaf            | 2021000022     |
      | Afaf            | 2021000024     |
      | Shady.Abdelatif | 2021000022     |

  @Future
  Scenario: (03) Read allowed actions in Settlement Object Screen - Object does not exist (Exception)
    Given user is logged in as "Amr.Khalil"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000021" successfully
    When "Amr.Khalil" requests to read actions of Settlement with code "2019000002"
    Then an error notification is sent to "Amr.Khalil" with the following message "Gen-msg-01"
