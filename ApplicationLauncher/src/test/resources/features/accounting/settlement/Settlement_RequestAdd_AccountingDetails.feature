# Author Dev: Ahmed Ali
# Author Quality: Khadrah Ali
Feature: Settlement Request Add Accounting Details

  Background:
    Given the following users and roles exist:
      | Name                       | Role                                          |
      | Shady.Abdelatif            | Accountant_Signmedia                          |
      | Shady.Abdelatif.NoAccounts | Accountant_Signmedia_CannotReadGLAccountsRole |
      | hr1                        | SuperUser                                     |
    And the following roles and sub-roles exist:
      | Role                                          | Sub-role                  |
      | Accountant_Signmedia                          | SettlementOwner_Signmedia |
      | Accountant_Signmedia_CannotReadGLAccountsRole | SettlementOwner_Signmedia |
      | Accountant_Signmedia                          | ChartOfAccountsViewer     |
      | SuperUser                                     | SuperUserSubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | SettlementOwner_Signmedia | Settlement:UpdateAccountingDetails | [purchaseUnitName='Signmedia'] |
      | ChartOfAccountsViewer     | GLAccount:ReadAll                  |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Settlement:UpdateAccountingDetails | [purchaseUnitName='Flexo'] |
    And the following users doesn't have the following permissions:
      | User                       | Permission        |
      | Shady.Abdelatif.NoAccounts | GLAccount:ReadAll |
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State  | CreatedBy       | CreationDate         | DocumentOwner |
      | 2021000001     | SETTLEMENTS    | Draft  | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000002     | SETTLEMENTS    | Posted | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Ashraf Salah  |
      | 2021000003     | SETTLEMENTS    | Draft  | hr1             | 05-Aug-2018 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000001     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002     | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003     | 0001 - Flexo     | 0001 - AL Madina |

  #EBS-1313
  Scenario: (01) Request to Add/Cancel AccountingDetails in Settlement in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add a record to AccountingDetails section of Settlement with code "2021000001"
    Then a new add AccountingDetails dialoge is opened and AccountingDetails section of Settlement with code "2021000001" becomes locked by "Shady.Abdelatif"
    And the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads |
      | ReadAccounts    |
    And the following mandatory fields are returned to "Shady.Abdelatif":
      | MandatoriesFields |
      | accountingEntry   |
      | glAccountCode     |
      | glSubAccountCode  |
      | amount            |
    And the following editable fields are returned to "Shady.Abdelatif":
      | EditableFields   |
      | accountingEntry  |
      | glAccountCode    |
      | glSubAccountCode |
      | amount           |

  #EBS-1313
  Scenario: (02) Request to Add/Cancel AccountingDetails in Settlement when AccountingDetails section is locked by another user (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    When "hr1" requests to add a record to AccountingDetails section of Settlement with code "2021000001"
    When "Shady.Abdelatif" requests to add a record to AccountingDetails section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

  #EBS-1313
  @Future
  Scenario: (03) Request to Add/Cancel AccountingDetails in Settlement that doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Settlement with code "2021000001" successfully
    When "Shady.Abdelatif" requests to add a record to AccountingDetails section of Settlement with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-1313
  Scenario: (04) Request to Add/Cancel AccountingDetails in Settlement in a state that does not allow this action - All states except Draft (Exception)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add a record to AccountingDetails section of Settlement with code "2021000002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-03"
    And AccountingDetails section of Settlement with code "2021000002" is not locked by "Shady.Abdelatif"

  #EBS-1313
  Scenario: (05) Request to Add/Cancel AccountingDetails in Settlement by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add a record to AccountingDetails section of Settlement with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-1313
  Scenario: (06) Request to Add/Cancel AccountingDetails in Settlement by a User with no authorized reads - All allowed states (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoAccounts"
    When "Shady.Abdelatif.NoAccounts" requests to add a record to AccountingDetails section of Settlement with code "2021000001"
    Then a new add AccountingDetails dialoge is opened and AccountingDetails section of Settlement with code "2021000001" becomes locked by "Shady.Abdelatif.NoAccounts"
    And there are no authorized reads returned to "Shady.Abdelatif.NoAccounts"
    And the following mandatory fields are returned to "Shady.Abdelatif.NoAccounts":
      | MandatoriesFields |
      | accountingEntry   |
      | glAccountCode     |
      | glSubAccountCode  |
      | amount            |
    And the following editable fields are returned to "Shady.Abdelatif.NoAccounts":
      | EditableFields   |
      | accountingEntry  |
      | glAccountCode    |
      | glSubAccountCode |
      | amount           |

  #EBS-1313
  Scenario: (07) Request to Add/Cancel AccountingDetails in Settlement While the edit session of Settlement in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And AccountingDetails section of Settlement with code "2021000001" is locked by "Shady.Abdelatif" at "17-Feb-2021 09:10 AM"
    When "Shady.Abdelatif" cancels saving AccountingDetails section of Settlement with code "2021000001" at "17-Feb-2021 09:30 AM"
    Then the lock by "Shady.Abdelatif" on AccountingDetails section of Settlement with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"

  #EBS-1313
  Scenario: (08) Request to Add/Cancel AccountingDetails in Settlement after lock session is expired
    Given user is logged in as "Shady.Abdelatif"
    And AccountingDetails section of Settlement with code "2021000001" is locked by "Shady.Abdelatif" at "17-Feb-2021 09:00 AM"
    When "Shady.Abdelatif" cancels saving AccountingDetails section of Settlement with code "2021000001" at "17-Feb-2021 09:31 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-07"

  #EBS-1313
  @Future
  Scenario: (09) Request to Add/Cancel AccountingDetails in Settlement after lock session is expire and Settlement doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And AccountingDetails section of Settlement with code "2021000001" is locked by "Shady.Abdelatif" at "17-Feb-2021 09:00 AM"
    And "hr1" first deleted the Settlement with code "2021000001" successfully at "17-Feb-2021 09:31 AM"
    When "Shady.Abdelatif" cancels saving AccountingDetails section of Settlement with code "2021000001" at "17-Feb-2021 09:35 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
