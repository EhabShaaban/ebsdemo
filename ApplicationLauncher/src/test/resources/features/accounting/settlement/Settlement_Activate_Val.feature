# Author: Eman Mansour (EBS-8570)
# updated by: Fatma Al Zahraa (EBS - 8967)
Feature: Activate Settlement Validation

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission      | Condition                      |
      | SettlementOwner_Signmedia | Settlement:Post | [purchaseUnitName='Signmedia'] |
    And the following GLAccounts has the following Subledgers:
      | GLAccount                 | Subledger     |
      | 10221 - service vendors 2 | Local_Vendors |
      | 10431 - Bank              | Banks         |
      | 10430 - Cash              | Treasuries    |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44332    | 19-Feb-2021 10:00 PM | User Daily Rate       |
      | 2020000036 | USD              | EGP               | 17.447259   | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 19-Mar-2021 10:00 PM | Central Bank Of Egypt |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |

    And insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 1000    |

    And insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 200     |


  #EBS-8570
  Scenario: (01) Activate Settlement with amount greater than bank balance

    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy       | CreationDate         | DocumentOwner   |
      | 2021000021     | SETTLEMENTS    | Draft | Shady.Abdelatif | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000021     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000021     | EGP      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value       |
      | 2021000021     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1000.478921 |
      | 2021000021     | CREDIT       | 10431 - Bank              | 1234567893458 - EGP - Bank Misr | 1000.478921 |

    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" activates the Settlement with Code "2021000021" and JournalEntryDate "17-Jun-2022 11:00 AM" at "07-Jan-2021 09:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "amount" field "S-msg-11" and sent to "Shady.Abdelatif"

  #EBS-8570
  Scenario: (02) Activate Settlement with amount greater than treasury balance

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy   | CreationDate         | DocumentOwner   |
      | 2021000024     | SETTLEMENTS    | Draft | Gehan.Ahmed | 05-Aug-2018 09:02 AM | Shady.Abdelatif |
    #@INSERT
    And the following Settlements CompanyData exist:
      | SettlementCode | BusinessUnit     | Company          |
      | 2021000024     | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following Settlements Details exist:
      | SettlementCode | Currency |
      | 2021000024     | EGP      |
    #@INSERT
    And the following Settlements AccountingDetails exist:
      | SettlementCode | Credit/Debit | Account                   | SubAccount                      | Value |
      | 2021000024     | DEBIT        | 10221 - service vendors 2 | 000002 - Zhejiang               | 1500  |
      | 2021000024     | CREDIT       | 10430 - Cash              | 0002 - Signmedia Treasury       | 300   |
      | 2021000024     | CREDIT       | 10431 - Bank              | 1234567893458 - EGP - Bank Misr | 1200  |
    When "Shady.Abdelatif" activates the Settlement with Code "2021000024" and JournalEntryDate "17-Jun-2022 11:00 AM" at "07-Jan-2021 09:30 AM"
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "amount" field "S-msg-11" and sent to "Shady.Abdelatif"
