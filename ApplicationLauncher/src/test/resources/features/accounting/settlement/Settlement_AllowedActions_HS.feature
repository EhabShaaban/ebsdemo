# Author Dev: Ahmed Ali
# Author Quality: Khadrah Ali
Feature: Allowed Actions Settlement - Home Screen

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
      | CreateOnlyUser  | CreateOnlyRole       |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | Accountant_Signmedia | SettlementViewer_Signmedia |
      | Accountant_Signmedia | SettlementOwner_Signmedia  |
      | M.D                  | SettlementViewer           |
      | CreateOnlyRole       | CreateOnlySubRole          |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | SettlementOwner_Signmedia  | Settlement:Create  |                                |
      | SettlementViewer_Signmedia | Settlement:ReadAll | [purchaseUnitName='Signmedia'] |
      | CreateOnlySubRole          | Settlement:Create  |                                |
      | SettlementViewer           | Settlement:ReadAll |                                |
    And the following users doesn't have the following permissions:
      | User           | Permission         |
      | Afaf           | Settlement:Create  |
      | Afaf           | Settlement:ReadAll |
      | CreateOnlyUser | Settlement:ReadAll |
      | Ashraf.Fathi   | Settlement:Create  |

  #EBS-1174
  Scenario Outline: (01) Read allowed actions in Settlement Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Settlement home screen
    Then the "<AllowedActions>" are displayed to "<User>":

    Examples:
      | User            | AllowedActions |
      | Shady.Abdelatif | ReadAll,Create |
      | Ashraf.Fathi    | ReadAll        |
      | CreateOnlyUser  | Create         |

  #EBS-1174
  Scenario: (02) Read allowed actions in Settlement Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Settlement home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"
