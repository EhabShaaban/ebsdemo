# Author Dev: Mohamed Aboelnour
# Author Quality: Khadrah Ali
Feature: Create Settlement (Auth)

  Background:
    Given the following users and roles exist:
      | Name                                | Role                                                    |
      | Shady.Abdelatif                     | Accountant_Signmedia                                    |
      | Shady.Abdelatif.NoSettlementType    | Accountant_Signmedia_CannotReadSettlementType           |
      | Shady.Abdelatif.NoCreateWithAllRead | Accountant_Signmedia_CannotCreateSettlementWithAllReads |
      | Shady.Abdelatif.CreateWithNoRead    | Accountant_Signmedia_CreateSettlementWithNoReads        |
      | Afaf                                | FrontDesk                                               |

    And the following roles and sub-roles exist:
      | Role                                                    | Sub-role                  |
      | Accountant_Signmedia                                    | SettlementOwner_Signmedia |
      | Accountant_Signmedia                                    | SettlementTypeViewer      |
      | Accountant_Signmedia_CannotReadSettlementType           | SettlementOwner_Signmedia |
      | Accountant_Signmedia_CannotCreateSettlementWithAllReads | SettlementTypeViewer      |
      | Accountant_Signmedia_CreateSettlementWithNoReads        | SettlementOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission             | Condition |
      | SettlementOwner_Signmedia | Settlement:Create      |           |
      | SettlementTypeViewer      | SettlementType:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User                                | Permission             |
      | Shady.Abdelatif.NoSettlementType    | SettlementType:ReadAll |
      | Shady.Abdelatif.NoCreateWithAllRead | Settlement:Create      |
      | Shady.Abdelatif.CreateWithNoRead    | SettlementType:ReadAll |
      | Afaf                                | Settlement:Create      |

    And the following BusinessUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |

    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |

    And the following Settlement Types exist:
      | Code        |
      | SETTLEMENTS |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition                      |
      | 34 | Ashraf Salah | Settlement     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |

  #EBS-1059
  Scenario: (01) Request Create Settlement by an unauthorized to create user but with all authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif.NoCreateWithAllRead"
    When "Shady.Abdelatif.NoCreateWithAllRead" creates Settlement with the following values:
      | Type        | BusinessUnit | Company | Currency | DocumentOwner |
      | SETTLEMENTS | 0002         | 0002    | 0001     | 34            |
    Then "Shady.Abdelatif.NoCreateWithAllRead" is logged out
    And "Shady.Abdelatif.NoCreateWithAllRead" is forwarded to the error page

  #EBS-1059
  Scenario Outline: (02) Request Create Settlement by an authorized user to create, but without unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates Settlement with the following values:
      | Type        | BusinessUnit | Company | Currency | DocumentOwner |
      | SETTLEMENTS | 0002         | 0002    | 0001     | 34            |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                             |
      | Shady.Abdelatif.NoSettlementType |
      | Shady.Abdelatif.CreateWithNoRead |

  #EBS-1059
  Scenario: (03) Request Create Settlement by an unauthorized user to create due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Settlement with the following values:
      | Type        | BusinessUnit | Company | Currency | DocumentOwner |
      | SETTLEMENTS | 0001         | 0002    | 0001     | 34            |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-1059
  Scenario: (04) Request Create Settlement by an unauthorized user to create with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates Settlement with the following values:
      | Type        | BusinessUnit | Company | Currency | DocumentOwner |
      | SETTLEMENTS | 0002         | 0002    | 0001     | 34            |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page

  #EBS-1059
  Scenario: (05) Request Create Settlement by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to Create Settlement
    Then the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads     |
      | ReadSettlementTypes |

  #EBS-1059
  Scenario: (06) Request Create Settlement by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoSettlementType"
    When "Shady.Abdelatif.NoSettlementType" requests to Create Settlement
    Then there are no authorized reads returned to "Shady.Abdelatif.NoSettlementType"