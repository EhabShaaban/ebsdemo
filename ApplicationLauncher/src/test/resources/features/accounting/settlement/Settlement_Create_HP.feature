# Author Dev: Yara Ameen
# Author Quality: Khadrah Ali
Feature: Create Settlement (HP)

  Background:

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                   |
      | Accountant_Signmedia | SettlementOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                   | Permission        | Condition |
      | SettlementOwner_Signmedia | Settlement:Create |           |

   #@INSERT
    And the following Settlements GeneralData exist:
      | SettlementCode | SettlementType | State | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000026     | SETTLEMENTS    | Draft | Amr.Khalil | 04-Jan-2021 09:02 AM | Ashraf Salah  |
    And the following BusinessUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Settlement Types exist:
      | Code        |
      | SETTLEMENTS |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject | Permission         | Condition                      |
      | 34 | Ashraf Salah | Settlement     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
  #EBS-1063
  Scenario Outline: (01) Create Settlement, with all mandatory fields, by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "02-Feb-2021 02:00 PM"
    And Last created Settlement was with code "2021000026"
    When "Shady.Abdelatif" creates Settlement with the following values:
      | Type   | BusinessUnit | Company | Currency | DocumentOwner |
      | <Type> | 0002         | 0002    | 0001     | 34            |
    Then a new Settlement is created with the following values:
      | Code       | State | Type   | CreatedBy       | LastUpdatedBy   | CreationDate         | LastUpdateDate       | DocumentOwner     |
      | 2021000027 | Draft | <Type> | Shady.Abdelatif | Shady.Abdelatif | 02-Feb-2021 02:00 PM | 02-Feb-2021 02:00 PM | 34 - Ashraf Salah |
    And the Company Section of Settlement with code "2021000027" is created as follows:
      | Company        | BusinessUnit     |
      | 0002 - DigiPro | 0002 - Signmedia |
    And the Details Section of Settlement with code "2021000027" is created as follows:
      | Currency |
      | EGP      |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | Type        |
      | SETTLEMENTS |

