Feature: View Items in SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                                   |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |
      | SuperUser                 | SuperUserSubRole                          |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission             | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadItems | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadItems | [purchaseUnitName='Flexo']                                                 |
      | SuperUserSubRole                          | *:*                    |                                                                            |
    And the following users have the following permissions without the following conditions:
      | User              | Permission             | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadItems | [purchaseUnitName='Signmedia'] |

     #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy         | State         | CreationDate         |
      | 2019000005 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Rabie.abdelghafar | Draft         | 01-Dec-2019 02:20 PM |
      | 2019100001 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Rabie.abdelghafar | Draft         | 01-Dec-2019 02:20 PM |
      | 2019000001 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Rabie.abdelghafar | Opened,Active | 01-Dec-2019 02:20 PM |

        #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000005 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100001 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000005 | 000002 - Hot Laminated Frontlit Backlit roll                     | 0029 - Roll 2.20x50 | 100            | 330             | 0003 - Kilogram |
      | 2019000005 | 000002 - Hot Laminated Frontlit Backlit roll                     | 0029 - Roll 2.20x50 | 200            | 440             | 0003 - Kilogram |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.752432     | 2200.543732     | 0003 - Kilogram |

    And the following SalesInvoices have empty Items secion:
      | SICode     |
      | 2019100001 |

     #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019000005 | 0001 - Vat                                   | 1             | 1210      |
      | 2019000005 | 0002 - Commercial and industrial profits tax | 0.5           | 605       |
      | 2019000005 | 0003 - Real estate taxes                     | 10            | 12100     |

# EBS-5428
  Scenario: (01) View Items in SalesInvoice by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Items section of SalesInvoice with code "2019000005"
    Then the following values of Items section for SalesInvoice with code "2019000005" are displayed to "Ahmed.Al-Ashry":
      | Item                                         | OrderUnit           | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 200 | 440                  | 4               | 88000       |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 100 | 330                  | 3               | 33000       |

  Scenario: (02) View Items in SalesInvoice by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Items section of SalesInvoice with code "2019000001"
    Then the following values of Items section for SalesInvoice with code "2019000001" are displayed to "Ahmed.Al-Ashry":
      | Item                                                             | OrderUnit       | Qty        | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount         |
      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kilogram | 100.752432 | 2200.543732          | 34.654232       | 221710.132721356224 |

# EBS-5428
  Scenario: (03) View Items in SalesInvoice by an authorized user, where Items section is empty (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Items section of SalesInvoice with code "2019100001"
    Then an empty Items section in SalesInvoice is displayed to "Ahmed.Al-Ashry"

# EBS-5428
  Scenario: (04) View Items in SalesInvoice, where SalesInvoice dosn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019000005" successfully
    When "Ahmed.Al-Ashry" requests to view Items section of SalesInvoice with code "2019000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

# EBS-5428
  Scenario: (05) View Items in SalesInvoice by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view Items section of SalesInvoice with code "2019000005"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
