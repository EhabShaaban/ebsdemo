# Author Dev: Yara, Ahmed Ali
# Author Quality: Engy Abdel-Aziz (EBS-5608, EBS-5985)
# Author Quality: Marina, Eslam (EBS-6817)
# Reviewer: Somaya Ahmed and Hosam Bayomy (6-Aug-2020)
Feature: Create SalesInvoice - Happy Path

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                       |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia | SOViewer_Signmedia            |
      | Storekeeper_Signmedia | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia | CompanyViewer                 |
      | Storekeeper_Signmedia | DocumentOwnerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission               | Condition                      |
      | SalesInvoiceOwner_Signmedia   | SalesInvoice:Create      |                                |
      | SalesInvoiceTypeViewer        | SalesInvoiceType:ReadAll |                                |
      | PurUnitReader_Signmedia       | PurchasingUnit:ReadAll   | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia            | SalesOrder:ReadAll       | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia      | Customer:ReadAll         | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                 | Company:ReadAll          |                                |
      | DocumentOwnerViewer_Signmedia | DocumentOwner:ReadAll    | [purchaseUnitName='Signmedia'] |
    And the following BusinessUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
    And the following SalesInvoiceTypes exist:
      | Code                            | Name                            |
      | SALES_INVOICE_WITHOUT_REFERENCE | Sales invoice without reference |
      | SALES_INVOICE_FOR_SALES_ORDER   | Sales invoice for sales order   |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the Company "0001 - AL Madina" has the following Taxes:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And the following SalesOrders have the following TaxDetails:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000053 | 0007 - Vat                                   | 1             |
      | 2020000053 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000053 | 0009 - Real estate taxes                     | 10            |
      | 2020000053 | 0010 - Service tax                           | 12            |
      | 2020000053 | 0011 - Adding tax                            | 0.5           |
      | 2020000053 | 0012 - Sales tax                             | 12            |
      | 2020000011 | 0007 - Vat                                   | 1             |
      | 2020000011 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000011 | 0009 - Real estate taxes                     | 10            |
      | 2020000011 | 0010 - Service tax                           | 12            |
      | 2020000011 | 0011 - Adding tax                            | 0.5           |
      | 2020000011 | 0012 - Sales tax                             | 12            |

    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition                      |
      | 1000072 | Ahmed Al-Ashry | SalesInvoice   | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
    And the following SalesOrders with the following General, Company, and Details data exist:
      | Code       | Type                      | BusinessUnit     | State               | CurrencyISO | Company          | Customer                       | PaymentTerm                                |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved            | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | GoodsIssueActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
    And the following SalesOrders have the following Items with the following Qty, OrderUnit, SalesPrice and TotalAmount exist:
      | SOCode     | ItemId | ItemType                | Item                                          | OrderUnit           | Qty    | SalesPrice | TotalAmount |
      | 2020000053 | 67     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0035 - Roll 1.27x50 | 2.000  | 10.000     | 20.000      |
      | 2020000011 | 64     | COMMERCIAL - Commercial | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - Square Meter | 10.000 | 33.000     | 330.000     |

  ################ Create Happy Paths
  #EBS-5608, #EBS-5985
  Scenario: (01) Create SalesInvoice without reference, with all mandatory fields, by an authorized user(Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CurrentDateTime = "24-Jun-2020 10:00 AM"
    And Last created SalesInvoice was with code "2020000004"
    When "Mahmoud.Abdelaziz" creates SalesInvoice without Reference with the following values:
      | Type                            | BusinessUnit | Company | Customer | DocumentOwnerId |
      | SALES_INVOICE_WITHOUT_REFERENCE | 0002         | 0001    | 000007   | 1000072         |
    Then a new SalesInvoice of type Without_Reference is created with the following values:
      | Code       | State | CreatedBy         | LastUpdatedBy     | CreationDate         | LastUpdateDate       | Type                            | DocumentOwner            |
      | 2020000005 | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | 24-Jun-2020 10:00 AM | 24-Jun-2020 10:00 AM | SALES_INVOICE_WITHOUT_REFERENCE | 1000072 - Ahmed Al-Ashry |
    And the BusinessPartner Section of SalesInvoice without Reference with code "2020000005" is created as follows:
      | SOCode | CurrencyISO | PaymentTerm | Customer                       |
      |        |             |             | 000007 - مطبعة أكتوبر الهندسية |
    And the Company Section of SalesInvoice with code "2020000005" is created as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the Taxes Section of SalesInvoice with code "2020000005" is created as follows:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"

  #EBS-6817
  Scenario: (02) Create SalesInvoice for Approved SalesOrder, with all mandatory fields, by an authorized user(Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CurrentDateTime = "24-Jun-2020 10:00 AM"
    And Last created SalesInvoice was with code "2020000004"
    When "Mahmoud.Abdelaziz" creates SalesInvoice for SalesOrder with the following values:
      | Type                          | SOCode     | BusinessUnit | DocumentOwnerId |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | 0002         | 1000072         |
    Then a new SalesInvoice of type SalesOrder_Based is created with the following values:
      | Code       | State | CreatedBy         | LastUpdatedBy     | CreationDate         | LastUpdateDate       | Type                          | DocumentOwner            |
      | 2020000005 | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | 24-Jun-2020 10:00 AM | 24-Jun-2020 10:00 AM | SALES_INVOICE_FOR_SALES_ORDER | 1000072 - Ahmed Al-Ashry |
    And the BusinessPartner Section of SalesInvoice for SalesOrder with code "2020000005" is created as follows:
      | SOCode     | CurrencyISO | PaymentTerm                  | Customer                       |
      | 2020000053 | EGP         | 0003 - 100% Advanced Payment | 000007 - مطبعة أكتوبر الهندسية |
    And the Company Section of SalesInvoice with code "2020000005" is created as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And InvoiceItems section of SalesInvoice with code "2020000005" is created as follows:
      | Item                                          | Qty   | OrderUnit           | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 2.000 | 0035 - Roll 1.27x50 | 10.000               | 0.1574803149    | 20.000      |
    And the Taxes Section of SalesInvoice with code "2020000005" is created as follows:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"

  #EBS-6817
  Scenario: (03) Create SalesInvoice for GoodsIssueActivated SalesOrder, with all mandatory fields, by an authorized user(Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CurrentDateTime = "24-Jun-2020 10:00 AM"
    And Last created SalesInvoice was with code "2020000004"
    When "Mahmoud.Abdelaziz" creates SalesInvoice for SalesOrder with the following values:
      | Type                          | SOCode     | BusinessUnit | DocumentOwnerId |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000011 | 0002         | 1000072         |
    Then a new SalesInvoice of type SalesOrder_Based is created with the following values:
      | Code       | State | CreatedBy         | LastUpdatedBy     | CreationDate         | LastUpdateDate       | Type                          | DocumentOwner            |
      | 2020000005 | Draft | Mahmoud.Abdelaziz | Mahmoud.Abdelaziz | 24-Jun-2020 10:00 AM | 24-Jun-2020 10:00 AM | SALES_INVOICE_FOR_SALES_ORDER | 1000072 - Ahmed Al-Ashry |
    And the BusinessPartner Section of SalesInvoice for SalesOrder with code "2020000005" is created as follows:
      | SOCode     | CurrencyISO | PaymentTerm                                | Customer                       |
      | 2020000011 | EGP         | 0001 - 20% Deposit, 80% T/T Before Loading | 000007 - مطبعة أكتوبر الهندسية |
    And the Company Section of SalesInvoice with code "2020000005" is created as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And InvoiceItems section of SalesInvoice with code "2020000005" is created as follows:
      | Item                                          | Qty    | OrderUnit           | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | 10.000 | 0019 - Square Meter | 33.00                | 33.00           | 330.000     |
    And the Taxes Section of SalesInvoice with code "2020000005" is created as follows:
      | Tax                                          | TaxPercentage |
      | 0007 - Vat                                   | 1.0           |
      | 0008 - Commercial and industrial profits tax | 0.5           |
      | 0009 - Real estate taxes                     | 10.0          |
      | 0010 - Service tax                           | 12.0          |
      | 0011 - Adding tax                            | 0.5           |
      | 0012 - Sales tax                             | 12.0          |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-11"
