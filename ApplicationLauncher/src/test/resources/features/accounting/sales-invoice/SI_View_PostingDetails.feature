# Author Dev: Evram Hany
# Author Quality: Eslam Ayman & Khadra Ali
# Reviewer: Hosam Bayomy & Somaya Ahmed (10-Aug-2020)
Feature: View PostingDetails section in SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |

    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission                      | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadPostingDetails | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |

    And the following users have the following permissions without the following conditions:
      | User           | Permission                      | Condition                  |
      | Ahmed.Al-Ashry | SalesInvoice:ReadPostingDetails | [purchaseUnitName='Flexo'] |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2020000003 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019100008 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |
      | 2019100007 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000005 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |

    And the following PostingDetails for AccountingDocument with DocumentType "SalesInvoice" exist:
      | Code       | CreationDate         | CreatedBy                 | Accountant                | ExchangeRateCode | JournalEntryCode | CurrencyPrice |
      | 2019100008 | 28-Oct-2019 09:30 AM | Admin from BDKCompanyCode | Admin from BDKCompanyCode | 2018000013       | 2020000018       | 1 EGP         |
      | 2019100007 | 28-Oct-2019 09:30 AM | Admin from BDKCompanyCode | Admin from BDKCompanyCode | 2018000013       | 2020000019       | 1 EGP         |
      | 2020000003 | 27-Jul-2019 09:30 AM | hr1                       | hr1                       | 2018000013       | 2020000001       | 1 EGP         |

  #EBS-7027
  Scenario Outline: (01) View PostingDetails section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view PostingDetails section of SalesInvoice with code "<Code>"
    Then the following values of PostingDetails section for SalesInvoice with code "<Code>" are displayed to "Ahmed.Al-Ashry":
      | Code   | PostingDate   | PostedBy   | JournalEntryCode   | CurrencyPrice   | ExchangeRateCode   |
      | <Code> | <PostingDate> | <PostedBy> | <JournalEntryCode> | <CurrencyPrice> | <ExchangeRateCode> |

    Examples:
      | Code       | PostingDate          | PostedBy                  | JournalEntryCode | CurrencyPrice     | ExchangeRateCode |
      | 2020000003 | 27-Jul-2020 08:53 AM | hr1                       | 2020000001       | 1.0 EGP = 1.0 EGP | 2018000013       |
      | 2019100007 | 10-Sep-2019 10:30 AM | Admin from BDKCompanyCode | 2020000019       | 1.0 EGP = 1.0 EGP | 2018000013       |

  #EBS-7027
  Scenario: (02) View PostingDetails section where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019000005" successfully
    When "Ahmed.Al-Ashry" requests to view PostingDetails section of SalesInvoice with code "2019000005"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  ##EBS-7027
  Scenario: (03) View PostingDetails section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view PostingDetails section of SalesInvoice with code "2019100008"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
