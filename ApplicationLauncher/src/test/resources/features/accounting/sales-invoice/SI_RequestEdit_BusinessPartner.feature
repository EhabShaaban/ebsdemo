# Author: Mohamed Aboelnour, Ahmed Ali, Khadrah Ali (EBS-7464)
# Reviewer: Hosam Bayomy & Somaya Ahmed (16-Mar-2021) (EBS-7464)

Feature: Edit BusinessPartner section in SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name                                      | Role                                                 |
      | Mahmoud.Abdelaziz                         | Storekeeper_Signmedia                                |
      | Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency | SalesSpecialist_Signmedia_NoPaymentTermAndNoCurrency |
      | Rabie.abdelghafar                         | SalesSpecialist_Flexo                                |
      | hr1                                       | SuperUser                                            |
    And the following roles and sub-roles exist:
      | Role                                                 | Subrole                     |
      | Storekeeper_Signmedia                                | SalesInvoiceOwner_Signmedia |
      | SalesSpecialist_Flexo                                | SalesInvoiceOwner_Flexo     |
      | Storekeeper_Signmedia                                | PaymentTermViewer           |
      | Storekeeper_Signmedia                                | CurrencyViewer              |
      | SalesSpecialist_Signmedia_NoPaymentTermAndNoCurrency | SalesInvoiceOwner_Signmedia |
      | SuperUser                                            | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                         | Condition                      |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateBusinessPartner | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceOwner_Flexo     | SalesInvoice:UpdateBusinessPartner | [purchaseUnitName='Flexo']     |
      | CurrencyViewer              | Currency:ReadAll                   |                                |
      | PaymentTermViewer           | PaymentTerms:ReadAll               |                                |
      | SuperUserSubRole            | *:*                                |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                         | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:UpdateBusinessPartner | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                                      | Permission           |
      | Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency | Currency:ReadAll     |
      | Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency | PaymentTerms:ReadAll |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100001 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2020000002 | Draft  | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |

  #### Happy Paths ####
 # EBS-5619
  Scenario: (01) Request to edit BusinessPartner section - User with one role - In draft state only (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to edit BusinessPartner section of SalesInvoice with code "2019000002"
    Then BusinessPartner section of SalesInvoice with code "2019000002" becomes locked by "Mahmoud.Abdelaziz"
    And the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads |
      | ReadPaymentTerm |
      | ReadCurrency    |
    And there are no mandatory fields returned to "Mahmoud.Abdelaziz"
    And the following editable fields are returned to "Mahmoud.Abdelaziz":
      | EditableFields  |
      | paymentTermCode |
      | currencyIso     |

  #EBS-7464
  Scenario: (02) Request to edit SalesInvoiceDetails section for Sales Order type- User with one role - In draft state only (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to edit BusinessPartner section of SalesInvoice with code "2020000002"
    Then BusinessPartner section of SalesInvoice with code "2020000002" becomes locked by "Mahmoud.Abdelaziz"
    And the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads |
      | ReadPaymentTerm |
      | ReadCurrency    |
    And there are no mandatory fields returned to "Mahmoud.Abdelaziz"
    And the following editable fields are returned to "Mahmoud.Abdelaziz":
      | EditableFields  |
      | paymentTermCode |
      | currencyIso     |

  # EBS-5619
  Scenario: (03) Request to edit BusinessPartner section - User with one role - In draft state only (Happy Path - NoAuthorizedReads)
    Given user is logged in as "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency"
    When "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency" requests to edit BusinessPartner section of SalesInvoice with code "2019000002"
    Then BusinessPartner section of SalesInvoice with code "2019000002" becomes locked by "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency"
    And there are no authorized reads returned to "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency"
    And there are no mandatory fields returned to "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency"
    And the following editable fields are returned to "Ahmed.Al-Ashry.NoPaymentTermAndNoCurrency":
      | EditableFields  |
      | paymentTermCode |
      | currencyIso     |

  # EBS-5619
  Scenario: (04) Request to edit BusinessPartner section when section is locked by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first opened BusinessPartner section of SalesInvoice with code "2019000002" in the edit mode successfully
    When "Mahmoud.Abdelaziz" requests to edit BusinessPartner section of SalesInvoice with code "2019000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  # EBS-5619
  Scenario: (05) Request to edit BusinessPartner section of a SalesInvoice that doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully
    When "Mahmoud.Abdelaziz" requests to edit BusinessPartner section of SalesInvoice with code "2019100001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  # EBS-5619
  Scenario: (06) Request to edit BusinessPartner section when SalesInvoice is Posted (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to edit BusinessPartner section of SalesInvoice with code "2019000001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-03"
    And SalesInvoice section of SalesInvoice with code "2019000001" is not locked by "Mahmoud.Abdelaziz"

  # EBS-5619
  Scenario: (07) Request to edit BusinessPartner section by an unauthorized user in SalesInvoice - due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to edit BusinessPartner section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page


##  ######### Cancel Saving Business Partner   ########################################################################

  # EBS-5619
  Scenario: (08) Request to edit BusinessPartner section Cancel save within the edit session- User with one role - In draft state (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And BusinessPartner section of SalesInvoice with Code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving BusinessPartner section of SalesInvoice with Code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Mahmoud.Abdelaziz" on BusinessPartner section of SalesInvoice with Code "2019000002" is released

 # EBS-5619
  Scenario: (09) Request to edit BusinessPartner section Cancel save after edit session expire (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And BusinessPartner section of SalesInvoice with Code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 9:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving BusinessPartner section of SalesInvoice with Code "2019000002" at "07-Jan-2019 09:41 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"

  # EBS-5619
  Scenario: (10) Request to edit BusinessPartner section Cancel save after lock session is expire and SalesInvoice doesn't exsit (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And BusinessPartner section of SalesInvoice with Code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully at "07-Jan-2019 09:31 AM"
    When "Mahmoud.Abdelaziz" cancels saving BusinessPartner section of SalesInvoice with Code "2019100001" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  # EBS-5619
  Scenario: (11) Request to edit BusinessPartner section Cancel save by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" cancels saving BusinessPartner section of SalesInvoice with Code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
