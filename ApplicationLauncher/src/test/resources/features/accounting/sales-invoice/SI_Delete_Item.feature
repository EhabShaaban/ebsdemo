Feature: Delete Item in SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo |
      | hr1               | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Sub-role                    |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia |
      | Storekeeper_Signmedia | ItemViewer_Signmedia        |
      | SalesSpecialist_Flexo | SalesInvoiceOwner_Flexo     |
      | SalesSpecialist_Flexo | ItemViewer_Flexo            |
      | SuperUser             | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission               | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateItems | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceOwner_Flexo     | SalesInvoice:UpdateItems | [purchaseUnitName='Flexo']                                                              |
      | ItemViewer_Signmedia        | Item:ReadAll             | [purchaseUnitName='Signmedia']                                                          |
      | ItemViewer_Flexo            | Item:ReadAll             | [purchaseUnitName='Flexo']                                                              |
      | SuperUserSubRole            | *:*                      |                                                                                         |

    And the following users have the following permissions without the following conditions:
      | User              | Permission               | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000005 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |

    And SalesInvoice with code "2019000005" has the following Items:
      | ItemId | Item                                         | OrderUnit           | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 3      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 100 | 330.00               | 3.00            | 33000       |
      | 4      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 200 | 440.00               | 4.00            | 88000       |

    And the following TaxDetails exist in taxes section for SalesInvoice "2019000005"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 1210      |
      | 0002 - Commercial and industrial profits tax | 0.5           | 605       |
      | 0003 - Real estate taxes                     | 10            | 12100     |

    And SalesInvoice with code "2019000001" has the following Items:
      | ItemId | Item                                                             | OrderUnit       | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 1      | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kilogram | 100 | 999                  | 15.7322834645   | 99900       |

  Scenario: (01) Delete Item from SalesInvoice, where SalesInvoice is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Item with id "3" in SalesInvoice with code "2019000005"
    Then Item with id "1" from SalesInvoice with code "2019000005" is deleted
    And InvoiceTaxes section of SalesInvoice with Code "2019000005" is updated as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 880       |
      | 0002 - Commercial and industrial profits tax | 0.5           | 440       |
      | 0003 - Real estate taxes                     | 10            | 8800      |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-12"

  Scenario: (02) Delete Item from SalesInvoice, where Item doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted Item with id "1" of SalesInvoice with code "2019000005" successfully
    When "Mahmoud.Abdelaziz" requests to delete Item with id "1" in SalesInvoice with code "2019000005"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"

  Scenario: (03) Delete Item from SalesInvoice, where action not allowed per current state (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    When "Mahmoud.Abdelaziz" requests to delete Item with id "6" in SalesInvoice with code "2019000001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-13"

  Scenario: (04) Delete Item from SalesInvoice, where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019000005" successfully
    When "Mahmoud.Abdelaziz" requests to delete Item with id "1" in SalesInvoice with code "2019000005"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (05) Delete Item from SalesInvoice when SalesInvoiceItems section are locked by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And first "hr1" opens SalesInvoiceItems section of SalesInvoice with code "2019000005" in edit mode
    When "Mahmoud.Abdelaziz" requests to delete Item with id "1" in SalesInvoice with code "2019000005"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  Scenario: (06) Delete Item from SalesInvoice by Unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to delete Item with id "1" in SalesInvoice with code "2019000005"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
