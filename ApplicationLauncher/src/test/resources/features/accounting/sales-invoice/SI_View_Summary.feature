#@Author: Mohamed Aboelnour
Feature: View Summary section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission               | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadSummary | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadSummary | [purchaseUnitName='Flexo']                                                 |
    And the following users have the following permissions without the following conditions:
      | User              | Permission               | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadSummary | [purchaseUnitName='Signmedia'] |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty       | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.453721 | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining   |
      | 2020000005 | 20907.442              | 28434.12112           | 28434.12112 |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 02-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 02-Jun-2021 02:20 PM |
      | 2021000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft         | 03-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
      | 2021000003 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2021000003 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.453721      | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount  |
      | 2021000002 | 0007 - Vat                                   | 1             | 209.07442  |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 104.53721  |
      | 2021000002 | 0009 - Real estate taxes                     | 10            | 2090.7442  |
      | 2021000002 | 0010 - Service tax                           | 12            | 2508.89304 |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 104.53721  |
      | 2021000002 | 0012 - Sales tax                             | 12            | 2508.89304 |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining   |
      | 2021000002 | 28434.12112 |

  Scenario: (01) View SalesInvoice Summary section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Summary section of SalesInvoice with code "2021000002"
    Then the following values for InvoiceSummary for SalesInvoice with code "2021000002" are displayed to "Ahmed.Al-Ashry":
      | TotalAmountBeforeTaxes | TaxAmount  | NetAmount   | DownPayment | TotalRemaining |
      | 20907.442              | 7526.67912 | 28434.12112 | 0           | 28434.12112    |

  Scenario: (02) View SalesInvoice Summary section where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2021000003" successfully
    When "Ahmed.Al-Ashry" requests to view Summary section of SalesInvoice with code "2021000003"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario: (03) View SalesInvoice Summary section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view Summary section of SalesInvoice with code "2021000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
