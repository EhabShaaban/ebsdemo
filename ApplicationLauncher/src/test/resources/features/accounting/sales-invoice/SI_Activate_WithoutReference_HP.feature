# Author: Mohamed Aboelnour, Eslam Salam
# Reviewer: Somaya Ahmed (23-Jul-2020)

# Author: Zyad Ghorab (EBS-5719)
# Reviewer: Hosam Bayomy & Somaya Ahmed (11-Aug-2020) (EBS-5719)

# Author: Mohamed Aboelnour (EBS-8094)
# Reviewer: Hosam Bayomy & Somaya Ahmed (01-Mar-2021) (EBS-8094)

# Author: Mohamed Aboelnour, Ahmed Ali, Khadrah Ali (EBS-4853)
# Reviewer: Hosam Bayomy & Somaya Ahmed (16-Mar-2021) (EBS-4853)

Feature: Sales Invoice (Without Refernce) - Activate HP

  Activating a Sales Invoice  (Without Refernce) results in the following:
  - Sales Invoice state is changed to Active (EBS-5422)
  - Journal Entries for Sales Invoice is created, and Activation Details determined where: (EBS-5903)
  - the retrieved fiscal year is the active one where the due date belongs to it (EBS-8094)
  - latest ExchangeRate is retrieved based on of default strategy (EBS-4853)
  - Document currency is same as Company Local currency (EBS-5903)
  - Or Document currency is different from Company Local currency (EBS-5903)
  - Sales Invoice - Accounting Details are determined (EBS-5902) (Not developed yet)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                         |
      | Storekeeper_Signmedia | SalesInvoicePostOwner_Signmedia |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia     |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                            | Condition                                                                               |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:Activate                 | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:MarkAsReadyForDelivering | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |

    And the GLAccounts configuration exist:
      | GLAccountConfig    | GLAccount         |
      | CustomersGLAccount | 10203 - Customers |
      | SalesGLAccount     | 40202 - sales     |
      | TaxesGLAccount     | 20102 - Taxes     |

    And the following GLAccounts has the following Subledgers:
      | GLAccount         | SubLedger      |
      | 10203 - Customers | Local_Customer |
      | 40202 - sales     |                |
      | 20102 - Taxes     | Tax            |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 000035 | EGP              | EGP               | 1.00        | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 000036 | USD              | EGP               | 11.44       | 19-Feb-2021 10:00 PM | User Daily Rate       |
      | 000037 | USD              | EGP               | 17.44       | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 000038 | USD              | EGP               | 15.44       | 19-Mar-2021 10:00 PM | Central Bank Of Egypt |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |

  #EBS-5422, #EBS-5903, EBS-8094, EBS-4853
  Scenario: (01) Activate SalesInvoice, of type: Sales Invoice Without Reference, where: (Happy Path)
  - Sales Invoice is draft and all it's data is complete and correct
  - Currency of Sales Invoice is same as Company local currency

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State                     | CreationDate         |
      | 2020100004 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,ReadyForDelivering | 01-Dec-2020 02:20 PM |

    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2020100004 | 0001 - AL Madina | 0002 - Signmedia |

    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer         | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2020100004 | 000002 - Client2 |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2020100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |

    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2020100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2020100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2020100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2020100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2020100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2020100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2020100004 | 4064.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2020100004 | 2992.00        | 4069.12       | 4064.12   |

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last JournalEntry Code is "2019000004"
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_WITHOUT_REFERENCE" with code "2020100004" at "07-Jan-2020 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 06-Jan-2020 12:00 AM |
    Then SalesInvoice with Code "2020100004" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | Active |
    And ActivationDetails in SalesInvoice with Code "2020100004" is updated as follows:
      | Storekeeper       | ActivationDate       | DueDate              | FiscalYear |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | 06-Jan-2020 12:00 AM | 2020       |
    And the following JournalEntry is Created:
      | Code       | CreationDate         | CreatedBy         | JournalDate          | BusinessUnit     | ReferenceDocument         | Company          | FiscalYear |
      | 2020000001 | 07-Jan-2020 09:30 AM | Mahmoud.Abdelaziz | 06-Jan-2020 12:00 AM | 0002 - Signmedia | 2020100004 - SalesInvoice | 0001 - AL Madina | 2020       |
    And The following JournalItems for JournalEntry with code "2020000001":
      | JournalDate          | GLAccount         | SubAccount                                   | Credit | Debit   | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Jan-2020 12:00 AM | 10203 - Customers | 000002 - Client2                             | 0      | 4069.12 | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 40202 - sales     |                                              | 2992   | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0007 - Vat                                   | 29.92  | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 14.96  | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0009 - Real estate taxes                     | 299.2  | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0010 - Service tax                           | 359.04 | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0011 - Adding tax                            | 14.96  | 0       | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0012 - Sales tax                             | 359.04 | 0       | EGP                | EGP             | 1                         | 000035           |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-47"

  #EBS-5422, #EBS-5903, EBS-8094, EBS-4853
  Scenario: (02) Activate SalesInvoice, of type: Sales_Invoice_Without_Reference, where: (Happy Path)
  - Sales Invoice is draft and all it's data is complete and correct
  - Currency of Sales Invoice is different from Company local currency

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State                     | CreationDate         |
      | 2020100005 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,ReadyForDelivering | 01-Dec-2020 02:20 PM |

    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2020100005 | 0001 - AL Madina | 0002 - Signmedia |

    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2020100005 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | USD      | 5.00        |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2020100005 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |

    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2020100005 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2020100005 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2020100005 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2020100005 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2020100005 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2020100005 | 0012 - Sales tax                             | 12.00         | 359.04    |
  #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2020100005 | 4064.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2020100005 | 2992.00        | 4069.12       | 4064.12   |

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last JournalEntry Code is "2019000004"
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_WITHOUT_REFERENCE" with code "2020100005" at "07-Jan-2020 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 06-Jan-2020 12:00 AM |
    Then SalesInvoice with Code "2020100005" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | Active |
    And ActivationDetails in SalesInvoice with Code "2020100005" is updated as follows:
      | Storekeeper       | ActivationDate       | DueDate              | FiscalYear |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | 06-Jan-2020 12:00 AM | 2020       |
    And the following JournalEntry is Created:
      | Code       | CreationDate         | CreatedBy         | JournalDate          | BusinessUnit     | ReferenceDocument         | Company          | FiscalYear |
      | 2020000001 | 07-Jan-2020 09:30 AM | Mahmoud.Abdelaziz | 06-Jan-2020 12:00 AM | 0002 - Signmedia | 2020100005 - SalesInvoice | 0001 - AL Madina | 2020       |
    And The following JournalItems for JournalEntry with code "2020000001":
      | JournalDate          | GLAccount         | SubAccount                                   | Credit | Debit   | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Jan-2020 12:00 AM | 10203 - Customers | 000001 - Al Ahram                            | 0      | 4069.12 | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 40202 - sales     |                                              | 2992   | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0007 - Vat                                   | 29.92  | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 14.96  | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0009 - Real estate taxes                     | 299.2  | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0010 - Service tax                           | 359.04 | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0011 - Adding tax                            | 14.96  | 0       | USD                | EGP             | 17.44                     | 000037           |
      | 06-Jan-2020 12:00 AM | 20102 - Taxes     | 0012 - Sales tax                             | 359.04 | 0       | USD                | EGP             | 17.44                     | 000037           |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-47"
