# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam, Khadrah Ali
# Reviewer: Somaya Ahmed (27-Jul-2020)

Feature: Allowed Action For Sales Invoice (Object Screen)

  Background:
    Given the following users and roles exist:
      | Name               | Role                   |
      | Mahmoud.Abdelaziz  | Storekeeper_Signmedia  |
      | Ahmed.Seif         | Quality_Specialist     |
      | CannotReadSections | CannotReadSectionsRole |
      | Afaf               | FrontDesk              |

    And the following roles and sub-roles exist:
      | Role                   | Subrole                         |
      | Storekeeper_Signmedia  | SalesInvoicePostOwner_Signmedia |
      | Storekeeper_Signmedia  | SalesInvoiceOwner_Signmedia     |
      | Storekeeper_Signmedia  | SalesInvoiceViewer_Signmedia    |
      | Quality_Specialist     | SalesInvoiceViewer              |
      | CannotReadSectionsRole | CannotReadSectionsSubRole       |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                            | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:Delete                   | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:UpdateCompany            | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:UpdateBusinessPartner    | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:UpdateItems              | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:MarkAsReadyForDelivering | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll                  | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadGeneralData          | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadCompany              | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadBusinessPartner      | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadPostingDetails       | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadItems                | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadTaxes                | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadSummary              | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:Activate                 | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:ExportPDF                | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:SubmitEInvoice           | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceViewer              | SalesInvoice:ReadAll                  |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadGeneralData          |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadCompany              |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadBusinessPartner      |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadPostingDetails       |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadItems                |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadTaxes                |                                                                                         |
      | SalesInvoiceViewer              | SalesInvoice:ReadSummary              |                                                                                         |
      | CannotReadSectionsSubRole       | SalesInvoice:Delete                   |                                                                                         |
      | CannotReadSectionsSubRole       | SalesInvoice:Activate                 |                                                                                         |
      | CannotReadSectionsSubRole       | SalesInvoice:ExportPDF                |                                                                                         |
      | CannotReadSectionsSubRole       | SalesInvoice:ReadAll                  |                                                                                         |
      | CannotReadSectionsSubRole       | SalesInvoice:ReadGeneralData          |                                                                                         |

    And the following users doesn't have the following permissions:
      | User               | Permission                            |
      | Afaf               | SalesInvoice:Delete                   |
      | Afaf               | SalesInvoice:UpdateCompany            |
      | Afaf               | SalesInvoice:UpdateBusinessPartner    |
      | Afaf               | SalesInvoice:UpdateItems              |
      | Afaf               | SalesInvoice:MarkAsReadyForDelivering |
      | Afaf               | SalesInvoice:Activate                 |
      | Afaf               | SalesInvoice:ExportPDF                |
      | Afaf               | SalesInvoice:ReadAll                  |
      | Afaf               | SalesInvoice:ReadGeneralData          |
      | Afaf               | SalesInvoice:ReadCompany              |
      | Afaf               | SalesInvoice:ReadTaxes                |
      | Afaf               | SalesInvoice:ReadSummary              |
      | Afaf               | SalesInvoice:ReadBusinessPartner      |
      | Afaf               | SalesInvoice:ReadItems                |
      | Afaf               | SalesInvoice:ReadPostingDetails       |
      | Afaf               | SalesInvoice:SubmitEInvoice           |
      | CannotReadSections | SalesInvoice:UpdateCompany            |
      | CannotReadSections | SalesInvoice:UpdateBusinessPartner    |
      | CannotReadSections | SalesInvoice:UpdateItems              |
      | CannotReadSections | SalesInvoice:MarkAsReadyForDelivering |
      | CannotReadSections | SalesInvoice:ReadCompany              |
      | CannotReadSections | SalesInvoice:ReadTaxes                |
      | CannotReadSections | SalesInvoice:ReadSummary              |
      | CannotReadSections | SalesInvoice:ReadBusinessPartner      |
      | CannotReadSections | SalesInvoice:ReadItems                |
      | CannotReadSections | SalesInvoice:ReadPostingDetails       |
      | CannotReadSections | SalesInvoice:SubmitEInvoice           |
      | Ahmed.Seif         | SalesInvoice:Delete                   |
      | Ahmed.Seif         | SalesInvoice:UpdateCompany            |
      | Ahmed.Seif         | SalesInvoice:UpdateBusinessPartner    |
      | Ahmed.Seif         | SalesInvoice:UpdateItems              |
      | Ahmed.Seif         | SalesInvoice:Activate                 |
      | Ahmed.Seif         | SalesInvoice:ExportPDF                |
      | Ahmed.Seif         | SalesInvoice:MarkAsReadyForDelivering |
      | Ahmed.Seif         | SalesInvoice:SubmitEInvoice           |

    And the following users have the following permissions without the following conditions:
      | User              | Permission                            | Condition                  |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadAll                  | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadGeneralData          | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadCompany              | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadBusinessPartner      | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadPostingDetails       | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadItems                | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadTaxes                | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ReadSummary              | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:Delete                   | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:MarkAsReadyForDelivering | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:Activate                 | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:ExportPDF                | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:SubmitEInvoice           | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:UpdateCompany            | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:UpdateBusinessPartner    | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesInvoice:UpdateItems              | [purchaseUnitName='Flexo'] |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100015 | Draft  | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019100016 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019000003 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |

  #EBS-5557, EBS-3937
  Scenario Outline: (01) Read allowed actions in Sales Invoice Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesInvoice with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | AllowedActions                                                                                                                                                                                         | NoOfActions |
      | Mahmoud.Abdelaziz  | 2019000002 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary, UpdateCompany, UpdateBusinessPartner, UpdateItems, Delete, MarkAsReadyForDelivering | 13          |
      | Mahmoud.Abdelaziz  | 2019000001 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary, ExportPDF, SubmitEInvoice                                                           | 10          |
      | Mahmoud.Abdelaziz  | 2019100015 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary, UpdateBusinessPartner, Delete, Activate                                             | 11          |
      | Mahmoud.Abdelaziz  | 2019100016 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary, ExportPDF, SubmitEInvoice                                                           | 10          |
      | Ahmed.Seif         | 2019000002 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary                                                                                      | 8           |
      | Ahmed.Seif         | 2019000001 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary                                                                                      | 8           |
      | Ahmed.Seif         | 2019100015 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary                                                                                      | 8           |
      | Ahmed.Seif         | 2019100016 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary                                                                                      | 8           |
      | Ahmed.Seif         | 2019000003 | ReadAll, ReadGeneralData, ReadCompany, ReadBusinessPartner, ReadPostingDetails, ReadItems, ReadTaxes, ReadSummary                                                                                      | 8           |
      | CannotReadSections | 2019000002 | ReadAll, ReadGeneralData, Delete                                                                                                                                                                       | 3           |
      | CannotReadSections | 2019000001 | ReadAll, ReadGeneralData, ExportPDF                                                                                                                                                                    | 3           |
      | CannotReadSections | 2019100015 | ReadAll, ReadGeneralData, Activate, Delete                                                                                                                                                             | 4           |
      | CannotReadSections | 2019100016 | ReadAll, ReadGeneralData, ExportPDF                                                                                                                                                                    | 3           |
      | CannotReadSections | 2019000003 | ReadAll, ReadGeneralData, Delete                                                                                                                                                                       | 3           |

  #EBS-5557, EBS-3937
  Scenario Outline: (02) Read allowed actions in Sales Invoice Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of SalesInvoice with code "<SICode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User              | SICode     |
      | Mahmoud.Abdelaziz | 2019000003 |
      | Afaf              | 2019000001 |
      | Afaf              | 2019000002 |
      | Afaf              | 2019100015 |
      | Afaf              | 2019100016 |
      | Afaf              | 2019000003 |

  #EBS-5557, EBS-3937
  Scenario Outline: (03) Read allowed actions in Sales Invoice Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ahmed.Seif"
    And another user is logged in as "Mahmoud.Abdelaziz"
    And "Mahmoud.Abdelaziz" first deleted the SalesInvoice with code "<SICode>" successfully
    When "Ahmed.Seif" requests to read actions of SalesInvoice with code "<SICode>"
    Then an error notification is sent to "Ahmed.Seif" with the following message "Gen-msg-01"
    Examples:
      | SICode     |
      | 2019000002 |
      | 2019100015 |
