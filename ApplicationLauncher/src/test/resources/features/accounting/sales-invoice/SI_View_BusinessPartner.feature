# Dev Auther(EBS-3941): Ahmed Ali
# QA Reviewer(EBS-3941): Khadrah Ali
# Reviewer: engy & kareeem (22-Jan-2019 3:20 PM)
Feature: View business partner in SI

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia     |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
      | Ahmed.Seif        | Quality_Specialist        |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                                   |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | Storekeeper_Signmedia     | SalesInvoiceOwner_Signmedia               |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |
      | Quality_Specialist        | SalesInvoiceViewer                        |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission                       | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadBusinessPartner | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceOwner_Signmedia               | SalesInvoice:Delete              | [purchaseUnitName='Signmedia']                                             |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadBusinessPartner | [purchaseUnitName='Flexo']                                                 |
      | SalesInvoiceViewer                        | SalesInvoice:ReadBusinessPartner |                                                                            |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                       | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadBusinessPartner | [purchaseUnitName='Signmedia'] |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000003 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |
      | 2020000005 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2020000007 | Draft  | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
    And the following businessPartnerData for salesInvoics exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000002 | 000002 - Client2               |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 11.25       |
      | 2019000003 | 000004 - Client3               |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 10000       |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 2020000012 | 0002 - 20% advance, 80% Copy of B/L        | EGP      |             |
      | 2020000007 | 000007 - مطبعة أكتوبر الهندسية | 2020000011 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |

  #### Happy Path  ####
  Scenario: (01) View businessPartner section in SalesInvoice without reference by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view BusinessPartner section of SalesInvoice with code "2019000002"
    Then the following values are displayed to "Ahmed.Al-Ashry":
      | Customer         | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 000002 - Client2 |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 11.25       |

  #EBS-3941
  Scenario Outline: (02) View businessPartner section in SalesInvoice by an authorized user who has one role without conditions(Happy Path)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" requests to view BusinessPartner section of SalesInvoice with code "<SICode>"
    Then the following values are displayed to "Ahmed.Seif":
      | Customer   | SalesOrder   | PaymentTerm   | Currency   | DownPayment   |
      | <Customer> | <SalesOrder> | <PaymentTerm> | <Currency> | <DownPayment> |

    Examples:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000003 | 000004 - Client3               |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 10000       |
      | 2020000005 | 000007 - مطبعة أكتوبر الهندسية | 2020000012 | 0002 - 20% advance, 80% Copy of B/L        | EGP      |             |

  #EBS-3941
  Scenario: (03) View businessPartner section in SalesInvoice for SalesOrder by an authorized user with condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view BusinessPartner section of SalesInvoice with code "2020000005"
    Then the following values are displayed to "Ahmed.Al-Ashry":
      | Customer                       | SalesOrder | PaymentTerm                         | Currency | DownPayment |
      | 000007 - مطبعة أكتوبر الهندسية | 2020000012 | 0002 - 20% advance, 80% Copy of B/L | EGP      |             |

  #### Abuse Cases  ####
  Scenario: (04) View businessPartner section in SalesInvoice without reference by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view BusinessPartner section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  #EBS-3941
  Scenario: (05) View businessPartner section in SalesInvoice for SalesOrder where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "Mahmoud.Abdelaziz"
    And "Mahmoud.Abdelaziz" first deleted the SalesInvoice with code "2020000007" successfully
    When "Ahmed.Al-Ashry" requests to view BusinessPartner section of SalesInvoice with code "2020000007"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
