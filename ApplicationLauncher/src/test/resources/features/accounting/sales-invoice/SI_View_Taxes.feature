@Author: Yara Ameen
Feature: View Sales Invoice Taxes section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |

    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission             | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadTaxes | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadTaxes | [purchaseUnitName='Flexo']                                                 |

    And the following users have the following permissions without the following conditions:
      | User              | Permission             | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadTaxes | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy         | State         | CreationDate         |
      | 2019000002 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Rabie.abdelghafar | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000002 | 0002 - DigiPro | 0002 - Signmedia |

     #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2019000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 14.645         | 200             | 0003 - Kilogram |

     #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount   |
      | 2019000002 | 0007 - Vat                                   | 1             | 29.29       |
      | 2019000002 | 0008 - Commercial and industrial profits tax | 1.478235      | 43.29750315 |
      | 2019000002 | 0009 - Real estate taxes                     | 10            | 292.9       |
      | 2019000002 | 0010 - Service tax                           | 12            | 351.48      |
      | 2019000002 | 0011 - Adding tax                            | 0.5           | 14.645      |
      | 2019000002 | 0012 - Sales tax                             | 12            | 351.48      |

  Scenario: (01) View SalesInvoiceTaxes section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view Taxes section of SalesInvoice with code "2019000002"
    Then the following tax values for SalesInvoice with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | TaxName                                      | TaxPercentage | TaxAmount   |
      | 0007 - Vat                                   | 1             | 29.29       |
      | 0008 - Commercial and industrial profits tax | 1.478235      | 43.29750315 |
      | 0009 - Real estate taxes                     | 10            | 292.9       |
      | 0010 - Service tax                           | 12            | 351.48      |
      | 0011 - Adding tax                            | 0.5           | 14.645      |
      | 0012 - Sales tax                             | 12            | 351.48      |

  Scenario: (02) View SalesInvoiceTaxes section where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully
    When "Ahmed.Al-Ashry" requests to view Taxes section of SalesInvoice with code "2019100001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario: (03) View SalesInvoiceTaxes section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view Taxes section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page