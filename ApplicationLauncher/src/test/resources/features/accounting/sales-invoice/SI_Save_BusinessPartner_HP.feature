Feature: Save BusinessPartner section HP

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | hr1               | SuperUser             |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                     |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia |
      | SuperUser             | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                         | Condition                      |
      | SuperUserSubRole            | *:*                                |                                |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateBusinessPartner | [purchaseUnitName='Signmedia'] |
    And the following PaymentTerms exist:
      | Code | Name                                |
      | 0001 | 20% Deposit, 80% T/T Before Loading |
      | 0002 | 20% advance, 80% Copy of B/L        |
    And the following Currencies exist:
      | Code | Name                 |
      | 0001 | Egyptian Pound       |
      | 0002 | United States Dollar |
    And the following BusinessPartner for the following SalesInvoice exist:
      | SICode     | Customer         | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000002 | 000002 - Client2 |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 11.25       |
    And edit session is "30" minutes

  #********************************************** HAPPY PATH *********************************************#
  Scenario Outline: (01) Save BusinessPartner section within edit session by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And BusinessPartner section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" saves BusinessPartner section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:30 AM" with the following values:
      | Customer         | SalesOrder | PaymentTerm       | Currency |
      | 000002 - Client2 |            | <PaymentTermCode> | 0002     |
    Then SalesInvoice with code "2019000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       |
      | Mahmoud.Abdelaziz | 07-Jan-2019 09:30 AM |
    And BusinessPartner section of SalesInvoice with code "2019000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | SICode     | Customer         | SalesOrder | PaymentTerm   | Currency | DownPayment |
      | Mahmoud.Abdelaziz | 07-Jan-2019 09:30 AM | 2019000002 | 000002 - Client2 |            | <PaymentTerm> | USD      | 11.25       |
    And the lock by "Mahmoud.Abdelaziz" on BusinessPartner section of SalesInvoice with Code "2019000002" is released
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-04"
    Examples:
      | PaymentTermCode | PaymentTerm                         |
      # issue EBS-8396
      |                 |                                     |
      | 0002            | 0002 - 20% advance, 80% Copy of B/L |
