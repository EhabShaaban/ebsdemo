Feature: View CompanyData section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |

    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission               | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadCompany | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadCompany | [purchaseUnitName='Flexo']                                                 |

    And the following users have the following permissions without the following conditions:
      | User              | Permission               | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadCompany | [purchaseUnitName='Signmedia'] |

    And the following CompanyData SalesInvoices exist:
      | Code       | PurchaseUnitName |
      | 2019000002 | 0002 - Signmedia |
      | 2019100001 | 0002 - Signmedia |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |

    And the following CompanyData for SalesInvoices exists:
      | SICode     | Company          |
      | 2019000002 | 0001 - AL Madina |

  Scenario: (01) View CompanyData section by an authorized user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view CompanyData section of SalesInvoice with code "2019000002"
    Then the following values of CompanyData section for SalesInvoice with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | Company   |
      | AL Madina |
      | AL Madina |

  Scenario: (02) View CompanyData section where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully
    When "Ahmed.Al-Ashry" requests to view CompanyData section of SalesInvoice with code "2019100001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  Scenario: (03) View CompanyData section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view CompanyData section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page