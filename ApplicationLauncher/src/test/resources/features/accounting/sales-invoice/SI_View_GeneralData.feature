Feature: View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Rabie.abdelghafar | SalesSpecialist_Flexo     |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SalesSpecialist_Flexo     | SalesInvoiceViewer_Flexo                  |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission                   | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadGeneralData | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Flexo                  | SalesInvoice:ReadGeneralData | [purchaseUnitName='Flexo']                                                 |
    And the following users have the following permissions without the following conditions:
      | User              | Permission                   | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:ReadGeneralData | [purchaseUnitName='Signmedia'] |
    And the following GeneralData for SalesInvoice exists:
      | SICode     | Type                                                              | DocumentOwner                   | CreatedBy                       | State | CreationDate         |
      | 2019000002 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed.Al-Ashry - Ahmed Al-Ashry | Ahmed.Al-Ashry - Ahmed Al-Ashry | Draft | 28-Oct-2019 10:43 AM |

  # EBS-5618
  Scenario: (01) View GeneralData section by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view GeneralData section of SalesInvoice with code "2019000002"
    Then the following values of GeneralData section for SalesInvoice with code "2019000002" are displayed to "Ahmed.Al-Ashry":
      | SICode     | Type                                                              | PurchasingUnit   | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2019000002 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | Ahmed Al-Ashry | Ahmed Al-Ashry | Draft | 28-Oct-2019 10:43 AM |

  Scenario: (02) View GeneralData section where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully
    When "Ahmed.Al-Ashry" requests to view GeneralData section of SalesInvoice with code "2019100001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"

  # EBS-5618
  Scenario: (03) View GeneralData section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to view GeneralData section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
