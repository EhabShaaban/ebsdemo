# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam, Khadrah Ali
# Reviewer: Somaya Ahmed (27-Jul-2020)

Feature: Allowed Actions Sales Invoice - Home Screen

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia     |
      | Ahmed.Seif        | Quality_Specialist        |
      | CreateOnlyUser    | CreateOnlyRole            |
      | Afaf              | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Subrole                                   |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | Storekeeper_Signmedia     | SalesInvoiceViewer_Signmedia              |
      | Storekeeper_Signmedia     | SalesInvoiceOwner_Signmedia               |
      | Quality_Specialist        | SalesInvoiceViewer                        |
      | CreateOnlyRole            | CreateOnlySubRole                         |

    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission           | Condition                                                                  |
      | SalesInvoiceOwner_Signmedia               | SalesInvoice:Create  |                                                                            |
      | SalesInvoiceViewer_Signmedia              | SalesInvoice:ReadAll | [purchaseUnitName='Signmedia']                                             |
      | SalesInvoiceViewer                        | SalesInvoice:ReadAll |                                                                            |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadAll | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | CreateOnlySubRole                         | SalesInvoice:Create  |                                                                            |

    And the following users doesn't have the following permissions:
      | User           | Permission           |
      | Ahmed.Al-Ashry | SalesInvoice:Create  |
      | Afaf           | SalesInvoice:Create  |
      | Afaf           | SalesInvoice:ReadAll |
      | CreateOnlyUser | SalesInvoice:ReadAll |

  Scenario Outline: (01)  Read allowed actions in Sales Invoice Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Sales Invoice home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User              | AllowedActions |
      | Ahmed.Al-Ashry    | ReadAll        |
      | Mahmoud.Abdelaziz | ReadAll,Create |
      | Ahmed.Seif        | ReadAll        |
      | CreateOnlyUser    | Create         |

  Scenario: (02) Read allowed actions in Sales Invoice Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Sales Invoice home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"