#author: Evraam ,  Reviewer: Marina
Feature: ItemsLastSalesPrice

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                                   |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission                       | Condition                                                                  |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadAll             | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadBusinessPartner | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadItems           | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000001 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 02-Jun-2021 02:20 PM |
      | 2019100002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
      | 2019100006 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
      | 2019100007 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
      | 2019100008 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
      | 2019100009 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
      | 2020000009 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 03-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019000001 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100002 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100006 | 0002 - DigiPro | 0001 - Flexo     |
      | 2019100007 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100008 | 0002 - DigiPro | 0001 - Flexo     |
      | 2019100009 | 0002 - DigiPro | 0002 - Signmedia |
      | 2020000009 | 0002 - DigiPro | 0002 - Signmedia |

        #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000001 | 000001 - Al Ahram              | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100002 | 000002 - Client2               | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100006 | 000002 - Client2               | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100007 | 000001 - Al Ahram              | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100008 | 000001 - Al Ahram              |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100009 | 000002 - Client2               |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2020000009 | 000007 - مطبعة أكتوبر الهندسية | 2020000070 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                                             | OrderUnit           | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit            |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.00         | 999.00          | 0019 - Square Meter |
      | 2019000001 | 000008 - PRO-V-ST-2                                              | 0003 - Kg           | 100.00         | 999.00          | 0019 - Square Meter |
      | 2019000001 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0035 - Roll 1.27x50 | 100.00         | 999.00          | 0019 - Square Meter |
      | 2019000001 | 000001 - Hot Laminated Frontlit Fabric roll                      | 0003 - Kg           | 12.00          | 10000.00        | 0019 - Square Meter |
      | 2019100007 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 10.00          | 10.00           | 0019 - Square Meter |
      | 2019100008 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.00         | 100.00          | 0019 - Square Meter |
      | 2019100009 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0003 - Kg           | 100.00         | 100.00          | 0019 - Square Meter |
      | 2020000009 | 000058 - Flex Primer Varnish E22                                 | 0019 - M2           | 50.000         | 95.000          | 0019 - Square Meter |
      | 2020000009 | 000060 - Hot Laminated Frontlit Fabric roll 1                    | 0029 - Roll 2.20x50 | 50.000         | 0.5909          | 0019 - Square Meter |
    And the total number of existing posting SalesInvoice are "7"

   #@INSERT
    And the following SalesInvoices ActivationDetails exist:
      | Code       | Accountant | ActivationDate       | CurrencyPrice | JournalEntryCode | FiscalYear |
      | 2019000001 | Admin      | 28-Oct-2019 09:01 AM | 1 EGP = 1 EGP |                  |            |
      | 2019100002 | Admin      | 10-Sep-2019 12:30 PM | 1 EGP = 1 EGP |                  |            |
      | 2019100006 | Admin      | 10-Sep-2019 12:30 PM | 1 EGP = 1 EGP |                  |            |
      | 2019100007 | Admin      | 28-Oct-2019 09:30 AM | 1 EGP = 1 EGP |                  |            |
      | 2019100008 | Admin      | 28-Oct-2019 09:30 AM | 1 EGP = 1 EGP |                  |            |
      | 2019100009 | Admin      | 28-Oct-2019 09:30 AM | 1 EGP = 1 EGP |                  |            |
      | 2020000009 | Admin      | 30-Aug-2020 11:58 AM | 1 EGP = 1 EGP |                  |            |


    And the following Customers exist:
      | Code   | Name     | BusinessUnit     |
      | 000001 | Al Ahram | 0002 - Signmedia |
      | 000002 | Client2  | 0001 - Flexo     |

    And the following PurchaseUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
      | 0001 | Flexo     |

    And the following Items exist:
      | Code   | Name                                                    | PurchasingUnit |
      | 000006 | wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 0001           |

    And the following UnitOfMeasures section exist in Item with code "000006":
      | AlternateUnit | ConversionFactor | OldItemNumber     |
      | 0003          | 63.5             | 12345678910111231 |
      | 0035          | 63.5             | 12345678910111231 |
#
  #EBS-6230|
  Scenario: (01) View ItemLastSalesPrice by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ItemLastSalesPrice with itemCode "000006" and CustomerCode "000001" and UOM "0003" and PurchaseUnit "0002"
    Then the following SalesInvoicesItem values will be presented to "Ahmed.Al-Ashry":
      | SICode     | SIPostingDate        | UOM             | Item                                                             | Customer          | PurchaseUnit     | SalesPrice |
      | 2019100007 | 28-Oct-2019 09:30 AM | 0003 - Kilogram | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001 - Al Ahram | 0002 - Signmedia | 10.0       |

  #EBS-6230|
  Scenario: (02) View ItemLastSalesPrice by an authorized user with different uom (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ItemLastSalesPrice with itemCode "000006" and CustomerCode "000001" and UOM "0035" and PurchaseUnit "0002"
    Then the following SalesInvoicesItem values will be presented to "Ahmed.Al-Ashry":
      | SICode     | SIPostingDate        | UOM                 | Item                                                             | Customer          | PurchaseUnit     | SalesPrice |
      | 2019000001 | 28-Oct-2019 09:01 AM | 0035 - Roll 1.27x50 | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001 - Al Ahram | 0002 - Signmedia | 999.0      |

  #EBS-6230|
  Scenario: (03) View ItemLastSalesPrice by an authorized user with different item (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ItemLastSalesPrice with itemCode "000001" and CustomerCode "000001" and UOM "0003" and PurchaseUnit "0002"
    Then the following SalesInvoicesItem values will be presented to "Ahmed.Al-Ashry":
      | SICode     | SIPostingDate        | UOM             | Item                                        | Customer          | PurchaseUnit     | SalesPrice |
      | 2019000001 | 28-Oct-2019 09:01 AM | 0003 - Kilogram | 000001 - Hot Laminated Frontlit Fabric roll | 000001 - Al Ahram | 0002 - Signmedia | 10000.0    |

  #EBS-6230|
  Scenario: (04) View ItemLastSalesPrice by an authorized user eith different pu (Happy Path)    //different pu
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ItemLastSalesPrice with itemCode "000006" and CustomerCode "000001" and UOM "0003" and PurchaseUnit "0001"
    Then the following SalesInvoicesItem values will be presented to "Ahmed.Al-Ashry":
      | SICode     | SIPostingDate        | UOM             | Item                                                             | Customer          | PurchaseUnit | SalesPrice |
      | 2019100008 | 28-Oct-2019 09:30 AM | 0003 - Kilogram | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000001 - Al Ahram | 0001 - Flexo | 100.0      |

  #EBS-6230|
  Scenario: (05) View ItemLastSalesPrice by an authorized user eith different customer (Happy Path)    // different cu
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ItemLastSalesPrice with itemCode "000006" and CustomerCode "000002" and UOM "0003" and PurchaseUnit "0002"
    Then the following SalesInvoicesItem values will be presented to "Ahmed.Al-Ashry":
      | SICode     | SIPostingDate        | UOM             | Item                                                             | Customer         | PurchaseUnit     | SalesPrice |
      | 2019100009 | 28-Oct-2019 09:30 AM | 0003 - Kilogram | 000006 - wg-self adhesive vinal 100 micron 140grm glossy 1.27x50 | 000002 - Client2 | 0002 - Signmedia | 100.0      |
