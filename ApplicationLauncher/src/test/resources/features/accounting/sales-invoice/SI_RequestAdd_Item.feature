Feature: SI Request Add Items section

  Background:
    Given the following users and roles exist:
      | Name                              | Role                                                  |
      | Mahmoud.Abdelaziz                 | Storekeeper_Signmedia                                 |
      | Rabie.abdelghafar                 | SalesSpecialist_Flexo                                 |
      | Ahmed.Al-Ashry.NoItemsNoOrderUnit | SalesSpecialist_Signmedia_CannotReadItemsAndOrderUnit |
      | hr1                               | SuperUser                                             |
    And the following roles and sub-roles exist:
      | Role                                                  | Sub-role                    |
      | Storekeeper_Signmedia                                 | SalesInvoiceOwner_Signmedia |
      | SalesSpecialist_Flexo                                 | SalesInvoiceOwner_Flexo     |
      | SalesSpecialist_Signmedia_CannotReadItemsAndOrderUnit | SalesInvoiceOwner_Signmedia |
      | Storekeeper_Signmedia                                 | ItemViewer_Signmedia        |
      | SalesSpecialist_Flexo                                 | ItemViewer_Flexo            |
      | SuperUser                                             | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission               | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateItems | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceOwner_Flexo     | SalesInvoice:UpdateItems | [purchaseUnitName='Flexo']                                                              |
      | ItemViewer_Signmedia        | Item:ReadAll             | [purchaseUnitName='Signmedia']                                                          |
      | ItemViewer_Signmedia        | Item:ReadUoMData         | [purchaseUnitName='Signmedia']                                                          |
      | ItemViewer_Flexo            | Item:ReadAll             | [purchaseUnitName='Flexo']                                                              |
      | ItemViewer_Flexo            | Item:ReadUoMData         | [purchaseUnitName='Flexo']                                                              |
      | SuperUserSubRole            | *:*                      |                                                                                         |
    And the following users doesn't have the following permissions:
      | User                              | Permission       |
      | Ahmed.Al-Ashry.NoItemsNoOrderUnit | Item:ReadAll     |
      | Ahmed.Al-Ashry.NoItemsNoOrderUnit | Item:ReadUoMData |
    And the following users have the following permissions without the following conditions:
      | User              | Permission               | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:UpdateItems | [purchaseUnitName='Signmedia'] |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100001 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000003 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |

  Scenario: (01) Request to Add Item to SalesInvoice in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000002"
    Then a new add Item dialoge is opened and SalesInvoiceItems section of SalesInvoice with code "2019000002" becomes locked by "Mahmoud.Abdelaziz"
    And the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Mahmoud.Abdelaziz":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |
    And the following editable fields are returned to "Mahmoud.Abdelaziz":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |


  Scenario: (02) Request to Add Item to SalesInvoice in (Draft) state by an authorized user- with one role (Happy Path)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000003"
    Then a new add Item dialoge is opened and SalesInvoiceItems section of SalesInvoice with code "2019000003" becomes locked by "Rabie.abdelghafar"
    And the following authorized reads are returned to "Rabie.abdelghafar":
      | AuthorizedReads |
      | ReadItems       |
      | ReadItemUoM     |
    And the following mandatory fields are returned to "Rabie.abdelghafar":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |
    And the following editable fields are returned to "Rabie.abdelghafar":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |

  Scenario: (03) Request to Add Item to SalesInvoice when SalesInvoiceItems section is locked by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first requested to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000002" successfully
    When "Mahmoud.Abdelaziz" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  Scenario: (04) Request to Add Item to SalesInvoice that doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully
    When "Mahmoud.Abdelaziz" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019100001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (05) Request to Add Item to SalesInvoice in a state that doesn't allow this action - All states except Draft (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-03"
    And SalesInvoiceItems section of SalesInvoice with code "2019000001" is not locked by "Mahmoud.Abdelaziz"

  Scenario: (06) Request to Add Item to SalesInvoice by an unauthorized user due to unmatched condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When  "Rabie.abdelghafar" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  Scenario: (07) Request to Add Item to SalesInvoice by a User with no authorized reads - All allowed states (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry.NoItemsNoOrderUnit"
    When "Ahmed.Al-Ashry.NoItemsNoOrderUnit" requests to add Item to SalesInvoiceItems section of SalesInvoice with code "2019000002"
    Then a new add Item dialoge is opened and SalesInvoiceItems section of SalesInvoice with code "2019000002" becomes locked by "Ahmed.Al-Ashry.NoItemsNoOrderUnit"
    And there are no authorized reads returned to "Ahmed.Al-Ashry.NoItemsNoOrderUnit"
    And the following mandatory fields are returned to "Ahmed.Al-Ashry.NoItemsNoOrderUnit":
      | MandatoriesFields   |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |

    And the following editable fields are returned to "Ahmed.Al-Ashry.NoItemsNoOrderUnit":
      | EditableFields      |
      | itemCode            |
      | quantityInOrderUnit |
      | orderUnitCode       |
      | price               |

   ######### Cancel saving SalesInvoiceItems section ########################################################################

  Scenario: (08) Request to Add Item to SalesInvoice Cancel save within the edit session of Item to SalesInvoice in Draft state by an authorized user- with one or more roles (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And SalesInvoiceItems section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving SalesInvoiceItems section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Mahmoud.Abdelaziz" on SalesInvoiceItems section of SalesInvoice with code "2019000002" is released
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-04"

  Scenario: (09) Request to Add Item to SalesInvoice Cancel saving SalesInvoice Items section after lock session is expired
    Given user is logged in as "Mahmoud.Abdelaziz"
    And SalesInvoiceItems section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:00 AM"
    When "Mahmoud.Abdelaziz" cancels saving SalesInvoiceItems section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"

  Scenario: (10) Request to Add Item to SalesInvoice Cancel saving SalesInvoiceItems section after lock session is expire and SalesInvoice doesn't exist (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And SalesInvoiceItems section of SalesInvoice with code "2019100001" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the SalesInvoice with code "2019100001" successfully at "07-Jan-2019 09:31 AM"
    When "Mahmoud.Abdelaziz" cancels saving SalesInvoiceItems section of SalesInvoice with code "2019100001" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"


  Scenario: (11) Request to Add Item to SalesInvoice Cancel saving SalesInvoiceItems section with unauthorized user with condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" cancels saving SalesInvoiceItems section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page
