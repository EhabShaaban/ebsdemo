#Author: Yara Ameen
#Reviewer: Eslam Ayman
Feature:  View All Approved Sales Orders for drop-downs

  Background:
    Given the following users and roles exist:
      | Name        | Role                           |
      | Gehan.Ahmed | LogisticsResponsible_Signmedia |
      | Afaf        | FrontDesk                      |
    And the following roles and sub-roles exist:
      | Role                           | Subrole            |
      | LogisticsResponsible_Signmedia | SOViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole            | Permission         | Condition                      |
      | SOViewer_Signmedia | SalesOrder:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | SalesOrder:ReadAll |

    And the following SalesOrders with the following General, Company, and Details data exist:
      | Code       | Type                      | BusinessUnit     | State                 | CurrencyISO | Company          | Customer                       | PaymentTerm                                |
      | 2020000071 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | GoodsIssueActivated   | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000070 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | SalesInvoiceActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | SalesInvoiceActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000066 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Draft                 | EGP         | 0002 - DigiPro   | 000004 - عميل3                 | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000065 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000064 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000063 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000062 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Draft                 | EGP         | 0001 - AL Madina | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000061 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000060 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000059 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000058 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000057 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000056 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | DeliveryComplete      | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000055 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Expired               | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000054 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Canceled              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | WaitingApproval       | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Rejected              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000050 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Draft                 | EGP         | 0001 - AL Madina | 000006 - المطبعة الأمنية       |                                            |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | ReadyForDelivery      | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | GoodsIssueActivated   | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000010 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | SalesInvoiceActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000008 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | DeliveryComplete      | EGP         | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | 0003 - 100% Advanced Payment               |
      | 2020000007 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Expired               | EGP         | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Canceled              | EGP         | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading |
    And the total number of records of SalesOrders is 34


   #EBS-6817
  Scenario: (01) Read list of SalesOrders - in state Approved by authorized user with condition (Happy Path)
    Given user is logged in as "Gehan.Ahmed"
    When "Gehan.Ahmed" requests to read list of Approved and GoodsIssuedActivated SalesOrders with Business Unit "0002 - Signmedia"
    Then the following SalesOrders values will be presented to "Gehan.Ahmed":
      | Code       | Type                      | BusinessUnit     | State               | CurrencyISO | Company          | Customer                       | PaymentTerm                                |
      | 2020000071 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | GoodsIssueActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved            | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000067 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved            | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved            | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | GoodsIssueActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
    And total number of SalesOrders returned to "Gehan.Ahmed" is equal to 5

   #EBS-6817
  Scenario: (02) Read list of SalesOrders - by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read list of Approved and GoodsIssuedActivated SalesOrders with Business Unit "0002 - Signmedia"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
