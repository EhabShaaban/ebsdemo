# Author Dev: Ahmed Ali
# Author Quality: Eslam Salam
# Reviewer: Hosam Bayomy and Somaya Ahmed (5-Aug-2020)
Feature: Create Sales Invoice - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                   | Role                                             |
      | Mahmoud.Abdelaziz                      | Storekeeper_Signmedia                            |
      | Mahmoud.Abdelaziz.NoSalesInvoiceType   | Storekeeper_Signmedia_CannotReadSalesInvoiceType |
      | Mahmoud.Abdelaziz.NoBusinessUnit       | Storekeeper_Signmedia_CannotReadBusinessUnit     |
      | Mahmoud.Abdelaziz.NoSalesOrder         | Storekeeper_Signmedia_CannotReadSalesOrder       |
      | Mahmoud.Abdelaziz.NoCustomer           | Storekeeper_Signmedia_CannotReadCustomer         |
      | Mahmoud.Abdelaziz.NoCompany            | Storekeeper_Signmedia_CannotReadCompany          |
      | Mahmoud.Abdelaziz.NoDocumentOwner      | Storekeeper_Signmedia_CannotReadDocumentOwner    |
      | Mahmoud.Abdelaziz.NoCreateWithAllReads | Storekeeper_Signmedia_CannotCreateWithAllReads   |
    And the following roles and sub-roles exist:
      | Role                                             | Subrole                       |
      | Storekeeper_Signmedia                            | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia                            | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia                            | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia                            | SOViewer_Signmedia            |
      | Storekeeper_Signmedia                            | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia                            | CompanyViewer                 |
      | Storekeeper_Signmedia                            | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotReadSalesInvoiceType | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotReadBusinessUnit     | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotReadSalesOrder       | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadCustomer         | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadCustomer         | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotReadCustomer         | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotReadCustomer         | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotReadCustomer         | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotReadCustomer         | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadCompany          | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadCompany          | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotReadCompany          | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotReadCompany          | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotReadCompany          | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotReadCompany          | DocumentOwnerViewer_Signmedia |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotReadDocumentOwner    | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | SOViewer_Signmedia            |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | CompanyViewer                 |
      | Storekeeper_Signmedia_CannotCreateWithAllReads   | DocumentOwnerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission               | Condition                      |
      | SalesInvoiceOwner_Signmedia   | SalesInvoice:Create      |                                |
      | SalesInvoiceTypeViewer        | SalesInvoiceType:ReadAll |                                |
      | PurUnitReader_Signmedia       | PurchasingUnit:ReadAll   | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia            | SalesOrder:ReadAll       | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia      | Customer:ReadAll         | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                 | Company:ReadAll          |                                |
      | DocumentOwnerViewer_Signmedia | DocumentOwner:ReadAll    | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User                                   | Permission               |
      | Mahmoud.Abdelaziz.NoSalesInvoiceType   | SalesInvoiceType:ReadAll |
      | Mahmoud.Abdelaziz.NoBusinessUnit       | PurchasingUnit:ReadAll   |
      | Mahmoud.Abdelaziz.NoSalesOrder         | SalesOrder:ReadAll       |
      | Mahmoud.Abdelaziz.NoCustomer           | Customer:ReadAll         |
      | Mahmoud.Abdelaziz.NoCompany            | Company:ReadAll          |
      | Mahmoud.Abdelaziz.NoDocumentOwner      | DocumentOwner:ReadAll    |
      | Mahmoud.Abdelaziz.NoCreateWithAllReads | SalesInvoice:Create      |
    And the following users have the following permissions without the following conditions:
      | User              | Permission             | Condition                  |
      | Mahmoud.Abdelaziz | PurchasingUnit:ReadAll | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | SalesOrder:ReadAll     | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | Customer:ReadAll       | [purchaseUnitName='Flexo'] |
      | Mahmoud.Abdelaziz | DocumentOwner:ReadAll  | [purchaseUnitName='Flexo'] |
    And the following BusinessUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000004 | Client3               | 0001 - Flexo     |
    And the following SalesInvoiceTypes exist:
      | Code                            | Name                            |
      | SALES_INVOICE_WITHOUT_REFERENCE | Sales invoice without reference |
      | SALES_INVOICE_FOR_SALES_ORDER   | Sales invoice for sales order   |
    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition                      |
      | 1000072 | Ahmed Al-Ashry | SalesInvoice   | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
      | 1000073 | Shady.El-Sayed | SalesInvoice   | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |
    And the following SalesOrders with the following General, Company, and Details data exist:
      | Code       | Type                      | BusinessUnit     | State    | CurrencyISO | Company          | Customer                       | PaymentTerm                                |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | Approved | EGP         | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | 0001 - 20% Deposit, 80% T/T Before Loading |

  ### Create Sales Invoice (Auth)
  #EBS-6252
  Scenario Outline: (01) Request Create SalesInvoice by an unauthorized to create user but with all authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Mahmoud.Abdelaziz.NoCreateWithAllReads"
    When "Mahmoud.Abdelaziz.NoCreateWithAllReads" creates SalesInvoice with the following values:
      | Type   | SalesOrder   | BusinessUnit   | Company   | Customer   | DocumentOwnerId   |
      | <Type> | <SalesOrder> | <BusinessUnit> | <Company> | <Customer> | <DocumentOwnerId> |
    Then "Mahmoud.Abdelaziz.NoCreateWithAllReads" is logged out
    And "Mahmoud.Abdelaziz.NoCreateWithAllReads" is forwarded to the error page

    Examples:
      | Type                            | SalesOrder | BusinessUnit | Company | Customer | DocumentOwnerId |
      | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER   | 2020000053 | 0002         | 0001    | 000007   | 1000072         |

  #EBS-6252
  Scenario Outline: (02) Create SalesInvoice by an authorized user to create, but without unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates SalesInvoice with the following values:
      | Type   | SalesOrder   | BusinessUnit   | Company   | Customer   | DocumentOwnerId   |
      | <Type> | <SalesOrder> | <BusinessUnit> | <Company> | <Customer> | <DocumentOwnerId> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User                                 | Type                            | SalesOrder | BusinessUnit | Company | Customer | DocumentOwnerId |
      | Mahmoud.Abdelaziz.NoSalesInvoiceType | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz.NoBusinessUnit     | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz.NoCustomer         | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz.NoCompany          | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz.NoDocumentOwner    | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz.NoSalesOrder       | SALES_INVOICE_FOR_SALES_ORDER   | 2020000053 | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz                    | SALES_INVOICE_FOR_SALES_ORDER   | 2020000003 | 0002         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz                    | SALES_INVOICE_WITHOUT_REFERENCE |            | 0001         | 0001    | 000001   | 1000072         |
      | Mahmoud.Abdelaziz                    | SALES_INVOICE_WITHOUT_REFERENCE |            | 0001         | 0001    | 000004   | 1000072         |
      | Mahmoud.Abdelaziz                    | SALES_INVOICE_WITHOUT_REFERENCE |            | 0002         | 0001    | 000001   | 1000073         |

  ### Request Create Sales Invoice (Happy Path) and (Unauthorized/Abuse Case)
  #EBS-6252
  Scenario: (03) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoSalesInvoiceType"
    When "Mahmoud.Abdelaziz.NoSalesInvoiceType" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoSalesInvoiceType":
      | AuthorizedReads    |
      | ReadBusinessUnits  |
      | ReadSalesOrders    |
      | ReadCustomers      |
      | ReadCompanies      |
      | ReadDocumentOwners |

  #EBS-6252
  Scenario: (04) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoBusinessUnit"
    When "Mahmoud.Abdelaziz.NoBusinessUnit" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoBusinessUnit":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadSalesOrders       |
      | ReadCustomers         |
      | ReadCompanies         |
      | ReadDocumentOwners    |

  #EBS-6252
  Scenario: (05) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoCustomer"
    When "Mahmoud.Abdelaziz.NoCustomer" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoCustomer":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadBusinessUnits     |
      | ReadSalesOrders       |
      | ReadCompanies         |
      | ReadDocumentOwners    |

  #EBS-6252
  Scenario: (06) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoCompany"
    When "Mahmoud.Abdelaziz.NoCompany" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoCompany":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadBusinessUnits     |
      | ReadSalesOrders       |
      | ReadCustomers         |
      | ReadDocumentOwners    |

  #EBS-6252
  Scenario: (07) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoDocumentOwner"
    When "Mahmoud.Abdelaziz.NoDocumentOwner" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoDocumentOwner":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadBusinessUnits     |
      | ReadSalesOrders       |
      | ReadCustomers         |
      | ReadCompanies         |

  #EBS-6252
  Scenario: (08) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz.NoSalesOrder"
    When "Mahmoud.Abdelaziz.NoSalesOrder" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz.NoSalesOrder":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadBusinessUnits     |
      | ReadCustomers         |
      | ReadDocumentOwners    |
      | ReadCompanies         |

  #EBS-6252
  Scenario: (09) Request Create SalesInvoice by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to create SalesInvoice
    Then the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads       |
      | ReadSalesInvoiceTypes |
      | ReadBusinessUnits     |
      | ReadSalesOrders       |
      | ReadCustomers         |
      | ReadDocumentOwners    |
      | ReadCompanies         |

  #EBS-6252
  Scenario: (10) Request Create SalesInvoice by unauthorized user (Unauthorized/Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz.NoCreateWithAllReads"
    When "Mahmoud.Abdelaziz.NoCreateWithAllReads" requests to create SalesInvoice
    Then "Mahmoud.Abdelaziz.NoCreateWithAllReads" is logged out
    And "Mahmoud.Abdelaziz.NoCreateWithAllReads" is forwarded to the error page
