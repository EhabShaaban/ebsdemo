Feature: Sales Invoice - Ready for Delivering (HP)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                     |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                            | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:MarkAsReadyForDelivering | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
    And the following SalesInvoices exist:
      | Code       | State | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
    And the following GeneralData for SalesInvoices exists:
      | SICode     | Type                                                              | DocumentOwner                   | CreatedBy                       | State | CreationDate         |
      | 2019000002 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed.Al-Ashry - Ahmed Al-Ashry | Ahmed.Al-Ashry - Ahmed Al-Ashry | Draft | 28-Oct-2019 10:43 AM |
    And the following BusinessPartnerData for SalesInvoices exist:
      | SICode     | Customer         | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000002 | 000002 - Client2 |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 11.25       |
    And the following CompanyData for SalesInvoices exists:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000002 | 0001 - AL Madina | 0002 - Signmedia |
    And the following TaxDetails exist in taxes section for SalesInvoice "2019000002"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0007 - Vat                                   | 1             | 29.92     |
      | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 0009 - Real estate taxes                     | 10            | 299.2     |
      | 0010 - Service tax                           | 12            | 359.04    |
      | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 0012 - Sales tax                             | 12            | 359.04    |
    And SalesInvoice with code "2019000002" has the following Items:
      | Item                | OrderUnit    | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000008 - PRO-V-ST-2 | 0030 - Piece | 136 | 22.00                | 22.00           | 2992.00     |

  Scenario: (01) Post Sales Invoice Ready for Delivering by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" Marks SalesInvoice with code "2019000002" Ready for Delivering at "07-Jan-2019 09:30 AM"
    Then SalesInvoice with Code "2019000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State              |
      | Mahmoud.Abdelaziz | 07-Jan-2019 09:30 AM | ReadyForDelivering |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "SI-msg-03"
