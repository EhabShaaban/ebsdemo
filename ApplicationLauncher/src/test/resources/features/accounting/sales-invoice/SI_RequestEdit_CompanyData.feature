Feature: Request to edit and cancel CompanyData section in SalesInvoice

  Background:
    Given the following users and roles exist:
      | User                          | Role                                     |
      | Mahmoud.Abdelaziz             | Storekeeper_Signmedia                    |
      | Rabie.abdelghafar             | SalesSpecialist_Flexo                    |
      | Ahmed.Al-Ashry.NoCompanyTaxes | SalesSpecialist_Signmedia_NoCompanyTaxes |
      | hr1                           | SuperUser                                |

    And the following roles and sub-roles exist:
      | Role                                     | Subrole                     |
      | Storekeeper_Signmedia                    | SalesInvoiceOwner_Signmedia |
      | Storekeeper_Signmedia                    | CompanyViewer               |
      | SalesSpecialist_Signmedia_NoCompanyTaxes | SalesInvoiceOwner_Signmedia |
      | SalesSpecialist_Flexo                    | SalesInvoiceOwner_Flexo     |
      | SuperUser                                | SuperUserSubRole            |

    And the following sub-roles and permissions exist:
      | Subrole                     | Permission                 | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateCompany | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | SalesInvoiceOwner_Flexo     | SalesInvoice:UpdateCompany | [purchaseUnitName='Flexo']                                                              |
      | CompanyViewer               | Company:ReadTaxes          |                                                                                         |
      | SuperUserSubRole            | *:*                        |                                                                                         |

    And the following users doesn't have the following permissions:
      | User                          | Permission        |
      | Ahmed.Al-Ashry.NoCompanyTaxes | Company:ReadTaxes |

    And the following users have the following permissions without the following conditions:
      | User              | Permission                 | Condition                      |
      | Rabie.abdelghafar | SalesInvoice:UpdateCompany | [purchaseUnitName='Signmedia'] |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000002 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |

    And edit session is "30" minutes

  #EBS-5616
  Scenario: (01) Request to edit CompanyData section by an authorized user with one role in case state is DRAFT (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to edit CompanyData section of SalesInvoice with code "2019000002"
    Then CompanyData section of SalesInvoice with code "2019000002" becomes in edit mode and locked by "Mahmoud.Abdelaziz"
    And the following authorized reads are returned to "Mahmoud.Abdelaziz":
      | AuthorizedReads  |
      | ReadCompanyTaxes |
    And there are no mandatory fields returned to "Mahmoud.Abdelaziz"
    And the following editable fields are returned to "Mahmoud.Abdelaziz":
      | EditableFields |
      | CompanyTaxes   |

  #EBS-5616
  Scenario: (02) Request to edit CompanyData section by an authorized user with one role in case state is DRAFT (Happy Path/No authorized reads)
    Given user is logged in as "Ahmed.Al-Ashry.NoCompanyTaxes"
    When "Ahmed.Al-Ashry.NoCompanyTaxes" requests to edit CompanyData section of SalesInvoice with code "2019000002"
    Then CompanyData section of SalesInvoice with code "2019000002" becomes in edit mode and locked by "Ahmed.Al-Ashry.NoCompanyTaxes"
    And there are no authorized reads returned to "Ahmed.Al-Ashry.NoCompanyTaxes"
    And there are no mandatory fields returned to "Ahmed.Al-Ashry.NoCompanyTaxes"
    And the following editable fields are returned to "Ahmed.Al-Ashry.NoCompanyTaxes":
      | EditableFields |
      | CompanyTaxes   |

  #EBS-5616
  Scenario: (03) Request to edit CompanyData section that is locked by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first opened CompanyData section of SalesInvoice with code "2019000002" in the edit mode successfully
    When "Mahmoud.Abdelaziz" requests to edit CompanyData section of SalesInvoice with code "2019000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"

  #EBS-5616
  Scenario: (04) Request to edit CompanyData section of a deleted SalesInvoice (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted SalesInvoice with code "2019000002" successfully
    When "Mahmoud.Abdelaziz" requests to edit CompanyData section of SalesInvoice with code "2019000002"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  #EBS-5616
  Scenario: (05) Request to edit CompanyData section when SalesInvoice is Posted (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to edit CompanyData section of SalesInvoice with code "2019000001"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-03"
    And CompanyData section of SalesInvoice with code "2019000001" is not locked by "Mahmoud.Abdelaziz"

  #EBS-5616
  Scenario: (06) Request to edit CompanyData section with unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" requests to edit CompanyData section of SalesInvoice with code "2019000002"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page

  #EBS-5616
  Scenario: (07) Request to edit CompanyData section to Cancel saving by an authorized user with one role in case state is DRAFT (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CompanyData section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:10 AM"
    When "Mahmoud.Abdelaziz" cancels saving CompanyData section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then the lock by "Mahmoud.Abdelaziz" on CompanyData section of SalesInvoice with code "2019000002" is released

  #EBS-5616
  Scenario: (08) Request to edit CompanyData section to Cancel saving after lock session expires
    Given user is logged in as "Mahmoud.Abdelaziz"
    And CompanyData section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:00 AM"
    When "Mahmoud.Abdelaziz" cancels saving CompanyData section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:31 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-07"

  #EBS-5616
  Scenario: (09) Request to edit CompanyData section to Cancel saving after lock session is expired and SalesInvoice doesn't exist anymore(Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And CompanyData section of SalesInvoice with code "2019000002" is locked by "Mahmoud.Abdelaziz" at "07-Jan-2019 09:00 AM"
    And "hr1" first deleted the SalesInvoice with code "2019000002" successfully at "07-Jan-2019 09:31 AM"
    When "Mahmoud.Abdelaziz" cancels saving CompanyData section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:35 AM"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  #EBS-5616
  Scenario: (10) Request to edit CompanyData section to Cancel saving with unauthorized user (Abuse Case)
    Given user is logged in as "Rabie.abdelghafar"
    When "Rabie.abdelghafar" cancels saving CompanyData section of SalesInvoice with code "2019000002" at "07-Jan-2019 09:30 AM"
    Then "Rabie.abdelghafar" is logged out
    And "Rabie.abdelghafar" is forwarded to the error page