# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam
# Reviewer: Somaya Ahmed (24-July-2020)

Feature: View All SI types (Dropdown)

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Afaf           | FrontDesk                 |

    And the following roles and sub-roles exist:
      | Role                      | Sub-role               |
      | SalesSpecialist_Signmedia | SalesInvoiceTypeViewer |

    And the following sub-roles and permissions exist:
      | Subrole                | Permission               | Condition |
      | SalesInvoiceTypeViewer | SalesInvoiceType:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | SalesInvoice:ReadAll |

    And the following are ALL existing SalesInvoiceTypes:
      | Code                            | Name                            |
      | SALES_INVOICE_WITHOUT_REFERENCE | Sales invoice without reference |
      | SALES_INVOICE_FOR_SALES_ORDER   | Sales invoice for sales order   |
    And the total number of existing SalesInvoice Types are 2

    #EBS-6817
  Scenario: (01) Read list of SalesInvoiceTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all SalesInvoiceTypes
    Then the following SalesInvoice Types values will be presented to "Ahmed.Al-Ashry":
      | SalesInvoiceTypes                                                 |
      | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference |
      | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     |
    And total number of SalesInvoices Types returned to "Ahmed.Al-Ashry" is equal to 2

   #EBS-6817
  Scenario: (02) Read list of SalesInvoiceTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesInvoiceTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
