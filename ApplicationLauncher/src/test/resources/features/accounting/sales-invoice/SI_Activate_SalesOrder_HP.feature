# Author: Mohamed Aboelnour, Eslam Salam
# Reviewer: Somaya Ahmed (23-Jul-2020)

# Author: Zyad Ghorab (EBS-5719)
# Reviewer: Hosam Bayomy & Somaya Ahmed (11-Aug-2020) (EBS-5719)

# Author: Mohamed Aboelnour (EBS-8094)
# Reviewer: Hosam Bayomy & Somaya Ahmed (01-Mar-2021) (EBS-8094)

# Author: Mohamed Aboelnour, Ahmed Ali, Khadrah Ali (EBS-4853)
# Reviewer: Hosam Bayomy & Somaya Ahmed (16-Mar-2021) (EBS-4853)

# Reviewer: Somaya Ahmed (3-July-2021)

# Author: Ahmed Ali (EBS-8974)


  #TODO: Add another ExRate from EGP to EGP with different date with the type UserDailt, to make sure that the system is picking up the latest (EBS-1053)
  #TODO: In the givens, add the Sales Order Company, and add the Currency of the Local Company#TODO: In the givens, add a downPayment in the form of Notes Receivable, to make sure that the Sales Invoice Remainng is calculated correctly based on all types of downpayments (EBS-1053)
  #TODO: Check if you can unit test that (Activate Sales Invoice) can work only if the referenced Sales Order is in state Approved or GoodsIssueActivated, then write the unit tests, and remove Scenario (2) to optimize the execution time of integration tests (EBS-1053)
  #TODO: In scenario (2), the case of multi-currency (where the Sales invoice currency is different from the company local currency)is handled without realized ExRate, it will be covered in (EBS-8550)
  #TODO: Handle The collection the case where, downpayemnt currency is different from both SalesInvoice and company currency

Feature: Sales Invoice (based on Sales Order) - Activate HP

  Activating a Sales Invoice (based on Sales Order) results in the following:
  - Sales Invoice state is changed to Active (EBS-6820)
  - Sales Invoice - Accounting Details are determined (EBS-8524) (Not developed yet)
  ---- GLAccount are retrieved from COA Configuration (EBS-8524)
  ---- GLSubAccounts are retrieved from the Collection document (EBS-8524)
  ---- Add GL for Realized ExRate if any (EBS-1053)
  - Journal Entry for Sales Invoice is created (EBS-3947)
  ---- The retrieved fiscal year is the active one where the due date belongs to it (EBS-8094)
  ---- The latest ExchangeRate is retrieved based on the default strategy (EBS-4853)
  - Sales Invoice - Activation Details are determined (EBS-3947)
  - Related Sales Order state is changed to either "Invoiced" or "ReadyForDelivery" (EBS-5719)
  - Sales Invoice Remaining is set as Sales Invoice Total - All Previous downpayemts that can be:
  ---- Collections
  ---- Notes Receivables

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                         |
      | Storekeeper_Signmedia | SalesInvoicePostOwner_Signmedia |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia     |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                            | Condition                                                                               |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:Activate                 | [purchaseUnitName='Signmedia']                                                          |
      | SalesInvoiceOwner_Signmedia     | SalesInvoice:MarkAsReadyForDelivering | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |

    And the GLAccounts configuration exist:
      | GLAccountConfig    | GLAccount         |
      | CustomersGLAccount | 10203 - Customers |
      | SalesGLAccount     | 40202 - sales     |
      | TaxesGLAccount     | 20102 - Taxes     |

    And the following GLAccounts has the following Subledgers:
      | GLAccount         | SubLedger      |
      | 10203 - Customers | Local_Customer |
      | 40202 - sales     |                |
      | 20102 - Taxes     | Tax            |

    And the following SalesOrders exist:
      | Code       | Type                      | Customer                       | SalesResponsible      | ExpectedDeliveryDate | BusinessUnit     | State               |
      | 2020000069 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 05-Nov-2020 12:00 AM | 0002 - Signmedia | Approved            |
      | 2020000011 | LOCAL_SALES_ORDER - Local | 000007 - مطبعة أكتوبر الهندسية | 0004 - Ahmed.Al-Ashry | 25-Aug-2020 12:00 AM | 0002 - Signmedia | GoodsIssueActivated |

    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code   | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 000035 | EGP              | EGP               | 1.00        | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 000036 | USD              | EGP               | 11.44       | 19-Feb-2021 10:00 PM | User Daily Rate       |
      | 000037 | USD              | EGP               | 17.44       | 19-Mar-2021 10:00 PM | User Daily Rate       |
      | 000038 | USD              | EGP               | 15.44       | 19-Mar-2021 10:00 PM | Central Bank Of Egypt |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2019 | 1-Jan-2019 12:00 AM | 31-Dec-2019 11:59 PM | Closed |
      | 0002 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Active |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

  #EBS-6820, EBS-3947, EBS-5719, EBS-8094, EBS-4853, EBS-8974
  Scenario: (01) Activate SalesInvoice, of type: based on Local Sales Order, where: (Happy Path)
  - Sales Invoice is draft and all it's data is complete and correct
  - Related Sales Order is in Approved State
  - Currency of Sales Invoice is same as Company local currency
  - Sales Invoice Remaining is set as Sales Invoice Total - All Previous down payments that can be Collection

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2020000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 03-Nov-2020 09:40 AM |

    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2020000002 | 0001 - AL Madina | 0002 - Signmedia |

    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2020000002 | 000007 - مطبعة أكتوبر الهندسية | 2020000069 | 0003 - 100% Advanced Payment | EGP      | 0           |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item          | OrderUnit    | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2020000002 | 000051 - Ink5 | 0014 - Liter | 5              | 29              | 0019 - M2 |

    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2020000002 | 0007 - Vat                                   | 1.00          | 1.450     |
      | 2020000002 | 0008 - Commercial and industrial profits tax | 0.5           | 0.725     |
      | 2020000002 | 0009 - Real estate taxes                     | 10.00         | 14.500    |
      | 2020000002 | 0010 - Service tax                           | 12.00         | 17.400    |
      | 2020000002 | 0011 - Adding tax                            | 0.5           | 0.725     |
      | 2020000002 | 0012 - Sales tax                             | 12.00         | 17.400    |

#TODO: Revove the below statement as it's redundant
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2020000002 | 197.20    |

    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2020000002 | 145.00         | 197.20        | 197.20    |

    And the following GeneralData for the following Collections exists:
      | CCode      | State  | DocumentOwner            | CreatedBy | CreationDate        | BusinessUnit     | CollectionType | Company          | CompanyLocalCurrencyISO |
      | 2020000003 | Active | 1000072 - Ahmed Al-Ashry | A.Hamed   | 4-Oct-2020 09:00 AM | 0002 - Signmedia | CUSTOMER       | 0001 - AL Madina | EGP                     |
    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury | BankAccount                     | BusinessPartner                | Currency | Amount | Method |
      | 2020000003 | SALES_ORDER           | 2020000069            |          | 1234567893458 - EGP - Bank Misr | 000007 - مطبعة أكتوبر الهندسية | EGP      | 197.2  | BANK   |

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last JournalEntry Code is "2019000004"
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2020000002" at "07-Dec-2020 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 06-Jan-2021 12:00 AM |
    Then SalesInvoice with Code "2020000002" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 07-Dec-2020 09:30 AM | Active |
    And AccountingDetails in "SalesInvoice" with Code "2020000002" is updated as follows:
      | Credit/Debit | GLAccount         | GLSubAccount                                 | Amount |
      | DEBIT        | 10203 - Customers | 000007 - مطبعة أكتوبر الهندسية               | 197.2  |
      | CREDIT       | 40202 - sales     |                                              | 145    |
      | CREDIT       | 20102 - Taxes     | 0007 - Vat                                   | 1.45   |
      | CREDIT       | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 0.725  |
      | CREDIT       | 20102 - Taxes     | 0009 - Real estate taxes                     | 14.5   |
      | CREDIT       | 20102 - Taxes     | 0010 - Service tax                           | 17.4   |
      | CREDIT       | 20102 - Taxes     | 0011 - Adding tax                            | 0.725  |
      | CREDIT       | 20102 - Taxes     | 0012 - Sales tax                             | 17.4   |
    And ActivationDetails in SalesInvoice with Code "2020000002" is updated as follows:
      | Accountant        | ActivationDate       | DueDate              | FiscalYear |
      | Mahmoud.Abdelaziz | 07-Dec-2020 09:30 AM | 06-Jan-2021 12:00 AM | 2021       |
    And InvoiceSummaries section of SalesInvoice with Code "2020000002" is updated as follows:
      | TotalBeforeTax | TotalAfterTax | Remaining |
      | 145.00         | 197.20        | 0         |
    And the following JournalEntry is Created:
      | Code       | CreationDate         | CreatedBy         | JournalDate          | BusinessUnit     | ReferenceDocument         | Company          | FiscalYear |
      | 2020000001 | 07-Dec-2020 09:30 AM | Mahmoud.Abdelaziz | 06-Jan-2021 12:00 AM | 0002 - Signmedia | 2020000002 - SalesInvoice | 0001 - AL Madina | 2021       |
    And The following JournalItems for JournalEntry with code "2020000001":
      | JournalDate          | GLAccount         | SubAccount                                   | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Jan-2021 12:00 AM | 10203 - Customers | 000007 - مطبعة أكتوبر الهندسية               | 0.00   | 197.2 | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 40202 - sales     |                                              | 145    | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0007 - Vat                                   | 1.45   | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 0.725  | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0009 - Real estate taxes                     | 14.5   | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0010 - Service tax                           | 17.4   | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0011 - Adding tax                            | 0.725  | 0.00  | EGP                | EGP             | 1                         | 000035           |
      | 06-Jan-2021 12:00 AM | 20102 - Taxes     | 0012 - Sales tax                             | 17.4   | 0.00  | EGP                | EGP             | 1                         | 000035           |
    And The State of SalesOrder with code "2020000069" is changed to "SalesInvoiceActivated"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-47"

  #EBS-6820, EBS-3947, EBS-5719, EBS-8094, EBS-4853
  Scenario: (02) Activate SalesInvoice, of type: based on Local Sales Order, where: (Happy Path)-
  - Sales Invoice is draft and all it's data is complete and correct
  - Related Sales Order is GoodsIssueActivated State
  - Currency of Sales Invoice is different from Company local currency
  - Sales Invoice Remaining is set as Sales Invoice Total - All Previous down payments that can be Notes Receivables

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |

    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |

    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner                | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000007 - مطبعة أكتوبر الهندسية | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 448.80 | 0         |       |

    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType | AmountToCollect |
      | 2021000001 | 31                        | NOTES_RECEIVABLE_AS_COLLECTION | 2020000011   | SALES_ORDER  | 448.80          |

  #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy | State | CreationDate         |
      | 2020000007 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | hr1       | Draft | 06-Aug-2020 08:13 AM |

  #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2020000007 | 0001 - AL Madina | 0002 - Signmedia |

 #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer                       | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2020000007 | 000007 - مطبعة أكتوبر الهندسية | 2020000011 | 0001 - 20% Deposit, 80% T/T Before Loading | USD      |             |

  #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                          | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2020000007 | 000053 - Hot Laminated Frontlit Fabric roll 2 | 0019 - M2 | 10             | 33.00           | 0019 - M2 |

  #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2020000007 | 0007 - Vat                                   | 1.00          | 3.3       |
      | 2020000007 | 0008 - Commercial and industrial profits tax | 0.5           | 1.65      |
      | 2020000007 | 0009 - Real estate taxes                     | 10.00         | 33.00     |
      | 2020000007 | 0010 - Service tax                           | 12.00         | 39.6      |
      | 2020000007 | 0011 - Adding tax                            | 0.5           | 1.65      |
      | 2020000007 | 0012 - Sales tax                             | 12.00         | 39.6      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2020000007 | 448.80    |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2020000007 | 330.00         | 448.80        | 448.80    |

    Given user is logged in as "Mahmoud.Abdelaziz"
    And Last JournalEntry Code is "2019000004"
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2020000007" at "07-Jan-2020 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 12-Jan-2020 12:00 AM |
    Then SalesInvoice with Code "2020000007" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State  |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | Active |
    And AccountingDetails in "SalesInvoice" with Code "2020000007" is updated as follows:
      | Credit/Debit | GLAccount         | GLSubAccount                                 | Amount |
      | DEBIT        | 10203 - Customers | 000007 - مطبعة أكتوبر الهندسية               | 448.8  |
      | CREDIT       | 40202 - sales     |                                              | 330    |
      | CREDIT       | 20102 - Taxes     | 0007 - Vat                                   | 3.3    |
      | CREDIT       | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 1.65   |
      | CREDIT       | 20102 - Taxes     | 0009 - Real estate taxes                     | 33.00  |
      | CREDIT       | 20102 - Taxes     | 0010 - Service tax                           | 39.6   |
      | CREDIT       | 20102 - Taxes     | 0011 - Adding tax                            | 1.65   |
      | CREDIT       | 20102 - Taxes     | 0012 - Sales tax                             | 39.6   |
    And ActivationDetails in SalesInvoice with Code "2020000007" is updated as follows:
      | Accountant        | ActivationDate       | DueDate              | FiscalYear |
      | Mahmoud.Abdelaziz | 07-Jan-2020 09:30 AM | 12-Jan-2020 12:00 AM | 2020       |
    And InvoiceSummaries section of SalesInvoice with Code "2020000007" is updated as follows:
      | TotalBeforeTax | TotalAfterTax | Remaining |
      | 330.00         | 448.80        | 0         |
    And the following JournalEntry is Created:
      | Code       | CreationDate         | CreatedBy         | JournalDate          | BusinessUnit     | ReferenceDocument         | Company          | FiscalYear |
      | 2020000001 | 07-Jan-2020 09:30 AM | Mahmoud.Abdelaziz | 12-Jan-2020 12:00 AM | 0002 - Signmedia | 2020000007 - SalesInvoice | 0001 - AL Madina | 2020       |
    And The following JournalItems for JournalEntry with code "2020000001":
      | JournalDate          | GLAccount         | SubAccount                                   | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 12-Jan-2020 12:00 AM | 10203 - Customers | 000007 - مطبعة أكتوبر الهندسية               | 0.00   | 448.8 | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 40202 - sales     |                                              | 330    | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0007 - Vat                                   | 3.3    | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0008 - Commercial and industrial profits tax | 1.65   | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0009 - Real estate taxes                     | 33.00  | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0010 - Service tax                           | 39.6   | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0011 - Adding tax                            | 1.65   | 0.00  | USD                | EGP             | 17.44                     | 000037           |
      | 12-Jan-2020 12:00 AM | 20102 - Taxes     | 0012 - Sales tax                             | 39.6   | 0.00  | USD                | EGP             | 17.44                     | 000037           |
    And The State of SalesOrder with code "2020000011" is changed to "ReadyForDelivery"
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-47"
