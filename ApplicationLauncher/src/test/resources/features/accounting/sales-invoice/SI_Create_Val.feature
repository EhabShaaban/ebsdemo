#Dev Auther: Ahmed Ali
#Tester Reviewer: Khadrah Ali
Feature: Create Sales Invoice - Validation

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                       |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia   |
      | Storekeeper_Signmedia | SalesInvoiceTypeViewer        |
      | Storekeeper_Signmedia | PurUnitReader_Signmedia       |
      | Storekeeper_Signmedia | SOViewer_Signmedia            |
      | Storekeeper_Signmedia | CustomerViewer_Signmedia      |
      | Storekeeper_Signmedia | CompanyViewer                 |
      | Storekeeper_Signmedia | DocumentOwnerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission               | Condition                      |
      | SalesInvoiceOwner_Signmedia   | SalesInvoice:Create      |                                |
      | SalesInvoiceTypeViewer        | SalesInvoiceType:ReadAll |                                |
      | PurUnitReader_Signmedia       | PurchasingUnit:ReadAll   | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia            | SalesOrder:ReadAll       | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia      | Customer:ReadAll         | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                 | Company:ReadAll          |                                |
      | DocumentOwnerViewer_Signmedia | DocumentOwner:ReadAll    | [purchaseUnitName='Signmedia'] |
    And the following BusinessUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following SalesInvoiceTypes exist:
      | Code                          | Name                          |
      | SALES_INVOICE_FOR_SALES_ORDER | Sales invoice for sales order |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition                      |
      | 1000072 | Ahmed Al-Ashry | SalesInvoice   | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
    And the following SalesOrders with the following General, Company, and Details data exist:
      | Code       | Type                      | BusinessUnit     | State                 | CurrencyISO | Company          | Customer                       | PaymentTerm                                |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Approved              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000057 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Draft                 | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |                                            |
      | 2020000051 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Rejected              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000052 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | WaitingApproval       | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000055 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Expired               | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0001 - 20% Deposit, 80% T/T Before Loading |
      | 2020000056 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | DeliveryComplete      | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |
      | 2020000054 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | Canceled              | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000068 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | SalesInvoiceActivated | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment               |
      | 2020000012 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | ReadyForDelivery      | EGP         | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية | 0002 - 20% advance, 80% Copy of B/L        |

  #EBS-5407
  Scenario Outline: (01) Create SalesInvoice for SalesOrder with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates SalesInvoice for SalesOrder with the following values:
      | Type   | SOCode   | BusinessUnit   | DocumentOwnerId   |
      | <Type> | <SOCode> | <BusinessUnit> | <DocumentOwnerId> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

    Examples:
      | Type                          | SOCode     | BusinessUnit | DocumentOwnerId |
      |                               | 2020000053 | 0002         | 1000072         |
      | ""                            | 2020000053 | 0002         | 1000072         |
      | N/A                           | 2020000053 | 0002         | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER |            | 0002         | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | ""         | 0002         | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | N/A        | 0002         | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 |              | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | ""           | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | N/A          | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | 0002         |                 |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | 0002         | ""              |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | 0002         | N/A             |

  #EBS-5407
  Scenario Outline: (02) Create SalesInvoice for SalesOrder with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates SalesInvoice for SalesOrder with the following values:
      | Type   | SOCode   | BusinessUnit   | DocumentOwnerId   |
      | <Type> | <SOCode> | <BusinessUnit> | <DocumentOwnerId> |
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

    Examples:
      | Type                          | SOCode      | BusinessUnit | DocumentOwnerId |
      | NOT_EXIST_TYPE                | 2020000053  | 0002         | 1000072         |
      #Draft SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000057  | 0002         | 1000072         |
      #Rejected SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000051  | 0002         | 1000072         |
      #WaitingApproval SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000052  | 0002         | 1000072         |
      #Expired SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000055  | 0002         | 1000072         |
      #DeliveryComplete SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000056  | 0002         | 1000072         |
      #Not exist SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 11          | 0002         | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | STRING_CODE | 0002         | 1000072         |
      #Not exist BusinessUnit
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053  | 22222        | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053  | STRING_CODE  | 1000072         |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053  | 0002         | 9999.999        |
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053  | 0002         | STRING_ID       |

  #EBS-5407
  Scenario Outline: (03) Create SalesInvoice for SalesOrder with incorrect data entry (Validation Failure)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" creates SalesInvoice for SalesOrder with the following values:
      | Type   | SOCode   | BusinessUnit   | DocumentOwnerId   |
      | <Type> | <SOCode> | <BusinessUnit> | <DocumentOwnerId> |
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Mahmoud.Abdelaziz"

    Examples:
      | Type                          | SOCode     | BusinessUnit | DocumentOwnerId | Field           | ErrMsg     |
      #Cancelled SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000054 | 0002         | 1000072         | salesOrderCode  | Gen-msg-32 |
      #SalesInvoiceActivated SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000068 | 0002         | 1000072         | salesOrderCode  | Gen-msg-32 |
      #ReadyForDelievery SalesOrder
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000012 | 0002         | 1000072         | salesOrderCode  | Gen-msg-32 |
      #Not exist DocumentOwnerId
      | SALES_INVOICE_FOR_SALES_ORDER | 2020000053 | 0002         | 9999999         | documentOwnerId | Gen-msg-48 |
