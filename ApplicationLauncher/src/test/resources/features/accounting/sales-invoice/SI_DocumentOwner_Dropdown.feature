#Author: Ahmed Ali & Yara Ameen
#Reviewer: Eslam Ayman
Feature: SalesInvoices DropDown

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Afaf              | FrontDesk             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                       |
      | Storekeeper_Signmedia | DocumentOwnerViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                       | Permission            | Condition                      |
      | DocumentOwnerViewer_Signmedia | DocumentOwner:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id      | Name                | BusinessObject   | Permission         | Condition                      |
      | 1000072 | Ahmed Al-Ashry      | SalesReturnOrder | CanBeDocumentOwner |                                |
      | 1000072 | Ahmed Al-Ashry      | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
      | 16      | Ahmed Hamdy         | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |
      | 1000075 | Mahmoud.Hashim      | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |
      | 1000093 | Manar Mohammed      | SalesReturnOrder | CanBeDocumentOwner |                                |
      | 1000074 | Mohamed.Hafez       | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |
      | 18      | Rabie.abdelghafar   | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |
      | 1000077 | SeragEldin Meghawry | SalesReturnOrder | CanBeDocumentOwner |                                |
      | 40      | Shady.Abdelatif     | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Signmedia'] |
      | 1000073 | Shady.El-Sayed      | SalesInvoice     | CanBeDocumentOwner | [purchaseUnitName='Flexo']     |

  #EBS-6817
  Scenario: (01) Read All DocumentOwners Dropdown in SalseInvoice creation by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to read all DocumentOwners for "SalesInvoice"
    Then the following DocumentOwners values will be presented to "Mahmoud.Abdelaziz":
      | Name              |
      | Ahmed Al-Ashry    |
      | Ahmed Hamdy       |
      | Mahmoud.Hashim    |
      | Mohamed.Hafez     |
      | Rabie.abdelghafar |
      | Shady.Abdelatif   |
      | Shady.El-Sayed    |
    And total number of DocumentOwners returned to "Mahmoud.Abdelaziz" in a dropdown is equal to 7

  #EBS-6817
  Scenario: (02) Read All DocumentOwners in SalseInvoice creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "SalesInvoice"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
