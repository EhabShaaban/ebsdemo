Feature: SI Save Item (HP)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |

    And the following roles and sub-roles exist:
      | Role                  | Subrole                     |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia |
      | Storekeeper_Signmedia | ItemViewer_Signmedia        |

    And the following sub-roles and permissions exist:
      | Subrole                     | Permission               | Condition                                                                               |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:UpdateItems | ([purchaseUnitName='Signmedia'] && [invoiceTypeCode='SALES_INVOICE_WITHOUT_REFERENCE']) |
      | ItemViewer_Signmedia        | Item:ReadAll             | [purchaseUnitName='Signmedia']                                                          |
      | ItemViewer_Signmedia        | Item:ReadUoMData         | [purchaseUnitName='Signmedia']                                                          |

    And the following SalesInvoices exist for save new Item:
      | Code       | State | BusinessUnit     | Currency |
      | 2019000005 | Draft | 0002 - Signmedia | EGP      |

    And SalesInvoice with code "2019000005" has the following Items:
      | Item                                         | OrderUnit           | Qty | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 100 | 330.00               | 3.00            | 33000       |
      | 000002 - Hot Laminated Frontlit Backlit roll | 0029 - Roll 2.20x50 | 200 | 440.00               | 4.00            | 88000       |

    And the following Items exist with the following details:
      | Item                                          | Type       | UOM       | PurchaseUnit     |
      | 000060 - Hot Laminated Frontlit Fabric roll 1 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000001 - Hot Laminated Frontlit Fabric roll   | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000002 - Hot Laminated Frontlit Backlit roll  | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000003 - Flex Primer Varnish E33              | COMMERCIAL | 0003 - Kg | 0002 - Signmedia |
      | 000004 - Flex cold foil adhesive E01          | COMMERCIAL | 0003 - Kg | 0002 - Signmedia |
      | 000007 - PRO-V-ST-1                           | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000008 - PRO-V-ST-2                           | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000053 - Hot Laminated Frontlit Fabric roll 2 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000054 - Hot Laminated Frontlit Fabric roll 3 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |
      | 000051 - Ink5                                 | COMMERCIAL | 0019 - M2 | 0002 - Signmedia |

    And the following TaxDetails exist in taxes section for SalesInvoice "2019000005"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 1210      |
      | 0002 - Commercial and industrial profits tax | 0.5           | 605       |
      | 0003 - Real estate taxes                     | 10            | 12100     |

  #EBS-5425
  Scenario: (01) Save new SalesInvoice Item "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given  user is logged in as "Mahmoud.Abdelaziz"
    And InvoiceItems section of SalesInvoice with Code "2019000005" is locked by "Mahmoud.Abdelaziz" at "01-Jan-2019 11:00 AM"
    When "Mahmoud.Abdelaziz" saves the following new item to InvoiceItems section of SalesInvoice with Code "2019000005" at "01-Jan-2019 11:10 AM" with the following values:
      | Item                             | Qty | OrderUnit       | SalesPrice |
      | 000003 - Flex Primer Varnish E33 | 10  | 0003 - Kilogram | 10         |
    Then SalesInvoice with Code "2019000005" is updated as follows:
      | LastUpdatedBy     | LastUpdateDate       | State |
      | Mahmoud.Abdelaziz | 01-Jan-2019 11:10 AM | Draft |
    And InvoiceItems section of SalesInvoice with code "2019000005" is created as follows:
      | Item                                         | Qty | OrderUnit           | UnitPrice(OrderUnit) | UnitPrice(Base) | TotalAmount |
      | 000002 - Hot Laminated Frontlit Backlit roll | 100 | 0029 - Roll 2.20x50 | 330.00               | 3               | 33000       |
      | 000002 - Hot Laminated Frontlit Backlit roll | 200 | 0029 - Roll 2.20x50 | 440.00               | 4               | 88000       |
      | 000003 - Flex Primer Varnish E33             | 10  | 0003 - Kilogram     | 10.00                | 10              | 100         |

    And InvoiceTaxes section of SalesInvoice with Code "2019000005" is updated as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 1211      |
      | 0002 - Commercial and industrial profits tax | 0.5           | 605.5     |
      | 0003 - Real estate taxes                     | 10            | 12110     |

    And the lock by "Mahmoud.Abdelaziz" on InvoiceItems section of SalesInvoice with Code "2019000005" is released
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-16"
