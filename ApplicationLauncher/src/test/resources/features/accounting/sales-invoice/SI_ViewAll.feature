# Author: Evraam Hany (EBS-5727)
# Reviewer: Marina Salah
# Reviewer: Somaya Ahmed (29-Jun-2020)
Feature: View All Sales Invoices

  Background:
    Given the following users and roles exist:
      | Name           | Role                      |
      | hr1            | SuperUser                 |
      | Afaf           | FrontDesk                 |
      | Ahmed.Al-Ashry | SalesSpecialist_Signmedia |
      | Ashraf.Fathi   | M.D                       |
    And the following roles and sub-roles exist:
      | Role                      | Subrole                                   |
      | SuperUser                 | SuperUserSubRole                          |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | M.D                       | SalesInvoiceViewer                        |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission           | Condition                                                                  |
      | SuperUserSubRole                          | *:*                  |                                                                            |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadAll | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SalesInvoiceViewer                        | SalesInvoice:ReadAll |                                                                            |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | SalesInvoice:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User           | Permission           | Condition                  |
      | Ahmed.Al-Ashry | SalesInvoice:ReadAll | [purchaseUnitName='Flexo'] |
    And the following ViewAll data for SalesInvoices exist And the total number of existing SalesInvoices is 6
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer                       | DocumentOwner                         |
      | 2019000001 | Active | 28-Oct-2019 08:58 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed.Al-Ashry - Ahmed Al-Ashry       |
      | 2019000002 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2               | Ahmed.Al-Ashry - Ahmed Al-Ashry       |
      | 2019000003 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     | 0002 - DigiPro   | 000004 - Client3               | Rabie.abdelghafar - Rabie.abdelghafar |
      | 2019000004 | Draft  | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed.Al-Ashry - Ahmed Al-Ashry       |
      | 2019100015 | Draft  | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000007 - مطبعة أكتوبر الهندسية | Ahmed.Al-Ashry - Ahmed Al-Ashry       |
      | 2019100016 | Active | 03-Nov-2019 09:55 AM | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | Ahmed.Al-Ashry - Ahmed Al-Ashry       |

  # EBS-5727
  Scenario: (01) View All SalesInvoices by authorized user due to condition on (Business Unit) (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with no filter applied 5 records per page
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer                       | DocumentOwner  |
      | 2019100016 | Active | 03-Nov-2019 09:55 AM | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | Ahmed Al-Ashry |
      | 2019100015 | Draft  | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000007 - مطبعة أكتوبر الهندسية | Ahmed Al-Ashry |
      | 2019000004 | Draft  | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed Al-Ashry |
      | 2019000002 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2               | Ahmed Al-Ashry |
      | 2019000001 | Active | 28-Oct-2019 08:58 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 5

  # EBS-5727
  Scenario: (02) View All SalesInvoices by authorized user + Paging (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with no filter applied 1 records per page
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                          | PurchaseUnit     | Company        | Customer                 | DocumentOwner  |
      | 2019100016 | Active | 03-Nov-2019 09:55 AM | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | 0002 - Signmedia | 0002 - DigiPro | 000006 - المطبعة الأمنية | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 5

  #EBS-5727
  Scenario: (11) View All SalesInvoices by authorized user without condition (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of SalesInvoices with no filter applied 6 records per page
    Then the following SalesInvoices will be presented to "Ashraf.Fathi":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer                       | DocumentOwner     |
      | 2019100016 | Active | 03-Nov-2019 09:55 AM | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000006 - المطبعة الأمنية       | Ahmed Al-Ashry    |
      | 2019100015 | Draft  | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000007 - مطبعة أكتوبر الهندسية | Ahmed Al-Ashry    |
      | 2019000004 | Draft  | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed Al-Ashry    |
      | 2019000003 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     | 0002 - DigiPro   | 000004 - Client3               | Rabie.abdelghafar |
      | 2019000002 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2               | Ahmed Al-Ashry    |
      | 2019000001 | Active | 28-Oct-2019 08:58 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed Al-Ashry    |
    And the total number of records in search results by "Ashraf.Fathi" are 6

  # EBS-5727
  Scenario: (03) View All SalesInvoices filtered by InvoiceCode by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on InvoiceCode which contains "004" with 5 records per page while current locale is "en-US"
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company        | Customer          | DocumentOwner  |
      | 2019000004 | Draft | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro | 000001 - Al Ahram | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 1

  # EBS-5727
  Scenario: (04) View All SalesInvoices filtered by State by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on State which equals "Draft" with 5 records per page
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer                       | DocumentOwner  |
      | 2019100015 | Draft | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro   | 000007 - مطبعة أكتوبر الهندسية | Ahmed Al-Ashry |
      | 2019000004 | Draft | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram              | Ahmed Al-Ashry |
      | 2019000002 | Draft | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2               | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 3

  # EBS-5727
  Scenario: (05) View All SalesInvoices filtered by CreationDate by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on CreationDate which equals "28-Oct-2019" with 3 records per page
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer          | DocumentOwner  |
      | 2019000002 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2  | Ahmed Al-Ashry |
      | 2019000001 | Active | 28-Oct-2019 08:58 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 2

  #EBS-5727
  Scenario: (06) View All SalesInvoices filtered by SalesOrder by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on SalesOrder which contains "005" with 5 records per page
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State | CreationDate         | SalesOrder | Type                                                          | PurchaseUnit     | Company        | Customer                       | DocumentOwner  |
      | 2019100015 | Draft | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | 0002 - Signmedia | 0002 - DigiPro | 000007 - مطبعة أكتوبر الهندسية | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 1

  # EBS-5727
  Scenario: (07) View All SalesInvoices filtered by InvoiceType by authorized user (without condition) (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to read page 1 of SalesInvoices with filter applied on InvoiceTypeCode which equals "Sales invoice without reference" with 5 records per page while current locale is "en-US"
    Then the following SalesInvoices will be presented to "Ashraf.Fathi":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer          | DocumentOwner     |
      | 2019000004 | Draft  | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram | Ahmed Al-Ashry    |
      | 2019000003 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     | 0002 - DigiPro   | 000004 - Client3  | Rabie.abdelghafar |
      | 2019000002 | Draft  | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2  | Ahmed Al-Ashry    |
      | 2019000001 | Active | 28-Oct-2019 08:58 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro   | 000001 - Al Ahram | Ahmed Al-Ashry    |
    And the total number of records in search results by "Ashraf.Fathi" are 4

  # EBS-5727
  Scenario: (08) View All SalesInvoices filtered by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of SalesInvoices with filter applied on BusinessUnit which contains "Flexo" with 3 records per page while current locale is "en-US"
    Then the following SalesInvoices will be presented to "hr1":
      | Code       | State | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit | Company        | Customer         | DocumentOwner     |
      | 2019000003 | Draft | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo | 0002 - DigiPro | 000004 - Client3 | Rabie.abdelghafar |
    And the total number of records in search results by "hr1" are 1

  # EBS-5727
  Scenario: (09) View All SalesInvoices filtered by Company by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on Company which contains "DigiPro" with 3 records per page while current locale is "en-US"
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State  | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company        | Customer                       | DocumentOwner  |
      | 2019100016 | Active | 03-Nov-2019 09:55 AM | 2020000003 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro | 000006 - المطبعة الأمنية       | Ahmed Al-Ashry |
      | 2019100015 | Draft  | 03-Nov-2019 09:50 AM | 2020000053 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia | 0002 - DigiPro | 000007 - مطبعة أكتوبر الهندسية | Ahmed Al-Ashry |
      | 2019000004 | Draft  | 03-Nov-2019 09:40 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0002 - DigiPro | 000001 - Al Ahram              | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 4

  # EBS-5727
  Scenario: (10) View All SalesInvoices filtered by Customer by authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of SalesInvoices with filter applied on Customer which contains "ient" with 3 records per page while current locale is "en-US"
    Then the following SalesInvoices will be presented to "Ahmed.Al-Ashry":
      | Code       | State | CreationDate         | SalesOrder | Type                                                              | PurchaseUnit     | Company          | Customer         | DocumentOwner  |
      | 2019000002 | Draft | 28-Oct-2019 10:43 AM |            | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia | 0001 - AL Madina | 000002 - Client2 | Ahmed Al-Ashry |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 1

  # EBS-5727
  Scenario: (12) View All SalesInvoices by InvoiceCode by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of SalesInvoices with filter applied on Company which contains "Pro" with 3 records per page while current locale is "en-US"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
