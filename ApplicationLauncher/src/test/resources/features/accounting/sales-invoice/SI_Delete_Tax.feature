# Author Dev: Evraam Hany, Mohamed Aboelnour
# Author Quality: Eslam Salam
# Reviewer: Somaya Ahmed (?-Aug-2020)
Feature: Delete Tax in SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                      |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia     |
      | Ahmed.Al-Ashry    | SalesSpecialist_Signmedia |
      | hr1               | SuperUser                 |
    And the following roles and sub-roles exist:
      | Role                      | Sub-role                                  |
      | Storekeeper_Signmedia     | SalesInvoiceOwner_Signmedia               |
      | SalesSpecialist_Signmedia | SalesInvoiceViewer_Signmedia_LoggedInUser |
      | SuperUser                 | SuperUserSubRole                          |
    And the following sub-roles and permissions exist:
      | Subrole                                   | Permission               | Condition                                                                  |
      | SalesInvoiceOwner_Signmedia               | SalesInvoice:UpdateTaxes | [purchaseUnitName='Signmedia']                                             |
      | SalesInvoiceViewer_Signmedia_LoggedInUser | SalesInvoice:ReadTaxes   | ([purchaseUnitName='Signmedia'] && [documentOwnerUserName='LoggedInUser']) |
      | SuperUserSubRole                          | *:*                      |                                                                            |

    And the following users have the following permissions without the following conditions:
      | User              | Permission               | Condition                  |
      | Mahmoud.Abdelaziz | SalesInvoice:UpdateTaxes | [purchaseUnitName='Flexo'] |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019100015 | Draft  | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2020000004 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019100003 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |

    And the following TaxDetails exist in taxes section for SalesInvoice "2019100015"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 317.50    |
      | 0002 - Commercial and industrial profits tax | 0.5           | 158.75    |
      | 0003 - Real estate taxes                     | 10            | 3175.00   |

    And the following TaxDetails exist in taxes section for SalesInvoice "2020000004"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0007 - Vat                                   | 1             | 2.05      |
      | 0008 - Commercial and industrial profits tax | 0.5           | 1.025     |
      | 0009 - Real estate taxes                     | 10            | 20.5      |
      | 0010 - Service tax                           | 12            | 24.6      |
      | 0011 - Adding tax                            | 0.5           | 1.025     |
      | 0012 - Sales tax                             | 12            | 24.6      |

    And the following TaxDetails exist in taxes section for SalesInvoice "2019100003"
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 0         |
      | 0002 - Commercial and industrial profits tax | 0.5           | 0         |
      | 0003 - Real estate taxes                     | 10            | 0         |

    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100015 | 31750.00       | 35401.25      | 35396.25  |
      | 2020000004 | 205.00         | 278.8         | 278.8     |
      | 2019100003 | 0              | 0             | -5.00     |


  Scenario: (01) Delete Tax from SalesInvoice, where SalesInvoice is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Tax with code "0003" in SalesInvoice with code "2019100015"
    Then Tax with code "0003" from SalesInvoice with code "2019100015" is deleted
    And InvoiceTaxes section of SalesInvoice with Code "2019100015" is updated as follows:
      | Tax                                          | TaxPercentage | TaxAmount |
      | 0001 - Vat                                   | 1             | 317.50    |
      | 0002 - Commercial and industrial profits tax | 0.5           | 158.75    |
    And InvoiceSummaries section of SalesInvoice with Code "2019100015" is updated as follows:
      | TotalBeforeTax | TotalAfterTax | Remaining |
      | 31750.00       | 32226.25      | 32221.25  |
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-12"

  Scenario: (02) Delete Tax from SalesInvoice, where Tax doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted Tax with code "0003" of SalesInvoice with code "2019100015" successfully
    When "Mahmoud.Abdelaziz" requests to delete Tax with code "0003" in SalesInvoice with code "2019100015"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-20"

  Scenario: (03) Delete Tax from SalesInvoice, where action not allowed per current state (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Tax with code "0007" in SalesInvoice with code "2020000004"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-13"

  Scenario: (04) Delete Tax from SalesInvoice, where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "2019100015" successfully
    When "Mahmoud.Abdelaziz" requests to delete Tax with code "0003" in SalesInvoice with code "2019100015"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (05) Delete Tax from SalesInvoice by Unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete Tax with code "0003" in SalesInvoice with code "2019100015"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page

  Scenario: (06) Delete Tax from SalesInvoice by Unauthorized user due condition(Unauthorized access/Abuse case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete Tax with code "0003" in SalesInvoice with code "2019100003"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  @Future
  Scenario: (07) Delete Tax from SalesInvoice when section is locked by another user (Exception)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first opened Taxes section of SalesInvoice with code "2019100015" in the edit mode successfully
    When "Mahmoud.Abdelaziz" requests to edit Taxes section of SalesInvoice with code "2019100015"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-02"
