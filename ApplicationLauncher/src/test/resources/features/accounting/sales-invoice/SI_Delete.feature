# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam
# Reviewer: Somaya Ahmed (23-Jul-2020)
Feature: Delete SalesInvoice

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | Shady.Abdelatif   | Accountant_Signmedia  |
      | hr1               | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                     |
      | Storekeeper_Signmedia | SalesInvoiceOwner_Signmedia |
      | SuperUser             | SuperUserSubRole            |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission          | Condition                      |
      | SalesInvoiceOwner_Signmedia | SalesInvoice:Delete | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole            | *:*                 |                                |
    And the following users have the following permissions without the following conditions:
      | User              | Permission          | Condition                  |
      | Mahmoud.Abdelaziz | SalesInvoice:Delete | [purchaseUnitName='Flexo'] |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019100001 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100002 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019100015 | Draft  | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019100016 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |
      | 2019100003 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |

  #EBS-5724
  Scenario Outline: (01) Delete SalesInvoice draft by an authorized user  (Happy Path)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete SalesInvoice with code "<SICode>"
    Then SalesInvoice with code "<SICode>" is deleted from the system
    And a success notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-12"

    Examples:
      | SICode     |
      | 2019100001 |
      | 2019100015 |

  #EBS-5724
  Scenario Outline: (02) Delete SalesInvoice draft, where SalesInvoice doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And "hr1" first deleted the SalesInvoice with code "<SICode>" successfully
    When "Mahmoud.Abdelaziz" requests to delete SalesInvoice with code "<SICode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

    Examples:
      | SICode     |
      | 2019100001 |
      | 2019100015 |

  #EBS-5724
  Scenario Outline: (03) Delete SalesInvoice posted, where action not allowed per current state (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete SalesInvoice with code "<SICode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-13"

    Examples:
      | SICode     |
      | 2019100002 |
      | 2019100016 |

  #EBS-5724
  Scenario Outline: (04) Delete SalesInvoice posted that is locked by another user (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    And first "hr1" opens "<SISection>" section of SalesInvoice with code "<SICode>" in edit mode
    When "Mahmoud.Abdelaziz" requests to delete SalesInvoice with code "<SICode>"
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-14"

    Examples:
      | SISection       | SICode     |
      | CompanyData     | 2019100001 |
      | BusinessPartner | 2019100001 |
      | InvoiceItems    | 2019100001 |
      | BusinessPartner | 2019100015 |

  #EBS-5724
  Scenario Outline: (06) Delete SalesInvoice draft by an unauthorized user (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete SalesInvoice with code "<SICode>"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    Examples:
      | SICode     |
      | 2019100001 |
      | 2019100015 |

  Scenario: (05) Delete SalesInvoice by an unauthorized user due to unmatched condition  (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    When "Mahmoud.Abdelaziz" requests to delete SalesInvoice with code "2019100003"
    Then "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
