# Author Dev: Mohamed Aboelnour
# Author Quality: Eslam Salam, Khadrah Ali
# Reviewer: Somaya Ahmed (??-Aug-2020)
Feature: SalesInvoice Activate (Val)

  Background:
    Given the following users and roles exist:
      | Name              | Role                  |
      | Mahmoud.Abdelaziz | Storekeeper_Signmedia |
      | hr1               | SuperUser             |
    And the following roles and sub-roles exist:
      | Role                  | Subrole                         |
      | Storekeeper_Signmedia | SalesInvoicePostOwner_Signmedia |
      | SuperUser             | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission            | Condition                      |
      | SalesInvoicePostOwner_Signmedia | SalesInvoice:Activate | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole                | *:*                   |                                |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Invoiced | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000005 | 20000                  | 27200                 | 0         |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

  Scenario: (01) Validate SalesInvoice Activate with an invalid state - Active State (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2021000002 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_WITHOUT_REFERENCE" with code "2021000002" at "02-Jan-2021 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 12-Jan-2021 09:30 AM |
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-32"

  Scenario: (02) Validate SalesInvoice Activate that have Sales Order with an invalid state - Sales Invoice Activated State (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate     |
      | 04-Jun-2021 02:20 PM |
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-53"

  Scenario: (03) Validate SalesInvoice Activate when one of the sections is locked (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    And another user is logged in as "hr1"
    And first "hr1" opens "BusinessPartner" section of SalesInvoice with code "2021000002" in edit mode
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate     |
      | 04-Jun-2021 02:20 PM |
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-14"

  Scenario: (04)  Validate SalesInvoice Activate with malicious input  (Abuse Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate |
      | MALICIOUS-INPUT  |
    And "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page

  Scenario: (05) Validate SalesInvoice Activate where Sales Invoice doesn't exist (Exception Case)
    Given user is logged in as "Mahmoud.Abdelaziz"
    And another user is logged in as "hr1"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    And "hr1" first deleted the SalesInvoice with code "2021000002" successfully
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate     |
      | 04-Jun-2021 02:20 PM |
    Then an error notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-01"

  Scenario: (06) Validate SalesInvoice Activate with missing mandatory fields
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 |             |          |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate     |
      | 04-Jun-2021 02:20 PM |
    Then a failure notification is sent to "Mahmoud.Abdelaziz" with the following message "Gen-msg-40"
    And the following fields "currencyIso,paymentTermCode" which sent to "Mahmoud.Abdelaziz" are marked as missing

  Scenario: (07) Validate SalesInvoice Activate with journalEntryDate in not active fiscal year
    Given user is logged in as "Mahmoud.Abdelaziz"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2021000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2021000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2021000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2021000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2021000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2021000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2021000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2021000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2021000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2021000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000002 | 27200     |
    When "Mahmoud.Abdelaziz" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2021000002" at "03-Jun-2021 02:20 PM" with the following values:
      | JournalEntryDate     |
      | 04-Jun-2022 02:20 PM |
    And "Mahmoud.Abdelaziz" is logged out
    And "Mahmoud.Abdelaziz" is forwarded to the error page
