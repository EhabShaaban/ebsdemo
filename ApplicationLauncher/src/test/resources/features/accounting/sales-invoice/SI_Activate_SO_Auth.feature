# Author Dev: Evraam Hany
# Author Quality: Khadrah Ali
# Reviewer: Somaya Ahmed (?-Aug-2020)
Feature: Activate SalesInvoice Based On Sales Order (Auth)

  Background:
    Given the following users and roles exist:
      | Name       | Role               |
      | Ali.Hasan  | Storekeeper_Flexo  |
      | Ahmed.Seif | Quality_Specialist |
    And the following roles and sub-roles exist:
      | Role               | Subrole                     |
      | Storekeeper_Flexo  | SalesInvoicePostOwner_Flexo |
      | Quality_Specialist | SalesInvoiceViewer          |
    And the following sub-roles and permissions exist:
      | Subrole                     | Permission            | Condition                  |
      | SalesInvoicePostOwner_Flexo | SalesInvoice:Activate | [purchaseUnitName='Flexo'] |
      | SalesInvoiceViewer          | SalesInvoice:ReadAll  |                            |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000005 | 20000                  | 27200                 | 27200     |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 02-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State | CreationDate         |
      | 2020000002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Draft | 02-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2020000002 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2020000002 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                                | OrderUnit   | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit        |
      | 2020000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10             | 2000            | 0003 - Kilogram |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2020000002 | 0007 - Vat                                   | 1.00          | 200       |
      | 2020000002 | 0008 - Commercial and industrial profits tax | 0.5           | 100       |
      | 2020000002 | 0009 - Real estate taxes                     | 10.00         | 2000      |
      | 2020000002 | 0010 - Service tax                           | 12.00         | 2400      |
      | 2020000002 | 0011 - Adding tax                            | 0.5           | 100       |
      | 2020000002 | 0012 - Sales tax                             | 12.00         | 2400      |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2020000002 | 27200     |

  # EBS-6793
  Scenario: (01) Activate SalesInvoice Auth for Sales Order by an unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Seif"
    When "Ahmed.Seif" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2020000002" at "07-Jan-2019 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 06-Jan-2019 12:00 AM |
    Then "Ahmed.Seif" is logged out
    And "Ahmed.Seif" is forwarded to the error page

  # EBS-6793
  Scenario: (02) Activate SalesInvoice Auth for Sales Order by an unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ali.Hasan"
    When "Ali.Hasan" activates SalesInvoice of type "SALES_INVOICE_FOR_SALES_ORDER" with code "2020000002" at "07-Jan-2019 09:30 AM" with the following values:
      | JournalEntryDate     |
      | 06-Jan-2019 12:00 AM |
    Then "Ali.Hasan" is logged out
    And "Ali.Hasan" is forwarded to the error page
