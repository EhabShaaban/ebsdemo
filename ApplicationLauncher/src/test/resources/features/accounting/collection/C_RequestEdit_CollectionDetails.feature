# Author: Niveen Magdy (EBS-5484)
# Reviewer: Hossam Hassan, Ahmad Hamed (22-11-2020)
Feature: Collection - Request to edit and cancel CollectionDetails section

  Background:
    Given the following users and roles exist:
      | Name                  | Role                                       |
      | Shady.Abdelatif       | Accountant_Signmedia                       |
      | Ahmed.Hamdi.NoCompany | Accountant_Signmedia_CannotViewCompanyRole |
      | Ahmed.Hamdi           | AdminTreasury                              |
      | hr1                   | SuperUser                                  |

    And the following roles and sub-roles exist:
      | Role                                       | Sub-role                   |
      | Accountant_Signmedia                       | CollectionOwner_Signmedia  |
      | Accountant_Signmedia                       | CompanyViewer              |
      | AdminTreasury                              | CollectionOwner            |
      | AdminTreasury                              | CompanyViewer              |
      | SuperUser                                  | SuperUserSubRole           |
      | Accountant_Signmedia_CannotViewCompanyRole | CollectionOwner_Signmedia  |
      | Accountant_Signmedia_CannotViewCompanyRole | CollectionViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                         | Condition                      |
      | CollectionOwner_Signmedia  | Collection:UpdateCollectionDetails | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadAll                 | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadAccountingDetails   | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadActivationDetails   | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadCollectionDetails   | [purchaseUnitName='Signmedia'] |
      | CollectionOwner            | Collection:UpdateCollectionDetails |                                |
      | CompanyViewer              | Company:ReadAll                    |                                |
      | CompanyViewer              | Company:ReadBankDetails            |                                |
      | SuperUserSubRole           | *:*                                |                                |

    And the following users doesn't have the following permissions:
      | User                  | Permission                         |
      | Afaf                  | Collection:UpdateCollectionDetails |
      | Ahmed.Hamdi.NoCompany | Company:ReadAll                    |
      | Ahmed.Hamdi.NoCompany | Company:ReadBankDetails            |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Collection:UpdateCollectionDetails | [purchaseUnitName='Flexo'] |

    And the Company "0001 - AL Madina" has the following Treasuries:
      | Code | Name               |
      | 0001 | Flexo Treasury     |
      | 0002 | Signmedia Treasury |
      | 0003 | Offset Treasury    |
      | 0004 | Digital Treasury   |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |

    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank                                     | AccountNumber        |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893458 - EGP  |
      | 15 | 0001 - AL Madina | 0002 - National Bank of Egypt            | 1011121314678 - USD  |
      | 21 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893422 - EGP  |
      | 22 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893499 - USD  |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819789 - EGP  |
      | 18 | 0002 - DigiPro   | 0004 - CTBC Bank CO., LTD.               | 2021222324253 - USD  |
      | 19 | 0002 - DigiPro   | 0005 - Qatar National Bank Al Ahli-Tanta | 26272829303132 - EGP |
      | 20 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819712 - USD  |

    And the following Collections exist:
      | Code       | State  | BusinessUnit     |
      | 2020000006 | Active | 0001 - Flexo     |
      | 2020000005 | Active | 0002 - Signmedia |
      | 2020000004 | Draft  | 0001 - Flexo     |
      | 2020000001 | Draft  | 0002 - Signmedia |
      | 2019000060 | Draft  | 0002 - Signmedia |
      | 2019000003 | Draft  | 0001 - Flexo     |
      | 2019000002 | Active | 0002 - Signmedia |
      | 2019000005 | Draft  | 0002 - Signmedia |
      | 2019000061 | Draft  | 0002 - Signmedia |

    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner                | Currency | Amount      | Method |
     #CASH Collections
      | 2020000001 | SALES_ORDER           | 2020000053            | 0002 - Signmedia Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 20.0        | CASH   |
      | 2019000005 | NOTES_RECEIVABLE      | 2019000002            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 3000.0      | CASH   |
      | 2019000003 | SALES_INVOICE         | 2019100006            | 0001 - Flexo Treasury     |                                 | 000002 - Client2               | EGP      | 3000.0      | CASH   |
     #BANK Collections
      | 2020000004 | SALES_ORDER           | 2020000003            |                           | 1234567893458 - EGP - Bank Misr | 000006 - المطبعة الأمنية       | EGP      | 20.0        | BANK   |
      | 2019000061 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20.0        | BANK   |
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram              | EGP      | 20.0        | BANK   |
    #Active collection
      | 2020000006 | SALES_ORDER           | 2020000003            | 0001 - Flexo Treasury     |                                 | 000006 - المطبعة الأمنية       | EGP      | 300.0       | CASH   |
      | 2020000005 | NOTES_RECEIVABLE      | 2019000002            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20.0        | BANK   |
      | 2019000002 | SALES_INVOICE         | 2019100002            | 0002 - Signmedia Treasury |                                 | 000002 - Client2               | EGP      | 2000.123456 | CASH   |

    And edit session is "30" minutes

  ### Request to Edit Collection Details section ###
  #EBS-5484
  Scenario Outline: (01) Request to edit Collection details section by an authorized user with one role in case of BANK Method (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>"
    Then CollectionDetails section of Collection with code "<CCode>" becomes in edit mode and locked by "Ahmed.Hamdi"
    And the following authorized reads are returned to "Ahmed.Hamdi":
      | AuthorizedReads        |
      | ReadCompanyBankDetails |
    And there are no mandatory fields returned to "Ahmed.Hamdi"
    And the following editable fields are returned to "Ahmed.Hamdi":
      | EditableFields    |
      | amount            |
      | bankAccountNumber |
    Examples:
      | CCode      |
      | 2019000060 |
      | 2019000061 |
      | 2020000004 |

  #EBS-5484
  Scenario Outline: (02) Request to edit Collection details section by an authorized user with one role in case of CASH Method (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>"
    Then CollectionDetails section of Collection with code "<CCode>" becomes in edit mode and locked by "Ahmed.Hamdi"
    And the following authorized reads are returned to "Ahmed.Hamdi":
      | AuthorizedReads |
      | ReadTreasuries  |
    And there are no mandatory fields returned to "Ahmed.Hamdi"
    And the following editable fields are returned to "Ahmed.Hamdi":
      | EditableFields |
      | amount         |
      | treasuryCode   |
    Examples:
      | CCode      |
      | 2019000003 |
      | 2019000005 |
      | 2020000001 |

  #EBS-5484
  Scenario: (05) Request to edit Collection details section that is locked by another user (Exception)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "hr1" first opened the Collection with code "2020000004" in the edit mode successfully
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "2020000004"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-02"

  #EBS-5484
  Scenario: (06) Request to edit Collection details section of deleted Collection (Exception)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2020000001" successfully
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "2020000001"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"

  #EBS-5484
  Scenario Outline: (07) Request to edit Collection details section when it is not allowed in current state (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to edit CollectionDetails section of Collection with code "<CCode>" at "07-Jan-2020 09:35 AM"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And CollectionDetails section of Collection with code "<CCode>" is not locked by "hr1"
    Examples:
      | CCode      |
      | 2020000006 |
      | 2019000002 |
      | 2020000005 |

  #EBS-5484
  Scenario Outline: (08) Request to edit Collection details section with unauthorized user (with condition - without condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "<User>"
    When "<User>" requests to edit CollectionDetails section of Collection with code "2019000003"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User            |
      | Afaf            |
      | Shady.Abdelatif |

  #EBS-5484
  Scenario Outline: (09) Request to edit Collection details section in case of BANK Method (No authorized reads)
    Given user is logged in as "Ahmed.Hamdi.NoCompany"
    When "Ahmed.Hamdi.NoCompany" requests to edit CollectionDetails section of Collection with code "<CCode>"
    Then CollectionDetails section of Collection with code "<CCode>" becomes in edit mode and locked by "Ahmed.Hamdi.NoCompany"
    And there are no authorized reads returned to "Ahmed.Hamdi.NoCompany"
    And there are no mandatory fields returned to "Ahmed.Hamdi.NoCompany"
    And the following editable fields are returned to "Ahmed.Hamdi.NoCompany":
      | EditableFields    |
      | amount            |
      | bankAccountNumber |
    Examples:
      | CCode      |
      | 2019000060 |
      | 2019000061 |

  #EBS-5484
  Scenario Outline: (10) Request to edit Collection details section in case of CASH Method (No authorized reads)
    Given user is logged in as "Ahmed.Hamdi.NoCompany"
    When "Ahmed.Hamdi.NoCompany" requests to edit CollectionDetails section of Collection with code "<CCode>"
    Then CollectionDetails section of Collection with code "<CCode>" becomes in edit mode and locked by "Ahmed.Hamdi.NoCompany"
    And there are no authorized reads returned to "Ahmed.Hamdi.NoCompany"
    And there are no mandatory fields returned to "Ahmed.Hamdi.NoCompany"
    And the following editable fields are returned to "Ahmed.Hamdi.NoCompany":
      | EditableFields |
      | amount         |
      | treasuryCode   |
    Examples:
      | CCode      |
      | 2019000005 |
      | 2020000001 |

  #Request to Cancel Collection Details section ###
  #EBS-5484
  Scenario Outline: (11) Request to Cancel saving Collection details section by an authorized user with one role (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And CollectionDetails section of Collection with code "<CCode>" is locked by "Ahmed.Hamdi" at "07-Jan-2020 09:10 AM"
    When "Ahmed.Hamdi" cancels saving CollectionDetails section of Collection with code "<CCode>" at "07-Jan-2020 09:30 AM"
    Then the lock by "Ahmed.Hamdi" on CollectionDetails section of Collection with code "<CCode>" is released
    Examples:
      | CCode      |
     #CASH
      | 2019000003 |
      | 2019000005 |
      | 2020000001 |
    #BANK
      | 2019000060 |
      | 2019000061 |
      | 2020000004 |

  #EBS-5484
  Scenario Outline: (12) Request to Cancel saving Collection details section after lock session is expire
    Given user is logged in as "Ahmed.Hamdi"
    And CollectionDetails section of Collection with code "<CCode>" is locked by "Ahmed.Hamdi" at "07-Jan-2020 09:00 AM"
    When "Ahmed.Hamdi" cancels saving CollectionDetails section of Collection with code "<CCode>" at "07-Jan-2020 09:31 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-07"
    Examples:
      | CCode      |
     #CASH
      | 2019000003 |
      | 2019000005 |
      | 2020000001 |
    #BANK
      | 2019000060 |
      | 2019000061 |
      | 2020000004 |

  #EBS-5484
  Scenario Outline: (13) Request to Cancel saving Collection details section after lock session is expire and Collection doesn't exist (Exception)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And CollectionDetails section of Collection with code "<CCode>" is locked by "Ahmed.Hamdi" at "07-Jan-2020 09:00 AM"
    And "hr1" first deleted the Collection with code "<CCode>" successfully at "07-Jan-2020 09:31 AM"
    When "Ahmed.Hamdi" cancels saving CollectionDetails section of Collection with code "<CCode>" at "07-Jan-2020 09:35 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"
    Examples:
      | CCode      |
     #CASH
      | 2019000003 |
      | 2019000005 |
      | 2020000001 |
    #BANK
      | 2019000060 |
      | 2019000061 |
      | 2020000004 |

  #EBS-5484
  Scenario Outline: (14) Request to Cancel saving Collection details section with unauthorized user (with condition - without condition (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" cancels saving CollectionDetails section of Collection with code "2019000003"
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page

    Examples:
      | User            |
      | Afaf            |
      | Shady.Abdelatif |
