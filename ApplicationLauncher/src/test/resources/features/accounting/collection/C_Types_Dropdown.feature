#Author: Niveen Magdy
#Reviewer: Hossam Hassan
Feature:View all Collection Types DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role             |
      | Accountant_Signmedia | CollectionTypeViewer |

    And the following sub-roles and permissions exist:
      | Subrole              | Permission             | Condition |
      | CollectionTypeViewer | CollectionType:ReadAll |           |

    And the following users doesn't have the following permissions:
      | User | Permission             |
      | Afaf | CollectionType:ReadAll |
    And the following CollectionTypes exist:
      | Code             | Name                        |
      | SALES_INVOICE    | Against Sales Invoice       |
      | NOTES_RECEIVABLE | Against Notes Receivable    |
      | SALES_ORDER      | DownPayment for Sales Order |
      | DEBIT_NOTE       | Against Debit Note          |
      | PAYMENT_REQUEST  | Against Payment Request     |
    And the total number of existing Collection Types are 5

    #EBS-7467
  Scenario: (01) Read list of CollectionTypes dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all CollectionTypes
    Then the following Collection Types values will be presented to "Shady.Abdelatif":
      | CollectionType                              |
      | SALES_INVOICE - Against Sales Invoice       |
      | NOTES_RECEIVABLE - Against Notes Receivable |
      | SALES_ORDER - DownPayment for Sales Order   |
      | DEBIT_NOTE - Against Debit Note             |
      | PAYMENT_REQUEST - Against Payment Request   |
    And total number of Collection Types returned to "Shady.Abdelatif" is equal to 5

   #EBS-7467
  Scenario: (02) Read list of CollectionTypes dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all CollectionTypes
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
