#Author: Niveen Magdy
# updated by: Fatma Al Zahraa (EBS - 8759)
#Reviewer: Hossam Hassan
Feature: Notes Receivable DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |
    And the following NotesReceivables exist:
      | Code       | State  | BusinessUnit     |
      | 2019000054 | Active | 0002 - Signmedia |
      | 2019000053 | Draft  | 0002 - Signmedia |
      | 2019000051 | Active | 0002 - Signmedia |
      | 2019000050 | Draft  | 0002 - Signmedia |
      | 2019000005 | Draft  | 0002 - Signmedia |
      | 2019000004 | Active | 0002 - Signmedia |
      | 2019000003 | Draft  | 0001 - Flexo     |
      | 2019000002 | Active | 0002 - Signmedia |
      | 2019000001 | Draft  | 0002 - Signmedia |

  #EBS-7467
  #EBS-8759
  Scenario: (01) Read All NotesReceivables Dropdown in Collection creation by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all NotesReceivables with state Active according to user's BusinessUnit
    Then the following NotesReceivables for Collections values will be presented to "Shady.Abdelatif":
      | Code       |
      | 2019000054 |
      | 2019000051 |
      | 2019000004 |
      | 2019000002 |
    And total number of NotesReceivables for Collections returned to "Shady.Abdelatif" is equal to 4

  #EBS-7467
  Scenario: (02) Read All NotesReceivables in Collection creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all NotesReceivables with state Active according to user's BusinessUnit
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
