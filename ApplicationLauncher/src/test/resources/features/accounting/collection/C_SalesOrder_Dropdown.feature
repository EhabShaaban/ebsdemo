#Author: Niveen Magdy, Hossam Hassan (#EBS-7432)
#Reviewer: Ahmad Hamed (04-11-2020)
Feature: Sales Order DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole            |
      | Accountant_Signmedia | SOViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole            | Permission         | Condition                      |
      | SOViewer_Signmedia | SalesOrder:ReadAll | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | SalesOrder:ReadAll |

    And the following SalesOrders exist:
      | Code       | State                 | BusinessUnit     |
      | 2020000071 | GoodsIssueActivated   | 0002 - Signmedia |
      | 2020000070 | SalesInvoiceActivated | 0002 - Signmedia |
      | 2020000069 | Approved              | 0002 - Signmedia |
      | 2020000067 | Approved              | 0002 - Signmedia |
      | 2020000066 | Draft                 | 0001 - Flexo     |
      | 2020000065 | Draft                 | 0002 - Signmedia |
      | 2020000064 | Draft                 | 0002 - Signmedia |
      | 2020000056 | DeliveryComplete      | 0002 - Signmedia |
      | 2020000055 | Expired               | 0002 - Signmedia |
      | 2020000054 | Canceled              | 0002 - Signmedia |
      | 2020000053 | Approved              | 0002 - Signmedia |
      | 2020000052 | WaitingApproval       | 0002 - Signmedia |
      | 2020000051 | Rejected              | 0002 - Signmedia |
      | 2020000012 | ReadyForDelivery      | 0002 - Signmedia |
      | 2020000011 | GoodsIssueActivated   | 0002 - Signmedia |
      | 2020000003 | Approved              | 0001 - Flexo     |

    #EBS-7432
    And the following SalesOrders have the following remainings:
      | Code       | TotalRemaining |
      | 2020000071 | 0              |
      | 2020000070 | 10880.000      |
      | 2020000069 | 0              |
      | 2020000067 | 278.8          |
      | 2020000066 | 0              |
      | 2020000065 | 0              |
      | 2020000064 | 0              |
      | 2020000056 | 14858.000      |
      | 2020000055 | 0              |
      | 2020000054 | 0              |
      | 2020000053 | 27.2           |
      | 2020000052 | 0              |
      | 2020000051 | 0              |
      | 2020000012 | 544.000        |
      | 2020000011 | 448.8          |
      | 2020000003 | 22000          |


  #EBS-4805 #EBS-7432
  Scenario: (01) Read All SalesOrders Dropdown in Collection creation by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all SalesOrders with state GoodsIssueActivated or Approved where remaining is greater than Zero according to user's BusinessUnit
    Then the following SalesOrders for Collections values will be presented to "Shady.Abdelatif":
      | Code       |
      | 2020000067 |
      | 2020000053 |
      | 2020000011 |
    And total number of SalesOrders for Collections returned to "Shady.Abdelatif" is equal to 3

  #EBS-4805
  Scenario: (02) Read All SalesOrders in Collection creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesOrders with state GoodsIssueActivated or Approved where remaining is greater than Zero according to user's BusinessUnit
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
