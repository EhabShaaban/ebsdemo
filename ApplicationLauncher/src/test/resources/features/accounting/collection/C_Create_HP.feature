#Author: gehad, Niveen
#Reviewer: marina, Engy, Hossam Hassan
#EBS-7622: 3pts
Feature: Create Collection - Happy Path

  Background:

    Given the following users and roles exist:
      | Name            | Role                            |
      | Shady.Abdelatif | Accountant_Signmedia            |
      | Ahmed.Al-Ashry  | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury                   |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                         |
      | Accountant_Signmedia            | CollectionOwner_Signmedia       |
      | AdminTreasury                   | CollectionOwner                 |
      | Accountant_Signmedia            | DocumentOwnerViewer             |
      | AdminTreasury                   | DocumentOwnerViewer             |
      | Accountant_Signmedia            | CollectionTypeViewer            |
      | AdminTreasury                   | CollectionTypeViewer            |
      | Accountant_Signmedia            | CollectionMethodViewer          |
      | AdminTreasury                   | CollectionMethodViewer          |
      | Accountant_Signmedia            | SalesInvoiceViewer_Signmedia    |
      | AdminTreasury                   | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia            | NotesReceivableViewer_Signmedia |
      | AdminTreasury                   | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia            | SOViewer_Signmedia              |
      | AdminTreasury                   | SOViewer_Signmedia              |
      | CollectionResponsible_Signmedia | CollectionDocumentOwner         |
      | AdminTreasury                   | CollectionDocumentOwner         |
  #ToDo:
    #add credit-debit-noteViewer sub role and assign it to accountant and admin treasury roles

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                    | Condition                      |
      | CollectionOwner_Signmedia       | Collection:Create             |                                |
      | CollectionOwner                 | Collection:Create             |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll         |                                |
      | CollectionTypeViewer            | CollectionType:ReadAll        |                                |
      | CollectionMethodViewer          | CollectionMethod:ReadAll      |                                |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll          | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll       | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll            | [purchaseUnitName='Signmedia'] |
      | CollectionDocumentOwner         | Collection:CanBeDocumentOwner |                                |


    And the following CollectionTypes exist:
      | Code             | Name                        |
      | SALES_INVOICE    | Against Sales Invoice       |
      | NOTES_RECEIVABLE | Against Notes Receivable    |
      | SALES_ORDER      | DownPayment for Sales Order |
      | DEBIT_NOTE       | Against Debit Note          |
      | PAYMENT_REQUEST  | Against Payment Request     |

    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | Collection     | CanBeDocumentOwner |           |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019100007 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2020000009 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |

    And the following NotesReceivables exist:
      | Code       | State  | BusinessUnit     |
      | 2019000054 | Active | 0002 - Signmedia |

    And the following SalesOrders exist:
      | Code       | State               | BusinessUnit     |
      | 2020000053 | Approved            | 0002 - Signmedia |
      | 2020000011 | GoodsIssueActivated | 0002 - Signmedia |

    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                           | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000013 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION | Posted | Admin     | 02-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000013 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2019000013 | 000002 - Zhejiang | 1000   | 0001 - EGP |

    And the following Company and Currency and Customer and RemainingAmount exist in the following SalesInvoices:
      | SICode     | Company          | Currency | Customer                       | RemainingAmount |
      | 2019100007 | 0002 - DigiPro   | EGP      | 000001 - Al Ahram              | 7080.25         |
      | 2020000009 | 0001 - AL Madina | EGP      | 000007 - مطبعة أكتوبر الهندسية | 10880           |

    And the following Company and Currency and Customer and RemainingAmount exist in the following NotesReceivables:
      | NRCode     | Company          | Currency | Customer          | RemainingAmount |
      | 2019000054 | 0001 - AL Madina | EGP      | 000001 - Al Ahram | 333.0           |
#
    And the following Company and Currency and Customer exist in the following SalesOrder:
      | SOCode     | Company          | Currency | Customer                       |
      | 2020000053 | 0001 - AL Madina | EGP      | 000007 - مطبعة أكتوبر الهندسية |
      | 2020000011 | 0001 - AL Madina | EGP      | 000007 - مطبعة أكتوبر الهندسية |

    #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType      | State       | CreatedBy  | CreationDate         | DocumentOwner |
      | 2021000004 | UNDER_SETTLEMENT | PaymentDone | Amr.Khalil | 07-Mar-2021 09:02 AM | Ashraf Salah  |
    #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner      | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury                  | Description |
      | 2021000004 | CASH        | 000002 - Ahmed Hamdi |                 |             | 1000.00   | EGP         |                |                   | 0002 - Signmedia Treasury |             |

  # EBS-5863 , EBS-5988 and EBS-4805
  Scenario Outline: (01) Create Collection CASH, against Sales Invoice for both types(Sales invoice without reference,Sales invoice for sales order), with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType | RefDocument   | DocumentOwnerId |
      | CASH   | SALES_INVOICE  | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | SALES_INVOICE  | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company   | BusinessUnit     |
      | <Company> | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BusinessPartner   | RefDocument   |
      | CASH   |        | EGP      | <BusinessPartner> | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | RefDocument | Company          | BusinessPartner                |
      | 2019100007  | 0002 - DigiPro   | 000001 - Al Ahram              |
      | 2020000009  | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |

     # EBS-5964 , EBS-5878 and EBS-4805
  Scenario: (02) Create Collection CASH, against NotesReceivable, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType   | RefDocument | DocumentOwnerId |
      | CASH   | NOTES_RECEIVABLE | 2019000054  | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType   | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | NOTES_RECEIVABLE | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BusinessPartner   | RefDocument |
      | CASH   |        | EGP      | 000001 - Al Ahram | 2019000054  |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"

  #EBS-4805
  Scenario Outline: (03) Create Collection CASH, DownPayment for Sales Order , with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType | RefDocument   | DocumentOwnerId |
      | CASH   | SALES_ORDER    | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | SALES_ORDER    | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BusinessPartner                | RefDocument   |
      | CASH   |        | EGP      | 000007 - مطبعة أكتوبر الهندسية | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | RefDocument |
      | 2020000053  |
      | 2020000011  |
        #EBS-6185 and EBS-4805
  Scenario Outline: (04) Create Collection BANK TRANSFER, against Sales Invoice for both types(Sales invoice without reference,Sales invoice for sales order), with all mandatory fields by an authorized user(Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType | RefDocument   | DocumentOwnerId |
      | BANK   | SALES_INVOICE  | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | SALES_INVOICE  | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company   | BusinessUnit     |
      | <Company> | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BusinessPartner   | RefDocument   |
      | BANK   |        | EGP      | <BusinessPartner> | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | RefDocument | Company          | BusinessPartner                |
      | 2019100007  | 0002 - DigiPro   | 000001 - Al Ahram              |
      | 2020000009  | 0001 - AL Madina | 000007 - مطبعة أكتوبر الهندسية |

       # EBS-6185 and EBS-4805
  Scenario: (05) Create Collection BANK TRANSFER, against NotesReceivable, with all mandatory fields by an authorized user  (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType   | RefDocument | DocumentOwnerId |
      | BANK   | NOTES_RECEIVABLE | 2019000054  | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType   | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | NOTES_RECEIVABLE | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BankAccount | BusinessPartner   | RefDocument |
      | BANK   |        | EGP      |             | 000001 - Al Ahram | 2019000054  |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"

  #EBS-4805
  Scenario Outline: (06) Create Collection BANK, DownPayment for Sales Order, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType | RefDocument   | DocumentOwnerId |
      | BANK   | SALES_ORDER    | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | SALES_ORDER    | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method | Amount | Currency | BusinessPartner                | RefDocument   |
      | BANK   |        | EGP      | 000007 - مطبعة أكتوبر الهندسية | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | RefDocument |
      | 2020000053  |
      | 2020000011  |

  #EBS-7622
  Scenario Outline: (07) Create Collection for Debit Note, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2020 11:00 AM"
    And Last created Collection was with code "2020000015"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method   | CollectionType | RefDocument   | DocumentOwnerId |
      | <Method> | DEBIT_NOTE     | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType | DocumentOwner  | State |
      | 2020000016 | 01-Jan-2020 11:00 AM | Shady.Abdelatif | DEBIT_NOTE     | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2020000016" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2020000016" is updated as follows:
      | Method   | Amount | Currency | BusinessPartner   | RefDocument   |
      | <Method> | 1000   | EGP      | 000002 - Zhejiang | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | Method | RefDocument |
      | CASH   | 2019000013  |
      | BANK   | 2019000013  |

  #EBS-7951
  Scenario Outline: (08) Create Collection for Payment under settlement, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "07-Mar-2021 11:00 AM"
    And Last created Collection was with code "2021000000"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method   | CollectionType  | RefDocument   | DocumentOwnerId |
      | <Method> | PAYMENT_REQUEST | <RefDocument> | 1000072         |
    Then a new Collection is created as follows:
      | Code       | CreationDate         | CreatedBy       | CollectionType  | DocumentOwner  | State |
      | 2021000001 | 07-Mar-2021 11:00 AM | Shady.Abdelatif | PAYMENT_REQUEST | Ahmed Al-Ashry | Draft |
    And the CompanyDetails Section of Collection with code "2021000001" is updated as follows:
      | Company          | BusinessUnit     |
      | 0001 - AL Madina | 0002 - Signmedia |
    And the CollectionDetails Section of Collection with code "2021000001" is updated as follows:
      | Method   | Amount | Currency | BusinessPartner      | RefDocument   |
      | <Method> |        | EGP      | 000002 - Ahmed Hamdi | <RefDocument> |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | Method | RefDocument |
      | CASH   | 2021000004  |
