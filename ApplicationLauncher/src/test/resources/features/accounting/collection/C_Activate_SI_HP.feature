# Author: Ahmed Ali, Khadrah ALi
# Reviewer:
Feature: Collection Based on Sales Invoice - Activate HP

  Activating Collection Based on Sales Invoice results in the following:
  - Collection state is changed to Active
  - Collection Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document
  ---- GL record for realized exchange rate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Collection Currency is same as Sales Invoice currency, but different from the Company Local Currency (Future)
  -------- when Collection Currency is different from both Sales Invoice currency and Company Local Currency
  - Journal Entry for Collection is created:
  ---- The retrieved fiscal year is the active one where the due date belongs to it
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations)
  - Collection Activation Details are determined
  - The referenced Sales Invoice remaining is updated
  - Related JournalBalances are increased according to Collection amount (EBS-8510)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Feb-2021 09:00 AM | User Daily Rate       |
      | 2020000036 | EGP              | EGP               | 1           | 01-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 01-Mar-2021 09:00 AM | Central Bank Of Egypt |
      | 2020000038 | USD              | EGP               | 15.65       | 02-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000039 | EGP              | EGP               | 1           | 02-Mar-2021 09:00 AM | Central Bank Of Egypt |

  #EBS-7345
  Scenario: (01) Activate Collections, against SalesInvoice without reference, where collection method is Cash and DocumentCurrency = CompanyLocalCurrency, by an authorized user, all mandatory fields exist, Amount in Collection < Remaining in SalesInvoice (Happy Path)
  """
    - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date
  """
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000  |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000013 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 | 7000      |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount      | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2019000013        | 4700.266345 | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2021000001" and DueDate "06-Mar-2022 09:30 AM" at "02-Mar-2021 09:30 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 02-Mar-2021 09:30 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account           | SubAccount                | Value           |
      | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 4700.266345 EGP |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 4700.266345 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 02-Mar-2021 09:30 AM | 06-Mar-2022 09:30 AM | 1             | 2021000001       | 2022         |
    And Remaining of SalesInvoice with code "2019000013" is updated as follows "2299.733655"
    And State of SalesInvoice with code "2019000013" remains "Active"
    And JournalBalance with code "TREASURY0002000100010002" is updated as follows "904700.266345"
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 02-Mar-2021 09:30 AM | Ahmed.Hamdi | 06-Mar-2022 09:30 AM | 0002 - Signmedia | 2022       | 2021000001 - Collection | 0001 - AL Madina |
    And And the following new JournalEntryDetails are created for JournalEntry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                | Credit      | Debit       | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Mar-2022 09:30 AM | 10430 - Cash      | 0002 - Signmedia Treasury | 0           | 4700.266345 | EGP                | EGP             | 1                         | 2020000036       |
      | 06-Mar-2022 09:30 AM | 10203 - Customers | 000001 - Al Ahram         | 4700.266345 | 0           | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-7345
  Scenario: (02) Activate Collections, against SalesInvoice, where collection method is Bank and DocumentCurrency not equals CompanyLocalCurrency and Amount in Collection < Remaining in SalesInvoice (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00010002000200031516171819781 | 0001 - Flexo | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 40000   |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit |
      | 2019000013 | 0002 - DigiPro | 0001 - Flexo   |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | USD      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 | 7000      |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit | Company        |
      | 2021000001 | 0001 - Flexo | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury |
      | 2021000001 | SALES_INVOICE | BANK   | 000001 - Al Ahram | 2019000013        | 1000   | USD      | 1516171819781 - USD - Alex Bank |          |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2021000001" and DueDate "03-Mar-2021 11:56 AM" at "04-Mar-2021 11:56 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 04-Mar-2021 11:56 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account           | SubAccount                      | Value    |
      | DEBIT        | 10431 - Bank      | 1516171819781 - USD - Alex Bank | 1000 USD |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram               | 1000 USD |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 04-Mar-2021 11:56 AM | 03-Mar-2021 11:56 AM | 15.65         | 2021000001       | 2021         |
    And Remaining of SalesInvoice with code "2019000013" is updated as follows "6000"
    And State of SalesInvoice with code "2019000013" remains "Active"
    And JournalBalance with code "BANK00010002000200031516171819781" is updated as follows "41000"
    And the following Journal Entry is Created:
      | Code       | CreationDate        | CreatedBy   | JournalDate          | BusinessUnit | FiscalYear | ReferenceDocument       | Company        |
      | 2021000001 | 4-Mar-2021 11:56 AM | Ahmed.Hamdi | 03-Mar-2021 11:56 AM | 0001 - Flexo | 2021       | 2021000001 - Collection | 0002 - DigiPro |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                      | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 03-Mar-2021 11:56 AM | 10431 - Bank      | 1516171819781 - USD - Alex Bank | 0      | 1000  | USD                | EGP             | 15.65                     | 2020000038       |
      | 03-Mar-2021 11:56 AM | 10203 - Customers | 000001 - Al Ahram               | 1000   | 0     | USD                | EGP             | 15.65                     | 2020000038       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-7345
  Scenario: (03) Activate Collections, against SalesInvoice, where collection method is Cash and DocumentCurrency = CompanyLocalCurrency, by an authorized user, all mandatory fields exist, Amount in Collection = Remaining in SalesInvoice (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000  |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000013 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 | 5000      |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2019000013        | 5000   | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2021000001" and DueDate "06-Mar-2021 09:30 AM" at "07-Mar-2021 09:30 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 07-Mar-2021 09:30 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account           | SubAccount                | Value    |
      | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 5000 EGP |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 5000 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 07-Mar-2021 09:30 AM | 06-Mar-2021 09:30 AM | 1             | 2021000001       | 2021         |
    And Remaining of SalesInvoice with code "2019000013" is updated as follows "0"
    And State of SalesInvoice with code "2019000013" becomes "Closed"
    And JournalBalance with code "TREASURY0002000100010002" is updated as follows "905000"
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 07-Mar-2021 09:30 AM | Ahmed.Hamdi | 06-Mar-2021 09:30 AM | 0002 - Signmedia | 2021       | 2021000001 - Collection | 0001 - AL Madina |
    And And the following new JournalEntryDetails are created for JournalEntry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 06-Mar-2021 09:30 AM | 10430 - Cash      | 0002 - Signmedia Treasury | 0      | 5000  | EGP                | EGP             | 1                         | 2020000036       |
      | 06-Mar-2021 09:30 AM | 10203 - Customers | 000001 - Al Ahram         | 5000   | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-8949
  Scenario: (04) Activate Collections, against SalesInvoice (Happy Path)
  """
    - Activate Collection based on SalesInvoiceForSalesOrder
    - There is already Activate Collection(Bank) on the SalesInvoice 
    - Collection method is Bank
    - DocumentCurrency = CompanyLocalCurrency (EGP)
    - Collection Amount = SalesInvoice Remaining 
    """

    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 1000000 |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2021000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 10-Jun-2021 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2021000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2021000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Jun-2021 11:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2021000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2021000005 | 0007 - Vat                                   | 1             |
      | 2021000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2021000005 | 0009 - Real estate taxes                     | 10            |
      | 2021000005 | 0010 - Service tax                           | 12            |
      | 2021000005 | 0011 - Adding tax                            | 0.5           |
      | 2021000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2021000005 | 20000                  | 27200                 | 27200     |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2021000010 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 10-Jun-2021 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2021000010 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                  | Currency | DownPayment |
      | 2021000010 | 000001 - Al Ahram | 2021000005 | 0003 - 100% Advanced Payment | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2021000010 | 7200      |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate        | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Active | Admin     | 11-Jun-2021 8:02 AM | CUSTOMER            | Gehan Ahmed   |
      | 2021000002 | C_SI | Draft  | Admin     | 11-Jun-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury |
      | 2021000001 | SALES_INVOICE | BANK   | 000001 - Al Ahram | 2021000010        | 20000  | EGP      | 1234567893458 - EGP - Bank Misr |          |
      | 2021000002 | SALES_INVOICE | BANK   | 000001 - Al Ahram | 2021000010        | 7200   | EGP      | 1234567893458 - EGP - Bank Misr |          |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2021000002" and DueDate "15-Jun-2021 11:56 AM" at "11-Jun-2021 11:56 AM"
    Then Collection with Code "2021000002" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 11-Jun-2021 11:56 AM | Active |
    And AccountingDetails in Collection with Code "2021000002" is updated as follows:
      | Credit/Debit | Account           | SubAccount                      | Value    |
      | DEBIT        | 10431 - Bank      | 1234567893458 - EGP - Bank Misr | 7200 EGP |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram               | 7200 EGP |
    And ActivationDetails in Collection with Code "2021000002" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 11-Jun-2021 11:56 AM | 15-Jun-2021 11:56 AM | 1             | 2021000001       | 2021         |
    And Remaining of SalesInvoice with code "2021000010" is updated as follows "0"
    And State of SalesInvoice with code "2021000010" becomes "Closed"
    And JournalBalance with code "BANK00020001000100011234567893458" is updated as follows "1007200"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 11-Jun-2021 11:56 AM | Ahmed.Hamdi | 15-Jun-2021 11:56 AM | 0002 - Signmedia | 2021       | 2021000002 - Collection | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                      | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 15-Jun-2021 11:56 AM | 10431 - Bank      | 1234567893458 - EGP - Bank Misr | 0      | 7200  | EGP                | EGP             | 1                         | 2020000036       |
      | 15-Jun-2021 11:56 AM | 10203 - Customers | 000001 - Al Ahram               | 7200   | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
