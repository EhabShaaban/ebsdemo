#Author: Ahmad Hamed, Engy Mohamed

Feature: Delete Collection

  Background:
    Given the following users and roles exist:
      | Name           | Role                            |
      | Ahmed.Al-Ashry | CollectionResponsible_Signmedia |
      | hr1            | SuperUser                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                   |
      | CollectionResponsible_Signmedia | CollectionOwner_Signmedia |
      | SuperUser                       | SuperUserSubRole          |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission        | Condition                      |
      | CollectionOwner_Signmedia | Collection:Delete | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole          | *:*               |                                |

    And the following users have the following permissions without the following conditions:
      | User           | Permission        | Condition                  |
      | Ahmed.Al-Ashry | Collection:Delete | [purchaseUnitName='Flexo'] |

    And the following Collections exist:
      | Code       | State  | BusinessUnit     |
#      Based On SI
      | 2019000003 | Draft  | 0001 - Flexo     |
      | 2019000050 | Active | 0002 - Signmedia |
      | 2019000060 | Draft  | 0002 - Signmedia |
#      Based On NR
      | 2019000052 | Draft  | 0001 - Flexo     |
      | 2019000061 | Draft  | 0002 - Signmedia |
#      Based On SO
      | 2020000001 | Draft  | 0002 - Signmedia |
      | 2020000003 | Active | 0002 - Signmedia |
      | 2020000004 | Draft  | 0001 - Flexo     |

  #EBS-1411
  Scenario Outline: (01) Delete Collection - draft by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete Collection with code "<CCode>"
    Then Collection with code "<CCode>" is deleted from the system
    And a success notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-12"
    Examples:
      | CCode      |
      | 2019000060 |
      | 2019000061 |
      | 2020000001 |

  #EBS-1411
  Scenario Outline: (02) Delete Collection - draft where Collection doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "<CCode>" successfully
    When "Ahmed.Al-Ashry" requests to delete Collection with code "<CCode>"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
    Examples:
      | CCode      |
      | 2019000060 |
      | 2019000061 |
      | 2020000001 |

  #EBS-1411
  Scenario Outline: (03) Delete Collection - where action is not allowed per current state (Active) (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete Collection with code "<CCode>"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-13"
    Examples:
      | CCode      |
      | 2019000050 |
      | 2020000003 |

  #EBS-1411
  Scenario Outline: (04) Delete Collection - draft by an unauthorized user (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to delete Collection with code "<CCode>"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page
    Examples:
      | CCode      |
      | 2019000003 |
      | 2019000052 |
      | 2020000004 |

  #EBS-1411
  @Future
  Scenario Outline: (05) Delete Collection - that is locked by another user (Exception Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And first "hr1" opens "<CSection>" section of Collection with code "2020000001" in edit mode
    When "Ahmed.Al-Ashry" requests to delete Collection with code "2020000001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-14"
    Examples:
      | CSection          |
      | CollectionDetails |
