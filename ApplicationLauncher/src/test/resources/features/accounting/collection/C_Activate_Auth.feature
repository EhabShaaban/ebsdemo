#Author: Hossam Hassan (EBS-1082)
#Reviewer: Ahmad Hamed (02-12-2020)
Feature: Collection _ Activate (Auth)

  Background: 
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User            | Permission          |
      | Shady.Abdelatif | Collection:Activate |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg |  10 |       2000 |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   |             1 |
      | 2020000005 | 0008 - Commercial and industrial profits tax |           0.5 |
      | 2020000005 | 0009 - Real estate taxes                     |            10 |
      | 2020000005 | 0010 - Service tax                           |            12 |
      | 2020000005 | 0011 - Adding tax                            |           0.5 |
      | 2020000005 | 0012 - Sales tax                             |            12 |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000005 |                  20000 |                 27200 |     27200 |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100007 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019100007 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100007 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100007 |   7080.25 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2019000002 | C_SO | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000002 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |             | 0002 - Signmedia Treasury |
      | 2019000002 | SALES_ORDER   | CASH   | 000001 - Al Ahram |        2020000005 |   1000 | EGP      |             | 0002 - Signmedia Treasury |

  # EBS-1082
  Scenario Outline: (01) Activate Collections Auth, by an unauthorized user (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" Activates Collection based on "<CType>" with code "<CCode>" and DueDate "06-Nov-2020 09:30 AM" at "07-Nov-2020 09:30 AM"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

    Examples: 
      | CCode      | CType         |
      | 2019000001 | SALES_INVOICE |
      | 2019000002 | SALES_ORDER   |

