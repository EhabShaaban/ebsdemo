# Author: Ahmad Hamed, Hossam Hassan, Engy Mohamed (EBS-7375)
# Reviewer: Niveen Magdy (03-Dec-2020)

Feature: Collection - Save Collection Details section Validation

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
      | hr1         | SuperUser     |

    And the following roles and sub-roles exist:
      | Role          | Subrole          |
      | AdminTreasury | CollectionOwner  |
      | AdminTreasury | CompanyViewer    |
      | SuperUser     | SuperUserSubRole |

    And the following sub-roles and permissions exist:
      | Subrole          | Permission                         | Condition |
      | CollectionOwner  | Collection:UpdateCollectionDetails |           |
      | CompanyViewer    | Company:ReadAll                    |           |
      | SuperUserSubRole | *:*                                |           |

    And the following Collections exist:
      | Code       | State | BusinessUnit     |
      | 2019000001 | Draft | 0002 - Signmedia |
      | 2019000051 | Draft | 0002 - Signmedia |
      | 2019000060 | Draft | 0002 - Signmedia |
      | 2020000001 | Draft | 0002 - Signmedia |
      | 2020000002 | Draft | 0002 - Signmedia |
      | 2020000004 | Draft | 0001 - Flexo     |

    And the following Company Data for Collections exist:
      | CCode      | Company          | BusinessUnit     |
      | 2019000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019000051 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019000060 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2020000001 | 0001 - AL Madina | 0002 - Signmedia |
      | 2020000002 | 0001 - AL Madina | 0002 - Signmedia |
      | 2020000004 | 0001 - AL Madina | 0001 - Flexo     |

    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank                                     | AccountNumber        |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893458 - EGP  |
      | 15 | 0001 - AL Madina | 0002 - National Bank of Egypt            | 1011121314678 - USD  |
      | 21 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893422 - EGP  |
      | 22 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893499 - USD  |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819789 - EGP  |
      | 18 | 0002 - DigiPro   | 0004 - CTBC Bank CO., LTD.               | 2021222324253 - USD  |
      | 19 | 0002 - DigiPro   | 0005 - Qatar National Bank Al Ahli-Tanta | 26272829303132 - EGP |
      | 20 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819712 - USD  |

    And the Company "0001 - AL Madina" has the following Treasuries:
      | Code | Name               |
      | 0001 | Flexo Treasury     |
      | 0002 | Signmedia Treasury |
      | 0003 | Offset Treasury    |
      | 0004 | Digital Treasury   |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |

    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner                | Currency | Amount | Method |
     #CASH Collections
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 1000   | CASH   |
      | 2019000051 | NOTES_RECEIVABLE      | 2019000051            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 330    | CASH   |
      | 2020000001 | SALES_ORDER           | 2020000053            | 0002 - Signmedia Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 20     | CASH   |
     #BANK Collections
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram              | EGP      | 20     | BANK   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20     | BANK   |
      | 2020000004 | SALES_ORDER           | 2020000003            |                           | 1234567893458 - EGP - Bank Misr | 000006 - المطبعة الأمنية       | EGP      | 20     | BANK   |

    And edit session is "30" minutes

  #EBS-7375
  Scenario Outline: (01) Save CollectionDetails section after edit session expired for Collection (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>" at "16-Nov-2020 09:20 AM"
    When "Ahmed.Hamdi" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:51 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-07"
    Examples:
      | CCode      | Treasury                  | BankAccount                     | Amount | CollectionMethod |
     #CASH
      | 2019000001 | 0002 - Signmedia Treasury |                                 | 1000   | CASH             |
      | 2019000051 | 0002 - Signmedia Treasury |                                 | 330    | CASH             |
      | 2020000001 | 0002 - Signmedia Treasury |                                 | 20     | CASH             |
     #BANK
      | 2019000060 |                           | 1516171819789 - EGP - Alex Bank | 20     | BANK             |
      | 2020000002 |                           | 1234567893458 - EGP - Bank Misr | 20     | BANK             |
      | 2020000004 |                           | 1234567893458 - EGP - Bank Misr | 20     | BANK             |

    #EBS-7375
  Scenario Outline: (02) Save CollectionDetails section after edit session expired & Collection is deleted (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>" at "16-Nov-2020 09:20 AM"
    And "hr1" first deleted the Collection with code "<CCode>" successfully at "16-Nov-2020 09:51 AM"
    When "Ahmed.Hamdi" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:52 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"
    Examples:
      | CCode      | Treasury                  | BankAccount                     | Amount | CollectionMethod |
     #CASH
      | 2019000001 | 0002 - Signmedia Treasury |                                 | 1000   | CASH             |
      | 2019000051 | 0002 - Signmedia Treasury |                                 | 330    | CASH             |
      | 2020000001 | 0002 - Signmedia Treasury |                                 | 20     | CASH             |
     #BANK
      | 2019000060 |                           | 1516171819789 - EGP - Alex Bank | 20     | BANK             |
      | 2020000002 |                           | 1234567893458 - EGP - Bank Misr | 20     | BANK             |
      | 2020000004 |                           | 1234567893458 - EGP - Bank Misr | 20     | BANK             |

    #EBS-7375
  Scenario Outline: (03) Save CollectionDetails section With wrong/unrelated data entry (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    And "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>" at "16-Nov-2020 09:20 AM"
    When "Ahmed.Hamdi" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:25 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

    Examples:
      | CCode      | Treasury                | BankAccount                                              | Amount | CollectionMethod |
      #Cash with a Treasury that doesn't belong to the Company
      | 2020000001 | 0005 - Textile Treasury |                                                          | 150    | CASH             |
      #Bank with a BankAccount that doesn't belong to the Company
      | 2019000060 |                         | 1234567893499 - USD - Bank Misr                          | 20     | BANK             |
      | 2020000002 |                         | 26272829303132 - EGP - Qatar National Bank Al Ahli-Tanta | 20     | BANK             |
      | 2020000004 |                         | 26272829303132 - EGP - Qatar National Bank Al Ahli-Tanta | 20     | BANK             |

   #EBS-7375
  Scenario Outline: (04) Save CollectionDetails section With missing/malicious input (Abuse Case)
    Given user is logged in as "Ahmed.Hamdi"
    And "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>" at "16-Nov-2020 09:20 AM"
    When "Ahmed.Hamdi" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:25 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page
    Examples:
      | CCode      | Treasury                  | BankAccount                     | Amount       | CollectionMethod |
     #CASH with non-existing treasury
      | 2019000001 | 0008 - Potato Treasury    |                                 | 1000         | CASH             |
      | 2019000051 | 0008 - Potato Treasury    |                                 | 330          | CASH             |
      | 2020000001 | 0008 - Potato Treasury    |                                 | 20           | CASH             |
      | 2019000001 | ""                        |                                 | 1000         | CASH             |
      | 2019000051 | ""                        |                                 | 330          | CASH             |
      | 2020000001 | ""                        |                                 | 20           | CASH             |
     #BANK with non-existing bank
      | 2019000060 |                           | 1234567891011 - Potato Bank     | 20           | BANK             |
      | 2020000002 |                           | 1234567891011 - Potato Bank     | 20           | BANK             |
      | 2020000004 |                           | 1234567891011 - Potato Bank     | 20           | BANK             |
      | 2019000060 |                           | ""                              | 20           | BANK             |
      | 2020000002 |                           | ""                              | 20           | BANK             |
      | 2020000004 |                           | ""                              | 20           | BANK             |
     #CASH with wrong amount
      | 2019000001 | 0002 - Signmedia Treasury |                                 | 2000000000   | CASH             |
      | 2019000051 | 0002 - Signmedia Treasury |                                 | 6734.7634568 | CASH             |
      | 2019000051 | 0002 - Signmedia Treasury |                                 | -8345        | CASH             |
      | 2020000001 | 0002 - Signmedia Treasury |                                 | potato       | CASH             |
     #BANK with wrong amount
      | 2019000060 |                           | 1516171819789 - EGP - Alex Bank | 2000000000   | BANK             |
      | 2020000002 |                           | 1234567893458 - EGP - Bank Misr | 6734.7634568 | BANK             |
      | 2020000002 |                           | 1234567893458 - EGP - Bank Misr | -8345        | BANK             |
      | 2020000004 |                           | 1234567893458 - EGP - Bank Misr | potato       | BANK             |