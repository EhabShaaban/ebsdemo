# Author: Ahmed Ali
# Reviewer: Somaya Ahmed and Hosam Bayomy (2-July-2021)

  #TODO: In scenario (2), Sales Order Currency should be EGP while the collection Currency is USD, and calculations should be updated accordiningly (EBS-8668)
  #TODO: Add to the givens the COA Configuration for 10430 - Cash and 10203 - Customers (EBS-8558)

Feature: Collection Based on Local Sales Order - Activate HP

  Activating Collection Based on Sales Order results in the following:
  - Collection state is changed to Active
  - Collection Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document
  ---- GL record for realized exchange rate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Collection Currency is same as Sales Invoice currency, but different from the Company Local Currency (Future)
  -------- when Collection Currency is different from both Sales Invoice currency and Company Local Currency
  - Journal Entry for Collection is created:
  ---- The retrieved fiscal year is the active one where the due date belongs to it
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations)
  - Collection Activation Details are determined
  - The referenced Sales Order remaining is updated
  - Related JournalBalances are increased according to Collection amount (EBS-8510)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Feb-2021 09:00 AM | User Daily Rate       |
      | 2020000036 | EGP              | EGP               | 1           | 01-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 01-Mar-2021 09:00 AM | Central Bank Of Egypt |
      | 2020000038 | USD              | EGP               | 15.65       | 02-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000039 | EGP              | EGP               | 1           | 02-Mar-2021 09:00 AM | Central Bank Of Egypt |

  #EBS-1410 #EBS-7345
  Scenario: (01) Activate Collections, against SalesOrder, where collection method is Cash (Happy Path)
  """
    - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date
  """
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000  |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000005 | 20000                  | 27200                 | 27200     |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_ORDER | CASH   | 000001 - Al Ahram | 2020000005        | 12500  | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "2021000001" and DueDate "08-Mar-2022 11:00 AM" at "09-Mar-2021 11:00 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 09-Mar-2021 11:00 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account           | SubAccount                | Value     |
      | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 12500 EGP |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 12500 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 09-Mar-2021 11:00 AM | 08-Mar-2022 11:00 AM | 1             | 2021000001       | 2022         |
    And Remaining amount of SalesOrder with Code "2020000005" becomes "14700"
    And State of SalesOrder with Code "2020000005" remains "Approved"
    And JournalBalance with code "TREASURY0002000100010002" is updated as follows "912500"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 09-Mar-2021 11:00 AM | Ahmed.Hamdi | 08-Mar-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000001 - Collection | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 08-Mar-2022 11:00 AM | 10430 - Cash      | 0002 - Signmedia Treasury | 0      | 12500 | EGP                | EGP             | 1                         | 2020000036       |
      | 08-Mar-2022 11:00 AM | 10203 - Customers | 000001 - Al Ahram         | 12500  | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-1410 #EBS-7345
  Scenario: (02) Activate Collections, against SalesOrder, where collection method is Bank and DocumentCurrency not equals CompanyLocalCurrency (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance      |
      | BANK00020002000200031516171819781 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 44582.325489 |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company        | Store                      |
      | 2020000005 | 0002 - DigiPro | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | USD         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.125 | 352.3665   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining   |
      | 2020000005 | 3567.7108125           | 4852.086705           | 4852.086705 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount      | Currency | BankAccount                     | Treasury |
      | 2021000001 | SALES_ORDER | BANK   | 000001 - Al Ahram | 2020000005        | 1254.659874 | USD      | 1516171819781 - USD - Alex Bank |          |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "2021000001" and DueDate "08-Mar-2021 11:00 AM" at "09-Mar-2021 11:00 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 09-Mar-2021 11:00 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account           | SubAccount                      | Value           |
      | DEBIT        | 10431 - Bank      | 1516171819781 - USD - Alex Bank | 1254.659874 USD |
      | CREDIT       | 10203 - Customers | 000001 - Al Ahram               | 1254.659874 USD |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 09-Mar-2021 11:00 AM | 08-Mar-2021 11:00 AM | 15.65         | 2021000001       | 2021         |
    And Remaining amount of SalesOrder with Code "2020000005" becomes "3597.426831"
    And State of SalesOrder with Code "2020000005" remains "Approved"
    And JournalBalance with code "BANK00020002000200031516171819781" is updated as follows "45836.985363"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company        |
      | 2021000001 | 09-Mar-2021 11:00 AM | Ahmed.Hamdi | 08-Mar-2021 11:00 AM | 0002 - Signmedia | 2021       | 2021000001 - Collection | 0002 - DigiPro |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount         | SubAccount                      | Credit      | Debit       | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 08-Mar-2021 11:00 AM | 10431 - Bank      | 1516171819781 - USD - Alex Bank | 0           | 1254.659874 | USD                | EGP             | 15.65                     | 2020000038       |
      | 08-Mar-2021 11:00 AM | 10203 - Customers | 000001 - Al Ahram               | 1254.659874 | 0           | USD                | EGP             | 15.65                     | 2020000038       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
