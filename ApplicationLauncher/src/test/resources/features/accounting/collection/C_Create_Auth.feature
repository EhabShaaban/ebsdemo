# Author: Niveen Magdy 
# Reviewer: Hossam Hassan
Feature: Create Collection - Authorization

  Background:
    Given the following users and roles exist:
      | Name                                | Role                                                    |
      | Shady.Abdelatif                     | Accountant_Signmedia                                    |
      | Shady.Abdelatif.NoCreateWithAllRead | Accountant_Signmedia_CannotCreateCollectionWithAllReads |
      | Shady.Abdelatif.NoCollectionType    | Accountant_Signmedia_CannotReadCollectionType           |
      | Shady.Abdelatif.NoSalesOrder        | Accountant_Signmedia_CannotReadSalesOrder               |
      | Shady.Abdelatif.NoDocumentOwner     | Accountant_Signmedia_CannotReadDocumentOwner            |
      | Shady.Abdelatif.NoSalesInvoice      | Accountant_Signmedia_CannotReadSalesInvoice             |
      | Shady.Abdelatif.NoNotesReceivable   | Accountant_Signmedia_CannotReadNotesReceivable          |
      | Afaf                                | FrontDesk                                               |
    And the following roles and sub-roles exist:
      | Role                                                    | Sub-role                        |
      | Accountant_Signmedia                                    | CollectionOwner_Signmedia       |
      | Accountant_Signmedia                                    | DocumentOwnerViewer             |
      | Accountant_Signmedia                                    | CollectionTypeViewer            |
      | Accountant_Signmedia                                    | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia                                    | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia                                    | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotCreateCollectionWithAllReads | DocumentOwnerViewer             |
      | Accountant_Signmedia_CannotCreateCollectionWithAllReads | CollectionTypeViewer            |
      | Accountant_Signmedia_CannotCreateCollectionWithAllReads | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotCreateCollectionWithAllReads | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotCreateCollectionWithAllReads | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotReadCollectionType           | CollectionOwner_Signmedia       |
      | Accountant_Signmedia_CannotReadCollectionType           | DocumentOwnerViewer             |
      | Accountant_Signmedia_CannotReadCollectionType           | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadCollectionType           | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadCollectionType           | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotReadSalesOrder               | CollectionOwner_Signmedia       |
      | Accountant_Signmedia_CannotReadSalesOrder               | DocumentOwnerViewer             |
      | Accountant_Signmedia_CannotReadSalesOrder               | CollectionTypeViewer            |
      | Accountant_Signmedia_CannotReadSalesOrder               | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadSalesOrder               | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadDocumentOwner            | CollectionOwner_Signmedia       |
      | Accountant_Signmedia_CannotReadDocumentOwner            | CollectionTypeViewer            |
      | Accountant_Signmedia_CannotReadDocumentOwner            | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadDocumentOwner            | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadDocumentOwner            | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotReadSalesInvoice             | CollectionOwner_Signmedia       |
      | Accountant_Signmedia_CannotReadSalesInvoice             | DocumentOwnerViewer             |
      | Accountant_Signmedia_CannotReadSalesInvoice             | CollectionTypeViewer            |
      | Accountant_Signmedia_CannotReadSalesInvoice             | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadSalesInvoice             | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotReadNotesReceivable          | CollectionOwner_Signmedia       |
      | Accountant_Signmedia_CannotReadNotesReceivable          | DocumentOwnerViewer             |
      | Accountant_Signmedia_CannotReadNotesReceivable          | CollectionTypeViewer            |
      | Accountant_Signmedia_CannotReadNotesReceivable          | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadNotesReceivable          | SOViewer_Signmedia              |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                    | Condition                      |
      | CollectionOwner_Signmedia       | Collection:Create             |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll         |                                |
      | CollectionTypeViewer            | CollectionType:ReadAll        |                                |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll          | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll       | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll            | [purchaseUnitName='Signmedia'] |
      | CollectionDocumentOwner         | Collection:CanBeDocumentOwner |                                |

    And the following users doesn't have the following permissions:
      | User                                | Permission              |
      | Shady.Abdelatif.NoCreateWithAllRead | Collection:Create       |
      | Shady.Abdelatif.NoCollectionType    | CollectionType:ReadAll  |
      | Shady.Abdelatif.NoSalesOrder        | SalesOrder:ReadAll      |
      | Shady.Abdelatif.NoDocumentOwner     | DocumentOwner:ReadAll   |
      | Shady.Abdelatif.NoSalesInvoice      | SalesInvoice:ReadAll    |
      | Shady.Abdelatif.NoNotesReceivable   | NotesReceivable:ReadAll |
      | Afaf                                | Collection:Create       |
    And the following users have the following permissions without the following conditions:
      | User            | Permission         | Condition                  |
      | Shady.Abdelatif | Collection:ReadAll | [purchaseUnitName='Flexo'] |

    And the following CollectionTypes exist:
      | Code             | Name                        |
      | SALES_INVOICE    | Against Sales Invoice       |
      | NOTES_RECEIVABLE | Against Notes Receivable    |
      | SALES_ORDER      | DownPayment for Sales Order |

    And the following SalesOrders exist:
      | Code       | State    | BusinessUnit     |
      | 2020000069 | Approved | 0002 - Signmedia |
      | 2020000003 | Approved | 0001 - Flexo     |

    And the following NotesReceivables exist:
      | Code       | State  | BusinessUnit     |
      | 2019000054 | Active | 0002 - Signmedia |
      | 2019000055 | Active | 0001 - Flexo     |

    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019100006 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0001 - Flexo     |
      | 2019100007 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |


  #EBS-4365
  Scenario Outline: (01) Create Collection by an unauthorized to create user but with all authorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif.NoCreateWithAllRead"
    When "Shady.Abdelatif.NoCreateWithAllRead" creates Collection with the following values:
      | Method | CollectionType   | RefDocument   | DocumentOwnerId |
      | CASH   | <CollectionType> | <RefDocument> | 1000072         |
    Then "Shady.Abdelatif.NoCreateWithAllRead" is logged out
    And "Shady.Abdelatif.NoCreateWithAllRead" is forwarded to the error page
    Examples:
      | CollectionType   | RefDocument |
      | SALES_INVOICE    | 2019100007  |
      | NOTES_RECEIVABLE | 2019000054  |
      | SALES_ORDER      | 2020000069  |

  #EBS-4365
  Scenario Outline: (02) Create Collection by an authorized user to create, but without unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" creates Collection with the following values:
      | Method | CollectionType   | RefDocument   | DocumentOwnerId |
      | CASH   | <CollectionType> | <RefDocument> | 1000072         |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                              | CollectionType   | RefDocument |
      | Shady.Abdelatif.NoCollectionType  | SALES_INVOICE    | 2019100007  |
      | Shady.Abdelatif.NoDocumentOwner   | SALES_INVOICE    | 2019100007  |
      | Shady.Abdelatif.NoSalesInvoice    | SALES_INVOICE    | 2019100007  |
      | Shady.Abdelatif.NoNotesReceivable | NOTES_RECEIVABLE | 2019000054  |
      | Shady.Abdelatif.NoSalesOrder      | SALES_ORDER      | 2020000069  |
    
  #EBS-4365
  Scenario Outline: (03) Create Collection by an unauthorized user to create due to condition (Unauthorized/Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method | CollectionType   | RefDocument   | DocumentOwnerId |
      | CASH   | <CollectionType> | <RefDocument> | 1000072         |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | CollectionType   | RefDocument |
      | SALES_INVOICE    | 2019100006  |
      | NOTES_RECEIVABLE | 2019000055  |
      | SALES_ORDER      | 2020000003  |

  #EBS-4365
  Scenario Outline: (04) Create Collection by an unauthorized user to create with unauthorized reads (Unauthorized/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" creates Collection with the following values:
      | Method | CollectionType   | RefDocument   | DocumentOwnerId |
      | CASH   | <CollectionType> | <RefDocument> | 1000072         |
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
    Examples:
      | CollectionType   | RefDocument |
      | SALES_INVOICE    | 2019100007  |
      | NOTES_RECEIVABLE | 2019000054  |
      | SALES_ORDER      | 2020000069  |

 #EBS-4365
  Scenario: (05) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads     |
      | ReadType            |
      | ReadDocumentOwners  |
      | ReadSalesOrders     |
      | ReadSalesInvoices   |
      | ReadNotesReceivable |

 #EBS-4365
  Scenario: (06) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoCollectionType"
    When "Shady.Abdelatif.NoCollectionType" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif.NoCollectionType":
      | AuthorizedReads     |
      | ReadDocumentOwners  |
      | ReadSalesOrders     |
      | ReadSalesInvoices   |
      | ReadNotesReceivable |

 #EBS-4365
  Scenario: (07) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoDocumentOwner"
    When "Shady.Abdelatif.NoDocumentOwner" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif.NoDocumentOwner":
      | AuthorizedReads     |
      | ReadType            |
      | ReadSalesOrders     |
      | ReadSalesInvoices   |
      | ReadNotesReceivable |

 #EBS-4365
  Scenario: (08) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoSalesOrder"
    When "Shady.Abdelatif.NoSalesOrder" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif.NoSalesOrder":
      | AuthorizedReads     |
      | ReadType            |
      | ReadDocumentOwners  |
      | ReadSalesInvoices   |
      | ReadNotesReceivable |

 #EBS-4365
  Scenario: (09) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoSalesInvoice"
    When "Shady.Abdelatif.NoSalesInvoice" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif.NoSalesInvoice":
      | AuthorizedReads     |
      | ReadType            |
      | ReadDocumentOwners  |
      | ReadSalesOrders     |
      | ReadNotesReceivable |

 #EBS-4365
  Scenario: (10) Create Collection by an authorized user with authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoNotesReceivable"
    When "Shady.Abdelatif.NoNotesReceivable" requests to create Collection
    Then the following authorized reads are returned to "Shady.Abdelatif.NoNotesReceivable":
      | AuthorizedReads    |
      | ReadType           |
      | ReadDocumentOwners |
      | ReadSalesOrders    |
      | ReadSalesInvoices  |
