# Author: Niveen Magdy (EBS-7416)
# Reviewer: Ahmad Hamed (15-12-2020)
Feature: Collection - Save Collection Details section (Auth)

  Background:
    Given the following users and roles exist:
      | Name                  | Role                                       |
      | Shady.Abdelatif       | Accountant_Signmedia                       |
      | Ahmed.Hamdi.NoCompany | Accountant_Signmedia_CannotViewCompanyRole |
      | Ahmed.Hamdi           | AdminTreasury                              |
      | Afaf                  | FrontDesk                                  |

    And the following roles and sub-roles exist:
      | Role                                       | Sub-role                  |
      | Accountant_Signmedia                       | CollectionOwner_Signmedia |
      | Accountant_Signmedia                       | CompanyViewer             |
      | Accountant_Signmedia_CannotViewCompanyRole | CollectionOwner_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                   | Permission                         | Condition                      |
      | CollectionOwner_Signmedia | Collection:UpdateCollectionDetails | [purchaseUnitName='Signmedia'] |
      | CompanyViewer             | Company:ReadAll                    |                                |
      | CompanyViewer             | Company:ReadBankDetails            |                                |

    And the following users doesn't have the following permissions:
      | User                  | Permission                         |
      | Ahmed.Hamdi.NoCompany | Company:ReadAll                    |
      | Ahmed.Hamdi.NoCompany | Company:ReadBankDetails            |
      | Afaf                  | Collection:UpdateCollectionDetails |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                         | Condition                  |
      | Shady.Abdelatif | Collection:UpdateCollectionDetails | [purchaseUnitName='Flexo'] |

    And the following Collections exist:
      | Code       | State | BusinessUnit     |
      | 2019000001 | Draft | 0002 - Signmedia |
      | 2019000051 | Draft | 0002 - Signmedia |
      | 2019000060 | Draft | 0002 - Signmedia |
      | 2020000002 | Draft | 0002 - Signmedia |
      | 2020000004 | Draft | 0001 - Flexo     |

    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank                                     | AccountNumber        |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893458 - EGP  |
      | 15 | 0001 - AL Madina | 0002 - National Bank of Egypt            | 1011121314678 - USD  |
      | 21 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893422 - EGP  |
      | 22 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893499 - USD  |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819789 - EGP  |
      | 18 | 0002 - DigiPro   | 0004 - CTBC Bank CO., LTD.               | 2021222324253 - USD  |
      | 19 | 0002 - DigiPro   | 0005 - Qatar National Bank Al Ahli-Tanta | 26272829303132 - EGP |
      | 20 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819712 - USD  |

    And the Company "0001 - AL Madina" has the following Treasuries:
      | Code | Name               |
      | 0001 | Flexo Treasury     |
      | 0002 | Signmedia Treasury |
      | 0003 | Offset Treasury    |
      | 0004 | Digital Treasury   |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |

    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner          | Currency | Amount | Method |
     #CASH Collections
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram        | EGP      | 1000.0 | CASH   |
      | 2019000051 | NOTES_RECEIVABLE      | 2019000051            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram        | EGP      | 330.0  | CASH   |
     #BANK Collections
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram        | EGP      | 20.0   | BANK   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram        | EGP      | 20.0   | BANK   |
      | 2020000004 | SALES_ORDER           | 2020000003            |                           | 1234567893458 - EGP - Bank Misr | 000006 - المطبعة الأمنية | EGP      | 20.0   | BANK   |

  #EBS-7416
  Scenario Outline: (01) Save CollectionDetails section by unauthorized user, with unauthorized reads and unauthorized user due to condition (Unauthorized/Abuse case)
    Given user is logged in as "<User>"
    When "<User>" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:25 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                  | CCode      | Treasury               | BankAccount                     | Amount | CollectionMethod |
    #Cash
      | Afaf                  | 2019000001 | 0003 - Offset Treasury |                                 | 150.0  | CASH             |
      | Ahmed.Hamdi.NoCompany | 2019000051 | 0001 - Flexo Treasury  |                                 | 300.0  | CASH             |
    #Bank
      | Afaf                  | 2019000060 |                        | 1516171819712 - USD - Alex Bank | 30.0   | BANK             |
      | Ahmed.Hamdi.NoCompany | 2020000002 |                        | 1234567893499 - USD - Bank Misr | 50.0   | BANK             |
    #Unauthorized user due to condition
      | Shady.Abdelatif       | 2020000004 |                        | 1234567893499 - USD - Bank Misr | 70.0   | BANK             |
