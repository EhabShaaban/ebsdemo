#Author: Ahmad Hamed, Engy Mohamed
#Reviewer: Hossam Hassan (07-10-2020) (#EBS-4323)

Feature: Collection Object Screen Allowed Actions

  Background:
    Given the following users and roles exist:
      | Name               | Role                            |
      | Ahmed.Al-Ashry     | CollectionResponsible_Signmedia |
      | Shady.Abdelatif    | Accountant_Signmedia            |
      | Ahmed.Hamdi        | AdminTreasury                   |
      | CannotReadSections | CannotReadSectionsRole          |
      | hr1                | SuperUser                       |


    And the following roles and sub-roles exist:
      | Role                            | Subrole                    |
      | CollectionResponsible_Signmedia | CollectionOwner_Signmedia  |
      | CollectionResponsible_Signmedia | CollectionViewer_Signmedia |
      | Accountant_Signmedia            | CollectionViewer_Signmedia |
      | AdminTreasury                   | CollectionActivateOwner    |
      | AdminTreasury                   | CollectionOwner            |
      | AdminTreasury                   | CollectionViewer           |
      | CannotReadSectionsRole          | CannotReadSectionsSubRole  |
      | SuperUser                       | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                           | Condition                      |
      | CollectionOwner_Signmedia  | Collection:Delete                    | [purchaseUnitName='Signmedia'] |
      | CollectionOwner_Signmedia  | Collection:UpdateCollectionDetails   | [purchaseUnitName='Signmedia'] |
      | CollectionOwner            | Collection:UpdateCollectionDetails   |                                |
      | CollectionViewer_Signmedia | Collection:ReadAll                   | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadGeneralData           | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadCompany               | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadAccountingDetails     | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadCollectionDetails     | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadActivationDetails     | [purchaseUnitName='Signmedia'] |
      | CollectionViewer_Signmedia | Collection:ReadAccountingInformation | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadAll                   |                                |
      | CollectionViewer           | Collection:ReadGeneralData           |                                |
      | CollectionViewer           | Collection:ReadCompany               |                                |
      | CollectionViewer           | Collection:ReadAccountingDetails     |                                |
      | CollectionViewer           | Collection:ReadCollectionDetails     |                                |
      | CollectionViewer           | Collection:ReadActivationDetails     |                                |
      | CollectionViewer           | Collection:ReadAccountingInformation |                                |
      | CollectionActivateOwner    | Collection:Activate                  |                                |
      | CannotReadSectionsSubRole  | Collection:Activate                  |                                |
      | CannotReadSectionsSubRole  | Collection:Delete                    |                                |
      | CannotReadSectionsSubRole  | Collection:ReadAll                   |                                |
      | SuperUserSubRole           | *:*                                  |                                |

    And the following users doesn't have the following permissions:
      | User               | Permission                         |
      | Ahmed.Al-Ashry     | Collection:Activate                |
      | Shady.Abdelatif    | Collection:Activate                |
      | CannotReadSections | Collection:ReadActivationDetails   |
      | CannotReadSections | Collection:ReadCollectionDetails   |
      | CannotReadSections | Collection:ReadAccountingDetails   |
      | CannotReadSections | Collection:ReadCompany             |
      | CannotReadSections | Collection:UpdateCollectionDetails |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                           | Condition                  |
      | Ahmed.Al-Ashry  | Collection:Delete                    | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadAll                   | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadGeneralData           | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadCompany               | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadAccountingDetails     | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadCollectionDetails     | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadActivationDetails     | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry  | Collection:ReadAccountingInformation | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadGeneralData           | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadCompany               | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadAccountingDetails     | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadCollectionDetails     | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadActivationDetails     | [purchaseUnitName='Flexo'] |
      | Shady.Abdelatif | Collection:ReadAccountingInformation | [purchaseUnitName='Flexo'] |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100007 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100008 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100009 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2020100007 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2020 02:20 PM |
      | 2020100008 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2020 02:20 PM |
      | 2020100009 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2020 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019100007 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100008 | 0002 - DigiPro | 0002 - Signmedia |
      | 2019100009 | 0002 - DigiPro | 0001 - Flexo     |
      | 2020100007 | 0002 - DigiPro | 0002 - Signmedia |
      | 2020100008 | 0002 - DigiPro | 0002 - Signmedia |
      | 2020100009 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100007 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100008 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100009 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2020100007 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2020100008 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2020100009 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100007 | 7080.25   |
      | 2019100008 | 7080.25   |
      | 2019100009 | 7080.25   |
      | 2020100007 | 7080.25   |
      | 2020100008 | 7080.25   |
      | 2020100009 | 7080.25   |
 #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft  | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2019000002 | C_SI | Active | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2019000003 | C_SI | Draft  | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2020000001 | C_SI | Draft  | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2020000003 | C_SI | Active | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2020000004 | C_SI | Draft  | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |

    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000002 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000003 | 0001 - Flexo     | 0002 - DigiPro |
      | 2020000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000003 | 0002 - Signmedia | 0002 - DigiPro |
      | 2020000004 | 0001 - Flexo     | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2019100007        | 1000   | EGP      |             | 0002 - Signmedia Treasury |
      | 2019000002 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2019100008        | 1000   | EGP      |             | 0002 - Signmedia Treasury |
      | 2019000003 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2019100009        | 1000   | EGP      |             | 0001 - Flexo Treasury     |
      | 2020000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2020100007        | 1000   | EGP      |             | 0002 - Signmedia Treasury |
      | 2020000003 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2020100008        | 1000   | EGP      |             | 0002 - Signmedia Treasury |
      | 2020000004 | SALES_INVOICE | CASH   | 000001 - Al Ahram | 2020100009        | 1000   | EGP      |             | 0001 - Flexo Treasury     |


  #EBS-5962 #EBS-4323
  Scenario Outline: (01) Read allowed actions in Collection Object Screen - All States  (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Collection with code "<CCode>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | CCode      | AllowedActions                                                                                                                                                           | NoOfActions |
      | Ahmed.Al-Ashry     | 2019000001 | Delete, UpdateCollectionDetails, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation   | 9           |
      | Ahmed.Al-Ashry     | 2019000002 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Ahmed.Al-Ashry     | 2020000001 | Delete, UpdateCollectionDetails, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation   | 9           |
      | Ahmed.Al-Ashry     | 2020000003 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Shady.Abdelatif    | 2019000001 | Delete, UpdateCollectionDetails, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation   | 9           |
      | Shady.Abdelatif    | 2019000002 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Shady.Abdelatif    | 2020000001 | Delete, UpdateCollectionDetails, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation   | 9           |
      | Shady.Abdelatif    | 2020000003 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Ahmed.Hamdi        | 2019000001 | UpdateCollectionDetails, Activate, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation | 9           |
      | Ahmed.Hamdi        | 2019000002 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Ahmed.Hamdi        | 2019000003 | UpdateCollectionDetails, Activate, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation | 9           |
      | Ahmed.Hamdi        | 2020000001 | UpdateCollectionDetails, Activate, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation | 9           |
      | Ahmed.Hamdi        | 2020000003 | ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation                                    | 7           |
      | Ahmed.Hamdi        | 2020000004 | UpdateCollectionDetails, Activate, ReadAll, ReadGeneralData, ReadCompany, ReadAccountingDetails, ReadCollectionDetails, ReadActivationDetails, ReadAccountingInformation | 9           |
      | CannotReadSections | 2019000001 | Activate, ReadAll, Delete                                                                                                                                                | 3           |
      | CannotReadSections | 2019000002 | ReadAll                                                                                                                                                                  | 1           |
      | CannotReadSections | 2019000003 | Activate, ReadAll, Delete                                                                                                                                                | 3           |
      | CannotReadSections | 2020000001 | Activate, ReadAll, Delete                                                                                                                                                | 3           |
      | CannotReadSections | 2020000003 | ReadAll                                                                                                                                                                  | 1           |
      | CannotReadSections | 2020000004 | Activate, ReadAll, Delete                                                                                                                                                | 3           |

  #EBS-5962 #EBS-4323
  Scenario Outline: (02) Read allowed actions in Collection Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Collection with code "<CCode>"
    Then an authorization error notification is sent to "<User>" with the following message "Gen-msg-29"
    Examples:
      | User            | CCode      |
      | Ahmed.Al-Ashry  | 2019000003 |
      | Shady.Abdelatif | 2019000003 |
      | Ahmed.Al-Ashry  | 2020000004 |

  @Future
  #EBS-5962
  Scenario: (03) Read allowed actions in Collection Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2019000001" successfully
    When "Ahmed.Al-Ashry" requests to read actions of Collection with code "2019000001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"
