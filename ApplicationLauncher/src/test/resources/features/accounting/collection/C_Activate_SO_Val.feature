# Author: Niveen Magdy (EBS-1682)
# Reviewer: Ahmad Hamed (02-12-2020)
Feature: Collection - Activate Val

  Background: 
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
      | hr1         | SuperUser     |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
      | SuperUser     | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
      | SuperUserSubRole        | *:*                 |           |
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg |  10 |       2000 |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000005 | 0007 - Vat                                   |             1 |
      | 2020000005 | 0008 - Commercial and industrial profits tax |           0.5 |
      | 2020000005 | 0009 - Real estate taxes                     |            10 |
      | 2020000005 | 0010 - Service tax                           |            12 |
      | 2020000005 | 0011 - Adding tax                            |           0.5 |
      | 2020000005 | 0012 - Sales tax                             |            12 |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000005 |                  20000 |                 27200 |     27200 |
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance   |
      | TREASURY0001000200010001 | 0001 - Flexo     | 0002 - DigiPro   | 0001 - EGP | 0001 - Flexo Treasury     | 900000.00 |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000.00 |
      | TREASURY0002000200010002 | 0002 - Signmedia | 0002 - DigiPro   | 0001 - EGP | 0002 - Signmedia Treasury | 900000.00 |
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance  |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 40000.00 |
      | BANK00020002000200031516171819789 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - USD | 1516171819781 - USD - Alex Bank | 40000.00 |

  #EBS-1680 ,EBS-6076
  Scenario Outline: (01) Activate Collection, against SalesOrder Val - missing or malicious input (Abuse case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
      | 2020000002 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000002 | SALES_ORDER | BANK   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      | 1234567893458 - EGP - Bank Misr |                           |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "<CCode>" and DueDate "<DueDate>" at "09-Nov-2020 11:00 AM"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

    Examples: 
      | CCode      | DueDate |
      #CASH
      | 2020000001 |         |
      | 2020000001 | Ay7aga  |
      #BANK
      | 2020000002 |         |
      | 2020000002 | Ay7aga  |

  #
  #EBS-1680 ,EBS-6076
  Scenario Outline: (02) Activate Collection, against SalesOrder Val - with an invalid state - Active State (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
      | 2020000002 | C_SO | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000002 | SALES_ORDER | BANK   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      | 1234567893458 - EGP - Bank Misr |                           |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount                      | Value     |
      | 2020000001 | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury       | 12500 EGP |
      | 2020000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram               | 12500 EGP |
      | 2020000002 | DEBIT        | 10431 - Bank      | 1234567893458 - EGP - Bank Misr | 12500 EGP |
      | 2020000002 | CREDIT       | 10203 - Customers | 000001 - Al Ahram               | 12500 EGP |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "<CCode>" and DueDate "08-Nov-2020 11:00 AM" at "09-Nov-2020 11:00 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-32"

    Examples: 
      | CCode      |
      | 2020000001 |
      | 2020000002 |

  #EBS-1680 ,EBS-6076
  Scenario Outline: (03) Activate Collection, against SalesOrder Val - when CollectionDetails section is locked (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
      | 2020000002 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000002 | SALES_ORDER | BANK   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      | 1234567893458 - EGP - Bank Misr |                           |
    And another user is logged in as "hr1"
    And "hr1" requests to edit CollectionDetails section of Collection with code "<CCode>" at "09-Nov-2020 11:00 AM"
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "<CCode>" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-14"

    Examples: 
      | CCode      |
      | 2020000001 |
      | 2020000002 |

  #EBS-1680 ,EBS-6076
  Scenario Outline: (04) Activate Collection, against SalesOrder Val - where Collection doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
      | 2020000002 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000002 | SALES_ORDER | BANK   | 000001 - Al Ahram |        2020000005 |  12500 | EGP      | 1234567893458 - EGP - Bank Misr |                           |
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "<CCode>" successfully
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "<CCode>" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"

    Examples: 
      | CCode      |
      | 2020000001 |
      | 2020000002 |

  #EBS-1680 ,EBS-6076
  Scenario: (05) Activate Collection, against SalesOrder Val , where Collection has amount greater than the Total Remaining in Selected refDocument (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |  30000 | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "2020000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "C-msg-03"

  # EBS-1680 ,EBS-6076
  Scenario: (06) Activate Collection, against SalesOrder Val ,  mandatory fields (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000005 |        | EGP      |             |          |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "2020000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-40"
    And the following fields "amount , treasuryCode" which sent to "Ahmed.Hamdi" are marked as missing
  #EBS-1680
  Scenario: (07) Activate Collection, against SalesOrder Val - where SalesOrder state is not (Approved or GoodsIssueActivated)(Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000006 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Rejected | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000006 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000006 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000006 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg |  10 |       2000 |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000006 | 0007 - Vat                                   |             1 |
      | 2020000006 | 0008 - Commercial and industrial profits tax |           0.5 |
      | 2020000006 | 0009 - Real estate taxes                     |            10 |
      | 2020000006 | 0010 - Service tax                           |            12 |
      | 2020000006 | 0011 - Adding tax                            |           0.5 |
      | 2020000006 | 0012 - Sales tax                             |            12 |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000006 |                  20000 |                 27200 |     27200 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2020000001 | C_SO | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type        | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2020000001 | SALES_ORDER | CASH   | 000001 - Al Ahram |        2020000006 |  20000 | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_ORDER" with code "2020000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-53"

