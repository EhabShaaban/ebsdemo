#Author: Ahmad Hamed, Engy Mohamed

Feature: Collection - View Collection Details section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | AdminTreasury        | CollectionViewer           |
      | SuperUser            | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadCollectionDetails | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadCollectionDetails |                                |
      | SuperUserSubRole           | *:*                              |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | Collection:ReadCollectionDetails | [purchaseUnitName='Flexo'] |

    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |

    And the following Treasuries exist:
      | Code | Name               |
      | 0001 | Flexo Treasury     |
      | 0002 | Signmedia Treasury |

    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank             | AccountNumber       |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr | 1234567893458 - EGP |

    And the following Customers exist:
      | Code   | Name     | BusinessUnit     |
      | 000001 | Al Ahram | 0002 - Signmedia |
      | 000002 | Client2  | 0001 - Flexo     |

    And the following CollectionTypes exist:
      | Code             | Name                        |
      | SALES_INVOICE    | Against Sales Invoice       |
      | NOTES_RECEIVABLE | Against Notes Receivable    |
      | SALES_ORDER      | DownPayment for Sales Order |

    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                           | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2019000013 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION | Posted | Admin     | 02-Aug-2019 11:00 AM | Ashraf Salah  | 1000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000013 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2019000013 | 000002 - Zhejiang | 1000   | 0001 - EGP |
 #@INSERT
    And the following PaymentRequests GeneralData exist:
      | PRCode     | PaymentType      | State       | CreatedBy   | CreationDate         | DocumentOwner |
      | 2021000005 | UNDER_SETTLEMENT | PaymentDone | Gehan.Ahmed | 04-Jan-2021 09:02 AM | Ashraf Salah  |
     #@INSERT
    And the following PaymentRequests CompanyData exist:
      | PRCode     | BusinessUnit     | Company          |
      | 2021000005 | 0002 - Signmedia | 0001 - AL Madina |
     #@INSERT
    And the following PaymentRequests PaymentDetails exist:
      | PRCode     | PaymentForm | BusinessPartner      | DueDocumentType | DueDocument | NetAmount | CurrencyISO | CostFactorItem | BankAccountNumber | Treasury                  | Description |
      | 2021000005 | CASH        | 000002 - Ahmed Hamdi |                 |             | 1000.00   | EGP         |                |                   | 0002 - Signmedia Treasury |             |

    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_DN | Draft | Admin     | 5-Jan-2021 9:02 AM | VENDOR              | Gehan Ahmed   |
      | 2021000003 | C_PR | Draft | Admin     | 5-Jan-2021 9:02 AM | EMPLOYEE            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type            | Method | BusinessPartner      | ReferenceDocument | Amount      | Currency | BankAccount | Treasury                  |
      | 2021000001 | DEBIT_NOTE      | CASH   | 000002 - Zhejiang    | 2019000013        | 20          | EGP      |             | 0002 - Signmedia Treasury |
      | 2021000003 | PAYMENT_REQUEST | CASH   | 000002 - Ahmed Hamdi | 2021000005        | 1000.123456 | EGP      |             | 0002 - Signmedia Treasury |

    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner                | Currency | Amount     | Method |
     #CASH Collections
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 1000       | CASH   |
      | 2019000003 | SALES_INVOICE         | 2019100006            | 0001 - Flexo Treasury     |                                 | 000002 - Client2               | EGP      | 3000       | CASH   |
      | 2019000005 | NOTES_RECEIVABLE      | 2019000002            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 3000       | CASH   |
      | 2020000001 | SALES_ORDER           | 2020000053            | 0002 - Signmedia Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 20         | CASH   |
     #BANK Collections
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram              | EGP      | 20         | BANK   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20         | BANK   |
      | 2020000003 | SALES_ORDER           | 2020000069            |                           | 1234567893458 - EGP - Bank Misr | 000007 - مطبعة أكتوبر الهندسية | EGP      | 197.200000 | BANK   |

  #EBS-5485 #EBS-7953
  @ResetData
  Scenario Outline: (01) View Collection Details section - by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view Collection Details section of Collection with code "<CCode>"
    Then the following values of Collection Details section for Collection with code "<CCode>" are displayed to "Ahmed.Hamdi":
      | ReferenceDocumentType   | ReferenceDocumentCode   | Treasury   | BankAccount   | BusinessPartner   | Currency   | Amount   | Method   |
      | <ReferenceDocumentType> | <ReferenceDocumentCode> | <Treasury> | <BankAccount> | <BusinessPartner> | <Currency> | <Amount> | <Method> |
    Examples:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner                | Currency | Amount      | Method |
      #CASH Collections
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 1000        | CASH   |
      | 2019000003 | SALES_INVOICE         | 2019100006            | 0001 - Flexo Treasury     |                                 | 000002 - Client2               | EGP      | 3000        | CASH   |
      | 2019000005 | NOTES_RECEIVABLE      | 2019000002            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 3000        | CASH   |
      | 2020000001 | SALES_ORDER           | 2020000053            | 0002 - Signmedia Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 20          | CASH   |
      | 2021000001 | DEBIT_NOTE            | 2019000013            | 0002 - Signmedia Treasury |                                 | 000002 - Zhejiang              | EGP      | 20          | CASH   |
      | 2021000003 | PAYMENT_REQUEST       | 2021000005            | 0002 - Signmedia Treasury |                                 | 000002 - Ahmed Hamdi           | EGP      | 1000.123456 | CASH   |
      #BANK Collections
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram              | EGP      | 20          | BANK   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20          | BANK   |
      | 2020000003 | SALES_ORDER           | 2020000069            |                           | 1234567893458 - EGP - Bank Misr | 000007 - مطبعة أكتوبر الهندسية | EGP      | 197.200000  | BANK   |

 #EBS-5485
  @ResetData
  Scenario Outline: (02) View Collection Details section - by an authorized user who has one role with condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Collection Details section of Collection with code "<CCode>"
    Then the following values of Collection Details section for Collection with code "<CCode>" are displayed to "Shady.Abdelatif":
      | ReferenceDocumentType   | ReferenceDocumentCode   | Treasury   | BankAccount   | BusinessPartner   | Currency   | Amount   | Method   |
      | <ReferenceDocumentType> | <ReferenceDocumentCode> | <Treasury> | <BankAccount> | <BusinessPartner> | <Currency> | <Amount> | <Method> |
    Examples:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner   | Currency | Amount | Method |
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram | EGP      | 1000   | CASH   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram | EGP      | 20     | BANK   |

 #EBS-5485
  @ResetData
  Scenario: (03) View Collection Details section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Collection Details section of Collection with code "2019000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

 #EBS-5485
  @ResetData
  Scenario: (04) View Collection Details section - where Collection doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2019000001" successfully
    When "Shady.Abdelatif" requests to view Collection Details section of Collection with code "2019000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
