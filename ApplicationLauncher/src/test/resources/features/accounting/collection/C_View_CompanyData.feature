#Author: Ahmad Hamed, Engy Mohamed

Feature: Collection - View Company section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | AdminTreasury        | CollectionViewer           |
      | SuperUser            | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission             | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadCompany | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadCompany |                                |
      | SuperUserSubRole           | *:*                    |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission             | Condition                  |
      | Shady.Abdelatif | Collection:ReadCompany | [purchaseUnitName='Flexo'] |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Companies exist:
      | Code | Name      |
      | 0001 | AL Madina |
      | 0002 | DigiPro   |

    And the following Company Data for Collections exist:
      | CCode      | Company          | BusinessUnit     |
#      Based On SI
      | 2019000003 | 0002 - DigiPro   | 0001 - Flexo     |
      | 2019000060 | 0002 - DigiPro   | 0002 - Signmedia |
#      Based On NR
      | 2019000061 | 0001 - AL Madina | 0002 - Signmedia |
#      Based On SO
      | 2020000001 | 0001 - AL Madina | 0002 - Signmedia |

  #EBS-4459
  Scenario Outline: (01) View Collection Company section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Company section of Collection with code "<CCode>"
    Then the following values of Company section for Collection with code "<CCode>" are displayed to "Shady.Abdelatif":
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    Examples:
      | CCode      | BusinessUnit | Company   |
      | 2019000060 | Signmedia    | DigiPro   |
      | 2019000061 | Signmedia    | AL Madina |
      | 2020000001 | Signmedia    | AL Madina |

  #EBS-4459
  Scenario Outline: (02) View Collection Company section - by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view Company section of Collection with code "<CCode>"
    Then the following values of Company section for Collection with code "<CCode>" are displayed to "Ahmed.Hamdi":
      | BusinessUnit   | Company   |
      | <BusinessUnit> | <Company> |
    Examples:
      | CCode      | BusinessUnit | Company   |
      | 2019000003 | Flexo        | DigiPro   |
      | 2020000001 | Signmedia    | AL Madina |

  #EBS-4459
  Scenario: (03) View Collection Company section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view Company section of Collection with code "2019000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-4459
  Scenario: (04) View Collection Company section - where Collection doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2020000001" successfully
    When "Shady.Abdelatif" requests to view Company section of Collection with code "2020000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
