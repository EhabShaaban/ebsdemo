#Author Dev: Hossam Hassan (EBS-6222)
#Reviewer: Ahmad Hamed (04-11-2020)
Feature: View ActivationDetails section in Collection

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |
      | hr1             | SuperUser            |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | AdminTreasury        | CollectionViewer           |
      | SuperUser            | SuperUserSubRole           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadActivationDetails | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadActivationDetails |                                |
      | SuperUserSubRole           | *:*                              |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | Collection:ReadActivationDetails | [purchaseUnitName='Flexo'] |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |

    And the following Collections exist:
      | Code       | State  | BusinessUnit     |
      | 2020000006 | Active | 0001 - Flexo     |
      | 2020000005 | Active | 0002 - Signmedia |
      | 2020000003 | Active | 0002 - Signmedia |
      | 2020000002 | Draft  | 0002 - Signmedia |
      | 2020000001 | Draft  | 0002 - Signmedia |
      | 2019000002 | Active | 0002 - Signmedia |
      | 2019000001 | Draft  | 0002 - Signmedia |

    And the following ActivationDetails for AccountingDocument with DocumentType "Collection" exist:
      | Code       | CreationDate         | CreatedBy                 | Accountant                | ExchangeRateCode | JournalEntryCode | CurrencyPrice | ObjectTypeCode |
      | 2020000006 | 4-Oct-2020 09:00 AM  | H.Hassan                  | Admin from BDKCompanyCode | 2018000013       | 2020000054       | 1 EGP         | C_SO           |
      | 2020000005 | 4-Oct-2020 09:00 AM  | H.Hassan                  | Admin from BDKCompanyCode | 2018000013       | 2020000053       | 1 EGP         | C_NR           |
      | 2020000003 | 4-Oct-2020 09:00 AM  | A.Hamed                   | Admin from BDKCompanyCode | 2018000013       | 2020000052       | 1 EGP         | C_SO           |
      | 2019000002 | 17-Dec-2019 02:09 PM | Admin from BDKCompanyCode | Admin from BDKCompanyCode | 2018000013       | 2020000050       | 1 EGP         | C_SI           |

  #EBS-6222
  Scenario Outline: (01) View ActivationDetails section - by an authorized user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ActivationDetails section of Collection with code "<Code>"
    Then the following values of ActivationDetails section for Collection with code "<Code>" are displayed to "Shady.Abdelatif":
      | Code | Accountant | ActivationDate | PostedBy | JournalEntryCode | CurrencyPrice | ExchangeRateCode |
      |      |            |                |          |                  |               |                  |

    Examples:
      | Code       |
      | 2019000001 |
      | 2020000001 |
      | 2020000002 |

  #EBS-6222
  Scenario Outline: (02) View ActivationDetails section - by an authorized user who has one role with no condition(Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view ActivationDetails section of Collection with code "<Code>"
    Then the following values of ActivationDetails section for Collection with code "<Code>" are displayed to "Ahmed.Hamdi":
      | Code   | Accountant   | ActivationDate   | PostedBy   | JournalEntryCode   | CurrencyPrice   | ExchangeRateCode   |
      | <Code> | <Accountant> | <ActivationDate> | <PostedBy> | <JournalEntryCode> | <CurrencyPrice> | <ExchangeRateCode> |

    Examples:
      | Code       | Accountant                | ActivationDate       | PostedBy | JournalEntryCode | CurrencyPrice     | ExchangeRateCode |
      | 2020000006 | Admin from BDKCompanyCode | 10-Oct-2020 10:30 AM | H.Hassan | 2020000054       | 1.0 EGP = 1.0 EGP | 2018000013       |
      | 2020000003 | Admin from BDKCompanyCode | 10-Oct-2020 10:30 AM | A.Hamed  | 2020000052       | 1.0 EGP = 1.0 EGP | 2018000013       |

  #EBS-6222
  Scenario Outline: (03) View ActivationDetails section - by an authorized user who has one role with condition(Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ActivationDetails section of Collection with code "<Code>"
    Then the following values of ActivationDetails section for Collection with code "<Code>" are displayed to "Shady.Abdelatif":
      | Code   | Accountant   | ActivationDate   | PostedBy   | JournalEntryCode   | CurrencyPrice   | ExchangeRateCode   |
      | <Code> | <Accountant> | <ActivationDate> | <PostedBy> | <JournalEntryCode> | <CurrencyPrice> | <ExchangeRateCode> |

    Examples:
      | Code       | Accountant                | ActivationDate       | PostedBy                  | JournalEntryCode | CurrencyPrice     | ExchangeRateCode |
      | 2020000005 | Admin from BDKCompanyCode | 10-Oct-2020 10:30 AM | H.Hassan                  | 2020000053       | 1.0 EGP = 1.0 EGP | 2018000013       |
      | 2020000003 | Admin from BDKCompanyCode | 10-Oct-2020 10:30 AM | A.Hamed                   | 2020000052       | 1.0 EGP = 1.0 EGP | 2018000013       |
      | 2019000002 | Admin from BDKCompanyCode | 18-Dec-2019 10:30 AM | Admin from BDKCompanyCode | 2020000050       | 1.0 EGP = 1.0 EGP | 2018000013       |

  #EBS-6222
  Scenario: (04) View ActivationDetails section - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ActivationDetails section of Collection with code "2020000006"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-6222
  Scenario: (05) View ActivationDetails section - where Collection doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2020000002" successfully
    When "Shady.Abdelatif" requests to view ActivationDetails section of Collection with code "2020000002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
