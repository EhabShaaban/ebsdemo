# Author: Ahmed Ali
# Reviewer:
Feature: Collection Based on Notes Receivable - Activate HP

  Activating Collection Based on Notes Receivable results in the following:
  - Collection state is changed to Active
  - Collection Accounting Details are determined (Account Determination)
  ---- GLAccount are retrieved from COA Configuration
  ---- GLSubAccounts are retrieved from the Collection document
  ---- GL record for realized exchange rate is added, if any (calculated using the ExRate of the Journal Entry)
  -------- when Collection Currency is same as Sales Invoice currency, but different from the Company Local Currency (Future)
  -------- when Collection Currency is different from both Sales Invoice currency and Company Local Currency
  - Journal Entry for Collection is created:
  ---- The retrieved fiscal year is the active one where the due date belongs to it
  ---- The latest ExchangeRate is retrieved based on the default strategy (same ExRate used in Realized ExRate calculations)
  - Collection Activation Details are determined
  - The referenced Notes Receivable remaining is updated
  - Related JournalBalances are increased according to Collection amount (EBS-8510)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Feb-2021 09:00 AM | User Daily Rate       |
      | 2020000036 | EGP              | EGP               | 1           | 01-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 01-Mar-2021 09:00 AM | Central Bank Of Egypt |
      | 2020000038 | USD              | EGP               | 15.65       | 02-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000039 | EGP              | EGP               | 1           | 02-Mar-2021 09:00 AM | Central Bank Of Egypt |

  #EBS-7345
  Scenario: (01) Activate Collections, against NotesReceivable (Happy Path)
  """
  - Collection method is Cash
  - DocumentCurrency = CompanyLocalCurrency
  - By an authorized user
  - All mandatory fields exist
  - Amount in Collection = Amount in NotesReceivable
  - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date
  """
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000  |
   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
     #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERTSALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 2000   | 2000      |       |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType  | AmountToCollect |
      | 2021000003 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE | 2000            |

    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_NR | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type             | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | NOTES_RECEIVABLE | CASH   | 000001 - Al Ahram | 2021000003        | 2000   | EGP      |             | 0002 - Signmedia Treasury |

    When "Ahmed.Hamdi" Activates Collection based on "NOTES_RECEIVABLE" with code "2021000001" and DueDate "07-Jan-2022 09:30 AM" at "06-Jan-2021 09:30 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 06-Jan-2021 09:30 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account                   | SubAccount                | Value    |
      | DEBIT        | 10430 - Cash              | 0002 - Signmedia Treasury | 2000 EGP |
      | CREDIT       | 10208 - Notes receivables | 2021000003                | 2000 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 06-Jan-2021 09:30 AM | 07-Jan-2022 09:30 AM | 1             | 2021000001       | 2022         |
    And Remaining amount of NotesReceivable with Code "2021000003" becomes "0"
    And JournalBalance with code "TREASURY0002000100010002" is updated as follows "902000"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 06-Jan-2021 09:30 AM | Ahmed.Hamdi | 07-Jan-2022 09:30 AM | 0002 - Signmedia | 2022       | 2021000001 - Collection | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount                 | SubAccount                | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 07-Jan-2022 09:30 AM | 10430 - Cash              | 0002 - Signmedia Treasury | 0      | 2000  | EGP                | EGP             | 1                         | 2020000036       |
      | 07-Jan-2022 09:30 AM | 10208 - Notes receivables | 2021000003                | 2000   | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-7345
  Scenario: (02) Activate Collections, against NotesReceivable (Happy Path)
  """
  - Collection method is Bank
  - DocumentCurrency = CompanyLocalCurrency
  - By an authorized user
  - All mandatory fields exist
  - Amount in Collection < Amount in NotesReceivable
  """
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 1000    |
   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
     #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 2000   | 2000      |       |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType  | AmountToCollect |
      | 2021000003 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE | 2000            |

    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_NR | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type             | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury |
      | 2021000001 | NOTES_RECEIVABLE | BANK   | 000001 - Al Ahram | 2021000003        | 1500   | EGP      | 1234567893458 - EGP - Bank Misr |          |

    When "Ahmed.Hamdi" Activates Collection based on "NOTES_RECEIVABLE" with code "2021000001" and DueDate "07-Jan-2021 09:30 AM" at "06-Jan-2021 09:30 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 06-Jan-2021 09:30 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account                   | SubAccount                      | Value    |
      | DEBIT        | 10431 - Bank              | 1234567893458 - EGP - Bank Misr | 1500 EGP |
      | CREDIT       | 10208 - Notes receivables | 2021000003                      | 1500 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 06-Jan-2021 09:30 AM | 07-Jan-2021 09:30 AM | 1             | 2021000001       | 2021         |
    And Remaining amount of NotesReceivable with Code "2021000003" becomes "500"
    And JournalBalance with code "BANK00020001000100011234567893458" is updated as follows "2500"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 06-Jan-2021 09:30 AM | Ahmed.Hamdi | 07-Jan-2021 09:30 AM | 0002 - Signmedia | 2021       | 2021000001 - Collection | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount                 | SubAccount                      | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 07-Jan-2021 09:30 AM | 10431 - Bank              | 1234567893458 - EGP - Bank Misr | 0      | 1500  | EGP                | EGP             | 1                         | 2020000036       |
      | 07-Jan-2021 09:30 AM | 10208 - Notes receivables | 2021000003                      | 1500   | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
