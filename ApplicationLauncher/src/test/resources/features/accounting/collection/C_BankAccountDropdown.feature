Feature: BankAccounts DropDown

  Background:
    Given the following users and roles exist:
      | Name           | Role                            |
      | Ahmed.Al-Ashry | CollectionResponsible_Signmedia |
      | Afaf           | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                    |
      | CollectionResponsible_Signmedia | CollectionViewer_Signmedia |
      | CollectionResponsible_Signmedia | CompanyViewer              |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadAll | [purchaseUnitName='Signmedia'] |
      | CompanyViewer              | Company:ReadAll    |                                |
    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | Collection:ReadAll |
      | Afaf | Company:ReadAll    |

    And the following Companies exist:
      | Code | Name      |
      | 0002 | DigiPro   |
      | 0001 | AL Madina |

  Scenario: (01) Read All BankAccounts Dropdown in Create Collection by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all BankAccounts with CompanyCode "0002"
    Then the following BankAccounts values will be presented to "Ahmed.Al-Ashry":
      | AccountNumber                                            |
      | Alex Bank - 1516171819789 - EGP                          |
      | Alex Bank - 1516171819781 - USD                          |
      | Alex Bank - 1516171819712 - USD                          |
      | CTBC Bank CO., LTD. - 2021222324253 - USD                |
      | Qatar National Bank Al Ahli-Tanta - 26272829303132 - EGP |
    And total number of BankAccounts returned to "Ahmed.Al-Ashry" is equal to 5
#
  Scenario: (02) Read All BankAccounts Dropdown in Create Collection by an authorized user (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read all BankAccounts with CompanyCode "0001"
    Then the following BankAccounts values will be presented to "Ahmed.Al-Ashry":
      | AccountNumber                                |
      | Bank Misr - 1234567893458 - EGP              |
      | Bank Misr - 1234567893422 - EGP              |
      | Bank Misr - 1234567893499 - USD              |
      | Bank Misr - 1234567893491 - HKD              |
      | National Bank of Egypt - 1011121314678 - USD |
    And total number of BankAccounts returned to "Ahmed.Al-Ashry" is equal to 5

  @Future
  Scenario: (03) Read All BankAccounts Dropdown in Create Collection by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all BankAccounts with CompanyCode "0002"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page