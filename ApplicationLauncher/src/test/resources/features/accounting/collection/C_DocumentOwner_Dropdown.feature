#Author: Niveen Magdy
#Reviewer: Hossam Hassan
Feature: Document Owner DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                            |
      | Shady.Abdelatif | Accountant_Signmedia            |

      | Ahmed.Al-Ashry  | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury                   |

      | Afaf            | FrontDesk                       |
    And the following roles and sub-roles exist:
      | Role                            | Subrole                 |
      | Accountant_Signmedia            | DocumentOwnerViewer     |
      | AdminTreasury                   | DocumentOwnerViewer     |

      | CollectionResponsible_Signmedia | CollectionDocumentOwner |
      | AdminTreasury                   | CollectionDocumentOwner |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission                    | Condition |
      | DocumentOwnerViewer     | DocumentOwner:ReadAll         |           |

      | CollectionDocumentOwner | Collection:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission            |
      | Afaf | DocumentOwner:ReadAll |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | Collection     | CanBeDocumentOwner |           |
      | 16      | Ahmed Hamdy    | Collection     | CanBeDocumentOwner |           |

  #EBS-7467
  Scenario: (01) Read All DocumentOwners Dropdown in Collection creation by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all DocumentOwners for "Collection"
    Then the following DocumentOwners values will be presented to "Shady.Abdelatif":
      | Name           |
      | Ahmed Al-Ashry |
      | Ahmed Hamdy    |
    And total number of DocumentOwners returned to "Shady.Abdelatif" in a dropdown is equal to 2

  #EBS-7467
  Scenario: (02) Read All DocumentOwners in Collection creation by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "Collection"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
