# Author : Niveen (EBS-4809), Ahmed Ali (EBS-9029)
# Reviewer: Ahmad Hamed (08-11-2020)
Feature: Collection - View Accounting Details Section

  Background: 
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | AdminTreasury        | CollectionViewer           |
      | SuperUser            | SuperUserSubRole           |
    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                       | Condition                      |
      | CollectionViewer_Signmedia | Collection:ReadAccountingDetails | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadAccountingDetails |                                |
      | SuperUserSubRole           | *:*                              |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                       | Condition                  |
      | Shady.Abdelatif | Collection:ReadAccountingDetails | [purchaseUnitName='Flexo'] |

  #EBS-4809
  Scenario: (01) View AccountingDetails section in Collection (Happy Path)
    """
    - By an authorized user who has one role without conditions
    - All values exist
    - Collection Method is CASH
    - Collection Currency is EGP
    """

    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit |
      | 2019000013 | 0001 - AL Madina | 0001 - Flexo   |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit | Company          |
      | 2021000001 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount      | Currency | BankAccount | Treasury              |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019000013 | 4700.266345 | EGP      |             | 0001 - Flexo Treasury |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount            | Value           |
      | 2021000001 | DEBIT        | 10430 - Cash      | 0001 - Flexo Treasury | 4700.266345 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram     | 4700.266345 EGP |
    When "Ahmed.Hamdi" requests to view AccountingDetails section of Collection with code "2021000001"
    Then the following values of AccountingDetails section for Collection with code "2021000001" are displayed to "Ahmed.Hamdi":
      | CCode      | Credit/Debit | Account           | SubAccount            | Value           |
      | 2021000001 | DEBIT        | 10430 - Cash      | 0001 - Flexo Treasury | 4700.266345 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram     | 4700.266345 EGP |

  #EBS-4809
  Scenario: (02) View AccountingDetails section in Collection (Happy Path)
    """
    - By an authorized user who has one role without conditions
    - All values exist
    - Collection Method is BANK
    - Collection Currency is USD
    """

    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit |
      | 2019000013 | 0001 - AL Madina | 0001 - Flexo   |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit | Company          |
      | 2021000001 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                                  | Treasury |
      | 2021000001 | SALES_INVOICE | BANK   | 000001 - Al Ahram |        2019000013 | 300.25 | USD      | 1011121314678 - USD - National Bank of Egypt |          |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount                                   | Value      |
      | 2021000001 | DEBIT        | 10431 - Bank      | 1011121314678 - USD - National Bank of Egypt | 300.25 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram                            | 300.25 EGP |
    When "Ahmed.Hamdi" requests to view AccountingDetails section of Collection with code "2021000001"
    Then the following values of AccountingDetails section for Collection with code "2021000001" are displayed to "Ahmed.Hamdi":
      | CCode      | Credit/Debit | Account           | SubAccount                                   | Value      |
      | 2021000001 | DEBIT        | 10431 - Bank      | 1011121314678 - USD - National Bank of Egypt | 300.25 USD |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram                            | 300.25 USD |

  #EBS-4809
  Scenario: (03) View AccountingDetails section in Collection (Happy Path)
    """
    - By an authorized user who has one role and a condition
    - All values exist
    - Collection Method is CASH
    - Collection Currency is EGP
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000013 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019000013 | 900.54 | EGP      |             | 0002 - Signmedia Treasury |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount                | Value      |
      | 2021000001 | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 900.54 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 900.54 EGP |
    When "Shady.Abdelatif" requests to view AccountingDetails section of Collection with code "2021000001"
    Then the following values of AccountingDetails section for Collection with code "2021000001" are displayed to "Shady.Abdelatif":
      | CCode      | Credit/Debit | Account           | SubAccount                | Value      |
      | 2021000001 | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 900.54 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 900.54 EGP |

  #EBS-4809
  Scenario: (04) View AccountingDetails section in Collection (Happy Path)
    """
    - By an authorized user who has one role and a condition
    - No AccountingDetails exist
    - Collection is Draft
    - Collection Method is CASH
    - Collection Currency is EGP
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000013 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019000013 | 900.54 | EGP      |             | 0002 - Signmedia Treasury |
    When "Shady.Abdelatif" requests to view AccountingDetails section of Collection with code "2021000001"
    Then an empty AccountingDetails section for Collection with code "2021000001" is displayed to "Shady.Abdelatif"

  #EBS-4809
  Scenario: (05) (01) View AccountingDetails section in Collection (Abuse Case)
    """
    - By an unauthorized user
    - All values exist
    - Collection Method is CASH
    - Collection Currency is EGP
    """

    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit |
      | 2019000013 | 0001 - AL Madina | 0001 - Flexo   |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Active | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit | Company          |
      | 2021000001 | 0001 - Flexo | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount      | Currency | BankAccount | Treasury              |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019000013 | 4700.266345 | EGP      |             | 0001 - Flexo Treasury |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount            | Value           |
      | 2021000001 | DEBIT        | 10430 - Cash      | 0001 - Flexo Treasury | 4700.266345 EGP |
      | 2021000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram     | 4700.266345 EGP |
    When "Shady.Abdelatif" requests to view AccountingDetails section of Collection with code "2021000001"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-4809
  Scenario: (06) View AccountingDetails section in Collection, where Collection doesn't exist (Exception Case)
    """
    - By an authorized user who has one role and a condition
    - No values exist
    - Collection is Draft
    - Another user removes the Collection
    """

    Given user is logged in as "Shady.Abdelatif"
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019000013 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019000013 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019000013 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019000013 |      7000 |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_SI | Draft | Admin     | 5-Jan-2021 9:02 AM | CUSTOMER            | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2021000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019000013 | 900.54 | EGP      |             | 0002 - Signmedia Treasury |
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view AccountingDetails section of Collection with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
