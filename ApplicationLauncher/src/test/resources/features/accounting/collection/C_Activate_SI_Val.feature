# Author: Niveen Magdy (EBS-1682)
# Reviewer: Ahmad Hamed (02-12-2020)
Feature: Collection - Activate Val

  Background: 
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
      | hr1         | SuperUser     |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
      | SuperUser     | SuperUserSubRole        |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
      | SuperUserSubRole        | *:*                 |           |
    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                              | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100007 | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company        | PurchasingUnit   |
      | 2019100007 | 0002 - DigiPro | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100007 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100007 |   7080.25 |
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance   |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 900000.00 |
      | TREASURY0002000200010002 | 0002 - Signmedia | 0002 - DigiPro   | 0001 - EGP | 0002 - Signmedia Treasury | 900000.00 |
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company          | Currency   | BankAccountNumber               | Balance  |
      | BANK00020001000100011234567893458 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 1234567893458 - EGP - Bank Misr | 40000.00 |
      | BANK00020002000200031516171819789 | 0002 - Signmedia | 0002 - DigiPro   | 0002 - USD | 1516171819781 - USD - Alex Bank | 40000.00 |

  #EBS-1680 ,EBS-6076
  Scenario Outline: (01) Activate Collection, against SalesInvoice Val - missing or malicious input (Abuse case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
      | 2019000002 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2019000002 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2019000002 | SALES_INVOICE | BANK   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      | 1234567893458 - EGP - Bank Misr |                           |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "<CCode>" and DueDate "<DueDate>" at "09-Nov-2020 11:00 AM"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

    Examples: 
      | CCode      | DueDate |
      #CASH
      | 2019000001 |         |
      | 2019000001 | Ay7aga  |
      #BANK
      | 2019000002 |         |
      | 2019000002 | Ay7aga  |

  #EBS-1680 ,EBS-6076
  Scenario: (02) Activate Collection, against SalesInvoice Val - with an invalid state - Active State (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Active | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |             | 0002 - Signmedia Treasury |
    #@INSERT
    And insert AccountingDetails for Collections as follow:
      | Code       | Credit/Debit | Account           | SubAccount                | Value    |
      | 2019000001 | DEBIT        | 10430 - Cash      | 0002 - Signmedia Treasury | 1000 EGP |
      | 2019000001 | CREDIT       | 10203 - Customers | 000001 - Al Ahram         | 1000 EGP |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2019000001" and DueDate "08-Nov-2020 11:00 AM" at "09-Nov-2020 11:00 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-32"

  #EBS-1680 ,EBS-6076
  Scenario: (03) Activate Collection, against SalesInvoice Val - when CollectionDetails section is locked (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |             | 0002 - Signmedia Treasury |
    And another user is logged in as "hr1"
    And "hr1" requests to edit CollectionDetails section of Collection with code "2019000001" at "09-Nov-2020 11:00 AM"
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2019000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-14"

  #EBS-1680 ,EBS-6076
  Scenario: (04) Activate Collection, against SalesInvoice Val - where Collection doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |             | 0002 - Signmedia Treasury |
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2019000001" successfully
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2019000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"

  #EBS-1680 ,EBS-6076
  Scenario: (05) Activate Collection, against SalesInvoice Val , where Collection has amount greater than the Total SalesInvoice Remaining (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury                  |
      | 2019000001 | SALES_INVOICE | CASH   | 000001 - Al Ahram |        2019100007 |  10000 | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2019000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "C-msg-01"

  # EBS-1680 ,EBS-6076
  Scenario: (06) Activate Collection, against SalesInvoice Val ,  mandatory fields (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner            |
      | 2019000001 | C_SI | Draft | Admin from BDKCompanyCode | 04-Dec-2019 09:00 AM | CUSTOMER            | 1000072 - Ahmed Al-Ashry |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type          | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount | Treasury |
      | 2019000001 | SALES_INVOICE | BANK   | 000001 - Al Ahram |        2019100007 |   1000 | EGP      |             |          |
    When "Ahmed.Hamdi" Activates Collection based on "SALES_INVOICE" with code "2019000001" and DueDate "10-Nov-2020 11:00 AM" at "09-Nov-2020 11:20 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-40"
    And the following fields "bankAccountCode" which sent to "Ahmed.Hamdi" are marked as missing
