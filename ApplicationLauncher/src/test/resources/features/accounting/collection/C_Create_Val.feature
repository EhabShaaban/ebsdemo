#Author: Gehad , Evraam,Niveen, Ahmed Hamed, Hossam Hassan
#Reviewer: Marina

Feature: Create Collection For Notes Receivable / Sales Invoice / SalesOrder - Validation

  Background:

    Given the following users and roles exist:
      | Name            | Role                            |
      | Shady.Abdelatif | Accountant_Signmedia            |
      | Ahmed.Al-Ashry  | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury                   |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                         |
      | Accountant_Signmedia            | CollectionOwner_Signmedia       |
      | AdminTreasury                   | CollectionOwner                 |
      | Accountant_Signmedia            | DocumentOwnerViewer             |
      | AdminTreasury                   | DocumentOwnerViewer             |
      | Accountant_Signmedia            | CollectionTypeViewer            |
      | AdminTreasury                   | CollectionTypeViewer            |
      | Accountant_Signmedia            | CollectionMethodViewer          |
      | AdminTreasury                   | CollectionMethodViewer          |
      | Accountant_Signmedia            | SalesInvoiceViewer_Signmedia    |
      | AdminTreasury                   | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia            | NotesReceivableViewer_Signmedia |
      | AdminTreasury                   | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia            | SOViewer_Signmedia              |
      | AdminTreasury                   | SOViewer_Signmedia              |
      | CollectionResponsible_Signmedia | CollectionDocumentOwner         |
      | AdminTreasury                   | CollectionDocumentOwner         |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                    | Condition                      |
      | CollectionOwner_Signmedia       | Collection:Create             |                                |
      | CollectionOwner                 | Collection:Create             |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll         |                                |
      | CollectionTypeViewer            | CollectionType:ReadAll        |                                |
      | CollectionMethodViewer          | CollectionMethod:ReadAll      |                                |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll          | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll       | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll            | [purchaseUnitName='Signmedia'] |
      | CollectionDocumentOwner         | Collection:CanBeDocumentOwner |                                |

    And the following CollectionTypes exist:
      | Code             | Name                        |
      | SALES_INVOICE    | Against Sales Invoice       |
      | NOTES_RECEIVABLE | Against Notes Receivable    |
      | SALES_ORDER      | DownPayment for Sales Order |
    And the following DocumentOwners exist:
      | Id      | Name           | BusinessObject | Permission         | Condition |
      | 1000072 | Ahmed Al-Ashry | Collection     | CanBeDocumentOwner |           |
    And the following SalesInvoices exist:
      | Code       | State  | Type                                                              | BusinessUnit     |
      | 2019000001 | Active | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2019000005 | Draft  | SALES_INVOICE_WITHOUT_REFERENCE - Sales invoice without reference | 0002 - Signmedia |
      | 2020000005 | Active | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order     | 0002 - Signmedia |

    And the following Company and Currency and Customer and RemainingAmount exist in the following SalesInvoices:
      | SICode     | Company          | Currency | Customer                       | RemainingAmount |
      | 2019000001 | 0002 - DigiPro   | EGP      | 000001 - Al Ahram              | 0               |
      | 2019000005 | 0002 - DigiPro   | EGP      | 000001 - Al Ahram              | 134910          |
      | 2020000005 | 0001 - AL Madina | EGP      | 000007 - مطبعة أكتوبر الهندسية | 544.0           |


    And the following NotesReceivables exist:
      | Code       | State  | BusinessUnit     |
      | 2019000054 | Active | 0002 - Signmedia |
      | 2019000001 | Draft  | 0002 - Signmedia |

    And the following Company and Currency and Customer and RemainingAmount exist in the following NotesReceivables:
      | NRCode     | Company          | Currency | Customer          | RemainingAmount |
      | 2019000054 | 0001 - AL Madina | EGP      | 000001 - Al Ahram | 333.0           |
      | 2019000001 | 0002 - DigiPro   | EGP      | 000001 - Al Ahram | 100             |

  #EBS-4363
    And the following SalesOrders exist:
      | Code       | State                 | BusinessUnit     |
      | 2020000070 | SalesInvoiceActivated | 0002 - Signmedia |
      | 2020000069 | Approved              | 0002 - Signmedia |
      | 2020000056 | DeliveryComplete      | 0002 - Signmedia |
      | 2020000055 | Expired               | 0002 - Signmedia |
      | 2020000054 | Canceled              | 0002 - Signmedia |
      | 2020000052 | WaitingApproval       | 0002 - Signmedia |
      | 2020000051 | Rejected              | 0002 - Signmedia |
      | 2020000012 | ReadyForDelivery      | 0002 - Signmedia |
      | 2020000009 | Draft                 | 0002 - Signmedia |

    #EBS-7432
    And the following SalesOrders have the following remainings:
      | Code       | TotalRemaining |
      | 2020000070 | 10880.000      |
      | 2020000069 | 0              |
      | 2020000056 | 14858.000      |
      | 2020000055 | 0              |
      | 2020000054 | 0              |
      | 2020000052 | 0              |
      | 2020000051 | 0              |
      | 2020000012 | 544.000        |
      | 2020000009 | 0              |

## EBS-5989 -- EBS-6226 -- EBS-7490 -- #EBS-4363
  Scenario Outline: (01) Create Collection Validation, against NotesReceivable and SalesInvoice, with missing mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method   | CollectionType   | RefDocument   | DocumentOwnerId   |
      | <Method> | <CollectionType> | <RefDocument> | <DocumentOwnerId> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | Method | CollectionType   | RefDocument | DocumentOwnerId |
      | ""     | NOTES_RECEIVABLE | 2019000054  | 1000072         |
      |        | NOTES_RECEIVABLE | 2019000054  | 1000072         |
      | ""     | SALES_INVOICE    | 2019000001  | 1000072         |
      |        | SALES_INVOICE    | 2019000001  | 1000072         |
  #EBS-4363
      | ""     | SALES_ORDER      | 2020000069  | 1000072         |
      |        | SALES_ORDER      | 2020000069  | 1000072         |

      | BANK   | NOTES_RECEIVABLE | 2019000054  | ""              |
      | BANK   | NOTES_RECEIVABLE | 2019000054  |                 |
      | BANK   | SALES_INVOICE    | 2019000001  | ""              |
      | BANK   | SALES_INVOICE    | 2019000001  |                 |
  #EBS-4363
      | BANK   | SALES_ORDER      | 2020000069  | ""              |
      | BANK   | SALES_ORDER      | 2020000069  |                 |

      | BANK   |                  | 2019000001  | 1000072         |
      | BANK   | ""               | 2019000001  | 1000072         |
      | BANK   | NOTES_RECEIVABLE | ""          | 1000072         |
      | BANK   | NOTES_RECEIVABLE |             | 1000072         |
      | BANK   | SALES_INVOICE    | ""          | 1000072         |
      | BANK   | SALES_INVOICE    |             | 1000072         |
  #EBS-4363
      | BANK   | SALES_ORDER      | ""          | 1000072         |
      | BANK   | SALES_ORDER      |             | 1000072         |


 # EBS-5989 -- EBS-6226 -- EBS-7490 -- #EBS-4363 -- #EBS-7432
  Scenario Outline: (02) Create Collection Validation, with malicious input (Abuse Case\Clientbypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method   | CollectionType   | RefDocument   | DocumentOwnerId   |
      | <Method> | <CollectionType> | <RefDocument> | <DocumentOwnerId> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | Method | DocumentOwnerId | CollectionType   | RefDocument  |
# create Collection with invalid data due to schema
      | ay7aga | 1000072         | NOTES_RECEIVABLE | 2019000054   |
      | ay7aga | 1000072         | SALES_INVOICE    | 2019000001   |
      | ay7aga | 1000072         | SALES_ORDER      | 2020000069   |
      | CASH   | ay7aga          | NOTES_RECEIVABLE | 2019000054   |
      | BANK   | ay7aga          | SALES_INVOICE    | 2019000001   |
      | BANK   | ay7aga          | SALES_ORDER      | 2020000069   |
      | CASH   | 1000072         | ay7aga           | 2019000054   |
      | BANK   | 1000072         | ay7aga           | 2019000001   |
      | BANK   | 1000072         | ay7aga           | 2020000069   |
      | BANK   | 1000072         | NOTES_RECEIVABLE | ay7aga       |
      | CASH   | 1000072         | SALES_INVOICE    | ay7aga       |
      | CASH   | 1000072         | SALES_ORDER      | ay7aga       |
      | CASH   | 9999.999        | NOTES_RECEIVABLE | 2019000054   |
      | BANK   | 1000072         | NOTES_RECEIVABLE | 999999999999 |
# create Collection with Does not exist DocumentOwner, documentRef
      | BANK   | 1000072         | NOTES_RECEIVABLE | 9999999999   |
      | BANK   | 1000072         | SALES_INVOICE    | 9999999999   |
      | BANK   | 1000072         | SALES_ORDER      | 9999999999   |
# create Collection with reference document in draft state
      | CASH   | 1000072         | SALES_INVOICE    | 2019000005   |
      | BANK   | 1000072         | NOTES_RECEIVABLE | 2019000001   |
  #EBS-4363
  # create Collection based on sales order with reference document in invalid state
      #draft
      | BANK   | 1000072         | SALES_ORDER      | 2020000009   |
      #WaitingApproval
      | BANK   | 1000072         | SALES_ORDER      | 2020000052   |
      #Rejected
      | BANK   | 1000072         | SALES_ORDER      | 2020000051   |
      #Canceled
      | BANK   | 1000072         | SALES_ORDER      | 2020000054   |
      #Expired
      | BANK   | 1000072         | SALES_ORDER      | 2020000055   |
      #DeliveryComplete
      | BANK   | 1000072         | SALES_ORDER      | 2020000056   |
      #EBS-7432
      # create Collection based on sales invoice with remaining 0
      | BANK   | 1000072         | SALES_INVOICE    | 2019000001   |
      # create Collection based on sales order with remaining 0
      | BANK   | 1000072         | SALES_ORDER      | 2020000069   |

  #EBS-7490 #EBS-4363
  Scenario Outline: (03)  Create Collection Validation, with incorrect data entry (Validation Failure)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates Collection with the following values:
      | Method   | CollectionType   | RefDocument   | DocumentOwnerId   |
      | <Method> | <CollectionType> | <RefDocument> | <DocumentOwnerId> |
    Then a failure notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-05"
    And the following error message is attached to "<Field>" field "<ErrMsg>" and sent to "Shady.Abdelatif"

    Examples:
      | Method | DocumentOwnerId | CollectionType | RefDocument | Field           | ErrMsg     |
        #EBS-4363
      #salesOrder is salesInvoiceActivated
      | CASH   | 1000072         | SALES_ORDER    | 2020000070  | refDocumentCode | C-msg-04   |
        #EBS-4363
      #salesOrder is ReadyForDelivery
      | CASH   | 1000072         | SALES_ORDER    | 2020000012  | refDocumentCode | C-msg-04   |
      #Not exist DocumentOwnerId
      | CASH   | 9999999         | SALES_INVOICE  | 2020000005  | documentOwnerId | Gen-msg-48 |
