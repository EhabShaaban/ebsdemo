# Author: Ahmad Hamed, Engy Mohamed (EBS-4454)
# Reviewer:

Feature: Allowed Actions Collection

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Homos           | ReadOnlyUser         |
      | Shady.Abdelatif | Accountant_Signmedia |
      | CreateOnlyUser  | CreateOnlyRole       |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                    |
      | ReadOnlyUser         | CollectionViewer           |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | Accountant_Signmedia | CollectionOwner_Signmedia  |
      | CreateOnlyRole       | CreateOnlySubRole          |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | CollectionOwner_Signmedia  | Collection:Create  |                                |
      | CollectionViewer_Signmedia | Collection:ReadAll | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadAll |                                |
      | CreateOnlySubRole          | Collection:Create  |                                |

    And the following users doesn't have the following permissions:
      | User           | Permission         |
      | Homos          | Collection:Create  |
      | Afaf           | Collection:Create  |
      | Afaf           | Collection:ReadAll |
      | CreateOnlyUser | Collection:ReadAll |

    #EBS-4454
  Scenario Outline: (01)  Read allowed actions in Collection Home Screen by authorized user
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of Collection home screen
    Then the "<AllowedActions>" are displayed to "<User>"
    Examples:
      | User            | AllowedActions  |
      | Homos           | ReadAll         |
      | Shady.Abdelatif | ReadAll, Create |
      | CreateOnlyUser  | Create          |

  Scenario: (02) Read allowed actions in Collection Home Screen By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of Collection home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"