#Author: Hossam Hassan
#Reviewer: Ahmad Hamed (13-10-2020)

Feature: Collection - View GeneralData section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | hr1             | SuperUser            |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ahmed.Hamdi     | AdminTreasury        |

    And the following roles and sub-roles exist:
      | Role                 | Sub-role                   |
      | SuperUser            | SuperUserSubRole           |
      | Accountant_Signmedia | CollectionViewer_Signmedia |
      | AdminTreasury        | CollectionViewer           |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission                 | Condition                      |
      | SuperUserSubRole           | *:*                        |                                |
      | CollectionViewer_Signmedia | Collection:ReadGeneralData | [purchaseUnitName='Signmedia'] |
      | CollectionViewer           | Collection:ReadGeneralData |                                |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                 | Condition                  |
      | Shady.Abdelatif | Collection:ReadGeneralData | [purchaseUnitName='Flexo'] |

    And the following GeneralData for the following Collections exists:
      | CCode      | State  | DocumentOwner            | CreatedBy                 | CreationDate         | BusinessUnit     | CollectionType | Company          | CompanyLocalCurrencyISO |
      | 2020000004 | Draft  | 1000072 - Ahmed Al-Ashry | A.Hamed                   | 4-Oct-2020 09:00 AM  | 0001 - Flexo     | CUSTOMER       | 0001 - AL Madina | EGP                     |
      | 2020000003 | Active | 1000072 - Ahmed Al-Ashry | A.Hamed                   | 4-Oct-2020 09:00 AM  | 0002 - Signmedia | CUSTOMER       | 0001 - AL Madina | EGP                     |
      | 2020000002 | Draft  | 1000072 - Ahmed Al-Ashry | hr1                       | 29-Sep-2020 09:00 AM | 0002 - Signmedia | CUSTOMER       | 0001 - AL Madina | EGP                     |
      | 2020000001 | Draft  | 1000072 - Ahmed Al-Ashry | A.Hamed                   | 29-Sep-2020 09:00 AM | 0002 - Signmedia | CUSTOMER       | 0001 - AL Madina | EGP                     |
      | 2019000001 | Draft  | 1000072 - Ahmed Al-Ashry | Admin from BDKCompanyCode | 04-Dec-2019 11:27 AM | 0002 - Signmedia | CUSTOMER       | 0002 - DigiPro   | EGP                     |

#EBS-4324
  Scenario Outline: (01) View GeneralData section in Collection, by an authorizd user who has one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of Collection with code "<Code>"
    Then the following values of GeneralData section for Collection with code "<Code>" are displayed to "Shady.Abdelatif":
      | CCode  | Type   | CreatedBy   | State   | CreationDate   | DocumentOwner   |
      | <Code> | <Type> | <CreatedBy> | <State> | <CreationDate> | <DocumentOwner> |
    Examples:
      | Code       | Type                                        | CreatedBy                 | State  | CreationDate         | DocumentOwner  |
      | 2020000003 | SALES_ORDER - DownPayment for Sales Order   | A.Hamed                   | Active | 4-Oct-2020 09:00 AM  | Ahmed Al-Ashry |
      | 2020000002 | NOTES_RECEIVABLE - Against Notes Receivable | hr1                       | Draft  | 29-Sep-2020 09:00 AM | Ahmed Al-Ashry |
      | 2019000001 | SALES_INVOICE - Against Sales Invoice       | Admin from BDKCompanyCode | Draft  | 04-Dec-2019 11:27 AM | Ahmed Al-Ashry |

#EBS-4324
  Scenario Outline: (02) View GeneralData section in Collection, by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view GeneralData section of Collection with code "<Code>"
    Then the following values of GeneralData section for Collection with code "<Code>" are displayed to "Ahmed.Hamdi":
      | CCode  | Type   | CreatedBy   | State   | CreationDate   | DocumentOwner   |
      | <Code> | <Type> | <CreatedBy> | <State> | <CreationDate> | <DocumentOwner> |
    Examples:
      | Code       | Type                                      | CreatedBy | State  | CreationDate        | DocumentOwner  |
      | 2020000003 | SALES_ORDER - DownPayment for Sales Order | A.Hamed   | Active | 4-Oct-2020 09:00 AM | Ahmed Al-Ashry |
      | 2020000004 | SALES_ORDER - DownPayment for Sales Order | A.Hamed   | Draft  | 4-Oct-2020 09:00 AM | Ahmed Al-Ashry |

#EBS-4324
  Scenario: (03) View GeneralData section in Collection, by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of Collection with code "2020000004"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

#EBS-4324
  Scenario: (04) View GeneralData section in Collection, where Collection doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the Collection with code "2020000001" successfully
    When "Shady.Abdelatif" requests to view GeneralData section of Collection with code "2020000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"
