# Author: Ahmed Ali, Khadrah ALi
# Reviewer:
Feature: Collection Based on Debit Notes - Activate HP

  Background:

  - Activating Collection Based on Debit Notes results in the following:
  - Collection state is changed to Active
  - Collection Activation Details are determined
  - Collection Accounting Details are determined
  - the retrieved fiscal year is the active one where the due date belongs to it
  - latest ExchangeRate is retrieved based on of default strategy
  - Journal Entries for Collection is created, and Activation Details determined where
  - The referenced Debit Notes remaining is updated
  - Related JournalBalances are increased according to Collection amount (EBS-8510)

    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                 |
      | AdminTreasury | CollectionActivateOwner |
    And the following sub-roles and permissions exist:
      | Subrole                 | Permission          | Condition |
      | CollectionActivateOwner | Collection:Activate |           |
    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |
    And the following ExchangeRate strategies exists:
      | Code                  | Name                  | isDefault |
      | USER_DAILY_RATE       | User Daily Rate       | true      |
      | CENTRAL_BANK_OF_EGYPT | Central Bank Of Egypt | false     |
    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Feb-2021 09:00 AM | User Daily Rate       |
      | 2020000036 | EGP              | EGP               | 1           | 01-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 01-Mar-2021 09:00 AM | Central Bank Of Egypt |
      | 2020000038 | USD              | EGP               | 15.65       | 02-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000039 | EGP              | EGP               | 1           | 02-Mar-2021 09:00 AM | Central Bank Of Egypt |

  #EBS-7625 #EBS-7983 #EBS-7626 #EBS-7696
  Scenario: (01) Activate Collections, against DebitNote, where collection method is Cash (Happy Path)
  """
    - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date
  """
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following TreasuryJournalBalances:
      | Code                     | BusinessUnit     | Company          | Currency   | Treasury                  | Balance      |
      | TREASURY0002000100010002 | 0002 - Signmedia | 0001 - AL Madina | 0001 - EGP | 0002 - Signmedia Treasury | 56245.269542 |
    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                           | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining  |
      | 2021000001 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION | Posted | Admin     | 02-Jan-2021 11:00 AM | Ashraf Salah  | 1256.32569 |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount     | Currency   |
      | 2021000001 | 000052 - Zhejiang | 1256.32569 | 0001 - EGP |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_DN | Draft | Admin     | 5-Jan-2021 9:02 AM | VENDOR              | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type       | Method | BusinessPartner   | ReferenceDocument | Amount     | Currency | BankAccount | Treasury                  |
      | 2021000001 | DEBIT_NOTE | CASH   | 000052 - Zhejiang | 2021000001        | 644.325984 | EGP      |             | 0002 - Signmedia Treasury |
    When "Ahmed.Hamdi" Activates Collection based on "DEBIT_NOTE" with code "2021000001" and DueDate "16-Mar-2022 11:00 AM" at "17-Mar-2021 11:00 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 17-Mar-2021 11:00 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account                | SubAccount                | Value          |
      | DEBIT        | 10430 - Cash           | 0002 - Signmedia Treasury | 644.325984 EGP |
      | CREDIT       | 20104 - Import Vendors | 000052 - Zhejiang         | 644.325984 EGP |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 17-Mar-2021 11:00 AM | 16-Mar-2022 11:00 AM | 1             | 2021000001       | 2022         |
    And Remaining amount of DebitNote with Code "2021000001" becomes "611.999706"
    And JournalBalance with code "TREASURY0002000100010002" is updated as follows "56889.595526"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company          |
      | 2021000001 | 17-Mar-2021 11:00 AM | Ahmed.Hamdi | 16-Mar-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000001 - Collection | 0001 - AL Madina |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount              | SubAccount                | Credit     | Debit      | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 16-Mar-2022 11:00 AM | 10430 - Cash           | 0002 - Signmedia Treasury | 0          | 644.325984 | EGP                | EGP             | 1                         | 2020000036       |
      | 16-Mar-2022 11:00 AM | 20104 - Import Vendors | 000052 - Zhejiang         | 644.325984 | 0          | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"

  #EBS-7625 #EBS-7983 #EBS-7626 #EBS-7696
  Scenario: (02) Activate Collections, against DebitNote, where collection method is Bank (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And Last Journal Entry Code is "2019000004"
    #@INSERT
    Given insert the following BankJournalBalances:
      | Code                              | BusinessUnit     | Company        | Currency   | BankAccountNumber               | Balance |
      | BANK00020002000200031516171819781 | 0002 - Signmedia | 0002 - DigiPro | 0002 - USD | 1516171819781 - USD - Alex Bank | 40000   |
    #@INSERT
    And the following GeneralData for AccountingNote exist:
      | Code       | NoteType | Type                           | State  | CreatedBy | CreationDate         | DocumentOwner | Remaining |
      | 2021000001 | DEBIT    | DEBIT_NOTE_BASED_ON_COMMISSION | Posted | Admin     | 02-Jan-2021 11:00 AM | Ashraf Salah  | 3000      |
    #@INSERT
    And the following CompanyData for AccountingNote exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following NoteDetails for AccountingNote exist:
      | Code       | BusinessPartner   | Amount | Currency   |
      | 2021000001 | 000052 - Zhejiang | 3000   | 0002 - USD |
    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State | CreatedBy | CreationDate       | BusinessPartnerType | DocumentOwner |
      | 2021000001 | C_DN | Draft | Admin     | 5-Jan-2021 9:02 AM | VENDOR              | Gehan Ahmed   |
    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type       | Method | BusinessPartner   | ReferenceDocument | Amount | Currency | BankAccount                     | Treasury |
      | 2021000001 | DEBIT_NOTE | BANK   | 000052 - Zhejiang | 2021000001        | 900    | USD      | 1516171819781 - USD - Alex Bank |          |
    When "Ahmed.Hamdi" Activates Collection based on "DEBIT_NOTE" with code "2021000001" and DueDate "16-Mar-2021 11:00 AM" at "17-Mar-2021 11:00 AM"
    Then Collection with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 17-Mar-2021 11:00 AM | Active |
    And AccountingDetails in Collection with Code "2021000001" is updated as follows:
      | Credit/Debit | Account                | SubAccount                      | Value   |
      | DEBIT        | 10431 - Bank           | 1516171819781 - USD - Alex Bank | 900 USD |
      | CREDIT       | 20104 - Import Vendors | 000052 - Zhejiang               | 900 USD |
    And ActivationDetails in Collection with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | DueDate              | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 17-Mar-2021 11:00 AM | 16-Mar-2021 11:00 AM | 15.65         | 2021000001       | 2021         |
    And Remaining amount of DebitNote with Code "2021000001" becomes "2100"
    And JournalBalance with code "BANK00020002000200031516171819781" is updated as follows "40900"
    And the following Journal Entry is Created:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument       | Company        |
      | 2021000001 | 17-Mar-2021 11:00 AM | Ahmed.Hamdi | 16-Mar-2021 11:00 AM | 0002 - Signmedia | 2021       | 2021000001 - Collection | 0002 - DigiPro |
    And The following Journal Items for Journal Entry with code "2021000001":
      | JournalDate          | GLAccount              | SubAccount                      | Credit | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 16-Mar-2021 11:00 AM | 10431 - Bank           | 1516171819781 - USD - Alex Bank | 0      | 900   | USD                | EGP             | 15.65                     | 2020000038       |
      | 16-Mar-2021 11:00 AM | 20104 - Import Vendors | 000052 - Zhejiang               | 900    | 0     | USD                | EGP             | 15.65                     | 2020000038       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
