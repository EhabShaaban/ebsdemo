# Author: Ahmad Hamed, Hossam Hassan, Engy Mohamed (EBS-7372)
# Reviewer: Niveen Magdy (23-11-2020)
Feature: Collection - Save Collection Details section (HP)

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |

    And the following roles and sub-roles exist:
      | Role          | Subrole         |
      | AdminTreasury | CollectionOwner |
      | AdminTreasury | CompanyViewer   |

    And the following sub-roles and permissions exist:
      | Subrole         | Permission                         | Condition |
      | CollectionOwner | Collection:UpdateCollectionDetails |           |
      | CompanyViewer   | Company:ReadAll                    |           |

    And the following Collections exist:
      | Code       | State | BusinessUnit     |
      | 2019000001 | Draft | 0002 - Signmedia |
      | 2019000051 | Draft | 0002 - Signmedia |
      | 2020000001 | Draft | 0002 - Signmedia |
      | 2019000060 | Draft | 0002 - Signmedia |
      | 2020000002 | Draft | 0002 - Signmedia |
      | 2020000004 | Draft | 0001 - Flexo     |

    And the following Company Data for Collections exist:
      | CCode      | Company          | BusinessUnit     |
      | 2019000001 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019000051 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2020000001 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019000060 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2020000002 | 0001 - AL Madina | 0002 - Signmedia |
      | 2020000004 | 0001 - AL Madina | 0001 - Flexo     |

    And the following BankAccounts exist for the following companies:
      | Id | Company          | Bank                                     | AccountNumber        |
      | 14 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893458 - EGP  |
      | 15 | 0001 - AL Madina | 0002 - National Bank of Egypt            | 1011121314678 - USD  |
      | 21 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893422 - EGP  |
      | 22 | 0001 - AL Madina | 0001 - Bank Misr                         | 1234567893499 - USD  |
      | 17 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819789 - EGP  |
      | 18 | 0002 - DigiPro   | 0004 - CTBC Bank CO., LTD.               | 2021222324253 - USD  |
      | 19 | 0002 - DigiPro   | 0005 - Qatar National Bank Al Ahli-Tanta | 26272829303132 - EGP |
      | 20 | 0002 - DigiPro   | 0003 - Alex Bank                         | 1516171819712 - USD  |

    And the Company "0001 - AL Madina" has the following Treasuries:
      | Code | Name               |
      | 0001 | Flexo Treasury     |
      | 0002 | Signmedia Treasury |
      | 0003 | Offset Treasury    |
      | 0004 | Digital Treasury   |

    And the Company "0002 - DigiPro" has the following Treasuries:
      | Code | Name                |
      | 0001 | Flexo Treasury      |
      | 0002 | Signmedia Treasury  |
      | 0003 | Offset Treasury     |
      | 0004 | Digital Treasury    |
      | 0005 | Textile Treasury    |
      | 0006 | Corrugated Treasury |

    And the following CollectionDetails for the following Collections exists:
      | CCode      | ReferenceDocumentType | ReferenceDocumentCode | Treasury                  | BankAccount                     | BusinessPartner                | Currency | Amount | Method |
     #CASH Collections
      | 2019000001 | SALES_INVOICE         | 2019100007            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 1000   | CASH   |
      | 2019000051 | NOTES_RECEIVABLE      | 2019000051            | 0002 - Signmedia Treasury |                                 | 000001 - Al Ahram              | EGP      | 330    | CASH   |
      | 2020000001 | SALES_ORDER           | 2020000053            | 0002 - Signmedia Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 20     | CASH   |
     #BANK Collections
      | 2019000060 | SALES_INVOICE         | 2019100007            |                           | 1516171819789 - EGP - Alex Bank | 000001 - Al Ahram              | EGP      | 20     | BANK   |
      | 2020000002 | NOTES_RECEIVABLE      | 2019000054            |                           | 1234567893458 - EGP - Bank Misr | 000001 - Al Ahram              | EGP      | 20     | BANK   |
      | 2020000004 | SALES_ORDER           | 2020000003            |                           | 1234567893458 - EGP - Bank Misr | 000006 - المطبعة الأمنية       | EGP      | 20     | BANK   |

  #EBS-7372
  Scenario Outline: (01) Save CollectionDetails section within edit session by an authorized user for a draft Collection (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to edit CollectionDetails section of Collection with code "<CCode>" at "16-Nov-2020 09:20 AM"
    When "Ahmed.Hamdi" saves CollectionDetails section of Collection with Code "<CCode>" with CollectionMethod "<CollectionMethod>" at "16-Nov-2020 09:30 AM" with the following values:
      | Amount   | BankAccount   | Treasury   |
      | <Amount> | <BankAccount> | <Treasury> |
    Then CollectionDetails section of Collection with Code "<CCode>" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate   | ReferenceDocumentType   | ReferenceDocumentCode   | Treasury   | BankAccount   | BusinessPartner   | Currency   | Amount   | Method             |
      | <LastUpdatedBy> | <LastUpdateDate> | <ReferenceDocumentType> | <ReferenceDocumentCode> | <Treasury> | <BankAccount> | <BusinessPartner> | <Currency> | <Amount> | <CollectionMethod> |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-04"
    Examples:
      | CCode      | LastUpdatedBy | LastUpdateDate       | ReferenceDocumentType | ReferenceDocumentCode | Treasury               | BankAccount                     | BusinessPartner                | Currency | Amount     | CollectionMethod |
      #Cash
      | 2019000001 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | SALES_INVOICE         | 2019100007            | 0003 - Offset Treasury |                                 | 000001 - Al Ahram              | EGP      | 150.123456 | CASH             |
      | 2019000051 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | NOTES_RECEIVABLE      | 2019000051            | 0001 - Flexo Treasury  |                                 | 000001 - Al Ahram              | EGP      | 300        | CASH             |
      | 2020000001 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | SALES_ORDER           | 2020000053            | 0003 - Offset Treasury |                                 | 000007 - مطبعة أكتوبر الهندسية | EGP      | 50         | CASH             |
      #Bank
      | 2019000060 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | SALES_INVOICE         | 2019100007            |                        | 1516171819712 - USD - Alex Bank | 000001 - Al Ahram              | EGP      | 30         | BANK             |
      | 2020000002 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | NOTES_RECEIVABLE      | 2019000054            |                        | 1234567893499 - USD - Bank Misr | 000001 - Al Ahram              | EGP      | 50.123456  | BANK             |
      | 2020000004 | Ahmed.Hamdi   | 16-Nov-2020 09:30 AM | SALES_ORDER           | 2020000003            |                        | 1234567893499 - USD - Bank Misr | 000006 - المطبعة الأمنية       | EGP      | 70         | BANK             |
