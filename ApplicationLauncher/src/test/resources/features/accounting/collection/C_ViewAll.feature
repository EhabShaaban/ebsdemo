# Author: Hossam Hassan, Engy Mohamed (EBS-4803)
# Reviewer: Ahmad Hamed (07-10-2020)

Feature: View All Collections

  Background:

    Given the following users and roles exist:
      | Name           | Role                            |
      | hr1            | SuperUser                       |
      | Afaf           | FrontDesk                       |
      | Ahmed.Al-Ashry | CollectionResponsible_Signmedia |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                    |
      | SuperUser                       | SuperUserSubRole           |
      | CollectionResponsible_Signmedia | CollectionViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                    | Permission         | Condition                      |
      | SuperUserSubRole           | *:*                |                                |
      | CollectionViewer_Signmedia | Collection:ReadAll | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | Collection:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User           | Permission         | Condition                  |
      | Ahmed.Al-Ashry | Collection:ReadAll | [purchaseUnitName='Flexo'] |
  #@INSERT
    And the following GeneralData for Customer exist:
      | Code   | MarketName                                                                        | LegalName                                                                         | Type  | CreditLimit | Currency   | State  | OldCustomerNumber | CreatedBy | CreationDate         |
      | 000001 | Al Ahram                                                                          | Al Ahram                                                                          | LOCAL | 5000.000    | 0001 - EGP | Active | 11                | Admin     | 20-Oct-2019 11:00 AM |
      | 000002 | Client2                                                                           | Client2                                                                           | LOCAL | 5000.000    | 0001 - EGP | Active | 12                | Admin     | 11-Oct-2019 11:00 AM |
      | 000003 | Al-foad Company                                                                   | Al-foad Company                                                                   | LOCAL | 5000.000    | 0001 - EGP | Active | 13                | Admin     | 10-Aug-2019 11:00 AM |
      | 000007 | مطبعة أكتوبر الهندسية                                                             | مطبعة أكتوبر الهندسية                                                             | LOCAL | 5000.000    | 0001 - EGP | Active | 14                | Admin     | 10-Aug-2019 11:00 AM |
      | 000008 | Cli&ent_5;wi=th!spe@cial?ch+ara/ct%ers,to(te$st)en:cod~ing#ho.me!sc*ree'nfil\"ter | Cli&ent_5;wi=th!spe@cial?ch+ara/ct%ers,to(te$st)en:cod~ing#ho.me!sc*ree'nfil\"ter | LOCAL | 5000.000    | 0001 - EGP | Active | 15                | Admin     | 10-Aug-2019 11:00 AM |
  #@INSERT
    And the following BusinessUnits for Customer exist:
      | Code   | BusinessUnit     |
      | 000001 | 0002 - Signmedia |
      | 000001 | 0002 - Signmedia |
      | 000001 | 0002 - Signmedia |
      | 000001 | 0002 - Signmedia |
      | 000001 | 0002 - Signmedia |

    #@INSERT
    And the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100007 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100002 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
      | 2019100006 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100007 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019100002 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100006 | 0002 - DigiPro   | 0001 - Flexo     |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100007 | 000001 - Al Ahram |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100002 | 000002 - Client2  |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |
      | 2019100006 | 000002 - Client2  |            | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      |             |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000053 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Dec-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000053 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer                       | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000053 | 000007 - مطبعة أكتوبر الهندسية | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 25-Dec-2020 12:00 AM |       |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2019000054 | 15-Jun-2019 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2019000054 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2019000054 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2019 02:05 PM | 0001 - EGP | 2000   | 2000      |       |

    #@INSERT
    And the following GeneralData for Collections exist:
      | Code       | Type | State  | CreatedBy                 | CreationDate         | BusinessPartnerType | DocumentOwner  |
      | 2019000001 | C_SI | Draft  | Admin from BDKCompanyCode | 04-Dec-2019 11:27 AM | CUSTOMER            | Ahmed Al-Ashry |
      | 2019000002 | C_SI | Active | Admin from BDKCompanyCode | 17-Dec-2019 02:09 PM | CUSTOMER            | Ahmed Al-Ashry |
      | 2019000003 | C_SI | Draft  | Admin from BDKCompanyCode | 17-Dec-2019 02:10 PM | CUSTOMER            | Ahmed Hamdy    |
      | 2019000004 | C_SO | Active | A.Hamed                   | 29-Dec-2019 09:00 AM | CUSTOMER            | Ahmed Al-Ashry |
      | 2020000001 | C_SO | Draft  | A.Hamed                   | 29-Sep-2020 09:00 AM | CUSTOMER            | Ahmed Al-Ashry |
      | 2020000002 | C_NR | Draft  | hr1                       | 29-Sep-2020 09:00 AM | CUSTOMER            | Ahmed Al-Ashry |
      | 2020000003 | C_SO | Active | A.Hamed                   | 29-Sep-2020 09:00 AM | CUSTOMER            | Ahmed Al-Ashry |
      | 2020000004 | C_SO | Active | A.Hamed                   | 29-Sep-2020 09:00 AM | CUSTOMER            | Ahmed Al-Ashry |

    #@INSERT
    And the following CompanyData for Collections exist:
      | Code       | BusinessUnit     | Company          |
      | 2019000001 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2019000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2019000003 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2019000004 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000003 | 0002 - Signmedia | 0001 - AL Madina |
      | 2020000004 | 0002 - Signmedia | 0001 - AL Madina |

    #@INSERT
    And the following CollectionDetails for Collections exist:
      | Code       | Type             | Method | BusinessPartner                                                                           | ReferenceDocument | Amount      | Currency | BankAccount                     | Treasury                  |
      | 2019000001 | SALES_INVOICE    | CASH   | 000001 - Al Ahram                                                                         | 2019100007        | 1000        | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2019000002 | SALES_INVOICE    | CASH   | 000002 - Client2                                                                          | 2019100002        | 2000.123456 | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2019000003 | SALES_INVOICE    | CASH   | 000002 - Client2                                                                          | 2019100006        | 3000        | EGP      |                                 | 0001 - Flexo Treasury     |
      | 2019000004 | SALES_ORDER      | CASH   | 000008 - Cli&ent_5;wi=th!spe@cial?ch+ara/ct%ers,to(te$st)en:cod~ing#ho.me!sc*ree'nfil"ter | 2020000053        | 20          | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000001 | SALES_ORDER      | CASH   | 000007 - مطبعة أكتوبر الهندسية                                                            | 2020000053        | 20          | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000002 | NOTES_RECEIVABLE | BANK   | 000001 - Al Ahram                                                                         | 2019000054        | 20          | EGP      | 1234567893458 - EGP - Bank Misr |                           |
      | 2020000003 | SALES_ORDER      | CASH   | 000007 - مطبعة أكتوبر الهندسية                                                            | 2020000053        | 20          | EGP      |                                 | 0002 - Signmedia Treasury |
      | 2020000004 | SALES_ORDER      | CASH   | 000003 - Al-foad Company                                                                  | 2020000053        | 20          | EGP      |                                 | 0002 - Signmedia Treasury |

    And the total number of all existing Collection records are 8

  Scenario: (01) View All Collections by authorized user due to condition (Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read page 1 of Collections with no filter applied 20 records per page
    Then the following values will be presented to "Ahmed.Al-Ashry":
      | Code       | DocumentOwner            | Type                                        | BusinessPartner                                                                                      | BusinessUnit     | ReferenceDocument                        | Amount          | State  |
      | 2020000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000003 - Al-foad Company                                                                  | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP          | Active |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000007 - مطبعة أكتوبر الهندسية                                                            | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP          | Active |
      | 2020000002 | 1000072 - Ahmed Al-Ashry | NOTES_RECEIVABLE - Against Notes Receivable | Customer - 000001 - Al Ahram                                                                         | 0002 - Signmedia | Against Notes Receivable - 2019000054    | 20 EGP          | Draft  |
      | 2020000001 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000007 - مطبعة أكتوبر الهندسية                                                            | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP          | Draft  |
      | 2019000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000008 - Cli&ent_5;wi=th!spe@cial?ch+ara/ct%ers,to(te$st)en:cod~ing#ho.me!sc*ree'nfil"ter | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP          | Active |
      | 2019000002 | 1000072 - Ahmed Al-Ashry | SALES_INVOICE - Against Sales Invoice       | Customer - 000002 - Client2                                                                          | 0002 - Signmedia | Against Sales Invoice - 2019100002       | 2000.123456 EGP | Active |
      | 2019000001 | 1000072 - Ahmed Al-Ashry | SALES_INVOICE - Against Sales Invoice       | Customer - 000001 - Al Ahram                                                                         | 0002 - Signmedia | Against Sales Invoice - 2019100007       | 1000 EGP        | Draft  |
    And the total number of records in search results by "Ahmed.Al-Ashry" are 7

  Scenario: (02) View All Collections by authorized user + Paging (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with no filter applied 2 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000003 - Al-foad Company       | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
    And the total number of records in search results by "hr1" are 2

  Scenario: (03) View All Collections by CollectionCode by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on CollectionCode which contains "003" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount   | State  |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP   | Active |
      | 2019000003 | 16 - Ahmed Hamdy         | SALES_INVOICE - Against Sales Invoice     | Customer - 000002 - Client2               | 0001 - Flexo     | Against Sales Invoice - 2019100006       | 3000 EGP | Draft  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (04) View All Collections by BusinessPartner by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerName which contains "Al Ahram" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                        | BusinessPartner              | BusinessUnit     | ReferenceDocument                     | Amount   | State |
      | 2020000002 | 1000072 - Ahmed Al-Ashry | NOTES_RECEIVABLE - Against Notes Receivable | Customer - 000001 - Al Ahram | 0002 - Signmedia | Against Notes Receivable - 2019000054 | 20 EGP   | Draft |
      | 2019000001 | 1000072 - Ahmed Al-Ashry | SALES_INVOICE - Against Sales Invoice       | Customer - 000001 - Al Ahram | 0002 - Signmedia | Against Sales Invoice - 2019100007    | 1000 EGP | Draft |
    And the total number of records in search results by "hr1" are 2

  Scenario: (05) View All Collections by BusinessPartner by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerCode which contains "00007" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000001 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Draft  |
    And the total number of records in search results by "hr1" are 2

  Scenario: (06) View All Collections by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessUnit which equals "0001" with 3 records per page  while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner    | Type                                  | BusinessPartner             | BusinessUnit | ReferenceDocument                  | Amount   | State |
      | 2019000003 | 16 - Ahmed Hamdy | SALES_INVOICE - Against Sales Invoice | Customer - 000002 - Client2 | 0001 - Flexo | Against Sales Invoice - 2019100006 | 3000 EGP | Draft |
    And the total number of records in search results by "hr1" are 1

  Scenario: (07) View All Collections by ReferenceDocument  by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on ReferenceDocumentCode which contains "002" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                  | BusinessPartner             | BusinessUnit     | ReferenceDocument                  | Amount          | State  |
      | 2019000002 | 1000072 - Ahmed Al-Ashry | SALES_INVOICE - Against Sales Invoice | Customer - 000002 - Client2 | 0002 - Signmedia | Against Sales Invoice - 2019100002 | 2000.123456 EGP | Active |
    And the total number of records in search results by "hr1" are 1

  Scenario: (08) View All Collections by ReferenceDocument by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on ReferenceDocumentName which contains "Sales" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000003 - Al-foad Company       | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000001 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Draft  |
    And the total number of records in search results by "hr1" are 3

  Scenario: (09) View All Collections by Amount by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on Amount which equals "2000.123456" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                  | BusinessPartner             | BusinessUnit     | ReferenceDocument                  | Amount          | State  |
      | 2019000002 | 1000072 - Ahmed Al-Ashry | SALES_INVOICE - Against Sales Invoice | Customer - 000002 - Client2 | 0002 - Signmedia | Against Sales Invoice - 2019100002 | 2000.123456 EGP | Active |
    And the total number of records in search results by "hr1" are 1

  Scenario: (10) View All Collections by State by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on State which equals "Draft" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                        | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount   | State |
      | 2020000002 | 1000072 - Ahmed Al-Ashry | NOTES_RECEIVABLE - Against Notes Receivable | Customer - 000001 - Al Ahram              | 0002 - Signmedia | Against Notes Receivable - 2019000054    | 20 EGP   | Draft |
      | 2020000001 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP   | Draft |
      | 2019000003 | 16 - Ahmed Hamdy         | SALES_INVOICE - Against Sales Invoice       | Customer - 000002 - Client2               | 0001 - Flexo     | Against Sales Invoice - 2019100006       | 3000 EGP | Draft |
    And the total number of records in search results by "hr1" are 3

  #EBS-4803
  Scenario: (11) View All Collections by Type by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on Type which equals "Against Notes Receivable" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                        | BusinessPartner              | BusinessUnit     | ReferenceDocument                     | Amount | State |
      | 2020000002 | 1000072 - Ahmed Al-Ashry | NOTES_RECEIVABLE - Against Notes Receivable | Customer - 000001 - Al Ahram | 0002 - Signmedia | Against Notes Receivable - 2019000054 | 20 EGP | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-4803
  Scenario: (12) View All Collections by DocumentOwner by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on DocumentOwner which equals "Hamdy" with 3 records per page
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner    | Type                                  | BusinessPartner             | BusinessUnit | ReferenceDocument                  | Amount   | State |
      | 2019000003 | 16 - Ahmed Hamdy | SALES_INVOICE - Against Sales Invoice | Customer - 000002 - Client2 | 0001 - Flexo | Against Sales Invoice - 2019100006 | 3000 EGP | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-4803
  Scenario: (13) View All Collections by BusinessPartner by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerType which contains "Customer" with 3 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                        | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000003 - Al-foad Company       | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order   | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000002 | 1000072 - Ahmed Al-Ashry | NOTES_RECEIVABLE - Against Notes Receivable | Customer - 000001 - Al Ahram              | 0002 - Signmedia | Against Notes Receivable - 2019000054    | 20 EGP | Draft  |
    And the total number of records in search results by "hr1" are 3

  #EBS-4803
  Scenario: (14) View All Collections filtered by BusinessPartner in arabic value by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerType which contains "أكتوبر" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
      | 2020000001 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Draft  |
    And the total number of records in search results by "hr1" are 2

  #EBS-4803
  Scenario Outline: (15) View All Collections filtered by BusinessPartner that has special character by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerType which contains "<Filter>" with 5 records per page while current locale is "en-us"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                                                                                      | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2019000004 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000008 - Cli&ent_5;wi=th!spe@cial?ch+ara/ct%ers,to(te$st)en:cod~ing#ho.me!sc*ree'nfil"ter | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
    And the total number of records in search results by "hr1" are 1
    Examples:
      | Filter      |
      | Cli&ent     |
      | ent_5       |
      | 5;wi        |
      | wi=th       |
      | th!sp       |
      | spe@cial    |
      | cial?ch     |
      | ch+ara      |
      | ara/ct      |
      | ct%ers      |
      | ers,to      |
      | to(te$st)en |
      | en:cod      |
      | cod~ing     |
      | ing#ho      |
      | ho.me       |
      | me!sc       |
      | sc*ree      |
      | ree'nfil    |

  #EBS-4803
  Scenario: (16) View All Collections filtered by BusinessPartner in arabic value and State by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of Collections with filter applied on BusinessPartnerType which contains "أكتوبر" and State equals "Active" with 5 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | DocumentOwner            | Type                                      | BusinessPartner                           | BusinessUnit     | ReferenceDocument                        | Amount | State  |
      | 2020000003 | 1000072 - Ahmed Al-Ashry | SALES_ORDER - DownPayment for Sales Order | Customer - 000007 - مطبعة أكتوبر الهندسية | 0002 - Signmedia | DownPayment for Sales Order - 2020000053 | 20 EGP | Active |
    And the total number of records in search results by "hr1" are 1

  Scenario: (17) View All Collections by CollectionCode by unauthorized user due to condition (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of Collections with filter applied on CollectionCode which contains "003" with 3 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
