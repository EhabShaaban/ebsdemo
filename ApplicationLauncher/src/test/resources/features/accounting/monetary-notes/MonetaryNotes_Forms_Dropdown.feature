# Author Dev: Eman Mansour
# Author Quality: Shirin Mahmoud

Feature: View All Monetary Note Forms

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |

  #EBS-8709
  Scenario: (01) Read list of MonetaryNote Forms dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all MonetaryNote Forms
    Then the following MonetaryNote Forms values will be presented to "Shady.Abdelatif":
      | Form             |
      | CHEQUE           |
      | BILL_OF_EXCHANGE |
      | TRUST_RECEIPT    |
    And total number of MonetaryNote Forms returned to "Shady.Abdelatif" is equal to 3

  #EBS-8709
  Scenario: (02) Read list of MonetaryNote Forms dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all MonetaryNote Forms
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
