# Author Dev: Eman Mansour
# Author Quality: Shirin Mahmoud

Feature: View All Notes Receivable Types

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |

  #EBS-8710
  Scenario: (01) Read list of NotesReceivable Types dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all NotesReceivable Types
    Then the following NotesReceivable Types values will be presented to "Shady.Abdelatif":
      | Type                           |
      | NOTES_RECEIVABLE_AS_COLLECTION |
    And total number of NotesReceivable Types returned to "Shady.Abdelatif" is equal to 1

  #EBS-8710
  Scenario: (02) Read list of NotesReceivable Types dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all NotesReceivable Types
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
