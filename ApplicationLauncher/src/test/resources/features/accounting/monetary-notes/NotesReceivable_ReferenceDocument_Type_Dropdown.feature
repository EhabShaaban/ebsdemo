# Author Dev: Fatma Al-Zahra
# Author Quality: Shirin Mahmoud

Feature: View All NotesReceivable - ReferenceDocument Types

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                           | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll              | [purchaseUnitName='Signmedia'] |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |

  #EBS-8809
  Scenario: (01) Read list of NotesReceivable -  ReferenceDocument Types dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all NotesReceivable ReferenceDocument Types
    Then the following NotesReceivable ReferenceDocument Types values will be presented to "Shady.Abdelatif":
      | DocumentType     |
      | SALES_INVOICE    |
      | SALES_ORDER      |
      | NOTES_RECEIVABLE |
    And total number of NotesReceivable ReferenceDocument Types returned to "Shady.Abdelatif" is equal to 3

  #EBS-8809
  Scenario: (02) Read list of NotesReceivable - ReferenceDocument Types dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all NotesReceivable ReferenceDocument Types
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page