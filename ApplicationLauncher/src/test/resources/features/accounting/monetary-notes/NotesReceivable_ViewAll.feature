# Author Dev: Updated By Order Team (27-April-2021) for #EBS-8650
# Author Quality: Shirin Mahmoud for #EBS-8650

Feature: View All Notes Receivables

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
      | SuperUser            | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole                | *:*                     |                                |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User            | Permission              | Condition                  |
      | Shady.Abdelatif | NotesReceivable:ReadAll | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate           | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM   | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000002 | 16-April-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 17-Mar-2021 02:05 PM   | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Ashraf Salah    | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner                | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram              | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram              | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000007 - مطبعة أكتوبر الهندسية | 541212     | Al - Ahly Bank | 20-Oct-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |

  #EBS-8650
  Scenario: (01) View all NotesReceivables, by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with no filter applied with 20 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0002 - Signmedia | Draft  |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0001 - Flexo     | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 3

  #EBS-8650
  Scenario: (02) View all NotesReceivables, by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of NotesReceivables with no filter applied with 20 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0002 - Signmedia | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "Shady.Abdelatif" are 2

  #EBS-8650
  Scenario: (03) View all NotesReceivables, Filter by Code by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Code which contains "002" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner   | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit | State |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-8650
  Scenario: (04) View all NotesReceivables, Filter by Type by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Type which equals "NOTES_RECEIVABLE_AS_COLLECTION" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0002 - Signmedia | Draft  |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0001 - Flexo     | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 3

  #EBS-8650
  Scenario: (05) View all NotesReceivables, Filter by BusinessPartner Code by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on BusinessPartner which contains "001" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner   | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0001 - Flexo     | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (06) View all NotesReceivables, Filter by BusinessPartner Name by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on BusinessPartner which contains "Ahram" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner   | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0001 - Flexo     | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (07) View all NotesReceivables, Filter by Depot Code by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Depot which contains "001" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit     | State |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0002 - Signmedia | Draft |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo     | Draft |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (08) View all NotesReceivables, Filter by Depot Name by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Depot which contains "القاهرة" with 10 records per page while current locale is "en-US"
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit     | State |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0002 - Signmedia | Draft |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo     | Draft |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (09) View all NotesReceivables, Filter by DueDate by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on DueDate which equals "17-Sep-2021" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner   | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit | State |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo | Draft |
    And the total number of records in search results by "hr1" are 1

  #EBS-8650
  Scenario: (10) View all NotesReceivables, Filter by Amount by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Amount which equals "1000" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit     | State |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0002 - Signmedia | Draft |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo     | Draft |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (11) View all NotesReceivables, Filter by Remaining by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on Remaining which equals "1000" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount         | Remaining      | BusinessUnit     | State |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0002 - Signmedia | Draft |
      | 2021000002 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              | 0001 - بنك القاهرة  باركليز - المهندسين | 17-Sep-2021 02:05 PM | 1000.000 - EGP | 1000.000 - EGP | 0001 - Flexo     | Draft |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (12) View all NotesReceivables, Filter by BusinessUnit by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on BusinessUnit which equals "0002" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner                | Depot                                   | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000003 | NOTES_RECEIVABLE_AS_COLLECTION | 000007 - مطبعة أكتوبر الهندسية | 0001 - بنك القاهرة  باركليز - المهندسين | 20-Oct-2021 02:05 PM | 1000.000 - EGP  | 1000.000 - EGP  | 0002 - Signmedia | Draft  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram              |                                         | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 2

  #EBS-8650
  Scenario: (13) View all NotesReceivables, Filter by State by authorized user (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesReceivables with filter applied on State which contains "Active" with 10 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                           | BusinessPartner   | Depot | DueDate              | Amount          | Remaining       | BusinessUnit     | State  |
      | 2021000001 | NOTES_RECEIVABLE_AS_COLLECTION | 000001 - Al Ahram |       | 16-Jun-2021 02:05 PM | 25000.000 - USD | 25000.000 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 1

  #EBS-8650
  Scenario: (14) View all NotesReceivables, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read page 1 of NotesReceivables with no filter applied with 20 records per page
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
