# Author Dev: Pirates Team (7-June-2021)
# Author SDETs: Ehab

Feature: ReferenceDocument - Sales Invoice DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia |
      | Accountant_Signmedia | SalesInvoiceViewer_Signmedia   |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                           | Condition                      |
      | NotesReceivableOwner_Signmedia | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia   | SalesInvoice:ReadAll                 | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | SalesInvoice:ReadAll |


   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         | DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM | Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000004 | 0002 - DigiPro   | 0001 - AlMadina Main Store |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000004 - Client3  | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
     #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit    | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000005 | 000009 - Machine1                   | 0014 - Liter | 10.000 | 2000.000   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000004 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000005 | 20000.000              | 27200.000             | 27200.000 |

      #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State                     | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active             | 01-Dec-2019 02:20 PM |
      | 2019100005 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active             | 01-Dec-2019 02:20 PM |
      | 2019100006 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active             | 01-Dec-2019 02:20 PM |
      | 2019100007 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,ReadyForDelivering | 01-Dec-2019 02:20 PM |
      | 2019100008 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active             | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100005 | 0002 - DigiPro   | 0002 - Signmedia |
      | 2019100006 | 0001 - AL Madina | 0001 - Flexo     |
      | 2019100007 | 0001 - AL Madina | 0002 - Signmedia |
      | 2019100008 | 0001 - AL Madina | 0002 - Signmedia |

    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
      | 2019100005 | 000001 - Al Ahram | 2020000004 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
      | 2019100006 | 000004 - Client3  | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
      | 2019100007 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
      | 2019100008 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit    | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit     |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC    | 136            | 22.00           | 0019 - M2    |
      | 2019100005 | 000008 - PRO-V-ST-2 | 0030 - PC    | 136            | 22.00           | 0019 - M2    |
      | 2019100006 | 000009 - Machine1   | 0014 - Liter | 136            | 22.00           | 0014 - Liter |
      | 2019100007 | 000008 - PRO-V-ST-2 | 0030 - PC    | 136            | 22.00           | 0019 - M2    |
      | 2019100008 | 000008 - PRO-V-ST-2 | 0030 - PC    | 136            | 22.00           | 0019 - M2    |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
      | 2019100005 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100005 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100005 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100005 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100005 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100005 | 0012 - Sales tax                             | 12.00         | 359.04    |
      | 2019100006 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100006 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100006 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100006 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100006 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100006 | 0012 - Sales tax                             | 12.00         | 359.04    |
      | 2019100007 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100007 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100007 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100007 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100007 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100007 | 0012 - Sales tax                             | 12.00         | 359.04    |
      | 2019100008 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100008 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100008 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100008 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100008 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100008 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |
      | 2019100005 | 4069.12   |
      | 2019100006 | 4069.12   |
      | 2019100007 | 4069.12   |
      | 2019100008 | 0         |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |
      | 2019100005 | 2992.00        | 4069.12       | 4069.12   |
      | 2019100006 | 2992.00        | 4069.12       | 4069.12   |
      | 2019100007 | 2992.00        | 4069.12       | 4069.12   |
      | 2019100008 | 2992.00        | 4069.12       | 0         |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |


  #EBS-8810
  Scenario: (01) Read list of SalesInvoice - ReferenceDocument [in Active State - has remaining > 0 - has the same businessPartner, company and businessUnit] dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all SalesInvoice - ReferenceDocument for NotesReceivable with code "2021000001"
    Then the following SalesInvoice - ReferenceDocument values will be presented to "Shady.Abdelatif":
      | Code       |
      | 2019100004 |
    And total number of SalesInvoice - ReferenceDocument returned to "Shady.Abdelatif" is equal to 1

  #EBS-8810
  Scenario: (02) Read list of SalesInvoice - ReferenceDocument dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesInvoice - ReferenceDocument for NotesReceivable with code "2021000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page