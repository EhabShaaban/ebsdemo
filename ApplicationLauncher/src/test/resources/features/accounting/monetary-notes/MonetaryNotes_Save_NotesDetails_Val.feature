# Author Dev: Eman Mansour

Feature: Save NotesReceivable Notes Details section Validation

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                       |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia |
      | SuperUser            | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                                   | Condition                      |
      | NotesReceivableOwner_Signmedia | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole               | *:*                                          |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                                   | Condition                  |
      | Shady.Abdelatif | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |

    And the following Depots exist:
      | Depot                                   |
      | 0001 - بنك القاهرة  باركليز - المهندسين |

  #EBS-7191
  Scenario: (01) Save NotesReceivable NotesDetails section, Depot not exists
    Given user is logged in as "Shady.Abdelatif"
    And NotesDetails section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:10 AM"
    When "Shady.Abdelatif" saves NotesDetails section of NotesReceivable with Code "2021000001" at "07-Jan-2021 09:30 AM" with the following values:
      | NoteNumber | NoteBank       | DueDate              | Depot                                   |
      | 123456     | Al - Alex Bank | 17-Jun-2021 02:05 PM | 9999 - بنك القاهرة  باركليز - المهندسين |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-7191
  Scenario: (01) Save NotesReceivable NotesDetails section, by an authorized user, missing or malicious input (Abuse case)
    Given user is logged in as "Shady.Abdelatif"
    And NotesDetails section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:10 AM"
    When "Shady.Abdelatif" saves NotesDetails section of NotesReceivable with Code "2021000001" at "07-Jan-2021 09:30 AM" with the following values:
      | NoteNumber | NoteBank       | DueDate              | Depot                                   |
      | AnyChar    | Al - Alex Bank | 17-Jun-2021 02:05 PM | 0001 - بنك القاهرة  باركليز - المهندسين |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
