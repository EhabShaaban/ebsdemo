# Author: Mohamed Aboelnour & Ehab Shabaan
# Reviewer: Somaya Ahmed (10-Aug-2021)

Feature: Notes Receivables View ActivationDetails section

  Background:
    Given the following users and roles exist:
      | Name           | Role                            |
      | Ahmed.Al-Ashry | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi    | AdminTreasury                   |
      | hr1            | SuperUser                       |

    And the following roles and sub-roles exist:
      | Role                            | Sub-role                        |
      | CollectionResponsible_Signmedia | NotesReceivableViewer_Signmedia |
      | AdminTreasury                   | NotesReceivableViewer           |
      | SuperUser                       | SuperUserSubRole                |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                            | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadActivationDetails | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer           | NotesReceivable:ReadActivationDetails |                                |
      | SuperUserSubRole                | *:*                                   |                                |

    And the following users have the following permissions without the following conditions:
      | User           | Permission                            | Condition                  |
      | Ahmed.Al-Ashry | NotesReceivable:ReadActivationDetails | [purchaseUnitName='Flexo'] |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |

    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |

    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |

    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |

    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |

    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000004 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |

    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |

    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |

    #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |

    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |

    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |

    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0003 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |

    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |

    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0001 - EGP | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0001 - EGP | 14000.000 | 14000.000 |                                         |

    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType     | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 5000.102487     |
      | 2021000001 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000004   | SALES_ORDER      | 2999.797278     |
      | 2021000001 | 3                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE    | 4000.100235     |
      | 2021000001 | 4                         | NOTES_RECEIVABLE_AS_COLLECTION | 2021000003   | NOTES_RECEIVABLE | 13000           |
      | 2021000003 | 5                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 14000           |

    And Insert the following AccountingDetails for MonetaryNotes:
      | Code       | Credit/Debit | Account                   | SubAccount        | Value       |
      | 2021000001 | DEBIT        | 10208 - Notes receivables | 2021000001        | 25000.000   |
      | 2021000001 | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 5000.102487 |
      | 2021000001 | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 2999.797278 |
      | 2021000001 | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 4000.100235 |
      | 2021000001 | CREDIT       | 10208 - Notes receivables | 2021000003        | 13000       |
      | 2021000003 | DEBIT        | 10208 - Notes receivables | 2021000003        | 14000       |
      | 2021000003 | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 14000       |

    #@INSERT
    And the following JournalEntries exist:
      | Code       | CreationDate         | CreatedBy      | JournalDate          | BusinessUnit     | FiscalPeriod | ReferenceDocument            | Company          |
      | 2021000101 | 01-Mar-2021 10:30 AM | Ahmed.Al-Ashry | 02-Mar-2021 10:30 AM | 0002 - Signmedia | 2021         | 2021000001 - NotesReceivable | 0001 - AL Madina |
      | 2021000102 | 01-Mar-2021 10:30 AM | Ahmed.Hamdi    | 02-Mar-2021 10:30 AM | 0002 - Signmedia | 2021         | 2021000003 - NotesReceivable | 0001 - AL Madina |

    #@INSERT
    And Insert the following MonetaryNotes ActivationDetails exist:
      | Code       | Accountant     | ActivationDate       | CurrencyPrice       | JournalEntryCode | FiscalYear |
      | 2021000001 | Ahmed.Al-Ashry | 01-Mar-2021 10:30 AM | 1.0 EGP = 1.0 EGP   | 2021000101       | 2021       |
      | 2021000003 | Ahmed.Hamdi    | 01-Mar-2021 10:30 AM | 1.0 USD = 17.44 EGP | 2021000102       | 2021       |

  #EBS-5446
  Scenario: (01) View ActivationDetails section in NotesReceivables - by an authorized user who has one role with condition(Happy Path)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ActivationDetails section of NotesReceivables with code "2021000001"
    Then the following values of ActivationDetails section of NotesReceivables with code "2021000001" are displayed to "Ahmed.Al-Ashry":
      | Code       | Accountant     | ActivationDate       | JournalEntryCode | FiscalYear | CurrencyPrice     |
      | 2021000001 | Ahmed.Al-Ashry | 01-Mar-2021 10:30 AM | 2021000101       | 2021       | 1.0 EGP = 1.0 EGP |

  #EBS-5446
  Scenario Outline: (02) View ActivationDetails section in NotesReceivables - by an authorized user who has one role with no condition(Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" requests to view ActivationDetails section of NotesReceivables with code "<Code>"
    Then the following values of ActivationDetails section of NotesReceivables with code "<Code>" are displayed to "Ahmed.Hamdi":
      | Code   | Accountant   | ActivationDate   | JournalEntryCode   | FiscalYear   | CurrencyPrice   |
      | <Code> | <Accountant> | <ActivationDate> | <JournalEntryCode> | <FiscalYear> | <CurrencyPrice> |
    Examples:
      | Code       | Accountant     | ActivationDate       | JournalEntryCode | FiscalYear | CurrencyPrice       |
      | 2021000001 | Ahmed.Al-Ashry | 01-Mar-2021 10:30 AM | 2021000101       | 2021       | 1.0 EGP = 1.0 EGP   |
      | 2021000003 | Ahmed.Hamdi    | 01-Mar-2021 10:30 AM | 2021000102       | 2021       | 1.0 USD = 17.44 EGP |

  #EBS-5446
  Scenario: (03) View ActivationDetails section in NotesReceivables - by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to view ActivationDetails section of NotesReceivables with code "2021000002"
    Then "Ahmed.Al-Ashry" is logged out
    And "Ahmed.Al-Ashry" is forwarded to the error page

  #EBS-5446
  @Future
  Scenario: (04) View ActivationDetails section in NotesReceivables - where NotesReceivables doesn't exist (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    And another user is logged in as "hr1"
    And "hr1" first deleted the NotesReceivables with code "2021000002" successfully
    When "Ahmed.Hamdi" requests to view ActivationDetails section of NotesReceivables with code "2021000002"
    Then an error notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-01"
