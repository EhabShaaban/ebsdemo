# Author Dev: Order Team (24-May-2021)
# Author Quality: Shirin Mahmoud

Feature: Save NotesReceivable Notes Details section HP

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                       |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia |
      | SuperUser            | SuperUserSubRole               |
    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                                   | Condition                      |
      | NotesReceivableOwner_Signmedia | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole               | *:*                                          |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                                   | Condition                  |
      | Shady.Abdelatif | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |

    And the following Depots exist:
      | Depot                                   |
      | 0001 - بنك القاهرة  باركليز - المهندسين |

  #EBS-8449
  Scenario Outline: (01) Save NotesReceivable NotesDetails section, by an authorized user for Draft NotesReceivable (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And NotesDetails section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:10 AM"
    When "Shady.Abdelatif" saves NotesDetails section of NotesReceivable with Code "2021000001" at "07-Jan-2021 09:30 AM" with the following values:
      | NoteNumber   | NoteBank   | DueDate   | Depot   |
      | <NoteNumber> | <NoteBank> | <DueDate> | <Depot> |
    Then NotesReceivable with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 07-Jan-2021 09:30 AM | Draft |
    And NotesDetails section of NotesReceivable with code "2021000001" is updated as follows:
      | NoteNumber   | NoteBank   | DueDate   | Depot   |
      | <NoteNumber> | <NoteBank> | <DueDate> | <Depot> |
    And the lock by "Shady.Abdelatif" on NotesDetails section of NotesReceivable with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"
    Examples:
      | NoteNumber | NoteBank       | DueDate              | Depot                                   |
      | 123456     | Al - Alex Bank | 17-Jun-2021 02:05 PM | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 123456     |                |                      |                                         |
      |            | Al - Alex Bank |                      |                                         |
      |            |                | 17-Jun-2021 02:05 PM |                                         |
      |            |                |                      | 0001 - بنك القاهرة  باركليز - المهندسين |
