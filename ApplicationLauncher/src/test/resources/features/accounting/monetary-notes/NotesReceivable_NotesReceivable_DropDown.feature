# Author Dev: Pirates Team (7-June-2021)

Feature: ReferenceDocument - NotesReceivable DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                           | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll              | [purchaseUnitName='Signmedia'] |
    And the following users doesn't have the following permissions:
      | User | Permission              |
      | Afaf | NotesReceivable:ReadAll |


   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
        #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
       #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
        #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
       #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
       #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000003 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000004 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000005 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000006 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000004 | 0002 - Signmedia | 0002 - DigiPro   |
      | 2021000005 | 0001 - Flexo     | 0001 - AL Madina |
      | 2021000006 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |
      | 2021000004 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |
      | 2021000005 | CHEQUE   | 000004 - Client3  | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |
      | 2021000006 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 0         |       |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType | AmountToCollect |
      | 2021000002 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000.000        |
      | 2021000004 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000.000        |
      | 2021000005 | 3                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000.000        |
      | 2021000006 | 4                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000.000        |

  #EBS-8813
  Scenario: (01) Read list of NotesReceivable - ReferenceDocument [in Active State - has remaining > 0 - has the same businessPartner, company and businessUnit] dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all NotesReceivable - ReferenceDocument for NotesReceivable with code "2021000001"
    Then the following NotesReceivable - ReferenceDocument values will be presented to "Shady.Abdelatif":
      | Code       |
      | 2021000002 |
    And total number of NotesReceivable - ReferenceDocument returned to "Shady.Abdelatif" is equal to 1

  #EBS-8813
  Scenario: (02) Read list of NotesReceivable - ReferenceDocument dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all NotesReceivable - ReferenceDocument for NotesReceivable with code "2021000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page