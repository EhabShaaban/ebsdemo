# Author Dev: Order Team (24-May-2021)
# Author Quality: Shirin Mahmoud

Feature: View NotesReceivable Reference Document section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
      | M.D                  | NotesReceivableViewer           |
      | SuperUser            | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                             | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadReferenceDocuments | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer           | NotesReceivable:ReadReferenceDocuments |                                |
      | SuperUserSubRole                | *:*                                    |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                             | Condition                  |
      | Shady.Abdelatif | NotesReceivable:ReadReferenceDocuments | [purchaseUnitName='Flexo'] |

   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
     #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro   |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 14000.000 | 14000.000 |                                         |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType     | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE    | 4000.000        |
      | 2021000001 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 4000.000        |
      | 2021000001 | 3                         | NOTES_RECEIVABLE_AS_COLLECTION | 2021000003   | NOTES_RECEIVABLE | 14000.000       |
    And the following MonetaryNotes have no ReferenceDocuments:
      | Code       |
      | 2021000002 |

  # EBS-6165
  Scenario: (01) View NotesReceivable ReferenceDocuments section by an authorized user who has one role with one condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ReferenceDocuments section of MonetaryNotes with code "2021000001"
    Then the following values of ReferenceDocuments section for MonetaryNotes with code "2021000001" are displayed to "Shady.Abdelatif":
      | Code       | ReferenceDocument             | AmountToCollect |
      | 2021000001 | 2019100004 - Sales Invoice    | 4000.000        |
      | 2021000001 | 2020000003 - Sales Order      | 4000.000        |
      | 2021000001 | 2021000003 - Notes Receivable | 14000.000       |

  # EBS-6165
  Scenario: (02) View NotesReceivable ReferenceDocuments section by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view ReferenceDocuments section of MonetaryNotes with code "2021000001"
    Then the following values of ReferenceDocuments section for MonetaryNotes with code "2021000001" are displayed to "Ashraf.Fathi":
      | Code       | ReferenceDocument             | AmountToCollect |
      | 2021000001 | 2019100004 - Sales Invoice    | 4000.000        |
      | 2021000001 | 2020000003 - Sales Order      | 4000.000        |
      | 2021000001 | 2021000003 - Notes Receivable | 14000.000       |

  # EBS-6165
  Scenario: (03) View NotesReceivable ReferenceDocuments section by an authorized user who has one role without conditions (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view ReferenceDocuments section of MonetaryNotes with code "2021000002"
    Then ReferenceDocuments section for MonetaryNotes with code "2021000002" is displayed empty to "Ashraf.Fathi"

  @Future
  Scenario: (04) View NotesReceivable ReferenceDocuments section where MonetaryNotes doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the MonetaryNotes with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view ReferenceDocuments section of MonetaryNotes with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  # EBS-6165
  Scenario: (05) View NotesReceivable ReferenceDocuments section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view ReferenceDocuments section of MonetaryNotes with code "2021000002"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page