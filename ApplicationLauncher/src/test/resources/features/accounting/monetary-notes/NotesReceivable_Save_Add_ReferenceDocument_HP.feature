# Author Dev: Eman Mansour
# Author Quality: Ehab Shabaan

Feature: Save NotesReceivable New Reference Document HP

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                        |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia | SOViewer_Signmedia              |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                           | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll                 | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll                   | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll              | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State                 | CreationDate         | DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved              | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | GoodsIssueActivated   | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | SalesInvoiceActivated | 11-Feb-2020 01:28 PM | Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
      | 2020000005 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
       #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000                  | 27200                 | 27200     |
      | 2020000004 | 20000                  | 27200                 | 27200     |
      | 2020000005 | 20000                  | 27200                 | 27200     |

      #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000005 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000002 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000  | 25000     |       |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000  | 25000     |       |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000            |
      | 2021000002 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000            |

    And edit session is "30" minutes

  # EBS-8484
  Scenario: (01) Save Add ReferenceDocument in NotesReceivable, Add SalesOrder with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "01-Jan-2021 11:00 AM"
    When "Shady.Abdelatif" saves the following new ReferenceDocument to ReferenceDocuments section of NotesReceivable with Code "2021000001" at "01-Jan-2021 11:10 AM" with the following values:
      | DocumentType | DocumentCode | AmountToCollect |
      | SALES_ORDER  | 2020000004   | 5000.265974     |
    Then NotesReceivable with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2021 11:10 AM | Draft |
    And the lock by "Shady.Abdelatif" on ReferenceDocument section of NotesReceivable with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"
    And ReferenceDocuments section of NotesReceivable with Code "2021000001" is updated as follows and displayed to "Shady.Abdelatif":
      | Code       | ReferenceDocument        | AmountToCollect |
      | 2021000001 | 2020000003 - Sales Order | 4000            |
      | 2021000001 | 2020000004 - Sales Order | 5000.265974     |

  # EBS-8484
  Scenario: (02) Save Add ReferenceDocument in NotesReceivable, Add SalesInvoice with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "01-Jan-2021 11:00 AM"
    When "Shady.Abdelatif" saves the following new ReferenceDocument to ReferenceDocuments section of NotesReceivable with Code "2021000001" at "01-Jan-2021 11:10 AM" with the following values:
      | DocumentType  | DocumentCode | AmountToCollect |
      | SALES_INVOICE | 2019100004   | 2000            |
    Then NotesReceivable with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2021 11:10 AM | Draft |
    And the lock by "Shady.Abdelatif" on ReferenceDocument section of NotesReceivable with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"
    And ReferenceDocuments section of NotesReceivable with Code "2021000001" is updated as follows and displayed to "Shady.Abdelatif":
      | Code       | ReferenceDocument          | AmountToCollect |
      | 2021000001 | 2020000003 - Sales Order   | 4000            |
      | 2021000001 | 2019100004 - Sales Invoice | 2000            |

  # EBS-8484
  Scenario: (03) Save Add ReferenceDocument in NotesReceivable, Add NotesReceivable with "editable" fields in "editable states" - with all fields or only mandatory fields - within edit session (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "01-Jan-2021 11:00 AM"
    When "Shady.Abdelatif" saves the following new ReferenceDocument to ReferenceDocuments section of NotesReceivable with Code "2021000001" at "01-Jan-2021 11:10 AM" with the following values:
      | DocumentType     | DocumentCode | AmountToCollect |
      | NOTES_RECEIVABLE | 2021000002   | 1000            |
    Then NotesReceivable with Code "2021000001" is updated as follows:
      | LastUpdatedBy   | LastUpdateDate       | State |
      | Shady.Abdelatif | 01-Jan-2021 11:10 AM | Draft |
    And the lock by "Shady.Abdelatif" on ReferenceDocument section of NotesReceivable with code "2021000001" is released
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-04"
    And ReferenceDocuments section of NotesReceivable with Code "2021000001" is updated as follows and displayed to "Shady.Abdelatif":
      | Code       | ReferenceDocument             | AmountToCollect |
      | 2021000001 | 2020000003 - Sales Order      | 4000            |
      | 2021000001 | 2021000002 - Notes Receivable | 1000            |
