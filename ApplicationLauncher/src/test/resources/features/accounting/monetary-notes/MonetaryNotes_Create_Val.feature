# Author Dev: Yara Ameen
# Updated: Pirates Team (5-July-2021) for # EBS-8808
# Author Quality:
Feature: Create NotesReceivables - VAL

  Background:

    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia | PurUnitReader_Signmedia         |
      | Accountant_Signmedia | CustomerViewer_Signmedia        |
      | Accountant_Signmedia | CompanyViewer                   |
      | Accountant_Signmedia | CurrencyViewer                  |
      | Accountant_Signmedia | DocumentOwnerViewer             |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:Create  |                                |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll  | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia        | Customer:ReadAll        | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                   | Company:ReadAll         |                                |
      | CurrencyViewer                  | Currency:ReadAll        |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll   |                                |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject  | Permission         | Condition |
      | 34 | Ashraf Salah | NotesReceivable | CanBeDocumentOwner |           |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |

    #EBS-8596
    #EBS-8808
  Scenario Outline: (01) Create NotesReceivables, with missing Mandatory field (Abuse Case\Client bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" creates NotesReceivable with the following values:
      | NoteForm   | Type   | BusinessUnit   | Company   | BusinessPartner   | DocumentOwnerId   | Amount   | CurrencyISO   |
      | <NoteForm> | <Type> | <BusinessUnit> | <Company> | <BusinessPartner> | <DocumentOwnerId> | <Amount> | <CurrencyISO> |
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
    Examples:
      | NoteForm | Type                           | BusinessUnit | Company | BusinessPartner | DocumentOwnerId | Amount | CurrencyISO |
      | CHEQUE   | NOTES_RECEIVABLE_AS_COLLECTION | 0002         | 0002    |                 | 34              | 7000   | EGP         |
      | CHEQUE   | NOTES_RECEIVABLE_AS_COLLECTION | 0002         | 0002    | 000007          | 34              | 0      | EGP         |
      | CHEQUE   | NOTES_RECEIVABLE_AS_COLLECTION | 9999         | 0002    | 000007          | 34              | 10     | EGP         |
