# Author Dev: Updated By Order Team (10-May-2021)
# Author Quality: Shirin Mahmoud

Feature: Request to edit and cancel NotesDetails section in NotesReceivable

  Background:
    Given the following users and roles exist:
      | Name                    | Role                                     |
      | hr1                     | SuperUser                                |
      | Shady.Abdelatif         | Accountant_Signmedia                     |
      | Shady.Abdelatif.NoDepot | Accountant_Signmedia_CannotViewDepotRole |

    And the following roles and sub-roles exist:
      | Role                                     | Sub-role                        |
      | SuperUser                                | SuperUserSubRole                |
      | Accountant_Signmedia                     | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia                     | DepotViewer                     |

      | Accountant_Signmedia_CannotViewDepotRole | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotViewDepotRole | NotesReceivableViewer_Signmedia |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                                   | Condition                      |
      | SuperUserSubRole                | *:*                                          |                                |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll                      | [purchaseUnitName='Signmedia'] |
      | DepotViewer                     | Depot:ReadAll                                |                                |

    And the following users doesn't have the following permissions:
      | User                    | Permission    |
      | Shady.Abdelatif.NoDepot | Depot:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User            | Permission                                   | Condition                  |
      | Shady.Abdelatif | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Ahmed.Hamdi     | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000003 | 0001 - Flexo     | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |

    And edit session is "30" minutes

  #EBS-4946
  Scenario:(01) Request to edit NotesDetails section by an authorized user in Draft State (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to edit NotesDetails section of NotesReceivable with code "2021000002"
    Then NotesDetails section of NotesReceivable with code "2021000002" becomes in edit mode and locked by "Shady.Abdelatif"
    And the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads |
      | ReadAllDepots   |
    And there are no mandatory fields returned to "Shady.Abdelatif"
    And the following editable fields are returned to "Shady.Abdelatif":
      | EditableFields |
      | noteNumber     |
      | noteBank       |
      | depotCode      |
      | dueDate        |

   #EBS-4946
  Scenario:(02) Request to edit NotesDetails section by an authorized user in Active State (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to edit NotesDetails section of NotesReceivable with code "2021000001"
    Then NotesDetails section of NotesReceivable with code "2021000001" becomes in edit mode and locked by "Shady.Abdelatif"
    And the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads |
      | ReadAllDepots   |
    And the following mandatory fields are returned to "Shady.Abdelatif":
      | MandatoriesFields |
      | noteNumber        |
      | noteBank          |
      | depotCode         |
      | dueDate           |
    And the following editable fields are returned to "Shady.Abdelatif":
      | EditableFields |
      | depotCode      |

  #EBS-4946
  Scenario:(03) Request to edit NotesDetails section that is locked by another user (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first opened the NotesDetails section of NotesReceivable with code "2021000002" in the edit mode successfully
    When "Shady.Abdelatif" requests to edit NotesDetails section of NotesReceivable with code "2021000002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

  #EBS-4946
  @Future
  Scenario:(04) Request to edit NotesDetails section of deleted NotesReceivable (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the NotesReceivable with code "2021000002" successfully
    When "Shady.Abdelatif" requests to edit NotesDetails section of NotesReceivable with code "2021000002"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-4946
  Scenario:(05) Request to edit NotesDetails section with unauthorized user (with condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to edit NotesDetails section of NotesReceivable with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page

  #EBS-4946
  Scenario: (06) Request to edit NotesDetails section (No authorized reads)
    Given user is logged in as "Shady.Abdelatif.NoDepot"
    When "Shady.Abdelatif.NoDepot" requests to edit NotesDetails section of NotesReceivable with code "2021000002"
    Then NotesDetails section of NotesReceivable with code "2021000002" becomes in edit mode and locked by "Shady.Abdelatif.NoDepot"
    And there are no authorized reads returned to "Shady.Abdelatif.NoDepot"
    And there are no mandatory fields returned to "Shady.Abdelatif.NoDepot"
    And the following editable fields are returned to "Shady.Abdelatif.NoDepot":
      | EditablleFields |
      | noteNumber      |
      | noteBank        |
      | depotCode       |
      | dueDate         |

  ######## Cancel Edit NoteDetails HP ########################################################################

  #EBS-4946
  Scenario:(07) Request to cancel saving NotesDetails section by an authorized user with one role (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And NotesDetails section of NotesReceivable with code "2021000002" is locked by "Shady.Abdelatif" at "17-Jan-2021 09:10 AM"
    When "Shady.Abdelatif" cancels saving NotesDetails section of NotesReceivable with code "2021000002" at "17-Jan-2021 09:30 AM"
    Then the lock by "Shady.Abdelatif" on NotesDetails section of NotesReceivable with code "2021000002" is released

  #EBS-4946
  Scenario:(08) Request to cancel saving NotesDetails section after lock session is expire
    Given user is logged in as "Shady.Abdelatif"
    And NotesDetails section of NotesReceivable with code "2021000002" is locked by "Shady.Abdelatif" at "17-Jan-2021 09:00 AM"
    When "Shady.Abdelatif" cancels saving NotesDetails section of NotesReceivable with code "2021000002" at "17-Jan-2021 09:31 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-07"

   #EBS-4946
  @Future
  Scenario:(09) Request to cancel saving NotesDetails section after lock session is expire and NotesReceivable doesn't exsit (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And NotesDetails section of NotesReceivable with code "2021000002" is locked by "Shady.Abdelatif" at "17-Jan-2021 09:00 AM"
    And "hr1" first deleted the NotesReceivable with code "2021000002" successfully at "17-Jan-2021 09:31 AM"
    When "Shady.Abdelatif" cancels saving NotesDetails section of NotesReceivable with code "2021000002" at "17-Jan-2021 09:35 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  #EBS-4946
  Scenario:(10) Request to cancel saving NotesDetails section with unauthorized user (with condition) (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" cancels saving NotesDetails section of NotesReceivable with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
