Feature: Allowed Actions For NotesReceivable (Home Screen)

  Background:
    Given the following users and roles exist:
      | Name           | Role                            |
      | Ahmed.Al-Ashry | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi    | AdminTreasury                   |
      | CreateOnlyUser | CreateOnlyRole                  |
      | Afaf           | FrontDesk                       |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                         |
      | CollectionResponsible_Signmedia | NotesReceivableOwner_Signmedia  |
      | CollectionResponsible_Signmedia | NotesReceivableViewer_Signmedia |
      | AdminTreasury                   | NotesReceivableViewer           |
      | CreateOnlyRole                  | CreateOnlySubRole               |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:Create  |                                |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer           | NotesReceivable:ReadAll |                                |
      | CreateOnlySubRole               | NotesReceivable:Create  |                                |

    And the following users doesn't have the following permissions:
      | User           | Permission              |
      | Ahmed.Hamdi    | NotesReceivable:Create  |
      | Afaf           | NotesReceivable:Create  |
      | Afaf           | NotesReceivable:ReadAll |
      | CreateOnlyUser | NotesReceivable:ReadAll |

    And the following users have the following permissions without the following conditions:
      | User           | Permission              | Condition                  |
      | Ahmed.Al-Ashry | NotesReceivable:ReadAll | [purchaseUnitName='Flexo'] |
  
 # Home Screen Scenarios
 
 # EBS-5869
  Scenario Outline: (01) Read allowed actions in NotesReceivable Home Screen by authorized user (Happy Path)
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of NotesReceivable home screen
    Then the "<AllowedActions>" are displayed to "<User>":
    Examples:
      | User           | AllowedActions |
      | Ahmed.Al-Ashry | ReadAll,Create |
      | Ahmed.Hamdi    | ReadAll        |
      | CreateOnlyUser | Create         |
 
 # EBS-5869
  Scenario: (02) Read allowed actions in NotesReceivable Home Screen - By Unauthorized User (Exception)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read actions of NotesReceivable home screen
    Then an authorization error notification is sent to "Afaf" with the following message "Gen-msg-29"