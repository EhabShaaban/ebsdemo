# Author Dev: Fatma Al Zahraa

Feature: Delete ReferenceDocument From ReferenceDocuments section

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Sub-role                        |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia | SOViewer_Signmedia              |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                              | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:DeleteReferenceDocument | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll                    | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll                      | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll                 | [purchaseUnitName='Signmedia'] |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
    #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10  | 2000       |
   #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
  #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000                  | 27200                 | 27200     |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000  | 25000     |       |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER  | 4000            |

  # EBS-8538
  Scenario: (01) Delete ReferenceDocument from NotesReceivable, where NotesReceivable is Draft - by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to delete ReferenceDocument with id "1" of type "SALES_ORDER" in NotesReceivable with code "2021000001"
    Then ReferenceDocument with id "1" from NotesReceivable with code "2021000001" is deleted
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-12"
