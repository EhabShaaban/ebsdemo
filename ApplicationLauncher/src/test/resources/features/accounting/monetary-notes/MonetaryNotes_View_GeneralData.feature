# Author Dev: Order Team (30-March-2021)
# Author Quality: Shirin Mahmoud

Feature: View NotesReceivable GeneralData section

  Background:

    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Ashraf.Fathi    | M.D                  |
      | hr1             | SuperUser            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
      | M.D                  | NotesReceivableViewer           |
      | SuperUser            | SuperUserSubRole                |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer           | NotesReceivable:ReadAll |                                |
      | SuperUserSubRole                | *:*                     |                                |
    And the following users have the following permissions without the following conditions:
      | User            | Permission              | Condition                  |
      | Shady.Abdelatif | NotesReceivable:ReadAll | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |

 # EBS-5132
  Scenario: (01) View MonetaryNotes GeneralData section by an authorized user who has one role with one condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of MonetaryNotes with code "2021000001"
    Then the following values of GeneralData section for MonetaryNotes with code "2021000001" are displayed to "Shady.Abdelatif":
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft |

  # EBS-5132
  Scenario Outline: (02) View MonetaryNotes GeneralData section by an authorizd user who has one role without conditions (Happy Path)
    Given user is logged in as "Ashraf.Fathi"
    When "Ashraf.Fathi" requests to view GeneralData section of MonetaryNotes with code "<Code>"
    Then the following values of GeneralData section for MonetaryNotes with code "<Code>" are displayed to "Ashraf.Fathi":
      | Code   | CreationDate   | Type   | CreatedBy   | DocumentOwner   | State   |
      | <Code> | <CreationDate> | <Type> | <CreatedBy> | <DocumentOwner> | <State> |
    Examples:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |

  # EBS-5132
  @Future
  Scenario: (03) View MonetaryNotes GeneralData section where MonetaryNotes doesn't exist (Exception Case)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the MonetaryNotes with code "2021000001" successfully
    When "Shady.Abdelatif" requests to view GeneralData section of MonetaryNotes with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  # EBS-5132
  Scenario: (04) View MonetaryNotes GeneralData section by unauthorized user due to condition (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to view GeneralData section of MonetaryNotes with code "2021000002"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page
