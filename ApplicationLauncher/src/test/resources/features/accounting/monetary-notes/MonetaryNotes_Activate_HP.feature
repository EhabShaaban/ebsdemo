# Author: Marina & Eman Mansour on Date: 15-Dec-2019, 11:25 AM
# Reviewer: Somaya on Date: 15-Dec-2019, 12:25 PM

# Updated: Order Team (27-April-2021) for # EBS-8815
# Updated: Pirates Team (7-June-2021) for # EBS-8815
# Updated: Pirates Team (21-June-2021)
# Updated: Pirates Team (05-July-2021) for #EBS-8977
# Updated: Eman Mansour (15-Aug-2021) for #EBS-9230
# Reviewer: Shirin Mahmoud for # EBS-8815
#Reviewer: Somaya Ahmed (2-July-2021)

#TODO: Add to the givens the COA Configuration for 10208 - Notes receivables and 10203 - Customers (EBS-5004)
# TODO: Add a new or update existing scenario to Handle the case where the currency of the Collection is different from the Currency of the Sales Order and the Company. For example: Sales Order Currency: EGP, Company Currency: EGP, and notes Receivable  Currency: USD (EBS-7744)
# TODO: Add to the givens the existing ExchangeRate Strageies with only one true, for multi-currency support (EBS-7744)
# TODO: Add to the givens the existing ExchangeRates records,  for multi-currency support (EBS-7744)

Feature: NotesReceivables - Activate - HP

  Activate NotesReceivables has the following effects:
  - NotesReceivables state is changed to Active
  - NotesReceivables Activation Details are determined
  - NotesReceivable Accounting Details are determined  EBS-4914
  --- GLAccount are retrieved from COA Configuration
  --- GLSubAccounts are retieved from the Collection document
  - Journal Entries are created
  --- The retrieved fiscal year is the active one where the due date belongs to it
  --- The latest ExchangeRate is retrieved based on of default strategy
  - The referenced Sales Invoice remaining is updated EBS-9131
  - The referenced Sales Order remaining is updated EBS-9130
  - The referenced NotesReceivable remaining is updated EBS-6065
  - The referenced document in another currency than NotesReceivable EBS-9306
  - NotesReceivable Accounting Details are determined  EBS-4914
  - add JournalEntryDetails EBS-8977
  - handle case when multiple fiscal years active , to make it clear that the system chooses one of them based on the year of the Journal Date EBS-9230


  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                  |
      | AdminTreasury | NotesReceivablePostOwner |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission               | Condition |
      | NotesReceivablePostOwner | NotesReceivable:Activate |           |


   #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
        #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |
       #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
        #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
       #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |
       #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000004 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22              | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |


    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 25000.000 | 25000.000 | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 14000.000 | 14000.000 | 0001 - بنك القاهرة  باركليز - المهندسين |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType     | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 5000.102487     |
      | 2021000001 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000004   | SALES_ORDER      | 2999.797278     |
      | 2021000001 | 3                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE    | 4000.100235     |
      | 2021000001 | 4                         | NOTES_RECEIVABLE_AS_COLLECTION | 2021000003   | NOTES_RECEIVABLE | 13000           |
      | 2021000003 | 5                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 4000            |
      #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |
      | 0003 | 2022 | 1-Jan-2022 12:00 AM | 31-Dec-2022 11:59 PM | Active |

    #@INSERT
    And the following ExchangeRates records exit:
      | Code       | FirstCurrencyIso | SecondCurrencyIso | SecondValue | ValidationFrom       | Type                  |
      | 2020000035 | USD              | EGP               | 11.44       | 01-Feb-2021 09:00 AM | User Daily Rate       |
      | 2020000036 | EGP              | EGP               | 1           | 01-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000037 | USD              | EGP               | 15.44       | 01-Mar-2021 09:00 AM | Central Bank Of Egypt |
      | 2020000038 | USD              | EGP               | 15.65       | 02-Mar-2021 09:00 AM | User Daily Rate       |
      | 2020000039 | EGP              | EGP               | 1           | 02-Mar-2021 09:00 AM | Central Bank Of Egypt |

  # EBS-5874, EBS-5876, EBS-8815, EBS-4914, EBS-6065, EBS-9130, EBS-9131, EBS-8977
  Scenario: (01) Activate NotesReceivable, by an authorized user, all mandatory fields exist (Happy Path)
    Given user is logged in as "Ahmed.Hamdi"
    And Last JournalEntry Code is "2021000001"
    When "Ahmed.Hamdi" Activates NotesReceivable with code "2021000001" and JournalEntryDate "17-Jun-2022 11:00 AM" at "17-Jun-2021 09:30 AM"
    Then NotesReceivable with Code "2021000001" is updated as follows:
      | LastUpdatedBy | LastUpdateDate       | State  |
      | Ahmed.Hamdi   | 17-Jun-2021 09:30 AM | Active |
    And ActivationDetails in NotesReceivable with Code "2021000001" is updated as follows:
      | ActivatedBy | ActivationDate       | JournalEntryDate     | CurrencyPrice | JournalEntryCode | FiscalPeriod |
      | Ahmed.Hamdi | 17-Jun-2021 09:30 AM | 17-Jun-2022 11:00 AM | 1             | 2021000002       | 2022         |
    And Remaining of SalesOrder with code "2020000003" is updated as follows "22199.897513"
    And Remaining of SalesOrder with code "2020000004" is updated as follows "24200.202722"
    And Remaining of SalesInvoice with code "2019100004" is updated as follows "69.019765"
    And Remaining of NotesReceivable with code "2021000003" is updated as follows "13169.3290734825"
    And AccountingDetails in NotesReceivable with Code "2021000001" is updated as follows:
      | Credit/Debit | Account                   | SubAccount        | Value           |
      | DEBIT        | 10208 - Notes receivables | 2021000001        | 25000 EGP       |
      | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 5000.102487 EGP |
      | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 2999.797278 EGP |
      | CREDIT       | 10203 - Customers         | 000001 - Al Ahram | 4000.100235 EGP |
      | CREDIT       | 10208 - Notes receivables | 2021000003        | 13000 EGP       |
    And a new JournalEntry is created as follows:
      | Code       | CreationDate         | CreatedBy   | JournalDate          | BusinessUnit     | FiscalYear | ReferenceDocument            | Company          |
      | 2021000002 | 17-Jun-2021 09:30 AM | Ahmed.Hamdi | 17-Jun-2022 11:00 AM | 0002 - Signmedia | 2022       | 2021000001 - NotesReceivable | 0001 - AL Madina |
    And And the following new JournalEntryDetails are created for JournalEntry with code "2021000002":
      | JournalDate          | GLAccount                 | GLSubAccount      | Credit      | Debit | Currency(Document) | Currency(Company) | ExRateToCurrency(Company) | ExchangeRateCode |
      | 17-Jun-2022 11:00 AM | 10208 - Notes receivables | 2021000001        | 0           | 25000 | EGP                | EGP             | 1                         | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10203 - Customers         | 000001 - Al Ahram | 5000.102487 | 0     | EGP                | EGP             | 1                         | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10203 - Customers         | 000001 - Al Ahram | 2999.797278 | 0     | EGP                | EGP             | 1                         | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10203 - Customers         | 000001 - Al Ahram | 4000.100235 | 0     | EGP                | EGP             | 1                         | 2020000036       |
      | 17-Jun-2022 11:00 AM | 10208 - Notes receivables | 2021000003        | 13000       | 0     | EGP                | EGP             | 1                         | 2020000036       |
    And a success notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-47"
