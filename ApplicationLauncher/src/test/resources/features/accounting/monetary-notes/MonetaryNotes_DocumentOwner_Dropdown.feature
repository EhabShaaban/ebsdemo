# Author Dev: Order Team (30-March-2021)
# Author Quality: Shirin Mahmoud

Feature: ViewAll DocumentOwners Dropdown

  Background:
    Given the following users and roles exist:
      | Name            | Role                     |
      | Shady.Abdelatif | Accountant_Signmedia     |
      | Ashraf.Salah    | AccountantHead_Signmedia |
      | Afaf            | FrontDesk                |
    And the following roles and sub-roles exist:
      | Role                     | Subrole                      |
      | Accountant_Signmedia     | DocumentOwnerViewer          |
      | Accountant_Signmedia     | NotesReceivableDocumentOwner |
      | AccountantHead_Signmedia | NotesReceivableDocumentOwner |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission                         | Condition |
      | DocumentOwnerViewer          | DocumentOwner:ReadAll              |           |
      | NotesReceivableDocumentOwner | NotesReceivable:CanBeDocumentOwner |           |
    And the following users doesn't have the following permissions:
      | User | Permission                         |
      | Afaf | DocumentOwner:ReadAll              |
      | Afaf | NotesReceivable:CanBeDocumentOwner |
    And the following DocumentOwners exist:
      | Id | Name            | BusinessObject  | Permission         | Condition |
      | 34 | Ashraf Salah    | NotesReceivable | CanBeDocumentOwner |           |
      | 40 | Shady.Abdelatif | NotesReceivable | CanBeDocumentOwner |           |
    And the total number of existing "NotesReceivable" DocumentOwners are 2
# EBS-8648
  Scenario: (01) Read list of DocumentOwners dropdown, by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all DocumentOwners for "NotesReceivable"
    Then the following DocumentOwners values will be presented to "Shady.Abdelatif":
      | Name            |
      | Ashraf Salah    |
      | Shady.Abdelatif |
    And total number of DocumentOwners returned to "Shady.Abdelatif" in a dropdown is equal to 2
# EBS-8648
  Scenario: (02) Read list of DocumentOwners dropdown, by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all DocumentOwners for "NotesReceivable"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
