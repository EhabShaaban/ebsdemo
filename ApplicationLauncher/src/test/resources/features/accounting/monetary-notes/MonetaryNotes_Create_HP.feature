# updated by: Fatma Al Zahraa (EBS - 4865)
# updated by: Fatma Al Zahraa (EBS - 8714)
# updated by: Fatma Al Zahraa (EBS - 8716)
# Author Quality: Shirin Mahmoud

Feature: Create NotesReceivable - Happy Path

  Background:

    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                         |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia | PurUnitReader_Signmedia         |
      | Accountant_Signmedia | CustomerViewer_Signmedia        |
      | Accountant_Signmedia | CompanyViewer                   |
      | Accountant_Signmedia | CurrencyViewer                  |
      | Accountant_Signmedia | DocumentOwnerViewer             |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:Create  |                                |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll  | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia        | Customer:ReadAll        | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                   | Company:ReadAll         |                                |
      | CurrencyViewer                  | Currency:ReadAll        |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll   |                                |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject  | Permission         | Condition |
      | 34 | Ashraf Salah | NotesReceivable | CanBeDocumentOwner |           |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |

  #EBS-4865
  #EBS-8714
  #EBS-8716
  Scenario Outline: (01) Create NotesReceivable, with all mandatory fields by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And CurrentDateTime = "01-Jan-2019 11:00 AM"
    And Last created NotesReceivable was with code "2019000099"
    When "Shady.Abdelatif" creates NotesReceivable with the following values:
      | NoteForm        | Type                           | BusinessUnit | Company | BusinessPartner | DocumentOwnerId | Amount | CurrencyISO |
      | <NoteFormValue> | NOTES_RECEIVABLE_AS_COLLECTION | 0002         | 0002    | 000007          | 34              | 7000   | EGP         |
    Then a new NotesReceivable is created with the following values:
      | Code       | State | CreatedBy       | LastUpdatedBy   | CreationDate         | LastUpdateDate       | Type                           | DocumentOwner |
      | 2019000100 | Draft | Shady.Abdelatif | Shady.Abdelatif | 01-Jan-2019 11:00 AM | 01-Jan-2019 11:00 AM | NOTES_RECEIVABLE_AS_COLLECTION | Ashraf Salah  |
    And the NotesReceivableCompany Section of NotesReceivable with code "2019000100" is updated as follows:
      | BusinessUnit     | Company        |
      | 0002 - Signmedia | 0002 - DigiPro |
    And the NotesReceivableDetails Section of NotesReceivable with code "2019000100" is updated as follows:
      | NoteForm        | BusinessPartner                | NoteNumber | NoteBank | DueDate | CurrencyISO | Amount   | Remaining | Depot |
      | <NoteFormValue> | 000007 - مطبعة أكتوبر الهندسية |            |          |         | EGP         | 7000.000 | 7000.000  |       |
    And a success notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-11"
    Examples:
      | NoteFormValue    |
      | CHEQUE           |
      | BILL_OF_EXCHANGE |
      | TRUST_RECEIPT    |