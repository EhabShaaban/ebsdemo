# Author: Aya & Eman Mansour on Date:23-Dec-2019, 15:33 AM

Feature: NotesReceivables - Activate - Val

  Activate NotesReceivables has the following val:
  1- Total of amount to collect equals NR amount

  Background:
    Given the following users and roles exist:
      | Name        | Role          |
      | Ahmed.Hamdi | AdminTreasury |
    And the following roles and sub-roles exist:
      | Role          | Subrole                  |
      | AdminTreasury | NotesReceivablePostOwner |
    And the following sub-roles and permissions exist:
      | Subrole                  | Permission               | Condition |
      | NotesReceivablePostOwner | NotesReceivable:Activate |           |

    #@INSERT
    And the following FiscalYears exist:
      | Code | Name | From                | To                   | State  |
      | 0001 | 2020 | 1-Jan-2020 12:00 AM | 31-Dec-2020 11:59 PM | Closed |
      | 0002 | 2021 | 1-Jan-2021 12:00 AM | 31-Dec-2021 11:59 PM | Active |

       #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State    | CreationDate         |DocumentOwner  |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved | 11-Feb-2020 01:28 PM |Manar Mohammed |
        #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000003 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |
       #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
        #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit   | Qty    | SalesPrice |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg | 10.000 | 2000.000   |
       #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |
       #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000004 | 20000.000              | 27200.000             | 27200.000 |

    #@INSERT
    Given the following GeneralData for SalesInvoices exist:
      | SICode     | Type                                                          | DocumentOwner  | CreatedBy      | State         | CreationDate         |
      | 2019100004 | SALES_INVOICE_FOR_SALES_ORDER - Sales invoice for sales order | Ahmed Al-Ashry | Ahmed.Al-Ashry | Opened,Active | 01-Dec-2019 02:20 PM |
    #@INSERT
    And the following Company for SalesInvoices exist:
      | SICode     | Company          | PurchasingUnit   |
      | 2019100004 | 0001 - AL Madina | 0002 - Signmedia |
    #@INSERT
    And the following Details for SalesInvoices exist:
      | SICode     | Customer          | SalesOrder | PaymentTerm                                | Currency | DownPayment |
      | 2019100004 | 000001 - Al Ahram | 2020000003 | 0001 - 20% Deposit, 80% T/T Before Loading | EGP      | 5.00        |
        #@INSERT
    And the following Items for SalesInvoices exist:
      | SICode     | Item                | OrderUnit | Qty(OrderUnit) | UnitPrice(Base) | BaseUnit  |
      | 2019100004 | 000008 - PRO-V-ST-2 | 0030 - PC | 136            | 22.00           | 0019 - M2 |
    #@INSERT
    And the following SalesInvoices TaxDetails exist:
      | SICode     | Tax                                          | TaxPercentage | TaxAmount |
      | 2019100004 | 0007 - Vat                                   | 1.00          | 29.92     |
      | 2019100004 | 0008 - Commercial and industrial profits tax | 0.5           | 14.96     |
      | 2019100004 | 0009 - Real estate taxes                     | 10.00         | 299.2     |
      | 2019100004 | 0010 - Service tax                           | 12.00         | 359.04    |
      | 2019100004 | 0011 - Adding tax                            | 0.5           | 14.96     |
      | 2019100004 | 0012 - Sales tax                             | 12.00         | 359.04    |
    #@INSERT
    And the following SalesInvoice Remaining exist:
      | SICode     | Remaining |
      | 2019100004 | 4069.12   |
    And the following Summaries for SalesInvoices exist:
      | SICode     | TotalBeforeTax | TotalAfterTax | Remaining |
      | 2019100004 | 2992.00        | 4069.12       | 4069.12   |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000004 | 16-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000003 | 0002 - Signmedia | 0001 - AL Madina |
      | 2021000004 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 2000.000  | 2000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 14000.000 | 14000.000 | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000004 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0001 - EGP | 14000.000 | 14000.000 | 0001 - بنك القاهرة  باركليز - المهندسين |
    And Insert the following ReferenceDocuments for MonetaryNotes:
      | Code       | MonetaryNoteRefDocumentId | Type                           | DocumentCode | DocumentType     | AmountToCollect |
      | 2021000001 | 1                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 5000.102487     |
      | 2021000001 | 2                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000004   | SALES_ORDER      | 2999.797278     |
      | 2021000001 | 3                         | NOTES_RECEIVABLE_AS_COLLECTION | 2019100004   | SALES_INVOICE    | 4000.100235     |
      | 2021000001 | 4                         | NOTES_RECEIVABLE_AS_COLLECTION | 2021000003   | NOTES_RECEIVABLE | 13000           |
      | 2021000003 | 5                         | NOTES_RECEIVABLE_AS_COLLECTION | 2020000003   | SALES_ORDER      | 4000            |


  #EBS-9162
  Scenario: (01) Activate NotesReceivable, by an authorized user, total amount not equal NR amount (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates NotesReceivable with code "2021000001" and JournalEntryDate "17-Jun-2021 11:00 AM" at "17-Jun-2021 09:30 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-05"
    And the following error message is attached to "amount" field "NR-msg-05" and sent to "Ahmed.Hamdi"

  #EBS-8814
  Scenario: (02) Activate NotesReceivable, by an authorized user, no Reference Documents (Exception Case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates NotesReceivable with code "2021000004" and JournalEntryDate "17-Jun-2021 11:00 AM" at "17-Jun-2021 09:30 AM"
    Then a failure notification is sent to "Ahmed.Hamdi" with the following message "Gen-msg-40"
    And the following fields "ReferenceDocuments" which sent to "Ahmed.Hamdi" are marked as missing

    #EBS-9255
  Scenario Outline: (03) Activate NotesReceivable, by an authorized user, missing or malicious input (Abuse case)
    Given user is logged in as "Ahmed.Hamdi"
    When "Ahmed.Hamdi" Activates NotesReceivable with code "2021000001" and JournalEntryDate "<JournalEntryDate>" at "17-Jun-2021 09:30 AM"
    Then "Ahmed.Hamdi" is logged out
    And "Ahmed.Hamdi" is forwarded to the error page

    Examples:
      | JournalEntryDate |
      |                  |
      | Ay 7aga          |
      | N/A              |


