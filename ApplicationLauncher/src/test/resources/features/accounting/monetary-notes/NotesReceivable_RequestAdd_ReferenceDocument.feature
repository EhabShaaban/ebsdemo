# Author Dev: Order Team (24-May-2021)
# Author Quality: Shirin Mahmoud

Feature: Request Add NotesReceivable Reference Document section

  Background:
    Given the following users and roles exist:
      | Name                           | Role                                        |
      | Shady.Abdelatif                | Accountant_Signmedia                        |
      | hr1                            | SuperUser                                   |
      | Shady.Abdelatif.NoSalesInvoice | Accountant_Signmedia_CannotReadSalesInvoice |
      | Shady.Abdelatif.NoSalesOrder   | Accountant_Signmedia_CannotReadSalesOrder   |
      | Shady.Abdelatif.NoDebitNote    | Accountant_Signmedia_CannotReadDebitNote    |
    And the following roles and sub-roles exist:
      | Role                                        | Subrole                         |
      | Accountant_Signmedia                        | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia                        | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia                        | SOViewer_Signmedia              |
      | Accountant_Signmedia                        | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia                        | AccountingNoteViewer_Signmedia  |
      | SuperUser                                   | SuperUserSubRole                |
      | Accountant_Signmedia_CannotReadSalesInvoice | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadSalesInvoice | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadSalesInvoice | SOViewer_Signmedia              |
      | Accountant_Signmedia_CannotReadSalesInvoice | AccountingNoteViewer_Signmedia  |
      | Accountant_Signmedia_CannotReadSalesOrder   | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadSalesOrder   | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadSalesOrder   | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadSalesOrder   | AccountingNoteViewer_Signmedia  |
      | Accountant_Signmedia_CannotReadDebitNote    | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadDebitNote    | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadDebitNote    | SalesInvoiceViewer_Signmedia    |
      | Accountant_Signmedia_CannotReadDebitNote    | SOViewer_Signmedia              |
    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                           | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
      | SalesInvoiceViewer_Signmedia    | SalesInvoice:ReadAll                 | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia              | SalesOrder:ReadAll                   | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll              | [purchaseUnitName='Signmedia'] |
      | AccountingNoteViewer_Signmedia  | AccountingNote:ReadAll               | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole                | *:*                                  |                                |

    And the following users doesn't have the following permissions:
      | User                           | Permission             |
      | Shady.Abdelatif.NoSalesInvoice | SalesInvoice:ReadAll   |
      | Shady.Abdelatif.NoSalesOrder   | SalesOrder:ReadAll     |
      | Shady.Abdelatif.NoDebitNote    | AccountingNote:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User            | Permission                           | Condition                  |
      | Shady.Abdelatif | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Flexo'] |

    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000003 | 0001 - Flexo     | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000001 - Al Ahram | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |

  # EBS-6071
  Scenario: (01) Request to add ReferenceDocument in NotesReceivable, by an authorized user have all authorized reads (SalesInvoices, SalesOrders, NotesReceivables, DebitNotes) (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then ReferenceDocument section of NotesReceivable with code "2021000001" becomes locked by "Shady.Abdelatif"
    Then the following authorized reads are returned to "Shady.Abdelatif":
      | AuthorizedReads            |
      | ReadReferenceDocumentTypes |
      | ReadSalesInvoices          |
      | ReadSalesOrders            |
      | ReadNotesReceivables       |
      | ReadDebitNotes             |
    And the following mandatory fields are returned to "Shady.Abdelatif":
      | MandatoriesFields |
      | documentType      |
      | refDocumentCode   |
      | amountToCollect   |
    And the following editable fields are returned to "Shady.Abdelatif":
      | EditableFields  |
      | documentType    |
      | refDocumentCode |
      | amountToCollect |

  # EBS-6071
  Scenario: (02) Request to add ReferenceDocument in NotesReceivable, by an authorized user doesn't have SalesInvoices authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoSalesInvoice"
    When "Shady.Abdelatif.NoSalesInvoice" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then ReferenceDocument section of NotesReceivable with code "2021000001" becomes locked by "Shady.Abdelatif.NoSalesInvoice"
    Then the following authorized reads are returned to "Shady.Abdelatif.NoSalesInvoice":
      | AuthorizedReads            |
      | ReadReferenceDocumentTypes |
      | ReadSalesOrders            |
      | ReadNotesReceivables       |
      | ReadDebitNotes             |
    And the following mandatory fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | MandatoriesFields |
      | documentType      |
      | refDocumentCode   |
      | amountToCollect   |
    And the following editable fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | EditableFields  |
      | documentType    |
      | refDocumentCode |
      | amountToCollect |

  # EBS-6071
  Scenario: (03) Request to add ReferenceDocument in NotesReceivable, by an authorized user doesn't have SalesOrders authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoSalesOrder"
    When "Shady.Abdelatif.NoSalesOrder" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then ReferenceDocument section of NotesReceivable with code "2021000001" becomes locked by "Shady.Abdelatif.NoSalesOrder"
    Then the following authorized reads are returned to "Shady.Abdelatif.NoSalesOrder":
      | AuthorizedReads            |
      | ReadReferenceDocumentTypes |
      | ReadSalesInvoices          |
      | ReadNotesReceivables       |
      | ReadDebitNotes             |
    And the following mandatory fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | MandatoriesFields |
      | documentType      |
      | refDocumentCode   |
      | amountToCollect   |
    And the following editable fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | EditableFields  |
      | documentType    |
      | refDocumentCode |
      | amountToCollect |

  # EBS-6071
  Scenario: (04) Request to add ReferenceDocument in NotesReceivable, by an authorized user doesn't have DebitNotes authorized reads (Happy Path)
    Given user is logged in as "Shady.Abdelatif.NoDebitNote"
    When "Shady.Abdelatif.NoDebitNote" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then ReferenceDocument section of NotesReceivable with code "2021000001" becomes locked by "Shady.Abdelatif.NoDebitNote"
    Then the following authorized reads are returned to "Shady.Abdelatif.NoDebitNote":
      | AuthorizedReads            |
      | ReadReferenceDocumentTypes |
      | ReadSalesOrders            |
      | ReadNotesReceivables       |
      | ReadSalesInvoices          |
    And the following mandatory fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | MandatoriesFields |
      | documentType      |
      | refDocumentCode   |
      | amountToCollect   |
    And the following editable fields are returned to "Shady.Abdelatif.NoSalesInvoice":
      | EditableFields  |
      | documentType    |
      | refDocumentCode |
      | amountToCollect |

  # EBS-6071
  Scenario: (05)  Request to add ReferenceDocument in NotesReceivable, when ReferenceDocument section is locked by another user (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    When "Shady.Abdelatif" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-02"

  @Future
  # EBS-6071
  Scenario: (06) Request to add ReferenceDocument in NotesReceivable, that doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And "hr1" first deleted the NotesReceivable with code "2021000001" successfully
    When "Shady.Abdelatif" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  # EBS-6071
  Scenario: (07) Request to add ReferenceDocument in NotesReceivable, in a state that doesn't allow this action [Active] (Exception)
    Given user is logged in as "hr1"
    When "hr1" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000002"
    Then an error notification is sent to "hr1" with the following message "Gen-msg-03"
    And ReferenceDocument section of NotesReceivable with code "2021000002" is not locked by "hr1"

  # EBS-6071
  Scenario: (08) Request to add ReferenceDocument in NotesReceivable, by an unauthorized user (with condition)(Abuse Case \ Client Bypassing)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page


    ######## Cancel add ReferenceDocument HP ########################################################################

  # EBS-6071
  Scenario: (09) Request to Cancel add ReferenceDocument in NotesReceivable, within the edit session by an authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:10 AM"
    When "Shady.Abdelatif" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001" at "07-Jan-2021 09:30 AM"
    Then the lock by "Shady.Abdelatif" on ReferenceDocument section of NotesReceivable with code "2021000001" is released

  # EBS-6071
  Scenario: (10) Request to Cancel add ReferenceDocument in NotesReceivable, after lock session is expired
    Given user is logged in as "Shady.Abdelatif"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:00 AM"
    When "Shady.Abdelatif" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001" at "07-Jan-2021 09:31 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-07"

  @Future
  # EBS-6071
  Scenario: (11) Request to Cancel add ReferenceDocument in NotesReceivable, after lock session is expire and NotesReceivable doesn't exist (Exception)
    Given user is logged in as "Shady.Abdelatif"
    And another user is logged in as "hr1"
    And ReferenceDocument section of NotesReceivable with code "2021000001" is locked by "Shady.Abdelatif" at "07-Jan-2021 09:00 AM"
    And "hr1" first deleted the NotesReceivable with code "2021000001" successfully at "07-Jan-2021 09:31 AM"
    When "Shady.Abdelatif" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000001" at "07-Jan-2021 09:35 AM"
    Then an error notification is sent to "Shady.Abdelatif" with the following message "Gen-msg-01"

  # EBS-6071
  Scenario: (12) Request to Cancel add ReferenceDocument in NotesReceivable, by unauthorized user (with condition) (Abuse Case)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code "2021000003"
    Then "Shady.Abdelatif" is logged out
    And "Shady.Abdelatif" is forwarded to the error page


