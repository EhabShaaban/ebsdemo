# Author Dev: Order Team (18-April-2021)
# Author Quality: Shirin Mahmoud

Feature: Allowed Actions For NotesReceivable (Object Screen)

  Background:
    Given the following users and roles exist:
      | Name               | Role                            |
      | Ahmed.Al-Ashry     | CollectionResponsible_Signmedia |
      | Ahmed.Hamdi        | AdminTreasury                   |
      | hr1                | SuperUser                       |
      | CannotReadSections | CannotReadSectionsRole          |

    And the following roles and sub-roles exist:
      | Role                            | Subrole                         |
      | CollectionResponsible_Signmedia | NotesReceivableOwner_Signmedia  |
      | CollectionResponsible_Signmedia | NotesReceivableViewer_Signmedia |
      | AdminTreasury                   | NotesReceivablePostOwner        |
      | AdminTreasury                   | NotesReceivableViewer           |
      | CannotReadSectionsRole          | CannotReadSectionsSubRole       |
      | SuperUser                       | SuperUserSubRole                |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission                                   | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:Delete                       | [purchaseUnitName='Signmedia'] |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Signmedia'] |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:AddReferenceDocument         | [purchaseUnitName='Signmedia'] |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:DeleteReferenceDocument      | [purchaseUnitName='Signmedia'] |

      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll                      | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadCompanyData              | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadNotesReceivableDetails   | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAccountingDetails        | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadActivationDetails        | [purchaseUnitName='Signmedia'] |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadReferenceDocuments       | [purchaseUnitName='Signmedia'] |

      | NotesReceivablePostOwner        | NotesReceivable:Activate                     |                                |

      | NotesReceivableViewer           | NotesReceivable:ReadAll                      |                                |
      | NotesReceivableViewer           | NotesReceivable:ReadCompanyData              |                                |
      | NotesReceivableViewer           | NotesReceivable:ReadNotesReceivableDetails   |                                |
      | NotesReceivableViewer           | NotesReceivable:ReadAccountingDetails        |                                |
      | NotesReceivableViewer           | NotesReceivable:ReadActivationDetails        |                                |
      | NotesReceivableViewer           | NotesReceivable:ReadReferenceDocuments       |                                |

      | CannotReadSectionsSubRole       | NotesReceivable:ReadAll                      |                                |
      | CannotReadSectionsSubRole       | NotesReceivable:Delete                       |                                |
      | CannotReadSectionsSubRole       | NotesReceivable:Activate                     |                                |
    And the following users doesn't have the following permissions:
      | User               | Permission                                   |
      | Ahmed.Al-Ashry     | NotesReceivable:Activate                     |
      | Ahmed.Hamdi        | NotesReceivable:Delete                       |
      | Ahmed.Hamdi        | NotesReceivable:UpdateNotesReceivableDetails |
      | Ahmed.Hamdi        | NotesReceivable:AddReferenceDocument         |
      | Ahmed.Hamdi        | NotesReceivable:DeleteReferenceDocument      |

      | CannotReadSections | NotesReceivable:UpdateNotesReceivableDetails |
      | CannotReadSections | NotesReceivable:AddReferenceDocument         |
      | CannotReadSections | NotesReceivable:DeleteReferenceDocument      |
      | CannotReadSections | NotesReceivable:ReadCompanyData              |
      | CannotReadSections | NotesReceivable:ReadNotesReceivableDetails   |
      | CannotReadSections | NotesReceivable:ReadAccountingDetails        |
      | CannotReadSections | NotesReceivable:ReadActivationDetails        |
      | CannotReadSections | NotesReceivable:ReadReferenceDocuments       |


    And the following users have the following permissions without the following conditions:
      | User           | Permission                                   | Condition                  |
      | Ahmed.Al-Ashry | NotesReceivable:Delete                       | [purchaseUnitName='Flexo'] |

      | Ahmed.Al-Ashry | NotesReceivable:ReadAll                      | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:ReadCompanyData              | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:ReadNotesReceivableDetails   | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:ReadAccountingDetails        | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:ReadActivationDetails        | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:ReadReferenceDocuments       | [purchaseUnitName='Flexo'] |

      | Ahmed.Al-Ashry | NotesReceivable:UpdateNotesReceivableDetails | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:AddReferenceDocument         | [purchaseUnitName='Flexo'] |
      | Ahmed.Al-Ashry | NotesReceivable:DeleteReferenceDocument      | [purchaseUnitName='Flexo'] |


    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft  |
      | 2021000002 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
      | 2021000003 | 15-Jun-2021 02:05 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Active |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner                | NoteNumber | NoteBank       | DueDate              | Currency   | Amount    | Remaining | Depot                                   |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram              | 263562356  | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |                                         |
      | 2021000002 | CHEQUE   | 000001 - Al Ahram              | 541212     | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 2021000003 | CHEQUE   | 000007 - مطبعة أكتوبر الهندسية | 541212     | Al - Ahly Bank | 20-Oct-2021 02:05 PM | 0001 - EGP | 1000.000  | 1000.000  | 0001 - بنك القاهرة  باركليز - المهندسين |


  #EBS-5869
  #EBS-8759
  Scenario Outline: (01) Read allowed actions in NotesReceivable Object Screen - All States [Draft-Active]
    Given user is logged in as "<User>"
    When "<User>" requests to read actions of NotesReceivable with code "<Code>"
    Then the "<AllowedActions>" are displayed to "<User>" and total number of returned allowed actions is "<NoOfActions>"
    Examples:
      | User               | Code       | State  | AllowedActions                                                                                                                                                                                                  | NoOfActions |
      | Ahmed.Al-Ashry     | 2021000001 | Draft  | Delete, ReadAll, ReadCompanyData, ReadNotesReceivableDetails, ReadAccountingDetails, ReadActivationDetails, ReadReferenceDocuments, UpdateNotesReceivableDetails, AddReferenceDocument, DeleteReferenceDocument | 10          |
      | Ahmed.Al-Ashry     | 2021000003 | Active | ReadAll, ReadCompanyData, ReadNotesReceivableDetails, ReadAccountingDetails, ReadActivationDetails, ReadReferenceDocuments, UpdateNotesReceivableDetails                                                        | 7           |
      | Ahmed.Hamdi        | 2021000001 | Draft  | Activate, ReadAll, ReadCompanyData, ReadNotesReceivableDetails, ReadAccountingDetails, ReadActivationDetails, ReadReferenceDocuments                                                                            | 7           |
      | Ahmed.Hamdi        | 2021000003 | Active | ReadAll, ReadCompanyData, ReadNotesReceivableDetails, ReadAccountingDetails, ReadActivationDetails, ReadReferenceDocuments                                                                                      | 6           |
      | CannotReadSections | 2021000001 | Draft  | Delete, ReadAll, Activate                                                                                                                                                                                       | 3           |
      | CannotReadSections | 2021000003 | Active | ReadAll                                                                                                                                                                                                         | 1           |

  #EBS-5869
  #EBS-8759
  Scenario: (02) Read allowed actions in NotesReceivable Object Screen - By Unauthorized User (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    When "Ahmed.Al-Ashry" requests to read actions of NotesReceivable with code "2021000002"
    Then an authorization error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-29"

  @Future
  #EBS-5869
  #EBS-8759
  Scenario: (03) Read allowed actions in NotesReceivable Object Screen - Object does not exist (Exception)
    Given user is logged in as "Ahmed.Al-Ashry"
    And another user is logged in as "hr1"
    And "hr1" first deleted the NotesReceivable with code "2021000001" successfully
    When "Ahmed.Al-Ashry" requests to read actions of NotesReceivable with code "2021000001"
    Then an error notification is sent to "Ahmed.Al-Ashry" with the following message "Gen-msg-01"