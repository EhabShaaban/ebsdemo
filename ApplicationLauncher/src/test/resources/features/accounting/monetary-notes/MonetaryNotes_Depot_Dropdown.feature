# Author Dev: Fatma Al-Zahra (EBS - 8807)
# Author Quality: Shirin Mahmoud

Feature: View All Depots

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole     |
      | Accountant_Signmedia | DepotViewer |
    And the following sub-roles and permissions exist:
      | Subrole     | Permission    | Condition |
      | DepotViewer | Depot:ReadAll |           |
    And the following users doesn't have the following permissions:
      | User | Permission    |
      | Afaf | Depot:ReadAll |
    And the following Depots exist:
      | Depot                                   |
      | 0001 - بنك القاهرة  باركليز - المهندسين |
      | 0002 - قسم التحصيل -  مرتدات / ويدوي    |
      | 0003 - محاكم - قضايا                    |
      | 0004 - أ.ق مرفوضة / مردودة للحافظة      |
      | 0005 - بنك مصر أمريكا الدولى- الدقى     |
      | 0006 - بنك دبى                          |
      | 0007 - بنك البنك الاهلى - الحلمية       |
    And the total number of existing Depots is 7

  Scenario: (01) Read list of Depots dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all Depots
    Then the following Depots values will be presented to "Shady.Abdelatif":
      | Depot                                   |
      | 0007 - بنك البنك الاهلى - الحلمية       |
      | 0006 - بنك دبى                          |
      | 0005 - بنك مصر أمريكا الدولى- الدقى     |
      | 0004 - أ.ق مرفوضة / مردودة للحافظة      |
      | 0003 - محاكم - قضايا                    |
      | 0002 - قسم التحصيل -  مرتدات / ويدوي    |
      | 0001 - بنك القاهرة  باركليز - المهندسين |
    And total number of Depots returned to "Shady.Abdelatif" is equal to 7

  Scenario: (02) Read list of Depots dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all Depots
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page