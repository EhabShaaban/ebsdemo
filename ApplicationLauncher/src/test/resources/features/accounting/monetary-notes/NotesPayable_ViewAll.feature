# Author Dev: Eman Mansour
# Author Quality: Shirin Mahmoud
Feature: View All Notes Payable

  Background: 
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | hr1             | SuperUser            |
      | Afaf            | FrontDesk            |
    And the following roles and sub-roles exist:
      | Role                 | Subrole                      |
      | Accountant_Signmedia | NotesPayableViewer_Signmedia |
      | SuperUser            | SuperUserSubRole             |
    And the following sub-roles and permissions exist:
      | Subrole                      | Permission           | Condition                      |
      | NotesPayableViewer_Signmedia | NotesPayable:ReadAll | [purchaseUnitName='Signmedia'] |
      | SuperUserSubRole             | *:*                  |                                |
    And the following users doesn't have the following permissions:
      | User | Permission           |
      | Afaf | NotesPayable:ReadAll |
    And the following users have the following permissions without the following conditions:
      | User            | Permission           | Condition                  |
      | Shady.Abdelatif | NotesPayable:ReadAll | [purchaseUnitName='Flexo'] |
    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate           | Type                     | CreatedBy | DocumentOwner   | State  |
      | 2021000001 | 15-Jun-2021 01:51 PM   | NOTES_PAYABLE_AS_PAYMENT | Admin     | Shady.Abdelatif | Active |
      | 2021000002 | 16-April-2021 02:05 PM | NOTES_PAYABLE_AS_PAYMENT | Admin     | Shady.Abdelatif | Draft  |
      | 2021000003 | 17-Mar-2021 02:05 PM   | NOTES_PAYABLE_AS_PAYMENT | Admin     | Ashraf Salah    | Draft  |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company        |
      | 2021000001 | 0002 - Signmedia | 0002 - DigiPro |
      | 2021000002 | 0001 - Flexo     | 0002 - DigiPro |
      | 2021000003 | 0002 - Signmedia | 0002 - DigiPro |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank       | DueDate              | Currency   | Amount      | Remaining   | Depot |
      | 2021000001 | CHEQUE   | 000002 - Zhejiang |  263562356 | QNB Bank       | 16-Jun-2021 02:05 PM | 0002 - USD |     25000.5 |     25000.5 |       |
      | 2021000002 | CHEQUE   | 000001 - Siegwerk |     541212 | Al - Ahly Bank | 17-Sep-2021 02:05 PM | 0001 - EGP | 1000.523681 | 1000.523681 |       |
      | 2021000003 | CHEQUE   | 000002 - Zhejiang |     541212 | Al - Ahly Bank | 20-Oct-2021 02:05 PM | 0001 - EGP |        1000 |        1000 |       |

  #EBS-6161
  Scenario: (01) View all NotesPayable, by an authorized user WITHOUT condition (Happy Path)
    Given user is logged in as "hr1"
    When "hr1" requests to read page 1 of NotesPayable with no filter applied with 20 records per page
    Then the following values will be presented to "hr1":
      | Code       | Type                     | BusinessPartner   | DueDate              | Amount            | Remaining         | BusinessUnit     | State  |
      | 2021000003 | NOTES_PAYABLE_AS_PAYMENT | 000002 - Zhejiang | 20-Oct-2021 02:05 PM | 1000 - EGP        | 1000 - EGP        | 0002 - Signmedia | Draft  |
      | 2021000002 | NOTES_PAYABLE_AS_PAYMENT | 000001 - Siegwerk | 17-Sep-2021 02:05 PM | 1000.523681 - EGP | 1000.523681 - EGP | 0001 - Flexo     | Draft  |
      | 2021000001 | NOTES_PAYABLE_AS_PAYMENT | 000002 - Zhejiang | 16-Jun-2021 02:05 PM | 25000.5 - USD     | 25000.5 - USD     | 0002 - Signmedia | Active |
    And the total number of records in search results by "hr1" are 3

  #EBS-6161
  Scenario: (02) View all NotesPayable, by an authorized user WITH condition (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read page 1 of NotesPayable with no filter applied with 20 records per page
    Then the following values will be presented to "Shady.Abdelatif":
      | Code       | Type                     | BusinessPartner   | DueDate              | Amount        | Remaining     | BusinessUnit     | State  |
      | 2021000003 | NOTES_PAYABLE_AS_PAYMENT | 000002 - Zhejiang | 20-Oct-2021 02:05 PM | 1000 - EGP    | 1000 - EGP    | 0002 - Signmedia | Draft  |
      | 2021000001 | NOTES_PAYABLE_AS_PAYMENT | 000002 - Zhejiang | 16-Jun-2021 02:05 PM | 25000.5 - USD | 25000.5 - USD | 0002 - Signmedia | Active |
    And the total number of records in search results by "Shady.Abdelatif" are 2
