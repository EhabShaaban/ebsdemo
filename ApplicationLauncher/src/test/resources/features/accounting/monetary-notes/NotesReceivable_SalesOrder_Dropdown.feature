# Author Dev: Eman Mansour
# Author SDETs: Ehab

Feature: ReferenceDocument - Sales Order DropDown

  Background:
    Given the following users and roles exist:
      | Name            | Role                 |
      | Shady.Abdelatif | Accountant_Signmedia |
      | Afaf            | FrontDesk            |

    And the following roles and sub-roles exist:
      | Role                 | Subrole                        |
      | Accountant_Signmedia | NotesReceivableOwner_Signmedia |
      | Accountant_Signmedia | SOViewer_Signmedia             |

    And the following sub-roles and permissions exist:
      | Subrole                        | Permission                           | Condition                      |
      | NotesReceivableOwner_Signmedia | NotesReceivable:AddReferenceDocument | [purchaseUnitName='Signmedia'] |
      | SOViewer_Signmedia             | SalesOrder:ReadAll                   | [purchaseUnitName='Signmedia'] |

    And the following users doesn't have the following permissions:
      | User | Permission         |
      | Afaf | SalesOrder:ReadAll |

    #@INSERT
    And the following GeneralData for SalesOrders exist:
      | Code       | Type                      | BusinessUnit     | SalesResponsible      | CreatedBy | State               | CreationDate         | DocumentOwner  |
      | 2020000001 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved            | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000002 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | Approved            | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000003 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | GoodsIssueActivated | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000004 | LOCAL_SALES_ORDER - Local | 0002 - Signmedia | 0004 - Ahmed.Al-Ashry | Admin     | GoodsIssueActivated | 11-Feb-2020 01:28 PM | Manar Mohammed |
      | 2020000005 | LOCAL_SALES_ORDER - Local | 0001 - Flexo     | 0004 - Ahmed.Al-Ashry | Admin     | Draft               | 11-Feb-2020 01:28 PM | Manar Mohammed |
    #@INSERT
    And the following Company for SalesOrders exist:
      | Code       | Company          | Store                      |
      | 2020000001 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000002 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000003 | 0002 - DigiPro   | 0001 - AlMadina Main Store |
      | 2020000004 | 0001 - AL Madina | 0001 - AlMadina Main Store |
      | 2020000005 | 0001 - AL Madina | 0001 - AlMadina Main Store |
    #@INSERT
    And the following SalesOrderData for SalesOrders exist:
      | Code       | Customer          | PaymentTerms                 | CustomerAddress                      | CurrencyISO | CustomerContactPerson | ExpectedDeliveryDate | Notes |
      | 2020000001 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000002 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000003 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000004 | 000001 - Al Ahram | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
      | 2020000005 | 000004 - Client3  | 0003 - 100% Advanced Payment | 000007 - 99, El Hegaz St, Heliopolis | EGP         | 000007 - Wael Fathay  | 05-Nov-2020 12:00 AM |       |
     #@INSERT
    And the following Items for SalesOrders exist:
      | Code       | Item                                | OrderUnit    | Qty    | SalesPrice |
      | 2020000001 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000002 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000003 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000004 | 000015 - Laptop HP Pro-book core i5 | 0020 - 20Kg  | 10.000 | 2000.000   |
      | 2020000005 | 000009 - Machine1                   | 0014 - Liter | 10.000 | 2000.000   |
    #@INSERT
    And the following TaxDetails for SalesOrders exist:
      | Code       | Tax                                          | TaxPercentage |
      | 2020000001 | 0007 - Vat                                   | 1             |
      | 2020000001 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000001 | 0009 - Real estate taxes                     | 10            |
      | 2020000001 | 0010 - Service tax                           | 12            |
      | 2020000001 | 0011 - Adding tax                            | 0.5           |
      | 2020000001 | 0012 - Sales tax                             | 12            |
      | 2020000002 | 0007 - Vat                                   | 1             |
      | 2020000002 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000002 | 0009 - Real estate taxes                     | 10            |
      | 2020000002 | 0010 - Service tax                           | 12            |
      | 2020000002 | 0011 - Adding tax                            | 0.5           |
      | 2020000002 | 0012 - Sales tax                             | 12            |
      | 2020000003 | 0007 - Vat                                   | 1             |
      | 2020000003 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000003 | 0009 - Real estate taxes                     | 10            |
      | 2020000003 | 0010 - Service tax                           | 12            |
      | 2020000003 | 0011 - Adding tax                            | 0.5           |
      | 2020000003 | 0012 - Sales tax                             | 12            |
      | 2020000004 | 0007 - Vat                                   | 1             |
      | 2020000004 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000004 | 0009 - Real estate taxes                     | 10            |
      | 2020000004 | 0010 - Service tax                           | 12            |
      | 2020000004 | 0011 - Adding tax                            | 0.5           |
      | 2020000004 | 0012 - Sales tax                             | 12            |
      | 2020000005 | 0007 - Vat                                   | 1             |
      | 2020000005 | 0008 - Commercial and industrial profits tax | 0.5           |
      | 2020000005 | 0009 - Real estate taxes                     | 10            |
      | 2020000005 | 0010 - Service tax                           | 12            |
      | 2020000005 | 0011 - Adding tax                            | 0.5           |
      | 2020000005 | 0012 - Sales tax                             | 12            |
    #@INSERT
    And the following SalesOrders have the following total amount and remainings:
      | Code       | TotalAmountBeforeTaxes | TotalAmountAfterTaxes | Remaining |
      | 2020000001 | 20000.000              | 27200.000             | 0.000     |
      | 2020000002 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000003 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000004 | 20000.000              | 27200.000             | 27200.000 |
      | 2020000005 | 20000.000              | 27200.000             | 27200.000 |
    And Insert the following GeneralData for MonetaryNotes:
      | Code       | CreationDate         | Type                           | CreatedBy | DocumentOwner   | State |
      | 2021000001 | 15-Jun-2021 01:51 PM | NOTES_RECEIVABLE_AS_COLLECTION | Admin     | Shady.Abdelatif | Draft |
    And Insert the following Company for MonetaryNotes:
      | Code       | BusinessUnit     | Company          |
      | 2021000001 | 0002 - Signmedia | 0001 - AL Madina |
    And Insert the following NotesDetails for MonetaryNotes:
      | Code       | NoteForm | BusinessPartner   | NoteNumber | NoteBank | DueDate              | Currency   | Amount    | Remaining | Depot |
      | 2021000001 | CHEQUE   | 000001 - Al Ahram | 263562356  | QNB Bank | 16-Jun-2021 02:05 PM | 0002 - USD | 25000.000 | 25000.000 |       |



   #EBS-8811
  Scenario: (01) Read list of SalesOrder - ReferenceDocument [in approved or goods issue activated State - has remaining > 0 - has the same businessPartner, company and businessUnit] dropdown by authorized user (Happy Path)
    Given user is logged in as "Shady.Abdelatif"
    When "Shady.Abdelatif" requests to read all SalesOrder - ReferenceDocument for NotesReceivable with code "2021000001"
    Then the following SalesOrder - ReferenceDocument values will be presented to "Shady.Abdelatif":
      | Code       |
      | 2020000004 |
      | 2020000002 |
    And total number of SalesOrder - ReferenceDocument returned to "Shady.Abdelatif" is equal to 2

  #EBS-8811
  Scenario: (02) Read list of SalesOrder - ReferenceDocument dropdown by unauthorized user (Unauthorized access/Abuse case)
    Given user is logged in as "Afaf"
    When "Afaf" requests to read all SalesOrder - ReferenceDocument for NotesReceivable with code "2021000001"
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page
