# Author: Evraam Hany, Eman Mansour
# QA Reviewer: Khadrah Ali, Ehab Shabaan

Feature: Create Notes Receivable - Authorization

  Background:
    Given the following users and roles exist:
      | Name                              | Role                                           |
      | Shady.Abdelatif                   | Accountant_Signmedia                           |
      | Shady.Abdelatif.NoPurchaseUnit    | Accountant_Signmedia_CannotReadPurchaseUnit    |
      | Shady.Abdelatif.NoCustomer        | Accountant_Signmedia_CannotReadCustomer        |
      | Shady.Abdelatif.NoCompany         | Accountant_Signmedia_CannotReadCompany         |
      | Shady.Abdelatif.NoCurrency        | Accountant_Signmedia_CannotReadCurrency        |
      | Shady.Abdelatif.NoDocumentOwner   | Accountant_Signmedia_CannotReadDocumentOwner   |
      | Shady.Abdelatif.NoNotesReceivable | Accountant_Signmedia_CannotReadNotesReceivable |
      | Afaf                              | FrontDesk                                      |

    And the following roles and sub-roles exist:
      | Role                                           | Subrole                         |
      | Accountant_Signmedia                           | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia                           | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia                           | PurUnitReader_Signmedia         |
      | Accountant_Signmedia                           | CustomerViewer_Signmedia        |
      | Accountant_Signmedia                           | CompanyViewer                   |
      | Accountant_Signmedia                           | CurrencyViewer                  |
      | Accountant_Signmedia                           | DocumentOwnerViewer             |

      | Accountant_Signmedia_CannotReadPurchaseUnit    | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadPurchaseUnit    | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadPurchaseUnit    | CustomerViewer_Signmedia        |
      | Accountant_Signmedia_CannotReadPurchaseUnit    | CompanyViewer                   |
      | Accountant_Signmedia_CannotReadPurchaseUnit    | CurrencyViewer                  |
      | Accountant_Signmedia_CannotReadPurchaseUnit    | DocumentOwnerViewer             |

      | Accountant_Signmedia_CannotReadCustomer        | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadCustomer        | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadCustomer        | PurUnitReader_Signmedia         |
      | Accountant_Signmedia_CannotReadCustomer        | CompanyViewer                   |
      | Accountant_Signmedia_CannotReadCustomer        | CurrencyViewer                  |
      | Accountant_Signmedia_CannotReadCustomer        | DocumentOwnerViewer             |

      | Accountant_Signmedia_CannotReadCompany         | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadCompany         | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadCompany         | PurUnitReader_Signmedia         |
      | Accountant_Signmedia_CannotReadCompany         | CustomerViewer_Signmedia        |
      | Accountant_Signmedia_CannotReadCompany         | CurrencyViewer                  |
      | Accountant_Signmedia_CannotReadCompany         | DocumentOwnerViewer             |

      | Accountant_Signmedia_CannotReadCurrency        | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadCurrency        | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadCurrency        | PurUnitReader_Signmedia         |
      | Accountant_Signmedia_CannotReadCurrency        | CustomerViewer_Signmedia        |
      | Accountant_Signmedia_CannotReadCurrency        | CompanyViewer                   |
      | Accountant_Signmedia_CannotReadCurrency        | DocumentOwnerViewer             |

      | Accountant_Signmedia_CannotReadDocumentOwner   | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadDocumentOwner   | NotesReceivableViewer_Signmedia |
      | Accountant_Signmedia_CannotReadDocumentOwner   | PurUnitReader_Signmedia         |
      | Accountant_Signmedia_CannotReadDocumentOwner   | CustomerViewer_Signmedia        |
      | Accountant_Signmedia_CannotReadDocumentOwner   | CompanyViewer                   |
      | Accountant_Signmedia_CannotReadDocumentOwner   | CurrencyViewer                  |

      | Accountant_Signmedia_CannotReadNotesReceivable | NotesReceivableOwner_Signmedia  |
      | Accountant_Signmedia_CannotReadNotesReceivable | PurUnitReader_Signmedia         |
      | Accountant_Signmedia_CannotReadNotesReceivable | CustomerViewer_Signmedia        |
      | Accountant_Signmedia_CannotReadNotesReceivable | CompanyViewer                   |
      | Accountant_Signmedia_CannotReadNotesReceivable | CurrencyViewer                  |
      | Accountant_Signmedia_CannotReadNotesReceivable | DocumentOwnerViewer             |

    And the following sub-roles and permissions exist:
      | Subrole                         | Permission              | Condition                      |
      | NotesReceivableOwner_Signmedia  | NotesReceivable:Create  |                                |
      | NotesReceivableViewer_Signmedia | NotesReceivable:ReadAll | [purchaseUnitName='Signmedia'] |
      | PurUnitReader_Signmedia         | PurchasingUnit:ReadAll  | [purchaseUnitName='Signmedia'] |
      | CustomerViewer_Signmedia        | Customer:ReadAll        | [purchaseUnitName='Signmedia'] |
      | CompanyViewer                   | Company:ReadAll         |                                |
      | CurrencyViewer                  | Currency:ReadAll        |                                |
      | DocumentOwnerViewer             | DocumentOwner:ReadAll   |                                |

    And the following users doesn't have the following permissions:
      | User                              | Permission              |
      | Shady.Abdelatif.NoPurchaseUnit    | PurchasingUnit:ReadAll  |
      | Shady.Abdelatif.NoCustomer        | Customer:ReadAll        |
      | Shady.Abdelatif.NoCompany         | Company:ReadAll         |
      | Shady.Abdelatif.NoCurrency        | Currency:ReadAll        |
      | Shady.Abdelatif.NoDocumentOwner   | DocumentOwner:ReadAll   |
      | Shady.Abdelatif.NoNotesReceivable | NotesReceivable:ReadAll |
      | Afaf                              | NotesReceivable:Create  |
      | Afaf                              | NotesReceivable:ReadAll |

    And the following PurchasingUnits exist:
      | Code | Name      |
      | 0001 | Flexo     |
      | 0002 | Signmedia |
    And the following Customers exist:
      | Code   | Name                  | BusinessUnit     |
      | 000007 | مطبعة أكتوبر الهندسية | 0002 - Signmedia |
      | 000002 | Client2               | 0001 - Flexo     |

    And the following DocumentOwners exist:
      | Id | Name         | BusinessObject  | Permission         | Condition |
      | 34 | Ashraf Salah | NotesReceivable | CanBeDocumentOwner |           |
    And the following Currencies exist:
      | Code | Name           | ISO |
      | 0001 | Egyptian Pound | EGP |
    And the following Companies exist:
      | Code | Name    |
      | 0002 | DigiPro |

  #EBS-5447, EBS-9160
  Scenario Outline: (01) Create NotesReceivables by an unauthorized user due to condition OR authorized user with unauthorized reads (Abuse Case)
    Given user is logged in as "<User>"
    When "<User>" creates NotesReceivable with the following values:
      | NoteForm | Type                           | BusinessUnit   | Company | BusinessPartner   | DocumentOwnerId | Amount | CurrencyISO |
      | CHEQUE   | NOTES_RECEIVABLE_AS_COLLECTION | <BusinessUnit> | 0002    | <BusinessPartner> | 34              | 7000   | EGP         |
    Then "<User>" is logged out
    And "<User>" is forwarded to the error page
    Examples:
      | User                              | BusinessUnit | BusinessPartner |
      | Afaf                              | 0002         | 000007          |
      | Shady.Abdelatif.NoPurchaseUnit    | 0002         | 000007          |
      | Shady.Abdelatif.NoCustomer        | 0002         | 000007          |
      | Shady.Abdelatif.NoCompany         | 0002         | 000007          |
      | Shady.Abdelatif.NoCurrency        | 0002         | 000007          |
      | Shady.Abdelatif.NoDocumentOwner   | 0002         | 000007          |
      | Shady.Abdelatif.NoNotesReceivable | 0002         | 000007          |
      | Shady.Abdelatif                   | 0002         | 000002          |
      | Shady.Abdelatif                   | 0001         | 000007          |

  #EBS-5447, EBS-9160
  Scenario Outline: (02) Create NotesReceivables by an authorized user with authorized reads
    Given user is logged in as "<User>"
    When "<User>" requests to create NotesReceivables
    Then the following are authorized reads "<AuthorizedReads>" returned to "<User>"
    Examples:
      | User                              | AuthorizedReads                                                                                       |
      | Shady.Abdelatif                   | ReadBusinessUnits, ReadCustomers, ReadCompany, ReadCurrency, ReadNotesReceivables, ReadDocumentOwners |
      | Shady.Abdelatif.NoPurchaseUnit    | ReadCustomers, ReadCompany, ReadCurrency, ReadNotesReceivables, ReadDocumentOwners                    |
      | Shady.Abdelatif.NoCustomer        | ReadBusinessUnits, ReadCompany, ReadCurrency, ReadNotesReceivables, ReadDocumentOwners                |
      | Shady.Abdelatif.NoCompany         | ReadBusinessUnits, ReadCustomers, ReadCurrency, ReadNotesReceivables, ReadDocumentOwners              |
      | Shady.Abdelatif.NoCurrency        | ReadBusinessUnits, ReadCustomers, ReadCompany, ReadNotesReceivables, ReadDocumentOwners               |
      | Shady.Abdelatif.NoDocumentOwner   | ReadBusinessUnits, ReadCustomers, ReadCompany, ReadCurrency, ReadNotesReceivables                     |
      | Shady.Abdelatif.NoNotesReceivable | ReadBusinessUnits, ReadCustomers, ReadCompany, ReadCurrency, ReadDocumentOwners                       |

  #EBS-5447, EBS-9160
  Scenario: (03) Create NotesReceivables by an unauthorized user
    Given user is logged in as "Afaf"
    When "Afaf" requests to create NotesReceivables
    Then "Afaf" is logged out
    And "Afaf" is forwarded to the error page