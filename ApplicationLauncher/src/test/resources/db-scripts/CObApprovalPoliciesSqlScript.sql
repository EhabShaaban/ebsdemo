INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate, 
            creationinfo, modificationinfo)
VALUES (1, '0001', 'IMPORT_PO', '[purchaseUnitName=''Signmedia'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate, 
            creationinfo, modificationinfo)
VALUES (2, '0002', 'LOCAL_PO', '[purchaseUnitName=''Flexo'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');
           
INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate, 
            creationinfo, modificationinfo)
VALUES (3, '0003', 'LOCAL_PO', '[purchaseUnitName=''Corrugated'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');
           
INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate, 
            creationinfo, modificationinfo)
VALUES (4, '0004', 'LOCAL_PO', '[purchaseUnitName=''Corrugated'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate,
            creationinfo, modificationinfo)
VALUES (5, '0005', 'LOCAL_PO', '[purchaseUnitName=''Signmedia'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobapprovalpolicy(
            id, code, documenttype, condition, creationdate, modifieddate,
            creationinfo, modificationinfo)
VALUES (6, '0006', 'IMPORT_PO', '[purchaseUnitName=''Flexo'']',
           '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

-- Approval Policy 0001 Control points           
INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate, 
            modifieddate, creationinfo, modificationinfo)
VALUES (1, 1, 1,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate, 
            modifieddate, creationinfo, modificationinfo)
VALUES (2, 1, 2,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate, 
            modifieddate, creationinfo, modificationinfo)
VALUES (3, 1, 3,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');
       
-- Approval Policy 0002 Control points           
INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate, 
            modifieddate, creationinfo, modificationinfo)
VALUES (4, 2, 4,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate, 
            modifieddate, creationinfo, modificationinfo)
VALUES (5, 2, 5,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

-- Approval Policy 0005 Control points

INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate,
            modifieddate, creationinfo, modificationinfo)
VALUES (9, 5, 2,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');

-- Approval Policy 0005 Control points

INSERT INTO iobapprovalpolicycontrolpoint(
            id, refinstanceid, controlpointid, creationdate,
            modifieddate, creationinfo, modificationinfo)
VALUES (10, 6, 4,
       '2019-04-30 09:07:25.496505', '2019-04-30 09:07:25.496505', 'hr1', 'hr1');