DELETE
from LObGlobalGLAccountConfig;
DELETE
from iobaccountingdocumentcompanydata;
DELETE
from IObAccountingDocumentAccountingDetails;
DELETE
from dobsalesinvoicejournalentry;
DELETE
from iobjournalentryitemcustomersubaccount;
DELETE
from iobjournalentryitemtaxsubaccount;
DELETE
from iobjournalentryitemsalessubaccount;
DELETE
from IObJournalEntryItemNotesReceivableSubAccount;
DELETE
from IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount;
DELETE
from dobcollectionjournalentry;
DELETE
from DObJournalEntry;
DELETE
from iobvendorinvoicedetailsdownpayment;



DELETE
from TObStockTransformationStoreTransaction;
DELETE
from iobstocktransformationdetails;
DELETE
from dobstocktransformation;
DELETE
from cobstocktransformationreason;
DELETE
from cobstocktransformationtype;


DELETE
from IObGoodsReceiptSalesReturnData;
DELETE
from DObGoodsReceiptSalesReturn;

------- Settlement Clear ---------

DELETE FROM IObSettlementDetails;
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'Settlement';
DELETE FROM dobsettlement;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'Settlement';

------- Costing Clear ---------

DELETE FROM IObCostingAccountingDetails;
DELETE FROM iobCostingDetails;
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'Costing';
DELETE FROM DObCosting;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'Costing';

------- SalesReturnOrder Clear ---------
DELETE
FROM iobsalesreturnordercompanydata;
DELETE
FROM iobsalesreturnordertax;
DELETE
FROM iobsalesreturnorderdetails;
DELETE
FROM iobsalesreturnorderitems;
DELETE
FROM dobsalesreturnorder;


delete
from DObCosting;

--------collection Clear ---------
DELETE
FROM iobcollectiondebitnotedetails;

DELETE
FROM IObAccountingDocumentCompanyData
    where objecttypecode='Collection';
delete from iobpaymentrequestcreditnotepaymentdetails;
delete
from dobaccountingdocument
where objecttypecode like 'C\_%';

--------ibu Clear ---------

DELETE
FROM IObAccountingDocumentCompanyData
where objecttypecode='InitialBalanceUpload';
delete
from dobaccountingdocument
where objecttypecode ='IBU';


/* Notes Receivables clear*/
DELETE
FROM iobaccountingdocumentaccountingdetails WHERE objecttypecode = 'NOTES_RECEIVABLE' ;
DELETE
FROM iobmonetarynotesdetails;
DELETE
FROM IObMonetaryNotesReferenceDocuments;
delete
from dobaccountingdocument
where objecttypecode LIKE '%_RECEIVABLE%';

--------DebitNote Clear---------
delete
from dobaccountingdocument
where objecttypecode like '%DEBIT_NOTE%' ;

/* store transactions clear*/
delete
from tobgoodsissuestoretransaction;
delete
from tobgoodsreceiptstoretransaction;
delete
from tobstoretransaction;

-------- GoodsIssue clear ---------
DELETE
FROM dobgoodsissuesalesinvoice;
DELETE
FROM dobgoodsissuesalesorder;
DELETE
FROM IObGoodsIssueSalesInvoiceData;
DELETE
FROM IObGoodsIssueSalesOrderData;
DELETE
FROM IObGoodsIssueSalesInvoiceItem;
DELETE
FROM IObGoodsIssueSalesOrderItem;
-------- lobreason clear ---------
DELETE
FROM lobreason;

------- SalesInvoice Clear ---------
DELETE
FROM iobsalesinvoicepostingdetails;
DELETE
FROM iobsalesinvoicetaxes;
DELETE
FROM iobsalesinvoicebusinesspartner;
DELETE
FROM IObAccountingDocumentCompanyData
WHERE objecttypecode = 'SalesInvoice';
DELETE
FROM dobsalesinvoice;
DELETE
FROM cobsalesinvoice;
DELETE
FROM iobsalesinvoiceitem;
DELETE
FROM dobaccountingdocument
where objecttypecode = 'SalesInvoice';

------- SalesOrder Clear ---------
DELETE
FROM iobsalesorderdata;
DELETE
FROM dobsalesorder;

------- LandedCost Clear ---------
DELETE
FROM IObLandedCostDetails;
DELETE
FROM IObAccountingDocumentCompanyData
WHERE objecttypecode = 'LandedCost';
DELETE
FROM IObAccountingDocumentActivationDetails;
DELETE
FROM DObLandedCost;
DELETE
FROM dobaccountingdocument
where objecttypecode = 'LandedCost';

/*Payment Request clear*/
DELETE
from iobpaymentrequestInvoicepaymentdetails;
DELETE
from IObPaymentRequestPurchaseOrderPaymentDetails;
DELETE
from IObPaymentRequestCreditNotePaymentDetails;
DELETE
from iobpaymentrequestpaymentdetails;
DELETE
FROM IObAccountingDocumentCompanyData
WHERE objecttypecode = 'PaymentRequest';
DELETE
from dobpaymentrequest;
DELETE
FROM dobaccountingdocument
where objecttypecode = 'PaymentRequest';
/*exchange rate clear*/
DELETE FROM cobexchangerate;

/**vendorinvoice clear*/
DELETE
FROM iobvendorinvoicepurchaseorder;
DELETE
FROM IObAccountingDocumentCompanyData
WHERE objecttypecode = 'VendorInvoice';
DELETE
FROM IObVendorInvoiceDetails;
DELETE
FROM dobvendorInvoice;
DELETE
FROM dobaccountingdocument
where objecttypecode = 'VendorInvoice';


/**Inventory Clear */
DELETE
FROM IObInventoryCompany;
DELETE
FROM IObInventoryActivationDetails;
delete
from dobinventorydocument;


/**Customer Clear */
DELETE
FROM iobcustomerbusinessunit;
DELETE
FROM mobcustomer;


/*Payment Request clear*/
DELETE
from dobpaymentrequest;


/*goods receipt clear*/
DELETE
from iobgoodsreceiptitembatches;
DELETE
from iobgoodsreceiptpurchaseorderitemquantities;
DELETE
from iobgoodsreceiptsalesreturnitemquantities;
DELETE
from IObGoodsReceiptPurchaseOrderReceivedItemsData;
DELETE
from IObGoodsReceiptSalesReturnReceivedItemsData;
DELETE
from iobgoodsreceiptpurchaseorderdata;
DELETE
from tobgoodsreceiptstoretransaction;
DELETE
from DObGoodsReceiptPurchaseOrder;
DELETE
from DObGoodsReceiptSalesReturn;

DELETE
from CObGoodsReceipt;
DELETE
from EntityUserCode
WHERE type = 'DObGoodsReceipt';

/*purchase order clear*/
DELETE
FROM ioborderlinedetailsquantities;
DELETE
FROM ioborderlinedetails;
DELETE
FROM iobordercycledates;
DELETE
FROM ioborderpaymenttermsdetails;
DELETE
FROM ioborderdocumentrequireddocuments;
DELETE
FROM iobenterprisedata;
DELETE
FROM iobordercompany;
DELETE
FROM ioborderdeliverydetails;
delete
from IObCostingAccountingDetails;
delete
from DObCosting;
DELETE
FROM iobpurchaseorderfulfillervendor;
DELETE
FROM dobpurchaseorder;
DELETE
FROM doborderdocument;


/*material data clear*/
DELETE
FROM IObVendorAccountingInfo;

DELETE
FROM iobmaterialrestriction;
DELETE
FROM cobmaterialstatus;
DELETE
FROM iobmaterialallowedview;
DELETE
FROM cobmaterialtype;
DELETE
FROM cobmaterial;

DELETE
FROM itemvendorrecord;

/*stock availability clear*/
DELETE from cobstockavailability;

DELETE
FROM cobmaterialgroup;

DELETE
FROM cobmaterial;
DELETE
FROM lobtaxinfo;
DELETE
FROM lobcompanytaxes;
DELETE
FROM lobvendortaxes;

/*Master data clear*/

DELETE
FROM iobalternativeuom;
DELETE
FROM iobitempurchaseunit;
DELETE
FROM iobvendorpurchaseunit;
DELETE
FROM itemvendorrecord;
DELETE
FROM mobvendor;
DELETE
FROM ioborderitem;
DELETE
FROM
 mobitem ;

--------CreditNote Clear---------
delete
from dobaccountingdocument
where objecttypecode like '%CREDIT_NOTE%' ;

DELETE
from IObCustomerAccountingInfo;
DELETE
from mobbusinesspartner;

/*shipping instructions clear*/
DELETE
FROM iobmaterialnature;
DELETE
FROM iobmodeoftransport;
DELETE
FROM cobshippinginstructions;
DELETE
FROM cobshipping;


/*Measures Data clear*/
DELETE
FROM CObMeasure;
DELETE
FROM public.iobuomconversion;


/*geolocale clear*/
DELETE
FROM countrydefinition;
DELETE
FROM subdivisiondefinition;
DELETE
FROM cobgeolocale;


DELETE
FROM portscountry;


/*payment terms clear*/
DELETE
FROM iobpaymenttermsinstallments;


/*approval policies clear*/
DELETE
FROM iobapprovalpolicycontrolpoint;
DELETE
FROM cobapprovalpolicy;
DELETE
FROM objectconditionfields;


/*control points clear*/
DELETE
FROM iobcontrolpointusers;
DELETE
FROM cobcontrolpoint;


/*simple material clear*/
DELETE
FROM lobsimplematerialdata
WHERE sysflag = FALSE;

/*sales invoice sequence*/
ALTER SEQUENCE iobsalesinvoicepostingdetails_id_seq RESTART WITH 1;

/*goods receipt sequence*/
ALTER SEQUENCE iobgoodsreceiptpurchaseorderreceiveditemsdata_id_seq RESTART WITH 10;
ALTER SEQUENCE iobgoodsreceiptpurchaseorderitemquantities_id_seq RESTART WITH 9;
ALTER SEQUENCE iobgoodsreceiptitembatches_id_seq RESTART WITH 5;
ALTER SEQUENCE DObGoodsReceiptPurchaseOrder_id_seq RESTART WITH 23;
ALTER SEQUENCE iobgoodsreceiptpurchaseorderdata_id_seq RESTART WITH 23;


/*Master data sequence*/
ALTER SEQUENCE IObItemAccountingInfo_id_seq RESTART WITH 8;
ALTER SEQUENCE IObItemPurchaseUnit_id_seq RESTART WITH 61;
ALTER SEQUENCE IObVendorPurchaseUnit_id_seq RESTART WITH 33;
ALTER SEQUENCE iobalternativeuom_id_seq RESTART WITH 33;
ALTER SEQUENCE masterdata_id_seq RESTART WITH 197;


/*Shipping instruction sequence*/
ALTER SEQUENCE cobshipping_id_seq RESTART WITH 2;
ALTER SEQUENCE cobshippinginstructions_id_seq RESTART WITH 2;
ALTER SEQUENCE iobmaterialnature_id_seq RESTART WITH 2;
ALTER SEQUENCE iobmodeoftransport_id_seq RESTART WITH 2;


/*Material Data sequence*/
ALTER SEQUENCE cobmaterial_id_seq RESTART WITH 44;
ALTER SEQUENCE iobmaterialrestriction_id_seq RESTART WITH 3;
ALTER SEQUENCE iobmaterialallowedview_id_seq RESTART WITH 5;
ALTER SEQUENCE cobmaterial_id_seq RESTART WITH 5;

/*Measures Data sequence*/
ALTER SEQUENCE cobmeasure_id_seq RESTART WITH 50;
ALTER SEQUENCE iobuomconversion_id_seq RESTART WITH 11;

/*geolocale sequence*/
ALTER SEQUENCE cobgeolocale_id_seq RESTART WITH 32;


/*payment term sequence*/
ALTER SEQUENCE cobpaymentterms_id_seq RESTART WITH 7;
ALTER SEQUENCE iobpaymenttermsinstallments_id_seq RESTART WITH 3;

/*currency sequence*/
ALTER SEQUENCE cobcurrency_id_seq RESTART WITH 14;


/*calendar sequence*/

/*approval policies sequence*/
ALTER SEQUENCE iobapprovalpolicycontrolpoint_id_seq RESTART WITH 1000;
ALTER SEQUENCE cobapprovalpolicy_id_seq RESTART WITH 1000;


/*control points sequence*/
ALTER SEQUENCE cobcontrolpoint_id_seq RESTART WITH 17;
ALTER SEQUENCE iobcontrolpointusers_id_seq RESTART WITH 23;

/*simple material sequence*/
ALTER SEQUENCE lobsimplematerialdata_id_seq RESTART WITH 14;

--------JournalBalance Clear ---------
DELETE FROM cobjournalbalance;

--------AccountingDocument Clear ---------
DELETE
FROM IObAccountingDocumentAccountingDetails;
DELETE from cobfiscalperiod;
