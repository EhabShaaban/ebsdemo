-- shipping data

INSERT INTO cobshipping (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (1, '0001', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '2', '["Active"]', '{"en":"Ship by Shecartoon","ar":"اللف بالشيكارتون"}');
INSERT INTO cobshipping (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (2, '0002', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '2', '["Active"]', '{"en":"Refregirated","ar":"حاوية مبردة"}');

INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (1, '{"ar":"ءءءءءءءءءءءءءءءءءءء",
"en":"Ship by Shecartoon Instructions Text.Ship by Shecartoon Instructions Text.Ship by Shecartoon Instructions Text.Ship by Shecartoon Instructions Text.Ship by Shecartoon Instructions Text."}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (2, '{"ar":"ءءءءءءءءءءءءءءءءءءء",
"en":"To pack your product effectively for export, you must provide complete, appropriate and accurate package markings. There are several considerations that govern your shipment marking.First, you must make sure that whatever markings you put on the outside of a carton can easily be read by anyone, anywhere in the world."}');

-- mode of transport
INSERT INTO iobmodeoftransport (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, modeoftransportid) VALUES (1, '2018-04-24 06:31:59.547774', '2018-04-24 06:31:59.547774', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 1, 9);

-- material nature
INSERT INTO iobmaterialnature (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, materialnatureid) VALUES (1, '2018-04-24 06:31:59.544904', '2018-04-24 06:31:59.544904', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 1, 1);
