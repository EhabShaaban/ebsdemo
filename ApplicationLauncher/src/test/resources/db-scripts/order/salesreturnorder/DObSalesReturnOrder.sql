DELETE
FROM doborderdocument
WHERE objecttypecode = 'SALES_RETURN'
  AND id in (6, 8, 9, 11, 12, 13, 14, 15, 16, 17);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (6,  '2020000051', '2020-06-25 07:48:37.000000 +00:00', '2020-06-25 07:48:38.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (8,  '2020000052', '2020-06-29 07:20:16.000000 +00:00', '2020-06-29 07:20:16.388000 +00:00', 'hr1', 'hr1', '["Draft"]', 'SALES_RETURN', 1000072);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (9,  '2020000053', '2020-06-29 09:21:01.000000 +00:00', '2020-06-29 09:21:01.000000 +00:00', 'hr1', 'hr1', '["Shipped"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (11, '2020000054', '2020-07-05 10:37:28.000000 +00:00', '2020-07-05 10:37:28.026000 +00:00', 'hr1', 'hr1', '["Draft"]', 'SALES_RETURN', 1000093);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (12, '2020000055', '2020-07-09 09:50:06.000000 +00:00', '2020-07-09 09:50:06.000000 +00:00', 'hr1', 'hr1', '["Draft"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (13, '2020000056', '2020-07-28 08:20:04.072000 +00:00', '2020-07-28 08:39:40.215000 +00:00', 'hr1', 'hr1', '["GoodsReceiptActivated"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (14, '2020000057', '2020-07-28 08:40:58.806000 +00:00', '2020-07-28 08:42:29.960000 +00:00', 'hr1', 'hr1', '["Shipped"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (15, '2020000058', '2020-08-09 07:38:39.608000 +00:00', '2020-08-09 07:39:32.116000 +00:00', 'hr1', 'hr1', '["Shipped"]', 'SALES_RETURN', 1000093);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (16, '2020000059', '2020-08-11 08:40:44.951000 +00:00', '2020-08-11 08:41:06.704000 +00:00', 'hr1', 'hr1', '["Shipped"]', 'SALES_RETURN', 1000072);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (17, '2020000060', '2020-08-30 07:59:33.038000 +00:00', '2020-08-30 07:02:05.758000 +00:00', 'hr1', 'hr1', '["Shipped"]', 'SALES_RETURN', 1000072);

INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (6, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (8, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (9, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (11, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (12, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (13, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (14, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (15, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (16, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (17, 1);