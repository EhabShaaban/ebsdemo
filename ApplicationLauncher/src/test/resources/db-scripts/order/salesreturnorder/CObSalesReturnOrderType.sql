DELETE FROM cobsalesreturnordertype;

INSERT INTO public.cobsalesreturnordertype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1, 'RETURN_SALES_ORDER_FOR_ITEMS', '2020-06-15 13:47:49.000000', '2020-06-15 13:47:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en": "Sales return for items", "ar": "طلب ارتجاع للأصناف"}');