DELETE FROM IObSalesReturnOrderTax;

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (1, 1, '2020-06-15 14:29:49.000000', '2020-06-15 14:29:52.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (2, 1, '2020-06-15 14:31:02.000000', '2020-06-15 14:31:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (3, 1, '2020-06-15 14:31:14.000000', '2020-06-15 14:31:16.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (4, 2, '2020-06-15 14:31:30.000000', '2020-06-15 14:31:32.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (5, 2, '2020-06-15 14:31:44.000000', '2020-06-15 14:31:46.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (6, 2, '2020-06-15 14:31:57.000000', '2020-06-15 14:31:59.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (7, 3, '2020-06-15 14:32:16.000000', '2020-06-15 14:32:20.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (8, 3, '2020-06-15 14:32:41.000000', '2020-06-15 14:32:43.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (9, 3, '2020-06-15 14:32:52.000000', '2020-06-15 14:32:57.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (10, 3, '2020-06-15 14:33:08.000000', '2020-06-15 14:33:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (11, 3, '2020-06-15 14:33:22.000000', '2020-06-15 14:33:25.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (12, 3, '2020-06-15 14:33:36.000000', '2020-06-15 14:33:40.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (13, 4, '2020-06-15 14:33:52.000000', '2020-06-15 14:33:54.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (14, 4, '2020-06-15 14:34:10.000000', '2020-06-15 14:34:13.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (15, 4, '2020-06-15 14:34:23.000000', '2020-06-15 14:34:26.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (16, 4, '2020-06-15 14:34:35.000000', '2020-06-15 14:34:37.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (17, 4, '2020-06-15 14:34:47.000000', '2020-06-15 14:34:49.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (18, 4, '2020-06-15 14:34:57.000000', '2020-06-15 14:35:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (19, 5, '2020-06-15 14:35:14.000000', '2020-06-15 14:35:16.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (20, 5, '2020-06-15 14:35:25.000000', '2020-06-15 14:35:29.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (21, 5, '2020-06-15 14:35:37.000000', '2020-06-15 14:35:39.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (22, 5, '2020-06-15 14:35:49.000000', '2020-06-15 14:35:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (23, 5, '2020-06-15 14:36:00.000000', '2020-06-15 14:36:03.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (24, 5, '2020-06-15 14:36:13.000000', '2020-06-15 14:36:15.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);