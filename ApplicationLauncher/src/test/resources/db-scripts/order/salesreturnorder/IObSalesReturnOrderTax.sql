DELETE FROM IObSalesReturnOrderTax WHERE refinstanceid in(6,9,11,12,13,14,15,16,17);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (25, 6, '2020-06-25 09:50:30.000000', '2020-06-25 09:50:31.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (26, 6, '2020-06-25 09:50:37.000000', '2020-06-25 09:50:39.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (27, 6, '2020-06-25 09:50:45.000000', '2020-06-25 09:50:46.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (29, 9, '2020-06-29 11:21:01.153000', '2020-06-29 11:21:01.153000', 'hr1', 'hr1', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (30, 9, '2020-06-29 11:21:04.037000', '2020-06-29 11:21:04.037000', 'hr1', 'hr1', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (31, 9, '2020-06-29 11:21:04.038000', '2020-06-29 11:21:04.038000', 'hr1', 'hr1', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (32, 9, '2020-06-29 11:21:04.039000', '2020-06-29 11:21:04.039000', 'hr1', 'hr1', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (33, 9, '2020-06-29 11:21:04.041000', '2020-06-29 11:21:04.041000', 'hr1', 'hr1', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (34, 9, '2020-06-29 11:21:04.043000', '2020-06-29 11:21:04.043000', 'hr1', 'hr1', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (36, 11, '2020-07-05 12:37:28.069000', '2020-07-05 12:37:28.069000', 'hr1', 'hr1', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (37, 11, '2020-07-05 12:37:28.081000', '2020-07-05 12:37:28.081000', 'hr1', 'hr1', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (38, 11, '2020-07-05 12:37:28.083000', '2020-07-05 12:37:28.083000', 'hr1', 'hr1', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (39, 11, '2020-07-05 12:37:28.085000', '2020-07-05 12:37:28.085000', 'hr1', 'hr1', '0010',  '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (40, 11, '2020-07-05 12:37:28.093000', '2020-07-05 12:37:28.093000', 'hr1', 'hr1', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (41, 11, '2020-07-05 12:37:28.101000', '2020-07-05 12:37:28.101000', 'hr1', 'hr1', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (42, 12, '2020-07-09 11:50:06.499000', '2020-07-09 11:50:06.499000', 'hr1', 'hr1', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (43, 12, '2020-07-09 11:50:06.508000', '2020-07-09 11:50:06.508000', 'hr1', 'hr1', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (44, 12, '2020-07-09 11:50:06.509000', '2020-07-09 11:50:06.509000', 'hr1', 'hr1', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (45, 13, '2020-07-28 10:20:04.110000', '2020-07-28 10:20:04.110000', 'hr1', 'hr1', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (46, 14, '2020-07-28 10:40:58.911000', '2020-07-28 10:40:58.911000', 'hr1', 'hr1', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (47, 14, '2020-07-28 10:40:58.924000', '2020-07-28 10:40:58.924000', 'hr1', 'hr1', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (48, 14, '2020-07-28 10:40:58.926000', '2020-07-28 10:40:58.926000', 'hr1', 'hr1', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (49, 14, '2020-07-28 10:40:58.928000', '2020-07-28 10:40:58.928000', 'hr1', 'hr1', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (50, 14, '2020-07-28 10:40:58.929000', '2020-07-28 10:40:58.929000', 'hr1', 'hr1', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (51, 14, '2020-07-28 10:40:58.931000', '2020-07-28 10:40:58.931000', 'hr1', 'hr1', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (52, 15, '2020-08-09 09:38:42.528000', '2020-08-09 09:38:42.528000', 'hr1', 'hr1', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (53, 15, '2020-08-09 09:38:42.540000', '2020-08-09 09:38:42.540000', 'hr1', 'hr1', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (54, 15, '2020-08-09 09:38:42.542000', '2020-08-09 09:38:42.542000', 'hr1', 'hr1', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (55, 15, '2020-08-09 09:38:42.543000', '2020-08-09 09:38:42.543000', 'hr1', 'hr1', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (56, 15, '2020-08-09 09:38:42.544000', '2020-08-09 09:38:42.544000', 'hr1', 'hr1', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (57, 15, '2020-08-09 09:38:42.545000', '2020-08-09 09:38:42.545000', 'hr1', 'hr1', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (58, 16, '2020-08-11 10:40:44.969000', '2020-08-11 10:40:44.969000', 'hr1', 'hr1', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (59, 16, '2020-08-11 10:40:44.978000', '2020-08-11 10:40:44.978000', 'hr1', 'hr1', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.IObSalesReturnOrderTax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (60, 16, '2020-08-11 10:40:44.980000', '2020-08-11 10:40:44.980000', 'hr1', 'hr1', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (61, 17, '2020-08-30 09:59:35.967000', '2020-08-30 09:59:35.967000', 'hr1', 'hr1', '0007', '{"ar":"ضريبة القيمة المضافة","en":"Vat"}', 1);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (62, 17, '2020-08-30 09:59:35.975000', '2020-08-30 09:59:35.975000', 'hr1', 'hr1', '0008', '{"ar":"ضريبة أرباح تجارية وصناعية","en":"Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (63, 17, '2020-08-30 09:59:35.976000', '2020-08-30 09:59:35.976000', 'hr1', 'hr1', '0009', '{"ar":"ضرائب عقارية","en":"Real estate taxes"}', 10);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (64, 17, '2020-08-30 09:59:35.977000', '2020-08-30 09:59:35.977000', 'hr1', 'hr1', '0010', '{"ar":"ضريبة خدمة","en":"Service tax"}', 12);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (65, 17, '2020-08-30 09:59:35.978000', '2020-08-30 09:59:35.978000', 'hr1', 'hr1', '0011', '{"ar":"ضريبة الاضافة","en":"Adding tax"}', 0.5);

INSERT INTO public.iobsalesreturnordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (66, 17, '2020-08-30 09:59:35.980000', '2020-08-30 09:59:35.980000', 'hr1', 'hr1', '0012', '{"ar":"ضريبة مبيعات","en":"Sales tax"}', 12);
