DELETE FROM iobsalesreturnorderitems WHERE refinstanceid IN (8,9,11,12,13,14,15,16,17);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (7, 8, '2020-06-29 09:20:00.000000', '2020-08-10 11:36:00.000000', 'hr1', 'hr1', 57, 41, 25, 952.5, 6);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (8, 9, '2020-06-29 11:21:01.000000', '2020-06-29 11:21:01.000000', 'hr1', 'hr1', 57, 41, 1, 10, 1);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (10, 11, '2020-07-05 12:37:28.000000', '2020-07-05 12:37:28.000000', 'hr1', 'hr1', 38, 4, 1, 6350, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (11, 12, '2020-07-09 11:50:06.483000', '2020-07-09 11:50:06.483000', 'hr1', 'hr1', 38, 4, 1, 63436.5, null);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (12, 12, '2020-07-09 11:50:06.494000', '2020-07-09 11:50:06.494000', 'hr1', 'hr1', 40, 4, 1, 999, null);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (13, 12, '2020-07-09 11:50:06.495000', '2020-07-09 11:50:06.495000', 'hr1', 'hr1', 38, 41, 1, 63436.5, null);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (14, 12, '2020-07-09 11:50:06.497000', '2020-07-09 11:50:06.497000', 'hr1', 'hr1', 33, 4, 1, 10000, null);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (15, 13, '2020-07-28 10:20:04.119000', '2020-07-28 10:35:57.523000', 'hr1', 'hr1', 57, 41, 1, 10, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (16, 14, '2020-07-28 10:40:58.834000', '2020-07-28 10:42:21.736000', 'hr1', 'hr1', 190, 23, 2, 1, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (17, 14, '2020-07-28 10:40:58.901000', '2020-07-28 10:42:14.650000', 'hr1', 'hr1', 75, 23, 1, 1, 8);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (18, 14, '2020-07-28 10:40:58.903000', '2020-07-28 10:42:08.325000', 'hr1', 'hr1', 57, 41, 99, 2, 5);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (19, 14, '2020-07-28 10:40:58.906000', '2020-07-28 10:42:01.200000', 'hr1', 'hr1', 64, 16, 2, 2, 6);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (20, 15, '2020-08-09 09:38:42.511000', '2020-08-09 09:39:27.666000', 'hr1', 'hr1', 64, 16, 1, 40, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (21, 15, '2020-08-09 09:38:42.524000', '2020-08-09 09:39:17.771000', 'hr1', 'hr1', 64, 46, 1, 30, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (22, 16, '2020-08-11 10:40:44.957000', '2020-08-11 10:41:00.905000', 'hr1', 'hr1', 57, 23, 2, 200, 3);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (24, 17, '2020-08-30 09:59:35.965000', '2020-08-30 09:59:57.335000', 'hr1', 'hr1', 71, 23, 50, 95, 5);

INSERT INTO public.iobsalesreturnorderitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, returnquantity, salesprice, returnreasonid)
VALUES (23, 17, '2020-08-30 09:59:35.952000', '2020-08-30 10:01:53.747000', 'hr1', 'hr1', 1, 37, 1, 65, 4);
