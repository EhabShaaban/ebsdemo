DELETE
FROM iobgoodsreceiptsalesreturndata;
DELETE
FROM doborderdocument
WHERE objecttypecode = 'SALES_RETURN';

DELETE
from EntityUserCode
WHERE type = 'DObSalesReturnOrder';
INSERT INTO EntityUserCode (type, code)
values ('DObSalesReturnOrder', '2020000050');

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (1, '2020000001', '2020-06-15 11:51:35.000000 +00:00', '2020-06-15 11:51:37.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'SALES_RETURN', 1000077);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (2, '2020000002', '2020-06-15 12:05:58.000000 +00:00', '2020-06-15 12:06:00.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Shipped"]', 'SALES_RETURN', 1000093);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (3, '2020000003', '2020-06-15 12:11:18.000000 +00:00', '2020-06-15 12:11:20.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["GoodsReceiptActivated"]', 'SALES_RETURN', 1000072);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (4, '2020000004', '2020-06-15 12:22:48.000000 +00:00', '2020-06-15 12:22:51.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["CreditNoteActivated"]', 'SALES_RETURN', 1000093);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (5, '2020000005', '2020-06-15 12:27:22.000000 +00:00', '2020-06-15 12:27:24.000000 +00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Completed"]', 'SALES_RETURN', 1000072);


INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (1, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (2, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (3, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (4, 1);
INSERT INTO public.dobsalesreturnorder (id, salesreturnordertypeid) VALUES (5, 1);