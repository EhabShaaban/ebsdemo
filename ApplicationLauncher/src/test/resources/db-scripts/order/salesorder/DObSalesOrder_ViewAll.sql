DELETE FROM doborderdocument where id between 51 AND 63;

ALTER SEQUENCE dobsalesorder_id_seq RESTART WITH 36;

DELETE from EntityUserCode WHERE type = 'DObSalesOrder';
INSERT INTO EntityUserCode (type, code) values ('DObSalesOrder', '2020000050');
--------------------------------------------------------------------------------------------

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (51, '2020000001','2020-02-01 07:52:00.000000', '2020-02-01 07:52:49.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (51, 1, 1, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (52, '2020000002','2020-02-04 08:37:05.000000', '2020-02-04 08:37:07.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["WaitingApproval"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (52,1, 1, 40);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (53, '2020000003', '2020-02-04 08:39:00.000000', '2020-02-04 08:39:56.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Approved"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (53, 1, 1, 41, 22300.0, 22000.0);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (54, '2020000004','2020-02-04 08:42:40.000000', '2020-02-04 08:42:42.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Rejected"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (54, 1, 1, 40);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (55, '2020000050','2020-02-04 08:44:27.000000', '2020-02-04 08:44:28.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (55, 1, 1, 39);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (56, '2020000005','2020-02-04 09:05:00.000000', '2020-02-04 09:06:01.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (56, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (57, '2020000006','2020-02-04 12:38:35.000000', '2020-02-04 12:38:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Canceled"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (57, 1, 1, 40);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (58, '2020000007', '2020-02-04 12:39:54.000000', '2020-02-04 12:39:55.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Expired"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (58, 1, 1, 40);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (59, '2020000008','2020-02-04 12:40:00.000000', '2020-02-04 12:40:25.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','["DeliveryComplete"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (59, 1, 1, 40, 22300.0, 22300.0);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (60, '2020000009', '2020-02-11 13:28:00.000000', '2020-02-11 13:28:45.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (60, 1, 19, 39);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (61, '2020000010', '2020-08-03 13:31:57.000000', '2020-08-03 13:35:24.000000', 'hr1','hr1','["SalesInvoiceActivated"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (61, 1, 19, 38, 340.0, 340.0);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (62, '2020000011','2020-08-03 13:36:14.000000', '2020-08-03 13:36:59.000000',  'hr1','hr1','["GoodsIssueActivated"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (62, 1, 19, 41, 448.800, 448.800);



INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (63, '2020000012', '2020-08-03 14:22:28.000000', '2020-08-03 14:23:12.000000','hr1','hr1','["ReadyForDelivery"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (63, 1, 19, 38, 544.0, 544.0);