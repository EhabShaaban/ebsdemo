DELETE FROM iobsalesorderdata
WHERE refinstanceid between 51 AND 63;
-- ALTER SEQUENCE iobsalesorderdata_id_seq RESTART WITH 35;

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (51, 51, '2020-02-01 08:29:13.000000', '2020-02-01 08:29:15.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 3, null, 6, 1, '2020-02-05 08:29:00.000000', 'this is notes for sales order 000001');

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (52, 52, '2020-02-04 08:39:06.000000', '2020-02-04 08:39:07.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 3, 4, 3, 1, '2020-03-10 08:39:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (53, 53, '2020-02-04 08:41:33.000000', '2020-02-04 08:41:34.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 4, 4, 6, 1, '2020-04-05 08:42:00.000000', 'this is notes for sales order 000003');

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (54, 54, '2020-02-04 08:43:41.000000', '2020-02-04 08:43:42.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 4, 4, 6, 1, '2020-03-10 08:43:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (55, 55, '2020-02-04 08:45:00.000000', '2020-02-04 08:45:01.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, null, null, null, null, null, null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (56, 56, '2020-02-04 09:26:57.000000', '2020-02-04 09:26:58.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 73, 5, 5, 3, 1, '2020-05-30 09:28:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (57,57, '2020-02-04 12:42:28.000000', '2020-02-04 12:42:30.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 4, 4, 6, 1, '2020-06-01 12:43:00.000000', 'this is notes for sales order 000006');

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (58, 58, '2020-02-04 12:43:50.000000', '2020-02-04 12:43:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 4, 4, 3, 1, '2020-07-01 12:44:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (59, 59, '2020-02-04 12:44:31.000000', '2020-02-04 12:44:33.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 3, 4, 5, 1, '2020-08-01 12:45:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (60, 60, '2020-02-11 13:31:14.000000', '2020-02-11 13:31:15.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 3, 1, '2020-04-07 10:21:00.000000', 'this is notes for sales order 000009');

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (61, 61, '2020-08-03 13:31:57.023000', '2020-08-03 13:34:08.494000', 'hr1', 'hr1', 73, 6, 5, 5, 1, '2020-08-31 00:00:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (62, 62, '2020-08-03 13:36:14.713000', '2020-08-03 13:36:39.809000', 'hr1', 'hr1', 73, 5, 5, 6, 1, '2020-08-25 00:00:00.000000', null);

INSERT INTO public.iobsalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, customeraddressid, contactpersonid, paymenttermid, currencyid, expecteddeliverydate, notes)
VALUES (63, 63, '2020-08-03 14:22:28.788000', '2020-08-03 14:22:50.840000', 'hr1', 'hr1', 73, 5, 5, 3, 1, '2020-08-29 00:00:00.000000', null);