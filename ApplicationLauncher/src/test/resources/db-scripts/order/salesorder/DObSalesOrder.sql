DELETE FROM doborderdocument WHERE id between 65 AND 85;
--------------------------------------------------------------------------------------------
INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (65, '2020000051','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Rejected"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (65,  1, 19, 41);

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (66, '2020000052','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["WaitingApproval"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,  salesordertypeid, businessunitid, salesresponsibleid)
VALUES (66,1, 19, 41);

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (67, '2020000053','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Approved"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (67, 1, 19, 41, 27.200, 27.200);

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (68, '2020000054','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Canceled"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (68, 1, 19, 41);

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (69, '2020000055','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Expired"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (69,  1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (70, '2020000056','2020-02-16 14:09:54.000000', '2020-02-16 14:09:58.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["DeliveryComplete"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (70, 1, 19, 41, 14858.0, 14858.0);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (71, '2020000057','2020-03-19 07:46:31.000000', '2020-03-19 07:46:32.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (71, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (72, '2020000058', '2020-03-19 07:47:03.000000', '2020-03-19 07:47:04.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (72, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (73, '2020000059', '2020-03-19 07:47:15.000000', '2020-03-19 07:47:16.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (73, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (74, '2020000060','2020-03-19 08:13:13.000000', '2020-03-19 08:13:14.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,  salesordertypeid, businessunitid, salesresponsibleid)
VALUES (74,  1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (75, '2020000061','2020-03-22 09:07:05.000000', '2020-03-22 09:07:07.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (75, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (76, '2020000062','2020-03-22 09:24:23.000000', '2020-03-22 09:24:24.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (76, 1, 1, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (77, '2020000063','2020-06-09 09:53:26.000000', '2020-06-09 09:53:27.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (77, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (78, '2020000064', '2020-06-09 09:53:51.000000', '2020-06-09 09:53:52.000000',  'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid)
VALUES (78, 1, 19, 41);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (79, '2020000065','2020-06-09 09:54:04.000000', '2020-06-09 09:54:06.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (79, 1, 19, 39);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (80, '2020000066','2020-06-09 11:45:38.000000', '2020-06-09 11:45:39.000000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Draft"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid)
VALUES (80,1, 1, 39);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (81, '2020000067','2020-06-30 13:57:45.654000', '2020-06-30 14:00:20.998000', 'Admin from BDKCompanyCode','Admin from BDKCompanyCode','["Approved"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (81, 1, 19, 41, 278.800, 278.800);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (82, '2020000068','2020-08-06 09:00:00', '2020-08-06 09:01:00', 'hr1', 'hr1', '["SalesInvoiceActivated"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (82, 1, 19, 41, 95.200, 95.200);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (83, '2020000069', '2020-08-17 12:37:00', '2020-08-17 12:39:00', 'hr1', 'hr1', '["Approved"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (83, 1, 19, 41, 197.200, 0);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (84, '2020000070','2020-08-30 09:55:00', '2020-08-30 09:58:00', 'hr1', 'hr1','["SalesInvoiceActivated"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id,salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (84, 1, 19, 41, 10880.0, 10880.0);


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (85, '2020000071', '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed','["GoodsIssueActivated"]', 1, 'SALES_ORDER');
INSERT INTO public.dobsalesorder (id, salesordertypeid, businessunitid, salesresponsibleid, totalamount, remaining)
VALUES (85, 1, 19, 41, 10.0, 0.0);