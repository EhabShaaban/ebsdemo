DELETE
FROM iobsalesordertax
WHERE id BETWEEN 55 AND 153;
--------------------------------------------------------------------------------------------

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (55, 65, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);


INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (61, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (62, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (63, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (64, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (65, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (66, 66, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (67, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (68, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (69, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (70, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (71, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (72, 67, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (73, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (74, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (75, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (76, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (77, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (78, 68, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (79, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (80, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (81, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (82, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (83, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (84, 69, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (85, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (86, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (87, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (88, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (89, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (90, 70, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (91, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (92, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (93, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (94, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (95, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (96, 73, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (97, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (98, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (99, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (100, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (101, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (102, 74, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (103, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (104, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (105, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10.123456789);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (106, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (107, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (108, 75, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (109, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (110, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (111, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (112, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (113, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (114, 76, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (115, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (116, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (117, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (118, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (119, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (120, 78, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (121, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (122, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (123, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (124, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (125, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (126, 79, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (127, 80, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0001', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (128, 80, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0002', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (129, 80, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0003', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (130, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0007', '{
    "ar": "ضريبة القيمة المضافة",
    "en": "Vat"
  }', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (131, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0008', '{
    "ar": "ضريبة أرباح تجارية وصناعية",
    "en": "Commercial and industrial profits tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (132, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0009', '{
    "ar": "ضرائب عقارية",
    "en": "Real estate taxes"
  }', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (133, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0010', '{
    "ar": "ضريبة خدمة",
    "en": "Service tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (134, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0011', '{
    "ar": "ضريبة الاضافة",
    "en": "Adding tax"
  }', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (135, 81, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '0012', '{
    "ar": "ضريبة مبيعات",
    "en": "Sales tax"
  }', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (136, 83, '2020-08-17 12:37:10.570000', '2020-08-17 12:37:10.570000', 'hr1', 'hr1', '0007', '{
  "ar": "ضريبة القيمة المضافة",
  "en": "Vat"
}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (137, 83, '2020-08-17 12:37:10.580000', '2020-08-17 12:37:10.580000', 'hr1', 'hr1', '0008', '{
  "ar": "ضريبة أرباح تجارية وصناعية",
  "en": "Commercial and industrial profits tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (138, 83, '2020-08-17 12:37:10.581000', '2020-08-17 12:37:10.581000', 'hr1', 'hr1', '0009', '{
  "ar": "ضرائب عقارية",
  "en": "Real estate taxes"
}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (139, 83, '2020-08-17 12:37:10.583000', '2020-08-17 12:37:10.583000', 'hr1', 'hr1', '0010', '{
  "ar": "ضريبة خدمة",
  "en": "Service tax"
}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (140, 83, '2020-08-17 12:37:10.584000', '2020-08-17 12:37:10.584000', 'hr1', 'hr1', '0011', '{
  "ar": "ضريبة الاضافة",
  "en": "Adding tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (141, 83, '2020-08-17 12:37:10.586000', '2020-08-17 12:37:10.586000', 'hr1', 'hr1', '0012', '{
  "ar": "ضريبة مبيعات",
  "en": "Sales tax"
}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (142, 84, '2020-08-30 09:55:27.680000', '2020-08-30 09:55:27.680000', 'hr1', 'hr1', '0007', '{
  "ar": "ضريبة القيمة المضافة",
  "en": "Vat"
}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (143, 84, '2020-08-30 09:55:27.690000', '2020-08-30 09:55:27.690000', 'hr1', 'hr1', '0008', '{
  "ar": "ضريبة أرباح تجارية وصناعية",
  "en": "Commercial and industrial profits tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (144, 84, '2020-08-30 09:55:27.691000', '2020-08-30 09:55:27.691000', 'hr1', 'hr1', '0009', '{
  "ar": "ضرائب عقارية",
  "en": "Real estate taxes"
}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (145, 84, '2020-08-30 09:55:27.693000', '2020-08-30 09:55:27.693000', 'hr1', 'hr1', '0010', '{
  "ar": "ضريبة خدمة",
  "en": "Service tax"
}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (146, 84, '2020-08-30 09:55:27.694000', '2020-08-30 09:55:27.694000', 'hr1', 'hr1', '0011', '{
  "ar": "ضريبة الاضافة",
  "en": "Adding tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (147, 84, '2020-08-30 09:55:27.696000', '2020-08-30 09:55:27.696000', 'hr1', 'hr1', '0012', '{
  "ar": "ضريبة مبيعات",
  "en": "Sales tax"
}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (148, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0007', '{
  "ar": "ضريبة القيمة المضافة",
  "en": "Vat"
}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (149, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0008', '{
  "ar": "ضريبة أرباح تجارية وصناعية",
  "en": "Commercial and industrial profits tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (150, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0009', '{
  "ar": "ضرائب عقارية",
  "en": "Real estate taxes"
}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (151, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0010', '{
  "ar": "ضريبة خدمة",
  "en": "Service tax"
}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (152, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0011', '{
  "ar": "ضريبة الاضافة",
  "en": "Adding tax"
}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     code, name, percentage)
VALUES (153, 85, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '0012', '{
  "ar": "ضريبة مبيعات",
  "en": "Sales tax"
}', 12);
