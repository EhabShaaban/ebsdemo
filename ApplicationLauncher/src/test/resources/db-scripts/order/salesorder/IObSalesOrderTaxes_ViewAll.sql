DELETE FROM iobsalesordertax;

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (1, 51, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (2, 51, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (3, 51, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (4, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (5, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (6, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (7, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (8, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (9, 52, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (10, 53, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (11, 53, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (12, 53, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (13, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (14, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (15, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (16, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (17, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (18, 54, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (19, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (20, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (21, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (22, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (23, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (24, 56, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (25, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (26, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (27, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (28, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (29, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (30, 61, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (31, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (32, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (33, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (34, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (35, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (36, 62, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (37, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0007', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (38, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (39, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0009', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (40, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0010', '{"ar": "ضريبة خدمة", "en": "Service tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (41, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (42, 63, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0012', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}', 12);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (43, 57, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (44, 57, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (45, 57, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (46, 58, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (47, 58, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (48, 58, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (49, 59, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (50, 59, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (51, 59, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (52, 60, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 1);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (53, 60, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0002', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}', 0.5);

INSERT INTO public.iobsalesordertax (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage)
VALUES (54, 60, '2020-08-11 16:51:19.066000', '2020-08-11 16:51:24.650000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}', 10);