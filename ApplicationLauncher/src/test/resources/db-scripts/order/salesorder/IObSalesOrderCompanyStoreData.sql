DELETE
FROM iobsalesordercompanystoredata
WHERE refinstanceid between 65 AND 85;

ALTER SEQUENCE iobsalesordercompanystoredata_id_seq RESTART WITH 50;
--------------------------------------------------------------------------------------------

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (73, 73, '2020-03-19 07:47:46.000000', '2020-03-19 07:47:47.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, null);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (74, 74, '2020-03-19 08:13:43.000000', '2020-03-19 08:13:44.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, null);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (75, 75, '2020-03-22 09:11:05.000000', '2020-03-22 09:11:06.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 16);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (76, 76, '2020-03-22 09:24:51.000000', '2020-03-22 09:24:53.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (65, 65, '2020-04-01 14:02:08.000000', '2020-04-01 14:02:09.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (66, 66, '2020-04-30 13:27:03.000000', '2020-04-30 13:27:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (67, 67, '2020-04-30 13:50:53.000000', '2020-04-30 13:50:54.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (68, 68, '2020-04-30 14:54:41.000000', '2020-04-30 14:54:42.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (69, 69, '2020-05-07 09:49:53.000000', '2020-05-07 09:49:54.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (70, 70, '2020-05-07 10:01:19.000000', '2020-05-07 10:01:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (78, 78, '2020-06-09 09:56:17.000000', '2020-06-09 09:56:18.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, null);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (79, 79, '2020-06-09 09:58:59.000000', '2020-06-09 09:59:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (80, 80, '2020-06-09 11:46:01.000000', '2020-06-09 11:46:02.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 10, 14);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (81, 81, '2020-06-30 13:59:53.981000', '2020-06-30 13:59:53.999000', 'hr1', 'hr1', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (83, 83, '2020-08-17 12:37:10.587000', '2020-08-17 12:37:19.339000', 'hr1', 'hr1', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (82, 82, '2020-08-06 14:54:35.541000', '2020-08-06 14:54:35.561000', 'hr1', 'hr1', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (84, 84, '2020-08-30 09:55:27.714000', '2020-08-30 09:57:05.917000', 'hr1', 'hr1', 7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, storeid)
VALUES (85, 85,  '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed',  7, 9);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, companyid, storeid)
VALUES (77, 77, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 7, null);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, companyid, storeid)
VALUES (72, 72, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 7, null);

INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, companyid, storeid)
VALUES (71, 71, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 7, null);

-- INSERT INTO public.iobsalesordercompanystoredata (id, refinstanceid, creationdate, modifieddate, creationinfo,
--                                                   modificationinfo, companyid, storeid)
-- VALUES (55, 55, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 7, null);
