DELETE FROM mobcustomer;

alter sequence mobcustomer_id_seq RESTART with 3000;

DELETE from EntityUserCode WHERE type = 'MObCustomer';
INSERT INTO EntityUserCode (type, code) values ('MObCustomer', '000007');

INSERT INTO public.mobcustomer (id, kapid, creditlimit, type, marketname, oldCustomerNumber)
VALUES (66, 34, 300000.0, 'LOCAL', '{"ar": "Al Ahram", "en": "Al Ahram"}', '1');

INSERT INTO public.mobcustomer (id, kapid, creditlimit, type, marketname, oldCustomerNumber)
VALUES (67, 36, null, 'LOCAL', '{"ar": "عميل2", "en": "Client2"}', '2');

INSERT INTO public.mobcustomer (id, kapid, creditlimit, type, marketname, oldCustomerNumber)
VALUES (68, 36, null, 'LOCAL', '{"ar": "عميل3", "en": "Client3"}', '3');

INSERT INTO public.mobcustomer (id, kapid, creditlimit, type, marketname, oldCustomerNumber)
VALUES (72, null, 100000.0, 'LOCAL', '{"ar": "المطبعة الأمنية", "en": "المطبعة الأمنية"}', '4');

INSERT INTO public.mobcustomer (id, kapid, creditlimit, type, marketname, oldCustomerNumber)
VALUES (73, null, 100000.0, 'LOCAL', '{"ar": "مطبعة أكتوبر الهندسية", "en": "مطبعة أكتوبر الهندسية"}', '5');
