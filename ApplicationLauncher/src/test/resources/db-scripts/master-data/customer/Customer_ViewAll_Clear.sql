TRUNCATE masterdata CASCADE;
TRUNCATE DObSalesOrder CASCADE;
ALTER SEQUENCE iobcustomercontactperson_id_seq RESTART WITH 1;
ALTER SEQUENCE iobcustomeraddress_id_seq RESTART WITH 1;
