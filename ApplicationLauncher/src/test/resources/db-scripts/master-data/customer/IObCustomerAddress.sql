DELETE FROM iobcustomeraddress;

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (1, 66, '2019-10-28 11:19:05.000000', '2019-10-28 11:19:07.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1, null);

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (2, 67, '2019-11-03 13:14:38.000000', '2019-11-03 13:14:40.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1, 'MAIN_ADDRESS');

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (3, 72, '2020-02-26 11:16:30.000000', '2020-02-26 11:16:31.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2, 'MAIN_ADDRESS');

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (4, 72, '2020-02-26 11:17:55.000000', '2020-02-26 11:18:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 3, 'HOME_ADDRESS');

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (5, 73, '2020-02-26 11:18:36.000000', '2020-02-26 11:18:37.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 3, 'MAIN_ADDRESS');

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (6, 73, '2020-03-30 13:36:12.000000', '2020-03-30 13:36:13.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 4, 'SECONDARY_ADDRESS');

INSERT INTO public.iobcustomeraddress (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, addressid, type)
VALUES (7, 68, '2020-06-09 11:47:29.000000', '2020-06-09 11:47:30.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 3, 'MAIN_ADDRESS');