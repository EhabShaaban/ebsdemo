DELETE FROM ItemVendorRecord;
DELETE FROM dobpurchaseorder;
DELETE FROM masterdata WHERE objecttypecode = '3';

ALTER SEQUENCE masterdata_id_seq RESTART WITH 197;

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (66, '000001', '2019-10-28 09:31:58.000000', '2019-10-28 09:31:59.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "Al Ahram", "en": "Al Ahram"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (67, '000002', '2019-10-28 10:37:46.000000', '2019-10-28 10:37:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "عميل2", "en": "Client2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (68, '000004', '2019-10-28 11:19:58.000000', '2019-10-28 11:20:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "عميل3", "en": "Client3"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (70, '000005', '2019-10-28 13:07:40.000000', '2019-10-28 13:07:43.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "Client4", "en": "Client4"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (72, '000006', '2020-02-04 07:57:25.000000', '2020-02-04 07:57:26.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "المطبعة الأمنية", "en": "المطبعة الأمنية"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (73, '000007', '2020-02-04 09:27:20.000000', '2020-02-04 09:27:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '3', '["Active"]', '{"ar": "مطبعة أكتوبر الهندسية", "en": "مطبعة أكتوبر الهندسية"}');

