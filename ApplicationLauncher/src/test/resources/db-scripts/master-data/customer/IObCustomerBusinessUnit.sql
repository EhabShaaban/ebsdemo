delete from iobcustomerbusinessunit;

alter sequence iobcustomerbusinessunit_id_seq RESTART with 3000;

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (1, 66, '2019-10-28 11:16:08.000000', '2019-10-28 11:16:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 19);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (2, 67, '2019-10-28 10:49:06.000000', '2019-10-28 10:49:07.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (3, 68, '2019-10-28 14:03:05.000000', '2019-10-28 14:03:07.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (4, 66, '2019-11-03 14:29:19.000000', '2019-11-03 14:29:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (5, 72, '2020-02-04 08:28:34.000000', '2020-02-04 08:28:35.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (6, 73, '2020-02-04 09:33:42.000000', '2020-02-04 09:33:43.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 19);

INSERT INTO public.iobcustomerbusinessunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessunitid)
VALUES (7, 72, '2020-02-04 10:35:40.000000', '2020-02-04 10:35:42.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 20);