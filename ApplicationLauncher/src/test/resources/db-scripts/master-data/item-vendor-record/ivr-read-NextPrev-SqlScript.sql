DELETE  FROM  ItemVendorRecord;

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (1, '000001', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 33, 37, 1, 'GDF530-440',12345678910, 50, 19);
INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (2, '000002', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 34, 37, 1, 'GDB550-610',234567891011, 100, 19);
INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (3, '000003', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 35, 42, 1, 'GDH670-777',34567891011, 100, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (4, '000004', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 36, 45, 2, 'GDH670-888',4567891011, 50, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (5, '000005', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 33, 38, 3, 'GDF630-440',567891011, 11500.80, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (6, '000006', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 34, 38, 4, 'GDF730-440',67891011, 600.40, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (7, '000007', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 35, 43, 5, 'GDF830-440',78910111, 50.30, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (8, '000008', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 38, 41, 1, 'R6000',89101112, 200.50, 1);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (9, '000009', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 36,44, 6, 'GDF530-200',9101112, 100.30, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (10, '000010', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 2, 1,37, 3, 'GDF530-201',1011121314, 50.00, 22);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (11, '000011', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 50, 1,37, 3, 'GDF530-202',11121314, 100.00, 23);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (12, '000012', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 1,37, 5, 'GDF530-203',12131415, 70.00, 1);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (13, '000013', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 2,37 ,40, 3, 'GDF530-205',13141516, 11500.80, 1);