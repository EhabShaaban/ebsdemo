delete from ItemVendorRecord where id between 51 and 73;

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode,oldItemNumber, price, purchaseunitid)
VALUES (51, '000051', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 37, 40, 1, 'GDH670-ccc',1234567891011 ,100, 1);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode,oldItemNumber, price, purchaseunitid)
VALUES (52, '000052', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 44, 48, 1, 'GDH670-aaa',2345678910111 ,100, 23);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode,oldItemNumber, price, purchaseunitid)
VALUES (53, '000053', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 44, 49, 1, 'GDH670-bbb',345678910111 ,100, 23);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (54, '000054', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 33, 49, 1, 'GDF530-440',12843, 50, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (55, '000055', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 34, 49, 1, 'GDB550-610',20090, 100, 19);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (56, '000056', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 35, 48, 1, 'GDH670-777',34567891011, 100, 1);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (57, '000057', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 36, 39, 2, 'GDH670-888',4567891011, 50, 23);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (58, '000058', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 37, 40, 1, 'GDH670-eee',62352536526, 100, 1);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode,oldItemNumber, price, purchaseunitid)
VALUES (59, '000059', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 44, 48, 1, 'GDH670-aaa',2345678910111 ,100, 23);

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,uomId, currencyid, itemvendorcode,oldItemNumber, price, purchaseunitid)
VALUES (60, '000060', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 44, 49, 1, 'GDH670-bbb',345678910111 ,100, 23);

INSERT INTO itemvendorrecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, olditemnumber, uomid, currencyid, itemvendorcode, price, purchaseunitid, alternativeuomid)
VALUES (61, '000061', '2020-02-04 12:05:49.797000', '2020-02-04 14:29:57.348000', 'hr1', 'hr1', 45, 37, '5432', 23, null, '1234', null, 1, null);

INSERT INTO itemvendorrecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, olditemnumber, uomid, currencyid, itemvendorcode, price, purchaseunitid, alternativeuomid)
VALUES (62, '000062', '2020-02-04 12:06:16.369000', '2020-02-04 14:28:42.641000', 'hr1', 'hr1', 46, 57, '44655', 41, null, '123', null, 19, 17);

INSERT INTO itemvendorrecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, olditemnumber, uomid, currencyid, itemvendorcode, price, purchaseunitid, alternativeuomid)
VALUES (63, '000063', '2020-02-04 12:05:49.797000', '2020-02-04 14:29:57.348000', 'hr1', 'hr1', 46, 57, '5432', 23, null, '1234', null, 19, null);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (65, '000065',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 35, '000030',6789101112, 4, 19);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (66, '000066',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 36, '12345',78910111234, 45, 19);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (67, '000067',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 48, 37, 'R5000',89101112131, 40, 1);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (68, '000068',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 48, 38, '12345',91011121314, 41, 1);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (69, '000069',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 45, 64, 'GDH670-888',101112131415, 48, 19);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (70, '000070',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 44, 'GDH670-888',12345678910111213, 48, 1);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (71, '000071',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 38, '12345',91011121314, 41, 1);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (72, '000072',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 52, 'GDH670-888',12345678910111213, 48, 23);

INSERT INTO ItemVendorRecord (id, code,  creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid, itemvendorcode, oldItemNumber, uomid, purchaseunitid)
VALUES (73, '000073',  '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 37, 'GDH670-999',12345678910111213, 23, 1);

