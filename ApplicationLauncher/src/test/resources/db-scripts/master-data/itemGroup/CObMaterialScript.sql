INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (1, 'FGOO', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '1', '["Active"]', '{"en":"Finishes Goods","ar":"بضاعة تامة "}');

INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (2, 'TGOO', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '1', '["Active"]', '{"ar":"سلعة تجارية","en":"Trading Goods"}');

INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (5, '0001', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '2', '["Inactive"]', '{"en":"Blocked for Purchasing","ar":"مغلق للشراء"}');

INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (20, '0003', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '2', '["Active"]', '{"en":"Dina","ar":"dina"}');

INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (4, 'TRIN', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '1', '["Active"]', '{"ar":"بضاعة تنقل من مخزن لاخر ","en":"Transit Inventory"}');

INSERT INTO cobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (3, 'RAWM', '2018-04-24 06:31:56.354203', '2018-04-24 06:31:56.354203', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '1', '["Active"]', '{"ar":"مواد خام","en":"Raw Materials"}');

INSERT INTO cobmaterialstatus (id, description)
VALUES (5, 'Blocked for Purchasing Due to .....');

INSERT INTO cobmaterialstatus (id, description)
VALUES (20, NULL);

INSERT INTO iobmaterialrestriction (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessoperation, restriction)
VALUES (1, 5, '2018-04-24 06:31:59.546298', '2018-04-24 06:31:59.546298', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 47, 'ALLOWED_WITH_WARNING');

INSERT INTO iobmaterialrestriction (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessoperation, restriction)
VALUES (2, 20, '2018-04-24 06:31:59.546298', '2018-04-24 06:31:59.546298', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 22, 'NOT_ALLOWED');

INSERT INTO cobmaterialtype (id, internalprocurement, externalprocurement, valuationmethod, accountcategory, quantityupdating, valueupdating)
VALUES (1, 'ALLOWED', 'ALLOWED', 'STANDARD', NULL, 'NO_VALUATION_AREAS', 'NO_VALUATION_AREAS');

INSERT INTO cobmaterialtype (id, internalprocurement, externalprocurement, valuationmethod, accountcategory, quantityupdating, valueupdating)
VALUES (2, 'ALLOWED', 'ALLOWED', 'STANDARD', NULL, 'NO_VALUATION_AREAS', 'NO_VALUATION_AREAS');

INSERT INTO cobmaterialtype (id, internalprocurement, externalprocurement, valuationmethod, accountcategory, quantityupdating, valueupdating)
VALUES (3, 'ALLOWED', 'ALLOWED', 'STANDARD', NULL, 'NO_VALUATION_AREAS', 'NO_VALUATION_AREAS');

INSERT INTO cobmaterialtype (id, internalprocurement, externalprocurement, valuationmethod, accountcategory, quantityupdating, valueupdating)
VALUES (4, 'ALLOWED_WITH_WARRING', 'ALLOWED', 'STANDARD', NULL, 'NO_VALUATION_AREAS', 'NO_VALUATION_AREAS');

INSERT INTO iobmaterialallowedview (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, materialview)
VALUES (1, 1, '2018-04-24 06:31:59.5314', '2018-04-24 06:31:59.5314', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 42);

INSERT INTO iobmaterialallowedview (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, materialview)
VALUES (2, 2, '2018-04-24 06:31:59.5314', '2018-04-24 06:31:59.5314','hr1 from BDKCompanyCode','hr1 from BDKCompanyCode',42);

INSERT INTO iobmaterialallowedview (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, materialview)
VALUES (3, 3, '2018-04-24 06:31:59.5314', '2018-04-24 06:31:59.5314', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 42);

INSERT INTO iobmaterialallowedview (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, materialview)
VALUES (4, 4, '2018-04-24 06:31:59.5314', '2018-04-24 06:31:59.5314', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', 42);

INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo, modificationinfo, name)
VALUES (1, 'ROOT', null, '', '000001', '2018-04-24 06:31:59.407293', '2018-04-24 06:31:59.407293', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '{ "en": "Ink", "ar": "حبر" }');

INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo, modificationinfo, name)
VALUES (2, 'HIERARCHY', 1, '', '000002', '2018-04-24 06:31:59.407293', '2018-04-24 06:31:59.407293', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '{ "ar": "Signmedia Inks", "en": "Signmedia Inks" }');

INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo, modificationinfo, name)
VALUES (3, 'LEAF', 2, '', '000003', '2018-04-24 06:31:59.407293', '2018-04-24 06:31:59.407293', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '{ "ar": "White Signmedia Inks", "en": "White Signmedia Inks" }');

ALTER SEQUENCE cobmaterialgroup_id_seq RESTART WITH 4;