DELETE FROM masterdata where objecttypecode = '4';
ALTER SEQUENCE masterdata_id_seq RESTART WITH 197;

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (193, '000001', '2021-01-06 10:09:32.969023', '2021-01-06 10:09:32.969023', 'admin', 'admin', '4', '["Active"]', '{"en": "Afaf" , "ar": "Afaf"}');
INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (194, '000002', '2021-01-06 10:09:51.343503', '2021-01-06 10:09:51.343503', 'admin', 'admin', '4', '["Active"]', '{"en": "Ahmed Hamdi" , "ar": "Ahmed Hamdi"}');
INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (195, '000003', '2021-01-06 10:10:23.075628', '2021-01-06 10:10:23.075628', 'admin', 'admin', '4', '["Active"]', '{"en": "Amr Khalil" , "ar": "Amr Khalil"}');
INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (196, '000004', '2021-01-06 10:10:37.798844', '2021-01-06 10:10:37.798844', 'admin', 'admin', '4', '["Active"]', '{"en": "Marwa Tawfik" , "ar": "Marwa Tawfik"}');

INSERT INTO public.mobbusinesspartner (id) VALUES (193);
INSERT INTO public.mobbusinesspartner (id) VALUES (194);
INSERT INTO public.mobbusinesspartner (id) VALUES (195);
INSERT INTO public.mobbusinesspartner (id) VALUES (196);

INSERT INTO public.mobemployee (id) VALUES (193);
INSERT INTO public.mobemployee (id) VALUES (194);
INSERT INTO public.mobemployee (id) VALUES (195);
INSERT INTO public.mobemployee (id) VALUES (196);
