delete from cobexchangerate;
ALTER SEQUENCE cobexchangerate_id_seq RESTART WITH 35;

DELETE FROM EntityUserCode WHERE type = 'CObExchangeRate';

INSERT INTO EntityUserCode (type, code)
values ('CObExchangeRate', '2018000034');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (1, '2018000001', 1.0, 1, 0.43, 4, '2019-08-05 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EGP-CNY","en": "EGP-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (2, '2018000002', 1.0, 1, 0.53, 4, '2019-08-04 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EGP-CNY","en": "EGP-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (3, '2018000003', 1.0, 1, 0.63, 4, '2019-08-03 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EGP-CNY","en": "EGP-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (4, '2018000004', 1.0, 2, 7.13, 4, '2019-08-05 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-CNY","en": "USD-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (5, '2018000005', 1.0, 2, 7.23, 4, '2019-08-04 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-CNY","en": "USD-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (6, '2018000006', 1.0, 2, 7.33, 4, '2019-08-03 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-CNY","en": "USD-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (7, '2018000007', 1.0, 2, 16.32, 1, '2019-09-18 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (8, '2018000008', 1.0, 2, 17.54, 1, '2019-06-05 09:02:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (9, '2018000009', 1.0, 1, 0.40, 4, '2019-08-05 08:02:00', '2019-08-05 08:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EGP-CNY","en": "EGP-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (10, '2018000010', 1.0, 2, 17.54, 1, '2019-09-18 09:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (11, '2018000011', 1.0, 2, 11.00, 1, '2019-09-19 09:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (12, '2018000012', 1.0, 2, 16.50, 1, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');

/* **************************************** EXCHANGE RATES FOR SAME CURRENCY **************************************** */
insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (13, '2018000013', 1.0, 1, 1.0, 1, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EGP-EGP","en": "EGP-EGP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (14, '2018000014', 1.0, 2, 1.0, 2, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-USD","en": "USD-USD"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (15, '2018000015', 1.0, 3, 1.0, 3, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "EUR-EUR","en": "EUR-EUR"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (16, '2018000016', 1.0, 4, 1.0, 4, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "CNY-CNY","en": "CNY-CNY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (17, '2018000017', 1.0, 5, 1.0, 5, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "JPY-JPY","en": "JPY-JPY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (18, '2018000018', 1.0, 6, 1.0, 6, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "HKD-HKD","en": "HKD-HKD"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (19, '2018000019', 1.0, 7, 1.0, 7, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "INR-INR","en": "INR-INR"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (20, '2018000020', 1.0, 8, 1.0, 8, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "KPW-KPW","en": "KPW-KPW"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (21, '2018000021', 1.0, 9, 1.0, 9, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "KPW-KPW","en": "KPW-KPW"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (22, '2018000022', 1.0, 10, 1.0, 10, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "SAR-SAR","en": "SAR-SAR"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (23, '2018000023', 1.0, 11, 1.0, 11, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "TRY-TRY","en": "TRY-TRY"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (24, '2018000024', 1.0, 12, 1.0, 12, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "AED-AED","en": "AED-AED"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (25, '2018000025', 1.0, 13, 1.0, 13, '2019-09-19 21:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "GBP-GBP","en": "GBP-GBP"}');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)
values (26, '2018000026', 1.0, 2, 17.44, 1, '2019-09-19 22:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "USD-EGP","en": "USD-EGP"}');
insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid, validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo, currentstates, typeid, name)

values (27, '2018000027', 1.0, 6, 12, 1, '2019-09-19 22:00:00', '2019-08-05 09:02:00', 'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{"ar": "HKD-EGP","en": "HKD-EGP"}');

INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (28, '2018000028', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 19.2264, 3, 1, 2, '{"ar":"EUR-EGP", "en":"EUR-EGP"}');

INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (29, '2018000029', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 4.2933, 12, 1, 2, '{"ar":"AED-EGP", "en":"AED-EGP"}');
		
INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (30, '2018000030', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 15.7684, 2, 1, 2, '{"ar":"USD-EGP", "en":"USD-EGP"}');
		
INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (31, '2018000031', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 15.2102, 5, 1, 2, '{"ar":"JPY-EGP", "en":"JPY-EGP"}');
		
INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (32, '2018000032', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 4.2023, 10, 1, 2, '{"ar":"SAR-EGP", "en":"SAR-EGP"}');

INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (33, '2018000033', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 21.4276, 13, 1, 2, '{"ar":"GBP-EGP", "en":"GBP-EGP"}');

INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (34, '2018000034', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 2.4162, 4, 1, 2, '{"ar":"CNY-EGP", "en":"CNY-EGP"}');
