DELETE
FROM cobexchangerate;
ALTER SEQUENCE cobexchangerate_id_seq RESTART WITH 7;
DELETE
FROM EntityUserCode
WHERE type = 'CObExchangeRate';

INSERT INTO EntityUserCode (type, code)
VALUES ('CObExchangeRate', '2018000006');


insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid,
                             validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo,
                             currentstates, typeid, name)
values (1, '2018000001', 1.0, 1, 0.43, 4, '2019-08-05 09:02:00', '2019-08-05 09:02:00',
        'Ashraf.Salah',
        '2019-08-05 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{
    "ar": "EGP-CNY",
    "en": "EGP-CNY"
  }');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid,
                             validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo,
                             currentstates, typeid, name)
values (2, '2018000002', 1.0, 1, 0.53, 4, '2019-08-06 09:00:00', '2019-08-06 09:00:00',
        'Ashraf.Salah',
        '2019-08-06 09:00:00', 'Ashraf.Salah', '["Active"]', 1, '{
    "ar": "EGP-CNY",
    "en": "EGP-CNY"
  }');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid,
                             validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo,
                             currentstates, typeid, name)
values (3, '2018000003', 1.0, 1, 0.63, 4, '2019-08-06 09:02:00', '2019-08-06 09:02:00',
        'Ahmed.Hamdi',
        '2019-08-06 09:02:00', 'Ahmed.Hamdi', '["Active"]', 1, '{
    "ar": "EGP-CNY",
    "en": "EGP-CNY"
  }');

insert into cobexchangerate (id, code, firstvalue, firstcurrencyid, secondvalue, secondcurrencyid,
                             validfrom,
                             creationdate, creationinfo, modifieddate, modificationinfo,
                             currentstates, typeid, name)
values (4, '2018000004', 1.0, 2, 0.43, 1, '2019-08-07 09:02:00', '2019-08-07 09:02:00',
        'Ashraf.Salah',
        '2019-08-07 09:02:00', 'Ashraf.Salah', '["Active"]', 1, '{
    "ar": "USD-EGP",
    "en": "USD-EGP"
  }');
  

INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (5, '2018000005', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 15.7684, 2, 1, 2, '{"ar":"USD-EGP", "en":"USD-EGP"}');
		
INSERT INTO cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, 
							firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, typeid, "name") 
VALUES (6, '2018000006', '2020-12-27 11:35:00', '2020-12-27 11:35:00', '2020-12-27 11:35:00', 'System', 'System', '["Active"]',
		1.0, 2.4162, 4, 1, 2, '{"ar":"CNY-EGP", "en":"CNY-EGP"}');
		