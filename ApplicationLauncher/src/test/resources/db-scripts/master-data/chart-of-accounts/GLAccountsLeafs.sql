INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (9, '10102', '{
  "ar": "Lands",
  "en": "Lands"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 2);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (9,
        'TYPE_11', 'DEBIT', null);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (10, '10202', '{
  "ar": "Treasury",
  "en": "Treasury"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (10,
        'TYPE_11', 'DEBIT', null, 'TreasuryGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (11, '10203', '{
  "ar": "Customers",
  "en": "Customers"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (11,
        'TYPE_11', 'DEBIT', 'Local_Customer', 'CustomersGLAccount');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (12, '10204', '{
  "ar": "Banks",
  "en": "Banks"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (12,
        'TYPE_11', 'DEBIT', 'Banks', 'BanksGLAccount');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (13, '10430', '{
  "ar": "Cash",
  "en": "Cash"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', NULL);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (13,
        'TYPE_11', 'DEBIT', 'Treasuries', 'CashGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (14, '41101', '{
  "ar": "Realized Differences on Exchange",
  "en": "Realized Differences on Exchange"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', NULL);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (14, 'TYPE_01', 'CREDIT', null, 'RealizedGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (15, '10431', '{
  "ar": "Bank",
  "en": "Bank"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', NULL);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (15, 'TYPE_01', 'DEBIT', 'Banks', 'BankGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (20, '10220', '{
  "ar": "GoodsInProgress",
  "en": "GoodsInProgress"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (20, 'TYPE_01', 'CREDIT', 'PO');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (21, '10221', '{
  "ar": "service vendors 2",
  "en": "service vendors 2"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (21,
        'TYPE_11', 'DEBIT', 'Local_Vendors');



INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (22, '10205', '{
  "ar": "Beginning inventory",
  "en": "Beginning inventory"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (22,
        'TYPE_02', 'DEBIT', 'PO');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (23, '10206', '{
  "ar": "Debitors",
  "en": "Debitors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (23,
        'TYPE_11', 'DEBIT', 'PO');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (24, '10107', '{
  "ar": "Lands 2",
  "en": "Lands 2"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 2);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (24,
        'TYPE_11', 'DEBIT', 'PO');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (25, '10207', '{
  "ar": "PO",
  "en": "PO"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (25,
        'TYPE_02', 'DEBIT', 'PO', 'PurchaseOrderGLAccount');



INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (26, '10208', '{
  "ar": "Notes receivables",
  "en": "Notes receivables"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (26,
        'TYPE_11', 'DEBIT', 'Notes_Receivables', 'NotesReceivablesGLAccount');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (27, '40101', '{
  "ar": "Realized exchange rate",
  "en": "Realized exchange rate"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 29);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (27,
        'TYPE_02', 'CREDIT', NULL);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (34, '20101', '{
  "ar": "Creditors",
  "en": "Creditors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 7);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (34,
        'TYPE_02', 'CREDIT', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (35, '20103', '{
  "ar": "Local Vendors",
  "en": "Local Vendors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 7);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (35,
        'TYPE_11', 'CREDIT', 'Local_Vendors');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (36, '20104', '{
  "ar": "Import Vendors",
  "en": "Import Vendors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 7);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (36,
        'TYPE_11', 'CREDIT', 'Local_Vendors');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (37, '30101', '{
  "ar": "sales expenses",
  "en": "sales expenses"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 31);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (37,
        'TYPE_02', 'DEBIT', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (38, '30102', '{
  "ar": "Adminstrative expenses",
  "en": "Adminstrative expenses"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 31);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (38,
        'TYPE_02', 'DEBIT', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (39, '30201', '{
  "ar": "Local",
  "en": "Local"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 32);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (39,
        'TYPE_02', 'DEBIT', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (40, '30202', '{
  "ar": "Vendors",
  "en": "Vendors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 32);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (40,
        null, 'DEBIT', 'Local_Vendors');



INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (41, '40102', '{
  "ar": "un realized exchange rate",
  "en": "un realized exchange rate"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 29);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (41,
        'TYPE_02', 'CREDIT', NULL, 'UnrealizedCurrencyGainLossGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (42, '40202', '{
  "ar": "sales",
  "en": "sales"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 33);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (42,
        'TYPE_02', 'CREDIT', NULL, 'SalesGLAccount');




INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (43, '20102', '{
  "ar": "Taxes",
  "en": "Taxes"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 7);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (43,
        'TYPE_11', 'CREDIT', 'Tax', 'TaxesGLAccount');


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (44, '20201', '{
  "ar": "Capital Price",
  "en": "Capital Price"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 30);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (44,
        'TYPE_03', 'CREDIT', null);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (45, '20202', '{
  "ar": "last year profit",
  "en": "last year profit"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 30);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (45,
        'TYPE_04', 'CREDIT', null);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (63, '70101', '{
  "ar": "Import Purchasing",
  "en": "Import Purchasing"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 62);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (63,
        'TYPE_02', 'DEBIT', 'PO', 'ImportPurchasingGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (64, '70102', '{
  "ar": "Local Purchasing",
  "en": "Local Purchasing"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 62);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger, mappedaccount)
VALUES (64,
        'TYPE_02', 'DEBIT', 'PO', 'LocalPurchasingGLAccount');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (65, '70103', '{
  "ar": "Purchasing",
  "en": "Purchasing"
}', '2021-06-05 09:02:00', '2021-06-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 62);

INSERT INTO LeafGLAccount (id, type, creditDebit, mappedaccount)
VALUES (65,
        'TYPE_02', 'DEBIT', 'PurchasingGLAccount');
