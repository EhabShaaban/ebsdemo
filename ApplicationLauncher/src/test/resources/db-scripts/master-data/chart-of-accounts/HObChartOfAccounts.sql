delete from LObGlobalGLAccountConfig;

delete
from iobvendoraccountinginfo;
delete
from iobitemaccountinginfo;

DELETE
FROM LeafGLAccount;

DELETE
FROM HObGLAccount;

ALTER SEQUENCE HObGLAccount_id_seq RESTART WITH 1006;


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (1, '1', '{
  "ar": "Assets",
  "en": "Assets"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', NULL);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (2, '101', '{
  "ar": "Fixed Assets",
  "en": "Fixed Assets"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 1);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (3, '10101', '{
  "ar": "Machines",
  "en": "Machines"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["InActive"]', '3', 2);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (3,
        'TYPE_01', 'DEBIT', 'Banks');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (4, '102', '{
  "ar": "Current Assets",
  "en": "Current Assets"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 1);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (5, '10201', '{
  "ar": "Service Vendors",
  "en": "Service Vendors"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '3', 4);

INSERT INTO LeafGLAccount (id, type, creditDebit, leadger)
VALUES (5,
        'TYPE_11', 'DEBIT', 'Local_Vendors');

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (6, '2', '{
  "ar": "Liabilities",
  "en": "Liabilities"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', NULL);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (7, '201', '{
  "ar": "Current Liabilities",
  "en": "Current Liabilities"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 6);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (8, '3', '{
  "ar": "Uses",
  "en": "Uses"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', NULL);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (28, '4', '{
  "ar": "Resources",
  "en": "Resources"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', NULL);


INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (29, '401', '{
  "ar": "Exchange Rate",
  "en": "Exchange Rate"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 28);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (30, '202', '{
  "ar": "Ownership equity",
  "en": "Ownership equity"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 6);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (31, '301', '{
  "ar": "Expenses",
  "en": "Expenses"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 8);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (32, '302', '{
  "ar": "Purchased Items",
  "en": "Purchased Items"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 8);



INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (33, '402', '{
  "ar": "sales",
  "en": "sales"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 28);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (60, '6', '{
  "ar": "NoChildAccount",
  "en": "NoChildAccount"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (61, '7', '{
  "ar": "Usage",
  "en": "Usage"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '1', null);

INSERT INTO HObGLAccount
(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, level,
 parentid)
VALUES (62, '701', '{
  "ar": "Total Purchases",
  "en": "Total Purchases"
}', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1',
        '["Active"]', '2', 61);
