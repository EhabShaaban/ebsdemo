DELETE FROM mobitem where id between 33 AND 52;
DELETE FROM mobitem where id in (74,75);

DELETE from EntityUserCode WHERE type = 'MObItem';
INSERT INTO EntityUserCode (type, code) values ('MObItem', '000050');

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (33, '12345678910111213', 23, 3, FALSE, 1, 'Hot Laminated Frontlit Fabric roll' ,42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (34, '12345678910111213', 23, 3, FALSE, 1, 'Hot Laminated Frontlit Backlit roll' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (35, '12345678910111213', 4, 3, TRUE, 1, 'Flex Primer Varnish E33' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (36, '12345678910111213', 4, 3, TRUE, 1, 'Flex cold foil adhesive E01' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (37, '12345678910111213', 23, 3, FALSE, 1, 'self adhesive vinal 100 micron 140 grm glossy 1.07x50' , 43);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (38, '12345678910111213', 23, 3, FALSE, 1, 'wg-self adhesive vinal 100 micron 140grm glossy 1.27x50' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (39, '12345678910111213', 23, 3, FALSE, 1, 'PRO-V-ST-1' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (40, '12345678910111213', 23, 3, FALSE, 1, 'PRO-V-ST-2' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (41, '12345678910111213', 16, 3, FALSE, 1, 'Machine1' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (42, '12345678910111213', 4, 3, FALSE, 1, 'Machine2' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (43, '12345678910111213', 16, 3, FALSE, 1, 'Ink1' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (44, '12345678910111213', 16, 3, FALSE, 1, 'Ink2' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (52, '12345678910111213', 16, 3, FALSE, 1, 'Ink3' , 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (74, '12345678910111231', 23, null , false, 2, 'Insurance service', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname,productmanagerid)
VALUES (75, '12345678910111231', 23, null , false, 3, 'Laptop HP Pro-book core i5', 42);