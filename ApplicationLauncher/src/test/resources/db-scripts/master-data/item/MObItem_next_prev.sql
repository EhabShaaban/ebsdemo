DELETE FROM ItemVendorRecord WHERE itemid >= 100003 AND itemid <= 100012;
DELETE FROM masterdata WHERE id >= 100003 AND id <= 100012;
DELETE FROM mobitem WHERE id >= 100003 AND id <= 100012;
DELETE FROM iobitempurchaseunit WHERE refinstanceid >= 100003 AND refinstanceid <= 100012;
ALTER SEQUENCE masterdata_id_seq RESTART WITH 100013;
---------|----------|
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100003, '100003', '2018-08-02 07:21:44.148581', '2018-08-02 07:35:21.795', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Flex Primer Varnish E33","ar":"فليكس برايم فرنيش اي 33 "}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100004, '100004', '2018-08-02 07:22:09.406611', '2018-08-02 07:36:51.836', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Flex cold foil adhesive E01","ar":"فليكس كولد فويل ادهيسف اي 1"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100005, '100005', '2018-08-05 09:02:00', '2018-08-02 07:39:01.105', 'hr1', 'hr1', '1', '["Active"]', '{"en":"self adhesive vinal 100 micron 140 grm glossy 1.07x50","ar":"سيلف ادهيسف فانيل 100 مايكرون 140 جرام جلوسي 1.07*50ZFHV"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100006, '100006', '2018-08-02 07:23:37.28926', '2018-08-02 07:42:28.616', 'hr1', 'hr1', '1', '["Active"]', '{"en":"wg-self adhesive vinal 100 micron 140grm glossy 1.27x50","ar":"وج-سيلف ادهوسيف فانيل 100 مايكرون 140 جرام 1.27*50"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100007, '100007', '2018-08-02 07:23:54.906973', '2018-08-02 07:23:54.906973', 'hr1', 'hr1', '1', '["Active"]', '{"en":"PRO-V-ST-1","ar":"برو-في-ست-1"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100008, '100008', '2018-08-05 09:02:00', '2018-08-02 07:24:13.27467', 'hr1', 'hr1', '1', '["Active"]', '{"en":"PRO-V-ST-2","ar":"برو-في-ست-2"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100009, '100009', '2018-08-02 07:24:30.4213', '2018-08-02 07:24:30.4213', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Machine1","ar":"المكينة 1"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100010, '100010', '2018-08-02 07:24:46.998024', '2018-08-02 07:24:46.998024', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Machine2","ar":"المكينة 2"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100011, '100011', '2018-08-02 07:24:59.567488', '2018-08-02 07:24:59.567488', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink1","ar":"حبر 1"}');
INSERT INTO masterdata (id,  code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) VALUES (100012, '100012', '2018-08-05 09:02:00', '2018-08-02 07:25:14.55312', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink2","ar":"حبر 2"}');

------------------------------------------- MASTER DATA END ------------------------------------------------

INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100003, '12345678910111213', 4, 3, TRUE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100004, '12345678910111213', 4, 3, TRUE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100005, '12345678910111213', 23, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100006, '12345678910111213', 23, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100007, '12345678910111213', 1, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100008, '12345678910111213', 1, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100009, '12345678910111213', 1, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100010, '12345678910111213', 1, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100011, '12345678910111213', 16, 3, FALSE,1,'item', 42);
INSERT INTO mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (100012, '12345678910111213', 16, 3, FALSE,1,'item', 42);


INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100003,  100003, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100004,  100004, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100005,  100005, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100006,  100006, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100007,  100007, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100008,  100008, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100009,  100008, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100010,  100009, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100011,  100010, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 21);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100012,  100011, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 22);
INSERT INTO iobitempurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid) VALUES (100013,  100012, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 23);
