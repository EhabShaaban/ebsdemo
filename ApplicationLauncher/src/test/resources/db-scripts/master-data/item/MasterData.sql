DELETE FROM ItemVendorRecord;
DELETE FROM iobpurchaseorderfulfillervendor;
DELETE FROM dobpurchaseorder;
DELETE FROM masterdata WHERE objecttypecode = '1';

ALTER SEQUENCE masterdata_id_seq RESTART WITH 197;

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (1, '000060', '2018-08-05 09:02:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Hot Laminated Frontlit Fabric roll 1","ar":"هوت لمنيتيت فرونت ليت فابريك رول 1 "}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (33, '000001', '2018-08-05 09:02:00.000000', '2018-08-02 07:28:42.661000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Hot Laminated Frontlit Fabric roll","ar":"هوت لمنيتيت فرونت ليت فابريك رول"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (34, '000002', '2018-08-05 09:02:00.000000', '2018-08-02 07:30:38.411000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Hot Laminated Frontlit Backlit roll","ar":"هوت لمنيتيت فرونت ليت باكليت رول"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (35, '000003', '2018-08-05 09:02:00.000000', '2018-08-02 07:35:21.795000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Flex Primer Varnish E33","ar":"فليكس برايم فرنيش اي 33"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (36, '000004', '2018-08-05 09:02:00.000000', '2018-08-02 07:36:51.836000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Flex cold foil adhesive E01","ar":"فليكس كولد فويل ادهيسف اي 1"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (37, '000005', '2018-08-05 09:02:00.000000', '2018-08-02 07:39:01.105000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"self adhesive vinal 100 micron 140 grm glossy 1.07x50","ar":"سيلف ادهيسف فانيل 100 مايكرون 140 جرام جلوسي 1.07*50ZFHV"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (38, '000006', '2018-08-05 09:02:00.000000', '2018-08-02 07:42:28.616000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"wg-self adhesive vinal 100 micron 140grm glossy 1.27x50","ar":"وج-سيلف ادهوسيف فانيل 100 مايكرون 140 جرام 1.27*50"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (39, '000007', '2018-08-05 09:02:00.000000', '2018-08-02 07:23:54.906973', 'hr1', 'hr1', '1', '["Active"]', '{"en":"PRO-V-ST-1","ar":"برو-في-ست-1"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (40, '000008', '2018-08-05 09:02:00.000000', '2018-08-02 07:24:13.274670', 'hr1', 'hr1', '1', '["Active"]', '{"en":"PRO-V-ST-2","ar":"برو-في-ست-2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (41, '000009', '2018-08-05 09:02:00.000000', '2018-08-02 07:24:30.421300', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Machine1","ar":"المكينة 1"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (42, '000010', '2018-08-05 09:02:00.000000', '2018-08-02 07:24:46.998024', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Machine2","ar":"المكينة 2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (43, '000011', '2018-08-05 09:02:00.000000', '2018-08-02 07:24:59.567488', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink1","ar":"حبر 1"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (44, '000012', '2018-08-05 09:02:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink2","ar":"حبر 2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (52, '000013', '2018-08-05 09:02:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink3","ar":"حبر 3"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (57, '000053', '2018-08-05 09:02:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Hot Laminated Frontlit Fabric roll 2","ar":"هوت لمنيتيت فرونت ليت فابريك رول 2 "}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (58, '000054', '2018-08-05 09:02:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Hot Laminated Frontlit Fabric roll 3","ar":"هوت لمنيتيت فرونت ليت فابريك رول 3 "}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (59, '000059', '2018-08-05 09:02:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink3","ar":"حبر 3"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (64, '000051', '2018-08-05 09:02:00.000000', '2018-08-02 07:28:42.661000', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink5","ar":"Ink5"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (65, '000052', '2018-08-05 09:02:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '1', '["Active"]', '{"en":"Ink7","ar":"حبر 7"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (71, '000058', '2020-01-15 13:24:32.000000', '2020-01-15 13:24:33.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"ar": "فليكس برايم فرنيش اي 22", "en": "Flex Primer Varnish E22"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (74, '000014', '2020-02-04 09:27:00.000000', '2020-02-04 09:27:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"ar": "خدمة تأمين", "en": "Insurance service"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (75, '000015', '2020-02-04 09:27:00.000000', '2020-02-04 09:27:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"ar": "لابتوب", "en": "Laptop HP Pro-book core i5"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (190, '000055', '2020-02-04 09:27:00.000000', '2020-02-04 09:27:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"ar": "خدمة تأمين2", "en": "Insurance service2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (191, '000056', '2020-08-07 09:46:50.000000', '2020-08-07 09:46:52.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"en": "Shipment service", "ar": "خدمة النقل"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (192, '000057', '2020-08-07 09:47:52.000000', '2020-08-07 09:47:54.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '["Active"]', '{"en": "Bank Fees", "ar": "رسوم بنكية"}');