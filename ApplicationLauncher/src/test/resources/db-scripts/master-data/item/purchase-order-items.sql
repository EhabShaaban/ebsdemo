delete from doborderdocument where id = 1001;

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid) VALUES (1001, '2018000001', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "Cleared"]', 'IMPORT_PO', 1);
INSERT INTO public.dobpurchaseorder (id) VALUES (1001);

INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, vendorid) VALUES (1, '2020-10-04 09:52:15.292205', '2020-10-04 09:52:15.292205', 'Admin', 'Admin', 1001, 46);

INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES(1, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1001, null, 19, 'BILL_TO');

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,modificationinfo,itemid,quantityindn,unitOfMeasureId)
VALUES (23,1001,'2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695','hr1','hr1',33,NULL,23);
