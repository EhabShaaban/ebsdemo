DELETE FROM mobitem where id Not between 33 AND 52;

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (1, '12345678910111213', 23, 3, false, 1, 'Hot Laminated Frontlit Fabric roll 1', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (57, '12345678910111213', 23, 3, false, 1, 'Hot Laminated Frontlit Fabric roll 2', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (58, '12345678910111213', 23, 3, false, 1, 'Hot Laminated Frontlit Fabric roll 3', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (59, '12345678910111213', 16, 3, false, 1, 'Ink3', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (64, '12345678910111213', 23, 3, false, 1, 'Ink5', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (65, '12345678910111213', 16, 3, false, 1, 'Ink7', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (71, '12345678910111231', 23, 3, false, 1, 'Flex Primer Varnish E22', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (74, '12345678910111231', 23, null , false, 2, 'Insurance service', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (75, '12345678910111231', 23, null , false, 3, 'Laptop HP Pro-book core i5', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (190, '12345678910111231', 23, null , false, 2, 'Insurance service', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (191, '12345678910111231', 23, null , false, 2, 'Shipment service', 42);

INSERT INTO public.mobitem (id, olditemnumber, basicunitofmeasure, itemgroup, batchmanagementrequired, typeid, marketname, productmanagerid)
VALUES (192, '12345678910111231', 23, null , false, 2, 'Bank Fees', 42);