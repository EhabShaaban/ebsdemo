DELETE FROM iobalternativeuom where refinstanceid in (59,57,1,65,64);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (16, 59, '2018-08-02 06:59:26.514192', '2018-08-02 07:17:09.108553', 'hr1', 'hr1', '12345678910111239', 49, 2.5);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (17, 57, '2018-08-02 06:59:26.514192', '2018-08-02 07:17:09.108553', 'hr1', 'hr1', '12345678910111231', 41, 63.5);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (18, 1, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', '12345678910111225', 37, 110);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (27, 65, '2020-01-15 11:38:06.000000', '2020-01-15 11:38:07.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 47, 1.5);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (28, 59, '2020-01-15 11:38:57.000000', '2020-01-15 11:38:59.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 48, 2);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (29, 64, '2020-01-15 11:39:27.000000', '2020-01-15 11:39:30.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 37, 110);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (31, 64, '2020-03-04 10:19:37.000000', '2020-03-04 10:19:38.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 16, 1);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (32, 64, '2020-03-04 10:20:18.000000', '2020-03-04 10:20:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 46, 1);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (51, 190, '2020-03-04 10:20:18.000000', '2020-03-04 10:20:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 41, 1);

INSERT INTO public.iobalternativeuom (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, olditemnumber, alternativeunitofmeasureid, conversionfactor)
VALUES (52, 192, '2020-03-04 10:20:18.000000', '2020-03-04 10:20:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '12345678910111231', 41, 1);
