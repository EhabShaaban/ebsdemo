DELETE FROM IObItemAccountingInfo WHERE id BETWEEN 10 AND 14;

INSERT INTO  IObItemAccountingInfo (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, chartofaccountid, costFactor)
VALUES (10, 190, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', 25, true);

INSERT INTO  IObItemAccountingInfo (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, chartofaccountid, costFactor)
VALUES (11, 191, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', 25, true);

INSERT INTO  IObItemAccountingInfo (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, chartofaccountid, costFactor)
VALUES (12, 192, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', 25, true);

INSERT INTO  IObItemAccountingInfo (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, chartofaccountid, costFactor)
VALUES (13, 35, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', 25, false);

INSERT INTO  IObItemAccountingInfo (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, chartofaccountid, costFactor)
VALUES (14, 74, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', 25, true);
