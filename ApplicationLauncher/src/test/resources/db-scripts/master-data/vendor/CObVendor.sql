DELETE FROM CObVendor;

INSERT INTO CObVendor(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (1, 'COMMERCIAL', '{"en": "Commercial", "ar": "تجاري"}', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'A.Hamed', 'A.Hamed', '["Active"]');

INSERT INTO CObVendor(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (2, 'SERVICE', '{"en": "Service", "ar": "خدمات"}', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'A.Hamed', 'A.Hamed', '["Active"]');

INSERT INTO CObVendor(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (3, 'CONSUMABLE', '{"en": "Consumable", "ar": "إستخدامات"}', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'A.Hamed', 'A.Hamed', '["Active"]');

INSERT INTO CObVendor(id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (4, 'FIXED_ASSETS', '{"en": "Fixed Assets", "ar": "أصول ثابتة"}', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'A.Hamed', 'A.Hamed', '["Active"]');