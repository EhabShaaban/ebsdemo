DELETE FROM ItemVendorRecord;
DELETE FROM mobvendor;
DELETE FROM iobvendorpurchaseunit;

INSERT INTO mobbusinesspartner (id) VALUES (45);
INSERT INTO mobbusinesspartner (id) VALUES (46);
INSERT INTO mobbusinesspartner (id) VALUES (47);
INSERT INTO mobbusinesspartner (id) VALUES (48);
INSERT INTO mobbusinesspartner (id) VALUES (49);
INSERT INTO mobbusinesspartner (id) VALUES (50);
INSERT INTO mobbusinesspartner (id) VALUES (51);
INSERT INTO mobbusinesspartner (id) VALUES (53);
INSERT INTO mobbusinesspartner (id) VALUES (54);
INSERT INTO mobbusinesspartner (id) VALUES (55);
INSERT INTO mobbusinesspartner (id) VALUES (56);

INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (45,2,111111111191111,'1-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (46,3,727222222222222,'2-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (47,4,333333333333333,'3-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (48,5,444444444444444,'4-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (49,2,555555555555555,'5-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (50,2,666666666666666,'6-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (51,2,777777777777777,'7-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (53,2,777777777778777,'7-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (54,3,999999999999999,'9-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (55,2,101010101010101,'10-ABC-Madina',1);
INSERT INTO mobvendor (id,purchaseResponsibleId,oldvendornumber,accountWithVendor, typeId) VALUES (56,2,11011011011011,'11-ABC-Madina',1);


INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (1, 45, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1,'Flexo');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (2, 46, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19 ,'Signmedia');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (3, 47, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 20 , 'Offset');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (4, 48, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 21 , 'Digital');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (5, 49, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 22 , 'Textile');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (6, 50, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 22 , 'Textile');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (7, 51, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 23 , 'Corrugated');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (8, 53, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19, 'Signmedia');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (9, 53, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1 , 'Flexo');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (10, 54, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 19,'Signmedia');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (11, 55, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1 ,'Flexo');
INSERT INTO iobvendorpurchaseunit (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, purchaseUnitName) VALUES (12, 56, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 23 ,'Corrugated');

