
DELETE FROM masterdata WHERE objecttypecode = '2';

ALTER SEQUENCE masterdata_id_seq RESTART WITH 197;

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (2, '000051', '2018-08-02 07:25:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 6","ar":"فيندور رقم 6"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (45, '000001', '2018-08-02 07:51:00.000000', '2018-08-02 07:59:50.277000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Siegwerk","ar":"سيجورك"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (46, '000002', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang","ar":"زاجاننج"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (47, '000003', '2018-08-02 07:51:00.000000', '2018-08-02 08:27:17.481000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"ABC","ar":"أ بي سي"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (48, '000004', '2018-08-02 07:52:00.000000', '2018-08-02 08:34:15.768000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor Digital","ar":"فيندور ديجيتال"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (49, '000005', '2018-08-06 10:43:00.000000', '2018-08-06 10:47:54.666000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor Textile","ar":"فيندور تيكستيل"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (50, '000006', '2018-08-06 10:43:00.000000', '2018-08-06 10:47:54.666000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor Corrugated","ar":"فيندور كوريجيتيد"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (51, '000007', '2018-08-06 10:43:00.000000', '2018-08-06 10:47:54.666000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 1","ar":"فيندور 1"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (53, '000008', '2018-08-06 10:43:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 2","ar":"فيندور 2"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (54, '000009', '2018-08-02 07:25:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 3","ar":"فيندور 3"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (55, '000010', '2018-08-02 07:25:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 4","ar":"فيندور 4"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (56, '000011', '2018-08-02 07:25:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 5","ar":"فيندور 5"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (61, '000012', '2018-08-05 09:02:00.000000', '2018-08-02 07:25:14.553120', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Vendor 6","ar":"فيندور رقم 6"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (62, '000052', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang","ar":"زاجاننج"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (63, '000053', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang","ar":"زاجاننج"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (76, '000013', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang 4","ar":"4 زاجاننج"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (77, '000054', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang 5","ar":"5 زاجاننج"}');

INSERT INTO public.masterdata (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (78, '000055', '2018-08-02 07:51:00.000000', '2018-08-02 08:20:58.390000', 'hr1', 'hr1', '2', '["Active"]', '{"en":"Zhejiang 6","ar":"6 زاجاننج"}');