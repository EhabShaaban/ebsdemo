DELETE from EntityUserCode WHERE type = 'CObMeasure';
INSERT INTO EntityUserCode (type, code) values ('CObMeasure', '0001');

TRUNCATE cobmeasure CASCADE;

ALTER SEQUENCE cobmeasure_id_seq RESTART WITH 1;
