DELETE FROM cobtreasury;

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (1, '{"en":"Flexo Treasury","ar":"خزنة فليكسو"}',  '0001', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (2, '{"en":"Signmedia Treasury","ar":"خزنة ساين ميديا"}',  '0002', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (3, '{"en":"Offset Treasury","ar":"خزنة اوفسيت"}',  '0003', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (4, '{"en":"Digital Treasury","ar":"خزنة ديجيتال"}',  '0004', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (5, '{"en":"Textile Treasury","ar":"خزنة تيكستايل"}',  '0005', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (6, '{"en":"Corrugated Treasury","ar":"خزنة كوروجيتيد"}',  '0006', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');

INSERT INTO cobtreasury (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (7, '{"en":"IVD Treasury","ar":"خزنة IVD"}',  '0007', '2020-10-20 06:31:56.325716', '2020-10-20 06:31:56.325716', 'Admin', 'Admin');
