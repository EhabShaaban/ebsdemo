DELETE FROM iobsalesinvoiceitem WHERE id BETWEEN 31 AND 39;
ALTER sequence iobsalesinvoiceitem_id_seq RESTART WITH 40;

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderUnitOfMeasureId, quantityinorderunit, price)
VALUES (31, 70, '2020-03-04 11:31:52.000000', '2020-03-04 11:31:53.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, 41, 50, 0.15748031496062992);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderUnitOfMeasureId, quantityinorderunit, price)
VALUES (32, 1901, '2020-03-04 11:31:52.000000', '2020-03-04 11:31:53.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, 41, 50, 635);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid, returnedquantity)
VALUES (33, 2007, '2020-07-27 08:52:49.413000', '2020-07-27 08:52:49.413000', 'hr1', 'hr1', 57, 41, 2, 0.15748031496062992, 23, 2);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (34, 2009, '2020-07-27 13:13:40.303000', '2020-07-27 13:13:40.303000', 'hr1', 'hr1', 190, 23, 2, 1, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (35, 2009, '2020-07-27 13:13:40.304000', '2020-07-27 13:13:40.304000', 'hr1', 'hr1', 75, 23, 1, 1, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (36, 2009, '2020-07-27 13:13:40.306000', '2020-07-27 13:13:40.306000', 'hr1', 'hr1', 57, 41, 99, 2, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (37, 2009, '2020-07-27 13:13:40.307000', '2020-07-27 13:13:40.307000', 'hr1', 'hr1', 64, 16, 2, 2, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (38, 2013, '2020-08-06 14:59:20.600000', '2020-08-06 14:59:20.600000', 'hr1', 'hr1', 64, 16, 1, 40, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (39, 2013, '2020-08-06 14:59:20.610000', '2020-08-06 14:59:20.610000', 'hr1', 'hr1', 64, 46, 1, 30, 23);

