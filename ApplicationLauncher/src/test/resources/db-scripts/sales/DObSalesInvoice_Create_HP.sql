DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND id = 123;
DELETE FROM iobsalesinvoiceitem WHERE id > 23 and id < 28;
DELETE FROM iobsalesinvoicetaxes WHERE id > 104 and id < 111;
delete from iobsalesinvoicebusinesspartner where id = 26;
delete from iobsalesinvoicecustomerbusinesspartner where id = 26;

DELETE FROM dobsalesinvoice WHERE id > 2008;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'SalesInvoice' AND id > 2008;

DELETE from EntityUserCode WHERE TYPE = 'DObSalesInvoice';

INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObSalesInvoice', '2020000004');


INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2009, '2020000004', '2020-07-27 15:13:40.299000', '2020-07-27 15:14:35.938000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2009, 2);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (123, 2009, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (24, 2009, '2020-07-27 13:13:40.303000', '2020-07-27 13:13:40.303000', 'hr1', 'hr1', 190, 23, 2, 1, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (25, 2009, '2020-07-27 13:13:40.304000', '2020-07-27 13:13:40.304000', 'hr1', 'hr1', 75, 23, 1, 1, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (26, 2009, '2020-07-27 13:13:40.306000', '2020-07-27 13:13:40.306000', 'hr1', 'hr1', 57, 41, 5, 0.031496062992125984, 23);
INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (27, 2009, '2020-07-27 13:13:40.307000', '2020-07-27 13:13:40.307000', 'hr1', 'hr1', 64, 16, 2, 2, 23);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, paymenttermid, currencyid, downpayment, salesorderid)
VALUES (26, 2009, '2020-07-27 15:13:40.318000', '2020-07-27 15:13:40.318000', 'hr1', 'hr1', 'CUSTOMER', 5, 1, null, 81);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (26, 73);

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (105, 2009, '2020-07-27 15:13:40.309000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 1, 0.17, 0007 ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (106, 2009, '2020-07-27 15:13:40.312000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 0.5, 0.085,  0008, '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (107, 2009, '2020-07-27 15:13:40.313000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 10, 1.7,  0009,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (108, 2009, '2020-07-27 15:13:40.314000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 12, 2.04,  0010,'{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (109, 2009, '2020-07-27 15:13:40.316000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 0.5, 0.085, 0011, '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount, code, name) VALUES (110, 2009, '2020-07-27 15:13:40.317000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 12, 2.04,  0012,'{"ar": "ضريبة مبيعات", "en": "Sales tax"}');