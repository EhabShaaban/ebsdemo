DELETE
FROM iobsalesinvoiceitem
where id >= 1
  and id < 31;
DELETE
FROM iobsalesinvoiceitem
where id in (20, 28, 29, 30, 33, 40, 41);
ALTER sequence iobsalesinvoiceitem_id_seq RESTART WITH 42;

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (1, 51, '2019-11-24 12:13:34.000000', '2019-11-24 12:13:38.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 4, 100, 999, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (2, 52, '2019-11-11 14:01:36.000000', '2019-11-11 14:01:37.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 40, 1, 136, 22, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (3, 55, '2019-11-14 08:40:33.000000', '2019-11-14 08:40:34.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 34, 37,
        100, 330, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (4, 55, '2019-01-01 11:00:00.000000', '2019-01-01 11:00:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 34, 37,
        200, 440, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid,
                                        returnedquantity)
VALUES (33, 58, '2020-08-13 10:14:23.822000', '2020-08-13 10:14:23.873000', 'hr1', 'hr1', 64, 16, 10, 5, 23, null);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (5, 59, '2019-11-11 14:01:36.000000', '2019-11-11 14:01:37.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 40, 1, 136, 22, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (6, 61, '2019-12-09 11:00:22.000000', '2019-12-09 11:00:23.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 4, 100, 100, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (7, 60, '2019-11-11 14:01:36.000000', '2019-11-11 14:01:37.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 40, 1, 136, 22, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (8, 62, '2019-11-24 12:13:34.000000', '2019-11-24 12:13:38.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 4, 10, 10, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (9, 51, '2020-02-17 15:24:45.000000', '2020-02-17 15:24:47.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 40, 4, 100, 999, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (10, 51, '2020-02-17 15:28:03.000000', '2020-02-17 15:28:05.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 41, 100, 999, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (11, 51, '2020-02-18 10:33:06.000000', '2020-02-18 10:33:08.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 33, 4, 12, 10000, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (12, 63, '2020-02-18 10:33:06.000000', '2020-02-18 10:33:08.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 4, 100, 100, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, price, baseUnitOfMeasureId)
VALUES (13, 64, '2020-02-18 10:33:06.000000', '2020-02-18 10:33:08.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 38, 4, 100, 100, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, returnedquantity, price,
                                        baseUnitOfMeasureId)
VALUES (14, 65, '2020-03-02 11:40:12.000000', '2020-03-02 11:40:13.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 57, 41, 15, 5, 150, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, returnedquantity, price,
                                        baseUnitOfMeasureId)
VALUES (15, 66, '2020-03-02 11:41:16.000000', '2020-03-02 11:41:17.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 57, 41, 30, 30, 200, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, returnedquantity, price,
                                        baseUnitOfMeasureId)
VALUES (16, 67, '2020-03-03 14:41:10.000000', '2020-03-03 14:41:11.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 64, 16, 50, 10, 130, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, returnedquantity, price,
                                        baseUnitOfMeasureId)
VALUES (17, 68, '2020-03-09 12:55:02.000000', '2020-03-09 12:55:05.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 64, 37, 10, 10, 200, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderUnitOfMeasureId, quantityinorderunit, returnedquantity, price,
                                        baseUnitOfMeasureId)
VALUES (18, 69, '2020-03-10 15:25:57.000000', '2020-03-10 15:25:58.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 57, 23, 20, 2, 200, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (20, 1904, '2020-06-29 08:52:15.404000', '2020-06-29 08:52:15.412000', 'hr1', 'hr1', 57, 41, 20, 15, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (28, 2010, '2020-08-06 08:08:27.517000', '2020-08-06 08:08:27.517000', 'hr1', 'hr1', 57, 23, 10, 40, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (29, 2011, '2020-08-06 08:11:54.032000', '2020-08-06 08:11:54.032000', 'hr1', 'hr1', 57, 23, 10, 25, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid)
VALUES (30, 2012, '2020-08-06 08:13:14.921000', '2020-08-06 08:13:14.921000', 'hr1', 'hr1', 57, 23, 10, 33, 23);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid,
                                        returnedquantity)
VALUES (40, 2014, '2020-08-30 09:58:15.532000', '2020-08-30 10:02:08.672000', 'hr1', 'hr1', 1, 37, 50,
        65, 23, 1);

INSERT INTO public.iobsalesinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, orderunitofmeasureid, quantityinorderunit, price, baseunitofmeasureid,
                                        returnedquantity)
VALUES (41, 2014, '2020-08-30 09:58:15.543000', '2020-08-30 10:02:08.661000', 'hr1', 'hr1', 71, 23, 50, 95, 23, 50);
