DELETE FROM IObAccountingDocumentActivationDetails where objecttypecode = 'SalesInvoice' AND id >9 AND id < 28;
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 28;

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)

VALUES (10, 51, '2019-10-28 09:01:00.000000', '2019-10-28 09:01:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate , objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (11, 57, '2019-10-28 09:01:00.000000', '2019-10-28 09:01:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (12, 61, '2019-10-28 09:01:00.000000', '2019-10-28 09:01:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (13, 62, '2019-10-28 09:30:00.000000', '2019-10-28 09:30:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (14, 63, '2019-10-28 09:30:00.000000', '2019-10-28 09:30:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (15, 64, '2019-10-28 09:30:00.000000', '2019-10-28 09:30:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-09-20 10:30:00', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (16, 65, '2020-03-07 11:20:45.000000', '2020-03-07 11:20:46.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2020-03-09 11:20:47.000000', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (17, 66, '2020-03-08 11:21:00.000000', '2020-03-08 11:21:01.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2020-03-09 11:21:03.000000', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (18, 67, '2020-03-09 11:21:15.000000', '2020-03-09 11:21:16.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2020-03-09 11:21:27.000000', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (19, 68, '2020-03-09 12:53:37.000000', '2020-03-09 12:53:39.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2020-03-09 12:54:54.000000', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode, accountant, activationDate , exchangerateid, currencyprice)
VALUES (20, 69, '2020-03-09 15:26:46.000000', '2020-03-09 15:26:48.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2020-03-10 15:26:50.000000', 'SalesInvoice', 'Admin from BDKCompanyCode', '2019-09-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (21, 1904, '2020-06-29 08:54:36.973000', '2020-06-29 08:54:36.979000', 'hr1', 'hr1', 'hr1', '2020-06-29 08:54:36.973000', '2020-06-29 00:00:00.000000',  13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (22, 2007, '2020-07-27 08:53:13.077000', '2020-07-27 08:53:13.091000', 'hr1', 'hr1', 'hr1', '2020-07-27 08:53:13.077000', '2020-07-26 22:00:00.000000', 13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (23, 2009, '2020-07-27 13:14:35.938000', '2020-07-27 13:14:35.944000', 'hr1', 'hr1', 'hr1', '2020-07-27 13:14:35.938000', '2020-07-26 22:00:00.000000', 13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (24, 2010, '2020-08-06 08:08:36.057000', '2020-08-06 08:08:36.064000', 'hr1', 'hr1', 'hr1', '2020-08-06 08:08:36.058000', '2020-08-26 00:00:00.000000', 13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (25, 2011, '2020-08-06 08:12:17.552000', '2020-08-06 08:12:17.557000', 'hr1', 'hr1', 'hr1', '2020-08-06 08:12:17.552000', '2020-08-31 00:00:00.000000', 13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (26, 2013, '2020-08-06 15:00:01.376000', '2020-08-06 15:00:01.382000', 'hr1', 'hr1', 'hr1', '2020-08-06 15:00:01.376000', '2020-09-01 00:00:00.000000', 13, 1, 'SalesInvoice');

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (27, 2014, '2020-08-30 09:58:44.893000', '2020-08-30 09:58:44.900000', 'hr1', 'hr1', 'hr1', '2020-08-30 09:58:44.893000', '2020-12-30 02:00:00.000000', 13, 1, 'SalesInvoice');