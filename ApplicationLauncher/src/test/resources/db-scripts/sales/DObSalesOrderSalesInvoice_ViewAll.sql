delete from dobaccountingdocument where id IN (1901, 1902);
delete from dobsalesinvoice where id IN (1901, 1902);
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND id >= 150 AND id <= 151;

DELETE FROM iobsalesinvoicetaxes WHERE id > 110 and id < 117;

DELETE FROM iobsalesinvoicesummary where refinstanceid in (1901,1902);

alter sequence iobsalesinvoicetaxes_id_seq RESTART with 117;


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
                                  modificationinfo, currentstates, objectTypeCode, documentownerid) values (1901, '2019100015', '2019-11-03 09:50:00.000000',
                                                                                           '2019-11-03 09:50:00.000000', 'Gehan.Ahmed',
                                                                                           'Gehan.Ahmed','["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (1901, 2);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (150, 1901, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
                                  modificationinfo, currentstates, objectTypeCode, documentownerid) values (1902, '2019100016', '2019-11-03 09:55:00.000000',
                                                                                           '2019-11-03 09:55:00.000000', 'Gehan.Ahmed', 'Gehan.Ahmed',
                                                                                           '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (1902, 2);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (151, 1902, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (111, 1901, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 317.5, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (112, 1901, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 158.75, 0.5 , '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (113, 1901, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 3175, 10 , '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (114, 1902, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 0, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (115, 1902, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 0, 0.5 , '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo,amount, percentage, code, name) VALUES (116, 1902, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Gehan.Ahmed', 'Gehan.Ahmed', 0, 10 , '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicesummary (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, remaining) VALUES (29, 1901, '2021-03-21 16:59:12.239000', '2021-03-21 16:59:12.239000', 'hr1', 'hr1', 35396.25);
INSERT INTO public.iobsalesinvoicesummary (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, remaining) VALUES (30, 1902, '2021-03-21 16:59:12.239000', '2021-03-21 16:59:12.239000', 'hr1', 'hr1', -5);