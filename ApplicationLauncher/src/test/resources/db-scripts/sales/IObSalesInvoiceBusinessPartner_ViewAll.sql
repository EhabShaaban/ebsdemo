delete from iobsalesinvoicebusinesspartner where id <5;
delete from iobsalesinvoicecustomerbusinesspartner where id <5;
ALTER sequence iobsalesinvoicebusinesspartner_id_seq RESTART WITH 5;
ALTER sequence iobsalesinvoicecustomerbusinesspartner_id_seq RESTART WITH 5;

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (1, 51, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', null, 6, 1, 10030);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (2, 52, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', null, 6, 1, 11.25);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (3, 53, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', null, 6, 1, 10000);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (4, 54, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', null, 6, 1, 20000);

INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (1, 66);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (2, 67);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (3, 68);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (4, 66);
