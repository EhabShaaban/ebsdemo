delete from iobsalesinvoicebusinesspartner where id in(20,21);
delete from iobsalesinvoicecustomerbusinesspartner where id in(20,21);
ALTER sequence iobsalesinvoicebusinesspartner_id_seq RESTART WITH 22;
ALTER sequence iobsalesinvoicecustomerbusinesspartner_id_seq RESTART WITH 22;

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (20, 1901, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', 67, 6, 1, 5);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (21, 1902, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', 53, 6, 1, 5);


INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (20, 73);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (21, 72);

