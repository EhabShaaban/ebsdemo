DELETE FROM cobsalesinvoice;
ALTER SEQUENCE cobsalesinvoice_id_seq RESTART WITH 3;

INSERT INTO public.cobsalesinvoice (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name)
VALUES (1, 'SALES_INVOICE_WITHOUT_REFERENCE', '2019-10-28 09:01:25.000000', '2019-10-28 09:01:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"ar": "فاتورة عميل بدون امر بيع", "en": "Sales invoice without reference"}');

INSERT INTO public.cobsalesinvoice (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name)
VALUES (2, 'SALES_INVOICE_FOR_SALES_ORDER', '2019-10-28 09:01:25.000000', '2019-10-28 09:01:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"ar": "فاتورة عميل بناء على امر بيع", "en": "Sales invoice for sales order"}');
