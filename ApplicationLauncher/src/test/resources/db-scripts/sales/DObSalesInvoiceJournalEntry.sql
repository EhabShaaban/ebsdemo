delete from dobjournalentry where id in (14,16,17,18,19,20,21,22,23);
delete from dobsalesinvoicejournalentry where id in (14,16,17,18, 19, 20, 21, 22, 23);
delete from iobjournalentryitem where refinstanceid between 14 and 20;
delete from iobjournalentryitem where refinstanceid = 23;

DELETE
from EntityUserCode
WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000020');

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (14, '2020000001', '2020-07-27 14:01:43.348000', '2020-07-27 14:01:43.348000', 'hr1', 'hr1', '["Active"]', null, '2020-07-27 00:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (14, 2007);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (16, '2020000002', '2020-07-27 15:26:36.196000', '2020-07-27 15:26:36.196000', 'hr1', 'hr1', '["Active"]', null, '2020-07-27 00:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (16, 2009);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (17, '2020000003', '2020-08-06 08:08:36.809000', '2020-08-06 08:08:36.809000', 'hr1', 'hr1', '["Active"]', null, '2020-08-26 00:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (17, 2010);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (18, '2020000004', '2020-08-06 08:12:18.213000', '2020-08-06 08:12:18.213000', 'hr1', 'hr1', '["Active"]', null, '2020-08-31 00:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (18, 2011);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (19, '2020000005', '2020-08-06 15:00:00', '2020-08-06 15:00:00', 'hr1', 'hr1', '["Active"]', null, '2020-09-01 00:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (19, 2013);


INSERT INTO DObJournalEntry(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentStates, dueDate, companyId, purchaseUnitId, objectTypeCode)
VALUES (21, '2020000018', '2019-02-16 10:30:00', '2019-04-03 10:30:00','hr1', 'hr1', '[Active]','2019-09-02 10:30:00', 10, 1, 'SalesInvoice');
INSERT INTO DObSalesInvoiceJournalEntry(id, documentReferenceId)
VALUES (21, 63);

INSERT INTO DObJournalEntry(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentStates, dueDate, companyId, purchaseUnitId, objectTypeCode)
VALUES (22, '2020000019', '2019-02-16 10:30:00', '2019-04-03 10:30:00','hr1', 'hr1', '[Active]','2019-09-02 10:30:00', 10, 19, 'SalesInvoice');
INSERT INTO DObSalesInvoiceJournalEntry(id, documentReferenceId)
VALUES (22, 62);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (23, '2020000020', '2020-08-30 09:58:52.419000', '2020-08-30 09:58:52.419000', 'hr1', 'hr1', '["Active"]', null, '2020-12-30 02:00:00.000000', 7, 19, 'SalesInvoice');
INSERT INTO public.dobsalesinvoicejournalentry (id, documentreferenceid)
VALUES (23, 2014);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (24, 14, '2020-07-27 13:10:54.615000', '2020-07-27 13:10:54.615000', 'hr1', 'hr1', 42, 'NONE', 500, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (25, 14, '2020-07-27 13:10:54.617000', '2020-07-27 13:10:54.617000', 'hr1', 'hr1', 43, 'Taxes', 5, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (26, 14, '2020-07-27 13:10:54.619000', '2020-07-27 13:10:54.619000', 'hr1', 'hr1', 43, 'Taxes', 2.5, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (27, 14, '2020-07-27 13:10:54.622000', '2020-07-27 13:10:54.622000', 'hr1', 'hr1', 43, 'Taxes', 50, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (28, 14, '2020-07-27 13:10:54.624000', '2020-07-27 13:10:54.624000', 'hr1', 'hr1', 43, 'Taxes', 60, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (29, 14, '2020-07-27 13:10:54.627000', '2020-07-27 13:10:54.627000', 'hr1', 'hr1', 43, 'Taxes', 2.5, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (30, 14, '2020-07-27 13:10:54.630000', '2020-07-27 13:10:54.630000', 'hr1', 'hr1', 43, 'Taxes', 60, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (32, 16, '2020-07-27 15:26:36.203000', '2020-07-27 15:26:36.203000', 'hr1', 'hr1', 11, 'Customers', 0, 23.12, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (33, 16, '2020-07-27 15:26:36.207000', '2020-07-27 15:26:36.207000', 'hr1', 'hr1', 42, 'NONE', 17, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (34, 16, '2020-07-27 15:26:36.209000', '2020-07-27 15:26:36.209000', 'hr1', 'hr1', 43, 'Taxes', 0.17, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (35, 16, '2020-07-27 15:26:36.212000', '2020-07-27 15:26:36.212000', 'hr1', 'hr1', 43, 'Taxes', 0.085, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (36, 16, '2020-07-27 15:26:36.214000', '2020-07-27 15:26:36.214000', 'hr1', 'hr1', 43, 'Taxes', 1.7, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (37, 16, '2020-07-27 15:26:36.216000', '2020-07-27 15:26:36.216000', 'hr1', 'hr1', 43, 'Taxes', 2.04, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (38, 16, '2020-07-27 15:26:36.218000', '2020-07-27 15:26:36.218000', 'hr1', 'hr1', 43, 'Taxes', 0.085, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (39, 16, '2020-07-27 15:26:36.219000', '2020-07-27 15:26:36.219000', 'hr1', 'hr1', 43, 'Taxes', 2.04, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (40, 17, '2020-08-06 08:08:36.828000', '2020-08-06 08:08:36.828000', 'hr1', 'hr1', 11, 'Customers', 0, 544, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (41, 17, '2020-08-06 08:08:36.845000', '2020-08-06 08:08:36.845000', 'hr1', 'hr1', 42, 'NONE', 400, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (42, 17, '2020-08-06 08:08:36.854000', '2020-08-06 08:08:36.854000', 'hr1', 'hr1', 43, 'Taxes', 4, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (43, 17, '2020-08-06 08:08:36.865000', '2020-08-06 08:08:36.865000', 'hr1', 'hr1', 43, 'Taxes', 2, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (44, 17, '2020-08-06 08:08:36.868000', '2020-08-06 08:08:36.868000', 'hr1', 'hr1', 43, 'Taxes', 40, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (45, 17, '2020-08-06 08:08:36.870000', '2020-08-06 08:08:36.870000', 'hr1', 'hr1', 43, 'Taxes', 48, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (46, 17, '2020-08-06 08:08:36.873000', '2020-08-06 08:08:36.873000', 'hr1', 'hr1', 43, 'Taxes', 2, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (47, 17, '2020-08-06 08:08:36.875000', '2020-08-06 08:08:36.875000', 'hr1', 'hr1', 43, 'Taxes', 48, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (48, 18, '2020-08-06 08:12:19.014000', '2020-08-06 08:12:19.014000', 'hr1', 'hr1', 11, 'Customers', 0, 340, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (49, 18, '2020-08-06 08:12:19.017000', '2020-08-06 08:12:19.017000', 'hr1', 'hr1', 42, 'NONE', 250, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (50, 18, '2020-08-06 08:12:19.019000', '2020-08-06 08:12:19.019000', 'hr1', 'hr1', 43, 'Taxes', 1.25, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (51, 18, '2020-08-06 08:12:19.021000', '2020-08-06 08:12:19.021000', 'hr1', 'hr1', 43, 'Taxes', 25, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (52, 18, '2020-08-06 08:12:19.023000', '2020-08-06 08:12:19.023000', 'hr1', 'hr1', 43, 'Taxes', 30, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (53, 18, '2020-08-06 08:12:19.025000', '2020-08-06 08:12:19.025000', 'hr1', 'hr1', 43, 'Taxes', 2.5, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (54, 18, '2020-08-06 08:12:19.027000', '2020-08-06 08:12:19.027000', 'hr1', 'hr1', 43, 'Taxes', 1.25, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (55, 18, '2020-08-06 08:12:19.029000', '2020-08-06 08:12:19.029000', 'hr1', 'hr1', 43, 'Taxes', 30, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (56, 19, '2020-08-06 15:00:07.766000', '2020-08-06 15:00:07.766000', 'hr1', 'hr1', 11, 'Customers', 0, 95.2, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (57, 19, '2020-08-06 15:00:07.782000', '2020-08-06 15:00:07.782000', 'hr1', 'hr1', 42, 'NONE', 70, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (58, 19, '2020-08-06 15:00:07.797000', '2020-08-06 15:00:07.797000', 'hr1', 'hr1', 43, 'Taxes', 0.7, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (59, 19, '2020-08-06 15:00:07.810000', '2020-08-06 15:00:07.810000', 'hr1', 'hr1', 43, 'Taxes', 0.35, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (60, 19, '2020-08-06 15:00:07.812000', '2020-08-06 15:00:07.812000', 'hr1', 'hr1', 43, 'Taxes', 7, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (61, 19, '2020-08-06 15:00:07.815000', '2020-08-06 15:00:07.815000', 'hr1', 'hr1', 43, 'Taxes', 8.4, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (62, 19, '2020-08-06 15:00:07.818000', '2020-08-06 15:00:07.818000', 'hr1', 'hr1', 43, 'Taxes', 0.35, 0, 1, 1, 1, 13);
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (63, 19, '2020-08-06 15:00:07.820000', '2020-08-06 15:00:07.820000', 'hr1', 'hr1', 43, 'Taxes', 8.4, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (72, 23, '2020-08-30 09:58:52.440000', '2020-08-30 09:58:52.440000', 'hr1', 'hr1', 11, 'Customers', 0, 10880, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (73, 23, '2020-08-30 09:58:52.459000', '2020-08-30 09:58:52.459000', 'hr1', 'hr1', 42, 'NONE', 8000, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (74, 23, '2020-08-30 09:58:52.469000', '2020-08-30 09:58:52.469000', 'hr1', 'hr1', 43, 'Taxes', 80, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (75, 23, '2020-08-30 09:58:52.481000', '2020-08-30 09:58:52.481000', 'hr1', 'hr1', 43, 'Taxes', 40, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (76, 23, '2020-08-30 09:58:52.482000', '2020-08-30 09:58:52.482000', 'hr1', 'hr1', 43, 'Taxes', 800, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (77, 23, '2020-08-30 09:58:52.484000', '2020-08-30 09:58:52.484000', 'hr1', 'hr1', 43, 'Taxes', 960, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (78, 23, '2020-08-30 09:58:52.486000', '2020-08-30 09:58:52.486000', 'hr1', 'hr1', 43, 'Taxes', 40, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (79, 23, '2020-08-30 09:58:52.488000', '2020-08-30 09:58:52.488000', 'hr1', 'hr1', 43, 'Taxes', 960, 0, 1, 1, 1, 13);
