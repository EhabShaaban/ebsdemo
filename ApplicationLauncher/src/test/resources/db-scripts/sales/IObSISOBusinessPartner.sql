delete from iobsalesinvoicebusinesspartner where id in (22,30);
delete from iobsalesinvoicecustomerbusinesspartner where id in (22,30);
ALTER sequence iobsalesinvoicebusinesspartner_id_seq RESTART WITH 31;
ALTER sequence iobsalesinvoicecustomerbusinesspartner_id_seq RESTART WITH 31;

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, salesorderid, paymenttermid, currencyid, downpayment)
VALUES (22, 70, '2019-10-28 08:58:58.000000', '2019-10-28 08:58:59.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', 'CUSTOMER', 67, 5, 1, 100);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, paymenttermid, currencyid, downpayment, salesorderid)
VALUES (24, 2007, '2020-07-27 10:52:49.353000', '2020-07-27 10:52:49.353000', 'hr1', 'hr1', 'CUSTOMER', 5, 1, null, 67);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, paymenttermid, currencyid, downpayment, salesorderid)
VALUES (26, 2009, '2020-07-27 15:13:40.318000', '2020-07-27 15:13:40.318000', 'hr1', 'hr1', 'CUSTOMER', 5, 1, null, 81);

INSERT INTO public.iobsalesinvoicebusinesspartner (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, type, paymenttermid, currencyid, downpayment, salesorderid)
VALUES (30, 2013, '2020-08-06 14:59:20.611000', '2020-08-06 14:59:20.611000', 'hr1', 'hr1', 'CUSTOMER', 5, 1, null, 82);

INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (22, 73);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (24, 73);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (26, 73);
INSERT INTO public.iobsalesinvoicecustomerbusinesspartner (id, customerid) VALUES (30, 73);
