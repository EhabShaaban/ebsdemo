DELETE FROM dobsalesinvoice where id > 54 AND id < 71;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'SalesInvoice' AND id > 54 AND id < 71;
DELETE FROM dobsalesinvoice WHERE id > 1903;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'SalesInvoice' AND id > 1903;
alter sequence dobsalesinvoice_id_seq RESTART with 2015;

DELETE FROM iobsalesinvoicetaxes WHERE id > 16 and id < 97;
DELETE FROM iobsalesinvoicetaxes WHERE refinstanceid > 2006;
alter sequence iobsalesinvoicetaxes_id_seq RESTART with 141;

DELETE from EntityUserCode WHERE TYPE = 'DObSalesInvoice';

DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND id >= 105 AND id <= 123;
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND id >= 152 AND id <= 155;
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND refinstanceid = 2014;

INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObSalesInvoice', '2020000009');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (55, '2019000005', '2019-11-03 09:40:00.000000', '2019-11-03 09:40:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (55, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (56, '2019100001', '2019-11-10 10:42:49.000000', '2019-11-10 10:42:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (56, 1);


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (57, '2019100002', '2019-11-10 10:44:44.000000', '2019-11-10 10:44:45.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (57, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (58, '2019100003', '2019-11-10 10:45:43.000000', '2019-11-10 10:45:45.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'SalesInvoice', 18);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (58, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (59, '2019100004', '2019-12-01 14:20:49.000000', '2019-12-01 14:20:51.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened", "ReadyForDelivering"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (59, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (60, '2019100005', '2019-12-01 14:20:49.000000', '2019-12-01 14:20:51.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened", "ReadyForDelivering"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (60, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (61, '2019100006', '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 18);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (61, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (62, '2019100007', '2019-12-28 08:58:00.000000', '2019-12-28 08:58:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (62, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (63, '2019100008', '2019-12-28 08:58:00.000000', '2019-12-28 08:58:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened", "Active"]', 'SalesInvoice', 18);

INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (63, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (64, '2019100009', '2019-12-28 08:58:00.000000', '2019-12-28 08:58:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (64, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (65, '2019100010', '2020-03-02 10:35:14.000000', '2020-03-02 10:35:20.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (65, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (66, '2019100011', '2020-03-02 10:37:03.000000', '2020-03-02 10:37:05.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (66, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
modificationinfo, currentstates, objectTypeCode, documentownerid) values (67, '2019100012', '2020-03-02 10:40:02.000000', '2020-03-02 10:40:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 18);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (67, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
modificationinfo, currentstates, objectTypeCode, documentownerid) values (68, '2019100013', '2020-03-09 12:51:20.000000', '2020-03-09 12:51:22.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 18);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (68, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
modificationinfo, currentstates, objectTypeCode, documentownerid) values (69, '2019100014', '2020-03-10 15:23:22.000000', '2020-03-10 15:23:23.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Opened", "Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (69, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
modificationinfo, currentstates, objectTypeCode, documentownerid) values (70, '2020000002', '2019-11-03 09:40:00.000000', '2019-11-03 09:40:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (70, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1904, '2020000001', '2020-06-29 08:51:20.978000', '2020-06-29 08:54:36.973000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (1904, 1);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2007, '2020000003', '2020-07-27 10:52:49.347000', '2020-07-27 10:53:13.078000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2007, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2009, '2020000004', '2020-07-27 15:13:40.299000', '2020-07-27 15:14:35.938000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2009, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2010, '2020000005', '2020-08-06 08:08:27.392000', '2020-08-06 08:08:36.058000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2010, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2011, '2020000006', '2020-08-06 08:11:54.016000', '2020-08-06 08:12:17.552000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2011, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2012, '2020000007', '2020-08-06 08:13:14.900000', '2020-08-06 08:13:14.900000', 'hr1', 'hr1', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2012, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2013, '2020000008', '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2013, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2014, '2020000009', '2020-08-30 09:58:12.645000', '2020-08-30 09:58:44.894000', 'hr1', 'hr1', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (2014, 2);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode)
VALUES (105, 55, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (106, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (107, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (108, 58, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1, 10, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (109, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (110, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (111, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1 , 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (112, 62, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (113, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1 , 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (114, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (115, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (116, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (117, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (118, 68, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1, 10, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (119, 69, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (120, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (121, 1904, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 11, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (122, 2007, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (123, 2009, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (152, 2010, '2020-08-06 08:08:27.410000', '2020-08-06 08:08:27.410000', 'hr1', 'hr1', 19, 7, null, 'SalesInvoice');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (153, 2011, '2020-08-06 08:11:54.019000', '2020-08-06 08:11:54.019000', 'hr1', 'hr1', 19, 7, null, 'SalesInvoice');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (154, 2012, '2020-08-06 08:13:14.903000', '2020-08-06 08:13:14.903000', 'hr1', 'hr1', 19, 7, null, 'SalesInvoice');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (155, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1', 19, 7, null, 'SalesInvoice');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (2006, 2014, '2020-08-30 09:58:15.494000', '2020-08-30 09:58:15.494000', 'hr1', 'hr1', 19, 7, null, 'SalesInvoice');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (16, 55, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1210, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (17, 55, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 605, 0.5, '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (18, 55, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 12100, 10, '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (19, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (20, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (21, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (22, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (23, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (24, 56, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (25, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (26, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (27, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (28, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (29, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (30, 57, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (31, 58, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (32, 58, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 0.5, '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (33, 58, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0, 10, '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (34, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (35, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',14.96,0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (36, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (37, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',299.2, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (38, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',14.96, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (39, 59, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',29.92, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (40, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (41, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',14.96, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (42, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (43, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 299.2, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (44, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 14.96, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (45, 60, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 29.92, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (46, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (47, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',50, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (48, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (49, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1000, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (50, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 50, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (51, 61, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 100, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (52, 62, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',10, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (53, 62, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',0.5, 0.5, '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (54, 62, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1, 10, '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (55, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (56, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',50, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (57, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (58, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1000, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (59, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 50, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (60, 63, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 100, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (61, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (62, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',50, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (63, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',1200, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (64, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1000, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (65, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 50, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (66, 64, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 100, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (67, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',270, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (68, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',11.25, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (69, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',270, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (70, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 225, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (71, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 11.25, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (72, 65, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 22.5, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (73, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',720, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (74, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',30, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (75, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',720, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (76, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 600, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (77, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 30, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (78, 66, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 60, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (79, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',780, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (80, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',32.5, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (81, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',780, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (82, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 650, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (83, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 32.5, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (84, 67, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 65, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');


INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (85, 68, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 200, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (86, 68, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 10, 0.5, '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (87, 68, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 20, 10, '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (88, 69, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 400, 1, '0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (89, 69, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 20, 0.5, '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (90, 69, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 40, 10, '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (91, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',5, 1, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (92, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',2.50, 0.5, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (93, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',50, 10, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (94, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 60, 12, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (95, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2.50, 0.5, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (96, 70, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 60, 12, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (103, 2007, '2020-07-27 10:52:49.404000', '2020-07-27 10:52:49.543000', 'hr1', 'hr1', 12, 60, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (105, 2009, '2020-07-27 15:13:40.309000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1',1, 0.17, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (106, 2009, '2020-07-27 15:13:40.312000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1',0.5, 0.085, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (107, 2009, '2020-07-27 15:13:40.313000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1',10, 1.7, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (108, 2009, '2020-07-27 15:13:40.314000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 12, 2.04, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (109, 2009, '2020-07-27 15:13:40.316000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 0.5, 0.085, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (110, 2009, '2020-07-27 15:13:40.317000', '2020-07-27 15:13:40.346000', 'hr1', 'hr1', 12, 2.04, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (117, 2010, '2020-08-06 08:08:27.432000', '2020-08-06 08:08:27.573000', 'hr1', 'hr1',1, 4, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (118, 2010, '2020-08-06 08:08:27.504000', '2020-08-06 08:08:27.574000', 'hr1', 'hr1',0.5, 2, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (119, 2010, '2020-08-06 08:08:27.505000', '2020-08-06 08:08:27.574000', 'hr1', 'hr1',10, 40, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (120, 2010, '2020-08-06 08:08:27.507000', '2020-08-06 08:08:27.574000', 'hr1', 'hr1', 12, 48, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (121, 2010, '2020-08-06 08:08:27.508000', '2020-08-06 08:08:27.574000', 'hr1', 'hr1', 0.5, 2, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (122, 2010, '2020-08-06 08:08:27.511000', '2020-08-06 08:08:27.574000', 'hr1', 'hr1', 12, 48, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (123, 2011, '2020-08-06 08:11:54.024000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1', 1, 2.5, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (124, 2011, '2020-08-06 08:11:54.026000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1', 0.5, 1.25, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (125, 2011, '2020-08-06 08:11:54.027000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1', 10, 25, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (126, 2011, '2020-08-06 08:11:54.028000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1',  12, 30, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (127, 2011, '2020-08-06 08:11:54.030000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1',  0.5, 1.25, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (128, 2011, '2020-08-06 08:11:54.031000', '2020-08-06 08:11:54.055000', 'hr1', 'hr1',  12, 30, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (129, 2012, '2020-08-06 08:13:14.909000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1', 1, 3.3, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (130, 2012, '2020-08-06 08:13:14.912000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1', 0.5, 1.65, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (131, 2012, '2020-08-06 08:13:14.914000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1', 10, 33, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (132, 2012, '2020-08-06 08:13:14.917000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1',  12, 39.6, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (133, 2012, '2020-08-06 08:13:14.919000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1',  0.5, 1.65, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (134, 2012, '2020-08-06 08:13:14.920000', '2020-08-06 08:13:14.946000', 'hr1', 'hr1',  12, 39.6, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (135, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1', 10, 7, '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (136, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1',  12, 8.4, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (137, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1',  0.5, 0.35, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (138, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1',  12, 8.4, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (139, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1', 1, 0.7, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, percentage, amount , code, name) VALUES (140, 2013, '2020-08-06 14:00:00', '2020-08-06 14:00:00', 'hr1', 'hr1', 0.5, 0.35, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (141, 2014, '2020-08-30 09:58:15.515000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0007', '{"ar":"ضريبة القيمة المضافة","en":"Vat"}', 1, 80);
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (142, 2014, '2020-08-30 09:58:15.526000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0008', '{"ar":"ضريبة أرباح تجارية وصناعية","en":"Commercial and industrial profits tax"}', 0.5, 40);
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (143, 2014, '2020-08-30 09:58:15.527000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0009', '{"ar":"ضرائب عقارية","en":"Real estate taxes"}', 10, 800);
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (144, 2014, '2020-08-30 09:58:15.528000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0010', '{"ar":"ضريبة خدمة","en":"Service tax"}', 12, 960);
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (145, 2014, '2020-08-30 09:58:15.530000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0011', '{"ar":"ضريبة الاضافة","en":"Adding tax"}', 0.5, 40);
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, code, name, percentage, amount) VALUES (146, 2014, '2020-08-30 09:58:15.531000', '2020-08-30 09:58:15.589000', 'hr1', 'hr1', '0012', '{"ar":"ضريبة مبيعات","en":"Sales tax"}', 12, 960);
