DELETE FROM iobsalesinvoicetaxes;
alter sequence iobsalesinvoicetaxes_id_seq RESTART with 16;

delete from dobsalesinvoice where id in(51,52,53,54);
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'SalesInvoice' AND id in(51,52,53,54);
alter sequence dobsalesinvoice_id_seq RESTART with 5;

DELETE from EntityUserCode WHERE TYPE = 'DObSalesInvoice';

DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'SalesInvoice' AND id >= 100 AND id <= 104;


INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObSalesInvoice', '2019000004');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (51, '2019000001', '2019-10-28 08:58:00.000000', '2019-10-28 08:58:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Opened","Active"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (51, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (52, '2019000002', '2019-10-28 10:43:00.000000', '2019-10-28 10:43:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (52, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (53, '2019000003', '2019-10-28 10:43:00.000000', '2019-10-28 12:12:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Draft"]', 'SalesInvoice', 18);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (53, 1);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (54, '2019000004', '2019-11-03 09:40:00.000000', '2019-11-03 09:40:00.000000', 'Ahmed.Al-Ashry', 'Ahmed.Al-Ashry', '["Draft"]', 'SalesInvoice', 1000072);
INSERT INTO public.dobsalesinvoice (id, typeid)
VALUES (54, 1);


INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (100, 51, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (102, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 7, null, 'SalesInvoice');

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (103, 53, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1, 10, null, 'SalesInvoice');

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode) 
VALUES (104, 54, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'SalesInvoice');


INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (1, 51, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 301197, 1 ,'0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (2, 51, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 150598.5, 0.5 , '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, percentage, code, name) VALUES (3, 51, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 3011970, 10,  '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');


INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (4, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',29.92, '0007' ,'{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (5, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',14.96, '0008', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (6, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',299.2,  '0009','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (7, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, '0010','{"ar": "ضريبة خدمة", "en": "Service tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (8, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',14.96, '0011', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (9, 52, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',359.03999999999996, '0012','{"ar": "ضريبة مبيعات", "en": "Sales tax"}');


INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (10, 53, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0 ,'0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (11, 53, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0 , '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (12, 53, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0,  '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (13, 54, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0 ,'0001' , '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (14, 54, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0 , '0002' , '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}');
INSERT INTO public.iobsalesinvoicetaxes (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, amount, code, name) VALUES (15, 54, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 0 , '0003' ,'{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');
