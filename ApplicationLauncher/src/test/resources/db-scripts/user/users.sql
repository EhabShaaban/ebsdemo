
DELETE from userassignment WHERE  userid =  2000000;
DELETE from useraccount WHERE  id =  2000000;
DELETE from ebsuser WHERE  id =  2000000;

INSERT INTO ebsuser (id, username, password, sysFlag)
VALUES (2000000, 'TestUser_With_Default_Password', '$2a$10$oFR.mFOfq4YcEZyWMVwSSep.kPBw7sxKJJ1dxLV7VeVeQ.sgC0M/C', true);

INSERT INTO useraccount (id, userid, accountstate, accountcreatetime, secondauthenticator, companycode, companyid,
        accountconfiguration)
        VALUES (2000000, 2000000, 'ACTIVE', '2013-12-08 16:42:07.834476+02', 1, 'BDKCompanyCode', 1,
        '[{"securityConfigurationType":"LOCAL","securityClearnceLevel":"CONFIDENTIAL","secondFactorCredentialValidityMode":10,"configurationState":"ACTIVE"},{"securityConfigurationType":"REMOTE","securityClearnceLevel":"CONFIDENTIAL","secondFactorCredentialValidityMode":10,"configurationState":"ACTIVE"}]');
INSERT INTO userassignment(id , roleid, userid, assignmenttime, assignmentstate) VALUES ((SELECT setval('userassignment_id_seq', (select max(id)+1 from userassignment) )), 99999, 2000000, '2018-06-27 9:42:07.834476+02', 'ACTIVE');
