delete from IObGoodsIssueSalesInvoiceData;
delete from iobcustomeraddress;
delete from lobaddress;
delete from lobcontactperson;
delete from lobreason;
delete from lobissuefrom;
-------------------------------------------------------------------------
INSERT INTO public.lobaddress (id, creationdate, modifieddate, creationinfo, modificationinfo, addressname, address, district, postalcode, countryid, cityid, governmentid, objecttypecode) VALUES (1, '2019-10-28 11:15:53.000000', '2019-10-28 11:15:55.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"en": "66, Salah Salem St", "ar": "66 شارع صلاح سالم"}', '{"en": "66, Salah Salem St", "ar": "66 شارع صلاح سالم"}', 'Masr Elkadyma', null, null, null, null, '1');
INSERT INTO public.lobaddress (id, creationdate, modifieddate, creationinfo, modificationinfo, addressname, address, district, postalcode, countryid, cityid, governmentid, objecttypecode) VALUES (2, '2019-10-28 11:15:53.000000', '2019-10-28 11:15:55.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"en": "Cairo", "ar": "القاهرة"}', '{"en": "66, Salah Salem St", "ar": "66 شارع صلاح سالم"}', 'Masr Elkadyma', null, null, null, null, '1');
INSERT INTO public.lobaddress (id, creationdate, modifieddate, creationinfo, modificationinfo, addressname, address, district, postalcode, countryid, cityid, governmentid, objecttypecode) VALUES (3, '2020-02-24 13:13:45.000000', '2020-02-24 13:13:47.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"ar": "الجيزة", "en": "Giza"}', '{"ar": "7شارع العبور ,الشيخ زايد", "en": "7th st, El Sheikh Zayed"}', 'El Sheikh Zayed', null, null, null, null, '1');
INSERT INTO public.lobaddress (id, creationdate, modifieddate, creationinfo, modificationinfo, addressname, address, district, postalcode, countryid, cityid, governmentid, objecttypecode) VALUES (4, '2020-03-30 13:33:15.000000', '2020-03-30 13:33:16.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"en": "Heliopolis Branch", "ar": "فرع هليوبوليس"}', '{"en": "99, El Hegaz St, Heliopolis", "ar": "99 شارع الحجاز - هليوبوليس"}', null, null, null, null, null, '1');
-------------------------------------------------------------------------
INSERT INTO public.lobcontactperson (id, creationdate, modifieddate, creationinfo, modificationinfo, name, title, telephone, email, department) VALUES (1, '2019-10-28 11:24:31.000000', '2019-10-28 11:24:32.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"ar": "وائل فتحي", "en": "Wael Fathay"}', 'Accountant', '01118644117', 'wael.fathy44@gmail.com', 'Accounting');
INSERT INTO public.lobcontactperson (id, creationdate, modifieddate, creationinfo, modificationinfo, name, title, telephone, email, department) VALUES (2, '2019-11-03 13:11:12.000000', '2019-11-03 13:11:14.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"ar": "Client 2 Contact Person", "en": "Client 2 Contact Person"}', 'Purchasing Responsible', null, null, null);
INSERT INTO public.lobcontactperson (id, creationdate, modifieddate, creationinfo, modificationinfo, name, title, telephone, email, department) VALUES (3, '2020-03-30 09:17:36.000000', '2020-03-30 09:17:40.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '{"en": "October Contact Person", "ar": "October Contact Person"}', 'accountant', null, null, null);
--------------------------------------------------------------------------
INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (1, '0001', '2020-06-15 13:10:34.000000', '2020-06-15 13:10:36.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Problem in product operation", "ar": "مشكلة في تشغيل المنتج"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (3, '0002', '2020-07-09 14:32:19.000000', '2020-07-09 14:32:20.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Problem in product quality", "ar": "مشكلة في جودة المنتج"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (4, '0003', '2020-07-09 14:32:53.000000', '2020-07-09 14:32:54.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Quantity doesn''t match the need", "ar": "الكمية غير مطابقة للاحتياج"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (5, '0004', '2020-07-09 14:34:10.000000', '2020-07-09 14:34:12.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Price doesn''t match the need", "ar": "السعر غير مطابق للاحتياج"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (6, '0005', '2020-07-09 14:34:32.000000', '2020-07-09 14:34:33.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Items doesn''t match the need", "ar": "اﻻصناف غير مطابقة للاحتياج"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (7, '0006', '2020-07-09 14:34:59.000000', '2020-07-09 14:35:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Problem in collection owner", "ar": "مشاكل في التحصيل"}');

INSERT INTO public.lobreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, type, name)
VALUES (8, '0007', '2020-07-09 14:35:26.000000', '2020-07-09 14:35:28.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'SRO_REASON', '{"en": "Updating customers'' information", "ar": "تعديل بيانات العميل"}');

---------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (1, '0001', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"جنوب القاهرة","ar":"جنوب القاهرة"}', '{"en":"42 إسماعيل أباظة - لاظوغلى","ar":"42 إسماعيل أباظة - لاظوغلى"}', '27941052');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (2, '0002', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"6-Oct","ar":"6-Oct"}', '{"en":"6 أكتوبر ح11مج 2 ع47 بجوار الطب البيطرى","ar":"6 أكتوبر ح11مج 2 ع47 بجوار الطب البيطرى"}', '38334925');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (3, '0003', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"القاهرة","ar":"القاهرة"}', '{"en":"38 ش رمسيس","ar":"38 ش رمسيس"}', '25750264');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (4, '0004', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"شمال القاهرة","ar":"شمال القاهرة"}', '{"en":"1 ش 26 يوليو","ar":"1 ش 26 يوليو"}', '25929551');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (5, '0005', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"الجيزة","ar":"الجيزة"}', '{"en":"82 ش وادى النيل مت عقبة جيزة","ar":"82 ش وادى النيل مت عقبة جيزة"}', '3345323');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (6, '0006', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"15-May","ar":"15-May"}', '{"en":"مدينة مايو حى رجال الأعمال مج 3 ع 9 ش 9 فوق الشهر العقارى","ar":"مدينة مايو حى رجال الأعمال مج 3 ع 9 ش 9 فوق الشهر العقارى"}', '25505119');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (7, '0007', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"الأسكندرية","ar":"الأسكندرية"}', '{"en":"21 ميدان سعد زغلول محطة الرمل","ar":"21 ميدان سعد زغلول محطة الرمل"}', '34865397');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (8, '0008', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"المنصورة","ar":"المنصورة"}', '{"en":"105 ش الجمهورية المنصورة","ar":"105 ش الجمهورية المنصورة"}', '502244067');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (9, '0009', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار القاهرة","ar":"إستثمار القاهرة"}', '{"en":"3ش صلاح سالم","ar":"3ش صلاح سالم"}', '22633790');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (10, '0010', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار أسيوط","ar":"إستثمار أسيوط"}', '{"en":"ديوان عام المحافظة","ar":"ديوان عام المحافظة"}', '882293225');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (11, '0011', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار الإسماعيلية","ar":"إستثمار الإسماعيلية"}', '{"en":"برج قرطبة الدور 3 أمام موقف الرئيسى","ar":"برج قرطبة الدور 3 أمام موقف الرئيسى"}', '643353487');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (12, '0012', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار الأسكندرية","ar":"إستثمار الأسكندرية"}', '{"en":"المنطقة الحرة طريق مصر إسكندرية الصحراوى كيلو 26","ar":"المنطقة الحرة طريق مصر إسكندرية الصحراوى كيلو 26"}', '34493664');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (13, '0013', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار العاشر من رمضان","ar":"إستثمار العاشر من رمضان"}', '{"en":"العاشر من رمضان","ar":"العاشر من رمضان"}', '111111111');
