-- Data for Name: cobbank;
DELETE FROM iobpurchaseorderfulfillervendor;
DELETE FROM dobpurchaseorder;
DELETE FROM iobordercompany;
DELETE FROM iobcompanybankdetails;
DELETE FROM cobbank;

INSERT INTO cobbank (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo) VALUES (1, '{"en":"Bank Misr","ar":"بنك مصر"}',  '0001', '2018-04-24 06:31:56.325716', '2018-04-24 06:31:56.325716', 'Default for DigiPro', 'Default for DigiPro');
INSERT INTO cobbank (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo) VALUES (2, '{"en":"National Bank of Egypt","ar":"البنك الأهلي"}',  '0002', '2018-04-24 06:31:56.325716', '2018-04-24 06:31:56.325716', 'Default for DigiPro', 'Default for DigiPro');
INSERT INTO cobbank (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo) VALUES (3, '{"en":"Alex Bank","ar":"بنك إسكندرية"}',  '0003', '2018-04-24 06:31:56.325716', '2018-04-24 06:31:56.325716', 'Default for DigiPro', 'Default for DigiPro');
INSERT INTO cobbank (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo) VALUES (4, '{"en":"CTBC Bank CO., LTD.","ar":"CTBC Bank CO., LTD."}',  '0004', '2018-04-24 06:31:56.325716', '2018-04-24 06:31:56.325716', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode');
INSERT INTO cobbank (id, name,  code, creationdate, modifieddate, creationinfo, modificationinfo) VALUES (5, '{"en":"Qatar National Bank Al Ahli-Tanta","ar":"Qatar National Bank Al Ahli-Tanta"}',  '0005', '2018-04-24 06:31:56.325716', '2018-04-24 06:31:56.325716', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode');
