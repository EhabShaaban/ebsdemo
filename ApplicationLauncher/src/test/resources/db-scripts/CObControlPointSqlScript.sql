INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (1, '{"en":"Signmedia Accountant Head","ar":"Signmedia Accountant Head"}', '0001', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (2, '{"en":"Signmedia Budget Controller","ar":"Signmedia Budget Controller"}', '0002', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (3, '{"en":"Signmedia Director","ar":"Signmedia Director"}', '0003', '2018-08-05 11:39:02.583125', '2018-08-05 11:54:42.785761', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (4, '{"en":"Flexo Accountant Head","ar":"Flexo Accountant Head"}', '0004', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (5, '{"en":"Flexo Budget Controller","ar":"Flexo Budget Controller"}', '0005', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (6, '{"en":"Flexo Director","ar":"Flexo Director"}', '0006', '2018-08-05 11:39:02.583125', '2018-08-05 11:54:42.785761', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (7, '{"en":"Corrugated Accountant Head","ar":"Corrugated Accountant Head"}', '0007', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (8, '{"en":"Corrugated Budget Controller","ar":"Corrugated Budget Controller"}', '0008', '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO cobcontrolpoint (id, name, code, creationdate, modifieddate, creationinfo, modificationinfo) 
VALUES (9, '{"en":"Corrugated Director","ar":"Corrugated Director"}', '0009', '2018-08-05 11:39:02.583125', '2018-08-05 11:54:42.785761', 'hr1', 'hr1');

------------------------CONTROL POINT USERS -------------------------------------
INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (1, 1, 34, true, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (2, 1, 49, false, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (3, 2, 44, true, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (4, 2, 34, false, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (5, 3, 29, true, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (6, 3, 37, false, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (7, 4, 17, true, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

INSERT INTO iobcontrolpointusers (id, refinstanceid, userid, ismain, creationdate, modifieddate, creationinfo, modificationinfo)
VALUES (8, 5, 44, true, '2018-05-31 09:07:25.496505', '2018-05-31 09:07:25.496505', 'hr1', 'hr1');

-------------------------------------