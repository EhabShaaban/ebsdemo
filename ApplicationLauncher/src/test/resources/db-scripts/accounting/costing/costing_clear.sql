delete from cobexchangerate where code='000035';
truncate tobgoodsreceiptstoretransaction cascade ;
truncate dobCosting cascade ;
delete from dobinventorydocument where inventoryDocumentType='GR_PO' ;
delete from dobaccountingdocument where objecttypecode='Costing';
delete from dobaccountingdocument where objecttypecode = 'IMPORT_GOODS_INVOICE' and code = '2021000002';

SELECT setval('dobinventorydocument_id_seq', (select max(id) from dobinventorydocument) );
SELECT setval('dobaccountingdocument_id_seq', (select max(id) from dobaccountingdocument) );
