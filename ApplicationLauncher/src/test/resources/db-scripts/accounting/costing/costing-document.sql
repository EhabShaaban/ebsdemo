INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                         currentstates, purchaseunitid, inventorydocumenttype)
VALUES (98, '2021000009', '2021-03-10 07:02:00.000000 +00:00', '2021-03-10 07:02:00.000000 +00:00', 'Admin', 'Admin',
        '["Active"]', 19, 'GR_PO');
INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                         currentstates, purchaseunitid, inventorydocumenttype)
VALUES (99, '2021000010', '2021-03-10 07:02:00.000000 +00:00', '2021-03-10 07:02:00.000000 +00:00', 'Admin', 'Admin',
        '["Active"]', 19, 'GR_PO');

INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeid)
VALUES (98, 1);
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeid)
VALUES (99, 1);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        companyid, plantid, storehouseid, storekeeperid)
VALUES (98, 98, '2021-09-05 10:28:15.630243', '2021-09-05 10:28:15.630243', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 7, 8, 9, 39);
INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        companyid, plantid, storehouseid, storekeeperid)
VALUES (99, 99, '2021-09-05 10:28:15.650163', '2021-09-05 10:28:15.650163', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 7, 8, 9, 39);

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename,
                                                     comments)
VALUES (57, 98, '2021-09-05 10:28:15.671237', '2021-09-05 10:28:15.671237', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 2090, 46, '{
    "en": "Zhejiang",
    "ar": "زاجاننج"
  }', '123456', 37, 'Gehan.Ahmed', null);
INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename,
                                                     comments)
VALUES (58, 99, '2021-09-05 10:28:15.689475', '2021-09-05 10:28:15.689475', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 2091, 46, '{
    "en": "Zhejiang",
    "ar": "زاجاننج"
  }', '123456', 37, 'Gehan.Ahmed', null);


INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate,
                                                                  creationinfo, modificationinfo, itemid,
                                                                  differencereason, objecttypecode)
VALUES (156, 98, '2021-09-05 10:28:15.705459', '2021-09-05 10:28:15.705459', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 33, '', '1');
INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate,
                                                                  creationinfo, modificationinfo, itemid,
                                                                  differencereason, objecttypecode)
VALUES (157, 98, '2021-09-05 10:28:15.719329', '2021-09-05 10:28:15.719329', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 34, '', '1');
INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate,
                                                                  creationinfo, modificationinfo, itemid,
                                                                  differencereason, objecttypecode)
VALUES (158, 99, '2021-09-05 10:28:15.740586', '2021-09-05 10:28:15.740586', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 33, '', '1');
INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate,
                                                                  creationinfo, modificationinfo, itemid,
                                                                  differencereason, objecttypecode)
VALUES (159, 99, '2021-09-05 10:28:15.756278', '2021-09-05 10:28:15.756278', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 34, '', '1');

INSERT INTO public.iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate,
                                                               creationinfo, modificationinfo, receivedqtyuoe,
                                                               unitofentryid, stocktype, notes, itemvendorrecordid,
                                                               estimatedcost, actualcost)
VALUES (156, 156, '2021-09-05 10:28:15.771786', '2021-09-05 10:28:15.771786', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 100, 23, 'UNRESTRICTED_USE', null, null, null, null);
INSERT INTO public.iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate,
                                                               creationinfo, modificationinfo, receivedqtyuoe,
                                                               unitofentryid, stocktype, notes, itemvendorrecordid,
                                                               estimatedcost, actualcost)
VALUES (157, 157, '2021-09-05 10:28:15.787419', '2021-09-05 10:28:15.787419', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 50, 6, 'UNRESTRICTED_USE', null, null, null, null);
INSERT INTO public.iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate,
                                                               creationinfo, modificationinfo, receivedqtyuoe,
                                                               unitofentryid, stocktype, notes, itemvendorrecordid,
                                                               estimatedcost, actualcost)
VALUES (158, 158, '2021-09-05 10:28:15.802439', '2021-09-05 10:28:15.802439', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 100, 23, 'UNRESTRICTED_USE', null, null, null, null);
INSERT INTO public.iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate,
                                                               creationinfo, modificationinfo, receivedqtyuoe,
                                                               unitofentryid, stocktype, notes, itemvendorrecordid,
                                                               estimatedcost, actualcost)
VALUES (159, 159, '2021-09-05 10:28:15.819117', '2021-09-05 10:28:15.819117', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 50, 6, 'UNRESTRICTED_USE', null, null, null, null);


-- Costing Document --

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                          currentstates, objecttypecode, documentownerid)
VALUES (2274, '2021000001', '2021-03-10 09:02:00.000000', '2021-03-10 09:02:00.000000', 'Admin', 'Admin', '["Active"]',
        'Costing', null);
INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                          currentstates, objecttypecode, documentownerid)
VALUES (2275, '2021000002', '2021-03-10 09:02:00.000000', '2021-03-10 09:02:00.000000', 'Admin', 'Admin', '["Active"]',
        'Costing', null);

INSERT INTO public.dobcosting (id)
VALUES (2274);
INSERT INTO public.dobcosting (id)
VALUES (2275);

INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseunitid, companyid, bankaccountid,
                                                     objecttypecode)
VALUES (2789, 2274, '2021-09-05 08:28:15.868205 +00:00', '2021-09-05 08:28:15.868205 +00:00', 'Admin', 'Admin', 19, 7,
        null, 'Costing');
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseunitid, companyid, bankaccountid,
                                                     objecttypecode)
VALUES (2790, 2275, '2021-09-05 08:28:15.886227 +00:00', '2021-09-05 08:28:15.886227 +00:00', 'Admin', 'Admin', 19, 7,
        null, 'Costing');

INSERT INTO public.iobcostingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                      goodsreceiptid, currencypriceestimatetime, currencypriceactualtime)
VALUES (5, 2274, '2021-09-05 08:28:15.902279 +00:00', '2021-09-05 08:28:15.902279 +00:00', 'Admin', 'Admin', 98, 11.44,
        null);
INSERT INTO public.iobcostingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                      goodsreceiptid, currencypriceestimatetime, currencypriceactualtime)
VALUES (6, 2275, '2021-09-05 08:28:15.925473 +00:00', '2021-09-05 08:28:15.925473 +00:00', 'Admin', 'Admin', 99, null,
        11.44);

INSERT INTO public.iobcostingitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                    itemid, uomid, quantity, stocktype, tobeconsideredincostingper,
                                    estimateunitlandedcost, actualunitlandedcost, estimateunitprice, actualunitprice)
VALUES (9, 2274, '2021-09-05 08:28:15.938532 +00:00', '2021-09-05 08:28:15.938532 +00:00', 'Admin', 'Admin', 33, 23,
        100.123456, 'UNRESTRICTED_USE', 0.40123456, 2000.123456, null, 25.123456, null);
INSERT INTO public.iobcostingitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                    itemid, uomid, quantity, stocktype, tobeconsideredincostingper,
                                    estimateunitlandedcost, actualunitlandedcost, estimateunitprice, actualunitprice)
VALUES (10, 2274, '2021-09-05 08:28:15.959251 +00:00', '2021-09-05 08:28:15.959251 +00:00', 'Admin', 'Admin', 34, 6,
        50.123456, 'UNRESTRICTED_USE', 0.20123456, 1000.123456, null, 15.123456, null);
INSERT INTO public.iobcostingitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                    itemid, uomid, quantity, stocktype, tobeconsideredincostingper,
                                    estimateunitlandedcost, actualunitlandedcost, estimateunitprice, actualunitprice)
VALUES (11, 2275, '2021-09-05 08:28:15.982138 +00:00', '2021-09-05 08:28:15.982138 +00:00', 'Admin', 'Admin', 33, 23,
        100.123456, 'UNRESTRICTED_USE', 0.40123456, null, 2000.123456, null, 25.123456);
INSERT INTO public.iobcostingitems (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                    itemid, uomid, quantity, stocktype, tobeconsideredincostingper,
                                    estimateunitlandedcost, actualunitlandedcost, estimateunitprice, actualunitprice)
VALUES (12, 2275, '2021-09-05 08:28:15.996286 +00:00', '2021-09-05 08:28:15.996286 +00:00', 'Admin', 'Admin', 34, 6,
        50.123456, 'UNRESTRICTED_USE', 0.20123456, null, 1000.123456, null, 15.123456);



DELETE
from EntityUserCode
WHERE type = 'DObGoodsReceipt';
INSERT INTO EntityUserCode (type, code)
values ('DObGoodsReceipt', '2021000010');

DELETE
from EntityUserCode
WHERE type = 'DObCosting';
INSERT INTO EntityUserCode (type, code)
values ('DObCosting', '2021000002');
