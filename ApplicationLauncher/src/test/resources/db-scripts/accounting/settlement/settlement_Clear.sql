delete from iobaccountingdocumentaccountingdetails where objecttypecode = 'Settlement';
delete from iobaccountingdocumentactivationdetails;
delete from dobSettlementjournalentry;
delete from dobjournalentry where objecttypecode = 'Settlement';
DELETE FROM IObSettlementDetails;
DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'Settlement';
DELETE FROM dobsettlement;
DELETE FROM dobaccountingdocument WHERE objecttypecode = 'Settlement';