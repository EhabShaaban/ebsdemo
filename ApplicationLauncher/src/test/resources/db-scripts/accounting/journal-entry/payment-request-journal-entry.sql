DELETE FROM dobpaymentrequestjournalentry WHERE ID > 31 AND ID < 40;
DELETE FROM dobpaymentrequestjournalentry WHERE ID IN (57, 58, 59);
DELETE FROM DObJournalEntry WHERE ID > 31 AND ID < 40;
DELETE FROM DObJournalEntry WHERE ID > 31 AND ID IN (57, 58, 59);

ALTER SEQUENCE DObJournalEntry_id_seq RESTART WITH 6200;

DELETE FROM EntityUserCode WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000058');

-- Payment Request Code: 2019000013
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (32, '2020000029', '2020-09-22 17:08:50.176000', '2020-09-22 17:08:50.176000', 'hr1', 'hr1', '["Active"]', null, '2019-08-08 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (32, 1013);

-- Payment Request Code: 2019000014
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (33, '2020000030', '2020-09-22 17:09:15.387000', '2020-09-22 17:09:15.387000', 'hr1', 'hr1', '["Active"]', null, '2019-08-09 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (33, 1014);

-- Payment Request Code: 2019000015
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (34, '2020000031', '2020-09-22 17:09:44.515000', '2020-09-22 17:09:44.515000', 'hr1', 'hr1', '["Active"]', null, '2019-08-13 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (34, 1015);

-- Payment Request Code: 2019000041
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (35, '2020000032', '2020-09-22 17:10:21.536000', '2020-09-22 17:10:21.536000', 'hr1', 'hr1', '["Active"]', null, '2019-08-13 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (35, 1041);

-- Payment Request Code: 2019000047
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (36, '2020000033', '2020-09-22 17:10:55.153000', '2020-09-22 17:10:55.153000', 'hr1', 'hr1', '["Active"]', null, '2019-08-13 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (36, 1047);

-- Payment Request Code: 2019000048
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (37, '2020000034', '2020-09-22 17:11:29.982000', '2020-09-22 17:11:29.982000', 'hr1', 'hr1', '["Active"]', null, '2019-08-13 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (37, 1048);

-- Payment Request Code: 2019000049
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (38, '2020000035', '2020-09-22 17:11:56.177000', '2020-09-22 17:11:56.177000', 'hr1', 'hr1', '["Active"]', null, '2019-08-13 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (38, 1049);


INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (39, '2020000036', '2020-09-22 10:30:00.000000', '2020-09-22 17:08:01.982000', 'Amr.Khalil', 'Amr.Khalil', '["Active"]', null, '2019-08-07 00:00:00.000000', 10, 23, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (39, 1053);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (57, '2020000056', '2020-12-09 17:16:50.360000', '2020-12-09 17:16:50.360000', 'hr1', 'hr1', '["Active"]', null, '2020-12-09 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (57, 2019);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (58, '2020000057', '2020-12-09 17:16:59.660000', '2020-12-09 17:16:59.660000', 'hr1', 'hr1', '["Active"]', null, '2020-12-09 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (58, 2020);


INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (59, '2020000058', '2020-12-09 17:16:59.660000', '2020-12-09 17:16:59.660000', 'hr1', 'hr1', '["Active"]', null, '2020-12-09 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (59, 2021);