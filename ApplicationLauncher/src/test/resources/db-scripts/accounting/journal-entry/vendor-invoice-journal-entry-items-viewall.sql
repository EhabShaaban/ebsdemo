DELETE FROM iobjournalentryitemvendorsubaccount WHERE ID IN (72, 74, 76, 109);
DELETE FROM iobjournalentryitempurchaseordersubaccount WHERE ID IN (73, 75, 77, 110);
DELETE FROM IObJournalEntryItem WHERE ID > 71 AND ID < 78;
DELETE FROM IObJournalEntryItem WHERE ID IN (109, 110);

ALTER SEQUENCE IObJournalEntryItem_id_seq RESTART WITH 4000;

-- Vendor Invoice Code: 2019000104
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (72, 23, '2020-09-01 10:47:19.926000', '2020-09-01 10:47:19.926000', 'hr1', 'hr1', 21, 'Vendors', 422, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (73, 23, '2020-09-01 10:47:19.941000', '2020-09-01 10:47:19.941000', 'hr1', 'hr1', 25, 'PO', 0, 422, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (73, 1049);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (72, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000108
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (74, 24, '2020-09-01 11:53:43.216000', '2020-09-01 11:53:43.216000', 'hr1', 'hr1', 21, 'Vendors', 52857.6, 0, 2, 1, 16.5, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (75, 24, '2020-09-01 11:53:43.220000', '2020-09-01 11:53:43.220000', 'hr1', 'hr1', 25, 'PO', 0, 52857.6, 2, 1, 16.5, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (75, 1044);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (74, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000109
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (76, 25, '2020-09-01 11:57:54.430000', '2020-09-01 11:57:54.430000', 'hr1', 'hr1', 5, 'Vendors', 720, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (77, 25, '2020-09-01 11:57:54.433000', '2020-09-01 11:57:54.433000', 'hr1', 'hr1', 25, 'PO', 0, 720, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (77, 1003);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (76, 45, 'Local_Vendors');

-- Vendor Invoice Code: 2020000001
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (109, 56, '2020-12-06 11:06:57.973000', '2020-12-06 11:06:57.973000', 'hr1', 'hr1', 21, 'Vendors', 8064, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (110, 56, '2020-12-06 11:06:57.986000', '2020-12-06 11:06:57.986000', 'hr1', 'hr1', 25, 'PO', 0, 8064, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (109, 46, 'Local_Vendors');

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (110, (SELECT id FROM dobpurchaseordergeneralmodel WHERE code = '2020000045'));