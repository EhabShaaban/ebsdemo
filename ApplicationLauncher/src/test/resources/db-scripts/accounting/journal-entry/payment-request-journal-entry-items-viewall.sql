DELETE FROM iobjournalentryitembanksubaccount WHERE ID IN (89, 120);
DELETE FROM iobjournalentryitempurchaseordersubaccount WHERE ID IN (88, 119);
DELETE FROM IObJournalEntryItem WHERE ID > 87 AND ID < 90;
DELETE FROM IObJournalEntryItem WHERE ID IN (119, 120);

ALTER SEQUENCE IObJournalEntryItem_id_seq RESTART WITH 6100;

-- Payment Request Code: 2019000003
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (88, 31, '2020-09-22 17:08:01.991000', '2020-09-22 17:08:01.991000', 'hr1', 'hr1', 25, 'PO', 0, 100, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (89, 31, '2020-09-22 17:08:01.997000', '2020-09-22 17:08:01.997000', 'hr1', 'hr1', 15, 'Banks', 100, 0, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (88, 1045);

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (89, 17);

-- Under settlement Payment Request
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (119, 61, '2021-01-04 16:45:22.884000', '2021-01-04 16:45:22.884000', 'hr1', 'hr1', 25, 'PO', 0, 1000, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (120, 61, '2021-01-04 16:45:22.897000', '2021-01-04 16:45:22.897000', 'hr1', 'hr1', 15, 'Banks', 1000, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (120, 20);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (119, null);