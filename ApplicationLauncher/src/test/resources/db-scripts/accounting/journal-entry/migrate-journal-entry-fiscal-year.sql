UPDATE dobjournalentry
SET fiscalperiodid = (select id
                      from cobfiscalperiod
                      where (name :: json ->> 'en') = TO_CHAR(duedate :: TIMESTAMP, 'yyyy'))
where fiscalperiodid is null;