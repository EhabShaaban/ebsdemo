DELETE FROM DObVendorInvoiceJournalEntry WHERE ID IN (23, 24, 25, 56);
DELETE FROM DObJournalEntry WHERE ID IN (23, 24, 25, 56);

ALTER SEQUENCE DObJournalEntry_id_seq RESTART WITH 4000;

DELETE FROM EntityUserCode WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000056');


-- Vendor Invoice Code: 2019000104
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (23, '2020000020', '2020-09-01 10:47:00.000000', '2020-09-01 10:47:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (23, 104);


-- Vendor Invoice Code: 2019000108
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (24, '2020000021', '2020-09-01 11:53:00.000000', '2020-09-01 11:53:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-08-02 00:00:00.000000', 10, 23, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (24, 108);

-- Vendor Invoice Code: 2019000109
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (25, '2020000022', '2020-09-01 11:57:00.000000', '2020-09-01 11:57:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 1, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (25, 109);

-- Vendor Invoice Code: 2020000001
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (56, '2020000056', '2020-12-06 11:06:00.931000', '2020-12-06 11:06:00.931000', 'hr1', 'hr1', '["Active"]', null, '2020-12-06 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (56, 2015);