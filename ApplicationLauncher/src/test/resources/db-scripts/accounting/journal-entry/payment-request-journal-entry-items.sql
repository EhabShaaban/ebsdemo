DELETE FROM iobjournalentryitembanksubaccount WHERE ID IN (97, 99, 112, 114, 116);
DELETE FROM iobjournalentryitempurchaseordersubaccount WHERE ID IN (90, 98, 111, 113, 115);
DELETE FROM IObJournalEntryItem WHERE ID > 89 AND ID < 109;
DELETE FROM IObJournalEntryItem WHERE ID IN (111, 112, 113, 114, 115, 116);

ALTER SEQUENCE IObJournalEntryItem_id_seq RESTART WITH 6300;

-- Payment Request Code: 2019000013
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (90, 32, '2020-09-22 17:08:50.190000', '2020-09-22 17:08:50.190000', 'hr1', 'hr1', 25, 'PO', 0, 1000, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (91, 32, '2020-09-22 17:08:50.196000', '2020-09-22 17:08:50.196000', 'hr1', 'hr1', 13, 'Treasuries', 1000, 0, 2, 1, 17.44, 26);
INSERT INTO public.iobjournalentryitemtreasurysubaccount (id, subaccountid)
VALUES (91, 2);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (90, 1018);

-- Payment Request Code: 2019000014
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (92, 33, '2020-09-22 17:09:15.395000', '2020-09-22 17:09:15.395000', 'hr1', 'hr1', 14, 'NONE', 0, -2818.272, 1, 1, 1, null);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (93, 33, '2020-09-22 17:09:15.398000', '2020-09-22 17:09:15.398000', 'hr1', 'hr1', 5, 'Vendors', 0, 378.8, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (94, 33, '2020-09-22 17:09:15.402000', '2020-09-22 17:09:15.402000', 'hr1', 'hr1', 13, 'Treasuries', 378.8, 0, 2, 1, 17.44, 26);
INSERT INTO public.iobjournalentryitemtreasurysubaccount (id, subaccountid)
VALUES (94, 2);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (93, 46, 'Local_Vendors');

-- Payment Request Code: 2019000015
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (95, 34, '2020-09-22 17:09:44.524000', '2020-09-22 17:09:44.524000', 'hr1', 'hr1', 14, 'NONE', 0, -744, 1, 1, 1, null);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (96, 34, '2020-09-22 17:09:44.527000', '2020-09-22 17:09:44.527000', 'hr1', 'hr1', 21, 'Vendors', 0, 100, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (97, 34, '2020-09-22 17:09:44.530000', '2020-09-22 17:09:44.530000', 'hr1', 'hr1', 15, 'Banks', 100, 0, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (96, 46, 'Local_Vendors');

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (97, 17);

-- Payment Request Code: 2019000041
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (98, 35, '2020-09-22 17:10:21.545000', '2020-09-22 17:10:21.545000', 'hr1', 'hr1', 21, 'Vendors', 0, 10, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (99, 35, '2020-09-22 17:10:21.549000', '2020-09-22 17:10:21.549000', 'hr1', 'hr1', 15, 'Banks', 10, 0, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (98, 46, 'Local_Vendors');

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (99, 17);

-- Payment Request Code: 2019000047
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (100, 36, '2020-09-22 17:10:55.161000', '2020-09-22 17:10:55.161000', 'hr1', 'hr1', 14, 'NONE', 0, -228162.48, 1, 1, 1, null);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (101, 36, '2020-09-22 17:10:55.164000', '2020-09-22 17:10:55.164000', 'hr1', 'hr1', 21, 'Vendors', 0, 30667, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (102, 36, '2020-09-22 17:10:55.167000', '2020-09-22 17:10:55.167000', 'hr1', 'hr1', 13, 'Treasuries', 30667, 0, 2, 1, 17.44, 26);
INSERT INTO public.iobjournalentryitemtreasurysubaccount (id, subaccountid)
VALUES (102, 2);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (101, 46, 'Local_Vendors');

-- Payment Request Code: 2019000048
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (103, 37, '2020-09-22 17:11:29.990000', '2020-09-22 17:11:29.990000', 'hr1', 'hr1', 14, 'NONE', 0, -223200, 1, 1, 1, null);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (104, 37, '2020-09-22 17:11:29.993000', '2020-09-22 17:11:29.993000', 'hr1', 'hr1', 21, 'Vendors', 0, 30000, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (105, 37, '2020-09-22 17:11:29.996000', '2020-09-22 17:11:29.996000', 'hr1', 'hr1', 13, 'Treasuries', 30000, 0, 2, 1, 17.44, 26);
INSERT INTO public.iobjournalentryitemtreasurysubaccount (id, subaccountid)
VALUES (105, 2);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (104, 46, 'Local_Vendors');

-- Payment Request Code: 2019000049
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (106, 38, '2020-09-22 17:11:56.186000', '2020-09-22 17:11:56.186000', 'hr1', 'hr1', 14, 'NONE', 0, -223200, 1, 1, 1, null);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (107, 38, '2020-09-22 17:11:56.189000', '2020-09-22 17:11:56.189000', 'hr1', 'hr1', 21, 'Vendors', 0, 30000, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (108, 38, '2020-09-22 17:11:56.193000', '2020-09-22 17:11:56.193000', 'hr1', 'hr1', 13, 'Treasuries', 30000, 0, 2, 1, 17.44, 26);
INSERT INTO public.iobjournalentryitemtreasurysubaccount (id, subaccountid)
VALUES (108, 2);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (107, 46, 'Local_Vendors');


INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (111, 57, '2020-12-09 17:16:50.365000', '2020-12-09 17:16:50.365000', 'hr1', 'hr1', 25, 'PO', 0, 1000, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (111, 2091);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (112, 57, '2020-12-09 17:16:50.368000', '2020-12-09 17:16:50.368000', 'hr1', 'hr1', 15, 'Banks', 1000, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (112, 20);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (113, 58, '2020-12-09 17:16:59.666000', '2020-12-09 17:16:59.666000', 'hr1', 'hr1', 25, 'PO', 0, 3000, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (113, 2091);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (114, 58, '2020-12-09 17:16:59.670000', '2020-12-09 17:16:59.670000', 'hr1', 'hr1', 15, 'Banks', 3000, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (114, 20);

-- Payment Request Code: 2020000003
INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (115, 59, '2020-12-09 17:16:59.666000', '2020-12-09 17:16:59.666000', 'hr1', 'hr1', 25, 'PO', 0, 100, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (115, 1023);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (116, 59, '2020-12-09 17:16:59.670000', '2020-12-09 17:16:59.670000', 'hr1', 'hr1', 15, 'Banks', 100, 0, 1, 1, 1, 13);

INSERT INTO public.iobjournalentryitembanksubaccount (id, subaccountid)
VALUES (116, 20);