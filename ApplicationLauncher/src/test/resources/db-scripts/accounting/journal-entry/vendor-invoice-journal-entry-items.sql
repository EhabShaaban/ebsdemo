delete from iobjournalentryitemvendorsubaccount WHERE id > 77 AND id < 85;
delete from iobjournalentryitemvendorsubaccount WHERE id = 117;
delete from iobjournalentryitempurchaseordersubaccount WHERE id > 78 AND id < 86;
delete from iobjournalentryitempurchaseordersubaccount WHERE id = 118;
delete from IObJournalEntryItem WHERE id > 77 AND id < 86;
delete from IObJournalEntryItem WHERE id IN (117, 118);

ALTER SEQUENCE IObJournalEntryItem_id_seq RESTART WITH 4000;

-- Vendor Invoice Code: 2019000034

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (78, 26, '2020-09-02 10:28:30.763000', '2020-09-02 10:28:30.763000', 'hr1', 'hr1', 21, 'Vendors', 378.8, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (79, 26, '2020-09-02 10:28:30.768000', '2020-09-02 10:28:30.768000', 'hr1', 'hr1', 25, 'PO', 0, 378.8, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (79, 1049);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (78, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000039

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (80, 27, '2020-09-02 11:10:50.393000', '2020-09-02 11:10:50.393000', 'hr1', 'hr1', 21, 'Vendors', 13233.6, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (81, 27, '2020-09-02 11:10:50.398000', '2020-09-02 11:10:50.398000', 'hr1', 'hr1', 25, 'PO', 0, 13233.6, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (81, 1056);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (80, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000040

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (82, 28, '2020-09-02 11:15:05.312000', '2020-09-02 11:15:05.312000', 'hr1', 'hr1', 21, 'Vendors', 2420208.8, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (83, 28, '2020-09-02 11:15:05.317000', '2020-09-02 11:15:05.317000', 'hr1', 'hr1', 25, 'PO', 0, 2420208.8, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (83, 1047);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (82, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000002

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (84, 29, '2020-09-02 11:37:37.945000', '2020-09-02 11:37:37.945000', 'hr1', 'hr1', 21, 'Vendors', 200, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (85, 29, '2020-09-02 11:37:37.948000', '2020-09-02 11:37:37.948000', 'hr1', 'hr1', 25, 'PO', 0, 200, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (85, 1010);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (84, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2019000004

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (86, 30, '2020-09-03 10:07:41.799000', '2020-09-03 10:07:41.799000', 'hr1', 'hr1', 21, 'Vendors', 10150, 0, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (87, 30, '2020-09-03 10:07:42.044000', '2020-09-03 10:07:42.044000', 'hr1', 'hr1', 25, 'PO', 0, 10150, 2, 1, 10, 26);

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (87, 2087);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (86, 46, 'Local_Vendors');

-- Vendor Invoice Code: 2020000003

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (117, 60, '2020-12-17 15:25:56.817000', '2020-12-17 15:25:56.817000', 'hr1', 'hr1', 21, 'Vendors', 42560, 0, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountid, type, credit, debit, documentcurrencyid, companycurrencyid, companycurrencyprice, companyexchangerateid)
VALUES (118, 60, '2020-12-17 15:25:56.835000', '2020-12-17 15:25:56.835000', 'hr1', 'hr1', 25, 'PO', 0, 42560.00000000001, 2, 1, 17.44, 26);

INSERT INTO public.iobjournalentryitemvendorsubaccount (id, subaccountid, type)
VALUES (117, 46, 'Local_Vendors');

INSERT INTO public.iobjournalentryitempurchaseordersubaccount (id, subaccountid)
VALUES (118, 1023);