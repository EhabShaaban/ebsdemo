DELETE FROM dobpaymentrequestjournalentry WHERE ID IN (31, 61);
DELETE FROM DObJournalEntry WHERE ID IN (31, 61);

ALTER SEQUENCE DObJournalEntry_id_seq RESTART WITH 6000;

DELETE FROM EntityUserCode WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2021000001');


-- Payment Request Code: 2019000003
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (31, '2020000028', '2020-09-22 10:30:00.000000', '2020-09-22 17:08:01.982000', 'Amr.Khalil', 'Amr.Khalil', '["Active"]', null, '2019-08-07 00:00:00.000000', 10, 23, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (31, 1003);

-- Under settlement Payment Request
INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (61, '2021000001', '2021-01-04 16:45:00.000000', '2021-01-04 16:45:00.000000', 'hr1', 'hr1', '["Active"]', null, '2021-01-04 00:00:00.000000', 10, 19, 'PaymentRequest');

INSERT INTO public.dobpaymentrequestjournalentry (id, documentreferenceid)
VALUES (61, 2026);
