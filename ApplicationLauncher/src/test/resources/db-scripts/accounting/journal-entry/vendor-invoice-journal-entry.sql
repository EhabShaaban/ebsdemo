DELETE FROM DObVendorInvoiceJournalEntry WHERE id > 25 AND id < 31;
DELETE FROM DObVendorInvoiceJournalEntry WHERE id = 60;
DELETE FROM DObJournalEntry WHERE id > 25 AND id < 31;
DELETE FROM DObJournalEntry WHERE id = 60;

ALTER SEQUENCE DObJournalEntry_id_seq RESTART WITH 4000;

DELETE FROM EntityUserCode WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000057');

-- Vendor Invoice Code: 2019000034

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (26, '2020000023', '2020-09-02 10:28:00.000000', '2020-09-02 10:28:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (26, 34);

-- Vendor Invoice Code: 2019000039

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (27, '2020000024', '2020-09-02 11:10:00.000000', '2020-09-02 11:10:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 1, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (27, 39);

-- Vendor Invoice Code: 2019000040

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (28, '2020000025', '2020-09-02 11:15:00.000000', '2020-09-02 11:15:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (28, 40);

-- Vendor Invoice Code: 2019000002

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (29, '2020000026', '2020-09-02 11:37:00.000000', '2020-09-02 11:37:00.000000', 'hr1', 'hr1', '["Active"]', null, '2019-09-02 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (29, 2);

-- Vendor Invoice Code: 2019000004

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (30, '2020000027', '2020-09-03 10:07:00.000000', '2020-09-03 10:07:00.000000', 'hr1', 'hr1', '["Active"]', null, '2020-09-02 00:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (30, 4);

-- Vendor Invoice Code: 2020000003

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (60, '2020000056', '2020-12-17 15:25:56.802000', '2020-12-17 15:25:56.802000', 'hr1', 'hr1', '["Active"]', null, '2020-12-17 02:00:00.000000', 10, 19, 'VendorInvoice');

INSERT INTO public.dobvendorinvoicejournalentry (id, documentreferenceid)
VALUES (60, 2022);