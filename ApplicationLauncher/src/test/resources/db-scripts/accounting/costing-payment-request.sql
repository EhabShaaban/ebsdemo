delete from dobaccountingdocument where id = 1047;
delete from IObPaymentRequestPaymentDetails where id = 1047;
delete from IObPaymentRequestInvoicePaymentDetails where id = 1047;
delete from IObVendorInvoiceDetails where id = 40;
delete from iobvendorinvoicepurchaseorder where id = 40;
delete from dobaccountingdocument where id in(1012,1041) ;
delete from dobpaymentrequest where id in(1012,1041);
delete from doborderdocument where id = 2089;
delete from dobpurchaseorder where id = 2089;

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode) values (1047, '2019000047', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Draft"]', 'PaymentRequest');
insert into dobpaymentrequest (id, paymentType)
values (1047, 'VENDOR');


insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description,  paymentForm, treasuryid)
values (1047, 1047, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 30667.00, 2, 'for estimated cost goods receipts', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1047, 40);


INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (40, 40, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023800', '2018-04-24 06:31:56.332658', 46);


INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (40, 40, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1047, 2, 3);


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode) values (1012 , '2019000012', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest');
insert into dobpaymentrequest (id, paymentType)
values (1012, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode) values (1041, '2019000041', '2018-08-11 09:02:00', '2018-08-11 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Active", "PaymentDone"]', 'PaymentRequest');
insert into dobpaymentrequest (id, paymentType)
values (1041, 'VENDOR');

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2089, '2020000046', '5-Aug-2020 9:02 AM'::TIMESTAMP, '5-Aug-2020 9:02 AM'::TIMESTAMP, 'admin', 'admin',
        concat('["', ARRAY_TO_STRING(string_to_array('Confirmed', ','), '","'), '"]'), 'SERVICE_PO',
        (SELECT id FROM userinfo WHERE userinfo.name = 'Gehan Ahmed'));

INSERT INTO public.dobpurchaseorder (id) VALUES (2089);



