DELETE from EntityUserCode WHERE type = 'DObAccountingNote';
INSERT INTO EntityUserCode (type, code) values ('DObAccountingNote', '2021000001');

TRUNCATE dobaccountingdocument CASCADE;
TRUNCATE DObSalesInvoice CASCADE;
TRUNCATE DObSalesOrder CASCADE;
TRUNCATE DObSalesReturnOrder CASCADE;
TRUNCATE DObAccountingNote CASCADE;

ALTER SEQUENCE dobaccountingdocument_id_seq RESTART WITH 1;
ALTER SEQUENCE DObSalesInvoice_id_seq RESTART WITH 1;
ALTER SEQUENCE DObAccountingNote_id_seq RESTART WITH 1;
ALTER SEQUENCE DObSalesOrder_id_seq RESTART WITH 1;
ALTER SEQUENCE DObSalesReturnOrder_id_seq RESTART WITH 1;
