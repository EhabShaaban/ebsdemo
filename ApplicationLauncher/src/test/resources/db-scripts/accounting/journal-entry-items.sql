delete from iobjournalentryitemvendorsubaccount WHERE id IN (6, 20);
delete from iobjournalentryitempurchaseordersubaccount WHERE id = 18;
delete from iobjournalentryitembanksubaccount WHERE id IN (7, 21);
delete from IObJournalEntryItem WHERE id IN (6, 7, 15, 16, 17, 18, 19, 20, 21);

ALTER SEQUENCE IObJournalEntryItem_id_seq RESTART WITH 3000;