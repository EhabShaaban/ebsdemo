delete from iobvendorinvoicetaxes;
delete from iobsalesinvoicetaxes;
delete from lobtaxinfo;
delete from lobvendortaxes;
delete from lobcompanytaxes;

--------------------------Company Taxes-------------------------------
-- DigiPro Company Taxes
INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name, objectTypeCode, percentage, description)
VALUES (1,  '0001', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 'CompanyTaxes',1,'none');
insert into lobcompanytaxes(id, companyid) values (1,10);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name, objectTypeCode, percentage, description )
VALUES (2,  '0002', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}','CompanyTaxes', 0.5, 'none');
insert into lobcompanytaxes(id, companyid) values (2,10);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (3,  '0003', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}','CompanyTaxes', 10 , 'none');
insert into lobcompanytaxes(id, companyid) values (3,10);

-- Madina Company Taxes
INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name, objectTypeCode, percentage, description)
VALUES (7,  '0007', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}', 'CompanyTaxes',1,'none');
insert into lobcompanytaxes(id, companyid) values (7,7);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name, objectTypeCode, percentage, description )
VALUES (8,  '0008', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة أرباح تجارية وصناعية", "en": "Commercial and industrial profits tax"}','CompanyTaxes', 0.5, 'none');
insert into lobcompanytaxes(id, companyid) values (8,7);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (9,  '0009', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data','{"ar": "ضرائب عقارية", "en": "Real estate taxes"}','CompanyTaxes', 10 , 'none');
insert into lobcompanytaxes(id, companyid) values (9,7);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (10,  '0010', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة خدمة", "en": "Service tax"}','CompanyTaxes', 12, 'none');
insert into lobcompanytaxes(id, companyid) values (10,7);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name, objectTypeCode, percentage, description )
VALUES (11,  '0011', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة الاضافة", "en": "Adding tax"}','CompanyTaxes', 0.5, 'none');
insert into lobcompanytaxes(id, companyid) values (11,7);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (12,  '0012', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة مبيعات", "en": "Sales tax"}','CompanyTaxes', 12, 'none');
insert into lobcompanytaxes(id, companyid) values (12,7);


--------------------------Vendor Taxes-------------------------------

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (13,  '0013', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة نقل", "en": "Shipping tax"}','VendorTaxes', 10, 'none');
insert into lobvendortaxes(id, vendorid) values (13,46);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (14,  '0014', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة خدمة", "en": "Service tax"}','VendorTaxes', 2, 'none');
insert into lobvendortaxes(id, vendorid) values (14,46);

INSERT INTO lobtaxinfo (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name , objectTypeCode, percentage, description)
VALUES (15,  '0015', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"ar": "ضريبة نقل", "en": "Shipping tax"}','VendorTaxes', 10, 'none');
insert into lobvendortaxes(id, vendorid) values (15,45);

