delete from DObJournalEntry ;

ALTER SEQUENCE DObJournalEntry_id_seq RESTART WITH 3000;

DELETE
from EntityUserCode
WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2019000000');

INSERT INTO DObJournalEntry(id, code, creationdate, modifieddate, creationinfo,
                                           modificationinfo, currentStates, dueDate,
                                           companyId, purchaseUnitId, objectTypeCode)
VALUES (0, '2019000000', '2019-08-02 10:30:00', '2019-08-02 10:30:00','Shady.Abdelatif',
        'Shady.Abdelatif', '["Active"]','2019-09-02 10:30:00',
        10, 23, 'VendorInvoice');
