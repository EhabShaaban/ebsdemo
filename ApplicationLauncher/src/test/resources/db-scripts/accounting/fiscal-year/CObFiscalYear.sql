delete from cobfiscalperiod;

INSERT INTO public.cobfiscalperiod (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name, fromdate, todate)
VALUES (1, '0001', '2021-02-08 13:42:29.468475', '2021-02-08 13:42:29.468475', 'Admin', 'Admin', '["Closed"]', '{"en": "2019" , "ar": "2019"}', '2018-12-31 22:00:00.000000', '2019-12-31 21:59:00.000000');

INSERT INTO public.cobfiscalperiod (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name, fromdate, todate)
VALUES (2, '0002', '2021-02-08 13:42:29.474928', '2021-02-08 13:42:29.474928', 'Admin', 'Admin', '["Closed"]', '{"en": "2020" , "ar": "2020"}', '2019-12-31 22:00:00.000000', '2020-12-31 21:59:00.000000');

INSERT INTO public.cobfiscalperiod (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name, fromdate, todate)
VALUES (3, '0003', '2021-02-08 13:42:29.481768', '2021-02-08 13:42:29.481768', 'Admin', 'Admin', '["Active"]', '{"en": "2021" , "ar": "2021"}', '2020-12-31 22:00:00.000000', '2021-12-31 21:59:00.000000');
