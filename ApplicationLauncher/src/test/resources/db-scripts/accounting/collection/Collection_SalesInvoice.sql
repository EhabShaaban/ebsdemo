INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (3, 'SALES_ORDER', '2020-09-28 00:00:00', '2020-09-28 00:00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"DownPayment for Sales Order","ar":"دفعة مقدمة لطلب بيع"}');

INSERT into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (82, '2020000003','2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', '["Active"]', 'C_SO', 1000072);

INSERT INTO public.dobcollection (id, collectiontype)
VALUES (82, 'CUSTOMER');

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, bankAccountId, purchaseunitid, objectTypeCode)
VALUES (2512, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 7, 14, 19, 'Collection');

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, businessPartnerid, refdocumenttypeid, amount, currencyid, description, collectionMethod, bankAccountId, treasuryId)
VALUES (12, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 73, 3, 197.200, 1, NULL, 'BANK', 14, NULL);

INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (12, 83);