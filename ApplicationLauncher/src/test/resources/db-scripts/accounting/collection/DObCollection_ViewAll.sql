DELETE FROM dobcollection where id between 90 and 94;
DELETE FROM dobaccountingdocument WHERE objecttypecode in ('C_NR','C_SI','C_SO') and id between 90 and 94;

DELETE FROM IObAccountingDocumentCompanyData WHERE objectTypeCode = 'Collection' AND id between 2521 and 2524;

ALTER SEQUENCE dobcollection_id_seq RESTART WITH 83;

DELETE from EntityUserCode WHERE type = 'DObCollection';
INSERT INTO EntityUserCode (type, code) values ('DObCollection', '2020000002');
----------------------------------------------------------------------------------------
insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId)
            values (90,  '2019000001', '2019-12-04 11:27:02.000000', '2019-12-04 11:27:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (90, 'CUSTOMER');

INSERT INTO public.IObAccountingDocumentCompanyData(id, refInstanceId, creationDate, modifiedDate, creationInfo, modificationInfo, companyId, bankAccountId, purchaseunitid, objectTypeCode)
values(2520, 90,'2019-12-19 08:14:26.000000', '2019-12-19 08:14:26.000000', 'gehad', 'gehad', 10, null,19,'Collection');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId)
           values (91, '2019000002', '2019-12-17 14:09:20.000000', '2019-12-17 14:09:22.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (91, 'CUSTOMER');

INSERT INTO public.IObAccountingDocumentCompanyData(id, refInstanceId, creationDate, modifiedDate, creationInfo, modificationInfo, companyId, bankAccountId, purchaseunitid, objectTypeCode)
values(2521, 91,'2019-12-19 08:14:26.000000', '2019-12-19 08:14:26.000000', 'gehad', 'gehad', 7, null,19,'Collection');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (92, '2019000003', '2019-12-17 14:10:17.000000', '2019-12-17 14:10:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_SI', 16);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (92, 'CUSTOMER');

INSERT INTO public.IObAccountingDocumentCompanyData(id, refInstanceId, creationDate, modifiedDate, creationInfo, modificationInfo, companyId, bankAccountId, purchaseunitid, objectTypeCode)
values(2522, 92,'2019-12-19 08:14:26.000000', '2019-12-19 08:14:26.000000', 'gehad', 'gehad', 10, null,1,'Collection');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (93, '2020000001', '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'A.Hamed', 'A.Hamed', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (93, 'CUSTOMER');

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, bankAccountId, purchaseunitid, objectTypeCode)
VALUES (2523, 93, '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'A.Hamed', 'A.Hamed', 7, NULL, 19, 'Collection');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (94, '2020000002', '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'hr1', 'hr1', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (94, 'CUSTOMER');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, bankAccountId, purchaseunitid, objectTypeCode)
VALUES (2524, 94, '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'hr1', 'hr1', 7, 14, 19, 'Collection');