DELETE FROM IObAccountingDocumentActivationDetails where objecttypecode in ('Collection') AND id between 70 AND 75;
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 76;
------------------------------------------------------------------------------------------
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)
VALUES (71, 74, '2019-12-19 08:14:24.000000', '2019-12-19 08:14:26.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2019-12-22 10:30:00', 'Collection', 'Admin from BDKCompanyCode', '2019-12-20 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)
VALUES (72, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', '2020-10-11 10:30:00', 'Collection', 'Admin from BDKCompanyCode', '2020-10-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)
VALUES (73, 84, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', '2020-10-11 10:30:00', 'Collection', 'Admin from BDKCompanyCode', '2020-10-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)
VALUES (74, 85, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', '2020-10-11 10:30:00', 'Collection', 'Admin from BDKCompanyCode', '2020-10-10 10:30:00' , 13, 1);

insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, duedate, objecttypecode , accountant, activationDate , exchangerateid, currencyprice)
VALUES (75, 86, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '2020-11-04 10:30:00', 'Collection', 'Admin from BDKCompanyCode', '2020-11-04 10:00:00' , 13, 1);
