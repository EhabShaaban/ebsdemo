delete from dobjournalentry where id in (50);
delete from dobcollectionjournalentry where id in (50);

DELETE
from EntityUserCode
WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000050');

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (50, '2020000050', '2019-12-18 10:30:00', '2019-12-18 10:30:00', 'hr1', 'hr1', '["Active"]', null, '2019-12-24 10:30:00', 7, 19, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (50, 91);
