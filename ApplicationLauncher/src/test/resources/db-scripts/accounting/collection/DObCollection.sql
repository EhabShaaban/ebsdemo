DELETE FROM dobaccountingdocument WHERE objecttypecode in ('C_NR','C_SI','C_SO') and id between 74 and 89;
DELETE FROM dobaccountingdocument WHERE objecttypecode in ('C_NR','C_SI','C_SO') and id between 95 and 97;
DELETE FROM dobcollection where id between 74 and 89;
DELETE FROM dobcollection where id between 95 and 97;

ALTER SEQUENCE dobcollection_id_seq RESTART WITH 98;

DELETE from EntityUserCode WHERE type = 'DObCollection';
INSERT INTO EntityUserCode
(type, code) values ('DObCollection', '2020000015');
----------------------------------------------------------------------------------------------
insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId)
           values (74, '2019000050', '2019-12-19 08:14:24.000000', '2019-12-19 08:14:26.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (74, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId)
           values (75, '2019000051', '2019-12-17 14:10:17.000000', '2019-12-17 14:10:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (75, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (76, '2019000052', '2019-12-17 14:10:17.000000', '2019-12-17 14:10:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (76, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (77, '2019000053', '2019-12-31 12:56:24.000000', '2019-12-31 12:56:25.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (77, 'CUSTOMER');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (78, '2019000004', '2020-01-01 12:21:20.000000', '2020-01-01 12:21:22.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (78, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (79, '2019000005', '2020-01-01 12:23:50.000000', '2020-01-01 12:23:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (79, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (80, '2019000060', '2020-03-04 08:56:00.451000', '2020-03-04 08:56:00.451000', 'hr1', 'hr1', '["Draft"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (80, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (81, '2019000061', '2020-03-04 08:56:00.451000', '2020-03-04 08:56:26.197000', 'hr1', 'hr1', '["Draft"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (81, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (82, '2020000003','2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', '["Active"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (82, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (83, '2020000004','2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (83, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (84, '2020000005','2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', '["Active"]', 'C_NR', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (84, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (85, '2020000006','2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', '["Active"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (85, 'CUSTOMER');


INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (86, '2020000007','2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '["Active"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (86, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (87, '2020000008','2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (87, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (88, '2020000009','2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (88, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (89, '2020000010','2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (89, 'CUSTOMER');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (95, '2020000011', '2020-12-02 09:00:00', '2020-12-02 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (95, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (96, '2020000012', '2020-12-02 09:00:00', '2020-12-02 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SI', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (96, 'CUSTOMER');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (97, '2020000013', '2020-12-03 09:00:00', '2020-12-03 09:00:00', 'admin', 'admin', '["Draft"]', 'C_SO', 1000072);
INSERT INTO public.dobcollection (id, collectiontype)
VALUES (97, 'CUSTOMER');