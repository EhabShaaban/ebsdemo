delete from dobjournalentry where id between 51 and 54;
delete from dobcollectionjournalentry where id between 51 and 54;

DELETE
from EntityUserCode
WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000058');
------------------------------------------------------------------------------

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (51, '2020000051', '2019-12-20 10:30:00', '2019-12-20 10:30:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', null, '2019-12-22 10:30:00', 10, 19, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (51, 74);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (52, '2020000052', '2020-10-10 10:30:00', '2020-10-10 10:30:00', 'A.Hamed', 'A.Hamed', '["Active"]', null, '2020-10-11 10:30:00', 7, 19, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (52, 82);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (53, '2020000053', '2020-10-10 10:30:00', '2020-10-10 10:30:00', 'H.Hassan', 'H.Hassan', '["Active"]', null, '2020-10-11 10:30:00', 7, 19, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (53, 84);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (54, '2020000054', '2020-10-10 10:30:00', '2020-10-10 10:30:00', 'H.Hassan', 'H.Hassan', '["Active"]', null, '2020-10-11 10:30:00', 10, 1, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (54, 85);

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (55, '2020000055', '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '["Active"]', null, '2020-11-04 10:30:00', 7, 19, 'Collection');
INSERT INTO public.dobcollectionjournalentry (id, documentreferenceid)
VALUES (55, 86);
