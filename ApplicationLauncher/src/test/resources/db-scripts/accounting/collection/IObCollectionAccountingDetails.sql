DELETE FROM iobaccountingdocumentcustomeraccountingdetails
where id in(106,120,124,125) ;

DELETE FROM IObAccountingDocumentNotesReceivableAccountingDetails
where id in(122) ;

DELETE FROM iobaccountingdocumentbankaccountingdetails
where id in(119,121,125) ;

DELETE FROM iobaccountingdocumenttreasuryaccountingdetails
where id in(105,123) ;

DELETE FROM iobaccountingdocumentaccountingdetails
WHERE objecttypecode = 'Collection'
and refinstanceid in (74,82,85,86,84);

--------------------------------------------2019000050----------------------------------------------------------------
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (105, 74, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'hr1', 'hr1', 'Treasuries', 'DEBIT', 13, 300,'Collection');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (105, 2);
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (106, 74, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'hr1', 'hr1', 'Local_Customer', 'CREDIT', 11, 300,'Collection');
INSERT INTO public.iobaccountingdocumentcustomeraccountingdetails (id, glsubaccountid)
VALUES (106, 66);

---------------------------------------------2020000003----------------------------------------------------

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (119, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 'Banks', 'DEBIT', 15, 20,'Collection');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (119, 15);
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (120, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 'Local_Customer', 'CREDIT', 11, 20,'Collection');
INSERT INTO public.iobaccountingdocumentcustomeraccountingdetails (id, glsubaccountid)
VALUES (120, 73);
---------------------------------------------2020000006----------------------------------------------------
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (123, 85, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', 'Treasuries', 'DEBIT', 13, 300,'Collection');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (123, 1);
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (124, 85, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 'Local_Customer', 'CREDIT', 11, 300,'Collection');
INSERT INTO public.iobaccountingdocumentcustomeraccountingdetails (id, glsubaccountid)
VALUES (124, 72);
---------------------------------------------2020000007----------------------------------------------------
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (125, 86, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 'Banks', 'DEBIT', 15, 10,'Collection');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (125, 14);
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,modifieddate, creationinfo,modificationinfo, subledger,accountingentry, glaccountid,amount, objecttypecode)
VALUES (126, 86, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 'Local_Customer', 'CREDIT', 11, 10,'Collection');
INSERT INTO public.iobaccountingdocumentcustomeraccountingdetails (id, glsubaccountid)
VALUES (126, 73);
---------------------------------------------------------------------2020000005----------------------------------------------------------------
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (121, 84, '2020-10-04 09:00:00', '2020-10-04 09:00:00',
        'H.Hassan', 'H.Hassan', 'Banks', 'DEBIT', 15, 20,
        'Collection');

INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (121, 15);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (122, 84, '2020-10-04 09:00:00', '2020-10-04 09:00:00',
        'H.Hassan', 'H.Hassan', 'Notes_Receivables', 'CREDIT', 26,
        20, 'Collection');
INSERT INTO public.IObAccountingDocumentNotesReceivableAccountingDetails (id, glsubaccountid)
VALUES (122, 2029);

