DELETE FROM cobcollectiontype;

INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1, 'SALES_INVOICE', '2019-12-04 11:27:02.000000', '2019-12-04 11:27:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"Against Sales Invoice","ar":"تحصيل مقابل فاتورة بيع"}');

INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (2, 'NOTES_RECEIVABLE', '2019-12-04 11:27:02.000000', '2019-12-04 11:27:04.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"Against Notes Receivable","ar":"تحصيل مقابل ورقة قبض"}');

INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (3, 'SALES_ORDER', '2020-09-28 00:00:00', '2020-09-28 00:00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"DownPayment for Sales Order","ar":"دفعة مقدمة لطلب بيع"}');

INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (4, 'DEBIT_NOTE', '2021-01-05 00:00:00', '2021-01-05 00:00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"Against Debit Note","ar":"تحصيل مقابل مذكرة خصم"}');

INSERT INTO public.cobcollectiontype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (5, 'PAYMENT_REQUEST', '2021-01-19 00:00:00', '2021-01-19 00:00:00', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', '{"en":"Against Payment Request","ar":"تحصيل مقابل إذن صرف"}');
