DELETE
FROM IObAccountingDocumentActivationDetails
WHERE id > 10
  AND id < 12;
DELETE
FROM iobcollectionsalesinvoicedetails
WHERE id < 4;
DELETE
FROM iobcollectiondetails
WHERE id < 4;

ALTER SEQUENCE iobcollectiondetails_id_seq RESTART WITH 6;
ALTER SEQUENCE iobcollectionsalesinvoicedetails_id_seq RESTART WITH 6;
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 13;
-------------------------------------------------------------------------------------------

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid, description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (20, 90, '2019-12-04 11:31:56.000000', '2019-12-04 11:31:58.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 1, 1000, 1, NULL, 'CASH', NULL, 2);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid, description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (21, 91, '2019-12-17 14:24:23.000000', '2019-12-17 14:24:24.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 67, 1, 2000.123456, 1, NULL, 'CASH', NULL, 2);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid, description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (22, 92, '2019-12-17 14:26:12.000000', '2019-12-17 14:26:13.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 67, 1, 3000, 1, NULL, 'CASH', NULL, 1);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid, description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (23, 93, '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'A.Hamed', 'A.Hamed', 73, 3, 20, 1, NULL, 'CASH', NULL,
        2);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid, description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (24, 94, '2020-09-29 09:00:00', '2020-09-29 09:00:00', 'hr1', 'hr1', 66, 2, 20, 1, NULL, 'BANK', 14, NULL);

INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (20, 62);
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (21, 57);
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (22, 61);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (23, 67);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (24, 2037);