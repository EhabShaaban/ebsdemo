DELETE
FROM iobcollectionsalesinvoicedetails
WHERE id IN (4, 7, 10, 25, 26);
ALTER SEQUENCE iobcollectionsalesinvoicedetails_id_seq RESTART WITH 28;

DELETE
FROM iobcollectionnotesreceivabledetails
WHERE id IN (2028, 2029, 2029, 2029, 2034, 2037, 2037);
ALTER SEQUENCE iobcollectionnotesreceivabledetails_id_seq RESTART WITH 28;

DELETE
FROM iobcollectionsalesorderdetails
WHERE id IN (12, 13, 15, 16, 17, 18, 19, 27);
ALTER SEQUENCE iobcollectionsalesorderdetails_id_seq RESTART WITH 28;

DELETE
FROM iobcollectiondetails
WHERE id BETWEEN 4 AND 19;
DELETE
FROM iobcollectiondetails
WHERE id BETWEEN 25 AND 27;
ALTER SEQUENCE iobcollectiondetails_id_seq RESTART WITH 28;
-------------------------------------------------------------------------------------------

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod)
VALUES (4, 74, '2019-12-17 14:26:12.000000', '2019-12-17 14:26:13.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 1, 14380382.0, 1, NULL, 'CASH');
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (4, 51);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, amount, currencyid, description,
                                         collectionMethod,
                                         refdocumenttypeid, bankAccountId, treasuryId)
VALUES (5, 75, '2019-12-17 14:26:12.000000', '2019-12-17 14:26:13.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 330, 1, NULL, 'CASH', 2, NULL, 2);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (5, 2034);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, amount, currencyid, description,
                                         collectionMethod,
                                         refdocumenttypeid)
VALUES (6, 76, '2019-12-17 14:26:12.000000', '2019-12-17 14:26:13.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 67, 3000, 1, NULL, 'CASH', 2);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (6, 2029);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, amount, currencyid, description,
                                         collectionMethod,
                                         refdocumenttypeid, bankAccountId, treasuryId)
VALUES (7, 77, '2019-12-31 12:57:53.000000', '2019-12-31 12:57:55.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 7080.25, 1, NULL, 'CASH', 1, NULL, 2);
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (7, 62);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, amount, currencyid, description,
                                         collectionMethod,
                                         refdocumenttypeid, bankAccountId, treasuryId)
VALUES (8, 78, '2020-01-01 12:22:52.000000', '2020-01-01 12:22:55.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 180, 1, NULL, 'CASH', 2, NULL, 2);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (8, 2029);


INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, amount, currencyid, description,
                                         collectionMethod,
                                         refdocumenttypeid, bankAccountId, treasuryId)
VALUES (9, 79, '2020-01-01 12:24:20.000000', '2020-01-01 12:24:21.000000',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 66, 3000, 1, NULL, 'CASH', 2, NULL, 2);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (9, 2029);


INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (10, 80, '2020-03-04 08:56:03.278000', '2020-03-04 08:56:03.278000', 'hr1', 'hr1', 66, 1, 20,
        1, NULL, 'BANK',
        17, NULL);
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (10, 62);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId)
VALUES (11, 81, '2020-03-04 08:56:29.064000', '2020-03-04 08:56:29.064000', 'hr1', 'hr1', 66, 2, 20,
        1, NULL, 'BANK',
        14);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (11, 2037);


INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId)
VALUES (25, 95, '2020-12-03 09:00:00.000000', '2020-12-03 09:00:00.000000', 'admin',
        'Admin from BDKCompanyCode', 66, 1, 600, 1, NULL, 'BANK', 14);
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (25, 2010);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod)
VALUES (26, 96, '2020-12-03 09:00:00.000000', '2020-12-03 09:00:00.000000', 'admin',
        'admin', 66, 1, 544, 1, NULL, 'BANK');
INSERT INTO public.iobcollectionsalesinvoicedetails (id, salesinvoiceid)
VALUES (26, 2010);

---------------------------------------------------------C_SO---------------------------------------------------------------------------------------------------------------------------------

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (12, 82, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 73, 3, 197.200,
        1, NULL, 'BANK', 14,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (12, 83);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId)
VALUES (13, 83, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'A.Hamed', 'A.Hamed', 72, 3, 20, 1,
        NULL, 'BANK', 14);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (13, 53);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId)
VALUES (14, 84, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', 66, 2, 20, 1,
        NULL, 'BANK', 14);
INSERT INTO public.iobcollectionnotesreceivabledetails (id, notesreceivableid)
VALUES (14, 2029);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, treasuryId)
VALUES (15, 85, '2020-10-04 09:00:00', '2020-10-04 09:00:00', 'H.Hassan', 'H.Hassan', 72, 3, 300, 1,
        NULL, 'CASH', 1);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (15, 53);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (16, 86, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 73, 3, 300, 1,
        NULL, 'BANK', 14,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (16, 85);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (17, 87, '2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', 73, 3, 197.200, 1,
        NULL, 'BANK', 14,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (17, 83);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (18, 88, '2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', 73, 3, null, 1,
        NULL, 'BANK', 14,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (18, 53);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (19, 89, '2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', 73, 3, null, 1,
        NULL, 'CASH', null,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (19, 53);

INSERT INTO public.iobcollectiondetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo,
                                         businessPartnerid, refdocumenttypeid, amount, currencyid,
                                         description,
                                         collectionMethod, bankAccountId, treasuryId)
VALUES (27, 97, '2020-11-26 09:00:00', '2020-11-26 09:00:00', 'admin', 'admin', 73, 3, 20.0, 1,
        NULL, 'BANK', 14,
        NULL);
INSERT INTO public.iobcollectionsalesorderdetails (id, salesorderid)
VALUES (27, 70);