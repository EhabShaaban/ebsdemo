
DELETE FROM iobaccountingdocumentcustomeraccountingdetails
where id in(108) ;

DELETE FROM iobaccountingdocumenttreasuryaccountingdetails
where id = 107;

DELETE FROM iobaccountingdocumentaccountingdetails
WHERE objecttypecode = 'Collection'
  and refinstanceid in (91);

--------------------------------------2019000002------------------------------------------------
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (107, 91, '2019-12-04 09:27:02.000000', '2019-12-04 09:27:02.000000',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'Treasuries', 'DEBIT', 13, 1000,
        'Collection');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (107, 2);
INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (108, 91, '2019-12-04 09:27:02.000000', '2019-12-04 09:27:02.000000',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 'Local_Customer', 'CREDIT', 11,
        1000, 'Collection');
INSERT INTO public.iobaccountingdocumentcustomeraccountingdetails (id, glsubaccountid)
VALUES (108, 67);
