TRUNCATE iobsalesinvoicetaxes CASCADE;
TRUNCATE IObSalesInvoiceSummary CASCADE;
TRUNCATE DObSalesInvoice CASCADE;

ALTER SEQUENCE DObSalesInvoice_id_seq RESTART WITH 1;
