TRUNCATE dobjournalentry CASCADE;
TRUNCATE cobfiscalperiod CASCADE;

TRUNCATE dobaccountingdocument CASCADE;

ALTER SEQUENCE iobmonetarynotesreferencedocuments_id_seq RESTART WITH 20;
DELETE from EntityUserCode WHERE type = 'DObNotesReceivable';
INSERT INTO EntityUserCode (type, code) values ('DObNotesReceivable', '2019000099');
