DELETE FROM iobvendorinvoicedetailsdownpayment  where id > 0 and id < 6;
DELETE FROM iobvendorinvoicepurchaseorder  where id > 1 and id < 42;
DELETE FROM iobvendorinvoicepurchaseorder where id in (135, 136);
delete from IObVendorInvoiceDetails where id > 1 and id < 42;
DELETE FROM IObVendorInvoiceDetails where id in (135, 136);
ALTER SEQUENCE iobvendorinvoicepurchaseorder_id_seq RESTART WITH 3000;
ALTER SEQUENCE iobvendorinvoicedetailsdownpayment_id_seq RESTART WITH 6;

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (34, 34, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023700', '2018-04-24 06:31:00', 46);


INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (2, 2, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023700', '2018-04-24 06:31:56.332658', 46);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (4, 4, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023700', '2018-04-24 06:31:56.332658', 46);

INSERT INTO iobvendorinvoicedetailsdownpayment (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, downpaymentid, downpaymentamount)
VALUES (4, 4, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1041 ,10);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (36, 36, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023110', '2018-04-24 06:31:56.332658', 46);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (39, 39, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023110', '2018-04-24 06:31:56.332658', 45);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (40, 40, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023800', '2018-04-24 06:31:56.332658', 46);

INSERT INTO public.iobvendorinvoicepurchaseorder(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, purchaseorderid, currencyid, paymenttermid)
    VALUES (2, 2, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode',
            'Admin from BDKCompanyCode', 1010, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, purchaseorderid, currencyid, paymenttermid)
VALUES (4, 4, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode',
		   'Admin from BDKCompanyCode', 2087, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (34, 34, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1049, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (36, 36, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1008, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (39, 39, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1056, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (40, 40, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1047, 2, 3);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                           modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (41, 41, '2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', '1200120301023130',
        '2020-12-09 09:00:00', 46);

INSERT INTO public.iobvendorinvoicedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, invoicenumber, invoiceDate, vendorId)
VALUES (135, 2022, '2020-12-17 15:23:46.142000', '2020-12-17 15:24:41.909000', 'hr1', 'hr1', '1200120301023700', '2018-04-24 00:00:00.000000', 46);

INSERT INTO public.iobvendorinvoicedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, invoicenumber, invoiceDate, vendorId)
VALUES (136, 2023, '2020-12-17 12:29:52.806000', '2020-12-17 12:30:15.841000', 'hr1', 'hr1', '12121212', '2020-12-18 00:00:00.000000', 46);
----------------------------------VI_PO-------------------------------------
INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                                 creationinfo,
                                                 modificationinfo, purchaseorderid, currencyid,
                                                 paymenttermid)
VALUES (41, 41, '2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', 1001, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, currencyid, paymenttermid)
VALUES (135, 2022, '2020-12-17 15:23:46.113000', '2020-12-17 15:24:41.907000', 'hr1', 'hr1', 2087, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, currencyid, paymenttermid)
VALUES (136, 2023, '2020-12-17 12:29:52.804000', '2020-12-17 12:30:15.839000', 'hr1', 'hr1', 2091, 1, 6);


INSERT INTO public.iobvendorinvoicedetailsdownpayment (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, downpaymentid, downpaymentamount)
VALUES (5, 135, '2020-12-17 15:24:41.903000', '2020-12-17 15:24:41.910000', 'hr1', 'hr1', 1041, 10);
