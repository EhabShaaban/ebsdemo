delete from IObVendorInvoiceDetails where id > 101 and id < 135;
delete from iobvendorinvoicepurchaseorder where id > 101 and id < 135;
ALTER SEQUENCE iobvendorinvoicepurchaseorder_id_seq RESTART WITH 3000;
ALTER SEQUENCE iobvendorinvoicedetailsdownpayment_id_seq RESTART WITH 6;


INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (133, 133, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023100', '2018-04-24 06:31:00', 76);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (131, 131, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', null, null, 45);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (109, 109, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023400', '2018-04-24 06:31:56.332658', 45);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (108, 108, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023500', '2018-04-24 06:31:56.332658', 56);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (107, 107, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023600', '2018-04-24 06:31:00', 56);


INSERT INTO iobvendorinvoicedetailsdownpayment (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, downpaymentid, downpaymentamount)
VALUES (2, 107, '2018-08-02 07:06:02.471551', '2018-08-02 07:06:02.471551', 'hr1', 'hr1', 1012 ,100.0);


INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (105, 105, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023800', '2018-04-24 06:31:56.332658', 46);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (104, 104, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023110', '2018-04-24 06:31:56.332658', 46);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (103, 103, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode',
            'Admin from BDKCompanyCode', '1200120301023120','2019-03-01 11:00:00', 46);

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                     modificationinfo, invoiceNumber, invoiceDate, vendorId)
VALUES (102, 102, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', '1200120301023130', '2018-04-24 06:31:00', 46);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (133, 133, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1017, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (131, 131, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1042, null, 3);


INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (109, 109, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1003, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (108, 108, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1044, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (107, 107, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1045, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (105, 105, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 2087, 2, 3);


INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (104, 104, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1049, 2, 3);


INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (103, 103, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1022, 2, 3);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo,
                                           modificationinfo, purchaseorderid, currencyid,
                                           paymenttermid)
VALUES (102, 102, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1001, 2, 3);

-- INSERT INTO public.iobvendorinvoicedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, invoicenumber, invoiceDate, vendorId)
-- VALUES (134, 2015, '2020-12-06 11:05:26.107000', '2020-12-06 11:06:35.095000', 'hr1', 'hr1', '202045202045', '2020-12-06 00:00:00.000000', 46);
--
-- INSERT INTO public.iobvendorinvoicepurchaseorder (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, currencyid, paymenttermid)
-- VALUES (134, 2015, '2020-12-06 11:05:26.097000', '2020-12-06 11:06:35.093000', 'hr1', 'hr1',
--         (SELECT id FROM dobpurchaseordergeneralmodel WHERE code = '2020000045'), 2, 3);