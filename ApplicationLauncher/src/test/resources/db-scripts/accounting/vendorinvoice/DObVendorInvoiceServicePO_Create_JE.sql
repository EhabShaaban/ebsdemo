DELETE FROM ioborderitem where id = 2100;
DELETE FROM iobpurchaseorderfulfillervendor where id = 2100;
DELETE FROM iobordercompany where id = 2100;
DELETE FROM dobpurchaseorder where id = 2100;
DELETE FROM doborderdocument where id = 2100;

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2100, '2020000100', '2020-08-05 11:02:00', '2020-08-05 11:02:00', 'admin', 'admin','["Confirmed"]', 'SERVICE_PO', 37);

INSERT INTO public.dobpurchaseorder (id) VALUES(2100);

INSERT INTO public.iobordercompany (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, companyid, businessunitid, partyrole, bankaccountid)
VALUES (2100, 2100, '2020-08-05 11:02:00', '2020-08-05 11:02:00', 'Admin', 'Admin', 10, 19, 'BILL_TO', null);

INSERT INTO iobpurchaseorderfulfillervendor (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, referencePoId)
VALUES (2100, 2100, '2020-08-05 11:02:00', '2020-08-05 11:02:00', 'Admin', 'Admin', 46, 1017);

INSERT INTO public.ioborderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, orderunitid, quantity, price)
VALUES (2100, 2100, '2020-08-05 11:02:00', '2020-08-05 11:02:00', 'Admin', 'Admin', 190, 23, 20, 200);


DELETE FROM iobvendorinvoiceitem WHERE id = 100;
DELETE FROM iobvendorinvoicepurchaseorder where id = 100;
DELETE FROM IObVendorInvoiceDetails where id = 100;
DELETE FROM IObAccountingDocumentCompanyData WHERE id = 500 and objecttypecode = 'VendorInvoice';
DELETE FROM dobvendorinvoice where id = 100;
DELETE FROM dobaccountingdocument where objecttypecode='VendorInvoice' and id = 100;

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates , objectTypeCode)
values(100, '2019000100', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Shady.Abdelatif', 'Shady.Abdelatif', '["Draft"]', 'VendorInvoice') ;
INSERT INTO dobvendorInvoice(id, typeId) VALUES (100, 4);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid,creationdate, modifieddate, creationinfo, modificationinfo, purchaseUnitId, companyId, bankAccountId, objectTypeCode)
VALUES (500, 100, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',19, 10, null, 'VendorInvoice');

INSERT INTO public.IObVendorInvoiceDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, invoiceNumber, invoiceDate, vendorid)
VALUES (100, 100, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1200120301023120', '2019-03-01 11:00:00', 46);

INSERT INTO public.iobvendorinvoicepurchaseorder(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, currencyid, paymenttermid)
VALUES (100, 100, '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2100, 2, 3);
        
INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
VALUES (100, 100, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed', 'Gehan.Ahmed',190,23,23,20,200);



