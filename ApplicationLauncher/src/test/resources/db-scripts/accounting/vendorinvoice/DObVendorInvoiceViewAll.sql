
DELETE FROM dobaccountingdocument where isVendorInvoice(objecttypecode) and id>100 and id < 134;
DELETE FROM dobaccountingdocument where isVendorInvoice(objecttypecode) and id = 2015;

SELECT setval('dobaccountingdocument_id_seq', (select max(id) from dobaccountingdocument) );
ALTER SEQUENCE IObVendorInvoiceDetails_id_seq RESTART WITH 3000;

DELETE
from EntityUserCode
WHERE type = 'DObVendorInvoice';

INSERT INTO EntityUserCode (type, code)
values ('DObVendorInvoice', '2020000004');

--dobvendorInvoice
insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates , objectTypeCode, documentownerid)values(133, '2019000133', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Draft"]', 'LOCAL_GOODS_INVOICE', 34) ;
INSERT INTO public.dobvendorInvoice(id)
VALUES (133);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentownerid)values(131, '2019000131', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Draft"]', 'CUSTOM_TRUSTEE_INVOICE', 34) ;
INSERT INTO public.dobvendorInvoice(id)
VALUES (131);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (109, '2019000109', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Posted"]', 'INSURANCE_INVOICE', 34);
INSERT INTO public.dobvendorInvoice(id)
VALUES (109);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (108, '2019000108', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Gehan.Ahmed',
        'Gehan.Ahmed',
        '["Posted"]', 'LOCAL_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (108);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (107, '2019000107', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Draft"]', 'CUSTOM_TRUSTEE_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (107);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (105, '2019000105', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Draft"]', 'INSURANCE_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (105);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (104, '2019000104', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Shady.Abdelatif',
        'Shady.Abdelatif',
        '["Posted"]', 'IMPORT_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (104);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (103, '2019000103', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed',
        '["Draft"]', 'IMPORT_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (103);

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentownerid) values (102, '2019000102', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode',
        '["Draft"]', 'IMPORT_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (102);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2015, '2020000002', '2020-12-06 11:05:26.056000', '2020-12-06 11:06:57.754000', 'hr1', 'hr1', '["Posted"]', 'SHIPMENT_INVOICE', 34);

INSERT INTO public.dobvendorinvoice (id)
VALUES (2015);