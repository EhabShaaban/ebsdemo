DELETE FROM IObAccountingDocumentActivationDetails where id > 49 and id < 77 and objecttypecode = 'VendorInvoice';
DELETE FROM IObAccountingDocumentActivationDetails where id in (80, 81);
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 82;

--2019000104
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (50, 104, '2020-09-01 08:47:19.710000', '2020-09-01 08:47:19.732000', 'hr1', 'hr1', 'hr1', '2020-09-01 08:47:19.710000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000108
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (51, 108, '2020-09-01 09:53:43.044000', '2020-09-01 09:53:43.070000', 'hr1', 'hr1', 'hr1', '2020-09-01 09:53:43.044000', '2019-08-01 22:00:00.000000', 26, 16.5, 'VendorInvoice');

--2019000109
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (52, 109, '2020-09-01 09:57:54.256000', '2020-09-01 09:57:54.264000', 'hr1', 'hr1', 'hr1', '2020-09-01 09:57:54.256000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000034
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (53, 34, '2020-09-02 08:28:30.589000', '2020-09-02 08:28:30.597000', 'hr1', 'hr1', 'hr1', '2020-09-02 08:28:30.589000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000039
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (54, 39, '2020-09-02 09:10:50.154000', '2020-09-02 09:10:50.164000', 'hr1', 'hr1', 'hr1', '2020-09-02 09:10:50.154000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000040
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (55, 40, '2020-09-02 09:15:05.129000', '2020-09-02 09:15:05.137000', 'hr1', 'hr1', 'hr1', '2020-09-02 09:15:05.129000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000002
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (56, 2, '2020-09-02 09:37:37.790000', '2020-09-02 09:37:37.797000', 'hr1', 'hr1', 'hr1', '2020-09-02 09:37:37.790000', '2019-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2019000004
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (57, 4, '2020-09-03 08:07:04.260000', '2020-09-03 08:07:04.270000', 'hr1', 'hr1', 'hr1', '2020-09-03 08:07:04.260000', '2020-09-01 22:00:00.000000', 26, 10, 'VendorInvoice');

--2020000001
insert into public.IObAccountingDocumentActivationDetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (58, 41, '2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', 'hr1', '2020-12-09 10:00:00', '2020-12-09 09:30:00', 26, 10, 'VendorInvoice');

--2020000002
INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (76, 2015, '2020-12-06 09:06:57.748000', '2020-12-06 09:06:57.762000', 'hr1', 'hr1', 'hr1', '2020-12-06 09:06:57.748000', '2020-12-05 22:00:00.000000', 26, 10, 'VendorInvoice');

INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (80, 2022, '2020-12-17 13:25:56.478000', '2020-12-17 13:25:56.545000', 'hr1', 'hr1', 'hr1', '2020-12-17 13:25:56.478000', '2020-12-17 00:00:00.000000', 26, 17.44, 'VendorInvoice');

-- 2020000004
INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (81, 2023, '2020-12-17 10:30:30.214000', '2020-12-17 10:30:30.220000', 'hr1', 'hr1', 'hr1', '2020-12-17 10:30:30.213000', '2020-12-19 00:00:00.000000', 13, 1, 'VendorInvoice');