DELETE
FROM IObVendorInvoiceTaxes
where id < 22;
DELETE
FROM IObVendorInvoiceTaxes
where id IN (57, 58);

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (1, 133, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 2, '0001', '{"ar": "ضريبة القيمة المضافة", "en": "Vat"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (2, 133, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 20, '0003', '{"ar": "ضرائب عقارية", "en": "Real estate taxes"}');

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (5, 131, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 0, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (6, 131, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 0, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (7, 109, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 180, '0015', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');


INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (8, 108, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 48, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (9, 108, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 9.6, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');



INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (10, 107, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1035, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (11, 107, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 207, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (14, 105, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 0, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (15, 105, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 0, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');


INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (16, 104, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 105, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (17, 104, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 12, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');


INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (18, 103, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 30, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (19, 103, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 6, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');


INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (20, 102, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 90, '0013', '{"ar": "ضريبة نقل", "en": "Shipping tax"}');
INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          amount, code, name)
VALUES (21, 102, '2019-12-09 10:58:08.000000', '2019-12-09 10:58:10.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 18, '0014', '{"ar": "ضريبة خدمة", "en": "Service tax"}');

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          code, name, percentage, amount)
VALUES (57, 2015, '2020-12-06 11:05:26.212000', '2020-12-06 11:05:26.212000', 'hr1', 'hr1', '0014',
        '{"ar":"ضريبة خدمة","en":"Service tax"}', null, 144);

INSERT INTO public.iobvendorinvoicetaxes (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          code, name, percentage, amount)
VALUES (58, 2015, '2020-12-06 11:05:26.212000', '2020-12-06 11:05:26.212000', 'hr1', 'hr1', '0013',
        '{"ar":"ضريبة نقل","en":"Shipping tax"}', null, 720);