DELETE
FROM iobpaymentrequestinvoicepaymentdetails;
DELETE
FROM dobvendorinvoice
WHERE id > 0
  AND id < 42;
DELETE FROM dobvendorinvoice WHERE id in (2022, 2023);
DELETE
FROM dobaccountingdocument
WHERE isVendorInvoice(objecttypecode)
  AND id > 0
  AND id < 42;
DELETE FROM dobaccountingdocument WHERE id in (2022, 2023);
SELECT setval('dobaccountingdocument_id_seq', (select max(id) from dobaccountingdocument) );

DELETE
FROM EntityUserCode
WHERE type = 'DObVendorInvoice';

INSERT INTO EntityUserCode (type, code)
VALUES ('DObVendorInvoice', '2020000004');

-- Posted Invoices


INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                                  objectTypeCode, documentownerid)
VALUES (2, '2019000002', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'ADMIN FROM BDKCompanyCode',
        'ADMIN FROM BDKCompanyCode',
        '["Posted"] ', 'IMPORT_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (2);

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                                  objectTypeCode, documentownerid)
VALUES (4, '2019000004', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Shady.Abdelatif',
        'Shady.Abdelatif',
        '["Posted"] ', 'CUSTOM_TRUSTEE_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (4);

/* New */

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                                  objectTypeCode, documentownerid)
VALUES (34, '2019000034', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'ADMIN FROM BDKCompanyCode',
        'ADMIN FROM BDKCompanyCode',
        '["Posted"] ', 'SHIPMENT_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (34);

/* New */

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                                  objectTypeCode, documentownerid)
VALUES (36, '2019000036', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Shady.Abdelatif',
        'Shady.Abdelatif',
        '["Draft"] ', 'SHIPMENT_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (36);

/* New */

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                                  objectTypeCode, documentownerid)
VALUES (39, '2019000039', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Shady.Abdelatif',
        'Shady.Abdelatif',
        '["Posted"] ', 'SHIPMENT_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (39);

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
                                  modificationinfo, currentstates, objectTypeCode, documentownerid)
VALUES (40, '2019000040', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'ADMIN FROM BDKCompanyCode',
        'ADMIN FROM BDKCompanyCode',
        '["Posted"] ', 'LOCAL_GOODS_INVOICE', 34);
INSERT INTO public.dobvendorInvoice(id)
VALUES (40);

INSERT INTO dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
                                  modificationinfo, currentstates, objectTypeCode, documentownerid)
VALUES (41, '2020000001', '2020-12-09 09:00:00', '2020-12-09 09:00:00',
        'A.Hamed','A.Hamed',
        '["Posted"] ', 'IMPORT_GOODS_INVOICE', 34);
INSERT INTO dobvendorInvoice(id)
VALUES (41);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2022, '2020000003', '2020-12-17 15:23:46.027000', '2020-12-17 15:25:56.528000', 'hr1', 'hr1', '["Posted"]', 'CUSTOM_TRUSTEE_INVOICE', 34);

INSERT INTO public.dobvendorinvoice (id)
VALUES (2022);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2023, '2020000004', '2020-12-17 12:29:52.790000', '2020-12-17 12:30:30.215000', 'hr1', 'hr1', '["Posted"]', 'LOCAL_GOODS_INVOICE', 34);

INSERT INTO public.dobvendorinvoice (id)
VALUES (2023);