DELETE FROM iobvendorinvoiceitem WHERE id >14 and id < 27;
DELETE FROM iobvendorinvoiceitem WHERE id IN (29, 30, 31);
ALTER SEQUENCE iobvendorinvoiceitem_id_seq RESTART WITH 32;
----------------------------------------------------------------------------
INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (15, 2, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',34,23,43,50,4);

INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (16, 34, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',33,23,39,50,3);

INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (17, 34, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',33,23,39,100,2);

INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (18, 36, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',34,23,43,50,4);

INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (19, 39, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',191,23,37,130,90);

INSERT INTO public.iobvendorinvoiceitem(
    id, refinstanceid, creationdate, modifieddate, creationinfo,
    modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
VALUES (20, 4, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed',192,23,37,10,1000);

INSERT INTO public.iobvendorinvoiceitem(
    id, refinstanceid, creationdate, modifieddate, creationinfo,
    modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
VALUES (21, 4, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed',190,23,37,10,15);


INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (22, 40, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 33, 23, 39, 50, 3);
INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (23, 40, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 33, 23, 37, 100, 1.8);


INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                        modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                        quantityinorderunit, price)
VALUES (24, 41, '2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', 35, 4, 43, 100, 15);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                        modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                        quantityinorderunit, price)
VALUES (25, 41, '2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', 34, 23, 39, 20, 15);


INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                        modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                        quantityinorderunit, price)
VALUES (26, 41,'2020-12-09 09:00:00', '2020-12-09 09:00:00', 'A.Hamed', 'A.Hamed', 33, 23, 39, 40, 100);

INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (29, 2022, '2020-12-17 15:23:46.153000', '2020-12-17 15:23:46.153000', 'hr1', 'hr1', 190, 23, 23, 20, 700);

INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (30, 2022, '2020-12-17 15:23:46.168000', '2020-12-17 15:23:46.168000', 'hr1', 'hr1', 192, 23, 23, 16, 1500);

INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (31, 2023, '2020-12-17 12:29:52.802000', '2020-12-17 12:29:52.802000', 'hr1', 'hr1', 34, 23, 37, 1, 0.99);