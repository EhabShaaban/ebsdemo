DELETE
FROM iobvendorinvoiceitem where id < 15;
DELETE
FROM iobvendorinvoiceitem where id IN (27, 28);
ALTER SEQUENCE iobvendorinvoiceitem_id_seq RESTART WITH 29;

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (1, 103, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 34, 23, 37, 50, 3);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (2, 103, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 34, 23, 37, 100, 2);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (3, 102, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 35, 4, 43, 100, 15);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (4, 102, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 34, 23, 39, 20, 15);


INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (5, 102, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 33, 23, 39, 40, 100);


INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (7, 107, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 44, 16, 43, 50, 4);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (8, 107, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 36, 4, 4, 50, 3);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (9, 107, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 35, 4, 43, 50, 4);


INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (10, 104, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',33,23,39,50,3);

INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (11, 104, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',33,23,39,100,2);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (12, 109, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 191, 23, 37, 6, 90);

INSERT INTO public.iobvendorinvoiceitem(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                  modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid,
                                  quantityinorderunit, price)
VALUES (13, 108, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
        'Gehan.Ahmed', 33, 23, 37, 6, 80);


INSERT INTO public.iobvendorinvoiceitem(
            id, refinstanceid, creationdate, modifieddate, creationinfo,
            modificationinfo, itemid,baseunitofmeasureid,orderunitofmeasureid,quantityinorderunit,price)
    VALUES (14, 133, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Gehan.Ahmed',
            'Gehan.Ahmed',34,23,43,50,4);

INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (27, 2015, '2020-12-06 11:05:26.117000', '2020-12-06 11:05:26.117000', 'hr1', 'hr1', 190, 23, 23, 20, 200);

INSERT INTO public.iobvendorinvoiceitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, baseunitofmeasureid, orderunitofmeasureid, quantityinorderunit, price)
VALUES (28, 2015, '2020-12-06 11:05:26.128000', '2020-12-06 11:05:26.128000', 'hr1', 'hr1', 192, 23, 23, 16, 200);