DELETE FROM IObAccountingDocumentCompanyData WHERE objecttypecode = 'VendorInvoice';
DELETE FROM iobaccountingdocumentactivationdetails WHERE objecttypecode = 'VendorInvoice';
DELETE FROM dobjournalentry WHERE objecttypecode = 'VendorInvoice';
DELETE FROM dobaccountingdocument WHERE isVendorInvoice(objecttypecode);
SELECT setval('dobaccountingdocument_id_seq', (select max(id) from dobaccountingdocument) );
