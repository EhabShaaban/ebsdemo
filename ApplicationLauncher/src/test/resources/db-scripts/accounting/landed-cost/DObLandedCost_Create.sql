DELETE FROM IObLandedCostDetails;
DELETE FROM IObAccountingDocumentCompanyData WHERE objectTypeCode = 'LandedCost';
DELETE FROM IObLandedCostFactorItems;

DELETE FROM DObLandedCost;
DELETE FROM DObAccountingDocument WHERE objectTypeCode = 'LandedCost';

DELETE from EntityUserCode WHERE TYPE = 'DObLandedCost';

INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObLandedCost', '2020000004');


INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1905, '2020000001', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id,  typeId)
VALUES (1905,  1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1906, '2020000002', '2020-06-24 09:02:00', '2020-06-25 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1906, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1907, '2020000003', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["ActualValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1907, 2);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1908, '2020000004', '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["ActualValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1908, 1);