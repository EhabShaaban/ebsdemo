DELETE FROM DObAccountingDocument WHERE objectTypeCode = 'LandedCost' AND id > 1999 AND id < 2006;
DELETE FROM DObLandedCost WHERE id > 1999 AND id < 2006;

alter sequence DObAccountingDocument_id_seq RESTART with 2039;

DELETE from EntityUserCode WHERE TYPE = 'DObLandedCost';
INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObLandedCost', '2020100009');

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2000, '2020100000', '06-Mar-2020 12:10 PM', '06-Mar-2020 12:10 PM', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2000, 2);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2001, '2020100001', '06-Apr-2020 12:10 PM', '06-Apr-2020 12:10 PM', 'Gehan.Ahmed', 'Gehan.Ahmed', '["ActualValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2001, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2002, '2020100002', '06-May-2020 12:10 PM', '06-May-2020 12:10 PM', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2002, 2);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2003, '2020100003', '06-Jun-2020 12:10 PM', '06-Jun-2020 12:10 PM', 'hr1', 'hr1', '["EstimatedValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2003, 2);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2004, '2020100004', '06-Jul-2020 12:10 PM', '06-Jul-2020 12:10 PM', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2004, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2005, '2020100005', '06-Aug-2020 12:10 PM', '06-Aug-2020 12:10 PM', 'hr1', 'hr1', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (2005, 1);
