DELETE FROM doblandedcostjournalentry WHERE documentreferenceid > 1904 AND documentreferenceid < 2025;
DELETE FROM DObAccountingDocument WHERE objectTypeCode = 'LandedCost' AND id > 1904 AND id < 2025;
DELETE FROM DObLandedCost WHERE id > 1904 AND id < 2025;

DELETE from EntityUserCode WHERE TYPE = 'DObLandedCost';
INSERT INTO EntityUserCode (TYPE, code)
VALUES ('DObLandedCost', '2020100010');

alter sequence DObAccountingDocument_id_seq RESTART with 2039;

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1905, '2020000001', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id,  typeId)
VALUES (1905,  1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1906, '2020000002', '2020-06-24 09:02:00', '2020-06-25 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["EstimatedValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1906, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1907, '2020000003', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["ActualValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1907, 2);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1908, '2020000004', '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["ActualValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1908, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1909, '2020000005', '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1909, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1910, '2020000006', '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["EstimatedValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1910, 1);

INSERT INTO DObAccountingDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (1911, '2020000007', '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["EstimatedValuesCompleted"]', 'LandedCost', 40);
INSERT INTO DObLandedCost(id, typeId)
VALUES (1911, 1);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2016, '2020100006', '2020-12-06 14:10:38.270000', '2020-12-06 14:11:37.852000', 'hr1', 'hr1', '["EstimatedValuesCompleted"]', 'LandedCost', 40);
INSERT INTO public.doblandedcost (id, typeid)
VALUES (2016, 1);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2017, '2020100007', '2020-12-06 14:10:38.270000', '2020-12-06 14:11:37.852000', 'hr1', 'hr1', '["EstimatedValuesCompleted"]', 'LandedCost', 40);

INSERT INTO public.doblandedcost (id, typeid)
VALUES (2017, 1);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2018, '2020100008', '2020-12-09 09:00:00', '2020-12-09 14:38:28.446000', 'hr1', 'hr1', '["EstimatedValuesCompleted"]', 'LandedCost', 40);

INSERT INTO public.doblandedcost (id, typeid)
VALUES (2018, 2);

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
VALUES (2024, '2020100009', '2020-12-17 12:28:23.478000', '2020-12-17 12:33:11.506000', 'hr1', 'hr1', '["EstimatedValuesCompleted"]', 'LandedCost', 40);

INSERT INTO public.doblandedcost (id, typeid)
VALUES (2024, 2);