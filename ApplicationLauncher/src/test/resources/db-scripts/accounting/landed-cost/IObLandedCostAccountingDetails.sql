DELETE FROM iobaccountingdocumentaccountingdetails where id > 199 and id < 204 AND objectTypeCode = 'LandedCost';
ALTER SEQUENCE iobaccountingdocumentaccountingdetails_id_seq RESTART WITH 204;


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (202, 1908, '2020-09-28 09:00:00.000000', '2020-09-28 09:00:00.000000', 'Ahmed.Seif',
        'Ahmed.Seif', 'PO', 'CREDIT', 25, 1800, 'LandedCost');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (202, 1003);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (203, 1908, '2020-09-28 09:00:00.000000', '2020-09-28 09:00:00.000000', 'Ahmed.Seif',
        'Ahmed.Seif', 'PO', 'DEBIT', 63, 1800, 'LandedCost');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (203, 1003);