delete from dobjournalentry where id in (42);
delete from doblandedCostjournalentry where id in (42);

DELETE
from EntityUserCode
WHERE type = 'DObJournalEntry';

INSERT INTO EntityUserCode (type, code)
values ('DObJournalEntry', '2020000043');

INSERT INTO public.dobjournalentry (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, version, duedate, companyid, purchaseunitid, objecttypecode)
VALUES (42, '2020000042', '2020-08-06 08:08:36.809000', '2020-08-06 08:08:36.809000', 'hr1', 'hr1', '["Active"]', null, '2020-08-26 00:00:00.000000', 7, 19, 'LandedCost');
INSERT INTO public.dobLandedcostjournalentry (id, documentreferenceid)
VALUES (42, 2001);
