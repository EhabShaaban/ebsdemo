DELETE FROM IObLandedCostDetails where id IN (1, 2, 3, 4, 5, 6, 7, 106, 107, 108,109);
INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid , damageStock)
VALUES (1, 1905, '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1008, 40, true);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId,vendorinvoiceid, damageStock)
VALUES (2, 1906, '2020-06-24 09:02:00', '2020-06-25 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1010, 2 ,true);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid, damageStock)
VALUES (3, 1907, '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1047, 40, true);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid, damageStock)
VALUES (4, 1908, '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1003, 109, false);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid, damageStock)
VALUES (5, 1909, '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1001, null, false);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid, damageStock)
VALUES (6, 1910, '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1018, null, false);

INSERT INTO IObLandedCostDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo, purchaseOrderId, vendorinvoiceid, damageStock)
VALUES (7, 1911, '2020-06-24 09:02:00', '2020-06-26 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 1045, 107, false);

INSERT INTO public.ioblandedcostdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, vendorinvoiceid, damagestock)
VALUES (106, 2016, '2020-12-06 14:10:38.284000', '2020-12-06 14:10:38.284000', 'hr1', 'hr1', 1001, 41, false);

INSERT INTO public.ioblandedcostdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, vendorinvoiceid, damagestock)
VALUES (107, 2017, '2020-12-06 14:10:38.284000', '2020-12-06 14:10:38.284000', 'hr1', 'hr1', 1023, 4, false);

INSERT INTO public.ioblandedcostdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, vendorinvoiceid, damagestock)
VALUES (108, 2018, '2020-12-09 14:37:16.731000', '2020-12-09 14:37:16.731000', 'hr1', 'hr1', 1017, 133, false);

INSERT INTO public.ioblandedcostdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseorderid, vendorinvoiceid, damagestock)
VALUES (109, 2024, '2020-12-17 12:28:23.483000', '2020-12-17 12:28:23.483000', 'hr1', 'hr1', 2091, 2023, false);