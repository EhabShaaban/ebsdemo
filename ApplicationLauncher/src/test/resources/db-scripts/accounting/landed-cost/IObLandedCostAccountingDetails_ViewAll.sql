DELETE FROM IObAccountingDocumentActivationDetails where id > 299 and id < 302 AND objectTypeCode = 'LandedCost';
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 302;

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (300, 2001, '2020-09-28 09:00:00.000000', '2020-09-28 09:00:00.000000', 'Ahmed.Seif',
        'Ahmed.Seif', 'PO', 'CREDIT', 25, 1151.00 , 'LandedCost');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (300, 1049);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (301, 2001, '2020-09-28 09:00:00.000000', '2020-09-28 09:00:00.000000', 'Ahmed.Seif',
        'Ahmed.Seif', 'PO', 'DEBIT', 63, 1151.00, 'LandedCost');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (301, 1049);
