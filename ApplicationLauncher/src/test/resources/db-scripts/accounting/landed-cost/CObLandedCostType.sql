DELETE FROM DObAccountingDocument WHERE objectTypeCode = 'LandedCost';
DELETE FROM DObLandedCost;
DELETE FROM CObLandedCostType;

INSERT INTO CObLandedCostType(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1, 'IMPORT', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active"]', '{"en":"Landed Cost For Import PO","ar":"مستند تكاليف لامر شراء استيرادي"}');

INSERT INTO CObLandedCostType(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (2, 'LOCAL', '2020-06-24 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active"]', '{"en":"Landed Cost For Local PO","ar":"مستند تكاليف لامر شراء محلي"}');
