DELETE from EntityUserCode WHERE type = 'DObInitialBalanceUpload';
INSERT INTO EntityUserCode (type, code)
values ('DObInitialBalanceUpload', '2021000001');

delete from dobinitialbalanceuploadjournalentry;
delete from dobjournalentry where objecttypecode = 'InitialBalanceUpload';

TRUNCATE dobaccountingdocument CASCADE;

delete from leafglaccount where id in(select id from HObGLAccount where level = '3' and code in ('30202','10207') );
delete from lobglobalglaccountconfig where accountid in(select id from HObGLAccount where level = '3' and code in ('30202','10207') );
delete from iobitemaccountinginfo where chartofaccountid in(select id from HObGLAccount where level = '3' and code in ('30202','10207') );
delete from iobjournalentryitem where type in ('PO', 'Local_Vendors');
delete from HObGLAccount where code in ('30202','10207');

TRUNCATE masterdata CASCADE;
TRUNCATE MObVendor CASCADE;
TRUNCATE cobfiscalperiod CASCADE;

ALTER SEQUENCE dobaccountingdocument_id_seq RESTART WITH 1;

