DELETE FROM IObAccountingDocumentActivationDetails where id < 10 AND objectTypeCode = 'PaymentRequest';
DELETE FROM IObAccountingDocumentActivationDetails where id IN (77, 78, 79, 82);
ALTER SEQUENCE IObAccountingDocumentActivationDetails_id_seq RESTART WITH 82;

--2019000003
insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice,objecttypecode)
VALUES (1, 1003, '2019-08-06 09:02:00', '2019-08-06 09:02:00', 'Amr.Khalil', 'Amr.Khalil', 'Amr.Khalil', '2019-08-06 09:02:00', '2019-08-07 09:02:00', 1, 17.85 ,'PaymentRequest');

--2019000013
insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
VALUES (2, 1013, '2019-08-07 09:02:00', '2019-08-07 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-08 09:02:00', 1, 10 ,'PaymentRequest');

--2019000014
insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
VALUES (3, 1014, '2019-08-08 09:02:00', '2019-08-08 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-09 09:02:00' , 1, 11 ,'PaymentRequest');

--2019000015
-- insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
-- VALUES (4, 1015, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 1, 10 ,'PaymentRequest');

--2019000047
-- insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
-- VALUES (5, 1047, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 26, 17.44 ,'PaymentRequest');

--2019000048
-- insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
-- VALUES (6, 1048, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 26, 17.44 ,'PaymentRequest');

--2019000049
-- insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
-- VALUES (7, 1049, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 26, 17.44 ,'PaymentRequest');

--2019000041
insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
VALUES (8, 1041, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 26, 17.44 ,'PaymentRequest');

--2019000053
insert into public.IObAccountingDocumentActivationDetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationDate, duedate, exchangerateid, currencyprice ,objecttypecode)
VALUES (9, 1053, '2019-08-12 09:02:00', '2019-08-12 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', 'Gehan.Ahmed', '2019-08-07 09:02:00','2019-08-13 09:02:00', 26, 17.44 ,'PaymentRequest');

INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (77, 2019, '2020-12-09 15:16:50.182000', '2020-12-09 15:16:50.235000', 'hr1', 'hr1', 'hr1', '2020-12-09 15:16:50.182000', '2020-12-08 22:00:00.000000', 13, 1, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (78, 2020, '2020-12-09 15:16:59.452000', '2020-12-09 15:16:59.507000', 'hr1', 'hr1', 'hr1', '2020-12-09 15:16:59.452000', '2020-12-08 22:00:00.000000', 13, 1, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (79, 2021, '2020-12-09 15:16:59.452000', '2020-12-09 15:16:59.507000', 'hr1', 'hr1', 'hr1', '2020-12-09 15:16:59.452000', '2020-12-08 22:00:00.000000', 13, 1, 'PaymentRequest');

-- Under settlement Payment Request
INSERT INTO public.iobaccountingdocumentactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, accountant, activationdate, duedate, exchangerateid, currencyprice, objecttypecode)
VALUES (82, 2026, '2021-01-04 14:45:22.496000', '2021-01-04 14:45:22.599000', 'hr1', 'hr1', 'hr1', '2021-01-04 14:45:22.496000', '2021-01-03 22:00:00.000000', 13, 1, 'PaymentRequest');