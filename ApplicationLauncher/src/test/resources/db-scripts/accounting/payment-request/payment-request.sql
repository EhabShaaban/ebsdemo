DELETE FROM dobaccountingdocument WHERE id in (1008, 1009, 1010, 1012, 1013, 1014, 1015, 1016, 1021, 1029, 1030, 1038, 1040, 1041, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 2019, 2020, 2021, 2027);
DELETE FROM dobpaymentrequest WHERE id in (1008, 1009, 1010, 1012, 1013, 1014, 1015, 1016, 1021, 1029, 1030, 1038, 1040, 1041, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 2019, 2020, 2021, 2027);
ALTER SEQUENCE dobpaymentrequest_id_seq RESTART WITH 2028;

DELETE
from EntityUserCode
WHERE type = 'DObPaymentRequest';

INSERT INTO EntityUserCode (type, code)
values ('DObPaymentRequest', '2021000003');


--------------------------------  DObPaymentRequest ----------------------------------------

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1008 ,'2019000008', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoCurrency', 'Gehan.Ahmed.NoCurrency','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
           values (1008,'OTHER_PARTY_FOR_PURCHASE');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1009 ,  '2019000009', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Ahmed.Hamdi.NoCompanyAndNoBankAccountAndNoGLAccount', 'Ahmed.Hamdi.NoCompanyAndNoBankAccountAndNoGLAccount','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1009, 'OTHER_PARTY_FOR_PURCHASE');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1010 ,'2019000010', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1010, 'VENDOR');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1012 , '2019000012', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1012, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1013 , '2019000013', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active", "PaymentDone"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1013, 'OTHER_PARTY_FOR_PURCHASE');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1014 , '2019000014', '2018-08-06 09:02:00', '2018-08-06 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active", "PaymentDone"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
values (1014, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1015 , '2019000015', '2018-08-11 09:02:00', '2018-08-11 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
values (1015, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1016 , '2019000016', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Ahmed.Hamdi', 'Ahmed.Hamdi','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1016, 'OTHER_PARTY_FOR_PURCHASE');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1021, '2019000021', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoPurchaseOrder', 'Gehan.Ahmed.NoPurchaseOrder','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1021, 'OTHER_PARTY_FOR_PURCHASE');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1029, '2019000029', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoInvoice', 'Gehan.Ahmed.NoInvoice','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
values (1029, 'VENDOR');



insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1030, '2019000030', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoPurchaseOrder', 'Gehan.Ahmed.NoPurchaseOrder', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
values (1030, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1038, '2019000038', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1038, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1040, '2019000040', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil', '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1040, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1041, '2019000041', '2018-08-11 09:02:00', '2018-08-11 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Active", "PaymentDone"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1041, 'VENDOR');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1047, '2019000047', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1047, 'VENDOR');



insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1048, '2019000048', '2018-08-05 10:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1048, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1049, '2019000049', '2018-08-05 10:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
values (1049, 'VENDOR');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1050, '2019000050', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoItems', 'Gehan.Ahmed.NoItems',
                   '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1050, 'OTHER_PARTY_FOR_PURCHASE');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1051, '2019000051', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.EditWithoutRead', 'Gehan.Ahmed.EditWithoutRead',
                   '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1051, 'OTHER_PARTY_FOR_PURCHASE');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo,
           modificationinfo, currentstates, objectTypeCode, documentOwnerId) values (1052 ,'2019000052', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoCompany', 'Gehan.Ahmed.NoCompany','["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id, paymentType)
           values (1052,'OTHER_PARTY_FOR_PURCHASE');


insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1053, '2019000053', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        '["Active", "PaymentDone"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
values (1053, 'OTHER_PARTY_FOR_PURCHASE');

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2019, '2020000001', '2020-12-09 14:38:53.440000', '2020-12-09 15:54:09.328000', 'hr1', 'hr1', '["Active","PaymentDone"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2019, 'OTHER_PARTY_FOR_PURCHASE');

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2020, '2020000002', '2020-12-09 15:54:40.946000', '2020-12-09 15:55:42.147000', 'hr1', 'hr1', '["Active","PaymentDone"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2020, 'OTHER_PARTY_FOR_PURCHASE');


INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2021, '2020000003', '2020-12-09 15:54:40.946000', '2020-12-09 15:55:42.147000', 'hr1', 'hr1', '["Draft"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2021, 'OTHER_PARTY_FOR_PURCHASE');

-- Under settlement Payment Request----
INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2027, '2021000003', '2021-01-04 09:02:00', '2021-01-04 16:45:22.594000', 'Gehan.Ahmed', 'Gehan.Ahmed',  '["Draft"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2027, 'UNDER_SETTLEMENT');
