DELETE
FROM iobaccountingdocumentaccountingdetails
WHERE objecttypecode = 'PaymentRequest';

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (3, 1002, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Local_Vendors', 'DEBIT', 5, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentVendoraccountingdetails (id, glsubaccountid)
VALUES (3, 45);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (4, 1002, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 12, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (4, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (5, 1003, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'PO', 'DEBIT', 25, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (5, 1045);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (6, 1003, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (6, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (7, 1004, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'PO', 'DEBIT', 25, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (7, 1010);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (8, 1004, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 15, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (8, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (9, 1005, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 5, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (9, 45);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (10, 1005, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 15, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (10, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (15, 1008, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Treasuries', 'CREDIT', 13, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (15, null);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (16, 1008, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (16, 1000);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (17, 1009, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (17, 1056);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (18, 1009, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 17, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (18, 17);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (19, 1010, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 488010, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (19, 17);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (20, 1010, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Local_Vendors', 'DEBIT', 21, 488010, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentVendoraccountingdetails (id, glsubaccountid)
VALUES (20, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (23, 1012, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 21, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (23, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (24, 1012, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 15, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (24, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (25, 1013, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'Treasuries', 'CREDIT', 13, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (25, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (26, 1013, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (26, 1018);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (27, 1014, '2018-08-06 09:02:00.000000', '2018-08-06 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'Treasuries', 'CREDIT', 13, 540, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (27, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (28, 1014, '2018-08-06 09:02:00.000000', '2018-08-06 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'Local_Vendors', 'DEBIT', 5, 540, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (28, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (29, 1015, '2018-08-11 09:02:00.000000', '2018-08-11 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'Banks', 'CREDIT', 15, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (29, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (30, 1015, '2018-08-11 09:02:00.000000', '2018-08-11 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 'Local_Vendors', 'DEBIT', 21, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (30, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (31, 1016, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (31, 1);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (32, 1016, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (32, 1036);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (41, 1021, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'PO', 'DEBIT', 25, null, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (41, null);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (42, 1021, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 15, null, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (42, null);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (57, 1029, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Local_Vendors', 'DEBIT', 21, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (57, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (58, 1029, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 2000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (58, null);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (59, 1030, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Local_Vendors', 'DEBIT', 21, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentVendoraccountingdetails (id, glsubaccountid)
VALUES (59, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (60, 1030, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (60, 17);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (73, 1038, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 300, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (73, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (74, 1038, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 21, 300, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (74, 46);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (77, 1040, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 5, 10, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentVendoraccountingdetails (id, glsubaccountid)
VALUES (77, 45);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (78, 1040, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 10, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (78, 1);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (79, 1041, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Local_Vendors', 'DEBIT', 21, 10, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentVendoraccountingdetails (id, glsubaccountid)
VALUES (79, 46);


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (80, 1041, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 'Banks', 'CREDIT', 15, 10, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (80, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (91, 1047, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 30667, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (91, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (92, 1047, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 21, 30667, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (92, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (93, 1048, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 21, 30000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (93, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (94, 1048, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 30000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (94, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (95, 1049, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Local_Vendors', 'DEBIT', 21, 30000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentvendoraccountingdetails (id, glsubaccountid)
VALUES (95, 46);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (96, 1049, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Treasuries', 'CREDIT', 13, 30000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (96, 2);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (97, 1050, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'PO', 'DEBIT', 25, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (97, 1010);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (98, 1050, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 15, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (98, 17);        


INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (99, 1051, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed.EditWithoutRead',
        'Gehan.Ahmed.EditWithoutRead', 'PO', 'DEBIT', 25, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (99, 1010);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (100, 1051, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed.EditWithoutRead',
        'Gehan.Ahmed.EditWithoutRead', 'Banks', 'CREDIT', 15, 250, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (100, 17);        

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (101, 1052, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed.NoCompany',
        'Gehan.Ahmed.NoCompany', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (101, 1010);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (102, 1052, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed.NoCompany',
        'Gehan.Ahmed.NoCompany', 'Treasuries', 'CREDIT', 13, 1000, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumenttreasuryaccountingdetails (id, glsubaccountid)
VALUES (102, null);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (103, 1053, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Ahmed.Hamdi',
        'Ahmed.Hamdi', 'Banks', 'CREDIT', 12, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (103, 17);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate,
                                                           modifieddate, creationinfo,
                                                           modificationinfo, subledger,
                                                           accountingentry, glaccountid,
                                                           amount, objecttypecode)
VALUES (104, 1053, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed.NoCompany',
        'Gehan.Ahmed.NoCompany', 'PO', 'DEBIT', 25, 100, 'PaymentRequest');
INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (104, 1045);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (302, 2019, '2020-12-09 14:38:53.463000', '2020-12-09 15:54:09.382000', 'hr1', 'hr1', 'Banks', 'CREDIT', 15, 1000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (302, 20);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (303, 2019, '2020-12-09 14:38:53.474000', '2020-12-09 15:54:09.393000', 'hr1', 'hr1', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (303, 2090);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (304, 2020, '2020-12-09 15:54:40.953000', '2020-12-09 15:55:42.157000', 'hr1', 'hr1', 'PO', 'DEBIT', 25, 3000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (304, 2090);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (305, 2020, '2020-12-09 15:54:40.956000', '2020-12-09 15:55:42.157000', 'hr1', 'hr1', 'Banks', 'CREDIT', 15, 3000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (305, 20);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (306, 2021, '2020-12-09 15:54:40.953000', '2020-12-09 15:55:42.157000', 'hr1', 'hr1', 'PO', 'DEBIT', 25, 100, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentorderaccountingdetails (id, glsubaccountid)
VALUES (306, 1023);

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (307, 2021, '2020-12-09 15:54:40.956000', '2020-12-09 15:55:42.157000', 'hr1', 'hr1', 'Banks', 'CREDIT', 15, 100, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentbankaccountingdetails (id, glsubaccountid)
VALUES (307, 20);

-- Under settlement Payment Request

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (309, 2025, '2021-01-04 16:36:21.094000', '2021-01-04 16:37:38.699000', 'Ashraf.Salah', 'Ashraf.Salah', 'Treasuries', 'CREDIT', 13, 1000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (308, 2025, '2021-01-04 16:36:21.080000', '2021-01-04 16:37:38.701000', 'Ashraf.Salah', 'Ashraf.Salah', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (310, 2026, '2021-01-04 16:38:48.503000', '2021-01-04 16:45:22.615000', 'Ashraf.Salah', 'hr1', 'Banks', 'CREDIT', 15, 1000, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentaccountingdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, subledger, accountingentry, glaccountid, amount, objecttypecode)
VALUES (311, 2026, '2021-01-04 16:38:48.517000', '2021-01-04 16:45:22.616000', 'Ashraf.Salah', 'hr1', 'PO', 'DEBIT', 25, 1000, 'PaymentRequest');