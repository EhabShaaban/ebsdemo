DELETE
FROM IObPaymentRequestPaymentDetails
WHERE id in
      (1008, 1009, 1010, 1012, 1013, 1014, 1015, 1016, 1021, 1029, 1030, 1041, 1038, 1040, 1047, 1048, 1049, 1050, 1051,
       1052, 1053, 1054, 1055, 1056, 1059);
DELETE
FROM IObPaymentRequestPurchaseOrderPaymentDetails
WHERE id in (1008, 1009, 1010, 1013, 1016, 1021, 1030, 1040, 1041, 1050, 1051, 1052, 1053, 1054, 1055, 1056);
DELETE
FROM IObPaymentRequestInvoicePaymentDetails
WHERE id in (1012, 1014, 1015, 1029, 1038, 1047, 1048, 1049);

ALTER SEQUENCE IObPaymentRequestPaymentDetails_id_seq RESTART WITH 1901;
insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1008, 1008, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoCurrency',
        'Gehan.Ahmed.NoCurrency',
        null, 'PURCHASEORDER', 1000, 2, 'for save payment details hp', 'CASH', null);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1008, 1010);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costFactorItemId)
values (1009, 1009, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        null, 'PURCHASEORDER', 1000, 2, 'for save payment details hp', 'BANK', 17, 192);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1009, 1056);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1010, 1010, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'PURCHASEORDER', 488010, 2, 'for save payment details hp', 'BANK', 17);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1010, 1001);
---------------------------------------

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1012, 1012, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 2000.0, 2, 'for save payment details hp', 'BANK', 17);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1012, 2);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid,
                                             costFactorItemId)
values (1013, 1013, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'PURCHASEORDER', 1000, 2, 'for save payment details hp', 'CASH', 2, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1013, 1018);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1014, 1014, '2018-08-06 09:02:00', '2018-08-06 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 28.8, 2, 'for save payment details hp', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1014, 34);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1015, 1015, '2018-08-11 09:02:00', '2018-08-11 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 100.00, 2, 'for save payment details hp', 'BANK', 17);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1015, 34);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid,
                                             costFactorItemId)
values (1016, 1016, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        45, 'PURCHASEORDER', 1000, 1, 'مصاريف بنكية', 'CASH', 1, 190);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1016, 1036);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costFactorItemId)
values (1021, 1021, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Lama Maher', 'Lama Maher',
        null, 'PURCHASEORDER', null, null, null, 'BANK', null, null);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1021, null);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1029, 1029, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoInvoice',
        'Gehan.Ahmed.NoInvoice',
        46, 'INVOICE', 2000, 2, 'for save payment details hp', 'BANK', 17);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1029, null);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1030, 1030, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoPurchaseOrder',
        'Gehan.Ahmed.NoPurchaseOrder',
        46, 'PURCHASEORDER', 1000, 2, 'for save payment details hp', 'BANK', 17);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1030, 1001);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount,
                                             currencyid, description, paymentForm, bankaccountid)
values (1041, 1041, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'PURCHASEORDER', 10.00, 2, 'Downpayent to a confirmed PO', 'BANK', 17);


insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1038, 1038, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 300.00, 2, 'Installment No 2 in invoice', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1038, 108);


insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1040, 1040, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        45, 'PURCHASEORDER', 10.00, 2, 'Downpayent to a confirmed PO', 'CASH', 1);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1040, 1003);


insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1041, 2087);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1047, 1047, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 30667.00, 2, 'for estimated cost goods receipts', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1047, 40);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1048, 1048, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 30000.00, 2, 'for estimated cost goods receipts', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1048, 40);


insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, treasuryid)
values (1049, 1049, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        46, 'INVOICE', 30000.00, 2, 'for estimated cost goods receipts', 'CASH', 2);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1049, 40);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costFactorItemId)
values (1050, 1050, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoItems', 'Gehan.Ahmed.NoItems',
        null, 'PURCHASEORDER', 250, 1, 'مصاريف بنكية', 'BANK', 17, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1050, 1010);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costFactorItemId)
values (1051, 1051, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.EditWithoutRead',
        'Gehan.Ahmed.EditWithoutRead',
        null, 'PURCHASEORDER', 250, 1, 'مصاريف بنكية', 'BANK', 17, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1051, 1010);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1052, 1052, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed.NoCompany',
        'Gehan.Ahmed.NoCompany',
        null, 'PURCHASEORDER', 1000, 2, 'for save payment details hp', 'CASH', null);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1052, 1010);


insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costfactoritemid)
values (1053, 1053, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        null, 'PURCHASEORDER', 250, 2, 'downpayment for po', 'BANK', 17, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1053, 1045);


INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1054, 2019, '2020-12-09 14:38:53.423000', '2020-12-09 14:39:39.885000', 'hr1', 'hr1', null, 'PURCHASEORDER',
        1000, 1, 'Payment for service PO', 'BANK', 20, 192, null);

INSERT INTO public.IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1054, 2091);

INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1055, 2020, '2020-12-09 15:54:40.927000', '2020-12-09 15:55:33.645000', 'hr1', 'hr1', null, 'PURCHASEORDER',
        3000, 1, 'Payment for service PO', 'BANK', 20, 192, null);

INSERT INTO public.IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1055, 2091);

INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1056, 2021, '2020-12-09 15:54:40.927000', '2020-12-09 15:55:33.645000', 'hr1', 'hr1', null, 'PURCHASEORDER',
        100, 1, 'Payment for Goods PO', 'BANK', 20, 192, null);

INSERT INTO public.IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1056, 1023);

INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1059, 2027, '2021-01-04 16:38:48.475000', '2021-01-04 16:45:15.970000', 'Ashraf.Salah', 'hr1', null, 'NONE', 1000,
        1, 'Under settlement', 'BANK', 20, null, null);




