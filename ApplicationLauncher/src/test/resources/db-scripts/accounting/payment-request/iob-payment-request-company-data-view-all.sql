
delete
from IObAccountingDocumentCompanyData where objecttypecode = 'PaymentRequest' AND id > 1 AND id < 6;
DELETE FROM IObAccountingDocumentCompanyData WHERE id IN (2536, 2537);

INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate,
                                                     creationinfo, modificationinfo, purchaseunitid,
                                                     companyid, objectTypeCode)
VALUES (2, 1002, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 1, 10, 'PaymentRequest');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate,
                                                     creationinfo, modificationinfo, purchaseunitid,
                                                     companyid, objectTypeCode)
VALUES (3, 1003, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 23, 10, 'PaymentRequest');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate,
                                                     creationinfo, modificationinfo, purchaseunitid,
                                                     companyid, objectTypeCode)
VALUES (4, 1004, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Gehan.Ahmed',
        'Gehan.Ahmed', 19, 10, 'PaymentRequest');
INSERT INTO public.IObAccountingDocumentCompanyData (id, refinstanceid, creationdate, modifieddate,
                                                     creationinfo, modificationinfo, purchaseunitid,
                                                     companyid, objectTypeCode)
VALUES (5, 1005, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'Amr.Khalil',
        'Amr.Khalil', 1, 10, 'PaymentRequest');

-- Under settlement Payment Request
INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (2536, 2025, '2021-01-04 14:36:20.996000', '2021-01-04 14:37:38.692000', 'Ashraf.Salah', 'Ashraf.Salah', 19, 10, null, 'PaymentRequest');

INSERT INTO public.iobaccountingdocumentcompanydata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchaseunitid, companyid, bankaccountid, objecttypecode)
VALUES (2537, 2026, '2021-01-04 14:38:48.468000', '2021-01-04 14:45:15.972000', 'Ashraf.Salah', 'hr1', 19, 10, null, 'PaymentRequest');
