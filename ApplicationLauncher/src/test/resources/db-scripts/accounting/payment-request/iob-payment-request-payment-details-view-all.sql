DELETE
FROM IObPaymentRequestPaymentDetails
WHERE id in (1002, 1003, 1004, 1005, 1057, 1058);
DELETE
FROM IObPaymentRequestPurchaseOrderPaymentDetails
WHERE id in (1002, 1003, 1004);
DELETE
FROM IObPaymentRequestInvoicePaymentDetails
WHERE id in (1005);

ALTER SEQUENCE IObPaymentRequestPaymentDetails_id_seq RESTART WITH 1908;

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1002, 1002, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        45, 'PURCHASEORDER', 1000, 3, 'For PO No 2018000010', 'BANK', 17);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1002, 1034);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costfactoritemid)
values (1003, 1003, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        null, 'PURCHASEORDER', 100, 2, 'downpayment for po', 'BANK', 17, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1003, 1045);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid,
                                             costFactorItemId)
values (1004, 1004, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
        null, 'PURCHASEORDER', 250, 1, 'مصاريف بنكية', 'BANK', 17, 74);

insert into IObPaymentRequestPurchaseOrderPaymentDetails (id, dueDocumentId)
values (1004, 1010);

insert into IObPaymentRequestPaymentDetails (id, refInstanceId, creationdate, modifieddate,
                                             creationinfo,
                                             modificationinfo, businesspartnerId, dueDocumentType,
                                             netAmount, currencyid, description, paymentForm, bankaccountid)
values (1005, 1005, '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
        45, 'INVOICE', 2000, 2, 'Installment No 2 in invoice', 'BANK', 17);

insert into IObPaymentRequestInvoicePaymentDetails (id, dueDocumentId)
values (1005, 39);

-- Under settlement Payment Request
INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1057, 2025, '2021-01-04 16:36:21.004000', '2021-01-04 16:37:38.691000', 'Ashraf.Salah', 'Ashraf.Salah', 194,
        'NONE', 1000, 2, 'Under settlement', 'CASH', null, null, null);

INSERT INTO public.iobpaymentrequestpaymentdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                    modificationinfo, businesspartnerid, duedocumenttype, netamount,
                                                    currencyid, description, paymentform, bankaccountid,
                                                    costfactoritemid, treasuryid)
VALUES (1058, 2026, '2021-01-04 16:38:48.475000', '2021-01-04 16:45:15.970000', 'Ashraf.Salah', 'hr1', 194, 'NONE',
        1000, 1, 'Under settlement', 'BANK', 20, null, null);