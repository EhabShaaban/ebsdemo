DELETE FROM dobaccountingdocument WHERE id in (1002, 1003, 1004, 1005, 2025, 2026, 2027);
DELETE FROM dobpaymentrequest WHERE id in (1002, 1003, 1004, 1005, 2025, 2026, 2027);

ALTER SEQUENCE dobpaymentrequest_id_seq RESTART WITH 2027;

DELETE
from EntityUserCode
WHERE type = 'DObPaymentRequest';

INSERT INTO EntityUserCode (type, code)
values ('DObPaymentRequest', '2021000002');

----------------------------------------------------------------------------------------------------------------------

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1002, '2019000002', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
                   '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1002, 'VENDOR');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1003, '2019000003', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
                   '["Active", "PaymentDone"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1003, 'OTHER_PARTY_FOR_PURCHASE');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1004, '2019000004', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed',
                   '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1004, 'OTHER_PARTY_FOR_PURCHASE');

insert into dobaccountingdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objectTypeCode, documentOwnerId)
values (1005, '2019000005', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'Amr.Khalil', 'Amr.Khalil',
                   '["Draft"]', 'PaymentRequest', 34);
insert into dobpaymentrequest (id,paymentType)
           values (1005, 'VENDOR');

-- Under settlement Payment Request
INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2025, '2021000001', '2021-01-04 09:02:00', '2021-01-04 16:37:38.683000', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Draft"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2025, 'UNDER_SETTLEMENT');

INSERT INTO public.dobaccountingdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentOwnerId)
VALUES (2026, '2021000002', '2021-01-04 09:02:00', '2021-01-04 16:45:22.594000', 'Ashraf.Salah', 'hr1', '["Active","PaymentDone"]', 'PaymentRequest', 34);

INSERT INTO public.dobpaymentrequest (id, paymenttype)
VALUES (2026, 'UNDER_SETTLEMENT');