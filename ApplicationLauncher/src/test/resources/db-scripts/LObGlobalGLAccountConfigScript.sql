delete from LObGlobalGLAccountConfig;

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (1, 'CashGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 13);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (2, 'RealizedGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 14);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (3, 'BankGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 15);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (4, 'CustomersGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 11);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (5, 'TaxesGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 43);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (6, 'SalesGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 42);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (7, 'NotesReceivablesGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 26);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (8, 'TreasuryGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 10);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (9, 'BanksGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 12);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (10, 'PurchasingGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 65);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (11, 'PurchaseOrderGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 25);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (12, 'UnrealizedCurrencyGainLossGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 41);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (13, 'ImportPurchasingGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 63);

insert into LObGlobalGLAccountConfig (id, code, creationDate, modifiedDate, creationInfo, modificationInfo, description, sysFlag, accountId)
VALUES (14, 'LocalPurchasingGLAccount', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'System', 'System', null, true, 64);
