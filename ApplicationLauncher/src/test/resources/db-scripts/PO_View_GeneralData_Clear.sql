TRUNCATE doborderdocument CASCADE;
DELETE FROM iobordercycledates;

ALTER SEQUENCE doborderdocument_id_seq RESTART WITH 1;
ALTER SEQUENCE iobordercompany_id_seq RESTART WITH 1;
ALTER SEQUENCE iobpurchaseorderfulfillervendor_id_seq RESTART WITH 1;
ALTER SEQUENCE ioborderitem_id_seq RESTART WITH 3;
ALTER SEQUENCE ioborderpaymentdetails_id_seq RESTART WITH 1;