DELETE FROM iobcompanypurchasingUnits;
DELETE FROM iobenterpriseaddress;
DELETE FROM iobcustomerbusinessunit;
DELETE FROM IObCustomerLegalRegistrationData;
DELETE FROM IObCustomerAddress;
DELETE FROM IObCustomerContactPerson;
DELETE FROM mobcustomer;
DELETE FROM lobtaxinfo;
DELETE FROM lobcompanytaxes;
DELETE FROM cobcompany;
DELETE FROM cobplant;
DELETE FROM cobstorehouse;
DELETE FROM cobenterprise;
DELETE FROM lobtaxadministrative;
DELETE FROM lobissuefrom;
DELETE FROM lobvendortaxes;
-- Data for Name: cobenterprise;


INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (1, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 13:27:50.517298', 'Default Data', 'Default Data', '5', '{"en":"Flexo","ar":"فليكسو"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (2, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.196950', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Amr.Khalil","ar":"عمر خليل"}', null, '[Active]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (3, '0002', '2018-04-24 06:31:56.332658', '2018-08-01 14:06:24.690616', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Gehan.Ahmed","ar":"جيهان أحمد"}', null, '[Active]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (4, '0003', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:21.764376', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Marwa.Tawfeek","ar":"مروة توفيق"}', null, '[Active]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (5, '0004', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:51.107396', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Asmaa.Elkhodary","ar":"أسماء الخضري"}', null, '[Active]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (6, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 13:49:38.042591', 'Default Data', 'Default Data', '2', '{"en":"Madina Group", "ar":"مجموعة المدينة"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (7, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 13:50:58.424023', 'Default Data', 'Default Data', '1', '{"en":"AL Madina","ar":"المدينة"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (8, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 13:16:47.319288', 'Default Data', 'Default Data', '3', '{"en":"Madina Tech Plant","ar":"مصنع المدينة للتقنية"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (9, '0001', '2018-04-24 06:31:56.332658', '2018-08-05 11:05:57.144565', 'Default Data', 'Default Data', '4', '{"en":"AlMadina Main Store","ar":"مخزن المدينة الرئيسي"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (10, '0002', '2018-08-01 12:57:07.743224', '2018-08-01 13:51:22.247312', 'Default Data', 'Default Data', '1', '{"en":"DigiPro","ar":"دي جي برو"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (11, '0003', '2018-08-01 12:57:26.189637', '2018-08-01 12:57:26.189637', 'Default Data', 'Default Data', '1', '{"en":"HPS","ar":"اتش بي اس"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (12, '0002', '2018-08-01 13:04:12.946625', '2018-08-01 13:04:12.946625', 'Default Data', 'Default Data', '3', '{"en":"DigiPro Plant","ar":"مقر دي جي برو"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (13, '0003', '2018-08-01 13:04:12.952392', '2018-08-01 13:04:12.952392', 'Default Data', 'Default Data', '3', '{"en":"HPS Plant","ar":"مقر اتش بي اس"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (14, '0003', '2018-08-01 13:24:38.966390', '2018-08-05 11:10:23.957283', 'Default Data', 'Default Data', '4', '{"en":"DigiPro Main Store","ar":"مخزن دي جي برو الرئيسي"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (15, '0005', '2018-08-01 13:24:38.971788', '2018-08-05 11:10:49.689472', 'Default Data', 'Default Data', '4', '{"en":"HPS Main Store","ar":"HPS Main Store"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (16, '0002', '2018-08-01 13:24:38.977631', '2018-08-05 11:10:07.274461', 'Default Data', 'Default Data', '4', '{"en":"AlMadina Secondary Store","ar":"مخزن المدينه الثانوي"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (17, '0004', '2018-08-01 13:24:38.983139', '2018-08-05 11:10:34.392712', 'Default Data', 'Default Data', '4', '{"en":"DigiPro Secondary Store","ar":"مخزن دي جي برو الثانوي"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (18, '0006', '2018-08-01 13:24:38.988818', '2018-08-05 11:10:58.457078', 'Default Data', 'Default Data', '4', '{"en":"HPS Secondary Store","ar":"مخزن اتش بي اس الرئيسي"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (19, '0002', '2018-08-01 13:34:19.946784', '2018-08-02 09:30:18.312919', 'Default Data', 'Default Data', '5', '{"en":"Signmedia","ar":"ساين ميديا"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (20, '0003', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '5', '{"en":"Offset","ar":"اوفسيت"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (21, '0004', '2018-08-01 13:34:19.960878', '2018-08-01 13:34:19.960878', 'Default Data', 'Default Data', '5', '{"en":"Digital","ar":"ديجيتال"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (22, '0005', '2018-08-01 13:34:19.965503', '2018-08-02 09:34:32.481037', 'Default Data', 'Default Data', '5', '{"en":"Textile","ar":"تيكستايل"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (23, '0006', '2018-08-01 13:34:21.460443', '2018-08-01 13:34:21.460443', 'Default Data', 'Default Data', '5', '{"en":"Corrugated","ar":"كوروجيتيد"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (24, '0005', '2018-08-01 14:03:16.942504', '2018-08-01 14:03:16.942504', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Khaled.Ahmed","ar":"خالد أحمد"}', null, '[Active]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (29, '0006', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '6', '{"en":"Obsolete.User","ar":" عضو ملغي"}', null, '["Inactive"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (31, '0007', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '6', '{"en":"Draft.User","ar":" عضو قيد الأنشاء"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (32, '0007', '2019-10-23 14:23:27.000000', '2019-10-23 14:23:33.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '5', '{"ar": "IVD", "en": "IVD"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (33, '0005', '2019-10-24 11:45:04.000000', '2019-10-24 11:45:05.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '1', '{"ar": "MedVal", "en": "MedVal"}', null, '[''active'']');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (34, '007', '2019-10-28 09:34:00.000000', '2019-10-28 09:34:01.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '8', '{"ar": "فخر", "en": "Fakhr"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (35, '0002', '2019-10-28 09:37:18.000000', '2019-10-28 09:37:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '9', '{"ar": "مسئول التحصيل", "en": "collection responsible"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (36, '0011', '2019-10-28 10:41:25.000000', '2019-10-28 10:41:27.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '8', '{"ar": "محمدين", "en": "Mohamedeen"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (37, '0003', '2019-10-28 11:22:09.000000', '2019-10-28 11:22:11.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '9', '{"ar": "احمد محمد", "en": "Ahmed Mohamed"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (38, '0001', '2019-10-28 11:31:54.000000', '2019-10-28 11:31:55.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '7', '{"ar": "سراج الدين مغاوري", "en": "SeragEldin Meghawry"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (39, '0002', '2020-02-04 07:53:16.000000', '2020-02-04 07:53:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '7', '{"ar": "محمد", "en": "Mohammed"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (40, '0003', '2020-02-04 08:37:22.000000', '2020-02-04 08:37:23.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '7', '{"ar": "منار", "en": "Manar"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (41, '0004', '2020-02-04 08:40:15.000000', '2020-02-04 08:40:17.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '7', '{"ar": "أحمد العشري", "en": "Ahmed.Al-Ashry"}', null, '["Active"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (42, '0001', '2020-06-07 11:47:12.000000', '2020-06-07 11:47:14.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '10', '{"en": "Mohamed Nabil", "ar": "محمد نبيل"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (43, '0002', '2020-06-07 11:49:06.000000', '2020-06-07 11:49:08.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '10', '{"en": "Mahmoud Amr", "ar": "محمود عمرو"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (44, '0003', '2020-06-07 11:50:08.000000', '2020-06-07 11:50:10.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '10', '{"en": "Moustafa", "ar": "مصطفي"}', null, '["Draft"]');
INSERT INTO public.cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates) VALUES (46, '0001', '2020-06-15 13:59:36.000000', '2020-06-15 13:59:37.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '11', '{"en": "Manar.Mohammed", "ar": "منار.محمد"}', null, '["Active"]');

INSERT INTO lobtaxadministrative (id, code, creationdate, modifieddate, creationinfo,modificationinfo, name )
VALUES (1,  '0001', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '{"en":"مأمورية شمال","ar":"مأمورية شمال"}');

INSERT INTO cobcompany (id, companygroupid, taxAdministrativeid, regNumber, taxCardNumber,
                        taxFileNumber)
VALUES (7, 6, 1, 'regNumber', 'taxCardNumber',
        'taxFileNumber');
INSERT INTO cobcompany (id, companygroupid, taxadministrativeid, regNumber, taxCardNumber,
                        taxFileNumber)
VALUES (10, 6, 1, 'regNumber',
        'taxCardNumber', 'taxFileNumber');
INSERT INTO cobcompany (id, companygroupid, taxadministrativeid, regNumber, taxCardNumber,
                        taxFileNumber)
VALUES (11, 6, 1, 'regNumber',
        'taxCardNumber', 'taxFileNumber');

INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (3,   7, '2018-04-24 06:31:59.42126', '2018-04-24 06:31:59.42126', 'Default for DigiPro', 'Default for DigiPro', 1);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (4,   7, '2018-08-02 10:02:08.973772', '2018-08-02 10:02:08.973772', 'Default for DigiPro', 'Default for DigiPro', 19);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (5,   7, '2018-08-02 10:02:08.982161', '2018-08-02 10:02:08.982161', 'Default for DigiPro', 'Default for DigiPro', 20);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (6,   7, '2018-08-02 10:02:08.989608', '2018-08-02 10:02:08.989608', 'Default for DigiPro', 'Default for DigiPro', 21);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (7,   7, '2018-08-02 10:02:08.9974', '2018-08-02 10:02:08.9974', 'Default for DigiPro', 'Default for DigiPro', 22);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (8,   7, '2018-08-02 10:02:10.207657', '2018-08-02 10:02:10.207657', 'Default for DigiPro', 'Default for DigiPro', 23);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (9,   10, '2018-08-02 10:05:26.407374', '2018-08-02 10:05:26.407374', 'Default for DigiPro', 'Default for DigiPro', 1);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (10,   10, '2018-08-02 10:05:26.416273', '2018-08-02 10:05:26.416273', 'Default for DigiPro', 'Default for DigiPro', 19);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (11,   10, '2018-08-02 10:05:26.424177', '2018-08-02 10:05:26.424177', 'Default for DigiPro', 'Default for DigiPro', 20);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (12,   10, '2018-08-02 10:05:26.432074', '2018-08-02 10:05:26.432074', 'Default for DigiPro', 'Default for DigiPro', 21);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (13,   10, '2018-08-02 10:05:26.43951', '2018-08-02 10:05:26.43951', 'Default for DigiPro', 'Default for DigiPro', 22);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (14,   10, '2018-08-02 10:05:26.447622', '2018-08-02 10:05:26.447622', 'Default for DigiPro', 'Default for DigiPro', 23);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (15,   11, '2018-08-02 10:05:26.455254', '2018-08-02 10:05:26.455254', 'Default for DigiPro', 'Default for DigiPro', 1);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (16,   11, '2018-08-02 10:05:26.46258', '2018-08-02 10:05:26.46258', 'Default for DigiPro', 'Default for DigiPro', 19);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (17,   11, '2018-08-02 10:05:26.470347', '2018-08-02 10:05:26.470347', 'Default for DigiPro', 'Default for DigiPro', 20);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (18,   11, '2018-08-02 10:05:26.478327', '2018-08-02 10:05:26.478327', 'Default for DigiPro', 'Default for DigiPro', 21);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (19,   11, '2018-08-02 10:05:26.486554', '2018-08-02 10:05:26.486554', 'Default for DigiPro', 'Default for DigiPro', 22);
INSERT INTO iobcompanypurchasingunits (id,  refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, purchasingunitid) VALUES (20,   11, '2018-08-02 10:05:27.795736', '2018-08-02 10:05:27.795736', 'Default for DigiPro', 'Default for DigiPro', 23);
INSERT INTO iobenterpriseaddress(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, addressline,
    countryid, cityid, postalcode)
VALUES (   7, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
           'hr1', 'hr1', 1, '{"en":"70, Salah Salem St.","ar":"70 شارع صلاح سالم"}',
           1, 4, 11455);

INSERT INTO iobenterpriseaddress(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, addressline,
    countryid, cityid, postalcode)
VALUES (   10, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
           'hr1', 'hr1', 1, '{"en":"66, Salah Salem St.","ar":"66 شارع صلاح سالم"}',
           3, 5, 11451);

INSERT INTO iobenterprisecontact(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, contactvalue, contacttypeid)
VALUES (  7, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
          'hr1', 'hr1', 1, '+202 2531 6666', 1);

INSERT INTO iobenterprisecontact(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, contactvalue, contacttypeid)
VALUES (  7, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
          'hr1', 'hr1', 1, '+202 2531 4567', 2);

INSERT INTO iobenterprisecontact(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, contactvalue, contacttypeid)
VALUES (  10, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
          'hr1', 'hr1', 1, '+202 2531 3955', 1);

INSERT INTO iobenterprisecontact(
    refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, objecttypecode, contactvalue, contacttypeid)
VALUES (  10, '2018-04-24 06:31:56.332658', '2018-04-24 06:31:56.332658',
          'hr1', 'hr1', 1, '+202 2531 3954', 2);


INSERT INTO cobplant (id, companyid) VALUES (8, 7);
INSERT INTO cobplant (id, companyid) VALUES (12, 10);
INSERT INTO cobplant (id, companyid) VALUES (13, 11);

INSERT INTO cobstorehouse (id, plantid) VALUES (9, 8);
INSERT INTO cobstorehouse (id, plantid) VALUES (16, 8);
INSERT INTO cobstorehouse (id, plantid) VALUES (14, 12);
INSERT INTO cobstorehouse (id, plantid) VALUES (17, 12);
INSERT INTO cobstorehouse (id, plantid) VALUES (15, 13);
INSERT INTO cobstorehouse (id, plantid) VALUES (18, 13);

