DELETE FROM doborderdocument Where id > 2087 and id < 2090 ;
----------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------2020000045-----------------------------------------------------------------
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2088, '2020000045', '5-Aug-2020 9:02 AM'::TIMESTAMP, '5-Aug-2020 9:02 AM'::TIMESTAMP, 'admin', 'admin',
        concat('["', ARRAY_TO_STRING(string_to_array('Confirmed', ','), '","'), '"]'), 'SERVICE_PO',
        (SELECT id FROM userinfo WHERE userinfo.name = 'Gehan Ahmed'));

INSERT INTO public.dobpurchaseorder (id) VALUES (2088);


INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankaccountid)
VALUES (159, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM dobpurchaseordergeneralmodel WHERE code = '2020000045'),
        (SELECT id FROM cobenterprise company
         WHERE concat(company.code, ' - ', company.name::json ->> 'en') = '0002 - DigiPro'
           AND company.objecttypecode = '1'),
        (SELECT id FROM cobenterprise businessUnit
         WHERE concat(businessUnit.code, ' - ', businessUnit.name::json ->> 'en') = '0002 - Signmedia'
           AND businessUnit.objecttypecode = '5'), 'BILL_TO',
        (select id from iobcompanybankdetailsgeneralmodel
         where bankaccountnumber = '1516171819789 - Alex Bank'));


INSERT INTO iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, vendorid, referencePoId)
VALUES (76, NOW(), NOW(), 'Admin', 'Admin',
        (select id from doborderdocument where code = '2020000045' and objecttypecode like '%_PO'),
        (select id from masterdata
        where concat(code, ' - ', name::json ->> 'en') = '000002 - Zhejiang' and objecttypecode = '2'),
        (select id from doborderdocument where code = '2018000001' and objecttypecode like '%_PO'));


INSERT INTO ioborderpaymentdetails(creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, paymenttermid, currencyid)
VALUES (NOW(), NOW(), 'Admin', 'Admin', (SELECT id FROM doborderdocument WHERE code = '2020000045' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM cobpaymentterms
         WHERE concat(cobpaymentterms.code, ' - ', cobpaymentterms.name::json ->> 'en') = '0002 - 20% advance, 80% Copy of B/L'),
        (SELECT id FROM cobcurrency
         WHERE concat(cobcurrency.code, ' - ', cobcurrency.iso) = '0002 - USD'));


INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo,
                          modificationinfo, refinstanceid, itemid, orderunitid,
                          quantity, price)
VALUES (CAST(100 AS INT8), NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000045' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM mobitemgeneralmodel
         WHERE concat(code, ' - ', name::json ->> 'en') = '000057 - Bank Fees'),
        (SELECT id FROM cobmeasure WHERE concat(code, ' - ', symbol::json ->> 'en') = '0019 - M2'),
        CAST(16.000 AS DOUBLE PRECISION), CAST(200.000 AS DOUBLE PRECISION));

INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo,
                                 modificationinfo, refinstanceid, itemid, orderunitid,
                                 quantity, price)
VALUES (CAST(102 AS INT8), NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000045' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM mobitemgeneralmodel
         WHERE concat(code, ' - ', name::json ->> 'en') = '000055 - Insurance service2'),
        (SELECT id FROM cobmeasure WHERE concat(code, ' - ', symbol::json ->> 'en') = '0019 - M2'),
        CAST(20.000 AS DOUBLE PRECISION), CAST(200.000 AS DOUBLE PRECISION));


INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (3, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000045' AND objecttypecode LIKE '%_PO'),
        '0013', CONCAT('{"en": "', 'Shipping tax', '" , "ar": "', 'Shipping tax', '"}')::json,
        CAST(10 AS DOUBLE PRECISION));

INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (4, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000045' AND objecttypecode LIKE '%_PO'),
        '0014', CONCAT('{"en": "', 'Service tax', '" , "ar": "', 'Service tax', '"}')::json,
        CAST(2 AS DOUBLE PRECISION));
----------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------2020000046-----------------------------------------------------------------
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2089, '2020000046', '5-Aug-2020 9:02 AM'::TIMESTAMP, '5-Aug-2020 9:02 AM'::TIMESTAMP, 'admin', 'admin',
        concat('["', ARRAY_TO_STRING(string_to_array('Confirmed', ','), '","'), '"]'), 'SERVICE_PO',
        (SELECT id FROM userinfo WHERE userinfo.name = 'Gehan Ahmed'));

INSERT INTO public.dobpurchaseorder (id) VALUES (2089);


INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankaccountid)
VALUES (160, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM dobpurchaseordergeneralmodel WHERE code = '2020000046'),
        (SELECT id FROM cobenterprise company
         WHERE concat(company.code, ' - ', company.name::json ->> 'en') = '0002 - DigiPro'
           AND company.objecttypecode = '1'),
        (SELECT id FROM cobenterprise businessUnit
         WHERE concat(businessUnit.code, ' - ', businessUnit.name::json ->> 'en') = '0002 - Signmedia'
           AND businessUnit.objecttypecode = '5'), 'BILL_TO',
        (select id from iobcompanybankdetailsgeneralmodel
         where bankaccountnumber = '1516171819789 - Alex Bank'));


INSERT INTO iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, vendorid, referencePoId)
VALUES (77, NOW(), NOW(), 'Admin', 'Admin',
        (select id from doborderdocument where code = '2020000046' and objecttypecode like '%_PO'),
        (select id from masterdata
        where concat(code, ' - ', name::json ->> 'en') = '000002 - Zhejiang' and objecttypecode = '2'),
        (select id from doborderdocument where code = '2018000023' and objecttypecode like '%_PO'));


INSERT INTO ioborderpaymentdetails(creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, paymenttermid, currencyid)
VALUES (NOW(), NOW(), 'Admin', 'Admin', (SELECT id FROM doborderdocument WHERE code = '2020000046' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM cobpaymentterms
         WHERE concat(cobpaymentterms.code, ' - ', cobpaymentterms.name::json ->> 'en') = '0002 - 20% advance, 80% Copy of B/L'),
        (SELECT id FROM cobcurrency
         WHERE concat(cobcurrency.code, ' - ', cobcurrency.iso) = '0002 - USD'));


INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo,
                          modificationinfo, refinstanceid, itemid, orderunitid,
                          quantity, price)
VALUES (CAST(103 AS INT8), NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000046' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM mobitemgeneralmodel
         WHERE concat(code, ' - ', name::json ->> 'en') = '000057 - Bank Fees'),
        (SELECT id FROM cobmeasure WHERE concat(code, ' - ', symbol::json ->> 'en') = '0019 - M2'),
        CAST(16.000 AS DOUBLE PRECISION), CAST(1500.000 AS DOUBLE PRECISION));

INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo,
                                 modificationinfo, refinstanceid, itemid, orderunitid,
                                 quantity, price)
VALUES (CAST(104 AS INT8), NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000046' AND objecttypecode LIKE '%_PO'),
        (SELECT id FROM mobitemgeneralmodel
         WHERE concat(code, ' - ', name::json ->> 'en') = '000055 - Insurance service2'),
        (SELECT id FROM cobmeasure WHERE concat(code, ' - ', symbol::json ->> 'en') = '0019 - M2'),
        CAST(20.000 AS DOUBLE PRECISION), CAST(700.000 AS DOUBLE PRECISION));


INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (5, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000046' AND objecttypecode LIKE '%_PO'),
        '0013', CONCAT('{"en": "', 'Shipping tax', '" , "ar": "', 'Shipping tax', '"}')::json,
        CAST(10 AS DOUBLE PRECISION));

INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (6, NOW(), NOW(), 'Admin', 'Admin',
        (SELECT id FROM doborderdocument WHERE code = '2020000046' AND objecttypecode LIKE '%_PO'),
        '0014', CONCAT('{"en": "', 'Service tax', '" , "ar": "', 'Service tax', '"}')::json,
        CAST(2 AS DOUBLE PRECISION));
----------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------2020000047-----------------------------------------------------------------
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2090, '2020000047', '2020-12-09 11:50:59.782000', '2020-12-09 11:53:36.451000', 'hr1', 'hr1', '["Confirmed"]', 'SERVICE_PO', 37);

INSERT INTO public.dobpurchaseorder (id)
VALUES (2090);

INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, bankaccountid, partyrole)
VALUES (161, '2020-12-09 11:50:59.805000', '2020-12-09 11:51:10.025000', 'hr1', 'hr1', 2090, 10, 19, 17, 'BILL_TO');

INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, vendorid, referencepoid)
VALUES (78, '2020-12-09 11:50:59.815000', '2020-12-09 11:50:59.815000', 'hr1', 'hr1', 2090, 46, 1017);

INSERT INTO public.ioborderpaymentdetails (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, paymenttermid, currencyid)
VALUES (8, '2020-12-09 11:53:17.040000', '2020-12-09 11:53:17.056000', 'hr1', 'hr1', 2090, 5, 1);

INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, orderunitid, quantity, price)
VALUES (105, '2020-12-09 11:51:40.860000', '2020-12-09 11:51:40.865000', 'hr1', 'hr1', 2090, 192, 23, 1, 4000);

INSERT INTO public.ioborderitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, orderunitid, quantity, price)
VALUES (106, '2020-12-09 11:52:18.249000', '2020-12-09 11:52:18.255000', 'hr1', 'hr1', 2090, 190, 23, 1, 5000);

INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (7, '2020-12-09 11:50:59.830000', '2020-12-09 11:50:59.830000', 'hr1', 'hr1', 2090, '0013', '{"ar":"ضريبة نقل","en":"Shipping tax"}', 10);

INSERT INTO public.iobordertax (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, code, name, percentage)
VALUES (8, '2020-12-09 11:50:59.841000', '2020-12-09 11:50:59.841000', 'hr1', 'hr1', 2090, '0014', '{"ar":"ضريبة خدمة","en":"Service tax"}', 2);