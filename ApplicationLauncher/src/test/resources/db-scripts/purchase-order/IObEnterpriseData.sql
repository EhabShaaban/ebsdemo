delete from iobenterprisedata WHERE refinstanceid IN (1001,1002,1003,1004,1005,1009,1017,1018,1023,1024,1035,1036,1037,1042,1044,1045,1046,1047,1048,1049,1050,1051,1057);
delete from iobordercompany WHERE refinstanceid IN (1001,1002,1003,1004,1005,1009,1017,1018,1023,1024,1035,1036,1037,1042,1044,1045,1046,1047,1048,1049,1050,1051,1057,2087,2157,2158,6993,6995,6996,6997,6998,6999,7000);

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (345, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 5, '{"ar":"أسماء الخضري","en":"Asmaa Elkhodary"}', NULL, 'PurchasingResponsible');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (347, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (348, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 20, '{"ar":"اوفسيت","en":"Offset"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (327, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (336, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (338, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.104', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (339, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (340, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (337, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.109', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (334, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.116', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (329, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (331, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.686', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (332, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (343, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.181', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (349, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.182', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (350, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.183', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (351, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.185', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (344, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.186', 'hr1', 'hr1', 1, '{"ar":"فليكسو","en":"Flexo"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (341, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.193', 'hr1', 'hr1', 2, '{"ar":"عمرو خليل","en":"Amr Khalil"}', NULL, 'PurchasingResponsible');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (333, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (330, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (357, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (359, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (358, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (360, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (361, 1005, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (362, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (363, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (370, 1009, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (371, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (372, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (385, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (386, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (387, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (388, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (389, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (390, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (391, 1021, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (392, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (393, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (414, 1021, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/*Company Data*/
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (394, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.684', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (395, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.102', 'hr1', 'hr1', 7, '{"en":"AL Madina","ar":"المدينة"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (396, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.179', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (397, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (398, 1005, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (402, 1009, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (410, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (411, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (416, 1024, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) 
VALUES (417, 1042, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

/* PO :  code = 201800025  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (418, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (419, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (420, 1025, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (421, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (422, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/* PO :  code = 201800026  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (423, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{"en":"Corrugated","ar":"كوروجيتيد"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (424, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)

VALUES (425, 1026, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (426, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (427, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (352, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (353, 1013, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (354, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (355, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (406, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* PO :  code = 201800027  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (428, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (429, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (430, 1027, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (431, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (432, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/* PO :  code = 201800028  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (433, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{"en":"Corrugated","ar":"كوروجيتيد"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (434, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (435, 1028, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (436, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (437, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (493, 1023, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (443, 1035, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (444, 1035, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (445, 1035, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (446, 1035, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (447, 1035, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (448, 1036, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (449, 1036, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (450, 1036, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (451, 1036, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (452, 1036, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (453, 1037, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (454, 1037, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (455, 1037, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');


INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (456, 1037, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (457, 1037, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/*po 2018000044*/
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (458, 1044, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/*po 2018000045*/
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (459, 1045, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (460, 1045, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (461, 1045, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (462, 1045, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (463, 1045, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (494, 1057, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* PO :  code = 201800046  Draft PurchaseUnitCode = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (466, 1046, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (467, 1046, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* PO :  code = 201800047  Draft PurchaseUnitCode = 0002 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (471, 1047, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (472, 1047, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (473, 1047, '2018-08-05 09:20:14.352235', '2018-08-06 09:20:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (474, 1047, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (475, 1047, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

 /* PO :  code = 201800048  Draft PurchaseUnitCode = 0002 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (476, 1048, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (477, 1048, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (478, 1048, '2018-08-05 09:20:14.352235', '2018-08-06 09:20:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (479, 1048, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (480, 1048, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

 /* PO :  code = 201800049  Draft PurchaseUnitCode = 0002 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (481, 1049, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (482, 1049, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (483, 1049, '2018-08-05 09:20:14.352235', '2018-08-06 09:20:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (484, 1049, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (485, 1049, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

 /* PO :  code = 201800050  Draft PurchaseUnitCode = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (486, 1050, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (487, 1050, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* PO :  code = 201800047  Draft PurchaseUnitCode = 0002 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (488, 1051, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 2,  '{"ar":"ساين ميديا","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (489, 1051, '2018-08-05 09:20:17.53797', '2018-08-06 09:20:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (490, 1051, '2018-08-05 09:20:14.352235', '2018-08-06 09:20:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (491, 1051, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (492, 1051, '2018-08-05 09:20:35.018932', '2018-08-06 09:20:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (373, 1010, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (374, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (375, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (403, 1010, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (376, 1011, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (377, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (378, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (404, 1011, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (364, 1006, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (365, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (366, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (399, 1006, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (408, 1015, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (382, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (383, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (384, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (409, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (405, 1012, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (413, 1020, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (465, 1020, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"Madina Tech Plant","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (412, 1019, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (464, 1019, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"Madina Tech Plant","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (401, 1008, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (367, 1008, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (368, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (369, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (415, 1023, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO public.iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (497, 2091, '2020-12-17 12:23:46.672000', '2020-12-17 12:23:46.689000', 'hr1', 'hr1', 9, '{"ar":"مخزن المدينة الرئيسي","en":"AlMadina Main Store"}', null, 'Storehouse');

INSERT INTO public.iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (496, 2091, '2020-12-17 12:23:46.660000', '2020-12-17 12:23:46.687000', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', null, 'Plant');

INSERT INTO public.iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (495, 2091, '2020-12-17 12:23:46.645000', '2020-12-17 12:23:46.684000', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', null, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (498, 2091,  '2020-12-17 12:23:46.645000', '2020-12-17 12:23:46.684000', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Company');
 -------------------------------------- iobordercompany --------------------------------------------
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES(1, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2000, null, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (2, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1000, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (3, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1001, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (4, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1002, 7, 19, 'BILL_TO', 18);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (5, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1003, 10, 1, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (6, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1004, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (7, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1005, 10, 19, 'BILL_TO', 18);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (8, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1006, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (9, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1007, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (10, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1008, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (11, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1009, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (12, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1010, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (13, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1011, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (14, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1012, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (15, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1013, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (16, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1014, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (17, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1015, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (18, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1016, 10, 19, 'BILL_TO', 18);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (19, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1017, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (20, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1018, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (21, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1019, 10, 19, 'BILL_TO', 18);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (22, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1020, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (23, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1021, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (24, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1022, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (25, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1023, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (26, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1024, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (27, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1025, 10, 1, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (28, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1026, 10, 23, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (29, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1027, 10, 1, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole, bankAccountId) VALUES (30, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1028, 10, 23, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (31, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1029, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (32, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1030, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (33, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1031, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (34, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1032, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (35, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1034, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (36, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1035, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (37, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1036, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (38, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1037, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (39, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (40, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (41, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (42, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (43, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1104, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (44, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1105, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (45, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1106, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (46, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1038, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (47, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1039, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (48, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1040, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (49, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1041, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (50, '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', 1042, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (51, '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', 1043, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (52, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1044, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (53, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1045, 10, 23, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (54, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1046, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (55, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1047, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (56, '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', 1048, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (57, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1049, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (58, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1050, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole,bankaccountid) VALUES (59, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1051, 10, 19, 'BILL_TO',17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (60, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1052, null, 22, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (61, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1053, null, 21, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (62, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1054, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (63, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1055, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (64, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1056, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (65, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1057, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (66, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (67, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (68, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (69, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (70, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 104, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (71, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 105, null, 19, 'BILL_TO');

---- New PO ViewAll ----
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, bankaccountid, partyrole)
VALUES (158, '2020-10-01 08:58:54.994652', '2020-10-01 08:58:54.994652', 'Admin', 'Admin', 2087, 7, 19, 21, 'BILL_TO');

----- New PO For landed cost -----
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, bankaccountid, partyrole)
VALUES (162, '2020-12-17 10:21:47.901000', '2020-12-17 10:23:54.124000', 'hr1', 'hr1', 2091, 7, 19, 14, 'BILL_TO');