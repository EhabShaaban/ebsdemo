delete
from ioborderpaymenttermsdetails;
delete
from ioborderdeliverydetails;
delete
from iobordercontainersdetails;

/* Purchase Order 2018000001*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1001, 1001,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1001, 1001, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1001, 1001,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000002*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1002, 1002,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1002, 1002, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1002, 1002,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000003*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1003, 1003,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1003, 1003, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1003, 1003,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000004*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1004, 1004,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1004, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1004, 1004,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000005*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1005, 1005,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1005, 1005, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1005, 1005,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000006*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1006, 1006,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1006, 1006, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1006, 1006,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000007*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1007, 1007,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1007, 1007, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1007, 1007,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000008*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1008, 1008,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1008, 1008, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1008, 1008,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000009*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1009, 1009,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1009, 1009, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1009, 1009,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000010*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1010, 1010,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1010, 1010, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1010, 1010,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000011*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1011, 1011,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1011, 1011, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1011, 1011,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000012*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1012, 1012,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1012, 1012, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1012, 1012,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);


/* Purchase Order 2018000013*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1013, 1013,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1013, 1013, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1013, 1013,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000014*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1014, 1014,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1014, 1014, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1014, 1014,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000015*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1015, 1015,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1015, 1015, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1015, 1015,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000016*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1016, 1016,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1016, 1016, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1016, 1016,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000017*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1017, 1017,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1017, 1017, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1017, 1017,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000018*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1018, 1018,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1018, 1018, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1018, 1018,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000019*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1019, 1019,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1019, 1019, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1019, 1019,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000020*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1020, 1020,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1020, 1020, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1020, 1020,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000021*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1021, 1021,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1021, 1021, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1021, 1021,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000022*/

INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1022, 1022,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1022, 1022, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1022, 1022,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000023*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1023, 1023,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1023, 1023, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1023, 1023,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000024*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1024, 1024,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1024, 1024, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1024, 1024,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000025*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1025, 1025,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1025, 1025, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1025, 1025,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000026*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1026, 1026,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1026, 1026, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1026, 1026,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000027*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1027, 1027,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1027, 1027, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1027, 1027,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

/* Purchase Order 2018000028*/
INSERT INTO public.ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, collectiondate,
                                           numberofdays, deliverydate,
                                           incotermid, incotermname,
                                           modeoftransportid, modeoftransportname,
                                           shippinginstructionsid, shippinginstructionsname,
                                           loadingportid, loadingportname,
                                           dischargeportid, dischargeportname)
VALUES (1028, 1028,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1028, 1028, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, containerid, quantity)
VALUES (1028, 1028,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);
