delete
from ioborderlinedetailsquantities;
delete
from ioborderlinedetails;

INSERT INTO ioborderlinedetails (id,refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (23,
        1001,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (1,23, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 40.0, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 100, 0);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (24,
        1001,
        '2018-08-05 09:07:32.140484',
        '2018-08-06 09:04:17.697',
        'hr1',
        'hr1',
        34,
        NULL,
        23);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (2,24, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 20.0, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 15, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (25,
        1001,
        '2018-08-05 09:07:32.140484',
        '2018-08-06 09:04:17.697',
        'hr1',
        'hr1',
        35,
        NULL,
        4);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (3,25, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 10.0, 43, '{"en":"Drum-50Kg","ar":"Drum-50Kg"}', 15, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitofmeasureid)
VALUES (26,
        1002,
        '2018-08-05 09:11:35.018932',
        '2018-08-06 09:33:48.111',
        'hr1',
        'hr1',
        35,
        NULL,
        4);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (4,26, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100.0, 4, '{"en":"Kg","ar":"كجم"}', 10, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitofmeasureid)
VALUES (27,
        1002,
        '2018-08-05 09:12:42.938282',
        '2018-08-06 09:33:48.113',
        'hr1',
        'hr1',
        36,
        NULL,
        4);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (5,27, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100.0, 4, '{"en":"Kg","ar":"كجم"}', 12, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitofmeasureid)
VALUES (28,
        1003,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.188',
        'hr1',
        'hr1',
        35,
        NULL,
        4);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (6,28, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50.0, 43, '{"en":"Drum-50Kg","ar":"Drum-50Kg"}', 4, 0);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitofmeasureid)
VALUES (29,
        1003,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        36,
        NULL,
        4);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (7,29, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50.0, 4, '{"en":"Kg","ar":"كجم"}', 3, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (30,
        1022,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        34,
        18000.0,
        23);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (8,30, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (9,30, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (31,
        1023,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        34,
        18000.0,
        23);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (10,31, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (11,31, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);

----

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (32,
        1021,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        34,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (33,
        1006,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        34,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (34,
        1006,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        35,
        NULL,
        4);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (35,
        1025,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        37,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,
                                 creationdate,
                                 modifieddate,
                                 creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityInDn,
                                 unitofmeasureid)
VALUES (36,
        1026,
        '2018-08-05 12:13:08.51866',
        '2018-08-06 09:46:15.19',
        'hr1',
        'hr1',
        44,
        NULL,
        16);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (37,
        1013,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (38,
        1013,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);


INSERT INTO ioborderlinedetails (id,
                                 refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (40,
        1014,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        35,
        NULL,
        4);

INSERT INTO ioborderlinedetails (id,refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (41,
        1027,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        38,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (42,
        1028,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        44,
        NULL,
        16);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (43,
        1102,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (44,
        1103,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (45,
        1105,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (46,
        1007,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (47,
        1008,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (48,
        1009,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
                                refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (49,
        1011,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (50,
        1012,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (51,
        1015,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (52,
        1016,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (53,
        1017,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (54,
        1018,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (55,
        1019,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,
             refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (56,
        1020,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (57,
        1005,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

INSERT INTO ioborderlinedetails (id,refinstanceid,creationdate,
                                 modifieddate,creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (58,
        1010,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);




INSERT INTO ioborderlinedetails (id,
                                 refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (60,
        102,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);


INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (61,
        0103,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);



INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (62,
        104,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (12,62, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (13,62, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);



INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (63,
        105,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        34,
        NULL,
        23);



INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (14,63, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10 );
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (15,63, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}',
        3,
        0);



INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (18,32, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (19,32, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);


INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (20,37, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (21,37, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);


INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (22,38, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 100, 37, '{"en":"Roll 2.20x50","ar":"Roll 2.20x50"}', 2, 10);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                                  creationinfo, modificationinfo, quantity, orderunitid,
                                                  orderunitsymbol, price, discountpercentage)
VALUES (23,38, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 50, 39, '{"en":"Roll 2.70x50","ar":"Roll 2.70x50"}', 3, 0);
