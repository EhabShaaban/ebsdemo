DELETE FROM doborderdocument WHERE objecttypecode like '%_PO';
delete from iobenterprisedata;
delete from ioborderlinedetails;
delete from ioborderdocumentrequireddocuments;
DELETE from EntityUserCode WHERE type = 'DObPurchaseOrder';

INSERT INTO EntityUserCode (type, code)
values ('DObPurchaseOrder', '2018000999');

INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (2000, '2018000999', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1000, '2018000000', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1001, '2018000001', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1002, '2018000002', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "DeliveryComplete",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1003, '2018000003', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1004, '2018000004', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1005, '2018000005', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1006, '2018000006', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1007, '2018000007', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1008, '2018000008', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1009, '2018000009', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "FinishedProduction",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1010, '2018000010', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1011, '2018000011', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "Arrived",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1012, '2018000012', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1013, '2018000013', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1014, '2018000014', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1015, '2018000015', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1016, '2018000016', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "Approved",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1017, '2018000017', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "Confirmed",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1018, '2018000018', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1019, '2018000019', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "DeliveryComplete",  "NotReceived"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1020, '2018000020', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1021, '2018000021', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1022, '2018000022', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1023, '2018000023', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1024, '2018000024', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1025, '2018000025', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1026, '2018000026', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1027, '2018000027', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1028, '2018000028', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1029, '2018000029', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 11 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1030, '2018000030', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1031, '2018000031', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1032, '2018000032', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1100, '2018100000', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1101, '2018100001', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1102, '2018100002', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1103, '2018100003', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1104, '2018100004', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (1105, '2018100005', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0100, '2018000100', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0101, '2018000101', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0102, '2018000102', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0103, '2018000103', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0104, '2018000104', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'IMPORT_PO');
INSERT INTO public.doborderdocument ( id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, documentownerid, objecttypecode) VALUES (0105, '2018000105', '2018-08-05 09:02:00', '2018-08-05 09:02:00', 'hr1', 'hr1', '["Draft"]', 37 , 'LOCAL_PO');

INSERT INTO public.dobpurchaseorder (id) VALUES (2000);
INSERT INTO public.dobpurchaseorder (id) VALUES (1000);
INSERT INTO public.dobpurchaseorder (id) VALUES (1001);
INSERT INTO public.dobpurchaseorder (id) VALUES (1002);
INSERT INTO public.dobpurchaseorder (id) VALUES (1003);
INSERT INTO public.dobpurchaseorder (id) VALUES (1004);
INSERT INTO public.dobpurchaseorder (id) VALUES (1005);
INSERT INTO public.dobpurchaseorder (id) VALUES (1006);
INSERT INTO public.dobpurchaseorder (id) VALUES (1007);
INSERT INTO public.dobpurchaseorder (id) VALUES (1008);
INSERT INTO public.dobpurchaseorder (id) VALUES (1009);
INSERT INTO public.dobpurchaseorder (id) VALUES (1010);
INSERT INTO public.dobpurchaseorder (id) VALUES (1011);
INSERT INTO public.dobpurchaseorder (id) VALUES (1012);
INSERT INTO public.dobpurchaseorder (id) VALUES (1013);
INSERT INTO public.dobpurchaseorder (id) VALUES (1014);
INSERT INTO public.dobpurchaseorder (id) VALUES (1015);
INSERT INTO public.dobpurchaseorder (id) VALUES (1016);
INSERT INTO public.dobpurchaseorder (id) VALUES (1017);
INSERT INTO public.dobpurchaseorder (id) VALUES (1018);
INSERT INTO public.dobpurchaseorder (id) VALUES (1019);
INSERT INTO public.dobpurchaseorder (id) VALUES (1020);
INSERT INTO public.dobpurchaseorder (id) VALUES (1021);
INSERT INTO public.dobpurchaseorder (id) VALUES (1022);
INSERT INTO public.dobpurchaseorder (id) VALUES (1023);
INSERT INTO public.dobpurchaseorder (id) VALUES (1024);
INSERT INTO public.dobpurchaseorder (id) VALUES (1025);
INSERT INTO public.dobpurchaseorder (id) VALUES (1026);
INSERT INTO public.dobpurchaseorder (id) VALUES (1027);
INSERT INTO public.dobpurchaseorder (id) VALUES (1028);
INSERT INTO public.dobpurchaseorder (id) VALUES (1029);
INSERT INTO public.dobpurchaseorder (id) VALUES (1030);
INSERT INTO public.dobpurchaseorder (id) VALUES (1031);
INSERT INTO public.dobpurchaseorder (id) VALUES (1032);
INSERT INTO public.dobpurchaseorder (id) VALUES (1100);
INSERT INTO public.dobpurchaseorder (id) VALUES (1101);
INSERT INTO public.dobpurchaseorder (id) VALUES (1102);
INSERT INTO public.dobpurchaseorder (id) VALUES (1103);
INSERT INTO public.dobpurchaseorder (id) VALUES (1104);
INSERT INTO public.dobpurchaseorder (id) VALUES (1105);
INSERT INTO public.dobpurchaseorder (id) VALUES (100);
INSERT INTO public.dobpurchaseorder (id) VALUES (101);
INSERT INTO public.dobpurchaseorder (id) VALUES (102);
INSERT INTO public.dobpurchaseorder (id) VALUES (103);
INSERT INTO public.dobpurchaseorder (id) VALUES (104);
INSERT INTO public.dobpurchaseorder (id) VALUES (105);



INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (345, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 5, '{"ar":"أسماء الخضري","en":"Asmaa Elkhodary"}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (347, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (348, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 20, '{"ar":"اوفسيت","en":"Offset"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (327, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (336, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (338, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.104', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (339, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (340, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (337, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.109', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (334, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.116', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (329, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (331, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.686', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (332, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (343, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.181', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (349, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.182', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (350, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.183', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (351, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.185', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (344, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.186', 'hr1', 'hr1', 1, '{"ar":"فليكسو","en":"Flexo"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (341, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.193', 'hr1', 'hr1', 2, '{"ar":"عمرو خليل","en":"Amr Khalil"}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (333, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (330, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (352, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (353, 1013, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (354, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (355, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (357, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{"ar":"جيهان أحمد","en":"Gihan Ahmed"}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (359, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (358, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (360, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{"ar":"ساينج","en":"Signmedia"}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (361, 1005, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (362, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (363, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (364, 1006, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (365, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (366, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (367, 1008, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (368, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (369, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (370, 1009, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (371, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (372, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (373, 1010, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (374, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (375, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (376, 1011, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (377, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (378, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (379, 1014, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (380, 1014, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (381, 1014, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (382, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (383, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (384, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (385, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (386, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (387, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (388, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (389, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (390, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (391, 1021, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (392, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype) VALUES (393, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/*Company Data*/

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (394, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.684', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (395, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.102', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (396, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.179', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (397, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (398, 1005, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (399, 1006, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (400, 1007, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (401, 1008, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (402, 1009, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (403, 1010, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (404, 1011, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (405, 1012, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (406, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (407, 1014, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (408, 1015, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (409, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (410, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (411, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (412, 1019, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (413, 1020, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (414, 1021, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (415, 1024, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');

/* */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (417, 1023, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');

/* PO :  code = 201800025  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (418, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (419, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (420, 1025, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (421, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (422, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/* PO :  code = 201800026  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (423, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{"en":"Corrugated","ar":"كوروجيتيد"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (424, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (425, 1026, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (426, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (427, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/* PO :  code = 201800027  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (428, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{"en":"Flexo","ar":"فليكسو"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (429, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (430, 1027, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (431, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (432, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');

/* PO :  code = 201800028  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (433, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{"en":"Corrugated","ar":"كوروجيتيد"}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (434, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{"en":"DigiPro","ar":"دي جي برو"}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (435, 1028, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{"ar":"المدينة","en":"AL Madina"}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (436, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{"ar":"مصنع المدينة للتقنية","en":"Madina Tech Plant"}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (437, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{"ar":"مخزن المدينه الثانوي","en":"Madina Secondary Store"}', NULL, 'Storehouse');


INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (2, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2000, null, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (3, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1000, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (4, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1001, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (5, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1002, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (6, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1003, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (7, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1004, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (8, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1005, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (9, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1006, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (10, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1007, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (11, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1008, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (12, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1009, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (13, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1010, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (14, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1011, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (15, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1012, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole, bankaccountid)
VALUES (16, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1013, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (17, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1014, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (18, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1015, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (19, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1016, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (20, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1017, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (21, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1018, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (22, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1019, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (23, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1020, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole, bankaccountid)
VALUES (24, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1021, 10, 19, 'BILL_TO', 17);
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (25, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1022, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (26, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1023, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (27, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1024, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (28, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1025, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (29, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1026, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (30, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1027, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (31, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1028, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (32, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1029, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (33, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1030, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (34, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1031, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (35, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1032, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (36, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (37, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (38, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (39, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (40, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1104, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (41, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1105, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (42, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (43, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (44, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (45, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (46, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 104, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (47, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 105, null, 19, 'BILL_TO');


INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (100, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 2000, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (101, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1000, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (102, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1001, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (103, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1002, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (104, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1003, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (105, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1004, 47);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (106, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1005, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (107, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1006, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (108, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1007, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (109, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1008, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (110, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1009, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (111, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1010, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (112, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1011, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (113, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1012, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (114, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1013, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (115, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1014, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (116, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1015, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (117, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1016, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (118, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1017, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (119, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1018, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (120, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1019, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (121, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1020, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (122, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1021, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (123, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1022, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (124, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1023, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (125, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1024, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (126, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1025, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (127, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1026, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (128, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1027, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (129, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1028, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (130, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1029, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (131, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1030, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (132, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1031, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (133, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1032, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (134, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1100, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (135, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1101, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (136, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1102, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (137, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1103, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (138, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1104, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (139, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 1105, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (140, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 100, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (141, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 101, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (142, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 102, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (143, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 103, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (144, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 104, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (145, '2020-10-04 10:18:14.056364', '2020-10-04 10:18:14.056364', 'Admin', 'Admin', 105, 46);

