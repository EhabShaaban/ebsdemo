DELETE FROM iobpurchaseorderfulfillervendor;
DELETE FROM iobgoodsreceiptpurchaseorderdata;
DELETE FROM doborderdocument WHERE objecttypecode like '%_PO';

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (100, '2018000100', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (101, '2018000101', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (102, '2018000102', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (103, '2018000103', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (104, '2018000104', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (105, '2018000105', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1000, '2018000000', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1001, '2018000001', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1002, '2018000002', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "DeliveryComplete",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1003, '2018000003', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1004, '2018000004', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1005, '2018000005', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1006, '2018000006', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1007, '2018000007', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1008, '2018000008', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1009, '2018000009', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "FinishedProduction",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1010, '2018000010', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1011, '2018000011', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Arrived",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1012, '2018000012', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1013, '2018000013', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1014, '2018000014', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1015, '2018000015', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1016, '2018000016', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Approved",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1017, '2018000017', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Confirmed",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1018, '2018000018', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "Received"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1019, '2018000019', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "DeliveryComplete",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1020, '2018000020', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1021, '2018000021', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1022, '2018000022', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1023, '2018000023', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1024, '2018000024', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1025, '2018000025', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1026, '2018000026', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1027, '2018000027', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1028, '2018000028', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1029, '2018000029', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'IMPORT_PO', 11);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1030, '2018000030', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1031, '2018000031', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1032, '2018000032', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1034, '2018000034', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1035, '2018000035', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1036, '2018000036', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1037, '2018000037', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Approved",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1038, '2018000038', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1039, '2018000039', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1040, '2018000040', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1041, '2018000041', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1042, '2018000042', '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1043, '2018000043', '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1044, '2018000044', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "DeliveryComplete",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1045, '2018000045', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1046, '2018000046', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1047, '2018000047', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1048, '2018000048', '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1049, '2018000049', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1050, '2018000050', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1051, '2018000051', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1052, '2018000052', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 26);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1053, '2018000053', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 33);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1054, '2018000054', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1055, '2018000055', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "Received"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1056, '2018000056', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Cleared",  "Received"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1057, '2018000057', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1100, '2018100000', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1101, '2018100001', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1102, '2018100002', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1103, '2018100003', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1104, '2018100004', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1105, '2018100005', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1106, '2018100006', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2000, '2018000999', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

----- New PO ViewAll -----

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2087, '2020000004', '2020-08-05 07:02:00.000000', '2020-08-05 07:02:00.000000', 'Admin', 'Admin', '["Draft"]', 'SERVICE_PO', 37);

----- New PO For landed cost -----

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2091, '2020000048', '2020-12-17 10:21:47.888000', '2020-12-17 10:25:41.526000', 'hr1', 'hr1', '["InOperation","Active","Shipped","NotReceived"]', 'LOCAL_PO', 37);
