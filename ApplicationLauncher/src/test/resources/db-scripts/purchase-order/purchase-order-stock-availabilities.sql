delete
from dobpurchaseorder
where id between 2001 and 2003;
delete
from doborderdocument
where id between 2001 and 2003;
delete
from ioborderapprovalcycle
where id between 1000 and 1002;
delete
from ioborderapprover
where id between 66 and 70;
delete
from iobordercycledates
where id between 1000 and 1002;
delete
from ioborderdeliverydetails
where id between 2002 and 2004;
delete
from ioborderlinedetails
where id between 1000 and 1002;
delete
from ioborderlinedetailsquantities
where id between 1000 and 1003;
delete
from ioborderpaymenttermsdetails
where id between 20001 and 20003;
delete
from ioborderdocumentrequireddocuments
where id between 1000 and 1002;
---------------------------------------------po ---------------------------

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2001, '2020000001', '2020-02-04 12:06:32.274000', '2020-02-04 12:13:54.207000', 'hr1', 'hr1',
        '["Active","Cleared","NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2002, '2020000002', '2020-02-04 12:15:10.195000', '2020-02-04 12:19:19.399000', 'hr1', 'hr1',
        '["InOperation","Active","Shipped","NotReceived"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2003, '2020000003', '2020-02-04 12:19:54.425000', '2020-02-04 12:22:13.761000', 'hr1', 'hr1',
        '["InOperation","Active","Shipped","NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.dobpurchaseorder (id)
VALUES (2001);
INSERT INTO public.dobpurchaseorder (id)
VALUES (2002);
INSERT INTO public.dobpurchaseorder (id)
VALUES (2003);

-------approver cycle --------------

INSERT INTO public.ioborderapprovalcycle (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          approvalcyclenum, startdate, enddate, finaldecision)
VALUES (1000, 2001, '2020-02-04 12:09:38.401000', '2020-02-04 12:12:34.337000', 'hr1', 'hr1', 1,
        '2020-02-04 12:09:38.401000', '2020-02-04 12:12:34.314000', 'APPROVED');
INSERT INTO public.ioborderapprovalcycle (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          approvalcyclenum, startdate, enddate, finaldecision)
VALUES (1001, 2002, '2020-02-04 12:17:35.194000', '2020-02-04 12:18:50.237000', 'hr1', 'hr1', 1,
        '2020-02-04 12:17:35.194000', '2020-02-04 12:18:50.213000', 'APPROVED');
INSERT INTO public.ioborderapprovalcycle (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                          approvalcyclenum, startdate, enddate, finaldecision)
VALUES (1002, 2003, '2020-02-04 12:21:37.672000', '2020-02-04 12:21:49.386000', 'hr1', 'hr1', 1,
        '2020-02-04 12:21:37.672000', '2020-02-04 12:21:49.349000', 'APPROVED');

------------ order approver ---------------

INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (66, 1000, '2020-02-04 12:09:38.406000', '2020-02-04 12:11:04.758000', 'hr1', 'Ashraf.Salah', 1, 34,
        '2020-02-04 12:11:04.746000', null, 'APPROVED');
INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (67, 1000, '2020-02-04 12:09:38.407000', '2020-02-04 12:11:41.019000', 'hr1', 'Mohamed.Abdelmoniem', 2, 44,
        '2020-02-04 12:11:41.010000', null, 'APPROVED');
INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (68, 1000, '2020-02-04 12:09:38.407000', '2020-02-04 12:12:34.323000', 'hr1', 'Essam.Mahmoud', 3, 29,
        '2020-02-04 12:12:34.314000', null, 'APPROVED');
INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (69, 1001, '2020-02-04 12:17:35.198000', '2020-02-04 12:18:50.222000', 'hr1', 'Mohamed.Abdelmoniem', 2, 44,
        '2020-02-04 12:18:50.213000', null, 'APPROVED');
INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                     controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (70, 1002, '2020-02-04 12:21:37.677000', '2020-02-04 12:21:49.361000', 'hr1', 'Mohamed.Abdelmoniem', 2, 44,
        '2020-02-04 12:21:49.349000', null, 'APPROVED');

-----------------------------order cycle dates --------------------------------

INSERT INTO public.iobordercycledates (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                       actualpirequesteddate, actualconfirmationdate, actualproductionfinisheddate,
                                       actualshippingdate, actualarrivaldate, actualclearancedate,
                                       actualdeliverycompletedate)
VALUES (1000, 2001, '2020-02-04 12:09:35.291000', '2020-02-04 12:13:54.221000', 'hr1', 'hr1',
        '2020-02-18 22:12:00.000000', '2020-02-18 09:11:00.000000', '2020-02-26 09:11:00.000000',
        '2020-02-27 09:11:00.000000', '2020-04-30 09:11:00.000000', '2020-05-30 09:11:00.000000', null);
INSERT INTO public.iobordercycledates (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                       actualpirequesteddate, actualconfirmationdate, actualproductionfinisheddate,
                                       actualshippingdate, actualarrivaldate, actualclearancedate,
                                       actualdeliverycompletedate)
VALUES (1001, 2002, '2020-02-04 12:17:32.061000', '2020-02-04 12:19:19.416000', 'hr1', 'hr1',
        '2020-02-11 22:12:00.000000', '2020-02-22 09:11:00.000000', null, '2020-02-29 09:11:00.000000', null, null,
        null);
INSERT INTO public.iobordercycledates (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                       actualpirequesteddate, actualconfirmationdate, actualproductionfinisheddate,
                                       actualshippingdate, actualarrivaldate, actualclearancedate,
                                       actualdeliverycompletedate)
VALUES (1002, 2003, '2020-02-04 12:21:30.221000', '2020-02-04 12:22:13.773000', 'hr1', 'hr1',
        '2020-02-17 22:12:00.000000', '2020-03-27 22:12:00.000000', null, '2020-05-24 22:12:00.000000', null, null,
        null);

---------------------------- order delivery details ------------------------

INSERT INTO public.ioborderdeliverydetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                            modificationinfo, collectiondate, numberofdays, deliverydate, incotermid,
                                            incotermname, modeoftransportid, modeoftransportname,
                                            shippinginstructionsid, shippinginstructionsname, loadingportid,
                                            loadingportname, dischargeportid, dischargeportname)
VALUES (2002, 2001, '2020-02-04 12:08:45.545000', '2020-02-04 12:08:45.564000', 'hr1', 'hr1',
        '2020-02-10 00:00:00.000000', null, null, 2, 'CIF', 9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }', 1, '{
    "ar": "اللف بالشيكارتون",
    "en": "Ship by Shecartoon"
  }', 2005, '{
    "ar": "Antwerp",
    "en": "Antwerp"
  }', 2006, '{
    "ar": "Kaohsiung",
    "en": "Kaohsiung"
  }');
INSERT INTO public.ioborderdeliverydetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                            modificationinfo, collectiondate, numberofdays, deliverydate, incotermid,
                                            incotermname, modeoftransportid, modeoftransportname,
                                            shippinginstructionsid, shippinginstructionsname, loadingportid,
                                            loadingportname, dischargeportid, dischargeportname)
VALUES (2003, 2002, '2020-02-04 12:17:11.532000', '2020-02-04 12:17:11.551000', 'hr1', 'hr1',
        '2020-02-25 00:00:00.000000', null, null, 1, 'FOB', 10, '{
    "ar": "نقل بري",
    "en": "By Land"
  }', 1, '{
    "ar": "اللف بالشيكارتون",
    "en": "Ship by Shecartoon"
  }', 2007, '{
    "ar": "Barcelona",
    "en": "Barcelona"
  }', null, '{}');
INSERT INTO public.ioborderdeliverydetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                            modificationinfo, collectiondate, numberofdays, deliverydate, incotermid,
                                            incotermname, modeoftransportid, modeoftransportname,
                                            shippinginstructionsid, shippinginstructionsname, loadingportid,
                                            loadingportname, dischargeportid, dischargeportname)
VALUES (2004, 2003, '2020-02-04 12:21:13.854000', '2020-02-04 12:21:13.870000', 'hr1', 'hr1',
        '2020-02-29 00:00:00.000000', null, null, 1, 'FOB', 10, '{
    "ar": "نقل بري",
    "en": "By Land"
  }', 1, '{
    "ar": "اللف بالشيكارتون",
    "en": "Ship by Shecartoon"
  }', null, '{}', null, '{}');

-------------------------------order line details----------------------

INSERT INTO public.ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, quantityindn, unitofmeasureid)
VALUES (1000, 2001, '2020-02-04 12:07:20.559000', '2020-02-04 12:08:01.086000', 'hr1', 'hr1', 57, 100, 23);
INSERT INTO public.ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, quantityindn, unitofmeasureid)
VALUES (1001, 2002, '2020-02-04 12:16:20.201000', '2020-02-04 12:16:32.242000', 'hr1', 'hr1', 57, 1200, 23);
INSERT INTO public.ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, quantityindn, unitofmeasureid)
VALUES (1002, 2003, '2020-02-04 12:20:32.121000', '2020-02-04 12:20:54.796000', 'hr1', 'hr1', 57, 12, 23);

------------------------------ order line details quantities---------------------------------

INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                                  discountpercentage, itemvendorrecordid)
VALUES (1000, 1000, '2020-02-04 12:07:45.268000', '2020-02-04 12:07:45.289000', 'hr1', 'hr1', 50, 23, null, 2, 0, 62);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                                  discountpercentage, itemvendorrecordid)
VALUES (1001, 1000, '2020-02-04 12:08:01.093000', '2020-02-04 12:08:01.110000', 'hr1', 'hr1', 10, 41, null, 2, 0, 63);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                                  discountpercentage, itemvendorrecordid)
VALUES (1002, 1001, '2020-02-04 12:16:32.248000', '2020-02-04 12:16:32.265000', 'hr1', 'hr1', 2, 23, null, 2, 0, 62);
INSERT INTO public.ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                  modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                                  discountpercentage, itemvendorrecordid)
VALUES (1003, 1002, '2020-02-04 12:20:44.328000', '2020-02-04 12:20:44.346000', 'hr1', 'hr1', 12, 23, null, 2, 0, 62);

------------------------------ ordre payment details -------------------------------------

INSERT INTO public.ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                modificationinfo, paymenttermid, paymenttermname, currencyid,
                                                currencyname)
VALUES (20001, 2001, '2020-02-04 12:08:45.517000', '2020-02-04 12:08:51.211000', 'hr1', 'hr1', 6, '{
  "ar": "20% عربون, 80% T/T قبل الشحن",
  "en": "20% Deposit, 80% T/T Before Loading"
}', 2, '{
  "ar": "دولار أمريكي",
  "en": "United States Dollar"
}');
INSERT INTO public.ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                modificationinfo, paymenttermid, paymenttermname, currencyid,
                                                currencyname)
VALUES (20002, 2002, '2020-02-04 12:17:11.496000', '2020-02-04 12:17:11.554000', 'hr1', 'hr1', 6, '{
  "ar": "20% عربون, 80% T/T قبل الشحن",
  "en": "20% Deposit, 80% T/T Before Loading"
}', 1, '{
  "ar": "جنيه مصري",
  "en": "Egyptian Pound"
}');
INSERT INTO public.ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                modificationinfo, paymenttermid, paymenttermname, currencyid,
                                                currencyname)
VALUES (20003, 2003, '2020-02-04 12:21:13.816000', '2020-02-04 12:21:13.875000', 'hr1', 'hr1', 6, '{
  "ar": "20% عربون, 80% T/T قبل الشحن",
  "en": "20% Deposit, 80% T/T Before Loading"
}', 2, '{
  "ar": "دولار أمريكي",
  "en": "United States Dollar"
}');

----------------------------------order required document------------------------------------------

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (1000, 2001, '2020-02-04 12:08:54.926000', '2020-02-04 12:08:54.943000', 'hr1', 'hr1', 1, 1);
INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (1001, 2002, '2020-02-04 12:17:20.320000', '2020-02-04 12:17:20.334000', 'hr1', 'hr1', 1, 2);
INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (1002, 2003, '2020-02-04 12:21:21.436000', '2020-02-04 12:21:21.450000', 'hr1', 'hr1', 1, 2);

----------------------------------------------------------
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (72, '2020-02-04 12:06:32.274000', '2020-02-04 12:13:54.207000', 'hr1', 'hr1', 2001, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (73, '2020-02-04 12:15:10.195000', '2020-02-04 12:19:19.399000', 'hr1', 'hr1', 2002, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (74, '2020-02-04 12:19:54.425000', '2020-02-04 12:22:13.761000', 'hr1', 'hr1', 2003, null, 19, 'BILL_TO');

-----------------------------------------------------------

INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (73, '2020-10-04 09:52:15.292205', '2020-10-04 09:52:15.292205', 'Admin', 'Admin', 2001, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (74, '2020-10-04 09:52:15.292205', '2020-10-04 09:52:15.292205', 'Admin', 'Admin', 2002, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (75, '2020-10-04 09:52:15.292205', '2020-10-04 09:52:15.292205', 'Admin', 'Admin', 2003, 46);
