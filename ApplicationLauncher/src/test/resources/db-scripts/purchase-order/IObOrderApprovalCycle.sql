DELETE FROM IObOrderApprover;
DELETE FROM IObOrderApprovalCycle;

-- First approval cycle for 20180005
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (1, 1005, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'REJECTED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (1, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'REJECTED', 'No cash flow for Sigmedia');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (1, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 44);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (1, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 3, 29);

-- Second approval cycle for 20180005
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (2, 1005, '2018-09-21 13:00:00', '2018-09-21 15:00:00', 'hr1', 'hr1', '2018-09-21 13:00:00', '2018-09-21 15:00:00', 'APPROVED', 2);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (2, '2018-09-21 13:00:00', '2018-09-21 13:30:00', 'hr1', 'hr1', 1, 34, '2018-09-21 13:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (2, '2018-09-21 13:00:00', '2018-09-21 14:00:00', 'hr1', 'hr1', 2, 44, '2018-09-21 14:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (2, '2018-09-21 13:00:00', '2018-09-21 15:00:00', 'hr1', 'hr1', 3, 29, '2018-09-21 15:00:00', 'APPROVED');
        
-- First approval cycle for 2018000032
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (3, 1032, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 11:00:00', 'REJECTED', 1);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (3, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 4, 17, '2018-09-21 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (3, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 5, 44, '2018-09-21 10:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (3, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 20, '2018-09-21 11:00:00', 'REJECTED');
        
-- Second approval cycle for 2018000032
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (4, 1032, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', '2018-09-21 13:00:00', NULL, NULL, 2);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (4, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', 4, 17, '2018-09-21 13:15:00', 'APPROVED', 'No cash flow for Flexo');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (4, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 5, 44);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (4, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 20);
        
-- First approval cycle for 2018000029
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (5, 1029, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 11:00:00', 'APPROVED', 1);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (5, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 7, 17, '2018-09-21 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (5, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 8, 44, '2018-09-21 10:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (5, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 9, 20, '2018-09-21 11:00:00', 'APPROVED');
        
-- Second approval cycle for 2018000029
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (6, 1029, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', '2018-09-21 13:00:00', '2018-09-21 13:15:00', 'REJECTED', 2);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (6, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', 7, 17, '2018-09-21 13:15:00', 'REJECTED', 'No cash flow for Corrugated');
        
-- First approval cycle for 2018000016
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (7, 1016, '2018-09-20 13:00:00', '2018-09-20 13:00:00', 'hr1', 'hr1', '2018-09-20 13:00:00', '2018-09-20 13:15:00', 'REJECTED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (7, '2018-09-20 13:00:00', '2018-09-20 13:00:00', 'hr1', 'hr1', 1, 34, '2018-09-20 13:15:00', 'REJECTED', 'No cash flow for Sigmedia');
        
-- Second approval cycle for 2018000016
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (8, 1016, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 11:00:00', 'APPROVED', 2);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (8, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 1, 34, '2018-09-21 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (8, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 44, '2018-09-21 10:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (8, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 3, 29, '2018-09-21 11:00:00', 'APPROVED');
        
-- First approval cycle for 2018000030
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (9, 1030, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 11:00:00', 'APPROVED', 1);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (9, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 4, 17, '2018-09-21 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (9, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 5, 44, '2018-09-21 10:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (9, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 20, '2018-09-21 11:00:00', 'APPROVED');
        
-- Second approval cycle for 2018000030
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (10, 1030, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', '2018-09-21 13:00:00', '2018-09-21 13:15:00', 'REJECTED', 2);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (10, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', 4, 17, '2018-09-21 13:15:00', 'REJECTED', 'No cash flow for Flexo');
        
-- First approval cycle for 2018000031
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (11, 1031, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 11:00:00', 'APPROVED', 1);
         
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (11, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 7, 17, '2018-09-21 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (11, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 8, 44, '2018-09-21 10:30:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (11, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 9, 20, '2018-09-21 11:00:00', 'APPROVED');
        
-- Second approval cycle for 2018000031
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (12, 1031, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', '2018-09-21 13:00:00', '2018-09-21 13:15:00', 'REJECTED', 2);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (12, '2018-09-21 13:00:00', '2018-09-21 13:00:00', 'hr1', 'hr1', 7, 17, '2018-09-21 13:15:00', 'REJECTED', 'No cash flow for Corrugated');
        
-- First approval cycle for 2018000015 
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (13, 1015, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00', NULL, NULL, 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (13, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 29, '2019-01-07 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (13, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', 1, 34);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (13, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 44);

-- First approval cycle for 2018000034 
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)

VALUES (14, 1034, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (14, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 30, '2019-01-07 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (14, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', 1, 35);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (14, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 45);

-- First approval cycle for 2018000035 
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)

VALUES (15, 1035, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (15, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 31, '2019-01-07 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (15, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', 1, 36);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (15, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 46);
        
-- First approval cycle for 2018000036
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (16, 1036, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (16, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 32, '2019-01-07 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (16, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', 1, 37);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (16, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 47);
        
-- First approval cycle for 2018000037
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (17, 1037, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (17, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 33, '2019-01-07 10:00:00', 'APPROVED');
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (17, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', 1, 38);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (17, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 48);
        
-- First approval cycle for 2018000009
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (18, 1009, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (18, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000017
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (19, 1017, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (19, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 35, '2019-01-07 10:00:00', 'APPROVED');

-- First approval cycle for 2018000011
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (20, 1011, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (20, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000010
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (21, 1010, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (21, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000042
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (22, 1042, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (22, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000043
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (23, 1043, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (23, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000018
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (24, 1018, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (24, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000023
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (25, 1023, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (25, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000003
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (26, 1003, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (26, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');

-- First approval cycle for 2018000045
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (27, 1045, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (27, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000008
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (28, 1008, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (28, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000038
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (29, 1038, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (29, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');

-- First approval cycle for 2018000039
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (30, 1039, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 'hr1', 'hr1', '2019-01-07 09:20:00',  '2018-09-21 11:00:00', 'APPROVED', 1);
        
INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES (30, '2019-01-07 09:20:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 6, 34, '2019-01-07 10:00:00', 'APPROVED');
        
-- First approval cycle for 2018000007
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (31, 1007, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', null, 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (31, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (31, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 2, 34);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid)
VALUES (31, '2018-09-21 09:00:00', '2018-09-21 09:00:00', 'hr1', 'hr1', 3, 29);

-- First approval cycle for 2018000047
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (32, 1047, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (32, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2018000051
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (33, 1051, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (33, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2018000049
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (34, 1049, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (34, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2018000048
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (35, 1048, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (35, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2018000046
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (36, 1046, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (36, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2018000050
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, startdate, enddate, finaldecision, approvalcyclenum)
VALUES (37, 1050, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision, notes)
VALUES (37, '2018-09-21 09:00:00', '2018-09-21 09:15:00', 'hr1', 'hr1', 1, 34, '2018-09-21 09:15:00', 'APPROVED', null);

-- First approval cycle for 2020000048
INSERT INTO public.ioborderapprovalcycle (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, approvalcyclenum, startdate, enddate, finaldecision)
VALUES (1003, 2091, '2020-12-17 10:24:10.729000', '2020-12-17 10:24:34.733000', 'hr1', 'hr1', 1, '2020-12-17 10:24:10.729000', '2020-12-17 10:24:34.670000', 'APPROVED');

INSERT INTO public.ioborderapprover (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, notes, decision)
VALUES (71, 1003, '2020-12-17 10:24:10.734000', '2020-12-17 10:24:34.701000', 'hr1', 'Mohamed.Abdelmoniem', 2, 44, '2020-12-17 10:24:34.670000', null, 'APPROVED');