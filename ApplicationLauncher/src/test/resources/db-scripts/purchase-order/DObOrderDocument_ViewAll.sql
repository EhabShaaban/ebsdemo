DELETE FROM dobpurchaseorder;
DELETE FROM doborderdocument where id > 0 and id < 1045;

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1, '2018000001', '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', '["Active","Cleared"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (2, '2018000002', '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', '["Active","DeliveryComplete"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1006, '2018000006', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1007, '2018000007', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1008, '2018000008', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active",  "InOperation", "Confirmed"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1010, '2018000010', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1011, '2018000011', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "Arrived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1012, '2018000012', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1013, '2018000013', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1014, '2018000014', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1015, '2018000015', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1016, '2018000016', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Approved"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1018, '2018000018', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Shipped"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1019, '2018000019', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "DeliveryComplete"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1020, '2018000020', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Inactive", "Cancelled"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1021, '2018000021', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1025, '2018000025', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1026, '2018000026', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1027, '2018000027', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1028, '2018000028', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1030, '2018000030', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active" , "InOperation","OpenForUpdates"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1032, '2018000032', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "WaitingApproval"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, objecttypecode, documentownerid)
VALUES (1034, '2018000034', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Active", "InOperation", "Approved"]', 'IMPORT_PO', 37);