DELETE
FROM dobpurchaseorder
where id >= 2018100002
  AND id <= 2018100010;
DELETE
FROM doborderdocument
where id >= 2018100002
  AND id <= 2018100010
  AND objecttypecode LIKE '%_PO';

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100002, '2018100002', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'LOCAL_PO');

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100003, '2018100003', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'LOCAL_PO');

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100004, '2018100004', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'LOCAL_PO');


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100005, '2018100005', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'LOCAL_PO');

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100006, '2018100006', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'IMPORT_PO');

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100007, '2018100007', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'IMPORT_PO');


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100008, '2018100008', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'IMPORT_PO');


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100009, '2018100009', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'IMPORT_PO');


INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, currentstates, documentownerid, objecttypecode)
VALUES (2018100010, '2018100010', '2018-08-05 09:02:00', '2018-08-05 09:02:00',
        'hr1', 'hr1', '["Draft"]', 1, 'IMPORT_PO');


-- PurchaseOrder-----------------------------------------------------------------------------------------------

INSERT INTO public.dobpurchaseorder (id) VALUES (2018100002);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100003);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100004);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100005);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100006);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100007);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100008);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100009);
INSERT INTO public.dobpurchaseorder (id) VALUES (2018100010);

INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (88, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100002, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (89, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100005, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (90, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100006, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (91, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100003, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (92, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100009, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (93, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100010, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (94, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100008, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (95, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100004, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, companyid, businessunitid, partyrole) VALUES (96, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2018100007, null, 1, 'BILL_TO');
