DELETE FROM doborderdocument WHERE objecttypecode like '%_PO';
delete
from iobenterprisedata;
delete
from ioborderlinedetails;
delete
from ioborderdocumentrequireddocuments;
DELETE
from EntityUserCode
WHERE type = 'DObPurchaseOrder';
ALTER SEQUENCE doborderdocument_id_seq RESTART WITH 2092;

INSERT INTO EntityUserCode (type, code)
values ('DObPurchaseOrder', '2018000999');

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2000, '2018000999', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2091, '2020000048', '2020-12-17 10:21:47.888000', '2020-12-17 10:25:41.526000', 'hr1', 'hr1',
        '["InOperation","Active","Shipped","NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1000, '2018000000', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1001, '2018000001', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1002, '2018000002', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "DeliveryComplete",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1003, '2018000003', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1004, '2018000004', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1005, '2018000005', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1006, '2018000006', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1007, '2018000007', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1012, '2018000012', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Inactive", "Cancelled"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1013, '2018000013', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1014, '2018000014', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1015, '2018000015', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "WaitingApproval"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1016, '2018000016', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Approved"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1017, '2018000017', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Confirmed"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1018, '2018000018', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1019, '2018000019', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "DeliveryComplete"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1020, '2018000020', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Inactive", "Cancelled"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1024, '2018000024', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1027, '2018000027', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1028, '2018000028', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1030, '2018000030', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1031, '2018000031', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates"]', 'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1101, '2018100001', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1103, '2018100003', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1105, '2018100005', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (101, '2018000101', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (103, '2018000103', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (105, '2018000105', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'LOCAL_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1008, '2018000008', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1009, '2018000009', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "FinishedProduction",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1010, '2018000010', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1011, '2018000011', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Arrived",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1021, '2018000021', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1022, '2018000022', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1023, '2018000023', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1025, '2018000025', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1026, '2018000026', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1029, '2018000029', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates"]', 'IMPORT_PO', 11);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1032, '2018000032', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "WaitingApproval",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1100, '2018100000', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1102, '2018100002', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1104, '2018100004', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (100, '2018000100', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (102, '2018000102', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (104, '2018000104', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', '["Draft"]',
        'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1034, '2018000034', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1035, '2018000035', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);


INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1036, '2018000036', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Approved",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1037, '2018000037', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Approved",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1038, '2018000038', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1039, '2018000039', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Confirmed",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1040, '2018000040', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1041, '2018000041', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Confirmed",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1042, '2018000042', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1043, '2018000043', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1044, '2018000044', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "DeliveryComplete",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1045, '2018000045', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1046, '2018000046', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1047, '2018000047', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1048, '2018000048', '2018-08-05 09:20:00.000000', '2018-08-05 09:20:00.000000', 'hr1', 'hr1',
        '["Active", "InOperation", "Shipped",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1049, '2018000049', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1050, '2018000050', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Arrived",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1051, '2018000051', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active", "Cleared",  "NotReceived"]', 'IMPORT_PO', 37);

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (1057, '2018000057', '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active" , "InOperation","OpenForUpdates",  "NotReceived"]', 'LOCAL_PO', 37);

INSERT INTO public.dobpurchaseorder (id)
VALUES (2000);
INSERT INTO public.dobpurchaseorder (id)
VALUES (2091);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1000);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1001);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1002);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1003);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1004);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1005);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1006);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1007);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1008);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1009);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1010);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1011);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1012);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1013);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1014);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1015);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1016);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1017);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1018);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1019);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1020);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1021);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1022);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1023);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1024);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1025);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1026);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1027);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1028);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1029);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1030);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1031);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1032);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1034);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1035);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1036);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1037);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1038);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1039);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1040);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1041);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1042);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1043);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1044);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1045);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1046);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1047);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1048);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1049);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1050);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1051);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1057);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1100);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1101);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1102);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1103);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1104);
INSERT INTO public.dobpurchaseorder (id)
VALUES (1105);
INSERT INTO public.dobpurchaseorder (id)
VALUES (100);
INSERT INTO public.dobpurchaseorder (id)
VALUES (101);
INSERT INTO public.dobpurchaseorder (id)
VALUES (102);
INSERT INTO public.dobpurchaseorder (id)
VALUES (103);
INSERT INTO public.dobpurchaseorder (id)
VALUES (104);
INSERT INTO public.dobpurchaseorder (id)
VALUES (105);

/***********************************************************************************************************************************/


INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (345, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 5, '{
  "ar": "أسماء الخضري",
  "en": "Asmaa Elkhodary"
}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (347, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (348, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 20, '{
  "ar": "اوفسيت",
  "en": "Offset"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (327, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{
  "ar": "جيهان أحمد",
  "en": "Gihan Ahmed"
}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (336, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (338, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.104', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (339, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (340, 1002, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (337, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.109', 'hr1', 'hr1', 19, '{
  "ar": "ساينج",
  "en": "Signmedia"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (334, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.116', 'hr1', 'hr1', 3, '{
  "ar": "جيهان أحمد",
  "en": "Gihan Ahmed"
}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (329, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (331, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.686', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (332, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (343, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.181', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (349, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.182', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Payer');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (350, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.183', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (351, 1003, '2018-08-05 12:13:08.51866', '2018-08-06 09:46:15.185', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (344, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.186', 'hr1', 'hr1', 1, '{
  "ar": "فليكسو",
  "en": "Flexo"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (341, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.193', 'hr1', 'hr1', 2, '{
  "ar": "عمرو خليل",
  "en": "Amr Khalil"
}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (333, 1001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (330, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{
  "ar": "ساينج",
  "en": "Signmedia"
}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (352, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{
  "ar": "ساينج",
  "en": "Signmedia"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (353, 1013, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (354, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (355, 1013, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (357, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.7', 'hr1', 'hr1', 3, '{
  "ar": "جيهان أحمد",
  "en": "Gihan Ahmed"
}', NULL, 'PurchasingResponsible');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (359, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (358, 1023, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (360, 1023, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 19, '{
  "ar": "ساينج",
  "en": "Signmedia"
}', NULL, 'PurchasingUnit');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (361, 1005, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (362, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (363, 1005, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (364, 1006, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (365, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (366, 1006, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (367, 1008, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (368, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (369, 1008, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (370, 1009, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (371, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (372, 1009, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (373, 1010, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (374, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (375, 1010, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (379, 1014, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (380, 1014, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (381, 1014, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (382, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (383, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (384, 1016, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (385, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (386, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (387, 1017, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (388, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (389, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (390, 1018, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (391, 1021, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (392, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (393, 1021, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

/*Company Data*/

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (394, 1001, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.684', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (395, 1002, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.102', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (396, 1003, '2018-08-05 09:20:14.103959', '2018-08-06 09:46:15.179', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (397, 1004, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (398, 1005, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (399, 1006, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (400, 1007, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (401, 1008, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (402, 1009, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (403, 1010, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (404, 1011, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (405, 1012, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (406, 1013, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (407, 1014, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (408, 1015, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (409, 1016, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (410, 1017, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (411, 1018, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (412, 1019, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (413, 1020, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (414, 1021, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (415, 1024, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

/* */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (417, 1023, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');

/* PO :  code = 201800025  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (418, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{
  "en": "Flexo",
  "ar": "فليكسو"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (419, 1025, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (420, 1025, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (421, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (422, 1025, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

/* PO :  code = 201800026  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (423, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{
  "en": "Corrugated",
  "ar": "كوروجيتيد"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (424, 1026, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (425, 1026, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (426, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (427, 1026, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

/* PO :  code = 201800027  Draft PurchaseUnitCode = 0001 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (428, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 1, '{
  "en": "Flexo",
  "ar": "فليكسو"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (429, 1027, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (430, 1027, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (431, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (432, 1027, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

/* PO :  code = 201800028  Draft PurchaseUnitCode = 0006 ConsigneeCode = 0001 PlantCode = 0001  StorehouseCode  = 0002 Company = 0002 */
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (433, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 23, '{
  "en": "Corrugated",
  "ar": "كوروجيتيد"
}', NULL, 'PurchasingUnit');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (434, 1028, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.693', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (435, 1028, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.103', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (436, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.108', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (437, 1028, '2018-08-05 09:11:35.018932', '2018-08-06 09:33:48.105', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (438, 1050, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 10, '{
  "en": "DigiPro",
  "ar": "دي جي برو"
}', NULL, 'Company');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (439, 1050, '2018-08-05 09:02:17.53797', '2018-08-06 09:04:17.685', 'hr1', 'hr1', 7, '{
  "ar": "المدينة",
  "en": "AL Madina"
}', NULL, 'Consignee');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (440, 1050, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.692', 'hr1', 'hr1', 8, '{
  "ar": "مصنع المدينة للتقنية",
  "en": "Madina Tech Plant"
}', NULL, 'Plant');

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (441, 1050, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

-- -------------------------------------------------------------------------------------------------
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (48, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1001, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (49, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1002, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (50, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1003, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (51, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1004, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (52, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1005, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (53, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1006, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (54, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1007, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (55, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1008, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (56, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1009, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (57, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1010, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (58, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1011, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (59, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1012, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (60, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1013, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (61, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1014, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (62, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1015, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (63, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1016, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (64, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1017, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (65, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1018, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (66, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1019, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (67, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1020, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (68, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1021, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (69, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1024, 10, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (70, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1025, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (71, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1026, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (72, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1027, 10, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (73, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1028, 10, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (74, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1050, 10, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (75, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1042, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (76, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (77, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 101, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (78, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1043, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (79, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1105, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (80, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (81, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1031, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (82, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1104, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (83, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 104, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (84, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (85, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1023, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (86, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1000, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (87, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 2000, null, 20, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (88, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1029, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (89, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1022, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (90, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1102, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (91, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 100, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (92, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (93, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1032, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (94, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 103, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (95, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1035, null, 23, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (96, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 105, null, 19, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (97, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1034, null, 1, 'BILL_TO');
INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (98, '2018-08-05 09:02:00.000000', '2018-08-05 09:02:00.000000', 'hr1', 'hr1', 1030, null, 1, 'BILL_TO');

-- -------------------------------------------------------------------------------------------------
INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (1, 1001, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (2, 1001, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (3, 1005, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (4, 1005, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (5, 1006, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (6, 1006, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (7, 1008, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (8, 1008, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (9, 1009, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (10, 1009, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (11, 1010, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (12, 1010, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (13, 1011, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (14, 1011, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (15, 1013, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (16, 1013, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (17, 1014, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (18, 1014, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (19, 1016, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (20, 1016, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (21, 1017, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (22, 1017, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (23, 1018, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (24, 1018, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (25, 1021, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (26, 1021, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (27, 1025, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (28, 1025, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (29, 1026, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (30, 1026, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (31, 1027, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (32, 1027, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (33, 1028, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (34, 1028, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 4, 4);

INSERT INTO public.ioborderdocumentrequireddocuments (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                      modificationinfo, numberofcopies, attachmenttypeid)
VALUES (35, 1050, '2018-09-29 07:40:19.423000', '2018-09-30 07:40:25.730000', 'hr1', 'hr1', 5, 5);
-- -------------------------------------------------------------------------------------------------
delete
from ioborderdeliverydetails
where id = 1999;

INSERT INTO ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, collectiondate,
                                    numberofdays, deliverydate,
                                    incotermid, incotermname,
                                    modeoftransportid, modeoftransportname,
                                    shippinginstructionsid, shippinginstructionsname,
                                    loadingportid, loadingportname,
                                    dischargeportid, dischargeportname)
VALUES (1999, 1050,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        30, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

delete
from ioborderpaymenttermsdetails
where id = 1999;

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (1999, 1050, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

delete
from iobordercontainersdetails
where id = 1999;

INSERT INTO iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                      creationinfo, modificationinfo, containerid, quantity)
VALUES (1999, 1050,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);
-- -------------------------------------------------------------------------------------------------
delete
from ioborderlinedetails
where id = 1999;

INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                 quantityindn, unitOfMeasureId)
VALUES (1999, 1050, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.695', 'hr1', 'hr1', 33, 10020, 23);

delete
from ioborderlinedetailsquantities
where id = 1999;

INSERT INTO ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, quantity, orderunitid,
                                           orderunitsymbol, price, discountpercentage)
VALUES (1999, 1999, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 40.0, 41, '{
    "en": "Roll 2.20x50",
    "ar": "Roll 2.20x50"
  }', 100, 0);

delete
from ItemVendorRecord
where id = 1999;

INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,
                              uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (1999, '001999', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 33, 41, 1, 'GDF530-440', 11,
        50, 19);

-- -------------------------------------------------------------------------------------------------

INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (146, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 2000, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (147, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1000, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (148, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1001, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (149, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1002, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (150, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1003, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (151, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1004, 47);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (152, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1005, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (153, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1006, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (154, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1007, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (155, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1008, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (156, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1009, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (157, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1010, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (158, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1011, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (159, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1012, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (160, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1013, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (161, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1014, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (162, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1015, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (163, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1016, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (164, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1017, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (165, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1018, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (166, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1019, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (167, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1020, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (168, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1021, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (169, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1022, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (170, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1023, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (171, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1024, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (172, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1025, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (173, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1026, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (174, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1027, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (175, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1028, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (176, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1029, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (177, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1030, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (178, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1031, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (179, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1032, 45);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (180, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1100, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (181, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1101, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (182, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1102, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (183, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1103, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (184, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1104, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (185, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1105, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (186, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 100, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (187, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 101, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (188, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 102, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (189, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 103, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (190, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 104, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (191, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 105, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (192, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1034, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (193, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1035, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (194, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1042, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (195, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1043, 46);
INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (196, '2020-10-04 10:20:32.520338', '2020-10-04 10:20:32.520338', 'Admin', 'Admin', 1050, 46);

--

INSERT INTO iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                      containerid, quantity)
VALUES (1011, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);

INSERT INTO ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                    collectiondate, numberofdays, deliverydate, incotermid, incotermname,
                                    modeoftransportid, modeoftransportname, shippinginstructionsid,
                                    shippinginstructionsname, loadingportid, loadingportname, dischargeportid,
                                    dischargeportname)
VALUES (1011, 1011, '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1', '2020-10-06', NULL, NULL, 2,
        'CIF', 9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }', 2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }', 2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }', 2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                                         paymenttermid, paymenttermname, currencyId, currencyName)
VALUES (1011, 1011, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 3, '{
  "en": "20% advance, 80% Copy of B/L",
  "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
}', 2, '{
  "en": "United States Dollar",
  "ar": "دولار أمريكي"
}');

INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                 quantityindn, unitOfMeasureId)
VALUES (49, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.695', 'hr1', 'hr1', 33, 66, 23);

INSERT INTO ioborderlinedetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                 quantityindn, unitOfMeasureId)
VALUES (63, 1011, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.695', 'hr1', 'hr1', 44, 66, 16);

INSERT INTO ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                           modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                           discountpercentage)
VALUES (55, 49, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000', 'hr1', 'hr1', 50.0, 43, '{
  "en": "Drum-50Kg",
  "ar": "Drum-50Kg"
}', 4, 0);

INSERT INTO ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                           modificationinfo, quantity, orderunitid, orderunitsymbol, price,
                                           discountpercentage)
VALUES (25, 63, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000', 'hr1', 'hr1', 33, 48, '{
  "en": "Bottle-2Liter",
  "ar": "Bottle-2Liter"
}', 11, 0);
