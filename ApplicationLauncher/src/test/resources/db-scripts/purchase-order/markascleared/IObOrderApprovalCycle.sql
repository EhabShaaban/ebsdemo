DELETE FROM IObOrderApprover;
DELETE FROM IObOrderApprovalCycle;

-- First approval cycle for 2018000011
INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate, 
            creationinfo, modificationinfo, startdate, enddate, finaldecision,approvalcyclenum)
VALUES (20, 1011, '2019-01-07 09:20:00', '2019-01-07 09:20:00', 
        'hr1', 'hr1', '2019-01-07 09:20:00',   '2018-09-21 11:00:00', 'APPROVED',1);
        
INSERT INTO public.ioborderapprover( refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime, decision)
VALUES ( 20, '2019-01-07 09:20:00', '2018-09-21 09:00:00',
        'hr1', 'hr1', 6, 34,'2019-01-07 10:00:00', 'APPROVED');