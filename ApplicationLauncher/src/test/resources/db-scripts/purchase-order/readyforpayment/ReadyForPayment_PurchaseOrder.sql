delete
from dobpurchaseorder
where id = 2001;
delete
from doborderdocument
where id = 2001;

INSERT INTO public.doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                     currentstates, objecttypecode, documentownerid)
VALUES (2001, '2019000001', '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'hr1', 'hr1',
        '["Active",  "InOperation", "Approved",  "NotReceived"]', 'IMPORT_PO', 37);
INSERT INTO public.dobpurchaseorder (id)
VALUES (2001);


delete
from iobenterprisedata
where id = 493;
delete
from iobenterprisedata
where id = 494;
delete
from iobordercompany
where id = 1;

INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (493, 2001, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964', 'hr1', 'hr1', 7, '{
  "en": "AL Madina",
  "ar": "المدينة"
}', NULL, 'Company');
INSERT INTO iobenterprisedata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,
                               enterpriseid, enterprisename, enterpriseaddress, enterprisetype)
VALUES (494, 2001, '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.689', 'hr1', 'hr1', 16, '{
  "ar": "مخزن المدينه الثانوي",
  "en": "Madina Secondary Store"
}', NULL, 'Storehouse');

INSERT INTO public.iobordercompany (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid,
                                    companyid, businessunitid, partyrole)
VALUES (1, '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'hr1', 'hr1', 2001, 7, 19, 'BILL_TO');


delete
from ioborderdeliverydetails
where id = 2001;

INSERT INTO ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, collectiondate,
                                    numberofdays, deliverydate,
                                    incotermid, incotermname,
                                    modeoftransportid, modeoftransportname,
                                    shippinginstructionsid, shippinginstructionsname,
                                    loadingportid, loadingportname,
                                    dischargeportid, dischargeportname)
VALUES (2001, 2001,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        2002, '{
    "en": "Damietta",
    "ar": "دمياط"
  }',
        2001, '{
    "en": "Alexandria old port",
    "ar": "اسكندرية الميناء القديم"
  }');

delete
from ioborderpaymenttermsdetails
where id = 2001;

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (2001, 2001, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

delete
from iobordercontainersdetails
where id = 2001;

INSERT INTO iobordercontainersdetails(id, refinstanceid, creationdate, modifieddate,
                                      creationinfo, modificationinfo, containerid, quantity)
VALUES (2001, 2001,
        '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1', 1034, 10);
-----
delete
from iobordercycledates
where id = 46;

INSERT INTO iobordercycledates(id, refinstanceid, creationdate,
                               modifieddate, creationinfo, modificationinfo, actualpirequesteddate,
                               actualconfirmationdate, actualproductionfinisheddate, actualshippingdate,
                               actualarrivaldate, actualClearanceDate, actualdeliverycompletedate)
VALUES (46, 2001,
        '2018-08-07 09:02:00', '2018-08-07 09:02:00', 'hr1', 'hr1',
        '2018-08-07 09:02:00', null, null, null, null, null, null);
-----
delete
from ioborderapprovalcycle
where id = 14;
delete
from ioborderapprover
where refinstanceid = 14;

INSERT INTO public.ioborderapprovalcycle(id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, startdate, enddate, finaldecision,
                                         approvalcyclenum)
VALUES (14, 2001, '2018-09-21 13:00:00', '2018-09-21 15:00:00',
        'hr1', 'hr1', '2018-09-21 13:00:00', '2018-09-21 15:00:00', 'APPROVED', 1);

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 13:30:00',
        'hr1', 'hr1', 1, 34, '2018-09-21 13:30:00', 'APPROVED');

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 14:00:00',
        'hr1', 'hr1', 2, 44, '2018-09-21 14:00:00', 'APPROVED');

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 15:00:00',
        'hr1', 'hr1', 3, 29, '2018-09-21 15:00:00', 'APPROVED');

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 15:00:00',
        'hr1', 'hr1', 4, 17, '2018-09-21 15:00:00', 'APPROVED');

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 15:00:00',
        'hr1', 'hr1', 5, 20, '2018-09-21 15:00:00', 'APPROVED');

INSERT INTO public.ioborderapprover(refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, controlpointid, approverid, decisiondatetime,
                                    decision)
VALUES (14, '2018-09-21 13:00:00', '2018-09-21 15:00:00',
        'hr1', 'hr1', 6, 17, '2018-09-21 15:00:00', 'APPROVED');
-----
delete
from ioborderlinedetails
where id = 79;

INSERT INTO ioborderlinedetails (id,
                                 refinstanceid, creationdate,
                                 modifieddate, creationinfo,
                                 modificationinfo,
                                 itemid,
                                 quantityindn,
                                 unitOfMeasureId)
VALUES (79,
        2001,
        '2018-08-05 09:06:37.065784',
        '2018-08-06 09:04:17.695',
        'hr1',
        'hr1',
        33,
        NULL,
        23);

delete
from ioborderlinedetailsquantities
where id = 65;
INSERT INTO ioborderlinedetailsquantities (id, refinstanceid, creationdate, modifieddate,
                                           creationinfo, modificationinfo, quantity, orderunitid,
                                           orderunitsymbol, price, discountpercentage)
VALUES (65, 79, '2018-08-05 12:13:08.518660', '2018-08-06 09:46:15.190000',
        'hr1', 'hr1', 40.0, 41, '{
    "en": "Roll 2.20x50",
    "ar": "Roll 2.20x50"
  }', 100, 0);

delete
from ItemVendorRecord
where id = 18;
INSERT INTO ItemVendorRecord (id, code, creationdate, modifieddate, creationinfo, modificationinfo, vendorid, itemid,
                              uomId, currencyid, itemvendorcode, oldItemNumber, price, purchaseunitid)
VALUES (18, '000018', '2018-08-02 10:30:00', '2018-08-02 10:38:58.509', 'hr1', 'hr1', 46, 33, 41, 1, 'GDF530-440', 11,
        50, 19);

---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
delete
from ioborderdeliverydetails
where id = 2002;

INSERT INTO ioborderdeliverydetails(id, refinstanceid, creationdate, modifieddate,
                                    creationinfo, modificationinfo, collectiondate,
                                    numberofdays, deliverydate,
                                    incotermid, incotermname,
                                    modeoftransportid, modeoftransportname,
                                    shippinginstructionsid, shippinginstructionsname,
                                    loadingportid, loadingportname,
                                    dischargeportid, dischargeportname)
VALUES (2002, 1016,
        '2018-08-05 09:10:14.352235', '2018-08-06 09:33:48.124', 'hr1', 'hr1',
        '2020-10-06',
        NULL, NULL,
        2, 'CIF',
        9, '{
    "ar": "نقل بحري",
    "en": "By Sea"
  }',
        2, '{
    "en": "Refregirated",
    "ar": "حاوية مبردة"
  }',
        null, null,
        null, null);

delete
from ioborderpaymenttermsdetails
where id = 2002;

INSERT INTO ioborderpaymenttermsdetails (id, refinstanceid, creationdate, modifieddate,
                                         creationinfo, modificationinfo, paymenttermid, paymenttermname, currencyId,
                                         currencyName)
VALUES (2002, 1016, '2018-08-05 09:23:14.015964', '2018-08-05 09:23:14.015964',
        'hr1', 'hr1', 3,
        '{
          "en": "20% advance, 80% Copy of B/L",
          "ar": "20% فوري و80% مقابل نسخة من بوليصة الشحن"
        }', 2,
        '{
          "en": "United States Dollar",
          "ar": "دولار أمريكي"
        }');

INSERT INTO public.iobpurchaseorderfulfillervendor (id, creationdate, modifieddate, creationinfo, modificationinfo,
                                                    refinstanceid, vendorid)
VALUES (99, '2020-10-04 10:16:38.674437', '2020-10-04 10:16:38.674437', 'Admin', 'Admin', 2001, 46);

