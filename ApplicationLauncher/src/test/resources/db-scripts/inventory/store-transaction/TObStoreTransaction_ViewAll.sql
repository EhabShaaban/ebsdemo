DELETE
from tobgoodsissuestoretransaction;
DELETE
from TObgoodsReceiptStoreTransaction;
DELETE
from tobstoretransaction;
DELETE
from EntityUserCode
WHERE type = 'TObStoreTransaction';
INSERT INTO EntityUserCode (type, code)
values ('TObStoreTransaction', '2019000007');
---------------------------------------------------------------------------------------

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                        unitofmeasureid, quantity, EstimateCost, stocktype, actualCost, batchno,
                                        remainingquantity, reftransactionid, transactionOperation, companyid, plantid,
                                        storehouseid, purchaseunitid, objectTypeCode, originaladdingdate)
VALUES (1, '2019000001', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1', 34, 37, 10.12345678,
        200.123456789, 'DAMAGED_STOCK', 200.12345678, null, 10.123456789, null, 'ADD', 7, 8, 9, 19, 'GR',
        '2019-01-07 09:30:00.000000');
INSERT INTO public.TObgoodsReceiptStoreTransaction (id, documentreferenceid)
VALUES (1, 37);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, unitofmeasureid, quantity, EstimateCost, stocktype, actualCost, batchno,
                                        remainingquantity, reftransactionid,
                                        transactionOperation, companyid, plantid, storehouseid, purchaseunitid,
                                        objectTypeCode, originalAddingDate)
VALUES (7, '2019000007', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1',
        34, 37, 10.123456789, 20.123456789, 'UNRESTRICTED_USE', 20.123456789, null, 10.12345678 , null,
        'ADD', 7, 8, 9, 19, 'GR', '2019-01-07 09:30:00.000000');
INSERT INTO public.TObgoodsReceiptStoreTransaction (id, documentreferenceid)
VALUES (7, 37);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                        unitofmeasureid, quantity, EstimateCost, stocktype, actualCost, batchno,
                                        remainingquantity, reftransactionid, transactionOperation, companyid, plantid,
                                        storehouseid, purchaseunitid, objectTypeCode, originalAddingDate)
VALUES (13, '2019000018', '2019-01-07 09:32:00.000000', '2019-01-07 09:32:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,
        16, 10.12345678, 120.123456789, 'UNRESTRICTED_USE', 120.123456789, null, 3.123456789, null, 'ADD', 10, 12, 14,
        1, 'GR', '2019-01-07 09:32:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,
                                        unitofmeasureid, quantity, EstimateCost, stocktype, actualCost, batchno,
                                        remainingquantity, reftransactionid, transactionOperation, companyid, plantid,
                                        storehouseid, purchaseunitid, objectTypeCode, originalAddingDate)
VALUES (21, '2020000001', '2020-02-05 15:48:00.000000', '2020-02-05 15:48:00.000000', 'hr1', 'hr1', 57, 23, 150.123456789, 0.123456789,
        'UNRESTRICTED_USE', 0.123456789, null, 120.123456789, null, 'ADD', 7, 8, 9, 19, 'GR', '2020-02-05 15:48:00.000000');
INSERT INTO public.tobgoodsreceiptstoretransaction (id, documentreferenceid)
VALUES (21, 42);

-------------------------------GI_ST---------------------------------------------------------
INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, unitofmeasureid, quantity, stocktype, batchno, remainingquantity,
                                        reftransactionid,
                                        transactionOperation, companyid, plantid, storehouseid, purchaseunitid,
                                        objectTypeCode)
VALUES (2, '2019000002', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1',
        57, 23, 30.123456789, 'UNRESTRICTED_USE', null, 0.123456789, 21,
        'TAKE', 7, 8, 16, 19, 'GI');
INSERT INTO public.tobgoodsissuestoretransaction (id, documentreferenceid)
VALUES (2, 3);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                                        itemid, unitofmeasureid, quantity, stocktype, batchno, remainingquantity,
                                        reftransactionid,
                                        transactionOperation, companyid, plantid, storehouseid, purchaseunitid,
                                        objectTypeCode)
VALUES (5, '2019000005', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1',
        64, 16, 5.123456789, 'UNRESTRICTED_USE', null, 0.123456789, 13,
        'TAKE', 10, 12, 14, 1, 'GI');
INSERT INTO public.tobgoodsissuestoretransaction (id, documentreferenceid)
VALUES (5, 1);

