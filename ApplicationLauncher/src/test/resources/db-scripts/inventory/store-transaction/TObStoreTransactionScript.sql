DELETE from tobgoodsissuestoretransaction;
DELETE from TObgoodsReceiptStoreTransaction;
DELETE from TObStockTransformationStoreTransaction;
Delete from tobstoretransaction;

ALTER SEQUENCE tobstoretransaction_id_seq RESTART WITH 51;
DELETE from EntityUserCode WHERE type = 'TObStoreTransaction';
INSERT INTO EntityUserCode (type, code)
values ('TObStoreTransaction', '2020000033');
---------------------------------------------------------------------------------
INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid,transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originaladdingdate)
VALUES (1, '2019000001', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1',34, 37, 10.00, 200, 'DAMAGED_STOCK', 200, null, 10.00, null,'ADD', 7, 8, 9, 19,'GR','2019-01-07 09:30:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (7, '2019000007', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1', 34, 37, 10.5, 20, 'UNRESTRICTED_USE', 20.00, null, 10.50, null, 'ADD', 7, 8, 9, 19,'GR', '2019-01-07 09:30:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objecttypecode,originalAddingDate)
VALUES (8, '2019000008', '2019-12-02 08:09:00.000000', '2019-12-02 08:09:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 44, 16, 1002, 37, 'UNRESTRICTED_USE', 35, null, 1002, null, 'ADD', 7, 8, 16, 1,'GR', '2019-12-02 08:09:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (9, '2019000014', '2019-01-07 09:30:00.000000', '2020-08-06 12:01:00', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 13, 100, 'UNRESTRICTED_USE', 100, null, 10, null, 'ADD', 7, 8, 9, 19,'GR', '2019-01-07 09:30:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (11, '2019000016', '2019-01-07 09:30:00.000000', '2020-08-06 12:01:00', 'fatma', 'Mahmoud.Abdelaziz', 64,46, 2, 100, 'UNRESTRICTED_USE', 100, null, 1, null, 'ADD', 7, 8, 9, 19,'GR', '2019-01-07 09:30:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (12, '2019000017', '2019-01-07 09:31:00.000000', '2019-01-07 09:31:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 8, 120, 'UNRESTRICTED_USE', 120, null, 3, null, 'ADD', 7, 8, 9, 1,'GR', '2019-01-07 09:31:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (13, '2019000018', '2019-01-07 09:32:00.000000', '2019-01-07 09:32:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 10, 120, 'UNRESTRICTED_USE', 120, null, 3, null, 'ADD', 10, 12, 14, 1,'GR', '2019-01-07 09:32:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (14, '2019000019', '2019-01-07 09:33:00.000000', '2019-01-07 09:33:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 8, 120, 'UNRESTRICTED_USE', 120, null, 6, null, 'ADD', 7, 8, 9, 19,'GR', '2019-01-07 09:33:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objecttypecode,originalAddingDate)
VALUES (16, '2019100003', '2019-11-20 13:17:00.000000', '2019-11-20 13:17:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 33, 29, 40, 40, 'DAMAGED_STOCK', 35, null, 40, null, 'ADD', 7, 8, 16, 19,'GR', '2019-11-20 13:17:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objecttypecode,originalAddingDate)
VALUES (19, '2019100006', '2019-11-20 13:21:00.000000', '2019-11-20 13:21:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 58, 23, 150, 37, 'UNRESTRICTED_USE', 35, null, 149, null, 'ADD', 7, 8, 9, 19,'GR', '2019-11-20 13:21:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objecttypecode,originalAddingDate)
VALUES (20, '2019100007', '2019-11-20 13:12:00.000000', '2019-11-20 13:12:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 40, 1, 136, 37, 'UNRESTRICTED_USE', 35, null, 0, null, 'ADD', 7, 8, 9, 19,'GR', '2019-11-20 13:12:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate)
VALUES (21, '2020000001', '2020-02-05 15:48:00.000000', '2020-08-05 13:45:35.500000', 'hr1', 'hr1', 57, 23, 150, 0, 'UNRESTRICTED_USE', 0, null, 100, null, 'ADD', 7, 8, 9, 19, 'GR', '2020-02-05 15:48:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (22, '2020000002', '2020-02-05 15:48:00.000000', '2020-02-05 15:48:00.000000', 'hr1', 'hr1', 57, 41, 100, 20, 'UNRESTRICTED_USE', 20, null, 0, null, 'ADD', 7, 8, 9, 19,'GR', '2020-02-05 15:48:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (23, '2020000003', '2020-02-05 15:49:00.000000', '2020-02-05 15:49:00.000000', 'hr1', 'hr1', 57, 23, 100, 0, 'UNRESTRICTED_USE', 0, null, 100, null, 'ADD', 7, 8, 16, 19,'GR', '2020-02-05 15:49:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (24, '2020000005', '2020-02-05 15:49:00.000000', '2020-02-05 15:49:00.000000', 'hr1', 'hr1', 57, 23, 10, 0, 'UNRESTRICTED_USE', 0, null, 10, null, 'ADD', 7, 8, 9, 19,'GR', '2020-02-05 15:49:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (25, '2020000004', '2020-02-05 15:49:00.000000', '2020-02-05 15:49:00.000000', 'hr1', 'hr1', 57, 41, 20, 0, 'UNRESTRICTED_USE', 0, null, 19, null, 'ADD', 7, 8, 9, 19,'GR', '2020-02-05 15:49:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (26, '2020000006', '2020-02-05 15:50:00.000000', '2020-02-05 15:50:00.000000', 'hr1', 'hr1', 57, 23, 15, 0, 'DAMAGED_STOCK', 0, null, 13, null, 'ADD', 10, 12, 14, 19,'GR', '2020-02-05 15:50:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode)
VALUES (27, '2020000011', '2020-02-07 09:31:00.000000', '2020-02-07 09:31:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 5, null, 'UNRESTRICTED_USE', null, null, 0, 20, 'TAKE', 7, 8, 9, 1,'STK_TR');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid,unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid,storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (28, '2020000012', '2020-02-07 09:31:00.000000', '2020-02-07 09:31:00.000000', 'fatma', 'Mahmoud.Abdelaziz', 64,16, 5, 120, 'DAMAGED_STOCK', 120, null, 5, 20, 'ADD', 7, 8, 9, 1,'STK_TR', '2020-02-07 09:31:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode)
VALUES (29, '2020000013', '2020-02-07 15:50:00.000000', '2020-02-07 15:50:00.000000', 'hr1', 'hr1', 57, 23, 2, null, 'DAMAGED_STOCK', null, null, 0, 26, 'TAKE', 10, 12, 14, 19,'STK_TR');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originalAddingDate)
VALUES (30, '2020000014', '2020-02-07 15:50:00.000000', '2020-02-07 15:50:00.000000', 'hr1', 'hr1', 57, 23, 2, 0, 'UNRESTRICTED_USE', 0, null, 2, 26, 'ADD', 10, 12, 14, 19,'STK_TR', '2020-02-07 15:50:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno,remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originaladdingdate)
VALUES (33, '2020000017', '2020-11-03 07:41:00.000000', '2020-11-03 07:41:00.000000', 'hr1', 'hr1', 64, 16, 2, null, 'UNRESTRICTED_USE', null, null, 0, 14, 'TAKE', 7, 8, 9, 19,'STK_TR', null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originaladdingdate)
VALUES (34, '2020000018', '2020-11-03 07:41:00.000000', '2020-11-03 07:41:00.000000', 'hr1', 'hr1', 64, 16, 2, 120, 'DAMAGED_STOCK', 120, null, 2, null, 'ADD', 7, 8, 9, 19,'STK_TR', '2020-11-03 07:41:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid,transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originaladdingdate)
VALUES (35, '2020000019', '2020-12-07 09:30:00.000000', '2020-12-07 09:30:00.000000', 'hr1', 'hr1',64, 16, 10.00, 120, 'UNRESTRICTED_USE', 120.00, null, 10.00, null,'ADD', 7, 8, 9, 19,'GR','2020-12-07 09:30:00.000000');

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo,itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid,transactionOperation, companyid, plantid, storehouseid, purchaseunitid,objectTypeCode,originaladdingdate)
VALUES (36, '2020000020', '2020-12-07 09:30:00.000000', '2020-12-07 09:30:00.000000', 'hr1', 'hr1',64, 16, 10.00, 120, 'UNRESTRICTED_USE', 120.00, null, 10.00, null,'ADD', 10, 12, 14, 19,'GR','2020-12-07 09:30:00.000000');

-------------------------------------------------GI_ST-----------------------------------------------

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (2, '2019000002', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1', 57, 23, 30, null, 'UNRESTRICTED_USE', null, null, 0, 21, 'TAKE', 7, 8, 16, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (5, '2019000005', '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000', 'hr1', 'hr1', 64, 16, 5, null, 'UNRESTRICTED_USE', null, null, 0, 13, 'TAKE', 10, 12, 14, 1, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (15, '2019100008', '2019-11-20 13:13:00.000000', '2019-11-20 13:13:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 40, 1, 136, null, 'UNRESTRICTED_USE', null, null, 0, 20, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (37, '2020000021', '2020-12-08 09:30:00.000000', '2020-12-08 09:30:00.000000', 'Admin', 'Admin', 64, 16, 2, null, 'UNRESTRICTED_USE', null, null, 0, 13, 'TAKE', 10, 12, 14, 1, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (39, '2020000022', '2020-06-29 11:08:00.000000', '2020-06-29 11:08:00.000000', 'hr1', 'hr1', 57, 41, 2, null, 'UNRESTRICTED_USE', null, null, 0, 22, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (40, '2020000023', '2020-07-27 07:52:00.000000', '2020-07-27 07:52:00.000000', 'hr1', 'hr1', 57, 41, 98, null, 'UNRESTRICTED_USE', null, null, 0, 22, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (41, '2020000024', '2020-07-27 07:52:00.000000', '2020-07-27 07:52:00.000000', 'hr1', 'hr1', 57, 41, 1, null, 'UNRESTRICTED_USE', null, null, 0, 25, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (42, '2020000025', '2020-07-27 05:52:00.000000', '2020-07-27 05:52:32.898000', 'hr1', 'hr1', 64, 16, 2, null, 'UNRESTRICTED_USE', null, null, 0, 9, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (44, '2020000027', '2020-08-05 13:44:00.000000', '2020-08-05 13:44:49.581000', 'hr1', 'hr1', 57, 23, 10, null, 'UNRESTRICTED_USE', null, null, 0, 21, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (45, '2020000028', '2020-08-05 13:45:00.000000', '2020-08-05 13:45:35.471000', 'hr1', 'hr1', 57, 23, 10, null, 'UNRESTRICTED_USE', null, null, 0, 21, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (46, '2020000029', '2020-08-06 12:01:00.000000', '2020-08-06 12:01:00.000000', 'hr1', 'hr1', 64, 16, 1, null, 'UNRESTRICTED_USE', null, null, 0, 9, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (47, '2020000030', '2020-08-06 12:01:00.000000', '2020-08-06 12:01:00.000000', 'hr1', 'hr1', 64, 46, 1, null, 'UNRESTRICTED_USE', null, null, 0, 11, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (48, '2020000031', '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 58, 23, 1, null, 'UNRESTRICTED_USE', null, null, 0, 19, 'TAKE', 7, 8, 9, 19, 'GI', null, null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (49, '2020000032', '2020-12-21 15:34:00', '2020-12-21 15:34:00', 'hr1', 'hr1', 34, 37, 0.5, 0, 'DAMAGED_STOCK', null, null, 0.5, null, 'ADD', 7, 8, 9, 19, 'GR', '2020-12-21 15:34:00', null);

INSERT INTO public.tobstoretransaction (id, code, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, estimatecost, stocktype, actualcost, batchno, remainingquantity, reftransactionid, transactionoperation, companyid, plantid, storehouseid, purchaseunitid, objecttypecode, originaladdingdate, updatingactualcostdate)
VALUES (50, '2020000033', '2020-12-21 15:34:00', '2020-12-21 15:34:00', 'hr1', 'hr1', 34, 37, 0.5, 9.98, 'UNRESTRICTED_USE', null, null, 0.5, null, 'ADD', 7, 8, 9, 19, 'GR', '2020-12-21 15:34:00', null);
