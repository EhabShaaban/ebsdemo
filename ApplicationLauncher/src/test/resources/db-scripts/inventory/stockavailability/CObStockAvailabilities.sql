truncate cobstockavailability;

insert into cobstockavailability (code, name, creationdate, modifieddate, creationinfo, modificationinfo, companyid,
                                  plantid, storehouseid, businessunitid, itemid, unitofmeasureid, stocktype,
                                  availablequantity)
select concat(
               grouped.stocktype,
               grouped.purchaseUnitCode,
               grouped.companyCode,
               grouped.plantCode,
               grouped.storehouseCode,
               grouped.itemCode,
               grouped.unitOfMeasureCode) as code,
       '{"ar":"", "en":""}',
       NOW(),
       NOW(),
       'hr1',
       'hr1',
       c1.id                              as companyId,
       c2.id                              as plantId,
       c3.id                              as storehouseId,
       c4.id                              as businessUnitId,
       m1.id                              as itemId,
       cm1.id                             as unitOfMeasureId,
       grouped.stocktype,
       grouped.totalQty
from (select c1.code                                    as companyCode,
             c2.code                                    as plantCode,
             c3.code                                    as storehouseCode,
             c4.code                                    as purchaseUnitCode,
             TObStoreTransaction.stocktype,
             m1.code                                    as itemCode,
             cm1.code                                   as unitOfMeasureCode,
             sum(TObStoreTransaction.remainingquantity) as totalQty
      from TObStoreTransaction
               join cobenterprise c1 on TObStoreTransaction.companyId = c1.id and c1.objecttypecode = '1'
               left join cobenterprise c2 on TObStoreTransaction.plantid = c2.id and c2.objecttypecode = '3'
               left join cobenterprise c3 on TObStoreTransaction.storehouseid = c3.id and c3.objecttypecode = '4'
               left join cobenterprise c4 on TObStoreTransaction.purchaseunitid = c4.id and c4.objecttypecode = '5'
               left join masterdata m1 on TObStoreTransaction.itemid = m1.id and m1.objecttypecode = '1'
               left join cobmeasure cm1 on TObStoreTransaction.unitofmeasureid = cm1.id
      group by itemcode,
               unitofmeasurecode,
               purchaseunitcode,
               storehousecode, companycode, plantcode, stocktype) grouped
         left join cobenterprise c1 on c1.code = grouped.companyCode and c1.objecttypecode = '1'
         left join cobenterprise c2 on c2.code = grouped.plantCode and c2.objecttypecode = '3'
         left join cobenterprise c3 on c3.code = grouped.storehouseCode and c3.objecttypecode = '4'
         left join cobenterprise c4 on c4.code = grouped.purchaseUnitCode and c4.objecttypecode = '5'
         left join masterdata m1 on grouped.itemCode = m1.code and m1.objecttypecode = '1'
         left join cobmeasure cm1 on grouped.unitOfMeasureCode = cm1.code;
