DELETE from tobgoodsreceiptstoretransaction;
DELETE from dobcosting;
DELETE from dobinventorydocument where inventorydocumenttype in('GR_PO','GR_SR');
DELETE from IObInventoryCompany where refinstanceid in (select id from dobinventorydocument  where inventorydocumenttype in('GR_PO','GR_SR')) ;
DELETE from IObInventoryActivationDetails where refinstanceid in (select id from dobinventorydocument  where inventorydocumenttype in('GR_PO','GR_SR')) ;
DELETE from dobgoodsreceiptpurchaseorder;
DELETE from dobgoodsreceiptsalesreturn;
DELETE from iobgoodsreceiptsalesreturndata;
DELETE from EntityUserCode WHERE type = 'DObGoodsReceipt';
INSERT INTO EntityUserCode (type, code) values ('DObGoodsReceipt', '2020000022');
-----------------------------------------------------------
INSERT INTO dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (10,2018000000,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (10,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (11,2018000002, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (11,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (12,2018000003, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (12,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (13,2018000004, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Active"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (13,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (15,2018000999, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (15,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (16,2018000008, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (16,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (17,2018000009, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (17,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (19,2018000011, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (19,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (20,2018000012, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (20,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (21,2018000001, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (21,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (22,2018000015, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (22,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (23,2018000016, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (23,1);
----------------------------------------------------------------------------------

INSERT INTO iobgoodsreceiptpurchaseorderdata (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,purchaseorderid,vendorid,vendorname,deliverynote,purchaseresponsibleid,purchaseresponsiblename)
VALUES (10,10,'2019-01-01 11:00:00','2019-01-01 11:00:00','Ahmed.Ali','Ahmed.Ali',1001,46,
        '{"ar":"زاجاننج","en":"Zhejiang"}','123456',37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (11,11, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed Gamal', 'Ahmed Gamal', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456',  37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (12,12, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (13,13, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (15,15, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 21, 'Marwa Tawfeek');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (16,16, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456',37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
    id,refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
    deliverynote,  purchaseresponsibleid,
    purchaseresponsiblename)
VALUES (17,17, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
    id,refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
    deliverynote,  purchaseresponsibleid,
    purchaseresponsiblename)
VALUES (19,19, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1044, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', NULL,  37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
    id,refinstanceid, creationdate, modifieddate,
    creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
    deliverynote,  purchaseresponsibleid,
    purchaseresponsiblename)
VALUES (20,20, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1045, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (21,21, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456',  37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (22,22, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(
            id,refinstanceid, creationdate, modifieddate,
            creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
            deliverynote,  purchaseresponsibleid,
            purchaseresponsiblename)
   VALUES (23,23, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
   'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37, 'Gehan Ahmed');
--------------------------------------------------------------------------------------------------------------
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (11, 10, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (12, 11, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (13, 12, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (14, 13, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (16, 15, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (17, 16, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (18, 17, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (20, 19, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (21, 20, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 62);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (22, 21, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 16, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (23, 22, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (24, 23, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 7, 8, 9, 39);

----------------------------------------------------------------------------------------------------------------------

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (11,11,'2018-01-06 13:00:00','2018-01-06 13:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 13:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (13,13,'2018-01-06 13:00:00','2019-12-02 08:09:00.000000','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2019-12-02 08:09:00.000000');
