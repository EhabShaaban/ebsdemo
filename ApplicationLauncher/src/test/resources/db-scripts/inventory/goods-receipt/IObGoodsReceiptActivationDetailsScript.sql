DELETE from IObInventoryActivationDetails where refinstanceid in (select id from dobinventorydocument  where inventorydocumenttype in ('GR_PO','GR_SR')) ;
ALTER SEQUENCE IObInventoryActivationDetails_id_seq RESTART WITH 77;
-------------------------------------------------------------------------

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (11,11,'2018-01-06 13:00:00','2018-01-06 13:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 13:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (13,13,'2018-01-06 13:00:00','2019-12-02 08:09:00.000000','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2019-12-02 08:09:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (24,24,'2018-01-06 13:00:00','2018-01-06 13:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 13:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (26,26,'2018-01-06 11:00:00','2018-01-06 11:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 11:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (27,27,'2018-01-06 11:00:00','2018-01-06 11:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 11:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (28,28,'2018-01-06 11:00:00','2018-01-06 11:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 11:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (29,29,'2018-01-06 11:00:00','2018-01-06 11:00:00','Ahmed.Ali','Ahmed.Ali','Ahmed.Ali','2018-01-06 11:00:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (37,37, '2019-01-07 09:30:00.000000', '2019-01-07 09:30:00.000000','hr1','hr1','hr1','2019-01-07 09:30:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (38,38,'2019-01-01 13:37:29.244000', '2019-01-01 13:37:29.244000','hr1','hr1','hr1','2019-01-07 09:30:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (39,39,'2019-01-01 13:37:29.244000', '2020-12-07 09:30:00.000000','hr1','hr1','hr1','2020-12-07 09:30:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (40,40,'2019-01-01 13:37:29.244000', '2020-12-07 09:30:00.000000','hr1','hr1','hr1','2020-12-07 09:30:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (41,41,'2019-01-01 13:37:29.244000', '2019-01-07 09:33:00','hr1','hr1','hr1','2019-01-07 09:33:00');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (42,42, '2020-02-04 13:17:01.000000', '2020-02-04 13:17:01.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','Admin from BDKCompanyCode', '2020-02-04 13:17:01.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (43,43, '2020-02-04 13:22:34.000000', '2020-02-04 13:22:34.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','Admin from BDKCompanyCode' , '2020-02-04 13:22:34.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (44,44, '2020-02-04 13:24:26.000000', '2020-02-04 13:24:26.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','Admin from BDKCompanyCode' , '2020-02-04 13:24:26.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (45,45, '2020-02-04 13:28:43.000000', '2020-02-04 13:28:43.000000','Admin from BDKCompanyCode','Admin from BDKCompanyCode','Admin from BDKCompanyCode' , '2020-02-04 13:28:43.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (46,46, '2020-03-05 09:00:00.000000', '2020-03-05 09:00:00.000000','hr1','hr1','hr1' , '2020-03-05 09:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (47,47, '2020-03-05 09:00:00.000000', '2020-03-05 09:00:00.000000','hr1','hr1','hr1' , '2020-03-05 09:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (48,48, '2020-03-05 11:00:00.000000', '2020-03-05 11:00:00.000000','hr1','hr1','hr1', '2020-03-05 11:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (49,49, '2020-03-23 10:00:00.000000', '2020-03-23 10:00:00.000000','hr1','hr1','hr1', '2020-03-23 10:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (50,50, '2020-03-29 09:00:00.000000', '2020-03-29 09:00:00.000000','hr1','hr1','hr1', '2020-03-29 09:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (51,51, '2020-03-29 09:00:00.000000', '2020-03-29 09:00:00.000000','hr1','hr1','hr1', '2020-03-29 09:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (65,65, '2020-03-29 09:00:00.000000', '2020-03-29 09:00:00.000000','hr1','hr1','hr1', '2020-03-29 09:00:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (69,69, '2020-07-15 15:58:48.384000', '2020-07-15 15:58:48.384000','hr1','hr1','hr1', '2020-07-15 15:58:48.384000');

INSERT INTO iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (70, 90, '2020-12-21 17:33:40.601000', '2020-12-21 17:33:40.614000', 'hr1', 'hr1', 'hr1', '2020-12-21 17:33:40.601000');