DELETE
FROM iobgoodsreceiptpurchaseorderdata;
ALTER SEQUENCE iobgoodsreceiptpurchaseorderdata_id_seq RESTART WITH 57;
-------------------------------------------------------------------------

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (17, 17, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11,
        'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (19, 19, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1044, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', NULL, 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (20, 20, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1045, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 11,
        'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                              modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                              purchaseresponsibleid, purchaseresponsiblename)
VALUES (10, 10, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed.Ali', 'Ahmed.Ali', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (11, 11, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (12, 12, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11,
        'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (13, 13, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11,
        'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (14, 14, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed.Ali', 'Ahmed.Ali', 1001, 46,
        '{  "ar": "زاجاننج",  "en": "Zhejiang"}', '123456', 37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (15, 15, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed Gamal', 'Ahmed Gamal', 1003, 45, '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 21,
        'Marwa Tawfeek');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (16, 16, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (21, 21, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (22, 22, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (23, 23, '2019-01-01 11:00:00', '2019-01-01 11:00:00',
        'Ahmed.Ali', 'Ahmed.Ali', 1001, 46, '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37,
        'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (24, 24, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'hr1', 'hr1', 1018, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (25, 25,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1047, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (26, 26,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1045, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 11, 'Amr Khalil');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (27, 27,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1023, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (28, 28,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1046, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (29, 29,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1050, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid,
                                                     creationdate, modifieddate, creationinfo, modificationinfo,
                                                     purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (30, 30,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1051, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        NULL, 37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (31, 31, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1003, 45,
        '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (32, 32, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1057, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (33, 33,
        '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed.Ali', 'Ahmed.Ali', 1047, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}',
        '123456', 37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (34, 34, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed.Ali', 'Ahmed.Ali', 1049, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', '123456', 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (35, 35, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (36, 36, '2019-11-20 15:38:26.731000', '2019-11-20 15:38:26.731000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (37, 37, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{  "ar": "زاجاننج",  "en": "Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (38, 38, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (39, 39, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (40, 40, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (41, 41, '2019-11-20 15:34:09.085000', '2019-11-20 15:34:09.085000', 'hr1', 'hr1', 1001, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (42, 42, '2020-02-04 12:32:21.000000', '2020-02-04 12:32:23.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1001, 46, '{"ar": "زاجاننج", "en": "Zhejiang"}', null, 37,
        'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (43, 43, '2020-02-04 12:55:24.000000', '2020-02-04 12:55:25.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1002, 46, '{"ar": "زاجاننج", "en": "Zhejiang"}', null, 37,
        'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (44, 44, '2020-02-04 13:04:02.000000', '2020-02-04 13:04:03.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1001, 46, '{"ar": "زاجاننج", "en": "Zhejiang"}', null, 37,
        'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (45, 45, '2020-02-04 13:07:59.000000', '2020-02-04 13:08:00.000000', 'Admin from BDKCompanyCode',
        'Admin from BDKCompanyCode', 1003, 46, '{"ar": "زاجاننج", "en": "Zhejiang"}', null, 37,
        'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (46, 46, '2020-03-05 10:36:10.466000', '2020-03-05 10:36:10.466000', 'hr1', 'hr1', 1011, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (47, 47, '2020-03-05 10:43:37.289000', '2020-03-05 10:43:37.289000', 'hr1', 'hr1', 1012, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (48, 48, '2020-03-05 12:08:03.008000', '2020-03-05 12:08:03.008000', 'hr1', 'hr1', 1013, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (49, 49, '2020-03-23 11:25:10.010000', '2020-03-23 11:25:10.010000', 'hr1', 'hr1', 1019, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (50, 50, '2020-03-29 08:41:42.484000', '2020-03-29 08:41:42.484000', 'hr1', 'hr1', 1020, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO public.iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                                     modificationinfo, purchaseorderid, vendorid, vendorname,
                                                     deliverynote, purchaseresponsibleid, purchaseresponsiblename)
VALUES (51, 51, '2020-03-29 08:56:13.088000', '2020-03-29 08:56:13.088000', 'hr1', 'hr1', 1021, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (52, 52, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1003, 45,
        '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate, creationinfo,
                                             modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                             purchaseresponsibleid, purchaseresponsiblename)
VALUES (53, 53, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'Ahmed Gamal', 'Ahmed Gamal', 1003, 45,
        '{"ar":"سيجورك","en":"Siegwerk"}', NULL, 11, 'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata(id, refinstanceid, creationdate, modifieddate,
                                             creationinfo, modificationinfo, purchaseorderid, vendorid, vendorname,
                                             deliverynote, purchaseresponsibleid,
                                             purchaseresponsiblename)
VALUES (54, 89, '2020-09-27 14:56:00.000000', '2020-09-27 14:58:00.000000',
        'Ahmed Gamal', 'Ahmed Gamal', 1008, 46, '{"en":"Zhejiang","ar":"زاجاننج"}', NULL, 11,
        'Amr Khalil');

INSERT INTO iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                              modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                              purchaseresponsibleid, purchaseresponsiblename, comments)
VALUES (55, 90, '2020-12-21 17:33:09.179000', '2020-12-21 17:33:09.179000', 'hr1', 'hr1', 2091, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed', null);

INSERT INTO iobgoodsreceiptpurchaseorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo,
                                              modificationinfo, purchaseorderid, vendorid, vendorname, deliverynote,
                                              purchaseresponsibleid, purchaseresponsiblename, comments)
VALUES (56, 91,'2020-12-22 09:30:00', '2020-12-22 09:30:00', 'A.Hamed', 'A.Hamed', 2091, 46,
        '{"ar":"زاجاننج","en":"Zhejiang"}', null, 37, 'Gehan Ahmed', null);