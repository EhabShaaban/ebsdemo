ALTER SEQUENCE dobgoodsreceiptpurchaseorder_id_seq RESTART WITH 92;
ALTER SEQUENCE dobgoodsreceiptsalesreturn_id_seq RESTART WITH 88;
ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 94;
DELETE from tobgoodsreceiptstoretransaction;
delete from dobinventorydocument where inventorydocumenttype in ('GR_PO', 'GR_SR') ;
DELETE from dobgoodsreceiptpurchaseorder;
DELETE from dobgoodsreceiptsalesreturn;
DELETE from EntityUserCode WHERE type = 'DObGoodsReceipt';
INSERT INTO EntityUserCode (type, code) values ('DObGoodsReceipt', '2020000032');
--------------------------------------------------------------------------------

INSERT INTO dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (10,2018000000,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (10,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (11,2018000002, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (11,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (12,2018000003, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (12,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (13,2018000004, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Active"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (13,1);

INSERT INTO dobinventorydocument(id, code, creationdate, modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (14, 2018100016, '2018-01-05 11:00:00', '2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (14,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (15,2018000999, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (15,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (16,2018000008, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (16,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (17,2018000009, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (17,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (19,2018000011, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (19,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
    VALUES (20,2018000012, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (20,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (21,2018000001, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (21,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (22,2018000015, '2018-01-05 11:00:00', '2018-01-05 11:00:00',  'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (22,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (23,2018000016, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (23,1);


INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (24,2018000013,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (24,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (25,2018000014,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (25,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (26,2018000017,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Active"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (26,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (27,2018000018,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (27,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
 VALUES (28,2018000019,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (28,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (29,2018000020,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (29,1);

INSERT INTO public.dobinventorydocument (id,code,creationdate,modifieddate,creationinfo,modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (30,2018000021,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (30,1);

INSERT INTO public.dobinventorydocument(
            id,code, creationdate, modifieddate, creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (31, 2018000100 , '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
           'Ahmed.Ali', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (31,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (32,2018000024, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali',
     'Ahmed.Ali', '["Draft"]',23,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (32,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (33,2018000025, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (33,1);

INSERT INTO dobinventorydocument(
            id, code, creationdate, modifieddate,  creationinfo,
            modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (34,2018000026, '2018-01-05 11:00:00', '2018-01-05 11:00:00', 'Ahmed.Ali','Ahmed.Ali','["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (34,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (35, '2019000001', '2019-11-20 13:34:00.000000', '2019-11-20 13:37:29.244000', 'hr1', 'hr1', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (35,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (36, '2019000002', '2019-11-20 13:38:00.000000', '2019-11-20 13:47:36.545000', 'hr1', 'hr1', '["Draft"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (36,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (37, '2019000088', '2019-01-07 09:30:00.000000', '2019-01-07 09:35:00.000000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (37,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (38, '2019000020', '2019-01-01 13:34:09.056000', '2019-01-01 13:37:29.244000',  'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (38,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (39, '2019000021', '2019-01-01 13:34:09.056000', '2019-01-01 13:37:29.244000',  'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (39,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (40, '2019000022', '2019-01-01 13:34:09.056000', '2019-01-01 13:37:29.244000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (40,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (41, '2019000023', '2019-01-01 13:34:09.056000', '2019-01-01 13:37:29.244000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (41,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (42, '2020000001', '2020-02-04 11:16:58.000000', '2020-02-04 11:17:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (42,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (43, '2020000002', '2020-02-04 11:22:32.000000', '2020-02-04 11:22:33.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (43,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (44, '2020000003', '2020-02-04 11:24:24.000000', '2020-02-04 11:24:25.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (44,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (45, '2020000004', '2020-02-04 11:28:39.000000', '2020-02-04 11:28:41.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (45,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (46, '2020000005', '2020-03-05 08:36:10.430000', '2020-03-05 08:36:42.007000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (46,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (47, '2020000006', '2020-03-05 08:43:37.279000', '2020-03-05 08:45:50.764000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (47,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (48, '2020000007', '2020-03-05 10:08:03.000000', '2020-03-05 10:10:11.253000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (48,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (49, '2020000013', '2020-03-23 09:25:09.987000', '2020-03-23 09:33:16.696000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (49,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (50, '2020000014', '2020-03-29 08:41:42.468000', '2020-03-29 08:44:33.932000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (50,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (51, '2020000015', '2020-03-29 08:56:13.083000', '2020-03-29 08:59:42.393000', 'hr1', 'hr1', '["Active"]',19,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (51,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (52, '2020000018', '2020-03-29 08:56:13.083000', '2020-03-29 08:59:42.393000',   'hr1', 'hr1', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (52,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (53, '2020000019', '2020-03-29 08:56:00.000000', '2020-03-29 08:59:00.000000', 'hr1', 'hr1', '["Draft"]',1,'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeId)
VALUES (53,1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (89, '2020000030', '2020-09-27 14:56:00.000000', '2020-09-27 14:58:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeid)
VALUES (89, 1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (90, '2020000031', '2020-12-21 15:33:09.162000', '2020-12-21 15:33:40.605000', 'hr1', 'hr1', '["Active"]', 19, 'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeid)
VALUES (90, 1);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (91, '2020000032', '2020-12-22 09:30:00', '2020-12-22 09:30:00', 'A.Hamed', 'A.Hamed', '["Draft"]', 19, 'GR_PO');
INSERT INTO public.dobgoodsreceiptpurchaseorder (id, typeid)
VALUES (91, 1);
-------------------------------------------------------------------GR_SR--------------------------------------------------------------------------------------
INSERT INTO dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (64, '2020000020', '2020-03-29 08:56:00.000000', '2020-03-29 08:59:00.000000', 'Admin', 'Admin', '["Draft"]',1,'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeId)
VALUES (64,2);

INSERT INTO dobinventorydocument (id, code, creationdate, modifieddate,  creationinfo, modificationinfo,currentstates,purchaseunitid,inventorydocumenttype)
VALUES (65, '2020000021', '2020-03-29 08:56:00.000000', '2020-03-29 08:59:00.000000', 'Admin', 'Admin', '["Active"]',1,'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeId)
VALUES (65,2);

INSERT INTO dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (69, '2020000022', '2020-07-15 13:58:00.000000', '2020-07-15 13:58:00.000000', 'hr1', 'hr1', '["Active"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeId)
VALUES (69,2);

INSERT INTO dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (70, '2020000023', '2020-07-16 09:30:00', '2020-07-16 09:30:00', 'A.Hamed', 'A.Hamed', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeId)
VALUES (70,2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (74, '2020000024', '2020-07-29 09:49:00.000000', '2020-07-29 09:49:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (74, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (76, '2020000025', '2020-07-29 10:13:00.000000', '2020-07-29 10:13:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (76, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (81, '2020000026', '2020-08-07 10:00:00.000000', '2020-08-07 10:00:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (81, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (83, '2020000027', '2020-08-18 14:47:00.000000', '2020-08-18 14:47:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (83, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (84, '2020000028', '2020-08-18 14:56:00.000000', '2020-08-18 14:58:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (84, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (87, '2020000029', '2020-08-27 14:56:00.000000', '2020-08-27 14:58:00.000000', 'hr1', 'hr1', '["Draft"]', 19, 'GR_SR');
INSERT INTO public.dobgoodsreceiptsalesreturn (id, typeid)
VALUES (87, 2);
