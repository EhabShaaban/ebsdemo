DELETE from tobgoodsreceiptstoretransaction;
DELETE from dobinventorydocument where inventorydocumenttype ='GR_PO';
DELETE from dobgoodsreceiptpurchaseorder;
DELETE from dobgoodsreceiptsalesreturn;
delete from CObGoodsReceipt;

INSERT INTO CObGoodsReceipt (id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (1, 'PURCHASE_ORDER', '{"ar": "إذن إضافة من أمر شراء", "en": "Goods Receipt Based on Purchase Order"}', '2020-10-28 11:12:18.000000', '2020-10-28 11:12:20.000000', 'Admin', 'Admin', '["Draft"]');

INSERT INTO CObGoodsReceipt (id, code, name, creationdate, modifieddate, creationinfo, modificationinfo, currentstates)
VALUES (2, 'SALES_RETURN', '{"ar": "إذن إضافة من طلب إرتجاع", "en": "Goods Receipt Based on Sales Return"}', '2020-10-28 11:12:18.000000', '2020-10-28 11:12:20.000000', 'Admin', 'Admin', '["Draft"]');

