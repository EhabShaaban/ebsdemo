ALTER SEQUENCE iobgoodsreceiptpurchaseorderreceiveditemsdata_id_seq RESTART WITH 156;
ALTER SEQUENCE iobgoodsreceiptpurchaseorderitemquantities_id_seq RESTART WITH 156;
ALTER SEQUENCE iobgoodsreceiptsalesreturnreceiveditemsdata_id_seq RESTART WITH 152;
ALTER SEQUENCE iobgoodsreceiptsalesreturnitemquantities_id_seq RESTART WITH 152;
ALTER SEQUENCE iobgoodsreceiptitembatches_id_seq RESTART WITH 5;

DELETE FROM iobgoodsreceiptitembatches;
DELETE FROM IObGoodsReceiptPurchaseOrderItemQuantities;
DELETE FROM IObGoodsReceiptPurchaseOrderReceivedItemsData;
DELETE FROM IObGoodsReceiptSalesReturnItemQuantities;
DELETE FROM IObGoodsReceiptSalesReturnReceivedItemsData;
-------------------------------------------------------------------------------------
INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (1,16,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,'Overdelivery to opltimize container capacity',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (2,12,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',37,null,1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (3,20,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',44,'يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (4,11,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,'Overdelivery to opltimize container capacity',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (5,14,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',34,null,1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (6,17,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',37,'يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (7,19,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',44,'يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (8,31,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',35,'Crashed while discharge',2);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (9,31,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,'Overdelivery to opltimize container capacity',1);

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (10, 35, '2019-11-20 15:34:09.109000', '2019-11-20 15:36:36.344000', 'hr1', 'hr1', 33,  null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,  differencereason, objecttypecode)
VALUES (11, 35, '2019-11-20 15:34:09.120000', '2019-11-20 15:37:29.237000', 'hr1', 'hr1', 34,  null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (12,16,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',35,'Samples taken by clearance authorities',2);

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,  differencereason, objecttypecode)
VALUES (15, 36, '2019-11-20 15:38:26.750000', '2019-11-20 15:38:26.750000', 'hr1', 'hr1', 35, null, '2');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (16,32,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',44,'يوجع عبوة ناقصة لم يتم استلامها وذلك لاخذها كعينة للتحليل في الجمارك',1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (17,33,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,null,1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (19,34,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,null,1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (21, 29,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',34,null,1);

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (22,13,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 44, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (24,24,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 44, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (25,26,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 44, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (26,27,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 44, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (27,28,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 44, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid,differencereason, objecttypecode)
VALUES (40, 38, '2019-11-20 15:34:09.109000', '2019-11-20 15:36:36.344000', 'hr1', 'hr1', 64,  null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (41, 39, '2019-11-20 15:34:09.109000', '2019-11-20 15:36:36.344000', 'hr1', 'hr1', 64, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (42, 40, '2019-11-20 15:34:09.109000', '2019-11-20 15:36:36.344000', 'hr1', 'hr1', 64,  null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (43, 41, '2019-11-20 15:34:09.109000', '2019-11-20 15:36:36.344000', 'hr1', 'hr1', 64,  null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo,modificationinfo, itemid,  differencereason,objecttypecode)
VALUES (110, 37, '2019-11-20 15:34:09.120000', '2019-11-20 15:37:29.237000', 'hr1', 'hr1', 34,null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (111, 42, '2020-02-04 14:08:19.000000', '2020-02-04 14:08:20.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (112, 43, '2020-02-04 14:40:37.000000', '2020-02-04 14:40:39.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (113, 44, '2020-02-04 14:41:34.000000', '2020-02-04 14:41:35.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (114, 45, '2020-02-04 14:41:51.000000', '2020-02-04 14:41:52.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 57, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (126, 46, '2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 33, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (127, 47, '2020-03-05 10:43:37.304000', '2020-03-05 10:43:48.885000', 'hr1', 'hr1', 33, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (128, 48, '2020-03-05 12:08:03.023000', '2020-03-05 12:10:04.773000', 'hr1', 'hr1', 33, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (129, 48, '2020-03-05 12:08:03.026000', '2020-03-05 12:08:52.193000', 'hr1', 'hr1', 34, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (135, 49, '2020-03-23 11:25:10.037000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 33, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (136, 50, '2020-03-23 11:25:10.037000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 33, null, '1');

INSERT INTO public.IObGoodsReceiptPurchaseOrderReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (137, 51, '2020-03-23 11:25:10.037000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 57, null, '1');

INSERT INTO public.IObGoodsReceiptSalesReturnReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (138, 65, '2020-03-25 11:00:00', '2020-03-25 11:00:00', 'A.Hamed', 'A.Hamed', 64, null, '1');

INSERT INTO public.IObGoodsReceiptSalesReturnReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (139, 69, '2020-03-25 11:00:00', '2020-03-25 11:00:00', 'hr1', 'hr1', 57, null, '1');

INSERT INTO public.IObGoodsReceiptSalesReturnReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (140, 64, '2020-03-29 11:00:00', '2020-03-29 11:00:00', 'hr1', 'hr1', 64, null, '1');

INSERT INTO public.IObGoodsReceiptSalesReturnReceivedItemsData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (141, 70, '2020-07-16 09:30:00', '2020-07-16 09:30:00', 'A.Hamed', 'A.Hamed', 57, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (143, 74, '2020-07-29 09:49:56.602000', '2020-07-29 09:49:56.602000', 'hr1', 'hr1', 57, 'Item was damaged before delivery', '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (145, 76, '2020-07-29 10:13:32.304000', '2020-07-29 10:13:32.304000', 'hr1', 'hr1', 64, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (146, 76, '2020-07-29 10:13:32.314000', '2020-07-29 10:13:32.314000', 'hr1', 'hr1', 57, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (147, 81, '2020-08-07 10:00:00.000000', '2020-08-07 10:00:00.000000', 'hr1', 'hr1', 64, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (148, 83, '2020-08-18 14:47:00.000000', '2020-08-18 14:47:00.000000', 'hr1', 'hr1', 57, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (149, 84, '2020-08-18 14:56:00.000000', '2020-08-18 14:58:00.000000', 'hr1', 'hr1', 57, null, '1');

INSERT INTO public.iobgoodsreceiptsalesreturnreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (150, 87, '2020-08-27 14:56:00.000000', '2020-08-27 14:58:00.000000', 'hr1', 'hr1', 57, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (151,89,'2020-09-27 14:56:00.000000', '2020-09-27 14:58:00.000000', 'hr1', 'hr1', 1, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (152,26,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 35, null, '1');

INSERT INTO IObGoodsReceiptPurchaseOrderReceivedItemsData (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,itemid,differencereason,objecttypecode)
VALUES (153,26,'2020-03-05 10:36:10.488000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 36, null, '1');

INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (154, 90, '2020-12-21 17:33:09.203000', '2020-12-21 17:33:30.985000', 'hr1', 'hr1', 34, null, '1');

INSERT INTO public.iobgoodsreceiptpurchaseorderreceiveditemsdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, differencereason, objecttypecode)
VALUES (155, 91, '2020-12-22 09:30:00', '2020-12-22 09:30:00', 'A.Hamed', 'A.Hamed', 34, null, '1');
-------------------------------------------------------------------------------------------------------
INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (1,1,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 40.123456,37,'DAMAGED_STOCK','Package Opened');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (2,1,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 10.123456,38,'DAMAGED_STOCK','Package Opened');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (3,2,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',1002.0,40,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (4,3,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',14.123456,48,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (5,3,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',15.123456,49,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (6,6,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',2,40,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (7,7,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',33,48,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (8,4,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali', 40.0,37,'DAMAGED_STOCK','Package Opened');

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (9, 10, '2019-11-20 15:36:08.341000', '2019-11-20 15:36:08.341000', 'hr1', 'hr1', 10, 38,  'UNRESTRICTED_USE', null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (10, 11, '2019-11-20 15:37:17.613000', '2019-11-20 15:37:17.613000', 'hr1', 'hr1', 10, 37,  'DAMAGED_STOCK', null, NULL);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (11,16,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',14,48,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (12,16,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',15,49,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (13,17,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',40,39,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (14,17,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',80,37,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (16,19,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',10,39,'UNRESTRICTED_USE',null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (21, 40, '2019-11-20 15:37:17.613000', '2019-11-20 15:37:17.613000', 'hr1', 'hr1', 13, 16,'UNRESTRICTED_USE', null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (22, 40, '2019-11-20 15:37:17.613000', '2019-11-20 15:37:17.613000', 'hr1', 'hr1', 2, 46, 'UNRESTRICTED_USE', null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (23, 41, '2019-11-20 15:36:08.341000', '2019-11-20 15:36:08.341000', 'hr1', 'hr1', 10, 16, 'UNRESTRICTED_USE', null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (24, 42, '2019-11-20 15:36:08.341000', '2019-11-20 15:36:08.341000', 'hr1', 'hr1', 10, 16,'UNRESTRICTED_USE', null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (25, 43, '2019-11-20 15:36:08.341000', '2019-11-20 15:36:08.341000', 'hr1', 'hr1', 8, 16,  'UNRESTRICTED_USE', null, NULL);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (26,22,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',1002.0,43,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (28,24,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',1002.0,40,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (29,25,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',50.0,43,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (30,26,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',120.0,40,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (31,27,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',120.0,40,'UNRESTRICTED_USE','يوجد رول مطوي وذلك غالبا لوضعه في الكونتينر بطريقة خاطئة');

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo,modificationinfo, receivedqtyuoe, unitofentryid,stocktype, notes, itemvendorrecordid)
VALUES (100, 110, '2019-11-20 15:37:17.613000', '2019-11-20 15:37:17.613000', 'hr1', 'hr1', 10, 37, 'DAMAGED_STOCK',null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (101, 111, '2020-02-04 14:18:11.000000', '2020-02-04 14:18:12.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 100, 41, 'UNRESTRICTED_USE', null, 63);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (102, 111, '2020-02-04 14:34:52.000000', '2020-02-04 14:34:53.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 150, 23, 'UNRESTRICTED_USE', null, 62);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (103, 112, '2020-02-04 14:43:02.000000', '2020-02-04 14:43:03.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 100, 23, 'UNRESTRICTED_USE', null, 62);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (104, 113, '2020-02-04 14:43:47.000000', '2020-02-04 14:43:49.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 10, 23, 'UNRESTRICTED_USE', null, 62);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (105, 113, '2020-02-04 14:44:13.000000', '2020-02-04 14:44:15.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 20, 41, 'UNRESTRICTED_USE', null, 63);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid,  stocktype, notes, itemvendorrecordid)
VALUES (106, 114, '2020-02-04 14:44:49.000000', '2020-02-04 14:44:50.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 15, 23, 'UNRESTRICTED_USE', null, 62);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo,modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid)
VALUES (107, 110, '2019-11-20 15:37:17.613000', '2019-11-20 15:37:17.613000', 'hr1', 'hr1', 10.5, 37, 'UNRESTRICTED_USE',null, NULL);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (120, 126, '2020-03-05 10:36:37.982000', '2020-03-05 10:36:37.982000', 'hr1', 'hr1', 10, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (121, 127, '2020-03-05 10:43:48.885000', '2020-03-05 10:43:48.885000', 'hr1', 'hr1', 10, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (122, 129, '2020-03-05 12:08:52.193000', '2020-03-05 12:08:52.193000', 'hr1', 'hr1', 38, 37, 'UNRESTRICTED_USE', null, 2, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (123, 128, '2020-03-05 12:09:21.878000', '2020-03-05 12:09:21.878000', 'hr1', 'hr1', 38, 38, 'UNRESTRICTED_USE', null, 5, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (124, 128, '2020-03-05 12:09:55.824000', '2020-03-05 12:09:55.824000', 'hr1', 'hr1', 23, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (125, 128, '2020-03-05 12:10:04.773000', '2020-03-05 12:10:04.773000', 'hr1', 'hr1', 2, 37, 'DAMAGED_STOCK', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (131, 135, '2020-03-23 11:25:37.304000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 120, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (132, 21,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',10,37,'DAMAGED_STOCK','يوجد عبوتين ناقصتين لم يتم استلامهما وذلك لاخذهما كعينة للتحليل في الجمارك');

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (133, 136, '2020-03-23 11:25:37.304000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 120, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptPurchaseOrderItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (134, 137, '2020-03-23 11:25:37.304000', '2020-03-23 11:25:37.304000', 'hr1', 'hr1', 120, 37, 'UNRESTRICTED_USE', null, 1, null, null);

INSERT INTO public.IObGoodsReceiptSalesReturnItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype ,notes)
VALUES (135, 138, '2020-03-25 11:00:00', '2020-03-25 11:00:00', 'A.Hamed', 'A.Hamed', 9, 37, 'DAMAGED_STOCK',null);

INSERT INTO public.IObGoodsReceiptSalesReturnItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype ,notes)
VALUES (136, 139, '2020-03-25 11:00:00', '2020-03-25 11:00:00', 'hr1', 'hr1', 1, 41, 'DAMAGED_STOCK',null);

INSERT INTO public.IObGoodsReceiptSalesReturnItemQuantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype ,notes)
VALUES (137, 140, '2020-03-29 11:00:00', '2020-03-29 11:00:00', 'hr1', 'hr1', 5, 37, 'UNRESTRICTED_USE',null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (141, 146, '2020-08-05 06:45:00.000000', '2020-08-05 06:45:00.000000', 'hr1', 'hr1', 99, 41, 'DAMAGED_STOCK', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (142, 145, '2020-08-05 06:45:00.000000', '2020-08-05 06:45:00.000000', 'hr1', 'hr1', 1, 16, 'DAMAGED_STOCK', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (143, 141, '2020-08-05 11:01:00.000000', '2020-08-05 11:01:00.000000', 'hr1', 'hr1', 1, 41, 'DAMAGED_STOCK', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (144, 143, '2020-08-06 09:40:52.962000', '2020-08-06 09:40:52.975000', 'hr1', 'hr1', 1, 41, 'UNRESTRICTED_USE', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (145, 147, '2020-08-07 10:00:00.000000', '2020-08-07 10:00:00.000000', 'hr1', 'hr1', 1, 16, 'UNRESTRICTED_USE', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (146, 147, '2020-08-07 10:00:00.000000', '2020-08-07 10:00:00.000000', 'hr1', 'hr1', 1, 46, 'UNRESTRICTED_USE', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (147, 149, '2020-08-18 14:58:00.000000', '2020-08-18 14:58:00.000000', 'hr1', 'hr1', 1, 41, 'UNRESTRICTED_USE', null);

INSERT INTO public.iobgoodsreceiptsalesreturnitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes)
VALUES (148, 150, '2020-08-27 14:58:00.000000', '2020-08-27 14:58:00.000000', 'hr1', 'hr1', 1, 41, 'UNRESTRICTED_USE', null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (149,151,'2020-09-27 14:56:00.000000', '2020-09-27 14:58:00.000000','Ahmed.Ali','Ahmed.Ali',10.0,39,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (150,17,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',10,39,'DAMAGED_STOCK',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (151,17,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',20,37,'DAMAGED_STOCK',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (152,152,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',50.0,43,'UNRESTRICTED_USE',null);

INSERT INTO IObGoodsReceiptPurchaseOrderItemQuantities (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,receivedqtyuoe,unitofentryid,stocktype,notes)
VALUES (153,153,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',50.0,4,'UNRESTRICTED_USE',null);

INSERT INTO iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (154, 154, '2020-12-21 17:33:22.183000', '2020-12-21 17:34:21.693000', 'hr1', 'hr1', 0.5, 37, 'UNRESTRICTED_USE', null, 2, 1.008181818181818, null);

INSERT INTO iobgoodsreceiptpurchaseorderitemquantities (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, receivedqtyuoe, unitofentryid, stocktype, notes, itemvendorrecordid, estimatedcost, actualcost)
VALUES (155, 154, '2020-12-21 17:33:30.945000', '2020-12-21 17:34:23.574000', 'hr1', 'hr1', 0.5, 37, 'DAMAGED_STOCK', null, 2, 1.008181818181818, null);

-----------------------------------------------------------------------------------------------------
INSERT INTO iobgoodsreceiptitembatches (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,batchcode,receivedqtyuoe,unitofentryid,productiondate,expirationdate,stocktype,notes)
VALUES (1,12,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',14536777, 9.123456,42,'2018-01-25 00:00:00','2020-01-25 00:00:00','DAMAGED_STOCK','Crashed while discharge');

INSERT INTO iobgoodsreceiptitembatches (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,batchcode,receivedqtyuoe,unitofentryid,productiondate,expirationdate,stocktype,notes)
VALUES (2,12,'2018-01-05 11:00:00','2018-01-05 11:00:00','Ahmed.Ali','Ahmed.Ali',14536444, 2.123456,42,'2018-01-23 00:00:00','2020-01-23 00:00:00','UNRESTRICTED_USE',null);

INSERT INTO public.iobgoodsreceiptitembatches (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, batchcode, receivedqtyuoe, unitofentryid, productiondate, expirationdate,stocktype, notes, itemvendorrecordid)
VALUES (3, 15, '2018-01-05 11:00:00.000000', '2018-01-05 11:00:00.000000', 'Ahmed.Ali', 'Ahmed.Ali', '14536444', 2.123456, 42, '2019-08-06 00:00:00.000000', '2020-10-10 00:00:00.000000', 'UNRESTRICTED_USE', null, null);

INSERT INTO public.iobgoodsreceiptitembatches (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, batchcode, receivedqtyuoe, unitofentryid, productiondate, expirationdate,stocktype, notes, itemvendorrecordid)
VALUES (4, 15, '2018-01-05 11:00:00.000000', '2018-01-05 11:00:00.000000', 'Ahmed.Ali', 'Ahmed.Ali', '14536777', 9.123456, 42, '2019-08-06 00:00:00.000000', '2020-10-10 00:00:00.000000', 'DAMAGED_STOCK', 'Crashed while discharge', null);

