ALTER SEQUENCE iobstocktransformationdetails_id_seq RESTART WITH 4;
------------------------------------------------------------------------------------
INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId, transformationreasonid)
VALUES (1, 54, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'UNRESTRICTED_USE',2.0, 1, 2);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId,newstoreTransactionId, transformationreasonid)
VALUES (2, 55, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'DAMAGED_STOCK',2.0, 12,28 ,1);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId, newstoreTransactionId,transformationreasonid)
VALUES (3, 56, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'UNRESTRICTED_USE',2.0, 26, 30,2);

