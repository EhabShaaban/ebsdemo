ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 94;
ALTER SEQUENCE dobstocktransformation_id_seq RESTART WITH 57;
ALTER SEQUENCE iobinventoryactivationdetails_id_seq RESTART WITH 54;
DELETE from IObInventoryActivationDetails where refinstanceid in (select id from dobinventorydocument  where inventorydocumenttype ='StkTr') ;
DELETE FROM IObStockTransformationDetails;
DELETE FROM DObInventoryDocument WHERE inventorydocumenttype = 'StkTr';
DELETE FROM dobstocktransformation;
--------------------------------------------------------------------------------
INSERT INTO DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (54, '2020000001', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000',  'hr1', 'hr1', '["Draft"]', 19,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (54, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid,inventorydocumenttype)
VALUES (55, '2020000002', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', '["Active"]', 1,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (55, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid,inventorydocumenttype)
VALUES (56, '2020000003', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', '["Active"]', 19,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (56, 1);
------------------------------------------------------------------------------------
INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (52,55,'2020-11-03 09:40:00.000000','2020-11-03 09:40:00.000000','hr1','hr1','hr1','2020-11-03 09:45:00.000000');

INSERT INTO IObInventoryActivationDetails (id,refinstanceid,creationdate,modifieddate,creationinfo,modificationinfo,activatedBy,activationDate)
VALUES (53,56,'2020-11-03 09:40:00.000000','2020-11-03 09:40:00.000000','hr1','hr1','hr1','2020-11-03 09:45:00.000000');
