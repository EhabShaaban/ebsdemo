DELETE FROM dobstocktransformation ;
DELETE FROM cobstocktransformationreason WHERE id in (1,2);
DELETE FROM cobstocktransformationtype WHERE id =1;


INSERT INTO cobstocktransformationtype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1,'STOCK_TYPE_TO_STOCK_TYPE', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'hr1', 'hr1', '["Active"]',
'{"en":"Stock Type to Stock Type","ar":"من نوع مخزون إلي نوع مخزون آخر"}');

INSERT INTO cobstocktransformationreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name , typeid,fromStockType,toStockType)
VALUES (1,'0001', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'hr1', 'hr1', '["Active"]',
'{"en":"Items damaged during storage","ar":"تم تلفها اثناء التخزين"}', 1,'UNRESTRICTED_USE','DAMAGED_STOCK');

INSERT INTO cobstocktransformationreason (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name , typeid,fromStockType,toStockType)
VALUES (2,'0002', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'hr1', 'hr1', '["Active"]',
'{"en":"Item will be sold","ar":"سيتم بيع الصنف "}', 1,'DAMAGED_STOCK','UNRESTRICTED_USE');
