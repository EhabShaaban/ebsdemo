ALTER SEQUENCE dobstocktransformation_id_seq RESTART WITH 71;
ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 94;
DELETE FROM DObInventoryDocument WHERE inventorydocumenttype = 'StkTr';
DELETE FROM dobstocktransformation;
DELETE FROM EntityUserCode
WHERE type = 'DObStockTransformation';
INSERT INTO EntityUserCode (type, code)
VALUES ('DObStockTransformation', '2020000050');
----------------------------------------------------------------------

INSERT INTO DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (54, '2020000001', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000',  'hr1', 'hr1', '["Draft"]', 19,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (54, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid,inventorydocumenttype)
VALUES (55, '2020000002', '2020-11-03 09:40:00.000000', '2020-11-04 09:40:00.000000', 'hr1', 'hr1', '["Active"]', 1,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (55, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid,inventorydocumenttype)
VALUES (56, '2020000003', '2020-11-03 09:40:00.000000', '2020-11-04 09:40:00.000000', 'hr1', 'hr1', '["Active"]', 19,'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (56, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid, inventorydocumenttype)
VALUES (57, '2020000004', '2020-02-06 21:40:00.000000', '2020-02-06 21:40:00.000000', 'Niveen', 'Niveen', '["Draft"]',19, 'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (57, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid, inventorydocumenttype)
VALUES (58, '2020000005', '2020-02-06 21:40:00.000000', '2020-02-06 21:40:00.000000', 'Niveen', 'Niveen', '["Draft"]',19, 'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (58, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid, inventorydocumenttype)
VALUES (59, '2020000048', '2020-11-03 09:40:00.000000', '2020-11-04 09:40:00.000000', 'A.Hamed', 'A.Hamed','["Active"]', 19, 'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (59, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid, inventorydocumenttype)
VALUES (60, '2020000049', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'A.Hamed', 'A.Hamed', '["Draft"]',19, 'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (60, 1);

INSERT INTO DObInventoryDocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,purchaseunitid, inventorydocumenttype)
VALUES (61, '2020000050', '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'A.Hamed', 'A.Hamed', '["Draft"]',19, 'StkTr');
INSERT INTO dobstocktransformation(id, transformationtypeid)
VALUES (61, 1);
