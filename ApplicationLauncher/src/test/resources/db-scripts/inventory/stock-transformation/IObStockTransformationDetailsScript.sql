ALTER SEQUENCE iobstocktransformationdetails_id_seq RESTART WITH 51;
DELETE FROM IObStockTransformationDetails;
------------------------------------------------------------------------------
INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId, transformationreasonid)
VALUES (1, 54, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'UNRESTRICTED_USE',2.0, 1, 2);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId,newstoreTransactionId, transformationreasonid)
VALUES (2, 55, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'DAMAGED_STOCK',2.0, 12,28 ,1);


INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,newstocktype,transformedqty,storeTransactionId, newstoreTransactionId,transformationreasonid)
VALUES (3, 56, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'hr1', 'hr1', 'UNRESTRICTED_USE',2.0, 26, 30,2);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,
                                          newstocktype, transformedqty, storeTransactionId, transformationreasonid)
VALUES (4, 57, '2020-02-06 21:40:00.000000', '2020-02-06 21:40:00.000000', 'Niveen', 'Niveen', 'DAMAGED_STOCK', 5.0, 14,
        1);

insert into IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,
                                          newstocktype, transformedqty, storeTransactionId, transformationreasonid)
VALUES (5, 58, '2020-02-06 21:40:00.000000', '2020-02-06 21:40:00.000000', 'Niveen', 'Niveen', 'DAMAGED_STOCK', 1.0,
        11, 1);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,
                                          newstocktype, transformedqty, storeTransactionId,newstoreTransactionId, transformationreasonid)
VALUES (48, 59, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'A.Hamed', 'A.Hamed', 'DAMAGED_STOCK', 2.0,9,34, 1);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,
                                          newstocktype, transformedqty, storeTransactionId, transformationreasonid)
VALUES (49, 60, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'A.Hamed', 'A.Hamed', 'DAMAGED_STOCK',5.0, 11, 1);

INSERT INTO IObStockTransformationDetails(id, refInstanceId, creationdate, modifieddate, creationinfo, modificationinfo,
                                          newstocktype, transformedqty, storeTransactionId, transformationreasonid)
VALUES (50, 61, '2020-11-03 09:40:00.000000', '2020-11-03 09:40:00.000000', 'A.Hamed', 'A.Hamed', 'UNRESTRICTED_USE',2.0, 1, 2);
