DELETE FROM iobgoodsissuesalesinvoiceitem;
DELETE FROM iobgoodsissuesalesorderitem;
ALTER SEQUENCE iobgoodsissuesalesinvoiceitem_id_seq RESTART WITH 11;
ALTER SEQUENCE iobgoodsissuesalesorderitem_id_seq RESTART WITH 34;

------------------------------------------------GI_SI Items------------------------------------------------------
INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (1, '2019-11-06 14:25:42.000000', '2019-11-06 14:25:52.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 1, 64, 16, 5, null, 200);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno)
VALUES (3, '2019-11-06 14:38:28.000000', '2019-11-06 14:38:30.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2 ,58, 23, 100, null);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno)
VALUES (4, '2019-11-06 14:38:59.000000', '2019-11-06 14:39:01.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2, 57, 23, 500, null);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno)
VALUES (5, '2019-11-06 14:39:20.000000', '2019-11-06 14:39:21.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 2, 40, 1, 136, null);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (6 ,'2019-11-19 10:57:16.000000', '2019-11-19 10:57:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 6, 40, 1, 136, null, 22);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno)
VALUES (7, '2019-11-19 12:41:52.000000', '2019-11-19 12:41:53.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 58, 23, 200, null);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno)
VALUES (8, '2019-11-20 14:57:49.000000', '2019-11-20 14:57:51.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 40, 1, 136, null);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (9, '2019-01-01 14:38:28.000000', '2019-01-01 14:38:30.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 9, 64, 16, 14, null, 22);

INSERT INTO public.iobgoodsissuesalesinvoiceitem (id, creationdate, modifieddate, creationinfo, modificationinfo, refinstanceid, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (10, '2019-11-19 10:57:16.000000', '2019-11-19 10:57:19.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 3, 57, 23, 30, null, 200);

------------------------------------------------GI_SO Items------------------------------------------------------
INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (11,62, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin',  57, 41, 2, null, 10);

INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (12,63, '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin',  64, 16, 2, null, 10);

INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (13,66, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin',  57, 41, 2, null, 10);

INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (14,67, '2020-08-05 09:00:00.000000', '2020-08-05 09:00:00.000000', 'Admin', 'Admin',  64, 16, 2, null, 10);

INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (15,68, '2020-11-09 09:30:00.000000', '2020-11-09 09:30:00.000000', 'A.Hamed', 'A.Hamed',  57, 41, 5, null, 2);

INSERT INTO public.iobgoodsissuesalesorderitem (id,refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo,  itemid, unitofmeasureid, quantity, batchno, price)
VALUES (16,68, '2020-11-09 09:30:00.000000', '2020-11-09 09:30:00.000000', 'A.Hamed', 'A.Hamed',  64, 16, 2, null, 2);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (18, 71, '2020-07-27 08:07:38.112000', '2020-07-27 08:07:38.112000', 'hr1', 'hr1', 64, 16, 2, null, 2);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (19, 71, '2020-07-27 07:52:19.124000', '2020-07-27 07:52:19.124000', 'hr1', 'hr1', 57, 41, 99, null, 2);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (21, 77, '2020-08-05 13:42:33.575000', '2020-08-05 13:42:33.575000', 'hr1', 'hr1', 57, 23, 10, null, 40);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (22, 78, '2020-08-05 13:45:30.905000', '2020-08-05 13:45:30.905000', 'hr1', 'hr1', 57, 23, 10, null, 33);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (23, 79, '2020-08-06 11:35:03.933000', '2020-08-06 11:35:03.933000', 'hr1', 'hr1', 57, 23, 10, null, 25);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (24, 80, '2020-08-06 12:01:00', '2020-08-06 12:01:00', 'hr1', 'hr1', 64, 16, 1, null, 40);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (25, 80, '2020-08-06 12:01:00', '2020-08-06 12:01:00', 'hr1', 'hr1', 64, 46, 1, null, 30);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (26, 82, '2020-08-17 12:39:35.694000', '2020-08-17 12:39:35.694000', 'hr1', 'hr1', 64, 46, 5, null, 29);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (27, 85, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 57, 23, 10, null, 33);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (28, 86, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 57, 23, 10, null, 40);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (29, 88, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 58, 23, 1, null, 10);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (30, 92, '2021-02-02 10:00:22.934000', '2021-02-02 10:00:22.934000', 'hr1', 'hr1', 71, 23, 50, null, 95);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (31, 92, '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', 1, 37, 50, null, 65);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (32, 93, '2021-02-02 10:00:22.934000', '2021-02-02 10:00:22.934000', 'hr1', 'hr1', 71, 23, 50, null, 95);

INSERT INTO public.iobgoodsissuesalesorderitem (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, itemid, unitofmeasureid, quantity, batchno, price)
VALUES (33, 93, '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', 1, 37, 50, null, 65);
