DELETE FROM tobgoodsissuestoretransaction;
DELETE FROM tobgoodsreceiptstoretransaction;
DELETE FROM tobstoretransaction;
DELETE FROM dobgoodsissuesalesinvoice;
DELETE FROM DObGoodsIssueSalesOrder;
DELETE FROM dobinventorydocument where inventorydocumenttype in ('GI_SI', 'GI_SO');
DELETE FROM IObGoodsIssueSalesOrderData;
delete from IObInventoryCompany where id between 1 and 4;
delete from IObInventoryCompany where id IN (55, 56);
ALTER SEQUENCE IObInventoryCompany_id_seq RESTART WITH 87;
ALTER SEQUENCE iobgoodsissuesalesorderdata_id_seq RESTART WITH 3;

DELETE FROM EntityUserCode WHERE type = 'DObGoodsIssue';
INSERT INTO EntityUserCode (type, code)
VALUES ('DObGoodsIssue', '2021000001');
ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 83;
------------------------------------------------------------------------------------
---------------------------------------------GI_SI-------------------------------------------------------
INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (1, '2019000001', '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 1,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (1, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (2, '2019000002', '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (2, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (3, '2019000003', '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (3, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (4, '2019000004', '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 1,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (4, 1);
-----------------------------------------------------------------------------------------------------------
INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (1, 2, '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, 'For GI No 2019000002',59);
INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (2, 4, '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, null, 38, null,null );
INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (3, 3, '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, null,66 );
INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (4, 1, '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 1, 2 , 38, 'For GI No 2019000001',68 );

-----------------------------------------------------------------------------------------------------------

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (1, 2, '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 39);
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (2, 3, '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 16, 39);
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (3, 4, '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 39);
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (4, 1, '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 10, 12, 14, 25);
------------------------------------------------------------------------------------------------------------
---------------------------------------------GI_SO-------------------------------------------------------
INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (62, '2020000001', '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', '["Draft"]', 19,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (62, 2);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (63, '2020000002', '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', '["Active"]', 1,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (63, 2);
------------------------------------------------------------------------------------------------------------
INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (1, 62, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 73, 3, 1, 41, null,67);

INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (2, 63, '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', 72, 2, 2, 40, null,59 );
------------------------------------------------------------------------------------------------------------
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (55, 62, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (56, 63, '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', 10, 12, 14, 39);
------------------------------------------------------------------------------------------------------------
