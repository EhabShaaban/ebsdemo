DELETE from IObGoodsIssueSalesOrderData;
ALTER SEQUENCE iobgoodsissuesalesorderdata_id_seq RESTART WITH 17;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (1, 62, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 73, 3, 1, 41, null,67);

INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (2, 63, '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', 72, 2, 2, 40, null,59 );

INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (3, 66, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 73, 3, 1, 41, null,67);

INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (4, 67, '2020-08-05 09:00:00.000000', '2020-08-05 09:00:00.000000', 'Admin', 'Admin', 72, 2, 2, 40, null,59 );

INSERT INTO public.IObGoodsIssueSalesOrderData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesRepresentativeId, comments, salesOrderId )
VALUES (5, 68, '2020-11-09 09:00:00.000000', '2020-11-09 09:00:00.000000', 'A.Hamed', 'A.Hamed', 73, 4, 1, 41, null,81 );

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (6, 71, '2020-07-27 07:52:19.110000', '2020-07-27 07:52:19.110000', 'hr1', 'hr1', 81, 73, 4, 1, 41, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (7, 77, '2020-08-05 13:42:33.572000', '2020-08-05 13:42:33.572000', 'hr1', 'hr1', 63, 73, 3, 1, 38, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (8, 78, '2020-08-05 13:45:30.902000', '2020-08-05 13:45:30.902000', 'hr1', 'hr1', 62, 73, 3, 1, 41, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (9, 79, '2020-08-06 11:35:03.920000', '2020-08-06 11:35:03.920000', 'hr1', 'hr1', 61, 73, 4, 1, 38, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (10, 80, '2020-08-06 12:01:00', '2020-08-06 12:01:00', 'hr1', 'hr1', 82, 73, 1, 1, 38, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (11, 82, '2020-08-17 12:39:35.718000', '2020-08-17 12:39:35.718000', 'hr1', 'hr1', 83, 73, 4, 1, 41, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (12, 85, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 62, 73, 3, 1, 41, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (13, 86, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 63, 73, 3, 1, 38, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (14, 88, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 85, 73, 4, 1, 38, 'potato GI');

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (15, 92, '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', 84, 73, 4, 1, 41, null);

INSERT INTO public.iobgoodsissuesalesorderdata (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, salesorderid, customerid, addressid, contactpersonid, salesrepresentativeid, comments)
VALUES (16, 93, '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', 84, 73, 4, 1, 41, null);
