DELETE from IObGoodsIssueSalesInvoiceData;
ALTER SEQUENCE iobgoodsissuesalesinvoicedata_id_seq RESTART WITH 11;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (1, 2, '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, 'For GI No 2019000002',59);

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (2, 4, '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, null, 38, null,null );

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (3, 3, '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, null,66 );

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId )
VALUES (4, 1, '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 72, 1, 2 , 38, 'For GI No 2019000001',68 );

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId)
VALUES (6, 6, '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, null,59);

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId)
VALUES (7, 7, '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 66, 1, 1, 38, null,59);

INSERT INTO public.IObGoodsIssueSalesInvoiceData (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, customerid, addressid, contactpersonid, salesrepresentativeid, comments, salesInvoiceId)
VALUES (10, 9, '2019-01-01 09:02:00.000000', '2019-01-01 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 67, 1, 1, 38, 'For GI No 2019000010',59);