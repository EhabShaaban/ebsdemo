delete from iobinventoryactivationdetails where id in (66,71,73,74,75,76);
ALTER SEQUENCE IObInventoryActivationDetails_id_seq RESTART WITH 77;
----------------------------------------------------------------------------------------------------
INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (66, 66, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'hr1', 'hr1', 'hr1', '2020-11-06 09:00:00.000000');

INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (71, 71, '2020-07-27 07:52:32.686000', '2020-07-27 07:52:32.698000', 'hr1', 'hr1', 'hr1', '2020-07-27 07:52:32.686000');

INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (73, 77, '2020-08-05 13:44:49.495000', '2020-08-05 13:44:49.502000', 'hr1', 'hr1', 'hr1', '2020-08-05 13:44:49.495000');

INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (74, 78, '2020-08-05 13:45:35.376000', '2020-08-05 13:45:35.383000', 'hr1', 'hr1', 'hr1', '2020-08-05 13:45:35.376000');

INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (75, 80, '2020-08-06 12:01:00', '2020-08-06 12:01:00', 'hr1', 'hr1', 'hr1', '2020-08-06 12:01:00');

INSERT INTO public.iobinventoryactivationdetails (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, activatedby, activationdate)
VALUES (76, 88, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 'hr1', '2020-11-03 11:30:00');
