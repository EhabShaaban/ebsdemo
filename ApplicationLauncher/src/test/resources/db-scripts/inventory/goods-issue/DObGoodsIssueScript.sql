delete from tobgoodsissuestoretransaction;
delete from tobgoodsreceiptstoretransaction;
delete from tobstoretransaction;
delete from dobgoodsissuesalesinvoice;
delete from DObGoodsIssueSalesOrder;
delete from dobinventorydocument where inventorydocumenttype in ('GI_SI', 'GI_SO');

DELETE from EntityUserCode WHERE type = 'DObGoodsIssue';
INSERT INTO EntityUserCode (type, code)
values ('DObGoodsIssue', '2021000002');
ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 94;
---------------------------------------------GI_SI-------------------------------------------------------
INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (1, '2019000001', '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 1,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (1, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (2, '2019000002', '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (2, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (3, '2019000003', '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (3, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (4, '2019000004', '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 1,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (4, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (6, '2019000005', '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (6, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (7, '2019000006', '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (7, 1);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (9, '2019000010', '2019-01-01 09:02:00.000000', '2019-01-01 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Draft"]', 19,'GI_SI');
INSERT INTO public.DObGoodsIssueSalesInvoice (id, typeid)
VALUES (9, 1);

---------------------------------------------GI_SO-------------------------------------------------------
INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (62, '2020000001', '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', '["Draft"]', 19,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (62, 2);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (66, '2020000003', '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', '["Active"]', 19,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (66, 2);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (63, '2020000002', '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', '["Active"]', 1,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (63, 2);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (67, '2020000004', '2020-08-05 09:00:00.000000', '2020-08-05 09:00:00.000000', 'Admin', 'Admin', '["Draft"]', 1,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (67, 2);

INSERT INTO public.DObInventoryDocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,  purchaseunitid, inventorydocumenttype)
VALUES (68, '2020000005', '2020-11-09 09:00:00.000000', '2020-11-09 09:00:00.000000', 'A.Hamed', 'A.Hamed', '["Draft"]', 19,'GI_SO');
INSERT INTO public.DObGoodsIssueSalesOrder (id, typeid)
VALUES (68, 2);

INSERT INTO dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (71, '2020000006', '2020-07-27 05:52:19.088000', '2020-07-27 05:52:32.689000', 'hr1', 'hr1', '["Active"]', 19, 'GI_SO');
INSERT INTO dobgoodsissuesalesorder (id, typeid)
VALUES (71, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (77, '2020000008', '2020-08-05 13:42:33.563000', '2020-08-05 13:44:49.498000', 'hr1', 'hr1', '["Active"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (77, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (78, '2020000009', '2020-08-05 13:45:30.896000', '2020-08-05 13:45:35.378000', 'hr1', 'hr1', '["Active"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (78, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (79, '2020000010', '2020-08-06 11:35:00.933000', '2020-08-06 11:35:00.933000', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (79, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (80, '2020000011', '2020-08-06 12:01:00', '2020-08-06 12:01:00', 'hr1', 'hr1', '["Active"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (80, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (82, '2020000012', '2020-08-17 12:39:00', '2020-08-17 12:39:00', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (82, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (85, '2020000013', '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (85, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (86, '2020000014', '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (86, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (88, '2020000015', '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', '["Active"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (88, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (92, '2021000001', '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (92, 2);

INSERT INTO public.dobinventorydocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, purchaseunitid, inventorydocumenttype)
VALUES (93, '2021000002', '2020-02-02 12:40:00', '2020-02-02 12:40:00', 'hr1', 'hr1', '["Draft"]', 19, 'GI_SO');
INSERT INTO public.dobgoodsissuesalesorder (id, typeid)
VALUES (93, 2);

