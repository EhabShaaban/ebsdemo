delete from IObInventoryCompany where id between 1 and 10;
delete from IObInventoryCompany where id between 55 and 59;
delete from IObInventoryCompany where id in (71, 77, 78, 79, 80, 82, 85, 86, 88,92);
ALTER SEQUENCE IObInventoryCompany_id_seq RESTART WITH 94;

---------------------------------------------GI_SI-------------------------------------------------------
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (1, 2, '2019-08-05 09:02:00.000000', '2019-08-05 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (2, 3, '2019-08-06 09:02:00.000000', '2019-08-06 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 16, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (3, 4, '2019-10-10 09:02:00.000000', '2019-10-10 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (4, 1, '2019-07-03 09:02:00.000000', '2019-07-03 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 10, 12, 14, 25);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (6, 6, '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 25);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (7, 7, '2019-11-19 09:02:00.000000', '2019-11-19 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 25);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (10, 9, '2019-01-01 09:02:00.000000', '2019-01-01 09:02:00.000000', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', 7, 8, 9, 39);

---------------------------------------------GI_SO-------------------------------------------------------
INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (55, 62, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (56, 63, '2020-11-08 09:00:00.000000', '2020-11-08 09:00:00.000000', 'Admin', 'Admin', 10, 12, 14, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (57, 66, '2020-11-06 09:00:00.000000', '2020-11-06 09:00:00.000000', 'Admin', 'Admin', 7, 8, 9, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (58, 67, '2020-08-05 09:00:00.000000', '2020-08-05 09:00:00.000000', 'Admin', 'Admin', 10, 12, 14, 39);

INSERT INTO public.IObInventoryCompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (59, 68, '2020-11-09 09:00:00.000000', '2020-11-09 09:00:00.000000', 'A.Hamed', 'A.Hamed', 7, 8, 9, 39);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (71, 71, '2020-07-27 07:52:19.126000', '2020-07-27 07:52:19.126000', 'hr1', 'hr1', 7, 8, 9, 25);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (77, 77, '2020-08-05 13:42:33.577000', '2020-08-05 13:42:33.577000', 'hr1', 'hr1', 7, 8, 9, 39);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (78, 78, '2020-08-05 13:45:30.908000', '2020-08-05 13:45:30.908000', 'hr1', 'hr1', 7, 8, 9, 39);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (79, 79, '2020-08-06 11:35:03.944000', '2020-08-06 11:35:03.944000', 'hr1', 'hr1', 7, 8, 9, 39);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (80, 80, '2020-08-06 12:00:00', '2020-08-06 12:01:00', 'hr1', 'hr1', 7, 8, 9, 39);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (82, 82, '2020-08-17 12:39:35.708000', '2020-08-17 12:39:35.708000', 'hr1', 'hr1', 7, 8, 9, 32);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (85, 85, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 7, 8, 9, 32);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (86, 86, '2020-08-17 12:40:00', '2020-08-17 12:40:00', 'hr1', 'hr1', 7, 8, 9, 32);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (88, 88, '2020-11-03 11:00:00', '2020-11-03 11:00:00', 'A.Hamed', 'A.Hamed', 7, 8, 9, 32);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (92, 92, '2021-02-02 12:40:00', '2021-02-02 12:40:00', 'hr1', 'hr1', 7, 8, 9, 25);

INSERT INTO public.iobinventorycompany (id, refinstanceid, creationdate, modifieddate, creationinfo, modificationinfo, companyid, plantid, storehouseid, storekeeperid)
VALUES (93, 93, '2021-05-11 12:40:00', '2021-05-11 12:40:00', 'hr1', 'hr1', 7, 8, 16, 25);
