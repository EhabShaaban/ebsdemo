DELETE from EntityUserCode WHERE type = 'DObInitialStockUpload';
DELETE from EntityUserCode WHERE type = 'TObStoreTransaction';
INSERT INTO EntityUserCode (type, code)
values ('TObStoreTransaction', '2020000033');

TRUNCATE dobinventorydocument CASCADE;
TRUNCATE cobstockavailability CASCADE;

TRUNCATE tobinitialstockuploadstoretransaction CASCADE ;
DELETE from TObStoreTransaction WHERE objecttypecode = 'ISU';

ALTER SEQUENCE dobinventorydocument_id_seq RESTART WITH 1;
