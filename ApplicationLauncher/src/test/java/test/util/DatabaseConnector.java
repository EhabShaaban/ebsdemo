package test.util;

import com.ebs.dac.common.utils.exceptions.ResourceNotFoundException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @auther Marisoft Team
 * @since Dec 12, 2017
 */
public class DatabaseConnector {

  private static DatabaseConnector myInstance = null;
  private static String databaseURL;
  private static String databaseUser;
  private static String databasePassword;
  private final Logger logger = LoggerFactory.getLogger(getClass());
  private Connection con;

  public static DatabaseConnector getInstance() {
    if (myInstance == null) myInstance = new DatabaseConnector();
    return myInstance;
  }

  public static void setConnectionValues(
      String databaseURL, String databaseUser, String databasePassword) {
    DatabaseConnector.databaseURL = databaseURL;
    DatabaseConnector.databaseUser = databaseUser;
    DatabaseConnector.databasePassword = databasePassword;
  }

  public void executeSQLScript(String scriptFilesPathes) throws Exception {

    Statement stmt = null;
    String currentFilePath = null;
    try {
      StringTokenizer filesPathesTokenizer = new StringTokenizer(scriptFilesPathes, ",");
      while (filesPathesTokenizer.hasMoreTokens()) {
        currentFilePath = filesPathesTokenizer.nextToken();
        String scriptQueries = parseSQLQueries(currentFilePath);
        StringTokenizer queriesTokenizer = new StringTokenizer(scriptQueries, ";");
        while (queriesTokenizer.hasMoreTokens()) {
          stmt = con.createStatement();
          stmt.execute(queriesTokenizer.nextToken());
        }
      }
    } catch (Exception ex) {
      logger.error("Error happend at file: " + currentFilePath);
      throw new Exception("Error happend at file: " + currentFilePath, ex);
    } finally {
      if (stmt != null) {
        stmt.close();
      }
    }
  }

  public PreparedStatement createPreparedStatement(String query) throws Exception {
    return con.prepareStatement(query);
  }

  public ResultSet executeQuery(PreparedStatement preparedStatement) throws Exception {
    return preparedStatement.executeQuery();
  }

  public int executeUpdate(PreparedStatement preparedStatement) throws Exception {
    return preparedStatement.executeUpdate();
  }

  public void closeConnection() throws SQLException {
    if (con != null) {
      con.close();
    }
  }

  public void createConnection() throws SQLException, IOException {
    Properties props = new Properties();
    props.setProperty("user", databaseUser);
    props.setProperty("password", databasePassword);
    con = DriverManager.getConnection(databaseURL, props);
  }

  private String parseSQLQueries(String fileName) throws Exception {

    StringBuilder result = new StringBuilder();

    URL url = getClass().getClassLoader().getResource(fileName);
    if (url == null) {
      throw new ResourceNotFoundException("File: '" + fileName + "' not found.");
    }
    File file = new File(url.getFile());

    try (Scanner scanner = new Scanner(file)) {

      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        result.append(line).append("\n");
      }

      scanner.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

    return result.toString();
  }
}
