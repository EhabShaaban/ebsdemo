package com.ebs.dda.accessrights;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/access-rights/1-users-insertion.feature",
      "classpath:features/access-rights/2-parent-roles-insertion.feature",
      "classpath:features/access-rights/3-users-parent-roles-assignment.feature",
      "classpath:features/access-rights/4-system-admin-access-rights.feature",
      "classpath:features/access-rights/item-access-rights.feature",
      "classpath:features/access-rights/vendor-access-rights.feature",
      "classpath:features/access-rights/item-vendor-record-access-rights.feature",
      "classpath:features/access-rights/measure-access-rights.feature",
      "classpath:features/access-rights/purchase-unit-access-rights.feature",
      "classpath:features/access-rights/purchase-responsible-access-rights.feature",
      "classpath:features/access-rights/storekeeper-access-rights.feature",
      "classpath:features/access-rights/currency-access-rights.feature",
      "classpath:features/access-rights/item-group-access-rights.feature",
      "classpath:features/access-rights/document-types-access-rights.feature",
      "classpath:features/access-rights/incoterm-access-rights.feature",
      "classpath:features/access-rights/transport-modes-access-rights.feature",
      "classpath:features/access-rights/container-types-access-rights.feature",
      "classpath:features/access-rights/countries-access-rights.feature",
      "classpath:features/access-rights/ports-access-rights.feature",
      "classpath:features/access-rights/payment-term-access-rights.feature",
      "classpath:features/access-rights/shipping-instructions-access-rights.feature",
      "classpath:features/access-rights/purchase-order-access-rights.feature",
      "classpath:features/access-rights/goods-receipt-access-rights.feature",
      "classpath:features/access-rights/company-access-rights.feature",
      "classpath:features/access-rights/plant-access-rights.feature",
      "classpath:features/access-rights/storehouse-access-rights.feature",
        "classpath:features/access-rights/stocktypes-access-rights.feature",
        "classpath:features/access-rights/invoice-access-rights.feature"
    },
    strict = true)
public class AccessRightsInsertionCucumberRunner {}