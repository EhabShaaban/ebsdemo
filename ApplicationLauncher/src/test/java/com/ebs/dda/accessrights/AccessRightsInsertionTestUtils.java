package com.ebs.dda.accessrights;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ebs.dac.security.usermanage.utils.BDKUserUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class AccessRightsInsertionTestUtils {

  protected EntityManagerDatabaseConnector entityManagerDatabaseConnector;

  public void setEntityManagerDatabaseConnector(
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/access-rights/update-after-seeding.sql");
    return str.toString();
  }

  public void createUsers(DataTable usersDatatable) {
    List<Map<String, String>> usersMaps = usersDatatable.asMaps(String.class, String.class);
    for (Map<String, String> userMap : usersMaps) {
      Object[] userDetails = getUserByUsername(userMap);
      if (userDetails == null) {
        String encodedPassword =
            BDKUserUtils.encodePassword(userMap.get(IFeatureFileCommonKeys.PASSWORD));
        int numberOfInsertedUsers = insertUser(userMap, encodedPassword);
        int numberOfInsertedUserAccounts = insertUserAccount(userMap);
        int numberOfInsertedUsersInfo = insertUserInfo(userMap);
        assertTrue(
            numberOfInsertedUsers == 1
                && numberOfInsertedUserAccounts == 1
                && numberOfInsertedUsersInfo == 1);
      }
    }
  }

  private int insertUser(Map<String, String> userMap, String encodedPassword) {
    return entityManagerDatabaseConnector.executeInsertQuery(
        "insertUser", userMap.get(IFeatureFileCommonKeys.USER), encodedPassword);
  }

  private int insertUserAccount(Map<String, String> userMap) {
    return entityManagerDatabaseConnector.executeInsertQuery(
        "insertUserAccount", userMap.get(IFeatureFileCommonKeys.USER));
  }

  private int insertUserInfo(Map<String, String> userMap) {
    return entityManagerDatabaseConnector.executeInsertQuery(
        "insertUserInfo",
        userMap.get(IFeatureFileCommonKeys.USER),
        userMap.get(IFeatureFileCommonKeys.PROFILE_NAME));
  }

  public void assertUsersHaveBeenCreated(DataTable usersDatatable) {
    List<Map<String, String>> usersMaps = usersDatatable.asMaps(String.class, String.class);
    for (Map<String, String> userMap : usersMaps) {
      Object[] userDetails = getUserByUsername(userMap);
      assertEquals(userMap.get(IFeatureFileCommonKeys.PROFILE_NAME), userDetails[1]);
      BDKUserUtils.checkPassword(
          userMap.get(IFeatureFileCommonKeys.PASSWORD), userDetails[2].toString());
    }
  }

  private Object[] getUserByUsername(Map<String, String> userMap) {
    return (Object[])
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUserByUserName", userMap.get(IFeatureFileCommonKeys.USER));
  }

  public void createRoles(DataTable rolesDataTable) {
    List<Map<String, String>> rolesMaps = rolesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> roleMap : rolesMaps) {
      Object role = getRoleByName(roleMap);
      if (role == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQuery(
                "InsertRole", roleMap.get(IFeatureFileCommonKeys.ROLE));
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  public void assertThatRolesHaveBeenCreated(DataTable rolesDataTable) {
    List<Map<String, String>> rolesMaps = rolesDataTable.asMaps(String.class, String.class);
    Object role;
    for (Map<String, String> roleMap : rolesMaps) {
      role = getRoleByName(roleMap);
      assertNotNull(role);
    }
  }

  private Object getRoleByName(Map<String, String> roleMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getRoleByName", roleMap.get(IFeatureFileCommonKeys.ROLE));
  }

  public void assignUsersToParentRoles(DataTable usersAndRolesDataTable) {
    List<Map<String, String>> usersAndRolesMaps =
        usersAndRolesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> userAndRoleMap : usersAndRolesMaps) {
      Object userAndRole = getUserAndRoleAssignmentByUsernameAndRoleName(userAndRoleMap);
      if (userAndRole == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQuery(
                "assignUserToParentRole",
                userAndRoleMap.get(IFeatureFileCommonKeys.USER),
                userAndRoleMap.get(IFeatureFileCommonKeys.ROLE));
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  public void assertThatUsersHaveBeenAssignedToParentRoles(DataTable usersAndRolesDataTable) {
    List<Map<String, String>> usersAndRolesMaps =
        usersAndRolesDataTable.asMaps(String.class, String.class);
    Object userAndRoleAssignment;
    for (Map<String, String> userAndRoleMap : usersAndRolesMaps) {
      userAndRoleAssignment = getUserAndRoleAssignmentByUsernameAndRoleName(userAndRoleMap);
      assertNotNull(userAndRoleAssignment);
    }
  }

  private Object getUserAndRoleAssignmentByUsernameAndRoleName(Map<String, String> userAndRoleMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getUserAndRoleAssignmentByUsernameAndRoleName",
        userAndRoleMap.get(IFeatureFileCommonKeys.USER),
        userAndRoleMap.get(IFeatureFileCommonKeys.ROLE));
  }

  public void assignRolesToSubroles(DataTable rolesAndSubroles) {
    List<Map<String, String>> rolesMaps = rolesAndSubroles.asMaps(String.class, String.class);
    for (Map<String, String> roleMap : rolesMaps) {
      Object roleAssignment = getRoleAssignmentByParentRoleAndSubroleNames(roleMap);
      if (roleAssignment == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQuery(
                "assignParentRoleToSubrole",
                roleMap.get(IFeatureFileCommonKeys.ROLE),
                roleMap.get(IFeatureFileCommonKeys.SUBROLE));
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  public void assertThatParentRolesHasSubroles(DataTable rolesAndSubrolesTable) {
    List<Map<String, String>> rolesAndSubrolesMaps =
        rolesAndSubrolesTable.asMaps(String.class, String.class);
    Object roleAssignment;
    for (Map<String, String> roleAndSubroleMap : rolesAndSubrolesMaps) {
      roleAssignment = getRoleAssignmentByParentRoleAndSubroleNames(roleAndSubroleMap);
      assertNotNull(roleAssignment);
    }
  }

  private Object getRoleAssignmentByParentRoleAndSubroleNames(
      Map<String, String> roleAndSubroleMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getRoleAssignmentByParentRoleAndSubroleNames",
        roleAndSubroleMap.get(IFeatureFileCommonKeys.ROLE),
        roleAndSubroleMap.get(IFeatureFileCommonKeys.SUBROLE));
  }

  public void createEntityPermissions(String entitySysName, DataTable permissionsDataTable) {
    List<Map<String, String>> permissionsMaps =
        permissionsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> permissionMap : permissionsMaps) {
      String permissionExpression = getPermissionExpression(entitySysName, permissionMap);
      Object permission =
          getPermissionByEntityNameAndPermissionExpression(entitySysName, permissionExpression);
      if (permission == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQuery(
                "insertEntityPermission", entitySysName, permissionExpression);
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  public void assertThatEntityPermissionsExist(
      String entitySysName, DataTable permissionsDataTable) {
    List<Map<String, String>> permissionsMaps =
        permissionsDataTable.asMaps(String.class, String.class);
    Object permission;
    for (Map<String, String> permissionMap : permissionsMaps) {
      String permissionExpression = getPermissionExpression(entitySysName, permissionMap);
      permission =
          getPermissionByEntityNameAndPermissionExpression(entitySysName, permissionExpression);
      assertNotNull(permission);
    }
  }

  private Object getPermissionByEntityNameAndPermissionExpression(
      String entitySysName, String permissionExpression) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getPermissionByEntityNameAndPermissionExpression", entitySysName, permissionExpression);
  }

  public void createEntityConditions(String entitySysName, DataTable conditionsDataTable) {
    List<Map<String, String>> conditionsMaps =
        conditionsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> conditionMap : conditionsMaps) {
      Object condition =
          getConditionByEntityNameAndConditionExpression(entitySysName, conditionMap);
      if (condition == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQueryForRolesAndConditions(
                "insertEntityCondition",
                entitySysName,
                conditionMap.get(IFeatureFileCommonKeys.CONDITION));
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  public void assertThatEntityConditionsCreated(
      String entitySysName, DataTable conditionsDataTable) {
    List<Map<String, String>> conditionsMaps =
        conditionsDataTable.asMaps(String.class, String.class);
    Object condition;
    for (Map<String, String> conditionMap : conditionsMaps) {
      condition = getConditionByEntityNameAndConditionExpression(entitySysName, conditionMap);
      assertNotNull(condition);
    }
  }

  private Object getConditionByEntityNameAndConditionExpression(
      String entitySysName, Map<String, String> conditionMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getConditionByEntityNameAndConditionExpression",
        entitySysName,
        conditionMap.get(IFeatureFileCommonKeys.CONDITION));
  }

  public void assignRolesAndPermissionsAndConditions(
      String entitySysName, DataTable rolesPermissionsAndConditions) {
    List<Map<String, String>> rolesPermissionsAndConditionsMaps =
        rolesPermissionsAndConditions.asMaps(String.class, String.class);
    for (Map<String, String> rolePermissionAndConditionMap : rolesPermissionsAndConditionsMaps) {
      String permissionExpression =
          getPermissionExpression(entitySysName, rolePermissionAndConditionMap);
      Object rolePermissionAndConditionAssignment =
          getRolePermissionAndConditionAssignment(
              entitySysName, rolePermissionAndConditionMap, permissionExpression);
      if (rolePermissionAndConditionAssignment == null) {
        int numberOfInsertedRecords =
            entityManagerDatabaseConnector.executeInsertQueryForRolesAndConditions(
                "insertRolePermissionAndConditionAssignment",
                rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.SUBROLE),
                permissionExpression,
                entitySysName,
                getValue(rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.CONDITION)));
        assertTrue(numberOfInsertedRecords == 1);
      }
    }
  }

  private String getPermissionExpression(
      String entitySysName, Map<String, String> rolePermissionAndConditionMap) {
    return entitySysName
        + ":"
        + rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.PERMISSION);
  }

  public void assertThatRolesAndPermissionsAndConditionsAssignedSuccessfully(
      String entitySysName, DataTable rolesPermissionsAndConditions) {
    List<Map<String, String>> rolesPermissionsAndConditionsMaps =
        rolesPermissionsAndConditions.asMaps(String.class, String.class);
    for (Map<String, String> rolePermissionAndConditionMap : rolesPermissionsAndConditionsMaps) {
      String permissionExpression =
          getPermissionExpression(entitySysName, rolePermissionAndConditionMap);
      Object entity =
          getRolePermissionAndConditionAssignment(
              entitySysName, rolePermissionAndConditionMap, permissionExpression);
      assertNotNull(entity);
    }
  }

  private Object getRolePermissionAndConditionAssignment(
      String entitySysName,
      Map<String, String> rolePermissionAndConditionMap,
      String permissionExpression) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getRolePermissionAndConditionAssignment",
        rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.SUBROLE),
        permissionExpression,
        getValue(rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.CONDITION)),
        entitySysName,
        getValue(rolePermissionAndConditionMap.get(IFeatureFileCommonKeys.CONDITION)));
  }

  private String getValue(String value) {
    return value.trim().isEmpty() ? null : value;
  }
}
