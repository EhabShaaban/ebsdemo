package com.ebs.dda.accessrights;

import java.io.File;
import java.sql.PreparedStatement;

import com.ebs.dda.SpringBootRunner;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class AccessRightsInsertionStep extends SpringBootRunner implements En {

  private AccessRightsInsertionTestUtils accessRightsInsertionTestUtils;
  private static boolean executeOnceBeforeAll = false;

  public AccessRightsInsertionStep() {

    Given("^System admin is logged in$", () -> {});

    When(
        "^System admin wants to create the following users:$",
        (DataTable usersDatatable) -> {
          accessRightsInsertionTestUtils.createUsers(usersDatatable);
        });

    Then(
        "^the following users created successfully:$",
        (DataTable usersDatatable) -> {
          accessRightsInsertionTestUtils.assertUsersHaveBeenCreated(usersDatatable);
        });

    When(
        "^System admin wants to create the following roles :$",
        (DataTable rolesDataTable) -> {
          accessRightsInsertionTestUtils.createRoles(rolesDataTable);
        });

    Then(
        "^the following roles created successfully:$",
        (DataTable rolesDataTable) -> {
          accessRightsInsertionTestUtils.assertThatRolesHaveBeenCreated(rolesDataTable);
        });

    When(
        "^System admin wants to assign the following users to parent roles:$",
        (DataTable usersAndRolesDataTable) -> {
          accessRightsInsertionTestUtils.assignUsersToParentRoles(usersAndRolesDataTable);
        });

    Then(
        "^the following users are assigned to parent roles:$",
        (DataTable usersAndRolesDataTable) -> {
          accessRightsInsertionTestUtils.assertThatUsersHaveBeenAssignedToParentRoles(
              usersAndRolesDataTable);
        });

    When(
        "^System admin wants to assign the following parent roles to subroles:$",
        (DataTable rolesAndSubroles) -> {
          accessRightsInsertionTestUtils.assignRolesToSubroles(rolesAndSubroles);
        });

    Then(
        "^the following parent roles are assigned to subroles:$",
        (DataTable rolesAndSubRolesTable) -> {
          accessRightsInsertionTestUtils.assertThatParentRolesHasSubroles(rolesAndSubRolesTable);
        });

    When(
        "^System admin wants to create the following \"([^\"]*)\" entity permissions:$",
        (String entitySysName, DataTable permissionsDataTable) -> {
          accessRightsInsertionTestUtils.createEntityPermissions(
              entitySysName, permissionsDataTable);
        });

    Then(
        "^the following \"([^\"]*)\" entity permissions created successfully:$",
        (String entitySysName, DataTable permissionsDataTable) -> {
          accessRightsInsertionTestUtils.assertThatEntityPermissionsExist(
              entitySysName, permissionsDataTable);
        });

    When(
        "^System admin wants to create the following \"([^\"]*)\" entity conditions:$",
        (String entitySysName, DataTable conditionsDataTable) -> {
          accessRightsInsertionTestUtils.createEntityConditions(entitySysName, conditionsDataTable);
        });

    Then(
        "^the following \"([^\"]*)\" entity conditions created successfully:$",
        (String entitySysName, DataTable conditionsDataTable) -> {
          accessRightsInsertionTestUtils.assertThatEntityConditionsCreated(
              entitySysName, conditionsDataTable);
        });

    When(
        "^System admin wants to assign the following roles , permissions and conditions for \"([^\"]*)\" entity:$",
        (String entitySysName, DataTable rolesPermissionsAndConditions) -> {
          accessRightsInsertionTestUtils.assignRolesAndPermissionsAndConditions(
              entitySysName, rolesPermissionsAndConditions);
        });

    Then(
        "^the following roles , permissions and conditions assignment created successfully for \"([^\"]*)\" entity:$",
        (String entitySysName, DataTable rolesPermissionsAndConditions) -> {
          accessRightsInsertionTestUtils
              .assertThatRolesAndPermissionsAndConditionsAssignedSuccessfully(
                  entitySysName, rolesPermissionsAndConditions);
        });
  }

  @Before
  public void before() throws Exception {
    accessRightsInsertionTestUtils = new AccessRightsInsertionTestUtils();
    accessRightsInsertionTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    databaseConnector.createConnection();
    if (executeOnceBeforeAll == false) {
      File f = new File("accessrights.sql");
      if (f.exists()) {
        f.delete();
      }
      updateRoleSequence();
      updateUsersSequence();
      updateUserAccountSequence();
      updatePermissionExpression();
      executeOnceBeforeAll = true;
    }
  }

  @After
  public void after() throws Exception {
    databaseConnector.closeConnection();
  }

  private void updateRoleSequence() throws Exception {
    PreparedStatement preparedStatement =
        databaseConnector.createPreparedStatement(
            "SELECT setval('role_id_seq', (select max(id) from role) );");
    databaseConnector.executeQuery(preparedStatement);
  }

  private void updateUsersSequence() throws Exception {
    PreparedStatement updateUsersSeq =
        databaseConnector.createPreparedStatement(
            "SELECT setval('ebsuser_id_seq', (select max(id) from ebsuser) );");
    databaseConnector.executeQuery(updateUsersSeq);
  }

  private void updateUserAccountSequence() throws Exception {
    PreparedStatement updateUserAccountSeq =
        databaseConnector.createPreparedStatement(
            "SELECT setval('useraccount_id_seq', (select max(id) from useraccount) );");
    databaseConnector.executeQuery(updateUserAccountSeq);
  }

  private void updatePermissionExpression() throws Exception {
    PreparedStatement updateUserAccountSeq =
        databaseConnector.createPreparedStatement(
            "SELECT setval('permission_id_seq', (select max(id) from permission) );");
    databaseConnector.executeQuery(updateUserAccountSeq);
  }
}
