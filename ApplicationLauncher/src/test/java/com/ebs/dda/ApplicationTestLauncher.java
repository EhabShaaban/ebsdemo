package com.ebs.dda;

import com.ebs.dac.spring.configs.*;
import com.ebs.dda.config.CommandApplicationLauncher;
import com.ebs.dda.config.QueryApplicationLauncher;
import com.ebs.dda.config.VendorBeans;
import com.ebs.dda.config.inventory.goodsreceipt.DObGoodsReceiptCommandBeans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import javax.persistence.EntityManagerFactory;

@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class})
@Import({
  JPAConfiguration.class,
  CommandApplicationLauncher.class,
  QueryApplicationLauncher.class,
  ShiroHazelCastCacheConfig.class,
  ShiroConfig.class,
  WebConfig.class,
  CorsFilterBean.class,
  DObGoodsReceiptCommandBeans.class,
  AuthorizationConfig.class,
  VendorBeans.class,
  TestUtilsBeans.class
})
@EntityScan({
  "com.ebs.dac.security.jpa.entities",
  "com.ebs.dac.dbo.jpa.entities",
  "com.ebs.dda.dbo.jpa.entities",
  "com.ebs.dda.masterdata.dbo.jpa.entities",
  "com.ebs.dda.shipping.dbo.jpa.entities",
  "com.ebs.dda.purchases.dbo.jpa.entities",
  "com.ebs.dac.dbo.jpa.converters",
  "com.ebs.dda.approval.dbo.jpa.entities",
  "com.ebs.dda.accounting.paymentrequest.dbo.jpa.entites",
  "com.ebs.dda.accounting.dbo.jpa.entities",
  "com.ebs.dda.accounting.journalentry.dbo.jpa.entities",
  "com.ebs.dda.dbo.jpa.entities.lookups",
  "com.ebs.dda.sales.dbo.jpa.entities",
  "com.ebs.dda.notesreceivables.dbo.jpa.entities",
  "com.ebs.dda.jpa.accounting.collection",
  "com.ebs.dda.jpa",
})
@ComponentScan("rest")
public class ApplicationTestLauncher {

  private static final Logger log = LoggerFactory.getLogger(ApplicationTestLauncher.class);

  public static void main(String[] args) {
    log.info(
        ""
            + SpringApplication.run(ApplicationTestLauncher.class, args)
                .getBean(ShiroConfig.FilterChainDefinitionInterceptor.class)
                .getMap());
  }

  @Bean("entityManagerDatabaseConnector")
  public EntityManagerDatabaseConnector createConnector(EntityManagerFactory entityManagerFactory) {
    return new EntityManagerDatabaseConnector(entityManagerFactory);
  }
}
