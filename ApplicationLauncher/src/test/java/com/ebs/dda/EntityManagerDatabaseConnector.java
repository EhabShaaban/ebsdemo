package com.ebs.dda;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.eclipse.persistence.jpa.JpaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityManagerDatabaseConnector {

  Logger logger = LoggerFactory.getLogger(EntityManagerDatabaseConnector.class);
  private EntityManagerFactory entityManagerFactory;

  public EntityManagerDatabaseConnector(EntityManagerFactory entityManagerFactory2) {
    this.entityManagerFactory = entityManagerFactory2;
  }

  public List executeNativeNamedQuery(String queryName, Object... params) {
    Query nativeQuery = getNativeQuery(queryName, params);
    return nativeQuery.getResultList();
  }

  public Object executeNativeNamedQuerySingleResult(String queryName, Object... params) {
    try {
      Query nativeQuery = getNativeQuery(queryName, params);
      return nativeQuery.getSingleResult();
    } catch (NoResultException e) {
      return null;
    }
  }

  private Query getNativeQuery(String queryName, Object[] params) {
    EntityManager em = entityManagerFactory.createEntityManager();
    Query nativeQuery = em.createNamedQuery(queryName);
    IntStream.range(0, params.length)
        .forEach(index -> nativeQuery.setParameter(index + 1, params[index]));
    return nativeQuery;
  }

  public int executeInsertQueryForRolesAndConditions(String queryName, Object... params) {
    EntityManager em = entityManagerFactory.createEntityManager();
    Query nativeQuery = em.createNamedQuery(queryName);
    IntStream.range(0, params.length)
        .forEach(index -> nativeQuery.setParameter(index + 1, params[index]));

    em.getTransaction().begin();
    int executeUpdate = nativeQuery.executeUpdate();
    em.getTransaction().commit();
    IntStream.range(0, params.length)
        .forEach(
            index -> {
              if (params[index] != null && params[index].toString().contains("'")) {
                String condition = params[index].toString().replace("'", "''");
                params[index] = condition;
              }
            });

    StringBuilder nativeQueryString = getStringQuery(nativeQuery, params);
    writeQueriesToFile(nativeQueryString);

    em.close();

    return executeUpdate;
  }

  public int executeInsertQuery(String queryName, Object... params) {
    EntityManager em = entityManagerFactory.createEntityManager();
    Query nativeQuery = em.createNamedQuery(queryName);
    IntStream.range(0, params.length)
        .forEach(index -> nativeQuery.setParameter(index + 1, params[index]));

    em.getTransaction().begin();
    int executeUpdate = nativeQuery.executeUpdate();
    em.getTransaction().commit();
    StringBuilder nativeQueryString = getStringQuery(nativeQuery, params);
    writeQueriesToFile(nativeQueryString);

    em.close();

    return executeUpdate;
  }

  public void executeInsertQueryForObject(String queryName, Object... params) {
    EntityManager em = entityManagerFactory.createEntityManager();
    Query nativeQuery = em.createNamedQuery(queryName);
    IntStream.range(0, params.length)
        .forEach(index -> nativeQuery.setParameter(index + 1, params[index]));
    em.getTransaction().begin();
    nativeQuery.executeUpdate();
    em.getTransaction().commit();
    em.close();
  }

  private StringBuilder getStringQuery(Query nativeQuery, Object... params) {
    StringBuilder nativeQueryString =
        new StringBuilder(nativeQuery.unwrap(JpaQuery.class).getDatabaseQuery().getSQLString());
    for (int i = 0; i < params.length; i++) {
      int paramIndex = nativeQueryString.indexOf("?");
      String parameterValue = null;
      if (params[i] != null) {
        parameterValue = "'" + params[i].toString() + "'";
      } else {
        parameterValue = "null";
      }
      nativeQueryString.replace(paramIndex, paramIndex + 1, parameterValue);
    }
    return nativeQueryString;
  }

  private void writeQueriesToFile(StringBuilder nativeQueryString) {
    BufferedWriter bufferedWriter;

    try {
      bufferedWriter = new BufferedWriter(new FileWriter("accessrights.sql", true));
      bufferedWriter.append(nativeQueryString);
      bufferedWriter.append("\n");
      bufferedWriter.close();
    } catch (IOException e) { // TODO Auto-generated catch block
    }
  }
}
