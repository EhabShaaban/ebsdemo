package com.ebs.dda;

public interface IGeneralRestURLs {
	String BASE_URL = "/services";
	String DOCUMENT_OWNERS = BASE_URL + "/documentowners";
	String FISCAL_YEARS = BASE_URL + "/fiscalyear";
	String ACTIVE_FISCAL_YEARS = FISCAL_YEARS + "/activefiscalyear/v2";
	String GET_ALL_FISCAL_YEARS = FISCAL_YEARS + "/";
	String GET_ALL_ACTIVE_FISCAL_YEARS = ACTIVE_FISCAL_YEARS + "/";


}
