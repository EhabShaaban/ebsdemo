package com.ebs.dda;

public interface IFeatureFileCommonKeys {

  String CODE = "Code";
  String PO_CODE = "POCode";
  String GR_CODE = "GRCode";
  String NAME = "Name";
  String LAST_UPDATED_BY = "LastUpdatedBy";
  String LAST_UPDATE_DATE = "LastUpdateDate";
  String CREATED_BY = "CreatedBy";
  String CREATION_DATE = "CreationDate";
  String TYPE = "Type";
  String STATE = "State";
  String PURCHASING_UNIT = "PurchasingUnit";
  String PURCHASE_UNIT = "PurchaseUnit";
  String PURCHASE_UNIT_NAME = "PurchaseUnitName";
  String SYMBOL = "Symbol";
  String IVR_CODE = "IVRCode";
  String ITEM_CODE = "ItemCode";
  String ITEM_NAME = "ItemName";
  String ITEM_QUANTITY_IN_DN = "QtyInDN";
  String ITEM_BASE_UNIT = "Base";
  String ITEM_ORDER_UNIT = "OrderUnit";
  String IS_BATCH_MANAGED = "IsBatchManaged";
  String QUANTITY = "Qty";
  String QUANTITY_ID = "QtyID";
  String BATCH_ID = "BatchID";
  String GROSS_UNIT_PRICE = "UnitPrice(Gross)";
  String DISCOUNT = "Discount";
  String ITEM_CODE_AT_VENDOR = "ItemCodeAtVendor";
  String COPIES = "Copies";
  String SHIPPING_INSTRUCTIONS = "ShippingInstructions";
  String PURCHASING_RESPONSIBLE = "PurchasingResponsible";
  String PURCHASE_ORDER = "PurchaseOrder";
  String STOREHOUSE = "Storehouse";
  String STOREKEEPER = "Storekeeper";
  String VENDOR = "Vendor";
  String VENDOR_CODE = "VendorCode";
  String VENDOR_NAME = "VendorName";
  String ITEM = "Item";
  String ITEM_VENDOR_CODE = "ItemVendorCode";

  String PLANT = "Plant";

  String FIELD_NAME = "FieldName";
  String OPERATION = "Operation";
  String VALUE = "Value";

  String ROLE = "Role";
  String SUBROLE = "Subrole";
  String PERMISSION = "Permission";
  String CONDITION = "Condition";
  String USER = "User";
  String PASSWORD = "Password";
  String PROFILE_NAME = "ProfileName";
  String POSTING_DATE = "PostingDate";
  String MISSING_FIELDS = "MissingFields";
  String SECTION_NAME = "SectionName";

  String CONTROL_POINT = "ControlPoint";
  String IS_MAIN = "IsMain";
  String DOCUMENT_TYPE = "DocumentType";

  String DEFECTS_DESCRIPTION = "DefectDescription";
  String DEFECTS_REASON = "DefectReason";
  String STOCK_TYPE = "StockType";
  String BATCH_NUMBER = "BatchNo";
  String PRODUCTION_DATE = "ProdDate";
  String EXPIRATION_DATE = "ExpireDate";
  String BATCH_QUANTITY_UOE = "BatchQty(UoE)";
  String RECEIVED_QTY_UOE = "ReceivedQty(UoE)";
  String UOE = "UoE";
  String TOTAL_RECEIVED_QTY_BASE = "TotalReceivedQty(Base)";

  String DIFFERENCE_REASON = "DifferenceReason";
  String PURCHASING_UNIT_CODE = "PurchaseUnitCode";
  String PURCHASING_UNIT_NAME = "PurchaseUnitName";

  String COMPANY_CODE = "CompanyCode";
  String COMPANY = "Company";
  String BANK_CODE = "BankCode";
  String BANK = "Bank";
  String ACCOUNT_NUMBER = "AccountNumber";
  String BANK_DETAILS_ID = "BankDetailsId";
  String BANK_ACCOUNT_ID = "BankAccountId";
  String CURRENCY_CODE = "CurrencyCode";
  String BUSINESS_UNIT = "BusinessUnit";

  String CUSTOMER = "Customer";
  String GOODS_STATE = "GoodsState";
  String TOTAL_REMAINING = "TotalRemaining";

  String QTY_ORDER_UNIT = "Qty(OrderUnit)";
  String QTY_BASE_UNIT = "Qty(Base)";
  String ITEM_GROSS_COST = "ItemGrossCost";
  String PAYMENT_TERMS = "PaymentTerms";
  String PRICE = "Price";
  String STORE_TRANSACTION_CODE = "StoreTransactionCode";
  String UOM = "UOM";
  String QTY = "Qty";
  String ESTIMATE_COST = "EstimateCost";
  String TRANSACTION_TYPE = "TransactionType";
  String ACTUAL_COST = "ActualCost";
  String REFERENCE_DOCUMENT = "ReferenceDocument";
  String REMAINING_QTY = "RemainingQty";
  String REF_TRANSACTION = "RefTransaction";

  String STORE = "Store";
  String CURRENCY = "Currency";
  String DELIVERY_NOTE_PL = "DeliveryNote/PL";
  String GOODS_RECEIPT_CODE = "GoodsReceiptCode";
  String VENDOR_INVOICE = "VendorInvoice";
  String VENDOR_INVOICE_CODE = "VendorInvoiceCode";
  String BASE_UNIT = "BaseUnit";

  String PRODUCT_MANAGER = "ProductManager";
  String ID = "Id";
  String DOCUMENT_OWNER = "DocumentOwner";
  String DOCUMENT_OWNER_ID = "DocumentOwnerId";
  String REASON = "Reason";

  String BUSINESS_OBJECT = "BusinessObject";
  String REFERENCE_PO = "ReferencePO";
  String BANK_ACCOUNT_NUMBER = "BankAccountNumber";
  String TREASURY = "Treasury";

  String OBJECT_TYPE_CODE = "ObjectTypeCode";
  String ACTIVATION_DATE = "ActivationDate";
  String ACTIVATED_BY = "ActivatedBy";

  String PI_REQUEST_DATE = "PIRequestDate";
  String CONFIRMATION_DATE = "ConfirmationDate";
  String PRODUCTION_FINISHED_DATE = "ProductionFinishedDate";
  String SHIPPING_DATE = "ShippingDate";
  String ARRIVAL_DATE = "ArrivalDate";
  String CLEARANCE_DATE = "ClearanceDate";
  String DELIVERY_COMPLETE_DATE = "DeliveryCompleteDate";

  String CYCLE_NUMBER = "CycleNumber";
  String START_DATE = "StartDate";
  String END_DATE = "EndDate";
  String FINAL_DECISION = "FinalDecision";

  String FROM = "From";
  String TO = "To";
  String IS_STANDARD = "IsStandard";
  String ATTACHMENT = "Attachment";
  String BALANCE = "Balance";
  String AVAILABLE_QUANTITY = "AvailableQuantity";

  String FORM = "Form";

}
