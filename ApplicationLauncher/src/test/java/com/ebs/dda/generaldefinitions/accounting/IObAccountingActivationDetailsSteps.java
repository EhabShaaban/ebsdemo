package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import org.springframework.beans.factory.InitializingBean;

public class IObAccountingActivationDetailsSteps extends SpringBootRunner
    implements En, InitializingBean {

  private AccountingDocumentCommonTestUtils utils;

  public IObAccountingActivationDetailsSteps() {
    Given(
        "^the following ActivationDetails for AccountingDocument with DocumentType \"([^\"]*)\" exist:$",
        (String documentType, DataTable activationDetailsDataTable) -> {
          utils.assertThatAccountingDocumentHasThoseActivationDetails(
              documentType, activationDetailsDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new AccountingDocumentCommonTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
