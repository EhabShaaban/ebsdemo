package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObCollectionCommonSteps extends SpringBootRunner implements En {

  private DObCollectionTestUtils utils;

  public DObCollectionCommonSteps() {
    Given("^the following GeneralData for Collections exist:$", (DataTable generalDataTable) -> {
      utils.createCollectionGeneralData(generalDataTable);
    });

    And("^the following CompanyData for Collections exist:$", (DataTable companyDataTable) -> {
      utils.createCollectionCompanyData(companyDataTable);
    });

    And("^the following CollectionDetails for Collections exist:$",
        (DataTable collectionDetailsDataTable) -> {
          utils.createCollectionDetailsData(collectionDetailsDataTable);
        });

    And("^insert AccountingDetails for Collections as follow:$",
        (DataTable collectionAccountingDetails) -> {
          utils.createCollectionAccountingDetails(collectionAccountingDetails);
        });

  }

  @Before
  public void setup() throws Exception {
    utils = new DObCollectionTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
