package com.ebs.dda.generaldefinitions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.CobCurrenciesTestUtils;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IGeneralRestURLs;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCBEUpdateTestUtils;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCommonTestUtils;
import com.ebs.dda.masterdata.itemgroup.CObItemGroupFeatureTestUtils;
import com.ebs.dda.masterdata.measures.utils.CObMeasuresCommonTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CommonBackgroundSteps extends SpringBootRunner implements En, InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonBackgroundSteps.class);

	private CommonTestUtils commonTestUtils;
	private CobCurrenciesTestUtils currenciesTestUtils;
	private CObMeasuresCommonTestUtils measuresTestUtils;
	private CObItemGroupFeatureTestUtils itemGroupFeatureTestUtils;
	private CObExchangeRateCommonTestUtils exchangeRateCommonTestUtils;
	private CObExchangeRateCBEUpdateTestUtils exchangeRateCBEUpdateTestUtils;

	public CommonBackgroundSteps() {

		Given("^the following users and roles exist:$", (DataTable usersDataTable) -> {
			commonTestUtils.assertThatUsersWithRolesExist(usersDataTable);
		});

		Given("^the following roles and sub-roles exist:$", (DataTable rolesAndSubRolesTable) -> {
			commonTestUtils.assertThatRolesAndSubRolesExist(rolesAndSubRolesTable);
		});

		Given("^the following sub-roles and permissions exist:$",
						(DataTable permissionDataTable) -> {
							commonTestUtils.assertThatRolesHavePermissionsAndConditions(
											permissionDataTable);
						});

		Given("^edit session is \"([^\"]*)\" minutes$", (String editSessionTimeout) -> {
			int sessionTime = Integer.parseInt(editSessionTimeout);
			commonTestUtils.assertThatEditSessionTimeIsCorrect(sessionTime);
		});

		Given("^the following Currencies exist:$", (DataTable currencyDataTable) -> {
			currenciesTestUtils.assertThatCurrenciesExist(currencyDataTable);
		});

		Given("^the following PurchasingUnits exist:$", (DataTable purchasingUnitsTable) -> {
			commonTestUtils.assertThatPurchaseUnitsExist(purchasingUnitsTable);
		});

		Given("^the following BusinessUnits exist:$", (DataTable businessUnitsDataTable) -> {
			commonTestUtils.assertThatPurchaseUnitsExist(businessUnitsDataTable);
		});

		And("^the following Banks exist:$", (DataTable bankTable) -> {
			commonTestUtils.assertThatBanksExist(bankTable);
		});

		And("^the following Treasuries exist:$", (DataTable treasuriesTable) -> {
			commonTestUtils.assertThatTreasuriesExist(treasuriesTable);
		});

		And("^the following Companies Bank Accounts exist:$", (DataTable companyBanks) -> {
			commonTestUtils.assertThatCompaniesHaveBankAccounts(companyBanks);
		});

		Given("^the following users doesn't have the following permissions:$",
						(DataTable permissionDataTable) -> {
							commonTestUtils.assertThatUserHaveNoPermissions(permissionDataTable);
						});

		Given("^the following users have the following permissions without the following conditions:$",
						(DataTable permissionDataTable) -> {
							commonTestUtils.assertThatUserHavePermissionsWithoutCondition(
											permissionDataTable);
						});

		Given("^the following Measures exist:$", (DataTable measuresDataTable) -> {
			measuresTestUtils.assertMeasureExistsByCodeAndSymbolAndName(measuresDataTable);
		});

		Given("^the following ItemGroups exist:$", (DataTable itemGroupTable) -> {
			itemGroupFeatureTestUtils.assertItemGroupsExist(itemGroupTable);
		});
		Given("^the following DocumentOwners exist:$", (DataTable documentOwnerDataTable) -> {
			commonTestUtils.assertThatDocumentOwnersAreExist(documentOwnerDataTable);
		});
		And("^the following BankAccounts exist for the following companies:$",
						(DataTable companyBanks) -> {
							commonTestUtils.assertThatCompaniesHaveBankAccountsWithIDs(
											companyBanks);
						});

		Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\" and total number of returned allowed actions is \"([^\"]*)\"$",
						(String allowedActions, String userName, Integer noOfActions) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							commonTestUtils.assertThatAllowedActionsResponseIsCorrect(response,
											allowedActions, noOfActions);
						});
		Then("^the following DocumentOwners values will be presented to \"([^\"]*)\":$",
						(String userName, DataTable documentOwnersDT) -> {
							commonTestUtils.assertThatDocumentOwnersAreRetrieved(
											userActionsTestUtils.getUserResponse(userName),
											documentOwnersDT);
						});

		Given("^the total number of existing \"([^\"]*)\" DocumentOwners are (\\d+)$",
						(String objectSysName, Integer totalNumber) -> {
							commonTestUtils.assertThatTotalNumberOfDocumentOwnersIsCorrect(
											totalNumber, objectSysName);
						});

		When("^\"([^\"]*)\" requests to read all DocumentOwners for \"([^\"]*)\"$",
						(String userName, String objectSysName) -> {
							String url = IGeneralRestURLs.DOCUMENT_OWNERS + '/'
											+ objectSysName.toLowerCase();
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = commonTestUtils.sendGETRequest(cookie, url);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		Given("^the following are ALL existing ExchangeRates records from \"([^\"]*)\" to \"([^\"]*)\":$",
						(String FirstCurrencyIso, String SecondCurrencyIso,
										DataTable erDataTable) -> {
							exchangeRateCommonTestUtils
											.assertThatTheFollowingExchangeRatesExist(erDataTable);
						});
		Given("^the number of existing ExchangeRates records from \"([^\"]*)\" to \"([^\"]*)\" is (\\d+)$",
						(String FirstCurrencyIso, String SecondCurrencyIso,
										Integer expectedERCount) -> {
							exchangeRateCommonTestUtils.assertTotalNumberOfExchangeRatesExist(
											expectedERCount, FirstCurrencyIso, SecondCurrencyIso);
						});
		Given("^there is no existing ExchangeRates records from \"([^\"]*)\" to \"([^\"]*)\"$",
						(String FirstCurrencyIso, String SecondCurrencyIso) -> {
							exchangeRateCommonTestUtils.assertNoExchangeRatesExist(FirstCurrencyIso,
											SecondCurrencyIso);
						});

		Given("^the following Exchange Rates exist:$", (DataTable erDataTable) -> {
			exchangeRateCommonTestUtils.assertThatTheFollowingExchangeRatesExist(erDataTable);
		});
		Given("^the following ExchangeRate strategies exists:$", (DataTable strategiesDT) -> {
			exchangeRateCBEUpdateTestUtils.assertThatExchangeRateEntryStrategies(strategiesDT);
		});

		Given("^the following is the latest ExchangeRate record from \"([^\"]*)\" to \"([^\"]*)\" based on default strategy:$",
						(String firstCurrency, String secondCurrency,
										DataTable latestExchangeRateDataTable) -> {
							exchangeRateCBEUpdateTestUtils
											.assertThatLatestExchangeRateOfDefaultStrategy(
															firstCurrency, secondCurrency,
															latestExchangeRateDataTable);
						});
		And("^the following ExchangeRates records exit:$", (DataTable latestExchangeRateDataTable) -> {
			exchangeRateCBEUpdateTestUtils.createExchangeRateRecords(latestExchangeRateDataTable);
		});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		commonTestUtils = new CommonTestUtils();
		commonTestUtils.setProperties(getProperties());
		commonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		currenciesTestUtils = new CobCurrenciesTestUtils(entityManagerDatabaseConnector);
		measuresTestUtils = new CObMeasuresCommonTestUtils(getProperties());
		measuresTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		itemGroupFeatureTestUtils = new CObItemGroupFeatureTestUtils(getProperties());
		itemGroupFeatureTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		exchangeRateCommonTestUtils = new CObExchangeRateCommonTestUtils(getProperties());
		exchangeRateCommonTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		exchangeRateCBEUpdateTestUtils = new CObExchangeRateCBEUpdateTestUtils(getProperties(),
						entityManagerDatabaseConnector);
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		LOGGER.info("\n======================================================\n"
						+ "Running Feature: " + scenario.getId() + "\n" + "Running Scenario: "
						+ scenario.getName()
						+ "\n======================================================\n");
		databaseConnector.createConnection();
	}

	@After
	public void after() throws Exception {
		databaseConnector.closeConnection();
	}
}
