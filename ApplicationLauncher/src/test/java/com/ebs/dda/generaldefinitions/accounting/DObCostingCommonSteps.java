package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.DObCostingCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObCostingCommonSteps extends SpringBootRunner implements En {
  private DObCostingCommonTestUtils utils;

  public DObCostingCommonSteps() {

    Given(
        "^insert the following Costing GeneralData:$",
        (DataTable costingDocument) -> {
          utils.insertCosting(costingDocument);
        });

    Given(
        "^insert the following Costing CompanyData:$",
        (DataTable costingCompanyData) -> {
          utils.insertCostingCompanyData(costingCompanyData);
        });

    Given(
        "^insert the following Costing Details:$",
        (DataTable costingDetails) -> {
          utils.insertCostingDetails(costingDetails);
        });

    Given(
        "^insert the following Costing AccountingDetails:$",
        (DataTable costingAccountingDetails) -> {
          utils.insertCostingAccountingDetails(costingAccountingDetails);
        });

    Given(
        "^insert the following Costing Items:$",
        (DataTable costingItemsDataTable) -> {
          utils.insertCostingItems(costingItemsDataTable);
        });

    And(
        "^insert the following Costing ActivationDetails:$",
        (DataTable grAccountingDocumentActivationDetails) -> {
          utils.insertCostingActivationDetails(grAccountingDocumentActivationDetails);
        });

    Then(
        "^the Costing Details of Costing Document is (Created|Updated) As Follows:$",
        (String operationType, DataTable costingDetailsDataTable) -> {
          utils.assertCostingDetails(operationType, costingDetailsDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObCostingCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
