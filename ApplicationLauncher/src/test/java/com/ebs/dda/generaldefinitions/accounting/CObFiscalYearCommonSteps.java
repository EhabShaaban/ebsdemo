package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.fiscalyear.utils.CObFiscalYearTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class CObFiscalYearCommonSteps extends SpringBootRunner implements En {

  private CObFiscalYearTestUtils utils;

  public CObFiscalYearCommonSteps() {
    Given(
        "^the following FiscalYears exist:$",
        (DataTable fiscalPeriodTable) -> {
          utils.createFiscalYear(fiscalPeriodTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new CObFiscalYearTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
