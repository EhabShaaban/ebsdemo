package com.ebs.dda.generaldefinitions;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.IVRRestUrls;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCommonTestUtils;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class ItemVendorRecordCommonSteps extends SpringBootRunner implements En {

  private CommonTestUtils commonTestUtils;
  private ItemVendorRecordCommonTestUtils utils;

  public ItemVendorRecordCommonSteps() {

    Given(
        "^\"([^\"]*)\" first deleted the IVR with code \"([^\"]*)\" successfully$",
        (String username, String ivrCode) -> {
          String deleteUrl = IVRRestUrls.DELETE_URL + ivrCode;
          Response response =
              commonTestUtils.sendDeleteRequest(
                  userActionsTestUtils.getUserCookie(username), deleteUrl);
          userActionsTestUtils.setUserResponse(username, response);
          commonTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deleted the IVR with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String username, String ivrCode, String dateTime) -> {
          commonTestUtils.freeze(dateTime);
          String deleteUrl = IVRRestUrls.DELETE_URL + ivrCode;
          Response response =
              commonTestUtils.sendDeleteRequest(
                  userActionsTestUtils.getUserCookie(username), deleteUrl);
          userActionsTestUtils.setUserResponse(username, response);
          commonTestUtils.assertResponseSuccessStatus(response);
        });
      And("^insert the following ItemVendorRecords GeneralData exist:$", (DataTable itemVendorRecordsDataTable) -> {
          utils.insertItemVendorRecords(itemVendorRecordsDataTable);
      });
  }

  @Before
  public void setup() throws Exception {
    commonTestUtils = new CommonTestUtils();
    commonTestUtils.setProperties(getProperties());
    commonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	utils = new ItemVendorRecordCommonTestUtils(getProperties());
	utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

}
