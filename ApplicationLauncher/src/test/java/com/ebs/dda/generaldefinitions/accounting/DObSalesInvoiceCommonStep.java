package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObSalesInvoiceCommonStep extends SpringBootRunner implements En {

  private DObSalesInvoiceTestUtils utils;

  public DObSalesInvoiceCommonStep() {
    Given(
        "^the following GeneralData for SalesInvoices exist:$",
        (DataTable generalDataTable) -> {
          utils.createSalesInvoiceGeneralData(generalDataTable);
        });

    Given(
        "^the following Company for SalesInvoices exist:$",
        (DataTable dataTable) -> {
          utils.createSalesInvoiceCompany(dataTable);
        });

    Given(
        "^the following Details for SalesInvoices exist:$",
        (DataTable businessPartnerTable) -> {
          utils.createSalesInvoiceCustomerBusinessPartner(businessPartnerTable);
        });

    Given(
        "^the following Items for SalesInvoices exist:$",
        (DataTable itemTable) -> {
          utils.createSalesInvoiceItem(itemTable);
        });
    And(
        "^the following SalesInvoices TaxDetails exist:$",
        (DataTable taxesDataTable) -> {
          utils.createSalesInvoiceTaxes(taxesDataTable);
        });
    And(
        "^the following SalesInvoice Remaining exist:$",
        (DataTable summaryDataTable) -> {
          utils.createSalesInvoiceSummaryRemaining(summaryDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObSalesInvoiceTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
