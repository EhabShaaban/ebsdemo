package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.taxes.utils.LObTaxesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;

public class LObTaxesCommonSteps extends SpringBootRunner implements En {

    private LObTaxesTestUtils utils;

    public LObTaxesCommonSteps() {
        Given("^the Vendor \"([^\"]*)\" has the following Taxes:$",
                (String vendorCodeName, DataTable vendorTaxesDataTable) -> {
                    utils.assertThatTheFollowingVendorTaxesExist(vendorCodeName,
                            vendorTaxesDataTable);
                });

        Given("^the Company \"([^\"]*)\" has the following Taxes:$", (String companyCodeName, DataTable taxesDataTable) -> {
            utils.assertThatTheFollowingCompanyTaxDetailsExist(companyCodeName,
                    taxesDataTable);
        });

        Given("^the Company \"([^\"]*)\" has no Taxes:$", (String companyCodeName) -> {
            utils.assertThatTheFollowingCompanyHasNoTaxDetails(companyCodeName);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new LObTaxesTestUtils(getProperties(), entityManagerDatabaseConnector);
    }
}
