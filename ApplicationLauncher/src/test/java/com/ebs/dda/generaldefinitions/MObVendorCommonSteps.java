package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObVendorCommonSteps extends SpringBootRunner implements En {

  private MObVendorCommonTestUtils vendorCommonTestUtils;

  public MObVendorCommonSteps() {

    Given(
        "^\"([^\"]*)\" first deleted the Vendor with code \"([^\"]*)\" successfully$",
        (String userName, String vendorCode) -> {
          Response vendorDeleteResponse =
              vendorCommonTestUtils.deleteVendorByCode(
                  userActionsTestUtils.getUserCookie(userName), vendorCode);
          userActionsTestUtils.setUserResponse(userName, vendorDeleteResponse);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Vendor with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String vendorCode, String dateTime) -> {
          vendorCommonTestUtils.freeze(dateTime);
          Response vendorDeleteResponse =
              vendorCommonTestUtils.deleteVendorByCode(
                  userActionsTestUtils.getUserCookie(userName), vendorCode);
          userActionsTestUtils.setUserResponse(userName, vendorDeleteResponse);
        });

    Given(
        "^the following Vendors have the following Taxes:$",
        (DataTable vendorTaxesDataTable) -> {
          vendorCommonTestUtils.assertThatVendorsHaveTheFollowingTaxes(vendorTaxesDataTable);
        });

    Given(
        "^the following GeneralData for Vendor exist:$",
        (DataTable generalDataTable) -> {
          vendorCommonTestUtils.createVendorGeneralData(generalDataTable);
        });

    Given(
        "^the following BusinessUnits for Vendor exist:$",
        (DataTable businessUnitsTable) -> {
          vendorCommonTestUtils.createVendorBusinessUnits(businessUnitsTable);
        });
  }

  @Before
  public void setup() throws Exception {
    vendorCommonTestUtils = new MObVendorCommonTestUtils(getProperties());
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

}
