package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.Journalbalance.utils.CObJournalBalanceTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class CObJournalBalanceCommonSteps extends SpringBootRunner implements En {

  private CObJournalBalanceTestUtils utils;

  public CObJournalBalanceCommonSteps() {
    And("^insert the following BankJournalBalances:$", (DataTable bankJournalDataTable) -> {
      utils.insertBankJournalBalance(bankJournalDataTable);
    });
    And("^insert the following TreasuryJournalBalances:$", (DataTable treasuryJournalDataTable) -> {
      utils.insertTreasuryJournalBalance(treasuryJournalDataTable);
    });

    Then("^JournalBalance with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String journalBalanceCode, String updatedBalance) -> {
          utils.assertThatJournalBalanceIsUpdated(journalBalanceCode, updatedBalance);
        });

  }

  @Before
  public void setup() throws Exception {
    utils = new CObJournalBalanceTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
