package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObMonetaryNoteCommonSteps extends SpringBootRunner implements En {

  private DObMonetaryNotesCommonTestUtils utils;

  public DObMonetaryNoteCommonSteps() {
    Given(
        "^Insert the following GeneralData for MonetaryNotes:$",
        (DataTable monetaryNoteTable) -> {
          utils.createMonetaryNoteGeneralData(monetaryNoteTable);
        });

    Given(
        "^Insert the following Company for MonetaryNotes:$",
        (DataTable monetaryNoteTable) -> {
          utils.createMonetaryNoteCompanyData(monetaryNoteTable);
        });

    Given(
        "^Insert the following NotesDetails for MonetaryNotes:$",
        (DataTable monetaryNoteTable) -> {
          utils.createMonetaryNoteDetails(monetaryNoteTable);
        });
    Given(
        "^Insert the following AccountingDetails for MonetaryNotes:$",
        (DataTable monetaryNoteTable) -> {
          utils.createMonetaryNoteAccountingDetails(monetaryNoteTable);
        });
    Given(
        "^the following MonetaryNotes have no AccountingDetails:$",
        (DataTable monetaryNoteTable) -> {
          utils.assertThatMonetaryNotesDoesNotHaveAccountingDetails(monetaryNoteTable);
        });
    Given(
        "^Insert the following ReferenceDocuments for MonetaryNotes:$",
        (DataTable monetaryNoteTable) -> {
          utils.createMonetaryNoteReferenceDocuments(monetaryNoteTable);
        });
    Given(
        "^the following MonetaryNotes have no ReferenceDocuments:$",
        (DataTable monetaryNoteTable) -> {
          utils.assertThatMonetaryNoteHasNoReferenceDocuments(monetaryNoteTable);
        });

    And("^Insert the following MonetaryNotes ActivationDetails exist:$", (DataTable activationDetails) -> {
        utils.createMonetaryNoteActivationDetails(activationDetails);
    });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObMonetaryNotesCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
