package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceDropDownStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Read All SalesInvoices Dropdown";

  private DObSalesInvoiceDropDownTestUtils utils;

  public DObSalesInvoiceDropDownStep() {
    Given("^the following SalesInvoices exist for dropdown:$",
        (DataTable salesInvoiceDataTable) -> {
          utils.assertThatTheFollowingSalesInvoicesExist(salesInvoiceDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all Opened and Active SalesInvoices where remaining is greater than Zero according to user’s BusinessUnit$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.getReadAllSalesInvoicesUrl());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read all Active SalesInvoices of businessUnit with code \"([^\"]*)\" and customer with code \"([^\"]*)\"$",
        (String userName, String businessUnitCode, String customerCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie,
              utils.getReadAllSalesInvoicesUrl(businessUnitCode, customerCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^the following SalesInvoices values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesInvoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatSalesInvoicesResponseIsCorrect(salesInvoicesDataTable, response);
        });

    Then("^total number of SalesInvoices returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsInResponseIsCorrect(response, numberOfRecords);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceDropDownTestUtils(properties, entityManagerDatabaseConnector);

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.getDbScriptsToClearInsertionDependancies());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
