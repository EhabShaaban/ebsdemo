package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;

public class DObSalesOrderCommonStep extends SpringBootRunner implements En {

  private DObSalesOrderDeleteTestUtils utils;

  public DObSalesOrderCommonStep() {

    Given("^\"([^\"]*)\" first deleted the SalesOrder with code \"([^\"]*)\" successfully$",
        (String userName, String salesOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesOrderRestUrl(salesOrderCode);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatSalesOrderIsDeletedSuccessfully(salesOrderCode);
        });

    Given(
        "^\"([^\"]*)\" first deleted the SalesOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String salesOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesOrderRestUrl(salesOrderCode);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatSalesOrderIsDeletedSuccessfully(salesOrderCode);
        });

    Given("^the following GeneralData for SalesOrders exist:$", (DataTable generalDataTable) -> {
      utils.createSalesOrderGeneralData(generalDataTable);
    });

    Given("^the following Company for SalesOrders exist:$", (DataTable companyDataTable) -> {
      utils.createSalesOrderCompanyData(companyDataTable);
    });

    Given("^the following SalesOrderData for SalesOrders exist:$",
        (DataTable salesOrderDataTable) -> {
          utils.createSalesOrderSalesOrderData(salesOrderDataTable);
        });

    Given("^the following Items for SalesOrders exist:$", (DataTable itemsDataTable) -> {
      utils.createSalesOrderItems(itemsDataTable);
    });

    Given("^the following TaxDetails for SalesOrders exist:$", (DataTable taxesDataTable) -> {
      utils.createSalesOrderTaxes(taxesDataTable);
    });

    Given("^the following SalesOrders have the following total amount and remainings:$",
        (DataTable totalAndRemainingDT) -> {
          utils.insertSalesOrderTotalAndRemaining(totalAndRemainingDT);
        });

  }

  @Before
  public void setup() throws Exception {
    utils = new DObSalesOrderDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
