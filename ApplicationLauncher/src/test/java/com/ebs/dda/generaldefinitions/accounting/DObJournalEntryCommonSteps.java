package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryItemsViewAllTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObJournalEntryCommonSteps extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObJournalEntryViewAllTestUtils utils;
  private DObJournalEntryItemsViewAllTestUtils itemsUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;

  public DObJournalEntryCommonSteps() {

    Given(
        "^the following JournalEntries exist:$",
        (DataTable journalEntriesDataTable) -> {
          utils.insertJournalEntries(journalEntriesDataTable);
        });

    And(
        "^the following JournalEntries Details exist:$",
        (DataTable journalEntriesDetailsDataTable) -> {
          itemsUtils.insertJournalEntriesDetails(journalEntriesDetailsDataTable);
        });

    Then(
        "^the following Journal Entry is Created:$",
        (DataTable journalEntriesDataTable) -> {
          journalEntryViewAllTestUtils
              .assertThatJournalEntriesExistWithCompanyAndRefDocumentCodeName(
                  journalEntriesDataTable);
        });

    Then(
        "^The following Journal Items for Journal Entry with code \"([^\"]*)\":$",
        (String journalEntryCode, DataTable journalEntryItemsDateTable) -> {
          salesInvoicePostTestUtils.assertThatJournalEntryItemsCreatedSuccessfully(
              journalEntryCode, journalEntryItemsDateTable);
        });

    Given(
        "^the last created JournalEntry was with code \"([^\"]*)\"$",
        (String journalEntryCode) -> {
          utils.assertLastJournalEntry(journalEntryCode);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    itemsUtils =
        new DObJournalEntryItemsViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(getProperties());
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) {
    if (!hasBeenExecuted) {
      utils.executeJournalEntryReferenceDocumentFunction();
      utils.executeJournalEntryDetailTypeFunction();
      utils.executeJournalEntryDetailSubAccountDeterminationFunction();
      hasBeenExecuted = true;
    }
  }
}
