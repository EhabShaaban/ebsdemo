package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitialStockUploadCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObInitialStockUploadCommonSteps extends SpringBootRunner implements En {

    private DObInitialStockUploadCommonTestUtils utils;
    public  DObInitialStockUploadCommonSteps (){
        Given("^the following GeneralData for InitialStockUploads exist:$", (DataTable generalDataTable) -> {
            utils.createInitialStockUploadGeneralData(generalDataTable);
        });

        Given("^the following CompanyData for InitialStockUploads exist:$", (DataTable companyDataTable) -> {
            utils.createInitialStockUploadCompany(companyDataTable);
        });

        Given("^the following ItemsData for InitialStockUploads exist:$", (DataTable itemDataTable) -> {
            utils.createInitialStockUploadItem(itemDataTable);
        });

    }
    @Before
    public void setup() throws Exception {
        utils = new DObInitialStockUploadCommonTestUtils();
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }
}
