package com.ebs.dda.generaldefinitions.purchaseorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderPaymentAndDeliveryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObPurchaseOrderPaymentAndDeliveryCommonSteps extends SpringBootRunner implements En {

  private DObPurchaseOrderPaymentAndDeliveryTestUtils paymentAndDeliveryTestUtils;

  public DObPurchaseOrderPaymentAndDeliveryCommonSteps() {

    Given(
        "^the following Incoterms exist:$",
        (DataTable incotermDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatIncotermsExist(incotermDataTable);
        });

    Given(
        "^the following ModeOfTransports exist:$",
        (DataTable transportModesDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatTransportModesExist(transportModesDataTable);
        });

    Given(
        "^the following ContainerTypes exist:$",
        (DataTable containerTypeDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatContainerTypesExist(containerTypeDataTable);
        });

    Given(
        "^the following Ports exist:$",
        (DataTable portsDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatPortsExist(portsDataTable);
        });

    Given(
        "^the following PaymentTerms exist:$",
        (DataTable paymentTermsDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatPaymentTermsExist(paymentTermsDataTable);
        });

    Given(
        "^the following ShippingInstructions exist:$",
        (DataTable shippingInstructionsDataTable) -> {
          paymentAndDeliveryTestUtils.assertThatShippingInstructionsExist(
              shippingInstructionsDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\" is released$",
        (String username, String purchaseOrderCode) -> {
          paymentAndDeliveryTestUtils.assertThatLockIsReleased(
              username, purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
        });

  }

  @Before
  public void setup() throws Exception {
    paymentAndDeliveryTestUtils = new DObPurchaseOrderPaymentAndDeliveryTestUtils(getProperties());
  }
}
