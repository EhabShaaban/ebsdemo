package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;

public class DObSalesReturnOrderCommonStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderDeleteTestUtils utils;

  public DObSalesReturnOrderCommonStep() {

    Given(
        "^\"([^\"]*)\" first deleted the SalesReturnOrder with code \"([^\"]*)\" successfully$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesReturnOrderRestUrl(salesReturnOrderCode);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatSalesReturnOrderIsDeletedSuccessfully(salesReturnOrderCode);
        });

    Given(
        "^\"([^\"]*)\" first deleted the SalesReturnOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesReturnOrderRestUrl(salesReturnOrderCode);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatSalesReturnOrderIsDeletedSuccessfully(salesReturnOrderCode);
        });

    Given(
        "^Insert the following GeneralData for SalesReturnOrders exist:$",
        (DataTable generalDataTable) -> {
          utils.createSalesReturnOrderGeneralData(generalDataTable);
        });

    Given(
        "^Insert the following CompanyData for SalesReturnOrders exist:$",
        (DataTable companyDataTable) -> {
          utils.createSalesReturnOrderCompanyData(companyDataTable);
        });

    Given(
        "^Insert the following ReturnDetails for SalesReturnOrders exist:$",
        (DataTable returnDetailsTable) -> {
          utils.createSalesReturnOrderReturnDetails(returnDetailsTable);
        });

    Given(
        "^Insert the following Item for SalesReturnOrders exist:$",
        (DataTable itemTable) -> {
          utils.createSalesReturnOrderItems(itemTable);
        });

    Given(
        "^Insert the following Item with Id for SalesReturnOrders exist:$",
        (DataTable itemTable) -> {
          utils.createSalesReturnOrderItemsWithId(itemTable);
        });

    Given(
        "^Insert the following Taxes for SalesReturnOrders exist:$",
        (DataTable taxesTable) -> {
          utils.createSalesReturnOrderTaxes(taxesTable);
        });

    Given(
        "^the following values for ItemsSummary for salesReturnOrders exist:$",
        (DataTable ItemsSummaryTable) -> {
          utils.assertThatSalesReturnOrderSummaryExist(ItemsSummaryTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObSalesReturnOrderDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
