package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObGoodsIssueCommonStep extends SpringBootRunner implements En {

  private DObGoodsIssueCommonTestUtils utils;

  public DObGoodsIssueCommonStep() {
    Given(
        "^the following GeneralData for GoodsIssues SalesOrder exist:$",
        (DataTable generalDataTable) -> {
          utils.createGoodsIssueSalesOrderGeneralData(generalDataTable);
        });

    Given(
        "^the following CompanyData for GoodsIssues SalesOrder exist:$",
        (DataTable sompanyDataTable) -> {
          utils.createGoodsIssueSalesOrderCompany(sompanyDataTable);
        });

    Given(
        "^the following SalesOrderData for GoodsIssues SalesOrder exist:$",
        (DataTable salesOrderDataTable) -> {
          utils.createGoodsIssueSalesOrderData(salesOrderDataTable);
        });

    Given(
        "^the following ItemData for GoodsIssues SalesOrder exist:$",
        (DataTable itemsDataTable) -> {
          utils.createGoodsIssueSalesOrderItem(itemsDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObGoodsIssueCommonTestUtils();
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
