package com.ebs.dda.generaldefinitions.accounting;

import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostFactorItemsViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;

public class DObLandedCostCommonSteps extends SpringBootRunner implements En {

  private DObLandedCostCommonTestUtils utils;
  private DObLandedCostFactorItemsViewTestUtils costFactorItemsViewTestUtils;

  public DObLandedCostCommonSteps() {

    Given("^the following LandedCosts GeneralData exist:$", (DataTable landedCostGeneralDataDT) -> {
      utils.insertLandedCostGeneralData(landedCostGeneralDataDT);
    });

    Given("^the following LandedCosts CompanyData exist:$", (DataTable landedCostCompanyDataDT) -> {
      utils.insertLandedCostCompanyData(landedCostCompanyDataDT);
    });

    Given("^the following LandedCosts Details exist:$", (DataTable landedCostDetailsDataDT) -> {
      utils.insertLandedCostDetailsData(landedCostDetailsDataDT);
    });
    Given("^the following CostFactorItems for LandedCosts exist:$",
        (DataTable costFactorItemsDataTable) -> {
          utils.assertThatLandedCostFactorItemsExists(costFactorItemsDataTable);
        });
    Given(
        "^the following CostFactorItemSummary for LandedCosts exist:$",
        (DataTable itemsSummaryDataTable) -> {
          utils.assertThatLandedCostHasItemsSummary(itemsSummaryDataTable);
        });
    Given("^the following LandedCosts CostFactorItems exist:$", (DataTable costFactorItemsDT) -> {
      utils.insertCostFactorItems(costFactorItemsDT);
    });

    And("^CostFactorItem \"([^\"]*)\" in LandedCost \"([^\"]*)\" updated as follows:$",
        (String costFactorItem, String landedCostCode, DataTable costFactorItemDataTable) -> {
          costFactorItemsViewTestUtils.assertCostFactorItemsWithDetailsExist(costFactorItem,
              landedCostCode, costFactorItemDataTable);
        });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();

    utils = new DObLandedCostCommonTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    costFactorItemsViewTestUtils = new DObLandedCostFactorItemsViewTestUtils(properties);
    costFactorItemsViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

  }
}
