package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptCommonTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptDeleteItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObGoodsReceiptsCommonStep extends SpringBootRunner implements En {

  private DObGoodsReceiptCommonTestUtils goodsReceiptTestUtils;
  private IObGoodsReceiptDeleteItemTestUtils goodsReceiptDeleteItemTestUtils;

  public DObGoodsReceiptsCommonStep() {

    Given(
        "^\"([^\"]*)\" first deleted the GoodsReceipt with code \"([^\"]*)\" successfully$",
        (String userName, String goodsReceiptCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = goodsReceiptTestUtils.deleteGoodsReceipt(goodsReceiptCode, cookie);
          goodsReceiptTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deleted the GoodsReceipt with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode, String dateTime) -> {
          goodsReceiptTestUtils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = goodsReceiptTestUtils.deleteGoodsReceipt(goodsReceiptCode, cookie);
          goodsReceiptTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Item with code \"([^\"]*)\" of GoodsReceipt with code \"([^\"]*)\" successfully$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response deleteItemResponse =
              goodsReceiptDeleteItemTestUtils.deleteItem(itemCode, goodsReceiptCode, cookie);
          goodsReceiptTestUtils.assertResponseSuccessStatus(deleteItemResponse);
        });
    And(
        "^\"([^\"]*)\" first deleted the Item with code \"([^\"]*)\" of GoodsReceipt with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode, String dateTime) -> {
          goodsReceiptTestUtils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response deleteItemResponse =
              goodsReceiptDeleteItemTestUtils.deleteItem(itemCode, goodsReceiptCode, cookie);
          goodsReceiptTestUtils.assertResponseSuccessStatus(deleteItemResponse);
        });

    Given(
        "^the following GeneralData for GoodsReceipts SalesReturn exist:$",
        (DataTable goodsReceiptHeaderData) -> {
          goodsReceiptTestUtils.createGoodsReceiptSalesReturnGeneralData(goodsReceiptHeaderData);
        });

    Given(
        "^the following CompanyData for GoodsReceipts SalesReturn exist:$",
        (DataTable companyDataTable) -> {
          goodsReceiptTestUtils.createGoodsReceiptSalesReturnCompany(companyDataTable);
        });

    Given(
        "^the following SalesReturnData for GoodsReceipts SalesReturn exist:$",
        (DataTable goodsReceiptSalesReturnDataTable) -> {
          goodsReceiptTestUtils.createGoodsReceiptSalesReturnData(goodsReceiptSalesReturnDataTable);
        });

    Given(
        "^GoodsReceipt SalesReturn with code \"([^\"]*)\" has the following received items:$",
        (String goodsReceiptCode, DataTable receivedItems) -> {
          goodsReceiptTestUtils.createGoodsReceiptSalesReturnItem(goodsReceiptCode, receivedItems);
        });

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt SalesReturn \"([^\"]*)\" has the following received quantities:$",
        (String item, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
          goodsReceiptTestUtils.createGoodsReceiptSalesReturnItemQuantity(
              goodsReceiptCode, item, ordinaryItemQuantities);
        });
    Given(
        "^insert the following GoodsReceipt GeneralData:$",
        (DataTable grGeneralData) -> {
          goodsReceiptTestUtils.insertGoodsReceiptGeneralData(grGeneralData);
        });

    Given(
        "^insert the following GoodsReceipt CompanyData:$",
        (DataTable grCompanyData) -> {
          goodsReceiptTestUtils.insertGoodsReceiptCompanyData(grCompanyData);
        });

    Given(
        "^insert the following GoodsReceipt PurchaseOrderData:$",
        (DataTable grPurchaseOrderData) -> {
          goodsReceiptTestUtils.insertGoodsReceiptPurchaseOrderData(grPurchaseOrderData);
        });
    Given(
        "^insert the following GoodsReceipt Items:$",
        (DataTable grItemsDataTable) -> {
          goodsReceiptTestUtils.insertGoodsReceiptItems(grItemsDataTable);
        });

    Given(
        "^insert the following GoodsReceipt ItemQuantities:$",
        (DataTable itemQuantitiesDataTable) -> {
          goodsReceiptTestUtils.insertGoodsReceiptItemQuantities(itemQuantitiesDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    goodsReceiptTestUtils = new DObGoodsReceiptCommonTestUtils();
    goodsReceiptTestUtils.setProperties(getProperties());
    goodsReceiptTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsReceiptDeleteItemTestUtils = new IObGoodsReceiptDeleteItemTestUtils(getProperties());
  }
}
