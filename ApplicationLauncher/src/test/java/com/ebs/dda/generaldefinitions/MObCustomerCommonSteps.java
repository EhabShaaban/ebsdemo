package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class MObCustomerCommonSteps extends SpringBootRunner implements En {

  private MObCustomerCommonTestUtils utils;

  public MObCustomerCommonSteps() {

    Given(
        "^the following Customers exist:$",
        (DataTable customerDataTable) -> {
          utils.assertThatCustomersExist(customerDataTable);
        });

    Given(
        "^the following GeneralData for Customer exist:$",
        (DataTable generalDataTable) -> {
          utils.createCustomerGeneralData(generalDataTable);
        });

    Given(
        "^the following BusinessUnits for Customer exist:$",
        (DataTable businessUnitsTable) -> {
          utils.createCustomerBusinessUnits(businessUnitsTable);
        });

    Given(
        "^the following Customer Addresses exist:$",
        (DataTable customerAddressTable) -> {
          utils.createCustomerAddress(customerAddressTable);
        });
    Given(
        "^the following Customer ContactPersons exist:$",
        (DataTable contactPersonTable) -> {
          utils.createCustomerContactPerson(contactPersonTable);
        });

    Given(
        "^the following LegalRegistrationData for Customer exist:$",
        (DataTable legalRegistrationDataTable) -> {
          utils.createCustomerLegalRegistrationData(legalRegistrationDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new MObCustomerCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
