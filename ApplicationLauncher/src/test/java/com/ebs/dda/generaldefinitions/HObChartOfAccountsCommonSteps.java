package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateHPTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class HObChartOfAccountsCommonSteps extends SpringBootRunner
    implements En, InitializingBean {

  private HObChartOfAccountsCreateHPTestUtils utils;

  public HObChartOfAccountsCommonSteps() {
    Given(
        "^the following GLAccounts exist:$",
        (DataTable glAccountsDataTable) -> {
          utils.insertGlAccounts(glAccountsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new HObChartOfAccountsCreateHPTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
