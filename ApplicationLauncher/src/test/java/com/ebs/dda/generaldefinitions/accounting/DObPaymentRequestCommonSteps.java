package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObPaymentRequestCommonSteps extends SpringBootRunner implements En {

  private DObPaymentRequestViewGeneralDataTestUtils utils;

  public DObPaymentRequestCommonSteps() {

    Given(
        "^the following PaymentRequests GeneralData exist:$",
        (DataTable prGeneralDataDT) -> {
          utils.insertPaymentRequestGeneralData(prGeneralDataDT);
        });

    Given(
        "^the following PaymentRequests CompanyData exist:$",
        (DataTable prCompanyDataDT) -> {
          utils.insertPaymentRequestCompanyData(prCompanyDataDT);
        });

    Given(
        "^the following PaymentRequests PaymentDetails exist:$",
        (DataTable prDetailsDataDT) -> {
          utils.insertPaymentRequestDetailsData(prDetailsDataDT);
        });
      And("^the following PaymentRequests PostingDetails exist:$", (DataTable postingDetails) -> {
          utils.insertPaymentRequestPostingDetails(postingDetails);
      });

  }

    @Before
    public void setup() throws Exception {
        utils = new DObPaymentRequestViewGeneralDataTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }
}
