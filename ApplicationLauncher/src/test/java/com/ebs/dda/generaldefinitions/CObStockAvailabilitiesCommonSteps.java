package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.storetransaction.utils.DObStockAvailabilitiesViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class CObStockAvailabilitiesCommonSteps extends SpringBootRunner implements En {
    private DObStockAvailabilitiesViewAllTestUtils utils;

    public CObStockAvailabilitiesCommonSteps(){
        Given("^insert the following StockAvailabilities exist:$", (DataTable stockAvailabilitiesDataTable) -> {
            utils.insertStockAvailabilities(stockAvailabilitiesDataTable);
        });

        Then("^the following StockAvailabilities will be created or updated:$",
                (DataTable stockAvailabilitiesDataTable) -> {
            utils.validateStockAvailabilitiesMatchDatabase(stockAvailabilitiesDataTable);
        });

    }

    @Before
    public void setup() throws Exception {
        utils = new DObStockAvailabilitiesViewAllTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

}
