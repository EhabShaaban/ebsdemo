package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemCommonTestUtils;
import com.ebs.dda.masterdata.item.utils.MObItemReadItemUomTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObItemCommonSteps extends SpringBootRunner implements En {

  private MObItemCommonTestUtils itemCommonTestUtils;
  private MObItemReadItemUomTestUtils itemUomTestUtils;

  public MObItemCommonSteps() {

    Given(
        "^the following Items exist with more data:$",
        (DataTable itemsDataTable) -> {
          itemCommonTestUtils.assertItemExist(itemsDataTable);
        });

    Given(
        "^the following Items exist:$",
        (DataTable itemsData) -> {
          itemCommonTestUtils.assertItemsExistByCodeAndPurchaseUnits(itemsData);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Item with code \"([^\"]*)\" successfully$",
        (String userName, String itemCode) -> {
          Response deletePurchaseOrderResponse =
              itemCommonTestUtils.deleteItemByCode(
                  userActionsTestUtils.getUserCookie(userName), itemCode);
          userActionsTestUtils.setUserResponse(userName, deletePurchaseOrderResponse);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Item with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String itemCode, String dateTime) -> {
          itemCommonTestUtils.freeze(dateTime);
          Response deletePurchaseOrderResponse =
              itemCommonTestUtils.deleteItemByCode(
                  userActionsTestUtils.getUserCookie(userName), itemCode);
          userActionsTestUtils.setUserResponse(userName, deletePurchaseOrderResponse);
        });

    Given(
        "^the following Items exist with the below details:$",
        (DataTable itemsDataTable) -> {
          itemUomTestUtils.assertThatItemsExist(itemsDataTable);
        });

    Given(
        "^the following UOMs exist for Item \"([^\"]*)\":$",
        (String itemCodeName, DataTable itemUOMsDataTable) -> {
          itemUomTestUtils.assertThatUOMsExistForItem(itemCodeName, itemUOMsDataTable);
        });
	And("^insert the following Items GeneralData exist:$", (DataTable itemsDataTable) -> {
		itemCommonTestUtils.insertItemsGeneralData(itemsDataTable);
	});
	And("^insert the following Items AlternateUnitOfMeasures exist:$",
					(DataTable alternateUnitOfMeasuresDataTable) -> {
						itemCommonTestUtils.insertItemsAlternateUnitOfMeasures(
										alternateUnitOfMeasuresDataTable);
					});
      And("^the following Items BusinessUnits exist:$", (DataTable businessUnitsDataTable) -> {
          itemCommonTestUtils.insertItemBusinessUnits(businessUnitsDataTable);
      });
  }

  @Before
  public void setup() throws Exception {
    itemCommonTestUtils = new MObItemCommonTestUtils(getProperties());
    itemCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    itemUomTestUtils =
        new MObItemReadItemUomTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
