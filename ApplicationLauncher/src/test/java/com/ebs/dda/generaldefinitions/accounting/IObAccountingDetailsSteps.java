package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import org.springframework.beans.factory.InitializingBean;

public class IObAccountingDetailsSteps extends SpringBootRunner implements En, InitializingBean {

    private AccountingDocumentCommonTestUtils utils;


    public IObAccountingDetailsSteps() {
        And("^AccountingDetails in \"([^\"]*)\" with Code \"([^\"]*)\" is updated as follows:$", (String objectType, String accountingDocumentCode, DataTable accountingDetailsDataTable) -> {
            utils.assertThatAccountingDetailsAreUpdated(objectType, accountingDocumentCode, accountingDetailsDataTable);

        });
    }

    @Before
    public void setup() throws Exception {
        utils = new AccountingDocumentCommonTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }
}
