package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.java8.En;

public class LoginSteps extends SpringBootRunner implements En {

  public LoginSteps() {

    Given(
        "^user is logged in as \"([^\"]*)\"$",
        (String username) -> {
          userActionsTestUtils.login(username);
        });

    Given(
        "^another user is logged in as \"([^\"]*)\"$",
        (String userName) -> userActionsTestUtils.login(userName));
  }


}
