package com.ebs.dda.generaldefinitions.purchaseorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderCommonSteps extends SpringBootRunner implements En {

  private DObPurchaseOrderCommonTestUtils purchaseOrderCommonTestUtils;

  public DObPurchaseOrderCommonSteps() {
    Given(
        "^the following Import PurchaseOrders exist:$",
        (DataTable purchaseOrderTable) -> {
          purchaseOrderCommonTestUtils.assertThatImportPurchaseOrdersExist(purchaseOrderTable);
        });

    Given(
        "^the following Local PurchaseOrders exist:$",
        (DataTable purchaseOrderTable) -> {
          purchaseOrderCommonTestUtils.assertThatLocalPurchaseOrdersExist(purchaseOrderTable);
        });

    Given(
        "^the following GeneralData for PurchaseOrders exist:$",
        (DataTable purchaseOrderTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderGeneralData(purchaseOrderTable);
        });

    Given(
        "^the following CompanyData for PurchaseOrders exist:$",
        (DataTable purchaseOrderTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderCompanyData(purchaseOrderTable);
        });

    Given(
        "^the following VendorData for PurchaseOrders exist:$",
        (DataTable purchaseOrderTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderVendorData(purchaseOrderTable);
        });

    Given(
        "^the following ItemsData for PurchaseOrders exist:$",
        (DataTable itemsDataTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderItemsData(itemsDataTable);
        });

    Given(
        "^the following PurchaseOrders have no Items:$",
        (DataTable purchaseOrderDataTable) -> {
          purchaseOrderCommonTestUtils.assertThatOrderHasNoItems(purchaseOrderDataTable);
        });

    Given(
        "^the following PurchaseOrders have the following TaxDetails:$",
        (DataTable purchaseOrderTaxDetailsDataTable) -> {
          purchaseOrderCommonTestUtils.assertThatPurchaseOrdersHaveTheFollowingTaxDetails(
              purchaseOrderTaxDetailsDataTable);
        });

    Given(
        "^the following PurchaseOrders have no Taxes:$",
        (DataTable purchaseOrderDataTable) -> {
          purchaseOrderCommonTestUtils.assertThatPurchaseOrderHasNoTaxes(purchaseOrderDataTable);
        });

    Given(
        "^the following PaymentTermData for PurchaseOrders exist:$",
        (DataTable PaymentTermDataTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderPaymentTermData(PaymentTermDataTable);
        });

    Given(
        "^the following CycleDates for PurchaseOrders exist:$",
        (DataTable CycleDatesDataTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderCycleDates(CycleDatesDataTable);
        });

    Given(
        "^the following ApprovalCycles for PurchaseOrders exist:$",
        (DataTable approvalCyclesDataTable) -> {
          purchaseOrderCommonTestUtils.createPurchaseOrderApprovalCycle(approvalCyclesDataTable);
        });

    Given(
        "^the following user information exist:$",
        (DataTable usersInformationTable) -> {
          purchaseOrderCommonTestUtils.assertThatUsersInformationIsCorrect(usersInformationTable);
        });

    And(
        "^the following Companies exist:$",
        (DataTable companyTable) -> {
          purchaseOrderCommonTestUtils.assertThatCompaniesExist(companyTable);
        });

    Given(
        "^\"([^\"]*)\" first deleted the PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String userName, String purchaseOrderCode) -> {
          Response response =
              purchaseOrderCommonTestUtils.deletePurchaseOrderByCode(
                  userActionsTestUtils.getUserCookie(userName), purchaseOrderCode);
          purchaseOrderCommonTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deleted the PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          purchaseOrderCommonTestUtils.freeze(dateTime);
          Response response =
              purchaseOrderCommonTestUtils.deletePurchaseOrderByCode(
                  userActionsTestUtils.getUserCookie(userName), purchaseOrderCode);
          purchaseOrderCommonTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deletes Item with code \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String username, String itemCode, String orderCode) -> {
          Response response =
              purchaseOrderCommonTestUtils.deleteItemInPurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), orderCode, itemCode);
          purchaseOrderCommonTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first deletes Item with code \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String username, String itemCode, String purchaseOrderCode, String dateTime) -> {
          purchaseOrderCommonTestUtils.freeze(dateTime);
          Response response =
              purchaseOrderCommonTestUtils.deleteItemInPurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), purchaseOrderCode, itemCode);
          purchaseOrderCommonTestUtils.assertResponseSuccessStatus(response);
        });
    Given(
        "^the following Remaining for PurchaseOrders exist:$",
        (DataTable remainingDataTable) -> {
          purchaseOrderCommonTestUtils.setPurchaseOrderRemaining(remainingDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    purchaseOrderCommonTestUtils = new DObPurchaseOrderCommonTestUtils();
    purchaseOrderCommonTestUtils.setProperties(getProperties());
    purchaseOrderCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
