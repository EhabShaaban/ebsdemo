package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.storetransaction.utils.TObStoreTransactionCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObStoreTransactionCommonStep extends SpringBootRunner implements En {

  private TObStoreTransactionCreateTestUtils utils;

  public DObStoreTransactionCommonStep() {

    And(
        "^insert the following StoreTransactions:$",
        (DataTable storeTransactions) -> {
          utils.insertStoreTransactions(storeTransactions);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new TObStoreTransactionCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
