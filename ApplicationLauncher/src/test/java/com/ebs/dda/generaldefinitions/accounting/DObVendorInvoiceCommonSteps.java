package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObVendorInvoiceCommonSteps extends SpringBootRunner implements En {

  private DObVendorInvoiceCommonTestUtils utils;

  public DObVendorInvoiceCommonSteps() {

    Given(
        "^the following Companies have the following local Currencies:$",
        (DataTable companyDataTable) -> {
          utils.assertThatCompanyExistWithCurrency(companyDataTable);
        });
    And(
        "^the following VendorInvoices GeneralData exist:$",
        (DataTable vendorInvoiceDataTable) -> {
          utils.insertVendorInvoiceGeneralData(vendorInvoiceDataTable);
        });

    Given(
        "^the following VendorInvoices CompanyData exist:$",
        (DataTable vendorInvoiceCompanyDataTable) -> {
          utils.insertVendorInvoiceCompanyData(vendorInvoiceCompanyDataTable);
        });
    Given(
        "^the following VendorInvoices Details exist:$",
        (DataTable invoicesDetailsDataTable) -> {
          utils.insertVendorInvoiceDetails(invoicesDetailsDataTable);
        });
    Given(
        "^the following VendorInvoices Items exist:$",
        (DataTable viItemsDT) -> {
          utils.insertVendorInvoiceItems(viItemsDT);
        });
    And(
        "^the following VendorInvoice  Summary exist:$",
        (DataTable vendorInvoiceSummaryDataTable) -> {
          utils.insertVendorInvoiceSummary(vendorInvoiceSummaryDataTable);
      });
      And("^the following VendorInvoices ActivationDetails exist:$",
              (DataTable postingDetailsDataTable) -> {
                  utils.insertVendorInvoicePostingDetails(
                          postingDetailsDataTable);
              });
  }

  @Before
  public void setup() throws Exception {
    utils = new DObVendorInvoiceCommonTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
