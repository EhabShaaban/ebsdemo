package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.AccountingNoteCommonTestUtil;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObAccountingNoteCommonSteps extends SpringBootRunner implements En {

  private AccountingNoteCommonTestUtil utils;

  public DObAccountingNoteCommonSteps() {
    Given(
        "^the following GeneralData for AccountingNote exist:$",
        (DataTable accountingNoteTable) -> {
          utils.createAccountingNoteGeneralData(accountingNoteTable);
        });

    Given(
        "^the following CompanyData for AccountingNote exist:$",
        (DataTable accountingNoteTable) -> {
          utils.createAccountingNoteCompanyData(accountingNoteTable);
        });

    Given(
        "^the following NoteDetails for AccountingNote exist:$",
        (DataTable accountingNoteTable) -> {
          utils.createAccountingNoteDetails(accountingNoteTable);
        });
    Then(
        "^total number of DocumentOwners returned to \"([^\"]*)\" in a dropdown is equal to (\\d+)$",
        (String userName, Integer expectedTotalNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfDocumentOwnersEquals(expectedTotalNumber, response);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new AccountingNoteCommonTestUtil();
    utils.setProperties(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
