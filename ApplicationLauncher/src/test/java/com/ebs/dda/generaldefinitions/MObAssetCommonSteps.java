package com.ebs.dda.generaldefinitions;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.assetmasterdata.utils.MObAssetCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class MObAssetCommonSteps extends SpringBootRunner implements En {

  private MObAssetCommonTestUtils utils;

  public MObAssetCommonSteps() {

    Given(
        "^the following GeneralData for AssetMasterData exist:$",
        (DataTable generalDataTable) -> {
          utils.createAssetMasterDataGeneralData(generalDataTable);
        });

    Given(
        "^the following BusinessUnits for AssetMasterData exist:$",
        (DataTable businessUnitsTable) -> {
          utils.createAssetMasterDataBusinessUnits(businessUnitsTable);
        });
  }

  @Before
  public void setup() throws Exception {
    utils = new MObAssetCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
