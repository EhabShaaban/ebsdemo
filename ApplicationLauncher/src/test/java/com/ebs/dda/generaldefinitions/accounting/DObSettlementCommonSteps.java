package com.ebs.dda.generaldefinitions.accounting;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils;
import com.ebs.dda.accounting.settlement.utils.IObSettlementViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementCommonSteps extends SpringBootRunner implements En {

  private DObSettlementsCommonTestUtils utils;
  private IObSettlementViewAccountingDetailsTestUtils viewAccountingDetailsTestUtils;

  public DObSettlementCommonSteps() {

    Given(
        "^the following Settlements GeneralData exist:$",
        (DataTable settlementGeneralDataDT) -> {
          utils.insertSettlementsGeneralData(settlementGeneralDataDT);
        });

    Given(
        "^the following Settlements CompanyData exist:$",
        (DataTable settlementCompanyDataDT) -> {
          utils.insertSettlementCompanyData(settlementCompanyDataDT);
        });

    Given(
        "^the following Settlements Details exist:$",
        (DataTable settlementDetailsDataDT) -> {
          utils.insertSettlementDetailsData(settlementDetailsDataDT);
        });
    And(
        "^the following Settlements ActivationDetails exist:$",
        (DataTable postingDetailsDataTable) -> {
            utils.insertSettlementActivationDetails(postingDetailsDataTable);
        });
      And(
              "^the following Settlements AccountingDetails exist:$",
              (DataTable AccountingDetailsTable) -> {
                  utils.insertSettlementAccountingDetailsData(AccountingDetailsTable);
              });
      When(
              "^\"([^\"]*)\" requests to view AccountingDetails section of Settlement with code \"([^\"]*)\"$",
              (String userName, String settlementCode) -> {
                  Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                  Response response =
                          utils.sendGETRequest(cookie, viewAccountingDetailsTestUtils.getViewAccountingDetailsUrl(settlementCode));
                  userActionsTestUtils.setUserResponse(userName, response);
              });

  }

  @Before
  public void setup() throws Exception {
    utils = new DObSettlementsCommonTestUtils(getProperties());
      viewAccountingDetailsTestUtils = new IObSettlementViewAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      viewAccountingDetailsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
