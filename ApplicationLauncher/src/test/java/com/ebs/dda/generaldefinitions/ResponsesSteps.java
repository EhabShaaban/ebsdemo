package com.ebs.dda.generaldefinitions;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.SpringBootRunner;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class ResponsesSteps extends SpringBootRunner implements En {

  private CommonTestUtils commonTestUtils;

  public ResponsesSteps() {

    Then(
        "^a success notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\"$",
        (String userName, String msgCode) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatSuccessResponseHasMessageCode(response, msgCode);
        });

    Then(
        "^a failure notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\"$",
        (String userName, String errorCode) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatResponseHasFailStatusAndMsgCodeEquals(response, errorCode);
        });

    Then(
        "^an error notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\"$",
        (String username, String expectedMessageCode) -> {
          Response readResponse = userActionsTestUtils.getUserResponse(username);
          commonTestUtils.assertThatResponseHasErrorStatusAndMsgCodeEquals(
              readResponse, expectedMessageCode);
        });

    Then(
        "^an authorization error notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\"$",
        (String userName, String messageCode) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatResponseHasErrorStatusAndMsgCodeEquals(response, messageCode);
        });

    Then(
        "^\"([^\"]*)\" is logged out$",
        (String username) -> {
          commonTestUtils.assertThatUserIsLoggedOut(userActionsTestUtils.getUserCookie(username));
        });

    Then(
        "^\"([^\"]*)\" is forwarded to the error page$",
        (String username) -> {
          commonTestUtils.assertThatResponseIsClientBypassingResponse(
              userActionsTestUtils.getUserResponse(username));
        });

    Then(
        "^the following authorized reads are returned to \"([^\"]*)\":$",
        (String userName, DataTable authorizedReadsData) -> {
          commonTestUtils.assertThatUserHasAuthorizedReads(
              authorizedReadsData, userActionsTestUtils.getUserResponse(userName));
        });

    Then(
        "^the following are authorized reads \"([^\"]*)\" returned to \"([^\"]*)\"$",
        (String authorizedReadsData, String userName) -> {
          commonTestUtils.assertThatUserHasAuthorizedReads(
              authorizedReadsData, userActionsTestUtils.getUserResponse(userName));
        });

    Then(
        "^the following mandatory fields are returned to \"([^\"]*)\":$",
        (String userName, DataTable mandatoriesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatMandatoriesFieldsReturned(response, mandatoriesDataTable);
        });

    Then(
        "^the following editable fields are returned to \"([^\"]*)\":$",
        (String userName, DataTable editablesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatEditableFieldsReturned(response, editablesDataTable);
        });

    Then(
        "^there are no authorized reads returned to \"([^\"]*)\"$",
        (String userName) -> {
          commonTestUtils.assertThatUserHasNoAuthorizedReads(
              userActionsTestUtils.getUserResponse(userName));
        });

    Then(
        "^there are no mandatory fields returned to \"([^\"]*)\"$",
        (String userName) -> {
          commonTestUtils.assertThatResponseMandatoriesListIsEmpty(
              userActionsTestUtils.getUserResponse(userName));
        });

    And(
        "^the following error message is attached to \"([^\"]*)\" field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String fieldName, String messageCode, String username) -> {
          commonTestUtils.assertThatFieldHasErrorMessageCode(
              userActionsTestUtils.getUserResponse(username), messageCode, fieldName);
        });
    And(
        "^the system responds to \"([^\"]*)\" within \"([^\"]*)\" second$",
        (String userName, String expectedResponseTime) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatResponseTimeIsLessThan(response, expectedResponseTime);
        });
    And(
        "^a success notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\" with the following JournalEntry code \"([^\"]*)\"$",
        (String userName, String msgCode, String journalEntryCode) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          commonTestUtils.assertThatSuccessResponseHasMessageCode(response, msgCode);
          commonTestUtils.assertThatResponseHasJournalEntryCode(response, journalEntryCode);
        });
  }

  @Before
  public void setup() throws Exception {
    Map<String, Object> properties = getProperties();
    commonTestUtils = new CommonTestUtils();
    commonTestUtils.setProperties(properties);
  }
}
