package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class IObSalesReturnOrderItemDeleteTestUtils extends DObSalesReturnOrderCommonTestUtils {

  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[ItemDelete]";

  public IObSalesReturnOrderItemDeleteTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatSalesReturnOrderItemIsDeletedSuccessfully(
      String salesReturnOrderCode, Integer itemId) {
    Object actualSalesReturnOrderItem =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderItemBySalesReturnOrderCodeAndItemId", salesReturnOrderCode, itemId);
    assertNull(
        ASSERTION_MSG + "Sales Return Order Item does not deleted successfully",
        actualSalesReturnOrderItem);
  }

  public String getDeleteSalesReturnOrderItemRestUrl(String salesReturnOrderCode, Integer itemId) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IDObSalesReturnOrderRestURLs.COMMAND);
    deleteUrl.append("/");
    deleteUrl.append(salesReturnOrderCode);
    deleteUrl.append(IDObSalesReturnOrderRestURLs.ITEM_DATA);
    deleteUrl.append("/");
    deleteUrl.append(itemId);
    return deleteUrl.toString();
  }
}
