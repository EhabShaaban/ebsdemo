package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderCreateValueObject;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesReturnOrderCreateTestUtils extends DObSalesReturnOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public DObSalesReturnOrderCreateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatTheFollowingProductManagersExist(DataTable productManagerDataTable) {
    List<Map<String, String>> productManagerList =
        productManagerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> productManagerRow : productManagerList) {
      Object productManager =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getProductManagerByCodeAndName",
              constructProductManagerQueryParams(productManagerRow));
      assertNotNull(ASSERTION_MSG + " Product Manager is not exist", productManager);
    }
  }

  private Object[] constructProductManagerQueryParams(Map<String, String> productManagerRow) {
    return new Object[] {
      productManagerRow.get(IFeatureFileCommonKeys.CODE),
      productManagerRow.get(IFeatureFileCommonKeys.NAME),
    };
  }

  public void assertSalesInvoicesWithStateCustomerBusinessUnitCompanyExist(
      DataTable salesInvoiceDataTable) {
    List<Map<String, String>> salesInvoiceMaps =
        salesInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceMap : salesInvoiceMaps) {
      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoicesByCodeStateCustomerBusinessUnitCompany",
                  constructSalesInvoiceDataQueryParams(salesInvoiceMap));
      assertNotNull(
          ASSERTION_MSG + " Sales Invoice does not exist " + salesInvoiceMap.toString(),
          dObSalesInvoiceGeneralModel);
    }
  }

  private Object[] constructSalesInvoiceDataQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      '%' + salesInvoiceMap.get(IFeatureFileCommonKeys.STATE) + '%',
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.CUSTOMER),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.BUSINESS_UNIT),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.COMPANY)
    };
  }

  public Response createSalesReturnOrder(DataTable salesReturnOrderDataTable, Cookie cookie) {
    JsonObject salesReturnOrder = prepareSalesReturnOrderJsonObject(salesReturnOrderDataTable);
    String restURL = IDObSalesReturnOrderRestURLs.CREATE_SALES_RETURN_ORDER;
    return sendPostRequest(cookie, restURL, salesReturnOrder);
  }

  private JsonObject prepareSalesReturnOrderJsonObject(DataTable salesReturnOrderDataTable) {
    Map<String, String> salesReturnOrderMap =
        salesReturnOrderDataTable.asMaps(String.class, String.class).get(0);
    JsonObject salesReturnOrderJsonObject = new JsonObject();

    addValueToJsonObject(
        salesReturnOrderJsonObject,
        IDObSalesReturnOrderCreateValueObject.TYPE_CODE,
        salesReturnOrderMap.get(IOrderFeatureFileCommonKeys.TYPE));

    addValueToJsonObject(
        salesReturnOrderJsonObject,
        IDObSalesReturnOrderCreateValueObject.SALES_INVOICE_CODE,
        salesReturnOrderMap.get(ISalesFeatureFileCommonKeys.SALES_INVOICE));

    try {
      salesReturnOrderJsonObject.addProperty(
          IDObSalesReturnOrderCreateValueObject.DOCUMENT_OWNER_ID,
          Long.valueOf(salesReturnOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));
    } catch (NumberFormatException exception) {
      salesReturnOrderJsonObject.addProperty(
          IDObSalesReturnOrderCreateValueObject.DOCUMENT_OWNER_ID,
          salesReturnOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID));
    }

    return salesReturnOrderJsonObject;
  }

  public void assertThatSalesReturnOrderIsCreatedSuccessfully(DataTable salesReturnOrderDataTable) {
    Map<String, String> salesReturnOrderData =
        salesReturnOrderDataTable.asMaps(String.class, String.class).get(0);
    Object[] salesReturnOrder = (Object[]) retrieveSalesReturnOrderData(salesReturnOrderData);

    Assert.assertNotNull(salesReturnOrder);
    assertDateEquals(
        salesReturnOrderData.get(IFeatureFileCommonKeys.CREATION_DATE),
        new DateTime(salesReturnOrder[0]).withZone(DateTimeZone.UTC));

    assertDateEquals(
        salesReturnOrderData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(salesReturnOrder[1]).withZone(DateTimeZone.UTC));
  }

  private Object retrieveSalesReturnOrderData(Map<String, String> salesReturnOrderRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedSalesReturnOrder",
        constructCreatedSalesReturnOrderQueryParams(salesReturnOrderRow));
  }

  private Object[] constructCreatedSalesReturnOrderQueryParams(
      Map<String, String> salesReturnOrderRow) {
    return new Object[] {
      salesReturnOrderRow.get(IFeatureFileCommonKeys.CODE),
      '%' + salesReturnOrderRow.get(IFeatureFileCommonKeys.STATE) + '%',
      salesReturnOrderRow.get(IFeatureFileCommonKeys.CREATED_BY),
      salesReturnOrderRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      salesReturnOrderRow.get(IOrderFeatureFileCommonKeys.TYPE),
      salesReturnOrderRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void assertThatSalesReturnOrderCompanyDataIsCreatedSuccessfully(
      String salesReturnOrderCode,
      DataTable sroCompanyDataTable,
      IObSalesReturnOrderViewCompanyDataTestUtils salesReturnOrderViewCompanyDataTestUtils) {
    Map<String, String> salesReturnOrderCompanyData =
        sroCompanyDataTable.asMaps(String.class, String.class).get(0);
    salesReturnOrderViewCompanyDataTestUtils.ASSERTION_MSG = ASSERTION_MSG;
    salesReturnOrderViewCompanyDataTestUtils
        .assertThatTheFollowingSalesReturnOrdersExistWithCompanyData(
            salesReturnOrderCode, salesReturnOrderCompanyData);
  }

  public void assertThatSalesReturnOrderReturnDetailsDataIsCreatedSuccessfully(
      String salesReturnOrderCode,
      DataTable sroReturnDetailsDataTable,
      IObSalesReturnOrderViewReturnDetailsTestUtils salesReturnOrderViewReturnDetailsTestUtils) {
    Map<String, String> returnDetails =
        sroReturnDetailsDataTable.asMaps(String.class, String.class).get(0);
    salesReturnOrderViewReturnDetailsTestUtils.ASSERTION_MSG = ASSERTION_MSG;
    salesReturnOrderViewReturnDetailsTestUtils.assertThatSalesReturnDetailsExists(
        salesReturnOrderCode, returnDetails);
  }

  public void assertThatSalesReturnOrderItemsDataIsCreatedSuccessfully(
      String salesReturnOrderCode, DataTable salesReturnItemsDataTable) {
    List<Map<String, String>> salesReturnItems =
        salesReturnItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnItem : salesReturnItems) {
      assertThatSalesReturnOrderItemExists(salesReturnOrderCode, salesReturnItem);
    }
    assertThatTheseAreTheOnlyItemsExist(salesReturnOrderCode, salesReturnItemsDataTable);
  }

  private void assertThatSalesReturnOrderItemExists(
      String salesReturnOrderCode, Map<String, String> itemDataMap) {
    Object item =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCreatedIObSalesReturnOrderItem",
            constructSalesReturnOrderItemDataQueryParams(salesReturnOrderCode, itemDataMap));
    Assert.assertNotNull(
        ASSERTION_MSG
            + " Item data of sales return order "
            + salesReturnOrderCode
            + " does not exist",
        item);
  }

  private Object[] constructSalesReturnOrderItemDataQueryParams(
      String salesReturnOrderCode, Map<String, String> itemDataMap) {
    return new Object[] {
      salesReturnOrderCode,
      itemDataMap.get(IFeatureFileCommonKeys.ITEM),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY),
      itemDataMap.get(IOrderFeatureFileCommonKeys.SALES_PRICE),
      itemDataMap.get(IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT)
    };
  }

  public void assertThatSalesReturnOrderTaxesDetailsIsCreatedSuccessfully(
      String salesReturnOrderCode, DataTable salesReturnTaxesDataTable) {
    List<Map<String, String>> salesReturnTaxes =
        salesReturnTaxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnTax : salesReturnTaxes) {
      assertThatSalesReturnOrderTaxExists(salesReturnOrderCode, salesReturnTax);
    }
  }

  public void assertThatSalesReturnOrderTaxExists(
      String salesReturnOrderCode, Map<String, String> taxDataMap) {
    Object tax =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCreatedIObSalesReturnOrderTax",
            constructTaxesDetailQueryParams(salesReturnOrderCode, taxDataMap));
    Assert.assertNotNull(
        ASSERTION_MSG
            + " Item data of sales return order "
            + salesReturnOrderCode
            + " does not exist",
        tax);

    BigDecimal itemsTotalAmount =
        (BigDecimal)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getTotalAmountForIObSalesReturnOrderItems", salesReturnOrderCode);

    BigDecimal actualTotalTaxAmount =
        itemsTotalAmount
            .multiply(
                getBigDecimal(taxDataMap.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)))
            .divide(new BigDecimal("100.0"));

    Assert.assertEquals(
        ASSERTION_MSG + " Total Tax Amounts not equals ",
        getBigDecimal(taxDataMap.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT))
            .compareTo(actualTotalTaxAmount),
        0);
  }

  public void assertThatTheFollowingTaxesExistForSalesInvoice(
      DataTable salesInvoiceTaxesDataTable) {
    List<Map<String, String>> taxDetailsMaps =
        salesInvoiceTaxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {
      String salesInvoiceCode = taxDetailsMap.get(ISalesFeatureFileCommonKeys.SI_CODE);
      Object salesInvoiceCompanyTaxesDetailsGeneralModel =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getTaxesForCompanyInSalesInvoice",
              constructTaxesDetailQueryParams(salesInvoiceCode, taxDetailsMap));
      Assert.assertNotNull(
          ASSERTION_MSG + " Tax detail doesn't exist " + taxDetailsMap,
          salesInvoiceCompanyTaxesDetailsGeneralModel);
    }
  }

  private Object[] constructTaxesDetailQueryParams(
      String salesObjectCode, Map<String, String> taxDetailsMap) {
    return new Object[] {
      salesObjectCode,
      taxDetailsMap.get(IAccountingFeatureFileCommonKeys.TAX_NAME),
      taxDetailsMap.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public Response requestCreateSalesReturnOrder(Cookie cookie) {
    String restURL = IDObSalesReturnOrderRestURLs.CREATE_SALES_RETURN_ORDER;
    return sendGETRequest(cookie, restURL);
  }
}
