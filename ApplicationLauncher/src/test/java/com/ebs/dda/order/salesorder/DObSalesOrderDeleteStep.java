package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderDeleteTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderEditCompanyAndStoreTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderEditSalesOrderDataTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderRequestAddCancelItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesOrderDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObSalesOrderDeleteTestUtils utils;
  private IObSalesOrderEditCompanyAndStoreTestUtils editCompanyAndStoreTestUtils;
  private IObSalesOrderEditSalesOrderDataTestUtils editSalesOrderDataTestUtils;
  private IObSalesOrderRequestAddCancelItemTestUtils requestAddCancelItemTestUtils;

  public DObSalesOrderDeleteStep() {

    When(
        "^\"([^\"]*)\" requests to delete SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesOrderRestUrl(salesOrderCode);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrder with code \"([^\"]*)\" is deleted$",
        (String salesOrderCode) -> {
          utils.assertThatSalesOrderIsDeletedSuccessfully(salesOrderCode);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" section of SalesOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String salesOrderCode) -> {
          String lockUrl = utils.getLockUrlForSalesOrderSection(salesOrderCode, sectionName);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesOrderDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);

    editCompanyAndStoreTestUtils = new IObSalesOrderEditCompanyAndStoreTestUtils(getProperties());
    editSalesOrderDataTestUtils = new IObSalesOrderEditSalesOrderDataTestUtils(getProperties());
    requestAddCancelItemTestUtils = new IObSalesOrderRequestAddCancelItemTestUtils(getProperties());

    utils.setEditCompanyAndStoreTestUtils(editCompanyAndStoreTestUtils);
    utils.setEditSalesOrderDataTestUtils(editSalesOrderDataTestUtils);
    utils.setRequestAddCancelItemTestUtils(requestAddCancelItemTestUtils);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete SalesOrder,")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete SalesOrder,")) {
      userActionsTestUtils.unlockAllSections(
          IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
