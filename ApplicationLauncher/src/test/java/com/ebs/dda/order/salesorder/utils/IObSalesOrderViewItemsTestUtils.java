package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderTaxGeneralModel;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class IObSalesOrderViewItemsTestUtils extends IObSalesOrderItemsCommonTestUtils {

  public IObSalesOrderViewItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecutionSalesOrder() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/inventory/stockavailability/stockavailabilityClear.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCycleDates_ViewAll.sql");
    return str.toString();
  }

  public void assertThatTheFollowingSalesOrderDataExist(DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrderDataList =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderDataRow : salesOrderDataList) {
      Object salesOrderData =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getIObSalesOrderDataByCodeAndCustomer",
                  constructSalesOrderDataQueryParams(salesOrderDataRow));
      assertNotNull(salesOrderData);
    }
  }

  private Object[] constructSalesOrderDataQueryParams(Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      salesOrderDataRow.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }

  public void assertThatSalesOrderHasTheFollowingTaxes(DataTable taxesDataTable) {
    List<Map<String, String>> taxesList = taxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxRow : taxesList) {
      Object rowId =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesOrderTax", constructSalesOrderTaxQueryParams(taxRow));
      assertNotNull(
          "Sales Order with code "
              + taxRow.get(IFeatureFileCommonKeys.CODE)
              + " "
              + taxRow.get(ISalesOrderFeatureFileCommonKeys.TAX)
              + " "
              + taxRow.get(ISalesOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
              + " doesn't have taxes",
          rowId);
    }
  }

  private Object[] constructSalesOrderTaxQueryParams(Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(IFeatureFileCommonKeys.CODE),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.TAX),
      new BigDecimal(salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.TAX_PERCENTAGE))
    };
  }

  public void assertCorrectValuesAreDisplayedInItemSection(
      Response response, DataTable salesOrderItemsDataTable) throws Exception {
    List<Map<String, String>> expectedItemList =
        salesOrderItemsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemList = getItemsFromResponse(response);
    for (int i = 0; i < actualItemList.size(); i++) {
      MapAssertion mapAssertion = new MapAssertion(expectedItemList.get(i), actualItemList.get(i));
      assertActualAndExpectedItemsFromResponse(mapAssertion);
    }
  }

  private List<HashMap<String, Object>> getItemsFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Items");
  }

  private void assertActualAndExpectedItemsFromResponse(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM,
        IIObSalesOrderItemGeneralModel.ITEM_CODE,
        IIObSalesOrderItemGeneralModel.ITEM_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM_ORDER_UNIT,
        IIObSalesOrderItemGeneralModel.UNIT_OF_MEASURE_CODE,
        IIObSalesOrderItemGeneralModel.UNIT_OF_MEASURE_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        IFeatureFileCommonKeys.QTY, IIObSalesOrderItemGeneralModel.QUANTITY);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.SALES_PRICE, IIObSalesOrderItemGeneralModel.SALES_PRICE);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.TOTAL_AMOUNT, IIObSalesOrderItemGeneralModel.TOTAL_AMOUNT);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.LAST_SALES_PRICE,
        IIObSalesOrderItemGeneralModel.LAST_SALES_PRICE);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.AVAILABLE_QUANTITIES,
        IIObSalesOrderItemGeneralModel.AVAILABLE_QUANTITY);
  }

  public void assertThatItemTaxesResponseIsCorrect(Response response, DataTable taxesDataTable)
      throws Exception {
    List<Map<String, String>> expectedTaxesList = taxesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTaxesList = getTaxesFromResponse(response);
    for (int i = 0; i < actualTaxesList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedTaxesList.get(i), actualTaxesList.get(i));
      assertThatExpectedTaxesMatchesActualOnes(mapAssertion);
    }
  }

  private void assertThatExpectedTaxesMatchesActualOnes(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesOrderFeatureFileCommonKeys.TAX,
        IIObSalesOrderTaxGeneralModel.TAX_CODE,
        IIObSalesOrderTaxGeneralModel.TAX_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.TAX_AMOUNT, IIObSalesOrderTaxGeneralModel.TAX_AMOUNT);
  }

  private List<HashMap<String, Object>> getTaxesFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Taxes");
  }

  public void assertThatItemSummaryResponseIsCorrect(
      Response response, DataTable itemSummaryDataTable) throws Exception {
    Map<String, String> expectedItemSummary =
        itemSummaryDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualItemSummary = getItemSummaryFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedItemSummary, actualItemSummary);
    assertThatExpectedItemSummaryMatchesActualOne(mapAssertion);
  }

  private void assertThatExpectedItemSummaryMatchesActualOne(MapAssertion mapAssertion) {
    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES,
        IIObSalesOrderItemGeneralModel.TOTAL_AMOUNT_BEFORE_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.TOTAL_TAXES, IIObSalesOrderItemGeneralModel.TOTAL_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES,
        IIObSalesOrderItemGeneralModel.TOTAL_AMOUNT_AFTER_TAXES);
  }

  private Map<String, Object> getItemSummaryFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemSummaryResponse = getMapFromResponse(response);
    return readMapFromString(actualItemSummaryResponse, "ItemSummary");
  }

  public void assertThatSalesOrderApprovalDateIsCorrect(
      String salesOrderCode, String expectedApprovalDate) {
    Object actualApprovalDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderApprovalDate", salesOrderCode);
    assertDateEquals(
        expectedApprovalDate, new DateTime(actualApprovalDate).withZone(DateTimeZone.UTC));
  }

  public void assertThatItemHasTheFollowingSalesPrices(
      String itemCode, DataTable lastSalesPriceDataTable, Integer numberOfLastSalesPrices) {

    List<Map<String, String>> expectedLastSalesPriceList =
        lastSalesPriceDataTable.asMaps(String.class, String.class);

    assertEquals(
        "Number of LastSalesPrices is not correct",
        numberOfLastSalesPrices.intValue(),
        expectedLastSalesPriceList.size());

    for (Map<String, String> expectedLastSalesPrice : expectedLastSalesPriceList) {

      Object actualSalesPriceDate =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getItemLastSalesPrices",
              constructItemLastSalesPriceQueryParams(itemCode, expectedLastSalesPrice));

      assertNotNull("LastSalesPrices does not exist", actualSalesPriceDate);

      DateTime actualSalesPriceDateTime =
          new DateTime(actualSalesPriceDate).withZone(DateTimeZone.UTC);

      actualSalesPriceDateTime =
          actualSalesPriceDateTime.minusSeconds(actualSalesPriceDateTime.getSecondOfMinute());

      assertDateEquals(
          expectedLastSalesPrice.get(ISalesOrderFeatureFileCommonKeys.SALES_PRICE_DATE),
          actualSalesPriceDateTime);
    }
  }

  private Object[] constructItemLastSalesPriceQueryParams(
      String itemCode, Map<String, String> expectedLastSalesPrice) {
    return new Object[] {
      itemCode,
      expectedLastSalesPrice.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      expectedLastSalesPrice.get(IFeatureFileCommonKeys.CUSTOMER),
      expectedLastSalesPrice.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      new BigDecimal(expectedLastSalesPrice.get(ISalesOrderFeatureFileCommonKeys.SALES_PRICE)),
    };
  }

  public void assertThatSalesOrderCancellationDateIsCorrect(
      String salesOrderCode, String expectedCancellationDate) {
    Object actualCancellationDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderCancellationDate", salesOrderCode);
    assertDateEquals(
        expectedCancellationDate, new DateTime(actualCancellationDate).withZone(DateTimeZone.UTC));
  }

  public void assertThatTheFollowingSalesOrdersHaveNoCompanyAndStoreData(
      DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrdersList =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderRow : salesOrdersList) {
      Object salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObCompanyAndStoreDataBySalesOrderCode",
              salesOrderRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE));
      assertNull(salesOrderCode);
    }
  }

  public void assertThatTheFollowingSalesOrdersHaveNoItems(DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrdersList =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderRow : salesOrdersList) {
      Object salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesOrderItemBySalesOrderCode",
              salesOrderRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE));
      assertNull(salesOrderCode);
    }
  }

  public void assertThatItemTaxesResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualTaxesList = getTaxesFromResponse(response);
    assertEquals(0, actualTaxesList.size());
  }

  public void assertThatItemsResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualItemsList = getItemsFromResponse(response);
    assertEquals(0, actualItemsList.size());
  }
}
