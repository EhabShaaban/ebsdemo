package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewSalesOrderDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObSalesOrderViewSalesOrderDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObSalesOrderViewSalesOrderDataTestUtils utils;

  public IObSalesOrderViewSalesOrderDataStep() {
    Given(
        "^the following SalesOrderData for the following SalesOrders exist:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatTheFollowingSalesOrderDataExist(salesOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view SalesOrderData section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readSalesOrderData(cookie, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of SalesOrderData section for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable salesOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewSalesOrderDataResponseIsCorrect(response, salesOrderDataTable);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils =
                new IObSalesOrderViewSalesOrderDataTestUtils(
                        getProperties(), entityManagerDatabaseConnector);
    }

  @Before
  public void setup(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View SalesOrderData section")) {
          databaseConnector.createConnection();
          if (!hasBeenExecutedOnce) {
              databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
              hasBeenExecutedOnce = true;
          }
          databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
      }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
      if (contains(scenario.getName(), "View SalesOrderData section")) {
          databaseConnector.closeConnection();
      }
  }
}
