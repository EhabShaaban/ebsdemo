package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderDeleteItemsTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderRequestAddCancelItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderDeleteItemsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObSalesOrderDeleteItemsTestUtils utils;
  private IObSalesOrderRequestAddCancelItemTestUtils requestAddCancelItemTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSalesOrderDeleteItemsTestUtils(getProperties(), entityManagerDatabaseConnector);
    requestAddCancelItemTestUtils = new IObSalesOrderRequestAddCancelItemTestUtils(getProperties());
  }

  public IObSalesOrderDeleteItemsStep() {

    When(
        "^\"([^\"]*)\" requests to delete Item with id \"([^\"]*)\" in SalesOrder with code \"([^\"]*)\"$",
        (String userName, String itemId, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendDeleteRequest(
                  cookie, utils.getSalesOrderDeleteItemsURL(salesOrderCode, itemId));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Item with id \"([^\"]*)\" from SalesOrder with code \"([^\"]*)\" is deleted$",
        (String itemId, String salesOrderCode) -> {
          utils.assertThatSalesOrderItemIsDeletedById(itemId);
        });

    Given(
        "^\"([^\"]*)\" first deleted Item with id \"([^\"]*)\" of SalesOrder with code \"([^\"]*)\" successfully$",
        (String userName, String itemId, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          utils.sendDeleteRequest(
              cookie, utils.getSalesOrderDeleteItemsURL(salesOrderCode, itemId));
          utils.assertThatSalesOrderItemIsDeletedById(itemId);
        });

    Given(
        "^first \"([^\"]*)\" opens SalesOrderItems section of SalesOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String salesOrderCode) -> {
          String lockUrl = requestAddCancelItemTestUtils.getLockItemsSectionUrl(salesOrderCode);
          userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete Item from SalesOrder")) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete Item from SalesOrder")) {
      afterSetup();
    }
  }

  private void afterSetup() throws Exception {
    userActionsTestUtils.unlockAllSections(
        IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    databaseConnector.closeConnection();
  }
}
