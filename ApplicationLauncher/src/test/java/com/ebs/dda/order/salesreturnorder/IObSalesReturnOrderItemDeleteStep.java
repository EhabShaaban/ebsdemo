package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderItemDeleteTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewItemsDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderItemDeleteStep extends SpringBootRunner implements En {

  private IObSalesReturnOrderItemDeleteTestUtils utils;
  private IObSalesReturnOrderViewItemsDataTestUtils viewItemsDataTestUtils;

  public IObSalesReturnOrderItemDeleteStep() {

    When(
        "^\"([^\"]*)\" requests to delete SROItem with id (\\d+) in SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, Integer itemId, String salesReturnOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesReturnOrderItemRestUrl(salesReturnOrderCode, itemId);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SROItem with id (\\d+) from SalesReturnOrder with code \"([^\"]*)\" is deleted$",
        (Integer itemId, String salesReturnOrderCode) -> {
          utils.assertThatSalesReturnOrderItemIsDeletedSuccessfully(salesReturnOrderCode, itemId);
        });

    Given(
        "^\"([^\"]*)\" first deleted SROItem with id (\\d+) of SalesReturnOrder with code \"([^\"]*)\" successfully$",
        (String userName, Integer itemId, String salesReturnOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesReturnOrderItemRestUrl(salesReturnOrderCode, itemId);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatSalesReturnOrderItemIsDeletedSuccessfully(salesReturnOrderCode, itemId);
        });

    Then(
        "^Only the following Items remaining in SalesReturnOrder with code \"([^\"]*)\":$",
        (String salesReturnOrderCode, DataTable itemDataTable) -> {
          viewItemsDataTestUtils.assertThatSalesReturnOrderItemDataExists(itemDataTable);
          utils.assertThatTheseAreTheOnlyItemsExist(salesReturnOrderCode, itemDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderItemDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
    viewItemsDataTestUtils =
        new IObSalesReturnOrderViewItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete SROItem from SalesReturnOrder,")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete SROItem from SalesReturnOrder,")) {
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObSalesReturnOrderRestURLs.COMMAND + "/",
          IDObSalesReturnOrderRestURLs.UNLOCK_ALL_SECTION,
          utils.getSectionsNames());
    }
  }
}
