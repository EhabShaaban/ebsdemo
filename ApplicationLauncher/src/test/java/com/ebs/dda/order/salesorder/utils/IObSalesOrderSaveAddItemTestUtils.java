package com.ebs.dda.order.salesorder.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderItemValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IExpectedKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class IObSalesOrderSaveAddItemTestUtils extends IObSalesOrderItemsCommonTestUtils {

  public IObSalesOrderSaveAddItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public Response saveAddItem(Cookie cookie, String salesOrderCode, DataTable newItemDataTable) {
    JsonObject newItemJsonObject = constructAddItemValueObject(newItemDataTable);
    return sendPUTRequest(cookie, getSaveNewItemUrl(salesOrderCode), newItemJsonObject);
  }

  private JsonObject constructAddItemValueObject(DataTable newItemDataTable) {
    JsonObject newItemValueObject = new JsonObject();

    Map<String, String> newItemMap = newItemDataTable.asMaps(String.class, String.class).get(0);

    newItemValueObject.addProperty(
        IIObSalesOrderItemValueObject.ITEM_CODE,
        getFirstValue(newItemMap, IFeatureFileCommonKeys.ITEM));

    newItemValueObject.addProperty(
        IIObSalesOrderItemValueObject.UNIT_OF_MEASURE_CODE,
        getFirstValue(newItemMap, IFeatureFileCommonKeys.ITEM_ORDER_UNIT));

    parseBigDicimalIfPossible(
        newItemValueObject, newItemMap, IIObSalesOrderItemValueObject.QUANTITY, IExpectedKeys.QTY);

    parseBigDicimalIfPossible(
        newItemValueObject,
        newItemMap,
        IIObSalesOrderItemValueObject.SALES_PRICE,
        ISalesOrderFeatureFileCommonKeys.SALES_PRICE);

    return newItemValueObject;
  }

  private void parseBigDicimalIfPossible(
      JsonObject newItemValueObject,
      Map<String, String> newItemMap,
      String property,
      String valueKey) {
    try {
      newItemValueObject.addProperty(property, new BigDecimal(newItemMap.get(valueKey)));
    } catch (NumberFormatException ex) {
      newItemValueObject.addProperty(property, newItemMap.get(valueKey));
    }
  }

  private String getSaveNewItemUrl(String salesOrderCode) {
    StringBuilder url = new StringBuilder();
    url.append(IDObSalesOrderRestURLs.COMMAND)
        .append("/")
        .append(salesOrderCode)
        .append(IDObSalesOrderRestURLs.UPDATE_ITEMS);
    return url.toString();
  }

  public void assertThatSalesOrderIsUpdated(String salesOrderCode, DataTable salesOrderDataTable) {
    Map<String, String> salesOrderMap =
        salesOrderDataTable.asMaps(String.class, String.class).get(0);

    Object salesOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getModifiedSalesOrder",
            constructModifiedSalesOrderQueryParams(salesOrderCode, salesOrderMap));

    assertNotNull(
        "[SO][Save][Item] SalesOrder is not updated successfully", salesOrderModifiedDate);

    assertDateEquals(
        salesOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(salesOrderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] constructModifiedSalesOrderQueryParams(
      String salesOrderCode, Map<String, String> salesOrderMap) {
    return new Object[] {salesOrderCode, salesOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)};
  }
}
