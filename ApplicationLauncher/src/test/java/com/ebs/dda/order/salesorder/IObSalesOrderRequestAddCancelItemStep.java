package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderRequestAddCancelItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class IObSalesOrderRequestAddCancelItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObSalesOrderRequestAddCancelItemTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new IObSalesOrderRequestAddCancelItemTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public IObSalesOrderRequestAddCancelItemStep() {
    When(
        "^\"([^\"]*)\" requests to add Item to SalesOrderItems section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          String lockUrl = utils.getLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item dialoge is opened and SalesOrderItems section of SalesOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesOrderCode,
              IDObSalesOrderSectionNames.ITEMS_SECTION,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^\"([^\"]*)\" first requested to add Item to SalesOrderItems section of SalesOrder with code \"([^\"]*)\" successfully$",
        (String userName, String salesOrderCode) -> {
          String lockUrl = utils.getLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrderItems section of SalesOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesOrderCode,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesOrderSectionNames.ITEMS_SECTION);
        });

    Given(
        "^SalesOrderItems section of SalesOrder with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = utils.getLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lock_url, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesOrderItems section of SalesOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockUrl = utils.getUnLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on SalesOrderItems section of SalesOrder with code \"([^\"]*)\" is released$",
        (String userName, String salesOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesOrderCode,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesOrderItems section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          String unLockUrl = utils.getUnLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), "Request to Add Item to SalesOrder")
        || contains(scenario.getName(), "Request to Cancel saving SalesOrder Items section")) {
      beforeSetup();
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Add Item to SalesOrder")
        || contains(scenario.getName(), "Request to Cancel saving SalesOrder Items section")) {
      afterSetup();
    }
  }

  private void beforeSetup() throws Exception {
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      hasBeenExecutedOnce = true;
    }
    databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
  }

  private void afterSetup() throws Exception {
    utils.unfreeze();
    userActionsTestUtils.unlockAllSections(
        IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    databaseConnector.closeConnection();
  }
}
