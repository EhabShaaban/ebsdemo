package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderReadAllTypesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesReturnOrderReadAllTypesStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderReadAllTypesTestUtils utils;

  public DObSalesReturnOrderReadAllTypesStep() {
    Given(
        "^the following SalesReturnOrderTypes exist:$",
        (DataTable soTypeDataTable) -> {
          utils.assertThatSalesReturnOrderTypesExist(soTypeDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all SalesReturnOrderTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllSalesReturnOrderTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesReturnOrderTypes values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesReturnOrderTypeDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllSalesReturnOrderTypesResponseIsCorrect(
              response, salesReturnOrderTypeDataTable);
        });

    Then(
        "^total number of SalesReturnOrderTypes returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalNumberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
        });

    And(
        "^the total number of existing SalesReturnOrderTypes is (\\d+)$",
        (Long numberOfSalesReturnOrderTypes) -> {
          this.utils.assertThatTotalNumberOfSalesReturnOrderTypesIsCorrect(
              numberOfSalesReturnOrderTypes);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesReturnOrderReadAllTypesTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }
}
