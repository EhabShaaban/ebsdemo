package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObSalesOrderViewAllTestUtils extends DObSalesOrderCommonTestUtils {

  public DObSalesOrderViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    return str.toString();
  }

  public void assertThatTotalNumberOfSalesOrdersEquals(Long totalNumberOfRecords) {
    Object expectedTotalNumberOfSalesOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getTotalNumberOfSalesOrderViewAll");

    assertEquals(totalNumberOfRecords, expectedTotalNumberOfSalesOrder);
  }

  public void assertSalesOrderDataIsCorrect(DataTable salesOrderDataTable, Response response)
      throws Exception {
    List<Map<String, String>> expectedSalesOrderList =
        salesOrderDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesOrderResponse = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedSalesOrderList.size(); i++) {
      assertCorrectShownData(actualSalesOrderResponse.get(i), expectedSalesOrderList.get(i));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> actualSalesOrderMap, Map<String, String> expectedSalesOrderMap)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedSalesOrderMap, actualSalesOrderMap);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSalesOrderGeneralModel.USER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_CODE,
        IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.CUSTOMER,
        IDObSalesOrderGeneralModel.CUSTOMER_CODE,
        IDObSalesOrderGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE,
        IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_CODE,
        IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME);

    mapAssertion.assertDateTime(
        ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE,
        IDObSalesOrderGeneralModel.EXPECTED_DELIVERY_DATE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesOrderGeneralModel.CURRENT_STATES);
  }
}
