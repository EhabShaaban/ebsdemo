package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.ICObSalesOrderType;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObSalesOrderReadAllTypesTestUtils extends DObSalesOrderCreateTestUtils {

  public DObSalesOrderReadAllTypesTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatSalesOrderTypesExist(DataTable soTypeDataTable) {
    List<Map<String, String>> salesOrderTypesList =
        soTypeDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderTypeRow : salesOrderTypesList) {
      Object salesOrderType =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesOrderTypeByCodeAndName",
              constructSalesOrderTypeQueryParams(salesOrderTypeRow));
      assertNotNull(salesOrderType);
    }
  }

  private Object[] constructSalesOrderTypeQueryParams(Map<String, String> salesOrderTypeRow) {
    return new Object[] {
      salesOrderTypeRow.get(IFeatureFileCommonKeys.CODE),
      salesOrderTypeRow.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public Response readAllSalesOrderTypes(Cookie cookie) {
    String restURL = IDObSalesOrderRestURLs.READ_ALL_SALES_ORDER_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllSalesOrderTypesResponseIsCorrect(
      Response response, DataTable salesOrderTypeDataTable) throws Exception {

    List<Map<String, String>> expectedSalesOrderTypes =
        salesOrderTypeDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualSalesOrderTypes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesOrderTypes.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(expectedSalesOrderTypes.get(i), actualSalesOrderTypes.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IOrderFeatureFileCommonKeys.SALES_ORDER_TYPE,
          ICObSalesOrderType.USER_CODE,
          ICObSalesOrderType.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {

    List<HashMap<String, Object>> salesOrderTypes = getListOfMapsFromResponse(response);

    Assert.assertEquals(totalNumberOfRecords.intValue(), salesOrderTypes.size());
  }

  public void assertThatTotalNumberOfSalesOrderTypesIsCorrect(
      Long expectedNumberOfSalesOrderTypes) {
    Object actualNumberOfSalesOrderTypes =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderTypesCount");
    assertEquals(expectedNumberOfSalesOrderTypes, actualNumberOfSalesOrderTypes);
  }
}
