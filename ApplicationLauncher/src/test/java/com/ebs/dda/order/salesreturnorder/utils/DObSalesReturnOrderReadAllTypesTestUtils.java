package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesreturnorder.ICObSalesReturnOrderType;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCreateTestUtils;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObSalesReturnOrderReadAllTypesTestUtils extends DObSalesOrderCreateTestUtils {

  protected String ASSERTION_MSG = "[ReadAllTypes]";

  public DObSalesReturnOrderReadAllTypesTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatSalesReturnOrderTypesExist(DataTable sroTypeDataTable) {
    List<Map<String, String>> salesReturnOrderTypesList =
        sroTypeDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnOrderTypeRow : salesReturnOrderTypesList) {
      Object salesReturnOrderType =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesReturnOrderTypeByCodeAndName",
              constructSalesReturnOrderTypeQueryParams(salesReturnOrderTypeRow));
      assertNotNull(ASSERTION_MSG + "Sales Return Order Type is not exist", salesReturnOrderType);
    }
  }

  private Object[] constructSalesReturnOrderTypeQueryParams(
      Map<String, String> salesReturnOrderTypeRow) {
    return new Object[] {
      salesReturnOrderTypeRow.get(IFeatureFileCommonKeys.CODE),
      salesReturnOrderTypeRow.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public Response readAllSalesReturnOrderTypes(Cookie cookie) {
    String restURL = IDObSalesReturnOrderRestURLs.READ_ALL_SALES_RETURN_ORDER_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllSalesReturnOrderTypesResponseIsCorrect(
      Response response, DataTable salesReturnOrderTypeDataTable) throws Exception {

    List<Map<String, String>> expectedSalesReturnOrderTypes =
        salesReturnOrderTypeDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualSalesReturnOrderTypes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesReturnOrderTypes.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(
              expectedSalesReturnOrderTypes.get(i), actualSalesReturnOrderTypes.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IOrderFeatureFileCommonKeys.SALES_RETURN_ORDER_TYPE,
          ICObSalesReturnOrderType.USER_CODE,
          ICObSalesReturnOrderType.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {

    List<HashMap<String, Object>> salesOrderTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " total Number Of Records is not correct",
        totalNumberOfRecords.intValue(),
        salesOrderTypes.size());
  }

  public void assertThatTotalNumberOfSalesReturnOrderTypesIsCorrect(
      Long expectedNumberOfSalesReturnOrderTypes) {
    Object actualNumberOfSalesReturnOrderTypes =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderTypesCount");
    assertEquals(
        ASSERTION_MSG + " total Number Of Records is not correct",
        expectedNumberOfSalesReturnOrderTypes,
        actualNumberOfSalesReturnOrderTypes);
  }
}
