package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.IObCustomerAddressesReadAllTestUtils;
import com.ebs.dda.masterdata.customer.utils.IObCustomerContactPersonReadAllTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderSaveSalesOrderDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderSaveSalesOrderDataHappyPathStep extends SpringBootRunner implements En {
  private IObSalesOrderSaveSalesOrderDataTestUtils utils;
  private IObCustomerAddressesReadAllTestUtils customerAddressesReadAllTestUtils;
  private IObCustomerContactPersonReadAllTestUtils contactPersonReadAllTestUtils;

  public IObSalesOrderSaveSalesOrderDataHappyPathStep() {

    Given(
        "^the following Addresses for Customers exist:$",
        (DataTable customersAddressesDataTable) -> {
          customerAddressesReadAllTestUtils
              .assertThatTheFollowingCustomersHaveTheFollowingAddresses(
                  customersAddressesDataTable);
        });

    Given(
        "^the following ContactPersons for Customers exist:$",
        (DataTable customerContactPersonDataTable) -> {
          contactPersonReadAllTestUtils
              .assertThatTheFollowingCustomersHaveTheFollowingContactPersons(
                  customerContactPersonDataTable);
        });

    When(
        "^\"([^\"]*)\" saves SalesOrderData section of SalesOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String salesOrderCode,
            String dateTime,
            DataTable salesOrderDataDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveSalesOrderDataSection(salesOrderCode, salesOrderDataDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrderData section of SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable salesOrderDataDataTable) -> {
          utils.assertThatSalesOrderDataIsUpdatedAsFollows(salesOrderCode, salesOrderDataDataTable);
        });

      Then("^SalesOrderData section of SalesOrder with Code \"([^\"]*)\" remains as follows:$", (String salesOrderCode, DataTable salesOrderDataDataTable) -> {
          utils.assertThatSalesOrderDataIsUpdatedAsFollows(salesOrderCode, salesOrderDataDataTable);
      });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSalesOrderSaveSalesOrderDataTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    customerAddressesReadAllTestUtils =
        new IObCustomerAddressesReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    contactPersonReadAllTestUtils =
        new IObCustomerContactPersonReadAllTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save SalesOrderData section")) {
      databaseConnector.executeSQLScript(utils.getDbClearScriptsExecution());
    }
  }

  @After
  public void afterSetup(Scenario scenario) {
    if (contains(scenario.getName(), "Save SalesOrderData section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
