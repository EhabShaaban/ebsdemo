package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderCompanyAndStoreDataGeneralModel;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class IObSalesOrderViewCompanyStoreDataTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderViewCompanyStoreDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    return str.toString();
  }

  public Response readCompanyAndStoreData(Cookie cookie, String salesOrderCode) {
    return sendGETRequest(cookie, getViewCompanyAndStoreDataUrl(salesOrderCode));
  }

  private String getViewCompanyAndStoreDataUrl(String salesOrderCode) {
    return IDObSalesOrderRestURLs.QUERY
        + "/"
        + salesOrderCode
        + IDObSalesOrderRestURLs.VIEW_COMPANY_AND_STORE_DATA;
  }

  public void assertThatViewCompanyAndStoreDataResponseIsCorrect(
      String salesOrderCode, Response response, DataTable companyStoreDataTable) throws Exception {
    Map<String, String> expectedCompanyStoreData =
        companyStoreDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualCompanyStoreData = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expectedCompanyStoreData, actualCompanyStoreData);

    mapAssertion.assertValue(
        ISalesOrderFeatureFileCommonKeys.SO_CODE,
        IIObSalesOrderCompanyAndStoreDataGeneralModel.SALES_ORDER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IIObSalesOrderCompanyAndStoreDataGeneralModel.COMPANY_CODE,
        IIObSalesOrderCompanyAndStoreDataGeneralModel.COMPANY_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.STORE,
        IIObSalesOrderCompanyAndStoreDataGeneralModel.STORE_CODE,
        IIObSalesOrderCompanyAndStoreDataGeneralModel.STORE_NAME);
  }
}
