package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCreateTestUtils;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ItemOrderUnitsTestUtils extends DObSalesOrderCreateTestUtils {

  public ItemOrderUnitsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertNumberOfItemOrderUnitsMatchesExpected(
      String item, String salesReturnCode, Integer orderUnitsNumber) {

    Long itemOrderUnitsNumber =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getItemOrderUnitsCountBySalesReturnOrderCodeAndItemCode",
                constructItemOrderUnitQueryParams(salesReturnCode, item));
    assertEquals(
        "OrderUnits of Item " + item + " doesn't match expected!",
        Long.valueOf(orderUnitsNumber),
        itemOrderUnitsNumber);
  }

  private Object[] constructItemOrderUnitQueryParams(String salesReturnCode, String item) {
    return new Object[] {salesReturnCode, item.split(" - ")[0]};
  }

  public Response readItemOrderUnits(Cookie cookie, String salesReturnCode, String itemCode) {
    String restURL =
        IDObSalesReturnOrderRestURLs.READ_ITEM_ORDER_UNITS + salesReturnCode + "/" + itemCode;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatItemOrderUnitsResponseIsCorrect(
      Response response, DataTable itemOrderUnitsDataTable) throws Exception {
    List<Map<String, String>> expectedItemOrderUnits =
        itemOrderUnitsDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualItemOrderUnits = getListOfMapsFromResponse(response);

    if (actualItemOrderUnits.size() == expectedItemOrderUnits.size()) {
      for (int i = 0; i < expectedItemOrderUnits.size(); i++) {
        assertOrderUnitMatchesExpected(expectedItemOrderUnits.get(i), actualItemOrderUnits.get(i));
      }
    }
  }

  private void assertOrderUnitMatchesExpected(
      Map<String, String> expectedItemOrderUnits, HashMap<String, Object> actualItemOrderUnits)
      throws Exception {
    String itemOrderUnit = expectedItemOrderUnits.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT);
    assertEquals(
        itemOrderUnit.split(" - ")[0],
        actualItemOrderUnits.get(IIObSalesReturnOrderItemGeneralModel.ORDER_UNIT_CODE));
    assertEquals(
        itemOrderUnit.split(" - ")[1],
        getEnglishValue(
            actualItemOrderUnits.get(IIObSalesReturnOrderItemGeneralModel.ORDER_UNIT_SYMBOL)));
  }
}
