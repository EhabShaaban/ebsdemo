package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderReadAllTypesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesOrderReadAllTypesStep extends SpringBootRunner implements En {

  private DObSalesOrderReadAllTypesTestUtils utils;

  public DObSalesOrderReadAllTypesStep() {
    Given(
        "^the following SalesOrderTypes exist:$",
        (DataTable soTypeDataTable) -> {
          utils.assertThatSalesOrderTypesExist(soTypeDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all SalesOrderTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllSalesOrderTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesOrderTypes values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesOrderTypeDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllSalesOrderTypesResponseIsCorrect(
              response, salesOrderTypeDataTable);
        });

    Then(
        "^total number of SalesOrderTypes returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalNumberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
        });

    And(
        "^the total number of existing SalesOrderTypes is (\\d+)$",
        (Long numberOfSalesOrderTypes) -> {
          this.utils.assertThatTotalNumberOfSalesOrderTypesIsCorrect(numberOfSalesOrderTypes);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObSalesOrderReadAllTypesTestUtils(getProperties(), entityManagerDatabaseConnector);
    }

  @Before
  public void setup(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Read list of SalesOrderTypes dropdown")) {
          databaseConnector.createConnection();
          databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
      if (contains(scenario.getName(), "Read list of SalesOrderTypes dropdown")) {
          databaseConnector.closeConnection();
      }
  }
}
