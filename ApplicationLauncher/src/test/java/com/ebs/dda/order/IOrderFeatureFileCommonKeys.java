package com.ebs.dda.order;

public interface IOrderFeatureFileCommonKeys {
  String TYPE = "Type";
  String SALES_RESPONSIBLE = "SalesResponsible";
  String DOCUMENT_OWNER = "DocumentOwner";
  String SALES_ORDER_TYPE = "SalesOrderType";
  String DOCUMENT_OWNER_ID = "DocumentOwnerId";
  String SALES_RETURN_ORDER_TYPE = "SalesReturnOrderType";
  String ITEM_ID = "ItemId";
  String PO_ITEM_ID = "POItemId";
  String ORDER_UNIT = "OrderUnit";
  String QUANTITY = "Quantity";
  String PRICE = "Price";
  String SRO_ITEM_ID = "SROItemId";
  String SALES_PRICE = "SalesPrice";
  String ITEM_TOTAL_AMOUNT = "ItemTotalAmount";
  String RETURN_REASON = "ReturnReason";
  String REFERENCE_PURCHASE_ORDER = "ReferencePurchaseOrder";

  String TOTAL_AMOUNT_BEFORE_TAXES = "TotalAmountBeforeTaxes";
  String TOTAL_TAXES = "TotalTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "TotalAmountAfterTaxes";

  String CURRENCY_ISO = "CurrencyISO";
  String PAYMENT_TERMS = "PaymentTerms";
}
