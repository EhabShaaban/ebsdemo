package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.storehouse.utils.CObStorehouseDropdownTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderSaveCompanyAndStoreTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderSaveCompanyAndStoreStep extends SpringBootRunner implements En {
  private IObSalesOrderSaveCompanyAndStoreTestUtils utils;
  private CObStorehouseDropdownTestUtils storehouseDropdownTestUtils;

  public IObSalesOrderSaveCompanyAndStoreStep() {

    Given(
        "^the following Plants exist for Company \"([^\"]*)\":$",
        (String companyCodeName, DataTable plantsDataTable) -> {
          String companyCode = utils.getCodeFromCodeName(companyCodeName);
          storehouseDropdownTestUtils.assertThatCompanyHasTheFollowingPlants(
              companyCode, plantsDataTable);
        });

    Given(
        "^the following Storehouses exist for Plant \"([^\"]*)\":$",
        (String storehouseCodeName, DataTable storehousesDataTable) -> {
          String storehouseCode = utils.getCodeFromCodeName(storehouseCodeName);
          storehouseDropdownTestUtils.assertThatPlantHasTheFollowingStorehouses(
              storehouseCode, storehousesDataTable);
        });

    When(
        "^\"([^\"]*)\" saves CompanyAndStore section of SalesOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String salesOrderCode,
            String dateTime,
            DataTable companyStoreDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveCompanyAndStoreData(salesOrderCode, companyStoreDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^CompanyAndStore section of SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable companyStoreDataTable) -> {
          utils.assertThatCompanyAndStoreDataIsUpdatedAsFollows(
              salesOrderCode, companyStoreDataTable);
        });

    Then(
        "^CompanyAndStore section of SalesOrder with Code \"([^\"]*)\" remains as follows:$",
        (String salesOrderCode, DataTable companyStoreDataTable) -> {
          utils.assertThatCompanyAndStoreDataIsUpdatedAsFollows(
              salesOrderCode, companyStoreDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSalesOrderSaveCompanyAndStoreTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    storehouseDropdownTestUtils =
        new CObStorehouseDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save CompanyAndStore section")) {
      databaseConnector.executeSQLScript(utils.getDbClearScriptsExecution());
    }
  }

  @After
  public void afterSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save CompanyAndStore section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
