package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderSectionNames;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObSalesReturnOrderCommonTestUtils extends CommonTestUtils {

  protected final String ASSERTION_MSG = "[SRO]";

  public DObSalesReturnOrderCommonTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String clearSalesReturnOrder() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/Sales_Invoice_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }

  public void createSalesReturnOrderGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesReturnOrderGeneralData",
          constructInsertSalesReturnOrderGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertSalesReturnOrderGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.STATE),
      generalDataMap.get(IOrderFeatureFileCommonKeys.DOCUMENT_OWNER),
      generalDataMap.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  public void createSalesReturnOrderCompanyData(DataTable companyDataTable) {
    List<Map<String, String>> companyDataListMap =
        companyDataTable.asMaps(String.class, String.class);

    for (Map<String, String> companyDataMap : companyDataListMap) {
      String salesReturnOrderCode = companyDataMap.get(IFeatureFileCommonKeys.CODE);
      createTheFollowingSalesReturnOrdersWithCompanyData(salesReturnOrderCode, companyDataMap);
    }
  }

  public void createTheFollowingSalesReturnOrdersWithCompanyData(
      String salesReturnOrderCode, Map<String, String> companyDataMap) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "createSalesReturnOrderCompany",
        constructInsertSalesReturnOrderCompanyDataQueryParams(
            salesReturnOrderCode, companyDataMap));
  }

  private Object[] constructInsertSalesReturnOrderCompanyDataQueryParams(
      String salesReturnOrderCode, Map<String, String> companyDataMap) {
    return new Object[] {
      salesReturnOrderCode,
      companyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      companyDataMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void createSalesReturnOrderReturnDetails(DataTable returnDetailsTable) {
    List<Map<String, String>> returnDetailsMaps =
        returnDetailsTable.asMaps(String.class, String.class);
    for (Map<String, String> returnDetailsMap : returnDetailsMaps) {
      String salesReturnOrderCode = returnDetailsMap.get(IFeatureFileCommonKeys.CODE);
      createSalesReturnDetails(salesReturnOrderCode, returnDetailsMap);
    }
  }

  public void createSalesReturnDetails(
      String salesReturnOrderCode, Map<String, String> returnDetailsMap) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "createSalesReturnOrderReturnDetails",
        constructInsertSalesReturnOrderReturnDetailsQueryParams(
            salesReturnOrderCode, returnDetailsMap));
  }

  private Object[] constructInsertSalesReturnOrderReturnDetailsQueryParams(
      String salesReturnOrderCode, Map<String, String> returnDetailsTable) {
    return new Object[] {
      salesReturnOrderCode,
      returnDetailsTable.get(ISalesFeatureFileCommonKeys.SI_CODE),
      returnDetailsTable.get(IFeatureFileCommonKeys.CUSTOMER),
      returnDetailsTable.get(ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO)
    };
  }

  public void createSalesReturnOrderItemsWithId(DataTable itemsTable) {
    List<Map<String, String>> itemsMaps = itemsTable.asMaps(String.class, String.class);
    for (Map<String, String> itemsMap : itemsMaps) {
      createItemsWithId(itemsMap);
    }
  }

  public void createItemsWithId(Map<String, String> itemsMap) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "createSalesReturnOrderItemsWithId",
        constructInsertSalesReturnOrderItemsQueryParamsWithId(itemsMap));
  }

  private Object[] constructInsertSalesReturnOrderItemsQueryParamsWithId(
      Map<String, String> itemsTable) {
    return new Object[] {
      itemsTable.get(IOrderFeatureFileCommonKeys.SRO_ITEM_ID),
      itemsTable.get(IFeatureFileCommonKeys.CODE),
      itemsTable.get(IFeatureFileCommonKeys.ITEM),
      itemsTable.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemsTable.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY),
      itemsTable.get(IOrderFeatureFileCommonKeys.SALES_PRICE),
      itemsTable.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON)
    };
  }

  public void createSalesReturnOrderItems(DataTable itemsTable) {
    List<Map<String, String>> itemsMaps = itemsTable.asMaps(String.class, String.class);
    for (Map<String, String> itemsMap : itemsMaps) {
      createItems(itemsMap);
    }
  }

  private void createItems(Map<String, String> itemsMap) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "createSalesReturnOrderItems", constructInsertSalesReturnOrderItemsQueryParams(itemsMap));
  }

  private Object[] constructInsertSalesReturnOrderItemsQueryParams(Map<String, String> itemsTable) {
    return new Object[] {
      itemsTable.get(IFeatureFileCommonKeys.CODE),
      itemsTable.get(IFeatureFileCommonKeys.ITEM),
      itemsTable.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemsTable.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY),
      itemsTable.get(IOrderFeatureFileCommonKeys.SALES_PRICE),
      itemsTable.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON)
    };
  }

  public void createSalesReturnOrderTaxes(DataTable taxesTable) {
    List<Map<String, String>> taxesMaps = taxesTable.asMaps(String.class, String.class);
    for (Map<String, String> taxesMap : taxesMaps) {
      String salesReturnOrderCode = taxesMap.get(IFeatureFileCommonKeys.CODE);
      createTaxes(salesReturnOrderCode, taxesMap);
    }
  }

  public void createTaxes(String salesReturnOrderCode, Map<String, String> taxesMap) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "createSalesReturnOrderTaxes",
        constructInsertSalesReturnOrderTaxesQueryParams(salesReturnOrderCode, taxesMap));
  }

  private Object[] constructInsertSalesReturnOrderTaxesQueryParams(
      String salesReturnOrderCode, Map<String, String> taxesTable) {
    return new Object[] {
      salesReturnOrderCode,
      taxesTable.get(ISalesReturnOrderFeatureFileCommonKeys.TAX),
      taxesTable.get(ISalesReturnOrderFeatureFileCommonKeys.TAX),
      taxesTable.get(ISalesReturnOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(Arrays.asList(IDObSalesReturnOrderSectionNames.ITEMS_SECTION));
  }

  public void assertThatSalesOrderStateAndModificationInfoIsUpdated(
      String salesReturnOrderCode, DataTable salesReturnOrderDataTable) {
    Map<String, String> expectedSalesReturnOrderMap =
        salesReturnOrderDataTable.asMaps(String.class, String.class).get(0);

    Object actualSalesReturnOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderWithUpdatedState",
            getSalesReturnOrderWithUpdatedStateQueryParams(
                salesReturnOrderCode, expectedSalesReturnOrderMap));

    assertNotNull(
        ASSERTION_MSG + " Sales Return Order " + salesReturnOrderCode + " State is not Updated",
        actualSalesReturnOrderModifiedDate);

    assertDateEquals(
        expectedSalesReturnOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(actualSalesReturnOrderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getSalesReturnOrderWithUpdatedStateQueryParams(
      String salesReturnOrderCode, Map<String, String> expectedSalesReturnOrderMap) {
    return new Object[] {
      salesReturnOrderCode,
      expectedSalesReturnOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      "%" + expectedSalesReturnOrderMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatSalesOrderStateIsUpdated(String salesReturnOrderCode, String state) {
    Object actualSalesReturnOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderWithCodeAndUpdatedState",
            getSalesReturnOrderWithStateQueryParams(salesReturnOrderCode, state));

    assertNotNull(
        ASSERTION_MSG + " Sales Return Order " + salesReturnOrderCode + " State is not Updated",
        actualSalesReturnOrderModifiedDate);
  }

  private Object[] getSalesReturnOrderWithStateQueryParams(
      String salesReturnOrderCode, String state) {
    return new Object[] {salesReturnOrderCode, "%" + state + "%"};
  }

  public void assertThatTheseAreTheOnlyItemsExist(
      String salesReturnOrderCode, DataTable itemDataTable) {
    List<Map<String, String>> expectedSalesReturnItemsList =
        itemDataTable.asMaps(String.class, String.class);
    Object actualSalesReturnOrderItemsCount =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCountIObSalesReturnOrderItemBySalesReturnOrderCode", salesReturnOrderCode);
    assertEquals(
        "These are other items exist in sales return order",
        (long) expectedSalesReturnItemsList.size(),
        actualSalesReturnOrderItemsCount);
  }

  public void assertThatSalesReturnOrderSummaryExist(DataTable salesReturnOrderSummaryTable) {
    List<Map<String, String>> salesReturnItemsSummaryMaps =
        salesReturnOrderSummaryTable.asMaps(String.class, String.class);

    for (Map<String, String> salesReturnItemsSummaryMap : salesReturnItemsSummaryMaps) {
      Object salesReturnItemSummaryGeneralModel =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesReturnOrderSummary",
              constructItemsSummaryQueryParams(salesReturnItemsSummaryMap));
      assertNotNull(
          "Sales Return Order Summary with code "
              + salesReturnItemsSummaryMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist!",
          salesReturnItemSummaryGeneralModel);
    }
  }

  private Object[] constructItemsSummaryQueryParams(
      Map<String, String> salesReturnItemsSummaryMap) {
    return new Object[] {
      salesReturnItemsSummaryMap.get(IFeatureFileCommonKeys.CODE),
      salesReturnItemsSummaryMap.get(IOrderFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES),
      salesReturnItemsSummaryMap.get(IOrderFeatureFileCommonKeys.TOTAL_TAXES),
      salesReturnItemsSummaryMap.get(IOrderFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES)
    };
  }

  public void assertThatSalesInvoiceHasItem(
      String salesInvoiceCode, Map<String, String> salesInvoiceItem) {
    IObSalesInvoiceItemsGeneralModel salesInvoiceItemsGeneralModel =
        (IObSalesInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesItemsByReturnedQuantity",
                constructSalesInvoiceItems(salesInvoiceCode, salesInvoiceItem));
    assertNotNull(
        "Sales Invoice Item does not exist " + salesInvoiceItem, salesInvoiceItemsGeneralModel);
  }

  public Object[] constructSalesInvoiceItems(
      String invoiceCode, Map<String, String> salesInvoiceItems) {
    return new Object[] {
      invoiceCode,
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ITEM),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.QUANTITY),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.RETURN_QUANTITY),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.SALES_PRICE),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.TOTAL_AMOUNT)
    };
  }
}
