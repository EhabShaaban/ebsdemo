package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewCompanyStoreDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObSalesOrderViewCompanyStoreDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObSalesOrderViewCompanyStoreDataTestUtils utils;

  public IObSalesOrderViewCompanyStoreDataStep() {

    Given(
        "^the following CompanyAndStoreData for the following SalesOrders exist:$",
        (DataTable companyStoreDataTable) -> {
          utils.assertThatTheFollowingCompanyAndStoreDataExist(companyStoreDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view CompanyAndStoreData section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readCompanyAndStoreData(cookie, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of CompanyAndStoreData section for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable companyStoreDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewCompanyAndStoreDataResponseIsCorrect(
              salesOrderCode, response, companyStoreDataTable);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
    utils =
            new IObSalesOrderViewCompanyStoreDataTestUtils(
                    getProperties(), entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View CompanyAndStoreData section")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
        }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
      if (contains(scenario.getName(), "View CompanyAndStoreData section")) {
          databaseConnector.closeConnection();
      }
  }
}
