package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemValueObject;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObSalesReturnOrderSaveEditItemTestUtils extends DObSalesReturnOrderCommonTestUtils {

  public String SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME = "SalesReturnOrderEntityLockCommand";

  public IObSalesReturnOrderSaveEditItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getLockItemsSectionUrl(String salesReturnOrderCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesReturnOrderRestURLs.COMMAND);
    lockUrl.append("/");
    lockUrl.append(salesReturnOrderCode);
    lockUrl.append(IDObSalesReturnOrderRestURLs.LOCK_ITEM_SECTION);
    return lockUrl.toString();
  }

  public Response saveItem(
      Cookie cookie, String salesReturnOrderCode, Integer itemId, DataTable itemDataTable) {
    JsonObject item = createSROItemJsonObject(itemDataTable);
    String restURL =
        IDObSalesReturnOrderRestURLs.COMMAND
            + '/'
            + salesReturnOrderCode
            + IDObSalesReturnOrderRestURLs.SAVE_EDIT_ITEM_SECTION
            + itemId;
    return sendPUTRequest(cookie, restURL, item);
  }

  private JsonObject createSROItemJsonObject(DataTable itemDataTable) {
    List<Map<String, String>> itemsData = itemDataTable.asMaps(String.class, String.class);

    JsonObject sroItemObject = new JsonObject();
    for (Map<String, String> itemData : itemsData) {
      try {
        sroItemObject.addProperty(
            IIObSalesReturnOrderItemValueObject.RETURN_QUANTITY,
            getBigDecimal(itemData.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY)));
      } catch (NumberFormatException ex) {
        sroItemObject.addProperty(
            IIObSalesReturnOrderItemValueObject.RETURN_QUANTITY,
            itemData.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY));
      }

      sroItemObject.addProperty(
          IIObSalesReturnOrderItemValueObject.RETURN_REASON_CODE,
          getFirstValue(itemData, ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON));
    }
    return sroItemObject;
  }

  private String getFirstValue(Map<String, String> itemDataMap, String key) {
    return itemDataMap.get(key).split(" - ")[0];
  }

  public void assertThatSalesOrderIsUpdated(
      String salesReturnOrderCode, DataTable salesReturnOrderDataTable) {
    Map<String, String> expectedSalesReturnOrderMap =
        salesReturnOrderDataTable.asMaps(String.class, String.class).get(0);

    Object actualSalesReturnOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedSalesReturnOrder",
            getSalesReturnOrderQueryParams(salesReturnOrderCode, expectedSalesReturnOrderMap));

    assertNotNull(
        ASSERTION_MSG + " Sales Return Order " + salesReturnOrderCode + " is not Updated",
        actualSalesReturnOrderModifiedDate);

    assertDateEquals(
        expectedSalesReturnOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(actualSalesReturnOrderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getSalesReturnOrderQueryParams(
      String salesReturnOrderCode, Map<String, String> expectedSalesReturnOrderMap) {
    return new Object[] {
      salesReturnOrderCode, expectedSalesReturnOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)
    };
  }

  public void assertThatSalesReturnOrderItemsDataIsUpdatedSuccessfully(
      String salesReturnOrderCode, DataTable salesReturnItemsDataTable) {
    List<Map<String, String>> salesReturnItems =
        salesReturnItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnItem : salesReturnItems) {
      assertThatSalesReturnOrderItemExists(salesReturnOrderCode, salesReturnItem);
    }
  }

  private void assertThatSalesReturnOrderItemExists(
      String salesReturnOrderCode, Map<String, String> itemDataMap) {
    Object item =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedIObSalesReturnOrderItem",
            constructSalesReturnOrderItemDataQueryParams(salesReturnOrderCode, itemDataMap));
    Assert.assertNotNull(
        ASSERTION_MSG
            + " Item data of sales return order "
            + salesReturnOrderCode
            + " with Item Id "
            + itemDataMap.get(IOrderFeatureFileCommonKeys.SRO_ITEM_ID)
            + " does not exist",
        item);
  }

  private Object[] constructSalesReturnOrderItemDataQueryParams(
      String salesReturnOrderCode, Map<String, String> itemDataMap) {
    return new Object[] {
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON),
      salesReturnOrderCode,
      Long.valueOf(itemDataMap.get(IOrderFeatureFileCommonKeys.SRO_ITEM_ID)),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY),
      itemDataMap.get(IOrderFeatureFileCommonKeys.SALES_PRICE),
      itemDataMap.get(IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT)
    };
  }

  public String getViewItemsDataUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.QUERY
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.ITEM_DATA;
  }
}
