package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.DObStockAvailabilitiesViewAllTestUtils;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderApproveTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesOrderApproveStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObSalesOrderApproveTestUtils utils;
  private DObStockAvailabilitiesViewAllTestUtils stockAvailabilitiesViewAllTestUtils;

  public DObSalesOrderApproveStep() {

    When(
        "^\"([^\"]*)\" requests to approve SalesOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getApproveSalesOrderRestUrl(salesOrderCode);
          Response response = utils.sendPUTRequest(userCookie, restUrl, new JsonObject());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable salesOrderDataTable) -> {
          utils.assertThatSalesOrderIsApproved(salesOrderCode, salesOrderDataTable);
        });

    Then(
        "^CycleDates of SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable cycleDatesDataTable) -> {
          utils.assertThatCycleDatesIsUpdated(salesOrderCode, cycleDatesDataTable);
        });

    Then(
        "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
        (String missingFields, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatResponseHasMissingFields(response, missingFields);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesOrderApproveTestUtils(getProperties(), entityManagerDatabaseConnector);
    stockAvailabilitiesViewAllTestUtils = new DObStockAvailabilitiesViewAllTestUtils(getProperties());
    stockAvailabilitiesViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Approve SalesOrder,")) {
      databaseConnector.createConnection();
          databaseConnector.executeSQLScript(
                  AccountingDocumentCommonTestUtils.clearAccountingDocuments());
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
          hasBeenExecutedOnce = true;
        databaseConnector.executeSQLScript(stockAvailabilitiesViewAllTestUtils.clearStockAvailabilities());
    }
  }

  @After
  public void afterSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Approve SalesOrder,")) {
      userActionsTestUtils.unlockAllSections(
          IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
