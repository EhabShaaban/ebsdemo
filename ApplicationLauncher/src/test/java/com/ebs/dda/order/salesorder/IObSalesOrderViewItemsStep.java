package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderViewItemsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObSalesOrderViewItemsTestUtils utils;

  public IObSalesOrderViewItemsStep() {

    Given(
            "^the following SalesOrders exist with the following Customers:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatTheFollowingSalesOrderDataExist(salesOrderDataTable);
        });

    Given(
        "^the following SalesOrders have the following Items:$",
        (DataTable salesOrderItemsDataTable) -> {
          utils.assertThatTheFollowingSalesOrderItemsExist(salesOrderItemsDataTable);
        });

    Given(
        "^the following SalesOrders have the following TaxDetails:$",
        (DataTable taxesDataTable) -> {
          utils.assertThatSalesOrderHasTheFollowingTaxes(taxesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view Items section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getViewSalesOrderItemsURL(salesOrderCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Items section for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable salesOrderItemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayedInItemSection(response, salesOrderItemsDataTable);
        });

    Then(
        "^the following values for TaxesSummary for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable taxesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemTaxesResponseIsCorrect(response, taxesDataTable);
        });

    Then(
        "^the following values for SalesOrderSummary for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable itemSummaryDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemSummaryResponseIsCorrect(response, itemSummaryDataTable);
        });

    And(
        "^ApprovalDate of SalesOrder with code \"([^\"]*)\" is \"([^\"]*)\"$",
        (String salesOrderCode, String approvalDate) -> {
          utils.assertThatSalesOrderApprovalDateIsCorrect(salesOrderCode, approvalDate);
        });

    Given(
        "^the lastSalesPrice data of item with code \"([^\"]*)\" and the following conditions exist with total number (\\d+):$",
        (String itemCode, Integer numberOfLastSalesPrices, DataTable lastSalesPriceDataTable) -> {
          utils.assertThatItemHasTheFollowingSalesPrices(
              itemCode, lastSalesPriceDataTable, numberOfLastSalesPrices);
        });

    Given(
        "^CancellationDate of SalesOrder with code \"([^\"]*)\" is \"([^\"]*)\"$",
        (String salesOrderCode, String cancellationDate) -> {
          utils.assertThatSalesOrderCancellationDateIsCorrect(salesOrderCode, cancellationDate);
        });

    And(
        "^the following SalesOrders have no CompanyAndStoreData:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatTheFollowingSalesOrdersHaveNoCompanyAndStoreData(salesOrderDataTable);
        });

    And(
        "^the following SalesOrders have no Items:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatTheFollowingSalesOrdersHaveNoItems(salesOrderDataTable);
        });

    And(
        "^TaxesSummary for SalesOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemTaxesResponseIsEmpty(response);
        });

    Then(
        "^Items section for SalesOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemsResponseIsEmpty(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSalesOrderViewItemsTestUtils(getProperties(), entityManagerDatabaseConnector);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View SalesOrder Items section")) {
      beforeSetup();
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View SalesOrder Items section")) {
      afterSetup();
    }
  }

  private void beforeSetup() throws Exception {
    databaseConnector.createConnection();
        databaseConnector.executeSQLScript(
                AccountingDocumentCommonTestUtils.clearAccountingDocuments());
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
  }

  private void afterSetup() throws Exception {
    databaseConnector.closeConnection();
  }
}
