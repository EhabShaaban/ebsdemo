package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;

import java.util.Map;

public class IObSalesReturnOrderRequestEditItemsDataTestUtils
    extends DObSalesReturnOrderCommonTestUtils {

  public static final String SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME = "SalesReturnOrderEntityLockCommand";

  public IObSalesReturnOrderRequestEditItemsDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }
  
}
