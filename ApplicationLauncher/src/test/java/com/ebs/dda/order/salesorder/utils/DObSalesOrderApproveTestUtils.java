package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesOrderApproveTestUtils extends DObSalesOrderCommonTestUtils {

    private final String ASSERTION_MSG = super.ASSERTION_MSG + "[Approve]";

    public DObSalesOrderApproveTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties);
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public String getDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append(super.getDbScriptsOneTimeExecution());
        str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
        str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
        str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
        str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
        str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
        str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
        str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
        str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
        str.append(",").append("db-scripts/sales/IObSalesInvoicePostingDetails.sql");
        str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
        str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");

        str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
        str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
        str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
        str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
        str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
        str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
        str.append(
                "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
        str.append(
                "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");

        str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
        str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
        str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
        str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
        str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
        str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");

        str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
        str.append(",")
                .append("db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
        str.append(",")
                .append("db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
        str.append(",").append("db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
        str.append(",")
                .append("db-scripts/inventory/stock-transformation/DObStockTransformationScript.sql");
        str.append(",")
                .append(
                        "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction.sql");
        str.append(",")
                .append(
                        "db-scripts/inventory/stock-transformation/IObStockTransformationDetailsScript.sql");
        return str.toString();
    }

    public String getDbScriptsExecution() {
        StringBuilder str = new StringBuilder();
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
        str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
        return str.toString();
    }

    public String getApproveSalesOrderRestUrl(String salesOrderCode) {
        return IDObSalesOrderRestURLs.COMMAND + "/" + salesOrderCode + IDObSalesOrderRestURLs.APPROVE;
    }

    public void assertThatSalesOrderIsApproved(String salesOrderCode, DataTable salesOrderDataTable) {
        Map<String, String> expectedApprovedSalesOrderMap =
                salesOrderDataTable.asMaps(String.class, String.class).get(0);

        Object actualSalesOrderModifiedDate =
                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                        "getApprovedSalesOrder",
                        getApprovedSalesOrderQueryParams(salesOrderCode, expectedApprovedSalesOrderMap));

        assertNotNull(
                ASSERTION_MSG + " Sales Order " + salesOrderCode + " is not approved",
                actualSalesOrderModifiedDate);

        assertDateEquals(
                expectedApprovedSalesOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                new DateTime(actualSalesOrderModifiedDate).withZone(DateTimeZone.UTC));
    }

    private Object[] getApprovedSalesOrderQueryParams(
            String salesOrderCode, Map<String, String> expectedApprovedSalesOrderMap) {
        return new Object[]{
                salesOrderCode,
                expectedApprovedSalesOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                "%" + expectedApprovedSalesOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
                expectedApprovedSalesOrderMap.get(ISalesOrderFeatureFileCommonKeys.TOTAL_AMOUNT),
                expectedApprovedSalesOrderMap.get(IFeatureFileCommonKeys.TOTAL_REMAINING)
        };
    }

    public void assertThatCycleDatesIsUpdated(String salesOrderCode, DataTable cycleDatesDataTable) {
        Map<String, String> expectedCycleDatesMap =
                cycleDatesDataTable.asMaps(String.class, String.class).get(0);

        Object[] actualCycleDates =
                (Object[])
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getUpdatedSalesOrderApprovalDate",
                                getUpdatedSalesOrderCycleDatesQueryParams(salesOrderCode, expectedCycleDatesMap));

        assertNotNull(
                ASSERTION_MSG + " Cycle Date of Sales Order " + salesOrderCode + " is not updated",
                actualCycleDates);

        assertDateEquals(
                expectedCycleDatesMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                new DateTime(actualCycleDates[0]).withZone(DateTimeZone.UTC));

        assertDateEquals(
                expectedCycleDatesMap.get(ISalesOrderFeatureFileCommonKeys.APPROVAL_DATE),
                new DateTime(actualCycleDates[1]).withZone(DateTimeZone.UTC));
    }

    private Object[] getUpdatedSalesOrderCycleDatesQueryParams(
            String salesOrderCode, Map<String, String> expectedApprovedSalesOrderMap) {
        return new Object[]{
                salesOrderCode, expectedApprovedSalesOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)
        };
    }
}
