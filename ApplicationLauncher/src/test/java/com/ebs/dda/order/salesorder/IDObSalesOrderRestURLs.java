package com.ebs.dda.order.salesorder;

public interface IDObSalesOrderRestURLs {

  String COMMAND = "/command/order/salesorder";
  String QUERY = "/services/order/salesorder";

  String CREATE = COMMAND + "/";
  String READ_ALL_SALES_ORDER_TYPES = QUERY + "/salesordertypes";
    String READ_ALL_ITEM_UOM = "/unitofmeasures";

  String VIEW_ALL_URL = QUERY;
  String GENERAL_DATA = "/generaldata";

  String AUTHORIZED_ACTIONS = QUERY + "/authorizedactions/";
  String VIEW_COMPANY_AND_STORE_DATA = "/companystoredata";
  String VIEW_ITEMS = "/items";
  String VIEW_SALES_ORDER_DATA = "/salesorderdata";

  String UNLOCK_LOCKED_SECTIONS = COMMAND + "/unlock/lockedsections/";

  String LOCK_COMPANY_AND_STORE_SECTION = "/lock/companystoredata";
  String UNLOCK_COMPANY_AND_STORE_SECTION = "/unlock/companystoredata";

  String LOCK_SALES_ORDER_DATA_SECTION = "/lock/salesorderdata";
  String UNLOCK_SALES_ORDER_SECTION = "/unlock/salesorderdata";
  String LOCK_SALES_ORDER_ITEMS_SECTION = "/lock/items";
    String UNLOCK_SALES_ORDER_ITEMS_SECTION = "/unlock/items";

    String DELETE_ITEMS = "/items/";

    String UPDATE_SALES_ORDER_DATA_SECTION = "/update/salesorderdata";

    String UPDATE_ITEMS = "/update/items";

    String UPDATE_COMPANY_AND_STORE_SECTION = "/update/companystoredata";
    String APPROVE = "/approve";

    String READ_APPROVED_SALES_ORDERS = "/getApprovedAndSalesInvoiceActivated";

}
