package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderItemDeleteTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderRequestEditItemsDataTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderRequestEditItemsDataStep extends SpringBootRunner implements En {

  private IObSalesReturnOrderRequestEditItemsDataTestUtils utils;
  private IObSalesReturnOrderItemDeleteTestUtils deleteTestUtils;

  public IObSalesReturnOrderRequestEditItemsDataStep() {

    When(
        "^\"([^\"]*)\" requests to Edit SROItem with id (\\d+) in SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, Integer itemId, String salesReturnCode) -> {
          String lockURL =
              IDObSalesReturnOrderRestURLs.REQUEST_EDIT_ITEM
                  + salesReturnCode
                  + IDObSalesReturnOrderRestURLs.LOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, salesReturnCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Edit SROItem dialoge is opened and SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String salesReturnCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesReturnCode,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION,
              IObSalesReturnOrderRequestEditItemsDataTestUtils
                  .SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" for SROItem with id (\\d+) is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesReturnCode, Integer itemId, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IDObSalesReturnOrderRestURLs.REQUEST_EDIT_ITEM
                  + salesReturnCode
                  + IDObSalesReturnOrderRestURLs.LOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          userActionsTestUtils.lockSection(userName, lockURL, salesReturnCode);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesReturnCode,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION,
              IObSalesReturnOrderRequestEditItemsDataTestUtils
                  .SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME);
        });

    When(
        "^\"([^\"]*)\" cancels saving SROItem with id (\\d+) in SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, Integer itemId, String salesReturnCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockURL =
              IDObSalesReturnOrderRestURLs.REQUEST_EDIT_ITEM
                  + salesReturnCode
                  + IDObSalesReturnOrderRestURLs.UNLOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" is released$",
        (String userName, String salesReturnCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesReturnCode,
              IObSalesReturnOrderRequestEditItemsDataTestUtils
                  .SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesInvoiceSectionNames.ITEM_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first requested to Edit SROItem with id (\\d+) in SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" successfully$",
        (String userName, Integer itemId, String salesReturnCode) -> {
          String lockURL =
              IDObSalesReturnOrderRestURLs.REQUEST_EDIT_ITEM
                  + salesReturnCode
                  + IDObSalesReturnOrderRestURLs.LOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          userActionsTestUtils.lockSection(userName, lockURL, salesReturnCode);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesReturnCode,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION,
              IObSalesReturnOrderRequestEditItemsDataTestUtils
                  .SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME);
        });

    Then(
        "^SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesReturnCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesReturnCode,
              IObSalesReturnOrderRequestEditItemsDataTestUtils
                  .SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving SROItem with id (\\d+) in SalesReturnOrderItems section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, Integer itemId, String salesReturnCode) -> {
          String unlockURL =
              IDObSalesReturnOrderRestURLs.REQUEST_EDIT_ITEM
                  + salesReturnCode
                  + IDObSalesReturnOrderRestURLs.UNLOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" first deleted SROItem with id (\\d+) of SalesReturnOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, Integer itemId, String salesReturnOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl =
              deleteTestUtils.getDeleteSalesReturnOrderItemRestUrl(salesReturnOrderCode, itemId);
          utils.sendDeleteRequest(userCookie, restUrl);
          deleteTestUtils.assertThatSalesReturnOrderItemIsDeletedSuccessfully(
              salesReturnOrderCode, itemId);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderRequestEditItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    deleteTestUtils =
        new IObSalesReturnOrderItemDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Edit SROItem to SalesReturnOrder")
        || contains(scenario.getName(), "Request to Cancel SROItem to SalesReturnOrder")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Edit SROItem to SalesReturnOrder")
        || contains(scenario.getName(), "Request to Cancel SROItem to SalesReturnOrder")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObSalesReturnOrderRestURLs.COMMAND + "/",
          IDObSalesReturnOrderRestURLs.UNLOCK_ALL_SECTION,
          utils.getSectionsNames());
    }
  }
}
