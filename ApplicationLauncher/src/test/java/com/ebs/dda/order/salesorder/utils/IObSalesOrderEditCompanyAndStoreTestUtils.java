package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import java.util.Map;

public class IObSalesOrderEditCompanyAndStoreTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderEditCompanyAndStoreTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");

    return str.toString();
  }

  public String getLockCompanyStoreUrl(String code) {
    return IDObSalesOrderRestURLs.COMMAND
        + "/"
        + code
        + IDObSalesOrderRestURLs.LOCK_COMPANY_AND_STORE_SECTION;
  }

  public String getUnLockCompanyStoreUrl(String code) {
    return IDObSalesOrderRestURLs.COMMAND
        + "/"
        + code
        + IDObSalesOrderRestURLs.UNLOCK_COMPANY_AND_STORE_SECTION;
  }

}
