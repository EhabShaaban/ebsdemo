package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObsalesOrderAllowedActionsHomeScreenStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private DObSalesOrderCommonTestUtils utils;

    public DObsalesOrderAllowedActionsHomeScreenStep() {

        When(
                "^\"([^\"]*)\" requests to read actions of SalesOrder home screen$",
                (String userName) -> {
                    String url = IDObSalesOrderRestURLs.AUTHORIZED_ACTIONS;
                    Response response =
                            utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObSalesOrderCommonTestUtils(getProperties());
        utils.setProperties(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in Sales Order Home Screen")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Read allowed actions in Sales Order Home Screen")) {
            databaseConnector.closeConnection();
        }
    }
}
