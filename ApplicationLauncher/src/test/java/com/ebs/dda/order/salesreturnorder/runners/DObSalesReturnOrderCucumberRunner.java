package com.ebs.dda.order.salesreturnorder.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/salesreturnorder/SRO_Create_HP.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Create_Val.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Create_Auth.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_View_Items.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_View_CompanyData.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_View_GeneralData.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Delete.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_View_ReturnDetails.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_DocumentOwner_Dropdown.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_ViewAll.feature",
      "classpath:features/ordermanagement/salesreturnorder/SalesReturnOrderTypes_Dropdowns.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_AllowedActions_HomeScreen.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_AllowedActions_ObjectScreen.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_RequestEdit_Item.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Mark_As_Shipped.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Item_Delete.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_ReturnReason_Dropdowns.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Save_Edit_Item_HP.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Save_Edit_Item_Auth.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_Save_Edit_Item_Val.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_ItemUOMs_Dropdown.feature",
      "classpath:features/ordermanagement/salesreturnorder/SRO_SalesInvoice_Dropdown.feature",
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.order.salesreturnorder"},
    strict = true,
    tags = "not @Future")
public class DObSalesReturnOrderCucumberRunner {}
