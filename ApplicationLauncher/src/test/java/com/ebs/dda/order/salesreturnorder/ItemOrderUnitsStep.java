package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewItemsDataTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.ItemOrderUnitsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class ItemOrderUnitsStep extends SpringBootRunner implements En {

  private ItemOrderUnitsTestUtils utils;
  private IObSalesReturnOrderViewItemsDataTestUtils salesReturnOrderViewItemsDataTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new ItemOrderUnitsTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesReturnOrderViewItemsDataTestUtils =
        new IObSalesReturnOrderViewItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  public ItemOrderUnitsStep() {

    Given(
        "^the total number of existing OrderUnits for Item \"([^\"]*)\" in SalesReturnOrder with code \"([^\"]*)\" is (\\d+)$",
        (String item, String salesReturnCode, Integer orderUnitsNumber) -> {
          utils.assertNumberOfItemOrderUnitsMatchesExpected(
              item, salesReturnCode, orderUnitsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read OrderUnits of Item \"([^\"]*)\" in SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String item, String salesReturnCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.readItemOrderUnits(cookie, salesReturnCode, item.split(" - ")[0]);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Item OrderUnits will be presented to \"([^\"]*)\":$",
        (String userName, DataTable itemOrderUnitsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemOrderUnitsResponseIsCorrect(response, itemOrderUnitsDataTable);
        });

    Then(
        "^total number of OrderUnits returned to \"([^\"]*)\" equals  (\\d+)$",
        (String userName, Integer orderUnitNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, orderUnitNumber);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of SalesReturnOrder Item OrderUnits -")) {
      databaseConnector.executeSQLScript(
          salesReturnOrderViewItemsDataTestUtils.clearSalesReturnOrder());
    }
  }
}
