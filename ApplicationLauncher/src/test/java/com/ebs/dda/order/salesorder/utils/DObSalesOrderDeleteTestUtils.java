package com.ebs.dda.order.salesorder.utils;

import static org.junit.Assert.assertNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import java.util.Map;

public class DObSalesOrderDeleteTestUtils extends DObSalesOrderCommonTestUtils {

  private IObSalesOrderEditCompanyAndStoreTestUtils editCompanyAndStoreTestUtils;
  private IObSalesOrderEditSalesOrderDataTestUtils editSalesOrderDataTestUtils;
  private IObSalesOrderRequestAddCancelItemTestUtils requestAddCancelItemTestUtils;

  public DObSalesOrderDeleteTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    return str.toString();
  }

  public void assertThatSalesOrderIsDeletedSuccessfully(String salesOrderCode) {
    Object actualSalesOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderByCode", salesOrderCode);
    assertNull("Sales Order does not deleted successfully", actualSalesOrder);
  }

  public String getDeleteSalesOrderRestUrl(String salesOrderCode) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IDObSalesOrderRestURLs.COMMAND);
    deleteUrl.append("/");
    deleteUrl.append(salesOrderCode);
    return deleteUrl.toString();
  }

  public String getLockUrlForSalesOrderSection(String salesOrderCode, String sectionName) {
    String lockUrl;
    switch (sectionName) {
      case IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION:
        lockUrl = editCompanyAndStoreTestUtils.getLockCompanyStoreUrl(salesOrderCode);
        break;
      case IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION:
        lockUrl = editSalesOrderDataTestUtils.getLockSalesOrderDataUrl(salesOrderCode);
        break;
      case IDObSalesOrderSectionNames.ITEMS_SECTION:
        lockUrl = requestAddCancelItemTestUtils.getLockItemsSectionUrl(salesOrderCode);
        break;
      default:
        lockUrl = "";
    }
    return lockUrl;
  }

  public void setEditCompanyAndStoreTestUtils(
      IObSalesOrderEditCompanyAndStoreTestUtils editCompanyAndStoreTestUtils) {
    this.editCompanyAndStoreTestUtils = editCompanyAndStoreTestUtils;
  }

  public void setEditSalesOrderDataTestUtils(
      IObSalesOrderEditSalesOrderDataTestUtils editSalesOrderDataTestUtils) {
    this.editSalesOrderDataTestUtils = editSalesOrderDataTestUtils;
  }

  public void setRequestAddCancelItemTestUtils(
      IObSalesOrderRequestAddCancelItemTestUtils requestAddCancelItemTestUtils) {
    this.requestAddCancelItemTestUtils = requestAddCancelItemTestUtils;
  }
}
