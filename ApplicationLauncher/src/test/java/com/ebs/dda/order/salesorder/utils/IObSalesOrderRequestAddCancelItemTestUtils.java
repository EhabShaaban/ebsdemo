package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import java.util.Map;

public class IObSalesOrderRequestAddCancelItemTestUtils extends IObSalesOrderItemsCommonTestUtils {

  public IObSalesOrderRequestAddCancelItemTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    return str.toString();
  }

  public String getUnLockItemsSectionUrl(String salesOrderCode) {
    StringBuilder unLockUrl = new StringBuilder();
    unLockUrl.append(IDObSalesOrderRestURLs.COMMAND);
    unLockUrl.append("/");
    unLockUrl.append(salesOrderCode);
    unLockUrl.append(IDObSalesOrderRestURLs.UNLOCK_SALES_ORDER_ITEMS_SECTION);
    return unLockUrl.toString();
  }
}
