package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderMarkAsShippedTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesReturnOrderMarkAsShippedStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderMarkAsShippedTestUtils utils;

  public DObSalesReturnOrderMarkAsShippedStep() {
    When(
        "^\"([^\"]*)\" requests to MarkAsShipped SalesReturnOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getSalesReturnOrderMarkAsShippedRestUrl(salesReturnOrderCode);
          Response response = utils.sendPUTRequest(userCookie, restUrl, new JsonObject());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesReturnOrder with code \"([^\"]*)\" is updated with shipped state as follows:$",
        (String salesReturnOrderCode, DataTable salesReturnOrderDataTable) -> {
          utils.assertThatSalesOrderStateAndModificationInfoIsUpdated(
              salesReturnOrderCode, salesReturnOrderDataTable);
        });

    Then(
        "^CycleDates of SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable cycleDatesDataTable) -> {
          utils.assertThatCycleDatesIsUpdated(salesReturnOrderCode, cycleDatesDataTable);
        });

    Then(
        "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
        (String missingFields, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatResponseHasMissingFields(response, missingFields);
        });

    Then(
        "^ReturnQuantity field in SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" is updated as follows:$",
        (String salesInvoiceCode, DataTable salesInvoiceItemDataTable) -> {
          utils.assertThatSalesInvoiceItemsUpdated(salesInvoiceCode, salesInvoiceItemDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesReturnOrderMarkAsShippedTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsShipped SalesReturnOrder")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "MarkAsShipped SalesReturnOrder")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObSalesReturnOrderRestURLs.COMMAND + "/",
          IDObSalesReturnOrderRestURLs.UNLOCK_ALL_SECTION,
          utils.getSectionsNames());
    }
  }
}
