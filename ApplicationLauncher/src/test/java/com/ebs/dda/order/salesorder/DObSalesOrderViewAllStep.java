package com.ebs.dda.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesOrderViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObSalesOrderViewAllTestUtils salesOrderViewAllTestUtils;

  public DObSalesOrderViewAllStep() {

    Given(
        "^the total number of records of SalesOrders is (\\d+)$",
        (Long totalRecordsNumber) -> {
          salesOrderViewAllTestUtils.assertThatTotalNumberOfSalesOrdersEquals(totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredData(
                  userCookie, IDObSalesOrderRestURLs.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesOrderViewAllTestUtils.assertResponseSuccessStatus(response);
          salesOrderViewAllTestUtils.assertSalesOrderDataIsCorrect(salesOrderDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesOrderViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on SOCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.USER_CODE, userCodeContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredData(
                  cookie, IDObSalesOrderRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on CustomerCode which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String customerCodeContains,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.CUSTOMER, customerCodeContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesOrderRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on CustomerName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String customerNameContains,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.CUSTOMER, customerNameContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesOrderRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on SalesResponsible which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String salesResponsibleContains,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME, salesResponsibleContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesOrderRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on ExpectedDeliveryDate which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName,
            Integer pageNumber,
            String expectedDeliveryDateContains,
            Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.EXPECTED_DELIVERY_DATE,
                  String.valueOf(
                      salesOrderViewAllTestUtils.getDateTimeInMilliSecondsFormat(
                          expectedDeliveryDateContains)));
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObSalesOrderRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String businessUnitContains,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME, businessUnitContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesOrderRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesOrders with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String stateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              salesOrderViewAllTestUtils.getContainsFilterField(
                  IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, stateContains);
          Response response =
              salesOrderViewAllTestUtils.requestToReadFilteredData(
                  cookie, IDObSalesOrderRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
    salesOrderViewAllTestUtils = new DObSalesOrderViewAllTestUtils(getProperties());
    salesOrderViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View all SalesOrders")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(salesOrderViewAllTestUtils.getDbScriptsExecution());
                hasBeenExecutedOnce = true;
            }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View all SalesOrders")) {
          databaseConnector.closeConnection();
      }
  }
}
