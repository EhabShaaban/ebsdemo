package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class DObSalesReturnOrderDeleteTestUtils extends DObSalesReturnOrderCommonTestUtils {

  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete]";

  public DObSalesReturnOrderDeleteTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatSalesReturnOrderIsDeletedSuccessfully(String salesReturnOrderCode) {
    Object actualSalesReturnOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderByCode", salesReturnOrderCode);
    assertNull(
        ASSERTION_MSG + "Sales Return Order does not deleted successfully", actualSalesReturnOrder);
  }

  public String getDeleteSalesReturnOrderRestUrl(String salesOrderCode) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IDObSalesReturnOrderRestURLs.COMMAND);
    deleteUrl.append("/");
    deleteUrl.append(salesOrderCode);
    return deleteUrl.toString();
  }
}
