package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderSaveEditItemTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewItemsDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderSaveEditItemStep extends SpringBootRunner implements En {

  private IObSalesReturnOrderSaveEditItemTestUtils utils;
  private IObSalesReturnOrderViewItemsDataTestUtils viewItemsDataTestUtils;

  public IObSalesReturnOrderSaveEditItemStep() {
    Given(
        "^Items section of SalesReturnOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesReturnOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockItemsSectionUrl(salesReturnOrderCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl, salesReturnOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesReturnOrderCode,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION,
              utils.SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME);
        });
    When(
        "^\"([^\"]*)\" saves the following edited SROItem with id (\\d+) in SalesReturnOrderItems section of SalesReturnOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            Integer itemId,
            String salesReturnOrderCode,
            String dateTime,
            DataTable ItemDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveItem(cookie, salesReturnOrderCode, itemId, ItemDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Items section of SalesReturnOrder with Code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable salesReturnItemsDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getViewItemsDataUrl(salesReturnOrderCode));
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertThatSalesReturnOrderItemsDataIsUpdatedSuccessfully(
              salesReturnOrderCode, salesReturnItemsDataTable);
        });
    Then(
        "^TaxesSummary section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable salesReturnTaxesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          viewItemsDataTestUtils.assertThatItemTaxesResponseIsCorrect(
              response, salesReturnTaxesDataTable);
        });

    Then(
        "^ItemsSummary section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode,
            String userName,
            DataTable salesReturnItemsSummaryDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          viewItemsDataTestUtils.assertThatItemSummaryResponseIsCorrect(
              response, salesReturnItemsSummaryDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Items section of SalesReturnOrder with Code \"([^\"]*)\" is released$",
        (String userName, String salesReturnOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesReturnOrderCode,
              utils.SALES_RETURN_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesReturnOrderSectionNames.ITEMS_SECTION);
        });

    Then(
        "^SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable salesReturnOrderDataTable) -> {
          utils.assertThatSalesOrderIsUpdated(salesReturnOrderCode, salesReturnOrderDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderSaveEditItemTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    viewItemsDataTestUtils =
        new IObSalesReturnOrderViewItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Edit SalesReturnOrder Item,")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Edit SalesReturnOrder Item,")) {
      utils.unfreeze();
    }
  }
}
