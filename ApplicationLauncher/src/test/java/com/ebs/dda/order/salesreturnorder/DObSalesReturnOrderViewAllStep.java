package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderGeneralModel;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesReturnOrderViewAllStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderViewAllTestUtils utils;

  public DObSalesReturnOrderViewAllStep() {

    Given(
        "^the total number of records of SalesReturnOrders is (\\d+)$",
        (Long totalRecordsNumber) -> {
          utils.assertThatTotalNumberOfSalesReturnOrdersEquals(totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesReturnOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertSalesReturnOrderDataIsCorrect(salesReturnOrderDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on SROCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObSalesReturnOrderGeneralModel.USER_CODE, userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String typeCodeEquals, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getEqualsFilterField(
                  IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_CODE, typeCodeEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on CreationDate which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String creationDateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObSalesReturnOrderGeneralModel.CREATION_DATE,
                  String.valueOf(utils.getDateTimeInMilliSecondsFormat(creationDateContains)));
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String businessUnitEquals, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getEqualsFilterField(
                  IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_CODE, businessUnitEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on Customer which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getContainsFilterField(
                  IDObSalesReturnOrderGeneralModel.CUSTOMER, filteringValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesReturnOrderRestURLs.VIEW_ALL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on SICode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String SICodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObSalesReturnOrderGeneralModel.SALES_INVOICE_CODE, SICodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesReturnOrders with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String stateName, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObSalesReturnOrderGeneralModel.CURRENT_STATES, stateName);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObSalesReturnOrderRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesReturnOrderViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all SalesReturnOrders,")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
