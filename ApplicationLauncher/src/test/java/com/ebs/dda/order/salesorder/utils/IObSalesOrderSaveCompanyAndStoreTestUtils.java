package com.ebs.dda.order.salesorder.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderCompanyAndStoreValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesOrderSaveCompanyAndStoreTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderSaveCompanyAndStoreTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveCompanyAndStoreData(
      String salesOrderCode, DataTable companyStoreDataTable, Cookie cookie) {
    JsonObject companyStoreJsonObject = getCompanyAndStoreJsonObject(companyStoreDataTable);
    String restURL = getSaveCompanyStoreURL(salesOrderCode);
    return sendPUTRequest(cookie, restURL, companyStoreJsonObject);
  }

  private String getSaveCompanyStoreURL(String salesOrderCode) {
    return IDObSalesOrderRestURLs.COMMAND
        + '/'
        + salesOrderCode
        + IDObSalesOrderRestURLs.UPDATE_COMPANY_AND_STORE_SECTION;
  }

  private JsonObject getCompanyAndStoreJsonObject(DataTable companyStoreDataTable) {
    JsonObject companyStoreJsonObject = new JsonObject();
    Map<String, String> companyStoreMap =
        companyStoreDataTable.asMaps(String.class, String.class).get(0);
    addValueToJsonObject(
        companyStoreJsonObject,
        IIObSalesOrderCompanyAndStoreValueObject.STORE_CODE,
        getFirstValue(companyStoreMap, IFeatureFileCommonKeys.STOREHOUSE));
    return companyStoreJsonObject;
  }

  public void assertThatCompanyAndStoreDataIsUpdatedAsFollows(
      String salesOrderCode, DataTable companyStoreDataTable) {
    Map<String, String> companyAndStoreDataMap =
        companyStoreDataTable.asMaps(String.class, String.class).get(0);

    Object companyAndStoreDataObject =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedIObSalesOrderCompanyAndStoreData",
            constructCompanyAndStoreDataQueryParams(salesOrderCode, companyAndStoreDataMap));

    assertNotNull("CompanyAndStoreDataObject does not exist", companyAndStoreDataObject);
  }

  private Object[] constructCompanyAndStoreDataQueryParams(
      String salesOrderCode, Map<String, String> companyAndStoreDataMap) {
    return new Object[] {
      companyAndStoreDataMap.get(IFeatureFileCommonKeys.STOREHOUSE), salesOrderCode
    };
  }
}
